<?php


class Freshbooks
{
	protected $oauth_consumer_key;
	protected $oauth_consumer_secret;
	protected $oauth_callback;
	protected $oauth_token;
	protected $oauth_token_secret;
	
	// $subdomain is the subdomain of the users Freshbooks account
	// (i.e. http://example.freshbooks.com; "example" being the subdomain).
	protected $subdomain;
	
	function __construct($oauth_consumer_key, $oauth_consumer_secret, $oauth_callback = NULL, $subdomain = NULL, $oauth_token = NULL, $oauth_token_secret = NULL)
	{
		$this->oauth_consumer_key = $oauth_consumer_key;
		$this->oauth_consumer_secret = $oauth_consumer_secret;
		
		// If a subdomain is not supplied, it will be set to the user who owns the application's subdomain,
		// This would be good if you are only authenticating yourself, and not other users.
		if($subdomain === NULL)
			$this->subdomain = $this->oauth_consumer_key;
		else
			$this->subdomain = $subdomain;
		
		if($oauth_callback)		$this->oauth_callback = $oauth_callback;
		if($oauth_token)		$this->oauth_token = $oauth_token;
		if($oauth_token_secret) $this->oauth_token_secret = $oauth_token_secret;
	}
	
	
	public function apiUrl() { return 'https://'.$this->subdomain.'.freshbooks.com/api/2.1/xml-in'; }
	public function accessTokenUrl()  { return 'https://'.$this->subdomain.'.freshbooks.com/oauth/oauth_access.php'; }
	public function authorizeUrl()    { return 'https://'.$this->subdomain.'.freshbooks.com/oauth/oauth_authorize.php'; }
	public function requestTokenUrl() { return 'https://'.$this->subdomain.'.freshbooks.com/oauth/oauth_request.php'; }
	
	
	public function createNonce($length)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$nonce = '';    
		for ($p = 0; $p < $length; $p++) {
		    $nonce .= $characters[mt_rand(0, strlen($characters)-1)];
		}
		return $nonce;
	}
	
	
	private function urlEncodeParams($params)
	{
		$postdata ='';
		foreach($params as $key => $value)
		{
		    if(!empty($postdata)) $postdata .= '&';
		    $postdata .= $key.'=';
		    $postdata .= urlencode($value);
		}
		return $postdata;
	}
	
	
	private function getRequestToken()
	{
		$params = array(
			'oauth_consumer_key' => $this->oauth_consumer_key,
			'oauth_callback' => $this->oauth_callback,
			'oauth_signature' => $this->oauth_consumer_secret. '&',
			'oauth_signature_method' => 'PLAINTEXT',
			'oauth_version' => '1.0',
			'oauth_timestamp' => time(),
			'oauth_nonce' => $this->createNonce(20),
		);
		
		
		return $this->OAuthRequest($this->requestTokenUrl(), $params);
	}
	
	
	public function getLoginUrl()
	{
		$token = $this->getRequestToken();
		$loginUrl = $this->authorizeUrl().'?oauth_token='.$token['oauth_token'];
		return $loginUrl;
	}
	
	
	public function getAccessToken($token, $verifier)
	{
		$params = array(
			'oauth_consumer_key' => $this->oauth_consumer_key,
			'oauth_token' => $token,
			'oauth_verifier' => $verifier,
			'oauth_signature' => $this->oauth_consumer_secret. '&',
			'oauth_signature_method' => 'PLAINTEXT',
			'oauth_version' => '1.0',
			'oauth_timestamp' => time(),
			'oauth_nonce' => $this->createNonce(20),
		);
		
		return $this->OAuthRequest($this->accessTokenUrl(), $params);
	}
	
	
	private function OAuthRequest($url, $params = array())
	{
		// URL encode our params
		$params = $this->urlEncodeParams($params);

		// send the request to FreshBooks
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res= curl_exec($ch);
         
		// parse the request
		$r = array();
		parse_str($res, $r);
	
		return $r;
	}
	
	
	private function buildAuthHeader()
	{
		$params = array(
			'oauth_version' => '1.0',
			'oauth_consumer_key' => $this->oauth_consumer_key,
			'oauth_token' => $this->oauth_token,
			'oauth_timestamp' => time(),
			'oauth_nonce' => $this->createNonce(20),
			'oauth_signature_method' => 'PLAINTEXT',
			'oauth_signature' => $this->oauth_consumer_secret. '&' .$this->oauth_token_secret
		);

		$auth = 'OAuth realm=""';
		foreach($params as $kk => $vv)
		{
			$auth .= ','.$kk . '="' . urlencode($vv) . '"';
		}
		
		return $auth;
	}
	
	
	public function post($request)
	{
		$headers = array(
		            'Authorization: '.$this->buildAuthHeader().'',
		            'Content-Type: application/xml; charset=UTF-8',
		            'Accept: application/xml; charset=UTF-8',
		            'User-Agent: My-Freshbooks-App-1.0');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apiUrl());
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

		$response = curl_exec($ch);
		
		curl_close($ch);
		$response = new SimpleXMLElement($response);
	   
		if($response->attributes()->status == 'ok')
			return $response;
		elseif($response->attributes()->status == 'fail' || $response->error)
		     {
			throw new FreshbooksAPIError($response->error);
		         
		     }
		else{
			throw new FreshbooksError('Oops, something went wrong. :(');
	}
	}
	
	// ------------- Helper functions to get different datasets -------------------- //

	//
	// This function will return all the categories on the account.
	//
	
	function add_customers($customer)
	{
		
		$data = array();
		
		$request = $customer;
	
		$c = $this->_get_data($request);
      
		if(is_null($this->fberror)) {
			foreach($c->categories AS $key => $row)
				foreach($row AS $key2 => $row2)
					$data[] = $row2;
        }
				
		return $data;
	}
	
		function add_customer_data($customer)
	{
		
		$data = array();
		
		$request = $customer;
	
		$c = $this->_get_data($request);
      
		if(is_null($this->fberror)) {
			foreach($c->categories AS $key => $row)
				foreach($row AS $key2 => $row2)
					$data[] = $row2;
        }else if($this->fberror){
			return $this->fberror;
		}
				
		return $c->client_id;
	}
	
	function add_items($item)
	{
		
		$data = array();
		
		$request = $item;
	
		$c = $this->_get_data($request);
      
		if(is_null($this->fberror)) {
			foreach($c->categories AS $key => $row)
				foreach($row AS $key2 => $row2)
					$data[] = $row2;
        }
				
		return $data;
	}
	
	function add_taxs($tax)
	{
		
		$data = array();
		
		$request = $tax;
	
		$c = $this->_get_data($request);
      
		if(is_null($this->fberror)) {
			foreach($c->categories AS $key => $row)
				foreach($row AS $key2 => $row2)
					$data[] = $row2;
        }
				
		return $data;
	}
	
	function add_invoices($invoices)
	{
		
		$data = array();
		
		$request = $invoices;
	
		$c = $this->_get_data($request);
	
		if(is_null($this->fberror)) {
	
		   if($c->attributes()->status=='ok')
		   {
		     return  ($c->invoice_id)?$c->invoice_id:true ;  
		   }else{
		         return false;
		   }
			foreach($c->categories AS $key => $row)
				foreach($row AS $key2 => $row2)
					$data[] = $row2;
					
        }else{
            return false;
        }
				
    	return $data;
	}
	
	
	function add_payment($payments)
	{
		
		$data = array();
		
		$request = $payments;
	
		$c = $this->_get_data($payments);
	   
	    
		if(is_null($this->fberror)) {
			foreach($c->categories AS $key => $row)
				foreach($row AS $key2 => $row2)
					$data[] = $row2;
					
        }
        
          $pid=  json_decode($c->payment_id);
           
        	if($pid)
			{    
			    
			   
			     $data['payment_id'] = $pid;
			      $data['status'] = 'ok';
			
		     }else
		     {
		           $data['status'] = 'error';
		    
		         
		     }
	
				
    	return $data;
	}
	
	
		
	function edit_payment($payments)
	{
		
		$data = array();
		
		$request = $payments;
	
		$c = $this->_get_data($payments);
	  
	    
		if(is_null($this->fberror)) {
		    
		      if($c->attributes()->status=='ok')
		        $res = true;
        }else{
           $res = false; 
        }
        
        	if($res)
			{    
			  
			      $data['status'] = 'ok';
			
		     }else
		     {
		           $data['status'] = 'error';
		    
		         
		     }
	
				
    	return $data;
        
         
	}
	
	
	function add_categories($xml)
	{
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			
			$request = $xml;
			//$request = $this->_build_xml('category.create', array('name' => 'gasoline'));	
		
			$c = $this->_get_data($request);
			if(is_null($this->fberror)) {
				foreach($c->categories AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
	
				// Loop through the next pages
				if($loop) {
					$pages = (int) $this->_get_data($request)->categories->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_categories($nextpage, $count));		
					}
				}
			}
					
			return $data;
		}
		return 0;
	}
	
	
	function get_categories($page = 1, $count = 100, $loop = TRUE)
	{
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			$request = $this->_build_xml('category.list', array('page' => $page, 'per_page' => $count));	
			$c = $this->_get_data($request);

			if(is_null($this->fberror)) {
				foreach($c->categories AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
	
				// Loop through the next pages
				if($loop) {
					$pages = (int) $this->_get_data($request)->categories->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_categories($nextpage, $count));		
					}
				}
			}
					
			return $data;
		}
		return 0;
	}

	//
	// This function will return all the expenses on the account.
	//
	function get_expenses($page = 1, $count = 100, $loop = TRUE)
	{
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			$request = $this->_build_xml('expense.list', array('page' => $page, 'per_page' => $count));
			$c = $this->_get_data($request);

			if(is_null($this->fberror)) {
				foreach($c->expenses AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
						
				// Loop through the next pages
				if($loop) {
					$pages = (int) $this->_get_data($request)->expenses->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_expenses($nextpage, $count));		
					}
				}
			}
					
			return $data;
		}
		return 0;
	}

	//
	// This function will return all the payments on the account.
	//
	function get_payments($page = 1, $count = 100, $loop = TRUE)
	{
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			$request = $this->_build_xml('payment.list', array('page' => $page, 'per_page' => $count));	
			$c = $this->_get_data($request);
			
			if(is_null($this->fberror)) {
				foreach($c->payments AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
				
				// Loop through the next pages
				if($loop) {
					$pages = (int) $this->_get_data($request)->payments->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_payments($nextpage, $count));		
					}
				}
			}
					
			return $data;
		}
		return 0;
	}

	//
	// This function will return all the customers in the account.
	//
	
	
	function get_customers_data($from=NULL, $to=NULL,$page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			
			$x_data = array('page' => $page, 'per_page' => $count);
			if($from)
			$x_data['updated_from'] =$from;
			
				if($to)
					$x_data['updated_to'] =$to;
			
			$request = $this->_build_xml('client.list', $x_data);	
			
		
			$c = $this->_get_data($request);
			
			
			if(is_null($this->fberror)) {
				foreach($c->clients AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->clients->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_customers($nextpage, $count));		
					}
				}
			}
			
			return $data;
			
		}
		
	}
	
	
	
	function get_items_data($from=NULL, $to=NULL,$page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			
				$x_data = array('page' => $page, 'per_page' => $count);
			if($from)
			$x_data['updated_from'] =$from;
			
				if($to)
					$x_data['updated_to'] =$to;
			
			$request = $this->_build_xml('item.list', $x_data);	
			
		
			$c = $this->_get_data($request);
		
		
			if(is_null($this->fberror)) {
				foreach($c->items AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->items->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_items($nextpage, $count));		
					}
				}
			}
			
			return $data;
			
		}
		
	}
	
	function get_invoices_data($from=NULL, $to=NULL,$page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			
				$x_data = array('page' => $page, 'per_page' => $count);
			if($from)
			$x_data['updated_from'] =$from;
			
				if($to)
					$x_data['updated_to'] =$to;
			$request = $this->_build_xml('invoice.list', $x_data);	
			
		    
			$c = $this->_get_data($request);
		  
			if(is_null($this->fberror)) {
				foreach($c->invoices AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->invoices->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_invoices($nextpage, $count));
					}
				}
			}
			
			return $data;
			
		}
		
	}


   function get_taxlist_data($from=NULL, $to=NULL,$page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
		
				$x_data = array('page' => $page, 'per_page' => $count);
			if($from)
			$x_data['updated_from'] =$from;
			
				if($to)
					$x_data['updated_to'] =$to;
			$request = $this->_build_xml('tax.list', $x_data);	
			$c = $this->_get_data($request);
		
		
			if(is_null($this->fberror)) {
				foreach($c->taxes AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->taxes->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_taxlist($nextpage, $count));		
					}
				}
			}
			
			return $data;
			
		}
		
	}
	
	function get_customers($page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			$request = $this->_build_xml('client.list', array('page' => $page, 'per_page' => $count));	
			
		
			$c = $this->_get_data($request);
			
			if(is_null($this->fberror)) {
				foreach($c->clients AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->clients->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_customers($nextpage, $count));		
					}
				}
			}
			
			return $data;
			
		}
		
	}
	
	function get_items($page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			$request = $this->_build_xml('item.list', array('page' => $page, 'per_page' => $count));	
			
		
			$c = $this->_get_data($request);
		
		
			if(is_null($this->fberror)) {
				foreach($c->items AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->items->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_items($nextpage, $count));		
					}
				}
			}
			
			return $data;
			
		}
		
	}
	
	function get_invoices($page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			$request = $this->_build_xml('invoice.list', array('page' => $page, 'per_page' => $count));	
			
		    
			$c = $this->_get_data($request);
		  
			if(is_null($this->fberror)) {
				foreach($c->invoices AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->invoices->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_invoices($nextpage, $count));
					}
				}
			}
			
			return $data;
			
		}
		
	}
	
	function get_taxlist($page = 1, $count = 100, $loop = TRUE)
	{
	   
		if($this->oauth_token_secret && $this->oauth_token) { 
			$data = array();
			$request = $this->_build_xml('tax.list', array('page' => $page, 'per_page' => $count));	
			
		
			$c = $this->_get_data($request);
		
		
			if(is_null($this->fberror)) {
				foreach($c->taxes AS $key => $row)
					foreach($row AS $key2 => $row2)
						$data[] = $row2;
		
				// Loop through the next pages
				if($loop) {
				   
					$pages = (int) $this->_get_data($request)->taxes->attributes()->pages;
					if(($pages > 1) && ($pages >= $page)) {
						$nextpage = $page + 1;
						$data = array_merge($data, $this->get_taxlist($nextpage, $count));		
					}
				}
			}
			
			return $data;
			
		}
		
	}
	//
	// Build the XML request.
	//
	private function _build_xml($method, $data)
	{
		$request = '<?xml version="1.0" encoding="utf-8"?><request method="' . $method . '">';
		
		foreach($data AS $key => $row)
			$request .= "<$key>$row</$key>";
		
		$request .= '</request>';
		return $request;
	}
	
	//
	// Make the data request.
	//
	private function _get_data($request)
	{
		$this->fberror = NULL;
		try {
		  $clients = $this->post($request);
	
		 
	
		  return $clients;
		}
		catch(FreshbooksError $e)
		{
		  $this->fberror = $e->getMessage();
		  return 0;
		}	
	}
	
	
}

class FreshbooksError extends Exception {}
class FreshbooksAPIError extends FreshbooksError {}



	