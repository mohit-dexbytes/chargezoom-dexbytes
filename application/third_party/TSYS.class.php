<?php
class TSYS {
    public $environment = 'sandbox';
    public $urlSandbox = 'https://stagegw.transnox.com/servlets/Transnox_API_server';
    public $urlProduction = 'https://gateway.transit-pass.com/servlets/TransNox_API_Server';
    public $urlLocalDev = 'https://localhost/charge-zoom/';
    public $apiKey = '';
    public $deviceID = '';
    public $transactionKey = '';

    public function statusCheck() {return $this->request(array('url' => '/fphc'));}

    ////////////////////
    // Authentication Generate Token //
    ////////////////////
    public function generateToken($userID, $password, $mID) {
        $obj = [];
        $url = $this->urlProduction;
        if ($this->environment == 'sandbox') {$url = $this->urlSandbox;}
        if ($this->environment == 'local') {$url = $this->urlLocalDev;}
        $obj['GenerateKey'] = array('mid' => $mID, 'userID' => $userID,'password' => $password ); 
     
        return $this->request(array(
            'method' => 'POST',
            'url' => $url,
            'fields' => $obj  
        ));
    }

    

    //////////////////
    // Transactions //
    //////////////////
    public function processTransaction($transaction) {
        $obj = [];
        $url = $this->urlProduction;
        $transactionKey = $this->transactionKey;
        if ($this->environment == 'sandbox') {$url = $this->urlSandbox;}
        if ($this->environment == 'local') {$url = $this->urlLocalDev;}
        return $this->request(array(
            'method' => 'POST',
            'url' => $url,
            'type' => 'transaction',
            'fields' => $transaction  
        ));
    }

    public function getTransaction($transactionID) {
        return $this->request(array(
            'apiKey' => $this->apiKey,
            'method' => 'GET',
            'url' => '/transaction/'.$transactionID
        ));
    }

    public function captureTransaction($transactionID) {


        $obj = [];
        $url = $this->urlProduction;
        $transactionKey = $this->transactionKey;
        $deviceID = $this->deviceID;
       
        if ($this->environment == 'sandbox') {$url = $this->urlSandbox;}
        if ($this->environment == 'local') {$url = $this->urlLocalDev;}
        $obj['Capture'] = array('deviceID' => $deviceID,'transactionKey' =>$transactionKey,'transactionID' => $transactionID);
        
        return $this->request(array(
            'method' => 'POST',
            'url' => $url,
            'type' => 'transaction',
            'fields' => $obj  
        ));
    }

    public function voidTransaction($transactionID) {
        $obj = [];
        $url = $this->urlProduction;
        $transactionKey = $this->transactionKey;
        $deviceID = $this->deviceID;
       
        if ($this->environment == 'sandbox') {$url = $this->urlSandbox;}
        if ($this->environment == 'local') {$url = $this->urlLocalDev;}
        $obj['Void'] = array('deviceID' => $deviceID,'transactionKey' =>$transactionKey,'transactionID' => $transactionID,'voidReason' => 'DEVICE_UNAVAILABLE');
        
        return $this->request(array(
            'method' => 'POST',
            'url' => $url,
            'type' => 'transaction',
            'fields' => $obj  
        ));
    }

    public function refundTransaction($transactionID, $refund) {
        $obj = [];
        $url = $this->urlProduction;
        $transactionKey = $this->transactionKey;
        $deviceID = $this->deviceID;
        if ($this->environment == 'sandbox') {$url = $this->urlSandbox;}
        if ($this->environment == 'local') {$url = $this->urlLocalDev;}
        $obj['Return'] = array('deviceID' => $deviceID,'transactionKey' =>$transactionKey,'transactionID' => $transactionID);
        return $this->request(array(
            'method' => 'POST',
            'url' => $url,
            'type' => 'transaction',
            'fields' => $obj  
        ));
       
    }

    ///////////////
    // Recurring //
    ///////////////
    

    ///////////////
    // Terminals //
    ///////////////
    

    private function request(array $options) {
        $url = $this->urlProduction;
        if ($this->environment == 'sandbox') {$url = $this->urlSandbox;}
        if ($this->environment == 'local') {$url = $this->urlLocalDev;}

        $ch = curl_init();
        $header = array('Content-Type: application/json');
        $curlConfig = array(
            CURLOPT_HTTPHEADER     => $header,
            CURLOPT_URL            => $options['url'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => 0,
        );
        if (array_key_exists('method', $options)) {
            if (strtolower($options['method']) == 'post') {
                $curlConfig[CURLOPT_POST] = true;
            }
            if (strtolower($options['method']) == 'delete') {
                $curlConfig[CURLOPT_CUSTOMREQUEST] = "DELETE";
            }
        }
        if (array_key_exists('fields', $options) && count($options['fields']) > 0) {
            $curlConfig[CURLOPT_POSTFIELDS] = json_encode($options['fields']);
        } else{
            if (array_key_exists('method', $options)) {
                if (strtolower($options['method']) == 'post') {
                    $curlConfig[CURLOPT_CUSTOMREQUEST] = "POST";
                    unset($curlConfig[CURLOPT_POST]);
                }
            }
        }
        
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        if(isset($options['type']) && $options['type'] == 'transaction'){
         
        }
        
        if (!$result) { return $result; }

        return json_decode($result, true);
    }
}