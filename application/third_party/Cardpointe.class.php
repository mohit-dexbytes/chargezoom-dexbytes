<?php

class Cardpointe {   

    public function authorize($siteName='fts',$merchantid, $cardpointeuser, $cardpointepass, $account, $expry, $amount, $cvv, $name, $baddress1, $city, $state, $postal = '')
    {
        if(ENVIRONMENT == 'production'){
            $url = 'https://'.$siteName.'.cardconnect.com/cardconnect/rest';
        }else{
            $url = 'https://'.$siteName.'-uat.cardconnect.com/cardconnect/rest';
        }
        if($postal == ''){
            $postal = '55555';
        }
        $amount = sprintf("%.2f", $amount);
        $data = json_encode(array(
            'merchid' => $merchantid, // this will be gotten dynamically depending on what the user has inputed on adding the merchant as initial
            'account' => $account,
            'expiry' => $expry,
            'amount' => $amount,
            'capture' => 'n',
            'cvv2' => $cvv,
            "name" => $name,
            "address" => $baddress1,
            "city" => $city,
            "region" => $state,
            'postal' => $postal,
            "cof" => "M",
            "cofscheduled" => "N",
            "ecomind" => "E"
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$cardpointeuser:$cardpointepass");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        if(empty($result)){
            $result['resptext'] = ' Gateway Failure - No Response';
            $result['retref'] = 'TXNFAILED' . time();
            $result['respcode'] = 400;
            
        }
        return $result;  
    }

    public function authorize_capture($siteName='fts',$merchantid, $cardpointeuser, $cardpointepass, $account, $expry, $amount, $cvv='', $name='', $baddress1='', $city='', $state='', $postal = '')
    {
        if(ENVIRONMENT == 'production'){
            $url = 'https://'.$siteName.'.cardconnect.com/cardconnect/rest';
        }else{
            $url = 'https://'.$siteName.'-uat.cardconnect.com/cardconnect/rest';
        }
        $amount = sprintf("%.2f", $amount);
        if($postal == ''){
            $postal = '55555';
        }
        $data = json_encode(array(
            'merchid' => $merchantid,
            'account' => $account,
            'expiry' => $expry,
            'amount' => $amount,
            'currency' => 'USD',
            'capture' => 'Y',
            'receipt' => 'Y',
            'cvv2' => $cvv,
            "name" => $name,
            "address" => $baddress1,
            "city" => $city,
            "region" => $state,
            'postal' => $postal,
            "cof" => "M",
            "cofscheduled" => "N",
            "ecomind" => "E"
        ));
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$cardpointeuser:$cardpointepass");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);  

        $result = json_decode($result, true);
        if(empty($result)){
            $result['resptext'] = ' Gateway Failure - No Response';
            $result['retref'] = 'TXNFAILED' . time();
            $result['respcode'] = 400;
        }
        return $result;
    }

    public function capture($siteName='fts',$merchantid, $cardpointeuser, $cardpointepass, $retref, $amount)
    {
        if(ENVIRONMENT == 'production'){
            $url = 'https://'.$siteName.'.cardconnect.com/cardconnect/rest';
        }else{
            $url = 'https://'.$siteName.'-uat.cardconnect.com/cardconnect/rest';
        }
        $amount = sprintf("%.2f", $amount);
        $data = json_encode(array(
            'retref' => $retref,
            'merchid' => $merchantid,
            'amount' => $amount
        ));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/capture');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$cardpointeuser:$cardpointepass");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);  
        $result = json_decode($result, true);
        if(empty($result)){
            $result['resptext'] = ' Gateway Failure - No Response';
            $result['retref'] = 'TXNFAILED' . time();
            $result['respcode'] = 400;
        }
        return $result;
    }

    public function refund($siteName='fts',$merchantid, $cardpointeuser, $cardpointepass, $retref, $amount)
    {
        if(ENVIRONMENT == 'production'){
            $url = 'https://'.$siteName.'.cardconnect.com/cardconnect/rest';
        }else{
            $url = 'https://'.$siteName.'-uat.cardconnect.com/cardconnect/rest';
        }
        $amount = sprintf("%.2f", $amount);
        $data = json_encode(array(
            "retref" => $retref,
            "merchid" => $merchantid,
            "amount" => $amount, 
        ));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/refund');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$cardpointeuser:$cardpointepass");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);  
        $result = json_decode($result, true);
        if(empty($result)){
            $result['resptext'] = ' Gateway Failure - No Response';
            $result['retref'] = 'TXNFAILED' . time();
            $result['respcode'] = 400;
        }
        return $result;
    }

    public function void($siteName='fts',$merchantid, $cardpointeuser, $cardpointepass, $retref)
    {
        if(ENVIRONMENT == 'production'){
            $url = 'https://'.$siteName.'.cardconnect.com/cardconnect/rest';
        }else{
            $url = 'https://'.$siteName.'-uat.cardconnect.com/cardconnect/rest';
        }
        $data = json_encode(array(
            "retref" => $retref,
            "merchid" => $merchantid,
        ));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/void');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$cardpointeuser:$cardpointepass");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);  
        $result = json_decode($result, true);
        if(empty($result)){
            $result['resptext'] = ' Gateway Failure - No Response';
            $result['retref'] = 'TXNFAILED' . time();
            $result['respcode'] = 400;
        }
        return $result;
    }

    public function ach_capture($siteName='fts',$merchantid, $cardpointeuser, $cardpointepass, $account, $routing, $amount, $account_type, $name, $baddress1, $city, $state, $postal = '')
    {
        if(ENVIRONMENT == 'production'){
            $url = 'https://'.$siteName.'.cardconnect.com/cardconnect/rest';
        }else{
            $url = 'https://'.$siteName.'-uat.cardconnect.com/cardconnect/rest';
        }
        $amount = sprintf("%.2f", $amount);
        if($postal == ''){
            $postal = '55555';
        }
        $data = json_encode(array(
            'accttype' => $account_type,
            'merchid' => $merchantid,
            'account' => $account,
            'bankaba' => $routing,
            'amount' => $amount,
            'capture' => 'Y',
            'name' => $name,
            "address" => $baddress1,
            "city" => $city,
            "region" => $state,
            'postal' => $postal,
            "cof" => "M",
            "cofscheduled" => "N",
            "ecomind" => "E"
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$cardpointeuser:$cardpointepass");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);  
        $result = json_decode($result, true);
        if(empty($result)){
            $result['resptext'] = ' Gateway Failure - No Response';
            $result['retref'] = 'TXNFAILED' . time();
            $result['respcode'] = 400;
        }
        return $result;
    }

    public function authorize_no($siteName='fts', $merchantid, $cardpointeuser, $cardpointepass, $account, $expry, $amount, $name, $baddress1, $city, $state, $postal = '')
    {
        if(ENVIRONMENT == 'production'){
            $url = 'https://'.$siteName.'.cardconnect.com/cardconnect/rest';
        }else{
            $url = 'https://'.$siteName.'-uat.cardconnect.com/cardconnect/rest';
        }
        $amount = sprintf("%.2f", $amount);
        if($postal == ''){
            $postal = '55555';
        }
        $data = json_encode(array(
            'merchid' => $merchantid,
            'account' => $account,
            'expiry' => $expry,
            'amount' => $amount,
            'capture' => 'Y',
            'receipt' => 'Y',
            "name" => $name,
            "address" => $baddress1,
            "city" => $city,
            "region" => $state,
            'postal' => $postal,
            "cof" => "M",
            "cofscheduled" => "N",
            "ecomind" => "E"
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$cardpointeuser:$cardpointepass");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);  
        $result = json_decode($result, true);
        if(empty($result)){
            $result['resptext'] = ' Gateway Failure - No Response';
            $result['retref'] = 'TXNFAILED' . time();
            $result['respcode'] = 400;
        }
        return $result;
    }
}