<?php
/**
 * Copyright (c) Payroc LLC 2017.
 */

namespace iTransact\iTransactSDK {

    require_once (__DIR__.'/Models/CardPayload.php');
    require_once (__DIR__.'/Models/AddressPayload.php');
    require_once (__DIR__.'/Models/ACHPayload.php');
    require_once (__DIR__.'/Models/IdPayload.php');
    require_once (__DIR__.'/Models/TransactionPayload.php');
    require_once (__DIR__.'/Models/ACHTransactionPayload.php');

    use iTransact\iTransactSDK\CardPayload;
    use iTransact\iTransactSDK\ACHPayload;
    use iTransact\iTransactSDK\AddressPayload;
    use iTransact\iTransactSDK\IdPayload;
    use iTransact\iTransactSDK\TransactionPayload;
    use iTransact\iTransactSDK\ACHTransactionPayload;

    /**
     * Class iTransactSDK - does the majority of the work behind the scenes.
     *
     *
     * @package iTransact\iTransactSDK
     * @author Preston Farr
     * @copyright Payroc LLC
     * @example
     * $trans = new iTTransaction();
     *
     * $trans::postCardTransaction($username, $somekey, $payload);
     */
    class iTransactSDK
    {
        /**
         *
         */
        const API_BASE_URL = "https://api.itransact.com";

        /**
         * @param string $apiUsername
         * @param string $apiKey
         * @param TransactionPayload $payload
         *
         * @return array
         */
        public static function generateHeaderArray($apiUsername, $apiKey, $payload)
        {
            $payloadSignature = self::signPayload($apiKey, $payload);
            $encodedUsername = base64_encode($apiUsername);
            return array(
                'Content-Type: application/json',
                'Authorization: ' . $encodedUsername . ':' . $payloadSignature
            );
        }

        /**
         * Workaround for the const not being able to have an expression assigned at runtime.
         *
         * @return string
         */
        public static function API_POST_TOKENS_URL(){
            return self::API_BASE_URL . "/tokens";
        }

        /**
         * Workaround for the const not being able to have an expression assigned at runtime.
         *
         * @return string
         */
        public static function API_GET_TOKENS_URL(){
            return self::API_POST_TOKENS_URL() . "/"; // Just add token id at the end
        }

        /**
         * Workaround for the const not being able to have an expression assigned at runtime.
         *
         * @return string
         */
        public static function API_POST_TRANSACTIONS_URL(){
            return self::API_BASE_URL . "/transactions";
        }

        /**
         * Workaround for the const not being able to have an expression assigned at runtime.
         *
         * @return string
         */
        public static function API_PATCH_VOID_TRANSACTIONS_URL($id){
            return self::API_BASE_URL . "/transactions/$id/void";
        }

        public static function API_GET_TRANSACTIONS_URL_BY_ID($id){
            return self::API_BASE_URL . "/transactions/$id";
        }


        public static function API_PATCH_REFUND_TRANSACTIONS_URL($id){
            return self::API_BASE_URL . "/transactions/$id/credit";
        }

        public static function API_PATCH_CAPTURE_AUTH_TRANSACTIONS_URL($id){
            return self::API_BASE_URL . "/transactions/$id/capture";
        }

        /**
         * Workaround for the const not being able to have an expression assigned at runtime.
         *
         * @return string
         */
        public static function API_GET_TRANSACTIONS_URL(){
            return self::API_POST_TRANSACTIONS_URL() . "/"; // Just add transaction id at the end
        }

        public static function API_POST_CREATE_CUSTOMER_URL(){
            return self::API_BASE_URL . "/customers";
        }

        /**
         * @param string $apikey
         * @param string $payload
         *
         * @return string
         */
        public static function signPayload($apikey, $payload)
        {
            $digest = hash_hmac('sha256', json_encode($payload), $apikey, true);
            return base64_encode($digest);
        }
    }

    /**
     * Publicly available
     *
     * Class iTTransaction
     * @package iTransact\iTransactSDK
     * @see
     */
    class iTTransaction
    {
        /**
         * Submits signed payload to iTransact's JSON API Gateway
         *
         * Use this function to generate headers (signing the payload in the process) and submit the transaction.
         *
         * @param string $apiUsername
         * @param string $apiKey
         * @param TransactionPayload $payload
         *
         * @return mixed
         */
        public function postCardTransaction($apiUsername, $apiKey, $payload)
        {
            $headers = iTransactSDK::generateHeaderArray($apiUsername, $apiKey, $payload);

            $ch = curl_init(iTransactSDK::API_POST_TRANSACTIONS_URL());
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($response === FALSE) {
                die(curl_error($ch));
            }

            $response = json_decode($response, TRUE);
            $response['status_code'] = $httpcode;
            return $response;
        }

        public function postACHTransaction($apiUsername, $apiKey, $payload)
        {
            $headers = iTransactSDK::generateHeaderArray($apiUsername, $apiKey, $payload);

            $ch = curl_init(iTransactSDK::API_POST_TRANSACTIONS_URL());
            $curlArray = array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS => json_encode($payload)
            );
            curl_setopt_array($ch, $curlArray);

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($response === FALSE) {
                die(curl_error($ch));
            }
        
            $response = json_decode($response, TRUE);
            $response['status_code'] = $httpcode;
            return $response;
        }

        public function postCreateCustomer($apiUsername, $apiKey, $payload)
        {
            $headers = iTransactSDK::generateHeaderArray($apiUsername, $apiKey, $payload);

            $ch = curl_init(iTransactSDK::API_POST_CREATE_CUSTOMER_URL());
            $curlArray = array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS => json_encode($payload)
            );
            curl_setopt_array($ch, $curlArray);

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($response === FALSE) {
                die(curl_error($ch));
            }
        
            $response = json_decode($response, TRUE);
            $response['status_code'] = $httpcode;
            return $response;
        }

        /**
         * Signs payload using the api key and returns only the signature.
         *
         * If you change your payload size AT ALL this signature (HMAC string) will also change
         *
         * @param string $apikey
         * @param string $payload
         *
         * @return string
         */
        public function signPayload($apikey, $payload)
        {
            return iTransactSDK::signPayload($apikey, $payload);
        }

        /**
         * Submits signed payload to iTransact's JSON API Gateway
         *
         * Use this function to generate headers (signing the payload in the process) and submit the transaction.
         *
         * @param string $apiUsername
         * @param string $apiKey
         * @param TransactionPayload $payload
         *
         * @return mixed
         */
        public function voidCardTransaction($apiUsername, $apiKey, $payload, $id)
        {
            $headers = iTransactSDK::generateHeaderArray($apiUsername, $apiKey, $payload);
            $ch = curl_init(iTransactSDK::API_PATCH_VOID_TRANSACTIONS_URL($id));
            curl_setopt_array($ch, array(
                // CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($response === FALSE) {
                die(curl_error($ch));
            }

            $response = json_decode($response, TRUE);
            $response['status_code'] = $httpcode;
            return $response;
        }

        public function refundTransaction($apiUsername, $apiKey, $payload, $id)
        {
            // echo "$apiKey";die;
            $headers = iTransactSDK::generateHeaderArray($apiUsername, $apiKey, $payload);
            $ch = curl_init(iTransactSDK::API_PATCH_REFUND_TRANSACTIONS_URL($id));
            curl_setopt_array($ch, array(
                // CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($response === FALSE) {
                die(curl_error($ch));
            }

            $response = json_decode($response, TRUE);
            $response['status_code'] = $httpcode;
            return $response;
        }

        public function captureAuthTransaction($apiUsername, $apiKey, $payload, $id)
        {
            // echo "$apiKey";die;
            $headers = iTransactSDK::generateHeaderArray($apiUsername, $apiKey, $payload);
            $ch = curl_init(iTransactSDK::API_PATCH_CAPTURE_AUTH_TRANSACTIONS_URL($id));
            curl_setopt_array($ch, array(
                // CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($response === FALSE) {
                die(curl_error($ch));
            }

            $response = json_decode($response, TRUE);
            $response['status_code'] = $httpcode;
            return $response;
        }
        // TODO - add other SDK methods for ACH, etc.
        /**
         * Submits signed payload to iTransact's JSON API Gateway
         *
         * Use this function to generate headers (signing the payload in the process) and submit the transaction.
         *
         * @param string $apiUsername
         * @param string $apiKey
         * @param TransactionID $id
         *
         * @return mixed
         */
        public function getTransactionByID($apiUsername, $apiKey, $payload,$id)
        {
            $headers = iTransactSDK::generateHeaderArray($apiUsername, $apiKey, $payload);
            $ch = curl_init(iTransactSDK::API_GET_TRANSACTIONS_URL_BY_ID($id));
            curl_setopt_array($ch, array(
                // CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($response === FALSE) {
                die(curl_error($ch));
            }

            $response = json_decode($response, TRUE);
            $response['status_code'] = $httpcode;
            return $response;
        }
    }
}