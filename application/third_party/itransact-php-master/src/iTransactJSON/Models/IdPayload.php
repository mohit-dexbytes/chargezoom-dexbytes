<?php
/**
 * Copyright (c) Payroc LLC 2017.
 */

/**
 * Created by PhpStorm.
 * User: prestonf
 * Date: 8/2/17
 * Time: 3:49 PM
 */

namespace iTransact\iTransactSDK;


/**
 * Class Address
 * @package iTransact\iTransactSDK
 */
class IdPayload
{

    public $id;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * IdPayload constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

}