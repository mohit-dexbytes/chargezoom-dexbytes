<?php


/*
use Sami\Sami;
use Sami\RemoteRepository\GitHubRemoteRepository;
use Symfony\Component\Finder\Finder;

echo $dir = __DIR__ . '/src';
$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->in($dir = __DIR__ . '/src');
echo "dddd".print_r($iterator); die;
return new Sami($iterator, [
    'title' => 'GlobalPayments\Api',
    'build_dir' => dirname($dir) . '/docs',
    'cache_dir' => dirname($dir) . '/docs-cache',
    'remote_repository' => new GitHubRemoteRepository('globalpayments/php-sdk', dirname($dir)),
]);


*/


spl_autoload_register(function ($class) {
	  $prefix = 'GlobalPayments\\API\\';
		$base_dir = __DIR__ . DIRECTORY_SEPARATOR;
		$len = strlen($prefix);
		if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }
		$relative_class = substr($class, $len);
		$filewithoutExtension = $base_dir . str_replace('\\', '/', $relative_class);
		$file =  $filewithoutExtension. '.php';

		//Below str_replace is for local testing. Remove it before putting on Composer.
	  if (file_exists($file) ) {
	        require ($file);
	   }
	});
