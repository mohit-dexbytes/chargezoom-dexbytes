<?php
class QBD_Sync
{
    public function __construct($merchantID)
    {
        $this->CI = &get_instance();
        
        $this->merchantID = $merchantID; 
		$this->CI->load->model('general_model');
        $merchantData    = $this->CI->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID));
        $this->resellerID = $merchantData['resellerID']; 
        $this->merchantData = $merchantData; 
        $comp_data   = $this->CI->general_model->get_row_data('tbl_company',array('merchantID'=>$merchantID));
        $this->qbwc_username = $comp_data['qbwc_username'];

        $this->CI->load->config('quickbooks');
        $this->CI->load->model('quickbooks_surcharge');
        $this->CI->quickbooks_surcharge->dsn('mysqli://' . $this->CI->db->username . ':' . $this->CI->db->password . '@' . $this->CI->db->hostname . '/' . $this->CI->db->database);
    }

    public function invoiceSync($args){
        $invoicesToSynnc = $invoicesNotToSynnc = [];
        $args['merchantID'] = $this->merchantID;
        $getInvoiceData = $this->CI->general_model->get_invoice_details_data($args);
        if($getInvoiceData){
            foreach ($getInvoiceData as $key => $invoiceData) {

                if(in_array($invoiceData['id'], $invoicesNotToSynnc)){
                    continue;
                }

                if(!is_numeric($invoiceData['Item_ListID']) && !is_numeric($invoiceData['Customer_ListID'])){
                    $invoicesToSynnc[$invoiceData['id']] = $invoiceData['invoiceID'];
                } else {
                    if(isset($invoicesToSynnc[$invoiceData['id']])){
                        unset($invoicesToSynnc[$invoiceData['id']]);
                    }
                    $invoicesNotToSynnc[] = $invoiceData['id'];
                }
            }


            if(!empty($invoicesToSynnc)){
                foreach ($invoicesToSynnc as $invSync) {
                    if(!is_numeric($invSync)){
                        $que_data = $this->CI->general_model->get_row_data('quickbooks_queue', array('ident' => $invSync));
                        if (!empty($que_data)) {
                            if ($que_data['qb_status'] == 's') {
                                $this->CI->quickbooks_surcharge->enqueue(QUICKBOOKS_MOD_INVOICE, $invSync, '1', '', $this->qbwc_username);
                            }
                
                            if ($que_data['qb_status'] == 'e' || $que_data['qb_status'] == 'i') {
                
                                $this->CI->quickbooks_surcharge->enqueue(QUICKBOOKS_MOD_INVOICE, $invSync, '1', '', $this->qbwc_username);
                            }
                        } else {
                            $this->CI->quickbooks_surcharge->enqueue(QUICKBOOKS_MOD_INVOICE, $invSync, '1', '', $this->qbwc_username);
                        }
                    } else {
                        $this->CI->quickbooks_surcharge->enqueue(QUICKBOOKS_ADD_INVOICE, $invSync, '1', '', $this->qbwc_username);
                    }
                }
            }
        }
        return $invoicesToSynnc;
    }
     /**
     * Create Default sync data for QBD Surcharing
     *
     * @param int $merchantID
     *
     * @return 
     */
    public function createDefaultSyncRecords(int $merchantID)
    {
        $comp_data = $this->CI->general_model->get_row_data('tbl_company', array('merchantID' =>    $merchantID));

        $salesAccountData = [
            'accountName' => 'Surcharge Income',
            'accountType' => 'Income',
            'merchantID' => $merchantID,
            'companyID' => $comp_data['id'],
            'qbwc_username' => $comp_data['qbwc_username']
        ];
        $sale_acc_id = $this->addSyncSettingsQBDAccount($salesAccountData);
        
        $salesItemData = [
            'productName' => 'Surcharge Fees',
            'accountID' => $sale_acc_id,
            'merchantID' => $merchantID,
            'companyID' => $comp_data['id'],
            'qbwc_username' => $comp_data['qbwc_username']
        ];

        $item_id = $this->addSyncSettingsonQBDItems($salesItemData);

        $insert_data = [];
        $insert_data['merchantID'] = $merchantID;
        

        $insert_data['defaultItemAccount'] = isset($sale_acc_id) ? $sale_acc_id : '';
        $insert_data['defaultItem'] = isset($item_id) ? $item_id : '';

        $already_exist = $this->CI->general_model->get_select_data('tbl_merchant_surcharge', ['id'], ['merchantID' => $merchantID]);
        if($already_exist){
            
            $this->CI->general_model->update_row_data('tbl_merchant_surcharge', array('merchantID' => $merchantID), $insert_data);
        }else{
            
            $insert_data['createdAt'] = date('Y-m-d H:i:s');
            $id = $this->CI->general_model->insert_row('tbl_merchant_surcharge', $insert_data);
            
        }
    }
    /**
     * Create sync QBD Account
     *
     * @param array $data
     *
     * @return account id
     */
    public function addSyncSettingsQBDAccount(array $data)
    {
        
        $already_exist = $this->CI->general_model->get_select_data('qb_item_account', ['ListID'], ['qbAccountmerchantID' => $data['merchantID'],'company' => $data['companyID'],'AccountType' => 'Income','IsActive' => 'true','Name' => $data['accountName']]);
        
        if(isset($already_exist['ListID']) && $already_exist){
            $a_List = $already_exist['ListID'];
        }else{
            $input_data = [];
            $a_List = mt_rand(4000000, 9000000);
            $input_data['ListID'] = $a_List;
            $input_data['Name'] = $data['accountName'];
            $input_data['TimeCreated'] = date('Y-m-d H:i:s');
            $input_data['TimeModified'] = date('Y-m-d H:i:s');
            $input_data['FullName'] = $data['accountName'];
            $input_data['IsActive'] = 'true';
            $input_data['AccountType'] = $data['accountType'];
            $input_data['Description'] = $data['accountName'];
            $input_data['company'] = $data['companyID'];
            $input_data['qbAccountmerchantID'] = $data['merchantID'];
            $accID = $this->CI->general_model->insert_row('qb_item_account', $input_data);
            $this->CI->quickbooks_pos_sync->enqueue(QUICKBOOKS_ADD_ACCOUNT, $accID, '1', '', $data['qbwc_username']);
        }
        
        return $a_List;
    }
    
    /**
     * Create sync QBD Items
     *
     * @param array $data
     *
     * @return item id
     */
    public function addSyncSettingsonQBDItems(array $data)
    {
        $already_exist_product = $this->CI->general_model->get_select_data('qb_test_item', ['ListID'], ['companyListID' => $data['companyID'],'IsActive' =>'true','FullName' => 'Surcharge Fees']);
        if(isset($already_exist_product['ListID']) && $already_exist_product){
            $lsID = $already_exist_product['ListID'];
        }else{
            $itemType = 'Service';
            $input_data['productName']  = $data['productName'];
            $input_data['productType'] = 'Service';
            $input_data['productQty'] = '1';
            $input_data['IsActive'] = 'true';
            
            $user = $data['qbwc_username'];
            $input_data['companyID'] = $data['companyID'];
            $input_data['AccountRef'] = $data['accountID'];
            $input_data['Discount'] = 0;
            $input_data['parentID'] = '';
            $input_data['parentFullName'] = '';
            $input_data['accountFullName'] = '';
            $input_data['productPrice'] = 0;
            $input_data['productDescription'] = $data['productName'];
            $input_data['merchantID'] = $data['merchantID'];
            $input_data['createdat'] = date('Y-m-d H:i:s');
            $input_data['updatedat'] = date('Y-m-d H:i:s');
            $input_data['qb_action'] = "Add Item";
            $lsID = mt_rand('4500000', '14458000');
            $input_data['productListID'] = $lsID;
            $proID =  $this->CI->general_model->insert_row('tbl_custom_product', $input_data);

            $product_data = array(
                'ListID' => $lsID, 'IsActive' => 'true', 'TimeCreated' => date('Y-m-d H:i:s'),
                'Name' => $input_data['productName'], 'FullName' => $input_data['productName'], 'Type' => $itemType,
                'SalesPrice' => $input_data['productPrice'], 'SalesDesc' => $input_data['productDescription'], 'AccountRef' => $input_data['AccountRef'],
                'Name' => $input_data['productName'], 'Discount' => $input_data['Discount'], 'Type' => $itemType,
                'Parent_ListID' => $input_data['parentID'], 'Parent_FullName' => $input_data['parentFullName'], 'companyListID' => $input_data['companyID']
            );
            $qb_item_id = $this->CI->general_model->insert_row('qb_test_item', $product_data);
        }
        return $lsID;
    }
}