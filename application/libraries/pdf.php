<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(1);

class pdf {
    
    public function __construct()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
         
        if ($params == NULL)
        {
            $param = '"en-GB-x","A4","","",10,10,10,10,6,3';         
        }
         
        return new mPDF($param);
    }
}


/*

class pdf 
{ 
    function __construct()
    { 
        include_once APPPATH.'third_party\mpdf_new\vendor\autoload.php'; 
    } 
    function pdf()
    { 
        $CI = & get_instance(); 
        log_message('Debug', 'mPDF class is loaded.'); 
    } 
    function load($param=[])
    { 
        return new \Mpdf\Mpdf($param); 
    }
}
*/

?>