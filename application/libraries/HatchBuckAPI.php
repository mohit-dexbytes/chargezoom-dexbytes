<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class HatchBuckAPI
{
	protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
            // Assign the CodeIgniter super-object
            $this->CI =& get_instance();
            $this->CI->load->model('general_model');

    }

    public function createContact($request_data = [])
    {

        $merchID = $request_data['merchID'];
        $url = HATCHBUCK_API_URL."contact?api_key=".HATCHBUCK_API_KEY;

        $request_payload = json_encode([
            'firstName' => $request_data['firstName'],
            'lastName' => $request_data['lastName'],
            'company' => $request_data['companyName'],
            'emails' => [
                [
                  'address' => $request_data['merchantEmail'],
                  'type' => 'Work'
                ]
            ],
            "phones" => [
                [
                  "number" => $request_data['merchantContact'],
                  'type' => 'Work'
                ]
            ],
            'tags' => [['name' => 'Merchant']],
            'status' => [
                'name' => 'Customer',
            ],
            'addresses' => [
                [
                  'street' => $request_data['merchantAddress1'],
                  'city' => $request_data['merchantCity'],
                  'state' => $request_data['merchantState'],
                  'country' => $request_data['merchantCountry'],
                  'type' => 'Work'
                ]
            ],
            'customFields' => [
                [
                    'name' => 'Merchant Type',
                    'type' => 'Text',
                    'value' => $request_data['merchant_type']
                ]
            ]
        ]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_payload);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json"
        ));
        $response = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
       
        if($response_code == 200){
            $response_payload = json_decode($response, 1);
            $this->CI->general_model->update_row_data('tbl_merchant_data', ['merchID' => $merchID], ['hatchbuck_contact_id' => $response_payload['contactId']]);
            $this->addTagsInContact($response_payload['contactId']);
            $contactID = $response_payload['contactId'];
        }else{
            $contactID = 0;
        }

        $insertArray = [
            'request_url' => $url,
            'api_type' => 'create_contact',
            'request_payload' => $request_payload,
            'response_payload' => $response,
            'status' => ($response_code == 200) ? 'success' : 'failure',
            'created_date' => date('Y-m-d H:i:s'),
        ];
        $this->CI->general_model->insert_row('hatchbuck_api_logs', $insertArray);

        return ['success' => ($response_code == 200) ? 'success' : 'failure','contactID' => $contactID];
    }

    // function for create talent contact on hatchbuck
    public function createTalentContact($request_data = [])
    {
        $url = HATCHBUCK_API_URL."contact?api_key=".HATCHBUCK_API_KEY;

        $request_payload = json_encode([
            'firstName' => $request_data['firstName'],
            'lastName' => $request_data['lastName'],
            'company' => null,
            'emails' => [
                [
                  'address' => $request_data['email'],
                  'type' => 'Work'
                ]
            ],
            "phones" => [
                [
                  "number" => $request_data['phone_number'],
                  'type' => 'Work'
                ]
            ],
            'tags' => [['name' => 'Talent']],
            'status' => [
                'name' => 'New Application Queue',
            ]
        ]);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_payload);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json"
        ));
        $response = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $insertArray = [
            'request_url' => $url,
            'api_type' => 'create_talent_contact',
            'request_payload' => $request_payload,
            'response_payload' => $response,
            'status' => ($response_code == 200) ? 'success' : 'failure',
            'created_date' => date('Y-m-d H:i:s'),
        ];
        $this->CI->general_model->insert_row('hatchbuck_api_logs', $insertArray);

        return ['success' => ($response_code == 200) ? 'success' : 'failure'];
    }

    // function for update contact on hatchbuck
    public function updateTalentContact($request_data = [])
    {
        $url = HATCHBUCK_API_URL."contact?api_key=".HATCHBUCK_API_KEY;

        $request_payload = json_encode([
            'contactId' => $request_data['contactId'],
            'firstName' => $request_data['firstName'],
            'lastName' => $request_data['lastName'],
            'company' => null,
            'emails' => [
                [
                  'address' => $request_data['email'],
                  'type' => 'Work'
                ]
            ],
            "phones" => [
                [
                  "number" => $request_data['phone_number'],
                  'type' => 'Work'
                ]
            ],
            'tags' => [['name' => 'Talent']],
            'status' => [
                'name' => $request_data['status'],
            ]
        ]);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_payload);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json"
        ));
        $response = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $hatchbuck_contact_id = '';
        if($response_code == 200){
            $response_payload = json_decode($response, 1);
            $hatchbuck_contact_id = $response_payload['contactId'];
        }

        $insertArray = [
            'request_url' => $url,
            'api_type' => 'update_talent_contact',
            'request_payload' => $request_payload,
            'response_payload' => $response,
            'status' => ($response_code == 200) ? 'success' : 'failure',
            'created_date' => date('Y-m-d H:i:s'),
        ];
        $this->CI->general_model->insert_row('hatchbuck_api_logs', $insertArray);

        return ['success' => ($response_code == 200) ? 'success' : 'failure', 'contact_id' => $hatchbuck_contact_id];
    }
    // function for update contact on hatchbuck
    public function updateContact($request_data = [])
    {
        $url = HATCHBUCK_API_URL."contact?api_key=".HATCHBUCK_API_KEY;

        $request_payload = json_encode([
            'firstName' => $request_data['firstName'],
            'lastName' => $request_data['lastName'],
            'company' => $request_data['companyName'],
            'emails' => [
                [
                  'address' => $request_data['merchantEmail'],
                  'type' => 'Work'
                ]
            ],
            "phones" => [
                [
                  "number" => $request_data['merchantContact'],
                  'type' => 'Work'
                ]
            ],
            'tags' => [['name' => 'Merchant']],
            'status' => [
                'name' => 'Customer',
            ],
            'addresses' => [
                [
                  'street' => $request_data['merchantAddress1'],
                  'city' => $request_data['merchantCity'],
                  'state' => $request_data['merchantState'],
                  'country' => $request_data['merchantCountry'],
                  'type' => 'Work'
                ]
            ],
        ]);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_payload);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json"
        ));
        $response = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $hatchbuck_contact_id = '';
        if($response_code == 200){
            $response_payload = json_decode($response, 1);
            $hatchbuck_contact_id = $response_payload['contactId'];
        }

        $insertArray = [
            'request_url' => $url,
            'api_type' => 'update_talent_contact',
            'request_payload' => $request_payload,
            'response_payload' => $response,
            'status' => ($response_code == 200) ? 'success' : 'failure',
            'created_date' => date('Y-m-d H:i:s'),
        ];
        $this->CI->general_model->insert_row('hatchbuck_api_logs', $insertArray);

        return ['success' => ($response_code == 200) ? 'success' : 'failure', 'contact_id' => $hatchbuck_contact_id];
    }
    // function for add tag in contact on hatchbuck
    public function addTagsInContact($contactId){
        
        $url = HATCHBUCK_API_URL."contact/".$contactId."/tags?api_key=".HATCHBUCK_API_KEY;
        $request_payload = json_encode([['name' => 'Buyer']]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_payload);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json"
        ));
        $response = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $insertArray = [
            'request_url' => $url,
            'api_type' => 'add_tag',
            'request_payload' => $request_payload,
            'response_payload' => $response,
            'status' => ($response_code == 201) ? 'success' : 'failure',
            'created_date' => date('Y-m-d H:i:s'),
        ];
        $this->CI->general_model->insert_row('hatchbuck_api_logs', $insertArray);

        return ['success' => ($response_code == 201) ? 'success' : 'failure'];

    }

    // function for add contact campaign
    public function addContactCampaign($contactId, $campaign_name){

        $url = HATCHBUCK_API_URL."contact/".$contactId."/campaign?api_key=".HATCHBUCK_API_KEY;
        // get campaign name from env file
        $request_payload = json_encode([['id' => $campaign_name]]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_payload);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json"
        ));
        $response = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $data = json_decode($response);
        
        if(isset($data->Message) && $data->Message == 'EmailId is not registered.'){
           $statusCode = 400; 
        }else{
            $statusCode = $response_code;
        }
        $insertArray = [
            'request_url' => $url,
            'api_type' => 'add_campaign',
            'request_payload' => $request_payload,
            'response_payload' => $response,
            'status' => ($response_code == 201) ? 'success' : 'failure',
            'created_date' => date('Y-m-d H:i:s'),
        ];
        $this->CI->general_model->insert_row('hatchbuck_api_logs', $insertArray);

        return ['success' => ($response_code == 201) ? 'success' : 'failure','statusCode' => $statusCode];

    }
}