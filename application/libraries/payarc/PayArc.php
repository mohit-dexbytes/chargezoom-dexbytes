<?php

namespace PayArc;

/**
 * Class PayArc
 *
 * @package PayArc
 */
Class PayArc{

    // @var string the api mode either sandbox or production
    protected $apiMode = 'sandbox';

    // @var string The PayArc API key to be used for requests.
    protected $secretKey;

    // @var string|null The version of the Stripe API to use for requests.
    public static $apiVersion = null;

    // @var string|null The account ID for connected accounts requests.
    public static $accountId = null;

    // @var boolean Defaults to true.
    public static $verifySslCerts = true;

    private static $isMbstringAvailable = null;
    private static $isHashEqualsAvailable = null;


    const VERSION = '1.0';

    /**
     * @return string The API key used for requests.
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * Sets the API key to be used for requests.
     *
     * @param string $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    /**
     * Sets the api Mode to be used for transactions.
     *
     * @param string $apiMode
     */
    public function setApiMode($apiMode)
    {
        $this->apiMode = $apiMode ? $apiMode : 'sandbox';
    }

    /**
     * @return string The API version used for requests. null if we're using the
     *    latest version.
     */
    public static function getApiVersion()
    {
        return self::$apiVersion;
    }

    /**
     * @param string $apiVersion The API version to use for requests.
     */
    public static function setApiVersion($apiVersion)
    {
        self::$apiVersion = $apiVersion;
    }

    /**
     * @return boolean
     */
    public static function getVerifySslCerts()
    {
        return self::$verifySslCerts;
    }

    /**
     * @param boolean $verify
     */
    public static function setVerifySslCerts($verify)
    {
        self::$verifySslCerts = $verify;
    }

    
    /**
     * @param string|mixed $value A string to UTF8-encode.
     *
     * @return string|mixed The UTF8-encoded string, or the object passed in if
     *    it wasn't a string.
     */
    public static function utf8($value)
    {
        if (self::$isMbstringAvailable === null) {
            self::$isMbstringAvailable = function_exists('mb_detect_encoding');

            if (!self::$isMbstringAvailable) {
                trigger_error("It looks like the mbstring extension is not enabled. " .
                    "UTF-8 strings will not properly be encoded. Ask your system. ", E_USER_WARNING);
            }
        }

        if (is_string($value) && self::$isMbstringAvailable && mb_detect_encoding($value, "UTF-8", true) != "UTF-8") {
            return utf8_encode($value);
        } else {
            return $value;
        }
    }

    /**
     * @param array $arr A map of param keys to values.
     * @param string|null $prefix
     *
     * @return string A querystring, essentially.
     */
    public static function urlEncode($arr, $prefix = null)
    {
        if (!is_array($arr)) {
            return $arr;
        }

        $r = array();
        foreach ($arr as $k => $v) {
            if (is_null($v)) {
                continue;
            }

            if ($prefix) {
                if ($k !== null && (!is_int($k) || is_array($v))) {
                    $k = $prefix."[".$k."]";
                } else {
                    $k = $prefix."[]";
                }
            }

            if (is_array($v)) {
                $enc = self::urlEncode($v, $k);
                if ($enc) {
                    $r[] = $enc;
                }
            } else {
                $r[] = urlencode($k)."=".urlencode($v);
            }
        }

        return implode("&", $r);
    }

    public function createHeaders($method=null) {
        $headers = [
            'Authorization: Bearer '.$this->getSecretKey(),
            'Accept: application/json',
        ];

        if($method == 'POST' || $method == 'PATCH') {
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }

        return $headers;
    }

    public function getCompatibleCountryCode($name='')
    {

        $array = [
            'INDIA' => 'IN',
            'UNITED KINGDOM' => 'UK',
            'BRITAIN' => 'UK',
            'UNITED STATE' => 'US',
            'UNITED STATES' => 'US',
            'UNITED STATES OF AMERICA' => 'US',
            'USA' => 'US',
            'AMERICA' => 'US',
            'FRANCE' => 'FR',
            'ITALY' => 'IT',
            'CANADA' => 'CA',
            'GERMANY' => 'DE'
        ];

        if(!empty($name) && isset($array[strtoupper($name)]) ) {
            return $array[strtoupper($name)];
        }

        return '';
    }

    public function logData($str, $title=null){
        // Log for few days only
        $log = (date('Y-m-d') < '2021-07-02');
        if($log){
            $fp = fopen('./uploads/payarc-log.txt', 'a');

            fwrite($fp, "\r\n".'-------'. ($title ? $title : '')  .'-'. date('c').'-------'."\r\n");
            fwrite($fp, $str);

            fclose($fp);
        }
    }

    public function createCreditCardToken($card_number, $exp_month, $exp_year, $cvv=null, $address_info=[]){
        
        $params = [];
        $params['card_source'] = 'INTERNET';
        $params['card_number'] = $card_number;
        $params['exp_month'] = $exp_month;
        $params['exp_year'] = $exp_year;
        $params['cvv'] = $cvv;

        if(!empty($address_info)) {
            if(!empty($address_info['address_line1'])){
                $params['address_line1'] = $address_info['address_line1'];
            }
            if(!empty($address_info['address_line2'])){
                $params['address_line2'] = $address_info['address_line2'];
            }
            if(!empty($address_info['state'])){
                $params['state'] = $address_info['state'];
            }
            if(!empty($address_info['country'])){
                $params['country'] = $this->getCompatibleCountryCode($address_info['country']);
            }
            
        }
        
        $token_response = $this->request('POST', '/tokens',  $params);

        $this->logData(json_encode($token_response), 'token');

        return $token_response;
    }

    public function createCharge($data)
    {
        

        $params = $data;

        if(isset($params['phone_number']) && $params['phone_number'] != '')
        {
            $params['phone_number'] = str_replace(['+','-','.','(',')'], '', $params['phone_number']);
            
            $params['phone_number'] = filter_var($params['phone_number'], FILTER_SANITIZE_NUMBER_INT);

            if( !( strlen($params['phone_number']) > 2 && strlen($params['phone_number']) < 12) )
            {
                $params['phone_number'] = '';
            }
        }

        if(isset($params['statement_description']) && $params['statement_description'] != ''){
            if( strlen($params['statement_description']) > 25 ){
                $params['statement_description'] = substr($params['statement_description'], 0, 25);
            }
        }

        if(isset($params['email']) && $params['email'] != ''){
            if( filter_var($params['email'], FILTER_VALIDATE_EMAIL) == false){
                $params['email'] = '';
            }
        }


        if(isset($params['ship_to_zip']) && $params['ship_to_zip'] != ''){

            $params['ship_to_zip'] = str_replace(['+','-','.','(',')'], '', $params['ship_to_zip']);
            
            $params['ship_to_zip'] = filter_var($params['ship_to_zip'], FILTER_SANITIZE_NUMBER_INT);

            if( !( strlen($params['ship_to_zip']) > 1 && strlen($params['ship_to_zip']) < 10) )
            {
                unset($params['ship_to_zip']);
            }
        }

        // Log Request Payload
        $this->logData(json_encode($params), 'create-charge-request');
        
        // Make Request
        $response = $this->request('POST', '/charges',  $params);

        $response_body = json_decode($response['response_body'], 1);

        if(isset($response_body['status']) && $response_body['status'] == 'error' && isset($response_body['errors'])){
            $messages = [];
            foreach($response_body['errors'] as $error){
                $messages[] = implode("\r\n", $error);
            }
            $response_body['real_message'] = $response_body['message'];
            $response_body['message'] = implode("\r\n", $messages); 

            $response['response_body'] = json_encode($response_body);
        }

        // Response Payload
        $this->logData($response['response_body'], 'create-charge-response');

        return $response;
    }

    public function retrieveCharge($charge_id)
    {
        
        $params = [];

        return $this->request('GET', '/charges/'.$charge_id,  $params);
    }

    // Capture a charge
    public function captureCharge($charge_id, $amount)
    {
        
        $params = ['amount' => $amount];

        return $this->request('POST', '/charges/'.$charge_id.'/capture',  $params);
    }

    // Void a charge
    public function voidCharge($charge_id, $reason='requested_by_customer')
    {
        
        // reason must be one of these: requested_by_customer, duplicate, fraudulent

        $params = ['reason' => $reason];

        return $this->request('POST', '/charges/'.$charge_id.'/void',  $params);
    }

    // Refund a charge
    public function refundCharge($charge_id, $amount)
    {
        
        $params = ['amount' => $amount];

        return $this->request('POST', '/charges/'.$charge_id.'/refunds',  $params);
    }

    public function getCustomers(){
        
        
        $params = ['limit' => 20, 'page'=>1];

        return $this->request('GET', '/customers',  $params);
    }

    public function getCustomer($customer_id)
    {
        
        
        $params = [];

        return $this->request('GET', '/customers/'.$customer_id,  $params);
    }

    public function createCustomer($email, $name, $other_info=[], $customer_id=null)
    {
        
        $params = ['name' => $name, 'email'=>$email];
        
        $route = '/customers';
        $method = 'POST';

        if($customer_id){
            $route .= '/'.$customer_id;
            $method = 'PATCH';
        }

        return $this->request($method, $route,  $params);
    }

    public function updateCustomer($customer_id=null, $email, $name='', $other_info=[]){

        return $this->createCustomer($email, $name, $other_info, $customer_id);
    }

    public function deleteCustomer($customer_id)
    {

        

        return $this->request('DELETE', '/customers/'.$customer_id, $headers);
    }

    public function createPlan($params)
    {
        
        // $params should have following details

        // plan_id
        // amount
        // name
        // interval (day in digits) day,week,month,year
        // interval_count 
        // trial_period_days
        // statement_descriptor
        // plan_description
        // currency  (usd)
        // plan_type  (Allowed values digital or physical)
                
        $route = '/plans';

        return $this->request('POST', $route,  $params);
    }

    public function updatePlan($plan_id, $params)
    {
        
        // $params should have following details
        
        // name
        // statement_descriptor
        // trial_period_days  (days in digits)
                
        $route = '/plans/'.$plan_id;


        return $this->request('PATCH', $route,  $params);
    }

    public function deletePlan($plan_id)
    {   
                
        $route = '/plans/'.$plan_id;

        return $this->request('DELETE', $route, $headers);
    }

    public function createSubscription($plan_id)
    {
        $route = '/plans/'.$plan_id;

        return $this->request('DELETE', $route, $headers);
    }

    public function updateSubscription($plan_id)
    {
        
        $route = '/plans/'.$plan_id;

        return $this->request('DELETE', $route, $headers);
    }

    public function request($method, $route_url,  $params=[])
    {

        $headers = $this->createHeaders(strtoupper($method));
        
        // Create a callback to capture HTTP headers for the response
        $rheaders = array();
        $headerCallback = function ($ch, $header_line) use (&$rheaders) {
            // Ignore the HTTP request line (HTTP/1.1 200 OK)
            if (strpos($header_line, ":") === false) {
                return strlen($header_line);
            }
            list($key, $value) = explode(":", trim($header_line), 2);
            $rheaders[trim($key)] = trim($value);
            return strlen($header_line);
        };

        $apiEndPoint = ($this->apiMode == 'sandbox' ? 'https://testapi.payarc.net/v1' : 'https://api.payarc.net/v1');
        
        $absUrl = self::utf8( ($apiEndPoint).$route_url );

        $ch = curl_init();

        // Method
        if(strtoupper($method) == 'POST') 
        {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, self::urlEncode($params));

        } else if(strtoupper($method) == 'PATCH') 
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($ch, CURLOPT_POSTFIELDS, self::urlEncode($params));

        } else if(strtoupper($method) == 'GET') {
            
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            if (count($params) > 0) {
                $encoded = self::urlEncode($params);
                $absUrl = "$absUrl?$encoded";
            }

        } else if (strtoupper($method) ==  'DELETE') {
            
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            if (count($params) > 0) {
                $encoded = self::urlEncode($params);
                $absUrl = "$absUrl?$encoded";
            }

        } else {
            throw new Exception("Unrecognized method $method");
        }

        curl_setopt($ch, CURLOPT_URL, $absUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, $headerCallback);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $rbody = curl_exec($ch);

        $errno = curl_errno($ch);
        
        if ($rbody === false) {
            $errno = curl_errno($ch);
            $message = curl_error($ch);
            curl_close($ch);
            
            $this->handleCurlError($absUrl, $errno, $message);
        }

        $rcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        return array('response_body'=> $rbody, 'response_code' => $rcode, 'response_headers' => $rheaders);
    }

    /**
     * @param number $errno
     * @param string $message
     * @throws Error\ApiConnection
     */
    private function handleCurlError($url, $errno, $message)
    {
        switch ($errno) {
            case CURLE_COULDNT_CONNECT:
            case CURLE_COULDNT_RESOLVE_HOST:
            case CURLE_OPERATION_TIMEOUTED:
                $msg = "Could not connect to PayArc ($url).  Please check your "
                 . "internet connection and try again.";
                break;
            case CURLE_SSL_CACERT:
            case CURLE_SSL_PEER_CERTIFICATE:
                $msg = "Could not verify PayArc's SSL certificate.  Please make sure "
                 . "that your network is not intercepting certificates.  "
                 . "(Try going to $url in your browser.)  "
                 . "If this problem persists,";
                break;
            default:
                $msg = "Unexpected error communicating with PayArc.  "
                 . "If this problem persists,";
        }
        
        $msg .= "\n\n(Network error [errno $errno]: $message)";
        
        throw new Exception($msg);
    }

    public function d($data=null, $flag=true){

        echo '<pre>';

        print_R($data);

        echo '<pre>';

        if($flag) exit();
    }
}

?>