<?php 


use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
Class Gateway {

   private $error = array();
   private $card  = array('cardNumber'=>'4111111111111111','expMonth'=>'02','expYear'=>'2030','cvv'=>'1234');
	function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('session');
				
	}


   public function chk_nmi_gateway_auth($nmi_array=array())
   {
		include APPPATH . 'third_party/nmiDirectPost.class.php';
	    if(!empty($nmi_array))
		{
			 
			$amount= 0.1;
			$card_data = $this->card;
			$nmi = new nmiDirectPost($nmi_array); 
			$nmi->setCcNumber($card_data['cardNumber']);
			$expmonth =  $card_data['expMonth'];
			$exyear   = $card_data['expYear'];
			$exyear   = substr($exyear,2);
			if(strlen($expmonth)==1){
				$expmonth = '0'.$expmonth;
			}
			$expry    = $expmonth.$exyear;  
			$nmi->setCcExp($expry);
			$nmi->setCvv($card_data['cvv']);
			$nmi->setAmount($amount);
		    
			$nmi->auth();
			$result = $nmi->execute();
			
			if($result['responsetext']=='Authentication Failed')
			return 0;
			else
			return 1;	
		}
   
   
   }
   
   public function chk_auth_gateway_auth($auth_array=array())
   {	
       
		include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
	    $this->ci->load->config('auth_pay');
		$apiloginID   	= $auth_array['user'];
		$transactionKey = $auth_array['password']; 
		$amount    = 1.1;
		$card_data = $this->card;
		$card_no  = $card_data['cardNumber'];
		$expmonth =  $card_data['expMonth'];
		
		$exyear   = $card_data['expYear'];
		$exyear   = substr($exyear,2);
		if(strlen($expmonth)==1){
			$expmonth = '0'.$expmonth;
		}
		$expry    = $expmonth.$exyear;
		$transaction = new AuthorizeNetAIM($apiloginID,$transactionKey); 
		$transaction->setSandbox($this->ci->config->item('Sandbox'));
		
		$result = $transaction->authorizeOnly($amount,$card_no,$expry);
	
		if($result->response_reason_code=='13')
        	$st= 0;
		else
		$st= 1;	
		
		return $st;
   }
   
   public function chk_paytrace_gateway_auth($auth_array=array())
   {
	    include APPPATH . 'third_party/PayTraceAPINEW.php';
	    $this->ci->load->config('paytrace');
		$payusername   	= $auth_array['user'];
		$paypassword 	= $auth_array['password']; 
		 $grant_type    = GRANT_TYPE;
		$amount    = 0.1;

		 $payAPI = new PayTraceAPINEW();
		//set the properties for this request in the class
			  
		$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
			
		//call a function of Utilities.php to verify if there is any error with OAuth token. 
		
		//If IsFoundOAuthTokenError results True, means no error 
		//next is to move forward for the actual request 

		if($oauth_result['http_status_code']=='401'){
		return 0;
		}
		else
		return 1;
   }
   
   
   
   
   public function chk_paypal_gateway_auth($auth_array=array())
   {
		 $this->ci->load->config('paypal');
		$username   	= $auth_array['user'];
		$password 	= $auth_array['password']; 
		$signature	    = $auth_array['signature']; 
		$amount    = 0.1;
		
		 $config = array(
						'Sandbox' => $this->ci->config->item('Sandbox'), 			
						'APIUsername' => $username, 
						'APIPassword' => $password,
						'APISignature' => $signature, 
						'APISubject' => '', 
						'APIVersion' => $this->ci->config->item('APIVersion')
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
			$card_data = $this->card;				
			$creditCardNumber = $card_data['cardNumber'];
			$expmonth 		  =  $card_data['expMonth'];
			$exyear   		  = $card_data['expYear'];
			$cvv2Number		  = $card_data['cvv'];	
			$this->ci->load->library('paypal/Paypal_pro', $config);	
			$padDateMonth 		= str_pad($expmonth, 2, '0', STR_PAD_LEFT);
											
			$DPFields = array(
							'paymentaction' => 'Auth', 
							'ipaddress' => '', 							
							'returnfmfdetails' => '0' 					
						);
						
			$CCDetails = array(
							'creditcardtype' => 'VISA',
							'acct' => $creditCardNumber, 
							'expdate' => $padDateMonth.$exyear, 				
							'cvv2' => $cvv2Number, 							
							 'startdate' => '', 			
							'issuenumber' => ''		 				    
						);
						
		$PayerInfo = array();  
						
		$PayerName = array();
					
		$BillingAddress = array(	);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 					
								'currencycode' => 'USD' , 					
								'itemamt' => '', 						
								'shippingamt' => '', 					
								'insuranceamt' => '', 					
								'shipdiscamt' => '', 				
								'handlingamt' => '', 					
								'taxamt' => '', 						 
								'desc' => '', 							
								'custom' => '', 						
								'invnum' => '', 						
								'buttonsource' => '', 					
								'notifyurl' => '', 						
								'recurring' => ''						
							);					

					
					$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
							
				        $PayPalResult = $this->ci->paypal_pro->DoDirectPayment($PayPalRequestData);
						
						if($PayPalResult['L_ERRORCODE0']=='10002' && strtoupper($PayPalResult['L_LONGMESSAGE0'])=='SECURITY HEADER IS NOT VALID')
						return 0;
						else
						return 1;	
                  
   }
   public function chk_stripe_gateway_auth($auth_array=array())
   {	
       
       $result=array(); $customer='';
			require_once APPPATH."third_party/stripe/init.php";	
			require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
			
			$user = $auth_array['password'];
			try 
			{
					\Stripe\Stripe::setApiKey($user);

					$customer = \Stripe\Customer::all(['limit' => 3]);
			      $st=1;
			} 
			catch(\Stripe\Exception\CardException $e) {
							$st=1;
			  // Since it's a decline, \Stripe\Exception\CardException will be caught
			 
			} catch (\Stripe\Exception\RateLimitException $e) {
			 $st=1;
			} catch (\Stripe\Exception\InvalidRequestException $e) {
			  $st=1;
			} catch (\ Stripe\Exception\AuthenticationException $e) {
				$st=0;   
			} catch (\Stripe\Exception\ApiConnectionException $e) {
			 $st=1;
			} catch (\Stripe\Exception\ApiErrorException $e) {
			  $st=1; 
			} catch (Exception $e) {
			$st=0; 
			}
    return $st;
			
   }
   public function chk_usaePay_gateway_auth($auth_array=array())
   {
			$user 	  = $auth_array['user'];
			$password = $auth_array['password'];
		
			require_once APPPATH."third_party/usaepay/usaepay.php";	
			$this->ci->load->config('usaePay');
			$card_data = $this->card;		
			
			$card_no   = $card_data['cardNumber'];
			$expmonth  =  $card_data['expMonth'];
			$exyear    = $card_data['expYear'];
			$cvv	   = $card_data['cvv'];	
			$amount    =0.1;
			
						$transaction = new umTransaction;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						$transaction->key=$user;
						$invNo  =mt_rand(1000000,2000000); 
						$transaction->pin=$password;
						$transaction->usesandbox=true;
						$transaction->invoice=$invNo;   	
						$transaction->description="Chargezoom Invoice Payment";	// description of charge
					
						$transaction->testmode=0;    // Change this to 0 for the transaction to process
						$transaction->command="authonly";
						
						
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
                           
							$transaction->amount = $amount;
							
						    $result = $transaction->Process();
			
							if($result['authcode']=='000000')
							return 0;
							else
							return 1;	
							die;
   
   }
   
   public function chk_heartland_gateway_auth($auth_array=array())
   {
     require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
     
     $crtxnID='';  
                 $config = new PorticoConfig();
                  $config->secretApiKey = $secretApiKey;
                	if($this->config->item('Sandbox'))
                     $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                      else
                     $config->serviceUrl =  $this->config->item('RPO_GLOBAL_URL');
                     $config->developerId  =$this->config->item('DeveloperId');
                     $config->versionNumber  = $this->config->item('VersionNumber');
            
                ServicesContainer::configureService($config);
                $card = new CreditCardData();
                $card->number = $card_no;
                $card->expMonth = $expmonth;
                $card->expYear = $exyear;
                if($cvv!="")
                $card->cvn = $cvv;
                 $card->cardType=$cardType;
           
                $address = new Address();
                $address->streetAddress1 = $address1;
                $address->city = $city;
                $address->state = $state;
                $address->postalCode = $zipcode;
                $address->country = $country;
                
                
                $invNo  =mt_rand(1000000,2000000);
                 
             	try
                {
                     $response = $card->charge($amount)
                     
                   
                    ->withCurrency(CURRENCY)
                    ->withAddress($address)
                    ->withInvoiceNumber($invNo)
                    ->withAllowDuplicates(true)
                    ->execute();
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                        
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                            $txnID      = $in_data['TxnID'];  
							 $ispaid 	 = 'true';
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 'false';
							 $app_amount = $in_data['AppliedAmount']-$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('TxnID'=>$in_data['TxnID'] );
							 if($chh_mail =='1')
							 {
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number =  $in_data['RefNumber']; 
							  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
							 }
							 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
					
							 $user = $in_data['qbwc_username'];
							 
                  
               
				    
                    $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID']);  
                   
				     $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
				    
				    	  $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
				 	
						
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Payment Failed '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID']);  
                  
                         
                     }		
						
                    
                 } 
                catch (BuilderException $e)
                    {
                        $error= 'Build Exception Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='ConfigurationException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'GatewayException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error=' ApiException Failure: ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
               
   
   }
   
   
   public function chk_cybersource_gateway_auth($auth_array=array())
   {
             $this->ci->load->config('cyber_pay');
   
   }









}
?>