<?php
//error_reporting(1);

class Freshbooks_data
{

    private $error = array();

    private $merchantID;
    private $realmID;
    private $accessToken;

    public function __construct($merchantID, $resellerID)
    {

        include_once APPPATH . 'third_party/Freshbooks.php';
        include_once APPPATH . 'third_party/FreshbooksNew.php';
        $this->ci         = &get_instance();
        $this->merchantID = $merchantID;
        $this->resellerID = $resellerID;
        $this->ci->load->model('General_model', 'general_model');

        $val = array(
            'merchantID' => $this->merchantID,
        );
        $fbs_data = $this->ci->general_model->get_row_data('tbl_freshbooks', $val);

        // print_r($fbs_data); die;
        $this->subdomain             = $fbs_data['sub_domain'];
        $key                         = $fbs_data['secretKey'];
        $this->accessToken           = $fbs_data['accessToken'];
        $this->access_token_secret   = $fbs_data['oauth_token_secret'];
        $this->fb_account            = $fbs_data['accountType'];
        $condition1                  = array('resellerID' => $this->resellerID);
        $sub1                        = $this->ci->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1                     = $sub1['merchantPortalURL'];
        $URL1                        = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';
        $this->oauth_consumer_key    = $this->subdomain;
        $this->oauth_consumer_secret = $key;
        $this->oauth_callback        = $URL1;
        //  define('OAUTH_CONSUMER_KEY', $this->subdomain);
        //define('OAUTH_CONSUMER_SECRET', $key);
        //define('OAUTH_CALLBACK', $URL1);

    }

    public function create_invoice_payment($invoiceID, $amount)
    {

        $in_data = $this->ci->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invoiceID' => $invoiceID));

        if (!empty($in_data)) {
            $val = array(
                'merchantID' => $this->merchantID,
            );

            $ispaid = 1;
            $st     = 'paid';
            if ($amount == '') {
                $amount = $in_data['BalanceRemaining'];
            }

            $bamount = $in_data['BalanceRemaining'] - $amount;
            if ($bamount > 0) {
                $ispaid = '0';
                $st     = 'unpaid';
            }
            $app_amount = $in_data['AppliedAmount'] + $amount;
            $up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
            $condition  = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);

            $this->ci->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);

            if (isset($this->accessToken) && isset($this->access_token_secret)) {
                if ($this->fb_account == 1) {
                    $c = new FreshbooksNew($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);

                    $payment  = array('invoiceid' => $invoiceID, 'amount' => array('amount' => $amount), 'type' => 'Check', 'date' => date('Y-m-d'));
                    $invoices = $c->add_payment($payment);
                } else {
                    $c = new Freshbooks($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret);

                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                            						<request method="payment.create">
                            						  <payment>
                            						    <invoice_id>' . $invoiceID . '</invoice_id>
                            						    <amount>' . $amount . '</amount>
                            						    <currency_code>USD</currency_code>
                            						    <type>Check</type>
                            						  </payment>
                            						</request>';
                    $invoices = $c->add_payment($payment);

                }

                return $invoices;

            }

        }
        return false;

    }

    public function update_invoice_payment($invoiceID, $amount, $payment_id, $updatedTXNAmount)
    {

        $in_data = $this->ci->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invoiceID' => $invoiceID));

        if (!empty($in_data)) {
            $val = array(
                'merchantID' => $this->merchantID,
            );

            $ispaid = 1;
            $st     = 'paid';
            if ($amount == '') {
                $amount = $in_data['BalanceRemaining'];
            }

            $bamount = $in_data['BalanceRemaining'] + $amount;
            if ($bamount > 0) {
                $ispaid = '0';
                $st     = 'unpaid';
            }
            $app_amount = $in_data['AppliedAmount'] - $amount;
            $up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
            $condition  = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);

            $this->ci->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);

            if (isset($this->accessToken) && isset($this->access_token_secret)) {
                if ($this->fb_account == 1) {
                    $c = new FreshbooksNew($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);

                    $payment  = array('invoiceid' => $invoiceID, 'payment_id' => $payment_id, 'amount' => array('amount' => $updatedTXNAmount), 'type' => 'Check', 'date' => date('Y-m-d'));
                    $invoices = $c->update_payment($payment);
                } else {
                    $c = new Freshbooks($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret);

                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                            						<request method="payment.create">
                            						  <payment>
                            						    <invoice_id>' . $invoiceID . '</invoice_id>
                            						    <amount>' . $amount . '</amount>
                            						    <currency_code>USD</currency_code>
                            						    <type>Check</type>
                            						  </payment>
                            						</request>';
                    $invoices = $c->update_payment($payment);

                }

                return $invoices;

            }

        }
        return false;

    }

    public function get_freshbooks_customers()
    {

        if (isset($this->accessToken) && isset($this->access_token_secret)) {

            $fb_data   = $this->ci->general_model->get_select_data('tbl_fb_config', array('lastUpdated'), array('merchantID' => $this->merchantID, 'fb_action' => 'CustomerQuery'));
            $last_date = $current_date = '';
            if (!empty($fb_data)) {
                $last_date    = $fb_data['lastUpdated'];
                $current_date = date('Y-m-d H:i:s');
                $my_timezone  = date_default_timezone_get();
                $from         = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
                $to           = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');

                $last_date    = $this->ci->general_model->datetimeconv($last_date, $from, $to);
                $current_date = $this->ci->general_model->datetimeconv($current_date, $from, $to);

                $last_date    = date('Y-m-d', strtotime('-6 hour', strtotime($last_date)));
                $current_date = date('Y-m-d H:i:s', strtotime('-3 hour', strtotime($current_date)));
            }

            if (!empty($fb_data)) {$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
                $updatedata['updatedAt']                              = date('Y-m-d H:i:s');

                $this->ci->general_model->update_row_data('tbl_fb_config', array('merchantID' => $this->merchantID, 'fb_action' => 'CustomerQuery'), $updatedata);
            } else {
                $updateda = date('Y-m-d') . 'T' . date('H:i:s');
                $upteda   = array('merchantID' => $this->merchantID, 'fb_action' => 'CustomerQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
                $this->ci->general_model->insert_row('tbl_fb_config', $upteda);
            }
            //echo $fb_account; die;

            if ($this->fb_account == 1) {

                $c = new FreshbooksNew($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);

                // $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);
                $customer = $c->get_customers_data($last_date, $current_date);

            }
            if ($this->fb_account == 2) {
                $c = new Freshbooks($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret);
                // $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
                $customer = $c->get_customers_data();
            }

            foreach ($customer as $oneCustomer) {
                $phoneNumber = '';
                if(isset($oneCustomer->home_phone) && !empty($oneCustomer->home_phone)){
                  $phoneNumber = $oneCustomer->home_phone;
                } else if(isset($oneCustomer->work_phone) && !empty($oneCustomer->work_phone)){
                  $phoneNumber = $oneCustomer->work_phone;
                }

                if ($this->fb_account == 1) {

                    $companyName = $oneCustomer->organization;
                    $username = $oneCustomer->username;
                    $fname = $oneCustomer->fname;
                    $lname = $oneCustomer->lname;

                    $FB_customer_details = array(
                        "Customer_ListID" => $oneCustomer->userid,
                        "firstName" => "$fname",
                        "lastName" => "$lname",
                        "companyName" => "$companyName",
                        "fullName" => "$fname $lname",
                        "userEmail" => $this->ci->db->escape_str(($oneCustomer->email)?$oneCustomer->email:$oneCustomer->contacts->contact->email),
                        "phoneNumber" => $this->ci->db->escape_str($phoneNumber),
                        "address1" => $this->ci->db->escape_str(($oneCustomer->p_street)?$oneCustomer->p_street:''),
                        "address2" => $this->ci->db->escape_str(($oneCustomer->p_street2)?$oneCustomer->p_street2:''),
                        "zipCode" => $this->ci->db->escape_str(($oneCustomer->p_code)?$oneCustomer->p_code:''),
                        "City" => $this->ci->db->escape_str(($oneCustomer->p_city)?$oneCustomer->p_city:''),
                        "State" => $this->ci->db->escape_str(($oneCustomer->p_province)?$oneCustomer->p_province:''),
                        "Country" => $this->ci->db->escape_str(($oneCustomer->p_country)?$oneCustomer->p_country:''),
                        
                        "ship_address1" => $this->ci->db->escape_str(($oneCustomer->s_street)?$oneCustomer->s_street:''),
                        "ship_address2" => $this->ci->db->escape_str(($oneCustomer->s_street2)?$oneCustomer->s_street2:''),
                        "ship_zipcode" => $this->ci->db->escape_str(($oneCustomer->s_code)?$oneCustomer->s_code:''),
                        "ship_city" => $this->ci->db->escape_str(($oneCustomer->s_city)?$oneCustomer->s_city:''),
                        "ship_state" => $this->ci->db->escape_str(($oneCustomer->s_province)?$oneCustomer->s_province:''),
                        "ship_country" => $this->ci->db->escape_str(($oneCustomer->s_country)?$oneCustomer->s_country:''),
                        
                        "TimeCreated" => $oneCustomer->signup_date,
                        "companyID" => $oneCustomer->accounting_systemid,
                        "updatedAt" => $this->ci->db->escape_str($oneCustomer->updated),
                        "merchantID" => $this->merchantID,
                      );

                    if ($this->ci->general_model->get_num_rows('Freshbooks_custom_customer', array('Customer_ListID' => $oneCustomer->userid, 'merchantID' => $this->merchantID)) > 0) {

                        $this->ci->general_model->update_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $oneCustomer->userid, 'merchantID' => $this->merchantID), $FB_customer_details);
                    } else {

                        $this->ci->general_model->insert_row('Freshbooks_custom_customer', $FB_customer_details);
                    }

                } else if ($this->fb_account == 2) {

                    $FB_customer_details = array(
                        "Customer_ListID" => $oneCustomer->client_id,
                        "firstName"       => $oneCustomer->first_name,
                        "lastName"        => $oneCustomer->last_name,
                        "fullName"        => $oneCustomer->organization,
                        //   "fullName" => $oneCustomer->username,
                        "userEmail"       => ($oneCustomer->email) ? $oneCustomer->email : $oneCustomer->contacts->contact->email,
                        "phoneNumber"     => ($oneCustomer->home_phone) ? $oneCustomer->home_phone : $oneCustomer->work_phone,
                        "address1"        => ($oneCustomer->p_street1) ? $oneCustomer->p_street1 : '',
                        "address2"        => ($oneCustomer->p_street2) ? $oneCustomer->p_street2 : '',
                        "zipCode"         => ($oneCustomer->p_code) ? $oneCustomer->p_code : '',
                        "City"            => ($oneCustomer->p_city) ? $oneCustomer->p_city : '',
                        "State"           => ($oneCustomer->p_state) ? $oneCustomer->p_state : '',
                        "Country"         => ($oneCustomer->p_country) ? $oneCustomer->p_country : '',

                        "ship_address1"   => ($oneCustomer->s_street1) ? $oneCustomer->s_street1 : '',
                        "ship_address2"   => ($oneCustomer->s_street2) ? $oneCustomer->s_street2 : '',
                        "ship_zipcode"    => ($oneCustomer->s_code) ? $oneCustomer->s_code : '',
                        "ship_city"       => ($oneCustomer->s_city) ? $oneCustomer->s_city : '',
                        "ship_state"      => ($oneCustomer->s_state) ? $oneCustomer->s_state : '',
                        "ship_country"    => ($oneCustomer->s_country) ? $oneCustomer->s_country : '',

                        "companyName"     => $oneCustomer->organization,
                        "companyID"       => 2,
                        "updatedAt"       => $oneCustomer->updated,
                        "merchantID"      => $this->merchantID,
                        "Reminder"        => '',

                    );

                    if ($this->ci->general_model->get_num_rows('Freshbooks_custom_customer', array('Customer_ListID' => $oneCustomer->client_id, 'merchantID' => $this->merchantID)) > 0) {
                        $this->ci->general_model->update_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $oneCustomer->client_id, 'merchantID' => $this->merchantID), $FB_customer_details);
                    } else {
                        $this->ci->general_model->insert_row('Freshbooks_custom_customer', $FB_customer_details);
                    }
                }
            }

        }
    }

    public function get_freshbooks_invoices()
    {
        $this->merchantID = $this->merchantID;
        $fb_data = $this->ci->general_model->get_select_data('tbl_fb_config', array('lastUpdated'), array('merchantID' => $this->merchantID, 'fb_action' => 'CustomerQuery'));

        $last_date = $current_date = array();
        if (!empty($fb_data)) {
            $last_date    = $fb_data['lastUpdated'];
            $current_date = date('Y-m-d H:i:s');
            $my_timezone  = date_default_timezone_get();
            $from         = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
            $to           = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');

            $last_date    = $this->ci->general_model->datetimeconv($last_date, $from, $to);
            $current_date = $this->ci->general_model->datetimeconv($current_date, $from, $to);

            $last_date    = date('Y-m-d H:i:s', strtotime('-6 hour', strtotime($last_date)));
            $current_date = date('Y-m-d H:i:s', strtotime('-3 hour', strtotime($current_date)));

        }

        if (!empty($fb_data)) {$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
            $updatedata['updatedAt']                              = date('Y-m-d H:i:s');

            $this->ci->general_model->update_row_data('tbl_fb_config', array('merchantID' => $this->merchantID, 'fb_action' => 'InvoiceQuery'), $updatedata);
        } else {
            $updateda = date('Y-m-d') . 'T' . date('H:i:s');
            $upteda   = array('merchantID' => $this->merchantID, 'fb_action' => 'InvoiceQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
        }

        $invNumber = 0;
		$in_data = $this->ci->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $this->merchantID));
		if (!empty($in_data)) {
			$invNumber    =   $in_data['postfix'];
		}

        if (isset($this->accessToken) && isset($this->access_token_secret)) {

            if ($this->fb_account == 1) {
                $c = new FreshbooksNew($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);

                $invoices = $c->get_invoices_data();

            } else {
                $c = new Freshbooks($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret);

                $invoices = $c->get_invoices_data($last_date, $current_date);

            }
            foreach ($invoices as $oneInvoice) {

                //  print_r($invoices);  die;
                if ($this->fb_account == 1) {
                    $invRefNumber = (string) $oneInvoice->invoice_number;
					$invRefNumberEx = explode('-', $invRefNumber);
					if(isset($invRefNumberEx[1]) && $invNumber < trim($invRefNumberEx[1])){
						$invNumber = trim($invRefNumberEx[1]);
					}

                    $invoice_id                                   = $oneInvoice->invoiceid;
                    $FB_invoice_details['invoiceID']              = $oneInvoice->invoiceid;
                    $FB_invoice_details['refNumber']              = (string) $invRefNumber;
                    $FB_invoice_details['ShipAddress_Addr1']      = $oneInvoice->p_street1;
                    $FB_invoice_details['ShipAddress_Addr2']      = $oneInvoice->street2;
                    $FB_invoice_details['ShipAddress_City']       = $oneInvoice->city;
                    $FB_invoice_details['ShipAddress_Country']    = $oneInvoice->country;
                    $FB_invoice_details['ShipAddress_PostalCode'] = $oneInvoice->code;
                    $FB_invoice_details['CustomerFullName']       = ($oneInvoice->organization) ? $oneInvoice->organization : '';
                    $FB_invoice_details['BalanceRemaining']       = $oneInvoice->outstanding->amount;
                    $FB_invoice_details['Total_payment']          = 0;
                    $FB_invoice_details['AppliedAmount']          = $oneInvoice->paid->amount;
                    $FB_invoice_details['UserStatus']             = $oneInvoice->payment_status;
                    $FB_invoice_details['CustomerListID']         = $oneInvoice->customerid;
                    $FB_invoice_details['TimeModified']           = ($oneInvoice->updated) ? $oneInvoice->updated : '';
                    $FB_invoice_details['TimeCreated']            = ($oneInvoice->created_at) ? $oneInvoice->created_at : '';
                    $FB_invoice_details['DueDate']                = $oneInvoice->due_date;
                    $FB_invoice_details['merchantID']             = $this->merchantID;

                    $to_tax = 0;
                    if ($oneInvoice->lines) {
                        $lines = $oneInvoice->lines;

                        $this->ci->general_model->delete_row_data('tbl_freshbook_invoice_item', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id));
                        $this->ci->general_model->delete_row_data('tbl_freshbook_invoice_item_tax', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id));
                        foreach ($lines as $line) {

                            $line_data['itemID']          = $line->lineid;
							$line_data['itemName'] = $line->name;
                            $line_data['itemDescription'] = (string) $line->description;
                            $line_data['itemPrice']       = $line->unit_cost->amount;
                            $line_data['itemQty']         = $line->qty;
                            $line_data['totalAmount']     = $line->amount->amount;
                            if ($line->taxName1) {
                                // $tax_val =$this->general_model->get_row_data('tbl_taxes_fb',array('merchantID'=>$this->merchantID,'friendlyName'=>$line->tax1_name));
                                $line_data['taxName']             = (string) $line->taxName1;
                                $to_tax += $line_data['taxValue'] = $line->taxAmount1;
                                $line_data['taxPercent']          = $line->taxNumber1;
                            } else {
                                $line_data['taxName']  = '';
                                $line_data['taxValue'] = 0;
                            }
                            $line_data['merchantID'] = $this->merchantID;
                            $line_data['invoiceID']  = $invoice_id;
                            $line_data['createdAt']  = date('Y-m-d H:i:s', strtotime($line->updated));

                            $itemInvID = $this->ci->general_model->insert_row('tbl_freshbook_invoice_item', $line_data);
                            if($itemInvID){
								$taxIndex = 1;
								$taxIndexFound = true;
								do {
									if (isset($line->{"taxAmount$taxIndex"}) && $line->{"taxAmount$taxIndex"} > 0) {
										$taxInvItem = [];
										$taxInvItem['itemLineID'] = $itemInvID;
										$taxInvItem['merchantID'] = $this->merchantID;
										$taxInvItem['invoiceID'] = $invoice_id;

										$taxInvItem['taxName'] = $line->{"taxName$taxIndex"};
										$taxInvItem['taxAmount'] = $line->{"taxAmount$taxIndex"};
										$taxInvItem['taxNumber'] = $line->{"taxNumber$taxIndex"};

										$this->general_model->insert_row('tbl_freshbook_invoice_item_tax', $taxInvItem);
										++$taxIndex;
									} else {
										$taxIndexFound = false;
									}
								} while ($taxIndexFound === true);
							}
                        }
                    }
                    $FB_invoice_details['totalTax'] = $to_tax;
                } else {
                    $invoice_id                                   = $oneInvoice->invoice_id;
                    $FB_invoice_details['invoiceID']              = $oneInvoice->invoice_id;
                    $FB_invoice_details['refNumber']              = (string) $oneInvoice->number;
                    $FB_invoice_details['ShipAddress_Addr1']      = ($oneInvoice->p_street1) ? $oneInvoice->p_street1 : '';
                    $FB_invoice_details['ShipAddress_Addr2']      = ($oneInvoice->p_street2) ? $oneInvoice->p_street2 : '';
                    $FB_invoice_details['ShipAddress_City']       = ($oneInvoice->p_city) ? $oneInvoice->p_city : '';
                    $FB_invoice_details['ShipAddress_Country']    = ($oneInvoice->p_country) ? $oneInvoice->p_country : '';
                    $FB_invoice_details['ShipAddress_PostalCode'] = ($oneInvoice->p_code) ? $oneInvoice->p_code : '';
                    $FB_invoice_details['CustomerFullName']       = ($oneInvoice->organization) ? $oneInvoice->organization : '';
                    $FB_invoice_details['BalanceRemaining']       = $oneInvoice->amount_outstanding;
                    $FB_invoice_details['Total_payment']          = 0;
                    $FB_invoice_details['AppliedAmount']          = $oneInvoice->paid;
                    $FB_invoice_details['UserStatus']             = $oneInvoice->status;
                    $FB_invoice_details['CustomerListID']         = $oneInvoice->client_id;
                    $FB_invoice_details['TimeModified']           = ($oneInvoice->updated) ? $oneInvoice->updated : '';
                    $FB_invoice_details['TimeCreated']            = ($oneInvoice->created_at) ? $oneInvoice->created_at : '';
                    $FB_invoice_details['DueDate']                = ($oneInvoice->date) ? $oneInvoice->date : '';
                    $FB_invoice_details['merchantID']             = $this->merchantID;

                    $to_tax = 0;
                    if ($oneInvoice->lines->line) {
                        $lines = $oneInvoice->lines->line;
                        $this->ci->general_model->delete_row_data('tbl_freshbook_invoice_item', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id));
                        foreach ($lines as $line) {
                            $line_data['itemID']          = $line->order;
                            $line_data['itemDescription'] = (string) $line->name;
                            $line_data['itemPrice']       = $line->unit_cost;
                            $line_data['itemQty']         = $line->quantity;
                            $line_data['totalAmount']     = $line->amount;
                            if ($line->tax1_name) {
                                // $tax_val =$this->general_model->get_row_data('tbl_taxes_fb',array('merchantID'=>$this->merchantID,'friendlyName'=>$line->tax1_name));
                                $line_data['taxName']             = (string) $line->tax1_name;
                                $to_tax += $line_data['taxValue'] = (($line->quantity * $line->unit_cost) * $line->tax1_percent) / 100;
                                $line_data['taxPercent']          = $line->tax1_percent;
                            } else {
                                $line_data['taxName']  = array();
                                $line_data['taxValue'] = 0;
                            }
                            $line_data['merchantID'] = $this->merchantID;
                            $line_data['invoiceID']  = $oneInvoice->invoice_id;

                            $this->ci->general_model->insert_row('tbl_freshbook_invoice_item', $line_data);

                        }
                    }
                    $FB_invoice_details['totalTax'] = $to_tax;
                }

                if($FB_invoice_details['UserStatus'] == 'partial'){
                    $FB_invoice_details['UserStatus'] = 'unpaid';
                }

                if($FB_invoice_details['BalanceRemaining'] == 0 || $FB_invoice_details['Total_payment'] == $FB_invoice_details['AppliedAmount']){
                    $FB_invoice_details['IsPaid'] = 1;
                } else {
                    $FB_invoice_details['IsPaid'] = 0;
                }

                if ($this->ci->general_model->get_num_rows('Freshbooks_test_invoice', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id)) > 0) {
                    $this->ci->general_model->update_row_data('Freshbooks_test_invoice', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id), $FB_invoice_details);
                } else {
                    $this->ci->general_model->insert_row('Freshbooks_test_invoice', $FB_invoice_details);
                }

            }

            if($invNumber != 0){
				$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $invNumber));
			}
        }

    }

    public function get_freshbooks_items()
    {
        $this->merchantID = $this->merchantID;

        $fb_data   = $this->ci->general_model->get_select_data('tbl_fb_config', array('lastUpdated'), array('merchantID' => $this->merchantID, 'fb_action' => 'ItemQuery'));
        $last_date = $current_date = '';
        if (!empty($fb_data)) {
            $last_date    = $fb_data['lastUpdated'];
            $current_date = date('Y-m-d H:i:s');
            $my_timezone  = date_default_timezone_get();
            $from         = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
            $to           = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
            $last_date    = $this->ci->general_model->datetimeconv($last_date, $from, $to);
            $current_date = $this->ci->general_model->datetimeconv($current_date, $from, $to);

            $last_date    = date('Y-m-d H:i:s', strtotime('-6 hour', strtotime($last_date)));
            $current_date = date('Y-m-d H:i:s', strtotime('-3 hour', strtotime($current_date)));

        }

        if (!empty($fb_data)) {
            $updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
            $updatedata['updatedAt']   = date('Y-m-d H:i:s');

            $this->ci->general_model->update_row_data('tbl_fb_config', array('merchantID' => $this->merchantID, 'fb_action' => 'ItemQuery'), $updatedata);
        } else {
            $updateda = date('Y-m-d') . 'T' . date('H:i:s');
            $upteda   = array('merchantID' => $this->merchantID, 'fb_action' => 'ItemQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
            $this->ci->general_model->insert_row('tbl_fb_config', $upteda);
        }

        if (isset($this->accessToken) && isset($this->access_token_secret)) {
            if ($this->fb_account == 1) {
                $c = new FreshbooksNew($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);

                //$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);
                $items = $c->get_items_data();

                foreach ($items as $oneItem) {

                    $FB_item_details = array(
                        "productID"        => $oneItem->itemid,
                        "Name"             => $oneItem->name,
                        "SalesDescription" => $oneItem->description,
                        "saleCost"         => $oneItem->unit_cost->amount,
                        "QuantityOnHand"   => $oneItem->qty,
                        "TimeModified"     => $oneItem->updated,
                        "Inventory"        => $oneItem->inventory,
                        "Tax1"             => $oneItem->tax1,
                        "Tax2"             => $oneItem->tax2,
                        "IsActive"         => ($oneItem->vis_state == 0) ? 1 : '0',
                        "merchantID"       => $this->merchantID,
                        "fbCompanyID"      => $oneItem->accounting_systemid,
                    );

                    if ($this->ci->general_model->get_num_rows('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->itemid)) > 0) {
                        $this->ci->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->itemid), $FB_item_details);
                    } else {
                        $this->ci->general_model->insert_row('Freshbooks_test_item', $FB_item_details);
                    }

                }

            }
            if ($this->fb_account == 2) {

                $c = new Freshbooks($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret);

                // $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $this->subdomain, $this->accessToken, $this->access_token_secret);
                $items = $c->get_items_data();
                foreach ($items as $oneItem) {

                    $FB_item_details = array(
                        "productID"        => $oneItem->item_id,
                        "Name"             => $oneItem->name,
                        "SalesDescription" => $oneItem->description,
                        "saleCost"         => $oneItem->unit_cost,
                        "QuantityOnHand"   => $oneItem->quantity,
                        "TimeModified"     => $oneItem->updated,
                        "Inventory"        => ($oneItem->inventory) ? $oneItem->inventory : '',
                        "Tax1"             => ($oneItem->tax1_id) ? $oneItem->tax1_id : '',
                        "Tax2"             => ($oneItem->tax2_id) ? $oneItem->tax2_id : '',
                        "IsActive"         => ($oneItem->folder == 'active') ? '1' : '0',
                        "merchantID"       => $this->merchantID,
                    );
                    if (!empty($this->ci->general_model->get_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->item_id)))) {
                        $this->ci->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->item_id), $FB_item_details);
                    } else {
                        $this->ci->general_model->insert_row('Freshbooks_test_item', $FB_item_details);
                    }

                }
            }

        }

    }

    public function void_invoice_payment($invoiceID, $amount, $payment_id)
    {

        $in_data = $this->ci->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invoiceID' => $invoiceID));

        if (!empty($in_data)) {
            $val = array(
                'merchantID' => $this->merchantID,
            );

            $ispaid = 1;
            $st     = 'paid';
            if ($amount == '') {
                $amount = $in_data['BalanceRemaining'];
            }

            $bamount = $in_data['BalanceRemaining'] + $amount;
            if ($bamount > 0) {
                $ispaid = '0';
                $st     = 'unpaid';
            }
            $app_amount = $in_data['AppliedAmount'] - $amount;
            $up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
            $condition  = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);

            $this->ci->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);

            if (isset($this->accessToken) && isset($this->access_token_secret)) {
                if ($this->fb_account == 1) {
                    $c = new FreshbooksNew($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);

                    $invoices = $c->delete_payment($payment_id);
                } else {
                    $c = new Freshbooks($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret);

                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                            						<request method="payment.create">
                            						  <payment>
                            						    <invoice_id>' . $invoiceID . '</invoice_id>
                            						    <amount>' . $amount . '</amount>
                            						    <currency_code>USD</currency_code>
                            						    <type>Check</type>
                            						  </payment>
                            						</request>';
                    $invoices = $c->update_payment($payment);

                }

                return $invoices;

            }

        }
        return false;

    }

    /**
     * Method to check freshbook account connectivity
     * 
     * @return bool
     */
    public function isConnected() : bool
    {
        $isConnected = false;
        if (isset($this->accessToken) && isset($this->access_token_secret)) {
            if ($this->fb_account == 1) {
                $fbData = new FreshbooksNew($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret, $this->merchantID);
                $isConnected = $fbData->isConnected();
            } else {
                $c = new Freshbooks($this->oauth_consumer_key, $this->oauth_consumer_secret, $this->oauth_callback, $this->subdomain, $this->accessToken, $this->access_token_secret);
            }
        }

        return $isConnected;
    }
}
