<?php

namespace Maverick;

/**
 * Class MaverickGateway
 *
 * @package Maverick
 */
Class Maverick{

    // @var string the api mode either sandbox or production
    protected $apiMode = 'sandbox';

    // @var string The Maverick Access Token to be used for requests.
    protected $accessToken;

    // @var string The Maverick Terminal ID
    protected $terminalId;

    // @var string|null The version of the Stripe API to use for requests.
    public static $apiVersion = null;

    // @var boolean Defaults to true.
    public static $verifySslCerts = true;

    private static $isMbstringAvailable = null;
    
    const VERSION = '1.0';

    
    /**
     * Sets the API key to be used for requests.
     *
     * @param string $accessToken
     */
    public function setTerminalId($id)
    {
        $this->terminalId = $id;
    }
    /**
     * @return string Terminal Id
     */
    public function getTerminalId()
    {
        return $this->terminalId;
    }

    /**
     * @return string The API key used for requests.
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Sets the API key to be used for requests.
     *
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Sets the api Mode to be used for transactions.
     *
     * @param string $apiMode
     */
    public function setApiMode($apiMode)
    {
        $this->apiMode = $apiMode ? $apiMode : 'sandbox';
    }

    /**
     * @return string The API version used for requests. null if we're using the
     *    latest version.
     */
    public static function getApiVersion()
    {
        return self::$apiVersion;
    }

    /**
     * @param string $apiVersion The API version to use for requests.
     */
    public static function setApiVersion($apiVersion)
    {
        self::$apiVersion = $apiVersion;
    }

    /**
     * @return boolean
     */
    public static function getVerifySslCerts()
    {
        return self::$verifySslCerts;
    }

    /**
     * @param boolean $verify
     */
    public static function setVerifySslCerts($verify)
    {
        self::$verifySslCerts = $verify;
    }

    
    /**
     * @param string|mixed $value A string to UTF8-encode.
     *
     * @return string|mixed The UTF8-encoded string, or the object passed in if
     *    it wasn't a string.
     */
    public static function utf8($value)
    {
        if (self::$isMbstringAvailable === null) {
            self::$isMbstringAvailable = function_exists('mb_detect_encoding');

            if (!self::$isMbstringAvailable) {
                trigger_error("It looks like the mbstring extension is not enabled. " .
                    "UTF-8 strings will not properly be encoded. Ask your system. ", E_USER_WARNING);
            }
        }

        if (is_string($value) && self::$isMbstringAvailable && mb_detect_encoding($value, "UTF-8", true) != "UTF-8") {
            return utf8_encode($value);
        } else {
            return $value;
        }
    }

    /**
     * @param array $arr A map of param keys to values.
     * @param string|null $prefix
     *
     * @return string A querystring, essentially.
     */
    public static function urlEncode($arr, $prefix = null)
    {
        if (!is_array($arr)) {
            return $arr;
        }

        $r = array();
        foreach ($arr as $k => $v) {
            if (is_null($v)) {
                continue;
            }

            if ($prefix) {
                if ($k !== null && (!is_int($k) || is_array($v))) {
                    $k = $prefix."[".$k."]";
                } else {
                    $k = $prefix."[]";
                }
            }

            if (is_array($v)) {
                $enc = self::urlEncode($v, $k);
                if ($enc) {
                    $r[] = $enc;
                }
            } else {
                $r[] = urlencode($k)."=".urlencode($v);
            }
        }

        return implode("&", $r);
    }

    public function createHeaders($method=null) {
        $headers = [
            'Authorization: Bearer '.$this->getAccessToken(),
            'Accept: application/json',
        ];

        if($method == 'POST' || $method == 'PATCH') {
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }

        return $headers;
    }

    public function getClientIpAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
    }
    

    public function getCompatibleCountryCode($name='')
    {

        $array = [
            'INDIA' => 'IN',
            'UNITED KINGDOM' => 'UK',
            'BRITAIN' => 'UK',
            'UNITED STATE' => 'US',
            'UNITED STATES' => 'US',
            'UNITED STATES OF AMERICA' => 'US',
            'USA' => 'US',
            'AMERICA' => 'US',
            'FRANCE' => 'FR',
            'ITALY' => 'IT',
            'CANADA' => 'CA',
            'GERMANY' => 'DE'
        ];

        if(!empty($name) && isset($array[strtoupper($name)]) ) {
            return $array[strtoupper($name)];
        }

        return '';
    }

    public function logData($str, $title=null){
        // Log for few days only
        $log = (date('Y-m-d') < '2021-07-02');
        if($log){
            $fp = fopen('./uploads/maverick-log.txt', 'a');

            fwrite($fp, "\r\n".'-------'. ($title ? $title : '')  .'-'. date('c').'-------'."\r\n");
            fwrite($fp, $str);

            fclose($fp);
        }
    }

    public function cardAuthorization($nameOnCard, $cardNumber, $cardExp, $cardCvv='', $address=null, $city=null, $state=null, $country=null, $zipcode=null)
    {
        $params = [];

        $params['level'] = 1;
        $params['amount'] = 0;
        $params['card']['number'] = $cardNumber;
        $params['card']['exp'] = $cardExp;// format: mm/yy
        $params['card']['name'] = $nameOnCard;
        $params['card']['verification']['cvv'] = $cardCvv;

        if($address) {
            $params['card']['address']['street'] = $address;
        }
        if($city != null) {
            $params['card']['address']['city'] = $city;
        }
        if($state) {
            $params['card']['address']['state'] = $state;
        }
        if($country) {
            $params['card']['address']['country'] = $country;
        }
        if($zipcode) {
            $params['card']['address']['zip'] = $zipcode;
        }

        $response = $this->request('POST', '/payment/card-authentication',  $params);
        
        return $response;
    }

    public function create3Ds($cardNumber, $cardExp, $amount){
        $params = [];
        $params['amount'] = $amount;
        $params['card']['number'] = $cardNumber;
        $params['card']['exp'] = $cardExp; // format: mm/yy

        $params['browserData']['acceptHeader'] = 'application/json';
        $params['browserData']['userAgent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36';
        $params['browserData']['language'] = 'en-US';
        $params['browserData']['timezone'] = '+530';
        $params['browserData']['colorDepth'] = '30';
        $params['browserData']['screen']['height'] = '900';
        $params['browserData']['screen']['width'] = '1440';
        $params['browserData']['javaScriptEnabled'] = true;
        $params['browserData']['javaEnabled'] = false;

        $response = $this->request('POST', '/3ds/create',  $params);
    }

    public function check3Ds($id)
    {
        $response = $this->request('GET', '/3ds/'.$id.'/check');
    }

    public function processSale($params)
    {
        
        if($this->apiMode == 'sandbox'){
            // Reset the street and zipcode
            $params['card']['address']['street'] = 8320;
            $params['card']['address']['zip'] = 85284;
        }
        
        // Make Request
        $response = $this->request('POST', '/payment/sale',  $params);

        return $response;
    }

    public function processAuthorize($params)
    {
        
        
        if($this->apiMode == 'sandbox'){
            // Reset the street and zipcode
            $params['card']['address']['street'] = 8320;
            $params['card']['address']['zip'] = 85284;
        }
        
        // Make Request
        $response = $this->request('POST', '/payment/auth',  $params);

        
        return $response;
    }

    public function processAch($params)
    {

        $response = $this->request('POST', '/api/ach',  $params, true);
        return $response;
    }


    public function getDba($getId=true)
    {
        
        $response = $this->request('POST', '/api/dba',  [], true);

        if($response['response_code'] == '200') {
            $r = json_decode($response['response_body'], 1);

            if($getId) {
                return $r['items'][0]['id'];
            } else {
                return $r['items'];
            }
        } else {
            return 1;    
        }
    }

    public function retrieveSale($trxn_id)
    {
        
        $params = [];

        return $this->request('GET', '/payment/'.$trxn_id,  $params);
    }

    // Capture a sale
    public function captureSale($trxn_id, $amount)
    {
        
        $params = ['amount' => $amount];

        return $this->request('POST', '/payment/'.$trxn_id.'/capture',  $params);
    }

    // Void a sale
    public function voidSale($trxn_id, $reason='requested_by_customer')
    {
        
        // reason must be one of these: requested_by_customer, duplicate, fraudulent

        $params = ['reason' => $reason];

        return $this->request('POST', '/payment/'.$trxn_id.'/refund',  []);
    }

    // Refund a sale
    public function refundSale($trxn_id, $amount)
    {
        $params = ['amount' => $amount];

        return $this->request('POST', '/payment/'.$trxn_id.'/refund',  $params);
    } 
    
    // Void ACH sale
    public function voidAchSale($trxn_id)
    {
        
        return $this->request('POST', '/api/ach/'.$trxn_id.'/void',  [], true);
    }

    // Refund ACH sale
    public function refundAchSale($trxn_id, $amount)
    {
        $params = ['amount' => $amount];

        return $this->request('POST', '/api/ach/'.$trxn_id,  $params, true);
    }  

    public function getCustomers()
    {
    
    }

    public function getCustomer($customer_id)
    {
        
    }

    public function createCustomer($email, $name, $other_info=[], $customer_id=null)
    {
        
    }

    public function updateCustomer($customer_id=null, $email, $name='', $other_info=[])
    {

    }

    public function deleteCustomer($customer_id)
    {
        
    }

    public function request($method, $route_url,  $params=[], $ach=false)
    {

        $headers = $this->createHeaders(strtoupper($method));
        
        // Create a callback to capture HTTP headers for the response
        $rheaders = array();
        $headerCallback = function ($ch, $header_line) use (&$rheaders) {
            // Ignore the HTTP request line (HTTP/1.1 200 OK)
            if (strpos($header_line, ":") === false) {
                return strlen($header_line);
            }
            list($key, $value) = explode(":", trim($header_line), 2);
            $rheaders[trim($key)] = trim($value);
            return strlen($header_line);
        };

        $apiEndPoint = ($this->apiMode == 'production' ? 'https://gateway.maverickpayments.com' : 'https://sandbox-gateway.maverickpayments.com');
        
        // If ACH payment
        if($ach) {
            $apiEndPoint = ($this->apiMode == 'production' ? 'https://dashboard.maverickpayments.com' : 'https://sandbox-dashboard.maverickpayments.com');
        }

        $absUrl = self::utf8( ($apiEndPoint).$route_url );
        
        if( !isset($params['source'])) 
        {
            $params['source'] = 'Internet';// Internet, Phone and Mail
        }
        if( !isset($params['origin'])) 
        {
            $params['origin'] = 'Website';// Website, CRM etc
        }
        if( !isset($params['ip'])) 
        {
            $params['ip'] = ['address' => $this->getClientIpAddress()];
        }
        if( !isset($params['terminal'])) 
        {
            $params['terminal'] = ['id' => $this->getTerminalId()];
        }

        $ch = curl_init();

        // Method
        if(strtoupper($method) == 'POST') 
        {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, self::urlEncode($params));

        } else if(strtoupper($method) == 'PATCH') 
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($ch, CURLOPT_POSTFIELDS, self::urlEncode($params));

        } else if(strtoupper($method) == 'GET') {
            
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            if (count($params) > 0) {
                $encoded = self::urlEncode($params);
                $absUrl = "$absUrl?$encoded";
            }

        } else if (strtoupper($method) ==  'DELETE') {
            
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            if (count($params) > 0) {
                $encoded = self::urlEncode($params);
                $absUrl = "$absUrl?$encoded";
            }

        } else {
            throw new Exception("Unrecognized method $method");
        }

        //echo $absUrl;echo $method; print_R($headers);

        curl_setopt($ch, CURLOPT_URL, $absUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, $headerCallback);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $rbody = curl_exec($ch);

        $errno = curl_errno($ch);
        
        if ($rbody === false) {
            $errno = curl_errno($ch);
            $message = curl_error($ch);
            curl_close($ch);
            
            $this->handleCurlError($absUrl, $errno, $message);
        }

        $rcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if($rcode > 300 && getenv('ENV') != 'production'){
            // Log Error
            $CI = &get_instance();
            $CI->general_model->addPaymentLog(17, $_SERVER['REQUEST_URI'], ['env' => $this->apiMode, 'postData' => $params], json_decode($rbody, 1));
        }
        
        return array('response_body'=> $rbody, 'response_code' => $rcode, 'response_headers' => $rheaders);
    }

    /**
     * @param number $errno
     * @param string $message
     * @throws Error\ApiConnection
     */
    private function handleCurlError($url, $errno, $message)
    {
        switch ($errno) {
            case CURLE_COULDNT_CONNECT:
            case CURLE_COULDNT_RESOLVE_HOST:
            case CURLE_OPERATION_TIMEOUTED:
                $msg = "Could not connect to Maverick ($url).  Please check your "
                 . "internet connection and try again.";
                break;
            case CURLE_SSL_CACERT:
            case CURLE_SSL_PEER_CERTIFICATE:
                $msg = "Could not verify Maverick's SSL certificate.  Please make sure "
                 . "that your network is not intercepting certificates.  "
                 . "(Try going to $url in your browser.)  "
                 . "If this problem persists,";
                break;
            default:
                $msg = "Unexpected error communicating with Maverick.  "
                 . "If this problem persists,";
        }
        
        $msg .= "\n\n(Network error [errno $errno]: $message)";
        
        throw new Exception($msg);
    }

    public function d($data=null, $flag=true){

        echo '<pre>';

        print_R($data);

        echo '<pre>';

        if($flag) exit();
    }
}