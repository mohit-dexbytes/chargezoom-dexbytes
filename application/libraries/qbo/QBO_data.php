<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

//error_reporting(1);
/**
 * QBO_customer
 *
 * QuickBooks Online Library
 *
 * @package        QBO_data
 * @author        Chargezoom
 * @version        1.0
 * @based on    Developer Intuite QuickBooks Online
 *
 **/

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Facades\Payment;


class QBO_data
{
    private $qbo_config = array();
    private $merchantID;
    private $realmID;
    private $accessToken;
    private $refreshToken;
    protected $ci;
    public function __construct($merchID)
    {

        $this->ci         = &get_instance();
        $this->merchantID = $merchID;
        $this->ci->load->model('General_model', 'general_model');
        $this->ci->load->model('QBO_models/qbo_invoices_model');
        $this->ci->load->config('quickbooks_online');

        $data = $this->ci->general_model->get_row_data('QBO_token', array('merchantID' => $this->merchantID));
        if(empty($data['accessToken']) || empty($data['refreshToken'])){
            $md5Token = md5(microtime());
            $this->ci->general_model->update_row_data('QBO_token', array('merchantID' => $this->merchantID), ['accessToken' => $md5Token, 'refreshToken' => $md5Token]);
        }
        $this->accessToken  = $data['accessToken'];
        $this->refreshToken = $data['refreshToken'];
        $this->realmID      = $data['realmID'];

        $this->qbo_config = array(
            'auth_mode'       => $this->ci->config->item('AuthMode'),
            'ClientID'        => $this->ci->config->item('client_id'),
            'ClientSecret'    => $this->ci->config->item('client_secret'),
            'accessTokenKey'  => $this->accessToken,
            'refreshTokenKey' => $this->refreshToken,
            'QBORealmID'      => $this->realmID,
            'baseUrl'         => $this->ci->config->item('QBOURL'),

        );

      

    }

    public function get_customer_data($args = [], &$queueNextStatus=null)
    {
        // Queue Offset and Limit
        $queueOffset = isset($args['offset']) ? $args['offset'] : 0;
        $queueLimit = isset($args['max_limit']) ? $args['max_limit'] : 0;

        $realmID = $this->realmID;

        $dataService = DataService::Configure($this->qbo_config);
        $error       = $dataService->getLastError();
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $entities = array();

        $merchID = $this->merchantID;

        $val = array(
            'merchantID' => $merchID,
        );

        $whereCheck = ' ';
        $whereArgs = [];
        if(isset($args['customerId']) && $args['customerId']){
            $customerId = $args['customerId'];
            $whereArgs[] = " Id = '$customerId' ";
        }

        $qbo_data = $this->ci->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CustomerQuery'));

        if (!empty($qbo_data) && empty($whereArgs)) {
            $last_date   = $qbo_data['lastUpdated'];
            $timezone1 = ['time' => $last_date, 'current_format' => date_default_timezone_get(), 'new_format' => 'America/Los_Angeles'];
            $nft = explode(' ', getTimeBySelectedTimezone($timezone1));
            $latedata = $nft[0]."T".$nft[1]."-07:00";

            $sqlCustomer = "SELECT *  FROM Customer where Metadata.LastUpdatedTime > '" . $latedata . "' ";
        } else {
            if(!empty($whereArgs)){
                $whereCheck = 'WHERE ';
                $whereCheck .= implode(' AND', $whereArgs);
                $whereCheck = rtrim($whereCheck, 'AND');
            }
            $sqlCustomer = "SELECT *  FROM Customer $whereCheck";
        }  
        
        $st    = 0;
        $limit = 0;
        $keepRunning = true;
        $entities_data = [];

        $totalRun = 0;
        while ($keepRunning === true) {
            $mres   = MAXRESULT;
            $st_pos = $queueOffset + (MAXRESULT * $st) + 1;

            // Break and Pause the Queue
            if($queueLimit && $queueLimit == $totalRun){
                $queueNextStatus = 'Paused';
                $keepRunning = false;
                break;
            }

            $s_data = $dataService->Query("$sqlCustomer STARTPOSITION $st_pos MAXRESULTS $mres ");
            
            $totalRun += $mres;

            if ($s_data) {
                $entities_data[] = $s_data;

                $st = $st + 1;
                $limit = ($st) * MAXRESULT;
            } else {
                $keepRunning = false;
                break;
            }

        }

        $bug = $dataService->getLastError();

        if ($bug != null) {
            $error1['code'] = $bug->getHttpStatusCode();
            $error1['code'] = $bug->getResponseBody();
            $error          = $error1;
        }

        if(empty($whereArgs)){
            if (!empty($qbo_data)) {$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('00:00:00');
                $updatedata['updatedAt']                              = date('Y-m-d H:i:s');
                if (!empty($entities_data)) {
                    $this->ci->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CustomerQuery'), $updatedata);
                }
    
            } else {
                $updateda = date('Y-m-d') . 'T' . date('00:00:00');
                $upteda   = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CustomerQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
                if (!empty($entities_data)) {
                    $this->ci->general_model->insert_row('tbl_qbo_config', $upteda);
                }
    
            }
        }

        if (!empty($entities_data)) {

            foreach ($entities_data as $entities) {

                if (!empty($entities)) {

                    foreach ($entities as $oneCustomer) {
                        $QBO_customer_details = [];

                        $QBO_customer_details['Customer_ListID'] = $oneCustomer->Id;
                        $QBO_customer_details['firstname']       = $oneCustomer->GivenName;
                        $QBO_customer_details['lastname']        = $oneCustomer->FamilyName;
                        $QBO_customer_details['fullname']        = $oneCustomer->DisplayName;
                        $QBO_customer_details['userEmail']       = ($oneCustomer->PrimaryEmailAddr) ? $oneCustomer->PrimaryEmailAddr->Address : '';
                        $QBO_customer_details['phoneNumber']     = ($oneCustomer->PrimaryPhone) ? $oneCustomer->PrimaryPhone->FreeFormNumber : '';

                        if (!empty($oneCustomer->BillAddr->Line1)) {
                            $QBO_customer_details['address1'] = ($oneCustomer->BillAddr->Line1) ? $oneCustomer->BillAddr->Line1 : '';
                        }

                        if (!empty($oneCustomer->BillAddr->Line1)) {
                            $QBO_customer_details['address2'] = ($oneCustomer->BillAddr->Line2) ? $oneCustomer->BillAddr->Line2 : '';
                        }

                        if (!empty($oneCustomer->BillAddr->PostalCode)) {
                            $QBO_customer_details['zipCode'] = ($oneCustomer->BillAddr) ? $oneCustomer->BillAddr->PostalCode : '';
                        }

                        if (!empty($oneCustomer->BillAddr->Country)) {
                            $QBO_customer_details['Country'] = ($oneCustomer->BillAddr->Country) ? $oneCustomer->BillAddr->Country : '';
                        }

                        if (!empty($oneCustomer->BillAddr->CountrySubDivisionCode)) {
                            $QBO_customer_details['State'] = ($oneCustomer->BillAddr->CountrySubDivisionCode) ? $oneCustomer->BillAddr->CountrySubDivisionCode : '';
                        }

                        if (!empty($oneCustomer->BillAddr->City)) {
                            $QBO_customer_details['City'] = ($oneCustomer->BillAddr->City) ? $oneCustomer->BillAddr->City : '';
                        }

                        if (!empty($oneCustomer->ShipAddr->Line1)) {
                            $QBO_customer_details['ship_address1'] = ($oneCustomer->ShipAddr->Line1) ? $oneCustomer->ShipAddr->Line1 : '';
                        }

                        if (!empty($oneCustomer->ShipAddr->Line2)) {
                            $QBO_customer_details['ship_address2'] = ($oneCustomer->ShipAddr->Line2) ? $oneCustomer->ShipAddr->Line2 : '';
                        }

                        if (!empty($oneCustomer->ShipAddr->PostalCode)) {
                            $QBO_customer_details['ship_zipcode'] = ($oneCustomer->ShipAddr) ? $oneCustomer->ShipAddr->PostalCode : '';
                        }

                        if (!empty($oneCustomer->ShipAddr->Country)) {
                            $QBO_customer_details['ship_country'] = ($oneCustomer->ShipAddr->Country) ? $oneCustomer->ShipAddr->Country : '';
                        }

                        if (!empty($oneCustomer->ShipAddr->CountrySubDivisionCode)) {
                            $QBO_customer_details['ship_state'] = ($oneCustomer->ShipAddr->CountrySubDivisionCode) ? $oneCustomer->ShipAddr->CountrySubDivisionCode : '';
                        }

                        if (!empty($oneCustomer->ShipAddr->City)) {
                            $QBO_customer_details['ship_city'] = ($oneCustomer->ShipAddr->City) ? $oneCustomer->ShipAddr->City : '';
                        }

                        $QBO_customer_details['companyName'] = str_replace("'", "\'",$oneCustomer->CompanyName);
                        // $QBO_customer_details['companyName'] = $oneCustomer->CompanyName;
                        $QBO_customer_details['companyID']   = $realmID;
                        $QBO_customer_details['createdAt']   = date("Y-m-d H:i:s", strtotime($oneCustomer->MetaData->CreateTime));
                        $QBO_customer_details['updatedAt']   = date("Y-m-d H:i:s", strtotime($oneCustomer->MetaData->LastUpdatedTime));
                        $QBO_customer_details['listID']      = '1';
                        $QBO_customer_details['merchantID']  = $merchID;
                        if ($this->ci->general_model->get_num_rows('QBO_custom_customer', array('companyID' => $realmID, 'Customer_ListID' => $oneCustomer->Id, 'merchantID' => $merchID)) > 0) {

                            $this->ci->general_model->update_row_data('QBO_custom_customer', array('companyID' => $realmID, 'Customer_ListID' => $oneCustomer->Id, 'merchantID' => $merchID), $QBO_customer_details);

                        } else {

                            $this->ci->general_model->insert_row('QBO_custom_customer', $QBO_customer_details);

                        }

                    }
                }

            }

            $qbo_log = array('syncType' => 'QC','type' => 'customerImport','qbStatus' => 1, 'qbAction' => 'CustomerDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $this->merchantID);
            $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
            if($syncid){
                $qbSyncID = 'QC-'.$syncid;

                $qbSyncStatus = 'Customer Data Imported';
                $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
            }
        }

        return true;
    }

    public function get_items_data()
    {
        $realmID = $this->realmID;

        $dataService = DataService::Configure($this->qbo_config);

        //$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $dataService->setLogLocation("./uploads/qbo_logs.txt");

        $entities = array();

        $merchID = $this->merchantID;
        $val     = array(
            'merchantID' => $merchID,
        );

        $qbo_data = $this->ci->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'ItemQuery'));

        if (!empty($qbo_data)) {
            $last_date = $qbo_data['lastUpdated'];
            //  2015-07-23T10:58:12-07:00
            // $current_date =date('Y-m-d H:i:s');
            $my_timezone = date_default_timezone_get();
            $from        = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
            $to          = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
            $last_date   = $this->ci->general_model->datetimeconvqbo($last_date, $from, $to);
            $last_date1  = date('Y-m-d H:i', strtotime('-9 hour', strtotime($last_date)));

            $latedata = date('Y-m-d', strtotime($last_date1)) . 'T' . date('H:i:s', strtotime($last_date1));

            // $entities = $dataService->Query("SELECT * FROM Item where Metadata.LastUpdatedTime > '" . $latedata . "' ");
            $sqlItem = "SELECT *  FROM Item where Active IN (false, true) AND Metadata.LastUpdatedTime > '" . $latedata . "' ";


        } else {
            // $entities = $dataService->Query("SELECT * FROM Item ");
            // $count = $dataService->Query("SELECT count(*) FROM Item  ");
            // if ($count) {

            // } else {
            //     $count = 3000;
            // }

            $bug = $dataService->getLastError();
            $sqlItem = "SELECT *  FROM Item where Active IN (false, true)";

        }

        $st    = 0;
        $limit = 0;
        $keepRunning = true;
        $entities_data = [];

        while ($keepRunning === true) {
            $mres   = MAXRESULT;
            $st_pos = MAXRESULT * $st + 1;

            $s_data = $dataService->Query("$sqlItem STARTPOSITION $st_pos MAXRESULTS $mres ");
            if ($s_data) {
                $entities_data[] = $s_data;

                $st = $st + 1;
                $limit = ($st) * MAXRESULT;
            } else {
                $keepRunning = false;
                break;
            }

        }

        $bug = $dataService->getLastError();

        if ($bug != null) {
            $error1['code'] = $bug->getHttpStatusCode();
            $error1['code'] = $bug->getResponseBody();
            $error          = $error1;
        }

        if (!empty($qbo_data)) {
            $updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('00:00:00');
            $updatedata['updatedAt']   = date('Y-m-d H:i:s');
            if (!empty($entities_data)) {
                $this->ci->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'ItemQuery'), $updatedata);
            }

        } else {
            $updateda = date('Y-m-d') . 'T' . date('00:00:00');
            $upteda   = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'ItemQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
            if (!empty($entities_data)) {
                $this->ci->general_model->insert_row('tbl_qbo_config', $upteda);
            }

        }

        if (!empty($entities_data)) {
            foreach($entities_data as $entities){
                if (!empty($entities)) {
        

                    //echo '<pre>';print_R((array)$entities[0]->ItemGroupDetail->ItemGroupLine);die;
                    foreach ($entities as $oneInvoice) {
        
                        $QBO_item_details['productID']         = $oneInvoice->Id;
                        $QBO_item_details['IsActive']          = $oneInvoice->Active;
                        $QBO_item_details['TimeCreated']       = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->CreateTime));
                        $QBO_item_details['TimeModified']      = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->LastUpdatedTime));
                        $QBO_item_details['Name']              = $oneInvoice->Name;
                        $QBO_item_details['SKU']               = $oneInvoice->Sku;
                        $QBO_item_details['SalesDescription']  = $oneInvoice->Description;
                        $QBO_item_details['saleCost']          = $oneInvoice->UnitPrice;
                        $QBO_item_details['Type']              = $oneInvoice->Type;
                        $QBO_item_details['parent_ListID']     = $oneInvoice->ParentRef;
                        $QBO_item_details['QuantityOnHand']    = $oneInvoice->QtyOnHand;
                        $QBO_item_details['InvStartDate']      = $oneInvoice->InvStartDate;
                        $QBO_item_details['AssetAccountRef']   = $oneInvoice->AssetAccountRef;
                        $QBO_item_details['purchaseCost']      = $oneInvoice->PurchaseCost;
                        $QBO_item_details['purchaseDesc']      = $oneInvoice->PurchaseDesc;
                        $QBO_item_details['IncomeAccountRef']  = $oneInvoice->IncomeAccountRef;
                        $QBO_item_details['ExpenseAccountRef'] = $oneInvoice->ExpenseAccountRef;
                        $QBO_item_details['CompanyID']         = $realmID;
                        $QBO_item_details['merchantID']        = $merchID;
                        if ($this->ci->general_model->get_num_rows('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneInvoice->Id, 'merchantID' => $merchID)) > 0) {
        
                            $this->ci->general_model->update_row_data('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneInvoice->Id, 'merchantID' => $merchID), $QBO_item_details);
        
                        } else {
        
                            $this->ci->general_model->insert_row('QBO_test_item', $QBO_item_details);
        
                        }

                        // IF items is group item then ItemGroupDetail
                        if($oneInvoice->Type == 'Group')
                        {
                            $qboTestItem = $this->ci->general_model->get_row_data('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneInvoice->Id, 'merchantID' => $merchID));

                            // CHeck Group Child Items
                            if ($this->ci->general_model->get_num_rows('qbo_group_lineitem', array('groupListID' => $qboTestItem['ListID'])) > 0) {
                                // Delete Previous 
                                $this->ci->general_model->delete_row_data('qbo_group_lineitem', array('groupListID' => $qboTestItem['ListID']));
                            }


                            // Add Group Child Items
                            $priceTotal = 0;
                            if(isset($oneInvoice->ItemGroupDetail) && !empty($oneInvoice->ItemGroupDetail)){
                                foreach($oneInvoice->ItemGroupDetail->ItemGroupLine as $groupLineItem)
                                {
                                    
                                    if(isset($groupLineItem->ItemRef) && !empty($groupLineItem->ItemRef)){

                                        $itemData =    $this->ci->general_model->get_row_data('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $groupLineItem->ItemRef, 'merchantID' => $merchID));
                                        

                                        if ($itemData) {
                                            $input_data1 = array();
                                            $input_data1['itemListID'] = $itemData['ListID'];
                                            $input_data1['FullName']   = $itemData['Name'];
                                            $input_data1['Quantity']   = $groupLineItem->Qty;
                                            $input_data1['Price']     = $itemData['saleCost'];
                                            $input_data1['groupListID'] = $qboTestItem['ListID'];

                                            $priceTotal += (float)$itemData['saleCost'];
                                            $this->ci->general_model->insert_row('qbo_group_lineitem', $input_data1);
                                        }
                                    }
                                }
                            }
                            

                            // Update Product Price
                            $this->ci->general_model->update_row_data('QBO_test_item', array('ListID' => $qboTestItem['ListID']), ['saleCost' => $priceTotal ]);
                        }
                    }
        
                    $qbo_log = array('syncType' => 'QC','type' => 'productImport','qbStatus' => 1, 'qbAction' => 'ItemsDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $this->merchantID);
                    $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
                    if($syncid){
                        $qbSyncID = 'QC-'.$syncid;
                        $qbSyncStatus = 'Items Data Imported Init';
                        $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
                    }

                }
            }

            $qbo_log = array('syncType' => 'QC','type' => 'itemImport','qbStatus' => 1, 'qbAction' => 'ItemsDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $this->merchantID);
            $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
            if($syncid){
                $qbSyncID = 'QC-'.$syncid;
                $qbSyncStatus = 'Items Data Imported Init';
                $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
            }
        }

    }

    public function get_account_data()
    {

        $realmID = $this->realmID;
        $merchID = $this->merchantID;

        $dataService = DataService::Configure($this->qbo_config);

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $allAccounts = $dataService->FindAll('Account');
        

        foreach ($allAccounts as $oneAccount) {

            $QBO_item_details['accountID']          = $oneAccount->Id;
            $QBO_item_details['accountName']        = $oneAccount->Name;
            $QBO_item_details['accountType']        = $oneAccount->AccountType;
            $QBO_item_details['accountTypeDetails'] = $oneAccount->AccountSubType;
            $QBO_item_details['companyID']          = $realmID;
            $QBO_item_details['merchantID']         = $this->merchantID;
            $QBO_item_details['createdAt']          = date('Y-m-d H:i:s');

            $ac_num = $this->ci->general_model->get_num_rows('QBO_accounts_list', array('merchantID' => $this->merchantID, 'accountID' => $oneAccount->Id, 'companyID' => $realmID));
            if ($ac_num > 0) {
                $this->ci->general_model->update_row_data('QBO_accounts_list', array('merchantID' => $this->merchantID, 'accountID' => $oneAccount->Id, 'companyID' => $realmID), $QBO_item_details);
            } else {

                $this->ci->general_model->insert_row('QBO_accounts_list', $QBO_item_details);
            }

        }

        $qbo_log = array('syncType' => 'QC','type' => 'accountImport','qbStatus' => 1, 'qbAction' => 'AccountDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $this->merchantID);
        $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
        if($syncid){
            $qbSyncID = 'QC-'.$syncid;
            $qbSyncStatus = 'Account Data Imported Init';
            $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
        }
    }

    public function get_tax_data()
    {
        $realmID = $this->realmID;
        $merchID = $this->merchantID;

        $dataService = DataService::Configure($this->qbo_config);

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        /***********************Gettting Agency Data*****************/
		$condition =  array('merchantID'=>$merchID);

        $tagency = $dataService->Query("SELECT * FROM TaxAgency  ");
        //print_r($tagency); die;
        $err_msg = '';
        $error   = $dataService->getLastError();
        if ($error != null) {
            $err_msg .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
            $err_msg .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            $err_msg .= "The Response message is: " . $error->getResponseBody() . "\n";

        } else {
            if (!empty($tagency)) {
                foreach ($tagency as $oneagen) {
                    $agcy_num = $this->ci->general_model->get_num_rows('tbl_taxagency_qbo', array('agencyID' => $oneagen->Id, 'merchantID' => $merchID, 'releamID' => $realmID));

                    $QBO_tax_agency['agencyID']     = $oneagen->Id;
                    $QBO_tax_agency['agencyName']   = $oneagen->DisplayName;
                    $QBO_tax_agency['agencyDomain'] = $oneagen->domain;
                    $QBO_tax_agency['createdAt']    = date('Y-m-d H:i:s', strtotime($oneagen->MetaData->CreateTime));
                    $QBO_tax_agency['updatedAt']    = date('Y-m-d H:i:s', strtotime($oneagen->MetaData->LastUpdatedTime));
                    $QBO_tax_agency['merchantID']   = $merchID;
                    $QBO_tax_agency['releamID']     = $realmID;
                    if ($oneagen->TaxTrackedOnSales) {
                        $QBO_tax_agency['trackOnSales'] = 1;
                    } else {
                        $QBO_tax_agency['trackOnSales'] = 0;
                    }
                    if ($agcy_num == 0) {
                        $this->ci->general_model->insert_row('tbl_taxagency_qbo', $QBO_tax_agency);
                    } else {
                        $this->ci->general_model->update_row_data('tbl_taxagency_qbo', array('agencyID' => $oneagen->Id, 'merchantID' => $merchID, 'releamID' => $realmID), $QBO_tax_agency);
                    }

                }

                $qbo_log = array('syncType' => 'QC','type' => 'taxImport','qbStatus' => 1, 'qbAction' => 'TaxDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
                $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
                if($syncid){
                    $qbSyncID = 'QC-'.$syncid;
                    $qbSyncStatus = 'Tax Data Imported Init';
                    $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
                }
            }
        }

        /************************End Tax Agency****************/

        $taxs = $dataService->Query("SELECT * FROM TaxRate where RateValue !='' ");

        $taxCode = $dataService->Query("SELECT * FROM TaxCode where Name !='NON'");

        $err_msg = '';
        $error   = $dataService->getLastError();
        if ($error != null) {
            $err_msg .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
            $err_msg .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            $err_msg .= "The Response message is: " . $error->getResponseBody() . "\n";

        }

        if (!empty($taxs)){
            foreach ($taxs as $onetax) {
                $QBO_tax_details['taxID']        = $onetax->Id;
                $QBO_tax_details['friendlyName'] = $onetax->Name;
                $QBO_tax_details['taxRate']      = $onetax->RateValue;
                $QBO_tax_details['date']         = date('Y-m-d H:i:s', strtotime($onetax->MetaData->CreateTime));
                $QBO_tax_details['merchantID']   = $merchID;
                $QBO_tax_details['agencyID']     = $onetax->AgencyRef;
    
                $ch_num = $this->ci->general_model->get_num_rows('tbl_taxes_qbo', array('taxID' => $onetax->Id, 'merchantID' => $merchID));
                if ($ch_num == 0) {
                    $this->ci->general_model->insert_row('tbl_taxes_qbo', $QBO_tax_details);
                } else {
                    $this->ci->general_model->update_row_data('tbl_taxes_qbo', array('taxID' => $onetax->Id, 'merchantID' => $merchID), $QBO_tax_details);
                }
    
            }
        }

        if(isset($taxCode) && !empty($taxCode)) {
			$del1 = $this->ci->general_model->delete_row_data('tbl_taxe_code_qbo', $condition);
			foreach ($taxCode as $taxref) {
				$totalTaxRate = 0;
				$taxIds= '';
				
				if($taxref->SalesTaxRateList == ''){
					$tax_num = 0;
				} else if(array_key_exists("TaxRateRef",$taxref->SalesTaxRateList->TaxRateDetail)){
					$taxIds = $tax_num = $taxref->SalesTaxRateList->TaxRateDetail->TaxRateRef;
					$keyT = array_search($tax_num, array_column($taxs, 'Id'));
					$taxData = $taxs[$keyT];
					$totalTaxRate = $taxData->RateValue;
				} else if(array_key_exists("TaxRateRef",$taxref->SalesTaxRateList->TaxRateDetail[0])){
					$tax_num = $taxref->SalesTaxRateList->TaxRateDetail[0]->TaxRateRef;

					$taxIds = [];
					$taxRateDetail = $taxref->SalesTaxRateList->TaxRateDetail;
					foreach ($taxRateDetail as $trData) {
						$keyT = array_search($trData->TaxRateRef, array_column($taxs, 'Id'));
						$taxData = $taxs[$keyT];
						$totalTaxRate += $taxData->RateValue;
						$taxRateRef = $trData->TaxRateRef;
						$taxIds[] = $taxRateRef;
					}
					$taxIds = implode(',',$taxIds);
				}
				
				$QBO_tax_ref['taxRef'] = $tax_num;
				$QBO_tax_ref['Name'] = $taxref->Name;
				$QBO_tax_ref['Description'] = $taxref->Description;
				$QBO_tax_ref['taxID'] = $taxref->Id;
				$QBO_tax_ref['merchantID'] = $merchID;
				
				$QBO_tax_ref['total_tax_rate'] = $totalTaxRate;
				$QBO_tax_ref['tax_ids'] = $taxIds;
				$QBO_tax_ref['is_active'] = (strtolower($taxref->Active) == 'true') ? 1 : 0;

				$ch_num1 = $this->ci->general_model->get_num_rows('tbl_taxe_code_qbo',array('taxID'=>$taxref->Id, 'merchantID'=>$merchID));
				if($ch_num1==0)
				    $this->ci->general_model->insert_row('tbl_taxe_code_qbo', $QBO_tax_ref);
				else
					$this->ci->general_model->update_row_data('tbl_taxe_code_qbo',array('taxID'=>$onetax->Id, 'merchantID'=>$merchID), $QBO_tax_ref);
			}
		}

    }

    public function get_credit_data()
    {
        $realmID = $this->realmID;
        $merchID = $this->merchantID;

        $dataService = DataService::Configure($this->qbo_config);

        //$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $qbo_data = $this->ci->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery'));

        if (!empty($qbo_data)) {

            $entities = $dataService->Query("SELECT * FROM CreditMemo where Metadata.LastUpdatedTime > '" . $qbo_data['lastUpdated'] . "' ");

        } else {
            $entities = $dataService->Query("SELECT * FROM CreditMemo ");

        }

        $eror  = '';
        $error = $dataService->getLastError();
        if ($error != null) {
            $eror .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
            $eror .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            $eror .= "The Response message is: " . $error->getResponseBody() . "\n";

        }

        if (!empty($qbo_data)) {$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('00:00:00');
            $updatedata['updatedAt']                              = date('Y-m-d H:i:s');
            if (!empty($entities)) {
                $this->ci->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery'), $updatedata);
            }

        } else {
            $updateda = date('Y-m-d') . 'T' . date('00:00:00');
            $upteda   = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
            if (!empty($entities)) {
                $this->ci->general_model->insert_row('tbl_qbo_config', $upteda);
            }

        }

        //  print_r($entities); die;

        if (!empty($entities)) {

            foreach ($entities as $oneCredit) {

                //  print_r($oneCredit); die;

                $qb_credit['TxnID']            = $oneCredit->Id;
                $qb_credit['CustomerListID']   = $oneCredit->CustomerRef;
                $qb_credit['RefNumber']        = ($oneCredit->AutoDocNumber) ? $oneCredit->AutoDocNumber : $oneCredit->DocNumber;
                $qb_credit['CreditRemaining']  = $oneCredit->RemainingCredit;
                $qb_credit['AccountListID']    = $oneCredit->ARAccountRef;
                $qb_credit['TxnDate']          = $oneCredit->TxnDate;
                $qb_credit['DueDate']          = $oneCredit->DueDate;
                $qb_credit['TxnNumber']        = $oneCredit->DocNumber;
                $qb_credit['customerMemoNote'] = $oneCredit->CustomerMemo;
                $qb_credit['creditMemoNote']   = $oneCredit->PrivateNote;
                $qb_credit['TimeCreated']      = date('Y-m-d H:i:s', strtotime($oneCredit->MetaData->CreateTime));
                $qb_credit['TimeModified']     = date('Y-m-d H:i:s', strtotime($oneCredit->MetaData->LastUpdatedTime));
                $qb_credit['SubTotal']         = $oneCredit->TotalAmt;
                $qb_credit['TotalAmount']      = $oneCredit->TotalAmt;
                $qb_credit['merchantID']       = $merchID;
                $qb_credit['companyID']        = $realmID;

                // print_r($qb_credit); die;
                if ($this->ci->general_model->get_num_rows('qbo_customer_credit', array('companyID' => $realmID, 'TxnID' => $oneCredit->Id, 'merchantID' => $merchID)) > 0) {

                    $this->ci->general_model->update_row_data('qbo_customer_credit', array('companyID' => $realmID, 'TxnID' => $oneCredit->Id, 'merchantID' => $merchID), $qb_credit);

                } else {

                    $this->ci->general_model->insert_row('qbo_customer_credit', $qb_credit);

                }

            }

            $qbo_log = array('syncType' => 'QC','type' => 'creditImport','qbStatus' => 1, 'qbAction' => 'CreditDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
            $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
            if($syncid){
                $qbSyncID = 'QC-'.$syncid;
                $qbSyncStatus = 'Credit Data Imported Init';
                $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
            }
        }
    }

    public function get_invoice_data($args = [], &$queueNextStatus=null)
    {
        $forcePull = isset($args['forcePull']) ? $args['forcePull'] : false;
        // Queue Offset and Limit
        $queueOffset = isset($args['offset']) ? $args['offset'] : 0;
        $queueLimit = isset($args['max_limit']) ? $args['max_limit'] : 0;

        $realmID     = $this->realmID;
        $dataService = DataService::Configure($this->qbo_config);
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $entities = array();
        $entities_data = [];

        $merchID = $this->merchantID;

        $val = array(
            'merchantID' => $merchID,
        );

        $whereCheck = ' ';
        $whereArgs = [];
        if(isset($args['customerId']) && $args['customerId']){
            $customerId = $args['customerId'];
            $whereArgs[] = " CustomerRef = '$customerId' ";
        }

        if(isset($args['invoiceID']) && $args['invoiceID']){
            $invoiceID = $args['invoiceID'];
            $whereArgs[] = " Id = '$invoiceID' ";
        }
        
        $qbo_data = $this->ci->general_model->get_select_data('tbl_qbo_config', array('lastUpdated', 'lastTimeStamp'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'InvoiceQuery'));
        
        $latedata = date('Y-m-d') . 'T' . date('H:i:s');
        
        // If it is force Pull true then do not check the last meta data time
        if (!$forcePull && !empty($qbo_data)) {
            $last_date   = $qbo_data['lastUpdated'];
            $lastTimeStamp   = $qbo_data['lastTimeStamp'];

            if ($lastTimeStamp == 'UTC') {
                $my_timezone = date_default_timezone_get();
                $from = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
                $to   = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
                $last_date =  $this->ci->general_model->datetimeconvqbo($last_date, $from, $to);
                $last_date1 = date('Y-m-d H:i', strtotime('-9 hour', strtotime($last_date)));

                $latedata = date('Y-m-d', strtotime($last_date1)) . 'T' . date('H:i:s', strtotime($last_date1));
            } else {
                $latedata = $last_date;
            }

            $whereArgs[] = " Metadata.LastUpdatedTime > '$latedata' ";

        } else  if(empty($whereArgs)){
            // If there is no where condition then pass this
            $whereArgs[] = " Balance > '0' ";
        }

        // Concatenate all where conditions with AND
        if(!empty($whereArgs)){
            $whereCheck = implode(' AND', $whereArgs);
            $whereCheck = rtrim($whereCheck, 'AND');
        }

        $st    = 0;
        $limit = 0;
        $totalRun = 0;
        $keepRunning = true;
        while ($keepRunning === true) {
            $mres   = MAXRESULT;
            $st_pos = $queueOffset + (MAXRESULT * $st) + 1;

            // Break and Pause the Queue
            if($queueLimit && $queueLimit == $totalRun){
                $queueNextStatus = 'Paused';
                $keepRunning = false;
                break;
            }

            $invoiceSQL = "SELECT *  FROM Invoice where $whereCheck STARTPOSITION $st_pos MAXRESULTS $mres ";

            $totalRun += $mres;

            $s_data = $dataService->Query($invoiceSQL);

            if ($s_data) {
                $entities_data[] = $s_data;

                $st = $st + 1;

                $limit = ($st) * MAXRESULT;

            } else {
                $keepRunning = false;
                break;
            }

        }

        $err   = '';
        $error = $dataService->getLastError();
        if ($error != null) {
            $err .= $error->getHttpStatusCode() . "\n";
            $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            $err .= "The Response message is: " . $error->getResponseBody() . "\n";
        }

        //echo '<pre>';print_R($entities_data);die;

        if (!empty($entities_data)) {
            // get all invoices of customer
            $get_all_invoices = $this->ci->general_model->get_table_select_data('QBO_test_invoice', ['id', 'invoiceID'],array('companyID' => $realmID, 'merchantID' => $merchID));
            $customerExistInvoices = [];
            if($get_all_invoices){
                foreach ($get_all_invoices as $key => $value) {
                    $customerExistInvoices[$value['invoiceID']] = $value;
                }
            }
            
            $prevUpdateTime = strtotime($latedata);
            foreach ($entities_data as $entities) {

                if (!empty($entities)) {
                    $updatedInvoiceBatch = [];
                    $insertInvoiceBatch = [];
                    $insertInvoiceItemBatch = [];
                    foreach ($entities as $oneInvoice) {
                        #Update Last Sync
                        $mostUpdatedTime = strtotime($oneInvoice->MetaData->LastUpdatedTime);
                        if($mostUpdatedTime > $prevUpdateTime){
                            $prevUpdateTime = $mostUpdatedTime;
                            $latedata= $oneInvoice->MetaData->LastUpdatedTime;
                        }

                        $QBO_invoice_details = [];
                        $QBO_invoice_details['invoiceID'] = $oneInvoice->Id;
                        $QBO_invoice_details['refNumber'] = $oneInvoice->DocNumber;

                        // if ($oneInvoice->ShipAddr != "") {
                            $QBO_invoice_details['ShipAddress_Addr1']      = isset($oneInvoice->ShipAddr->Line1) ? $oneInvoice->ShipAddr->Line1 : '';
                            $QBO_invoice_details['ShipAddress_Addr2']      = isset($oneInvoice->ShipAddr->Line2) ? $oneInvoice->ShipAddr->Line2 : '';
                            $QBO_invoice_details['ShipAddress_City']       = isset($oneInvoice->ShipAddr->City) ? $oneInvoice->ShipAddr->City : '';
                            $QBO_invoice_details['ShipAddress_State']      = isset($oneInvoice->ShipAddr->CountrySubDivisionCode) ? $oneInvoice->ShipAddr->CountrySubDivisionCode : '';
                            $QBO_invoice_details['ShipAddress_Country']    = isset($oneInvoice->ShipAddr->Country) ? $oneInvoice->ShipAddr->Country : '';
                            $QBO_invoice_details['ShipAddress_PostalCode'] = isset($oneInvoice->ShipAddr->PostalCode) ? $oneInvoice->ShipAddr->PostalCode : '';
                        // }
                        // if ($oneInvoice->BillAddr != "") {
                            $QBO_invoice_details['BillAddress_Addr1']   = isset($oneInvoice->BillAddr->Line1) ? $oneInvoice->BillAddr->Line1 : '';
                            $QBO_invoice_details['BillAddress_Addr2']   = isset($oneInvoice->BillAddr->Line2) ? $oneInvoice->BillAddr->Line2 : '';
                            $QBO_invoice_details['BillAddress_City']    = isset($oneInvoice->BillAddr->City) ? $oneInvoice->BillAddr->City : '';
                            $QBO_invoice_details['BillAddress_State']   = isset($oneInvoice->BillAddr->CountrySubDivisionCode) ? $oneInvoice->BillAddr->CountrySubDivisionCode : '';
                            $QBO_invoice_details['BillAddress_Country'] = isset($oneInvoice->BillAddr->Country) ? $oneInvoice->BillAddr->Country : '';

                            $QBO_invoice_details['BillAddress_PostalCode'] = isset($oneInvoice->BillAddr->PostalCode) ? $oneInvoice->BillAddr->PostalCode : '';
                        // }

                        $QBO_invoice_details['DueDate']          = $oneInvoice->DueDate;
                        $QBO_invoice_details['BalanceRemaining'] = $oneInvoice->Balance;
                        $QBO_invoice_details['Total_payment']    = $oneInvoice->TotalAmt;
                        $QBO_invoice_details['CustomerListID']   = $oneInvoice->CustomerRef;

                        if ($oneInvoice->Balance == 0) {
                            $QBO_invoice_details['IsPaid'] = 1;
                        } else {
                            $QBO_invoice_details['IsPaid'] = 0;
                        }

                        $QBO_invoice_details['merchantID']   = $merchID;
                        $QBO_invoice_details['companyID']    = $realmID;
                        $QBO_invoice_details['TimeCreated']  = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->CreateTime));
                        $QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->LastUpdatedTime));

                        if ($oneInvoice->TxnTaxDetail) {
                            $taxref = $oneInvoice->TxnTaxDetail->TxnTaxCodeRef;
                            if ($taxref) {
                                $tax_data =    $this->ci->general_model->get_row_data('tbl_taxe_code_qbo', array('taxID' => $taxref, 'merchantID' => $merchID));
                                if(isset($tax_data['total_tax_rate']) && $tax_data['total_tax_rate'] != ''){
                                    $QBO_invoice_details['taxRate'] = $tax_data['total_tax_rate'];
                                }else{
                                    $QBO_invoice_details['taxRate'] = 0;
                                }
                                
                                $QBO_invoice_details['taxID'] = $taxref;
                            } else {
                                $QBO_invoice_details['taxRate'] = 0;
                                $QBO_invoice_details['taxID'] = null;
                            }
            
                            $QBO_invoice_details['totalTax'] = $oneInvoice->TxnTaxDetail->TotalTax;
                        } else {
                            $QBO_invoice_details['totalTax'] = 0.00;
                            $QBO_invoice_details['taxRate'] = 0;
                            $QBO_invoice_details['taxID'] = null;
                        }

                        // if ($this->ci->general_model->get_num_rows('QBO_test_invoice', array('companyID' => $realmID, 'invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID)) > 0) {
                        if (isset($customerExistInvoices[$oneInvoice->Id])){
                            $QBO_invoice_details['id'] = $customerExistInvoices[$oneInvoice->Id]['id'];
                            $updatedInvoiceBatch[] = $QBO_invoice_details;
                            // $this->ci->general_model->update_row_data('QBO_test_invoice', array('companyID' => $realmID, 'invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID), $QBO_invoice_details);
                            
                            $line_items = $oneInvoice->Line;
                            $TotalAmt = $oneInvoice->TotalAmt;

                            $l_data = array();
                            $k      = 0;

                            if (!empty($line_items)) {

                                $this->ci->general_model->delete_row_data('tbl_qbo_invoice_item', array('invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID, 'releamID' => $realmID));
                                foreach ($line_items as $line) {

                                    $l_data = array();
                                    if (isset($line->Id)) {

                                        $l_data['itemID'] = $line->LineNum;

                                        if ($line->Description) {
                                            $l_data['itemDescription'] = $line->Description;
                                        }else{
                                            $l_data['itemDescription'] = null;
                                        }

                                        $l_data['totalAmount'] = ($line->Amount) ? $line->Amount : '0';

                                        $saleItem = $line->SalesItemLineDetail;
                                        
                                        $l_data['itemRefID'] = ($saleItem && isset($saleItem->ItemRef)) ? $saleItem->ItemRef : '';

                                        if ($saleItem && isset($saleItem->UnitPrice)) {
                                            $l_data['itemPrice'] = $saleItem->UnitPrice;
                                        } else {
                                            $l_data['itemPrice'] = $line->Amount;
                                        }

                                        // GroupLineDetail
                                        if(isset($line->GroupLineDetail) && isset($line->GroupLineDetail->GroupItemRef)){
                                            /* Fetch Group Item*/
                                            $groupLineItems = $line->GroupLineDetail->Line;
                                            $groupAmountTotal = 0;
                                            if(!empty($groupLineItems)){
                                                foreach ($groupLineItems as $groupLine)
                                                {
                                                    $groupAmountTotal += $groupLine->Amount;
                                                }
                                            }

                                            $l_data['itemRefID'] = $line->GroupLineDetail->GroupItemRef;
                                            $l_data['totalAmount'] = $groupAmountTotal;
                                            $l_data['itemPrice'] = $groupAmountTotal;
                                        }

                                        if ($saleItem && isset($saleItem->Qty)) {
                                            $l_data['itemQty'] = $saleItem->Qty;
                                        } else {
                                            $l_data['itemQty'] = 1;
                                        }

                                        //   $l_data['itemQty'] =$saleItem->Qty;
                                        $l_data['invoiceID']  = $oneInvoice->Id;
                                        $l_data['merchantID'] = $merchID;
                                        $l_data['releamID']   = $realmID;
                                        $l_data['createdAt']  = date('Y-m-d H:i:s');

                                        if ($saleItem && isset($saleItem->TaxCodeRef))
                                            $l_data['itemTax'] = ($saleItem->TaxCodeRef == "TAX") ? 1 : 0;
                                        else
                                            $l_data['itemTax'] = 0;

                                        $insertInvoiceItemBatch[] = $l_data;

                                        // $this->ci->general_model->insert_row('tbl_qbo_invoice_item', $l_data);

                                    }

                                }

                            }

                        } else {

                            $line_items = $oneInvoice->Line;

                            $l_data = array();
                            $k      = 0;

                            if (!empty($line_items)) {

                                $this->ci->general_model->delete_row_data('tbl_qbo_invoice_item', array('invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID, 'releamID' => $realmID));
                                foreach ($line_items as $line) {

                                    $l_data = array();
                                    if (isset($line->Id)) {

                                        $l_data['itemID'] = $line->LineNum;
                                        if ($line->Description) {
                                            $l_data['itemDescription'] = $line->Description;
                                        }else{
                                            $l_data['itemDescription'] = null;
                                        }

                                        $l_data['totalAmount'] = ($line->Amount) ? $line->Amount : '0';

                                        $saleItem = $line->SalesItemLineDetail;
                                        $l_data['itemRefID'] = ($saleItem && isset($saleItem->ItemRef)) ? $saleItem->ItemRef : '';
                                        if ($saleItem && isset($saleItem->UnitPrice)) {
                                            $l_data['itemPrice'] = $saleItem->UnitPrice;
                                        } else {
                                            $l_data['itemPrice'] = $line->Amount;
                                        }

                                        // GroupLineDetail
                                        if(isset($line->GroupLineDetail) && isset($line->GroupLineDetail->GroupItemRef)){
                                            $l_data['itemRefID'] = $line->GroupLineDetail->GroupItemRef;
                                        }

                                        if ($saleItem && isset($saleItem->Qty)) {
                                            $l_data['itemQty'] = $saleItem->Qty;
                                        } else {
                                            $l_data['itemQty'] = 1;
                                        }

                                        //   $l_data['itemQty'] =$saleItem->Qty;
                                        $l_data['invoiceID']  = $oneInvoice->Id;
                                        $l_data['merchantID'] = $merchID;
                                        $l_data['releamID']   = $realmID;
                                        $l_data['createdAt']  = date('Y-m-d H:i:s');

                                        if ($saleItem && isset($saleItem->TaxCodeRef))
                                            $l_data['itemTax'] = ($saleItem->TaxCodeRef == "TAX") ? 1 : 0;
                                        else
                                            $l_data['itemTax'] = 0;
                                        
                                        $insertInvoiceItemBatch[] = $l_data;
                                        // $this->ci->general_model->insert_row('tbl_qbo_invoice_item', $l_data);
                                    }

                                }
                            }
                            $insertInvoiceBatch[] = $QBO_invoice_details;
                            // $this->ci->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
                        }

                        // $this->ci->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
                    }

                    // update invoices item in batch
                    if($insertInvoiceItemBatch){
                        $this->ci->db->insert_batch('tbl_qbo_invoice_item', $insertInvoiceItemBatch);
                    }

                    // update invoices in batch
                    if($updatedInvoiceBatch){
                        $this->ci->db->update_batch('QBO_test_invoice', $updatedInvoiceBatch, 'id');
                    }
                    // insert invoices in batch
                    if($insertInvoiceBatch){
                        $this->ci->db->insert_batch('QBO_test_invoice', $insertInvoiceBatch);
                    }

                    $qbo_log = array('syncType' => 'QC','type' => 'invoiceImport','qbStatus' => 1, 'qbAction' => 'InvoiceDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $this->merchantID);
                    $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
                    if($syncid){
                        $qbSyncID = 'QC-'.$syncid;
                        $qbSyncStatus = 'Invoice Data Imported Init';
                        $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
                    }
                }

            }
        }
        
        if(empty($whereArgs)){
            if (!empty($qbo_data)) {
                $updatedata['lastUpdated'] = $latedata; // date('Y-m-d') . 'T' . date('H:i:s');
                $updatedata['lastTimeStamp'] = 'America/Los_Angeles'; // date('Y-m-d') . 'T' . date('H:i:s');
                $updatedata['updatedAt'] = date('Y-m-d H:i:s');
                if (!empty($entities_data)) {
                    $this->ci->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'InvoiceQuery'), $updatedata);
                }
    
            } else {
                $updateda = date('Y-m-d') . 'T' . date('H:i:s');
                $upteda   = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'InvoiceQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
                if (!empty($entities_data)) {
                    $this->ci->general_model->insert_row('tbl_qbo_config', $upteda);
                }
    
            }
        }
    }

	public function restore_product_services(){
		$realmID = $this->realmID;
        $merchID = $this->merchantID;

        $dataService = DataService::Configure($this->qbo_config);
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
		$qbo_data = $this->ci->general_model->get_select_data('tbl_qbo_config',array('lastUpdated'),array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'ItemQuery'));
		
		if(!empty($qbo_data))
		{
			$last_date   = $qbo_data['lastUpdated'];
			//  2015-07-23T10:58:12-07:00
			// $current_date =date('Y-m-d H:i:s');
			$my_timezone = date_default_timezone_get();
			$from = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
			$to   = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
			$last_date =  $this->ci->general_model->datetimeconvqbo($last_date, $from, $to);
			$last_date1= date('Y-m-d H:i',strtotime('-9 hour',strtotime($last_date)));
			
			$latedata= date('Y-m-d',strtotime($last_date1)).'T'.date('H:i:s',strtotime($last_date1));   
		
			$entities = $dataService->Query("SELECT * FROM Item where Metadata.LastUpdatedTime > '".$latedata."' ");
		
		}else{
			$entities = $dataService->Query("SELECT * FROM Item ");
		}

		// echo 'die';
		// echo '<pre>';
		// print_r($entities);
		// print_r($error);die;
		$error = $dataService->getLastError();
		
	   
		if(!empty($qbo_data))
		{  
			$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
			$updatedata['updatedAt'] =date('Y-m-d H:i:s');
			if(!empty($entities))
			$this->ci->general_model->update_row_data('tbl_qbo_config',array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'ItemQuery'),$updatedata);
		}
		else{
			$updateda= date('Y-m-d') . 'T' . date('H:i:s');
			$upteda = array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'ItemQuery','lastUpdated'=>$updateda, 'createdAt'=>date('Y-m-d H:i:s'), 'updatedAt'=>date('Y-m-d H:i:s'));
			if(!empty($entities))
			$this->ci->general_model->insert_row('tbl_qbo_config', $upteda);
		} 
	  	//  $entities = $dataService->Query("SELECT * FROM Item ");
		
		if(!empty($entities)){
		
			foreach ($entities as $oneInvoice) {
				  
				 $QBO_item_details['productID'] = $oneInvoice->Id;
				 $QBO_item_details['IsActive'] = $oneInvoice->Active;
				 $QBO_item_details['TimeCreated'] = date('Y-m-d H:i:s',strtotime($oneInvoice->MetaData->CreateTime));
				 $QBO_item_details['TimeModified'] = date('Y-m-d H:i:s',strtotime($oneInvoice->MetaData->LastUpdatedTime));
				 $QBO_item_details['Name'] = $oneInvoice->Name;
				 $QBO_item_details['SKU'] = $oneInvoice->Sku;
				 $QBO_item_details['SalesDescription'] = $oneInvoice->Description;
				 $QBO_item_details['saleCost'] = $oneInvoice->UnitPrice;
				 $QBO_item_details['Type'] = $oneInvoice->Type;
				 $QBO_item_details['parent_ListID'] = $oneInvoice->ParentRef;
				 $QBO_item_details['QuantityOnHand'] = $oneInvoice->QtyOnHand;
				 $QBO_item_details['InvStartDate'] = $oneInvoice->InvStartDate;
				 $QBO_item_details['AssetAccountRef'] = $oneInvoice->AssetAccountRef;
				 $QBO_item_details['purchaseCost'] = $oneInvoice->PurchaseCost;
				 $QBO_item_details['purchaseDesc'] = $oneInvoice->PurchaseDesc;
				 $QBO_item_details['IncomeAccountRef'] = $oneInvoice->IncomeAccountRef;
				 $QBO_item_details['ExpenseAccountRef'] = $oneInvoice->ExpenseAccountRef;
				 $QBO_item_details['CompanyID'] =$realmID;
				 $QBO_item_details['merchantID'] = $merchID;
			 if($this->ci->general_model->get_num_rows('QBO_test_item',array('CompanyID'=>$realmID,'productID'=>$oneInvoice->Id , 'merchantID'=>$merchID)) > 0){  
			   
			   
				 $this->ci->general_model->update_row_data('QBO_test_item',array('CompanyID'=>$realmID, 'productID'=>$oneInvoice->Id, 'merchantID'=>$merchID), $QBO_item_details);
			   
			   }else{
			   
				$this->ci->general_model->insert_row('QBO_test_item', $QBO_item_details);
			 
			 
			   }
				
			    // $this->ci->general_model->insert_row('QBO_test_item', $QBO_item_details);
            }
            
            $qbo_log = array('syncType' => 'QC','type' => 'productImport','qbStatus' => 1, 'qbAction' => 'ProductDataImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $this->merchantID);
            $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
            if($syncid){
                $qbSyncID = 'QC-'.$syncid;
                $qbSyncStatus = 'Product Data Imported Init';
                $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
            }
		}
    }
    
    public function check_qbo_invoice($invoice,$isRedirect = true){
        $realmID     = $this->realmID;
        $dataService = DataService::Configure($this->qbo_config);
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $entities = array();

        $merchID = $this->merchantID;
        $invoiceID = $invoice['invoiceID'];

        $val = array(
            'merchantID' => $merchID,
        );

        $redirect = 'QBO_controllers/Create_invoice/Invoice_details';
        if(isset($_SERVER['HTTP_REFERER'])) {
            $redirect = $_SERVER['HTTP_REFERER'];
        }

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $qbo_data = $this->ci->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'ValidateInvoice'));

        
        $err = false;
        $success = true;
        $deleteInvoice = [];
        
        $entities = $dataService->Query("SELECT * FROM Invoice where Id='$invoiceID' ");
        $error = $dataService->getLastError();
		if ($error != null) {
            $success = false;
			$err .= $error->getHttpStatusCode() . "\n";
			$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err .= "The Response message is: " . $error->getResponseBody() . "\n";
        }

        if(empty($entities)){
            $err = 'Invoice not found at QBO';
            $success = false; 
            $deleteInvoice = true;
        }

        if(!$success) {
            if($deleteInvoice){
                $this->ci->general_model->update_row_data('QBO_test_invoice', ['invoiceID' => $invoiceID], ['isDeleted' => 1]);    
            }

            if($isRedirect){
                $this->ci->session->set_flashdata('message', "<div class='alert alert-danger'>  <strong>Error: $err </strong></div>");
                redirect($redirect, 'refresh');
            }
        }

        if($success) {
            $this->get_invoice_data(['invoiceID' => $invoiceID]);
        }
        
        return $deleteInvoice;
        // return [
        //     'error' => $err,
        //     'success' => $entities
        // ];
    }

    public function check_qbo_invoice_mul($invoiceList,$isRedirect = true){
        $realmID     = $this->realmID;
        $dataService = DataService::Configure($this->qbo_config);
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        
        $ids = '';
        $inv = array_column($invoiceList,'invoiceID');

        foreach($inv as $kk => $id) {
            $ids .= "'$id',";
        }

        $ids = substr($ids, 0, -1);

        $sql = "SELECT Id FROM Invoice WHERE Id IN ($ids)";
        $deletedEntities = $dataService->Query($sql);
        $newId = [];

        if($deletedEntities && !empty($deletedEntities))
            $newId = array_column($deletedEntities,'Id');
        
        $deleteId = [];
        foreach($inv as $kk => $id) {
            if(!in_array($id, $newId)){
                $deleteId[] = $id;
                unset($invoiceList[$kk]);
            }
        }

        if(!empty($deleteId)){
            $this->ci->general_model->update_mul_row_data('QBO_test_invoice', array('invoiceID' => $deleteId), ['isDeleted' => 1]);
        }

        return $invoiceList;
    }

    public function syncAll($args = []){
        $this->get_customer_data($args);
        $this->get_items_data();
        $this->get_invoice_data($args);
        $this->get_payment_method();
    }
    /* CZ to QBO sync by log file*/
    public function sync_invoice_data($logData = [])
    {

        $realmID = $this->realmID;
        $merchID = $logData['merchantID'];
        $invoiceID  = $logData['invoiceID'];
        $customer_ref =  $logData['CustomerListID'];
        $address1 = $logData['BillAddress_Addr1'];
        $address2 = $logData['BillAddress_Addr2'];
        $country = $logData['BillAddress_Country'];
        $state = $logData['BillAddress_State'];
        $city = $logData['BillAddress_City'];
        $zipcode = $logData['BillAddress_PostalCode'];
        $phone = '';
        $baddress1 = $logData['BillAddress_Addr1'];
        $baddress2 = $logData['BillAddress_Addr2'];
        $bcountry = $logData['BillAddress_Country'];
        $bstate = $logData['BillAddress_State'];
        $bcity = $logData['BillAddress_City'];
        $bzipcode = $logData['BillAddress_PostalCode'];
        $bphone = '';
        $taxval = $logData['taxRate'];
        $duedate = $logData['DueDate'];
        $lineArray = [];

        $dataService = DataService::Configure($this->qbo_config);
        $error       = $dataService->getLastError();
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $inv_data = $this->ci->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
        if (!empty($inv_data)) {
            $inv_pre   = $inv_data['prefix'];
            $inv_po    = $inv_data['postfix'] + 1;
            $new_inv_no = $inv_pre . $inv_po;
            $Number = $new_inv_no;
        } else {
            $this->ci->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

            redirect('/QBO_controllers/Create_invoice/add_invoice', 'refresh');
        }

        $item_val = $this->ci->qbo_invoices_model->get_item_data_new(array('invoiceID' => $invoiceID, 'merchantID' => $merchID, 'releamID' => $realmID));
        
        $inv_amnt = 0;
        for ($i = 0; $i < count($item_val); $i++) {
            $amount = $item_val[$i]['totalAmount'] * $item_val[$i]['itemQty'];

            $inv_amnt += $amount;


            $LineObj = Line::create([
                "Amount" => $item_val[$i]['totalAmount'] * $item_val[$i]['itemQty'],
                "DetailType" => "SalesItemLineDetail",
                "Description" => $item_val[$i]['itemDescription'],
                "SalesItemLineDetail" => [
                    "ItemRef" => $item_val[$i]['itemLineID'],
                    "Qty" => $item_val[$i]['itemQty'],
                    "UnitPrice" => $item_val[$i]['totalAmount'],
                    "TaxCodeRef" => [
                        "value" => (isset($item_val[$i]['itemTax']) && $item_val[$i]['itemTax']) ? "TAX" : "NON",
                    // "value" => "TAX"
                    ]

                ],

            ]);
            $lineArray[] = $LineObj;
        }

        $theResourceObj = Invoice::create([
                "CustomerRef" => $customer_ref,
                "DocNumber" => $Number,
                "BillAddr" => [
                    "City" =>  $bcity,
                    "Line1" =>  $baddress1,
                    "Line2" =>  $baddress2,
                    "CountrySubDivisionCode" =>  $bstate,
                    "Country" =>  $bcountry,
                    "PostalCode" =>  $bzipcode
                ],
                "ShipAddr" => [
                    "Line1" =>  $address1,
                    "Line2" =>  $address2,
                    "City" =>  $city,
                    "Country" =>  $country,
                    "PostalCode" =>  $zipcode
                ],




                "TxnTaxDetail" => [
                    "TxnTaxCodeRef" => [
                        "value" => $taxval
                    ],
                ],

                "TotalAmt" => $inv_amnt,

                "Line" => $lineArray,

                "DueDate" =>  $duedate,

            ]);
        


        $resultingObj = $dataService->Add($theResourceObj);

        $error = $dataService->getLastError();
        $response = [];
        $error1 = '';
        if ($error != null) {

            $error1 .=  $error->getResponseBody() . "\n";

            $response['status'] ='0';
            $response['message'] = $error1;
            
            
        } else {
            $invID =  $resultingObj->Id;
            $QBO_invoice_details['invoiceID'] = $invID;
            $this->ci->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);

            $response['status'] ='1';
            $response['message'] = 'success';
            
        }
        return $response;
       
    }
    public function sync_transaction_data($logData,$txnData)
    {
        $response = [];
        $realmID = $this->realmID;
        $merchID = $logData['merchantID'];
        $invoiceID  = $logData['invoiceID'];
        $customerID =  $logData['CustomerListID'];
        
        $amount = $txnData['transactionAmount'];

        $dataService = DataService::Configure($this->qbo_config);
        $error       = $dataService->getLastError();
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $newPaymentObj = Payment::create([
            "TotalAmt"    => $amount,
            "SyncToken"   => 1, // $updatedResult->SyncToken,
            "CustomerRef" => $customerID,
            "Line"        => [
                "LinkedTxn" => [
                    "TxnId"   => $invoiceID,
                    "TxnType" => "Invoice",
                ],
                "Amount"    => $amount,
            ],
        ]);
        //print_r($newPaymentObj);die;
        $savedPayment = $dataService->Add($newPaymentObj);
        $error = $dataService->getLastError();

        if ($error != null) {
            $er = '';
            $er .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
            $er .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            $er .= "The Response message is: " . $error->getResponseBody() . "\n";
            
            $msg    = "Payment Success but " . $error->getResponseBody();
            $response['status'] ='0';
            $response['message'] = $msg;
        } else {
            
            $response['status'] ='1';
            $response['message'] = 'success';

          
        }
        
        return $response;
       
    }
    public function sync_customer_data($custData)
    {
        $response = [];
        $realmID = $this->realmID;
       
        $dataService = DataService::Configure($this->qbo_config);
        $error       = $dataService->getLastError();
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $lsID =mt_rand(7000000,8000000);          
        $customerObj = Customer::create([
             
             "GivenName"=>  $custData['firstName'],
             "FamilyName"=>  $custData['lastName'],
             "FullyQualifiedName"=>  $custData['fullName'],
             "DisplayName"=>  $custData['fullName'],
             "CompanyName"=>  $custData['companyName'],
             "PrimaryPhone"=>  [
                 "FreeFormNumber"=>  $custData['phoneNumber'],
                 "CountryCode"=> $custData['zipCode']
             ],
             "PrimaryEmailAddr"=>  [
                 "Address" => $custData['userEmail']
             ],
             "BillAddr"=>  [
                 "Line1" => $custData['address1'],
                 "Line2" => $custData['address2'],
                 "PostalCode" => $custData['zipCode'],
                 "Country" => $custData['Country'],
                 "CountrySubDivisionCode" => $custData['State'],
                 "City" => $custData['City']
             ],
            "ShipAddr"=>  [
                 "Line1" => $custData['ship_address1'],
                 "Line2" => $custData['ship_address2'],
                 "PostalCode" => $custData['ship_zipcode'],
                 "Country" => $custData['ship_country'],
                 "CountrySubDivisionCode" => $custData['ship_state'],
                 "City" => $custData['ship_city']
             ]
            ]);
            
            
        
        
       $resultingCustomerObj = $dataService->Add($customerObj);
      
               
        $error = $dataService->getLastError();


        if ($error != null) 
        {
            $error1='';
       
            $error1.=  $error->getResponseBody() . "\n";
            $response['status'] ='0';
            $response['message'] = $error1; 

        }else{ 
             $st ='1';
             $action = 'Add Customer'; 
             $error1='success';
             $lsID= $resultingCustomerObj->Id;

             $this->ci->general_model->update_row_data('QBO_custom_customer', array('customerID' => $custData['customerID']),array('Customer_ListID' => $lsID));

            $response['status'] ='1';
            $response['message'] = 'success';
        }

        return $response;
       
    }
    public function sync_product_data($productData)
    {
        $response = [];

        $realmID = $this->realmID;
        $productName = $productData['Name'];
        $sku = $productData['SKU'];
        $type = $productData['Type'];
        $productParent = $productData['parent_ListID'];
        $incomeAccount = $productData['IncomeAccountRef'];
        $expenseAccount = $productData['ExpenseAccountRef'];
        $purProDescription = $productData['purchaseDesc'];
        $saleProDescription = $productData['SalesDescription'];
        $purchasePrice = $productData['purchaseCost'];
        $salePrice = $productData['saleCost'];

       
        $dataService = DataService::Configure($this->qbo_config);
        $error       = $dataService->getLastError();
        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $qbaction = 'Add Item';
        $dateTime = new \DateTime('NOW');
        $Item     = Item::create([
            "Name"              => $productName,
            "Sku"               => $sku,
            "Type"              => $type,
            "ParentRef"         => $productParent,

            "SubItem"           => true,
            "Active"            => true,
            "IncomeAccountRef"  => [
                "value" => $incomeAccount,
            ],
            "ExpenseAccountRef" => [
                "value" => $expenseAccount,
            ],
            "PurchaseDesc"      => $purProDescription,
            "Description"       => $saleProDescription,
            "PurchaseCost"      => $purchasePrice,
            "UnitPrice"         => $salePrice,
            "InvStartDate"      => $dateTime,
        ]);

        if ($resultingObj = $dataService->Add($Item)) {
            $prodid = $resultingObj->Id;

            $this->ci->general_model->update_row_data('QBO_test_item', array('ListID' => $productData['ListID']),array('productID' => $prodid));
            $response['status'] ='1';
            $response['message'] = 'success';
        }

        if ($dataService->getLastError() != null) {
            $response['status'] ='0';
            $response['message'] = 'Failed'; 
        }

        return $response;
       
    }

    public function get_payment_method()
    {
        $realmID = $this->realmID;
        $merchID = $this->merchantID;

        $dataService = DataService::Configure($this->qbo_config);

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        /***********************Gettting Agency Data*****************/
        $paymentMethod = $dataService->Query("SELECT * FROM PaymentMethod");
        $err_msg = '';
        $error   = $dataService->getLastError();
        if ($error != null) {
            $err_msg .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
            $err_msg .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            $err_msg .= "The Response message is: " . $error->getResponseBody() . "\n";

        } else {
            if (!empty($paymentMethod)) {
                foreach ($paymentMethod as $singleMethod) {
                    $methodItem = [
                        'payment_id' => $singleMethod->Id,
                        'payment_name' => $singleMethod->Name,
                        'payment_type' => $singleMethod->Type,
                        'is_active' => $singleMethod->Active,
                        'merchantID' => $merchID,
                        'created_at' => $singleMethod->MetaData->CreateTime,
                        'updated_at' => $singleMethod->MetaData->LastUpdatedTime,
                    ];

                    $ch_num = $this->ci->general_model->get_num_rows('QBO_payment_method', array('payment_id' => $singleMethod->Id, 'merchantID' => $merchID));
                    if ($ch_num == 0) {
                        $this->ci->general_model->insert_row('QBO_payment_method', $methodItem);
                    } else {
                        $this->ci->general_model->update_row_data('QBO_payment_method', array('payment_id' => $singleMethod->Id, 'merchantID' => $merchID), $methodItem);
                    }
                }

                $qbo_log = array('syncType' => 'QC','type' => 'paymentMethodImport','qbStatus' => 1, 'qbAction' => 'paymentMethodImportInit', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
                $syncid = $this->ci->general_model->insert_row('tbl_qbo_log', $qbo_log);
                if($syncid){
                    $qbSyncID = 'QC-'.$syncid;
                    $qbSyncStatus = 'Payment Method Imported Init';
                    $this->ci->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
                }
            }
        }
    }
    /**
     * Create Default Sync Records for Surcharge
     *
     * @param int $merchantID
     *
     * @return
     */
    public function createDefaultSyncRecords(int $merchantID)
    {
        $con = array('merchantID'=> $merchantID);  
        $token_data    = $this->ci->general_model->get_row_data('QBO_token', $con);

        $accessToken  = $token_data['accessToken'];
        $refreshToken = $token_data['refreshToken'];
        $realmID      = $token_data['realmID'];

        $url = $this->ci->config->item('QBOURL')."v3/company/".$realmID."/account";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{
              "Name": "Surcharge Fees", 
              "AccountType": "Income"
            }
        ');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$accessToken
        ));

        $sales_response = curl_exec($ch);
        curl_close($ch);

    
        $this->get_qbo_accounts_data($merchantID);

        $saleAccountData = $this->ci->general_model->get_row_data('QBO_accounts_list', ['merchantID' => $merchantID, 'accountName' => 'Surcharge Fees']);


        $salesItemData = [
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken,
            'realmID' => $realmID,
            'productName' => 'Surcharge Fees',
            'accountID' => $saleAccountData['accountID'],
            'merchantID' => $merchantID,
        ];
        $this->addSyncSettingsQBOItems($salesItemData);

        $this->getQboData();

        $sales_item_data = $this->ci->general_model->get_select_data('QBO_test_item', ['productID'], ['merchantID' => $merchantID, 'Name' => 'Surcharge Fees']);

        $cz_sales_account = $this->ci->general_model->get_select_data('QBO_accounts_list', ['accountID'], ['merchantID' => $merchantID, 'accountName' => 'Surcharge Fees']);
        $cz_receivable_account = $this->ci->general_model->get_select_data('QBO_accounts_list', ['accountID'], ['merchantID' => $merchantID, 'accountType' => 'Surcharge Income']);
        

        $insert_data = [];
        $insert_data['merchantID'] = $merchantID;
        

        $insert_data['defaultItem'] = isset($sales_item_data['productID']) ? $sales_item_data['productID'] : '';

        $insert_data['defaultItemAccount'] = isset($cz_sales_account['accountID']) ? $cz_sales_account['accountID'] : '';
    
        $already_exist = $this->ci->general_model->get_select_data('tbl_merchant_surcharge', ['id'], ['merchantID' => $merchantID]);
        if($already_exist){
            $this->ci->general_model->update_row_data('tbl_merchant_surcharge', array('merchantID' => $merchantID), $insert_data);
        }else{
            $insert_data['createdAt'] = date('Y-m-d H:i:s');
            $this->ci->general_model->insert_row('tbl_merchant_surcharge', $insert_data);
        }
    }

    /**
     * Get accounts from QBO
     *
     * @param int $merchantID
     *
     * @return
     */
    public function get_qbo_accounts_data(int $merchantID)
    {
        $this->get_account_data();
    }

    /**
     * Create sync QBO Items
     *
     * @param array $data
     *
     * @return
     */
    public function addSyncSettingsQBOItems(array $data)
    {
        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $productName = $data['productName'];
        $accountID = $data['accountID'];
        $merchantID = $data['merchantID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->ci->config->item('AuthMode'),
            'ClientID'        => $this->ci->config->item('client_id'),
            'ClientSecret'    => $this->ci->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->ci->config->item('QBOURL'),

        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $qbaction = 'Add Item';
        $dateTime = new \DateTime('NOW');
        $Item     = Item::create([
            "Name"              => $productName,
            "Type"              => 'Service',
            "ParentRef"         => '',
            "SubItem"           => true,
            "Active"            => true,
            "IncomeAccountRef"  => [
                "value" => $accountID,
            ],
            "ExpenseAccountRef" => [
                "value" => $accountID,
            ],
            "PurchaseDesc"      => $productName,
            "Description"       => $productName,
            "PurchaseCost"      => 0,
            "UnitPrice"         => 0,
            "InvStartDate"      => $dateTime,
        ]);

        if ($resultingObj = $dataService->Add($Item)) {
            $prodid = $resultingObj->Id;
        }
        if ($dataService->getLastError() != null) {
            $prodid                       = mt_rand(200000, 600000);
            $item_details['productID']    = $prodid;
            $item_details['TimeCreated']  = date('Y-m-d H:i:s');
            $item_details['TimeModified'] = date('Y-m-d H:i:s');
            $item_details['IsActive']     = 'false';
            $item_details['merchantID']     = $merchantID;

            $this->ci->general_model->insert_row('QBO_test_item', $item_details);
        }
    }
    /**
     * Description: Fetch all QBO sync data from QBO to CZ 
    */ 
    public function getQboData()
    {
             
        $this->get_tax_data();
        $this->get_account_data();
        $this->get_customer_data();
        $this->get_items_data();
        $this->get_credit_data();
        $this->get_invoice_data();
        $this->get_payment_method();
             
    }
}
