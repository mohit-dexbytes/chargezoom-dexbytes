<?php 

class Api_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	
		 $this->db = $this->load->database('devdb',TRUE);
	}
	
	function insert_row($table_name, $array)
	{

		if($this->db->insert($table_name,$array)){
         
		return $this->db->insert_id();
		}else{ 
		return false;
		}
	}
	
	function update_row($table_name,$reg_no,$array)
	{
		$this->db->where('reg_no', $reg_no);
		if($this->db->update($table_name, $array))
			return true;
		else
			return false;
	}
	
	public function check_existing_user($tbl, $array)
    {
        $this->db->select('*');
        $this->db->from($tbl);
		if($array !='')
        $this->db->where($array);
		
        $query = $this->db->get();
		if($query->num_rows() > 0)
			return true;
        
        else 
          return false; 
	}
	
	
	function update_row_data($table_name,$condition, $array)
	{
	
		$this->db->where($condition);
		$trt = $this->db->update($table_name, $array); 
		if($trt)
			return true;
		else
			return false;
	}
	
	   public function get_row_data($table, $con) {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	  public function get_table_data($table, $con) {
	    $data=array();

			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }
	  
	
	
	
}
	
	  