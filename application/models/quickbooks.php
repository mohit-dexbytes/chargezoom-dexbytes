<?php

class Quickbooks extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set the DSN connection string for the queue class
     */
    public function dsn($dsn)
    {
        $this->_dsn = $dsn;
    }

    /**
     * Queue up a request for the Web Connector to process
     */
    public function enqueue($action, $ident, $priority = 0, $extra = null, $user = null)
    {
        $Queue = new QuickBooks_WebConnector_Queue($this->_dsn);

        return $Queue->enqueue($action, $ident, $priority, $extra, $user);
    }

    public function testcustomer($action)
    {

        $Customer = new QuickBooks_QBXML_Object_Customer($this->_dsn);
        $Customer->setFullName('web:Keith Palmer');
        print('FullName: ' . $Customer->getFullName() . "\n");
        print('Name: ' . $Customer->getName() . "\n");
        print('Parent: ' . $Customer->getParentName() . "\n");
        print("\n");
        print($Customer->asQBXML($action));

    }

    public function get_invoice_data()
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select('inv.*, cust.*, nmi.*,cvalt.*,cmp.merchantID, cmp.qbwc_username ');

        $this->db->from('qb_test_invoice inv');
        $this->db->join('qb_test_customer cust', 'inv.Customer_ListID = cust.ListID', 'INNER');
        $this->db->join('tbl_cutomer_vault cvalt', 'cust.ListID = cvalt.customerListID', 'INNER');
        $this->db->join('tbl_company cmp', 'cmp.id = cust.companyID', 'inner');
        $this->db->join('tbl_nmi_data nmi', 'cmp.merchantID = nmi.merchantID', 'INNER');
        $this->db->where("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') <=", $today);
        $this->db->where("inv.BalanceRemaining != ", '0.00');
        $this->db->where('inv.userStatus', 'Active');
        $this->db->where('cust.customerStatus', '1');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }

        return $res;
    }

    public function get_invoice_data_auto_pay()
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select('inv.*,tci.invoiceRefNumber, tci.cardID, tmg.gatewayType, cust.companyID, tmg.gatewayID, tmg.gatewayType,tmg.gatewayUsername, tmg.gatewaySignature,tmg.gatewayPassword,tmg.extra_field_1,  cmp.merchantID, cmp.qbwc_username, mr.resellerID, mr.companyName as mrCompanyName,tci.paymentMethod');
        $this->db->from('qb_test_invoice inv');
        $this->db->join('tbl_custom_invoice tci', 'inv.RefNumber =tci.RefNumber AND tci.merchantID = inv.qb_inv_merchantID and tci.Customer_ListID = inv.Customer_ListID', 'INNER');
        $this->db->join('qb_test_customer cust', 'inv.Customer_ListID = cust.ListID AND inv.qb_inv_merchantID = cust.qbmerchantID', 'INNER');
        $this->db->join('tbl_company cmp', 'cmp.id = cust.companyID', 'inner');
        $this->db->join('tbl_merchant_gateway tmg', 'tci.gatewayID = tmg.gatewayID', 'INNER');
        $this->db->join('tbl_merchant_data mr', 'mr.merchID=cmp.merchantID', 'inner');

        $this->db->where("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') =", $today);
        $this->db->where('inv.IsPaid', 'false');
        $this->db->where("inv.BalanceRemaining != ", '0.00');
        $this->db->where('inv.userStatus', 'Active');
        $this->db->where('cust.customerStatus', '1');
        $this->db->where('tci.autoPayment', '1');
        $this->db->where('mr.isSuspend', '0');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('mr.isEnable', '1');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }

        return $res;
    }

    public function get_invoice_data_qbo_auto_pay()
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select('inv.BalanceRemaining, inv.Total_payment, inv.invoiceID, inv.DueDate,cust.Customer_ListID,inv.IsPaid,
	   tci.cardID, tmg.gatewayType,tmg.gatewayType,tmg.gatewayUsername,tmg.gatewayPassword,tmg.gatewaySignature,tci.scheduleDate as schedule, tci.scheduleAmount,  cust.merchantID, mr.resellerID');
        $this->db->from('QBO_test_invoice inv');
        $this->db->join('tbl_scheduled_invoice_payment tci', 'inv.invoiceID =tci.invoiceID', 'LEFT');
        $this->db->join('QBO_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'tci.gatewayID = tmg.gatewayID', 'INNER');
        $this->db->join('tbl_merchant_data mr', 'mr.merchID=cust.merchantID', 'inner');
        $this->db->where("(DATE_FORMAT(tci.scheduleDate,'%Y-%m-%d')='$today' or  (DATE_FORMAT(inv.DueDate,'%Y-%m-%d') ='$today' and tci.scheduleDate IS NULL))   ");
        $this->db->where('inv.IsPaid', '0');
        $this->db->where("inv.BalanceRemaining != ", '0.00');
        $this->db->where("inv.UserStatus != ", '1');

        $this->db->where('cust.customerStatus', 'true');
        $this->db->where('cust.merchantID = inv.merchantID ');


        $query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $res = $query->result_array();
		}
		
		return $res;
	} 		
	
	
		
	public function get_credit_by_customer($customerID)
	{
        $r_amount = 0; 		
    	$this->db->select('sum(creditAmount) as credit_amount');
		$this->db->from('tbl_credits');
		$this->db->where('creditStatus','0');
		$this->db->where('creditName', $customerID);
	    	$query = $this->db->get();
	   if($query->num_rows() > 0)
	   {
	       
		  $r_amount1 = $query->row_array();
		  if($r_amount1['credit_amount'])
		  $r_amount =$r_amount1['credit_amount'];
	
	   }
		return  $r_amount;
	}
	
	
	
	public function get_invoice_data_pay($invoiceID)
	{
        $res = array();
        
        $merchantID = false;
        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }
	
		$this->db->select('inv.*, cust.*, cmp.merchantID, cmp.qbwc_username');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	
	    $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
      
        $this->db->where('inv.TxnID' , $invoiceID);
        
        if($merchantID){   
            $this->db->where('cmp.merchantID' , $merchantID);
        }
	    $this->db->where('cust.customerStatus', '1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
		    return $res = $query->row_array();
		}
		
		return $res;
	} 
	
	
	
	
	
	
	
	
	public function get_invoice_mail_data($type)
	{
		$res = array();
	
		$this->db->select('inv.*, cust.*,l.overDue');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	    $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
		$this->db->join('tbl_merchant_data mer' , 'cmp.merchantID = mer.merchID', 'inner');
        $this->db->join('tbl_customer_login l','cust.ListID = l.customerID','LEFT');
        $this->db->where("mer.isEnable", 1);
        $this->db->where("mer.isDelete", 0);
        $this->db->where("mer.isSuspend", 0);
	    $this->db->where('cust.customerStatus', '1');
		$this->db->where ('IsPaid', 'false');
		  $this->db->where('inv.userStatus' , 'Active');
		if($type=='0')
		 $this->db->where ("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < ", date('Y-m-d'));
        if($type=='1')	
	    $this->db->where ("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') > ", date('Y-m-d'));
		$query = $this->db->get();
		
		
		if($query->num_rows() > 0){
		
		  return $res = $query->result_array();
		}
		
		return $res;
		
		
	}
	
	public function get_qbo_invoice_data_pay($invoiceID, $merchantID)
	{
		$res = array();
	
		$this->db->select('inv.*,cust.*');
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('QBO_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where('inv.invoiceID' , $invoiceID);
         $this->db->where('inv.merchantID' , $merchantID);
	    $this->db->where('cust.merchantID' , $merchantID);
		$query = $this->db->get();

		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	}

    public function updateMsg($queue_id, $statusCode=0, $statusMessage=null, $statusSeverity=null){

        $msg = $statusSeverity ? $statusSeverity.": ".$statusMessage : $statusMessage;

        $this->db->where('quickbooks_queue_id', $queue_id);
		if($this->db->update('quickbooks_queue', ['qbdStatusCode' => $statusCode, 'qbdStatusMessage' => $msg ]))
			return true;
		else
			return false;
    }
	
}
