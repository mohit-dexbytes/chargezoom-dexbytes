<?php
class Common_model extends CI_Model {

   	public function __construct()
   	{
      parent::__construct();
   	}

   	public function get_all($db_fields,$table,$mid,$order)
	{
		$this->db->select($db_fields);
		$this->db->from($table);
		$this->db->order_by($mid,$order);
		$query = $this->db->get();
		return $query->result();	
	}
	
	public function get_single($fields,$table)
	{
		$this->db->select($fields);
		$this->db->from($table);
		$query = $this->db->get();
		return $query->row_array();
	}
	
	public function get_single_content($fields,$table,$db_field,$id)
	{
		$this->db->select($fields);
		$this->db->from($table);
		$this->db->where($db_field,$id);
		$query = $this->db->get();
		return $query->row_array();
	}
	
	public function get_contents($fields,$table,$field_id,$id,$mid,$order)
	{
		$this->db->select($fields);
		$this->db->from($table);
		$this->db->order_by($mid,$order);
		$this->db->where($field_id,$id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function insert_content($table,$contents)
	{
		if($this->db->insert($table, $contents)) {
			echo 1;	
		} else {
			echo 0;
		}
	}
	
	public function delete_contents($table,$field_id,$id)
	{
		if($this->db->delete($table, array($field_id => $id )))
		{
			echo 1;
		} else {
			echo 0;	
		}
	}
	
	public function edit($table,$data,$id_field_table,$id,$sucess_message,$error_message)
	{
		$this->db->where($id_field_table, $id);
		if($this->db->update($table, $data)) {
			echo $sucess_message;
		} else {
			echo $error_message;	
		}
	}
	
	public function check_number($table,$id)
	{
		$query = $this->db->query("select id from ".$table." where id =".$id."");
		return $query->num_rows();
	}
	
	public function count_table($table)
	{
		$query = $this->db->query("select id from ".$table."");
		return $query->num_rows();	
	}
	
	public function count_rows($field,$table,$where,$id)
	{
		$query = $this->db->query("select ".$field." from ".$table." where ".$where."= '".$id."'");
		return $query->num_rows();
	}
	
	public function get_contents_withp($table,$field,$segment,$per_pg,$offset)
	{
		$this->db->order_by('id','desc');
		$this->db->where($field,$segment);
		$query=$this->db->get($table,$per_pg,$offset);
		return $query->result();
	}
	
	public function get_table_with_pagination($table,$fields,$limit, $start)
	{
		$query = $this->db->query("select ".$fields." from ". $table."  order by id desc LIMIT $start, $limit");
		return $query->result();
	}
	
	public function get_table_with_pagination_user($table,$where,$id,$limit, $start)
	{
		$query = $this->db->query("select * from ". $table." where ".$where."=".$id." order by id desc LIMIT $start, $limit");
		return $query->result();
	}
	
	public function single_result_query($query)
	{
		$query = $this->db->query($query);
		return $query->row_array();
	}
	
	public function custom_query($query)
	{
		$query = $this->db->query($query);
		return $query->result();
	}
	
	public function custom_count($query)
	{
		$query = $this->db->query($query);
		return $query->num_rows();	
	}
	
	public function get_terms_data($merchID){
    	$data=array();
	    $query = $this->db->query("SELECT *, dpt.id as dfid, pt.id as pid FROM `tbl_default_payment_terms` dpt
		LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID
		UNION SELECT *, dpt.id as dfid, pt.id as pid  FROM `tbl_default_payment_terms` dpt 
			RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID 
		WHERE pt.merchantID = $merchID ORDER BY dfid IS NULL, dfid asc");
	   	if($query->num_rows()>0 ) {
			$data = $query->result_array();
			return $data;
		} else {
			return $data;
		}
	}
	
	public function check_payment_term($name,$netterm,$merchID)
	{
		$query = $this->db->query("SELECT * FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID WHERE dpt.name IN ('".$name."') AND pt.pt_netTerm IS NULL OR dpt.netTerm IN ('".$netterm."') AND pt.pt_netTerm IS NULL OR pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 UNION SELECT * FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 AND pt.merchantID = $merchID AND pt.enable != 1");
		return $query->num_rows();	
	}

	public function getPrintTransactionReceiptData($userID, $transactionID, $type)
	{
	    $this->db->select('tr.*, sum(tr.transactionAmount) as transactionAmount, tr.invoiceTxnID');
	    $this->db->from('customer_transaction tr');
	    
	    if($type == 1){
	    	$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
	    	$this->db->select('cust.firstName as FirstName, cust.lastName as LastName, cust.fullName as FullName, cust.userEmail as Email, cust.phoneNumber as Phone, cust.address1 as BillingAddress_Addr1, cust.address2 as BillingAddress_Addr2, cust.Country as BillingAddress_Country, cust.State as BillingAddress_State, cust.City as BillingAddress_City, cust.zipCode as BillingAddress_PostalCode, cust.ship_address1 as ShipAddress_Addr1, cust.ship_address2 as ShipAddress_Addr2, cust.ship_country as ShipAddress_Country, cust.ship_state as ShipAddress_State, cust.ship_city as ShipAddress_City, cust.ship_zipcode as ShipAddress_PostalCode, cust.companyName as CompanyName, cust.companyID as CompanyID, cust.profile_picture as Profile_Picture, customerStatus as CustomerStatus');
	    	$this->db->join('QBO_custom_customer cust', 'tr.customerListID = cust.Customer_ListID AND tr.merchantID = cust.merchantID', 'INNER');
	    }else if($type == 2){
	    	$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
	    	$this->db->select('cust.FirstName as FirstName, cust.LastName as LastName, cust.FullName as FullName, cust.Contact as Email, cust.Phone as Phone, cust.BillingAddress_Addr1 as BillingAddress_Addr1, cust.BillingAddress_Addr2 as BillingAddress_Addr2, cust.BillingAddress_Country as BillingAddress_Country, cust.BillingAddress_State as BillingAddress_State, cust.BillingAddress_City as BillingAddress_City, cust.BillingAddress_PostalCode as BillingAddress_PostalCode, cust.ShipAddress_Addr1 as ShipAddress_Addr1, cust.ShipAddress_Addr2 as ShipAddress_Addr2, cust.ShipAddress_Country as ShipAddress_Country, cust.ShipAddress_State as ShipAddress_State, cust.ShipAddress_City as ShipAddress_City, cust.ShipAddress_PostalCode as ShipAddress_PostalCode, cust.companyName as CompanyName, cust.companyID as CompanyID, cust.profile_picture as Profile_Picture, customerStatus as CustomerStatus');
	    	$this->db->join('qb_test_customer cust', 'tr.customerListID = cust.ListID AND tr.merchantID = cust.qbmerchantID', 'INNER');
	    }else if($type == 3){
	    	$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
	    	$this->db->select('cust.firstName as FirstName,cust.lastName as LastName,cust.fullName as FullName,cust.userEmail as Email,cust.phoneNumber as Phone,cust.address1 as BillingAddress_Addr1,cust.address2 as BillingAddress_Addr2,cust.Country as BillingAddress_Country,cust.State as BillingAddress_State,cust.City as BillingAddress_City,cust.zipCode as BillingAddress_PostalCode,cust.ship_address1 as ShipAddress_Addr1,cust.ship_address2 as ShipAddress_Addr2,cust.ship_country as ShipAddress_Country,cust.ship_state as ShipAddress_State,cust.ship_city as ShipAddress_City,cust.ship_zipcode as ShipAddress_PostalCode,cust.companyName as CompanyName,cust.companyID as CompanyID,cust.profile_picture as Profile_Picture,customerStatus as CustomerStatus');
	    	$this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.Customer_ListID AND tr.merchantID = cust.merchantID', 'INNER');
	    }else if($type == 4){
	    	$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
	    	$this->db->select('cust.firstName as FirstName,cust.lastName as LastName,cust.fullName as FullName,cust.userEmail as Email,cust.phoneNumber as Phone,cust.address1 as BillingAddress_Addr1,cust.address2 as BillingAddress_Addr2,cust.Country as BillingAddress_Country,cust.State as BillingAddress_State,cust.City as BillingAddress_City,cust.zipCode as BillingAddress_PostalCode,cust.ship_address1 as ShipAddress_Addr1,cust.ship_address2 as ShipAddress_Addr2,cust.ship_country as ShipAddress_Country,cust.ship_state as ShipAddress_State,cust.ship_city as ShipAddress_City,cust.ship_zipcode as ShipAddress_PostalCode,cust.companyName as CompanyName,cust.companyID as CompanyID,cust.profile_picture as Profile_Picture,customerStatus as CustomerStatus');
	    	$this->db->join('xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID AND tr.merchantID = cust.merchantID', 'INNER');
	    }else{
	    	$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
	    	$this->db->select('cust.FirstName as FirstName, cust.LastName as LastName, cust.FullName as FullName, cust.Contact as Email, cust.Phone as Phone, cust.BillingAddress_Addr1 as BillingAddress_Addr1, cust.BillingAddress_Addr2 as BillingAddress_Addr2, cust.BillingAddress_Country as BillingAddress_Country, cust.BillingAddress_State as BillingAddress_State, cust.BillingAddress_City as BillingAddress_City, cust.BillingAddress_PostalCode as BillingAddress_PostalCode, cust.ShipAddress_Addr1 as ShipAddress_Addr1, cust.ShipAddress_Addr2 as ShipAddress_Addr2, cust.ShipAddress_Country as ShipAddress_Country, cust.ShipAddress_State as ShipAddress_State, cust.ShipAddress_City as ShipAddress_City, cust.ShipAddress_PostalCode as ShipAddress_PostalCode, cust.companyName as CompanyName, cust.companyID as CompanyID, cust.profile_picture as Profile_Picture, customerStatus as CustomerStatus');
	    	$this->db->join('chargezoom_test_customer cust', 'tr.customerListID = cust.ListID AND tr.merchantID = cust.qbmerchantID', 'INNER');
	    }

	    $this->db->where("tr.merchantID ", $userID);
	    $this->db->where("tr.transactionID ", $transactionID);
	    $this->db->group_by("tr.id");
	    $this->db->order_by("tr.id",'desc');
	    $result = $this->db->get()->row_array();
	    if ($result)
	    {	
	    	$result['invoice_no'] = '';
	        if (!empty($result['invoice_id']))
	        {
	            $inv_array = array();
	            $inv_array = explode(', ', $result['invoice_id']);
	            $res_inv = array();
	            foreach ($inv_array as $inv)
	            {	
	            	if($type == 1){
				    	$qr = $this->db->query("Select refNumber from QBO_test_invoice where invoiceID = '". $inv."'")->row_array();
		                if ($qr)
		                {
		                    $res_inv[] = $qr['refNumber'];
		                }
				    }else if($type == 2){
				    	$qq = $this->db->query(" Select RefNumber from qb_test_invoice where TxnID='" . trim($inv) . "'")->row_array();
		                if ($qq)
		                {
		                    $res_inv[] = $qq['RefNumber'];
		                }
				    }else if($type == 3){

		                $qr = $this->db->query("Select refNumber from Freshbooks_test_invoice where invoiceID = '". $inv."'")->row_array();
		                if ($qr)
		                {
		                    $res_inv[] = $qr['refNumber'];
		                }
				    }else if($type == 4){
				    	$qr = $this->db->query("Select refNumber from Xero_test_invoice where invoiceID = '". $inv."'")->row_array();
		                if ($qr)
		                {
		                    $res_inv[] = $qr['refNumber'];
		                }
				    }else{
				    	$qq = $this->db->query(" Select RefNumber from chargezoom_test_invoice where TxnID='" . trim($inv) . "'")->row_array();
		                if ($qq)
		                {
		                    $res_inv[] = $qq['RefNumber'];
		                }
				    }
	            }
	            $result['invoice_no'] = implode(', ', $res_inv);
	        }
	    }
	    return $result;
	}

	public function getPrintTransactionReceiptPDF(string $transactionID = '')
	{
		if ($this->session->userdata('logged_in')) {
			$loginData = $this->session->userdata('logged_in');
			$user_id = $loginData['merchID'];
			$operator = $loginData['firstName'].' '.$loginData['lastName'];
		}

		if ($this->session->userdata('user_logged_in')) {
			$loginData = $this->session->userdata('user_logged_in');
			$user_id 				= $loginData['merchantID'];
			$operator = $loginData['userFname'].' '.$loginData['userLname'];
		}
		$type = $loginData['active_app'];
		$transactionDetail = $data = $this->getPrintTransactionReceiptData($user_id, $transactionID, $type);

		$html = '';
		if($data){
			if(strpos(strtolower($data['transactionType']), 'refund') !== false){
                $tr_type = "Refund";

            }else if (strpos($data['transactionType'], 'sale') !== false || strtoupper($data['transactionType']) == 'AUTH_CAPTURE') {
                $tr_type = "Sale";

            } else if (strpos($data['transactionType'], 'capture') !== false || strtoupper($data['transactionType']) != 'AUTH_CAPTURE') {
                $tr_type = "Capture";

            } else if (strpos($data['transactionType'], 'Offline Payment') !== false) {
                $tr_type = "Offline Payment";
            }

			$transactionCode = $data['transactionCode'];
			if($transactionCode == 200 || $transactionCode == 100 || $transactionCode == 1 || $transactionCode == 111){
            	$status = 'Successful';
            }else{
                $status = 'Failed';
            }
            $date = $data['transactionDate'];
            if(isset($loginData['merchant_default_timezone']) && !empty($loginData['merchant_default_timezone'])){
                $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $loginData['merchant_default_timezone']];
                $date = getTimeBySelectedTimezone($timezone);
            }

			$referenceMemo = $data['referenceMemo'];
			$transactionType = $data['transactionGateway'];
			$isSurcharge = 0;
			$surCharge = 0;
			$totalAmount = 0;
			if($transactionType == 10){
        		$resultAmount = getiTransactTransactionDetails($user_id,$transactionID);
        		$totalAmount = $resultAmount['totalAmount'];
        		$surCharge = $resultAmount['surCharge'];
        		$isSurcharge = $resultAmount['isSurcharge'];
        		if($resultAmount['payAmount'] != 0){
        			$data['transactionAmount'] = $resultAmount['payAmount'];
        		}   
        	}

	        $custom_data = $data['custom_data_fields'];
	        $po_number = '';
			$payment_type = '';
            if($custom_data){
	            $json_data = json_decode($custom_data, 1);
            	if(empty($data['invoice_no'])){
	                if(isset($json_data['invoice_number'])){
	                    $data['invoice_no'] = $json_data['invoice_number'];
	                }
	            }

                if(isset($json_data['po_number'])){
                    $po_number = $json_data['po_number'];
                }

				if(isset($json_data['payment_type'])){
                    $payment_type = $json_data['payment_type'];
                }
            }
            ini_set('memory_limit','256M'); 
		  	$this->load->library("TPdf");
			
			$config_data = $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID'=> $user_id ));
			if($config_data['ProfileImage']!=""){
				$logo = LOGOURL1.$config_data['ProfileImage']; 
			}else{
				$logo  = CZLOGO;
			}

			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);   
			if(!defined('PDF_HEADER_TITLE')){
				define(PDF_HEADER_TITLE,'Invoice');
			} 

			if(!defined('PDF_HEADER_STRING')){
				define(PDF_HEADER_STRING,'');
			} 
		    $pdf->SetPrintHeader(False);
		    $pdf->SetCreator(PDF_CREATOR);
		    $pdf->SetAuthor('Chargezoom 1.0');
		    $pdf->SetTitle('Heartland PayPortal B2B Virtual Terminal');
		    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		    $pdf->SetFont('helvetica', '', 10, '', true);   
		    $pdf->setCellPaddings(1, 1, 1, 1);
			$pdf->setCellMargins(1, 1, 1, 1);
	    	$pdf->AddPage();
	  		
	  		$y = 20;
			$logo_div = '<div style="text-align:left; float:left ">
			<img src="'.$logo.'" border="0" height="50"/>
			</div>';
			
			$path = FCPATH.'uploads/transaction_pdf/';
			$pdf_file_name = 'Transaction-Receipt-'.$transactionID.'.pdf';
	        $pdfFilePath = $path.$pdf_file_name;

	        if (!file_exists($path)) {
			    mkdir($path, 0777, true);
			}

			// set color for background
			$pdf->SetFillColor(255, 255, 255);
			// write the first column
			$pdf->writeHTMLCell(80, 50, 12, $y, $logo_div, 0, 0, 1, true, 'J', true);

			$y = 50;

			// set color for background
			$pdf->SetFillColor(255, 255, 255);

			// set color for text
			$pdf->SetTextColor(51, 51, 51);

			$html = '';
			$html .= '<h2>Transaction Receipt</h2></br></br></br></br>';
			// write the first column
			$pdf->writeHTMLCell(80, 50, '', $y, $html, 0, 0, 0, true, 'J', true);

			$html = '';
			$returnMessageObj = messageConversionObj($transactionDetail,'');
			
			if(isset($returnMessageObj) && !empty($returnMessageObj)){
                $html .= '<span style="font-size: 12px;color: '.$returnMessageObj['returnColorCode'].';">'.$returnMessageObj['message'].'</span><br/>';
            }else{
            	$html .= '<span style="font-size: 12px;color: #cf4436;;">Transaction Failed - Declined</span><br/>';
            }
			

			$pdf->writeHTMLCell(80, 50, '', 60, $html, 0, 0, 0, true, '', true);

			$html = '';
			if(strpos(strtolower($data['transactionType']), 'refund') !== false)
			{
				$html .= '<b style="font-size: 14px;">Amount: </b><span style="font-size: 12px;">('.priceFormat($data['transactionAmount'], true).')</span><br/>';
			}else{
				$html .= '<b style="font-size: 14px;">Amount: </b><span style="font-size: 12px;">'.priceFormat($data['transactionAmount'], true).'</span><br/>';
			}
			
			$pdf->writeHTMLCell(80, 50, '', 70, $html, 0, 0, 0, true, 'J', true);

			$html = '';
			$html .= '<br><b>Date: </b><span>'.date("m/d/Y h:i A", strtotime($date)).'</span><br/><br/>';
			$html .= '<b>IP Address: </b><span>'.getClientIpAddr().'</span><br/><br/>';
			$html .= '<b>Invoice(s): </b><span>'.$data['invoice_no'].'</span><br/><br/>';
			$html .= '<b>Payment Type: </b><span>'.$payment_type.'</span><br/><br/>';
			// write the first column
			$pdf->writeHTMLCell(80, 50, '', 80, $html, 0, 0, 0, true, 'J', true);

			$html = ' ';
			$html .= '<br/>';
			$html .= '<b>Operator: </b><span>'.$operator.'</span><br/><br/>';
			$html .= '<b>Transaction ID: </b><span>'.$transactionID.'</span><br/><br/>';
			$html .= '<b>PO Number: </b><span>'.$po_number.'</span><br/><br/>';

			// write the first column
			$pdf->writeHTMLCell(80, 50, '', '', $html, 0, 1, 1, true, 'J', true);

			$billingAdd = 0;
            $shippingAdd = 0;
            $isAdd = 0;
            if($data['BillingAddress_Addr1'] || $data['BillingAddress_Addr2'] || $data['BillingAddress_Country'] || $data['BillingAddress_State'] || $data['BillingAddress_City'] || $data['BillingAddress_PostalCode']){
                $billingAdd = 1;
                $isAdd = 1;
            }

            if($data['ShipAddress_Addr1'] || $data['ShipAddress_Addr2'] || $data['ShipAddress_Country'] || $data['ShipAddress_State'] || $data['ShipAddress_City'] || $data['ShipAddress_PostalCode']){
                $shippingAdd = 1;
                $isAdd = 1;
            }

	        $phone_icon = '<img src="'.FCPATH.IMAGES.'phone_icon.png'.'" width="10" height="10"/>';
	        $envelope_icon = '<img src="'.FCPATH.IMAGES.'envelope_icon.png'.'" width="10" height="10"/>';
	        if($isAdd){
	        	if($billingAdd){
	        		$html = '<table style="border: 1px solid #e8e8e8;" cellpadding="4"  >
				    	<tr style="border: 1px solid #e8e8e8;background-color:#f9f9f9;">
				        	<th style="border: 1px solid #f9f9f9;color: #333333;" align="left"> Billing Address</th>
				    	</tr>';

				   	$address_html = '<br><br><span>  '.$data['FirstName'].' '.$data['LastName'].'</span><br/><span>  </span>';
				   	$address_html .= '<span>'.$data['FullName'].'</span><br/><br/><span>  </span>';
				   
				   	if ($data['BillingAddress_Addr1'] != '') {
	                	$address_html .= $data['BillingAddress_Addr1'].'<br><span>  </span>'; 
	                }

	                if ($data['BillingAddress_Addr2'] != '') {
	                	$address_html .= $data['BillingAddress_Addr2'].'<br><span>  </span>'; 
	                }

	                $address_html .= ''.($data['BillingAddress_City']) ? $data['BillingAddress_City'] . ', ' : '';

                    $address_html .= ($data['BillingAddress_State']) ? $data['BillingAddress_State'].' ' : '';
                    $address_html .= ($data['BillingAddress_PostalCode']) ? $data['BillingAddress_PostalCode'].'<br><span>  </span>' : ''; 
                    $address_html .= ($data['BillingAddress_Country']) ? $data['BillingAddress_Country'].'<br><span>  </span>' : ''; 
	                $address_html .= '<br><span>  </span>';  
	                $address_html .= '<span style=""> '.$phone_icon.'</span> '.$data['Phone'].'<br><span>  </span>';
	                $address_html .= '<span style=""> '.$envelope_icon.'</span> <a href="javascript:void(0)"  style="text-decoration: none;">'.$data['Email'].'</a><br>';
					$html .=  '<tr style="border: 1px solid #e8e8e8;"><td  style="border: 1px solid #e8e8e8;" colspan="2">'.$address_html .'</td></tr>';

				   	$html .= '</table>';
	        		
				   	$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 0, 1, true, 'J', true);
				}

				if($shippingAdd){
	        		
				   	$html = '<table style="border: 1px solid #e8e8e8;" cellpadding="4"  >
				    	<tr style="border: 1px solid #e8e8e8;background-color:#f9f9f9;">
				        	<th style="border: 1px solid #f9f9f9;color: #333333;" align="left"> Shipping Address</th>
				    	</tr>';

				    $address_html = '<br><br><span>  '.$data['FirstName'].' '.$data['LastName'].'</span><br/><span>  </span>';
				   	$address_html .= '<span>'.$data['FullName'].'</span><br/><br/><span>  </span>';

				   	if ($data['ShipAddress_Addr1'] != '') {
	                	$address_html .= $data['ShipAddress_Addr1'].'<br><span>  </span>'; 
	                }

	                if ($data['ShipAddress_Addr2'] != '') {
	                	$address_html .= $data['ShipAddress_Addr2'].'<br><span>  </span>';
	                }

	                $address_html .= ''.($data['ShipAddress_City']) ? $data['ShipAddress_City'] . ', ' : '';

	                    $address_html .= ($data['ShipAddress_State']) ? $data['ShipAddress_State'].' ' : '';
	                    $address_html .= ($data['ShipAddress_PostalCode']) ? $data['ShipAddress_PostalCode'].'<br><span>  </span>' : ''; 
	                    $address_html .= ($data['ShipAddress_Country']) ? $data['ShipAddress_Country'].'<br><span>  </span>' : ''; 
	                $address_html .= '<br><span>  </span>';  
	                $address_html .= '<span style=""> '.$phone_icon.'</span> '.$data['Phone'].'<br><span>  </span>';
	                $address_html .= '<span style=""> '.$envelope_icon.'</span> <a href="javascript:void(0)" style="text-decoration: none;">'.$data['Email'].'</a><br>';
					$html .=  '<tr style="border: 1px solid #e8e8e8;"><td  style="border: 1px solid #e8e8e8;" colspan="2">'.$address_html .'</td></tr>';

				   	$html .= '</table>';
				   	$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 1, 1, true, 'J', true);
				}
			}

			if(!empty($referenceMemo))
			{
				$html = '<b>Reference Memo: </b><span>'.$referenceMemo.'</span><br/><br/>';
				$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 0, 1, true, 'J', true);
			}

			$pdf->Output($pdfFilePath, 'F');
		}
	}
}
