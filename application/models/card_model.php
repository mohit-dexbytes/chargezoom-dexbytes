<?php

class Card_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	
		 $this->db1 = $this->load->database('otherdb', TRUE);
		 $this->db2 = $this->load->database('devdb',TRUE);
	}
	
	
	public function get_ach_card_data($customerID, $merchantID)
	{
	        $card = array();
	        $sql = " SELECT cr.accountName,cr.CardID, customerCardfriendlyName from customer_card_data cr  where  cr.customerListID='$customerID'  and cr.merchantID='$merchantID' and cr.accountNumber!=''   "; 
			
		
				    $query1 = $this->db1->query($sql);
				    if($query1->num_rows()>0)
                    $card =   $query1->result_array();
				 
                return  $card;
	    
	}
	  
	  
	     public function get_card_data($customerID){  
  
                       $card = array();
             if($this->session->userdata('logged_in')){
    			$merchantID = $this->session->userdata('logged_in')['merchID'];
    		}
    		if($this->session->userdata('user_logged_in')){
    			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
    			}	
		
	   	        
			  
		        $sql = "SELECT CardID,customerCardfriendlyName , CardType, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

				
						 $card_data['CardID'] = $card_data['CardID'];
						 $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
						  $card_data['CardType']  = $card_data['CardType'];
						
				
						 $card[$key] = $card_data;
					   }
				}		
					
                return  $card;

     }
     
     public function get_select_card_data($con)
     {
         $r_data=array();
       $this->db1->select('customerCardfriendlyName,CardType');
       $this->db1->from('customer_card_data');
       $this->db1->where($con);
       $qur = $this->db1->get();
       if($qur->num_rows() > 0)
       {
          $r_data = $qur->row_array();     
       }
	  
	    return $r_data;
     }  
    public function get_card_expiry_data($customerID){  
  
                       $card = array();
             if($this->session->userdata('logged_in')){
    			$merchantID = $this->session->userdata('logged_in')['merchID'];
    		}
    		if($this->session->userdata('user_logged_in')){
    			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
    			}	
		
	   	       
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID' AND CardType !='Echeck'"; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
                   
                   
                   
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $card_data['CardNo']  = substr($this->decrypt($card_data['CustomerCard']),-4) ;
						 $card_data['CardID'] = $card_data['CardID'] ;
						 $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						$card_data['customerListID']  = $card_data['customerListID'] ;
						 $card_data['accountNumber'] = $card_data['accountNumber'] ;
						 $card_data['accountName']  = $card_data['accountName'] ;
						 $card_data['CardType'] = $card_data['CardType'] ;
                
                         $isSurcharge = 0;
                         $setSurcharge = isset($card_data['isSurcharge'])?$card_data['isSurcharge']:0;
                        if($card_data['CardNo'] != '' && $setSurcharge != 1){
                            $isSurcharge = $this->chkCardSurcharge(['cardNumber' => $this->decrypt($card_data['CustomerCard'])]);
                        }
                        $card_data['isSurcharge']                 = $isSurcharge;
						 $card[$key] = $card_data;
					   }
				}		
					
                return  $card;

	}
     
     
     	
	public function get_credit_card_info($compID)
	{
		$card_data=array();
	  
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
		 $query1   = $this->db1->query($sql);
		$card_data =   $query1->result_array();
		return $card_data;
	}
	
	
	
	public function get_credit_card_info_data($compID){
	    
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
    	 $query1   = $this->db1->query($sql);
	    return $query1->result_array();
	}
	
	
   public function get_single_card_data($cardID)
    {  
  
                  $card = array();
   
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
					$card['CardNo']                   = $this->decrypt($card_data['CustomerCard']);
					$card['cardMonth']                = $card_data['cardMonth'];
					$card['cardYear']                 = $card_data['cardYear'];
					$card['CardID']                   = $card_data['CardID'];
					$card['CardCVV']                  = ''; 
					$card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
					$card['Billing_Addr1']            = $card_data['Billing_Addr1'];
					$card['Billing_Addr2']            = $card_data['Billing_Addr2'];
					$card['Billing_City']             = $card_data['Billing_City'];
					$card['Billing_State']            = $card_data['Billing_State'];
					$card['Billing_Country']          = $card_data['Billing_Country'];
					$card['Billing_Contact']          = $card_data['Billing_Contact'];
					$card['Billing_Zipcode']          = $card_data['Billing_Zipcode'];
					$card['CardType']                 = $card_data['CardType'];
					$card['accountNumber']                  = $card_data['accountNumber'];
					$card['routeNumber']                  = $card_data['routeNumber'];
					$card['accountName']                 = $card_data['accountName'];
					$card['accountType']                 = $card_data['accountType'];
					$card['accountHolderType']                 = $card_data['accountHolderType'];
					$card['secCodeEntryMethod']                 = $card_data['secCodeEntryMethod'];
                    $card['customerListID']                = $card_data['customerListID'];
                    
                    $isSurcharge = 0;
                    if($card['CardNo'] != '' && $card_data['isSurcharge'] != 1){
                        $isSurcharge = $this->chkCardSurcharge(['cardNumber' => $card['CardNo']]);
                    }
					$card['isSurcharge']                 = $isSurcharge;
				}		
					
					return  $card;

    }
 

	
  public function get_expiry_card_data($customerID, $type){  
  
                       $card = array();
					   
			  if($type=='1'){ 
			    /**************Expired Card***********/
			    
		     	$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date 
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day )  order by expired_date asc  "; 
			  }
            
			  if($type=='0'){
				  
				  /**************Expiring SOON Card***********/
				$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where 
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  order by CardID desc  ";  
			 			  
			  }	
				    $query1 = $this->db1->query($sql);
			        $card_data =   $query1->row_array();
					
			        if(!empty($card_data)){  
					      
			             $card['CustomerCard']     = substr($this->decrypt($card_data['CustomerCard']),12); 
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['expiry']    = $card_data['expired_date'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = ''; 
						  $card['customerListID'] = $card_data['customerListID'];
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						  
						
					}
					
					return  $card;
					
					
				 
    }

    public function last_four_digit_card($cid, $mID)
    {
        $card = '';
        $sql = "Select CustomerCard from  customer_card_data where  customerListID = '" . $cid . "' and merchantID ='" . $mID . "'  order by CardID desc  limit 1   ";

        $query1 = $this->db1->query($sql);

        if ($query1->num_rows() > 0) {
            $card_data = $query1->row_array();
            $card      = substr($this->decrypt($card_data['CustomerCard']), 12);
        }
        return $card;
    }

    public function check_friendly_name($cid, $cfrname)
    {
        $card = array();

        $query1 = $this->db1->query("Select CardID from customer_card_data where customerListID = '" . $cid . "' and customerCardfriendlyName ='" . $cfrname . "' ");
        $card = $query1->row_array();

        return $card;

    }

    public function insert_new_card($card)
    {

        if ($this->session->userdata('logged_in')) {
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

        $card_no  = $card['card_number'];
        $expmonth = $card['expiry'];
        $exyear   = $card['expiry_year'];
        $cvv      = $card['cvv'];

        $b_add1    = $card['Billing_Addr1'];
        $b_add2    = $card['Billing_Addr2'];
        $b_city    = $card['Billing_City'];
        $b_state   = $card['Billing_State'];
        $b_country = $card['Billing_Country'];
        $b_contact = $card['Billing_Contact'];
        $b_zip     = $card['Billing_Zipcode'];

        $friendlyname = $card['friendlyname'];
        $customer     = $card['customerID'];
        $is_default = 0;

        $checkCustomerCard = checkCustomerCard($customer,$merchantID);
        if($checkCustomerCard == 0){
            $is_default = 1;
        }
        

        $insert_array = array('cardMonth' => $expmonth,
            'cardYear'                        => $exyear,
            'CustomerCard'                    => $this->encrypt($card_no),
            'CardCVV'                         => '',

            'Billing_Addr1'                   => $b_add1,
            'Billing_Addr2'                   => $b_add2,
            'Billing_City'                    => $b_city,
            'Billing_State'                   => $b_state,
            'Billing_Country'                 => $b_country,
            'Billing_Contact'                 => $b_contact,
            'Billing_Zipcode'                 => $b_zip,
            'is_default'                      => $is_default,
            'customerListID'                  => $customer,
            'merchantID'                      => $merchantID,
            'customerCardfriendlyName'        => $friendlyname,
            'createdAt'                       => date("Y-m-d H:i:s"));

        $this->db1->insert('customer_card_data', $insert_array);
        return $this->db1->insert_id();

    }

    public function update_card($card, $cid, $cfrname)
    {
        $this->load->library('encrypt');

        $card_no   = $card['card_number'];
        $expmonth  = $card['expiry'];
        $exyear    = $card['expiry_year'];
        $cvv       = $card['cvv'];
        $b_add1    = $card['Billing_Addr1'];
        $b_add2    = $card['Billing_Addr2'];
        $b_city    = $card['Billing_City'];
        $b_state   = $card['Billing_State'];
        $b_country = $card['Billing_Country'];
        $b_contact = $card['Billing_Contact'];
        $b_zip     = $card['Billing_Zipcode'];

        $update_data = array('cardMonth' => $expmonth,
            'cardYear'                       => $exyear,
            'CustomerCard'                   => $this->encrypt($card_no),
            'CardCVV'                        => '',
            'Billing_Addr1'                  => $b_add1,
            'Billing_Addr2'                  => $b_add2,
            'Billing_City'                   => $b_city,
            'Billing_State'                  => $b_state,
            'Billing_Country'                => $b_country,
            'Billing_Contact'                => $b_contact,
            'Billing_Zipcode'                => $b_zip,
            'updatedAt'                      => date("Y-m-d H:i:s"));
        $this->db1->where('customerListID', $cid);
        $this->db1->where('customerCardfriendlyName', $cfrname);
        $this->db1->update('customer_card_data', $update_data);
        return true;

    }

    public function get_merchant_card_expiry_data($merchantID)
    {
        $card_data = array();
        $card      = array();

        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c
		     	where   merchantListID='$merchantID'  ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->result_array();

        return $card_data;

    }

    public function get_merchant_single_card_data($merchantCardID)
    {

        $card = array();

        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c
		     	where  merchantCardID='$merchantCardID'    ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {

            $card['CardNo']               = $this->decrypt($card_data['MerchantCard']);
            $card['CardMonth']            = $card_data['CardMonth'];
            $card['CardYear']             = $card_data['CardYear'];
            $card['merchantCardID']       = $card_data['merchantCardID'];
            $card['CardCVV']              = ''; 
            $card['merchantFriendlyName'] = $card_data['merchantFriendlyName'];
        }

        return $card;

    }

    public function get_single_mask_card_data($cardID)
    {

        $card = array();

        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
		     	where  CardID='$cardID'    ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();

        if (!empty($card_data)) {
            $re             = '/\d(?=\d{4})/m';
            $str            = $this->decrypt($card_data['CustomerCard']);
            $card['CardNo'] = preg_replace($re, "X", $str);
            $card['cardMonth']                = $card_data['cardMonth'];
            $card['cardYear']                 = $card_data['cardYear'];
            $card['CardID']                   = $card_data['CardID'];
            $card['CardCVV']                  = '';
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];

            $card['accountName']   = $card_data['accountName'];
            $card['accountNumber'] = $card_data['accountNumber'];
            $card['routeNumber']   = $card_data['routeNumber'];
            $card['accountType']   = $card_data['accountType'];

            $card['accountHolderType']  = $card_data['accountHolderType'];
            $card['secCodeEntryMethod'] = $card_data['secCodeEntryMethod'];

            $card['Billing_Addr1']   = $card_data['Billing_Addr1'];
            $card['Billing_Addr2']   = $card_data['Billing_Addr2'];
            $card['Billing_City']    = $card_data['Billing_City'];
            $card['Billing_State']   = $card_data['Billing_State'];
            $card['Billing_Country'] = $card_data['Billing_Country'];
            $card['Billing_Contact'] = ($card_data['Billing_Contact']) ? $card_data['Billing_Contact'] : '';
            $card['Billing_Zipcode'] = $card_data['Billing_Zipcode'];
            $card['is_default']                   = $card_data['is_default'];
        }

        return $card;

    }

    public function create_dev_api_key($ins_data)
    {

        $this->db2->insert('tbl_merchant_api', $ins_data);
        return $id = $this->db2->insert_id();

    }

    public function delete_dev_api_key($con)
    {

        $this->db2->where($con);
        $del = $this->db2->delete('tbl_merchant_api');
        return $del;

    }

    public function chk_card_firendly_name($cid, $friendlyname, $merchantID = '')
    {
        $msql = '';
        if($merchantID != ''){
            $msql = "AND merchantID = '$merchantID'";
        }

        $query = $this->db1->query("select *  from customer_card_data where customerListID ='" . $cid . "' and   customerCardfriendlyName ='" . $friendlyname . "' $msql");
        if ($query->num_rows() > 0) {
            $crdata = $query->row_array();
        } else {
            $crdata = 0;
        }
        return $crdata;

    }

    public function update_card_data($condition, $card_data = array(), $skipKeys=['message'])
    {
        $card_data['CardCVV'] = '';

        $dataToUpdate = [];
		foreach ($card_data as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToUpdate[$key] = strip_tags($value);
			else
				$dataToUpdate[$key] = $value;
		}

        $status = false;
        $this->db1->where($condition);
        $updt = $this->db1->update('customer_card_data', $dataToUpdate);
        if ($updt) {
            $status = true;
            $this->db1->select('CardID');
            $this->db1->from('customer_card_data');
            $this->db1->where($condition);
            $query = $this->db1->get();
            if ($query->num_rows() > 0) {
                $card = $query->row_array();
                return $card['CardID'];
            }
            return $status;


        }
        return $status;

    }

    public function delete_card_data($condition)
    {
        $status = false;
        $this->db1->where($condition);
        $updt = $this->db1->delete('customer_card_data');
        if ($updt) {
            $status = true;
        }
        return $status;

    }


    public function insert_card_data($card_data, $skipKeys=['message'])
    {
        $card_data['CardCVV'] = '';

        $is_default = 0;
        $checkCustomerCard = checkCustomerCard($card_data['customerListID'],$card_data['merchantID']);
        if($checkCustomerCard == 0){
            $is_default = 1;
        }
        $card_data['is_default'] = $is_default;

        $dataToInsert = [];
		foreach ($card_data as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		}

        $this->db1->insert('customer_card_data', $card_data);

        return $status = $this->db1->insert_id();

    }

    public function sign($message, $key)
    {

        return hash_hmac('sha256', $message, $key) . $message;
    }


    public function verify($bundle, $key)
    {
        return hash_equals(
            hash_hmac('sha256', mb_substr($bundle, 64, null, '8bit'), $key),
            mb_substr($bundle, 0, 64, '8bit')
        );
    }

    public function getKey($password, $keysize = 40)
    {
        return hash_pbkdf2('sha256', $password, 'some_token', 100000, $keysize, true);
    }

    public function encrypt($message)
    {

        if (empty($message)) {
            return null;
        }
        $password = ENCRYPTION_KEY;
        $iv       = random_bytes(16);
        $key      = $this->getKey($password);
        $result   = $this->sign(openssl_encrypt($message, 'aes-256-ctr', $key, OPENSSL_RAW_DATA, $iv), $key);
        return bin2hex($iv) . bin2hex($result);
    }
	 
	public function get_merch_card_data($cardID)
		{
		        $res=[];

		        $this->db1->select('mcd.*');
		        $this->db1->from('merchant_card_data mcd');
		        $this->db1->where('mcd.merchantCardID', $cardID);
		        $res = $this->db1->get()->row();
		        
		        if(isset($res->MerchantCard) && $res->MerchantCard != null){
		        	$res->MerchantCard = $this->decrypt($res->MerchantCard);
		        }
		        if(isset($res->CardCVV) && $res->CardCVV != null){
		        	$res->CardCVV = '';
		        }
		       
			 	return $res;
		    
		}
           
 	public function update_merchant_card_data($condition, $card_data=array(), $skipKeys=['message'])
	 {  
        $dataToUpdate = [];
		foreach ($card_data as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToUpdate[$key] = strip_tags($value);
			else
				$dataToUpdate[$key] = $value;
		}

	     $status=false;
	     $this->db1->where($condition);
		 $updt = $this->db1->update('merchant_card_data',$dataToUpdate);
		 if($updt)
		 {
            $status =true;
            $this->db1->select('*');
            $this->db1->from('merchant_card_data');
             $this->db1->where($condition);
             $query = $this->db1->get();
             if($query->num_rows() >0)
            {  
             $card =  $query->row_array();
             return   $card['merchantCardID'];
            }
            return $status;
            
		 }	 
		 return $status;

	 }	
	  public function insert_merchant_card_data($card_data, $skipKeys=['message'])
	 {  
	    
      
        $dataToInsert = [];
		foreach ($card_data as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		}
	   
		 $this->db1->insert('merchant_card_data',$dataToInsert);

		
    	return	 $status = $this->db1->insert_id();

	 }  


    public function decrypt($hash)
    {

        if (empty($hash)) {
            return null;
        }

        $password = ENCRYPTION_KEY;
        $iv       = hex2bin(substr($hash, 0, 32));
        $data     = hex2bin(substr($hash, 32));
        $key      = $this->getKey($password);
        if (!$this->verify($data, $key)) {
            return null;
        }
        return openssl_decrypt(mb_substr($data, 64, null, '8bit'), 'aes-256-ctr', $key, OPENSSL_RAW_DATA, $iv);
    }

    public function process_card($card_data, $payoption = '')
    {

        $card_no      = '';
        $cvv          = '';
        $card_type    = '';
        $friendlyname = '';

        $card_no = $card_data['CustomerCard'];
        $card_data['isSurcharge'] = $this->chkCardSurcharge(['cardNumber' => $card_no]);

        $cvv = $card_data['CardCVV'];

        $card_type = $card_data['CardType'];

        $friendlyname = $card_type . ' - ' . substr($card_no, -4);

        $customerID = $card_data['customerListID'];

        $card_condition = array(
            'customerListID'           => $card_data['customerListID'],
            'customerCardfriendlyName' => $friendlyname,
        );

        if(isset($card_data['CardNo'])){
            unset($card_data['CardNo']);
        }

        $crdata = $this->chk_card_firendly_name($customerID, $friendlyname);

        if(isset($card_data['CardNo'])){
            unset($card_data['CardNo']);
        }

        if ($crdata > 0) {
            if ($payoption == 2) {

                // checking for echeck

            } else {

                $card_data['CustomerCard'] = $this->encrypt($card_no);
                $card_data['CardCVV']      = '';
                $card_data['updatedAt']    = date("Y-m-d H:i:s");

            }

            $id1 = $this->update_card_data($card_condition, $card_data);
        } else {
            if ($payoption == 2) {

                // checking for echeck

            } else {

                $card_data['CustomerCard']             = $this->encrypt($card_no);
                $card_data['CardCVV']                  = '';
                $card_data['customerCardfriendlyName'] = $friendlyname;
                $card_data['createdAt']                = date("Y-m-d H:i:s");
            }

            $id1 = $this->insert_card_data($card_data);

        }

        return $id1;

    }

    public function get_ach_info_data($customerID)
    {

        $achAccount = array();
        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $sql = "SELECT *
		from customer_card_data c
		where  customerListID='$customerID'  and merchantID='$merchantID' AND CardType ='Echeck'";
        $query1    = $this->db1->query($sql);
        $ach_datas = $query1->result_array();

        if (!empty($ach_datas)) {
            foreach ($ach_datas as $key => $ach_data) {
                $ach_data['CardID']                   = $ach_data['CardID'];
                $ach_data['customerCardfriendlyName'] = $ach_data['customerCardfriendlyName'];
                $achAccount[$key]                     = $ach_data;
            }
        }

        return $achAccount;

    }

	public function process_ack_account($acctData)
    {
		$accountNumber = $acctData['accountNumber'];
		$friendlyname = 'Echeck - '.substr($accountNumber, -4);
        $customerID = $acctData['customerListID'];
        $card_condition = array(
            'customerListID'           => $acctData['customerListID'],
            'customerCardfriendlyName' => $friendlyname,
        );
		$crdata = $this->chk_card_firendly_name($customerID, $friendlyname);
		$acctData['customerCardfriendlyName'] = $friendlyname;
		$acctData['CardType'] = 'Echeck';
        
        if ($crdata > 0) {
			$id1 = $this->update_card_data($card_condition, $acctData);
        } else {
			$acctData['createdAt']                = date("Y-m-d H:i:s");
            $id1 = $this->insert_card_data($acctData);
        }

        return $id1;

    }
	 
	public function get_customer_card_data($customerID){  
	
		$card = array();
		if($this->session->userdata('logged_in')){
		$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}	



		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		where  customerListID='$customerID'  and merchantID='$merchantID' ORDER by is_default DESC "; 
		$query1 = $this->db1->query($sql);
		$card_datas =   $query1->result_array();



		if(!empty($card_datas )){	
			foreach($card_datas as $key=> $card_data){
				$card_number = ($card_data['CardType'] == 'Echeck') ? $card_data['accountNumber'] : $this->decrypt($card_data['CustomerCard']);
				$card_data['CardNo']  = substr($card_number,-4) ;
				$card_data['CardID'] = $card_data['CardID'] ;
				$card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
				$card_data['customerListID']  = $card_data['customerListID'] ;
				$card_data['accountNumber'] = $card_data['accountNumber'] ;
				$card_data['accountName']  = $card_data['accountName'] ;
				$card_data['CardType'] = $card_data['CardType'] ;

				$card[$key] = $card_data;
			}
		}		
		
		return  $card;

	}

	public function get_merchant_card_data($args){  
  

		$condition = [
			'merchantID' => $args['merchantID'],
			'customerListID' => $args['customerID'],
		];

		if($args['account_type'] == 1) {
		} else if($args['account_type'] == 2) {
			$condition['accountNumber'] = $args['acc_number'];
		} else {
			return false;
		}

		if(isset($args['edit_cardID']) && !empty($args['edit_cardID'])){
			$condition['CardID !='] = $args['edit_cardID'];
		}
	
		$returnValue = false;

		$this->db1->select('*');
		$this->db1->from('customer_card_data');
		$this->db1->where($condition);
		$query = $this->db1->get();
		if($query->num_rows() >0){  
			if($args['account_type'] == 1) {
				$cardList = $query->result_array();
				foreach ($cardList as $key => $singleCard) {
					$cardNumber = $this->decrypt($singleCard['CustomerCard']);
					if($cardNumber == $args['card_number']){
						$returnValue = true;
						break;
					}
				}
			} else {
				$returnValue = true;
			}

		}

		return $returnValue;
    }    
    
    public function chkCardSurcharge($args){
		$cardNumber = $args['cardNumber'];
		if(empty($cardNumber)){
            return 0;
        }	
		$query  = $this->db->query("SELECT *
		FROM tbl_card_bin_range
		WHERE LEFT($cardNumber, LENGTH(low_bin)) BETWEEN low_bin AND max_bin
		ORDER BY LENGTH(low_bin) DESC LIMIT 1");
		if($query->num_rows()>0 ) {
			$res = $query->row_array();
			if(isset($res['is_surcharge'])){
				return $res['is_surcharge'];
			}else{
				return 0;
			}
		}
		
		return 0;
	}
    public function getCardData($customerID,$cardType = ''){  
  
        $card = array();
        if($this->session->userdata('logged_in')){
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if($this->session->userdata('user_logged_in')){
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }   
        $condtnApply = '';
        if($cardType == 1){
            $condtnApply = '';
        }else if($cardType == 2){
            $condtnApply = " AND CardType != 'Echeck' ";
        }else if($cardType == 3){
            $condtnApply = " AND CardType = 'Echeck' ";
        }else if($cardType == 4){
            $condtnApply = " AND CardType = 'No record found' ";
        }else{
            $condtnApply = " AND CardType != 'Echeck' ";
        }
        
        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
                where  customerListID='$customerID'  and merchantID='$merchantID' $condtnApply "; 
                    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
                   
        if(!empty($card_datas )){ 
            foreach($card_datas as $key=> $card_data){

                $card_data['CardNo']  = null;
                $card_data['CardID'] = $card_data['CardID'] ;
                $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
                $card_data['customerListID']  = $card_data['customerListID'] ;
                $card_data['accountNumber'] = $card_data['accountNumber'] ;
                $card_data['accountName']  = $card_data['accountName'] ;
                $card_data['CardType'] = $card_data['CardType'] ;

                $isSurcharge = 0;
                $setSurcharge = isset($card_data['isSurcharge'])?$card_data['isSurcharge']:0;
                
                $card_data['isSurcharge'] = $setSurcharge;
                $card[$key] = $card_data;
            }
        }       
                    
        return  $card;

    }
    public function getCustomerCardDataByID($customerID){  
  
        $card = array();
        if($this->session->userdata('logged_in')){
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if($this->session->userdata('user_logged_in')){
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }   
        
               
                 
        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
        where  customerListID='$customerID'  and merchantID='$merchantID' AND CardType !='Echeck'"; 
        $query1 = $this->db1->query($sql);
        $card_datas =   $query1->result_array();
                   
                   
        if(!empty($card_datas )){ 
            foreach($card_datas as $key=> $card_data){
             $decryptCard = '';
            
             $card_data['CardID'] = $card_data['CardID'] ;
             $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
             $card_data['customerListID']  = $card_data['customerListID'] ;
             $card_data['accountNumber'] = $card_data['accountNumber'] ;
             $card_data['accountName']  = $card_data['accountName'] ;
             $card_data['CardType'] = $card_data['CardType'] ;

             $isSurcharge = 0;
             $setSurcharge = isset($card_data['isSurcharge'])?$card_data['isSurcharge']:0;

            $card_data['isSurcharge']                 = $isSurcharge;
             $card[$key] = $card_data;
           }
        }       
                
        return  $card;

    }

    public function getRecentCustomerCardData($customerID,$CardType){  
    
        $card = array();
        if($this->session->userdata('logged_in')){
        $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if($this->session->userdata('user_logged_in')){
        $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }   

        if($CardType == 1){
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
            where  customerListID='$customerID'  and merchantID='$merchantID' and CustomerCard != '' and CardType != 'Echeck' ORDER BY createdAt DESC"; 
        }else if($CardType == 3){
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
            where  customerListID='$customerID'  and merchantID='$merchantID' and is_default = 1 ORDER BY createdAt DESC"; 
        }else{
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
            where  customerListID='$customerID'  and merchantID='$merchantID' and accountNumber != '' ORDER BY createdAt DESC"; 
        }

        
        $query1 = $this->db1->query($sql);
        $card_data =   $query1->row_array();

        if(isset($card_data['CardID'])){   
            $card_data['CardID'] = $card_data['CardID'] ;
            if($CardType == 1){
                $card_number = $this->decrypt($card_data['CustomerCard']);
                $card_data['CardNo']  = $card_number;

            }else if($CardType == 3){
                if($card_data['CustomerCard'] != ''){
                    $card_number = $this->decrypt($card_data['CustomerCard']);
                    $card_data['CardNo']  = $card_number;
                }else{
                    $card_data['accountNumber'] = $card_data['accountNumber'];
                    $card_data['accountName']  = $card_data['accountName'];
                }
                

            }else{
                $card_data['accountNumber'] = $card_data['accountNumber'];
                $card_data['accountName']  = $card_data['accountName'];
                
            }
            $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
            $card_data['customerListID']  = $card_data['customerListID'] ;
            $card_data['CardType'] = $card_data['CardType'] ;

           
        }       
        
        return  $card_data;

    }
    public function update_customer_card_data($condition, $card_data=array())
     {  
         $status=false;
         $this->db1->where($condition);
         $updt = $this->db1->update('customer_card_data',$card_data);
         if($updt)
         {
            $status =true;
            $this->db1->select('*');
            $this->db1->from('customer_card_data');
             $this->db1->where($condition);
             $query = $this->db1->get();
             if($query->num_rows() >0)
            {  
             $card =  $query->row_array();
             return   $card['CardID'];
            }
            return $status;
            
         }   
         return $status;

     }  
    public function insertBillingdata($card_data)
    {
        $card_data['CardCVV'] = '';
        $is_default = 0;
        
        $this->db1->insert('customer_card_data', $card_data);

        return $status = $this->db1->insert_id();
    }
}

