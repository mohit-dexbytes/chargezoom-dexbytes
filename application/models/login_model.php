<?php
Class Login_model extends CI_Model
{
	private $tbl_user = 'tbl_merchant_data'; // user table name 

	/**
     * Constructor of the class
     *
     */
	function __construct() 
	{ 
		parent::__construct(); 
		define('FORGET_PASSWORD_USER_TYPE', 'Merchant'); 
	}
	
	function Login()
	{
		parent::Model();
	}

	
   
   	function check_user_mecrchant($login_id, $password)
	{
		$this->db->select('m.*');
		$this->db->from('tbl_merchant_data m');
		$this->db->join('tbl_reseller r','r.resellerID=m.resellerID','inner');
		$this->db->where('merchantEmail', $login_id);
		$this->db->where('m.isEnable', '1');
		$this->db->where('m.isDelete', '0');
		$this->db->where('r.isEnable', '1');
		$this->db->where('r.isDelete', '0');
		$this->db->limit(1);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		    $merch_data = $query->row_array();

            if ($merch_data['merchantPasswordNew'] !== null) {
                if (!password_verify($password, $merch_data['merchantPasswordNew'])) {
                    return false;
                }
            } else {
                if (md5($password) !== $merch_data['merchantPassword']) {
                    return false;
                }
            }

			$this->db->where('merchantEmail', $login_id);
			$this->db->update($this->tbl_user, array(
				'is_logged_in' => '1'
			));

			return $query->row();
		} else {
            return false;
		}
	}


	function user_logout()
	{ 
		$this->db->trans_start();
		$session_data = $this->session->userdata('logged_in'); 
		$this->db->where('merchID', $session_data['merchID']); 
		$this->db->update($this->tbl_user,array('is_logged_in'=>'0')); 
		$this->session->unset_userdata('logged_in'); 

		$this->db->trans_complete(); 
		return $this->db->trans_status();

	}
	
	//Forgot Password 
	
	function temp_reset_password($code,$email)
	{
        $passBcrypt = password_hash($code, PASSWORD_BCRYPT);

		$this->db->where('merchantEmail', $email);
	    $this->db->set('merchantPasswordNew', $passBcrypt);
	    if($this->db->update($this->tbl_user))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// Admin Change Password
	
	function savenewpass($id, $password)
	{
        $passBcrypt = password_hash($password, PASSWORD_BCRYPT);

		if (isset($id) && $id != '')
		{
		 $this->db->where('merchID', $id);
		 $this->db->set('merchantPasswordNew', $passBcrypt);
		 $query = $this->db->update($this->tbl_user);
		 
		 if ($query) 
			return true;
		 else
			return false;
		}
	 }
	 
	 
	 
	function merchant_user_logout()
	{ 
		$this->db->trans_start();
		$this->session->userdata('user_logged_in');
	
		$this->session->unset_userdata('user_logged_in'); 
		$this->session->sess_destroy(); 
		

	}
 
	 
   function get_domain_logo($condition){
   
  
	   $res = array();
	   $this->db->select('ms.*, tr.*');
	   $this->db->from('Config_merchant_portal ms');
	   $this->db->join('tbl_reseller tr', 'tr.resellerID= ms.resellerID', 'INNER');
	   $this->db->where($condition);
	   $query1 = $this->db->get();
	   
	   if($query1->num_rows() >0)
		{
		    $res   =  $query1->row_array();
		    $res['type'] = 'Merchant';
		}else{
	  
	    $res = array();
		$this->db->select('cs.* ');
		$this->db->from('tbl_config_setting cs');
		$this->db->join('tbl_merchant_data md','md.merchID=cs.merchantID','inner');
	
		$this->db->where('customerPortal','1');
		$this->db->where($condition);
		$query = $this->db->get();
		if($query->num_rows() >0)
		{
		    $res   =  $query->row_array();
		    $res['type'] = 'Customer';
		}
	} 
	   
	   return $res ;
	     
	 }	 
}