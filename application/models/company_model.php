<?php
Class Company_model extends CI_Model
{
	
	function general()
	{
		parent::Model();
		$this->load->database();
		
	}
		
    
	public function get_transaction_data($userID){
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
	    $this->db->where("cust.companyID ", $userID);
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	public function get_invoice_data($userID, $customerID = null)
	{

		$res =array();
		$today  = date('Y-m-d');
		
		$this->db->cache_on();

		$sql = "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , inv.Customer_ListID as ListID, inv.Customer_FullName as FullName,
		( case   when  IsPaid ='true'     then 'Paid' 
		when  IsPaid ='true' and userStatus ='Paid'     then 'Paid'
		when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Open' 
			when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' limit 1 )='300'  then 'Failed'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Overdue'
		
		else 'Voided' end ) as status, 
		( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
		ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
		FROM qb_test_invoice inv
		
		LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL )
		WHERE inv.qb_inv_merchantID = '$userID'    
		
		AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL)  ";
		$i   = 0;
		$con = '';
		if($customerID){
			$sql .= ' AND inv.Customer_ListID = "'.$customerID.'" ';	
		}
		$column1 = array('inv.Customer_FullName', 'inv.RefNumber', 'inv.AppliedAmount');
		foreach ($column1 as $item) {

			if ($_POST['search']['value']) {

				if ($i === 0) {
					$con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
				} else {
					$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

				}

			}

			$column[$i] = $item;
			$i++;
		}

	  if ($con != '') {
		  $con .= ' ) ';
		  $sql .= $con;
	  }
	  	$column = getTableRowOrderColumnValue($_POST['order'][0]['column']);

		if($column == 0){
			if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
				$orderName = 'inv.RefNumber';
			}else{
				$orderName = 'FullName';
			}
			
	    }elseif($column == 1){
	    	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	    		$orderName = 'DueDate';
	    	}else{
	    		$orderName = 'FullName';
	    	}
	        
	    }elseif($column == 2){
	    	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	    		$orderName = 'inv.BalanceRemaining';
	    	}else{
	    		$orderName = 'inv.RefNumber';
	    	}
	        
	    }elseif($column == 3){
	    	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	    		$orderName = 'c_status';
	    	}else{
	    		$orderName = 'DueDate';
	    	}
	        
	    }elseif($column == 4){
	    	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	    		$orderName = 'inv.RefNumber';
	    	}else{
	    		$orderName = 'inv.BalanceRemaining';
	    	}
	        
	    }elseif($column == 5){
	        $orderName = 'c_status';
	    }else{
	        $orderName = 'inv.RefNumber';    
	    }
	    $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);
		$sql .= " ORDER BY $orderName  $orderBY ";
	 	$postLength = getTableRowLengthValue($_POST['length']);
	 	$startRow = getTableRowStartValue($_POST['start']);
	 if($postLength != -1){
		  $sql .='LIMIT '.$postLength.' OFFSET '.$startRow.'';
	  } 
	$query = $this->db->query($sql);
		if($query->num_rows() > 0){
		
			$res= $query->result_array(); 
		
		}

		$this->db->cache_off();

		return $res;
	}
	public function get_invoice_data_by_status($userID,$status, $customerID = null)
	{
	
		$res =array();
		 $today  = date('Y-m-d');
	  $cond = "inv.IsPaid = 'false' AND userStatus!='cancel' ";
	  if($status == 'Past Due'){
		$cond = "DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel'  AND ((select count(*) as sch_count from customer_transaction where invoiceTxnID = inv.TxnID AND merchantID = '$userID' limit 1) ='0' OR (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1) !='300')";
		}
		if($status == 'Failed'){
			$cond = "DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel' and (select count(id) from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' AND transactionCode='300' ) > 0";
		}
		if($status == 'Paid'){
			$cond = "IsPaid ='true'";
		}
		if($status == 'Cancelled'){
			$cond = "userStatus='cancel'";
		}

		if($status == 'Partial'){
			$cond .= "AND inv.AppliedAmount != 0 AND inv.BalanceRemaining > 0";
		}

		if($status == 'Open'){
			$cond .= "AND inv.BalanceRemaining > 0";
		}

		$this->db->cache_on();
		  $sql = "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , inv.Customer_ListID as ListID, inv.Customer_FullName as FullName,
		  ( case   when  IsPaid ='true'     then 'Paid' 
		  WHEN  inv.IsPaid = 'false' AND userStatus!='cancel' AND inv.AppliedAmount != 0 AND inv.BalanceRemaining > 0 THEN 'Partial'
		  WHEN  inv.IsPaid = 'false' AND userStatus!='cancel' AND inv.AppliedAmount = 0  THEN  'Open'
		   when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
		  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Current' 
			when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select count(id) from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' AND transactionCode='300') > 0  then 'Failed'
		   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Overdue'
		   
		  else 'Voided' end ) as status, 
		  ( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
		   ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
		  FROM qb_test_invoice inv
		  
		  LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
		  WHERE inv.qb_inv_merchantID = '$userID'   
		   
		   AND($cond)
			
		   AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
		  ";
		   $i   = 0;
		   $con = '';
		   if($customerID){
		  		$sql .= ' AND inv.Customer_ListID = "'.$customerID.'" ';	
		  	}
		   $column1 = array('inv.Customer_FullName', 'inv.RefNumber', 'inv.AppliedAmount');
		   foreach ($column1 as $item) {
	
			   if ($_POST['search']['value']) {
	
				   if ($i === 0) {
					   $con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
				   } else {
					   $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
	
				   }
	
			   }
	
			   $column[$i] = $item;
			   $i++;
		   }
	
		   if ($con != '') {
			   $con .= ' ) ';
			   $sql .= $con;
		   }
		    $column = getTableRowOrderColumnValue($_POST['order'][0]['column']);

	        if($column == 0){
	        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
					$orderName = 'inv.RefNumber';
				}else{
					$orderName = 'FullName';
				}
	            
	        }elseif($column == 1){
	        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	        		$orderName = 'DueDate';
	        	}else{
	        		$orderName = 'FullName';
	        	}
	            
	        }elseif($column == 2){
	        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	        		$orderName = 'inv.BalanceRemaining';
	        	}else{
	        		$orderName = 'inv.RefNumber';
	        	}
	            
	        }elseif($column == 3){
	        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	        		$orderName = 'c_status';
	        	}else{
	        		$orderName = 'DueDate';
	        	}
	            
	        }elseif($column == 4){
	        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
	        		$orderName = 'inv.RefNumber';
	        	}else{
	        		$orderName = 'inv.BalanceRemaining';
	        	}
	            
	        }elseif($column == 5){
	            $orderName = 'c_status';
	        }else{
	            $orderName = 'inv.RefNumber';    
	        }
		   $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);
		   $sql .= " ORDER BY $orderName  $orderBY ";
		   $postLength = getTableRowLengthValue($_POST['length']);
	 	   $startRow = getTableRowStartValue($_POST['start']); 
		   if($postLength != -1){
			$sql .='LIMIT '.$postLength.' OFFSET '.$startRow.'';
		}
		
		$query  =  $this->db->query($sql);
			if($query->num_rows() > 0){
			
		
				   $res= $query->result_array(); 
			
			}
			$this->db->cache_off();
	   return $res;
		}
	public function get_open_invoice_data($userID, $customerID = null)
	{

		$res =array();
		$today  = date('Y-m-d');
		
		$this->db->cache_on();

		$sql  =  "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , inv.Customer_ListID as ListID, inv.Customer_FullName as FullName, 
		( case   when  IsPaid ='true'     then 'Paid' 
		when  IsPaid ='true' and userStatus ='Paid'     then 'Paid'
		when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Current' 
			when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' limit 1 )='300'  then 'Failed'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Overdue'
		
		else 'Voided' end ) as status, 
		( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
		ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
		FROM qb_test_invoice inv
		LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
		WHERE inv.qb_inv_merchantID = '$userID'   
		AND (userStatus!='cancel' and inv.IsPaid = 'false')   
		AND DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' 
		AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
		";
		$i   = 0;
		$con = '';
		if($customerID){
	  		$sql .= ' AND inv.Customer_ListID = "'.$customerID.'" ';	
	  	}
		$column1 = array('inv.Customer_FullName', 'inv.RefNumber', 'inv.AppliedAmount');
		foreach ($column1 as $item) {
  
			if ($_POST['search']['value']) {
  
				if ($i === 0) {
					$con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
				} else {
					$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
  
				}
  
			}
  
			$column[$i] = $item;
			$i++;
		}
  
		if ($con != '') {
			$con .= ' ) ';
			$sql .= $con;
		}

		$column = getTableRowOrderColumnValue($_POST['order'][0]['column']);

        if($column == 0){
        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
				$orderName = 'inv.RefNumber';
			}else{
				$orderName = 'FullName';
			}
            
        }elseif($column == 1){
        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
        		$orderName = 'DueDate';
        	}else{
        		$orderName = 'FullName';
        	}
            
        }elseif($column == 2){
        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
        		$orderName = 'inv.BalanceRemaining';
        	}else{
        		$orderName = 'inv.RefNumber';
        	}
            
        }elseif($column == 3){
        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
        		$orderName = 'c_status';
        	}else{
        		$orderName = 'DueDate';
        	}
            
        }elseif($column == 4){
        	if(isset($_POST['customerID']) && $_POST['customerID'] != ''){
        		$orderName = 'inv.RefNumber';
        	}else{
        		$orderName = 'inv.BalanceRemaining';
        	}
            
        }elseif($column == 5){
            $orderName = 'c_status';
        }else{
            $orderName = 'inv.RefNumber';    
        }

        $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);
		$sql .= " ORDER BY $orderName  $orderBY ";
	    $postLength = getTableRowLengthValue($_POST['length']);
	 	$startRow = getTableRowStartValue($_POST['start']);
	   if($postLength != -1){
			$sql .='LIMIT '.$postLength.' OFFSET '.$startRow.'';
		} 
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
	
			$res= $query->result_array(); 
		
		}
		$this->db->cache_off();

		return $res;
	}
	public function get_invoice_upcomming_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  `inv`.TxnID, inv.RefNumber, `inv`.AppliedAmount,`inv`.BalanceRemaining, `inv`.DueDate,  
	  DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND  userStatus!='cancel'   and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND  userStatus!='cancel'  then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND  userStatus!='cancel' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  inner join tbl_company t_comp on t_comp.id = cust.companyID	  
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and  
	  t_comp.merchantID = '$userID' AND `cust`.`qbmerchantID` =  '$userID' and  cust.customerStatus='1'   order by   DueDate  asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	

	
	public function get_invoice_latest_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active' and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'   then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 

	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   inner join tbl_company t_comp on t_comp.id = cust.companyID	
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and 
	  
	  t_comp.merchantID = '$userID' AND `cust`.`qbmerchantID` =  '$userID'  and  cust.customerStatus='1'   order by   DATE_FORMAT(inv.TimeModified, '%d-%m-%Y %l.%i%p')  desc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	public function get_invoice_data_by_id($invID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and 
	  
	  `cust`.`companyID` = '$userID'   order by   DATE_FORMAT(inv.TimeModified, '%d-%m-%Y %l.%i%p')  desc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	public  function  get_invoice_item_data ($invoiceID){
		$res =array();
		
      $sql = "Select itm.*, qb_item.* from  qb_test_invoice_lineitem  itm  inner join qb_test_item qb_item on  itm.Item_ListID = qb_item.ListID where  itm.TxnID = '$invoiceID' ";	
		
	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
		
		
	} 		
	public  function  get_invoice_details_item_data ($invoiceID,$cId){
		$res =array();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
      $sql = "Select itm.*, qb_item.* from  qb_test_invoice_lineitem  itm  inner join qb_test_item qb_item on  itm.Item_ListID = qb_item.ListID where  itm.TxnID = '$invoiceID' and itm.inv_itm_merchant_id = '$user_id' and qb_item.companyListID = '$cId'";	
		
	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
		
		
	} 
	

public function get_company_invoice_data_trigger_inv($compID){	
	$res = array();
	$query = $this->db->query("select count(*) as tri_inv_count from qb_test_invoice 
	inner join qb_test_customer cust  on cust.ListID = qb_test_invoice.Customer_ListID  
	inner join tbl_company tcom on tcom.id=cust.companyID   
	WHERE tcom.merchantID = '".$compID."' and cust.qbmerchantID = '".$compID."' and customerStatus='1'  and AppliedAmount!='0.00' ");
		if($query->num_rows() > 0){
		
		return  $res=$query->row();
		
		}
	    return false;
	
}		
	
public function get_company_invoice_data_applies_inv($compID){	
	$res = array();
	$query = $this->db->query("select sum(-AppliedAmount) as applied_amount from  qb_test_invoice 
	inner join qb_test_customer cust  on cust.ListID = qb_test_invoice.Customer_ListID 
	inner join tbl_company tcom on tcom.id=cust.companyID   
	WHERE tcom.merchantID = '".$compID."' and cust.qbmerchantID = '".$compID."' and userStatus=''  and customerStatus='1'   ");
		if($query->num_rows() > 0){
		
		return  $res=$query->row();
		
		}
	    return false;
	
}	
	
 public function get_company_invoice_data_paynet($compID){
		$res = array();
	
	  $this->db->select("  sum(-inv.AppliedAmount) as net_amount "); 
	  $this->db->from('qb_test_invoice inv');
	  $this->db->join('qb_test_customer cust','cust.ListID = inv.Customer_ListID','inner');
	  $this->db->join('tbl_company tcom','tcom.id=cust.companyID');
	  $this->db->where('tcom.merchantID',$compID);
	  $this->db->where('cust.qbmerchantID',$compID);
	   $this->db->where('cust.customerStatus', '1');
	  $query =$this->db->get();
		if($query->num_rows() > 0){
		
		return  $res=$query->row();
		
		}
	    return false;
	}	
	
	public function get_invoice_data_count($condition, $stats){
		
		$num =0;
	    
		$this->db->select('inv.id');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company cmp','cmp.id = cust.companyID','INNER');
	    $this->db->where($condition);
	    if($stats == 3){
	    	$this->db->where('cust.qbmerchantID',$condition['cmp.merchantID']);
	    }
	    if($stats == 1){
	    	$this->db->where('cust.qbmerchantID',$condition['cmp.merchantID']);
	    }
		$this->db->where('cust.customerStatus','1');
		$this->db->where('inv.qb_inv_merchantID',$condition['cmp.merchantID']);
		
		$query = $this->db->get();

		$num = $query->num_rows();
		return $num;	
	} 		
	
	/**
	 * Get failed invoice count 
	 *
	 * @param integer $user_id
	 * @return integer 
	 */
	public function get_invoice_data_count_failed($user_id){
		$today =date('Y-m-d');
		
		$this->db->select('count(inv.id) as invoice_count');
		$this->db->from('qb_test_invoice inv');
		$this->db->where('inv.qb_inv_merchantID', $user_id);
		$this->db->where('inv.IsPaid', 'false');
		$this->db->where("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < ", $today);
		$this->db->where('(select count(ct.id) as c FROM customer_transaction ct WHERE ct.invoiceTxnID=inv.TxnID AND ct.transactionCode=\'300\' AND ct.merchantID='.$user_id.') > 0');

		return $this->db->get()->row()->invoice_count;
    }	
	 
	 
	 
	
	
		public function get_company_invoice_data_count($compID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as incount  
		  FROM qb_test_invoice inner join qb_test_customer cust  
		  on cust.ListID = qb_test_invoice.Customer_ListID 
		  inner join tbl_company tcom on tcom.id=cust.companyID 
		  WHERE tcom.merchantID = '".$compID."' AND cust.qbmerchantID = '".$compID."'  AND cust.customerStatus='1'  ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->incount;
		}
		return false;
	     
	}	
		public function get_company_customer_count($compID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as cust_count  FROM qb_test_customer  
		  inner join tbl_company tcom on tcom.id=qb_test_customer.companyID 
		  WHERE   tcom.merchantID = '".$compID."' AND qbmerchantID = '".$compID."' AND customerStatus='1' ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->cust_count;
		}
		return false;
	}	
	
	
	
	
	
	 public function get_company_invoice_data_payment($compID){
		$res = array();
		  $query  =  $this->db->query('select (select sum(tr.transactionAmount)  from customer_transaction tr 
		inner join qb_test_customer cust on cust.ListID=tr.customerListID 
		inner join tbl_company cmp on cust.companyID=cmp.id  
		inner join tbl_merchant_data mr1 on cmp.merchantID=mr1.merchID
		where mr1.merchID = "'.$compID.'" and   (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
		or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionType ="Offline Payment" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="Refund" or tr.transaction_user_status IS NULL)   and tr.transactionID!=""  and cust.customerStatus="1"  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as total_amount,
		(select sum(tr.transactionAmount) from customer_transaction tr 
		inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id inner join tbl_merchant_data mr1 on cmp.merchantID=mr1.merchID where   mr1.merchID = "'.$compID.'" and cust.customerStatus="1" AND (tr.transactionType = "Refund" OR tr.transactionType = "Pay_refund" OR tr.transactionType = "Credit" ) and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")  and tr.transactionID!=""   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as refund_amount');
		if($query->num_rows()){
		
		return  $res=$query->result_array();
		
		}
	    return false;
	} 

	

	public function get_chart_data($coID){
		
		
    $query = $this->db->query("call get_month_sum_data('".$coID."')	");		
    
	if($query->num_rows() > 0){
		
		  $res=$query->result_array();
		  $chart_arr = array();
		  
		 
		  foreach($res as $chart){
			  
			   $chart_arr['earn_amount'][] = $chart['earn'];
			   $chart_arr['month_total_invoice'][]   = $chart['month_total_invoice'];
			   $chart_arr['inv_month'][]   = $chart['tDate'];
			  
		  }
		 return  $chart_arr; 
		}
		
	}	
	 
	 
	 
	 
	
	  public function get_plan_data_invoice($userID){
		
    	$result =array();
	     $type=array("Discount", "Payment", "SalesTax");
		$this->db->select('qb_item.qb_item_id, qb_item.ListID as ListID, qb_item.FullName as FullName, qb_item.Name ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
		$this->db->where('comp.merchantID',$userID);
		
	    $this->db->where('qb_item.IsActive','true');
	    $this->db->where_not_in('qb_item.Type',$type);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
			foreach ($result as $key => $value) {
				if(!$value['FullName'])
					$result[$key]['FullName'] = $value['Name'];
			}

			usort($result, function($a, $b) {
				return strcmp(strtolower($a["FullName"]), strtolower($b["FullName"]));
			});
		}
		return $result;
	
	} 		
    
      public function get_select_data($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
					
					if($query->num_rows()>0 ) {
						$data = $query->row_array();
					   return $data;
					} else {
						return $data;
					}			
			}
	
	   public function get_plan_data($userID){
		
		$result =array();
	    
		$this->db->select('qb_item.Name as Name, qb_item.SalesDesc as Pro_desc, qb_item.FullName as FullName, qb_item.QuantityOnHand, qb_item.Parent_FullName as Parent_FullName,qb_item.Type as Type, qb_item.SalesPrice as SalesPrice, qb_item.ListID as ListID,qb_item.EditSequence as EditSequence, Discount, qb_item.IsActive ,  (case when qb_item.IsActive="true" then "Done"  when qb_item.IsActive="false" then "Done" else "Done" end) as c_status   ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
	    $this->db->where('qb_item.IsActive','true');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
        
        }
       
       return $result;
	} 		
	
	public function get_product_list($userID){
		
		$result =array();
		
	    
		$this->db->select('qb_item.Name as Name, qb_item.SalesDesc as Pro_desc, qb_item.FullName as FullName, qb_item.QuantityOnHand, qb_item.Parent_FullName as Parent_FullName,qb_item.Type as Type, qb_item.SalesPrice as SalesPrice, qb_item.ListID as ListID,qb_item.EditSequence as EditSequence, Discount, qb_item.IsActive ,  (case when qb_item.IsActive="true" then "Done"  when qb_item.IsActive="false" then "Done" else "Done" end) as c_status   ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
	    $i   = 0;
		$con = '';
	    $column1 = array('qb_item.Name','qb_item.SalesDesc','qb_item.FullName','qb_item.QuantityOnHand','qb_item.Parent_FullName', 'qb_item.Type' ); 
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

	    $column = getTableRowOrderColumnValue($_POST['order'][0]['column']);

        if($column == 0){
            $orderName = 'qb_item.Name';
        }elseif($column == 1){
            $orderName = 'qb_item.SalesDesc';
        }elseif($column == 2){
            $orderName = 'qb_item.QuantityOnHand';
        }elseif($column == 3){
            $orderName = 'qb_item.Type';
        }elseif($column == 4){
            $orderName = 'qb_item.SalesPrice';
        }else{
            $orderName = 'qb_item.Name';    
        }
        $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);

		$this->db->order_by($orderName, $orderBY);
		$postLength = getTableRowLengthValue($_POST['length']);
	 	$startRow = getTableRowStartValue($_POST['start']);
	 	
		if ($postLength != -1) {
            $this->db->limit($postLength, $startRow);
		}

		$i   = 0;
		$con = '';
		

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }

		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
        
        }
       
       return $result;
	} 
			
	


    
	   public function get_invoice_data_by_due($condion,$type=1){
		
		$res =array();
	    
		$this->db->select("`inv`.Customer_ListID,`inv`.Customer_FullName, cust.companyName, cust.FirstName, cust.LastName, `cust`.Contact,`cust`.FullName, (case when cust.ListID !='' then 'Active' else 'InActive' end) as status , `inv`.ShipAddress_Addr1, `cust`.ListID, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		
	    $this->db->where(['inv.IsPaid' => 'false', 'inv.userStatus' => 'Active']);
	    $this->db->where($condion);
	    if($type == 2){
	    	$this->db->where('cust.qbmerchantID',$condion['comp.merchantID']);
	    }
        $this->db->group_by('inv.Customer_ListID');
		$this->db->limit(10);
		$this->db->order_by('balance','desc');
	   
		$this->db->where('cust.customerStatus','1');
		$query = $this->db->get();
        if($query->num_rows() > 0){
		 return $res= $query->result_array();
		}	
	
		return $res;
	
	} 		
	

     
	   public function get_invoice_data_by_past_due($userID){
		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'   AND userStatus='Active' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` AND cust.qbmerchantID = $userID
	   INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today' AND inv.qb_inv_merchantID = $userID  AND `IsPaid` = 'false'  and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'  AND cust.customerStatus='1'      order by  BalanceRemaining   desc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 		
	
  public function get_invoice_data_by_past_time_due($userID){
		
		$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID,`inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	  DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'   AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` AND cust.qbmerchantID = $userID
		    INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND inv.qb_inv_merchantID = $userID AND `IsPaid` = 'false'  and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'  AND cust.customerStatus='1'   order by  BalanceRemaining   desc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 	

	public function get_transaction_failure_report_data($userID){
	    
	    	$res =array();
	    
	    $sql ="SELECT `tr`.`id`, `tr`.`invoiceTxnID`, `tr`.`customerListID`, `tr`.`transactionID`, `tr`.`transactionStatus`, `tr`.`transactionCode`, 
	    `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`, 
	    (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber, 
	    `cust`.`FullName` FROM (`customer_transaction` tr) 
	    INNER JOIN `qb_test_customer` cust ON `tr`.`customerListID` = `cust`.`ListID` 
	    INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	    WHERE `comp`.`merchantID` = '$userID' AND `tr`.`transactionDate` >= DATE_SUB(CURDATE(), INTERVAL 30 Day) AND transactionCode NOT IN ('200','1','100','111') AND `cust`.`customerStatus` = '1' ";
		
	
		$query = $this->db->query($sql); 
		if($query->num_rows() > 0){
	 return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
		
  public function get_invoice_data_open_due($userID, $status){
		
		$res =array();
		$con='';
		if($status=='8'){
		 $con.="and `inv`.BalanceRemaining !='0.00' and `inv`.`IsPaid` = 'false' ";
			
		}
		
		
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  `inv`.RefNumber, `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,inv.TimeCreated as addOnDate,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 

	  FROM qb_test_invoice inv 

	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` AND cust.qbmerchantID = $userID 
	  INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   
	  
	  `comp`.`merchantID` = '$userID'  $con AND inv.qb_inv_merchantID = $userID AND cust.customerStatus='1' and userStatus='Active' order by  DueDate   asc");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 	
	
	
	public function get_transaction_report_data($userID, $minvalue, $maxvalue){
		$res =array();
		$trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
		$this->db->select('tr.*, cust.*, inv.RefNumber');
		$this->db->select('sum(tr.transactionAmount) as balance,tr.invoiceTxnID,tr.id as TxnID,cust.ListID as Customer_ListID,  cust.ListID, cust.FullName ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID AND cust.qbmerchantID = '.$userID,'INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->join('qb_test_invoice inv', "inv.TxnID= tr.invoiceTxnID AND inv.qb_inv_merchantID = '$userID' ", 'LEFT');
		$this->db->where("cust.qbmerchantID", $userID);
		$this->db->where('comp.merchantID ', $userID);
		$this->db->where('tr.merchantID ', $userID);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
		$this->db->where('cust.customerStatus','1');
        $this->db->group_by("tr.transactionID");
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$res1 = $query->result_array();
            foreach ($res1 as $result) {
				if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select RefNumber   from   qb_test_invoice where TxnID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['RefNumber'];
                        }
                    }

                    $result['invoice_no'] = implode(',', $res_inv);
                }
                $res[] = $result;
			}
		}
		return  $res;
		
	} 		
	
	
	
	
	
	
		public function template_data_list($condition){
		
        $res=array();
		$this->db->select('tp.*, typ.* ');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID = tp.templateType','inner');
		$this->db->where($condition);
		
	  	$query = $this->db->get();
		  return $res =  $query->result_array();
		
	} 		
    	
	
		public function template_data($con){
		
	$res=array();
		$this->db->select('tp.*, typ.*');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID=tp.templateType','left');
	    $this->db->where($con);
		
	  	$query = $this->db->get();
		  return $res =  $query->row_array();
		
	} 		
    	
    	
	   public function get_invoice_due_by_company($condion,$status){
		
		$res =array();
	    $today  = date('Y-m-d');
  		$this->db->select("cust.FullName as label, inv.DueDate,  sum(inv.BalanceRemaining) as balance ");
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->group_by('cust.ListID');
		$this->db->order_by('balance','desc');
		$this->db->limit(5);
		if($status=='1'){
	      $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '".$today."'  AND `IsPaid` = 'false' AND `userStatus` != 'cancel'  ");
		}
		else if($status=='0'){ 

			$this->db->where("`IsPaid` = 'false'  AND `userStatus` != 'cancel'  ");
		}
	      
  		  $this->db->where($condion);
  		  $this->db->where('cust.qbmerchantID',$condion['comp.merchantID']);
  		  $this->db->where('qb_inv_merchantID',$condion['comp.merchantID']);
		  $this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
    if($query->num_rows() > 0)
    {
		$get_result1 = $query->result_array();
		
			      
			            foreach ($get_result1 as $k=> $count_merch) 
			            {
			             $get_result[$k][] = $count_merch['label'];
			            $get_result[$k][] = (float)$count_merch['balance'];
			          
                    
			        }
			      $res=  $get_result ;
		
    }
		return $res;
	} 		
	
	
	
	   public function get_paid_invoice_recent($condion){
		
		$res =array();
	    $today  = date('Y-m-d');

		$this->db->select("cust.FullName,inv.RefNumber,inv.TxnID, inv.Customer_ListID, cust.companyName, (-inv.AppliedAmount) as balance, inv.DueDate, inv.TimeModified ");
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        
		$this->db->order_by('TimeModified','desc');
		$this->db->limit(10);
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today   AND userStatus='Active' and inv.AppliedAmount !='0.00' ");
	    $this->db->where($condion);
	    $this->db->where('cust.qbmerchantID',$condion['comp.merchantID']);
		$this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	
	} 		
	
	
	public function get_oldest_due($userID){
		     

		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, inv.Customer_FullName, cust.companyName, inv.Customer_ListID, `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,                  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact, DATEDIFF('$today', inv.DueDate) as aging_days,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'   then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE `inv`.`qb_inv_merchantID` = '$userID' and  `IsPaid` = 'false' AND userStatus='Active' and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID' and `cust`.`qbmerchantID` = '$userID'  AND customerStatus='1'    order by  inv.DueDate   asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	
		
	}
	
	
	public function get_subcription_data(){
		$res    = array();
		$today  = date('Y-m-d');	
		$date   = date('d');
		$this->db->select('sb.*,cust.FullName, cust.companyName, cust.Contact, comp.qbwc_username,mer.prefix,mer.postfix,pl.proRate,pl.proRateBillingDay,pl.autoBilling,pl.nextMonthInvoiceDate');
		$this->db->from('tbl_subscriptions sb');
		$this->db->join('qb_test_customer cust','sb.customerID = cust.ListID AND sb.merchantDataID = cust.qbmerchantID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		$this->db->join('tbl_subscriptions_plan_qb pl','pl.planID=sb.subscriptionPlan','LEFT');
		$this->db->join('tbl_merchant_invoices mer', 'mer.merchantID= sb.merchantDataID','INNER');
		
		$this->db->where('sb.nextGeneratingDate = "'.$today.'" ');
		$this->db->where('cust.customerStatus','1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return  $res=$query->result_array();
		}
		return  $res;
	}		
		
	/************Unused methods***********/
	
	
	   public function get_Failure_paymemt_data($coID){
		
		$percent =0;
	    
		$sql = "Select count(*) as total_pay ,
		(select count(*) from customer_transaction where transactionType='Auto-nmi' 
		or transactionType='Sale' or transactionType='Capture' and transactionCode='100' ) as success_pay, 
		(select count(*) from customer_transaction where (transactionType='Auto-nmi' or transactionType='Sale' or transactionType='Capture') and transactionCode !='100' ) as failed_pay from customer_transaction inner join qb_test_customer cust on cust.ListID= customer_transaction.customerListID inner join tbl_company tcom on tcom.id=cust.companyID  WHERE tcom.merchantID = '".$coID."' and cust.qbmerchantID = '".$coID."' ";
		$query = $this->db->query($sql);		
		
      
	     $res=$query->row();
		 $total_pay   = $res->total_pay;
	     $success_pay = $res->success_pay;
		 $failed_pay  = $res->failed_pay; 
		 if($success_pay>0 && $failed_pay > 0)
         $percent=number_format((100 * $failed_pay) / $success_pay, 2) ;  
		
		return $percent;
	
	
	} 	
	
	
	
	public function get_product_account_data($con){
		
		$res = array();
		
		$this->db->select("pr.*");
		$this->db->from('qb_item_account pr');
		$this->db->join('tbl_company comp','comp.id = pr.company','INNER');
		$this->db->where($con);
		$this->db->order_by("AccountType",'asc');
		$query = $this->db->get();
		if($query->num_rows()>0){
			
			$res = $query->result_array();
		}
		
		
		return $res;
	}
	public function get_product_data($con){
		
		$res = array();
		
		$this->db->select("pr.*");
		 $type=array("Discount", "Payment", "SalesTax");
		$this->db->from('qb_test_item pr');
		$this->db->join('tbl_company comp','comp.id = pr.companyListID','INNER');
		$this->db->where($con);
		$this->db->where("Parent_ListID",'');
		 $this->db->where_not_in('pr.Type',$type);
		$query = $this->db->get();
		if($query->num_rows()>0){
			
			$res = $query->result_array();
		}
		
		
		return $res;
	}
	
	
	
	
	 
	public function get_piechart_due_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate < CURDATE( ) and cust.companyID = '$coID' and IsPaid='false' ";
	

		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
	
	
   
	public function get_piechart_30_data($coID){
		$num =0;
	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID WHERE inv.DueDate >= DATE_SUB(CURDATE(), INTERVAL 29 Day) and cust.companyID = '".$coID."' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
	
	
	public function get_piechart_60_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 59 Day ) AND DATE_SUB( CURDATE() ,INTERVAL 30 Day ) and cust.companyID = '$coID' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
	
	
	public function get_piechart_80_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust 
	on inv.Customer_ListID = cust.ListID where inv.DueDate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 89 Day ) AND DATE_SUB( CURDATE() ,INTERVAL 60 Day ) and cust.companyID = '$coID' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
	
    
	public function get_piechart_90_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate <= DATE_SUB( CURDATE() ,INTERVAL 90 Day ) and cust.companyID = '$coID'  and IsPaid='false'";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
		
	public function get_billing_invoice_data($invoice){	
	  
 	$sql = "SELECT * fROM tbl_merchant_billing_invoice where invoice = $invoice";
		
			 $query1 = $this->db->query($sql);
		  return $res =  $query1->result_array();
	}
	
	public function get_subplan_data($planID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT * FROM tbl_subscriptions_plan_qb INNER JOIN tbl_subscription_plan_item_qb ON tbl_subscription_plan_item_qb.planID =  '".$planID."' WHERE tbl_subscriptions_plan_qb.planID = '".$planID."'");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 




     public function get_plan_data_item($userID){
		
	$result =array();
	    
		$this->db->select('qb_item.ListID as ListID, qb_item.Name as Name ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
      $this->db->where(array('qb_item.Type !='=> 'Group'));
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
		}
		return $result;
	
	}
	
   public function get_inv_data($inv_id)
   {
   
   $result=array();
   
   $query =   $this->db->query(" Select tr.*  from customer_transaction tr inner join qb_test_customer q on tr.customerListID=q.ListID  inner join qb_test_invoice inv on tr.invoiceTxnID=inv.TxnID    where tr.invoiceTxnID='".$inv_id."' and  transactionType IN ('Stripe_sale,Sale,Pay_sale,Paypal_sale,Auth_captue') and transactionCode IN ('100,200,1,111') and tr.transaction_user_status !='refund')  ");
   
   	if($query->num_rows() > 0){
	  $result = $query->row_array();
		}
     
   
      return $result;
   
   }
   
   
        
   	  public function chart_general_volume($mID)
    {   
 
    
       $res= array();
		     $months=array();
		    for ($i = 11; $i >=0 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
            }  
      $chart_arr = array();
   
    
    
     foreach($months as $key=> $month)
	 { 
    
$sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  
  customer_transaction tr    
  
  	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
  WHERE tr.merchantID = "'.$mID.'"  AND    
  date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
		or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		
			 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
    
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where 
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and     tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
             $earn= $row['volume']- $rfquery->row_array()['rm_amount'];
            
             $res[$key]['volume'] =$earn;
            $res[$key]['Month'] =$month;
        }else{
           
            $res[$key]['volume'] =0.00;
            $res[$key]['Month'] =$month;
        }
     }  
    
    
     
     
   
      return  $res; 
}

	public function chart_all_volume($mID)
	{   

		$res= array();
		$months=array();
		$resObj = array();
		for ($i = 11; $i >=1 ; $i--) {
			$months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
		}
		$months[] = date("M-Y", strtotime( date( 'Y-m' )." 0 months"));  
		$chart_arr = array();
		$totalRevenue = 0;
		$totalRevenue = $totalCCA = $totalECLA = 0;
		foreach($months as $key=> $month)
		{ 
			$earn = $this->get_recent_volume_dashboard($mID,$month);
			$res[$key]['revenu_volume'] = $earn;
			$res[$key]['revenu_Month'] =$month;

			$earn2 = $this->get_creditcard_payment($mID,$month);
			$res[$key]['online_volume'] =$earn2;
			$res[$key]['online_Month'] = $month;
				
		

			$earn4 = $this->get_echeck_payment($mID,$month);
			$res[$key]['eCheck_volume'] =$earn4;
			$res[$key]['eCheck_Month'] =$month;

			$totalRevenue = $totalRevenue + $earn;
			$totalCCA = $totalCCA + $earn2;
			$totalECLA = $totalECLA + $earn4;
		}  

		$resObj['data'] = $res;
		$resObj['totalRevenue'] = $totalRevenue;
		$resObj['totalCCA'] = $totalCCA;
		$resObj['totalECLA'] = $totalECLA;
		return  $resObj; 
	}
   	  public function get_recent_volume($mID)
    {   
 
    
       $res= 0.00;
       
       $month = date("M-Y");
		 
    
$sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  
  customer_transaction tr    
  
  	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
  WHERE tr.merchantID = "'.$mID.'"  and  
  date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		
			 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
    	inner join	qb_test_customer cust on cust.ListID= tr.customerListID
		inner join tbl_merchant_data mr1 on cust.qbmerchantID=mr1.merchID where 
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.qbmerchantID="'.$mID.'" and  tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
             $earn= $row['volume']- $rfquery->row_array()['rm_amount'];
            
             $res =$earn;
           
        }else{
           
            $res =0.00;
           
        }
        return  $res; 
     }  
    
	 public function get_recent_volume_dashboard($mID,$month)
	 {   
  
	 
		$res= 0.00;
		$earn = 0.00;
		$refund = 0.00;
		
 		$sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
		   customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID WHERE tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
		   date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or tr.transactionType ="ECheck" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
			or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
		 tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		 and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		 
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{    
			 $row = $query->row_array();
			 $earn = $row['volume'];
			
		}
		
		 $rfsql = 'select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
			inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
			   date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and 
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
			and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL ';
	 		 $rfquery = $this->db->query($rfsql);

	 		 
			if($rfquery->num_rows()>0)
			{    
				$row = $rfquery->row_array();
				$refund = $row['refund'];
				
			}
			
		 	$res = $earn - $refund;
		 	return  $res; 
	  }  
	  public function get_creditcard_payment($mID,$month)
	  {   
   
	  
		 	$res= 0.00;
			$earn = 0.00;
			$refund = 0.00;
			 
		  
			$sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
				customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
				WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
				date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway NOT LIKE "%echeck%"  and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
				  or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
				  tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
				  and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

			
			$query = $this->db->query($sql);
			if($query->num_rows()>0)
			{    
			  $row = $query->row_array();
			  $earn = $row['volume'];
			 
			}

			$rfsql = 'select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
			inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
			   date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway NOT LIKE "%echeck%" and 
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
			and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL ';
	 		$rfquery = $this->db->query($rfsql);
			
			if($rfquery->num_rows()>0)
			{    

				$row1 = $rfquery->row_array();
				$refund1 = $row1['refund'];

			}
			$earn = $earn - $refund1;
			return  $earn; 
	   	}  
	    public function get_offline_payment($mID,$month)
		{   
			$res= 0.00;
			$sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
		 		customer_transaction tr    
			 inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
			 WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
			 date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and (tr.transactionType ="Offline Payment") and  (tr.transactionCode ="100" or 
			   tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			   and ( tr.transaction_user_status !="3" )    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
			   
			$query = $this->db->query($sql);
			if($query->num_rows()>0)
			{    
				$row = $query->row_array();
				$earn= $row['volume'];
				   
				$res =$earn;
				  
			}else{
				  
				$res =0.00;
				  
			}
		    return  $res; 
		} 
		public function get_outstanding_payment($userID){

			$res =array();
			 $today  = date('Y-m-d');
			 $month = date("M-Y");
			 $query  =  $this->db->query("SELECT  `inv`.TxnID, inv.RefNumber, `inv`.AppliedAmount,`inv`.BalanceRemaining, `inv`.DueDate, Month(inv.DueDate) 
				,sum(`inv`.BalanceRemaining) as balance, sum(`inv`.AppliedAmount) as payment,
				cust.*
				FROM qb_test_invoice inv 
				INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` AND cust.qbmerchantID = '$userID'
				WHERE inv.IsPaid = 'false'   and userStatus!='cancel' and inv.qb_inv_merchantID = '$userID' and cust.qbmerchantID = '$userID'  and  cust.customerStatus='1' order by DueDate  asc");
			  
			
				if($query->num_rows() > 0){
				
					$row = $query->row_array();
					$pay = str_replace('-','',$row['payment']);
					$earn = $row['balance']-(float)$pay;
				    $res = $earn;
				}else{
					$res = '0.00';
				}
				 return  $res;
			}
    
     public function get_schedule_payment_invoice($mID)
      {
          	$res =array();
           $this->db->select('cust.ListID,qt.TxnID,qt.RefNumber as RefNumber, cust.FullName,qt.BalanceRemaining, qt.DueDate,qt.AppliedAmount,sc.scheduleDate');
         	$this->db->from('qb_test_invoice qt');
	    	$this->db->join('qb_test_customer cust','qt.Customer_ListID = cust.ListID','INNER'); 
	    	$this->db->join('tbl_scheduled_invoice_payment sc','qt.TxnID = sc.invoiceID AND sc.merchantID = '.$mID,'INNER'); 
			$this->db->where('cust.qbmerchantID',$mID);
			$this->db->where('qt.qb_inv_merchantID',$mID);
			$this->db->where('sc.merchantID',$mID);
		  $this->db->where('IsPaid','false');
		  $this->db->where("sc.scheduleDate IS NOT NULL ");
		  
		$query = $this->db->get();
		if($query->num_rows() >0)
		{
		    $res = $query->result_array();
		}
		
		return $res;
      }
      
    
        public function get_schedule_payment_invoice_csv($mID)
      {
          	$res =array();
              $this->db->select('qt.RefNumber as RefNumber, cust.FullName,qt.BalanceRemaining, qt.DueDate,qt.AppliedAmount,sc.scheduleDate');
         	$this->db->from('qb_test_invoice qt');
	    	$this->db->join('qb_test_customer cust','qt.ListID = cust.Customer_ListID','INNER'); 
	    	$this->db->join('tbl_scheduled_invoice_payment sc','qt.TxnID = sc.invoiceID','INNER'); 
		 $this->db->where('cust.qbmerchantID',$mID);
		 	 $this->db->where('sc.merchantID',$mID);
		  $this->db->where('IsPaid','false');
		  $this->db->where("sc.scheduleDate != '0000-00-00'  ");
		  
		$query = $this->db->get();
		if($query->num_rows() >0)
		{
		    $res = $query->result_array();
		}
		
		return $res;
      }
      
      
      
    public function get_tax_data($user_id)
   {
    
    $result=array();
       	$this->db->select('tx.*  ');
		$this->db->from('tbl_taxes tx');
	    $this->db->join('tbl_company comp','comp.id = tx.companyID','INNER');
	   
	    $this->db->where("comp.merchantID",$user_id);
        $this->db->order_by("friendlyName");
		$query = $this->db->get();
    if($query->num_rows() > 0){
			$result = $query->result_array();
		}
		return $result;
      
   
   }
   
   
   public function get_ivoice_data_sch($rec_d)
   {
       $res=array();
      $ap_data = $this->get_row_data('app_integration_setting',array('merchantID'=>$rec_d['merchantID']));
      
      $recAmount =$rec_d['amount'];
      $userID    =$rec_d['merchantID'];
      $today     = date('Y-m-d');
      $date = $today;
      $con      = ' inv.BalanceRemaining <= "'.$recAmount.'"  AND   ';
       if($rec_d['optionData']==1)
      {
        $rec_date = $today;
        $con.=  " ifnull(sc.scheduleDate,`inv`.DueDate )='".$rec_date."'   ";
      }
      if($rec_d['optionData']==2)
      {
        $rec_date = $today;
        $con.=  " DATE_FORMAT(inv.TimeCreated, '%Y-%m-%d')='".$rec_date."'   ";
      }
      if($rec_d['optionData']==3)
	  {
	      $rec_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($today)) . " -1 day"));
	  
        $con.=  ' DATE_FORMAT(inv.TimeCreated, "%Y-%m-%d")="'.$rec_date.'"   ';  
	        
	  }                
	                     if($ap_data['appIntegration']==1)
	                     {
	                         
	                     $query  =  $this->db->query("SELECT `inv`.*,  inv.invoiceID as inv,
	  
                    	   ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
                    	  FROM QBO_test_invoice inv
                    	  INNER JOIN `QBO_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`  
                    	 
                    	  LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`invoiceID`
                    	  WHERE inv.IsPaid='0' and  cust.merchantID = '$userID'  and  cust.customerStatus='true'    
                    	   AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
                           AND (sc.customerID=inv.CustomerListID OR sc.customerID IS NULL) 
                            AND ( DATE_FORMAT(sc.ScheduleDate, '%Y-%m-%d') < $today  OR sc.scheduleDate IS NULL) 
                           
                           
                            AND ((sc.customerID=cust.Customer_ListID and sc.merchantID=cust.merchantID)OR sc.customerID IS NULL) 
                            AND $con   and   cust.Customer_ListID='".$rec_d['customerID']."'");
	                     }
	                     if($ap_data['appIntegration']==2)
	                     {
	                       $query  =  $this->db->query("SELECT `inv`.*,  inv.TxnID as inv,
	  
                    	   ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
                    	  FROM qb_test_invoice inv
                    	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
                    	 
                    	  LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID`
                    	  WHERE inv.IsPaid='false' and  cust.qbmerchantID = '$userID'  and  cust.customerStatus='1'    
                    	   AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
                           AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
                            AND ( DATE_FORMAT(sc.ScheduleDate, '%Y-%m-%d') < $today  OR sc.scheduleDate IS NULL) 
                           
                           
                            AND ((sc.customerID=cust.ListID and sc.merchantID=cust.qbmerchantID)OR sc.customerID IS NULL) 
                            AND $con   and   cust.ListID='".$rec_d['customerID']."'");
                        }
	                     if($ap_data['appIntegration']==3)
	                     {
	                      $query  =  $this->db->query("SELECT `inv`.*,  inv.TxnID as inv,
	  
                    	   ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
                    	  FROM chargezoom_test_invoice inv
                    	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
                    	 
                    	  LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID`
                    	  WHERE inv.IsPaid='false' and  cust.qbmerchantID = '$userID'  and  cust.customerStatus='1'    
                    	   AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
                           AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
                            AND ( DATE_FORMAT(sc.ScheduleDate, '%Y-%m-%d') < $today  OR sc.scheduleDate IS NULL) 
                           
                           
                            AND ((sc.customerID=cust.ListID and sc.merchantID=cust.qbmerchantID)OR sc.customerID IS NULL) 
                            AND $con   and   cust.ListID='".$rec_d['customerID']."'");
	                     }
	                     if($ap_data['appIntegration']==4)
	                     {
	                     }
	                     if($ap_data['appIntegration']==5)
	                     {
	                         $query  =  $this->db->query("SELECT `inv`.*,  inv.TxnID as inv,
	  
                    	   ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
                    	  FROM chargezoom_test_invoice inv
                    	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
                    	 
                    	  LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID`
                    	  WHERE inv.IsPaid='false' and  cust.qbmerchantID = '$userID'  and  cust.customerStatus='1'    
                    	   AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
                           AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
                            AND ( DATE_FORMAT(sc.ScheduleDate, '%Y-%m-%d') < $today  OR sc.scheduleDate IS NULL) 
                           
                           
                            AND ((sc.customerID=cust.ListID and sc.merchantID=cust.qbmerchantID)OR sc.customerID IS NULL) 
                            AND $con   and   cust.ListID='".$rec_d['customerID']."'");
	                     }
	                     
	                     
    		if($query->num_rows() > 0){
    	           $res= $query->result_array(); 
            
            }
            
            return $res;
            
	         
	                     
   }             
   
    public function get_row_data($table, $con) {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
    
    	public function custom_template_data_list($condition){
		
        $res=array();
		$this->db->select('tp.* ');
		$this->db->from('tbl_email_template tp');
		$this->db->where($condition);
		
	  	$query = $this->db->get();
		  return $res =  $query->result_array();
		
	} 	
	
	
	
	public function get_invoice_details_data($invID)
	{
		$user_id = false;
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		$company_ID = $this->db->select('id')->from('tbl_company')->where('merchantID', $user_id)->get();
		$cID = '';
		if($company_ID->num_rows()>0 ) {
			$company_IDs = $company_ID->row_array();
			$cID = $company_IDs['id'];
		}

		$res=array();
		$this->db->select('inv.Customer_ListID as Customer_ListID,  inv.Customer_FullName, inv.TxnID as invoiceID,inv.RefNumber as invoiceNumber, inv.BalanceRemaining as Balance, (-inv.AppliedAmount) as AppliedAmount,ShipAddress_Addr1 as saddress1,
		ShipAddress_Addr2 as saddress2,ShipAddress_City as scity,ShipAddress_State as sstate,ShipAddress_PostalCode as szipcode, ShipAddress_Country as scountry,DueDate, 
		Billing_Addr1 as baddress1,Billing_Addr2 as baddress2, Billing_City as bcity,Billing_State as bstate,Billing_Country as country,Billing_PostalCode as bzipcode,TaxRate,TotalTax,TaxListID');
		$this->db->from('qb_test_invoice inv');
		$this->db->where('TxnID',$invID);
		if($user_id){
			$this->db->where('qb_inv_merchantID',$user_id);
		}
		$query = $this->db->get();
		  
		  if($query->num_rows()>0)
		  {
            $res['invoice_data']=$query->row_array();
             $res['invoice_data']['type'] = 2;
           
        
			 $this->db->select('qb_test_item.qb_item_id as qb_item_id,qb_test_item.Name as itemName, item_FullName, Item_ListID, Descrip as Description,Quantity, Rate as itemPrice');
			 $this->db->from('qb_test_invoice_lineitem');
			 $this->db->join('qb_test_item','qb_test_invoice_lineitem.Item_ListID = qb_test_item.ListID','INNER');
			 $this->db->where(array('TxnID'=>$res['invoice_data']['invoiceID']));
			 $this->db->where('inv_itm_merchant_id',$user_id);
			 $this->db->where('qb_test_item.companyListID',$cID);

			 $query2 = $this->db->get();
			  if($query2->num_rows()>0)
		  {
			$res['item_data']=$query2->result_array();
			
		  }	 
		 else
		 {
			$res['item_data']=array();
		 }
	
			
			
			
			  $this->db->select('cs.companyName as companyName, Contact as customerEmail, Phone as PhoneNumber, cs.FirstName as FirstName, cs.LastName as LastName,cs.FullName as FullName,
			  ShipAddress_Addr1 as csaddress1,ShipAddress_Addr2 as csaddress2,ShipAddress_City as cscity,
			  ShipAddress_State as csstate,ShipAddress_PostalCode as cszipcode,BillingAddress_Addr1 as cbaddress1,BillingAddress_Addr2 as cbaddress2, BillingAddress_City as cbcity,BillingAddress_State as cbstate,BillingAddress_Country as ccountry,BillingAddress_PostalCode as cbzipcode');
				$this->db->from('qb_test_customer cs');
				$this->db->where('ListID',$res['invoice_data']['Customer_ListID']);
				if($user_id){
					$this->db->where('qbmerchantID',$user_id);
				}
				$query1 = $this->db->get();
		        
		   if($query1->num_rows()>0)
		  {
			$res['customer_data']=$query1->row_array();
			
		  }	 
		 else
		 {
			$res['customer_data']=array();
		 }
	
		
		
	}

	     return $res;
	
	}
	public function get_invoice_past_data_count($uid)
	{
	    $num=0;
	    $today = date('Y-m-d');
	 
		$cond = "DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel'  AND ((select count(*) as sch_count from customer_transaction where invoiceTxnID = inv.TxnID limit 1) ='0' OR (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1) !='300')";
		$query  =  $this->db->query("SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay ,inv.Customer_ListID as ListID, inv.Customer_FullName as FullName,
		
		ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
		FROM qb_test_invoice inv
	
		LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND (sc.merchantID=  '".$uid."' OR sc.merchantID IS NULL)
		WHERE inv.qb_inv_merchantID ='$uid'   
		
		AND($cond)
		AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
		ORDER BY inv.TimeCreated  DESC ");
	    if( $query->num_rows() >0)
	    {
	       $num = $query->num_rows();
	    }else{
			$num = 0;
		}
	  return  $num ;
	}
	public function get_invoice_paid_data_count($uid)
	{
		$this->db->select('count(inv.TxnID) as invoice_count');
		$this->db->from("qb_test_invoice inv");
		$this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=inv.TxnID and sc.merchantID=inv.qb_inv_merchantID', 'LEFT');
		$this->db->where('inv.qb_inv_merchantID', $uid);
		$this->db->where('inv.IsPaid', 'true');

		return $this->db->get()->row()->invoice_count;
	}
	public function get_subscription_invoice_data($userID, $subID)
	{
	
		$res =array();
		$today  = date('Y-m-d');
		$sql = "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.ListID, cust.FullName, 
		( case   when  IsPaid ='true'     then 'Success' 
		when  IsPaid ='true' and userStatus ='Paid'     then 'Success'
		 when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Upcoming' 
		  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
		 when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Past Due'
		 
		else 'Cancelled' end ) as status, 
		( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
		 ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
		FROM qb_test_invoice inv
		INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` AND cust.qbmerchantID = '$userID' 
		INNER JOIN tbl_subscription_auto_invoices TSAI ON ( TSAI.invoiceID = inv.TxnID OR TSAI.invoiceID = inv.insertInvID ) AND TSAI.subscriptionID = $subID AND app_type = 2 
		LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND sc.merchantID = '$userID'
		WHERE  inv.qb_inv_merchantID = '$userID' AND cust.qbmerchantID = '$userID'  and  cust.customerStatus='1'    
		 AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
		 AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
		  AND ((sc.customerID=cust.ListID and sc.merchantID=cust.qbmerchantID)OR sc.customerID IS NULL)
		  GROUP BY inv.TxnID
		ORDER BY inv.TimeCreated  DESC ";
	
		$query  =  $this->db->query($sql);
		if($query->num_rows() > 0){
			$res= $query->result_array(); 
		}
	
	   return $res;
	}
	public function get_invoice_list_count($userID, $customerID = null)
{

	$res =array();
 	$today  = date('Y-m-d');
  
	 $sql = "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , inv.Customer_ListID as ListID, inv.Customer_FullName as FullName,
	 ( case   when  IsPaid ='true'     then 'Success' 
	 when  IsPaid ='true' and userStatus ='Paid'     then 'Success'
	  when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
	 when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' limit 1 )='300'  then 'Failed'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Past Due'
	  
	 else 'Cancelled' end ) as status, 
	 ( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
	  ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
	 FROM qb_test_invoice inv
	
	 LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL )
	 WHERE inv.qb_inv_merchantID = '$userID'    
	  
	  AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL)  ";
	 $i   = 0;
	 $con = '';
	 $column1 = array('inv.Customer_FullName', 'inv.RefNumber', 'inv.AppliedAmount');
		foreach ($column1 as $item) {
  
			if ($_POST['search']['value']) {
  
				if ($i === 0) {
					$con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
				} else {
					$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
  
				}
  
			}
  
			$column[$i] = $item;
			$i++;
		}
  
		if ($con != '') {
			$con .= ' ) ';
			$sql .= $con;
		}
		if($customerID){
	  		$sql .= ' AND inv.Customer_ListID = "'.$customerID.'" ';	
	  	}
	   $sql .= " ORDER BY inv.TimeCreated  DESC ";
	  $query = $this->db->query($sql);
		$res = $query->num_rows();

   return $res;
	}
	public function get_invoice_data_by_status_count($userID,$status, $customerID = null)
{

	$res =array();
 	$today  = date('Y-m-d');
	$cond = "inv.IsPaid = 'false' AND userStatus!='cancel' ";
  
	if($status == 'Past Due'){
		$cond = "DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel'  AND ((select count(*) as sch_count from customer_transaction where invoiceTxnID = inv.TxnID AND merchantID = '$userID' limit 1) ='0' OR (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1) !='300')";
	}
	if($status == 'Failed'){
		$cond = "DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel' and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' limit 1 )='300'";
	}
	if($status == 'Paid'){
		$cond = "IsPaid ='true'";
	}
	if($status == 'Cancelled'){
		$cond = "userStatus='cancel'";
	}

	if($status == 'Partial'){
		$cond .= "AND inv.AppliedAmount != 0 AND inv.BalanceRemaining > 0";
	}

	if($status == 'Open'){
		$cond .= "AND inv.BalanceRemaining > 0";
	}

	$sql = "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , inv.Customer_ListID as ListID, inv.Customer_FullName as FullName,
	( case   when  IsPaid ='true'     then 'Success' 
	when  IsPaid ='true' and userStatus ='Paid'     then 'Success'
	 when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
	when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Upcoming' 
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' limit 1 )='300'  then 'Failed'
	 when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Past Due'
	 
	else 'Cancelled' end ) as status, 
	( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
	 ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
	FROM qb_test_invoice inv
	
	LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
	WHERE inv.qb_inv_merchantID = '$userID'   
	 
	 AND($cond)
	  
	 AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
	";
	 $i   = 0;
	 $con = '';
	 if($customerID){
  		$sql .= ' AND inv.Customer_ListID = "'.$customerID.'" ';	
  	}
	 $column1 = array('inv.Customer_FullName', 'inv.RefNumber', 'inv.AppliedAmount');
	  foreach ($column1 as $item) {

		  if ($_POST['search']['value']) {

			  if ($i === 0) {
				  $con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
			  } else {
				  $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

			  }

		  }

		  $column[$i] = $item;
		  $i++;
	  }

	  if ($con != '') {
		  $con .= ' ) ';
		  $sql .= $con;
	  }
	 $sql .= " ORDER BY inv.TimeCreated  DESC"; 
	$query  =  $this->db->query($sql);
		$res = $query->num_rows();
        

   return $res;
	}
	public function get_open_invoice_data_count($userID, $customerID = null){
		$res =array();
		$today  = date('Y-m-d');
	
		$sql  =  "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , inv.Customer_ListID as ListID, inv.Customer_FullName as FullName, 
		( case   when  IsPaid ='true'     then 'Success' 
		when  IsPaid ='true' and userStatus ='Paid'     then 'Success'
		when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Upcoming' 
			when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID AND merchantID = '$userID' limit 1 )='300'  then 'Failed'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Past Due'
		
		else 'Cancelled' end ) as status, 
		( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
		ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
		FROM qb_test_invoice inv
		LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID` AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
		WHERE inv.qb_inv_merchantID = '$userID'   
		AND (userStatus!='cancel' and inv.IsPaid = 'false')   
		AND DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' 
		AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
		";
		$i   = 0;
		$con = '';
		if($customerID){
	  		$sql .= ' AND inv.Customer_ListID = "'.$customerID.'" ';	
	  	}
		$column1 = array('inv.Customer_FullName', 'inv.RefNumber', 'inv.AppliedAmount');
			foreach ($column1 as $item) {
	  
				if ($_POST['search']['value']) {
	  
					if ($i === 0) {
						$con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
					} else {
						$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
	  
					}
	  
				}
	  
				$column[$i] = $item;
				$i++;
			}
	  
			if ($con != '') {
				$con .= ' ) ';
				$sql .= $con;
			}
		   $sql .= " ORDER BY inv.TimeCreated  DESC ";
		$query = $this->db->query($sql);
			$res = $query->num_rows(); 
			
			

	return $res;
	}
	public function get_product_list_count($userID){
		
		$result =array();
	    
		$this->db->select('qb_item.* ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
		$i   = 0;
		$con = '';
		$column1 = array('qb_item.Name','qb_item.SalesDesc','qb_item.FullName','qb_item.QuantityOnHand','qb_item.Parent_FullName', 'qb_item.Type' ); 
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
		$query = $this->db->get();
		$res = $query->num_rows();
       
       return $res;

	}
	public function get_paid_recent($userID){
		
		$res =array();

		$today = date("Y-m-d H:i");
		$res =array();
		$tcode =array('100','200', '120','111','1');
		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
	
		$this->db->select('(tr.transactionAmount) as balance,tr.invoiceTxnID,tr.transactionID,tr.id as TxnID, tr.transactionDate,  cust.ListID as Customer_ListID, cust.FullName, cust.companyName,(select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID AND qb_inv_merchantID = '.$userID.' limit 1) as RefNumber');
		
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		
	    $this->db->where("tr.merchantID ", $userID);
	     
		$this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	    $this->db->order_by("tr.id", 'desc');
		$this->db->group_by("tr.transactionID");
		$this->db->limit(10);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 	$res = $query->result_array();
		  
		}
		
		return $res;
	
	} 	
	public function get_invoice_data_pay($invoiceID)
	{
		$res = array();
	
		$this->db->select('inv.*, cust.*, cmp.merchantID, cmp.qbwc_username');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	    $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
	    $this->db->where('inv.TxnID' , $invoiceID);
	    $this->db->where('cust.customerStatus', '1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	} 

	public function get_echeck_payment($mID,$month)
	{   
	   
		 $res= 0.00;
		 $earn = 0.00;
		 $refund = 0.00;
		$refund1 = 0.00;
		$sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
			customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
			WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
			date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and  tr.gateway LIKE "%echeck%"  and (tr.transactionType ="Sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
			  or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
			  tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			  and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

		
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{    
		  $row = $query->row_array();
		  $earn = $row['volume'];
		 
		}

		$rfquery = $this->db->query('select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
			inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
			   date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway LIKE "%echeck%"  and 
			( UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "REFUND" )
			and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL  GROUP BY date_format(transactionDate, "%b-%Y") ');

		if($rfquery->num_rows()>0)
		{    
			$row = $rfquery->row_array();
			$refund = $row['refund'];

		}
		
        if($earn >= $refund){
        	$earn = $earn - $refund;
        }
        
        
		$res = $earn;
		return  $res;
			
	}
	public function get_subscription_revenue($userID, $subID)
	{
	
		$res =array();
		$today  = date('Y-m-d');
		

		$sql = "SELECT SUM(CASE WHEN inv.AppliedAmount < 0 THEN -inv.AppliedAmount ELSE 0 END)  + SUM(CASE WHEN inv.AppliedAmount>=0 THEN inv.AppliedAmount ELSE 0 END) as revenue
			FROM qb_test_invoice inv
			INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` AND cust.qbmerchantID = '$userID'
			INNER JOIN tbl_subscription_auto_invoices TSAI ON TSAI.invoiceID = inv.TxnID AND TSAI.subscriptionID = $subID AND app_type = 2
			LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID`
			WHERE  inv.qb_inv_merchantID = '$userID'  and  cust.customerStatus='1'    
			AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
			AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
			AND ((sc.customerID=cust.ListID and sc.merchantID=cust.qbmerchantID)OR sc.customerID IS NULL) 
		ORDER BY inv.TimeCreated  DESC";
	
		$query  =  $this->db->query($sql);
		if($query->num_rows() > 0){
			
			$res = $query->row_array(); 

			return $res['revenue'] / 2;
		}
		
		return 0;
	}			 

	public function getAutoRecurringInvoices(){
        $res = array();
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));
        $todayDate = date('d');
        $lastDay = date('t');

        $autoCondition = "( 
            ( QTI.DueDate = '$today' AND TRP.optionData = 1)
            OR ( QTI.TimeCreated LIKE '$today%' AND TRP.optionData = 2)
            OR ( QTI.TimeCreated LIKE '$yesterday%' AND TRP.optionData = 3)
            OR ( ( TRP.month_day = $todayDate OR ( TRP.month_day > '$lastDay' AND '$lastDay' = '$todayDate') ) AND TRP.optionData = 4)
        )";

        $sql = "SELECT QTI.*, QCC.companyID, QCC.FirstName, QCC.LastName, QCC.Contact as userEmail,QCC.companyName,QCC.Phone, QCC.ListID as Customer_ListID, QCC.FullName, TMG.gatewayUsername, TMG.gatewayPassword, TMG.extra_field_1, TMG.gatewaySignature,TMG.gatewayMerchantID, TMG.gatewayType, TRP.cardID, TRP.recurring_send_mail, TMG.gatewayID, tc.qbwc_username, tc.qbwc_password, TMD.resellerID, TMD.merchID
        FROM qb_test_invoice QTI
        INNER JOIN tbl_recurring_payment TRP ON TRP.merchantID = QTI.qb_inv_merchantID AND TRP.customerID = QTI.Customer_ListID
        INNER JOIN tbl_merchant_data TMD ON TMD.merchID = QTI.qb_inv_merchantID AND TMD.isSuspend = 0 AND TMD.isDelete = 0 AND TMD.isEnable = 1
        INNER JOIN qb_test_customer QCC ON QCC.qbmerchantID = QTI.qb_inv_merchantID AND QCC.ListID = QTI.Customer_ListID
        INNER JOIN tbl_merchant_gateway TMG ON QTI.qb_inv_merchantID = TMG.merchantID AND TMG.set_as_default = 1
        INNER JOIN tbl_company tc ON tc.id = QCC.companyID
        LEFT JOIN tbl_subscription_auto_invoices TSAI ON QTI.TxnID = TSAI.invoiceID AND TSAI.app_type = 2 AND TSAI.invoiceID = null
        WHERE QTI.BalanceRemaining <= TRP.amount AND QTI.BalanceRemaining > 0 AND ($autoCondition)
        GROUP BY QTI.id";
    
        $query = $this->db->query($sql);
           
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
   
    }	
  
	
}


