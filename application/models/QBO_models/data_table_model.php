<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Data_table_model extends CI_Model
{

/*--------------- All Talent List For Public Searching Page Start --------------- */ 
  
		var $table = 'QBO_custom_customer cus';
		var $column = array('firstName', 'lastName', 'fullName', 'userEmail', 'phoneNumber', 'Customer_ListID');
		var $order = array('firstName' => 'asc');


		
	  private function get_datatables_all_customers_list($mid, $st)
		{
			
       	 $this->db->select('firstName, lastName, fullName, userEmail, phoneNumber, Customer_ListID, (select sum(BalanceRemaining) 
		 from 	QBO_test_invoice where CustomerListID = cus.Customer_ListID) as Balance ');
	
		 $this->db->from($this->table);
		
        $this->db->where('cus.merchantID', $mid);
        if($st=='1')
        {
             $this->db->where('cus.customerStatus', 'true');
          }else{
                 $this->db->where('cus.customerStatus', 'false');
         }
      
	

        $i = 0;

        foreach ($this->column as $item) 
        {
            if($_POST['search']['value']){
            
             ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
             
			}
				$column[$i] = $item;
				$i++;
        }

        if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $order = $this->order;
            $this->db->order_by(key($order), 'asc');
        }
    }
		
		
		
   function get_datatables_customers_list($mid, $st)
    {
        $this->get_datatables_all_customers_list($mid, $st);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_all_customers($mid, $st)
    {
        
        $this->get_datatables_all_customers_list($mid, $st);
        $query = $this->db->get();
		
        $count=  $query->num_rows();
		 return $count;
    }

    public function count_all_customers($mid)
     {
        $this->db->from($this->table);
        $this->db->where('cus.merchantID', $mid);
        
	    return $this->db->count_all_results();
     }
  
  
  /*--------------- All Talent List For Public Searching End ----------------- */ 
  
} 