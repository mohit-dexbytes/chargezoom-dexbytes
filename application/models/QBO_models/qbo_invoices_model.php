<?php defined('BASEPATH') or exit('No direct script access allowed');

class Qbo_invoices_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public $table1 = 'QBO_test_invoice qb';
    public $order1 = array('BalanceRemaining' => 'desc');
    
    public $column1 = array('cs.fullName', 'qb.refNumber', 'qb.DueDate', 'qb.Total_payment', 'qb.BalanceRemaining', 'qb.invoiceID');

    /**************** Invoices ***************/

    private function _get_datatables_invoices_query($condition, $filter)
    {
        if($filter == ''){
            $filter = 'Current';
        }
        $statusFlagPartial = 'Partial';
        $statusFlagUnpaid = 'Open';
        if ($filter != 'All') {
            if ($filter == 'Partial') {
                $cond = 'qb.IsPaid="0" AND qb.UserStatus="0" AND qb.Total_payment != qb.BalanceRemaining';
            }
            if ($filter == 'Open') {
                $cond = 'qb.BalanceRemaining > 0 AND qb.IsPaid="0" AND qb.UserStatus="0"';
            }
            if ($filter == 'Current') {
                $cond = 'qb.DueDate >= CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"';
                $statusFlagPartial = 'Current';
                $statusFlagUnpaid = 'Current';
            }
            if ($filter == 'Past Due') {
                $cond = 'qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"';
                $statusFlagPartial = 'Overdue';
                $statusFlagUnpaid = 'Overdue';
            }
           
            if ($filter == 'Paid') {
                $cond = 'qb.IsPaid=' . "1" . ' and qb.UserStatus="0"';
            }
            if ($filter == 'Cancelled') {
                $cond = 'qb.UserStatus="1"';
            }
            if ($filter == 'Failed') {
                $cond = 'qb.IsPaid <> "1" and qb.UserStatus="0" AND qb.merchantID=' . $condition['qb.merchantID'] . ' AND tr.transactionID != "" and tr.merchantID=' . $condition['qb.merchantID'] . ' and (tr.transactionCode ="300")';
                $statusFlagPartial = 'Failed';
                $statusFlagUnpaid = 'Failed';
            }


            $this->db->select('tr.gateway as payment_gateway, qb.invoiceID,qb.id as ID,qb.refNumber,qb.CustomerListID, qb.DueDate,qb.Total_payment,qb.BalanceRemaining, qb.UserStatus, cs.fullName AS custname,
            qb.IsPaid,
			 (case when (sc.scheduleDate IS NOT NULL and qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Scheduled"
                when (qb.IsPaid="0" AND qb.UserStatus="0" AND qb.Total_payment != qb.BalanceRemaining ) THEN "'.$statusFlagPartial.'"
                when (qb.BalanceRemaining > 0 AND qb.IsPaid="0" AND qb.UserStatus="0") THEN "'.$statusFlagUnpaid.'"
                when (qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Open"
					when (qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"  ) THEN "Overdue"
					when (qb.merchantID=' . $condition['qb.merchantID'] . ' and tr.merchantID=' . $condition['qb.merchantID'] . ' and (tr.transactionCode ="300") AND qb.IsPaid !="1" ) THEN "Failed"
					when (qb.IsPaid="1" and qb.UserStatus="0" ) Then "Paid" ELSE "Voided" END) as status
			');
            $this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
            $this->db->from("QBO_test_invoice qb");

            $this->db->join('QBO_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
            $this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID AND tr.merchantID = qb.merchantID', 'left');

            $this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');
           
            $this->db->where('qb.merchantID', $condition['qb.merchantID']);
            $this->db->where('qb.isDeleted', 0);
            $this->db->where('cs.merchantID', $condition['qb.merchantID']);
           
            $this->db->where('qb.companyID', $condition['qb.companyID']);
            $this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL )
		and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL)
		and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) and ' . $cond . ' ');
        }
        if ($filter == 'All') {
            $this->db->select('qb.invoiceID,qb.id as ID,qb.refNumber,qb.CustomerListID, qb.DueDate,qb.Total_payment,qb.BalanceRemaining, qb.IsPaid, qb.UserStatus, cs.fullName AS custname,
			 (case when (sc.scheduleDate IS NOT NULL and qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Scheduled" when
				(qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Open"
					when (qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"  ) THEN "Overdue"
					when (qb.merchantID=' . $condition['qb.merchantID'] . ' and tr.merchantID=' . $condition['qb.merchantID'] . ' and (tr.transactionCode ="300")  AND qb.IsPaid !="1" ) THEN "Failed"
					when (qb.IsPaid="1" and qb.UserStatus="0" ) Then "Paid" ELSE "Voided" END) as status
			');
            $this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
            $this->db->from("QBO_test_invoice qb");

            $this->db->join('QBO_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
            $this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID ', 'left');

            $this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');
           
            $this->db->where('qb.merchantID', $condition['qb.merchantID']);
            $this->db->where('qb.isDeleted', 0);
            $this->db->where('cs.merchantID', $condition['qb.merchantID']);
          
            $this->db->where('qb.companyID', $condition['qb.companyID']);
            $this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL )
		and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL)
		and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');
        }

        $i   = 0;
        $con = '';

        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        if($customerID){
            $this->db->where('qb.CustomerListID', $customerID);
        }

        foreach ($this->column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $column = getTableRowOrderColumnValue($_POST['order'][0]['column']);
        if($column == 0){
            $orderName = 'cs.fullName';
        }elseif($column == 1){
            $orderName = 'cs.fullName';
        }elseif($column == 2){
            $orderName = 'qb.refNumber';
        }elseif($column == 3){
            $orderName = 'qb.DueDate';
        }elseif($column == 4){
            $orderName = 'qb.BalanceRemaining';
        }elseif($column == 5){
            $orderName = 'status';
        }else{
            $orderName = 'qb.DueDate';    
        }
        $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);

        $this->db->order_by($orderName, $orderBY);
       

        $this->db->group_by('qb.invoiceID');
    }

    private function _get_datatables_invoices_query11($condition)
    {
        $this->db->select('qb.invoiceID,qb.refNumber,qb.CustomerListID, qb.DueDate,qb.Total_payment,qb.BalanceRemaining, qb.UserStatus, cs.fullName AS custname,
		     (case when (sc.scheduleDate IS NOT NULL and qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Scheduled" when
                (qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Open"
                    when (qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"  ) THEN "Overdue"
                    when (qb.IsPaid="1" and qb.UserStatus="0" ) Then "Paid" ELSE "Cancelled" END) as status
            ');
        $this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
        $this->db->from("QBO_test_invoice qb");

        $this->db->join('QBO_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
        $this->db->join('tbl_scheduled_invoice_payment sc', 'qb.invoiceID = sc.invoiceID', 'left');

        $this->db->where('qb.merchantID', $condition['qb.merchantID']);
        $this->db->where('cs.merchantID', $condition['qb.merchantID']);

        $this->db->where('qb.companyID', $condition['qb.companyID']);
        $this->db->where('( sc.merchantID ="' . $condition['qb.merchantID'] . '" or  sc.merchantID IS NULL )
	    and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL)
	    and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');

        $i   = 0;
        $con = '';
        foreach ($this->column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        } else {
            $order = $this->order1;
            $this->db->order_by(key($order), 'desc');
        }

    }

    public function get_datatables_invoices($condition, $filter)
    {

        $this->_get_datatables_invoices_query($condition, $filter);
        $postLength = getTableRowLengthValue($_POST['length']);
        $startRow = getTableRowStartValue($_POST['start']);
        if ($postLength != -1) {
            $this->db->limit($postLength, $startRow);
        }

        $query = $this->db->get();
        $data = array('result' => $query->result());
        return $data;
    }

    public function count_all_invoices($condition,$filter)
    { 
        $this->db->select('qb.id');

        
        $this->db->from("QBO_test_invoice qb");

        $this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID AND tr.merchantID = qb.merchantID', 'left');
        $this->db->join('QBO_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
        $this->db->where('qb.merchantID', $condition['qb.merchantID']);
        $this->db->where('qb.isDeleted', 0);
        $this->db->where('cs.merchantID', $condition['qb.merchantID']);
        
        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        if($customerID){
            $this->db->where('qb.CustomerListID', $customerID);
        }

        $this->db->where('qb.companyID', $condition['qb.companyID']);
    
    $i   = 0;
        $con = '';
        foreach ($this->column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        if($filter == ''){
            $filter = 'Current';
        }
        if ($filter != 'All') {
            if ($filter == 'Current') {
                $this->db->where('qb.IsPaid', 0);
                $this->db->where('qb.UserStatus', 0);
                $this->db->where('qb.DueDate >= CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"');
            }
            if ($filter == 'Past Due') {
                $this->db->where('qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"');
               
            }
            if ($filter == 'Open') {
                $this->db->where('qb.BalanceRemaining >', 0);
                $this->db->where('qb.IsPaid', 0);
                $this->db->where('qb.UserStatus', 0);
            }
           
            if ($filter == 'Paid') {
                $this->db->where('qb.IsPaid', 1);
                $this->db->where('qb.UserStatus', 0);
            }
            if ($filter == 'Cancelled') {
                $this->db->where('qb.UserStatus', 1);
            }
            if ($filter == 'Failed') {
                $this->db->where('qb.merchantID', $condition['qb.merchantID']);
                $this->db->where('tr.merchantID', $condition['qb.merchantID']);
                $this->db->where('tr.transactionID != ""');
                $this->db->where('tr.transactionCode', '300');
                
            }
        }
        $this->db->group_by('qb.invoiceID');
        
        $query = $this->db->get();
        $count = $query->num_rows();
       
        return $count;
    }

    public function count_filtered_invoices($condition)
    {

        $this->db->select('qb.invoiceID,qb.DueDate,qb.BalanceRemaining, cs.fullName AS custname,
		     (case when (sc.scheduleDate!="0000-00-00" and qb.IsPaid="0") THEN "Scheduled" when
                (qb.DueDate >= CURDATE() AND qb.IsPaid="0") THEN "Open"
                    when (qb.DueDate < CURDATE() and qb.IsPaid="0") THEN "Overdue" when (qb.BalanceRemaining="0" and qb.IsPaid="1") Then "Paid" ELSE "TEST" END) as status
            ');
        $this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
        $this->db->from("QBO_test_invoice qb");

        $this->db->join('QBO_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
        $this->db->join('tbl_scheduled_invoice_payment sc', 'qb.invoiceID = sc.invoiceID', 'left');

        $this->db->where('qb.merchantID', $condition['qb.merchantID']);
        $this->db->where('cs.merchantID', $condition['qb.merchantID']);

        $this->db->where('qb.companyID', $condition['qb.companyID']);
        $i = 0;

        foreach ($this->column1 as $item) {

            if ($_POST['search']['value']) {

                ($i === 0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);

            }

            $column[$i] = $item;
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        } else {
            $order = $this->order1;
            $this->db->order_by(key($order), 'desc');
        }

        $query = $this->db->get();
        return $query->num_rows();

    }

    /********************** End ****************/

/*------------------------------------------------------ Payment Transaction Page For QBO Start ----------------------------------------------------------------- */

    public $table  = 'customer_transaction tr';
    public $order  = array('tr.transactionDate' => 'desc');
    public $column = array('tr.invoiceID', 'tr.transactionAmount', 'tr.transactionDate', 'tr.transactionType', 'tr.transactionCode', 'tr.transactionID', 'tr.id', 'cust.Customer_ListID', 'cust.fullName');

    private function _get_datatables_transaction_query($userID)
    {
        $this->db->select('tr.invoiceID,tr.transactionAmount,tr.transactionDate,tr.transactionType,tr.transactionCode,tr.transactionID, tr.id,  cust.Customer_ListID,  cust.fullName ');
        $this->db->select('ifnull(sum(r.refundAmount),0) as partial', false);

        $this->db->from("customer_transaction tr");

        $this->db->join('QBO_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_customer_refund_transaction r', 'tr.transactionID=r.creditTransactionID', 'left');

        $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("tr.transactionID", 'desc');

        $i = 0;

        foreach ($this->column as $item) {

            if ($_POST['search']['value']) {

                ($i === 0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);

            }

            $column[$i] = $item;
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), 'desc');
        }

    }

    public function get_datatables_transaction($userID)
    {

        $res = array();

        $this->_get_datatables_transaction_query($userID);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $res = $query->result();

        }

        return $res;

    }

    public function count_all_transaction($userID)
    {

        $this->db->select('tr.invoiceID,tr.transactionAmount,tr.transactionDate,tr.transactionType,tr.transactionCode,tr.transactionID,tr.id,  cust.Customer_ListID,  cust.fullName ');
        $this->db->select('ifnull(sum(r.refundAmount),0) as partial', false);

        $this->db->from("customer_transaction tr");

        $this->db->join('QBO_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_customer_refund_transaction r', 'tr.transactionID=r.creditTransactionID', 'left');

        $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("tr.transactionID", 'desc');
        $query = $this->db->get();

        $count = $query->num_rows();

        return $count;
    }

    public function count_filtered_transaction($userID)
    {
        $this->db->select('tr.invoiceID,tr.transactionAmount,tr.transactionDate,tr.transactionType,tr.transactionCode,tr.transactionID, tr.id,  cust.Customer_ListID,  cust.fullName ');
        $this->db->select('ifnull(sum(r.refundAmount),0) as partial', false);

        $this->db->from("customer_transaction tr");

        $this->db->join('QBO_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_customer_refund_transaction r', 'tr.transactionID=r.creditTransactionID', 'left');

        $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("tr.transactionID", 'desc');

        $i = 0;

        foreach ($this->column as $item) {

            if ($_POST['search']['value']) {

                ($i === 0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);

            }

            $column[$i] = $item;
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), 'desc');
        }

        $query = $this->db->get();
        return $query->num_rows();

    }

    public function get_item_data($con)
    {
        $res = array();
        $this->db->select(' *, (select Name from QBO_test_item where productID =tbl_qbo_invoice_item.itemRefID and  merchantID="' . $con['merchantID'] . '")as Name,
       (select SalesDescription from QBO_test_item where productID =tbl_qbo_invoice_item.itemRefID and  merchantID="' . $con['merchantID'] . '")as itemDescription  ');
        $this->db->from('tbl_qbo_invoice_item');
      
        $this->db->where($con);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_item_data_new($con)
    {
        $res = array();
        $this->db->select(' *, (select Name from QBO_test_item where productID =tbl_qbo_invoice_item.itemRefID and  merchantID="' . $con['merchantID'] . '")as Name,
            (select Type from QBO_test_item where productID =tbl_qbo_invoice_item.itemRefID and  merchantID="' . $con['merchantID'] . '")as Type,
       (select itemDescription from QBO_test_item where productID =tbl_qbo_invoice_item.itemRefID and  merchantID="' . $con['merchantID'] . '")');
        $this->db->from('tbl_qbo_invoice_item');
        
        $this->db->where($con);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }

}
