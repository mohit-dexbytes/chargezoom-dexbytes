<?php


Class Qbo_customer_model extends CI_Model
{
	public function get_customer_invoice_data_sum($custID, $uid){
    
    
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,   sum(inv.BalanceRemaining) as amount FROM QBO_test_invoice inv  WHERE `inv`.`CustomerListID` = '".$custID."'   and inv.merchantID='".$uid."' ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
	
    public function customer_by_id($id, $uid) 
	{
        $this->db->select('cus.*');
        $this->db->select('IFNULL(loginAt,"") as loginAt',FALSE);
		$this->db->from('QBO_custom_customer cus');
	    $this->db->join('tbl_merchant_data mr','mr.merchID = cus.merchantID','inner');
	    $this->db->join('tbl_customer_login cr','cr.customerID=cus.Customer_ListID','left');
		$this->db->where('cus.Customer_ListID', $id);
        $this->db->where('cus.merchantID', $uid);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
	}
	
	public function get_invoice_upcomming_data($userID, $mid)
	{
	
	      $today=date('Y-m-d');
       $res=array();
      $query=   $this->db->query("SELECT `qb`.*, `cs`.`fullName` AS custname, (case   when (sc.scheduleDate IS NOT NULL and IsPaid='0')
       THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'
      AND IsPaid='0') THEN 'Open' 
      when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and IsPaid='0')  THEN 'Overdue'
      when (qb.BalanceRemaining='0' and IsPaid='1') Then 'Paid'
      ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
       FROM (`QBO_test_invoice` qb) 
       INNER JOIN `QBO_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID` 
       LEFT JOIN `tbl_scheduled_invoice_payment` sc ON  (`sc`.`invoiceID`=`qb`.`invoiceID` and sc.merchantID='".$mid."') 
       WHERE `qb`.`merchantID` = '".$mid."' AND `cs`.`merchantID` = '".$mid."'  and  `qb`.`CustomerListID` = '".$userID."' and qb.isDeleted = 0 
       AND `cs`.`Customer_ListID` = '".$userID."'  and IsPaid='0' and qb.UserStatus !='1'
        AND (sc.merchantID=  '".$mid."' OR sc.merchantID IS NULL)
       AND (sc.customerID=qb.CustomerListID OR sc.customerID IS NULL) 
        AND ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID)OR sc.customerID IS NULL)
		Group by `qb`.`invoiceID`
       ");
       	if($query->num_rows() > 0){
		
		  $res= $query->result_array();
		}
		
		return $res;
	}


    	public function get_invoice_details($uid)
    	{
    
    
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,ifnull(  ( Select count(*)  FROM QBO_test_invoice where  merchantID=$uid  and BalanceRemaining=0 ),0) as Paid,
           ifnull( (select count(*) 
           from QBO_test_invoice qt 
           inner join 	tbl_scheduled_invoice_payment sc on sc.invoiceID=qt.invoiceID and sc.merchantID=qt.merchantID
           where  sc.scheduleDate IS NOT NULL and  sc.customerID=qt.CustomerListID and qt.merchantID='$uid' and  sc.merchantID=$uid and qt.BalanceRemaining > 0 ),'0') as schedule, 
           ifnull( (select count(*) 
           from QBO_test_invoice 
           inner join customer_transaction tr on tr.invoiceID=QBO_test_invoice.invoiceID AND tr.merchantID = $uid 
           where  QBO_test_invoice.merchantID=$uid and tr.merchantID=$uid and (tr.transactionCode ='300') AND QBO_test_invoice.isDeleted = 0 ),'0') as Failed
            FROM QBO_test_invoice inv  
            WHERE   inv.merchantID='".$uid."' AND inv.isDeleted = 0 ");
            
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
	
	public function get_invoice_latest_data($userID, $mid)
	{

	$res =array();
 	
 	$today  = date('Y-m-d');
    
                                            
 		$query  =  $this->db->query("SELECT qb.* 
 		FROM QBO_test_invoice qb  INNER JOIN `QBO_custom_customer` cs ON `cs`.`Customer_ListID`=`qb`.`CustomerListID`   WHERE (BalanceRemaining = 0 or  BalanceRemaining!= Total_payment) AND CustomerListID = '".$userID."' and UserStatus!='1'   
 		and qb.merchantID= '".$mid."'  and cs.Customer_ListID='".$userID."'  and cs.merchantID = '".$mid."'  ");  

	 	if($query->num_rows() > 0){
			  return  $res=$query->result_array();
			}
	   
	    return  $res;
	    
 
 	}
 	
 	public function get_customer_invoice_data_payment($custID, $mid){
$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  ifnull(sum(Total_payment),0) as applied_amount, ifnull(sum(Total_payment-BalanceRemaining),0) as applied_amount1,  
		  ifnull( (select sum(BalanceRemaining) from QBO_test_invoice where `CustomerListID` = '".$custID."' and UserStatus!='1'   and (BalanceRemaining!=0  and Total_payment!=BalanceRemaining) and merchantID=$mid ),'0')as applied_due, 
ifnull( (select sum(BalanceRemaining) from QBO_test_invoice where UserStatus!='1' and  `CustomerListID` = '".$custID."'  and merchantID=$mid  ),'0')as remaining_amount , 
		  ifnull( (select sum(BalanceRemaining) from QBO_test_invoice where  UserStatus!='1'  and   `CustomerListID` = '".$custID."'  and merchantID=$mid  ),'0') as upcoming_balance FROM QBO_test_invoice   WHERE UserStatus!='1' and  `CustomerListID` = '".$custID."'  and merchantID=$mid    ");
		 $res=$query->row();
		 
		 return  $res;
		
	}
	
	 public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
    public function get_all_customer_details($mid, $st){
        $res=array();
        $this->db->select('cus.*, (select sum(BalanceRemaining)  from 	QBO_test_invoice where CustomerListID = cus.Customer_ListID and merchantID="'.$mid.'" and userStatus!="1") as Balance ');
	   	$this->db->from('QBO_custom_customer cus ');
		$this->db->where('cus.merchantID', $mid);
	
        if($st=='1')
        {
        $this->db->where('cus.customerStatus', 'true');
         }else{
        $this->db->where('cus.customerStatus', 'false');
		 }
		 $column1 = array('cus.fullName', 'cus.firstName','cus.lastName','cus.userEmail','cus.phoneNumber');
		 $i   = 0;
        $con = '';
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $column = getTableRowOrderColumnValue($_POST['order'][0]['column']);
	    if($column == 0){
	        $orderName = 'cus.fullName';
	    }elseif($column == 1){
	        $orderName = 'cus.firstName';
	    }elseif($column == 2){
	        $orderName = 'cus.userEmail';
	    }elseif($column == 3){
	        $orderName = 'cus.phoneNumber';
	    }elseif($column == 4){
	        $orderName = 'Balance';
	    }else{
	        $orderName = 'cust.firstName'; 
	    }
	    $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);

	    $postLength = getTableRowLengthValue($_POST['length']);
	    $startRow = getTableRowStartValue($_POST['start']);

		if($postLength != -1)
        $this->db->limit($postLength, $startRow);
		$query = $this->db->order_by($orderName, $orderBY)->get();
		if($query -> num_rows() > 0){
			$data = array('num' => '10000', 'result' => $query->result_array());
		}else{
			$data = array('num' => '0', 'result' => $query->result_array());
		}
			
		 
		  
        	return 	$data; 
	}
	public function get_all_customer_count($mid, $st='1'){
		$res=array();
        $this->db->select('*');
	   	$this->db->from('QBO_custom_customer cus ');
		$this->db->where('cus.merchantID', $mid);
		$this->db->where('cus.customerStatus', 'true');
        if($st=='1')
        {
        $this->db->where('cus.customerStatus', 'true');
         }else{
        $this->db->where('cus.customerStatus', 'false');
		 }
		 $column1 = array('cus.fullName', 'cus.firstName','cus.lastName','cus.userEmail','cus.phoneNumber');
		 $i   = 0;
        $con = '';
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
		$query = $this->db->get();
	
 $cnt = $query->num_rows();
return $cnt;
 		
	}
	
    public function customer_details($custID, $mid){
        
         $this->db->select('*');
		
		$this->db->from('QBO_custom_customer cus');
		$this->db->where('Customer_ListID', $custID);
        $this->db->where('cus.merchantID', $mid);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
    }
	

function get_customers_data($merchantID) 
	{
     
        $this->db->select('*');
        $this->db->from('QBO_custom_customer') ;
        $this->db->where('merchantID', trim($merchantID));
        $this->db->where('customerStatus','true');
        $this->db->order_by('fullName','asc');
        $this->db->order_by('companyName','asc');

		$query = $this->db->get();
		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}	


	public function get_transaction_data_refund($userID){
		$today = date("Y-m-d H:i");
		 $res=array();
        $query =$this->db->query("Select tr.*, tr.invoiceID, DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, 
        (select refNumber from QBO_test_invoice where invoiceID = tr.invoiceID and merchantID=$userID LIMIT 1) as refNumber , cust.* from customer_transaction tr 
		inner join  QBO_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."' 
		and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='pay-sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture' or tr.transactionType ='sale') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
		and ((tr.transaction_user_status !='3' and tr.transaction_user_status !='2')  or  tr.transaction_user_status  IS NULL)
		and  tr.gateway NOT IN ('AUTH ECheck','NMI ECheck', 'iTransact ECheck') and tr.transactionID!='' GROUP BY tr.transactionID DESC");

		if($query->num_rows() > 0){
		
		 $res= $query->result_array();
		}
		return $res;
	}
	
	public function get_transaction_data_captue($userID){
		
     	$today = date("Y-m-d H:i");
		 $res=array();
		 


		$type = array('AUTH', 'PAYPAL_AUTH','PAY_AUTH','AUTH_ONLY','STRIPE_AUTH');
		$tcode = array('100','200','100','111','1');
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,cust.Customer_ListID,  cust.fullName');
	
	   $this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','cust.Customer_ListID=tr.customerListID','inner');
		$this->db->where('cust.merchantID', $userID);
		
		$this->db->where('tr.merchantID', $userID);
		$this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
         $this->db->where_in('tr.transaction_user_status',5);
	    $this->db->order_by("tr.transactionDate", 'desc');
	
	    $this->db->group_by("tr.transactionID");
		$query = $this->db->get();  	

		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	
	public function get_transaction_data_erefund($userID){
		$today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from QBO_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
       
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('QBO_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        
        $this->db->where("cust.merchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->like("tr.gateway", 'ECheck');

        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
       
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();

            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber   from   QBO_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                       
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }

                 
                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');


                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;
                        
                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }

       
        return $res;
	}
	
	
	public function get_transaction_data_ecaptue($userID){
		
		$query =$this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  QBO_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
	 	and (tr.gateway='NMI ECheck'  or tr.gateway='AUTH ECheck')  and (tr.transaction_user_status ='1') 
		  ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	
	public function get_subscriptions_data($userID){
					
		$this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	    $this->db->group_by("sbs.subscriptionID");
	    $this->db->order_by("sbs.createdAt", 'desc');
	  
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}

	public function get_subscriptions_plan_data($userID){
        $cardDb = getenv('DB_DATABASE2');	

	    $this->db->select('sbs.planID,sbs.subscriptionID,sbs.startDate,sbs.subscriptionAmount, sbs.customerID,sbs.nextGeneratingDate, sbs.merchantDataID,cust.fullName, cust.companyName, tmg.*,  spl.planName as sub_planName, c.customerCardfriendlyName');
	    
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');

		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
		$this->db->join('tbl_subscriptions_plan_qbo spl','spl.planID = sbs.planID','left');
		 $this->db->join("$cardDb.customer_card_data c",'c.CardID=sbs.cardID','left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	     $this->db->where("cust.merchantID ", $userID);
	    $this->db->group_by("sbs.subscriptionID");
	    $this->db->order_by("sbs.createdAt", 'desc');
	  
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}

		
	}
	
		
	public function get_total_subscription($merchID){   
	   $res = array();	  
       $today = date("Y-m-d H:i");
	   $next_due_date = date('Y-m', strtotime("+30 days"));
	   
	   
	    $this->db->select('*');
        $this->db->from('tbl_subscriptions_qbo sbs');
        $this->db->join('QBO_custom_customer cust','sbs.customerID=cust.Customer_ListID AND cust.merchantID = sbs.merchantDataID','inner');
        $this->db->where('cust.merchantID',$merchID);
        $this->db->where('sbs.subscriptionStatus',"1");
        $this->db->where('cust.customerStatus',"true");
        $this->db->where('DATE_FORMAT(sbs.startDate, "%Y-%m")>=',date("Y-m"));
      
        $this->db->group_by('sbs.customerID');
        $query = $this->db->get();
         if($query->num_rows() > 0)
         {
		
		    $res['total_subcription'] = $query->num_rows();
		}
		else
		{
			$res['total_subcription'] =0;
		}
	   
	   
	  
      $query = $this->db->query(" SELECT count(*) as sub_count  FROM (`tbl_subscriptions_qbo` sbs) 
	   INNER JOIN `QBO_custom_customer` cust ON `sbs`.`customerID` = `cust`.`Customer_ListID` AND cust.merchantID = sbs.merchantDataID
	 
	  WHERE `cust`.`merchantID` = '".$merchID."' AND `cust`.`customerStatus` = 'true' 
	  and sbs.subscriptionStatus='1' and DATE_FORMAT(startDate, '%Y-%m')>= '".date('Y-m')."'   ");
	  if($query->num_rows() > 0){
		
		    $res['active_subcription'] = $query->row_array()['sub_count'];
		}else{
			$res['active_subcription'] =0;
		}
        
        $today_date         = date("Y-m-d");
        $next_7_date = date('Y-m-d', strtotime("+7 days"));
        $next_1_month = date('Y-m-d', strtotime("+1 month"));
		
		$query1 = $this->db->query("SELECT
				COUNT(1) AS exp_count
			FROM
			tbl_subscriptions_qbo sbs
			INNER JOIN QBO_custom_customer cust ON
				sbs.customerID = cust.Customer_ListID AND sbs.merchantDataID = cust.merchantID
			WHERE
				sbs.merchantDataID = '$merchID' AND cust.customerStatus = 'true' AND subscriptionPlan != '0' AND totalInvoice > '0' AND sbs.subscriptionStatus = '1' AND (
					( sbs.endDate BETWEEN '$today_date' AND  '$next_1_month' AND invoicefrequency IN ('mon', '2mn', 'qtr', 'six', 'yrl', '2yr', '3yr'))
					OR
					( sbs.endDate BETWEEN '$today_date' AND  '$next_7_date' AND invoicefrequency IN ('dly', '1wk', '2wk'))
		)");
	 
	   if($query1->num_rows() > 0){
		
		    $res['exp_subcription'] = $query1->row_array()['exp_count'];
		}else{
			$res['exp_subcription'] =0;
		}
		
		$query1 = $this->db->query("SELECT
				COUNT(distinct(sbs.subscriptionID)) AS failed_count
			FROM
				QBO_test_invoice inv
			INNER JOIN QBO_custom_customer cust ON
				inv.CustomerListID = cust.Customer_ListID AND cust.merchantID = '$merchID'
			INNER JOIN tbl_subscription_auto_invoices TSAI ON
				TSAI.invoiceID = inv.invoiceID AND app_type = 1
			INNER JOIN customer_transaction CTR ON
				CTR.invoiceID = inv.invoiceID AND transactionCode = 300  AND CTR.merchantID = '$merchID' 
			INNER JOIN tbl_subscriptions_qbo sbs ON TSAI.subscriptionID = sbs.subscriptionID
			WHERE
				inv.merchantID = '$merchID' AND sbs.merchantDataID = '$merchID' AND cust.customerStatus = 'true' AND inv.IsPaid = '0' AND inv.userStatus ='0' GROUP BY sbs.subscriptionID");
	 
	   	if($query1->num_rows() > 0){
		    $res['failed_count'] = $query1->row_array()['failed_count'];
		}else{
			$res['failed_count'] =0;
		}
		
		return  $res;
		
   }
	

	
	
		public function get_cust_subscriptions_data($userID){
	
			$cardDb = getenv('DB_DATABASE2');	
					
		$this->db->select('sbs.*, cust.fullName,pl.planName as sub_planName, tmg.*, c.customerCardfriendlyName');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
    	$this->db->join('tbl_subscriptions_plan_qbo pl','pl.planID = sbs.planID','left');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
		$this->db->join("$cardDb.customer_card_data c",'c.CardID=sbs.cardID','inner');
	    $this->db->where($userID);
	    $this->db->group_by("sbs.subscriptionID");
	    $this->db->order_by("sbs.createdAt", 'desc');
	  
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	public function get_transaction_history_data($userID, $customerID = null)
	{
		$today = date("Y-m-d H:i");
		$res=array();
		$tcode =array('100','200', '120','111','1');

		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY-SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND','CREDIT');
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,  cust.Customer_ListID,  cust.fullName');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->where_in('UPPER(tr.transactionType)',$type);
       $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	    $this->db->where("tr.transactionID >", 0);
	   $this->db->group_by("tr.transactionID", 'desc');
		if($customerID){
        	$this->db->where("tr.customerListID", $customerID);
		}
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 
	   
		
		 foreach( $res1 as $result)
		 {
		     if(!empty($result['invoice_id']))
		     {
		        $result['invoice_no'] = '';
				$inv =$result['invoice_id'];
				$invList = explode(',', $inv);

				foreach($invList as $key1 => $value) {
					$value = trim($value);
					if(empty($value) || $value == ''){
						unset($invList[$key1]);	
					}
					$invList[$key1] = "'".$value."'";
				}

				$inv = implode(',', $invList); 
				$res_inv=array();
		     
		     $qq= $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  
		     QBO_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");
		     
		    
		      if( $qq->num_rows >0){
		       $res_inv= $qq->row_array()['invoce']; 
		    
		    $result['invoice_no'] =$res_inv;
		  
		      }
		    
		     }
		     $ref_amt=0;
	
		     if($result['transactionID']!="" && strtoupper($result['transactionType'])!=strtoupper('Offline Payment') )
		     {
		     $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$result['transactionID'].'"  group by creditTransactionID  ');
	
		    $data =  $qr->row_array();
		   
		       if(!empty($data)){
		         $result['partial']= $data['refundAmount']; 
		       }
		       else {
                    $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                    $result['partial'] = $ref_amt;
                    
                }
		      }
		      else
		     {
		          $result['partial'] =$ref_amt;
		     }
		     
		     
		     $res[] =$result;
		 } 
		}
	
	
		return $res;
		
	}

	public function get_transaction_data($userID)
	{
		$today = date("Y-m-d H:i");
		$res=array();
		$tcode =array('100','200', '120','111','1');
		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY-SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,  cust.Customer_ListID,  cust.fullName');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->where_in('UPPER(tr.transactionType)',$type);
       $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	   $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 
	   
		
		 foreach( $res1 as $result)
		 {
		     if(!empty($result['invoice_id']))
		     {
		         
				$inv =$result['invoice_id'];
				$invList = explode(',', $inv);

				foreach($invList as $key1 => $value) {
					$value = trim($value);
					if(empty($value) || $value == ''){
						unset($invList[$key1]);	
					}
					$invList[$key1] = "'".$value."'";
				}

				$inv = implode(',', $invList); 
				$res_inv=array();
		     
		     $qq= $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  
		     QBO_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");
		     
		    
		      if( $qq->num_rows >0){
		       $res_inv= $qq->row_array()['invoce']; 
		    
		    $result['invoice_no'] =$res_inv;
		  
		      }
		    
		     }
		     $ref_amt=0;
	
		     if($result['transactionID']!="" && strtoupper($result['transactionType'])!=strtoupper('Offline Payment') )
		     {
		     $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$result['transactionID'].'"  group by creditTransactionID  ');
	
		    $data =  $qr->row_array();
		   
		       if(!empty($data))
		         $result['partial']= $data['refundAmount']; 
		       else
		    
		      $result['partial'] =$ref_amt;
		      }
		      else
		     {
		          $result['partial'] =$ref_amt;
		     }
		     
		     
		     $res[] =$result;
		 } 
		}
	
	
		return $res;
		
	}
	
		public function get_refund_transaction_data($userID){
	
		$query =	$this->db->query ("SELECT `tr`.*, `cust`.* FROM (`customer_transaction` tr) 
	INNER JOIN `QBO_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID`
	WHERE `tr`.`merchantID` = '$userID' and cust.merchantID= '$userID'   
	AND (`tr`.`transactionType` = 'refund' or `tr`.`transactionType` = 'stripe_refund' OR `tr`.`transactionType` = 'usaepay_refund' OR `tr`.`transactionType` = 'pay_refund' OR `tr`.`transactionType` = 'paypal_refund' OR `tr`.`transactionType` = 'credit' ) ORDER BY `tr`.`transactionDate` desc") ;
	
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 


	public function get_credit_user_data($c_id)
    {
		$res = array();
		$this->db->select('cr.*, cust.companyName, cust.fullName');
		$this->db->from('qbo_customer_credit cr');
		$this->db->join('QBO_custom_customer cust','cr.CustomerListID = cust.Customer_ListID','INNER');
	
	    $this->db->where('cr.merchantID',$c_id);
    	$this->db->where('cust.merchantID', $c_id);
	
		$this->db->order_by('cr.TimeModified','desc');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 	

	  public function get_customer_note_data($custID,$mID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `QBO_custom_customer` cust ON `pr`.`customerID` 
	  = `cust`.`Customer_ListID` WHERE `pr`.`customerID` = '".$custID."' and  pr.merchantID = '".$mID."'  group  by pr.noteID desc");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 
	
    	function get_billing_data($merchID) 
	{
		 $sql = ' SELECT * from tbl_merchant_billing_invoice where merchantID = "'.$merchID.'" ';
		 

		$query = $this->db->query($sql);
		
		if($query->num_rows()>0 ) {
				return $query->result_array();
			   
			} else {
				return false;
			}	

		
	}
	
	 public function get_merchant_billing_data($invID){
		   $res = array();
		   $query = $this->db->query("SELECT bl.*, mr.*, ct.city_name, st.state_name, c.country_name FROM (tbl_merchant_billing_invoice bl) INNER JOIN tbl_merchant_data mr ON mr.merchID=bl.merchantID LEFT JOIN state st ON st.state_id=mr.merchantState LEFT JOIN country c ON c.country_id=mr.merchantCountry LEFT JOIN city ct ON ct.city_id=mr.merchantCity WHERE bl.merchant_invoiceID = '$invID' AND bl.status = 'pending'");
		   
		   if($query->num_rows()>0 ) {
				$res = $query->row_array();
			   return $res;
			} else {
				return $res;
			}
		
		   
	   }
   
   

    public function get_invoice_transaction_data($invoiceID, $userID,$action)
	   {
		 $res=array();
		$this->db->select('tr.id, tr.transactionID,sum(tr.transactionAmount) as transactionAmount,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transactionType, tr.transaction_user_status, cust.fullName as FullName, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID');
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("cust.merchantID ", $userID);
	    if($action=='invoice')
		$this->db->where('FIND_IN_SET("'.$invoiceID.'", tr.invoiceID) ');
		if($action=='transaction')
		$this->db->where("tr.transactionID ", $invoiceID);
		$this->db->where("tr.merchantID ", $userID);
		$this->db->group_by('transactionID');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		   $res= $query->result_array();
		}
		
		return  $res;
		
	} 		
	
   public function get_invoice_list_data($condition)
   {
       $today=date('Y-m-d');
       $res=array();
    $query=   $this->db->query("SELECT `qb`.*, `cs`.`fullName` AS custname, (case   when (sc.scheduleDate!='0000-00-00' and IsPaid='0')
       THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'
      AND IsPaid='0') THEN 'Open' 
      when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and IsPaid='0')  THEN 'Overdue'
      when (qb.BalanceRemaining='0' and IsPaid='1') Then 'Paid'
      ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate 
       FROM (`QBO_test_invoice` qb) 
       INNER JOIN `QBO_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID` 
       LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `qb`.`invoiceID`=`sc`.`invoiceID`
       WHERE qb.merchantID = '".$condition['qb.merchantID']."' AND `cs`.`merchantID` = '".$condition['qb.merchantID']."' 
       AND `qb`.`companyID` = '".$condition['qb.companyID']."' AND (sc.merchantID=  '".$condition['qb.merchantID']."' OR sc.merchantID IS NULL)
       AND (sc.customerID=qb.CustomerListID OR sc.customerID IS NULL) 
        AND ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID)OR sc.customerID IS NULL)
       ");
       	if($query->num_rows() > 0){
		
		  $res= $query->result_array();
		}
		
		return $res;
   }

   
   
	public function get_transaction_details_data($con)
	{
		$today = date("Y-m-d H:i");
			$res =array();
			$this->db->select('sum(tr.transactionAmount) as transactionAmount ');
			$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
		$this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
	
	    $this->db->where($con);
	    $this->db->order_by("tr.transactionDate", 'desc');
		  $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->row_array();
	     $res['transactionAmount'] = ($res1['transactionAmount']- $res1['partial']);
		}
		return $res;
		
	} 		
	
	
	public function get_invoice_item_data($con)
	{
	 $res  = array();
	$query =  $this->db->query(" SELECT itm.invoiceID, itm.itemRefID as itemID,ifnull(it.AssetAccountRef,'')as AssetAccountRef ,ifnull(it.IncomeAccountRef,'')as IncomeAccountRef,ifnull(it.ExpenseAccountRef,'') as ExpenseAccountRef  from tbl_qbo_invoice_item itm inner join QBO_test_item it on it.productID=itm.itemRefID where itm.invoiceID in ($con) limit 1 "); 
	 if($query->num_rows > 0)
	 {
	     $res =$query->row_array();
	     
	      if($res['AssetAccountRef'])
    	    $res['AssetAccountName']    =  $this->db->query(" SELECT ifnull(accountName,'') as accountName from  QBO_accounts_list where accountID= '".$res['AssetAccountRef']."'  limit  1 ")->row_array()['accountName']; 		  
    	  
    	  if($res['IncomeAccountRef'])
    	    $res['IncomeAccountName']   =  $this->db->query(" SELECT ifnull(accountName,'') as accountName from  QBO_accounts_list where accountID= '".$res['IncomeAccountRef']."'  limit  1 ")->row_array()['accountName']; 
    	  if($res['ExpenseAccountRef'])
    	     $res['ExpenseAccountName']   =  $this->db->query(" SELECT ifnull(accountName,'') as accountName from  QBO_accounts_list where accountID= '".$res['ExpenseAccountRef']."'  limit  1 ")->row_array()['accountName'];
	 }
	 
	 return $res;
	 
	}
	
	public function get_merc_invoice_item_data($con,$merchID)
	{
	 $res  = array();
	$query =  $this->db->query(" SELECT
    itm.invoiceID,
    itm.merchantID, itm.totalAmount, itm.itemDescription,itm.itemQty,itm.itemPrice,itm.itemTax,  
    itm.itemRefID AS itemID
FROM
    tbl_qbo_invoice_item itm

WHERE
    itm.merchantID = $merchID AND  itm.invoiceID in ($con)"); 
	 if($query->num_rows > 0)
	 {
	     $res =$query->result_array();
	     
	   
	 }
	 
	 return $res;
	 
	}
	public function get_merc_invoice_tax_data($con,$merchID)
	{
	 $res  = array();
	$query =  $this->db->query(" SELECT taxID
  
	FROM
	QBO_test_invoice

	WHERE
		merchantID = $merchID AND invoiceID = $con"
	);	 
	 if($query->num_rows > 0)
	 {
	     $res =$query->row_array();
	 }
	 
	 return $res;
	 
	}
	
	  public function get_qbo_item_data($userID)
	  {
		
    	$result =array();
	    
		$this->db->select('qb_item.Name as Name,qb_item.productID as productID  ');
		$this->db->from('QBO_test_item qb_item');
	    $this->db->join('tbl_merchant_data comp','comp.merchID = qb_item.merchantID','INNER');
	   
	    $this->db->where('qb_item.merchantID',$userID);
	     $this->db->where('qb_item.IsActive','true');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
        
        }
       
       return $result;
      }
      
	
	
		public function get_invoice_data_template($con, $user_id = null)
		{

	$res =array();
 	$today  = date('Y-m-d');
 		if(empty($user_id)){
 			if($this->session->userdata('logged_in'))
		    {
		     	$user_id 				= $this->session->userdata('logged_in')['merchID'];
		    }else if($this->session->userdata('user_logged_in'))
			{
				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
 		}
	      
	 
	  $query  =  $this->db->query("SELECT `inv`.invoiceID,`inv`.refNumber as RefNumber,`inv`.DueDate,  ifnull(sum(Total_payment-BalanceRemaining),0) as AppliedAmount,
	     `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*, 
	  ifnull((select transactionType from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1),'' ) as  paymentType, 
	  ifnull((select transactionCode from customer_transaction where invoiceID =inv.invoiceID  order by id desc limit 1),'') as tr_status,
	    ifnull((select transactionDate from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1 ),'') as tr_date,
		ifnull((select transactionAmount from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1 ),'') as tr_amount,
	    ifnull((select transactionStatus from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1 ),'') as tr_data,
		
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = '0'   then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = '0'    and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID  and merchantID='".$user_id."' limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = '0'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='1'     then 'Success'
	  else 'Canceled' end ) as status, cust.userEmail as email
	  FROM QBO_test_invoice inv 
	  INNER JOIN  QBO_custom_customer cust ON inv.CustomerListID = cust.Customer_ListID
	  WHERE  inv.CustomerListID !='' $con  and cust.merchantID='".$user_id."' and  inv.merchantID='".$user_id."'   limit 1 ");
	  
		if($query->num_rows() > 0){
		
		  return  $res=$query->row_array();
		}
	     return  $res;
	} 		
	


	
		function update_refund($tr_id, $gateway)
	{
		$sql =' UPDATE customer_transaction set transaction_user_status="2" where (  id="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    
	    if($gateway=='NMI')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (  id="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
	     if($gateway=='AUTH')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
	    }
	    
	      if($gateway=='PAYTRACE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where (id="'.$tr_id.'"  and (transactionType="pay_sale" or transactionType="pay-sale" or transactionType="pay_capture") )'; 
	        
	    }
	    
	       if($gateway=='PAYPAL')
	    {
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (id="'.$tr_id.'"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )'; 
	        
	    }
	    
	        if($gateway=='STRIPE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
	    }
	    
	      if($gateway=='GLOBAL')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	        
	    }
	    
	        if($gateway=='USAEPAY')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	        
	    }
	     if($gateway=='CYBER')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	        
	    }
	    
	  
	   if($this->db->query($sql))
	   return true;
	   else
	   return false;
	}
	
	
	
	function update_refund_payment($tr_id, $gateway)
	{
		$sql =' UPDATE customer_transaction set transaction_user_status="2" where (  transactionID="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
		
	    if($gateway=='NMI')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (  transactionID="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
	     if($gateway=='AUTH')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
	    }
	    
	      if($gateway=='PAYTRACE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where (transactionID="'.$tr_id.'"  and (transactionType="pay_sale" or transactionType="pay-sale" or transactionType="pay_capture") )'; 
	        
	    }
	    
	       if($gateway=='PAYPAL')
	    {
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )'; 
	        
	    }
	    
	        if($gateway=='STRIPE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
	    }
	    
	        if($gateway=='GLOBAL')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	        
	    }
	        if($gateway=='USAEPAY')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	        
	    }
	      if($gateway=='CYBER')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	        
	    }
		if ($gateway == iTransactGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }
        if ($gateway == TSYSGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }
		if ($gateway == 'CardPointe') {

            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where (  transactionID="' . $tr_id . '" and (transactionType="capture" or transactionType="sale") )';
        }
	   if($this->db->query($sql))
	   return true;
	   else
	   return false;
	}


     
	public function chk_transaction_details($con)
	{
		$today = date("Y-m-d H:i");
			$res =array();
			$this->db->select('sum(tr.transactionAmount) as transactionAmount ');
			$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
		    $this->db->from('customer_transaction tr');
		    $this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
		    $this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
		
	        $this->db->where($con);
		    $this->db->where('cust.customerStatus', 'true');
	        $this->db->order_by("tr.transactionDate", 'desc');
		     $this->db->group_by("tr.transactionID");
		    $this->db->group_by("tr.invoiceID");
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->row_array();
	     $res['transactionAmount'] = ($res1['transactionAmount']- $res1['partial']);
		}
		return $res;
		
	} 		
	
	
	
   public function get_due_invoice_data($inv, $mID)
   {
          $res = array(); 
          $this->db->select('inv.CustomerListID');
          $this->db->from('QBO_test_invoice inv');
          $this->db->where('inv.IsPaid',0);
          $this->db->where('inv.merchantID',$mID);
          $this->db->where_in('invoiceID', $inv);
          $query = $this->db->get();
          if($query->num_rows() > 0)
          {
            $res = $query->row_array();
            
          }
   
    return $res;
   }
   
   
	public function get_transaction_details_by_id($userID,$trID)
	{
		$today = date("Y-m-d H:i");
		$res=array();
		$tcode =array('100','200','111','1');
		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY-SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
		$this->db->select('tr.id, tr.transactionID,tr.gateway,tr.transactionCard,tr.customerListID,tr.merchantID,tr.transactionGateway,tr.gateway,tr.gatewayID,tr.transactionAmount,  cust.Customer_ListID,  cust.fullName, tr.transactionType');
	    $this->db->select('ifnull(GROUP_CONCAT(tr.transactionAmount SEPARATOR ","),"") as tr_amount', FALSE);
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR ","),"") as invoice_id', FALSE);
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.qbListTxnID) SEPARATOR "," ),"") as qbListTxnID', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID", $userID);
        $this->db->where("cust.merchantID", $userID);
          $this->db->where("tr.transactionID", $trID);
        
        $this->db->where_in('UPPER(tr.transactionType)',$type);
       $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	   $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $result= $query->row_array();
		 
	   

		     if(!empty($result['invoice_id']))
		     {
		         
		         $inv =$result['invoice_id'];
		          $res_inv=array();
		     
		     $qq= $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  
		     QBO_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");
		     
		    
		      if( $qq->num_rows >0){
		       $res_inv= $qq->row_array()['invoce']; 
		    
		       $result['invoice_no'] =$res_inv;
		  
		      }
		    
		     }
		     $ref_amt=0;
	
		     if($result['transactionID']!="" && strtoupper($result['transactionType'])!=strtoupper('Offline Payment') )
		     {
		     $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$result['transactionID'].'"  group by creditTransactionID  ');
	
		    $data =  $qr->row_array();
		   
		       if(!empty($data))
		         $result['partial']= $data['refundAmount']; 
		       else
		    
		      $result['partial'] =$ref_amt;
		      }
		      else
		     {
		          $result['partial'] =$ref_amt;
		     }
		     
		     
		     $res =$result;
		 } 

	
		return $res;
		
	}
	
	/**
	 * Get overdue invoice count function
	 *
	 * @param integer $mid
	 * @return integer $count
	 */
	public function get_overdue_invoice($mid){

		$this->db->select('count(qb.invoiceID) as invoice_count');
		$this->db->from("QBO_test_invoice qb");
		$this->db->where('qb.merchantID', $mid);
		$this->db->where('qb.IsPaid', '0');
		$this->db->where('qb.UserStatus', '0');
		$this->db->where('qb.isDeleted', 0);
		$this->db->where('qb.DueDate < CURDATE()');
			
		return $this->db->get()->row()->invoice_count;
	}
	/**
	 * Get failed invoice count function
	 *
	 * @param integer $mid
	 * @return integer $count
	 */
	public function get_failed_invoice($mid){
		$this->db->select('count(qb.invoiceID) as invoice_count');
		$this->db->from("QBO_test_invoice qb");
		$this->db->join('customer_transaction tr', "tr.invoiceID=qb.invoiceID AND tr.merchantID = qb.merchantID AND tr.transactionCode = '300'", 'INNER');
		$this->db->where('qb.merchantID', $mid);
		$this->db->where('qb.IsPaid <> ', '1');
		$this->db->where('qb.UserStatus', '0');
		$this->db->where('qb.isDeleted', 0);
		
		return $this->db->get()->row()->invoice_count;
	}

	/**
	 * Get paid invoice count function
	 *
	 * @param integer $mid
	 * @return integer $count
	 */
	public function get_paid_invoice($mid){

		$this->db->select('count(qb.invoiceID) as invoice_count');
		$this->db->from("QBO_test_invoice qb");
		$this->db->where('qb.merchantID', $mid);
		$this->db->where('qb.IsPaid', '1');
		$this->db->where('qb.UserStatus', '0');
		$this->db->where('qb.isDeleted', 0);
		
		return $this->db->get()->row()->invoice_count;
	}
		public function get_shedule_invoice($mid){

			$this->db->select('count(qb.invoiceID) as invoice_count');
			$this->db->from("QBO_test_invoice qb");
			$this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'INNER');
			$this->db->where('qb.merchantID', $mid);
			$this->db->where('qb.IsPaid', '0');
			$this->db->where('qb.UserStatus', '0');
			$this->db->where('qb.isDeleted', 0);
			$this->db->where('sc.scheduleDate IS NOT NULL');

			return $this->db->get()->row()->invoice_count;
		}
	
		public function get_transaction_data_by_id($TxnID, $userID,$action){
		
			$this->db->select('tr.transactionID,sum(tr.transactionAmount)as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, cust.FullName, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID ');
			$this->db->from('customer_transaction tr');
			$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
		    $this->db->where("cust.merchantID", $userID);
			if($action == 'transaction'){
				$this->db->where("tr.id", $TxnID);
			} else {
				$this->db->where("tr.transactionID", $TxnID);
			}
			$query = $this->db->get();
			if($query->num_rows() > 0){
			
			  return $query->result_array();
			}
			
		}
		function get_merchant_billing_details($invID) 
		{
		     $sql = $this->db->query(" Select * from tbl_merchant_invoice_item where merchantInvoiceID='".$invID."'  ");
			 
	         $result=array();
		          if($sql->num_rows()>0){
			          $result =   $sql->result_array();
		          } 
			 
			    	return $result;

			
		}
		public function get_transaction_datarefund($userID)
	    {
	        $today = date("Y-m-d H:i");
	        $res   = array();
	        $tcode = array('100', '200', '111', '1');
	        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

	        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from QBO_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
	        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
	        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
	        $this->db->from('customer_transaction tr');
	        $this->db->join('QBO_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
	        
	        $this->db->where("cust.merchantID ", $userID);
	        $this->db->where("tr.merchantID ", $userID);
	        $this->db->not_like("tr.gateway", 'ECheck');

	        $this->db->where_in('UPPER(tr.transactionType)', $type);
	        $this->db->where_in('(tr.transactionCode)', $tcode);
	        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
	        $this->db->order_by("tr.transactionDate", 'desc');
	        $this->db->group_by("tr.transactionID");

	        $query = $this->db->get();
	        if ($query->num_rows() > 0) {

	            $res1 = $query->result_array();

	            foreach ($res1 as $result) {

	                if (!empty($result['invoice_id'])) {
	                    $inv_array = array();
	                    $inv_array = explode(', ', $result['invoice_id']);
	                    $res_inv   = array();
	                    foreach ($inv_array as $inv) {
	                        $qq = $this->db->query(" Select refNumber   from   QBO_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
	                       
	                        if ($qq->num_rows > 0) {
	                            $res_inv[] = $qq->row_array()['refNumber'];
	                        }
	                    }

	                    $result['invoice_no'] = implode(',', $res_inv);
	                }

	                $ref_amt = 0;

	                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

	                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');


	                    $data = $qr->row_array();

	                    if (!empty($data)) {
	                        $result['partial'] = $data['refundAmount'];
	                    } else {
	                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

	                        $result['partial'] = $ref_amt;
	                        
	                    }

	                } else {
	                    $result['partial'] = $ref_amt;
	                }

	                $res[] = $result;
	            }
	        }

	        return $res;

	    }
		public function getPartialNewAmount($transactionID)
	    {
	        $ref_amt = 0;
	        
	        
	        $qr = $this->db->query('SELECT id FROM customer_transaction WHERE transactionID ="' . $transactionID . '" ORDER BY `id` DESC');
	        $data = $qr->result_array();
	        
	        if (!empty($data)) {
	            foreach ($data as $value) {
	                $qr1 = $this->db->query('SELECT sum(transactionAmount) as transactionAmount FROM customer_transaction WHERE parent_id ="' . $value['id'] . '" ORDER BY `id` DESC');
	                $data1 = $qr1->row_array();
	                
	                if (!empty($data1)) {
	                    $ref_amt = $ref_amt + $data1['transactionAmount'];
	                }

	            }
	            
	            return $ref_amt;
	            
	        } else {
	            return $ref_amt;
	        }
	    }

	public function get_transaction_history_ajax_data($userID, $customerID = null){
		$res = false;

		$this->_transaction_history_ajax($userID, $customerID = null);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$res1= $query->result_array();
			foreach( $res1 as $result)
			{
				if(!empty($result['invoice_id']))
				{
					$result['invoice_no'] = '';
					$inv =$result['invoice_id'];
					$invList = explode(',', $inv);

					foreach($invList as $key1 => $value) {
						$value = trim($value);
						if(empty($value) || $value == ''){
							unset($invList[$key1]);	
						}
						$invList[$key1] = "'".$value."'";
					}

					$inv = implode(',', $invList); 
					$res_inv=array();
					
					$qq= $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  
					QBO_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");
				
					if( $qq->num_rows >0){
					$res_inv= $qq->row_array()['invoce']; 
				
					$result['invoice_no'] =$res_inv;
				
					}
			
				}
				$ref_amt=0;
	
				if($result['transactionID']!="" && strtoupper($result['transactionType'])!=strtoupper('Offline Payment') )
				{
					$qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$result['transactionID'].'"  group by creditTransactionID  ');
		
					$data =  $qr->row_array();
				
					if(!empty($data)){
						$result['partial']= $data['refundAmount']; 
					}
					else {
						$ref_amt = $this->getPartialNewAmount($result['transactionID']);

						$result['partial'] = $ref_amt;
						
					}
				} else {
					$result['partial'] =$ref_amt;
				}
				$res[] =$result;
			} 
		}
        return $res;
    }

	public function _transaction_history_ajax($userID, $customerID = null)
	{
		$today = date("Y-m-d H:i");
		$res=array();
		$tcode =array('100','200', '120','111','1');

		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY-SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND','CREDIT');
		
		$columnSearch = [
			'tr.invoiceID',
			'cust.fullName',
			'qti.refNumber',
			'tr.transactionDate',
			'tr.transactionID',
		];

		$con = ''; $i = 0;

		if ($_POST['search']['value']) {
			foreach ($columnSearch as $item) {
				$searchValue = $_POST['search']['value'];
				if($item == 'tr.transactionDate'){
					$searchValue = date("Y-m-d", strtotime($searchValue));
				}

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $searchValue . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $searchValue . '%"';
                }
				$i++;
            }
        }

		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,  cust.Customer_ListID,  cust.fullName');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(qti.refNumber) SEPARATOR "," ),"") as invoice_ref_number', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID AND cust.merchantID = tr.merchantID ','INNER');
		$this->db->join('QBO_test_invoice qti','tr.invoiceID=qti.invoiceID AND tr.merchantID=qti.merchantID','left');
		$this->db->where("tr.merchantID ", $userID);
		$this->db->where_in('UPPER(tr.transactionType)',$type);
		$this->db->where_in('(tr.transactionCode)',$tcode);
		
		if($customerID){
			$this->db->where("tr.customerListID", $customerID);
		}

		if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }

        $orderBY = $_POST['order'][0]['dir'];
		$column = $_POST['order'][0]['column'];

        if($column == 0){
            $orderName = 'cust.fullName';
        }elseif($column == 1){
            $orderName = 'qti.refNumber';
        }elseif($column == 2){
            $orderName = 'transactionAmount';
        }elseif($column == 3){
            $orderName = 'tr.transactionDate'; 
        }elseif($column == 4){
            $orderName = 'tr.transactionType';
        }elseif($column == 5){
            $orderName = 'tr.transactionID';
        }else{
            $orderName = 'tr.transactionDate'; 
			$orderBY = 'desc';   
        }
        $this->db->where("tr.transactionID >", 0);
        $this->db->order_by($orderName, $orderBY);
		$this->db->group_by("tr.transactionID", 'desc');

	}

	public function get_transaction_history_ajax_total_data($userID, $customerID = null){
		$this->_transaction_history_ajax($userID, $customerID = null);
        $query = $this->db->get();
        return $query->num_rows();
    }
}