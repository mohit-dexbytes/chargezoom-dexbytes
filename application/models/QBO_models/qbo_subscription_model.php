<?php
Class Qbo_subscription_model extends CI_Model
{
  public function get_subcription_data(){
	 $res    = array();
	 $today  = date('Y-m-d');	
	 $date   = date('d');
     $this->db->select('sb.*,cust.fullName ');
     $this->db->from('tbl_subscriptions_qbo sb');
	 $this->db->join('QBO_custom_customer cust','sb.customerID = cust.Customer_ListID','INNER');
	 $this->db->where('sb.nextGeneratingDate = "'.$today.'" ');
	 $this->db->group_by('sb.subscriptionID');
	 $query = $this->db->get();
	 if($query->num_rows() > 0){
		return  $res=$query->result_array();
	 }
	 return  $res;

	
	}
	
	public function get_sub_invoice($table,$con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	public function get_subscriptions_gatway_data($val){
	
					
	 $this->db->select('*,cust.fullName');
     $this->db->from('tbl_subscriptions_qbo sb');
	 $this->db->join('QBO_custom_customer cust','sb.customerID = cust.Customer_ListID','INNER');
	 $this->db->where($val);
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}

	public function get_subscription_invoice_data($userID, $subID)
	{
	
		$this->db->select('qb.invoiceID,qb.refNumber,qb.CustomerListID, qb.DueDate,qb.Total_payment,qb.BalanceRemaining, qb.AppliedAmount, qb.IsPaid, qb.UserStatus, cs.fullName AS custname,
			(case when (sc.scheduleDate IS NOT NULL and qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Scheduled" when
				(qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="0" ) THEN "Open"
					when (qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="0"  ) THEN "Overdue"
					when (qb.merchantID=' . $userID . ' and tr.merchantID=' . $userID . ' and (tr.transactionCode ="300")  ) THEN "Failed"
					when (qb.IsPaid="1" and qb.UserStatus="0" ) Then "Paid" ELSE "Cancelled" END) as status
		');
		$this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
		$this->db->from("QBO_test_invoice qb");
		$this->db->join('tbl_subscription_auto_invoices TSAI', "TSAI.invoiceID = qb.invoiceID AND TSAI.subscriptionID = $subID AND app_type = 1", 'inner');

		$this->db->join('QBO_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
		$this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID ', 'left');

		$this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');

		$this->db->where('qb.merchantID', $userID);
		$this->db->where('qb.isDeleted', 0);
		$this->db->where('cs.merchantID', $userID);

		$this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL ) and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL) and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');
		$this->db->group_by('qb.invoiceID');
	
		$query = $this->db->get();
	   	return $query->result_array();
	}
	public function get_subscription_revenue($userID, $subID)
	{
	
		
		$this->db->select(' (SUM(qb.Total_payment) - SUM(qb.BalanceRemaining) ) as revenue', false);
		$this->db->from("QBO_test_invoice qb");
		$this->db->join('tbl_subscription_auto_invoices TSAI', "TSAI.invoiceID = qb.invoiceID AND TSAI.subscriptionID = $subID AND app_type = 1", 'inner');

		$this->db->join('QBO_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
		$this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID ', 'left');

		$this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');

		$this->db->where('qb.merchantID', $userID);
		$this->db->where('qb.isDeleted', 0);
		$this->db->where('cs.merchantID', $userID);

		$this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL ) and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL) and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');
		$this->db->group_by('qb.invoiceID');
	
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			
			$res = $query->row_array(); 

			return $res['revenue'];
		}
		
	   	return 0;
	}
}