<?php
Class User_Login_model extends CI_Model
{
	private $tbl_user = 'qb_test_customer'; // user table name 
	function Login()
	{
		parent::Model();
	}
	
	public function get_user_login($email, $password)
    {
       $res=array();
	   
	   $this->db->select('*');
	   $this->db->from('tbl_merchant_user');
	   $this->db->where('userEmail',$email);
	   $query = $this->db->get();
	   
	   if($query->num_rows()>0){
		   $res = $query->row_array();

           if ($res['userPasswordNew'] !== null) {
               if (!password_verify($password, $res['userPasswordNew'])) {
                   return [];
               }
           } else {
               if ($password !== $res['userPassword']) {
                   return [];
               }
           }
	   }

	   return $res;
    }
	
	

	 
}