<?php

Class Customer_model extends CI_Model
{
	private $tbl_user = 'chargezoom_test_customer'; // user table name 
	function Customer()
	{
		parent::Model();
	}

	
	
	
	
	
	 function get_serch_customer($key,$merchantID) 
	{
		 $sql = " SELECT cust.ListID as ListID, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, cust.companyName as customerCompany, 
		 cust.FullName as customerFullName, cust.Contact as customerEmail, 
		 cust.Phone as customerPhone, 
		 cust.TimeCreated as customerTime, cust.IsActive as IsActive, comp.* , (select sum(BalanceRemaining) 
		 from chargezoom_test_invoice where Customer_ListID = cust.ListID  and userStatus != 'cancel') as Payment from chargezoom_test_customer cust 
		 inner join tbl_company comp on comp.id=cust.companyID 
		 where comp.merchantID = '".$merchantID."' and cust.FirstName LIKE '%".$key."%' OR cust.LastName  LIKE '%".$key."%'  or   cust.FullName  LIKE '%".$key."%'    ";
		 

		$query = $this->db->query($sql);
		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}

		
    function get_customers($merchantID) 
	{
		 $sql = ' SELECT cust.ListID as ListID, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, 
		 cust.companyName as customerCompany, cust.FullName as customerFullName, cust.Contact as customerEmail, cust.Phone as customerPhone, 
		 cust.IsActive as IsActive, cust.TimeCreated as customerTime, comp.* , (select sum(BalanceRemaining) from chargezoom_test_invoice
		 where Customer_ListID = cust.ListID  and userStatus != "cancel") as Payment from chargezoom_test_customer cust  inner join tbl_company comp on comp.id=cust.companyID   where comp.merchantID = "'.$merchantID.'" and customerStatus="1" ';
		 

		$query = $this->db->query($sql);
		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}
  
	public function getCustomerBalancePayment($customerID, $merchantID=0)
    {
		$sql = 'select sum(BalanceRemaining) as balancePayment from chargezoom_test_invoice where Customer_ListID=\''.$customerID.'\' and userStatus !=\'cancel\'';

        if($merchantID > 0)
        {
            
        }

        $query = $this->db->query($sql);
	
        
        if($query->num_rows()){
            $row = $query->row();
            return $row->balancePayment;
        } else {
            return 0;
        }
    }
  
    function get_all_active_customers($merchantID,$postdata,$st) 
	{
	   $ress=array();
	   $paymentSql = 0; 

	   	if($st == 1){
			$sql = 'SELECT cust.ListID as ListID, cust.EditSequence, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, cust.companyName as customerCompany, 
			cust.FullName as customerFullName, cust.Contact as customerEmail,cust.qb_status,cust.qbAction,  
			cust.Phone as customerPhone, cust.IsActive as IsActive,  (case when cust.IsActive="true" then "Done"  when cust.IsActive="Pending" then "In Sync" else "Done" end) as c_status,
			cust.TimeCreated as customerTime, comp.* , '.$paymentSql.' as Payment from chargezoom_test_customer cust 
			inner join tbl_company comp on comp.id=cust.companyID   where comp.merchantID = "'.$merchantID.'" AND cust.IsActive != "false"';
			
	   	} else { 
			$sql = 'SELECT cust.ListID as ListID,cust.EditSequence, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, cust.companyName as customerCompany, 
			cust.FullName as customerFullName, cust.Contact as customerEmail, cust.Phone as customerPhone, cust.IsActive as IsActive, cust.qb_status,cust.qbAction, 
			(case when cust.IsActive="false" then "Done" when cust.IsActive="true" then "Done" else "Done" end) as c_status, cust.TimeCreated as customerTime, comp.* , 
			'.$paymentSql.' as Payment from chargezoom_test_customer cust 
			inner join tbl_company comp on comp.id=cust.companyID where comp.merchantID = "'.$merchantID.'" AND cust.IsActive != "true" AND cust.customerStatus = "0"';
			
		   }
		
		// Search Where condition
		$column1 = array('cust.FirstName', 'cust.LastName', 'cust.FullName', 'cust.Contact');

		if ( !empty($_POST['search']['value']) ) 
		{
			$whereArr = [];
			foreach ($column1 as $item) {
				$whereArr[] =  $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
			}
			$sql .= ' AND ('.implode( ' OR ', $whereArr).') ';
		}

		   

		    $column = getTableRowOrderColumnValue($_POST['order'][0]['column']);
		    if($column == 0){
		        $orderName = 'cust.FullName';
		    }elseif($column == 1){
		        $orderName = 'cust.FirstName';
		    }elseif($column == 2){
		        $orderName = 'cust.Contact';
		    }elseif($column == 3){
		        $orderName = 'cust.Phone';
		    }elseif($column == 4){
		        $orderName = 'Payment';
		    }else{
		        $orderName = 'cust.FirstName'; 
		    }

		    $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);

		    $sql .=' ORDER BY  '.$orderName.' '.$orderBY.' ';
		   
		    $postLength = getTableRowLengthValue($_POST['length']);
		    $startRow = getTableRowStartValue($_POST['start']);
			  if($postLength != -1){
				   $sql .=' LIMIT '.$postLength.' OFFSET '.$startRow.'';
			  }
		   
			$query = $this->db->query($sql);
		if($query -> num_rows() > 0){
           $ress= $query->result_array(); 
          return $ress;
        }
      

		return false; 
	}
	
	function get_all_customer_count($merchantID,$st) 
	{
	   $ress=array();

	   $sql = 'SELECT count(cust.ListID) as customerCount from chargezoom_test_customer cust 
	   inner join tbl_company comp on comp.id=cust.companyID   where comp.merchantID = "'.$merchantID.'"';

	   	if($st == 1){
			$sql .= ' AND cust.IsActive != "false"';
			
	   	} else { 
			$sql .= ' AND cust.IsActive != "true" AND cust.customerStatus = "0"';
			
		}

		// Search Where condition
		$column1 = array('cust.FirstName', 'cust.LastName', 'cust.FullName', 'cust.Contact');

		if ( !empty($_POST['search']['value']) ) 
		{
			$whereArr = [];
			foreach ($column1 as $item) {
				$whereArr[] =  $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
			}
			$sql .= ' AND ('.implode( ' OR ', $whereArr).') ';
		}

		$query = $this->db->query($sql);
	

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->customerCount;
        }

        return 0;
		
	}

    function   get_all_qbd_records($merchantID)
    {
    $resst1=$resst2=$resst3=$resst4=array();
        $resst=array();	
       $sql = 'SELECT cs.customerID as ID, cs.FullName as Data, cs.qb_status, cs.qb_action,	cs.updatedAt as time,
       ( case   when  cs.customerID!=0 then "Customer" else "Customer" end) as c_type  from tbl_custom_customer cs 
		 inner join tbl_company comp on comp.id=cs.companyID   
         where cs.merchantID = "'.$merchantID.'"  ';
         $query1 = 	$this->db->query($sql );
        if($query1-> num_rows() > 0)
		{	
        $resst1 =    $query1->result_array(); 
       
        } 
    
    $sql1 = 'SELECT inv.insertInvID as ID , inv.RefNumber as Data,  inv.qb_status,inv.qb_action, inv.TimeModified as time,
    ( case   when  inv.insertInvID!=0 then "Invoice" else "Invoice" end) as c_type  from tbl_custom_invoice inv 
		  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
	  inner join tbl_company t_comp on t_comp.id = cust.companyID
	  WHERE  t_comp.merchantID = "'.$merchantID.'"  and  cust.customerStatus=1 ';
    
     $query2 = 	$this->db->query($sql1 );
        if($query2-> num_rows() > 0)
		{	
        $resst2 =    $query2->result_array(); 
        } 
    $sql2=' Select qb.productID as ID, qb.productFullName as Data, qb.qb_status, qb.qb_action,  qb.updatedAt as time,
    ( case   when  qb.productID!=0 then "Product" else "Product" end) as c_type from  tbl_custom_product qb  inner join tbl_company comp on comp.id=qb.companyID    where comp.merchantID = "'.$merchantID.'"  ';  
      $query3 = 	$this->db->query($sql2 );
        if($query3-> num_rows() > 0)
		{	
        $resst3 =    $query3->result_array(); 
        } 
        
        
         $sql4 = 'SELECT l.payID as ID , l.paymentID as Data,  l.qb_status,l.qb_action, l.TimeModified as time,
    ( case   when  l.paymentID!=0 then "Invoice Payment" else "Invoice Payment" end) as c_type  from 	tbl_qbd_invoice_payment_log l 
		  
	  WHERE  l.merchantID = "'.$merchantID.'" ';
    
     $query3= 	$this->db->query($sql4 );
        if($query3-> num_rows() > 0)
		{	
        $resst4 =    $query3->result_array(); 
        } 
    $resst =  array_merge($resst1, $resst2,$resst3,$resst4);
  return $resst;	

    }








       public function get_select_data($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
					
					if($query->num_rows()>0 ) {
						$data = $query->row_array();
					   return $data;
					} else {
						return $data;
					}			
			}
    function get_all_customers($merchantID) 
	{
         $resst=array();
		 $sql = 'SELECT cust.ListID as ListID,cust.EditSequence, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, cust.companyName as customerCompany, 
		 cust.FullName as customerFullName, cust.Contact as customerEmail, cust.Phone as customerPhone, cust.IsActive as IsActive, cust.qb_status,cust.qbAction, 
		 (case when cust.IsActive="false" then "Done" when cust.IsActive="true" then "Done" else "Done" end) as c_status, cust.TimeCreated as customerTime, comp.* , 
		 (select sum(BalanceRemaining) from chargezoom_test_invoice where Customer_ListID = cust.ListID  and userStatus != "cancel") as Payment from chargezoom_test_customer cust 
		 inner join tbl_company comp on comp.id=cust.companyID where comp.merchantID = "'.$merchantID.'" AND cust.IsActive != "true" AND cust.customerStatus = "0"';
		 

		$query = $this->db->query($sql);
		if($query -> num_rows() > 0)
		{	
			   $ress= $query->result_array(); 
        return $ress;		
        
        
        
        }else

		return false; 
	}


	
	function get_customers_data($merchantID) 
	{
		 $sql = ' SELECT cust.* from chargezoom_test_customer  cust inner join tbl_company on tbl_company.id=cust.companyID   where tbl_company.merchantID = "'.$merchantID.'" and cust.customerStatus="1" order by FullName asc ';
		 

		$query = $this->db->query($sql);

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}

	
	function get_customer_details($id){
      $data  =array();
	  
	 $customer_pro =   "call get_customer_details(?)";
	  
     $condition = array('usercutomerID'=>$id);
     $query = $this->db->query($customer_pro, $condition);
     $data =  $query->row();
    return $data;
	}
	
	
	function customer_by_id($id) 
	{
         $this->db->select('cus.*,cmp.qbwc_username ');
		 $this->db->select('IFNULL(loginAt,"") as loginAt',FALSE);
		$this->db->from('chargezoom_test_customer cus');
		$this->db->join('tbl_company cmp','cmp.id = cus.companyID','INNER');
		$this->db->join('tbl_customer_login cr','cr.customerID=cus.ListID','left');
		$this->db->where('cus.ListID', $id);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 

		else

		return false; 
	}
    
    function customer_qbc_by_invoice($id) 
	{
         $this->db->select('cmp.qbwc_username ');
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cus', 'cus.ListID=inv.Customer_ListID','INNER');
		$this->db->join('tbl_company cmp','cmp.id = cus.companyID','INNER');
		$this->db->where('inv.TxnID', $id);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 

		else

		return false; 
	}
	
	function customer_qbc_by_id($id) 
	{
         $this->db->select('cmp.qbwc_username ');
		
		$this->db->from('chargezoom_test_customer cus');
		$this->db->join('tbl_company cmp','cmp.id = cus.companyID','INNER');
		$this->db->where('cus.ListID', $id);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 

		else

		return false; 
	}
	
	public function get_customer_invoice_data($custID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.*, 
	 ( case   when  IsPaid ='true'    AND userStatus='Active'   then 'Success'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Upcoming' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'    and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' AND userStatus='Active'   then 'Past Due'
	   
	  else 'Cancel' end ) as status FROM chargezoom_test_invoice inv
	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  WHERE `inv`.`Customer_ListID` = '$custID' ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
		
	public function get_invoice_upcomming_data($userID, $mID)
	{

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, DATE_FORMAT(inv.DueDate, '%Y-%m-%d') as due, inv.IsPaid, 
	  `inv`.RefNumber,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining,  `inv`.DueDate,  cust.*,
	  (case 
            when (sc.scheduleDate!='0000-00-00' and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
	     when (DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel') then 'Open' 
		 when (DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel')  then 'Past Due'
	  
	     when (DATE_FORMAT(inv.DueDate, '%Y-%m-%d') > '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel'
		 and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300' ) then 'Failed'
	     when (DATE_FORMAT(inv.DueDate, '%Y-%m-%d') > '$today' and IsPaid ='true'  AND userStatus!='cancel')  then 'Paid'
	  
	  else 'Cancel' end ) as status ,
	  ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
	  FROM chargezoom_test_invoice inv 
	 INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	 
	 LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID`
	  WHERE   IsPaid ='false'   and userStatus!='cancel'  AND (sc.merchantID=  '".$mID."' OR sc.merchantID IS NULL)
       AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
        AND ((sc.customerID=cust.ListID and sc.merchantID=cust.qbmerchantID)OR sc.customerID IS NULL) and 
	  
	  `inv`.`Customer_ListID` = '$userID' and cust.qbmerchantID='$mID'     order by   DueDate  asc  ");
	
		if($query->num_rows() > 0)
		{
		
		   $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	
	
	
	public function get_invoice_latest_data($userID)
	{

	$res =array();
 	$today  = date('Y-m-d');
 	
 	
 	 $this->db->select("inv.TxnID, inv.RefNumber , inv.IsPaid, `inv`.BalanceRemaining, (-inv.AppliedAmount) as AppliedAmount, inv.DueDate, cust.ListID,cust.FullName,DATE_FORMAT(inv.TimeModified,'%b %d, %Y %l:%i %p')as TimeModifiedinv", FALSE);
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','cust.ListID=inv.Customer_ListID','inner');
		$this->db->where('cust.ListID', $userID);
		$this->db->where('(IsPaid ="true" ) and inv.userStatus!="cancel"');
	    $this->db->order_by('inv.TimeModified','desc'); 
	    	$query = $this->db->get();
	 
		if($query->num_rows() > 0){
		
		  $res=$query->result_array();
		}
		
	     return  $res;
	} 		
	
	
	public function get_customer_invoice_data_sum($custID){
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,  
		  sum(inv.BalanceRemaining-inv.AppliedAmount) as amount   FROM chargezoom_test_invoice inv  WHERE `inv`.`Customer_ListID` = '".$custID."' and userStatus!='cancel'   ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
	
	public function get_customer_invoice_data_payment($custID){
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  sum(-AppliedAmount) as applied_amount,  
		  ifnull( (select sum(BalanceRemaining) from chargezoom_test_invoice where `Customer_ListID` = '".$custID."' and BalanceRemaining!='0.00'  and AppliedAmount!='0.00' and userStatus!='cancel'   ),'')as applied_due, 
ifnull( (select sum(BalanceRemaining) from chargezoom_test_invoice where `Customer_ListID` = '".$custID."' and userStatus!='cancel'  ),'')as remaining_amount , 
		  ifnull( (select sum(BalanceRemaining) from chargezoom_test_invoice where `Customer_ListID` = '".$custID."' AND   IsPaid ='false'  and userStatus!='cancel'  ),'') as upcoming_balance FROM chargezoom_test_invoice    WHERE `Customer_ListID` = '".$custID."' and userStatus!='cancel'    ");
		 $res=$query->row();
		 return  $res;
		
	}	
	
	
	public function get_customer_invoice_data_count($custID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as incount  FROM chargezoom_test_invoice   WHERE Customer_ListID = '".$custID."' ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->incount;
		}
		return false;
	     
	}	
	
	
	
	
	public function get_customer_note_data($custID, $mID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `chargezoom_test_customer` cust ON `pr`.`customerID` = `cust`.`ListID`
	  WHERE `pr`.`customerID` = '".$custID."' and   `pr`.`merchantID` = '".$mID."' group by pr.noteID desc");
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
    public function get_transaction_history_data($userID, $customerID = null)
	{
		$today = date("Y-m-d H:i");
		$res =array();
		$tcode =array('100','200', '120','111','1');
		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND', 'CREDIT');
	
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceTxnID,  cust.ListID, cust.FullName,(select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID limit 1) as RefNumber, ');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		
	    $this->db->where("comp.merchantID ", $userID);
	     $this->db->where("tr.merchantID ", $userID);
		 $this->db->where_in('UPPER(tr.transactionType)',$type);
          $this->db->where_in('(tr.transactionCode)',$tcode);
	     $this->db->order_by("tr.id", 'desc');
	     $this->db->where("tr.transactionID >", 0);
		  $this->db->group_by("tr.transactionID");
		if($customerID){
	     	$this->db->where("tr.customerListID", $customerID);
		}
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 
		 
		 foreach( $res1 as $result)
		 {
		    
		      if(!empty($result['invoice_id']))
		     {
		         $inv_array = array();
		         $inv_array =explode(', ',$result['invoice_id']);
		         $res_inv=array();
		         foreach($inv_array as $inv)
		         {
		           $qq= $this->db->query(" Select RefNumber   from   chargezoom_test_invoice where TxnID='".trim($inv)."'  limit 1 ");
    		      if( $qq->num_rows >0){
    		       $res_inv[]= $qq->row_array()['RefNumber']; 
    		       }
		         }
		         
		       $result['invoice_no'] =implode(',',$res_inv);
		     }
		     
		     $ref_amt=0;
	
		     if($result['transactionID']!="" && strtoupper($result['transactionType'])!=strtoupper('Offline Payment') )
		     {
		     $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$result['transactionID'].'"  group by creditTransactionID  ');
	
		    $data =  $qr->row_array();
		   
		       if(!empty($data)){
		         $result['partial']= $data['refundAmount']; 
		       } else {
                    $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                    $result['partial'] = $ref_amt;
                    
                }
		    
		      $result['partial'] =$ref_amt;
		      }
		      else
		     {
		          $result['partial'] =$ref_amt;
		     }
		     
		     $res[] =$result;
		 } 
		}
		
		return $res;
		
	}

	public function get_transaction_data($userID)
	{
		$today = date("Y-m-d H:i");
		$res =array();
		$tcode =array('100','200', '120','111','1');
		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
	
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceTxnID,  cust.ListID, cust.FullName,(select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID limit 1) as RefNumber, ');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		
	    $this->db->where("comp.merchantID ", $userID);
	     $this->db->where("tr.merchantID ", $userID);
	     
		 $this->db->where_in('UPPER(tr.transactionType)',$type);
          $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	     $this->db->order_by("tr.id", 'desc');
		  $this->db->group_by("tr.transactionID");
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 
		 
		 foreach( $res1 as $result)
		 {
		    
		      if(!empty($result['invoice_id']))
		     {
		         $inv_array = array();
		         $inv_array =explode(', ',$result['invoice_id']);
		         $res_inv=array();
		         foreach($inv_array as $inv)
		         {
		           $qq= $this->db->query(" Select RefNumber   from   chargezoom_test_invoice where TxnID='".trim($inv)."'  limit 1 ");
    		      if( $qq->num_rows >0){
    		       $res_inv[]= $qq->row_array()['RefNumber']; 
    		       }
		         }
		         
		       $result['invoice_no'] =implode(',',$res_inv);
		     }
		     
		     $ref_amt=0;
	
		     if($result['transactionID']!="" && strtoupper($result['transactionType'])!=strtoupper('Offline Payment') )
		     {
		     $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$result['transactionID'].'"  group by creditTransactionID  ');
	
		    $data =  $qr->row_array();
		   
		       if(!empty($data))
		         $result['partial']= $data['refundAmount']; 
		       else
		    
		      $result['partial'] =$ref_amt;
		      }
		      else
		     {
		          $result['partial'] =$ref_amt;
		     }
		     
		     $res[] =$result;
		 } 
		}
		
		return $res;
		
	} 		
	
		
	
	 
	public function get_transaction_data_captue($userID){
		
		
		$query =$this->db->query("Select tr.*,  tr.invoiceTxnID, (select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID )
		as RefNumber  ,cust.* from customer_transaction tr inner join 
		chargezoom_test_customer cust on  tr.customerListID = cust.ListID inner join tbl_company on cust.companyID= tbl_company.id 
		where tbl_company.merchantID='".$userID."' 
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
		and ( (tr.transaction_user_status ='5' and tr.transaction_user_status !='refund') or tr.transaction_user_status IS NULL ) and cust.customerStatus='1' ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 	


	public function get_transaction_data_refund($userID){
		$today = date("Y-m-d H:i");
		
		$query =$this->db->query("Select tr.*, tr.invoiceTxnID, DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, (select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber , cust.* from customer_transaction tr 
		inner join  chargezoom_test_customer cust on  tr.customerListID = cust.ListID 
		inner join tbl_company on cust.companyID= tbl_company.id 
		where tbl_company.merchantID='".$userID."'
		and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
	and ((tr.transaction_user_status NOT IN (2,3)) or  tr.transaction_user_status  IS NULL)  and tr.gateway!='AUTH ECheck'  and tr.gateway!='NMI ECheck'   and tr.transactionID!='' and cust.customerStatus='1' GROUP BY tr.transactionID DESC  ");

		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 	
	
	
	
	
	 
	public function get_transaction_data_ecaptue($userID){
		
		
		$query =$this->db->query("Select tr.*, (select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID )
		as RefNumber  ,cust.* from customer_transaction tr inner join 
		chargezoom_test_customer cust on  tr.customerListID = cust.ListID inner join tbl_company on cust.companyID= tbl_company.id 
		where tbl_company.merchantID='".$userID."' 
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
		and (tr.gateway='NMI ECheck'  or tr.gateway='AUTH ECheck')  and (tr.transaction_user_status !='refund' or  tr.transaction_user_status  IS NULL)  and cust.customerStatus='1' ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 	


	public function get_transaction_data_erefund($userID){
		
		$today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceTxnID,tr.transaction_user_status,  cust.ListID, cust.FullName,(select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID limit 1) as RefNumber, ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('chargezoom_test_customer cust', 'tr.customerListID = cust.ListID', 'INNER');
        $this->db->join('tbl_company comp', 'comp.id = cust.companyID', 'INNER');

        $this->db->where("comp.merchantID ", $userID);
        $this->db->where("cust.qbmerchantID ", $userID);
        $this->db->where("tr.merchantID", $userID);
        $this->db->like("tr.gateway", 'ECheck');
        
        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();

            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select RefNumber   from   chargezoom_test_invoice where TxnID='" . trim($inv) . "'  limit 1 ");
                       
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['RefNumber'];
                        }
                    }

                  
                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {
                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID  ');

                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $result['partial'] = $ref_amt;
                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }

        return $res;
		
	} 	
	
	public function get_invoice_data_template($con){

	$res =array();
 	$today  = date('Y-m-d');
	 
	 
	  $query  =  $this->db->query("SELECT `inv`.TxnID,`inv`.RefNumber as RefNumber, `inv`.DueDate, 
	  (-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*, 
	  ifnull((select transactionType from customer_transaction where invoiceTxnID =inv.TxnID order by id desc limit 1),'' ) as  paymentType, 
	  ifnull((select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID  order by id desc limit 1),'') as tr_status,
	    ifnull((select transactionDate from customer_transaction where invoiceTxnID =inv.TxnID order by id desc limit 1 ),'') as tr_date,
		ifnull((select transactionAmount from customer_transaction where invoiceTxnID =inv.TxnID order by id desc limit 1 ),'') as tr_amount,
	    ifnull((select transactionStatus from customer_transaction where invoiceTxnID =inv.TxnID order by id desc limit 1 ),'') as tr_data,
		
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'   AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status, cust.Contact as email
	  FROM chargezoom_test_invoice inv 
	  INNER JOIN  chargezoom_test_customer cust ON inv.Customer_ListID = cust.ListID
	  WHERE  inv.Customer_ListID !='' $con   and cust.customerStatus='1'  limit 1 ");
	  
		if($query->num_rows() > 0){
		
		  return  $res=$query->row_array();
		}
	     return  $res;
	} 		
	
	
	
	public function get_customer_transaction_data($userID){
		
		$this->db->select('tr.*, cust.* ,(select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber' );
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("cust.ListID ", $userID);
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	public function get_invoice_customer_transaction_data($invoiceID, $userID){
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("cust.ListID", $userID);
		 $this->db->where("tr.invoiceTxnID ", $invoiceID);
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	
	public function get_invoice_data_byID($invoiceID){
		$res =array();
		$this->db->select('inv.*, cust.*,comp.*, inv.EditSequence as EditSequence ');
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("inv.TxnID", $invoiceID);
	
		
		
		  $query = $this->db->get();
		   $res= $query->row_array();
		  return $res;
		
	}
	
	
	
	
	
	
	
	 
	
	 
	public function get_subscription_data($userID){
	
			$this->db->select('sbs.*, cust.FullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions sbs');
		$this->db->join('chargezoom_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where("comp.merchantID ", $userID);
		  $this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	public function get_subscription_plan_data($userID, $cust='')
	{
		$cardDb = getenv('DB_DATABASE2');	
	
		$this->db->select('sbs.planID,sbs.subscriptionID,sbs.startDate,sbs.totalAmount, sbs.subscriptionAmount,  sbs.customerID,	sbs.nextGeneratingDate, sbs.merchantDataID,cust.FullName, cust.FirstName, cust.LastName, cust.companyName, tmg.gatewayMerchantID,tmg.gatewayFriendlyName,  spl.planName as sub_planName, c.customerCardfriendlyName');
		$this->db->from('tbl_chargezoom_subscriptions sbs');
		$this->db->join('chargezoom_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
		$this->db->join('tbl_chargezoom_subscriptions_plan spl','spl.planID = sbs.planID','Left');
			 $this->db->join("$cardDb.customer_card_data c",'c.CardID=sbs.cardID','left');
	    $this->db->where("comp.merchantID ", $userID);
	    if($cust!="")
	      $this->db->where('cust.ListID', $cust);
		  $this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
    
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 	
		
	public function get_refund_transaction_data($userID)
	{
	 $res=array();
	$query =	$this->db->query ("SELECT `tr`.*, `cust`.* FROM (`customer_transaction` tr) 
	INNER JOIN `chargezoom_test_customer` cust ON `tr`.`customerListID` = `cust`.`ListID`
	INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID` WHERE `cust`.`customerStatus` = '1' AND `comp`.`merchantID` = '$userID'
	AND UPPER(`tr`.`transactionType`) IN ('REFUND','PAY_REFUND','CREDIT','STRIPE_REFUND','PAYPAL_REFUND' ) ORDER BY `tr`.`transactionDate` desc") ;
	
	
		if($query->num_rows() > 0){
		
		  $res= $query->result_array();
		}
		
		return $res;
		
	} 	
	
	
	
		
	public function get_credit_by_customer($customerID){
          $r_amount = 0; 		
		$this->db->select('sum(creditAmount) as credit_amount');
		$this->db->from('tbl_credits');
		$this->db->where('creditStatus','0');
		$this->db->where('creditName', $customerID);
       	
		$query = $this->db->get();
		echo $this->db->last_query(); 
		if($query->num_rows() > 0){
		
		  $r_amount = $query->row_array()['credit_amount'];
		}
		
		return $r_amount;
	}
	
	
	function get_billing_data($merchID) 
	{
		 $sql = ' SELECT * from tbl_merchant_billing_invoice where merchantID = "'.$merchID.'" ';
		 
         $result=array();
		$query = $this->db->query($sql);
		
		if($query->num_rows()>0 )
		{
		   $result =$query->result_array();
		 
			
			   
			}
			return $result;

		
	}
	
	
		function get_merchant_billing_details($invID) 
	{
	     $sql = $this->db->query(" Select * from tbl_merchant_invoice_item where merchantInvoiceID='".$invID."'  ");
		 
         $result=array();
	          if($sql->num_rows()>0){
		          $result =   $sql->result_array();
	          } 
		 
		    	return $result;

		
	}
	
	 public function get_merchant_billing_data($invID){
		   $res = array();
		   $query = $this->db->query("SELECT bl.*, mr.*, ct.city_name, st.state_name, c.country_name FROM (tbl_merchant_billing_invoice bl) INNER JOIN tbl_merchant_data mr ON mr.merchID=bl.merchantID LEFT JOIN state st ON st.state_id=mr.merchantState LEFT JOIN country c ON c.country_id=mr.merchantCountry LEFT JOIN city ct ON ct.city_id=mr.merchantCity WHERE bl.merchant_invoiceID = '$invID' AND bl.status = 'pending'");
		   
		   if($query->num_rows()>0 ) {
				$res = $query->row_array();
			   return $res;
			} else {
				return $res;
			}
		
		   
	   }
	

	public function get_merchant_transaction_data($merchantID){
		$today = date("Y-m-d H:i");
			$query = $this->db->query("Select tr.*, bil.*  from  tbl_merchant_tansaction tr inner join tbl_merchant_billing_invoice bil on tr.merchant_invoiceID=bil.merchant_invoiceID where tr.merchantID = '$merchantID'");
		
	
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	/****************Subscription Gateway*******************/
	
	
	 
	public function get_subscriptiongateway_data($gatewayID, $userID){
	$res=array();
		$this->db->select('sbs.subscriptionName,sbs.subscriptionID, cust.FullName, spl.planName as sub_planName');
		$this->db->from('tbl_subscriptions sbs');
		$this->db->join('chargezoom_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->join('tbl_subscriptions_plan_qb spl','spl.planID = sbs.planID','Left');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','INNER');
	    $this->db->where("comp.merchantID ", $userID);
		$this->db->where("sbs.paymentGateway ", $gatewayID);
		  $this->db->where('cust.customerStatus', '1');
		  
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  $res = $query->result_array();
		}
		return $res;
		
	} 		
	
   public function get_total_subscription($merchID){   
	   $res = array();	  
       $today = date("Y-m-d H:i");
	   $next_due_date = date('Y-m', strtotime("+30 days"));
        
        
        $this->db->select('*');
        $this->db->from('tbl_chargezoom_subscriptions sbs');
        $this->db->join('chargezoom_test_customer cust','sbs.customerID=cust.ListID AND sbs.merchantDataID = cust.qbmerchantID','inner');
        $this->db->where('cust.qbmerchantID',$merchID);
        $this->db->where('sbs.subscriptionStatus',"1");
        $this->db->where('cust.customerStatus',"1");
        $this->db->where('DATE_FORMAT(sbs.nextGeneratingDate, "%Y-%m")>=',date("Y-m"));
      
        $this->db->group_by('sbs.customerID');
        $query = $this->db->get();
         if($query->num_rows() > 0)
         {
		
		    $res['total_subcription'] = $query->num_rows();
		}
		else
		{
			$res['total_subcription'] =0;
		}
        
    
        
          $query = $this->db->query(" SELECT count(*) as sub_count  FROM (`tbl_chargezoom_subscriptions` sbs) 
	  INNER JOIN `chargezoom_test_customer` cust ON `sbs`.`customerID` = `cust`.`ListID` AND sbs.merchantDataID = cust.qbmerchantID
	  INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID` 
	  WHERE `comp`.`merchantID` = '".$merchID."' AND `cust`.`customerStatus` = '1' 
	  and sbs.subscriptionStatus='1' and DATE_FORMAT(nextGeneratingDate, '%Y-%m')>= '".date('Y-m')."'   ");
	  if($query->num_rows() > 0){
		
		    $res['active_subcription'] = $query->row_array()['sub_count'];
		}else{
			$res['active_subcription'] =0;
		}
		
		$today_date         = date("Y-m-d");
        $next_7_date = date('Y-m-d', strtotime("+7 days"));
        $next_1_month = date('Y-m-d', strtotime("+1 month"));
		
		$query1 = $this->db->query("SELECT COUNT(1) AS exp_count
			FROM
				tbl_chargezoom_subscriptions sbs
			INNER JOIN chargezoom_test_customer cust ON
				sbs.customerID = cust.ListID AND sbs.merchantDataID = cust.qbmerchantID
			WHERE
				sbs.merchantDataID = '$merchID' AND cust.customerStatus = '1' AND subscriptionPlan != '0' AND totalInvoice > '0' AND sbs.subscriptionStatus = '1' AND (
					( sbs.endDate BETWEEN '$today_date' AND  '$next_1_month' AND invoicefrequency IN ('mon', '2mn', 'qtr', 'six', 'yrl', '2yr', '3yr'))
					OR
					( sbs.endDate BETWEEN '$today_date' AND  '$next_7_date' AND invoicefrequency IN ('dly', '1wk', '2wk'))
		)");
	
	   if($query1->num_rows() > 0){
		
		    $res['exp_subcription'] = $query1->row_array()['exp_count'];
		}else{
			$res['exp_subcription'] =0;
		}
		
		 $query1 = $this->db->query("SELECT
				COUNT(distinct(sbs.subscriptionID)) AS failed_count
			FROM chargezoom_test_invoice inv
			INNER JOIN chargezoom_test_customer cust ON
				inv.Customer_ListID = cust.ListID AND cust.qbmerchantID = '$merchID'
			INNER JOIN tbl_company comp ON comp.id = cust.companyID AND comp.merchantID = '$merchID'
			INNER JOIN tbl_subscription_auto_invoices TSAI ON
				TSAI.invoiceID = inv.TxnID AND app_type = 5
			INNER JOIN customer_transaction CTR ON
                CTR.invoiceTxnID = inv.TxnID AND transactionCode = 300  AND CTR.merchantID = '$merchID' 
			INNER JOIN tbl_chargezoom_subscriptions sbs ON TSAI.subscriptionID = sbs.subscriptionID
			WHERE
				sbs.merchantDataID = '$merchID' AND cust.customerStatus = '1' AND inv.IsPaid = 'false' OR inv.userStatus ='Active' GROUP BY sbs.subscriptionID
		");
	 
	   if($query1->num_rows() > 0){
		
		    $res['failed_count'] = $query1->row_array()['failed_count'];
		}else{
			$res['failed_count'] =0;
		}
		
		return  $res;
		
   }
	
	
	
	
	public function get_invoice_schedule_data_count($uid)
	{
	    $num=0;
	  $query=  $this->db->query("select count(qt.TxnID) as sch_count  from chargezoom_test_invoice qt 
	    inner join 	tbl_scheduled_invoice_payment sc on sc.invoiceID=qt.TxnID 
	    INNER JOIN `chargezoom_test_customer` cust ON `qt`.`Customer_ListID` = `cust`.`ListID`
	    where  sc.scheduleDate IS NOT NULL and  sc.customerID=qt.Customer_ListID and cust.qbmerchantID='$uid' 
	    and  sc.merchantID='$uid' and qt.IsPaid='false' and qt.userStatus!='cancel' ");
	    
	    if( $query->num_rows() >0)
	    {
	       $num = $query->row_array()['sch_count'];
	    }
	  return  $num ;
	}
	
	
	public function get_invoice_past_data_count($uid)
	{
	    $num=0;
	    $today = date('Y-m-d');
		$query=  $this->db->query("select count(*) as sch_count  from chargezoom_test_invoice qt 
	    left join 	tbl_scheduled_invoice_payment sc on sc.invoiceID=qt.TxnID 
	    INNER JOIN `chargezoom_test_customer` cust ON `qt`.`Customer_ListID` = `cust`.`ListID`
	    where   (DATE_FORMAT(qt.DueDate, '%Y-%m-%d') < '$today' and qt.IsPaid = 'false'   and userStatus!='cancel' AND ((select count(*) as sch_count from customer_transaction where invoiceTxnID = qt.TxnID limit 1) ='0' OR (select transactionCode from customer_transaction where invoiceTxnID =qt.TxnID limit 1) !='300')
	    and  cust.qbmerchantID='$uid' )");
	    
	    if( $query->num_rows() >0)
	    {
	       $num = $query->row_array()['sch_count'];
	    }
	  return  $num ;
	}
	
	
	
	public function get_transaction_details_data($con)
	{
		$today = date("Y-m-d H:i");
			$res =array();
			$this->db->select('sum(tr.transactionAmount) as transactionAmount ');
			$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
		
	    $this->db->where($con);
		  $this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("tr.transactionDate", 'desc');
		  $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->row_array();
	     $res['transactionAmount'] = ($res1['transactionAmount']- $res1['partial']);
		}
		return $res;
		
	} 		
	
	
	
    
	public function get_invoice_transaction_data($invoiceID, $userID,$action){
		
		$this->db->select('tr.id, tr.transactionID,sum(tr.transactionAmount)as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, cust.FullName, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID');
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("comp.merchantID ", $userID);
	    if($action=='invoice')
		$this->db->where("tr.invoiceTxnID ", $invoiceID);
		if($action=='transaction')
		$this->db->where("tr.transactionID ", $invoiceID);
		  $this->db->group_by('transactionID');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	
	
	public function chk_transaction_details($con)
	{
		$today = date("Y-m-d H:i");
			$res =array();
			$this->db->select('sum(tr.transactionAmount) as transactionAmount ');
			$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
		
	    $this->db->where($con);
		  $this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("tr.transactionDate", 'desc');
		  $this->db->group_by("tr.transactionID");
		    $this->db->group_by("tr.invoiceTxnID");
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->row_array();
	     $res['transactionAmount'] = ($res1['transactionAmount']- $res1['partial']);
		}
		return $res;
		
	} 		
	
	
		
	public function get_credit_user_data($con)
    {
		$res = array();
		$this->db->select('cr.*, cust.companyName');
		$this->db->from('chargezoom_customer_credit cr');
		$this->db->join('chargezoom_test_customer cust','cr.CustomerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where($con);
	
		$this->db->order_by('cr.TimeModified','desc');
		$query = $this->db->get();

		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 

	
	
		function update_refund($tr_id, $gateway)
	{
	    $sql='';
	    if($gateway=='NMI')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (  id="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
	     if($gateway=='AUTH')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
	    }
	    
	      if($gateway=='PAYTRACE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where (id="'.$tr_id.'"  and (transactionType="pay_sale" or transactionType="pay_capture") )'; 
	        
	    }
	    
	       if($gateway=='PAYPAL')
	    {
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (id="'.$tr_id.'"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )'; 
	        
	    }
	    
	        if($gateway=='STRIPE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
	    }
	    
	        if($gateway=='USAEPAY')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	         
	        
	    }
	      if($gateway=='GLOBAL')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	         
	        
		}
		
		$sql =' UPDATE customer_transaction set transaction_user_status="2" where ( id="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	    
	  if($sql!="")
	  {
	   if($this->db->query($sql))
	   return true;
	   else
	   return false;
	   
	  }
	  return false;
	}
	
	
	
	function update_refund_payment($tr_id, $gateway)
	{
	    
	    if($gateway=='NMI')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (  transactionID="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
	     if($gateway=='AUTH')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
	    }
	    
	      if($gateway=='PAYTRACE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where (transactionID="'.$tr_id.'"  and (transactionType="pay_sale" or transactionType="pay_capture") )'; 
	        
	    }
	    
	       if($gateway=='PAYPAL')
	    {
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )'; 
	        
	    }
	    
	        if($gateway=='STRIPE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
	    }
	    
	        if($gateway=='GLOBAL')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	       
	        
	    }
	        if($gateway=='USAEPAY')
	    {
	           $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="sale" or transactionType="capture") ) ';
	     
	        
		}
		if ($gateway == iTransactGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }
        if ($gateway == FluidGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }
	    if ($gateway == TSYSGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }
		if($gateway=='CardPointe') {
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (  transactionID="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
        
	  	if (strtolower($gateway) == "heartland") {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }

	   if($this->db->query($sql))
	   return true;
	   else
	   return false;
	}


    public function get_due_invoice_data($inv, $mID)
   {
          $res = array(); 
          $this->db->select('inv.Customer_ListID');
          $this->db->from('chargezoom_test_invoice inv');
          $this->db->join('chargezoom_test_customer cst','inv.Customer_ListID=cst.ListID','inner');
          $this->db->where('inv.IsPaid','false');
          $this->db->where('cst.qbmerchantID',$mID);
          $this->db->where_in('inv.TxnID', $inv);
          $query = $this->db->get();

          if($query->num_rows() > 0)
          {
            $res = $query->row_array();
            
          }
   
    return $res;
   }
	
	
		public function get_transaction_details_by_id($userID,$trID)
	{
		$today = date("Y-m-d H:i");
		$res=array();
		$tcode =array('100','200','111','1');
		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
		$this->db->select('tr.id, tr.transactionID,tr.transactionType,tr.transactionCard,tr.customerListID,tr.merchantID,tr.transactionGateway,tr.gatewayID,tr.gateway,tr.transactionAmount,  cust.ListID,  cust.fullName,tr.invoiceTxnID');
	    $this->db->select('ifnull(GROUP_CONCAT(tr.transactionAmount SEPARATOR ","),"") as tr_amount', FALSE);
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR ","),"") as invoice_id', FALSE);
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.qbListTxnID) SEPARATOR "," ),"") as qbListTxnID', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->where("comp.merchantID ", $userID);
	 
	    $this->db->where("tr.merchantID", $userID);
        $this->db->where("comp.merchantID", $userID);
          $this->db->where("tr.transactionID", $trID);
        
        $this->db->where_in('UPPER(tr.transactionType)',$type);
       $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	   $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $result= $query->row_array();
		 
	   
		
		     if(!empty($result['invoice_id']))
		     {
		         
		         $inv =$result['invoice_id'];
		          $res_inv=array();
		     
		     $qq= $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  
		     chargezoom_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");
		     
		    
		      if( $qq->num_rows >0){
		       $res_inv= $qq->row_array()['invoce']; 
		    
		       $result['invoice_no'] =$res_inv;
		  
		      }
		    
		     }
		     $ref_amt=0;
	
		     if($result['transactionID']!="" && strtoupper($result['transactionType'])!=strtoupper('Offline Payment') )
		     {
		     $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$result['transactionID'].'"  group by creditTransactionID  ');
	
		    $data =  $qr->row_array();
		   
		       if(!empty($data))
		         $result['partial']= $data['refundAmount']; 
		       else
		    
		      $result['partial'] =$ref_amt;
		      }
		      else
		     {
		          $result['partial'] =$ref_amt;
		     }
		     
		     
		     $res =$result;
		 } 
	
		return $res;
		
	}
	
	public function get_transaction_data_by_id($TxnID, $userID,$action){
		
		$this->db->select('tr.id, tr.transactionID,sum(tr.transactionAmount)as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, cust.FullName, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID');
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		
	    $this->db->where("cust.qbmerchantID", $userID);
		if($action == 'transaction'){
            $this->db->where("tr.id", $TxnID);
        } else {
            $this->db->where("tr.transactionID", $TxnID);
        }
		
		$query = $this->db->get();
        

		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	public function get_transaction_datarefund($userID)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceTxnID,tr.transaction_user_status,  cust.ListID, cust.FullName,(select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID limit 1) as RefNumber, ');
       
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('chargezoom_test_customer cust', 'tr.customerListID = cust.ListID', 'INNER');
        
        $this->db->where("cust.qbmerchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->not_like("tr.gateway", 'ECheck');

        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();

            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select RefNumber   from   chargezoom_test_invoice where TxnID='" . trim($inv) . "'  limit 1 ");
                       
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['RefNumber'];
                        }
                    }

                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');


                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;
                        
                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }

        return $res;

    }
	public function getPartialNewAmount($transactionID)
    {
        $ref_amt = 0;
        
        
        $qr = $this->db->query('SELECT id FROM customer_transaction WHERE transactionID ="' . $transactionID . '" ORDER BY `id` DESC');
        $data = $qr->result_array();
        
        if (!empty($data)) {
            foreach ($data as $value) {
                $qr1 = $this->db->query('SELECT sum(transactionAmount) as transactionAmount FROM customer_transaction WHERE parent_id ="' . $value['id'] . '" ORDER BY `id` DESC');
                $data1 = $qr1->row_array();
                
                if (!empty($data1)) {
                    $ref_amt = $ref_amt + $data1['transactionAmount'];
                }

            }
            
            return $ref_amt;
            
        } else {
            return $ref_amt;
        }
    }

	public function get_transaction_history_ajax_data($userID, $customerID = null){
		$res = false;

		$this->_transaction_history_ajax($userID, $customerID = null);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
		if ($query->num_rows() > 0) {
            $res1 = $query->result_array();

            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select RefNumber   from   chargezoom_test_invoice where TxnID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['RefNumber'];
                        }
                    }

                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {
                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID  ');

                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;
                        
                    }

                } else {
                    
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }
        return $res;
    }

	public function _transaction_history_ajax($userID, $customerID = null)
	{
		$today = date("Y-m-d H:i");
		$res=array();
		$tcode =array('100','200', '120','111','1');

		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY-SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND','CREDIT');
		
		$columnSearch = [
			'tr.invoiceID',
			'cust.FullName',
			'inv.RefNumber',
			'tr.transactionDate',
			'tr.transactionID',
		];

		$con = ''; $i = 0;

		if ($_POST['search']['value']) {
			foreach ($columnSearch as $item) {
				$searchValue = $_POST['search']['value'];
				if($item == 'tr.transactionDate'){
					$searchValue = date("Y-m-d", strtotime($searchValue));
				}

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $searchValue . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $searchValue . '%"';
                }
				$i++;
            }
        }

		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceTxnID,  cust.ListID, cust.FullName,(select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID limit 1) as RefNumber, ');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->join('chargezoom_test_invoice inv', "inv.TxnID= tr.invoiceTxnID", 'LEFT');
        $this->db->where("comp.merchantID ", $userID);
		$this->db->where("tr.merchantID ", $userID);
		$this->db->where_in('UPPER(tr.transactionType)',$type);
		$this->db->where_in('(tr.transactionCode)',$tcode);
        if(!empty($customerID)){
            $this->db->where("tr.customerListID ", $customerID);
        }

		if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }

        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);

        $orderBY = $_POST['order'][0]['dir'];
		$column = $_POST['order'][0]['column'];

        if($column == 0){
            $orderName = 'cust.fullName';
        }elseif($column == 1){
            $orderName = 'inv.refNumber';
        }elseif($column == 2){
            $orderName = 'transactionAmount';
        }elseif($column == 3){
            $orderName = 'tr.transactionDate'; 
        }elseif($column == 4){
            $orderName = 'tr.transactionType';
        }elseif($column == 5){
            $orderName = 'tr.transactionID';
        }else{
            $orderName = 'tr.transactionDate'; 
			$orderBY = 'desc';   
        }
        $this->db->where("tr.transactionID >", 0);
        $this->db->order_by($orderName, $orderBY);
		$this->db->group_by("tr.transactionID", 'desc');
		

	}

	public function get_transaction_history_ajax_total_data($userID, $customerID = null){
		$this->_transaction_history_ajax($userID, $customerID = null);
        $query = $this->db->get();
        return $query->num_rows();
    }
}






