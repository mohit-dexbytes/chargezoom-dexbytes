<?php
Class Company_model extends CI_Model
{
	
	function general()
	{
		parent::Model();
		$this->load->database();
		
	}
		
    
	public function get_transaction_data($userID){
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
	    $this->db->where("cust.companyID ", $userID);
		
		
		$query = $this->db->get();
     //	echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
/*
	
	public function get_invoice_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.ListID, cust.FullName, 
	  ( case   when  IsPaid ='true'     then 'Success' 
	  when  IsPaid ='true' and userStatus ='Paid'     then 'Success'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus='Active'  then 'Upcoming' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Past Due'
	   
	  else 'Cancel' end ) as status FROM qb_test_invoice inv
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
	  inner join tbl_company t_comp on t_comp.id = cust.companyID
	  WHERE  t_comp.merchantID = '$userID'  and  cust.customerStatus='1' ORDER BY inv.TimeCreated  DESC ");
	
    	//echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
*/

public function get_invoice_data($userID)
{

	$res =array();
 	$today  = date('Y-m-d');
  /*SELECT `inv`.RefNumber as RefNumber, inv.EditSequence, inv.TxnID as TxnID, inv.AppliedAmount, inv.Customer_ListID, inv.DueDate,inv.BalanceRemaining,inv.IsPaid,inv.userStatus,DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.ListID, cust.FullName, */
	  $query  =  $this->db->query("SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.ListID, cust.FullName, 
	  ( case   when  IsPaid ='true'     then 'Success' 
	  when  IsPaid ='true' and userStatus ='Paid'     then 'Success'
	   when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Upcoming' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Past Due'
	   
	  else 'Cancelled' end ) as status, 
	  ( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
	   ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
	  FROM qb_test_invoice inv
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
	  inner join tbl_company t_comp on t_comp.id = cust.companyID
	  LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`TxnID`
	  WHERE  t_comp.merchantID = '$userID'  and  cust.customerStatus='1'    
	   AND (sc.merchantID=  '".$userID."' OR sc.merchantID IS NULL)
       AND (sc.customerID=inv.Customer_ListID OR sc.customerID IS NULL) 
        AND ((sc.customerID=cust.ListID and sc.merchantID=cust.qbmerchantID)OR sc.customerID IS NULL) 
	  ORDER BY inv.TimeCreated  DESC ");
	//echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		//  return  $res=$query->result_array();
		  	//$result = $query->result_array();
               $res= $query->result_array(); 
        
        }

   return $res;
	/*	foreach($ress  as $res)
            {
		    $daata= 	$this->get_select_data('tbl_custom_invoice', array('TxnID','invoicelsID','RefNumber','Customer_ListID','Customer_FullName', 'AppliedAmount','BalanceRemaining','IsPaid', 'userStatus', 'DueDate', 'qb_status'), array('invoicelsID'=>$res['TxnID']) );
		 
            if(!empty($daata))
		  {
            
              $res['ListID']          = $daata['Customer_ListID'];
            
			 $res['RefNumber']        = $daata['RefNumber'];
			 $res['FullName']         = $daata['Customer_FullName'];
			 $res['AppliedAmount']    = $daata['AppliedAmount'];
			 $res['BalanceRemaining'] = $daata['BalanceRemaining'];
             $res['TxnID']            = $daata['invoicelsID'];
              $res['IsPaid']  = $daata['IsPaid'];
              if($daata['qb_status']==2){
            
              $res['c_status']  ='Pending';
             }
                if($daata['qb_status']==1){
           
         
              $res['c_status']  ='Pending';
             }
             if($daata['qb_status']==3){
            
              $res['c_status']  ='Failed';
             }
            
		  }
           $resst[] = $res;
        }
        
        
           $resst1=array();
        
      $sql="SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.ListID, cust.FullName,  inv.invoicelsID as TxnID,
	  ( case   when  IsPaid ='true'     then 'Success' 
	  when  IsPaid ='true' and userStatus ='Paid'     then 'Success'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  then 'Upcoming' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   then 'Past Due'
	   
	  else 'Cancel' end ) as status,  ( case   when  inv.qb_status=0 then 'Pending' when  inv.qb_status=3 then 'Failed' else 'Pending' end) as c_status  FROM tbl_custom_invoice inv
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
	  inner join tbl_company t_comp on t_comp.id = cust.companyID
	  WHERE  t_comp.merchantID = '$userID'  and  cust.customerStatus='1' and  (inv.qb_status=0  || inv.qb_status=3 )  and (inv.invoicelsID='' or inv.invoicelsID IS NULL) ORDER BY inv.TimeCreated  DESC   ";
         $query1 = 	$this->db->query($sql );
        if($query1-> num_rows() > 0)
		{	
        $resst1 =    $query1->result_array(); 
        $resst =  array_merge($resst, $resst1);
       
        }  
        
 // print_r($resst); die; 
        return $resst;		
     
		  
		  
		}
	     return  $res; */
	} 		
	
	
	
	
	public function get_invoice_upcomming_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  `inv`.TxnID, inv.RefNumber, `inv`.AppliedAmount,`inv`.BalanceRemaining, `inv`.DueDate,  
	  DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND  userStatus!='cancel'   and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND  userStatus!='cancel'  then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND  userStatus!='cancel' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  inner join tbl_company t_comp on t_comp.id = cust.companyID	  
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and  
	  t_comp.merchantID = '$userID'  and  cust.customerStatus='1'   order by   DueDate  asc limit 10 ");
	
     //	echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	

	
	public function get_invoice_latest_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active' and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'   then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 

	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   inner join tbl_company t_comp on t_comp.id = cust.companyID	
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and 
	  
	  t_comp.merchantID = '$userID'  and  cust.customerStatus='1'   order by   DATE_FORMAT(inv.TimeModified, '%d-%m-%Y %l.%i%p')  desc limit 10 ");
	
     //	echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	public function get_invoice_data_by_id($invID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and 
	  
	  `cust`.`companyID` = '$userID'   order by   DATE_FORMAT(inv.TimeModified, '%d-%m-%Y %l.%i%p')  desc limit 10 ");
	
     //	echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	public  function  get_invoice_item_data ($invoiceID){
		$res =array();
		
      $sql = "Select itm.*, qb_item.* from  qb_test_invoice_lineitem  itm  inner join qb_test_item qb_item on  itm.Item_ListID = qb_item.ListID where  itm.TxnID = '$invoiceID' ";	
		
	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
		
		
	} 		
	
	

public function get_company_invoice_data_trigger_inv($compID){	
	$res = array();
	$query = $this->db->query("select count(*) as tri_inv_count from qb_test_invoice 
	inner join qb_test_customer cust  on cust.ListID = qb_test_invoice.Customer_ListID  
	inner join tbl_company tcom on tcom.id=cust.companyID   
	WHERE tcom.merchantID = '".$compID."' and customerStatus='1'  and AppliedAmount!='0.00' ");
		if($query->num_rows() > 0){
		
		return  $res=$query->row();
		
		}
	    return false;
	
}		
	
public function get_company_invoice_data_applies_inv($compID){	
	$res = array();
	$query = $this->db->query("select sum(-AppliedAmount) as applied_amount from  qb_test_invoice 
	inner join qb_test_customer cust  on cust.ListID = qb_test_invoice.Customer_ListID 
	inner join tbl_company tcom on tcom.id=cust.companyID   
	WHERE tcom.merchantID = '".$compID."' and userStatus=''  and customerStatus='1'   ");
		if($query->num_rows() > 0){
		
		return  $res=$query->row();
		
		}
	    return false;
	
}	
	
 public function get_company_invoice_data_paynet($compID){
		$res = array();
	
	  $this->db->select("  sum(-inv.AppliedAmount) as net_amount "); 
	  $this->db->from('qb_test_invoice inv');
	  $this->db->join('qb_test_customer cust','cust.ListID = inv.Customer_ListID','inner');
	  $this->db->join('tbl_company tcom','tcom.id=cust.companyID');
	  $this->db->where('tcom.merchantID',$compID);
	   $this->db->where('cust.customerStatus', '1');
	  $query =$this->db->get();
		if($query->num_rows() > 0){
		
		return  $res=$query->row();
		
		}
	    return false;
	}	
	
	

	
	
	
	   public function get_invoice_data_count($condition, $stats){
		
		$num =0;
	    
		$this->db->select('inv.* ');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company cmp','cmp.id = cust.companyID','INNER');
	    $this->db->where($condition);
	   
		$this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
 //echo $this->db->last_query(); 
		$num = $query->num_rows();
		return $num;
	
	} 		
	
	   public function get_invoice_data_count_failed($user_id){
		$today =date('Y-m-d');
		$num =0;
	
	   $sql ="SELECT `inv`.* FROM (`qb_test_invoice` inv) 
	   INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` 
	    INNER JOIN `tbl_company` cmp ON `cust`.`companyID` = `cmp`.`id` 
	   WHERE DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < '$today' AND  `IsPaid` = 'false' 
	   AND userStatus='Active' AND cust.customerStatus='1' AND `cmp`.`merchantID` = '$user_id' 
	   and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'   ";
	
	  $query = $this->db->query($sql);
	  $num   = $query->num_rows();
		return $num;
	
     }	
	 
	 
	 
	
	
		public function get_company_invoice_data_count($compID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as incount  
		  FROM qb_test_invoice inner join qb_test_customer cust  
		  on cust.ListID = qb_test_invoice.Customer_ListID 
		  inner join tbl_company tcom on tcom.id=cust.companyID 
		  WHERE tcom.merchantID = '".$compID."'  AND cust.customerStatus='1'  ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->incount;
		}
		return false;
	     
	}	
		public function get_company_customer_count($compID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as cust_count  FROM qb_test_customer  
		  inner join tbl_company tcom on tcom.id=qb_test_customer.companyID 
		  WHERE   tcom.merchantID = '".$compID."' AND customerStatus='1' ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->cust_count;
		}
		return false;
	}	
	
	
	
	
	
	 public function get_company_invoice_data_payment($compID){
		$res = array();
		  $query  =  $this->db->query('select (select sum(tr.transactionAmount)  from customer_transaction tr 
		inner join qb_test_customer cust on cust.ListID=tr.customerListID 
		inner join tbl_company cmp on cust.companyID=cmp.id  
		inner join tbl_merchant_data mr1 on cmp.merchantID=mr1.merchID
		where mr1.merchID = "'.$compID.'" and   (tr.transactionType ="Sale" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
		or  tr.transactionType ="Auth_capture" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionType ="Offline Payment" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="Refund" or tr.transaction_user_status IS NULL)   and tr.transactionID!=""  and cust.customerStatus="1"  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as total_amount,
		(select sum(tr.transactionAmount) from customer_transaction tr 
		inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id inner join tbl_merchant_data mr1 on cmp.merchantID=mr1.merchID where   mr1.merchID = "'.$compID.'" and cust.customerStatus="1" AND (tr.transactionType = "Refund" OR tr.transactionType = "Pay_refund" OR tr.transactionType = "Credit" ) and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")  and tr.transactionID!=""   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as refund_amount');
		//  echo $this->db->last_query(); die;
		if($query->num_rows()){
		
		return  $res=$query->result_array();
		
		}
	    return false;
	} 

	

	public function get_chart_data($coID){
		
		
    $query = $this->db->query("call get_month_sum_data('".$coID."')	");		
    
	if($query->num_rows() > 0){
		
		  $res=$query->result_array();
		  $chart_arr = array();
		  
		 
		  foreach($res as $chart){
			  
			   $chart_arr['earn_amount'][] = $chart['earn'];
			   $chart_arr['month_total_invoice'][]   = $chart['month_total_invoice'];
			   $chart_arr['inv_month'][]   = $chart['tDate'];
			  
		  }
		 return  $chart_arr; 
		}
		
	}	
	 
	 
	 
	 
	
	  public function get_plan_data_invoice($userID){
		
	$result =array();
	    
		$this->db->select('qb_item.ListID as ListID, qb_item.FullName as FullName ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
		
		$query = $this->db->get();
        //	echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
			$result = $query->result_array();
		}
		return $result;
	
	} 		
    
      public function get_select_data($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
				  //echo $this->db->last_query(); die;
					
					if($query->num_rows()>0 ) {
						$data = $query->row_array();
					   return $data;
					} else {
						return $data;
					}			
			}
	
	   public function get_plan_data($userID){
		
	$result =array();
	    
		$this->db->select('qb_item.Name as Name, qb_item.SalesDesc as Pro_desc, qb_item.FullName as FullName,qb_item.Parent_FullName as Parent_FullName,qb_item.Type as Type, qb_item.SalesPrice as SalesPrice, qb_item.ListID as ListID,qb_item.EditSequence as EditSequence, Discount, qb_item.IsActive ,  (case when qb_item.IsActive="true" then "Done"  when qb_item.IsActive="false" then "Done" else "Done" end) as c_status   ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
		
		$query = $this->db->get();
        //	echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
			$result = $query->result_array();
        
        }
       
       return $result;
     /*          $ress= $query->result_array(); 
		foreach($ress  as $res)
            {
		    $daata= 	$this->get_select_data('tbl_custom_product', array('productID','productName', 'productFullName','productType','IsActive', 'productPrice', 'parentFullName', 'qb_status'), array('productListID'=>$res['ListID'],'merchantID'=>$userID) );
		 
            if(!empty($daata))
		  {
            
              $res['Parent_FullName'] = $daata['parentFullName'];
            
			 $res['Name'] = $daata['productName'];
			 $res['FullName'] = $daata['productFullName'];
			 $res['SalesPrice'] = $daata['productPrice'];
			 $res['Type'] = $daata['productType'];
            $res['productID'] = $daata['productID'];
              $res['IsActive']  = $daata['IsActive'];
              if($daata['qb_status']==2){
            
              $res['c_status']  ='Pending';
             }
                if($daata['qb_status']==1){
           
         
              $res['c_status']  ='Pending';
             }
             if($daata['qb_status']==3){
            
              $res['c_status']  ='Failed';
             }
            
		  }
           $resst[] = $res;
        }
        
        
           $resst1=array();
        
      $sql=' Select qb.productID, qb.productName as Name, qb.productFullName as FullName, qb.parentFullName as Parent_FullName, qb.productType as Type, qb.productPrice as SalesPrice, qb.productListID as ListID, qb.IsActive,  ( case   when  qb.qb_status=0 then "Pending" when  qb.qb_status=3 then "Failed" else "Pending" end) as c_status from  tbl_custom_product qb  inner join tbl_company comp on comp.id=qb.companyID    where comp.merchantID = "'.$userID.'" and  (qb.qb_status=0  || qb.qb_status=3 )  and qb.productListID=""';
         $query1 = 	$this->db->query($sql );
        if($query1-> num_rows() > 0)
		{	
        $resst1 =    $query1->result_array(); 
        $resst =  array_merge($resst, $resst1);
       
        } 
        
    //   print_r($resst); die; 
        return $resst;		
     
        
		}
		return $result;
	   */
	} 		
	
	
	
			
	


    
	   public function get_invoice_data_by_due($condion){
		
		$res =array();
	    
		//$this->db->select('inv.*, cust.*, sum(inv.BalanceRemaining) as balance ');
		$this->db->select("`inv`.Customer_ListID,`inv`.Customer_FullName, cust.FirstName, cust.LastName, `cust`.Contact,`cust`.FullName, (case when cust.ListID !='' then 'Active' else 'InActive' end) as status , `inv`.ShipAddress_Addr1, `cust`.ListID, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		
	    $this->db->where($condion);
        $this->db->group_by('inv.Customer_ListID');
		$this->db->limit(10);
		$this->db->order_by('balance','desc');
	   
		$this->db->where('cust.customerStatus','1');
		$query = $this->db->get();
      	//echo $this->db->last_query();  die;
        if($query->num_rows() > 0){
		 return $res= $query->result_array();
		}	
	
		//print_r($res); die;
		return $res;
	
	} 		
	

     
	   public function get_invoice_data_by_past_due($userID){
		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'   AND userStatus='Active' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `IsPaid` = 'false'  and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'  AND cust.customerStatus='1'      order by  BalanceRemaining   desc limit 10 ");
	
    // echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 		
	
  public function get_invoice_data_by_past_time_due($userID){
		
		$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID,`inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	  DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'   AND userStatus='Active' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'   AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
		    INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  AND `IsPaid` = 'false'  and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'  AND cust.customerStatus='1'   order by  BalanceRemaining   desc limit 10 ");
	
     //	echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 	

	public function get_transaction_failure_report_data($userID){
	    
	    	$res =array();
	    
	    $sql ="SELECT `tr`.`id`, `tr`.`invoiceTxnID`, `tr`.`customerListID`, `tr`.`transactionID`, `tr`.`transactionStatus`, `tr`.`transactionCode`,  `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber, `cust`.`FullName` FROM (`customer_transaction` tr) INNER JOIN `qb_test_customer` cust ON `tr`.`customerListID` = `cust`.`ListID` INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID` WHERE `comp`.`merchantID` = '$userID' AND transactionCode NOT IN ('200','1','100','111') AND `cust`.`customerStatus` = '1'";
		
	
		$query = $this->db->query($sql); 
     //	echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
	 return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
		
  public function get_invoice_data_open_due($userID, $status){
		
		$res =array();
		$con='';
		if($status=='8'){
		 $con.="and `inv`.BalanceRemaining !='0.00' ";
		
		}
		
		
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  `inv`.RefNumber, `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 

	  FROM qb_test_invoice inv 

	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   
	  
	  `comp`.`merchantID` = '$userID'  $con  AND cust.customerStatus='1'  order by  DueDate   asc limit 10 ");
	
     //	echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 	
	
	
	public function get_transaction_report_data($userID, $minvalue, $maxvalue){
			$res =array();
		$trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
		$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber,  ');
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
	    $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where('comp.merchantID ', $userID);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
		$this->db->where('cust.customerStatus','1');
		
		
		$query = $this->db->get();
     //	echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		   return  $res=$query->result_array();
		}
	     return  $res;
		
	} 		
	
	
	
	
	
	
		public function template_data_list($condition){
		
        $res=array();
		$this->db->select('tp.*, typ.* ');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID = tp.templateType','inner');
		$this->db->where($condition);
		
	  	$query = $this->db->get();
    //   echo $this->db->last_query(); die;
		  return $res =  $query->result_array();
		
	} 		
    	
	
		public function template_data($con){
		
	$res=array();
		$this->db->select('tp.*, typ.*');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID=tp.templateType','inner');
	    $this->db->where($con);
		
	  	$query = $this->db->get();
   //    echo $this->db->last_query(); die;
		  return $res =  $query->row_array();
		
	} 		
    	
    	
	   public function get_invoice_due_by_company($condion,$status){
		
		$res =array();
	    $today  = date('Y-m-d');
  		$this->db->select("cust.FullName as label, inv.DueDate,  sum(inv.BalanceRemaining) as balance ");
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->group_by('cust.ListID');
		$this->db->order_by('balance','desc');
		$this->db->limit(10);
		if($status=='1'){
		//	 echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
	      $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '".$today."'  AND `IsPaid` = 'false' AND `userStatus` != 'cancel'  ");
		}
		else if($status=='0'){ 

			$this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d')  AND `IsPaid` = 'false'  AND `userStatus` != 'cancel'  ");
		}
	      
  		  $this->db->where($condion);
		  $this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
    //  echo $this->db->last_query(); 
    if($query->num_rows() > 0)
    {
		$get_result1 = $query->result_array();
		
			      
			            foreach ($get_result1 as $k=> $count_merch) 
			            {
			             $get_result[$k][] = $count_merch['label'];
			            $get_result[$k][] = (float)$count_merch['balance'];
			          
                    
			        }
			      $res=  $get_result ;
		
    }
	//	print_r($res); die;
		return $res;
	} 		
	
	
	
	   public function get_paid_invoice_recent($condion){
		
		$res =array();
	    $today  = date('Y-m-d');

		$this->db->select("cust.FullName,inv.RefNumber,inv.TxnID, inv.Customer_ListID, cust.companyName, (-inv.AppliedAmount) as balance, inv.DueDate, inv.TimeModified ");
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        
		$this->db->order_by('TimeModified','desc');
		$this->db->limit(10);
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today   AND userStatus='Active' and inv.AppliedAmount !='0.00' ");
	    $this->db->where($condion);
		$this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
     // echo $this->db->last_query();  die;
		$res = $query->result_array();
	//	print_r($res); die;
		return $res;
	
	} 		
	
	
	public function get_oldest_due($userID){
		     

		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, inv.Customer_FullName, cust.companyName, inv.Customer_ListID, `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,                  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  AND userStatus='Active'  then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus='Active'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus='Active'   then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  AND userStatus='Active'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE    `IsPaid` = 'false' AND userStatus='Active' and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'   AND customerStatus='1'    order by  inv.TimeModified   asc limit 10 ");
	
     //echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	
		
	}
	
	
	public function get_subcription_data(){
	 $res    = array();
	 $today  = date('Y-m-d');	
	 $date   = date('d');
     $this->db->select('sb.*,cust.FullName, comp.qbwc_username,mer.prefix,mer.postfix,pl.proRate,pl.proRateBillingDay   ');
     $this->db->from('tbl_subscriptions sb');
	 $this->db->join('qb_test_customer cust','sb.customerID = cust.ListID','INNER');
	 $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	 $this->db->join('tbl_subscription_plan_qb pl','pl.planID=sb.subscriptionPlan','LEFT');
	 $this->db->join('tbl_merchant_invoices mer', 'mer.merchantID= sb.merchantDataID','INNER');
	 
	 $this->db->where('sb.nextGeneratingDate = "'.$today.'" ');
	 $this->db->where('cust.customerStatus','1');
 //echo  $this->db->last_query(); die;
	 $query = $this->db->get();
	 //print_r($query);die;
	 if($query->num_rows() > 0){
		return  $res=$query->result_array();
	 }
	 return  $res;

	
	}		
		
	/************Unused methods***********/
	
	
	   public function get_Failure_paymemt_data($coID){
		
		$percent =0;
	    
		$sql = "Select count(*) as total_pay ,
		(select count(*) from customer_transaction where transactionType='Auto-nmi' 
		or transactionType='Sale' or transactionType='Capture' and transactionCode='100' ) as success_pay, 
		(select count(*) from customer_transaction where (transactionType='Auto-nmi' or transactionType='Sale' or transactionType='Capture') and transactionCode !='100' ) as failed_pay from customer_transaction inner join qb_test_customer cust on cust.ListID= customer_transaction.customerListID inner join tbl_company tcom on tcom.id=cust.companyID  WHERE tcom.merchantID = '".$coID."' ";
		$query = $this->db->query($sql);		
		
      
	     $res=$query->row();
		 $total_pay   = $res->total_pay;
	     $success_pay = $res->success_pay;
		 $failed_pay  = $res->failed_pay; 
		 if($success_pay>0 && $failed_pay > 0)
         $percent=number_format((100 * $failed_pay) / $success_pay, 2) ;  
		
		return $percent;
	
	
	} 	
	
	
	
	public function get_product_account_data($con){
		
		$res = array();
		
		$this->db->select("pr.*");
		$this->db->from('qb_item_account pr');
		$this->db->join('tbl_company comp','comp.id = pr.company','INNER');
		$this->db->where($con);
		$this->db->order_by("AccountType",'asc');
		$query = $this->db->get();
	//	echo $query->num_rows(); die;
		if($query->num_rows()>0){
			
			$res = $query->result_array();
		}
		
		
		return $res;
	}
	public function get_product_data($con){
		
		$res = array();
		
		$this->db->select("pr.*");
		$this->db->from('qb_test_item pr');
		$this->db->join('tbl_company comp','comp.id = pr.companyListID','INNER');
		$this->db->where($con);
		$this->db->where("Parent_ListID",'');
		$query = $this->db->get();
	//	echo $query->num_rows(); die;
		if($query->num_rows()>0){
			
			$res = $query->result_array();
		}
		
		
		return $res;
	}
	
	
	
	
	 
	public function get_piechart_due_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate < CURDATE( ) and cust.companyID = '$coID' and IsPaid='false' ";
	

		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
       // $num = $query1->num_rows();  
		return $num;
	
	}	
	
	
   
	public function get_piechart_30_data($coID){
		$num =0;
	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID WHERE inv.DueDate >= DATE_SUB(CURDATE(), INTERVAL 29 Day) and cust.companyID = '".$coID."' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
       // $num = $query1->num_rows();  
		return $num;
	
	}	
	
	
	public function get_piechart_60_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 59 Day ) AND DATE_SUB( CURDATE() ,INTERVAL 30 Day ) and cust.companyID = '$coID' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
       // $num = $query1->num_rows();  
		return $num;
	
	}	
	
	
	public function get_piechart_80_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust 
	on inv.Customer_ListID = cust.ListID where inv.DueDate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 89 Day ) AND DATE_SUB( CURDATE() ,INTERVAL 60 Day ) and cust.companyID = '$coID' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
       // $num = $query1->num_rows();  
		return $num;
	
	}	
	
    
	public function get_piechart_90_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM qb_test_invoice inv inner join qb_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate <= DATE_SUB( CURDATE() ,INTERVAL 90 Day ) and cust.companyID = '$coID'  and IsPaid='false'";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
       // $num = $query1->num_rows();  
		return $num;
	
	}	
		
	public function get_billing_invoice_data($invoice){	
	  
 	$sql = "SELECT * fROM tbl_merchant_billing_invoice where invoice = $invoice";
		
			 $query1 = $this->db->query($sql);
    //   echo $this->db->last_query(); die;
		  return $res =  $query1->result_array();
	}
	
	public function get_subplan_data($planID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT * FROM tbl_subscriptions_plan_qb INNER JOIN tbl_subscription_plan_item_qb ON tbl_subscription_plan_item_qb.planID =  '".$planID."' WHERE tbl_subscriptions_plan_qb.planID = '".$planID."'");
	
     //	echo  $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 




     public function get_plan_data_item($userID){
		
	$result =array();
	    
		$this->db->select('qb_item.ListID as ListID, qb_item.Name as Name ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
      $this->db->where(array('qb_item.Type !='=> 'Group'));
		
		$query = $this->db->get();
        //	echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
			$result = $query->result_array();
		}
		return $result;
	
	} 




   public function get_inv_data($inv_id)
   {
   
   $result=array();
   
   $query =   $this->db->query(" Select tr.*  from customer_transaction tr inner join qb_test_customer q on tr.customerListID=q.ListID  inner join qb_test_invoice inv on tr.invoiceTxnID=inv.TxnID    where tr.invoiceTxnID='".$inv_id."' and  transactionType IN ('Stripe_sale,Sale,Pay_sale,Paypal_sale,Auth_captue') and transactionCode IN ('100,200,1,111') and tr.transaction_user_status !='refund')  ");
   
   	if($query->num_rows() > 0){
	  $result = $query->row_array();
		}
     
   
      return $result;
   
   }
   
   
        
   	  public function chart_general_volume($mID)
    {   
 
    
       $res= array();
		     $months=array();
		 //   for ($i = 0; $i <= 11; $i++) {
		    for ($i = 11; $i >=0 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
            }  
      $chart_arr = array();
   
    
    
     foreach($months as $key=> $month)
	 { 
    
  // $sql = "SELECT Month(transactionDate) as Month,sum(transactionAmount) as volume FROM `customer_transaction` WHERE merchantID = '".$mID."' AND transactionDate >= CURDATE() - INTERVAL 1 YEAR GROUP BY Month(transactionDate)";
$sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  
  customer_transaction tr    
  
  	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
  WHERE tr.merchantID = "'.$mID.'"  AND    
  date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
		or  tr.transactionType ="Auth_capture" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		
			 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
    
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where 
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and     tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
             $earn= $row['volume']- $rfquery->row_array()['rm_amount'];
            
           // $res[] = $row; 
             $res[$key]['volume'] =$earn;
            $res[$key]['Month'] =$month;
        }else{
           
            $res[$key]['volume'] =0.00;
            $res[$key]['Month'] =$month;
        }
     }  
    
    
     
     
   
      return  $res; 
}

   
   	  public function get_recent_volume($mID)
    {   
 
    
       $res= 0.00;
       
       $month = date("M-Y");
		 
    
  // $sql = "SELECT Month(transactionDate) as Month,sum(transactionAmount) as volume FROM `customer_transaction` WHERE merchantID = '".$mID."' AND transactionDate >= CURDATE() - INTERVAL 1 YEAR GROUP BY Month(transactionDate)";
$sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  
  customer_transaction tr    
  
  	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
  WHERE tr.merchantID = "'.$mID.'"  and  
  date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="Auth_capture" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		
			 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
    	inner join	qb_test_customer cust on cust.ListID= tr.customerListID
		inner join tbl_merchant_data mr1 on cust.qbmerchantID=mr1.merchID where 
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.qbmerchantID="'.$mID.'" and  tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
             $earn= $row['volume']- $rfquery->row_array()['rm_amount'];
            
           // $res[] = $row; 
             $res =$earn;
           
        }else{
           
            $res =0.00;
           
        }
        return  $res; 
     }  
    
    
     public function get_schedule_payment_invoice($mID)
      {
          	$res =array();
          //	Invoice | Customer Name | Balance | Due Date | Amount | Scheduled Date
          //$this->db->select(QBO_test_invoice qt inner join 	tbl_scheduled_invoice_payment sc on sc.invoiceID=qt.invoiceID where  sc.scheduleDate!='0000-00-00' and  sc.customerID=qt.CustomerListID and qt.merchantID='$uid' and  sc.merchantID=$uid and qt.BalanceRemaining > 0 ),'0') as schedule
           $this->db->select('cust.ListID,qt.TxnID,qt.RefNumber as RefNumber, cust.FullName,qt.BalanceRemaining, qt.DueDate,qt.AppliedAmount,sc.scheduleDate');
         	$this->db->from('qb_test_invoice qt');
	    	$this->db->join('qb_test_customer cust','qt.Customer_ListID = cust.ListID','INNER'); 
	    	$this->db->join('tbl_scheduled_invoice_payment sc','qt.TxnID = sc.invoiceID','INNER'); 
		 $this->db->where('cust.qbmerchantID',$mID);
		 // $this->db->where('qt.merchantID',$mID);
		 	 $this->db->where('sc.merchantID',$mID);
		  $this->db->where('IsPaid','false');
		  $this->db->where("sc.scheduleDate IS NOT NULL ");
		  
		$query = $this->db->get();
		if($query->num_rows() >0)
		{
		    $res = $query->result_array();
		}
		
	//	print_r($res);die;
		return $res;
      }
      
    
        public function get_schedule_payment_invoice_csv($mID)
      {
          	$res =array();
              $this->db->select('qt.RefNumber as RefNumber, cust.FullName,qt.BalanceRemaining, qt.DueDate,qt.AppliedAmount,sc.scheduleDate');
         	$this->db->from('qb_test_invoice qt');
	    	$this->db->join('qb_test_customer cust','qt.ListID = cust.Customer_ListID','INNER'); 
	    	$this->db->join('tbl_scheduled_invoice_payment sc','qt.TxnID = sc.invoiceID','INNER'); 
		 $this->db->where('cust.qbmerchantID',$mID);
		 // $this->db->where('qt.merchantID',$mID);
		 	 $this->db->where('sc.merchantID',$mID);
		  $this->db->where('IsPaid','false');
		  $this->db->where("sc.scheduleDate != '0000-00-00'  ");
		  
		$query = $this->db->get();
		if($query->num_rows() >0)
		{
		    $res = $query->result_array();
		}
		
	//	print_r($res);die;
		return $res;
      }
      
      
      
    public function get_tax_data($user_id)
   {
    
    $result=array();
       	$this->db->select('tx.*  ');
		$this->db->from('tbl_taxes tx');
	    $this->db->join('tbl_company comp','comp.id = tx.companyID','INNER');
	   
	    $this->db->where("comp.merchantID",$user_id);
    $query = $this->db->get();
    if($query->num_rows() > 0){
			$result = $query->result_array();
		}
		return $result;
      
   
   }


}


