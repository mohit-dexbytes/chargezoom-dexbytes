<?php

class Card_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	
		 $this->db1 = $this->load->database('otherdb', TRUE);
		 $this->db2 = $this->load->database('devdb',TRUE);
	}
	
	
	public function get_ach_card_data($customerID, $merchantID)
	{
	        $card = array();
	        $sql = " SELECT cr.accountName,cr.CardID from customer_card_data cr  where  cr.customerListID='$customerID'  and cr.merchantID='$merchantID' and cr.accountNumber!=''   "; 
			
		
				    $query1 = $this->db1->query($sql);
				    if($query1->num_rows()>0)
                    $card =   $query1->result_array();
				 
				//	$card= $card_datas;
                return  $card;
	    
	}
	  
	  
	     public function get_card_data($customerID){  
  
                       $card = array();
               		  // $this->load->library('encrypt');
             if($this->session->userdata('logged_in')){
    			$merchantID = $this->session->userdata('logged_in')['merchID'];
    		}
    		if($this->session->userdata('user_logged_in')){
    			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
    			}	
		
	   	        
			  
		        $sql = "SELECT CardID,customerCardfriendlyName , CardType, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

				
						 $card_data['CardID'] = $card_data['CardID'];
						 $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
						  $card_data['CardType']  = $card_data['CardType'];
						
				
						 $card[$key] = $card_data;
					   }
				}		
					
                return  $card;

     }
     
     public function get_select_card_data($con)
     {
         $r_data=array();
       $this->db1->select('customerCardfriendlyName,CardType');
       $this->db1->from('customer_card_data');
       $this->db1->where($con);
       $qur = $this->db1->get();
       if($qur->num_rows() > 0)
       {
          $r_data = $qur->row_array();     
       }
	  
	    return $r_data;
     }  
    public function get_card_expiry_data($customerID){  
  
                       $card = array();
               		  // $this->load->library('encrypt');
             if($this->session->userdata('logged_in')){
    			$merchantID = $this->session->userdata('logged_in')['merchID'];
    		}
    		if($this->session->userdata('user_logged_in')){
    			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
    			}	
		
	   	        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID'      "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $card_data['CardNo']  = substr($this->decrypt($card_data['CustomerCard']),-4) ;
						 $card_data['CardID'] = $card_data['CardID'] ;
						 $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						 //$card_data['CardNo']  = substr($this->decrypt($card_data['CustomerCard']),12) ;
						 $card_data['accountNumber'] = $card_data['accountNumber'] ;
						 $card_data['accountName']  = $card_data['accountName'] ;
						 $card_data['CardType'] = $card_data['CardType'] ;
				
						 $card[$key] = $card_data;
					   }
				}		
					
                return  $card;

     }
     
     
     	
	public function get_credit_card_info($compID)
	{
		$card_data=array();
	  
	 // $this->db1 = $this->load->database('otherdb', TRUE);
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
		 $query1   = $this->db1->query($sql);
		$card_data =   $query1->result_array();
		return $card_data;
	}
	
	
	
	public function get_credit_card_info_data($compID){
	    //$this->decrypt();
//$this->db1 = $this->load->database('otherdb', TRUE);
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
    	 $query1   = $this->db1->query($sql);
	    return $query1->result_array();
	}
	
	
   public function get_single_card_data($cardID)
  {  
  
                  $card = array();
               	 // $this->load->library('encrypt');
   
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->decrypt($card_data['CustomerCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						   $card['Billing_Addr1']	 = $card_data['Billing_Addr1'];	
                          $card['Billing_Addr2']	 = $card_data['Billing_Addr2'];
                          $card['Billing_City']	 = $card_data['Billing_City'];
                          $card['Billing_State']	 = $card_data['Billing_State'];
                          $card['Billing_Country']	 = $card_data['Billing_Country'];
                          $card['Billing_Contact']	 = $card_data['Billing_Contact'];
                          $card['Billing_Zipcode']	 = $card_data['Billing_Zipcode'];
                           $card['CardType']	 = $card_data['CardType'];
				}		
					
					return  $card;

       }
 

	
  public function get_expiry_card_data($customerID, $type){  
  
                       $card = array();
               		 //  $this->load->library('encrypt');
					   
			  if($type=='1'){ 
			    /**************Expired Card***********/
			    
		     	$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date 
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day )  order by expired_date asc  "; 
			  }
            
			  if($type=='0'){
				  
				  /**************Expiring SOON Card***********/
				$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where 
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  order by CardID desc  ";  
			 			  
			  }	
				    $query1 = $this->db1->query($sql);
			        $card_data =   $query1->row_array();
					
			        if(!empty($card_data)){  
					      
			             $card['CustomerCard']     = substr($this->decrypt($card_data['CustomerCard']),12); 
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['expiry']    = $card_data['expired_date'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  $card['customerListID'] = $card_data['customerListID'];
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						  
						
					}
					
					return  $card;
					
					
				 
    }
    
    
   public function  last_four_digit_card($cid, $mID)
   {
                       $card = '';
               		  // $this->load->library('encrypt');
               	       $sql =   "Select CustomerCard from  customer_card_data where  customerListID = '".$cid."' and merchantID ='".$mID."'  order by CardID desc  limit 1   ";
               
               		   $query1 = $this->db1->query($sql);
                 //  $card_datas =   $query1->result_array();
                   
                 //  print_r($card_datas);
               		   if($query1->num_rows() > 0)
               		   {
                          $card_data = $query1->row_array();
                           $card= substr($this->decrypt($card_data['CustomerCard']),12);
               		   }
               		   return  $card;
   }
    
    
     public function check_friendly_name($cid, $cfrname)
     {
		         $card = array();
               		//  $this->load->library('encrypt');
					
					
				$query1 = $this->db1->query("Select CardID from customer_card_data where customerListID = '".$cid."' and customerCardfriendlyName ='".$cfrname."' ");
				//	echo $this->db1->last_query();   die;
					$card = $query1->row_array();	
					
					return $card;
		
	}	

	
    public function insert_new_card($card)
    {
		         
               		//   $this->load->library('encrypt');
				
					    
					 
				    	$card_no  = $card['card_number']; 
						$expmonth = $card['expiry'];
						$exyear   = $card['expiry_year'];
						$cvv      = $card['cvv'];
						
						$b_add1   = $card['Billing_Addr1'];
						$b_add2   = $card['Billing_Addr2'];
						$b_city   = $card['Billing_City'];
						$b_state  = $card['Billing_State'];
						$b_country= $card['Billing_Country'];
						$b_contact= $card['Billing_Contact'];
						$b_zip   = $card['Billing_Zipcode'];
						
						$friendlyname = $card['friendlyname'];
				        $merchantID = $this->session->userdata('logged_in')['merchID'];
				        $customer   = $card['customerID'];
				 
			    	      
						$insert_array =  array( 'cardMonth'  =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->encrypt($card_no),
										  'CardCVV'      =>$this->encrypt($cvv), 
										  
										  'Billing_Addr1'	 =>$b_add1,
										  'Billing_Addr2'	 =>$b_add2,
										  'Billing_City'	 =>$b_city,
										  'Billing_State'	 =>$b_state,
										  'Billing_Country'	 =>$b_country,
										  'Billing_Contact'	 =>$b_contact,
										  'Billing_Zipcode'	 =>$b_zip,
										  
										 'customerListID' =>$customer, 
										 'merchantID'     =>$merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
					
					$this->db1->insert('customer_card_data', $insert_array);
					return $this->db1->insert_id();
					
		
	}	
	
	
	
    public function update_card($card,$cid, $cfrname)
    {
		         //$card = array();
               		   $this->load->library('encrypt');
				

                   //  print_r($card); die;
				    	$card_no  = $card['card_number']; 
						$expmonth = $card['expiry'];
						$exyear   = $card['expiry_year'];
						$cvv      = $card['cvv'];
						$b_add1   = $card['Billing_Addr1'];
						$b_add2   = $card['Billing_Addr2'];
						$b_city   = $card['Billing_City'];
						$b_state   = $card['Billing_State'];
						$b_country   = $card['Billing_Country'];
						$b_contact   = $card['Billing_Contact'];
						$b_zip   = $card['Billing_Zipcode'];
						 
						$update_data =  array( 'cardMonth'  =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->encrypt($card_no),
										  'CardCVV'      =>$this->encrypt($cvv), 
										   'Billing_Addr1'	 =>$b_add1,
										  'Billing_Addr2'	 =>$b_add2,
										  'Billing_City'	 =>$b_city,
										  'Billing_State'	 =>$b_state,
										  'Billing_Country'	 =>$b_country,
										  'Billing_Contact'	 =>$b_contact,
										  'Billing_Zipcode'	 =>$b_zip,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );					
						$this->db1->where('customerListID', $cid);
						$this->db1->where('customerCardfriendlyName', $cfrname);
						$this->db1->update('customer_card_data', $update_data);
						return true;
		
	}	
	
	
	
	
	   	
  public function get_merchant_card_expiry_data($merchantID){  
						$card_data = array();    
						$card      = array();
						$this->load->library('encrypt');
				    
			  
	     	 $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where   merchantListID='$merchantID'  "; 
				    $query1 = $this->db1->query($sql);
			        $card_data =   $query1->result_array();
			      
					
					return  $card_data;

 }
 
 	
	 public function get_merchant_single_card_data($merchantCardID){  
  
                  $card = array();
               	  $this->load->library('encrypt');

			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where  merchantCardID='$merchantCardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->decrypt($card_data['MerchantCard']) ;
						 $card['CardMonth']  = $card_data['CardMonth'];
						  $card['CardYear']  = $card_data['CardYear'];
						  $card['merchantCardID']    = $card_data['merchantCardID'];
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  $card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
				}		
					
					return  $card;

       }
 

  
  public function get_single_mask_card_data($cardID)
  {  
  
                  $card = array();
               	//  $this->load->library('encrypt');
        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
                   
                   
                   
				  if(!empty($card_data )){	
					//	str = str.replace(/\d(?=\d{4})/g, "X");
				     	$re = '/\d(?=\d{4})/m';
				     	$str =$this->decrypt($card_data['CustomerCard']);
					   	 $card['CardNo']     =  preg_replace($re, "X", $str); 
					   	 //preg_replace('\d(?=\d{4})', "X",$str);
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						  
						  $card['accountName']  = $card_data['accountName'];
						  $card['accountNumber']  = $card_data['accountNumber'];
						  $card['routeNumber']    = $card_data['routeNumber'];
						  $card['accountType']   = $card_data['accountType'];
						  
						  $card['accountHolderType']    = $card_data['accountHolderType'];
						  $card['secCodeEntryMethod']   = $card_data['secCodeEntryMethod'];
						  
                          $card['Billing_Addr1']	 = $card_data['Billing_Addr1'];	
                          $card['Billing_Addr2']	 = $card_data['Billing_Addr2'];
                          $card['Billing_City']	 = $card_data['Billing_City'];
                          $card['Billing_State']	 = $card_data['Billing_State'];
                          $card['Billing_Country']	 = $card_data['Billing_Country'];
                          $card['Billing_Contact']	 = $card_data['Billing_Contact'];
                          $card['Billing_Zipcode']	 = $card_data['Billing_Zipcode'];
				}		
					
					return  $card;

       }    
       
       
       
       public function create_dev_api_key($ins_data)
       {
          
             
             $this->db2->insert('tbl_merchant_api',$ins_data);
             return $id = $this->db2->insert_id();
           
       }
       
       
         public function delete_dev_api_key($con)
       {
          //   $this->db2 =$this->load->database('devdb',TRUE);
             
             $this->db2->where($con);
             $del =$this->db2->delete('tbl_merchant_api');
             return $del;
           
       }
 
   
    public function chk_card_firendly_name($cid, $friendlyname)
	{
	$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					   if($query->num_rows() >0)
                       {  
					  $crdata =   $query->row_array()['numrow'];
                       }else{
                          $crdata = 0;
                       }
    return $crdata;   
	
	}
   
     public function update_card_data($condition, $card_data=array())
	 {  
	     $status=false;
	     $this->db1->where($condition);
		 $updt = $this->db1->update('customer_card_data',$card_data);
		 if($updt)
		 {
            $status =true;
            $this->db1->select('CardID');
            $this->db1->from('customer_card_data');
             $this->db1->where($condition);
             $query = $this->db1->get();
             if($query->num_rows() >0)
            {  
             $card =  $query->row_array();
             return   $card['CardID'];
            }
            return $status;
            
		 }	 
		 return $status;

	 }	 
	 
	 
     public function delete_card_data($condition)
	 {  
	     $status=false;
	     $this->db1->where($condition);
		 $updt = $this->db1->delete('customer_card_data');
		 if($updt)
		 {
            $status =true;
		 }	 
		 return $status;

	 }	 
	 
	  public function insert_card_data($card_data)
	 {  
	     $status=false;
      
     //    print_r($card_data);
	   
		 $this->db1->insert('customer_card_data',$card_data);
  //echo $this->db1->last_query();  die;
		
		 return $status = $this->db1->insert_id();
		 

	 }	 
	 

	
function sign($message, $key) {

    return hash_hmac('sha256', $message, $key) . $message;
}

function verify($bundle, $key) {
    return hash_equals(
      hash_hmac('sha256', mb_substr($bundle, 64, null, '8bit'), $key),
      mb_substr($bundle, 0, 64, '8bit')
    );
}

function getKey($password, $keysize = 40) {
    return hash_pbkdf2('sha256',$password,'some_token',100000,$keysize,true);
}

function encrypt($message) {
     
   if(empty($message))
	return null;
	$password= ENCRYPTION_KEY;
    $iv = random_bytes(16);
    $key = $this->getKey($password);
    $result = $this->sign(openssl_encrypt($message,'aes-256-ctr',$key,OPENSSL_RAW_DATA,$iv), $key);
    return bin2hex($iv).bin2hex($result);
}

function decrypt($hash) {

if(empty($hash))
	return null;
	$password= ENCRYPTION_KEY;
    $iv = hex2bin(substr($hash, 0, 32));
    $data = hex2bin(substr($hash, 32));
    $key = $this->getKey($password);
    if (!$this->verify($data, $key)) {
      return null;
    }
    return openssl_decrypt(mb_substr($data, 64, null, '8bit'),'aes-256-ctr',$key,OPENSSL_RAW_DATA,$iv);
}

	 



public function process_card($card_data)
{

 $card_no =''; $cvv =''; $card_type ='';$friendlyname  ='';
 								 $card_no = $card_data['CustomerCard'] ;
   								 $cvv    =$card_data['CardCVV'] ;
										
                    				 		$card_type      =$card_data['CardType'] ;
                    				        $friendlyname   =  $card_type.' - '.substr($card_no,-4);
                                         $customerID = 	$card_data['customerListID'];
                    						$card_condition = array(
                    										 'customerListID' =>$card_data['customerListID'], 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    					
                    						$crdata =	$this->chk_card_firendly_name($customerID,$friendlyname)	;			
                    					     
                    				   
                    						if($crdata >0)
                    						{
                    							
                    						  
                    										   // $card_data ['CardType']     =$card_type;
                    										 $card_data ['CustomerCard'] =$this->encrypt($card_no);
                    										 $card_data ['CardCVV']      =$this->encrypt($cvv);
                    										 $card_data ['updatedAt']    = date("Y-m-d H:i:s");
                    										  
                    								
                    										  
                    							     		  
                    					
                    							$id1 = 		   $this->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					     	
                                                   			//	 $card_data ['CardType']     =$card_type;
                    										 $card_data ['CustomerCard'] =$this->encrypt($card_no);
                    										 $card_data ['CardCVV']      =$this->encrypt($cvv);
                    										 $card_data ['customerCardfriendlyName']=$friendlyname;
                    										  $card_data ['createdAt'] 	= date("Y-m-d H:i:s");
                    									
                    						
                    				            $id1 =    $this->insert_card_data($card_data);	
                    				          
                    						
                    						}

         return $id1;
                    			

}
	 
	 
           
 
}