<?php
Class Customer_Login_model extends CI_Model
{
	private $tbl_user = 'qb_test_customer'; // user table name 
	function Login()
	{
		parent::Model();
	}


    
	function check_domain($con){
	    $res = array();
		$this->db->select('cs.* ');
		$this->db->from('tbl_config_setting cs');
		$this->db->join('tbl_merchant_data md','md.merchID=cs.merchantID','inner');
		$this->db->join('tbl_company comp', 'comp.merchantID = md.merchID', 'inner');
		$this->db->join('qb_test_customer cust','cust.companyID = comp.id', 'inner');
		$this->db->where('customerPortal','1');
		$this->db->where($con);
		$query = $this->db->get();
		return $query->num_rows();
    }	
	
	

	// To check the login

	function customer_logout()
	{
		
		 
		$this->db->trans_start();
		$session_data = $this->session->userdata('customer_logged_in'); 
		$this->db->where('loginID', $session_data['loginID']); 
		$this->db->update('tbl_customer_login',array('is_logged_in'=>'0')); 
		$this->session->unset_userdata('customer_logged_in'); 
		$this->session->sess_destroy(); 
		$this->db->trans_complete(); 
		return $this->db->trans_status();

	}

	
	public function get_customer_login_details($con){
		$res = array();
		$this->db->select('cust.*, cl.*');
		$this->db->from('tbl_customer_login cl');
		$this->db->join('qb_test_customer cust','cust.Contact=cl.customerEmail', 'inner');
		$this->db->where($con);
		$query = $this->db->get();
	 echo $this->db->last_query(); 
		if($query->num_rows()>0){
		$res = $query->row_array();
		}
		return $res;
	}
	
	
	
	
	//Forgot Password 
	
	
	function temp_reset_customer_password($code,$email)
	{
		$this->db->where('customerEmail', $email);
	    $this->db->set('customerPassword',md5($code));
	    if($this->db->update('tbl_customer_login'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	

	// Admin Change Password
	
	function savenewpass($id , $password)
	{
		if (isset($id) && $id != '')
		{
		 $this -> db -> where('loginID', $id);
		 $this -> db -> set('customerPassword', md5($password));
		 $query = $this -> db -> update('tbl_customer_login');
		 
		 if ($query) 
			return true;
		 else
			return false;
		}
	 }
	 
	 
	 
}