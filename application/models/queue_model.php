<?php
class Queue_model extends CI_Model {

    
    const QUEUE_PENDING = 'Pending';
    const QUEUE_INPROGRESS = 'Inprogress';
    const QUEUE_COMPLETED = 'Completed';
    const QUEUE_FAILED = 'Failed';
    const QUEUE_CANCELED = 'Canceled';
    const QUEUE_PAUSED = 'Paused';

    protected $table = 'tbl_merchant_queues';

    public function __construct()
   	{
        parent::__construct();
   	}

    /**
     * Get Merchant Queue function
     *
     * @param integer $merchantID
     * @param string|array $queueStatus
     * @return array
     */
    public function getMerchantQueues(int $merchantID, $queueStatus=null)
    {
        $this->db->select('q.*');
        $this->db->from($this->table.' as q');
        $this->db->where('q.merchantID', $merchantID);

        if($queueStatus)
        {
            if(is_array($queueStatus)) {
                $this->db->where_in('q.queue_status', $queueStatus);
            } else {
                $this->db->where('upper(q.queue_status)', strtoupper($queueStatus));
            }
            
        }
        
        $this->db->order_by('q.created_date', 'ASC');

        $query = $this->db->get();
        if($query->num_rows){
            return $query->result_array();
        } else {
            return false;
        }
		
    }

    /**
     * Get Next Queue function
     *
     * @param string $queueStatus
     * @return array
     */
    public function getNextQueue(string $queueStatus= self :: QUEUE_PENDING ) 
    {
        $this->db->select('q.*');
        $this->db->from($this->table.' as q');
        $this->db->where('upper(q.queue_status)', strtoupper($queueStatus));
        $this->db->group_by('q.merchantID');
        $this->db->order_by('q.created_date', 'ASC');

        $query = $this->db->get();
        if($query->num_rows){
            return $query->row();
        } else {
            return false;
        }
		
    }


    /**
     * Create Queue
     *
     * @param integer $merchantID
     * @param string $operation
     * @param string $status
     * @param string $offset
     * @param array $request_payload
     * @param array $response_payload
     * @return array
     */
    public function addQueue(int $merchantID, string $operation='', string $status=self :: QUEUE_PENDING, int $offset = 0, array $request_payload = null, array $response_payload = null) : int
    {
        $data = [
            'merchantID' => $merchantID,
            'queue_status' => $status,
            'queue_offset' => $offset,
            'queue_operation' => $operation,
            'queue_request_payload' => !empty($request_payload) ? json_encode($request_payload) : null,
            'queue_response_payload' => !empty($response_payload) ? json_encode($response_payload) : null,
            'created_date' => date('Y-m-d H:i:s'),
            'updated_date' => date('Y-m-d H:i:s')
        ];

        if($this->db->insert($this->table, $data)) {
			return $this->db->insert_id();
		} else {
			return 0;
		}
    }

    /**
     * Update Queue Status function
     *
     * @param integer $queueID
     * @param string $queueStatus
     * @param integer $queueOffset
     * @param arrat $response_payload
     * @return void
     */
    public function updateQueueStatus(int $queueID, string $queueStatus, int $queueOffset=0, array $response_payload=null) : void
    {

        $where = [
            'id' => $queueID
        ];

        $update = ['queue_status' => $queueStatus, 'updated_date' => date('Y-m-d H:i:s')];

        if($queueOffset)
        {
            $update['queue_offset'] = $queueOffset;
        }

        if($response_payload)
        {
            $update['queue_response_payload'] = json_encode($response_payload);
        }

        $this->db->where($where);
        $this->db->update($this->table, $update);
    }

    public function checkRecentCompletedQueue(int $merchantID, int $seconds=60)
    {
        $from = date('Y-m-d H:i:s', (time()-$seconds));
        $to = date('Y-m-d H:i:s');

        $this->db->select('count(q.id) c');
        $this->db->from($this->table.' as q');
        $this->db->where('q.merchantID', $merchantID);
        $this->db->where('upper(q.queue_status)', 'COMPLETED');        
        $this->db->where('q.updated_date BETWEEN '.$this->db->escape($from).' AND '.$this->db->escape($to));

        $query = $this->db->get();
        if($query->num_rows){
            return $query->row()->c;
        } else {
            return 0;
        }
    }
}