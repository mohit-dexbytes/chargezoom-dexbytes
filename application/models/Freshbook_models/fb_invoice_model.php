<?php

class Fb_invoice_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public $table1 = 'Freshbooks_test_invoice qb';
    public $order1 = array('BalanceRemaining' => 'desc');
    public $column1 = array('cs.fullName', 'qb.refNumber', 'qb.DueDate', 'qb.Total_payment', 'qb.BalanceRemaining', 'qb.invoiceID');

    public function get_fb_invoice_data($table, $con)
    {
        $data = array();
        $this->db->select('*');
        $this->db->from($table);
        if (!empty($con)) {
            $this->db->where($con);
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data;
        }
    }

    public function get_invoice_details($uid)
    {
        $sqlQuery = "SELECT
            COUNT(*) as incount,
            IFNULL(
                ( Select COUNT(*)
                    FROM Freshbooks_test_invoice fti
                    INNER JOIN Freshbooks_custom_customer fcc ON fcc.customer_ListID = fti.CustomerListID AND fcc.merchantID = $uid
                    WHERE fti.merchantID = $uid  AND BalanceRemaining = 0
                ),0
            ) as Paid,
            IFNULL(
                (
                    Select COUNT(*)
                    FROM Freshbooks_test_invoice fti
                    INNER JOIN Freshbooks_custom_customer fcc ON fcc.customer_ListID = fti.CustomerListID AND fcc.merchantID = $uid
                    INNER JOIN 	tbl_scheduled_invoice_payment tsip ON tsip.invoiceID = fti.invoiceID
                    WHERE  tsip.scheduleDate != '0000-00-00' AND  tsip.customerID = fti.CustomerListID AND fti.merchantID = '$uid' AND  tsip.merchantID = $uid AND fti.BalanceRemaining > 0
                ),'0'
            ) as schedule,
            IFNULL(
                (
                    Select COUNT(*)
                    FROM Freshbooks_test_invoice fti
                    INNER JOIN Freshbooks_custom_customer fcc ON fcc.customer_ListID = fti.CustomerListID AND fcc.merchantID = $uid
                    INNER JOIN customer_transaction tr ON tr.invoiceID = fti.invoiceID
                    WHERE  fti.merchantID = $uid AND tr.merchantID = $uid AND ( tr.transactionCode = '300' )
                ),'0'
            ) as Failed
            FROM Freshbooks_test_invoice fti
            INNER JOIN Freshbooks_custom_customer fcc ON fcc.customer_ListID = fti.CustomerListID AND fcc.merchantID = $uid
            WHERE fti.merchantID= $uid
        ";
        $res   = array();
        $query = $this->db->query($sqlQuery);
        if ($query->num_rows() > 0) {

            $res = $query->row();
            return $res;
        }
        return false;
    }

    public function get_invoice_list_data($mID, $customerID = null)
    {
        $today = date('Y-m-d');
        $res   = array();

       
        $sql = "SELECT `qb`.*, `cs`.`fullName` AS custname,
                (case   when (sc.scheduleDate!='0000-00-00' and BalanceRemaining!='0.00')
                THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'
                AND IsPaid='0') THEN 'Open'
                when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and BalanceRemaining!='0.00')  THEN 'Overdue'
                when (BalanceRemaining='0.00' ) Then 'Paid'
                ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
            FROM (`Freshbooks_test_invoice` qb)
            INNER JOIN `Freshbooks_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID`
            LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`qb`.`invoiceID`
            WHERE qb.merchantID = '" . $mID . "' AND `cs`.`merchantID` = '" . $mID . "'
        ";

        if($customerID){
            $sql .= " AND `qb`.`CustomerListID` = '".$customerID."' ";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            $res = $query->result_array();
        }

        return $res;
    }
    public function get_invoice_data_by_status($mID,$status, $customerID = null)
    {
        $today = date('Y-m-d');
        $res   = array();
        if($status == 'Past Due'){
			$cond = "DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and BalanceRemaining!='0.00' and (select count(*) as sch_count from customer_transaction where invoiceTxnID = qb.TxnID limit 1 )='0'";
		}
		if($status == 'Failed'){
			$cond = "DATE_FORMAT(qb.DueDate, '%Y-%m-%d') < '$today' and qb.IsPaid = 'false'   and userStatus!='cancel' and (select transactionCode from customer_transaction where invoiceTxnID =qb.TxnID limit 1 )='300'";
		}
		if($status == 'Paid'){
			$cond = "IsPaid ='1'";
		}
		if($status == 'Cancelled'){
			$cond = "userStatus='cancel'";
        }
        $this->db->cache_on();
        $sql = "SELECT `qb`.*, `cs`.`fullName` AS custname,
                (case   when (sc.scheduleDate!='0000-00-00' and BalanceRemaining!='0.00')
                THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'
                AND IsPaid='0') THEN 'Open'
                when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and BalanceRemaining!='0.00')  THEN 'Overdue'
                when (BalanceRemaining='0.00' ) Then 'Paid'
                ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
            FROM (`Freshbooks_test_invoice` qb)
            INNER JOIN `Freshbooks_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID`
            LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`qb`.`invoiceID`
            WHERE qb.merchantID = '" . $mID . "' AND `cs`.`merchantID` = '" . $mID . "' AND($cond) ";
        if($customerID){
            $sql .= " AND `qb`.`CustomerListID` = '".$customerID."' ";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            $res = $query->result_array();
        }
        $this->db->cache_off();
        return $res;
    }
    public function get_open_invoice_data($mID, $customerID = null)
    {
        $today = date('Y-m-d');
        $res   = array();
        $this->db->cache_on();
        $sql = "SELECT `qb`.*, `cs`.`fullName` AS custname,
                (case   when (sc.scheduleDate!='0000-00-00' and BalanceRemaining!='0.00')
                THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'
                AND IsPaid='0') THEN 'Open'
                when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and BalanceRemaining!='0.00')  THEN 'Overdue'
                when (BalanceRemaining='0.00' ) Then 'Paid'
                ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
            FROM (`Freshbooks_test_invoice` qb)
            INNER JOIN `Freshbooks_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID`
            LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`qb`.`invoiceID`
            WHERE qb.merchantID = '" . $mID . "' AND `cs`.`merchantID` = '" . $mID . "' AND (userStatus!='cancel' and qb.IsPaid = '0')
        ";

        if($customerID){
            $sql .= " AND `qb`.`CustomerListID` = '".$customerID."' ";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            $res = $query->result_array();
        }
        $this->db->cache_off();
        return $res;
    }
    

    public function get_item_data($con)
    {
        $res = array();
        $this->db->select(' *, (select Name from Freshbooks_test_item where productID =tbl_freshbook_invoice_item.itemRefID and  merchantID="' . $con['merchantID'] . '")as Name,
       (select SalesDescription from Freshbooks_test_item where productID =tbl_freshbook_invoice_item.itemRefID and  merchantID="' . $con['merchantID'] . '")as itemDescription  ');
        $this->db->from('tbl_freshbook_invoice_item');
       
        $this->db->where($con);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_datatables_invoices($condition, $filter)
    {

        $this->_get_datatables_invoices_query($condition, $filter);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        $data = array('result' => $query->result());
        return $data;
    }

    private function _get_datatables_invoices_query($condition, $filter)
    {
        if($filter == ''){
            $filter = 'open';
        }
        if ($filter != 'All') {
            if ($filter == 'open') {
                $cond = 'qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="unpaid"';
            }
            if ($filter == 'Past Due') {

                $cond = 'qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="unpaid"';
            }

            if ($filter == 'Paid') {
                $cond = 'qb.IsPaid=' . "1";
            }
            if ($filter == 'Partial') {
                $cond = 'qb.IsPaid=0 AND qb.BalanceRemaining > 0 AND qb.AppliedAmount != 0';
            }
            if ($filter == 'Unpaid') {
                $cond = 'qb.IsPaid=0 AND qb.BalanceRemaining > 0';
            }
            if ($filter == 'Cancelled') {
                $cond = 'qb.UserStatus="1"';
            }
            if ($filter == 'Failed') {
                $cond = 'qb.merchantID=' . $condition['qb.merchantID'] . ' AND tr.transactionID != "" and tr.merchantID=' . $condition['qb.merchantID'] . ' and (tr.transactionCode ="300")';
            }


            $this->db->select('tr.gateway as payment_gateway, qb.invoiceID,qb.txnID as ID,qb.refNumber,qb.CustomerListID, qb.DueDate,qb.Total_payment,qb.BalanceRemaining, qb.UserStatus, cs.fullName AS custname,
            qb.IsPaid,
			 (case when (sc.scheduleDate IS NOT NULL and qb.IsPaid="0" and qb.UserStatus="unpaid" ) THEN "Scheduled" when
				(qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="unpaid" ) THEN "Open"
                WHEN qb.IsPaid=0 AND qb.BalanceRemaining > 0 AND qb.AppliedAmount != 0 THEN "Partial"
                WHEN qb.IsPaid=0 AND qb.BalanceRemaining THEN "Unpaid"
					when (qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="unpaid"  ) THEN "Overdue"
					when (qb.merchantID=' . $condition['qb.merchantID'] . ' and tr.merchantID=' . $condition['qb.merchantID'] . ' and (tr.transactionCode ="300") AND qb.IsPaid !="1" ) THEN "Failed"
					when (qb.IsPaid="1" ) Then "Paid" ELSE "Voided" END) as status
			');
            $this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
            $this->db->from("Freshbooks_test_invoice qb");

            $this->db->join('Freshbooks_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
            $this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID AND tr.merchantID = qb.merchantID', 'left');

            $this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');

            $this->db->where('qb.merchantID', $condition['qb.merchantID']);
            $this->db->where('cs.merchantID', $condition['qb.merchantID']);

            $this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL )
		and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL)
		and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) and ' . $cond . ' ');
        }
        if ($filter == 'All') {
            $this->db->select('qb.invoiceID,qb.txnID as ID,qb.refNumber,qb.CustomerListID, qb.DueDate,qb.Total_payment,qb.BalanceRemaining, qb.IsPaid, qb.UserStatus, cs.fullName AS custname,
            (case when (sc.scheduleDate IS NOT NULL and qb.IsPaid="0" and qb.UserStatus="unpaid" ) THEN "Scheduled" when
            (qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="unpaid" ) THEN "Open"
                when (qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="unpaid"  ) THEN "Overdue"
                when (qb.merchantID=' . $condition['qb.merchantID'] . ' and tr.merchantID=' . $condition['qb.merchantID'] . ' and (tr.transactionCode ="300") AND qb.IsPaid !="1" ) THEN "Failed"
                when (qb.IsPaid="1" ) Then "Paid" ELSE "Voided" END) as status
			');
            $this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
            $this->db->from("Freshbooks_test_invoice qb");

            $this->db->join('Freshbooks_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
            $this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID ', 'left');

            $this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');

            $this->db->where('qb.merchantID', $condition['qb.merchantID']);
            $this->db->where('cs.merchantID', $condition['qb.merchantID']);

            $this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL )
		and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL)
		and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');
        }

        $i   = 0;
        $con = '';

        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        if($customerID){
            $this->db->where('qb.CustomerListID', $customerID);
        }

        foreach ($this->column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $column = $_POST['order'][0]['column'];
        if($column == 0){
            $orderName = 'cs.fullName';
        }elseif($column == 1){
            $orderName = 'cs.fullName';
        }elseif($column == 2){
            $orderName = 'qb.refNumber';
        }elseif($column == 3){
            $orderName = 'qb.DueDate';
        }elseif($column == 4){
            $orderName = 'qb.BalanceRemaining';
        }elseif($column == 5){
            $orderName = 'status';
        }else{
            $orderName = 'qb.DueDate';    
        }
        $orderBY = $_POST['order'][0]['dir'];

        $this->db->order_by($orderName, $orderBY);
        $this->db->group_by('qb.invoiceID');
    }

    public function count_all_invoices($condition)
    {

       
        $this->db->select('*');

        $this->db->from("Freshbooks_test_invoice qb");

        $this->db->join('Freshbooks_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
       
        $this->db->where('qb.merchantID', $condition['qb.merchantID']);
        $this->db->where('cs.merchantID', $condition['qb.merchantID']);
        $query = $this->db->get();
        $count = $query->num_rows();
        return $count;
    }

    public function count_filtered_invoices($condition, $filter)
    {

        $this->_get_datatables_invoices_query($condition, $filter);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_invoice_item_data($invoiceID, $merchantID)
	{
        $res=array();
	
		$this->db->select('INV.itemLineID, INV.invoiceID, INV.itemID, INV.itemName, INV.merchantID, INV.totalAmount, INV.itemQty, INV.itemDescription, INV.itemPrice,  INV.createdAt, INV.taxValue, INV.taxPercent, INV.itemRefID, INV.releamID, INVT.taxName, INVT.item_tax_id, INVT.taxAmount, INVT.taxNumber');
		$this->db->from('tbl_freshbook_invoice_item INV');
		$this->db->join('tbl_freshbook_invoice_item_tax INVT','INVT.itemLineID = INV.itemLineID AND INVT.merchantID = INV.merchantID','LEFT');
	    $this->db->where("INV.invoiceID = '$invoiceID' AND INV.merchantID = '$merchantID'");		
        
		$query = $this->db->get();
		if($query->num_rows() > 0){
            $res = $query->result_array();
		}

		return  $res;
	}

    public function get_subscription_item_data($subscriptionID, $merchantID)
	{
        $res=array();
	
		$this->db->select('FBSI.*, FBSIT.taxID');
		$this->db->from('tbl_subscription_invoice_item_fb FBSI');
		$this->db->join('tbl_freshbook_subscription_item_tax FBSIT','FBSIT.itemID = FBSI.itemID AND FBSIT.subscriptionID = FBSI.subscriptionID','LEFT');
	    $this->db->where("FBSI.subscriptionID = '$subscriptionID'");		
        
		$query = $this->db->get();
		if($query->num_rows() > 0){
            $l_items = $query->result_array();

            foreach($l_items as $singleItem){
                $itemID = $singleItem['itemID'];
                
                $taxItems = [];
                if(isset($res[$itemID])){
                    $taxItems = $res[$itemID]['taxItems'];
                }

                $res[$itemID] = $singleItem;
                if($singleItem['taxID'] !== null && $singleItem['taxID']){
                    $taxItems[] = $singleItem['taxID'];
                }

                $res[$itemID]['taxItems'] = $taxItems; 
            }
		}

		return  $res;
	}

    public function get_subscription_invoice_data($userID, $subID)
	{
	
		$this->db->select('qb.invoiceID,qb.refNumber,qb.CustomerListID, qb.DueDate,qb.Total_payment,qb.BalanceRemaining, qb.AppliedAmount, qb.IsPaid, qb.UserStatus, cs.fullName AS custname,
            (case when (sc.scheduleDate IS NOT NULL and qb.IsPaid="0" and qb.UserStatus="unpaid" ) THEN "Scheduled" when
            (qb.DueDate >= CURDATE() AND qb.IsPaid="0" and qb.UserStatus="unpaid" ) THEN "Open"
                when (qb.DueDate < CURDATE() and qb.IsPaid="0" and qb.UserStatus="unpaid"  ) THEN "Overdue"
                when (qb.merchantID=' . $userID . ' and tr.merchantID=' . $userID . ' and (tr.transactionCode ="300") AND qb.IsPaid !="1" ) THEN "Failed"
                when (qb.IsPaid="1" ) Then "Paid" ELSE "Voided" END) as status
		');
		$this->db->select('(ifnull(sc.scheduleDate, qb.DueDate)) as DueDate', false);
		$this->db->from("Freshbooks_test_invoice qb");
		$this->db->join('tbl_subscription_auto_invoices TSAI', "TSAI.invoiceID = qb.invoiceID AND TSAI.subscriptionID = $subID AND app_type = 3", 'inner');

		$this->db->join('Freshbooks_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
		$this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID ', 'left');

		$this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');

		$this->db->where('qb.merchantID', $userID);
		$this->db->where('cs.merchantID', $userID);

		$this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL ) and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL) and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');
		$this->db->group_by('qb.invoiceID');
	
		$query = $this->db->get();
	   	return $query->result_array();
	}

    public function get_subscription_revenue($userID, $subID)
	{
	
		
		$this->db->select(' (SUM(qb.Total_payment) - SUM(qb.BalanceRemaining) ) as revenue', false);
		$this->db->from("Freshbooks_test_invoice qb");
		$this->db->join('tbl_subscription_auto_invoices TSAI', "TSAI.invoiceID = qb.invoiceID AND TSAI.subscriptionID = $subID AND app_type = 1", 'inner');

		$this->db->join('Freshbooks_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
		$this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID ', 'left');

		$this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');

		$this->db->where('qb.merchantID', $userID);
		$this->db->where('cs.merchantID', $userID);

		$this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL ) and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL) and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');
		$this->db->group_by('qb.invoiceID');
	
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			$res = $query->row_array(); 
			return $res['revenue'];
		}
		
	   	return 0;
	}
}
