<?php

class Fb_subscription_model extends CI_Model
{

    public function get_subscriptions_data($userID)
    {

        $this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
      
        $this->db->join('tbl_subscription_plan pl', 'pl.planID = sbs.subscriptionPlan', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->where("sbs.merchantDataID ", $userID);
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

	}
	
    public function get_invoice_data_fresh_auto_pay()
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select('inv.BalanceRemaining, inv.Total_payment, inv.invoiceID, inv.DueDate,cust.Customer_ListID,inv.IsPaid,
		tci.cardID, tmg.gatewayType,tmg.gatewayType,tmg.gatewayUsername,tmg.gatewayPassword,tmg.gatewaySignature,tci.scheduleDate as schedule, tci.scheduleAmount, cust.merchantID, mr.resellerID');
        $this->db->from('Freshbooks_test_invoice inv');
        $this->db->join('tbl_scheduled_invoice_payment tci', 'inv.invoiceID =tci.invoiceID', 'LEFT');
        $this->db->join('Freshbooks_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        
        $this->db->join('tbl_merchant_gateway tmg', 'tci.gatewayID = tmg.gatewayID', 'INNER');
        $this->db->join('tbl_merchant_data mr', 'mr.merchID=cust.merchantID', 'inner');
        $this->db->where("(DATE_FORMAT(tci.scheduleDate,'%Y-%m-%d')='$today' or (DATE_FORMAT(inv.DueDate,'%Y-%m-%d') ='$today' and tci.scheduleDate IS NULL)) ");
        $this->db->where('inv.IsPaid', '0');
        $this->db->where("inv.BalanceRemaining != ", '0.00');
        $this->db->where("inv.UserStatus != ", '1');

        $this->db->where('cust.customerStatus', 'true');
        $this->db->where('cust.merchantID = inv.merchantID ');

       
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }

        return $res;
	}
	
    public function get_subscriptions_plan_data_old($userID, $cust = '')
    {

       
        $this->db->select('sbs.planID,sbs.subscriptionID,sbs.startDate,sbs.subscriptionAmount, sbs.customerID, sbs.merchantDataID,cust.fullName, cust.companyName, tmg.gatewayMerchantID,tmg.gatewayFriendlyName,  spl.planName as sub_planName, c.customerCardfriendlyName');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_company comp', 'comp.id = cust.companyID', 'INNER');
       
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join('tbl_subscriptions_plan_fb spl', 'spl.planID = sbs.planID', 'Left');
        $this->db->join('cznet_chargezoom_card_db.customer_card_data c', 'c.CardID=sbs.cardID', 'left');
        $this->db->where("comp.merchantID ", $userID);
       
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
       
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_subscriptions_plan_data($userID)
    {
        $cardDb = getenv('DB_DATABASE2');	

        
        $this->db->select('sbs.planID,sbs.subscriptionID,sbs.startDate,sbs.nextGeneratingDate,sbs.subscriptionAmount, sbs.customerID, sbs.merchantDataID,cust.fullName, cust.companyName, tmg.* ,  spl.planName as sub_planName, c.customerCardfriendlyName');

        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');

        
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join('tbl_subscriptions_plan_fb spl', 'spl.planID = sbs.planID', 'left');
        $this->db->join("$cardDb.customer_card_data c", 'c.CardID=sbs.cardID', 'left');
       
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
      
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_customers_data($merchantID)
    {
        $sql = 'SELECT * from Freshbooks_custom_customer WHERE merchantID = "' . $merchantID . '"';

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }

    public function get_subcription_data()
    {
        $res   = array();
        $today = date('Y-m-d');
        $date  = date('d');
        $this->db->select('sb.*, cust.fullName,mr.resellerID as resellerID ');
        $this->db->from('tbl_subscriptions_fb sb');
        $this->db->join('Freshbooks_custom_customer cust', 'sb.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_merchant_data mr', 'sb.merchantDataID = mr.merchID', 'INNER');
        $this->db->where('sb.nextGeneratingDate = "' . $today . '" ');
        $this->db->where('mr.isDelete', '0');
        $this->db->group_by("sb.subscriptionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;

    }
    public function get_sub_invoice($table, $con)
    {

        $data = array();

        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($con);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data;
        }

    }

    public function get_subplan_data($planID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $this->db->select('fb.*, it.* ');
        $this->db->from('tbl_subscriptions_plan_fb fb');
        $this->db->join('tbl_subscription_plan_item_fb it', 'it.planID = fb.planID', 'INNER');
        $this->db->where('fb.planID', $planID);
        $query = $this->db->get();

       
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_subscriptions_gatway_data($val)
    {

        $this->db->select('*,fcust.fullName');
        $this->db->from('tbl_subscriptions_fb sb');
        $this->db->join('Freshbooks_custom_customer fcust', 'sb.customerID = fcust.Customer_ListID', 'INNER');
        $this->db->where($val);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }
}
