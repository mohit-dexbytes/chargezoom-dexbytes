<?php

class Fb_customer_model extends CI_Model
{
    public function get_customer_invoice_data_sum($custID, $uid)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT  count(*) as incount,   sum(inv.BalanceRemaining) as amount FROM Freshbooks_test_invoice inv  WHERE `inv`.`CustomerListID` = '" . $custID . "'   and inv.merchantID='" . $uid . "' ");
        if ($query->num_rows() > 0) {

            $res = $query->row();
            return $res;
        }
        return false;
    }
    public function customer_by_id($id, $uid ='')
    {
        $this->db->select('cus.*,cmp.*');

        $this->db->from('Freshbooks_test_invoice cus');
        $this->db->join('Freshbooks_custom_customer cmp', 'cmp.Customer_ListID = cus.CustomerListID', 'inner');
        $this->db->where('cus.CustomerListID', $id);
        if($uid != ''){
			$this->db->where('cmp.merchantID', $uid);
			$this->db->where('cus.merchantID', $uid);
		}
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function get_open_invoice_data($custID, $mID)
    {
        $today = date('Y-m-d');
        $res   = array();

        $sql = "SELECT `qb`.*, `cs`.`fullName` AS custname, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
            FROM (`Freshbooks_test_invoice` qb)
            INNER JOIN `Freshbooks_custom_customer` cs ON `cs`.`customer_ListID`=  `qb`.`customerListID`
            LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`qb`.`invoiceID`
            WHERE qb.merchantID = '" . $mID . "' AND `cs`.`merchantID` = '" . $mID . "' AND (userStatus!='cancel' and qb.IsPaid = 'false') AND `qb`.`customerListID`= '$custID' ORDER BY qb.invoiceID
        ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            $res = $query->result_array();
        }

        return $res;
    }

    public function get_invoice_upcomming_data($userID, $mid)
    {
        $res   = array();
        $today = date('Y-m-d');

        $query = $this->db->query("SELECT `qb`.*, `cs`.`fullName` AS custname, (case   when (sc.scheduleDate!='0000-00-00' and qb.BalanceRemaining!='0.00')
       THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'
      AND IsPaid='0') THEN 'Open'
      when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and qb.BalanceRemaining!='0.00')  THEN 'Overdue'
      when (qb.BalanceRemaining='0.00') Then 'Paid'
      ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
       FROM (`Freshbooks_test_invoice` qb)
       INNER JOIN `Freshbooks_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID`
       LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`qb`.`invoiceID`
       WHERE qb.merchantID = '" . $mid . "' AND `cs`.`merchantID` = '" . $mid . "'  and  qb.CustomerListID = '" . $userID . "'
       AND `cs`.`Customer_ListID` = '" . $userID . "'  and qb.BalanceRemaining!='0.00'
        AND (sc.merchantID=  '" . $mid . "' OR sc.merchantID IS NULL)
       AND (sc.customerID=qb.CustomerListID OR sc.customerID IS NULL)
        AND ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID)OR sc.customerID IS NULL)
       ");
        if ($query->num_rows() > 0) {

            $res = $query->result_array();
        }

        return $res;
    }

    public function get_invoice_latest_data_old($userID, $mid)
    {

        $res = array();

        $today = date('Y-m-d');

        $query = $this->db->query("SELECT *,(SELECT transactionDate FROM customer_transaction WHERE Freshbooks_test_invoice.A = customer_transaction.transactionAmount
 		AND customer_transaction.CustomerListID = '" . $userID . "' AND customer_transaction.transactionStatus != 'Failure' LIMIT 1) AS transactionDate
 		FROM Freshbooks_test_invoice WHERE (BalanceRemaining = 0 or BalanceRemaining!= Total_payment) AND CustomerListID = '" . $userID . "' AND Freshbooks_test_invoice.merchantID= '" . $mid . "'  ");

        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }

        return $res;

    }

    public function get_invoice_latest_data111($userID, $mid)
    {

        $res = array();

        $today = date('Y-m-d');

        $query = $this->db->query("SELECT qb.*
 		FROM Freshbooks_test_invoice qb  INNER JOIN `Freshbooks_custom_customer` cs ON `cs`.`Customer_ListID`=`qb`.`CustomerListID`
 		WHERE (BalanceRemaining = 0 or  BalanceRemaining!= Total_payment) AND CustomerListID = '" . $userID . "' and UserStatus!='1'
 		and qb.merchantID= '" . $mid . "'  and cs.Customer_ListID='" . $userID . "'  and cs.merchantID = '" . $mid . "'  ");

        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }

        return $res;

    }

    public function get_invoice_latest_data($userID, $mid)
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select('qb.*');
        $this->db->from('Freshbooks_test_invoice qb');
        $this->db->join('Freshbooks_custom_customer cs', 'cs.Customer_ListID=qb.CustomerListID and cs.merchantID=qb.merchantID', 'inner');
        $this->db->where('qb.merchantID', $mid);
        $this->db->where('qb.CustomerListID', $userID);
        $this->db->where('(qb.UserStatus != "1") AND (qb.BalanceRemaining =0 OR (qb.BalanceRemaining >0 AND qb.AppliedAmount>0))');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_customer_invoice_data_payment($custID, $mid)
    {
        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT  ifnull(sum(AppliedAmount),0) as applied_amount, ifnull(sum(AppliedAmount),0) as applied_amount1,
		  ifnull( (select sum(BalanceRemaining) from Freshbooks_test_invoice where `CustomerListID` = '" . $custID . "' and (BalanceRemaining!=0  and AppliedAmount!=BalanceRemaining) and merchantID=$mid ),'0')as applied_due,
            ifnull( (select sum(BalanceRemaining) from Freshbooks_test_invoice where `CustomerListID` = '" . $custID . "'  and merchantID=$mid  ),'0')as remaining_amount ,
		  ifnull( (select sum(BalanceRemaining) from Freshbooks_test_invoice where `CustomerListID` = '" . $custID . "'  and merchantID=$mid  ),'0') as upcoming_balance FROM Freshbooks_test_invoice   WHERE `CustomerListID` = '" . $custID . "'  and merchantID=$mid    ");
        $res = $query->row();

        return $res;

    }

    public function get_email_history($con)
    {

        $data = array();

        $this->db->select('*');
        $this->db->from('tbl_template_data');
        $this->db->where($con);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data;
        }

    }

    public function get_all_customer_details_old($mid, $st = '1')
    {
        $res = array();
        $this->db->select('cus.*');
        $this->db->select('ifnull((select sum(BalanceRemaining) from Freshbooks_test_invoice where Customer_ListID = cus.Customer_ListID and merchantID="$mid" and userStatus!="1"),0) as Balance ', false);
        $this->db->from('Freshbooks_custom_customer cus ');
        $this->db->where('cus.merchantID', $mid);
        if ($st == '1') {
            $this->db->where('cus.customerStatus', '1');
        } else {
            $this->db->where('cus.customerStatus', '0');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        return $res;
    }

    public function get_all_customer_details($mid, $st = '1')
    {
        $res = array();
        $this->db->select('cus.*, (select sum(BalanceRemaining)  from 	Freshbooks_test_invoice where CustomerListID = cus.Customer_ListID and merchantID="' . $mid . '"  and userStatus!="cancel") as Balance ');
        $this->db->from('Freshbooks_custom_customer cus');
        $this->db->where('cus.merchantID', $mid);

        if ($st == '1') {
            $this->db->where('cus.customerStatus', '1');
        } else {
            $this->db->where('cus.customerStatus', '0');
        }
        $column1 = array('cus.fullName', 'cus.firstName', 'cus.lastName', 'cus.userEmail', 'cus.phoneNumber');
        $i       = 0;
        $con     = '';
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $column = $_POST['order'][0]['column'];
        if ($column == 0) {
            $orderName = 'cus.fullName';
        } elseif ($column == 1) {
            $orderName = 'cus.firstName';
        } elseif ($column == 2) {
            $orderName = 'cus.userEmail';
        } elseif ($column == 3) {
            $orderName = 'cus.phoneNumber';
        } elseif ($column == 4) {
            $orderName = 'Balance';
        } else {
            $orderName = 'cust.firstName';
        }
        $orderBY = $_POST['order'][0]['dir'];

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->order_by($orderName, $orderBY)->get();
        if ($query->num_rows() > 0) {
            $data = array('num' => '10000', 'result' => $query->result_array());
        } else {
            $data = array('num' => '0', 'result' => $query->result_array());
        }

        return $data;
    }

    public function get_all_customer_count($mid, $st = '1')
    {
        $res = array();
        $this->db->select('*');
        $this->db->from('Freshbooks_custom_customer cus ');
        $this->db->where('cus.merchantID', $mid);
        if ($st == '1') {
            $this->db->where('cus.customerStatus', '1');
        } else {
            $this->db->where('cus.customerStatus', '0');
        }
        $column1 = array('cus.fullName', 'cus.firstName', 'cus.lastName', 'cus.userEmail', 'cus.phoneNumber');
        $i       = 0;
        $con     = '';
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $query = $this->db->get();
        $cnt   = $query->num_rows();
        return $cnt;

    }

    public function customer_details($custID, $merchantID = false)
    {

        $this->db->select('*');
        $this->db->select('IFNULL(loginAt,"") as loginAt', false);
        $this->db->from('Freshbooks_custom_customer cus');
        $this->db->join('tbl_customer_login cr', 'cr.customerID=cus.Customer_ListID', 'left');
        $this->db->where('cus.Customer_ListID', $custID);
        if ($merchantID) {
            $this->db->where('cus.merchantID', $merchantID);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function get_customers_data($merchantID)
    {
        $this->db->select('*');
        $this->db->from('Freshbooks_custom_customer');
        $this->db->where('merchantID', trim($merchantID));
        $this->db->order_by('fullName', 'asc');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }

    public function get_transaction_data_refund($userID)
    {
        $today = date("Y-m-d H:i");
        $query = $this->db->query("Select tr.*,  DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, cust.* from customer_transaction tr
		inner join  Freshbooks_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where tr.merchantID='" . $userID . "' and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture'
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200' or tr.transactionCode='120')
		and (tr.transaction_user_status NOT IN (3,4) or  tr.transaction_user_status  IS NULL)   and tr.transactionID!='' GROUP BY tr.transactionID DESC");

        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_transaction_data_captue($userID)
    {

        $query = $this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  Freshbooks_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where tr.merchantID='" . $userID . "'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only'
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')
		 and ( (tr.transaction_user_status NOT IN (3,4)) or tr.transaction_user_status IS NULL ) GROUP BY id ");

        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_transaction_data_erefund($userID)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from Freshbooks_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');

        $this->db->where("cust.merchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->like("tr.gateway", 'ECheck');

        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();
            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber   from   Freshbooks_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }

                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');

                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;

                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }
        return $res;
    }

    public function get_transaction_data_ecaptue($userID)
    {

        $query = $this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  Freshbooks_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='" . $userID . "' and  tr.merchantID='" . $userID . "'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only'
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')
	    and tr.gateway LIKE '%ECheck' and (tr.transaction_user_status !='refund' or  tr.transaction_user_status  IS NULL)
		   GROUP BY id ");

        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_subscription_data($userID)
    {

        $this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_subscription_plan pl', 'pl.planID = sbs.subscriptionPlan', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->where("sbs.merchantDataID ", $userID);
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_refund_transaction_data($userID)
    {
        $query = $this->db->query("SELECT `tr`.*, `cust`.* FROM (`customer_transaction` tr)
	INNER JOIN `Freshbooks_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID`
	WHERE `tr`.`merchantID` = '$userID' and cust.merchantID= '$userID'   AND (`tr`.`transactionType` = 'refund' or `tr`.`transactionType` = 'stripe_refund'  OR `tr`.`transactionType` = 'pay_refund' OR `tr`.`transactionType` = 'credit' ) ORDER BY `tr`.`transactionDate` desc");

        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_credit_user_data($con)
    {
        $res = array();
        $this->db->select('cr.*,sum(cr.creditAmount) as balance, cust.companyName, cust.fullName');
        $this->db->from('tbl_credits cr');
        $this->db->join('Freshbooks_custom_customer cust', 'cr.creditName = cust.Customer_ListID', 'INNER');
        $this->db->where($con);
        $this->db->group_by('cr.creditName');
        $this->db->order_by('cr.creditDate', 'desc');
        $query = $this->db->get();
        if ($query->num_rows()) {

            $res = $query->result_array();
        }
        return $res;

    }

    public function get_customer_note_data($custID, $mID)
    {

        $res = array();

        $query = $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `Freshbooks_custom_customer` cust ON `pr`.`customerID` = `cust`.`Customer_ListID` WHERE `pr`.`customerID` = '" . $custID . "'  and pr.merchantID='" . $mID . "' order by pr.noteID desc");

        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_subscriptions_data($condition)
    {

        $this->db->select('sbs.*, cust.fullName, pl.planName as sub_planName, cust.companyName, tmg.*');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_subscriptions_plan_fb pl', 'pl.planID = sbs.subscriptionPlan', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->where($condition);
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_cust_subscriptions_data_old($userID)
    {

        $cardDb = getenv('DB_DATABASE2');
        $this->db->select('sbs.*, cust.fullName,pl.planName as sub_planName, tmg.*, c.customerCardfriendlyName');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_subscriptions_plan_fb pl', 'pl.planID = sbs.planID', 'left');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join("$cardDb.customer_card_data c", 'c.CardID=sbs.cardID', 'inner');
        $this->db->where($userID);
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_cust_subscriptions_data($userID)
    {

        $cardDb = getenv('DB_DATABASE2');

        $res = array();

        $this->db->select('sbs.nextGeneratingDate,sbs.firstDate, sbs.subscriptionID,sbs.subscriptionAmount,cust.fullName,pl.planName as sub_planName, c.customerCardfriendlyName');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID and cust.merchantID=sbs.merchantDataID', 'INNER');
        $this->db->join('tbl_subscriptions_plan_fb pl', 'pl.planID = sbs.planID', 'left');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join("$cardDb.customer_card_data c", 'c.CardID=sbs.cardID', 'left');
        $this->db->where($userID);
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res = $query->result_array();
        }

        return $res;
    }

    public function get_billing_data($merchID)
    {
        $sql = ' SELECT * from tbl_merchant_billing_invoice where merchantID = "' . $merchID . '" ';

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result_array();

        } else {
            return false;
        }

    }

    public function get_merchant_billing_data($invID)
    {
        $res   = array();
        $query = $this->db->query("SELECT bl.*, mr.*, ct.city_name, st.state_name, c.country_name FROM (tbl_merchant_billing_invoice bl) INNER JOIN tbl_merchant_data mr ON mr.merchID=bl.merchantID LEFT JOIN state st ON st.state_id=mr.merchantState LEFT JOIN country c ON c.country_id=mr.merchantCountry LEFT JOIN city ct ON ct.city_id=mr.merchantCity WHERE bl.merchant_invoiceID = '$invID' AND bl.status = 'pending'");

        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            return $res;
        } else {
            return $res;
        }

    }
    public function get_terms_data($merchID)
    {

        $data  = array();
        $query = $this->db->query("SELECT *, dpt.id as dfid, pt.id as pid FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID UNION SELECT *, dpt.id as dfid, pt.id as pid  FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.merchantID = $merchID ORDER BY dfid IS NULL, dfid asc");
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data;
        }
    }

    public function check_payment_term($name, $netterm, $merchID)
    {
        $query = $this->db->query("SELECT * FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID WHERE dpt.name IN ('" . $name . "') AND pt.pt_netTerm IS NULL OR dpt.netTerm IN ('" . $netterm . "') AND pt.pt_netTerm IS NULL OR pt.pt_name IN ('" . $name . "') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('" . $netterm . "') AND pt.merchantID = $merchID  AND pt.enable != 1 UNION SELECT * FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.pt_name IN ('" . $name . "') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('" . $netterm . "') AND pt.merchantID = $merchID  AND pt.enable != 1 AND pt.merchantID = $merchID AND pt.enable != 1");
        return $query->num_rows();
    }

    public function update_refund_payment($tr_id, $gateway)
    {
        $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        if ($gateway == 'NMI') {

            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where (  transactionID="' . $tr_id . '" and (transactionType="capture" or transactionType="sale") )';
        }
        if ($gateway == 'AUTH') {

            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
        }

        if ($gateway == 'PAYTRACE') {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where (transactionID="' . $tr_id . '"  and (transactionType="pay_sale" or transactionType="pay_capture") )';

        }

        if ($gateway == 'PAYPAL') {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )';

        }

        if ($gateway == 'STRIPE') {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';

        }
        if ($gateway == iTransactGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }
        if ($gateway == TSYSGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }

        if ($this->db->query($sql)) {
            return true;
        } else {
            return false;
        }

    }

    public function get_invoice_transaction_data($invoiceID, $userID, $action)
    {

        $this->db->select('tr.transactionID,sum(tr.transactionAmount) as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, cust.fullName as FullName , tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID');
        $this->db->from('customer_transaction tr');
        $this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        $this->db->where("cust.merchantID ", $userID);
        if ($action == 'invoice') {
            $this->db->where('FIND_IN_SET("' . $invoiceID . '", tr.invoiceID) ');
        }

        if ($action == 'transaction') {
            $this->db->where("tr.transactionID", $invoiceID);
        }

        $this->db->where("tr.merchantID ", $userID);
        $this->db->group_by('transactionID');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_transaction_details_data($con)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $this->db->select('tr.transactionAmount');
        $this->db->select('ifnull(sum(r.refundAmount),0) as partial', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.customer_ListID', 'INNER');
        $this->db->join('tbl_customer_refund_transaction r', 'tr.transactionID=r.creditTransactionID', 'left');

        $this->db->where($con);
        $this->db->where("tr.transactionID >", 0);
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1                     = $query->row_array();
            $res['transactionAmount'] = ($res1['transactionAmount'] - $res1['partial']);
        }
        return $res;

    }

    public function get_invoice_item_data($con)
    {
        $res = array();

        $query = $this->db->query(" SELECT * from tbl_freshbook_invoice_item  where invoiceID in ($con)  ");
        if ($query->num_rows > 0) {
            $res = $query->result_array();

        }

        return $res;

    }

    public function get_invoice_transaction_details_data($con, $inID)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $this->db->select('tr.ID, tr.transactionAmount, invoiceRefID');
        $this->db->select('ifnull(sum(r.refundAmount),0) as partial', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.customer_ListID', 'INNER');
        $this->db->join('tbl_customer_refund_transaction r', 'tr.transactionID=r.creditTransactionID', 'left');

        $this->db->where($con);
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();
            foreach ($res1 as $res2) {
                if (!empty($res2['invoiceRefID'])) {
                    $in_data = json_decode($res['invoiceRefID']);

                    $key = array_search($inID, $in_data['inv_no']);
                    if ($key) {
                        $tramount = $in_data['inv_amount'][$key];
                    }

                    $res2['final_amount'] = $tramount;

                } else {
                    $res2['final_amount'] = ($res2['transactionAmount'] - $res2['partial']);

                }
                $res2['transactionAmount'] = ($res2['transactionAmount'] - $res2['partial']);

                $res = $res2;
            }

        }
        return $res;

    }

    public function get_fb_item_data($userID)
    {

        $result = array();

        $this->db->select('fb_item.Name as Name,fb_item.productID as productID  ');
        $this->db->from('Freshbooks_test_item fb_item');
        $this->db->join('tbl_merchant_data comp', 'comp.merchID = fb_item.merchantID', 'INNER');

        $this->db->where('fb_item.merchantID', $userID);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

        }

        return $result;
    }

    public function get_invoice_data_template($con)
    {

        $res   = array();
        $today = date('Y-m-d');
        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $query = $this->db->query("SELECT `inv`.invoiceID,`inv`.refNumber as RefNumber,`inv`.DueDate,  ifnull(sum(Total_payment-BalanceRemaining),0) as AppliedAmount,
	     `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	  ifnull((select transactionType from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1),'' ) as  paymentType,
	  ifnull((select transactionCode from customer_transaction where invoiceID =inv.invoiceID  order by id desc limit 1),'') as tr_status,
	    ifnull((select transactionDate from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1 ),'') as tr_date,
		ifnull((select transactionAmount from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1 ),'') as tr_amount,
	    ifnull((select transactionStatus from customer_transaction where invoiceID =inv.invoiceID order by id desc limit 1 ),'') as tr_data,

	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = '0'   then 'Upcoming'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = '0'    and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID  and merchantID='" . $user_id . "' limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = '0'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='1'     then 'Success'
	  else 'Canceled' end ) as status, cust.userEmail as email
	  FROM Freshbooks_test_invoice inv
	  INNER JOIN  Freshbooks_custom_customer cust ON inv.CustomerListID = cust.Customer_ListID
	  WHERE  inv.CustomerListID !='' $con  and cust.merchantID='" . $user_id . "' and  inv.merchantID='" . $user_id . "'   limit 1 ");

        if ($query->num_rows() > 0) {

            return $res = $query->row_array();
        }
        return $res;
    }

    public function get_subscriptions_plan_data($userID)
    {
        $this->db->select('sbs.*, cust.fullName, cust.companyName, spl.planName as sub_planName, tmg.*');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');

        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join('tbl_subscriptions_plan_fb spl', 'spl.planID = sbs.planID', 'left');
        $this->db->where("sbs.merchantDataID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_total_subscription($merchID)
    {
        $res           = array();
        $today         = date("Y-m-d H:i");
        $next_due_date = date('Y-m', strtotime("+30 days"));

        $this->db->select('*');
        $this->db->from('tbl_subscriptions_fb sbs');
        $this->db->join('Freshbooks_custom_customer cust', 'sbs.customerID=cust.Customer_ListID', 'inner');
        $this->db->where('cust.merchantID', $merchID);
        $this->db->where('sbs.subscriptionStatus', "1");
        $this->db->where('cust.customerStatus', "1");
        $this->db->where('DATE_FORMAT(sbs.startDate, "%Y-%m")>=', date("Y-m"));

        $this->db->group_by('sbs.customerID');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res['total_subcription'] = $query->num_rows();
        } else {
            $res['total_subcription'] = 0;
        }

        $query = $this->db->query(" SELECT count(*) as sub_count  FROM (`tbl_subscriptions_fb` sbs)
	   INNER JOIN `Freshbooks_custom_customer` cust ON `sbs`.`customerID` = `cust`.`Customer_ListID`

	  WHERE `cust`.`merchantID` = '" . $merchID . "' AND `cust`.`customerStatus` = '1'
	  and sbs.subscriptionStatus='1' and DATE_FORMAT(startDate, '%Y-%m')>= '" . date('Y-m') . "'   ");
        if ($query->num_rows() > 0) {

            $res['active_subcription'] = $query->row_array()['sub_count'];
        } else {
            $res['active_subcription'] = 0;
        }

        $today_date   = date("Y-m-d");
        $next_7_date  = date('Y-m-d', strtotime("+7 days"));
        $next_1_month = date('Y-m-d', strtotime("+1 month"));

        $query1 = $this->db->query("SELECT
				COUNT(1) AS exp_count
			FROM
				tbl_subscriptions_fb sbs
			INNER JOIN Freshbooks_custom_customer cust ON
				sbs.customerID = cust.Customer_ListID AND sbs.merchantDataID = cust.merchantID
			WHERE
				sbs.merchantDataID = '$merchID' AND cust.customerStatus = '1' AND subscriptionPlan != '0' AND totalInvoice > '0' AND sbs.subscriptionStatus = '1' AND (
					( sbs.endDate BETWEEN '$today_date' AND  '$next_1_month' AND invoicefrequency IN ('mon', '2mn', 'qtr', 'six', 'yrl', '2yr', '3yr'))
					OR
					( sbs.endDate BETWEEN '$today_date' AND  '$next_7_date' AND invoicefrequency IN ('dly', '1wk', '2wk'))
		)");

        if ($query1->num_rows() > 0) {

            $res['exp_subcription'] = $query1->row_array()['exp_count'];
        } else {
            $res['exp_subcription'] = 0;
        }

        $query1 = $this->db->query("SELECT
				COUNT(distinct(sbs.subscriptionID)) AS failed_count
			FROM
				Freshbooks_test_invoice inv
			INNER JOIN Freshbooks_custom_customer cust ON
				inv.CustomerListID = cust.Customer_ListID AND cust.merchantID = '$merchID'
			INNER JOIN tbl_subscription_auto_invoices TSAI ON
				TSAI.invoiceID = inv.invoiceID AND app_type = 3
			INNER JOIN customer_transaction CTR ON
				CTR.invoiceID = inv.invoiceID AND transactionCode = 300  AND CTR.merchantID = '$merchID'
			INNER JOIN tbl_subscriptions_fb sbs ON TSAI.subscriptionID = sbs.subscriptionID
			WHERE
				inv.merchantID = '$merchID' AND sbs.merchantDataID = '$merchID' AND cust.customerStatus = 'true' AND inv.IsPaid = '0' OR inv.userStatus !='paid'
		");

        if ($query1->num_rows() > 0) {
            $res['failed_count'] = $query1->row_array()['failed_count'];
        } else {
            $res['failed_count'] = 0;
        }

        return $res;
    }

    public function get_transaction_data_by_id($TxnID, $userID, $action)
    {

        $this->db->select('tr.transactionID,sum(tr.transactionAmount)as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, cust.FullName, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID');
        $this->db->from('customer_transaction tr');
        $this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        $this->db->where("cust.merchantID", $userID);
        if ($action == 'transaction') {
            $this->db->where("tr.id", $TxnID);
        } else {
            $this->db->where("tr.transactionID", $TxnID);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }
    public function get_merchant_billing_details($invID)
    {
        $sql = $this->db->query(" Select * from tbl_merchant_invoice_item where merchantInvoiceID='" . $invID . "'  ");

        $result = array();
        if ($sql->num_rows() > 0) {
            $result = $sql->result_array();
        }

        return $result;

    }
    public function get_transaction_datarefund($userID)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from Freshbooks_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');

        $this->db->where("cust.merchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->not_like("tr.gateway", 'ECheck');

        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();

            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber   from   Freshbooks_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }

                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');

                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;

                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }

        return $res;
    }
    public function getPartialNewAmount($transactionID)
    {
        $ref_amt = 0;

        $qr   = $this->db->query('SELECT id FROM customer_transaction WHERE transactionID ="' . $transactionID . '" ORDER BY `id` DESC');
        $data = $qr->result_array();

        if (!empty($data)) {
            foreach ($data as $value) {
                $qr1   = $this->db->query('SELECT sum(transactionAmount) as transactionAmount FROM customer_transaction WHERE parent_id ="' . $value['id'] . '" ORDER BY `id` DESC');
                $data1 = $qr1->row_array();

                if (!empty($data1)) {
                    $ref_amt = $ref_amt + $data1['transactionAmount'];
                }

            }

            return $ref_amt;

        } else {
            return $ref_amt;
        }
    }

}
