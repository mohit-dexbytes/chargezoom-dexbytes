<?php


Class Fb_company_model extends CI_Model
{
    
    public function get_invoice_data_by_due($condion){
		
		$res =array();
	    
		
		$this->db->select("`inv`.CustomerListID,`inv`.CustomerFullName, cust.firstName, cust.lastName, `cust`.userEmail,`cust`.fullName,`cust`.companyName, (case when cust.Customer_ListID !='' then 'Active' else 'InActive' end) as status , `inv`.ShipAddress_Addr1, `cust`.Customer_ListID, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
		$this->db->where(['IsPaid' => 0]);
         $this->db->where($condion);
        $this->db->group_by('inv.CustomerListID');
		$this->db->limit(10);
		$this->db->order_by('balance','desc');
	   
	   
		$query = $this->db->get();
		return $res = $query->result_array();
		return $res;
	
	} 	
	
	
		public function get_transaction_failure_report_data($userID){
	    
	    	$res =array();
	    
	    $sql ="SELECT `tr`.`id`, `tr`.`invoiceID`,  `tr`.`transactionID`, `tr`.`transactionStatus`, `tr`.`customerListID`, `tr`.`transactionCode`,  `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`,(select refNumber from Freshbooks_test_invoice where invoiceID = tr.invoiceID limit 1) as RefNumber, `cust`.`fullName` FROM (`customer_transaction` tr) INNER JOIN `Freshbooks_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID` WHERE `cust`.`merchantID` = '$userID' AND transactionCode NOT IN ('200','1','100','111') ";
		
		$query = $this->db->query($sql); 
		if($query->num_rows() > 0){
		 return $res= $query->result_array();
		}
	     return  $res;
	}
	
	public function get_invoice_data_by_past_due($userID){
		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,  `inv`.invoiceID, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , 
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `BalanceRemaining` != '0.00' and 
	  
	  `inv`.`merchantID` = '$userID' and  `cust`.`merchantID` = '$userID'   order by  BalanceRemaining   desc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	}
	
	public function get_invoice_data_by_past_time_due($userID){
		
		$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,  `inv`.invoiceID, 
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' and userStatus ='' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'   then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	  	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and BalanceRemaining != '0.00' and 
	  `inv`.`merchantID` = '$userID'  order by  DueDate   asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 
	
	public function get_transaction_report_data($userID, $minvalue, $maxvalue){
		$res =array();
		$trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
     	$this->db->select('tr.*, cust.*, (select RefNumber from Freshbooks_test_invoice where invoiceID= tr.invoiceID and CustomerListID = cust.customer_ListID and  merchantID="'.$userID.'" ) as RefNumber ');
		$this->db->from('customer_transaction tr');
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
		$this->db->where('cust.merchantID',$userID);	
		
		$query = $this->db->get();
     
		if($query->num_rows() > 0){
		
		return $res = $query->result_array();

		}
			return $res;
	} 
	
	public function get_invoice_data_open_due($userID, $status){
		
		$res =array();
		$con='';
		if($status=='8'){
		 $con.="and `inv`.BalanceRemaining !='0.00' and `inv`.`IsPaid` = 'false' ";
		
		}
		
		
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  inv.invoiceID, `inv`.RefNumber, (`inv`.Total_payment - `inv`.BalanceRemaining) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, `inv`.TimeCreated, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , 
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,inv.TimeCreated as addOnDate,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'   then 'Success'
	  else 'Canceled' end ) as status 

	  FROM Freshbooks_test_invoice inv 

	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE  `inv`.`IsPaid` = 0 and `inv`.`merchantID` = '$userID'  $con order by  DueDate   asc");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 
	
	public function get_invoice_upcomming_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  inv.RefNumber, inv.invoiceID, `inv`.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,  `inv`.TimeCreated,  `sc`.scheduleDate,   
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	    INNER JOIN `tbl_scheduled_invoice_payment` sc ON `inv`.`invoiceID` = `sc`.`invoiceID` 
	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' and  
	  inv.merchantID = '$userID'  and sc.merchantID = '$userID' and `sc`.`scheduleDate` != '0000-00-00'  group by  `sc`.`invoiceID`  order by   DueDate  asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 



      public function get_invoice_data_count($condition){
		
		$num =0;
	    
		$this->db->select('inv.* ');
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where($condition);
	    $this->db->where("BalanceRemaining != 0.00");
		
		$query = $this->db->get();
		$num = $query->num_rows();
		return $num;
	
	}
	

   
    
    public function get_invoice_due_by_company($condion,$status){
		
		$res =array();
	    $today  = date('Y-m-d');
		$this->db->select("cust.fullName as label, inv.DueDate, inv.invoiceID,  inv.refNumber, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
        $this->db->group_by('cust.Customer_ListID');
		$this->db->order_by('balance','desc');
		$this->db->limit(5);
	    if($status=='1'){
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '".$today."'  AND `BalanceRemaining` > 0 ");
	    }else{
	        $this->db->where("`BalanceRemaining` > 0  ");
	    } 
  		 $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}


    public function get_oldest_due($userID)
    {
		     
        $res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT cust.FullName, `inv`.invoiceID,  `inv`.refNumber, `inv`.CustomerListID,   (`inv`.Total_payment) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.FullName, DATEDIFF('$today', inv.DueDate) as aging_days,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND BalanceRemaining !='0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and BalanceRemaining ='0' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE  BalanceRemaining !='0.00' and 
	  
	  `inv`.`merchantID` = '$userID'  and  cust.merchantID='$userID' order by  inv.DueDate asc limit 10 ");
	  
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
		
	}


       public function get_company_invoice_data_payment($merchantID){
		$res = array();
		
		
		
		 $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
		inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID 
		where   (tr.transactionType ="sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture"  or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  cust.merchantID="'.$merchantID.'" and  tr.merchantID="'.$merchantID.'" ');
		
		
		 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID where 
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			and ( tr.transaction_user_status !="3" )     and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.merchantID="'.$merchantID.'" and  tr.merchantID="'.$merchantID.'"  ');
		    
		     if($rquery->num_rows()>0)
             {   
		    
		       $res['total_amount'] = $rquery->row_array()['total'];     
              
               
                $res['refund_amount'] =  $rfquery->row_array()['rm_amount'] ;
             }else{
                  $res['total'] =0; 
                
                 $res['refund'] =0;
                 
             }

	    return $res;
	} 
	
	   

		public function get_process_trans_count($user_id){
     
		$today = date("Y-m-d");
			$this->db->select('*');
		$this->db->from('customer_transaction tr');
	    $this->db->where("tr.merchantID ", $user_id);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
     
		if($query->num_rows()>0)
        {
            return $query->num_rows();  
        }
        else
        {
           return $query->num_rows();
        }
     } 
     
	 
     
    
    
public function get_invoice_data_count_failed($user_id){
		$today =date('Y-m-d');
		$num =0;
	
	   $sql ="SELECT `inv`.* FROM (`Freshbooks_test_invoice` inv) INNER JOIN `customer_transaction` tr ON `tr`.`invoiceID` = `inv`.`invoiceID` AND tr.transactionCode = '300' WHERE  `inv`.`merchantID` = '$user_id' ";
	
	  $query = $this->db->query($sql);
	  $num   = $query->num_rows();
		return $num;
	
     }	
     
    
	public function get_paid_invoice_recent($condion)
	{
		
		$res =array();
	    $today  = date('Y-m-d');

		$this->db->select("cust.FullName,inv.CustomerListID, cust.companyName, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID, inv.refNumber ");
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
		$this->db->join('customer_transaction trans','trans.invoiceID = inv.invoiceID','INNER');        
		$this->db->order_by('trans.transactionDate','desc');
		$this->db->limit(10);
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
	    $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	
	} 
	public function get_recent_volume_dashboard($mID,$month)
	{   
	  
		 
		$res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
           customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID WHERE tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
           date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
            or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
         tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
         and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2" )    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
         
        $query = $this->db->query($sql);
        
        
        if($query->num_rows()>0)
        {    
         $row = $query->row_array();
         $earn = $row['volume'];

        }
        $rfsql = 'select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
        inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
           date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and 
        (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
        and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
        and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL ';
         $rfquery = $this->db->query($rfsql);

         
        if($rfquery->num_rows()>0)
        {    
            $row = $rfquery->row_array();
            $refund = $row['refund'];
            
        }
        $res = $earn - $refund;
        return  $res; 
	}    
	public function get_creditcard_payment($mID,$month)
      {   
   
      
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
       
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and  tr.gateway NOT LIKE "%echeck%" and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

        
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
         
        }

        $rfsql = 'select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
            inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
               date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway NOT LIKE "%echeck%" and 
            (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
            and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
            and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL ';
        $rfquery = $this->db->query($rfsql);
        
        if($rfquery->num_rows()>0)
        {    

            $row1 = $rfquery->row_array();
            $refund1 = $row1['refund'];

        }
        $res = $earn - $refund1;

        return  $res; 
    }  
	   public function get_offline_payment($mID,$month)
		{   
			$res= 0.00;
			$sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
		 		customer_transaction tr    
			 inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
			 WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
			 date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and (tr.transactionType ="Offline Payment") and  (tr.transactionCode ="100" or 
			   tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			   and ( tr.transaction_user_status !="3" )    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
			   
			$query = $this->db->query($sql);
			if($query->num_rows()>0)
			{    
				$row = $query->row_array();
				$earn= $row['volume'];
				   
				$res =$earn;
				  
			}else{
				  
				$res =0.00;
				  
			}
		    return  $res; 
		} 
		public function get_outstanding_payment($userID){
			$res =array();
			$today  = date('Y-m-d');
			$month = date("M-Y");
			$query  =  $this->db->query("SELECT  inv.RefNumber, inv.invoiceID, `inv`.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,  `inv`.TimeCreated,  `sc`.scheduleDate,   
			Month(inv.DueDate),sum(`inv`.BalanceRemaining) as balance,sum(`inv`.AppliedAmount) as payment,
			cust.*
			FROM Freshbooks_test_invoice inv 
			INNER JOIN `tbl_scheduled_invoice_payment` sc ON `inv`.`invoiceID` = `sc`.`invoiceID` 
			INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
			WHERE inv.IsPaid = 'false'   and userStatus!='cancel' and  
			inv.merchantID = '$userID' and cust.merchantID = '$userID'  and sc.merchantID = '$userID' and `sc`.`scheduleDate` != '0000-00-00'  group by  `sc`.`invoiceID`  order by   DueDate  asc limit 10 ");
		
			
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$pay = str_replace('-','',$row['payment']);
			$earn = $row['balance']-(float)$pay;
			$res = $earn;
		}else{
			$res = '0.00';
		}
			return  $res;
	}

	public function chart_general_volume($mID)
    {   
 
    
       $res= array();
		     $months=array();
		
		    for ($i = 11; $i >=0 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
            }  
    
     foreach($months as $key=> $month)
	 { 
    
  
  $sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr  
  	inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
  WHERE tr.merchantID = "'.$mID.'"  AND 
  date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture"  or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		and  ( tr.transaction_user_status !="3" )   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		
			 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID where 
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3" )  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.merchantID="'.$mID.'" and  tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
             $row['volume'] = $row['volume']- $rfquery->row_array()['rm_amount'];
            
            $res[] = $row; 
            $res[$key]['Month'] =$month;
        }else{
           
            $res[$key]['volume'] =0.00;
            $res[$key]['Month'] =$month;
        }
     }  
            return $res;
       
    }
   
	public function chart_all_volume($mID)
	{   
		$res= array();
				$months=array();
		for ($i = 11; $i >=1 ; $i--) {
			$months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
		}
		$months[] = date("M-Y", strtotime( date( 'Y-m' )." 0 months"));  
		$chart_arr = array();



		foreach($months as $key=> $month)
		{ 
			$sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr  
				inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
			WHERE tr.merchantID = "'.$mID.'"  AND 
			date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
			or  tr.transactionType ="auth_capture"  or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
			tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			and  ( tr.transaction_user_status !="3" )   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
			
				
		$query = $this->db->query($sql);
			if($query->num_rows()>0)
			{    
				$row = $query->row_array();
				$earn= $row['volume'];
				$res[$key]['revenu_volume'] =$earn;
				$res[$key]['revenu_Month'] =$month;
			}else{
			
				$res[$key]['revenu_volume'] =0.00;
				$res[$key]['revenu_Month'] =$month;
			}

			$sql2 = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr  
				inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
			WHERE tr.merchantID = "'.$mID.'"  AND 
			date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or   tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
			or  tr.transactionType ="auth_capture"  or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
			tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			and  ( tr.transaction_user_status !="3" )   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
			
				
		$query2 = $this->db->query($sql2);
			if($query2->num_rows()>0)
			{    
				$row2 = $query2->row_array();
				$earn2= $row2['volume'];
				$res[$key]['online_volume'] =$earn2;
				$res[$key]['online_Month'] =$month;
			}else{
			
				$res[$key]['online_volume'] =0.00;
				$res[$key]['online_Month'] =$month;
			}
			$sql3 = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr  
				inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
			WHERE tr.merchantID = "'.$mID.'"  AND 
			date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Offline Payment" ) and  (tr.transactionCode ="100" or 
			tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			and  ( tr.transaction_user_status !="3" )   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
			
				
		$query3 = $this->db->query($sql3);
			if($query3->num_rows()>0)
			{    
				$row3 = $query3->row_array();
				$earn= $row3['volume'];
				$res[$key]['offline_volume'] =$earn;
				$res[$key]['offline_Month'] =$month;
			}else{
			
				$res[$key]['offline_volume'] =0.00;
				$res[$key]['offline_Month'] =$month;
			}
		}  
		return  $res; 
	}
       public function get_schedule_payment_invoice($mID)
      {
          	$res =array();

		 $query =	 $this->db->query("SELECT `qt`.`CustomerListID`, `qt`.`invoiceID`, `qt`.`refNumber` as RefNumber, `cust`.`fullName`, `qt`.`BalanceRemaining`, `qt`.`DueDate`, `qt`.`Total_payment`, `sc`.`scheduleDate` 
		 	 FROM (`Freshbooks_test_invoice` qt) INNER JOIN `Freshbooks_custom_customer` cust ON `qt`.`CustomerListID` = `cust`.`Customer_ListID` INNER JOIN `tbl_scheduled_invoice_payment` sc ON `qt`.`invoiceID` = `sc`.`invoiceID` WHERE 
		 	 `cust`.`merchantID` = '$mID' AND `qt`.`merchantID` = '$mID' AND sc.merchantID='$mID' and qt.BalanceRemaining!= '0.00' and sc.scheduleDate != '0000-00-00' ");
		
		  
		if($query->num_rows() >0)
		{
		    $res = $query->result_array();
		}
		
		return $res;
      }
      
      
         public function get_schedule_payment_invoice_csv($mID)
      {
          	$res =array();

         	 $query =	 $this->db->query("SELECT  `qt`.`refNumber` as RefNumber, `cust`.`fullName`, `qt`.`BalanceRemaining`, `qt`.`DueDate`, `qt`.`Total_payment`, `sc`.`scheduleDate` 
		 	 FROM (`Freshbooks_test_invoice` qt) INNER JOIN `Freshbooks_custom_customer` cust ON `qt`.`CustomerListID` = `cust`.`Customer_ListID` INNER JOIN `tbl_scheduled_invoice_payment` sc ON `qt`.`invoiceID` = `sc`.`invoiceID` WHERE 
		 	 `cust`.`merchantID` = '$mID' AND `qt`.`merchantID` = '$mID' AND sc.merchantID='$mID' and qt.BalanceRemaining!= '0.00' and sc.scheduleDate != '0000-00-00' ");
		
		if($query->num_rows() >0)
		{
		    $res = $query->result_array();
		}
		
		return $res;
      }
    
    	public function get_subplan_data($planID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT * FROM tbl_subscriptions_plan_fb INNER JOIN tbl_subscription_plan_item_fb ON tbl_subscription_plan_item_fb.planID =  '".$planID."' WHERE tbl_subscriptions_plan_fb.planID = '".$planID."'");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 	

       public function get_terms_data($merchID){
    	$data =array();
	    $query = $this->db->query("SELECT *, dpt.id as dfid, pt.id as pid FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID UNION SELECT *, dpt.id as dfid, pt.id as pid  FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.merchantID = $merchID ORDER BY dfid IS NULL, dfid asc");
	   if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	}
	
	public function check_payment_term($name,$netterm,$merchID)
	{
			$query = $this->db->query("SELECT * FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID WHERE dpt.name IN ('".$name."') AND pt.pt_netTerm IS NULL OR dpt.netTerm IN ('".$netterm."') AND pt.pt_netTerm IS NULL OR pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 UNION SELECT * FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 AND pt.merchantID = $merchID AND pt.enable != 1");
		return $query->num_rows();	
	}
	
	 public function get_plan_data($userID)
	  {
		
	$result =array();
	    
		$this->db->select('qb_item.Name as Name, qb_item.FullName as FullName,qb_item.Parent_FullName as Parent_FullName,qb_item.Type as Type, qb_item.SalesPrice as SalesPrice, qb_item.ListID as ListID,qb_item.EditSequence as EditSequence, Discount, qb_item.IsActive ,  (case when qb_item.IsActive="true" then "Done"  when qb_item.IsActive="false" then "Done" else "Done" end) as c_status   ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
		
		$query = $this->db->get();
        
		if($query->num_rows() > 0){
			$result = $query->result_array();
        
        }
       
       return $result;
      }
      
	 	public  function  get_invoice_item_data ($invoiceID){
	 	    
		$res =array();
		
      $sql = "Select itm.*, fb_item.* from   tbl_freshbook_invoice_item  itm  inner join Freshbooks_test_item fb_item on  itm.itemLineID = fb_item.ListID where  itm.invoiceID = '$invoiceID' ";	
	
		$query = $this->db->query($sql);
		  
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
		
		
	} 
	public function get_paid_recent($userID){
		
		$res =array();
	   
		$today = date("Y-m-d H:i");
		$res =array();
		$tcode =array('100','200', '120','111','1');
		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
	
		$this->db->select('(tr.transactionAmount) as balance,tr.invoiceTxnID,tr.transactionID,tr.id as TxnID, tr.transactionDate,  cust.Customer_ListID as CustomerListID, cust.FullName, cust.companyName,(select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID limit 1) as RefNumber');
		
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', FALSE);
		
		$this->db->from('customer_transaction tr');
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
		
	    $this->db->where("tr.merchantID ", $userID);
	     
		$this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	    $this->db->order_by("tr.id", 'desc');
		$this->db->group_by("tr.transactionID");
		$this->db->limit(10);
		$query = $this->db->get();
    	
		if($query->num_rows() > 0)
		{
		
		 	$res = $query->result_array();
		  
		}
		
		return $res;
	
	}
	public function chart_all_volume_new($mID)
	{   
		$res= array();
        $months=array();
        $resObj = array();
        for ($i = 11; $i >=1 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
        }  
        $months[] = date("M-Y", strtotime( date( 'Y-m' )." 0 months"));
        $chart_arr = array();
        $totalRevenue = 0;
        $totalRevenue = $totalCCA = $totalECLA = 0;
		foreach($months as $key=> $month)
		{ 
			$earn = $this->get_recent_volume_dashboard($mID,$month);
            $res[$key]['revenu_volume'] = $earn;
            $res[$key]['revenu_Month'] =$month;

            $earn2 = $this->get_creditcard_payment($mID,$month);
            $res[$key]['online_volume'] =$earn2;
            $res[$key]['online_Month'] = $month;
                
           
            $earn4 = $this->get_eCheck_payment($mID,$month);
            $res[$key]['eCheck_volume'] =$earn4;
            $res[$key]['eCheck_Month'] =$month;

            $totalRevenue = $totalRevenue + $earn;
            $totalCCA = $totalCCA + $earn2;
			$totalECLA = $totalECLA + $earn4;
		}  
		$resObj['data'] = $res;
        $resObj['totalRevenue'] = $totalRevenue;
        $resObj['totalCCA'] = $totalCCA;
		$resObj['totalECLA'] = $totalECLA;
        return $resObj; 
	} 		

	public function get_echeck_payment($mID,$month)
    {   
       
         $res= 0.00;
         $earn = 0.00;
         $refund = 0.00;
         $refund1 = 0.00;
        
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and  tr.gateway LIKE "%echeck%"  and (tr.transactionType ="Sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3" and  tr.transaction_user_status !="2" )  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

        
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
         
        }

        $rfquery = $this->db->query('select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
            inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
               date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway LIKE "%echeck%"  and 
            ( UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "REFUND" )
            and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
            and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL  GROUP BY date_format(transactionDate, "%b-%Y") ');

        if($rfquery->num_rows()>0)
        {    
            $row = $rfquery->row_array();
            $refund = $row['refund'];

        }

        if($earn >= $refund){
            $earn = $earn - $refund;
        }
        
        
        $res = $earn;
        return  $res;
            
    }
	public function getAutoRecurringInvoices(){
        $res = array();
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));
        $todayDate = date('d');
        $lastDay = date('t');

       
        $autoCondition = "( 
            ( FTI.DueDate = '$today' AND TRP.optionData = 1)
            OR ( FTI.TimeCreated LIKE '$today%' AND TRP.optionData = 2)
            OR ( FTI.TimeCreated LIKE '$yesterday%' AND TRP.optionData = 3)
            OR ( ( TRP.month_day = $todayDate OR ( TRP.month_day > '$lastDay' AND '$lastDay' = '$todayDate') ) AND TRP.optionData = 4)
        )";

        $sql = "SELECT FTI.*, FCC.companyID, FCC.firstName, FCC.lastName, FCC.userEmail,FCC.companyName,FCC.phoneNumber as Phone, FCC.Customer_ListID, FCC.fullName, TMG.gatewayUsername, TMG.gatewayPassword, TMG.gatewaySignature, TMG.extra_field_1,TMG.gatewayMerchantID, TMG.gatewayType, TRP.cardID, TRP.recurring_send_mail, TMG.gatewayID, FB.accessToken, FB.refreshToken, FB.realmID, FB.oauth_token_secret, FB.secretKey, FB.accountType,FB.sub_domain, TMD.resellerID,TMD.merchID
        FROM Freshbooks_test_invoice FTI
        INNER JOIN tbl_recurring_payment TRP ON TRP.merchantID = FTI.merchantID AND TRP.customerID = FTI.CustomerListID
        INNER JOIN tbl_merchant_data TMD ON TMD.merchID = FTI.merchantID
        INNER JOIN Freshbooks_custom_customer QCC ON FCC.merchantID = FTI.merchantID AND FCC.Customer_ListID = FTI.CustomerListID
        INNER JOIN tbl_merchant_gateway TMG ON TRP.merchantID = TMG.merchantID AND TMG.set_as_default = 1
        INNER JOIN tbl_freshbooks FB ON FB.merchantID = FTI.merchantID
        LEFT JOIN tbl_subscription_auto_invoices TSAI ON FTI.invoiceID = TSAI.invoiceID AND TSAI.app_type = 3 AND TSAI.invoiceID = null
        WHERE FTI.BalanceRemaining <= TRP.amount AND FTI.BalanceRemaining > 0 AND ($autoCondition)
         GROUP BY FTI.txnID";
    
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    
    }
    
	public function get_invoice_data_pay($invoiceID, $merchantID)
    {
        $res = array();

        $this->db->select('inv.*,cust.*');
        $this->db->from('Freshbooks_test_invoice inv');
        $this->db->join('Freshbooks_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->where('inv.invoiceID', $invoiceID);
        $this->db->where('inv.merchantID', $merchantID);
        $this->db->where('cust.merchantID', $merchantID);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $res = $query->row_array();
        }

        return $res;
    }

	public function get_open_invoices($userID, $mid){
	
		$today=date('Y-m-d');
	 	$res=array();
		$query=   $this->db->query("SELECT `qb`.*, `cs`.`fullName` AS custname, (case   when (sc.scheduleDate IS NOT NULL and IsPaid='0')
	 		THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'
			AND IsPaid='0') THEN 'Open' 
			when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and IsPaid='0')  THEN 'Overdue'
			when (qb.BalanceRemaining='0' and IsPaid='1') Then 'Paid'
			ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
			FROM (`Freshbooks_test_invoice` qb) 
			INNER JOIN `Freshbooks_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID` 
			LEFT JOIN `tbl_scheduled_invoice_payment` sc ON  (`sc`.`invoiceID`=`qb`.`invoiceID` and sc.merchantID='".$mid."') 
			WHERE `qb`.`merchantID` = '".$mid."' AND `cs`.`merchantID` = '".$mid."'  and  `qb`.`CustomerListID` = '".$userID."' 
			AND `cs`.`Customer_ListID` = '".$userID."'  and IsPaid='0' and qb.UserStatus ='unpaid'
			AND (sc.merchantID=  '".$mid."' OR sc.merchantID IS NULL)
			AND (sc.customerID=qb.CustomerListID OR sc.customerID IS NULL) 
			AND ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID)OR sc.customerID IS NULL)
			Group by `qb`.`invoiceID`
	 	");
		if($query->num_rows() > 0){
			$res= $query->result_array();
		}
	  
	  return $res;
  }
		
}