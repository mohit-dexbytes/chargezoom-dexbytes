<?php

class Freshbooks_model extends CI_Model
{

public function get_fb_invoice_data_pay($invoiceID,$merchID){
	
		$res = array();
	
		$this->db->select('inv.*,cust.firstName,cust.Customer_ListID,cust.lastName,cust.fullName,cust.userEmail,phoneNumber,cust.companyName,cust.companyID');
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where('inv.invoiceID' , $invoiceID);
	     $this->db->where('inv.merchantID' , $merchID);
	     $this->db->where('cust.merchantID' , $merchID);
	   
		$query = $this->db->get();

		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	}

	public function get_credit_by_customer($customerID){
          $r_amount = 0; 		
	$this->db->select('sum(creditAmount) as credit_amount');
		$this->db->from('tbl_credits');
		$this->db->where('creditStatus','0');
		$this->db->where('creditName', $customerID);
   	
		$query = $this->db->get();
		  $r_amount1 = $query->row_array();
	  
		return $r_amount =$r_amount1['credit_amount'];
	}
	
	public function get_transaction_history_data($userID, $customerID = null)
	{
	    $today = date("Y-m-d H:i");
	    $res = array();
	    $tcode = array(
	        '100', '200', '120', '111', '1'
	    );
	    $type = array(
	        'SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND', 'CREDIT'
	    );
	    $this->db->select('tr.*,  cust.Customer_ListID,cust.fullName');
	    $this->db->select('ifnull(sum(r.refundAmount),0) as partial', false);
	    $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', false);
	    $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
	    $this->db->from('customer_transaction tr');
	    $this->db->join('Freshbooks_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
	    $this->db->join('tbl_customer_refund_transaction r', 'tr.transactionID=r.creditTransactionID', 'left');
	    $this->db->where("tr.merchantID ", $userID);
	    $this->db->where("cust.merchantID ", $userID);
	    if(!empty($customerID)){
	    	$this->db->where("cust.Customer_ListID", $customerID);
	    }
	    $this->db->where_in('UPPER(tr.transactionType)', $type);
	    $this->db->where_in('(tr.transactionCode)', $tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	    $this->db->where("tr.transactionID >", 0);
	    $this->db->group_by("tr.transactionID", 'desc');
	    $query = $this->db->get();
	    if ($query->num_rows() > 0)
	    {

	        $res1 = $query->result_array();

	        foreach ($res1 as $k => $result)
	        {
	            $inv_data = $result['invoiceID'];
	            

	            if (!empty($result['invoice_id']) && preg_match('/^[0-9,]+$/', $result['invoice_id']))
	            {
	                $inv = $result['invoice_id'];

	                $qr = $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ',') as invoce  from  Freshbooks_test_invoice where invoiceID IN($inv) and  merchantID=$userID  GROUP BY merchantID");
	                if ($qr->num_rows() > 0)
	                {
	                    $res_inv = $qr->row_array() ['invoce'];
	                    $result['invoice_no'] = $res_inv;
	                }
	            }

	            $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {
                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID  ');

                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;
                        
                    }

                } else {
                    
                    $result['partial'] = $ref_amt;
                }

	            $res[] = $result;
	        }
	    }

	    return $res;
	}
	
	public function get_transaction_data($userID)
	{
		$today = date("Y-m-d H:i");
		$res = array();
			$tcode =array('100','200','120','111','1');
			$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
		$this->db->select('tr.*,  cust.Customer_ListID,cust.fullName');
    	$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
    	$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
        $this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
	    $this->db->where("tr.merchantID ", $userID);
	     $this->db->where("cust.merchantID ", $userID);
	      $this->db->where_in('UPPER(tr.transactionType)',$type);
          $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
	    $this->db->where("tr.transactionID >", 0);
		$this->db->group_by("tr.transactionID", 'desc');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 
	
		 foreach( $res1 as $k=> $result)
		 {
		    $inv_data = $result['invoiceID'];
		  
	
               if(!empty($result['invoice_id']) && preg_match('/^[0-9,]+$/', $result['invoice_id']))
		     {
		        $inv =$result['invoice_id']; 
		       
	         $qr=	       $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ',') as invoce  from  Freshbooks_test_invoice where invoiceID IN($inv) and  merchantID=$userID  GROUP BY merchantID");
		     if($qr->num_rows() > 0){
		      $res_inv=$qr->row_array()['invoce'];
		      $result['invoice_no'] =$res_inv;
		         }
		     }
         
		     $res[] =$result;
		 } 
		}
		
	
		return $res;	
	}
	
	
	
	
	public function get_invoice_transaction_data($userID)
	{
				$today = date("Y-m-d H:i");
					$res = array();
		$this->db->select('tr.id, tr.transactionID,tr.transactionAmount,tr.transactioncode,  cust.Customer_ListID,  cust.fullName ');
	    $this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
	    $this->db->where("tr.merchantID ", $userID);
	     $this->db->where("cust.merchantID ", $userID);
	    $this->db->order_by("tr.transactionDate", 'desc');
		$this->db->group_by("tr.transactionID", 'desc');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 
	
		 foreach( $res1 as $k=> $result)
		 {
		     if(!empty($result['invoiceID']) && preg_match('/^[0-9,]+$/', $result['invoiceID']))
		     {
		        $inv =$result['invoiceID']; 
		       
	         $qr=	       $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ',') as invoce  from  Freshbooks_test_invoice where invoiceID IN($inv) and  merchantID=$userID  GROUP BY merchantID");
		     if($qr->num_rows() > 0){
		      $res_inv=$qr->row_array()['invoce'];
		      $result['invoiceID'] =$res_inv;
		         }
		     }
		     $res[] =$result;
		 } 
		}
		return $res;	
	}
	public function getPartialNewAmount($transactionID)
    {
        $ref_amt = 0;
        
        
        $qr = $this->db->query('SELECT id FROM customer_transaction WHERE transactionID ="' . $transactionID . '" ORDER BY `id` DESC');
        $data = $qr->result_array();
        
        if (!empty($data)) {
            foreach ($data as $value) {
                $qr1 = $this->db->query('SELECT sum(transactionAmount) as transactionAmount FROM customer_transaction WHERE parent_id ="' . $value['id'] . '" ORDER BY `id` DESC');
                $data1 = $qr1->row_array();
                
                if (!empty($data1)) {
                    $ref_amt = $ref_amt + $data1['transactionAmount'];
                }

            }
            
            return $ref_amt;
            
        } else {
            return $ref_amt;
        }
    }
	
	public function get_product_table_data($table, $con,$order=array()) {
	    $data=array();
	  	//   $query = $this->db->select('*')->from($table)->where($con)->get();
		//	echo $this->db->last_query(); die;
		$this->db->select('*');
		$this->db->from($table);
		if(!empty($con)){
		$this->db->where($con);
		}
		if(!empty($order))
		{
			$this->db->order_by($order['name'],$order['type']);
		}
		$columns = array('Name', 'SalesDescription', 'Inventory', 'QuantityOnHand', 'saleCost');
		$i   = 0;
		$con = '';
		foreach ($columns as $item) {

			if ($_POST['search']['value']) {

				if ($i === 0) {
					$con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
				} else {
					$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

				}

			}

			$column[$i] = $item;
			$i++;
		}

		if ($con != '') {
			$con .= ' ) ';
			$this->db->where($con);
		}
		$column = $_POST['order'][0]['column'];

		if($column == 0){
			$orderName = 'Name';
		}elseif($column == 1){
			$orderName = 'SalesDescription';
		}elseif($column == 2){
			$orderName = 'Type';
		}elseif($column == 3){
			$orderName = 'purchaseCost';
		}elseif($column == 4){
			$orderName = 'saleCost';
		}else{
			$orderName = 'Name';    
		}
		$orderBY = $_POST['order'][0]['dir'];

		$this->db->order_by($orderName, $orderBY);

		if ($_POST['length'] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query  = $this->db->get();
		//	echo $this->db->last_query(); die;
		if($query->num_rows()>0 ) {
			$data = $query->result_array();
			return $data;
		} else {
			return $data;
		}
	}
	
	public function get_product_table_count($table, $con,$order=array()) {
	    $data=array();
		$this->db->select('*');
		$this->db->from($table);
		if(!empty($con)){
		$this->db->where($con);
		}
		$columns = array('Name', 'SalesDescription', 'Inventory', 'QuantityOnHand', 'saleCost');
		$i   = 0;
		$con = '';
		foreach ($columns as $item) {

			if ($_POST['search']['value']) {

				if ($i === 0) {
					$con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
				} else {
					$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

				}

			}

			$column[$i] = $item;
			$i++;
		}

		if ($con != '') {
			$con .= ' ) ';
			$this->db->where($con);
		}
		$query  = $this->db->get();
		$data = $query->num_rows();
		return $data;
	       
	}
}