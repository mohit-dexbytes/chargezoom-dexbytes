<?php

class Common extends CI_Model
{
    
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_list_data($table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
		
	}
	
             public function getCustomerData( $custID) {
			
			$user=array();	
			
			$this->db->select('*')->from('qb_test_customer')->where('ListID', $custID);
			if($query->num_rows) {
				$user = $query->row_array();
			   return $user;
			} else {
				return $user;
			}				
	
		}
	
	
	/**
	 * Queue up a request for the Web Connector to process
	 */
	public function enqueue($action, $ident, $priority = 0, $extra = null, $user = null)
	{
		$Queue = new QuickBooks_WebConnector_Queue($this->_dsn);
		
		return $Queue->enqueue($action, $ident, $priority, $extra, $user);
	}
}