<?php

class Company_model extends CI_Model
{

    public function get_invoice_data_by_due($condion,$type=1)
    {

        $res = array();

        $this->db->select("`inv`.CustomerListID,`inv`.CustomerFullName, cust.firstName, cust.lastName, `cust`.userEmail,`cust`.fullName,`cust`.companyName, (case when cust.Customer_ListID !='' then 'Active' else 'InActive' end) as status, `cust`.Customer_ListID, sum(inv.BalanceRemaining) as balance ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->where(['IsPaid' => 0]);
        $this->db->where($condion);
        if($type == 2){
            $this->db->where('cust.merchantID',$condion['inv.merchantID']);
        }
        $this->db->group_by('inv.CustomerListID');
        $this->db->limit(10);
        $this->db->order_by('balance', 'desc');

        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_transaction_failure_report_data($userID)
    {

        $res = array();
        $sql = "SELECT `tr`.`id`, `tr`.`transactionID`, `tr`.`invoiceID`, `tr`.`customerListID`,`tr`.`transactionStatus`, `tr`.`transactionCode`,  `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`,(select RefNumber from Xero_test_invoice where invoiceID = tr.invoiceID  limit 1) as RefNumber, `cust`.`fullName` FROM (`customer_transaction` tr) INNER JOIN `Xero_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID` WHERE `cust`.`merchantID` = '$userID' and tr.merchantID = '$userID'  AND `tr`.`transactionDate` >= DATE_SUB(CURDATE(), INTERVAL 30 Day) AND transactionCode NOT IN ('200','1','100','111') ";

        $query = $this->db->query($sql);
        //    echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_data_by_past_due($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,     `inv`.invoiceID,
            DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
            DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
            inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
            (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled'
            when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID limit 1 )='300'  then 'Failed'
            when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'
            when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0 then 'Success'
            else 'Canceled' end ) as status
            FROM Xero_test_invoice inv
            INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
            WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `BalanceRemaining` != 0 and
	        `inv`.`merchantID` = '$userID' and cust.merchantID = '$userID'   order by  BalanceRemaining   desc limit 10"
        );

        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_data_by_past_time_due($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT `inv`.RefNumber,`inv`.invoiceID, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 and userStatus ='' then 'Scheduled'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0   then 'Success'
	  else 'Canceled' end ) as status
	  FROM Xero_test_invoice inv
	  	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and `inv`.BalanceRemaining != 0 and
	  `inv`.`merchantID` = '$userID' and cust.merchantID = '$userID'   order by  `inv`.BalanceRemaining  desc limit 10");

        //    echo  $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            $res = $query->result_array();
            // print_r($res);die;
        }
        return $res;

    }

    public function get_transaction_report_data($userID, $minvalue, $maxvalue)
    {
        $res    = array();
        $trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')";

        $this->db->select('tr.*, cust.*, (select RefNumber from Xero_test_invoice where invoiceID= tr.invoiceID and CustomerListID = cust.customer_ListID and  merchantID="' . $userID . '"   ) as RefNumber ');
        $this->db->select('sum(tr.transactionAmount) as balance, tr.invoiceTxnID as refNumber, tr.invoiceTxnID,tr.id as TxnID, tr.transactionDate,  cust.Customer_ListID as CustomerListID, cust.Customer_ListID,  cust.fullName as FullName');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.customer_ListID AND tr.merchantID = cust.merchantID', 'INNER');
        $this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
        $this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
        $this->db->where('cust.merchantID', $userID);
	    $this->db->group_by("tr.transactionID", 'desc');

        $query = $this->db->get();
        //    echo $this->db->last_query(); die;
        if($query->num_rows() > 0)
		{
		    $res1= $query->result_array();
            foreach( $res1 as $result) {
		        if(!empty($result['invoice_id'])) {
                    $result['invoice_no'] = '';
                    $inv =$result['invoice_id'];
                    $invList = explode(',', $inv);

                    foreach($invList as $key1 => $value) {
                        $value = trim($value);
                        if(empty($value) || $value == ''){
                            unset($invList[$key1]);	
                        }
                        $invList[$key1] = "'".$value."'";
                    }

                    $inv = implode(',', $invList); 
                    $res_inv=array();
                
                    $qq= $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  
                    Xero_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");
                
                
                    if( $qq->num_rows >0){
                        $res_inv= $qq->row_array()['invoce']; 
                        $result['invoice_no'] =$res_inv;
                    }
		        }

		     $res[] =$result;
		    } 
		}
        return $res;

    }

    public function get_invoice_data_open_due($userID, $status)
    {

        $res = array();
        $con = '';
        if ($status == '8') {
            $con.="and `inv`.BalanceRemaining !='0.00' and `inv`.`IsPaid` = 0";

        }

        $today = date('Y-m-d');
        $query = $this->db->query("SELECT  `inv`.RefNumber, `inv`.invoiceID,  (`inv`.Total_payment - `inv`.BalanceRemaining) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	  DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,inv.TimeCreated as addOnDate,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0   then 'Success'
	  else 'Canceled' end ) as status

	  FROM Xero_test_invoice inv

	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE

	  `inv`.`merchantID` = '$userID' and cust.merchantID = '$userID'   $con order by  DueDate   asc");

        //    echo  $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_upcomming_data($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT  inv.RefNumber, `inv`.invoiceID, `inv`.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,  `sc`.scheduleDate,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'

	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0  then 'Success'
	  else 'Canceled' end ) as status
	  FROM Xero_test_invoice inv
	  INNER JOIN `tbl_scheduled_invoice_payment` sc ON `inv`.`invoiceID` = `sc`.`invoiceID`
	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 and
	  inv.merchantID = '$userID' and sc.merchantID = '$userID' and `sc`.`scheduleDate` != '0000-00-00'  group by  `sc`.`invoiceID`  order by DueDate  asc limit 10 ");

        //    echo  $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_qbo_tax_data($uID)
    {
        $res = array();

        $this->db->select('tax.*, tax.Name as friendlyName, total_tax_rate as taxRate');
        $this->db->from('tbl_taxe_code_qbo tax');
        //$this->db->join('tbl_taxe_code_qbo tax_code','tax.taxID = tax_code.taxRef','INNER');
        $this->db->where('tax.merchantID', $uID);
        $this->db->where('tax.is_active', 1);
        // $this->db->where('tax_code.merchantID', $uID);
        $this->db->order_by("tax.Name");

        $query = $this->db->get();

        $res = $query->result_array();
        return $res;
    }

    public function get_subplan_data($planID, $merchantID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT
                *
            FROM
                tbl_subscriptions_plan_xero
            INNER JOIN tbl_subscription_plan_item_xero TSXP ON TSXP.planID = '$planID'
            INNER JOIN Xero_test_item XTI ON XTI.productID = TSXP.itemListID AND XTI.merchantID = $merchantID
            WHERE tbl_subscriptions_plan_xero.planID = $planID
        ");

        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_terms_data($merchID)
    {
        $data  = array();
        $query = $this->db->query("SELECT *, dpt.id as dfid, pt.id as pid FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID UNION SELECT *, dpt.id as dfid, pt.id as pid  FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.merchantID = $merchID ORDER BY dfid IS NULL, dfid asc");
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data;
        }
    }

    public function check_payment_term($name, $netterm, $merchID)
    {
        $query = $this->db->query("SELECT * FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID WHERE dpt.name IN ('" . $name . "') AND pt.pt_netTerm IS NULL OR dpt.netTerm IN ('" . $netterm . "') AND pt.pt_netTerm IS NULL OR pt.pt_name IN ('" . $name . "') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('" . $netterm . "') AND pt.merchantID = $merchID  AND pt.enable != 1 UNION SELECT * FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.pt_name IN ('" . $name . "') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('" . $netterm . "') AND pt.merchantID = $merchID  AND pt.enable != 1 AND pt.merchantID = $merchID AND pt.enable != 1");
        return $query->num_rows();
    }

    public function chart_general_volume($mID)
    {

        $res    = array();
        $months = array();
        //   for ($i = 0; $i <= 11; $i++) {
        for ($i = 11; $i >= 0; $i--) {
            $months[] = date("M-Y", strtotime(date('Y-m') . " -$i months"));
        }

        foreach ($months as $key => $month) {

            // $sql = "SELECT Month(transactionDate) as Month,sum(transactionAmount) as volume FROM `customer_transaction` WHERE merchantID = '".$mID."' AND transactionDate >= CURDATE() - INTERVAL 1 YEAR GROUP BY Month(transactionDate)";
            $sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr
        WHERE tr.merchantID = "' . $mID . '"  AND
        date_format(tr.transactionDate, "%b-%Y") ="' . $month . '"  and
        (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or
        tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or
        tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture"
		or  tr.transactionType ="auth_capture"  or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and  tr.transaction_user_status!="3"
		  GROUP BY date_format(transactionDate, "%b-%Y") ';

            $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")
	 and  tr.transaction_user_status!="3" and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  tr.merchantID="' . $mID . '"  ');

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $row           = $query->row_array();
                $row['volume'] = $row['volume'] - $rfquery->row_array()['rm_amount'];

                $res[]              = $row;
                $res[$key]['Month'] = $month;
            } else {

                $res[$key]['volume'] = 0.00;
                $res[$key]['Month']  = $month;
            }
        }
        return $res;

    }

    public function chart_all_volume($mID)
    {
        $res    = array();
        $months = array();
        //   for ($i = 0; $i <= 11; $i++) {
        for ($i = 11; $i >=1 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
        }
        $months[] = date("M-Y", strtotime( date( 'Y-m' )." 0 months"));  
        $chart_arr = array();

        foreach ($months as $key => $month) {
            $sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr
			WHERE tr.merchantID = "' . $mID . '"  AND
			date_format(tr.transactionDate, "%b-%Y") ="' . $month . '"  and
			(tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or
			tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or
			tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture"
			or  tr.transactionType ="auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
			tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and  tr.transaction_user_status!="3"
			GROUP BY date_format(transactionDate, "%b-%Y") ';

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $row                        = $query->row_array();
                $earn                       = $row['volume'];
                $res[$key]['revenu_volume'] = $earn;
                $res[$key]['revenu_Month']  = $month;
            } else {

                $res[$key]['revenu_volume'] = 0.00;
                $res[$key]['revenu_Month']  = $month;
            }

            $sql2 = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr
			WHERE tr.merchantID = "' . $mID . '"  AND
			date_format(tr.transactionDate, "%b-%Y") ="' . $month . '"  and
			(tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Pay_sale" or
			tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" or  tr.transactionType ="auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and  tr.transaction_user_status!="3"
		  GROUP BY date_format(transactionDate, "%b-%Y") ';

            $query2 = $this->db->query($sql2);
            if ($query2->num_rows() > 0) {
                $row2                       = $query2->row_array();
                $earn2                      = $row2['volume'];
                $res[$key]['online_volume'] = $earn2;
                $res[$key]['online_Month']  = $month;
            } else {

                $res[$key]['online_volume'] = 0.00;
                $res[$key]['online_Month']  = $month;
            }
            $sql3 = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr
			WHERE tr.merchantID = "' . $mID . '"  AND
			date_format(tr.transactionDate, "%b-%Y") ="' . $month . '"  and
			(tr.transactionType ="Offline Payment") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and  tr.transaction_user_status!="3"
		  GROUP BY date_format(transactionDate, "%b-%Y") ';

            $query3 = $this->db->query($sql3);
            if ($query3->num_rows() > 0) {
                $row3                        = $query3->row_array();
                $earn                        = $row3['volume'];
                $res[$key]['offline_volume'] = $earn;
                $res[$key]['offline_Month']  = $month;
            } else {

                $res[$key]['offline_volume'] = 0.00;
                $res[$key]['offline_Month']  = $month;
            }
        }
        return $res;
    }

    public function get_company_invoice_data_payment($merchantID)
    {
        $res = array();

        $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where   (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture"
		or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")
	  and tr.transaction_user_status!="3" and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)   and  tr.merchantID="' . $merchantID . '" ');

        $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")
	    and tr.transaction_user_status!="3" and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and  tr.merchantID="' . $merchantID . '"  ');

        if ($rquery->num_rows() > 0) {

            $res['total_amount'] = $rquery->row_array()['total'];

            $res['refund_amount'] = $rfquery->row_array()['rm_amount'];
        } else {
            $res['total'] = 0;

            $res['refund'] = 0;

        }

        return $res;
    }

    public function get_process_trans_count($user_id)
    {

        $today = date("Y-m-d");
        $this->db->select('*');
        $this->db->from('customer_transaction tr');
        $this->db->where("tr.merchantID ", $user_id);
        $this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
        $this->db->order_by("tr.transactionDate", 'desc');

        $query = $this->db->get();
        //    echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return $query->num_rows();
        }
    }

    public function get_invoice_data_count($condition)
    {

        $num = 0;

        $this->db->select('inv.* ');
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->where($condition);
        $this->db->where("BalanceRemaining != 0.00");

        $query = $this->db->get();
        //     echo $this->db->last_query(); die;
        $num = $query->num_rows();
        return $num;

    }

    public function get_invoice_data_count_failed($user_id)
    {
        $today = date('Y-m-d');
        $num   = 0;

        $sql = "SELECT `inv`.* FROM (`Xero_test_invoice` inv) INNER JOIN `customer_transaction` tr ON `tr`.`invoiceID` = `inv`.`invoiceID` AND tr.transactionCode = '300' WHERE  `inv`.`merchantID` = '$user_id' AND `tr`.`merchantID` = '$user_id' ";

        $query = $this->db->query($sql);
        $num   = $query->num_rows();
        return $num;

    }

    public function get_paid_invoice_recent($condion)
    {

        $res   = array();
        $today = date('Y-m-d');

        $this->db->select("cust.FullName,inv.CustomerListID, cust.companyName, inv.refNumber, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('customer_transaction trans', 'trans.invoiceID = inv.invoiceID', 'INNER');
        $this->db->order_by('trans.transactionDate', 'desc');
        $this->db->limit(10);
        $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
        $this->db->where($condion);

        $query = $this->db->get();
        // echo $this->db->last_query();  die;
        $res = $query->result_array();
        //    print_r($res); die;
        return $res;

    }
    public function get_recent_volume_dashboard_old($condion)
    {

        $res   = array();
        $today = date('Y-m-d');

        $this->db->select("cust.FullName,inv.CustomerListID, cust.companyName, inv.refNumber, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('customer_transaction trans', 'trans.invoiceID = inv.invoiceID', 'INNER');
        $this->db->order_by('trans.transactionDate', 'desc');
        $this->db->limit(10);
        $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
        $this->db->where($condion);

        $query = $this->db->get();
        // echo $this->db->last_query();  die;
        $res = $query->result_array();
        return $res;
    }

    public function get_recent_volume_dashboard($mID,$month)
     {   
  
     
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
           customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID WHERE tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
           date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
            or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
         tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
         and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2" )    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
         
        $query = $this->db->query($sql);
        
        
         if($query->num_rows()>0)
         {    
             $row = $query->row_array();
             $earn = $row['volume'];
            
         }
         /*$rfquery = $this->db->query('select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
        inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
           date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and 
        (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
        and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
        and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL ');
         //$rfquery = $this->db->query($rfsql);
         //print_r($rfquery->row_array()); die;
         if($rfquery->num_rows()>0)
         {    
            $row = $rfquery->row_array();
            $refund= $row['refund'];
            
         }*/
         
         $res = $earn - $refund;
         return  $res; 
    }
    public function get_creditcard_payment_old($condion)
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select("cust.FullName,inv.CustomerListID, cust.companyName, inv.refNumber, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('customer_transaction trans', 'trans.invoiceID = inv.invoiceID', 'INNER');
        $this->db->order_by('trans.transactionDate', 'desc');
        $this->db->limit(10);
        $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
        $this->db->where('trans.transactionType != "Offline Payment"');
        $this->db->where($condion);

        $query = $this->db->get();
        // echo $this->db->last_query();  die;
        $res = $query->result_array();
        //    print_r($res); die;
        return $res;
    }

    public function get_creditcard_payment($mID,$month)
      {   
   
      
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        // $month = date("M-Y");
           
      
        // $sql = "SELECT Month(transactionDate) as Month,sum(transactionAmount) as volume FROM `customer_transaction` WHERE merchantID = '".$mID."' AND transactionDate >= CURDATE() - INTERVAL 1 YEAR GROUP BY Month(transactionDate)";
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and  tr.gateway NOT LIKE "%echeck%" and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

        
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
         
        }

        /*$rfquery = $this->db->query('select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
            inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
               date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway NOT LIKE "%echeck%" and 
            ( UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "REFUND" )
            and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
            and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL  GROUP BY date_format(transactionDate, "%b-%Y") ');
        if($rfquery->num_rows()>0)
        {    
            $row = $rfquery->row_array();
            $refund= $row['refund'];

        }*/

        $res = $earn - $refund;
        return  $res; 
    }  

    public function get_offline_payment_old($condion)
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select("cust.FullName,inv.CustomerListID, cust.companyName, inv.refNumber, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('customer_transaction trans', 'trans.invoiceID = inv.invoiceID', 'INNER');
        $this->db->order_by('trans.transactionDate', 'desc');
        $this->db->limit(10);
        $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
        $this->db->where('trans.transactionType = "Offline Payment"');
        $this->db->where($condion);

        $query = $this->db->get();
        // echo $this->db->last_query();  die;
        $res = $query->result_array();
        //    print_r($res); die;
        return $res;
    }

    public function get_offline_payment($mID,$month)
    {   
        $res= 0.00;
        //$month = date("M-Y");
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr    
         inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
         WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
         date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and (tr.transactionType ="Offline Payment") and  (tr.transactionCode ="100" or 
           tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
           and ( tr.transaction_user_status !="3" )    GROUP BY date_format(transactionDate, "%b-%Y") ' ;
           
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
            $earn= $row['volume'];
               
              // $res[] = $row; 
            $res =$earn;
              
        }else{
              
            $res =0.00;
              
        }
        return  $res; 
    }

    public function get_outstanding_payment($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $month = date("M-Y");
        $query = $this->db->query("SELECT  inv.RefNumber, `inv`.invoiceID, `inv`.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,
			Month(inv.DueDate),sum(`inv`.BalanceRemaining) as balance,sum(`inv`.AppliedAmount) as payment FROM Xero_test_invoice inv 
			WHERE  inv.IsPaid = '0'   and userStatus!='1' and
			inv.merchantID = '$userID'");

        //    echo  $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $pay  = str_replace('-', '', $row['payment']);
            $earn = $row['balance'] - (float) $pay;
            $res  = $earn;

        }
        return $res;
    }

    public function get_invoice_due_by_company($condion, $status)
    {

        $res   = array();
        $today = date('Y-m-d');
        $this->db->select("cust.fullName as label, inv.DueDate,  sum(inv.BalanceRemaining) as balance ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->group_by('cust.Customer_ListID');
        $this->db->order_by('balance', 'desc');
        $this->db->limit(5);
        if ($status == '1') {
            $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '" . $today . "'  AND `BalanceRemaining` > 0 ");
        } else {
            $this->db->where("`BalanceRemaining` > 0  ");
        }
        $this->db->where($condion);

        $query = $this->db->get();
        $res   = $query->result_array();
        // echo $this->db->last_query(); die;
        return $res;
    }
    public function get_oldest_due($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT cust.FullName, `inv`.invoiceID,  `inv`.refNumber, `inv`.CustomerListID, (`inv`.Total_payment) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.FullName, DATEDIFF('$today', inv.DueDate) as aging_days
        FROM Xero_test_invoice inv
        INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID` and  cust.merchantID='$userID'
        WHERE  BalanceRemaining > 0 and
        `inv`.`merchantID` = '$userID' AND `cust`.`merchantID` = '$userID' and  cust.merchantID='$userID' order by  inv.DueDate asc limit 10 ");

        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }
    public function get_plan_data($userID)
    {

        $result = array();

        $this->db->select('qb_item.Name as Name, qb_item.FullName as FullName,qb_item.Parent_FullName as Parent_FullName,qb_item.Type as Type, qb_item.SalesPrice as SalesPrice, qb_item.ListID as ListID,qb_item.EditSequence as EditSequence, Discount, qb_item.IsActive ,  (case when qb_item.IsActive="true" then "Done"  when qb_item.IsActive="false" then "Done" else "Done" end) as c_status   ');
        $this->db->from('qb_test_item qb_item');
        $this->db->join('tbl_company comp', 'comp.id = qb_item.companyListID', 'INNER');

        $this->db->where('comp.merchantID', $userID);

        $query = $this->db->get();
        //    echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

        }

        return $result;
    }

    public function get_schedule_payment_invoice($mID)
    {
        $res = array();
        //    Invoice | Customer Name | Balance | Due Date | Amount | Scheduled Date
        //$this->db->select(Xero_test_invoice qt inner join     tbl_scheduled_invoice_payment sc on sc.invoiceID=qt.invoiceID where  sc.scheduleDate!='0000-00-00' and  sc.customerID=qt.CustomerListID and qt.merchantID='$uid' and  sc.merchantID=$uid and qt.BalanceRemaining > 0 ),'0') as schedule
        $this->db->select('qt.CustomerListID,qt.invoiceID,qt.refNumber as RefNumber, cust.fullName,qt.BalanceRemaining, qt.DueDate,qt.Total_payment,sc.scheduleDate');
        $this->db->from('Xero_test_invoice qt');
        $this->db->join('Xero_custom_customer cust', 'qt.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_scheduled_invoice_payment sc', 'qt.invoiceID = sc.invoiceID', 'INNER');
        $this->db->where('cust.merchantID', $mID);
        $this->db->where('qt.merchantID', $mID);
        $this->db->where('sc.merchantID', $mID);
        $this->db->where('IsPaid', '0');
        $this->db->where("sc.scheduleDate IS NOT NULL  ");

        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        //    print_r($res);die;
        return $res;
    }

    public function get_schedule_payment_invoice_csv($mID)
    {
        $res = array();
        //    Invoice | Customer Name | Balance | Due Date | Amount | Scheduled Date
        //$this->db->select(Xero_test_invoice qt inner join     tbl_scheduled_invoice_payment sc on sc.invoiceID=qt.invoiceID where  sc.scheduleDate!='0000-00-00' and  sc.customerID=qt.CustomerListID and qt.merchantID='$uid' and  sc.merchantID=$uid and qt.BalanceRemaining > 0 ),'0') as schedule
        $this->db->select('qt.refNumber as RefNumber, cust.fullName,qt.BalanceRemaining, qt.DueDate,qt.Total_payment,sc.scheduleDate');
        $this->db->from('Xero_test_invoice qt');
        $this->db->join('Xero_custom_customer cust', 'qt.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_scheduled_invoice_payment sc', 'qt.invoiceID = sc.invoiceID', 'INNER');
        $this->db->where('cust.merchantID', $mID);
        $this->db->where('qt.merchantID', $mID);
        $this->db->where('sc.merchantID', $mID);
        $this->db->where('IsPaid', '0');
        $this->db->where("sc.scheduleDate IS NOT NULL ");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        //    print_r($res);die;
        return $res;
    }

    public function get_subcription_qbo_data()
    {
        $res = array();

        $today = date('Y-m-d');
        $this->db->select('sbs.*, cust.fullName, cust.companyName, spl.planName as sub_planName, tmg.*');
        $this->db->from('tbl_subscriptions_qbo sbs');
        $this->db->join('Xero_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join('tbl_subscriptions_plan_qbo spl', 'spl.planID = sbs.planID', 'left');
        $this->db->where("sbs.merchantDataID =cust.merchantID ");

        $this->db->where('sbs.nextGeneratingDate', $today);
        $this->db->where('cust.customerStatus', 'true');
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_invoice_item_data($invoiceID)
    {
        $res = array();

        if ($this->session->userdata('logged_in')) {
            $daa['login_info'] = $this->session->userdata('logged_in');
            //print_r($data['login_info']); die;
            $user_id = $daa['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $daa['login_info'] = $this->session->userdata('user_logged_in');
            $user_id           = $daa['login_info']['merchantID'];
        }

        $sql = "Select itm.*, qbo_item.* from  tbl_qbo_invoice_item  itm  inner join QBO_test_item qbo_item on  itm.itemRefID = qbo_item.productID where  itm.invoiceID = '$invoiceID' and itm.merchantID='$user_id' and qbo_item.merchantID='$user_id'  ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_details_data($invID)
    {
        $res = array();

        if ($this->session->userdata('logged_in')) {
            $daa['login_info'] = $this->session->userdata('logged_in');
            //print_r($data['login_info']); die;
            $merchantID = $daa['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $daa['login_info'] = $this->session->userdata('user_logged_in');
            $merchantID        = $daa['login_info']['merchantID'];
        }

        $res = array();
        $this->db->select('inv.invoiceID as invoiceID, CustomerListID, inv.refNumber as invoiceNumber, inv.BalanceRemaining as Balance,
		  inv.Total_payment as AppliedAmount,ShipAddress_Addr1 as saddress1,ShipAddress_Addr2 as saddress2,ShipAddress_City as scity,
		  ShipAddress_State as sstate,ShipAddress_PostalCode as szipcode,DueDate,BillAddress_Addr1 as baddress1,
		  BillAddress_Addr2 as baddress2, BillAddress_City as bcity,BillAddress_State as bstate,
		  BillAddress_Country as country,BillAddress_PostalCode as bzipcode,taxRate as TaxRate,totalTax as TotalTax,taxID as TaxListID');
        $this->db->from('Xero_test_invoice inv');
        $this->db->where('invoiceID', $invID);
        $this->db->where('merchantID', $merchantID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res['invoice_data']         = $query->row_array();
            $res['invoice_data']['type'] = 1;

            $this->db->select('Name as itemName, itemDescription as Description,itemQty as Quantity, itemPrice as itemPrice');
            $this->db->from('tbl_qbo_invoice_item invtm');
            $this->db->join('QBO_test_item itm', 'itm.productID=invtm.itemRefID', 'inner');
            $this->db->where('invtm.invoiceID', $invID);
            $this->db->where('invtm.merchantID', $merchantID);
            $this->db->where('itm.merchantID', $merchantID);
            $query2 = $this->db->get();
            if ($query2->num_rows() > 0) {
                $res['item_data'] = $query2->result_array();

            } else {
                $res['item_data'] = array();
            }

            $this->db->select('cs.userEmail as customerEmail,phoneNumber as PhoneNumber,cs.companyName as company,cs.firstName as FirstName, cs.lastName as LastName,cs.fullName as FullName,
			  ship_address1 as saddress1,ship_address2 as saddress2,ship_city as scity,ship_state as sstate,
		  ship_zipcode as szipcode, address1 as baddress1,address2 as baddress2,
		  City as bcity,State as bstate,Country as country,zipCode as bzipcode');
            $this->db->from('Xero_custom_customer cs');
            $this->db->where('Customer_ListID', $res['invoice_data']['CustomerListID']);
            $this->db->where('merchantID', $merchantID);
            $query1 = $this->db->get();

            if ($query1->num_rows() > 0) {
                $res['customer_data'] = $query1->row_array();

            } else {
                $res['customer_data'] = array();
            }

        }

        return $res;

    }

    public function get_invoice_data_auto_pay($args = [])
	{
		$res = array();
		$today = date('Y-m-d');
		
		$this->db->select('inv.*, cust.companyID, cust.firstName, cust.lastName, cust.Customer_ListID,
		tci.cardID, tci.gatewayID, tmg.gatewayType,tmg.gatewayUsername,tmg.gatewayPassword,tmg.gatewaySignature,tci.scheduleDate as schedule, tci.scheduleAmount,  mr.merchID as merchantID, mr.resellerID, mr.companyName as mrCompanyName, tci.scheduleID, tci.autoInvoicePay, tci.autoPay, tci.paymentMethod' );
		$this->db->from('Xero_test_invoice inv');
		$this->db->join('tbl_scheduled_invoice_payment tci','inv.invoiceID =tci.invoiceID AND inv.merchantID = tci.merchantID','INNER');
		$this->db->join('Xero_custom_customer cust','inv.CustomerListID = cust.Customer_ListID AND inv.merchantID = cust.merchantID','INNER');
		// $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
		$this->db->join('tbl_merchant_gateway tmg','tci.gatewayID = tmg.gatewayID','INNER');
		$this->db->join('tbl_merchant_data mr','mr.merchID= inv.merchantID','inner'); 
		$this->db->where("(DATE_FORMAT(tci.scheduleDate,'%Y-%m-%d')='$today' OR (DATE_FORMAT(inv.DueDate,'%Y-%m-%d') ='$today' or tci.scheduleDate IS NULL))");                                          
        $this->db->where('inv.IsPaid', '0');
		$this->db->where("inv.BalanceRemaining != ", '0.00');
		$this->db->where('( tci.autoPay = 1 OR tci.autoInvoicePay = 1 )');
        if(isset($args['scheduleID']) && !empty($args['scheduleID'])){
            $this->db->where('tci.scheduleID', $args['scheduleID']);
        }
		$this->db->where('tci.isProcessed', '0');
        $this->db->where('mr.isSuspend', '0');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('mr.isEnable', '1');
        // $this->db->group_by('inv.id');

        $this->db->order_by('inv.id', 'DESC');
		
		$query = $this->db->get();
   		// echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return $res = $query->result_array();
		}
		
		return $res;
	}

    public function getAutoRecurringInvoices(){
        $res = array();
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));
        $todayDate = date('d');
        $lastDay = date('t');

        $autoCondition = "( 
            ( QTI.DueDate = '$today' AND TRP.optionData = 1)
            OR ( QTI.TimeCreated LIKE '$today%' AND TRP.optionData = 2)
            OR ( QTI.TimeCreated LIKE '$yesterday%' AND TRP.optionData = 3)
            OR ( ( TRP.month_day = $todayDate OR ( TRP.month_day > '$lastDay' AND '$lastDay' = '$todayDate') ) AND TRP.optionData = 4)
        )";
        $sql = "SELECT QTI.*, QCC.companyID, QCC.firstName, QCC.lastName, QCC.userEmail,QCC.companyName,QCC.phoneNumber as Phone, QCC.Customer_ListID, QCC.fullName, TMG.gatewayUsername, TMG.gatewayPassword, TMG.gatewaySignature,TMG.gatewayMerchantID, TMG.gatewayType, TRP.cardID, TRP.recurring_send_mail, TMG.gatewayID, QT.accessToken, QT.refreshToken, QT.realmID, TMD.resellerID,TMD.merchID
        FROM Xero_test_invoice QTI
        INNER JOIN tbl_recurring_payment TRP ON TRP.merchantID = QTI.merchantID AND TRP.customerID = QTI.CustomerListID
        INNER JOIN tbl_merchant_data TMD ON TMD.merchID = QTI.merchantID AND TMD.isSuspend = 0 AND TMD.isDelete = 0 AND TMD.isEnable = 1
        INNER JOIN Xero_custom_customer QCC ON QCC.merchantID = QTI.merchantID AND QCC.Customer_ListID = QTI.CustomerListID
        INNER JOIN tbl_merchant_gateway TMG ON TRP.merchantID = TMG.merchantID AND TMG.set_as_default = 1
        INNER JOIN QBO_token QT ON QT.merchantID = QTI.merchantID
        LEFT JOIN tbl_subscription_auto_invoices TSAI ON QTI.invoiceID = TSAI.invoiceID AND TSAI.app_type = 4 AND TSAI.invoiceID = null
        WHERE QTI.BalanceRemaining <= TRP.amount AND QTI.BalanceRemaining > 0 AND ($autoCondition)
         GROUP BY QTI.id";
    
        $query = $this->db->query($sql);
           // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    
    }
    public function get_paid_recent($userID){
        
        $res =array();

        $today = date("Y-m-d H:i");
        $res =array();
        $tcode =array('100','200', '120','111','1');
        $type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT');
    
        $this->db->select('(tr.transactionAmount) as balance,tr.invoiceTxnID,tr.transactionID,tr.id as TxnID, tr.transactionDate,  cust.Customer_ListID as CustomerListID, cust.FullName, cust.companyName,(select refNumber from Xero_test_invoice where invoiceID= tr.invoiceID AND merchantID = '.$userID.' limit 1) as refNumber');
        
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceTxnID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', FALSE);
        //$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID AND  cust.merchantID =  tr.merchantID','INNER');
        $this->db->where("tr.merchantID ", $userID);
        //$this->db->where("cust.merchantID ", $userID);
        $this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->order_by("tr.id", 'desc');
        $this->db->group_by("tr.transactionID");
        $this->db->limit(10);
        $query = $this->db->get();
        //  echo $this->db->last_query(); die;
        if($query->num_rows() > 0)
        {
        
            $res = $query->result_array();
         //print_r($res); die;
          
        }
        
        return $res;
    
    }

    public function get_recent_transaction_data($userID, $customerID = null)
	{
		$today = date("Y-m-d H:i");
		$res=array();
		$tcode =array('100','200', '120','111','1');

		$type=array('SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY-SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND', 'CREDIT');
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as balance, tr.invoiceTxnID as refNumber, tr.invoiceTxnID,tr.id as TxnID, tr.transactionDate,  cust.Customer_ListID as CustomerListID, cust.Customer_ListID,  cust.fullName as FullName');
		$this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', FALSE);
		$this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
		if($customerID){
        	$this->db->where("tr.customerListID", $customerID);
		}
	    $this->db->group_by("tr.transactionID", 'desc');
	    $this->db->order_by("tr.id", 'desc');
	    $this->db->limit(10);
		$query = $this->db->get();
   		//echo   $this->db->last_query(); die;
		if($query->num_rows() > 0)
		{
		    $res1= $query->result_array();
            foreach( $res1 as $result) {
		        if(!empty($result['invoice_id'])) {
                    $result['invoice_no'] = '';
                    $inv =$result['invoice_id'];
                    $invList = explode(',', $inv);

                    foreach($invList as $key1 => $value) {
                        $value = trim($value);
                        if(empty($value) || $value == ''){
                            unset($invList[$key1]);	
                        }
                        $invList[$key1] = "'".$value."'";
                    }

                    $inv = implode(',', $invList); 
                    $res_inv=array();
                
                    $qq= $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  
                    Xero_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");
                
                
                    if( $qq->num_rows >0){
                        $res_inv= $qq->row_array()['invoce']; 
                        $result['invoice_no'] =$res_inv;
                    }
		        }

		     $res[] =$result;
		    } 
		}
	
		return $res;
		
	}

    public function chart_all_volume_new($mID)
    {
        $res= array();
        $months=array();
        $resObj = array();
            //   for ($i = 0; $i <= 11; $i++) {
        for ($i = 11; $i >=1 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
        }  
        $months[] = date("M-Y", strtotime( date( 'Y-m' )." 0 months"));
        $chart_arr = array();
        $totalRevenue = 0;
        $totalRevenue = $totalCCA = $totalECLA = 0;
        foreach ($months as $key => $month) {
            $earn = $this->get_recent_volume_dashboard($mID,$month);
            $res[$key]['revenu_volume'] = $earn;
            $res[$key]['revenu_Month'] =$month;

            $earn2 = $this->get_creditcard_payment($mID,$month);
            $res[$key]['online_volume'] =$earn2;
            $res[$key]['online_Month'] = $month;
                
            /*$earn3 = $this->get_offline_payment($mID,$month);
            $res[$key]['offline_volume'] =$earn3;
            $res[$key]['offline_Month'] =$month;*/

            $earn4 = $this->get_eCheck_payment($mID,$month);
            $res[$key]['eCheck_volume'] =$earn4;
            $res[$key]['eCheck_Month'] =$month;

            $totalRevenue = $totalRevenue + $earn;
            $totalCCA = $totalCCA + $earn2;
            $totalECLA = $totalECLA + $earn4;
        }
        $resObj['data'] = $res;
        $resObj['totalRevenue'] = $totalRevenue;
        $resObj['totalCCA'] = $totalCCA;
        $resObj['totalECLA'] = $totalECLA;
        return $resObj;
    }

    public function get_echeck_payment($mID,$month)
    {   
       
         $res= 0.00;
         $earn = 0.00;
         $refund = 0.00;
         $refund1 = 0.00;
        
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and  tr.gateway LIKE "%echeck%"  and (tr.transactionType ="Sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3" and  tr.transaction_user_status !="2" )  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

        
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
         
        }

        /*$rfquery = $this->db->query('select Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
            inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
               date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway LIKE "%echeck%"  and 
            ( UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "REFUND" )
            and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
            and (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL  GROUP BY date_format(transactionDate, "%b-%Y") ');

        if($rfquery->num_rows()>0)
        {    
            $row = $rfquery->row_array();
            $refund= $row['refund'];

        }*/

        /*$this->db->select('sum(tr.transactionAmount) as transactionAmount ');
        $this->db->select('ifnull(sum(r.refundAmount),0) as partial', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('tbl_customer_refund_transaction r', 'tr.transactionID=r.creditTransactionID', 'left');
        $this->db->where('r.merchantID',$mID);
        $this->db->where('tr.gateway like',"%echeck%");
        $this->db->where('tr.merchantID',$mID);
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID", 'desc');

        $query = $this->db->get();
        //    echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            $res1                     = $query->row_array();
            $refund1 = $res1['transactionAmount'];
        }*/

        if($earn >= $refund){
            $earn = $earn - $refund;
        }
        
        
        $res = $earn;
        return  $res;
            
    }

    public function template_data($con){
        
        $res=array();
        $this->db->select('tp.*, typ.*');
        $this->db->from('tbl_email_template tp');
        $this->db->join('tbl_teplate_type typ','typ.typeID=tp.templateType','left');
        $this->db->where($con);
        $query = $this->db->get();
        return $res =  $query->row_array();
    }  
    public function get_invoice_data_pay($invoiceID)
    {
        $res = array();
    
        $this->db->select('inv.*, cust.*, cmp.merchantID, cmp.qbwc_username');
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
        $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
        $this->db->where('inv.invoiceID' , $invoiceID);
        $this->db->where('cust.customerStatus', '1');
        $query = $this->db->get();
        if($query->num_rows() > 0){
        
          return $res = $query->row_array();
        }
        
        return $res;
    } 

    public function get_subscription_invoice_data($userID, $subID)
	{
        $today = date('Y-m-d');

		$sql = "SELECT
        qb.invoiceID,
        qb.refNumber,
        qb.CustomerListID,
        qb.DueDate,
        qb.Total_payment,
        qb.BalanceRemaining,
        qb.AppliedAmount,
        qb.IsPaid,
        qb.UserStatus,
        cs.fullName AS custname,
        (
            CASE WHEN IsPaid = 'true' THEN 'Paid' WHEN IsPaid = 'true' AND userStatus = 'Paid' THEN 'Paid' WHEN(
                sc.scheduleDate IS NOT NULL AND IsPaid = 'false' AND userStatus != 'cancel'
            ) THEN 'Scheduled' WHEN DATE_FORMAT(
                qb.DueDate,
                '%Y-%m-%d') >= '$today' AND IsPaid = 'false' AND userStatus != 'cancel' THEN 'Open' WHEN DATE_FORMAT(
                    qb.DueDate,
                    '%Y-%m-%d') < '$today' AND IsPaid = 'false' AND userStatus != 'cancel' AND(
                    SELECT
                        transactionCode
                    FROM
                        customer_transaction
                    WHERE
                        invoiceTxnID = qb.invoiceID AND merchantID = '$userID'
                    LIMIT 1
                ) = '300' THEN 'Failed' WHEN DATE_FORMAT(
                    qb.DueDate,
                    '%Y-%m-%d') < '$today' AND IsPaid = 'false' AND userStatus != 'cancel' THEN 'Overdue' ELSE 'Voided'
                END
                ) AS
            status
                ,
                (
                    IFNULL(sc.scheduleDate, qb.DueDate)
                ) AS DueDate
            FROM
                (Xero_test_invoice qb)
            INNER JOIN tbl_subscription_auto_invoices TSAI ON
                TSAI.invoiceID = qb.invoiceID AND TSAI.subscriptionID = $subID AND app_type = 4
            INNER JOIN Xero_custom_customer cs ON
                cs.customer_ListID = qb.CustomerListID
            LEFT JOIN customer_transaction tr ON
                tr.invoiceID = qb.invoiceID
            LEFT JOIN tbl_scheduled_invoice_payment sc ON
                sc.invoiceID = qb.invoiceID AND sc.merchantID = qb.merchantID
            WHERE
                qb.merchantID = '$userID' AND cs.merchantID = '$userID' AND(
                    sc.merchantID = cs.merchantID OR sc.merchantID IS NULL
                ) AND(
                    sc.customerID = qb.CustomerListID OR sc.customerID IS NULL
                ) AND(
                    (
                        sc.customerID = cs.Customer_ListID AND sc.merchantID = cs.merchantID
                    ) OR sc.customerID IS NULL
                )
            GROUP BY
                qb.invoiceID";
        $res = [];
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }
        
        return $res;
	}

    public function get_subscription_revenue($userID, $subID)
	{
		$this->db->select(' (SUM(qb.Total_payment) - SUM(qb.BalanceRemaining) ) as revenue', false);
		$this->db->from("Xero_test_invoice qb");
		$this->db->join('tbl_subscription_auto_invoices TSAI', "TSAI.invoiceID = qb.invoiceID AND TSAI.subscriptionID = $subID AND app_type = 4", 'inner');

		$this->db->join('Xero_custom_customer cs', 'cs.customer_ListID = qb.CustomerListID', 'inner');
		$this->db->join('customer_transaction tr', 'tr.invoiceID=qb.invoiceID ', 'left');

		$this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID and sc.merchantID=qb.merchantID', 'left');

		$this->db->where('qb.merchantID', $userID);
		$this->db->where('cs.merchantID', $userID);

		$this->db->where('( sc.merchantID=cs.merchantID OR sc.merchantID IS NULL ) and (sc.customerID=qb.CustomerListID or sc.customerID IS NULL) and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ');
		$this->db->group_by('qb.invoiceID');
	
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			
			$res = $query->row_array(); 

			return $res['revenue'];
		}
		
	   	return 0;
	}
}
