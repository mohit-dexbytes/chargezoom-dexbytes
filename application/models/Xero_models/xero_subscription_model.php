<?php


Class Xero_subscription_model extends CI_Model
{

    public function get_subscriptions_data($userID){
	
					
		$this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions_xero sbs');
		$this->db->join('Xero_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	public function get_subscriptions_plan_data($userID){
	
					
		$this->db->select('sbs.*, cust.fullName, cust.companyName, tmg.*,  spl.planName as sub_planName');
		$this->db->from('tbl_subscriptions_xero sbs');
		$this->db->join('Xero_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
		$this->db->join('tbl_subscriptions_plan_xero spl','spl.planID = sbs.planID','Left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	function get_customers_data($merchantID) 
	{
		 $sql = 'SELECT * from Xero_custom_customer WHERE merchantID = "'.$merchantID.'"';
		 

		$query = $this->db->query($sql);

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}
	
	public function get_subcription_data(){
	 $res    = array();
	 $today  = date('Y-m-d');	
	 $date   = date('d');
     $this->db->select('sb.*,cust.fullName ');
     $this->db->from('tbl_subscriptions_xero sb');
	 $this->db->join('Xero_custom_customer cust','sb.customerID = cust.Customer_ListID','INNER');
	 $this->db->where('sb.nextGeneratingDate = "'.$today.'" ');
	 $query = $this->db->get();
	 if($query->num_rows() > 0){
		return  $res=$query->result_array();
	 }
	 return  $res;

	
	}
		public function get_sub_invoice($table,$con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
		public function get_subplan_data($planID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT * FROM tbl_subscriptions_plan_xero INNER JOIN tbl_subscription_plan_item_xero ON tbl_subscription_plan_item_xero.planID =  '".$planID."' WHERE tbl_subscriptions_plan_xero.planID = '".$planID."'");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	}
	
		public function get_subscriptions_gatway_data($val){
	
					
	 $this->db->select('*,cust.fullName');
     $this->db->from('tbl_subscriptions_xero sb');
	 $this->db->join('Xero_custom_customer cust','sb.customerID = cust.Customer_ListID','INNER');
	 $this->db->where($val);
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
}