<?php

class Xero_model extends CI_Model
{

public function get_fb_invoice_data_pay($invoiceID,$mID){
		$res = array();
	
		$this->db->select('inv.*,cust.*');
		$this->db->from('Xero_test_invoice inv');
		$this->db->join('Xero_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where('inv.invoiceID' , $invoiceID);
	    $this->db->where('cust.merchantID' , $mID);
	    $this->db->where('inv.merchantID' , $mID);
	   
		$query = $this->db->get();

		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	}

public function get_credit_by_customer($customerID){
          $r_amount = 0; 		
      	$this->db->select('sum(creditAmount) as credit_amount');
		$this->db->from('tbl_credits');
		$this->db->where('creditStatus','0');
		$this->db->where('creditName', $customerID);	
		$query = $this->db->get();
		  $r_amount1 = $query->row_array();
		return $r_amount =($r_amount1['credit_amount'])?$r_amount1['credit_amount']:'0';
	}
	
	public function get_transaction_data($userID){
				$today = date("Y-m-d H:i");
		$this->db->select('tr.*, cust.*');
		$this->db->from('customer_transaction tr');
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}

public function get_invoice_data_pay($invoiceID, $merchantID)
	{
		$res = array();
	
		$this->db->select('inv.*,cust.*');
		$this->db->from('Xero_test_invoice inv');
		$this->db->join('Xero_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where('inv.invoiceID' , $invoiceID);
         $this->db->where('inv.merchantID' , $merchantID);
	    $this->db->where('cust.merchantID' , $merchantID);
		$query = $this->db->get();

		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	}
	

}
