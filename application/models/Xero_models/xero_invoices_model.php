<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Xero_invoices_model extends CI_Model {
    
  public function __construct()
    {
        parent::__construct();
        $this->load->database();
    } 


    var $table1 = 'Xero_test_invoice xe';
    var $order1 = array('BalanceRemaining' => 'desc');
  
    var $column1 = array( 'cs.fullName',  'xe.refNumber', 'xe.DueDate', 'xe.Total_payment', 'xe.BalanceRemaining', 'xe.invoiceID');



 
   
   	
  /**************** Invoices ***************/

   private function _get_datatables_invoices_query($condition)
   
    {
        	$this->db->select('xe.invoiceID,xe.refNumber,xe.CustomerListID, xe.DueDate,xe.Total_payment,xe.BalanceRemaining, xe.UserStatus, cs.fullName AS custname,
		     (case when (sc.scheduleDate IS NOT NULL and xe.IsPaid="0" and xe.UserStatus="0" ) THEN "Scheduled" when 
                (xe.DueDate >= CURDATE() AND xe.IsPaid="0" and xe.UserStatus="0" ) THEN "Open" 
                    when (xe.DueDate < CURDATE() and xe.IsPaid="0" and xe.UserStatus="0"  ) THEN "Overdue"
                    when (xe.IsPaid="1" and xe.UserStatus="0" ) Then "Paid" ELSE "Cancelled" END) as status
            ');
		 $this->db->select('(ifnull(sc.scheduleDate, xe.DueDate)) as DueDate',FALSE);
        $this->db->from("Xero_test_invoice xe");
	
		$this->db->join('Xero_custom_customer cs', 'cs.customer_ListID = xe.CustomerListID','inner');
		$this->db->join('tbl_scheduled_invoice_payment sc', 'xe.invoiceID = sc.invoiceID','left');
		 
	    $this->db->where('xe.merchantID', $condition['xe.merchantID']);
	    $this->db->where('cs.merchantID', $condition['xe.merchantID']);
	    
	    
	    $this->db->where('xe.companyID', $condition['xe.companyID']);
	    $this->db->where('( sc.merchantID ="'.$condition['xe.merchantID'].'" or  sc.merchantID IS NULL )
	    and (sc.customerID=xe.CustomerListID or sc.customerID IS NULL)
	    and ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID) OR sc.customerID IS NULL ) ' );
  
        
         $i = 0;
            $con='';
		   foreach ($this->column1 as $item) 
			{  
			
			 if($_POST['search']['value'])
				{
			       
			     
				if($i===0){
				    $con.= "(" .$item .' Like '. '"%'.$_POST['search']['value'].'%"' ;
				}else {
				    $con.= ' OR '.$item .' Like '.'"%'.$_POST['search']['value'].'%"' ;
				    
				}
					
				}
				
				
				$column[$i] = $item;
				$i++;
			} 
			
			if($con!='')
			{
			    $con.= ' ) ';
			    $this->db->where($con);
			}
			
			 
			if(isset($_POST['order']))
			{
				$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} else if(isset($this->order1))
				{
					$order= $this->order1;
					$this->db->order_by(key($order), $order[key($order)]);
				}else{
						$order = $this->order1;
						$this->db->order_by(key($order), 'desc');
				}
		 
    }
 
 
     function get_datatables_invoices($condition)
    { 
	 
        $this->_get_datatables_invoices_query($condition);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        
        return $query->result();
    }

  
    public function count_all_invoices($condition)
    {
		$this->db->select('*');
		
        $this->db->from("Xero_test_invoice xe");
	
		$this->db->join('Xero_custom_customer cs', 'cs.customer_ListID = xe.CustomerListID','inner');
		 
	    $this->db->where('xe.merchantID', $condition['xe.merchantID']);
	    $this->db->where('cs.merchantID', $condition['xe.merchantID']);
	    
	    
	    $this->db->where('xe.companyID', $condition['xe.companyID']);
	 
      $query=$this->db->get();
      $count =$query->num_rows();
        return $count;
    }
 
 
 
   function count_filtered_invoices($condition)
    {
	
        	$this->db->select('xe.invoiceID,xe.DueDate,xe.BalanceRemaining, cs.fullName AS custname,
		     (case when (sc.scheduleDate!="0000-00-00" and xe.IsPaid="0") THEN "Scheduled" when 
                (xe.DueDate >= CURDATE() AND xe.IsPaid="0") THEN "Open" 
                    when (xe.DueDate < CURDATE() and xe.IsPaid="0") THEN "Overdue" when (xe.BalanceRemaining="0" and xe.IsPaid="1") Then "Paid" ELSE "TEST" END) as status
            ');
		 $this->db->select('(ifnull(sc.scheduleDate, xe.DueDate)) as DueDate',FALSE);
        $this->db->from("Xero_test_invoice xe");
	
		$this->db->join('Xero_custom_customer cs', 'cs.customer_ListID = xe.CustomerListID','inner');
		$this->db->join('tbl_scheduled_invoice_payment sc', 'xe.invoiceID = sc.invoiceID','left');
		 
	    $this->db->where('xe.merchantID', $condition['xe.merchantID']);
	    $this->db->where('cs.merchantID', $condition['xe.merchantID']);
	    
	    
	    $this->db->where('xe.companyID', $condition['xe.companyID']);
	      $i = 0;

		   foreach ($this->column1 as $item) 
			{  
			
			 if($_POST['search']['value'])
				{
			  
			
					($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
					
				}
				
				
				$column[$i] = $item;
				$i++;
			} 
	    
	    
	    
	    
	    if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order1))
        {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $order = $this->order1;
            $this->db->order_by(key($order), 'desc');
        }
	    

	   $query = $this->db->get();
        return $query->num_rows();
     
		 
    }
 
 
 
    /********************** End ****************/



/*------------------------------------------------------ Payment Transaction Page For Xero Start ----------------------------------------------------------------- */

    var $table = 'customer_transaction tr';
    var $order = array('tr.transactionDate' => 'desc');
   var $column = array( 'tr.invoiceID', 'tr.transactionAmount',  'tr.transactionDate', 'tr.transactionType', 'tr.transactionCode', 'tr.transactionID',  'tr.id',  'cust.Customer_ListID', 'cust.fullName');


        private function _get_datatables_transaction_query($userID)
   
         {
        	$this->db->select('tr.invoiceID,tr.transactionAmount,tr.transactionDate,tr.transactionType,tr.transactionCode,tr.transactionID, tr.id,  cust.Customer_ListID,  cust.fullName ');
          	$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
    		 
            $this->db->from("customer_transaction tr");
    	
    		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
    	    $this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
    		 
    	    $this->db->where("tr.merchantID ", $userID);
            $this->db->where("cust.merchantID ", $userID);
            $this->db->group_by("tr.transactionID", 'desc');
            
        
         $i = 0;

		   foreach ($this->column as $item) 
			{  
			
			 if($_POST['search']['value'])
				{
			  
		
					($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
					
				}
				
				
				$column[$i] = $item;
				$i++;
			} 
			
			 
			if(isset($_POST['order']))
			{
				$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} else if(isset($this->order))
				{
					$order= $this->order;
					$this->db->order_by(key($order), $order[key($order)]);
				}else{
						$order = $this->order;
						$this->db->order_by(key($order), 'desc');
				}
		 
    }
 
 
     function get_datatables_transaction($userID)
    { 
        
	    $res= array();
	    
        $this->_get_datatables_transaction_query($userID);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
       
       
		if($query->num_rows() > 0)
		{
		
		 $res= $query->result();
     
		 
		}
		
	
	return $res;  
       
      
    }

  
    public function count_all_transaction($userID)
    {
        
    	$this->db->select('tr.invoiceID,tr.transactionAmount,tr.transactionDate,tr.transactionType,tr.transactionCode,tr.transactionID,tr.id,  cust.Customer_ListID,  cust.fullName ');
      	$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
		 
        $this->db->from("customer_transaction tr");
	
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
		 
	    $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("tr.transactionID", 'desc');
        $query=$this->db->get();
        
        $count =$query->num_rows();
   
        return $count;
    }
 
 
 
   function count_filtered_transaction($userID)
    {
		$this->db->select('tr.invoiceID,tr.transactionAmount,tr.transactionDate,tr.transactionType,tr.transactionCode,tr.transactionID, tr.id,  cust.Customer_ListID,  cust.fullName ');
      	$this->db->select('ifnull(sum(r.refundAmount),0) as partial', FALSE);
		 
        $this->db->from("customer_transaction tr");
	
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->join('tbl_customer_refund_transaction r','tr.transactionID=r.creditTransactionID','left');
		 
	    $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("tr.transactionID", 'desc');
        
	      $i = 0;

		   foreach ($this->column as $item) 
			{  
			
			 if($_POST['search']['value'])
				{
			  
			
					($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
					
				}
				
				
				$column[$i] = $item;
				$i++;
			} 
	    
	    
	    
	    
	    if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $order = $this->order;
            $this->db->order_by(key($order), 'desc');
        }
	    

	   $query = $this->db->get();
        return $query->num_rows();
     
		 
    }

   public function get_item_data($con)
   {
       $res=array();
       $this->db->select(' *, (select Name from Xero_test_item where productID =tbl_xero_invoice_item.itemRefID and  merchantID="'.$con['merchantID'].'")as Name,
       (select SalesDescription from Xero_test_item where productID =tbl_xero_invoice_item.itemRefID and  merchantID="'.$con['merchantID'].'")as itemDescription  ');
       $this->db->from('tbl_xero_invoice_item') ;
       
       $this->db->where($con);
         $query=$this->db->get();
         if($query->num_rows()>0)
         {
             return   $res = $query->result_array();  
         }
       return   $res;
   }





}