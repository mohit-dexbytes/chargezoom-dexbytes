<?php


Class Xero_customer_model extends CI_Model
{
	public function get_customer_invoice_data_sum($custID, $uid){
		$res = array();
		$today = date('Y-m-d');
    
       $res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,   sum(inv.BalanceRemaining) as amount FROM Xero_test_invoice inv  WHERE `inv`.`CustomerListID` = '".$custID."'   and inv.merchantID='".$uid."' and UserStatus!='VOIDED'   ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
	

public function get_customer_data($con)
{
   $res=array();
   $this->db->select('cust.*, (select sum(BalanceRemaining)  from Xero_test_invoice where merchantID ="'.$con['merchantID'].'" and CustomerListID=cust.Customer_ListID  and userStatus!="VOIDED" ) as balance ');
  $this->db->from('Xero_custom_customer cust');
	$this->db->where($con);
    $query = $this->db->get();
		if($query -> num_rows() > 0)

		 $res= $query->result_array(); 
        return $res;
		
}

    public function customer_by_id($id, $uid ='') 
	{
         $this->db->select('cus.*,cmp.*');
		
		$this->db->from('Xero_test_invoice cus');
	    $this->db->join('Xero_custom_customer cmp','cmp.Customer_ListID = cus.CustomerListID','inner');
		$this->db->where('cus.CustomerListID', $id);
		if($uid != ''){
			$this->db->where('cmp.merchantID', $uid);
			$this->db->where('cus.merchantID', $uid);
		}
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
	}
	

public function get_invoice_upcomming_data($userID, $mid)
	{
	
	      $today=date('Y-m-d');
       $res=array();
      $query=   $this->db->query("SELECT `xe`.*, `cs`.`fullName` AS custname, (case   when (sc.scheduleDate IS NOT NULL and IsPaid='0')
       THEN 'Scheduled' when (DATE_FORMAT(xe.DueDate, '%Y-%m-%d') >= '$today'
      AND IsPaid='0') THEN 'Open' 
      when (DATE_FORMAT(xe.DueDate, '%Y-%m-%d') <  '$today'   and IsPaid='0')  THEN 'Overdue'
      when (xe.BalanceRemaining='0' and IsPaid='1') Then 'Paid'
      ELSE 'TEST' END ) as status, ifnull(sc.scheduleDate, xe.DueDate) as DueDate
       FROM (`Xero_test_invoice` xe) 
       INNER JOIN `Xero_custom_customer` cs ON `cs`.`customer_ListID`=`xe`.`CustomerListID` 
       LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`xe`.`invoiceID`
       WHERE `xe`.`merchantID` = '".$mid."' AND `cs`.`merchantID` = '".$mid."'  and  `xe`.`CustomerListID` = '".$userID."' 
       AND `cs`.`Customer_ListID` = '".$userID."'  and IsPaid='0' and xe.UserStatus !='1'
        AND (sc.merchantID=  '".$mid."' OR sc.merchantID IS NULL)
       AND (sc.customerID=xe.CustomerListID OR sc.customerID IS NULL) 
        AND ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID)OR sc.customerID IS NULL)
       ");
       
       	if($query->num_rows() > 0){
		
		  $res= $query->result_array();
		}
		
		return $res;
	}

public function get_invoice_latest_data($userID, $mid){

	$res =array();
 	
 	$today  = date('Y-m-d');
    
 		$query  =  $this->db->query("SELECT *,(SELECT transactionDate FROM customer_transaction WHERE Xero_test_invoice.Total_payment = customer_transaction.transactionAmount AND    customer_transaction.CustomerListID = '".$userID."' AND customer_transaction.transactionStatus != 'Failure' LIMIT 1) AS transactionDate FROM Xero_test_invoice WHERE (BalanceRemaining = 0 or  BalanceRemaining!= Total_payment) AND CustomerListID = '".$userID."'   and Xero_test_invoice.merchantID=$mid  ");  

	 	if($query->num_rows() > 0){
			  return  $res=$query->result_array();
			}
	   
	    return  $res;
	    
 
 	}
	  public function get_invoice_details($uid)
	 {
    
    
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,ifnull(  ( Select count(*)  FROM Xero_test_invoice where  merchantID=$uid  and UserStatus!='VOIDED' and  BalanceRemaining=0.00 ),0) as Paid,
           ifnull( (select count(*) from Xero_test_invoice where  merchantID=$uid and  UserStatus!='VOIDED' and  DATE_FORMAT(DueDate,'%Y-%m-%d') >= '".$today."' and BalanceRemaining > 0 ),'0') as schedule, 
           ifnull( (select count(*) from Xero_test_invoice inner join customer_transaction tr on tr.invoiceID=Xero_test_invoice.invoiceID  where  Xero_test_invoice.merchantID=$uid and tr.merchantID=$uid and (tr.transactionCode ='300')  ),'0') as Failed
            FROM Xero_test_invoice inv  WHERE   inv.merchantID='".$uid."'  and inv.UserStatus!='VOIDED'  ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	


 	
 	public function get_customer_invoice_data_payment($custID, $mid){
$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  sum(AppliedAmount) as applied_amount,  
		  ifnull( (select sum(BalanceRemaining) from Xero_test_invoice where `CustomerListID` = '".$custID."' and merchantID=$mid  and UserStatus!='VOIDED' and  BalanceRemaining!='0.00' ),'0.00')as applied_due, 
ifnull( (select sum(BalanceRemaining) from Xero_test_invoice where `CustomerListID` = '".$custID."'  and merchantID=$mid   and UserStatus!='VOIDED' ),'0.00')as remaining_amount , 
		  ifnull( (select sum(BalanceRemaining) from Xero_test_invoice where CustomerListID = '".$custID."' and merchantID= $mid and   (userStatus ='AUTHORISED' OR userStatus ='DRAFT')  ),'0.00') as upcoming_balance FROM Xero_test_invoice   WHERE `CustomerListID` = '".$custID."' and merchantID=$mid  and BalanceRemaining=0.00   ");

		 $res=$query->row();
		 return  $res;
		
	}
	
	 public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
    public function customer_details($custID,$mID){
        
         $this->db->select('*');
		
		$this->db->from('Xero_custom_customer cus');
		$this->db->where('Customer_ListID', $custID);
			$this->db->where('merchantID', $mID);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
    }
	
	function get_customers_data($merchantID) 
	{
		 $this->db->select('*');
		 $this->db->from('Xero_custom_customer');
		 $this->db->where('customerStatus','true');
        $query = $this->db->get();    

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}
	
 





	public function get_transaction_data_refund($userID){
		$today = date("Y-m-d H:i");
		 $res=array();
        $query =$this->db->query("Select tr.*,  DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, cust.* from customer_transaction tr 
		inner join  Xero_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."' and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
		and ((tr.transaction_user_status !='success' and tr.transaction_user_status !='refund') or  tr.transaction_user_status  IS NULL)  and tr.gateway!='AUTH ECheck'  and tr.gateway!='NMI ECheck' and tr.transactionID!='' GROUP BY tr.transactionID DESC");

		if($query->num_rows() > 0){
		
		 $res= $query->result_array();
		}
		return $res;
	}
	
	public function get_transaction_data_captue($userID){
		
		
	$query =$this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  Xero_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
		 and ( (tr.transaction_user_status !='success' and tr.transaction_user_status !='refund') or tr.transaction_user_status IS NULL ) GROUP BY id ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	
		public function get_transaction_data_erefund($userID){
		$today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from Xero_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
      
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        
        $this->db->where("cust.merchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->like("tr.gateway", 'ECheck');


        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();

            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber   from   Xero_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }

                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');


                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;
                        
                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }
        return $res;
	}
	
	
		public function get_transaction_data_ecaptue($userID){
		
		
	$query =$this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  Xero_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
	 and (tr.gateway='NMI ECheck'  or tr.gateway='AUTH ECheck')  and (tr.transaction_user_status !='refund' or  tr.transaction_user_status  IS NULL) 
		   GROUP BY id ");
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
    
	
	public function get_subscription_data($userID){
	
					
		$this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
		
	public function get_transaction_data($userID){
				$today = date("Y-m-d H:i");
					$res =array();
		$this->db->select('tr.*, cust.*');
		$this->db->from('customer_transaction tr');
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
	    $this->db->order_by("tr.transactionDate", 'desc');
	    $this->db->where("tr.transactionID >", 0);
	   $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 foreach( $res1 as $result)
		 {
		     if(!empty($result['invoiceID']) && preg_match('/^[0-9,]+$/', $result['invoiceID']))
		     {
		         $inv =$result['invoiceID'];
		      $res_inv=  $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  Xero_test_invoice where invoiceID IN($inv) and  merchantID=$userID  GROUP BY merchantID ")->row_array()['invoce'];
		      $result['invoiceID'] =$res_inv;
		     }
		     $res[] =$result;
		 } 
		}
		return $res;
		
	}
	
	
		public function get_refund_transaction_data($userID){
	
		$query =	$this->db->query ("SELECT `tr`.*, `cust`.* FROM (`customer_transaction` tr) 
	INNER JOIN `Xero_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID`
	WHERE `tr`.`merchantID` = '$userID' AND (`tr`.`transactionType` = 'refund' OR `tr`.`transactionType` = 'pay_refund'  OR `tr`.`transactionType` = 'stripe_refund' OR `tr`.`transactionType` = 'credit' ) ORDER BY `tr`.`transactionDate` desc") ;
	
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	public function get_credit_user_data($con)
    {
		$res = array();
		$this->db->select('cr.*,sum(cr.creditAmount) as balance, cust.companyName, cust.fullName');
		$this->db->from('tbl_credits cr');
		$this->db->join('Xero_custom_customer cust','cr.creditName = cust.Customer_ListID','INNER');
	    $this->db->where($con);
		$this->db->group_by('cr.creditName');
		$this->db->order_by('cr.creditDate','desc');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 
    
    public function get_customer_note_data($custID,$mID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `Xero_custom_customer` cust ON `pr`.`customerID` = `cust`.`Customer_ListID` WHERE `pr`.`customerID` = '".$custID."'   and pr.merchantID='".$mID."' group by pr.noteID desc");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 
	
	public function get_subscriptions_data($condition){
	
			$this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions_fb sbs');
		$this->db->join('Xero_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where($condition);

	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
		public function get_cust_subscriptions_data($condition){
	
			$this->db->select('sbs.*, cust.fullName, cust.companyName, tmg.*');
		$this->db->from('tbl_subscriptions_xero sbs');
		$this->db->join('Xero_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where($condition);

	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	function get_billing_data($merchID) 
	{
		 $sql = ' SELECT * from tbl_merchant_billing_invoice where merchantID = "'.$merchID.'" ';
		 

		$query = $this->db->query($sql);
		
		if($query->num_rows()>0 ) {
				return $query->result_array();
			   
			} else {
				return false;
			}	

		
	}
	
		 public function get_merchant_billing_data($invID){
		   $res = array();
		   $query = $this->db->query("SELECT bl.*, mr.*, ct.city_name, st.state_name, c.country_name FROM (tbl_merchant_billing_invoice bl) INNER JOIN tbl_merchant_data mr ON mr.merchID=bl.merchantID LEFT JOIN state st ON st.state_id=mr.merchantState LEFT JOIN country c ON c.country_id=mr.merchantCountry LEFT JOIN city ct ON ct.city_id=mr.merchantCity WHERE bl.merchant_invoiceID = '$invID' AND bl.status = 'pending'");
		   
		   if($query->num_rows()>0 ) {
				$res = $query->row_array();
			   return $res;
			} else {
				return $res;
			}
		
		   
	   }
	   
	   	public function get_terms_data($merchID){
	   	  $data=array();
	    $query = $this->db->query("SELECT *, dpt.id as dfid, pt.id as pid FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID UNION SELECT *, dpt.id as dfid, pt.id as pid  FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.merchantID = $merchID ORDER BY dfid IS NULL, dfid asc");
	   if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	}
	
	public function check_payment_term($name,$netterm,$merchID)
	{
		$query = $this->db->query("SELECT * FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID WHERE dpt.name IN ('".$name."') AND pt.pt_netTerm IS NULL OR dpt.netTerm IN ('".$netterm."') AND pt.pt_netTerm IS NULL OR pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 UNION SELECT * FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 AND pt.merchantID = $merchID AND pt.enable != 1");
		
		
		return $query->num_rows();	
	}
	
	
	     public function get_invoice_transaction_data($invoiceID, $userID)
	   {
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('customer_transaction tr');
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("cust.merchantID ", $userID);
		 $this->db->where("tr.invoiceID ", $invoiceID);
		  $this->db->where("tr.merchantID ", $userID);
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	
	
	  public function get_xero_item_data($userID)
	  {
    	$result =array();
	    
		$this->db->select('qb_item.Name as Name,qb_item.productID as productID  ');
		$this->db->from('Xero_test_item qb_item');
	    $this->db->join('tbl_merchant_data comp','comp.merchID = qb_item.merchantID','INNER');
	   
	    $this->db->where('qb_item.merchantID',$userID);
	     $this->db->where('qb_item.IsActive','active');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
        
        }
       
       return $result;
      }




public function get_total_subscription($merchID){   
	  $res = array();	  
       $today = date("Y-m-d H:i");
	   $next_due_date = date('Y-m', strtotime("+30 days"));
        
        
           $this->db->select('count(*) as c_nt');
           $this->db->from('tbl_subscriptions_xero x');
           $this->db->join('Xero_custom_customer cs','x.customerID = cs.Customer_ListID','inner');
           $this->db->where('cs.merchantID',$merchID);
           $this->db->where('x.merchantDataID',$merchID);
           $this->db->where('cs.customerStatus','true');
           $this->db->where('x.subscriptionStatus','1');
            $query  = $this->db->get();
      
	 
	  if($query->num_rows() > 0){
		
		    $res['active_subcription'] = $query->row_array()['c_nt'];
		}else{
			$res['active_subcription'] =0;
		}
     
	    $query = $this->db->query("SELECT count(*) as sub_count  from (SELECT sbs.customerID  FROM 
	    (`tbl_subscriptions_xero` sbs) INNER JOIN `Xero_custom_customer` cust ON `sbs`.`customerID` = `cust`.`Customer_ListID` WHERE `cust`.`merchantID` = '".$merchID."' 
	    AND `cust`.`customerStatus` = 'true' and sbs.subscriptionStatus='1' and DATE_FORMAT(nextGeneratingDate, '%Y-%m')>= '".date('Y-m')."' group by sbs.customerID) tmp");
	
	  
	  if($query->num_rows() > 0){
		
		    $res['total_subcription'] = $query->row_array()['sub_count'];
		}else{
			$res['total_subcription'] =0;
		}
        
        
		
		  $query1 = $this->db->query("SELECT count(*) as exp_count  FROM (`tbl_subscriptions_xero` sbs)
		  
		 
		  
		   INNER JOIN `Xero_custom_customer` cust ON `sbs`.`customerID` = `cust`.`Customer_ListID` 
	 
	     WHERE `cust`.`merchantID` = '".$merchID."' AND `cust`.`customerStatus` = 'true'  
		  and ( DATE_FORMAT(nextGeneratingDate, '%Y-%m') >= '".date('Y-m')."' and  subscriptionPlan!='0' and sbs.subscriptionStatus='1' and 
		  DATE_FORMAT(endDate, '%Y-%m')='".date('Y-m',strtotime("+2 months"))."' and  DATE_FORMAT(endDate, '%Y-%m')<='".$next_due_date."' ) ");
	 
	 
	 
	   if($query1->num_rows() > 0){
		
		    $res['exp_subcription'] = $query1->row_array()['exp_count'];
		}else{
			$res['exp_subcription'] =0;
		}
		
		 $query1 = $this->db->query("SELECT count(*) as failed_count  FROM tbl_subscriptions_fail  f 
		  WHERE f.merchantID = '".$merchID."' 
		  and DATE_FORMAT(f.createdAt, '%Y-%m') = '".date('Y-m')."' ");
	 
	 
	   if($query1->num_rows() > 0){
		
		    $res['failed_count'] = $query1->row_array()['failed_count'];
		}else{
			$res['failed_count'] =0;
		}
		
		return  $res;
		
   }
		
	public function get_transaction_data_by_id($TxnID, $userID,$action){
		
		$this->db->select('tr.transactionID,sum(tr.transactionAmount)as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, cust.FullName, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID ');
		$this->db->from('customer_transaction tr');
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("cust.merchantID", $userID);
		if($action == 'transaction'){
            $this->db->where("tr.id", $TxnID);
        } else {
            $this->db->where("tr.transactionID", $TxnID);
        }
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	public function get_transaction_datarefund($userID)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from Xero_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        
        $this->db->where("cust.merchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->not_like("tr.gateway", 'ECheck');


        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();

            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber   from   Xero_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }

                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');


                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;
                        
                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }

        return $res;

    }
	public function getPartialNewAmount($transactionID)
    {
        $ref_amt = 0;
        
        
        $qr = $this->db->query('SELECT id FROM customer_transaction WHERE transactionID ="' . $transactionID . '" ORDER BY `id` DESC');
        $data = $qr->result_array();
        
        if (!empty($data)) {
            foreach ($data as $value) {
                $qr1 = $this->db->query('SELECT sum(transactionAmount) as transactionAmount FROM customer_transaction WHERE parent_id ="' . $value['id'] . '" ORDER BY `id` DESC');
                $data1 = $qr1->row_array();
                
                if (!empty($data1)) {
                    $ref_amt = $ref_amt + $data1['transactionAmount'];
                }

            }
            
            return $ref_amt;
            
        } else {
            return $ref_amt;
        }
    }
}
