<?php

class Xero_company_model extends CI_Model
{

    public function get_invoice_data_by_due($condion)
    {

        $res = array();

        $this->db->select("`inv`.CustomerListID,`inv`.invoiceID, `inv`.CustomerFullName, cust.firstName, cust.lastName, `cust`.userEmail,`cust`.fullName,`cust`.companyName, (case when cust.Customer_ListID !='' then 'Active' else 'InActive' end) as status, `cust`.Customer_ListID, sum(inv.BalanceRemaining) as balance ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->where($condion);

        $this->db->group_by('inv.CustomerListID');

        $this->db->order_by('balance', 'desc');
        $this->db->limit(10);

        $query = $this->db->get();
        $res = $query->result_array();
        return $res;

    }

    public function get_transaction_failure_report_data($userID)
    {

        $res = array();
        $sql = "SELECT `tr`.`id`,  `tr`.`invoiceID`, `tr`.`transactionID`, `tr`.`transactionStatus`, `tr`.`transactionCode`,  `tr`.`customerListID`,  `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`,(select RefNumber from Xero_test_invoice where invoiceID = tr.invoiceID ) as RefNumber, `cust`.`fullName` FROM (`customer_transaction` tr) INNER JOIN `Xero_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID` WHERE `cust`.`merchantID` = '$userID' AND transactionCode NOT IN ('200','1','100','111') ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_data_by_past_due($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate,  `inv`.invoiceID, `inv`.BalanceRemaining,
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' and UserStatus!='VOIDED' then 'Scheduled'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00'  and UserStatus!='VOIDED'   and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00'  and UserStatus!='VOIDED'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'  and UserStatus!='VOIDED'  then 'Success'
	  else 'Canceled' end ) as status
	  FROM Xero_test_invoice inv
	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `BalanceRemaining` != '0.00' and UserStatus!='VOIDED'  and

	  `inv`.`merchantID` = '$userID'  order by  BalanceRemaining   desc limit 10 ");

        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_data_by_past_time_due($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT `inv`.RefNumber, `inv`.invoiceID, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' and userStatus ='' then 'Scheduled'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'   then 'Success'
	  else 'Canceled' end ) as status
	  FROM Xero_test_invoice inv
	  	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and BalanceRemaining != '0.00' and
	  `inv`.`merchantID` = '$userID'  order by  DueDate   asc limit 10 ");

        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_transaction_report_data($userID, $minvalue, $maxvalue)
    {
        $res    = array();
        $trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')";

        $this->db->select('tr.*, cust.*, (select RefNumber from Xero_test_invoice where invoiceID= tr.invoiceID and CustomerListID = cust.customer_ListID and  merchantID="' . $userID . '" ) as RefNumber');
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.customer_ListID', 'INNER');
        $this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
        $this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
        $this->db->where('cust.merchantID', $userID);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_data_open_due($userID, $status)
    {

        $res = array();
        $con = '';
        if ($status == '8') {
            $con .= "and `inv`.BalanceRemaining !='0.00' and `inv`.`IsPaid` = 'false' ";

        }

        $today = date('Y-m-d');
        $query = $this->db->query("SELECT  `inv`.RefNumber,`inv`.invoiceID,   (`inv`.Total_payment - `inv`.BalanceRemaining)  as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, `inv`.TimeCreated,
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv ,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,inv.TimeCreated as addOnDate,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' then 'Scheduled'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'   then 'Success'
	  else 'Canceled' end ) as status

	  FROM Xero_test_invoice inv

	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE

	  `inv`.`merchantID` = '$userID'  $con order by  DueDate   asc");

        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_invoice_upcomming_data($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT  inv.RefNumber,  inv.invoiceID, `inv`.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,`inv`.TimeCreated,
	   DATEDIFF('$today', DATE_FORMAT(inv.DueDate, '%Y-%m-%d H:i')) as tr_Day,
	  cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'

	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0  then 'Success'
	  else 'Canceled' end ) as status
	  FROM Xero_test_invoice inv
	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 and
	  inv.merchantID = '$userID' and cust.merchantID = '$userID'   order by   DueDate  asc limit 10 ");

        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_company_invoice_data_payment($mechID)
    {
        $res   = array();
        $query = $this->db->query('select (select sum(transactionAmount) from customer_transaction tr WHERE  tr.merchantID = "' . $mechID . '" AND (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or tr.transactionType ="pay_sale" or tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or tr.transactionType ="capture" or tr.transactionType ="auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") AND (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and tr.transactionID!="" and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as total_amount,(select sum(tr.transactionAmount) from customer_transaction tr WHERE tr.merchantID = "' . $mechID . '" AND (tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "credit" ) and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and tr.transaction_user_status !="refund" and tr.transactionID!="" and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as refund_amount');
        if ($query->num_rows()) {

            return $res = $query->result_array();

        }
        return false;
    }

    public function get_process_trans_count($user_id)
    {

        $today = date("Y-m-d");
        $this->db->select('*');
        $this->db->from('customer_transaction tr');
        $this->db->where("tr.merchantID ", $user_id);
        $this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
        $this->db->order_by("tr.transactionDate", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return $query->num_rows();
        }
    }

    public function get_invoice_data_count($condition)
    {

        $num = 0;

        $this->db->select('inv.* ');
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->where($condition);
        $this->db->where("BalanceRemaining != 0.00");

        $query = $this->db->get();
        $num = $query->num_rows();
        return $num;

    }

    public function get_invoice_data_count_failed($user_id)
    {
        $today = date('Y-m-d');
        $num   = 0;

        $sql = "SELECT `inv`.* FROM (`Xero_test_invoice` inv) INNER JOIN `customer_transaction` tr ON `tr`.`invoiceID` = `inv`.`invoiceID` AND tr.transactionCode = '300' WHERE  `inv`.`merchantID` = '$user_id' ";

        $query = $this->db->query($sql);
        $num   = $query->num_rows();
        return $num;

    }

    public function get_paid_invoice_recent($condion)
    {

        $res   = array();
        $today = date('Y-m-d');

        $this->db->select("cust.FullName, inv.CustomerListID, cust.companyName, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID, inv.refNumber");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('customer_transaction trans', 'trans.invoiceID = inv.invoiceID', 'INNER');
        $this->db->order_by('trans.transactionDate', 'desc');
        $this->db->limit(10);
        $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
        $this->db->where($condion);

        $query = $this->db->get();
        $res = $query->result_array();
        return $res;

    }

    public function get_invoice_due_by_company($condion, $status)
    {

        $res   = array();
        $today = date('Y-m-d');
        $this->db->select("cust.fullName as label, inv.DueDate,  sum(inv.BalanceRemaining) as balance ");
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->group_by('cust.Customer_ListID');
        $this->db->order_by('balance', 'desc');
        $this->db->limit(10);
        if ($status == '1') {
            $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '" . $today . "'  AND `BalanceRemaining` != '0.00' ");
        } else {
            $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d')  AND `BalanceRemaining` != '0.00'  ");
        }
        $this->db->where($condion);

        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_oldest_due($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT cust.FullName, `inv`.invoiceID, `inv`.refNumber, (`inv`.Total_payment) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, inv.CustomerListID,DATEDIFF('$today', inv.DueDate) as aging_days, cust.Customer_ListID,cust.FullName,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' then 'Upcoming'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND BalanceRemaining !='0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and BalanceRemaining ='0' then 'Success'
	  else 'Canceled' end ) as status
	  FROM Xero_test_invoice inv
	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE  BalanceRemaining !='0.00' and

	  `inv`.`merchantID` = '$userID'  and  cust.merchantID='$userID' limit 10 ");

        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;

    }

    public function chart_general_volume($mID)
    {

        $res    = array();
        $months = array();
        for ($i = 11; $i >= 0; $i--) {
            $months[] = date("M-Y", strtotime(date('Y-m') . " -$i months"));
        }

        foreach ($months as $key => $month) {

            $sql = "SELECT  sum(tr.transactionAmount) as volume FROM customer_transaction tr WHERE merchantID = '" . $mID . "' AND  date_format(tr.transactionDate, '%b-%Y') ='" . $month . "' and   (tr.transactionType ='sale' or tr.transactionType ='Offline Payment' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture' or tr.transactionType ='pay_capture' or  tr.transactionType ='capture'
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1' or tr.transactionType ='Offline Payment' or tr.transactionCode='200')       GROUP BY date_format(transactionDate, '%b-%Y')";

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $res[]              = $query->row_array();
                $res[$key]['Month'] = $month;
            } else {

                $res[$key]['volume'] = 0.00;
                $res[$key]['Month']  = $month;
            }
        }
        return $res;

    }

    public function get_xero_tax_data($uID)
    {
        $res = array();

        $this->db->select('tax.*');
        $this->db->from('tbl_taxes_xero tax');
        $this->db->where('tax.merchantID', $uID);

        $query = $this->db->get();

        $res = $query->result_array();
        return $res;
    }

    public function get_recent_transaction_data($userID, $customerID = null)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '120', '111', '1');

        $type = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY-SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND', 'CREDIT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as balance, tr.invoiceTxnID as refNumber, tr.invoiceTxnID,tr.id as TxnID, tr.transactionDate,  cust.Customer_ListID as CustomerListID, cust.Customer_ListID,  cust.fullName as FullName');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) SEPARATOR "," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        if ($customerID) {
            $this->db->where("tr.customerListID", $customerID);
        }
        $this->db->group_by("tr.transactionID", 'desc');
        $this->db->order_by("tr.id", 'desc');
        $this->db->limit(10);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res1 = $query->result_array();
            foreach ($res1 as $result) {
                if (!empty($result['invoice_id'])) {
                    $result['invoice_no'] = '';
                    $inv                  = $result['invoice_id'];
                    $invList              = explode(',', $inv);

                    foreach ($invList as $key1 => $value) {
                        $value = trim($value);
                        if (empty($value) || $value == '') {
                            unset($invList[$key1]);
                        }
                        $invList[$key1] = "'" . $value . "'";
                    }

                    $inv     = implode(',', $invList);
                    $res_inv = array();

                    $qq = $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from
                    Xero_test_invoice where invoiceID IN($inv) and  merchantID='$userID'  GROUP BY merchantID ");

                    if ($qq->num_rows > 0) {
                        $res_inv              = $qq->row_array()['invoce'];
                        $result['invoice_no'] = $res_inv;
                    }
                }

                $res[] = $result;
            }
        }

        return $res;

    }

    public function get_recent_volume_dashboard($mID, $month)
    {

        $res    = 0.00;
        $earn   = 0.00;
        $refund = 0.00;

        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM
		   customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID WHERE tr.merchantID = "' . $mID . '"  and mr1.merchID = "' . $mID . '" and
		   date_format(tr.transactionDate, "%b-%Y") ="' . $month . '"  and    (tr.transactionType ="Sale" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture"
			or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or
		 tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")
		 and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")    GROUP BY date_format(transactionDate, "%b-%Y") ';

        $query = $this->db->query($sql);

        $res = $earn - $refund;
        return $res;
    }
    public function get_creditcard_payment($mID, $month)
    {

        $res    = 0.00;
        $earn   = 0.00;
        $refund = 0.00;

        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM
			customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
			WHERE tr.merchantID = "' . $mID . '" and mr1.merchID = "' . $mID . '"  and
			date_format(tr.transactionDate, "%b-%Y") ="' . $month . '" and  tr.gateway NOT LIKE "%echeck%" and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
			  or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or
			  tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")
			  and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2" )  GROUP BY date_format(transactionDate, "%b-%Y") ';

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row  = $query->row_array();
            $earn = $row['volume'];

        }

        $res = $earn - $refund;
        return $res;
    }
    public function get_offline_payment($mID, $month)
    {
        $res = 0.00;
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM
	 		customer_transaction tr
		 inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		 WHERE tr.merchantID = "' . $mID . '" and mr1.merchID = "' . $mID . '"  and
		 date_format(tr.transactionDate, "%b-%Y") ="' . $month . '"  and (tr.transactionType ="Offline Payment") and  (tr.transactionCode ="100" or
		   tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")
		   and ( tr.transaction_user_status !="3" )    GROUP BY date_format(transactionDate, "%b-%Y") ';

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row  = $query->row_array();
            $earn = $row['volume'];

            $res = $earn;

        } else {

            $res = 0.00;

        }
        return $res;
    }
    public function get_outstanding_payment($userID)
    {

        $res   = array();
        $today = date('Y-m-d');
        $month = date("M-Y");
        $query = $this->db->query("SELECT  Month(inv.DueDate),sum(`inv`.BalanceRemaining) as balance, sum(`inv`.AppliedAmount) as payment FROM Xero_test_invoice inv
	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE cust.merchantID=" . $userID . " and inv.IsPaid = 'false' and userStatus != 'cancel' and  cust.customerStatus='1'   order by DueDate asc");

        if ($query->num_rows() > 0) {
            $row  = $query->row_array();
            $pay  = str_replace('-', '', $row['payment']);
            $earn = $row['balance'] - (float) $pay;
            $res  = $earn;
        } else {
            $res = '0.00';
        }
        return $res;
    }
    public function get_paid_recent($userID)
    {

        $res = array();

        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '120', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('(tr.transactionAmount) as balance,tr.invoiceTxnID,tr.id as TxnID, tr.transactionDate,  cust.Customer_ListID as CustomerListID, cust.FullName, cust.companyName,(select RefNumber from Xero_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber');

        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');

        $this->db->where("tr.merchantID ", $userID);

        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->order_by("tr.id", 'desc');
        $this->db->group_by("tr.transactionID");
        $this->db->limit(10);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $res = $query->result_array();

        }

        return $res;

    }
    public function chart_all_volume_new($mID)
    {
        $res    = array();
        $months = array();
        $resObj = array();
        for ($i = 12; $i >= 1; $i--) {
            $months[] = date("M-Y", strtotime(date('Y-m') . " -$i months"));
        }
        $chart_arr    = array();
        $totalRevenue = 0;
        $totalRevenue = $totalCCA = $totalECLA = 0;
        foreach ($months as $key => $month) {

            $earn                       = $this->get_recent_volume_dashboard($mID, $month);
            $res[$key]['revenu_volume'] = $earn;
            $res[$key]['revenu_Month']  = $month;

            $earn2                      = $this->get_creditcard_payment($mID, $month);
            $res[$key]['online_volume'] = $earn2;
            $res[$key]['online_Month']  = $month;

            $earn4                      = $this->get_eCheck_payment($mID, $month);
            $res[$key]['eCheck_volume'] = $earn4;
            $res[$key]['eCheck_Month']  = $month;

            $totalRevenue = $totalRevenue + $earn;
            $totalCCA     = $totalCCA + $earn2;
            $totalECLA    = $totalECLA + $earn4;
        }
        $resObj['data']         = $res;
        $resObj['totalRevenue'] = $totalRevenue;
        $resObj['totalCCA']     = $totalCCA;
        $resObj['totalECLA']    = $totalECLA;
        return $resObj;
    }

    public function get_echeck_payment($mID, $month)
    {

        $res     = 0.00;
        $earn    = 0.00;
        $refund  = 0.00;
        $refund1 = 0.00;
        $sql     = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM
			customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
			WHERE tr.merchantID = "' . $mID . '" and mr1.merchID = "' . $mID . '"  and
			date_format(tr.transactionDate, "%b-%Y") ="' . $month . '"  and  tr.gateway LIKE "%echeck%"  and (tr.transactionType ="Sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
			  or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or
			  tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")
			  and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%b-%Y") ';

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row  = $query->row_array();
            $earn = $row['volume'];

        }

        if ($earn >= $refund) {
            $earn = $earn - $refund;
        }

        $res = $earn;
        return $res;

    }
}
