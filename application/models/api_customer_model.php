<?php
class Api_customer_model extends CI_Model
{
	
	public function __construct()
	{
  
        
		parent::__construct();
		$this->load->database();
        $this->db1 =$this->load->database('otherdb',true);
		
	}
	
  
	  	/*****************QBO Customer*******************/
    public function get_qbo_customer_details( $mid, $st='', $page=0, $limit='')
	{
        $res=array();
         $this->db->select('cus.*,    (select sum(BalanceRemaining) 
		 from 	QBO_test_invoice where CustomerListID = cus.Customer_ListID and merchantID="'.$mid.'") as Balance ');
		$this->db->from('QBO_custom_customer cus ');
        $this->db->where('cus.merchantID', $mid);
		if($st==1 || $st=='' )
		$this->db->where('cus.customerStatus','true');
		else
		$this->db->where('cus.customerStatus','false');
		$start=0;
		
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
		
		
      
		$query = $this->db->get();
		if($query -> num_rows() > 0)
	      $res['customers']= $query->result_array(); 
          $res['total_records'] = $this->get_qbo_total_customer($mid);
		return 	$res; 
    }
	
	 public function get_qbo_total_customer( $mid)
	 {
		 
		$num=0;
         $this->db->select('cus.* ');
		$this->db->from('QBO_custom_customer cus ');
        $this->db->where('cus.merchantID',$mid);
		
      
		$query = $this->db->get();
		if($query -> num_rows() > 0)

	      $num= $query ->num_rows(); 

		return 	$num; 
    }
	
	
	    public function qbo_customer_details($custID, $mid)
		{
        $res=array();
         $this->db->select('cus.*');
		
		$this->db->from('QBO_custom_customer cus');
		$this->db->where('cus.Customer_ListID', $custID);
         $this->db->where('cus.merchantID',$mid);
		$query = $this->db->get();

		if($query -> num_rows() > 0)
		$res = $query->row_array(); 
		return $res; 
    }
   
	/*****************END*******************/
  
	/*****************QBD Customer*******************/
    public function get_qbd_customer_details( $mid, $st='', $page=0, $limit='')
	{
        $res=array();
         $this->db->select('cus.*,    (select sum(BalanceRemaining) 
		 from 	qb_test_invoice where Customer_ListID = cus.ListID) as Balance ');
		$this->db->from('qb_test_customer cus ');
        $this->db->where('cus.qbmerchantID',$mid);
		if($st==1 || $st=='' )
		$this->db->where('cus.customerStatus','1');
		else
		$this->db->where('cus.customerStatus','0');
		$start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
		
		
      
		$query = $this->db->get();
		if($query -> num_rows() > 0)
	      $res['customers']= $query->result_array(); 
          $res['total_records'] = $this->get_qbd_total_customer($mid);
		return 	$res; 
    }
	
	 public function get_qbd_total_customer( $mid)
	 {
		 
		$num=0;
         $this->db->select('cus.* ');
		$this->db->from('qb_test_customer cus ');
        $this->db->where('cus.qbmerchantID',$mid);
		
      
		$query = $this->db->get();
		if($query -> num_rows() > 0)

	      $num= $query ->num_rows(); 

		return 	$num; 
    }
	
	
	    public function qbd_customer_details($custID, $mid)
		{
        $res=array();
         $this->db->select('cus.*');
		
		$this->db->from('qb_test_customer cus');
		$this->db->where('cus.ListID', $custID);
         $this->db->where('cus.qbmerchantID',$mid);
		$query = $this->db->get();

		if($query -> num_rows() > 0)
		$res = $query->row_array(); 
		return $res; 
       }
    
	 /*******END**********/
	 
	   public function get_company_customer_details( $mid, $st='', $page=0, $limit='')
	{
        $res=array();
         $this->db->select('cus.*,    (select sum(BalanceRemaining) 
		 from 	chargezoom_test_invoice where Customer_ListID = cus.ListID) as Balance ');
		$this->db->from('chargezoom_test_customer cus ');
        $this->db->where('cus.qbmerchantID',$mid);
		if($st==1 || $st=='' )
		$this->db->where('cus.customerStatus','1');
		else
		$this->db->where('cus.customerStatus','0');
		$start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
		
		
      
		$query = $this->db->get();
		if($query -> num_rows() > 0)
	      $res['customers']= $query->result_array(); 
          $res['total_records'] = $this->get_qbd_total_customer($mid);
		return 	$res; 
    }
	
	 public function get_company_total_customer( $mid)
	 {
		 
		$num=0;
         $this->db->select('cus.* ');
		$this->db->from('chargezoom_test_customer cus ');
        $this->db->where('cus.qbmerchantID',$mid);
		
      
		$query = $this->db->get();
		if($query -> num_rows() > 0)

	      $num= $query ->num_rows(); 

		return 	$num; 
    }
	
	
	    public function company_customer_details($custID, $mid)
		{
        $res=array();
         $this->db->select('cus.*');
		
		$this->db->from('chargezoom_test_customer cus');
		$this->db->where('cus.ListID', $custID);
         $this->db->where('cus.qbmerchantID',$mid);
		$query = $this->db->get();

		if($query -> num_rows() > 0)
		$res = $query->row_array(); 
		return $res; 
       }
    
	 /***************END*******************/
	 
     public function create_customer($input_data, $type)
	 {
		 
		 if($type==2)
		 {	 
		 $cusom_data=array( 'ListID'=>$input_data['customListID'],'FirstName'=>$input_data['firstName'],'LastName'=>$input_data['lastName'],'FullName'=>$input_data['fullName'],
							'Contact'=>$input_data['userEmail'],'companyName'=>$input_data['companyName'], 'companyID'=>$input_data['companyID'],
							
							'ShipAddress_Addr1'=>$input_data['ship_address1'],'ShipAddress_Addr2'=>$input_data['ship_address2'],'ShipAddress_City'=>$input_data['ship_city'],'ShipAddress_State'=>$input_data['ship_state'],
							'ShipAddress_PostalCode'=>$input_data['ship_zipcode'],'ShipAddress_Country'=>$input_data['ship_country'],
							 'BillingAddress_Addr1'=>$input_data['address1'],'BillingAddress_Addr2'=>$input_data['address2'],'BillingAddress_State'=>$input_data['state'],
							'BillingAddress_City'=>$input_data['city'],'BillingAddress_Country'=>$input_data['country'], 'BillingAddress_PostalCode'=>$input_data['zipcode'],
							'qbmerchantID'=>$input_data['qbmerchantID'],'qb_status'=>0,'customerStatus'=>1,'IsActive'=>'true','qb_status'=>0,'qbAction'=>$input_data['qb_action'],
							'Phone'=>$input_data['phoneNumber'],'TimeCreated'=>date('Y-m-d H:i:s'));
							
		$this->db->insert('qb_test_customer',$cusom_data);	

		 }
		 
		  if($type==1)
		 {	 
		 $cusom_data=array( 'Customer_ListID'=>$input_data['customListID'],'FirstName'=>$input_data['firstName'],'LastName'=>$input_data['lastName'],'fullName'=>$input_data['fullName'],
							'userEmail'=>$input_data['userEmail'],'companyName'=>$input_data['companyName'], 'companyID'=>$input_data['companyID'],
							'ship_address1'=>$input_data['ship_address1'],'ship_address2'=>$input_data['ship_address2'],'ship_city'=>$input_data['ship_city'],'ship_state'=>$input_data['ship_state'],
							'ship_zipcode'=>$input_data['ship_zipcode'],'ship_country'=>$input_data['ship_country'],
							 'address1'=>$input_data['address1'],'address2'=>$input_data['address2'],'State'=>$input_data['state'],
							'City'=>$input_data['city'],'Country'=>$input_data['country'], 'zipCode'=>$input_data['zipcode'],
							'merchantID'=>$input_data['merchantID'],'listID'=>$input_data['customListID'],'customerStatus'=>'true',
							'phoneNumber'=>$input_data['phoneNumber'],'updatedAt'=>date('Y-m-d H:i:s'), 'createdAt'=>date('Y-m-d H:i:s'));
							
		$this->db->insert('QBO_custom_customer',$cusom_data);	

		 }
		 
		 
		 
		 if($type==5)
		 {	 
		 $cusom_data=array( 'ListID'=>$input_data['customListID'],'FirstName'=>$input_data['firstName'],'LastName'=>$input_data['lastName'],'FullName'=>$input_data['fullName'],
							'Contact'=>$input_data['userEmail'],'companyName'=>$input_data['companyName'], 'companyID'=>$input_data['companyID'],
							
							'ShipAddress_Addr1'=>$input_data['ship_address1'],'ShipAddress_Addr2'=>$input_data['ship_address2'],'ShipAddress_City'=>$input_data['ship_city'],'ShipAddress_State'=>$input_data['ship_state'],
							'ShipAddress_PostalCode'=>$input_data['ship_zipcode'],'ShipAddress_Country'=>$input_data['ship_country'],
							 'BillingAddress_Addr1'=>$input_data['address1'],'BillingAddress_Addr2'=>$input_data['address2'],'BillingAddress_State'=>$input_data['state'],
							'BillingAddress_City'=>$input_data['city'],'BillingAddress_Country'=>$input_data['country'], 'BillingAddress_PostalCode'=>$input_data['zipcode'],
							'qbmerchantID'=>$input_data['qbmerchantID'],'customerStatus'=>1,'IsActive'=>'true','qb_status'=>0,
							'Phone'=>$input_data['phoneNumber'],'TimeCreated'=>date('Y-m-d H:i:s'));
							
		$this->db->insert('chargezezoom_test_customer',$cusom_data);	

		 }
		

		 
	 }
	 
	  public function update_customer($input_data, $type)
	 {
		 
		 if($type==2)
		 {	 
		 $cusom_data=array( 'ListID'=>$input_data['customListID'],'FirstName'=>$input_data['firstName'],'LastName'=>$input_data['lastName'],'FullName'=>$input_data['fullName'],
							'Contact'=>$input_data['userEmail'],'companyName'=>$input_data['companyName'], 'companyID'=>$input_data['companyID'],
							
							'ShipAddress_Addr1'=>$input_data['ship_address1'],'ShipAddress_Addr2'=>$input_data['ship_address2'],'ShipAddress_City'=>$input_data['ship_city'],'ShipAddress_State'=>$input_data['ship_state'],
							'ShipAddress_PostalCode'=>$input_data['ship_zipcode'],'ShipAddress_Country'=>$input_data['ship_country'],
							 'BillingAddress_Addr1'=>$input_data['address1'],'BillingAddress_Addr2'=>$input_data['address2'],'BillingAddress_State'=>$input_data['state'],
							'BillingAddress_City'=>$input_data['city'],'BillingAddress_Country'=>$input_data['country'], 'BillingAddress_PostalCode'=>$input_data['zipcode'],
							'qbmerchantID'=>$input_data['qbmerchantID'],'qb_status'=>0,'customerStatus'=>1,'qbAction'=>$input_data['qb_action'],
							'Phone'=>$input_data['phoneNumber'],'TimeModified'=>date('Y-m-d H:i:s'));
							
			$this->db->where('ListID',$input_data['customListID']);	
			$this->db->where('qbmerchantID',$input_data['qbmerchantID']);				
			$this->db->update('qb_test_customer',$cusom_data);	

		 }
		 
		  if($type==1)
		 {	 
		 $cusom_data=array( 'Customer_ListID'=>$input_data['customListID'],'FirstName'=>$input_data['firstName'],'LastName'=>$input_data['lastName'],'fullName'=>$input_data['fullName'],
							'userEmail'=>$input_data['userEmail'],'companyName'=>$input_data['companyName'], 'companyID'=>$input_data['companyID'],
							'ship_address1'=>$input_data['ship_address1'],'ship_address2'=>$input_data['ship_address2'],'ship_city'=>$input_data['ship_city'],'ship_state'=>$input_data['ship_state'],
							'ship_zipcode'=>$input_data['ship_zipcode'],'ship_country'=>$input_data['ship_country'],
							 'address1'=>$input_data['address1'],'address2'=>$input_data['address2'],'State'=>$input_data['state'],
							'City'=>$input_data['city'],'Country'=>$input_data['country'], 'zipCode'=>$input_data['zipcode'],
							'merchantID'=>$input_data['merchantID'],'listID'=>$input_data['customListID'],'customerStatus'=>'true',
							'phoneNumber'=>$input_data['phoneNumber'],'updatedAt'=>date('Y-m-d H:i:s'));
		$this->db->where('Customer_ListID',$input_data['customListID']);	
		$this->db->where('merchantID',$input_data['qbmerchantID']);				
		$this->db->update('QBO_custom_customer',$cusom_data);	

		 }
		 
		 
		  if($type==5)
		 {	 
		 $cusom_data=array( 'ListID'=>$input_data['customListID'],'FirstName'=>$input_data['firstName'],'LastName'=>$input_data['lastName'],'FullName'=>$input_data['fullName'],
							'Contact'=>$input_data['userEmail'],'companyName'=>$input_data['companyName'], 'companyID'=>$input_data['companyID'],
							
							'ShipAddress_Addr1'=>$input_data['ship_address1'],'ShipAddress_Addr2'=>$input_data['ship_address2'],'ShipAddress_City'=>$input_data['ship_city'],'ShipAddress_State'=>$input_data['ship_state'],
							'ShipAddress_PostalCode'=>$input_data['ship_zipcode'],'ShipAddress_Country'=>$input_data['ship_country'],
							 'BillingAddress_Addr1'=>$input_data['address1'],'BillingAddress_Addr2'=>$input_data['address2'],'BillingAddress_State'=>$input_data['state'],
							'BillingAddress_City'=>$input_data['city'],'BillingAddress_Country'=>$input_data['country'], 'BillingAddress_PostalCode'=>$input_data['zipcode'],
							'qbmerchantID'=>$input_data['qbmerchantID'],'customerStatus'=>1,
							'Phone'=>$input_data['phoneNumber'],'TimeModified'=>date('Y-m-d H:i:s'));
							
			$this->db->where('ListID',$input_data['customListID']);	
			$this->db->where('qbmerchantID',$input_data['qbmerchantID']);				
			$this->db->update('chargezoom_test_customer',$cusom_data);	

		 }

		 
	 }
	 
 
public function qbo_customer_card($csID, $mID)
    { 
        $card_array=array();
        $this->load->library('encrypt');
      
         $this->db1->select('*');
         $this->db1->from('customer_card_data');
         $this->db1->where('customerListID', $csID);
      	 $this->db1->where('merchantID', $mID);
         $query = $this->db1->get();
         if($query->num_rows() >0)
         {
            $cards =  $query->result_array();
           foreach($cards as $card)
           {
                   $card_data['CardID'] =$card['CardID'];
             $card_data['CustomerCard'] =substr($this->encrypt->decode($card['CustomerCard']),12); 
             $card_data['cardMonth'] =$card['cardMonth'];
             $card_data['cardYear'] =$card['cardYear'];
             $card_data['CardType'] =$card['CardType'];
             $card_data['CardCVV'] =substr($this->encrypt->decode($card['CardCVV']),12) ;
             $card_data['customerCardfriendlyName'] =$card['customerCardfriendlyName'];
             
             $card_array[] =$card_data;
           }
         }
      
         return $card_array;
      
    }
  
  
  
  
  
  	public function get_qbd_transaction_data_captue($userID, $page=0, $limit='' )
	{
		
		$today = date("Y-m-d H:i");
		 $res=array();
		 
		$type = array('AUTH', 'PAYPAL_AUTH','PAY_AUTH','AUTH_ONLY','STRIPE_AUTH');
		$tcode = array('100','200','100','111','1');
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,  cust.FullName');
	
	   $this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','cust.ListID=tr.customerListID','inner');
		$this->db->where('cust.merchantID', $userID);
		
		$this->db->where('tr.merchantID', $userID);
		$this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
		 $this->db->group_by("tr.transactionID");
		$start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
	   
		$query = $this->db->get();  
  
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
  
  	public function get_qbo_transaction_data_captue($userID,  $page=0, $limit='' )
	{
		
		$today = date("Y-m-d H:i");
		 $res=array();
		 


		$type = array('AUTH', 'PAYPAL_AUTH','PAY_AUTH','AUTH_ONLY','STRIPE_AUTH');
		$tcode = array('100','200','100','111','1');
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,  cust.fullName');
	
	   $this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','cust.Customer_ListID=tr.customerListID','inner');
		$this->db->where('cust.merchantID', $userID);
		
		$this->db->where('tr.merchantID', $userID);
		$this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
		  $this->db->group_by("tr.transactionID");
		$start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
	  
		$query = $this->db->get();  
  
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
  
  
  
  
  
  	public function get_qbo_transaction_data_refund($userID ,  $page=0, $limit='' )
    {
		$today = date("Y-m-d H:i");
		 $res=array();
		 
		

		$type = array('SALE', 'PAYPAL_SALE','PAYPAL_CAPTURE','PAY_SALE','PRIOR_AUTH_CAPTURE','PAY_CAPTURE','CAPTURE','AUTH_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE');
		$tcode = array('100','200','100','111','1');
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,  cust.FullName');
	
	   $this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','cust.Customer_ListID=tr.customerListID','inner');
		$this->db->where('cust.merchantID', $userID);
		
		$this->db->where('tr.merchantID', $userID);
		$this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
		 $this->db->group_by("tr.transactionID");
		$start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
	   
		
  
		$query = $this->db->get();  
		if($query->num_rows() > 0){
		
		 $res= $query->result_array();
		}
		return $res;
	}
      public function get_qbd_transaction_data_refund($userID,$page=0,$limit='')
	  {
		$today = date("Y-m-d H:i");
		$res  =array();
		$type = array('SALE', 'PAYPAL_SALE','PAYPAL_CAPTURE','PAY_SALE','PRIOR_AUTH_CAPTURE','PAY_CAPTURE','CAPTURE','AUTH_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE');
		$tcode = array('100','200','100','111','1');
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,  cust.FullName');
	
	   $this->db->select('DATEDIFF("'.$today.'", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', FALSE);
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','cust.ListID=tr.customerListID','inner');
		$this->db->where('tr.merchantID', $userID);
		$this->db->where_in('UPPER(tr.transactionType)',$type);
        $this->db->where_in('(tr.transactionCode)',$tcode);
	    $this->db->order_by("tr.transactionDate", 'desc');
		$this->db->group_by("tr.transactionID");
		$start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
	    
		
  
		$query = $this->db->get();  
		if($query->num_rows() > 0){
		
		$res = $query->result_array();
		}
		
		return $res;
	} 	
     public function get_qbo_transaction_data($userID)
	{
				$today = date("Y-m-d H:i");
		$this->db->select('tr.*, cust.*');
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
	    $this->db->order_by("tr.transactionDate", 'desc');
		 $this->db->group_by("tr.transactionID", 'desc');
		         $start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
	  
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
		public function get_qbo_refund_transaction_data($userID)
		{
	
		$query =	$this->db->query ("SELECT `tr`.*, `cust`.* FROM (`customer_transaction` tr) 
	INNER JOIN `QBO_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID`
	WHERE `tr`.`merchantID` = '$userID' and cust.merchantID= '$userID'   AND (`tr`.`transactionType` = 'refund' or `tr`.`transactionType` = 'stripe_refund'  OR `tr`.`transactionType` = 'pay_refund' OR `tr`.`transactionType` = 'credit' ) ORDER BY `tr`.`transactionDate` desc") ;
	
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
  
     function update_refund_payment($tr_id, $gateway)
	{
	    
	    if($gateway=='NMI')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where (  transactionID="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
	     if($gateway=='AUTH')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
	    }
	    
	      if($gateway=='PAYTRACE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where (transactionID="'.$tr_id.'"  and (transactionType="pay_sale" or transactionType="pay_capture") )'; 
	        
	    }
	    
	       if($gateway=='PAYPAL')
	    {
	         $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )'; 
	        
	    }
	    
	        if($gateway=='STRIPE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
		}
		if ($gateway == iTransactGatewayName) {
            $sql = ' UPDATE customer_transaction set transaction_user_status="2" where ( transactionID="' . $tr_id . '"  and (transactionType="sale" or transactionType="capture") ) ';
        }
	    
	  
	   if($this->db->query($sql))
	   return true;
	   else
	   return false;
	}
	
	
	


	
	 	public function get_qbo_customer_note_data($custID, $mrID)
		{

	$res =array();
 	
	  $this->db->select('pr.*,cust.fullName ');
	  $this->db->from('tbl_private_note pr');
	  $this->db->join('QBO_custom_customer cust', 'cust.Customer_ListID=pr.customerID','inner');
	  $this->db->where('cust.merchantID',$mrID);
	  $this->db->where('cust.Customer_ListID',$custID);
	  $this->db->where('pr.merchantID',$mrID);
	  $this->db->where('pr.customerID',$custID);
	  $this->db->where('cust.customerStatus','true');
	  $this->db->order_by('pr.noteID','desc'); 
	  $query = $this->db->get();
	  

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
		public function get_qbd_customer_note_data($custID, $mrID)
		{

	    $res =array();
 	
		  $this->db->select('pr.*,cust.FullName ');
		  $this->db->from('tbl_private_note pr');
		  $this->db->join('qb_test_customer cust', 'cust.ListID=pr.customerID','inner');
		  $this->db->where('cust.merchantID',$mrID);
		  $this->db->where('pr.merchantID',$mrID);
		  $this->db->where('pr.customerID',$custID);
		  $this->db->where('cust.customerStatus','1');
		  $this->db->order_by('pr.noteID','desc'); 
		  $query = $this->db->get();
	  

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	



	 public function get_qbd_subscriptions_data($condition, $page=0, $limit='' )
	  {
			$res=array();
			$this->db->select('sbs.*, cust.FullName, cust.companyName, pl.planName, tmg.*');
			$this->db->from('tbl_subscriptions sbs');
			$this->db->join('qb_test_customer cust','sbs.customerID = cust.ListID','INNER');
			$this->db->join('tbl_subscriptions_plan_qb pl','pl.planID = sbs.subscriptionPlan','left');
			$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
			$this->db->where($condition);		
			$this->db->where('cust.customerStatus', '1');
			$this->db->order_by("sbs.createdAt", 'desc');
			  $start=0;
				if($page > 0)
				{
				$start = $page*$limit;
				 $this->db->limit($limit,$start);	
				}else{
					if($limit > 0)
				$this->db->limit($limit);
			     else
				$this->db->limit(10);
				}
			
			$query = $this->db->get();
			if($query->num_rows() > 0){
			
			  $res= $query->result_array();
			  
			 foreach($res as $k=>$result)
			 {
				 $qr = $this->db->query("SELECT * from tbl_subscription_invoice_item where  subscriptionID ='".$result['subscriptionID']."' ");
				 $items = array();
				 if($qr->num_rows() > 0)
				 {
					$items = $qr->result_array();
				 }
				 $res[$k]['items'] = $items;
			 }
			}
			return $res;
		} 
		

	public function get_qbo_subscriptions_data($condition , $page=0, $limit='' )
	{
		$res=array();
		$this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		$this->db->join('tbl_subscriptions_plan_qbo pl','pl.planID = sbs.subscriptionPlan','left');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where($condition);		
		$this->db->where('cust.customerStatus', 'true');
	    $this->db->order_by("sbs.createdAt", 'desc');
		$start=0;
		if($page > 0)
		{
		$start = $page*$limit;
		 $this->db->limit($limit,$start);	
		}else{
			if($limit > 0)
		$this->db->limit($limit);
		 else
		$this->db->limit(10);
		}
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
			 $res= $query->result_array();
			 
			 foreach($res as $k=>$result)
			 {
				 $qr = $this->db->query("SELECT * from tbl_subscription_invoice_item_qbo where  subscriptionID ='".$result['subscriptionID']."' ");
				 $items = array();
				 if($qr->num_rows() > 0)
			     {
					$items = $qr->result_array();
				 }
				 $res[$k]['items'] = $items;
			 }
		}
		
		return $res;
		
	} 
  
	  	
		public function check_customer_data($customerID='',$full_name,$merchantID)
		{
			$resdata =array();
			if($full_name!="")
			{
				
				
			   $order= "id 'desc'";
				$in_data=0; 
			   $ap_data =  $this->get_row_order_data('app_integration_setting',array('merchantID'=>$merchantID), $order);
			
			   if(!empty($ap_data))
			   {
					if($ap_data['appIntegration']==2)
					{
					   
					  $con =   array('qbmerchantID'=>$merchantID,'FullName'=>$full_name);
					 $count =    $this->get_num_rows('qb_test_customer',$con);
					 
						 if( ($customerID=="" && $count==0) )
						 {
							$status= 'success' ;
						 }
						 else if($customerID !="")
						 {
							 $qq= $this->db->query(" Select count(*) as cnt from qb_test_customer where ListID !='".$customerID."' 
							 and qbmerchantID ='".$merchantID."' and FullName='".$full_name."'  ");
					
					  
							 if($count> 0)
							 $status= 'error' ;
							 else
							 $status= 'success' ;
						 }
						 else
						 {
							 $status= 'error' ;
						 }
					}
					if($ap_data['appIntegration']==1)
					{
					 $con =   array('merchantID'=>$merchantID,'fullName'=>$full_name);
					 $count =    $this->get_num_rows('QBO_custom_customer',$con);
					 
						 if( ($customerID=="" && $count==0) )
						 {
							$status= 'success' ;
						 }
						 else if($customerID !="")
						 {
							 $qq= $this->db->query(" Select count(*) as cnt from QBO_custom_customer where Customer_ListID !='".$customerID."' 
							 and merchantID ='".$merchantID."' and fullName='".$full_name."'  ");
							 
							 $count = $qq->row_array()['cnt'];
					  
							 if($count> 0)
							 $status= 'error' ;
							 else
							 $status= 'success' ;
						 }
						 else
						 {
							 $status= 'error' ;
						 }
				   
					}
			   
					
					
					 if($status=='success')
					 {
						  $resdata['status']  ='success';
					  
					 }
					 else
					 {
						$resdata['status']  ='error';
					  
					 }
				   
				   
			   }else{
				  
				   $resdata['status']  ='error';
				  
			   }
			  
			}else{
				  
				   $resdata['status']  ='error';
				  
			   }
			   
			   
		 return $resdata;
			
		}
	
	    public function get_row_order_data($table, $con,$ord_by='') 
	    {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->order_by('id','desc')->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	    }
	  
	 
		public function get_num_rows($table, $con) 
		{
				$num = 0;	
				$query = $this->db->select('*')->from($table)->where($con)->get();
			
				$num = $query->num_rows();
				return $num;
			
		}
	
  	public function get_invoice_item_data($con)
	{
	 $res  = array();
	$query =  $this->db->query(" SELECT itm.invoiceID, itm.itemRefID as itemID,ifnull(it.AssetAccountRef,'')as AssetAccountRef ,ifnull(it.IncomeAccountRef,'')as IncomeAccountRef,ifnull(it.ExpenseAccountRef,'') as ExpenseAccountRef  from tbl_qbo_invoice_item itm inner join QBO_test_item it on it.productID=itm.itemRefID where itm.invoiceID in ($con) limit 1 "); 
	 if($query->num_rows > 0)
	 {
	     $res =$query->row_array();
	     
	      if($res['AssetAccountRef'])
    	    $res['AssetAccountName']    =  $this->db->query(" SELECT ifnull(accountName,'') as accountName from  QBO_accounts_list where accountID= '".$res['AssetAccountRef']."'  limit  1 ")->row_array()['accountName']; 		  
    	  
    	  if($res['IncomeAccountRef'])
    	    $res['IncomeAccountName']   =  $this->db->query(" SELECT ifnull(accountName,'') as accountName from  QBO_accounts_list where accountID= '".$res['IncomeAccountRef']."'  limit  1 ")->row_array()['accountName']; 
    	  if($res['ExpenseAccountRef'])
    	     $res['ExpenseAccountName']   =  $this->db->query(" SELECT ifnull(accountName,'') as accountName from  QBO_accounts_list where accountID= '".$res['ExpenseAccountRef']."'  limit  1 ")->row_array()['accountName'];
	 }
	 
	 return $res;
	 
	}
	
	
  
  
	


 
	 
	 
  
  
  
	
}

 