<?php

class Quickbooks extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Set the DSN connection string for the queue class
	 */
	public function dsn($dsn)
	{
		$this->_dsn = $dsn;
	}
	
	/**
	 * Queue up a request for the Web Connector to process
	 */
	public function enqueue($action, $ident, $priority = 0, $extra = null, $user = null)
	{
		$Queue = new QuickBooks_WebConnector_Queue($this->_dsn);
		
		return $Queue->enqueue($action, $ident, $priority, $extra, $user);
	}
	
	
	

	public function testcustomer($action){
					
			$Customer = new QuickBooks_QBXML_Object_Customer($this->_dsn);
			$Customer->setFullName('web:Keith Palmer');
			print('FullName: ' . $Customer->getFullName() . "\n");
			print('Name: ' . $Customer->getName() . "\n");
			print('Parent: ' . $Customer->getParentName() . "\n");
			print("\n");
			print($Customer->asQBXML($action));
	
		
	}
	
	
	
	public function get_invoice_data(){
		$res = array();
		$today = date('Y-m-d');
		
		$this->db->select('inv.*, cust.*, nmi.*,cvalt.*,cmp.merchantID, cmp.qbwc_username ');
//	$this->db->select('inv.*, cust.* ');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	   $this->db->join('tbl_cutomer_vault cvalt','cust.ListID = cvalt.customerListID','INNER');
       $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
       $this->db->join('tbl_nmi_data nmi','cmp.merchantID = nmi.merchantID','INNER');
	   $this->db->where("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') <=", $today);
	    $this->db->where("inv.BalanceRemaining != ", '0.00');
	   $this->db->where('inv.userStatus' , '');
	   $this->db->where('cust.customerStatus', '1');
		
		
		$query = $this->db->get();
   //  echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return $res = $query->result_array();
		}
		
		return $res;
	} 		
	
	
	
	
		
	public function get_invoice_data_auto_pay(){
		$res = array();
		$today = date('Y-m-d');
		
	   $this->db->select('inv.*,tci.invoiceRefNumber, tci.cardID, tmg.gatewayType,tmg.gatewayType,tmg.gatewayUsername,tmg.gatewayPassword,  cmp.merchantID, cmp.qbwc_username' );
	   $this->db->from('qb_test_invoice inv');
	   $this->db->join('tbl_custom_invoice tci','inv.RefNumber =tci.RefNumber','INNER');
	   $this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
       $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
       $this->db->join('tbl_merchant_gateway tmg','tci.gatewayID = tmg.gatewayID','INNER');
	   
       $this->db->where("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') <=", $today);
	   $this->db->where('inv.IsPaid', 'false');
	   $this->db->where("inv.BalanceRemaining != ", '0.00');
	   $this->db->where('inv.userStatus' , '');
	   $this->db->where('cust.customerStatus', '1');
	   $this->db->where('tci.autoPayment', '1');
		
		
		$query = $this->db->get();
   //  echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return $res = $query->result_array();
		}
		
		return $res;
	} 		
	
	
		
	public function get_credit_by_customer($customerID){
          $r_amount = 0; 		
	$this->db->select('sum(creditAmount) as credit_amount');
		$this->db->from('tbl_credits');
		$this->db->where('creditStatus','0');
		$this->db->where('creditName', $customerID);
   //     $this->db->group_by('creditName');		
		$query = $this->db->get();
	   //   echo $this->db->last_query(); die;
		  $r_amount1 = $query->row_array();
	  
		return $r_amount =$r_amount1['credit_amount'];
	}
	
	
	
	
	
	
	
	public function get_invoice_data_pay($invoiceID){
		$res = array();
	
		$this->db->select('inv.*, cust.*, cmp.merchantID, cmp.qbwc_username');
//	$this->db->select('inv.*, cust.* ');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	
	    $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
      
	    $this->db->where('inv.TxnID' , $invoiceID);
	    $this->db->where('cust.customerStatus', '1');
		$query = $this->db->get();
   //  echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	} 		
	
	
	
	
	
	public function get_invoice_mail_data($type){
			$res = array();
	
		$this->db->select('inv.*, cust.*');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	    $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
		$this->db->join('tbl_merchant_data mer' , 'cmp.merchantID = mer.merchID', 'inner');
	  //  $this->db->where('inv.TxnID' , $invoiceID);
	    $this->db->where('cust.customerStatus', '1');
		$this->db->where ('IsPaid', 'false');
		$this->db->where ('userStatus', '');
		if($type=='0')
		 $this->db->where ("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < ", date('Y-m-d'));
        if($type=='1')	
	    $this->db->where ("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') > ", date('Y-m-d'));
		$query = $this->db->get();
		
		//$query = $this->db->query($sql);
    // echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
		
		  return $res = $query->result_array();
		}
		
		return $res;
		
		
	}
	
		public function get_qbo_invoice_data_pay($invoiceID){
		$res = array();
	
		$this->db->select('inv.*,cust.*');
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('QBO_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where('inv.invoiceID' , $invoiceID);
	   
		$query = $this->db->get();

		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	}
	
	
	
	
	
}