<?php
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use iTransact\iTransactSDK\iTTransaction;
Class General_model extends CI_Model
{
	
	function general()
	{
		parent::Model();
		$this->load->database();
	}
	
	function insert_row_data($table_name, $array, $skipKeys=['message'])
	{
		$dataToInsert = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		} 
	    
	    $st =$this->db->insert($table_name,$dataToInsert);
	  
		if($st){
         
		return $this->db->insert_id();
		}else{ 
		return false;
		}
	}
		function insert_row($table_name, $array, $skipKeys=['message'])
	{ 
		$dataToInsert = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		}
	    $st =$this->db->insert($table_name,$dataToInsert);
	  
		if($st){
         
		return $this->db->insert_id();
		}else{ 
		return false;
		}
	}
	function insert_row_tbl ($table_name, $array, $skipKeys=['message'])
	{
     
		$dataToInsert = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		}
	 if($this->db->insert($table_name,$array)){
         return $this->db->insert_id();
	 }else{ 
		return false;
		}
	}
	
	function update_row($table_name,$reg_no,$array)
	{
		$this->db->where('reg_no', $reg_no);
		if($this->db->update($table_name, $array))
			return true;
		else
			return false;
	}
	
	public function check_existing_user($tbl, $array)
    {
        $this->db->select('*');
        $this->db->from($tbl);
		if($array !='')
        $this->db->where($array);
		
        $query = $this->db->get();
		if($query->num_rows() > 0)
			return true;
        
        else 
          return false; 
	}
	
	
	function update_row_data($table_name,$condition, $array, $skipKeys=['message'])
	{
	
		$dataToUpdate = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToUpdate[$key] = strip_tags($value);
			else
				$dataToUpdate[$key] = $value;
		}

		$this->db->where($condition);
		$trt = $this->db->update($table_name, $dataToUpdate); 

		if($trt)
			return true;
		else
			return false;
	}
	
	   public function get_row_data($table, $con) {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();

			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	  public function get_table_data($table, $con,$order=array(),$limit =0,$startFrom = 0) {
	    $data=array();
	
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
      		if(!empty($order))
            {
            	$this->db->order_by($order['name'],$order['type']);
            }
			if($limit > 0){
				$this->db->limit($limit,$startFrom);
			}
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  	}
	  	
	         public function get_select_data($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
					
					if($query->num_rows()>0 ) {
						$data = $query->row_array();
					   return $data;
					} else {
						return $data;
					}			
			}
	  
	  
	         public function get_table_select_data($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
					
					if($query->num_rows()>0 ) {
						$data = $query->result_array();
					   return $data;
					} else {
						return $data;
					}			
			}
	  
	  
	  
	   public function get_row_order_data($table, $con,$ord_by='') {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->order_by('id','desc')->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	  
	  
	  
	  ///---------  to get the email history data --------///
	
	   public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	
	 //---------  to get the customer subscriptions --------///
	
	 
	public function get_subscriptions_data($condition)
	{
        $res=array();
	
		$this->db->select('sbs.*, cust.FullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions sbs');
		$this->db->join('qb_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		$this->db->join('tbl_subscriptions_plan_qb pl','pl.planID = sbs.planID','left');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
    
	    $this->db->where($condition);		
		$this->db->where('cust.customerStatus', '1');
	    $this->db->group_by("sbs.subscriptionID");
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  $res = $query->result_array();
		}
    
    
  
		return  $res;
	} 
	  
	  
	  
	  
	  public function get_num_rows($table, $con) {
			$num = 0;	
			$query = $this->db->select('*')->from($table)->where($con)->get();
		
			$num = $query->num_rows();
			return $num;
		
	}
	
	
	public function delete_row_data($table, $condition){
	   
     	    $this->db->where($condition);
    $this->db->delete($table);
    $del = $this->db->affected_rows();
            if($del > 0){
			  return true;
			}else{
			  return false;
			}
	
	}
	
      
//////// get data from tbl_auth//////		
		   
		   public function get_auth_data($auth_con) {
             $auth=array();
 

			 
			   $query  = $this->db->query("Select authName from tbl_auth where authID in($auth_con)");
			   if($query->num_rows()>0 ) {
				$datas = $query->result_array();
				foreach($datas as $key=>$data){
					$auth[] = $data['authName']; 
				}
				
				  return $auth;
			   } else {
				return $auth;
					}
   }
	  
	  
	

	public function get_credit_user_data($con)
    {
		if ($this->session->userdata('logged_in')) {
			$login_info 	= $this->session->userdata('logged_in');
			$user_id 				= $login_info['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$login_info 	= $this->session->userdata('user_logged_in');
			$user_id 				= $login_info['merchantID'];
		}
		
		$res = array();
		$this->db->select('cr.*, cust.companyName');
		$this->db->from('qb_customer_credit cr');
		$this->db->join('qb_test_customer cust','cr.CustomerListID = cust.ListID AND cust.qbmerchantID = '.$user_id,'INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where($con);
	
		$this->db->order_by('cr.TimeModified','desc');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 
	
      
	  
	 
    public function get_merchant_user_data($userID, $muID='')
    {
		if($muID != "") {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantUserID = '".$muID."' and  tmu.merchantID = '".$userID."' ");
   			$res = $query->row_array();
			
		} else {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantID = '".$userID."' ");
			$res = $query->result_array();
		} 
		return $res;
	}
	

	public function get_process_trans_count($user_id){
     
		$today = date("Y-m-d");
			$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber ');
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("comp.merchantID ", $user_id);
	    $this->db->where("cust.qbmerchantID ", $user_id);
		$this->db->where('cust.customerStatus', '1');
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->num_rows();  
        }
        else
        {
           return $query->num_rows();
        }
     } 


     public function get_process_trans_data($user_id){
     
		$today = date("Y-m-d");
			$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber ');
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("comp.merchantID ", $user_id);
		$this->db->where('cust.customerStatus', '1');
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return false;
        }
     } 

     public function get_modal_invoice_data($condition){
		
		$num =0;
	    
		$this->db->select('inv.* ');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company cmp','cmp.id = cust.companyID','INNER');
	    $this->db->where($condition);
		$this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return false;
        }
	
	} 		 
      
      
    
    public function get_qbo_invoice_data_let( $con) {
	    $data=array();
	    $today=date('Y-m-d');
	
			$this->db->select("qb.*,cs.fullName  AS custname, (case when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= $today and sc.scheduleDate='0000-00-00') OR sc.scheduleDate IS NULL  THEN  'Open'
			when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') < $today and sc.scheduleDate='0000-00-00') OR sc.scheduleDate IS NULL  THEN  'Past Due' when sc.scheduleDate!='0000-00-00' THEN 'Scheduled' when qb.BalanceRemaining='0' Then 'Paid'  ELSE 'Open'  END as status) ");
			$this->db->from('QBO_test_invoice qb');
		    $this->db->join('QBO_custom_customer cs', 'cs.customer_ListID=qb.CustomerListID','inner'); 
		    $this->db->join('tbl_scheduled_invoice_payment sc', 'sc.invoiceID=qb.invoiceID','left');
		    $this->db->where($con);
		
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }   
         
      
      
      
    public function get_qbo_invoice_data($table, $con) {
	    $data=array();
	  
			$this->db->select('*,(SELECT fullName FROM QBO_custom_customer WHERE customer_ListID = QBO_test_invoice.CustomerListID    LIMIT 1) AS custname');
			$this->db->from($table);
			if(!empty($con)){
			    
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }   
      
      
      
        public function check_existing_portalprefix($portalprefix)
         {
           $num = 0; 
		
				   
				   $query = $this->db->select('*')->from('tbl_config_setting')->where('portalprefix', $portalprefix)->get();
				   $num = $query->num_rows();
				   if( $query->num_rows()>0){
					$num =$query->num_rows();
				   return true;
					 }
					 $query1 = $this->db->select('*')->from('Config_merchant_portal')->where('portalprefix', $portalprefix)->get();
					if( $query->num_rows()>0){
					 $num =$query1->num_rows();
					 return true;
					 }  
						
				  if($num > 0)
				   return true;
						
						else 
						  return false; 
		 } 
		 
 
     public function check_existing_edit_portalprefix($tab, $rID, $lportalprefix){
    
		if($tab=='tbl_config_setting'){
		 $rdata = $this->get_row_data($tab,array('merchantID'=>$rID));
		 if($lportalprefix==$rdata['portalprefix']){
		  return false;
		 }else{ 
		 
	   if(!empty($this->get_row_data($tab,array('portalprefix'=>$lportalprefix))))
	    return true;
		  else 
	     $sta= false;
	 

		  if($sta ==false)
				if( !empty($this->get_row_data('Config_merchant_portal',array('portalprefix'=>$lportalprefix)))) 
				return  true;
		  else
	      $sta= false;
	  
		return $sta; 
	  }
	  
		}
      }  
      
      public function get_invoice_bal_data($table, $con) {
	    $data=array();
	  
			$this->db->select('BalanceRemaining');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }
      
       public function get_xero_invoice_data($table, $con) {
	    $data=array();
	  
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
               $this->db->where('UserStatus !=', 'DELETED');
               $this->db->where('UserStatus !=', 'VOIDED');
              	$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }   
	
		public function delete_tax_row_data($table, $condition){
	   
     	 
     	    $this->db->where("(Status='ARCHIVED' OR Status='DELETED') AND merchantID = $condition");
            if($this->db->delete($table)){
			  return true;
			}else{
			  return false;
			}
	}
	
	
	
	
	
	public function chk_echeck_gateway_status($merchantID, $page='' )
	{
	    $query = $this->db->query("Select * from tbl_merchant_gateway where merchantID='".$merchantID."'  and echeckStatus='1'");
	  $res=array(); 
	   
	   if($page!='')
	   {
	        if($query->num_rows() >0)
	    {
	        $res= $query->result_array();
	        
	    }
	        return $res;
	    
	       
	   }else{
	    if($query->num_rows() >0)
	    {
	        return true;
	        
	    }else{
	        return false;
	    }
	    
	   }
	    
	}

	
   public function chk_merch_plantype_status($userID)
   {    
      	
      	if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$isFreeTrial = $this->getMerchantFreeTrial($user_id);
		if($isFreeTrial){
			return [];
		}
		
        $this->db->select('p.*, planType');
        $this->db->from('plans p');
        $this->db->join('tbl_merchant_data mr','mr.plan_id = p.plan_id','inner');
		$this->db->where_in('merchant_plan_type', ['VT','Free', 'AS']);
		$this->db->where('mr.merchID', $userID);
        $query = $this->db->get();

        $res = false;
        if($query->num_rows()>0)
	   	{
	       $res = $query->row_array();
	   	}	
	 	return $res;   
   }  
   
   public function chk_merch_plantype_as($userID)
   {    
   		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$isFreeTrial = $this->getMerchantFreeTrial($user_id);
		if($isFreeTrial){
			return [];
		}
        $res=false;
        $this->db->select('p.*, planType');
        $this->db->from('plans p');
        $this->db->join('tbl_merchant_data mr','mr.plan_id = p.plan_id','inner');
		$this->db->where('merchant_plan_type', 'AS');
        $this->db->where('mr.merchID', $userID);
        $query = $this->db->get();
        if($query->num_rows()>0)
	   {
	       $res =$query->row_array();
	   }
	 return $res;
   
   } 
   public function chk_merch_plantype_vt($userID)
   {    
      	if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$isFreeTrial = $this->getMerchantFreeTrial($user_id);
		if($isFreeTrial){
			return [];
		}
		$res=false;
        $this->db->select('p.*, planType');
        $this->db->from('plans p');
        $this->db->join('tbl_merchant_data mr','mr.plan_id = p.plan_id','inner');
		$this->db->where_in('merchant_plan_type', ['VT','Free']);
        $this->db->where('mr.merchID', $userID);
        $query = $this->db->get();
        if($query->num_rows()>0)
	   {
	       $res =$query->row_array();
	   }
	 return $res;
   
   }
   public function chk_merch_plantype_es($userID)
   {    
      $res=false;
        $this->db->select('p.*, planType');
        $this->db->from('plans p');
        $this->db->join('tbl_merchant_data mr','mr.plan_id = p.plan_id','inner');
		$this->db->where('merchant_plan_type', 'SS');
        $this->db->where('mr.merchID', $userID);
        $query = $this->db->get();
        if($query->num_rows()>0)
	   {
	       $res =$query->row_array();
	   }
	 return $res;
   
   }
	
	
	
	public function get_random_number($mdata, $mode)
	{
	        if($mode==='DEV')
	        {
               $str =substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,10))),1,10);
               $str =$str.$mdata['companyName'].$mdata['merchID'].microtime();
	        }
	   
	       if($mode==='PRO')
	       {
	           $str =substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,10))),1,15);
               $str =$str.$mdata['companyName'].$mdata['merchantEmail'].$mdata['merchID'].microtime();
	           
	       }
	   $code =     sha1($str);
	       return  $code;
	    
	}
	
	/**
 * Replaces all but the last for digits with x's in the given credit card number
 * @param int|string $cc The credit card number to mask
 * @return string The masked credit card number
 */
function MaskCreditCard($cc){
	// Get the cc Length
	$cc_length = strlen($cc);
	// Replace all characters of credit card except the last four and dashes
	for($i=0; $i<$cc_length-4; $i++){
		if($cc[$i] == '-'){continue;}
		$cc[$i] = 'X';
	}
	// Return the masked Credit Card #
	return $cc;
}

  public  function getType($CCNumber)
 {
            
            
$creditcardTypes = array(
            array('Name'=>'American Express','cardLength'=>array(15),'cardPrefix'=>array('34', '37'))
            ,array('Name'=>'Maestro','cardLength'=>array(12, 13, 14, 15, 16, 17, 18, 19),'cardPrefix'=>array('5018', '5020', '5038', '6304', '6759', '6761', '6763'))
            ,array('Name'=>'Mastercard','cardLength'=>array(16),'cardPrefix'=>array('51', '52', '53', '54', '55'))
            ,array('Name'=>'Visa','cardLength'=>array(13,16),'cardPrefix'=>array('4'))
            ,array('Name'=>'JCB','cardLength'=>array(16),'cardPrefix'=>array('3528', '3529', '353', '354', '355', '356', '357', '358'))
            ,array('Name'=>'Discover','cardLength'=>array(16),'cardPrefix'=>array('6011', '622126', '622127', '622128', '622129', '62213',
                                        '62214', '62215', '62216', '62217', '62218', '62219',
                                        '6222', '6223', '6224', '6225', '6226', '6227', '6228',
                                        '62290', '62291', '622920', '622921', '622922', '622923',
                                        '622924', '622925', '644', '645', '646', '647', '648',
                                        '649', '65'))
            ,array('Name'=>'Solo','cardLength'=>array(16, 18, 19),'cardPrefix'=>array('6334', '6767'))
            ,array('Name'=>'Unionpay','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('622126', '622127', '622128', '622129', '62213', '62214',
                                        '62215', '62216', '62217', '62218', '62219', '6222', '6223',
                                        '6224', '6225', '6226', '6227', '6228', '62290', '62291',
                                        '622920', '622921', '622922', '622923', '622924', '622925'))
            ,array('Name'=>'Diners Club','cardLength'=>array(14),'cardPrefix'=>array('300', '301', '302', '303', '304', '305', '36'))
            ,array('Name'=>'Diners Club US','cardLength'=>array(16),'cardPrefix'=>array('54', '55'))
            ,array('Name'=>'Diners Club Carte Blanche','cardLength'=>array(14),'cardPrefix'=>array('300','305'))
            ,array('Name'=>'Laser','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('6304', '6706', '6771', '6709'))
    );     

            $CCNumber= trim($CCNumber);
            $type='Unknown';
            foreach ($creditcardTypes as $card){
                if (! in_array(strlen($CCNumber),$card['cardLength'])) {
                    continue;
                }
                $prefixes = '/^('.implode('|',$card['cardPrefix']).')/';            
                if(preg_match($prefixes,$CCNumber) == 1 ){
                    $type= $card['Name'];
                    break;
                }
            }
            return $type;
        }


   public function get_reseller_name_data($merchantID)
   {    if(!empty($merchantID)){
        $this->db->select('r.resellerfirstName, r.lastName, r.resellerCompanyName');
        $this->db->from('tbl_merchant_data m');
        $this->db->join('tbl_reseller r','r.resellerID=m.resellerID','inner');
        $this->db->where('m.merchID',$merchantID);
        $query = $this->db->get();
        $res = $query->row_array();
        $name = $res['resellerCompanyName'];
        return  $name;
    }
   }



    public function get_rows($table, $con) {
			$this->db->select('*');	
			$this->db->from($table);
			$this->db->where($con);
			$query = $this->db->get();
			return  $query->row();
	
	}
	
	/*************Convert Date one time zone to other timezone*********/
	
 function datetimeconv($datetime, $from, $to)
{
    try {
        if ($from['localeFormat'] != 'Y-m-d H:i:s') {
            $datetime = DateTime::createFromFormat($from['localeFormat'], $datetime)->format('Y-m-d H:i:s');
        }
        $datetime = new DateTime($datetime, new DateTimeZone($from['olsonZone']));
        $datetime->setTimeZone(new DateTimeZone($to['olsonZone']));
        return $datetime->format($to['localeFormat']);
    } catch (\Exception $e) {
        return null;
    }
}

	 function datetimeconvqbo($datetime, $from, $to)
   {
    try {
        if ($from['localeFormat'] != 'Y-m-d H:i:s') {
            $datetime = DateTime::createFromFormat($from['localeFormat'], $datetime)->format('Y-m-d H:i:s');
        }
        $datetime = new DateTime($datetime, new DateTimeZone($from['olsonZone']));
        $datetime->setTimeZone(new DateTimeZone($to['olsonZone']));
        return $datetime->format('Y-m-d H:i:s');
    } catch (\Exception $e) {
        return null;
    }
  }	
	
	
	
	
   
	public function get_invoice_transaction_details_data($mID,$inID,$appID)
	{
	$today = date("Y-m-d H:i");
		$res =array();
		
		$type = array('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE');
		$code = array('200','100','111','1');
		$this->db->select('tr.id,tr.transactionID,tr.transactionGateway, tr.transactionAmount');
		$this->db->from('customer_transaction tr');
		if($appID=='1')
		{
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
		
		      $this->db->where('cust.merchantID',$mID);
		}
		if($appID=='2')
		{
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID AND cust.qbmerchantID = '.$mID,'INNER');
		}
		
		if($appID=='3')
		{
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
		  $this->db->where('cust.merchantID',$mID);
		}
		if($appID=='4')
		{
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
		}
			if($appID=='5')
		{
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		}
	
        $this->db->where_in('UPPER(transactionType)',$type);
        $this->db->where_in('transactionCode',$code);
        $this->db->where('tr.merchantID',$mID);
      
    
  	if($appID=='2' || $appID=='5')
		{
		  $this->db->where("tr.invoiceTxnID",$inID);
		}
		else
		{
		     $this->db->where("tr.invoiceID",$inID);
		    
		}  
  
    $this->db->order_by('tr.transactionDate', 'desc');
   
		
    
	
	
	$query = $this->db->get();
	if($query->num_rows() > 0)
	{
	
		 $res1= $query->result_array();
		 
		 
		 foreach($res1 as $res2)
		 {   
        
		       $qr = $this->db->query('select refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$res2['transactionID'].'"  and creditInvoiceID="'.$inID.'" ');
        	   $refamt=0;
		       if($qr->num_rows() > 0)
		       {
		            $res_am=$qr->result_array();
		            foreach($res_am as $ref)
		            {
		                $refamt=$ref['refundAmount']+$refamt;
		            }
		           
		       }
		     
		       $res2['final_amount']      =    ($res2['transactionAmount']- $refamt);
		       $res2['transactionAmount'] =    ($res2['transactionAmount']-$refamt);
		       
		       $res[] =$res2;
		 }
	
   
	 }
	return $res;
	
} 	


	public function get_invoice_transaction_single_data($con,$mID,$appID)
	{
		$today = date("Y-m-d H:i");
			$res2 =array();
			$this->db->select('tr.id,tr.transactionID, sum(tr.transactionAmount) as transactionAmount  ');
    		$this->db->from('customer_transaction tr');
    		if($appID=='1')
    		{
    		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
    		}
    		if($appID=='2')
    		{
    		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
    		}
    		
    		if($appID=='3')
    		{
    		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
    		}
    		if($appID=='4')
    		{
    		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
    		}
    		
    		if($appID=='5')
    		{
    		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
    		}
    	
	    $this->db->where('tr.transactionID',$con);
	    $this->db->where('tr.merchantID',$mID);
	    $this->db->where('tr.merchantID',$mID);
	  
		  $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
    		 $res2= $query->row_array();
    		 
    		 
    		
    		          $qr = $this->db->query('select refundAmount   from tbl_customer_refund_transaction where creditTransactionID="'.$con.'" ');
        	    $refamt=0;
		       if($qr->num_rows() > 0)
		       {
		            $res_am=$qr->result_array();
		            foreach($res_am as $ref)
		            {
		                $refamt=$ref['refundAmount']+$refamt;
		            }
		           
    		      $res2['final_amount'] = $res2['transactionAmount']- $refamt;
    		      }else{
    		       $res2['final_amount']=    $res2['transactionAmount']- $refamt;
    		          
    		      }
    		    
    	
    	
	   
		 }
		return $res2;
		
	} 	
	
	
	
	public function get_public_page_data($m_id)
	{
	   $res=array();
	   
	   $this->db->select('r.resellerCompanyName, r.resellerProfileURL as ProfileURL,r.webURL, m.companyName, m.weburl');
	   $this->db->from('tbl_merchant_data m');
	   $this->db->join('tbl_reseller r','r.resellerID=m.resellerID','INNER');
	   $this->db->where('m.merchID',$m_id);
	   $query = $this->db->get();
	   if($query->num_rows()>0)
	   {
	       $res =$query->row_array();
	   }
	 return $res;
	}
	
	
	public function get_gateway_data($mID, $echeck=false)
	{
	    $res=$gateway=array();
		$def_url ='';
		
		$condition = $conditionDefault = [
			'merchantID'=>$mID
		];

		$isEcheckExists = $isCardExists = 0;

		if($echeck){
			$condition['echeckStatus'] = 1;
		}

	    $gateway = $this->get_table_data('tbl_merchant_gateway',$condition);
	    
	    $res['gateway'] = $gateway;
		$stpUser ="0";
		
		$conditionDefault['set_as_default'] = 1;
	    $def_gatway = $this->get_row_data('tbl_merchant_gateway', $conditionDefault);
	    if(!empty($def_gatway))
	    {
	        if($def_gatway['gatewayType']=='1')
	        {
				$def_url = 'Payments/';
	        }
	        if($def_gatway['gatewayType']=='2')
	        {
				$def_url = 'AuthPayment/';
				
	        }
	        if($def_gatway['gatewayType']=='3')
	        {
				$def_url = 'PaytracePayment/';
				
	        }
	        if($def_gatway['gatewayType']=='4')
	        {
				$def_url = 'PaypalPayment/';
				
	        }
	        if($def_gatway['gatewayType']=='5')
	        {
				$def_url = 'StripePayment/';
				$stpUser = $def_gatway['gatewayUsername'];
	        }
	        if($def_gatway['gatewayType']=='6')
	        {
				$def_url = 'UsaePay/';
				
	        }
			if($def_gatway['gatewayType']=='7')
	        {
				$def_url = 'GlobalPayment/';
				
	        }
	        
			if($def_gatway['gatewayType']=='8')
	        {
				$def_url = 'CyberSource/';
				
			}
			if($def_gatway['gatewayType']=='9')
	        {
				$def_url = 'Payments/';
			}
			if($def_gatway['gatewayType']=='10')
	        {
				$def_url = 'iTransactPayment/';
				
	        }
	        if($def_gatway['gatewayType']=='11')
	        {
				$def_url = 'FluidpayPayment/';
				
	        }
	        if($def_gatway['gatewayType']=='12')
	        {
				$def_url = 'TSYSPayment/';
				
	        }
	        if($def_gatway['gatewayType']=='13')
	        {
				$def_url = 'BasysIQProPayment/';
			}
			if($def_gatway['gatewayType']=='15')
	        {
				$def_url = 'PayarcPayment/';
			}
			if($def_gatway['gatewayType']=='17')
	        {
				$def_url = 'MaverickPayment/';
	        }
	        if($def_gatway['gatewayType']=='16')
	        {
				$def_url = 'EPXPayment/';
			}
			if($def_gatway['gatewayType']=='14')
	        {
				$def_url = 'CardPointePayment/';
	        }
	    }

		$echeckArray = array_filter($gateway, function ($var) {
			return ($var['echeckStatus'] == '1');
		});

		if(count($echeckArray) > 0){
			$isEcheckExists = 1;
		}

		$cardArray = array_filter($gateway, function ($var) {
			return ($var['creditCard'] == '1');
		});

		if(count($cardArray) > 0){
			$isCardExists = 1;

			if(!$echeck){
				$res['gateway'] = $cardArray;
			}
		}
	  
	    $res['url'] =   $def_url;
	    $res['stripeUser'] =   $stpUser;
	    $res['isEcheckExists'] =   $isEcheckExists;
	    $res['isCardExists'] =   $isCardExists;
	    
	    return $res;

	}

	public function track_user($user_type, $user_email, $loginID){
	               $this->load->library('user_agent');
                 $log['browser'] = $this->agent->browser();
                 $ip =   getClientIpAddr();
                  // set url 
                      $ch = curl_init(); 
                     curl_setopt($ch, CURLOPT_URL, "http://ip-api.com/json/$ip"); 
                  //return the transfer as a string 
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                    // $output contains the output string 
                     $output = curl_exec($ch);
                 // close curl resource to free up system resources 
		            curl_close($ch); 
                 
                  $user_data = json_decode($output, TRUE);
                
                   $log_data['ip_address'] = $ip;
                   $log_data['City']      = $user_data['city'];
                   $log_data['State']     = $user_data['regionName'];
                   $log_data['Country']   = $user_data['country'];
               
                   $log_data['ISP']   = $user_data['isp'];
                   $log_data['browser']   = $log['browser'];
                    $log_data['UserType'] = $user_type;
                    $log_data['emailAddress']  =$user_email; 
                  $log_data['UserID'] = $loginID;
                       $log_data['Login_at'] = date('Y-m-d H:i:s'); 
                     $result =  $this->general_model->insert_row('tbl_security_log', $log_data);
                  
				
				
	}	
	
    	
    		
    function get_random_string( $attr) 
    {
        $randstr='';
        
    
        $code =substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 0, 6);
    	if($attr=='CUS')
    	$num = mt_rand(5000000,9000000);
    	if($attr=='INV')
    	$num = mt_rand(1000000,5000000);
        if($attr=='PRO')
    	$num = mt_rand(500000,900000);
    	
    	$randstr =$attr.'-'.$num.'-'.$code;
        return $randstr;
    }
    	

	
	 public function insert_gateway_transaction_data($res, $tr_type, $gateway,$gt_type,$customer,$amount,$mID,$crtxnID,$resellerID,$invID, $echeckType = false, $transactionByUser = [], $custom_data_fields = [], $parent_id = null)
	{
						   $transaction=array();
						   $type = array('AUTH', 'PAYPAL_AUTH','PAY_AUTH','AUTH_ONLY','STRIPE_AUTH');
						 if(in_array(strtoupper($tr_type),$type))
						 	$transaction['transaction_user_status']   = '5';

						 if($tr_type == 'refund'){
						 	$transaction['transaction_user_status']   = '2';
						 }  
						   
						   if($gt_type==1)
						   {	   
								$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							   $transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['transactionID']     = isset($res['transactionid']) ? $res['transactionid'] : 'TXNFAILED-'.time();
							   $transaction['transactionStatus']  = $res['responsetext'];
							   $transaction['transactionStatus']     = isset($res['transactionStatus']) ? $res['transactionStatus'] : 'Declined';
							   $transaction['transactionCode']    =  $res['response_code'];
								$transaction['transactionType']   =  $res['type'];
								 $transaction['gateway']          = "NMI"; 
						   } 
						if($gt_type==2)
						   {	
							  $transaction['transactionDate']   = date('Y-m-d H:i:s');  
							   $transaction['transactionModified']= date('Y-m-d H:i:s'); 	
							   if($res->response_code == '1' && $res->transaction_id != 0 && $res->transaction_id != ''){
							   		$transaction['transactionID']       = $res->transaction_id;
							   		$transaction['transactionCode']    = $res->response_code;
							   }else{
							   		$transaction['transactionID']  = 'TXNFAILED'.time();
							   		$transaction['transactionCode']    = 400;
							   }	
							  
						   $transaction['transactionStatus']   = $res->response_reason_text;
						   $transaction['transactionType']     = $tr_type;	   
							  
						   $transaction['transactionCard']     = substr($res->account_number,4); 
							
							$transaction['gateway']   = "Auth"; 
						   } 
								if($gt_type==3)
						   {	   
					   
								if(isset($res['transaction_id'])){
									$transaction['transactionID']       = $res['transaction_id'];
								} else if(isset($res['check_transaction_id'])){
										$transaction['transactionID']       = $res['check_transaction_id'];
								}else{
									$transaction['transactionID']  = 'TXNFAILED'.time();
								}
						   $transaction['transactionStatus']   = $res['status_message'];
						   $transaction['transactionDate']     = date('Y-m-d H:i:s');
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 
						   $transaction['transactionCode']     = (isset($res['http_status_code'])) ? $res['http_status_code'] : 300;

						   	if(isset($res['masked_card_number'])) 
						   		$transaction['transactionCard']     = substr($res['masked_card_number'],12);  
						  $transaction['transactionType']    = $tr_type;
								 $transaction['gateway']       = "Paytrace";
						   } 		

							if($gt_type==4)
						   {	  
        							$tranID ='' ;$amt='0.00';
        							if(isset($res['TRANSACTIONID'])) {  $code='111';$tranID = $res['TRANSACTIONID'];   $amt=$res["AMT"];  }else{ $code='401';}
        							
        							if($tr_type=='Paypal_refund')
        							{	
        							 $tranID ='' ;$amt='0.00';
        					      if(isset($PayPalResult['REFUNDTRANSACTIONID'])) { $tranID = $PayPalResult['REFUNDTRANSACTIONID'];   $amt =$PayPalResult['GROSSREFUNDAMT'];  }
        							}
        						   
        						   $transaction['transactionID']       = $tranID;
        						   $transaction['transactionStatus']    = $res["ACK"];
        						   $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($res["TIMESTAMP"]));  
        							$transaction['transactionModified'] =  date('Y-m-d H:i:s',strtotime($res["TIMESTAMP"])); 
        						   $transaction['transactionCode']     = $code;  
        							$transaction['gateway']             = "Paypal";
        							 $transaction['transactionType']    = $tr_type;					   
        							
						   } 
						if($gt_type==5)
						{	   
						    
						      
								if(isset($res->paid) && $res->paid=='1' && $res->failure_code=="")
								{
								  $code		 =  '200';
								  $trID 	 = $res->id;
								  $status = $res->status;
								}
								else
								{
								   $code =  400; 	
								   $trID 	 = 'TXNFAILED-'.time();	
								   $status =  'Declined';
								}		
								$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							   $transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['transactionID']     = $trID;
							   $transaction['transactionStatus']  = $status;
							   $transaction['transactionCode']    =  $code;
								 $transaction['transactionType']    = $tr_type;	
								 $transaction['gateway']          = "Stripe"; 
						} 
						
							if($gt_type==6)
						{	   
								if($res['transactionCode']=='200')
								{
								  $code		 =  '200';
								  $trID 	 = $res['transactionId'];
								}
								else
								{
								   $code     =    $res['transactionCode']; 	
								   $trID 	 = $res['transactionId'];;	
								}		
								$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							   $transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['transactionID']     = $trID;
							   $transaction['transactionStatus']  = $res['status'];
							   $transaction['transactionCode']    =  $res['transactionCode'];
								 $transaction['transactionType']    = $tr_type;	
								 $transaction['gateway']          = "USAePay"; 
								
						} 
                        
						
						
						if($gt_type==7)
						{	   
								if($res['transactionCode']=='200')
								{
								  $code		 =  '200';
								  $trID 	 = $res['transactionId'];
								}
								else
								{
								   $code     =    $res['transactionCode']; 	
								   $trID 	 = $res['transactionId'];;	
								}		
								$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							   $transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['transactionID']     = $trID;
							   $transaction['transactionStatus']  = $res['status'];
							   $transaction['transactionCode']    =  $res['transactionCode'];
								 $transaction['transactionType']    = $tr_type;	
								 $transaction['gateway']          = "Heartland"; 
								if($echeckType){
									$transaction['paymentType'] = 2;
								}
								
						} 
                        
                        
                        if($gt_type==8)
						{	   
								if($res['transactionCode']=='200')
								{
								  $code		 =  '200';
								  $trID 	 = $res['transactionId'];
								}
								else
								{
								   $code     =    $res['transactionCode']; 	
								   $trID 	 = $res['transactionId'];;	
								}		
								$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							   $transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['transactionID']     = $trID;
							   $transaction['transactionStatus']  = $res['status'];
							   $transaction['transactionCode']    =  $res['transactionCode'];
								 $transaction['transactionType']    = $tr_type;	
								 $transaction['gateway']          = "Cybersource"; 
								
						} else if($gt_type==9){	   
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 
							$transaction['transactionID']     = $res['transactionid'];
							$transaction['transactionStatus']  = $res['responsetext'];
							$transaction['transactionCode']    =  $res['response_code'];
							$transaction['transactionType']   =  $res['type'];
							$transaction['gateway']          = "Chargezoom"; 
						} else if($gt_type== 10){
							$code = $res['status_code'] ;
							if($code =='201'){
								$code = '200';
							}

							$statusRes = 'Failed';
							if(isset($res['status'])){
								$statusRes = $res['status'];
							} else if(isset($res['error']) && isset($res['error']['message'])){
								$statusRes= $res['error']['message'];
							}
							
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 
							$transaction['transactionID']     = (isset($res['id']))?$res['id']:'TXNFAILED'.time();
							$transaction['transactionStatus']  = $statusRes ;
							$transaction['transactionCode']    =  $code; 
							$transaction['transactionType']   =  $tr_type;
							$transaction['gateway']          = iTransactGatewayName; 
						}  else if($gt_type== 11 || $gt_type== 13){
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 
							$transaction['transactionID']     = (isset($res['data']) && !empty($res['data'])) ? $res['data']['id'] : '';
							$transaction['transactionStatus']  = $res['msg'];

							$code = 300;
							if($res['status'] == 'success' && isset($res['data']) && !empty($res['data'])) {
								if(isset($res['data']['response_code'])){
									$code = $res['data']['response_code'];
								} else {
									$code = 100;
								}
							}
								 

							$transaction['transactionCode']    =  $code;
							$transaction['transactionType']   =  $tr_type;
							$transaction['gateway']          = $gt_type == 11 ? FluidGatewayName : BASYSGatewayName; 
						}else if($gt_type == 12){
							$responseType = $res['responseType'];
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 
							$transaction['transactionID']     = (isset($res[$responseType]['transactionID'])) ? $res[$responseType]['transactionID'] : 'TXNFail-'.time();
							$transaction['transactionStatus']  = $res[$responseType]['responseMessage'];

							$code = 300;
							if($res[$responseType]['status'] == 'PASS') {
								$code = 100;
							}

							$transaction['transactionCode']    =  $code;
							$transaction['transactionType']   =  $tr_type;
							$transaction['gateway']          = TSYSGatewayName; 
						}else if($gt_type==14){	   
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 
							$transaction['transactionID']     = $res['retref'];
							$transaction['transactionStatus']  = $res['resptext'];
							$transaction['transactionType']   =  $tr_type;
							$transaction['gateway']          = "CardPointe"; 

							$code = 300;
							if($res['resptext'] == 'Approved' || $res['resptext'] == "Success" || $res['resptext'] == "Approval") {
								$code = 100;
							}
							$transaction['transactionCode']   =  $code;
							
						
						}else if($gt_type== 15){
							
							
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 

							$transaction['transactionID']     = (isset($res['data']) && !empty($res['data'])) ? $res['data']['id'] : '';
							$transaction['transactionStatus']  = (isset($res['data']) && !empty($res['data'])) ? 'SUCCESS' : $res['message'];

							$code = 300;
							if(isset($res['data']) && !empty($res['data'])) {
								if(isset($res['data']['response_code'])){
									$code = $res['data']['response_code'];
								} else {
									$code = 100;
								}
							}

							$transaction['transactionCode']    =  $code;
							$transaction['transactionType']   =  $tr_type;
							$transaction['gateway']          = PayArcGatewayName;
						}else if($gt_type== 17){
							
							
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 

							$transaction['transactionID']     = (isset($res['data']) && !empty($res['data'])) ? $res['data']['id'] : '';
							$transaction['transactionStatus']  = (isset($res['status']) && $res['status'] == 'success') ? 'SUCCESS' : $res['message'];

							$code = 300;
							if($res['status'] == 'success' && isset($res['response_code']) && !empty($res['response_code'])) {
								$code = $res['response_code'];
							}

							$transaction['transactionCode']    =  $code;
							$transaction['transactionType']   =  $tr_type;
							$transaction['gateway']          = MaverickGatewayName;
						}else if($gt_type == 0){
							
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 
							$transaction['transactionID']     = $res['transactionID'];
							$transaction['transactionStatus']  = 'Offline Payment';

							$code = 200;
							$transaction['transactionCode']    =  $code;
							$transaction['transactionType']   =  'Offline Payment';
							$transaction['gateway']          = ''; 
						}else if($gt_type== 16){
							
							$code = 300;

							if(isset($res['AUTH_RESP']) && $res['AUTH_RESP'] == '00' ){
								$message = 'SUCCESS';
								$code = 100; 
								$transactionId = $res['AUTH_GUID'];
							}else{
								$message = $res['AUTH_RESP_TEXT'];
								$transactionId = isset($res['transactionid'])? $res['transactionid']:'TXNFAILED'.time();
							}
							$transaction['transactionDate']   = date('Y-m-d H:i:s');  
							$transaction['transactionModified']= date('Y-m-d H:i:s'); 

							$transaction['transactionID']     = $transactionId;
							
							$transaction['transactionStatus']  = $message;

							$transaction['transactionCode']    =  $code;
							$transaction['transactionType']   =  $tr_type;
							$transaction['gateway']          = EPXGatewayName;
						}
						
						if( ($gt_type != 5 && $gt_type != 2) && isset($res['FAILEDTXNID']) && !empty($res['FAILEDTXNID'])){
							$transaction['transactionID']     = $res['FAILEDTXNID'];
							$transaction['transactionStatus']  = $res['FAILEDTXNSTATUS'];

							$transaction['transactionCode']    =  300;
						}
						$transaction['gatewayID']       = $gateway;
						$transaction['transactionGateway'] = $gt_type;	
						$transaction['customerListID']     = $customer;
						$transaction['transactionAmount']  = $amount;
						$transaction['merchantID']        = $mID;
						$transaction['qbListTxnID']       = $crtxnID;
					    $transaction['resellerID']       = $resellerID;
					    $transaction['parent_id']       = $parent_id;
						    $res = $this->db->query('Select appIntegration from app_integration_setting where merchantID="'.$mID.'" ')->row_array();
							  $service = $res['appIntegration'];
							  if($service==2 || $service==5)
								$transaction['invoiceTxnID']        = $invID; 
							  else 	
								$transaction['invoiceID']        = $invID; 

							$transaction = alterTransactionCode($transaction);

							if($echeckType){
								$transaction['gateway'] = $transaction['gateway']. " ECheck";
							}
							
							if(!empty($transactionByUser)){
								$transaction['transaction_by_user_type'] = $transactionByUser['type'];
								$transaction['transaction_by_user_id'] = $transactionByUser['id'];
							}

							if($custom_data_fields){
								$transaction['custom_data_fields'] = json_encode($custom_data_fields);
							}

							if($parent_id){
								$transaction['parent_id'] = $parent_id;
							}

							$transaction['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
							$callCampaign = $this->triggerCampaign($mID,$transaction['transactionCode']);

							if(!isset($transaction['transactionID']) || empty($transaction['transactionID']) || !$transaction['transactionID']){
								$transaction['transactionID'] = 'TXNFail-'.time();
							}
							$transaction['transactionType']     = $tr_type;
							$trID = $this->insert_row('customer_transaction',$transaction);


							return $trID;
				   
			   }	




	public function get_payment_transaction_details_data($mID,$trID,$appID)
	{
		$today = date("Y-m-d H:i");
		$res =array();
		
		$type = array('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE');
		$code = array('200','100','111','1');
		$this->db->select('tr.id,tr.transactionID,tr.transactionGateway, tr.invoiceTxnID,    tr.invoiceID, tr.transactionAmount');
		$this->db->from('customer_transaction tr');
		if($appID=='1')
			{
			$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
			$this->db->join('QBO_test_invoice inv','inv.invoiceID = tr.invoiceID','INNER');
			$this->db->where('cust.merchantID',$mID);
		    $this->db->where('inv.merchantID',$mID);
		}
		if($appID=='2')
			{
			$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
			$this->db->join('qb_test_invoice inv','inv.TxnID = tr.invoiceTxnID','INNER');
			
			$this->db->where('cust.qbmerchantID',$mID);
		}
		
		if($appID=='3')
		{
			$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
			$this->db->join('Freshbooks_test_invoice inv','inv.invoiceID = tr.invoiceID','INNER');
			$this->db->where('cust.merchantID',$mID);
			$this->db->where('inv.merchantID',$mID);
		}
		if($appID=='4')
		{
			
			$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
			$this->db->join('Xero_test_invoice inv','inv.invoiceID = tr.invoiceID','INNER');
			$this->db->where('cust.merchantID',$mID);
			$this->db->where('inv.merchantID',$mID);
		}
		
		if($appID=='5')
		{
			$this->db->join('chargezoom_test_invoice inv','inv.TxnID = tr.invoiceTxnID','INNER');
			$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
			$this->db->where('cust.qbmerchantID',$mID);
		}
	
        $this->db->where_in('UPPER(transactionType)',$type);
        $this->db->where_in('transactionCode',$code);
        $this->db->where('tr.merchantID',$mID);
        
		$this->db->where("tr.transactionID",$trID);
	

  
		$this->db->order_by('tr.transactionDate', 'desc');
	   
			
		
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		
			 $res1= $query->result_array();
			 
			 
			 foreach($res1 as $res2)
			 {   
			 
			    
			          
			        if($appID=='2' || $appID=='5')
					{
					    if($appID=='2')
					    $inv_Data =  $this->get_select_data('qb_test_invoice',array('RefNumber'), array('TxnID'=>$res2['invoiceTxnID']));
					     if($appID=='5')
					      $inv_Data =  $this->get_select_data('chargezoom_test_invoice',array('RefNumber'), array('TxnID'=>$res2['invoiceTxnID']));
					    
					     $res2['invoceNumber'] = $inv_Data['RefNumber'];
						$qr = $this->db->query('select refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$res2['transactionID'].'"  and creditInvoiceID="'.$res2['invoiceTxnID'].'" ');
					}
					else
					{
					    
					    $inv_Data =  $this->get_select_data('QBO_test_invoice',array('refNumber'), array('invoiceID'=>$res2['invoiceID'],'merchantID'=>$mID));
					     $res2['invoceNumber'] = $inv_Data['refNumber'];
					$qr = $this->db->query('select refundAmount from tbl_customer_refund_transaction where creditTransactionID="'.$res2['transactionID'].'"  and creditInvoiceID="'.$res2['invoiceID'].'" ');
					}
				 $refamt=0;
				   if($qr->num_rows() > 0)
				   {
						$res_am=$qr->result_array();
						foreach($res_am as $ref)
						{
							$refamt=$ref['refundAmount']+$refamt;
						}
					   
				   }
				 
				   $res2['final_amount']      =    ($res2['transactionAmount']- $refamt);
				   $res2['transactionAmount'] =    ($res2['transactionAmount']-$refamt);
				   
				   $res[] =$res2;
			 }
		
	   
		 }
		 
		 
		return $res;
		
	} 	

	
	
	
		public function template_data($con)
		{
		
	$res=array();
		$this->db->select('tp.*, typ.*');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID=tp.templateType','inner');
	    $this->db->where($con);
		
	  	$query = $this->db->get();
		  return $res =  $query->row_array();
		
	} 	
	
	public function reseller_template_data($con)
	{
	
		$res=array();
		$this->db->select('tp.*, typ.*');
		$this->db->from('tbl_eml_temp_reseller tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID=tp.templateType','inner');
		$this->db->where($con);
		
		$query = $this->db->get();
		return $res =  $query->row_array();
		
	} 
	
	public  function send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $transactionID = '')
	{
		if(empty($toEmail) || !filter_var($toEmail, FILTER_VALIDATE_EMAIL)){
		      return false;
		}
		$attachments = [];
		$companyMercName = '';
		if ($this->session->userdata('logged_in') != "") {
			$companyMercName = $this->session->userdata('logged_in')['companyName'];
			$active_app = $this->session->userdata('logged_in')['active_app'];
		} 
		if ($this->session->userdata('user_logged_in') != "") {
			$companyMercName = $this->session->userdata('user_logged_in')['merchant_data']['companyName'];
			$active_app = $this->session->userdata('user_logged_in')['active_app'];
		}

		$customerDetail = $this->get_customer_details_all($customerID);
		$customerName = $customerDetail['full_name'];
		$company = $customerDetail['company_name'];
	    $phoneNumber='';
	        $user_id = $condition_mail['merchantID'];
	           $res =   $this->get_select_data('tbl_merchant_data',array('merchantEmail','companyName','firstName','lastName','merchantContact', 'merchant_default_timezone'), array('merchID'=>$user_id));

			   	$isValid = is_a_valid_customer([
				   'email' => $toEmail
			   	]);

				if(!$isValid){
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Customer is not synced with active merchant</strong></div>');
					return false;
				}
				
				if(isset($condition_mail['templateType']) == 5){
					
					if (isset($res['merchant_default_timezone'])  && !empty($res['merchant_default_timezone'])) {
			            // Convert added date in timezone 
			            $timezone = ['time' => date('Y-m-d H:i:s'), 'current_format' => 'UTC', 'new_format' => $res['merchant_default_timezone']];
			            $tr_date = getTimeBySelectedTimezone($timezone);
			            $tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
			        }

				}
	          	$view_data      = $this->template_data($condition_mail);	
				  if($view_data['attachedTo'] == 1 && ($view_data['templateType'] == 5 || $view_data['templateType'] == 7)){
					$this->load->model('common_model');
					$this->common_model->getPrintTransactionReceiptPDF($transactionID);
					$pdf_file_name = 'Transaction-Receipt-'.$transactionID.'.pdf';
					$attachments[] = [
						'filename' => $pdf_file_name,
						'content' => base64_encode(file_get_contents(FCPATH.'/uploads/transaction_pdf/'.$pdf_file_name))
					];
				}
					  
				$merchant_name=	($res['companyName'])?$res['companyName']:$res['firstName'].''.$res['lastName'];
				$config_email = $res['merchantEmail'];	  
							  $subject = $view_data['emailSubject'];
							  $fromEmail = $view_data['fromEmail'];
							$phoneNumber=$res['merchantContact'];
							$addCC      = $view_data['addCC'];
							$addBCC		= $view_data['addBCC'];
							
							if($view_data['fromEmail'] == ''){
								$fromEmail = 'donotreply@payportal.com';
							}
					     		
							  $in_link='';
							   $message = $view_data['message'];

							   $subject = stripslashes(str_ireplace('{{creditcard.type_name}}','Payment' ,$subject ));
							   
							   $message = stripslashes(str_ireplace('{{creditcard.type_name}}','' ,$message ));
							   $message = stripslashes(str_ireplace('{{transaction.currency_symbol}}','' ,$message ));
							   $message = stripslashes(str_ireplace('{{merchant_company_name}}',$companyMercName ,$message ));
							   $message = stripslashes(str_ireplace('{{customer.company}}',$company ,$message));
							   $message = stripslashes(str_ireplace('{{customer.name}}',$customerName ,$message));
							   $message = stripslashes(str_ireplace('{{customer_contact_name}}',$customer ,$message ));
							   $message =	stripslashes(str_ireplace('{{invoice.refnumber}}',$ref_number, $message));
								$message =	stripslashes(str_ireplace('{{invoice_payment_pagelink}}',$in_link, $message));
							   $message = stripslashes(str_ireplace('{{transaction.amount}}',($amount)?('$'.number_format($amount,2)):'0.00' ,$message )); 
							   $message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message ));
							   $message = stripslashes(str_ireplace('{{merchant_name}}',$merchant_name ,$message));
							   $message = stripslashes(str_ireplace('{{merchant_email}}',$config_email ,$message ));
     						   $message = stripslashes(str_ireplace('{{merchant_phone}}',$phoneNumber ,$message ));
    
                     $logo_url ='';
		   $config_data = $this->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
		   if(!empty($config_data))
		   $logo_url  = $config_data['ProfileImage']; 
           
	      if( !empty( $config_data['ProfileImage'])){   $logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  }else{  $logo_url = CZLOGO; }
    
    		$logo_url=	"<img src='".$logo_url."' />";
        $message = stripslashes(str_ireplace('{{logo}}',$logo_url ,$message ));
    
    
    
		if($view_data['replyTo'] != ''){
			$replyTo = $view_data['replyTo'];
		}else{
			$replyTo = $res['merchantEmail'];
		}
		$tr_date   = date('Y-m-d H:i:s');
		$email_data          = array(
				'customerID'=>$customerID,
				'merchantID'=>$user_id, 
				'emailSubject'=>$subject,
				'emailfrom'=>$config_email,
				'emailto'=>$toEmail,
				'emailcc'=>$addCC,
				'emailbcc'=>$addBCC,
				'emailreplyto'=>$replyTo,
				'emailMessage'=>$message,
				'emailsendAt'=>$tr_date,
				
				);


		
		
		$mail_sent = $this->sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo, $addCC, $addBCC, $attachments);
		if($mail_sent)
		{
			$email_data['send_grid_email_id'] = $mail_sent;
			$email_data['send_grid_email_status'] = 'Sent';
			$this->insert_row('tbl_template_data', $email_data); 
		}
		else
		{
			$email_data['send_grid_email_status'] = 'Failed';
			$email_data['mailStatus']=0;
			$this->insert_row('tbl_template_data', $email_data); 
		}
								
	                            
	
	
	}
	
	public  function payment_send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   )
	{
		if(empty($toEmail) || !filter_var($toEmail, FILTER_VALIDATE_EMAIL)){
		      return false;
		}
		$companyMercName = '';
		if ($this->session->userdata('logged_in') != "") {
			$sessionData = $this->session->userdata('logged_in');
			$companyMercName = $sessionData['companyName'];
			$merchantID = $sessionData['merchID'];
			$merchantEmail 	= $sessionData['merchantEmail'];
			$resellerID = $sessionData['resellerID'];
		} 
		if ($this->session->userdata('user_logged_in') != "") {
			$sessionData = $this->session->userdata('user_logged_in');
			$merchantID = $sessionData['merchantID'];
		}
		// Convert added date in timezone 
        if(isset($sessionData['merchant_default_timezone']) && !empty($sessionData['merchant_default_timezone'])){
          	$timezone = ['time' => $tr_date, 'current_format' => 'UTC', 'new_format' => $sessionData['merchant_default_timezone']];
          	$new_time = getTimeBySelectedTimezone($timezone);
          	$tr_date = date('Y-m-d h:i A', strtotime($new_time));

        }

		$plantype_es = $this->general_model->chk_merch_plantype_es($merchantID);
		if($plantype_es){
$phoneNumber='';
	        $user_id = $merchantID;
			$res =   $this->get_select_data('tbl_merchant_data',array('merchantEmail','companyName','firstName','lastName','merchantContact', 'resellerID'), array('merchID'=> $merchantID));
			$resellerID = $res['resellerID'];
			$merchantEmail = $res['merchantEmail'];
			$companyMercName = $res['companyName'];

		   	$condition_mail1 =  array('templateType' => '15', 'merchantID' => $merchantID);
			  $view_data      = $this->template_data($condition_mail1);	
			  if(!$view_data){
				  return false;
			  }
			  
				$isValid = is_a_valid_customer([
					'email' => $toEmail
				]);

				if(!$isValid){
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Customer is not synced with active merchant</strong></div>');
					return false;
				}
			
				$merchant_name=	($res['companyName'])?$res['companyName']:$res['firstName'].''.$res['lastName'];	  
							  $subject = $view_data['emailSubject'];
							  $fromEmail = $view_data['fromEmail'];
							$phoneNumber=$res['merchantContact'];
							$addCC      = $view_data['addCC'];
							$addBCC		= $view_data['addBCC'];
  
								if($view_data['fromEmail'] == ''){
									$fromEmail = 'donotreply@payportal.com';
								}
								$config_email =	$res['merchantEmail'];
							 if($view_data['templateType'] == '15'){
								 $fromEmail = 'donotreply@payportal.com';
								 $toEmail = $merchantEmail;
							 }	
							 $toEmail = $merchantEmail;
							  $in_link='';
							   $message = $view_data['message'];
						
							   $message = stripslashes(str_ireplace('{{merchant_company_name}}',$companyMercName ,$message ));
							   $message = stripslashes(str_ireplace('{{customer.company}}',$company ,$message));
							   $message = stripslashes(str_ireplace('{{customer_contact_name}}',$customer ,$message ));
							   $message = stripslashes(str_ireplace('{{customer.name}}',$customer ,$message ));
							   $message =	stripslashes(str_ireplace('{{invoice.refnumber}}',$ref_number, $message));
								$message =	stripslashes(str_ireplace('{{invoice_payment_pagelink}}',$in_link, $message));
							   $message = stripslashes(str_ireplace('{{transaction.amount}}',($amount)?('$'.number_format($amount,2)):'0.00' ,$message )); 
							   $message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message ));
							   $message = stripslashes(str_ireplace('{{merchant_name}}',$merchant_name ,$message));
							   $message = stripslashes(str_ireplace('{{merchant_email}}',$config_email ,$message ));
     						   $message = stripslashes(str_ireplace('{{merchant_phone}}',$phoneNumber ,$message ));
    
                     $logo_url ='';
		   $config_data = $this->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
		   if(!empty($config_data))
		   $logo_url  = $config_data['ProfileImage']; 
           
	      if( !empty( $config_data['ProfileImage'])){   $logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  }else{  $logo_url = CZLOGO; }
    
    		$logo_url=	"<img src='".$logo_url."' />";
		$message = stripslashes(str_ireplace('{{logo}}',$logo_url ,$message ));
		if($view_data['replyTo'] != ''){
			$replyTo = $view_data['replyTo'];
		}else{
			$replyTo = $res['merchantEmail'];
		}
    		$tr_date   = date('Y-m-d H:i:s');
			$email_data          = array(
					'customerID'=>$customerID,
					'merchantID'=>$user_id, 
					'emailSubject'=>$subject,
					'emailfrom'=>$fromEmail,
					'emailto'=>$toEmail,
					'emailcc'=>$addCC,
					'emailbcc'=>$addBCC,
					'emailreplyto'=>$replyTo,
					'emailMessage'=>$message,
					'emailsendAt'=>$tr_date,
					
					);
					
			
		
			
			$mail_sent = $this->sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo, $addCC, $addBCC);
			if($mail_sent)
			{
				$email_data['send_grid_email_id'] = $mail_sent;
				$email_data['send_grid_email_status'] = 'Sent';
				$this->insert_row('tbl_template_data', $email_data); 
			}
			else
			{
				$email_data['send_grid_email_status'] = 'Failed';
				$email_data['mailStatus']=0;
				$this->insert_row('tbl_template_data', $email_data); 
			}
			
		}
	    else{
			return true;
		}
	                            
	
	
	}
	public  function send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,$type   )
	{
	    
	            $user_id = $merchantID;
	           $res =   $this->get_select_data('tbl_merchant_data',array('merchantEmail','companyName','firstName','lastName','merchantContact'), array('merchID'=>$user_id));
	
					  
				$merchant_name=	($res['companyName'])?$res['companyName']:$res['firstName'].''.$res['lastName']	;	 
				$fromEmail = 'donotreply@payportal.com';	
    	$phoneNumber=$res['merchantContact'];	
    
    			  $logo_url ='';
		   $config_data = $this->general_model->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
		   if(!empty($config_data))
		   $logo_url  = $config_data['ProfileImage']; 

		   	$isValid = is_a_valid_customer([
				'email' => $toEmail
			]);

			if(!$isValid){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Customer is not synced with active merchant</strong></div>');
				return false;
			}
           
		   
	      if( !empty( $config_data['ProfileImage'])){   $logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  }else{  $logo_url = CZLOGO; }  
    
    $logo_url=	"<img src='".$logo_url."' width='".LOGOWIDTH."' height='".LOGOHEIGHT."' />";
				$in_link='';
                if($type=='capture')
                {
        			  $subject ="Capture Transaction #".$ref_number;
        			  $message= "<h5>Dear $customer,</h5><br><p>We would like to inform that your authorised amount $ $amount has settled and it will be credit within two or five working days.</p><br>
                        If you have any questions or concerns, please do not hesitate to call. <br>
                        We thank you for your business and look forward to continuing to work with you!<br><br>
                        Warm Regards<br>$merchant_name <br>$fromEmail <br>$logo_url";
                }
			  else
			  {
        			   $subject ="Void Transaction #".$ref_number;
        			   $message="<h5>Dear $customer,</h5><br><p>We would like to inform that your authorised amount $ $amount will be discarded within two or five working days</p><br>
                        If you have any questions or concerns, please do not hesitate to call. <br>
                        We thank you for your business and look forward to continuing to work with you!<br><br>
                       Warm Regards<br>$merchant_name <br>$fromEmail <br>$logo_url";
			  }
					     
						
							   
							  if($view_data['replyTo'] != ''){
								$replyTo = $view_data['replyTo'];
							}else{
								$replyTo = $res['merchantEmail'];
							} 
							   
							  $addBCC=$addCC='';
							  $tr_date   = date('Y-m-d H:i:s');
							   $email_data          = array(
			                            'customerID'=>$customerID,
										'merchantID'=>$user_id, 
										'emailSubject'=>$subject,
										'emailfrom'=>$config_email,
										'emailto'=>$toEmail,
										'emailcc'=>$addCC,
										'emailbcc'=>$addBCC,
										'emailreplyto'=>$replyTo,
										'emailMessage'=>$message,
										'emailsendAt'=>$tr_date,
										
										);
							  
							   
								
								$mail_sent = $this->sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo);
								if($mail_sent)
								{
									$email_data['send_grid_email_id'] = $mail_sent;
									$email_data['send_grid_email_status'] = 'Sent';
								    $this->insert_row('tbl_template_data', $email_data); 
								}
								else
								{
									$email_data['send_grid_email_status'] = 'Failed';
								    $email_data['mailStatus']=0;
								    $this->insert_row('tbl_template_data', $email_data); 
								}
								
	                          
	                            
	
	
	}
	
    
	
	public function set_next_date($paycycle,$date, $in_num,$proRate,$proRateday)
	{
					
						$next_date ='';
						  if($paycycle=='dly'){
							 $in_num   = ($in_num)? $in_num:'0';
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
						 }
						 if($paycycle=='1wk'){
							 $in_num   = ($in_num)? $in_num:'0';
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
						 }		 
						
						if($paycycle=='2wk'){
							 $in_num   = ($in_num)?$in_num + 1 :'0' ;
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
						 }
						if($paycycle=='mon')
						{
						    
						    
						     if($proRate==1)
						     {
						         
						         $in_num   = ($in_num)? $in_num:'0';
						         $in_num   = $in_num  +1;
							    $next_date =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));  
						     }
						    else
						    {
						         $in_num   = ($in_num)? $in_num:'0';
						         if($proRateday=='31')
						         $proRateday = date("t");
						         else if($proRateday=='30' || $proRateday=='29' || $proRateday=='28'  )
						         {
						              if(strtoupper(date("M"))=="JAN")
						               $proRateday  = date("t");
						         }else{
						             $proRateday=1;
						         }
						         
							    $next_date =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));  
                             
                              
						    }
                            
                     
						 }
						if($paycycle=='2mn'){
						    
							 $in_num   = ($in_num)?$in_num + 2*1:'0';
							$next_date =  strtotime(date("Y-m-01", strtotime($date)) . " +$in_num month");
						 }		 
						 if($paycycle=='qtr'){
							 $in_num   = ($in_num)?$in_num + 3*1:'0';
							$next_date =  strtotime(date("Y-m-01", strtotime($date)) . " +$in_num month");
						 }
						if($paycycle=='six'){
							 $in_num   = ($in_num)?$in_num + 6*1:'0';
							$next_date = date('Y-m-01', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }
						if($paycycle=='yrl'){
							 $in_num   = ($in_num)?$in_num + 12*1:'0';
							$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }	

						if($paycycle=='2yr'){
							 $in_num   = ($in_num)?$in_num + 2*12:'0';
							$next_date =  date('Y-m-01',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }
						if($paycycle=='3yr'){
							 $in_num   = ($in_num)?$in_num + 3*12:'0';
							$next_date =  date('Y-m-01', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }						 
							

						return $next_date;		
							
	}
	
	
	
	public function get_recurring_data($con)
	{
	         $ress =array();
	         $res_data=array() ; 
	          $app_data = $this->get_row_data('app_integration_setting',array('merchantID'=>$con['merchantID']));
	          
	          $this->db->select('r.*');
	          $this->db->from('tbl_recurring_payment r');
	          $this->db->where($con);
	          $query = $this->db->get();
	         
	          if($query->num_rows() > 0)
	          {
	              
	              $ress = $query->result_array();
	              
	          }
	          
	      
	        if(!empty($ress))  
	        {
	          foreach($ress as $res)
	          {
	              $pl='';
	           	
	              if($res['optionData']==1) 
	              {
	                  $pl = "On Due Date";
	              }else if($res['optionData']==2) 
	              {
	                  $pl = "On Invoice Date";
	              }else if($res['optionData']==3) 
	              {
	                  $pl = "1 Day After Invoice Date";
	              }else if($res['optionData']==4) 
	              {
	                  $pl = "Calendar Date";
	              }else{
	              	  $pl = "None";
	              }
	              
    	            $res['plan'] = $pl;
    	            $res_data[]  = $res;
	              
	          }
	          
	        }   
	          
	        return $res_data;
	            
	
	}
	
	
	
	public function check_refund_transaction_amount($tID)
	{
	    
	    $this->db->select('tr.transactionID, tr.transactionType, tr.transactionAmount, sum(r.refundAmount) as ref ');
	    $this->db->from('customer_transaction tr');
	    $this->db->join('tbl_customer_refund_transaction r','r.creditTransactionID=tr.transactionID','INNER');
	    $this->db->where_in('tr.transactionCode',array('200','1','100','111'));
	    $this->db->where('tr.transactionID',$tID);
	    $this->db->group_by('r.creditTransactionID');
	    
	    $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {
	        $res = $query->row_array();
	        
	        $rem = $res['transactionAmount']-$res['ref'];
	        if($rem > 0)
	        return true;
	        else
	        return false;
	        
	    }
	    else
	    {
	        return true;
	    }
	    
	    
	}


       public function insert_inv_number($mID)
   
   {
   
               $inv_data['merchantID'] = $mID;
               $inv_data['prefix']     ='CZ';
            	$inv_data['postfix']   = '10000';
                $inv_data['invoiceNo'] = 'CZ-'.'10000';
   				$this->insert_row('tbl_merchant_invoices',$inv_data);
   
   }
   public function insert_payterm($insert)
   {
               $pay_terms = array(
   array(
      'pt_name' => 'Due on Receipt' ,
      'pt_netTerm' => 0 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
  array(
      'pt_name' => 'Net 10' ,
      'pt_netTerm' => 10 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
                array(
      'pt_name' => 'Net 15' ,
      'pt_netTerm' => 15 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
                array(
      'pt_name' => 'Net 30' ,
      'pt_netTerm' => 30 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
);
   $this->db->insert_batch('tbl_payment_terms', $pay_terms); 
   
   }  

   public function get_merchant_data($condition)
   {
   
   		$res=array();
        $this->db->select('m.merchID, m.firstName, m.lastName, m.merchantEmail,m.merchantContact,m.resellerID,r.ProfileURL,r.resellerProfileURL, r.resellerCompanyName, m.merchantProfileURL,m.companyName');
   		$this->db->from('tbl_merchant_data m');
   		$this->db->join('tbl_reseller r','r.resellerID=m.resellerID','inner');
   		$this->db->where($condition);
   		$query = $this->db->get();
   		if($query->num_rows() > 0)
 	   {
          $res = $query->row_array();
       }
   
   		return $res;
   	}
   

// To generate PDF file   
 
   public function generate_invoice_pdf($customer_data,$invoice_data,$invoice_items,$mode)
   {
        
   	 if($this->session->userdata('logged_in')){
		$da['login_info'] 	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$da['login_info'] 	= $this->session->userdata('user_logged_in');
		
		$user_id 				= $da['login_info']['merchantID'];
		}	

		$condition4 			= array('merchID'=>$user_id );
		$company_data			= $this->get_select_data('tbl_merchant_data',array('firstName','lastName','companyName','merchantAddress1','merchantEmail','merchantContact','merchantAddress2','merchantCity','merchantState','merchantCountry','merchantZipCode'),$condition4);
		$config_data			= $this->get_select_data('tbl_config_setting',array('ProfileImage','serviceurl'),array('merchantID'=>$user_id ));
		$czlogo = CZLOGO;
		if($config_data['ProfileImage']!="")
			$logo = FCPATH .'uploads/merchant_logo/'. $config_data['ProfileImage'];
		else
			$logo  = $czlogo;
        $invoiceID = $invoice_data['invoiceID'].'-'.$invoice_data['invoiceNumber'];

        $no        = $invoiceID;
        $path = '';
     
        if($mode=='D')
        {
	     	$pdfFilePath = "$no.pdf";   
        }
         if($mode=='F')
        {
              $path        = FCPATH.'uploads/invoice_pdf/';
              $pdfFilePath = $path."$no.pdf";   
        }
	        
	       
	        
	      if($invoice_data['type']==1)
	      $payamount = $invoice_data['AppliedAmount']- $invoice_data['Balance'];
	      if($invoice_data['type']==2||$invoice_data['type']==5)
	      $payamount = $invoice_data['AppliedAmount'];
	      
			
	  ini_set('memory_limit','320M'); 
	  $this->load->library("TPdf");
			
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);   
	if(!defined('PDF_HEADER_TITLE')){
		define(PDF_HEADER_TITLE,'Invoice');
	} 

	if(!defined('PDF_HEADER_STRING')){
		define(PDF_HEADER_STRING,'');
	} 
    $pdf->SetPrintHeader(False);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Chargezoom 1.0');
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  
    $pdf->SetFont('dejavusans', '', 10, '', true);   
  // set cell padding
    $pdf->setCellPaddings(1, 1, 1, 1);

    // set cell margins
	 $pdf->setCellMargins(1, 1, 1, 1);
    // Add a page
  
        $pdf->AddPage();
  
   
		$y = 20;
		$logo_div='<div style="text-align:left; float:left ">
		<img src="'.$logo.'"  border="0" height="50"/>
		</div>';


		// set color for background
		$pdf->SetFillColor(255, 255, 255);
		// write the first column
		$pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);
		
	  if($company_data['companyName'] != '')
	    $tt = $company_data['companyName']."<br/>";
	   
	    if($company_data['merchantAddress1'] != '')
	        $tt .= ($company_data['merchantAddress1'])."<br/>";
	  
	    if($company_data['merchantAddress2'] != '')
	         $tt .= ($company_data['merchantAddress2'])."<br/>";
	     
	       if($company_data['merchantCity'] != '' || $company_data['merchantState'] != '' || $company_data['merchantZipCode'])
        	  $tt .= ($company_data['merchantCity']).", ".($company_data['merchantState'])." ".($company_data['merchantZipCode']).'<br/>';
        	  
        	    if($company_data['merchantCountry'] != '')
	                 $tt .= ($company_data['merchantCountry']); 
	

	    $y = 50;
		// set color for background
		$pdf->SetFillColor(255, 255, 255);

		// set color for text
		$pdf->SetTextColor(51, 51, 51);

		// write the first column
		$pdf->writeHTMLCell(80, 30, '', $y, $tt, 0, 0, 1, true, 'J', true);
		// set color for background
	
		// set color for text
				$pdf->SetTextColor(51, 51, 51);
        if($invoice_data['invoiceNumber'] != '')
        {
           $invoice = $invoice_data['invoiceNumber']."<br/><br/>" ;
        }
        
       if($invoice_data['DueDate'] != ''){
       
          $date = date("m/d/Y",strtotime($invoice_data['DueDate']));
        }
        
		$pdf->writeHTMLCell(80, 30, '', '', '<b>Invoice: </b>'. $invoice .'<b>Due Date: </b>'.$date ,0, 1, 1, true, 'J', true);
		
		$lll ='';

		$BillingAdd = 0;
        $ShippingAdd = 0;

        $isAdd = 0;
        if($invoice_data['baddress1']  || $invoice_data['baddress2'] || $invoice_data['bcity'] || $invoice_data['bstate'] || $invoice_data['bzipcode'] || $invoice_data['country']){
            $BillingAdd = 1;
            $isAdd = 1;
        }
        if($invoice_data['saddress1']  || $invoice_data['saddress2'] || $invoice_data['scity'] || $invoice_data['sstate'] || $invoice_data['szipcode'] ){
            $ShippingAdd = 1;
            $isAdd = 1;
        }


        if($BillingAdd){
        	if($customer_data['FirstName']!='')
            {
				$lastName = '';
				if($customer_data['LastName'] != ''){
					$lastName = $customer_data['LastName'];
				}
			    $lll.=$customer_data['FirstName'].' '.$lastName.'<br/>';
            }
	        if($invoice_data['baddress1']!=''){
	            
              $lll.= $invoice_data['baddress1'].'<br>';
	            
	        } 
					        
	        if($invoice_data['baddress2']!=''){  
              $lll.= $invoice_data['baddress2'].'<br>';
	        } 
	        if($invoice_data['bcity'] !='' || $invoice_data['bstate'] !='' || $invoice_data['bzipcode'] !='')
	        {
							      
				$lll.=($invoice_data['bcity'])?$invoice_data['bcity'].', ':'';
				$lll.=($invoice_data['bstate'])?$invoice_data['bstate'].' ':''; 
				$lll.=($invoice_data['bzipcode'])?$invoice_data['bzipcode']:'';
				$lll.='<br>';
			}
        }
						
					        
							  
							 
							  
							  
							 
				$lll_ship='';
				
			
        if($ShippingAdd){
        	if($customer_data['FirstName']!='')
			{
			   $lastName = '';
			   if($customer_data['LastName'] != ''){
				   $lastName = $customer_data['LastName'];
			   }
			   $lll_ship.=$customer_data['FirstName'].' '.$lastName.'<br/>';
			}
			if($invoice_data['saddress1']!='')
			{
                $lll_ship.=$invoice_data['saddress1']; 
			    $lll_ship.='<br>';
			}
			
			if($invoice_data['saddress2']!='')
			{
			   $lll_ship.=$invoice_data['saddress2']; 
          
		       $lll_ship.='<br>';  
			}
					
				
		 	if($invoice_data['scity']!='' || $invoice_data['sstate']!='' ||  $invoice_data['szipcode']!='')
		 	{		 
                    $lll_ship.=($invoice_data['scity'])?$invoice_data['scity'].', ':''; 
                    $lll_ship.=($invoice_data['sstate'])?$invoice_data['sstate'].' ':'';
                    $lll_ship.=($invoice_data['szipcode'])?$invoice_data['szipcode']:'';
				    $lll_ship.='<br>';
		 	}

        }          
				
                  
                  
							
				


$y = $pdf->getY();
		// write the first column
		if($BillingAdd){
			$pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>".$lll.'<br/>', 0, 0, 1, true, 'J', true);
			
		}else{
			$pdf->writeHTMLCell(80, 0, '', $y, "<b></b><br/><br/>", 0, 0, 1, true, 'J', true);
		}
		$pdf->SetTextColor(51, 51, 51);
		if($ShippingAdd){
			$pdf->writeHTMLCell(80, 0, '', '', '<b>Shipping Address</b>:<br/>'.$lll_ship."<br/>",0, 1, 1, true, 'J', true);
		}else{
			$pdf->writeHTMLCell(80, 0, '', '', "<b></b><br/><br/>",0, 1, 1, true, 'J', true);
		}
		$pdf->setCellPaddings(1, 1, 1, 1);
		$pdf->setCellMargins(0, 1, 0, 0);
		
		

// set cell margins
		
$html= "\n\n\n".'<h3></h3><table style="border: 1px solid #7f7f7f;" cellpadding="4"  >
	<tr style="border: 1px solid #7f7f7f;background-color:#3f3f3f;color:#FFFFFF;">
       
        <th style="border: 1px solid #7f7f7f;"colspan="2" align="center"><b>Product/Services</b></th>
        <th style="border: 1px solid #7f7f7f;" align="center"><b>Qty</b></th>
		  <th style="border: 1px solid #7f7f7f;" align="center"><b>Unit Rate</b></th>
		    <th style="border: 1px solid #7f7f7f;" align="center"><b>Amount</b></th>
        
    </tr>';
     $html1='';$total_val =0;
						$totaltax =0 ; $total=0; $tax=0; 
						foreach($invoice_items as $key=>$item)
						{
							$item['Description'] = str_replace("<","< ",$item['Description']);
							$item['Description'] = str_replace(">","> ",$item['Description']);
							$item['itemName'] = str_replace("<","< ",$item['itemName']);
							$item['itemName'] = str_replace(">","> ",$item['itemName']);

						$total+=  $item['Quantity']*$item['itemPrice']; 
				   
                    $html.='<tr style="border: 1px solid #7f7f7f;">
                       
						<td  style="border: 1px solid #7f7f7f;" colspan="2">'.$item['itemName'].' - '.$item['Description'].'</td>
                        <td  style="border: 1px solid #7f7f7f;"  align="center" >'.$item['Quantity'].'</td>
                        <td  style="border: 1px solid #7f7f7f;" align="right">'.number_format($item['itemPrice'],2).'</td>
						<td  style="border: 1px solid #7f7f7f;" align="right">'.number_format($item['Quantity']*$item['itemPrice'] , 2).'</td>
                    </tr>';
						} 
					$total_val =$total+$invoice_data['TotalTax'];
					$total_tax = ($invoice_data['TotalTax'])?$invoice_data['TotalTax']:0;
					$trate = ($invoice_data['TaxRate'])?$invoice_data['TaxRate']:'0.00';
					$html.= '<tr><td colspan="4" align="right">Subtotal</td><td style="border: 1px solid #7f7f7f;" align="right">'.number_format($total,2).'</td></tr>';
					$html.= '<tr><td style="border: 1px solid #7f7f7f;" colspan="4" align="right">Tax '.$trate.'%</td><td style="border: 1px solid #7f7f7f;" align="right">'.number_format($total_tax,2).'</td></tr>';
					$html.= '<tr><td style="border: 1px solid #7f7f7f;" colspan="4" align="right">Total</td><td style="border: 1px solid #7f7f7f;"  align="right">'.number_format($total_val,2).'</td></tr>';	
					$html.= '<tr><td style="border: 1px solid #7f7f7f;" colspan="4" align="right">Payment</td><td style="border: 1px solid #7f7f7f;" align="right">'.number_format($payamount,2).'</td></tr>';	
					$html.= '<tr><td   colspan="4" align="right">Balance</td><td align="right">'.number_format(($invoice_data['Balance']),2).'</td></tr> ';					
	
$email1 = ($company_data['merchantEmail'])?$company_data['merchantEmail']:'#';

$html.='</table>';
$url = base_url();
$info = parse_url($url);
$host = $info['host'];
$host_names = explode(".", $host);

if(isset($host_names[count($host_names)-2]) && $host_names[count($host_names)-1]){
  $terms_host_name = 'https://'.$host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1].'/terms';
  $pp_host_name = 'https://'.$host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1].'/privacy-policy';
}else{
  $terms_host_name = base_url().'../terms';
  $pp_host_name = base_url().'../privacy-policy';
}
if(isset($config_data['serviceurl']) && !empty($config_data['serviceurl'])){
	$terms_host_name = $config_data['serviceurl'];
}

$html.='<div>
	<div>Payments powered by Chargezoom</div>
	<div>Terms of Service - <a target="_blank" href="'.$terms_host_name.'">'.$terms_host_name.'</a></div>
	<div>Privacy Policy - <a target="_blank" href="'.$pp_host_name.'">'.$pp_host_name.'</a></div>
</div>';
$pdf->writeHTML($html, true, false, true, false, '');


    // ---------------------------------------------------------    
  
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    
    
    $pdf->Output($pdfFilePath, $mode);  
    if($mode=='F')
    return true;
    
    
}
public function generate_invoice_pdf_qbd($customer_data,$invoice_data,$invoice_items,$mode)
{
	 
	 if($this->session->userdata('logged_in')){
	 $da['login_info'] 	= $this->session->userdata('logged_in');
	 
	 $user_id 				= $da['login_info']['merchID'];
	 }
	 if($this->session->userdata('user_logged_in')){
	 $da['login_info'] 	= $this->session->userdata('user_logged_in');
	 
	 $user_id 				= $da['login_info']['merchantID'];
	 }	

	 $condition4 			= array('merchID'=>$user_id );
	 $company_data			= $this->get_select_data('tbl_merchant_data',array('firstName','lastName','companyName','merchantAddress1','merchantEmail','merchantContact','merchantAddress2','merchantCity','merchantState','merchantCountry','merchantZipCode'),$condition4);
	 $config_data			= $this->get_select_data('tbl_config_setting',array('ProfileImage','serviceurl'),array('merchantID'=>$user_id ));
	 $czlogo = CZLOGO;
	 
	 if($config_data['ProfileImage']!="")
	 	$logo = FCPATH .'uploads/merchant_logo/'. $config_data['ProfileImage'];
	 else
		$logo  = $czlogo;
	 $invoiceID = $invoice_data['invoiceID'].'-'.$invoice_data['invoiceNumber'];
	 $no        = $invoiceID;
	 $path = '';
  
	 if($mode=='D')
	 {
		  $pdfFilePath = "$no.pdf";   
	 }
	  if($mode=='F')
	 {
		   $path        = FCPATH.'uploads/invoice_pdf/';
		   $pdfFilePath = $path."$no.pdf";   
	 }
		 
		
		 
	   if($invoice_data['type']==1)
	   $payamount = $invoice_data['AppliedAmount']- $invoice_data['Balance'];
	   if($invoice_data['type']==2||$invoice_data['type']==5)
	   $payamount = $invoice_data['AppliedAmount'];
	   
		 
   ini_set('memory_limit','320M'); 
   $this->load->library("TPdf");
		 
 $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
 define(PDF_HEADER_TITLE,'Invoice');
 define(PDF_HEADER_STRING,'');
 $pdf->SetPrintHeader(False);
 $pdf->SetCreator(PDF_CREATOR);
 $pdf->SetAuthor('Chargezoom 1.0');
 $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

 $pdf->SetFont('dejavusans', '', 10, '', true);   
// set cell padding
 $pdf->setCellPaddings(1, 1, 1, 1);

 // set cell margins
  $pdf->setCellMargins(1, 1, 1, 1);
 // Add a page

	 $pdf->AddPage();


	 $y = 20;
	 $logo_div='<div style="text-align:left; float:left ">
	 <img src="'.$logo.'"  border="0" height="50"/>
	 </div>';


	 // set color for background
	 $pdf->SetFillColor(255, 255, 255);
	 // write the first column
	 $pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);
	 
   
   if($company_data['companyName'] != '')
	 $tt = $company_data['companyName']."<br/>";
	
	 if($company_data['merchantAddress1'] != '')
		 $tt .= ($company_data['merchantAddress1'])."<br/>";
   
	 if($company_data['merchantAddress2'] != '')
		  $tt .= ($company_data['merchantAddress2'])."<br/>";
	  
		if($company_data['merchantCity'] != '' || $company_data['merchantState'] != '' || $company_data['merchantZipCode'])
		   $tt .= ($company_data['merchantCity']).", ".($company_data['merchantState'])." ".($company_data['merchantZipCode']).'<br/>';
		   
			 if($company_data['merchantCountry'] != '')
				  $tt .= ($company_data['merchantCountry']); 
 

	 $y = 50;
	 // set color for background
	 $pdf->SetFillColor(255, 255, 255);

	 // set color for text
	 $pdf->SetTextColor(51, 51, 51);

	 // write the first column
	 $pdf->writeHTMLCell(80, 30, '', $y, $tt, 0, 0, 1, true, 'J', true);

	 // set color for background

	 // set color for text
			 $pdf->SetTextColor(51, 51, 51);
	 if($invoice_data['invoiceNumber'] != '')
	 {
		$invoice = $invoice_data['invoiceNumber']."<br/><br/>" ;
	 }
	 
	if($invoice_data['DueDate'] != ''){
	
	   $date = date("m/d/Y",strtotime($invoice_data['DueDate']));
	 }
	 
	 $pdf->writeHTMLCell(80, 30, '', '', '<b>Invoice: </b>'. $invoice .'<b>Due Date: </b>'.$date ,0, 1, 1, true, 'J', true);
	 
	 $lll ='';
	 $BillingAdd = 0;
    $ShippingAdd = 0;

    $isAdd = 0;
    if($invoice_data['baddress1']  || $invoice_data['baddress2'] || $invoice_data['bcity'] || $invoice_data['bstate'] || $invoice_data['bzipcode'] || $invoice_data['country']){
        $BillingAdd = 1;
        $isAdd = 1;
    }
    if($invoice_data['saddress1']  || $invoice_data['saddress2'] || $invoice_data['scity'] || $invoice_data['sstate'] || $invoice_data['szipcode'] ){
        $ShippingAdd = 1;
        $isAdd = 1;
    }

	$customerName = '';
	if (isset($customer_data['FirstName']) && $customer_data['FirstName'] != '') {
		$customerName = $customer_data['FirstName'] . ' ' . $customer_data['LastName'];
	} else if (isset($invoice_data['Customer_FullName']) && $invoice_data['Customer_FullName'] != ''){
		$customerName = $invoice_data['Customer_FullName'];
	}

	$customerPhone = '';
	if (isset($customer_data['PhoneNumber']) && $customer_data['PhoneNumber'] != ''){
		$customerPhone = $customer_data['PhoneNumber'];
	}

	$customerEmail = '';
	if (isset($customer_data['customerEmail']) && $customer_data['customerEmail'] != ''){
		$customerEmail = $customer_data['customerEmail'];
	}
 
	if($BillingAdd){
		$lll.= "$customerName<br>";
		$lll.=($invoice_data['baddress1'])?$invoice_data['baddress1'].'<br>':'';
		$lll.=($invoice_data['baddress2'])?$invoice_data['baddress2'].'<br>':'';
		$lll.=($invoice_data['bcity'])?$invoice_data['bcity'].', ':'';
		$lll.=($invoice_data['bstate'])?$invoice_data['bstate'].' ':''.' '; 
		$lll.=($invoice_data['bzipcode'])?$invoice_data['bzipcode']:'';
		$lll.='<br>';
		$lll.=(isset($invoice_data['country']) && $invoice_data['country'])?$invoice_data['country']:'';
	
		$lll.= "<span>&#9742;<span>:  $customerPhone<br>";
		$lll.= "<span>&#9993;<span>:  $customerEmail<br>";
	}				 
					 
						  
						 
						   
						  
						   
						   
				
			 $lll_ship='';
			 
	if($ShippingAdd){ 
		$lll_ship.= "$customerName<br>";
		$lll_ship.=($invoice_data['saddress1'])?$invoice_data['saddress1'].'<br>':'';
		$lll_ship.=($invoice_data['saddress2'])?$invoice_data['saddress2'].'<br>':'';
		$lll_ship.=($invoice_data['scity'])?$invoice_data['scity'].', ':'';
		$lll_ship.=($invoice_data['sstate'])?$invoice_data['sstate'].' ':''.' '; 
		$lll_ship.=($invoice_data['szipcode'])?$invoice_data['szipcode']:'';
		$lll_ship.='<br>';
		$lll_ship.=(isset($invoice_data['scountry']) && $invoice_data['scountry'])?$invoice_data['scountry']:'';
	
		$lll_ship.= "<span>&#9742;<span>:  $customerPhone<br>";
		$lll_ship.= "<span>&#9993;<span>:  $customerEmail<br>";
	}
			   
			 
			   
			   
						 
						  
				


$y = $pdf->getY();
	 // write the first column
	
	
	 if($BillingAdd){
		$pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>".$lll.'<br/>', 0, 0, 1, true, 'J', true);
		
	}else{
		$pdf->writeHTMLCell(80, 0, '', $y, '<b></b><br/><br/>', 0, 0, 1, true, 'J', true);
	}
	$pdf->SetTextColor(51, 51, 51);
	if($ShippingAdd){
		$pdf->writeHTMLCell(80, 0, '', '', '<b>Shipping Address</b>:<br/>'.$lll_ship."<br/>",0, 1, 1, true, 'J', true);
	}else{
		$pdf->writeHTMLCell(80, 0, '', '', '<b></b><br/><br/>',0, 1, 1, true, 'J', true);
	}
	
	 
$pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
	 $pdf->setCellMargins(0, 1, 0, 0);
$html= "\n\n\n".'<h3></h3><table style="border: 1px solid #7f7f7f;" cellpadding="4"  >
	<tr style="border: 1px solid #7f7f7f;background-color:#3f3f3f;color:#FFFFFF;">
	 <th style="border: 1px solid #7f7f7f;"colspan="2" align="center"><b>Product/Services</b></th>
	 <th style="border: 1px solid #7f7f7f;" align="center"><b>Qty</b></th>
	   <th style="border: 1px solid #7f7f7f;" align="center"><b>Unit Rate</b></th>
		 <th style="border: 1px solid #7f7f7f;" align="center"><b>Amount</b></th>
	 
 </tr>';
  $html1='';$total_val =0;
					 $totaltax =0 ; $total=0; $tax=0; 
					 
					 foreach($invoice_items as $key=>$item)
					 {
						$item['Description'] = str_replace("<","< ",$item['Description']);
						$item['Description'] = str_replace(">","> ",$item['Description']);
						$item['itemName'] = str_replace("<","< ",$item['itemName']);
						$item['itemName'] = str_replace(">","> ",$item['itemName']);
					 $total+=  $item['Quantity']*$item['itemPrice']; 
				
				 $html.='<tr style="border: 1px solid #7f7f7f;">
					
				 	 <td  style="border: 1px solid #7f7f7f;" colspan="2">'.$item['itemName'].' - '.$item['Description'].'</td>
					 <td  style="border: 1px solid #7f7f7f;"  align="center" >'.$item['Quantity'].'</td>
					 <td  style="border: 1px solid #7f7f7f;" align="right">'.number_format($item['itemPrice'],2).'</td>
					 <td  style="border: 1px solid #7f7f7f;" align="right">'.number_format($item['Quantity']*$item['itemPrice'] , 2).'</td>
				 </tr>';
					 } 
					 $total_val =$total+$invoice_data['TotalTax'];
					 $total_tax = ($invoice_data['TotalTax'])?$invoice_data['TotalTax']:0;
					 $trate = ($invoice_data['TaxRate'])?$invoice_data['TaxRate']:'0.00';
					 $html.= '<tr><td colspan="4" align="right">Subtotal</td><td style="border: 1px solid #7f7f7f;" align="right">'.number_format($total,2).'</td></tr>';	
					 $html.= '<tr><td style="border: 1px solid #7f7f7f;" colspan="4" align="right">Tax '.$trate.'%</td><td style="border: 1px solid #7f7f7f;" align="right">'.number_format($total_tax,2).'</td></tr>';
					$html.= '<tr><td style="border: 1px solid #7f7f7f;" colspan="4" align="right">Total</td><td style="border: 1px solid #7f7f7f;"  align="right">'.number_format($total_val,2).'</td></tr>';	
			$html.= '<tr><td style="border: 1px solid #7f7f7f;" colspan="4" align="right">Payment</td><td style="border: 1px solid #7f7f7f;" align="right">'.number_format($payamount,2).'</td></tr>';	
			$html.= '<tr><td   colspan="4" align="right">Balance</td><td align="right">'.number_format(($invoice_data['Balance']),2).'</td></tr> ';					
	 
  $email1 = ($company_data['merchantEmail'])?$company_data['merchantEmail']:'#';
 
 $html.='</table>';
 $url = base_url();
 $info = parse_url($url);
 $host = $info['host'];
 $host_names = explode(".", $host);
 
 if(isset($host_names[count($host_names)-2]) && $host_names[count($host_names)-1]){
   $terms_host_name = 'https://'.$host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1].'/terms';
   $pp_host_name = 'https://'.$host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1].'/privacy-policy';
 }else{
   $terms_host_name = base_url().'../terms';
   $pp_host_name = base_url().'../privacy-policy';
 }
 if(isset($config_data['serviceurl']) && !empty($config_data['serviceurl'])){
	 $terms_host_name = $config_data['serviceurl'];
 }
 
 $html.='<div>
	 <div>Payments powered by Chargezoom</div>
	 <div>Terms of Service - <a target="_blank" href="'.$terms_host_name.'">'.$terms_host_name.'</a></div>
	 <div>Privacy Policy - <a target="_blank" href="'.$pp_host_name.'">'.$pp_host_name.'</a></div>
 </div>';

$pdf->writeHTML($html, true, false, true, false, '');


 // ---------------------------------------------------------    

 // Close and output PDF document
 // This method has several options, check the source code documentation for more information.
 
 
 $pdf->Output($pdfFilePath, $mode);  
 if($mode=='F')
 return true;
 
 
}
   	public  function send_mail_invoice_data($condition_mail,$company,$customer,$toEmail, $customerID,$invoice_due_date,$invoicebalance,$tr_date,$invoiceID,$inv_no='')
                    
	{
	   
	          
	   
	           $user_id = $condition_mail['merchantID'];
	        
	           $res =   $this->get_select_data('tbl_merchant_data',array('merchantEmail','companyName','firstName','lastName','merchantContact'), array('merchID'=>$user_id));

	          $view_data      = $this->template_data($condition_mail);	
					  
			$merchant_name=	($res['companyName'])?$res['companyName']:$res['firstName'].''.$res['lastName'];	  
							  $subject = $view_data['emailSubject'];
							  $fromEmail = $view_data['fromEmail'];
						      $phoneNumber=$res['merchantContact'];
                                $companyName= $res['companyName'];
					     		$merchantEmail =	$res['merchantEmail'];	
							 if($view_data['fromEmail'] == ''){
								$fromEmail = 'donotreply@payportal.com';
							}
							   $message = $view_data['message'];
							   $addCC      = $view_data['addCC'];
							   $addBCC		= $view_data['addBCC'];

						   $subject =	stripslashes(str_ireplace('{{invoice.refnumber}}',$inv_no, $subject));
	    	  
	    	                $subject =	stripslashes(str_ireplace('{{transaction.transaction_date}}',$invoice_due_date, $subject));
	    	                $subject =	stripslashes(str_ireplace('{{invoice_date}}',$invoice_due_date, $subject));
	    	                $subject =	stripslashes(str_ireplace('{{company_name}}',$companyName, $subject));
							$subject = stripslashes(str_ireplace('{{invoice_due_date}}', $invoice_due_date, $subject ));
							$subject = stripslashes(str_ireplace('{{transaction.amount}}',($invoicebalance)?('$'.number_format($invoicebalance,2)):'0.00' ,$subject )); 
							   
							   $message = stripslashes(str_ireplace('{{customer.company}}',$company ,$message));
					
								
							   $message = stripslashes(str_ireplace('{{invoice.balance}}',($invoicebalance)?('$'.number_format($invoicebalance,2)):'0.00' ,$message )); 
							   $message = stripslashes(str_ireplace('{{transaction.amount}}',($invoicebalance)?('$'.number_format($invoicebalance,2)):'0.00' ,$message )); 
							   $message = stripslashes(str_ireplace('{{invoice_due_date}}', $invoice_due_date, $message ));
							   $message = stripslashes(str_ireplace('{{merchant_name}}',$merchant_name ,$message));
                               $message = stripslashes(str_ireplace('{{merchant_phone}}',$phoneNumber ,$message ));
							   $message = stripslashes(str_ireplace('{{merchant_email}}',$merchantEmail ,$message ));
							   $message = stripslashes(str_ireplace('{{invoice.refnumber}}',$inv_no, $message));

                   $paymentlink='';
                     $logo_url ='';
		   $config_data = $this->get_select_data('tbl_config_setting',array('ProfileImage','customerPortalURL'), array('merchantID'=>$user_id));
		   if(!empty($config_data))
		   {
		      $logo_url  = $config_data['ProfileImage']; 
		      $update_link = $config_data['customerPortalURL'];
           	   $ttt = explode(PLINK,$update_link);
                 
                $purl = $ttt[0].PLINK.'/customer/';
		        $code = $this->create_invoice_code($user_id);  
		        $invcode =  $this->safe_encode($invoiceID);
		        $paymentlink = $purl.'update_payment/'.$code.'/'.$invcode;
		        $link = '<a href="'.$paymentlink.'">Click Here</a>';
		        $message =	stripslashes(str_ireplace('{{invoice.url_permalink}}',$link, $message));
		        $message =	stripslashes(str_ireplace('{{invoice_payment_pagelink}}',$link, $message));
	      if( !empty( $config_data['ProfileImage'])){   $logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  }else{  $logo_url = CZLOGO; }
    
    		$logo_url=	"<img src='".$logo_url."' width='".LOGOWIDTH."' height='".LOGOHEIGHT."' />";
            $message = stripslashes(str_ireplace('{{logo}}',$logo_url ,$message ));
    

								if($view_data['replyTo'] != ''){
									$replyTo = $view_data['replyTo'];
								}else{
									$replyTo = $res['merchantEmail'];
								}
							   $tr_date   = date('Y-m-d H:i:s');
							   $email_data          = array(
			                            'customerID'=>$customerID,
										'merchantID'=>$user_id, 
										'emailSubject'=>$subject,
										'emailfrom'=>$fromEmail,
										'emailto'=>$toEmail,
										'emailcc'=>$addCC,
										'emailbcc'=>$addBCC,
										'emailreplyto'=>$replyTo,
										'emailMessage'=>$message,
										'emailsendAt'=>$tr_date,
										
										);
    
                 if($invoiceID!='')
                  {
                    	$email_data['emailCode']   = $invcode;
                    	$email_data['invoiceID']   = $invoiceID;
                }
					
					
			  if($inv_no!='')
	           $file_name = $invoiceID.'-'."$inv_no.pdf";
	           else
	           $file_name = "$invoiceID.pdf";
							  
							 
								
								
							if($file_name!="")	
							{
								$filename = FCPATH.'/uploads/invoice_pdf/'.$file_name;  
                               
		                    }
							
							$toaddCCArrEmail = [];
							if($addCC != ''){
								$toaddCCArr = (explode(";",$addCC));
								foreach ($toaddCCArr as $value) {
									$toaddCCArrEmail[]['email'] = trim($value);
								}
							}
							
							$toaddBCCArrEmail = [];
							if($addBCC != ''){
								$toaddBCCArr = (explode(";",$addBCC));
								foreach ($toaddBCCArr as $value) {
									$toaddBCCArrEmail[]['email'] = trim($value);
								}
							}

							$request_array = [
								"personalizations" => [
									[
									"to" => [
										[
										"email" => $toEmail
										]
									],
									"cc"=> $toaddCCArrEmail,
									"bcc"=>$toaddBCCArrEmail,
									"subject" => $subject
									]
								],
								"from" => [
									"email" => $fromEmail,
									"name" => $merchant_name
								],
								"reply_to" => [
									"email" => $replyTo
								],
								"content" => [
									[
									"type" => "text/html",
									"value" => $message
									]
									],
								
										
							];

							if(count($toaddCCArrEmail) == 0){
								unset($request_array['personalizations'][0]['cc']);
							}
							if(count($toaddBCCArrEmail) == 0){
								unset($request_array['personalizations'][0]['bcc']);
							}

							if($view_data['attachedTo'] == 1 && ($view_data['templateType'] == 1 || $view_data['templateType'] == 2)){
								if($file_name!="")	
								{
									$attachments[] = [
										'filename' => $file_name,
										'content' => base64_encode(file_get_contents(FCPATH.'/uploads/invoice_pdf/'.$file_name))
									];
									$request_array['attachments'] = $attachments;
								}
							}
							
							// get api key for send grid
							$api_key = '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';
							$url = 'https://api.sendgrid.com/v3/mail/send';
					
							// set authorization header
							$headerArray = [
								'Authorization: Bearer '.$api_key,
								'Content-Type: application/json',
								'Accept: application/json'
							];
					
							$ch = curl_init($url);
							curl_setopt ($ch, CURLOPT_POST, true);
							curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_HEADER, true);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					
							// add authorization header
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
					
							$response = curl_exec($ch);
							$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
							$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
							curl_close($ch);
					
							// parse header data from response
							$header_text = substr($response, 0, $header_size);
							// parse response body from curl response
							$body = substr($response, $header_size);
					
							$headers = createSendGridHeaders($header_text);
							
							// if mail sent success
							if($httpcode == '202' || $httpcode == '200'){
								$email_data['send_grid_email_id'] = isset($headers['X-Message-Id']) ? $headers['X-Message-Id'] : '';
            					$email_data['send_grid_email_status'] = 'Sent';
								$this->insert_row('tbl_template_data', $email_data); 
							}
								
							else
							{
								$email_data['send_grid_email_status'] = 'Failed';
								$email_data['mailStatus']=0;
								$this->insert_row('tbl_template_data', $email_data); 
							}
						
		   }                    
	
	
	}
	
      public function create_invoice_code($user_id)
   {
			$str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
			$shuffled = str_shuffle($str);
			$shuffled = substr($shuffled, 1, 12) . '=' . $user_id;
			return $this->safe_encode($shuffled);

   }	
 

   function safe_encode($string) {
		return strtr(base64_encode($string), '+/=', '-_-');
   }

 	public function merchant_default_gateway($args) {
		$res=array();
	
		$this->db->select('TMD.*, TMG.*');
		$this->db->from('tbl_merchant_data TMD');
		$this->db->join('tbl_merchant_gateway TMG','TMD.merchID = TMG.merchantID','INNER');
		$this->db->where('TMD.merchID', $args['merchID']);
		$this->db->where('TMG.set_as_default', '1');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		  $res = $query->result_array();
		}
		
		return $res;
	}
	public function get_reseller_table_data($table) {
			
		$data=array();	
		
		$query = $this->db->select('*')->from($table)->get();
		
		if($query->num_rows()>0 ) {
			$data = $query->result_array();
		   return $data;
		} else {
			return $data;
		}				
	}
	function update_mul_row_data($table_name,$condition, $array)
	{
		if(!empty($condition)){
			foreach($condition as $key => $value) {
				if(is_array($value)) {
					$this->db->where_in($key, $value);
				} else {
					$this->db->where($key, $value);
				}
			}
		}
		$trt = $this->db->update($table_name, $array); 
		if($trt)
			return true;
		else
			return false;
	}
	public function get_product_table_data($table, $con,$order=array()) {
	    $data=array();
	  
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
      		if(!empty($order))
            {
            	$this->db->order_by($order['name'],$order['type']);
			}
			$columns = array('Name', 'SalesDescription', 'Type', 'purchaseCost', 'saleCost');
			$i   = 0;
			$con = '';
			foreach ($columns as $item) {

				if ($_POST['search']['value']) {

					if ($i === 0) {
						$con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
					} else {
						$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

					}

				}

				$column[$i] = $item;
				$i++;
			}

			if ($con != '') {
				$con .= ' ) ';
				$this->db->where($con);
			}
			$column = getTableRowOrderColumnValue($_POST['order'][0]['column']);

	        if($column == 0){
	            $orderName = 'Name';
	        }elseif($column == 1){
	            $orderName = 'SalesDescription';
	        }elseif($column == 2){
	            $orderName = 'Type';
	        }elseif($column == 3){
	            $orderName = 'purchaseCost';
	        }elseif($column == 4){
	            $orderName = 'saleCost';
	        }else{
	            $orderName = 'Name';    
	        }
	        $orderBY = getTableRowOrderByValue($_POST['order'][0]['dir']);

    		$this->db->order_by($orderName, $orderBY);
    		$postLength = getTableRowLengthValue($_POST['length']);
    		$startRow = getTableRowStartValue($_POST['start']);
			if ($postLength != -1) {
				$this->db->limit($postLength, $startRow);
			}
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }
	  public function get_product_table_count($table, $con,$order=array()) {
	    $data=array();
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			$columns = array('Name', 'SalesDescription', 'Type', 'purchaseCost', 'saleCost');
			$i   = 0;
			$con = '';
			foreach ($columns as $item) {

				if ($_POST['search']['value']) {

					if ($i === 0) {
						$con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
					} else {
						$con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

					}

				}

				$column[$i] = $item;
				$i++;
			}

			if ($con != '') {
				$con .= ' ) ';
				$this->db->where($con);
			}
			$query  = $this->db->get();
			$data = $query->num_rows();
			return $data;
	       
	  }
	
	public function chk_merch_plantype_data($userID)
   {    
      $res=[];

        $this->db->select('p.*, planType');
        $this->db->select('mr.resellerID','mr.payOption','mr.cardID');
        $this->db->select('mr.payOption');
        $this->db->select('mr.cardID');
        $this->db->from('plans p');
        $this->db->join('tbl_merchant_data mr','mr.plan_id = p.plan_id','inner');
        $this->db->where('mr.merchID', $userID);
        $res = $this->db->get()->row();
        
      
	 	return $res;
   
   }  

	public  function getcardType($CCNumber)
    {
           
		$creditcardTypes = array(
            array('Name'=>'American Express','cardLength'=>array(15),'cardPrefix'=>array('34', '37'))
            ,array('Name'=>'Maestro','cardLength'=>array(12, 13, 14, 15, 16, 17, 18, 19),'cardPrefix'=>array('5018', '5020', '5038', '6304', '6759', '6761', '6763'))
            ,array('Name'=>'Mastercard','cardLength'=>array(16),'cardPrefix'=>array('51', '52', '53', '54', '55'))
            ,array('Name'=>'Visa','cardLength'=>array(13,16),'cardPrefix'=>array('4'))
            ,array('Name'=>'JCB','cardLength'=>array(16),'cardPrefix'=>array('3528', '3529', '353', '354', '355', '356', '357', '358'))
            ,array('Name'=>'Discover','cardLength'=>array(16),'cardPrefix'=>array('6011', '622126', '622127', '622128', '622129', '62213',
                                        '62214', '62215', '62216', '62217', '62218', '62219',
                                        '6222', '6223', '6224', '6225', '6226', '6227', '6228',
                                        '62290', '62291', '622920', '622921', '622922', '622923',
                                        '622924', '622925', '644', '645', '646', '647', '648',
                                        '649', '65'))
            ,array('Name'=>'Solo','cardLength'=>array(16, 18, 19),'cardPrefix'=>array('6334', '6767'))
            ,array('Name'=>'Unionpay','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('622126', '622127', '622128', '622129', '62213', '62214',
                                        '62215', '62216', '62217', '62218', '62219', '6222', '6223',
                                        '6224', '6225', '6226', '6227', '6228', '62290', '62291',
                                        '622920', '622921', '622922', '622923', '622924', '622925'))
            ,array('Name'=>'Diners Club','cardLength'=>array(14),'cardPrefix'=>array('300', '301', '302', '303', '304', '305', '36'))
            ,array('Name'=>'Diners Club US','cardLength'=>array(16),'cardPrefix'=>array('54', '55'))
            ,array('Name'=>'Diners Club Carte Blanche','cardLength'=>array(14),'cardPrefix'=>array('300','305'))
            ,array('Name'=>'Laser','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('6304', '6706', '6771', '6709'))
    	);     

        $CCNumber= trim($CCNumber);
        $type='Unknown';
        foreach ($creditcardTypes as $card){
            if (! in_array(strlen($CCNumber),$card['cardLength'])) {
                continue;
            }
            $prefixes = '/^('.implode('|',$card['cardPrefix']).')/';            
            if(preg_match($prefixes,$CCNumber) == 1 ){
                $type= $card['Name'];
                break;
            }
        }
      	return $type;
    }
    public function chk_merch_planFriendlyName($resellerID,$plandID)
   	{    
      	$res=[];

        $this->db->select('friendlyname');
        $this->db->where('plan_id', $plandID);
        $this->db->from('plan_friendlyname');
        $this->db->where('reseller_id', $resellerID);
        $res = $this->db->get()->row();
        
      	if(isset($res->friendlyname)){
      		return $res->friendlyname;
      	}else{
      		return null;
      	}
	 	
   
   } 
    public function sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo, $addCC = '', $addBCC = '', $attachments = []){

		$toaddCCArrEmail = [];
		if($addCC != ''){
			$toaddCCArr = (explode(";",$addCC));
			foreach ($toaddCCArr as $value) {
				$toaddCCArrEmail[]['email'] = trim($value);
			}
		}
		
		$toaddBCCArrEmail = [];
		if($addBCC != ''){
			$toaddBCCArr = (explode(";",$addBCC));
			foreach ($toaddBCCArr as $value) {
				$toaddBCCArrEmail[]['email'] = trim($value);
			}
		}

		$request_array = [
			"personalizations" => [
				[
				"to" => [
					[
					"email" => $toEmail
					],
				],
				"cc"=> $toaddCCArrEmail,
				"bcc"=>$toaddBCCArrEmail,
				"subject" => $subject
				]
			],
			"from" => [
				"email" => $fromEmail,
				"name" => $merchant_name
			],
			"reply_to" => [
				"email" => $replyTo
			],
			"content" => [
				[
				"type" => "text/html",
				"value" => $message
				]
			]
		];

		if(count($toaddCCArrEmail) == 0){
			unset($request_array['personalizations'][0]['cc']);
		}
		if(count($toaddBCCArrEmail) == 0){
			unset($request_array['personalizations'][0]['bcc']);
		}

		if($attachments){
			$request_array['attachments'] = $attachments;
		}

		// get api key for send grid
		$api_key = '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';
		$url = 'https://api.sendgrid.com/v3/mail/send';

		// set authorization header
		$headerArray = [
			'Authorization: Bearer '.$api_key,
			'Content-Type: application/json',
			'Accept: application/json'
		];

		$ch = curl_init($url);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		// add authorization header
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

		$response = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// parse header data from response
		$header_text = substr($response, 0, $header_size);
		// parse response body from curl response
		$body = substr($response, $header_size);

		$headers = createSendGridHeaders($header_text);
		// if mail sent success
		if($httpcode == '202' || $httpcode == '200'){
			$message_id = isset($headers['X-Message-Id']) ? $headers['X-Message-Id'] : '';
			$success = $message_id;
		}else{
			$success = false;
		}
		return $success;
	}
	public function getMerchantFreeTrial($merchantID)
   	{    if(!empty($merchantID)){
   			$today = date("Y-m-d");
	        $this->db->select('m.merchID');
	        $this->db->from('tbl_merchant_data m');
	        $this->db->where('m.merchID',$merchantID);
	        $this->db->where("DATE_FORMAT(m.trial_expiry_date, '%Y-%m-%d') >=", $today);
	        $res = $this->db->get()->row();
        	
	      	if(isset($res->merchID)){
	      		
	      		return true;
	      	}else{
	      		return false;
	      	}
	        
	    }else{
	    	return false;
	    }


	   }
	   
	public function success_transaction_count($userID)
    {
        $monthFirstDate = date('Y-m-01'); // First day of Month
    	$monthEndDate = date('Y-m-t').' 23:59:59'; // Last day of Month
		
        $res   = 0;
        $tcode = array('100', '200', '111', '1','120');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('count(transactionID) as c');
        $this->db->from('customer_transaction tr');
        $this->db->where("tr.merchantID ", $userID);
		$this->db->where("tr.transactionDate BETWEEN '$monthFirstDate' AND '$monthEndDate'");
        $this->db->where("tr.transactionID NOT IN ( '', '0' )");
        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        
        $res = $this->db->get()->row()->c;
		
		return $res;
    }

   	
    public function get_merchant_invoice_overview($invID)
    {
        $res=array();
        
        $this->db->select('count(merchantID) as merchant, pl.plan_name ');
        $this->db->from('tbl_merchant_invoice_item r');
        $this->db->join('plans pl','pl.plan_id=r.planID','inner');
        $this->db->where('r.merchantInvoiceID',$invID);
        $this->db->group_by('r.planID');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
           $res = $query->result_array();
        
        }
        
        	return $res;	
    }	
    public function getTermsSelectedID($merchantID)
   	{    if(!empty($merchantID)){
	        $this->db->select('id');
	        $this->db->from('tbl_payment_terms');
	        $this->db->where('merchantID',$merchantID);
	        $this->db->where('set_as_default',1);
	        $res = $this->db->get()->row();
        	
	      	if(isset($res->id)){
	      		$id = $res->id;
	      		return $id;
	      	}
	        
	    }else{
	    	return 0;
	    }
   	}
   	public function triggerCampaign($merchantID,$transactionCode){
   		$merchant_condition = ['merchID' => $merchantID];
	    $merchantData = $this->get_row_data('tbl_merchant_data',$merchant_condition);
   		if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
        {
	   		$tcode = array('100', '200', '111', '1','120');
								
			$data=array();	

			$this->db->select('transactionID')->from('customer_transaction');

			$this->db->where("merchantID", $merchantID);

			$this->db->where_in('(transactionCode)', $tcode);

			$query = $this->db->get();
			$countTotal = $query->num_rows();
			if($countTotal == 0){
				
				if (in_array($transactionCode, $tcode)){
					/* Start campaign in hatchbuck CRM*/  
	                $this->load->library('hatchBuckAPI');
	                
	                $merchantData['merchant_type'] = 'Merchant First Payment';        
	                
	                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_TRANSACTION);
	                if($status['statusCode'] == 400){
	                    $resource = $this->hatchbuckapi->createContact($merchantData);
	                    if($resource['contactID'] != '0'){
	                        $contact_id = $resource['contactID'];
	                        $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_TRANSACTION);
	                    }
	                }
	                /* End campaign in hatchbuck CRM*/ 
				}
			}
		}
   	}

	public function get_qbd_invoice_details($table, $invId, $user_id) {
			
		$data=array();	
		$where_au = "((TxnID = '$invId' AND TxnID != insertInvID) OR (insertInvID = '$invId' AND insertInvID > 0))";
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where(['qb_inv_merchantID' => $user_id]);
		$this->db->where($where_au);
		
		$query = $this->db->get();
		if($query->num_rows()>0 ) {
			$data = $query->row_array();
			return $data;
		} else {
			return $data;
		}				

	}
   	public  function surcharge_send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $transactionID   )
	{
		if(empty($toEmail) || !filter_var($toEmail, FILTER_VALIDATE_EMAIL)){
		      return false;
		}
		$companyMercName = '';
		if ($this->session->userdata('logged_in') != "") {
			$companyMercName = $this->session->userdata('logged_in')['companyName'];
		} 
		if ($this->session->userdata('user_logged_in') != "") {
			$companyMercName = $this->session->userdata('user_logged_in')['merchant_data']['companyName'];
		}
	    $phoneNumber='';
	    $user_id = $condition_mail['merchantID'];
	    $res =   $this->get_select_data('tbl_merchant_data',array('merchantEmail','companyName','firstName','lastName','merchantContact'), array('merchID'=>$user_id));
	
		$isValid = is_a_valid_customer([
			'email' => $toEmail
		]);

		if(!$isValid){
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Customer is not synced with active merchant</strong></div>');
			return false;
		}

		$addBCC=$addCC='';
		$view_data      = $this->template_data($condition_mail);
	    if(isset($view_data['templateID'])){
	    	$pay_data      = $this->getiTransactTransactionDetails($user_id,$transactionID);	
		    $surchargeAmount = $pay_data['surCharge'];
		    $totalAmount = $pay_data['totalAmount'];

			$merchant_name=	($res['companyName'])?$res['companyName']:$res['firstName'].''.$res['lastName'];	  
			$subject = $view_data['emailSubject'];
			$fromEmail = $view_data['fromEmail'];
			$phoneNumber=$res['merchantContact'];

			if($view_data['fromEmail'] == ''){
				$fromEmail = 'donotreply@payportal.com';
			}

			$addCC      = $view_data['addCC'];
			$addBCC		= $view_data['addBCC'];
						     		
			$in_link='';
			$message = $view_data['message'];

			$message = stripslashes(str_ireplace('{{merchant_company_name}}',$companyMercName ,$message ));
			$message = stripslashes(str_ireplace('{{customer.company}}',$company ,$message));
			$message = stripslashes(str_ireplace('{{customer_contact_name}}',$customer ,$message ));
			$message =	stripslashes(str_ireplace('{{invoice.refnumber}}',$ref_number, $message));
			$message =	stripslashes(str_ireplace('{{invoice_payment_pagelink}}',$in_link, $message));
			$message = stripslashes(str_ireplace('{{transaction.amount}}',($amount)?('$'.number_format($amount,2)):'0.00' ,$message )); 
			$message = stripslashes(str_ireplace('{{surcharge.amount}}',($surchargeAmount)?('$'.number_format($surchargeAmount,2)):'0.00' ,$message ));
			$message = stripslashes(str_ireplace('{{authorized.amount}}',($totalAmount)?('$'.number_format($totalAmount,2)):'0.00' ,$message ));
			$message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message ));
			$message = stripslashes(str_ireplace('{{merchant_name}}',$merchant_name ,$message));
			

			$logo_url ='';
			$config_data = $this->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
			if(!empty($config_data))
			   $logo_url  = $config_data['ProfileImage']; 
	           
			 
		    if( !empty( $config_data['ProfileImage'])){   $logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  }else{  $logo_url = CZLOGO; }
	    
	    	$logo_url=	"<img src='".$logo_url."' />";
	        $message = stripslashes(str_ireplace('{{logo}}',$logo_url ,$message ));

			if($view_data['replyTo'] != ''){
				$replyTo = $view_data['replyTo'];
			}else{
				$replyTo = $res['merchantEmail'];
			}
			$email_data          = array(
				'customerID'=>$customerID,
				'merchantID'=>$user_id, 
				'emailSubject'=>$subject,
				'emailfrom'=>$replyTo,
				'emailto'=>$toEmail,
				'emailcc'=>$addCC,
				'emailbcc'=>$addBCC,
				'emailreplyto'=>$replyTo,
				'emailMessage'=>$message,
				'emailsendAt'=>$tr_date,
					
			);
			
			$mail_sent = $this->sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo, $addCC, $addBCC);
			if($mail_sent)
			{
				$email_data['send_grid_email_id'] = $mail_sent;
				$email_data['send_grid_email_status'] = 'Sent';
				$this->insert_row('tbl_template_data', $email_data); 
			}
			else
			{
				$email_data['send_grid_email_status'] = 'Failed';
				$email_data['mailStatus']=0;
				$this->insert_row('tbl_template_data', $email_data); 
			}
	    }
	    
	}

	function getiTransactTransactionDetails($merchID,$transaction_id){
		
		$gt_result = $this->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchID,'gatewayType' => 10));

        $apiUsername  = $gt_result['gatewayUsername'];
        $apiKey  = $gt_result['gatewayPassword']; 
        $isSurcharge  = $gt_result['isSurcharge']; 
        $surCharge = 0;
		$totalAmount =0;
		$payAmount = 0;
		$data = array('surCharge' => 0,'totalAmount' => 0,'payAmount' => 0,'isSurcharge' => $isSurcharge );
        if($isSurcharge){
        	$sdk = new iTTransaction();
			$payload = [];

	        $result1 = $sdk->getTransactionByID($apiUsername, $apiKey, $payload,$transaction_id);
	        
	        if ($result1['status_code'] == '200' || $result1['status_code'] == '201') {
	        	if(isset($result1['surcharge_amount']) && $result1['surcharge_amount'] != ''){
	        		$data['surCharge'] = $result1['surcharge_amount'] / 100;
	        	}
	        	$data['totalAmount'] = $result1['authorized_amount'] / 100;
	        	$data['payAmount'] = $result1['amount'] / 100;
	        }
        }

		return $data;
	}

	public function get_customer_details_all($customerID){
		if ($this->session->userdata('logged_in') != "") {
			$companyMercName = $this->session->userdata('logged_in')['companyName'];
			$active_app = $this->session->userdata('logged_in')['active_app'];
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
		} 
		if ($this->session->userdata('user_logged_in') != "") {
			$companyMercName = $this->session->userdata('user_logged_in')['merchant_data']['companyName'];
			$active_app = $this->session->userdata('user_logged_in')['active_app'];
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$customerName = [
			'full_name' => '',
			'fname' => '',
			'lname' => '',
			'email' => '',
			'company_name' => '',
			'contact_number' => '',
			'customerId' => $customerID,
		];

		if($active_app == 1){
			$customer = $this->get_row_data('QBO_custom_customer', array('merchantID' => $merchantID,'Customer_ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['fullName'];
				$customerName['fname'] = $customer['firstName'];
				$customerName['lname'] = $customer['lastName'];
				$customerName['email'] = $customer['userEmail'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['phoneNumber'];
			}
		} else if($active_app == 2){
			$customer = $this->get_row_data('qb_test_customer', array('qbmerchantID' => $merchantID,'ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['FullName'];
				$customerName['fname'] = $customer['FirstName'];
				$customerName['lname'] = $customer['LastName'];
				$customerName['email'] = $customer['Contact'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['Phone'];
			}
		} else if($active_app == 3){
			$customer = $this->get_row_data('Freshbooks_custom_customer', array('merchantID' => $merchantID,'Customer_ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['fullName'];
				$customerName['fname'] = $customer['firstName'];
				$customerName['lname'] = $customer['lastName'];
				$customerName['email'] = $customer['userEmail'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['phoneNumber'];
			}
		} else if($active_app == 4){
			$customer = $this->get_row_data('Xero_custom_customer', array('merchantID' => $merchantID,'Customer_ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['fullName'];
				$customerName['fname'] = $customer['firstName'];
				$customerName['lname'] = $customer['lastName'];
				$customerName['email'] = $customer['userEmail'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['phoneNumber'];
			}
		} else if($active_app == 5){
			$customer = $this->get_row_data('chargezoom_test_customer', array('qbmerchantID' => $merchantID,'ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['FullName'];
				$customerName['fname'] = $customer['FirstName'];
				$customerName['lname'] = $customer['LastName'];
				$customerName['email'] = $customer['Contact'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['Phone'];
			}
		}

		return $customerName;
	}


	public function get_reseller_friendly_plan_name($planid, $rsID)
    {
	    $res = array();
	    $ress = array();
	    $today  = date('Y-m-d');
	    

	    $query  = $this->db->select('pl.*')->from('plans pl')->where('pl.plan_id', $planid)->get();


	    if ($query->num_rows() > 0) {
	      $r = $query->row_array();

	      
	        $q1 = $this->db->select('pf.friendlyname')->from('plan_friendlyname pf ')->where('pf.reseller_id', $rsID)->where('pf.plan_id', $r['plan_id'])->get();
	  
	        if ($q1->num_rows() > 0) {
	          $fname = $q1->row_array()['friendlyname'];
	        } else {
	          $fname  = $r['plan_name'];
	        }

	        $r['friendlyname'] = $fname;

	        $res = $r;
	      
	    }
	    return  $res;
	}
	  
	  /**
	 *  Login Throttling
	 */
	public function addLoginAttempt($ip, $post_data=null)
	{
		$base64_data = ($post_data) ? base64_encode(json_encode($post_data)) : null;
		$data = ['ip' => $ip, 'base64_data' => $base64_data, 'created_at' => date('Y-m-d H:i:s')];
		$this->db->insert('tbl_login_attempts', $data);
	}

	public function getLastLoginAttemptCount($ip, $seconds=300)
	{
		
		$this->db->select('count(id) as c');
		$this->db->from('tbl_login_attempts');
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-$seconds) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->group_by('ip');

		$data = $this->db->get()->row_array();
		
		return ($data ? $data['c'] : 0);
	}

	public function deleteLoginAttempt($ip)
	{
		// Deleting past an hour 
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-3600) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->delete('tbl_login_attempts');
	}
	/**
	 * 
	 * End Login Throttling
	 */
	
	  
	// Add Payment Action Log
	public function addPaymentLog($gateway_id, $access_url=null, $request=null, $response=null, $status=null)
	{
		
		$data = [];
		$data['payment_gateway_id'] = $gateway_id;
		$data['access_url'] = $access_url;
		$data['access_block'] = 'base';
		$data['request_payload'] = (is_array($request) ? json_encode($request): $request) ;
		$data['response_payload'] = (is_array($response) ? json_encode($response): $response) ;
		$this->db->insert('tbl_payment_logs', $data);
	
  	}
  	public function getTransactionByIDOnlySuccess($TxnID,$integrationType,$merchantID,$transactionType=1)
  	{
		if($transactionType == 1){
			if($integrationType == 2){
				$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.TxnID as txnInvoiceID,inv.RefNumber as RefNumber');

				$this->db->join('qb_test_invoice inv','tr.invoiceTxnID = inv.TxnID','INNER');
				$this->db->where("inv.qb_inv_merchantID ", $merchantID);
			}elseif($integrationType == 5){
				$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.TxnID as txnInvoiceID,inv.RefNumber as RefNumber');

				$this->db->join('chargezoom_test_invoice inv','tr.invoiceTxnID = inv.TxnID','INNER');
			}elseif($integrationType == 1){
				$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

				$this->db->join('QBO_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
				$this->db->where("inv.merchantID ", $merchantID);
			}elseif($integrationType == 3){
				$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

				$this->db->join('Freshbooks_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
				$this->db->where("inv.merchantID ", $merchantID);
			}elseif($integrationType == 4){
				$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

				$this->db->join('Xero_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
				$this->db->where("inv.merchantID ", $merchantID);
			}
		}else{
			$this->db->select('tr.*,tr.id as txnRowID');
		}
		$this->db->where("tr.merchantID", $merchantID);
		$this->db->from('customer_transaction tr');
		$this->db->where('tr.transaction_user_status',1);
		$this->db->where('tr.transactionType !=','refund');
		$this->db->where('tr.transactionID',$TxnID);
		$this->db->where_in('tr.transactionCode', [100,200,1,111,120]);

	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() >0 )
        {
        	$obj = [];
        	$data = $query->result_array();
        	foreach ($data as $value) {
        		# code...
        		$txnData = $this->getRefundTransactionByID($value['txnRowID']);
        		$calAmount = $value['transactionAmount'] - $txnData['Refundamount'];
        		$value['transactionAmount'] = $calAmount;
        		$obj[] = $value;
        		
        	}
            return $obj;  
        }
        else
        {
           return [];
        }				

	}
  	public function getRefundTransactionByID($trID)
  	{
		
		
		$this->db->select('tr.*,sum(tr.transactionAmount) as Refundamount');
		$this->db->from('customer_transaction tr');
		$this->db->where('tr.parent_id',$trID);
		$this->db->where('tr.transactionType','refund');
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
        	$data = $query->result_array();
        	foreach ($data as $value) {
        		# code...
        	}
            return $query->row_array();  
        }
        else
        {
           return [];
        }				

	}
	public function getCustomerDetails($customerID,$merchantID,$integrationType){
		if($integrationType == 1){
			$this->db->select('*');
			$this->db->from('QBO_custom_customer');
			$this->db->where('Customer_ListID',$customerID);
			$this->db->where('merchantID',$merchantID);
		}elseif($integrationType == 2){
			$this->db->select('FullName as fullName,FirstName as firstName,LastName as lastName,Phone as phoneNumber,Contact as userEmail,BillingAddress_Addr1 as address1,BillingAddress_Addr2 as address2,BillingAddress_City as City,BillingAddress_State as State,BillingAddress_Country as Country,BillingAddress_PostalCode as zipCode,ShipAddress_Addr1 as ship_address1,ShipAddress_Addr2 as ship_address2,ShipAddress_City as ship_city,ShipAddress_State as ship_state,ShipAddress_Country as ship_country,ShipAddress_PostalCode as ship_zipcode');
			$this->db->from('qb_test_customer');
			$this->db->where('ListID',$customerID);
			$this->db->where('qbmerchantID',$merchantID);


		}elseif($integrationType == 3){
			$this->db->select('*');
			$this->db->from('Freshbooks_custom_customer');
			$this->db->where('Customer_ListID',$customerID);
			$this->db->where('merchantID',$merchantID);
		}elseif($integrationType == 4){
			$this->db->select('*');
			$this->db->from('Xero_custom_customer');
			$this->db->where('Customer_ListID',$customerID);
			$this->db->where('merchantID',$merchantID);
		}elseif($integrationType == 5){
			$this->db->select('FullName as fullName,FirstName as firstName,LastName as lastName,Phone as phoneNumber,Contact as userEmail,BillingAddress_Addr1 as address1,BillingAddress_Addr2 as address2,BillingAddress_City as City,BillingAddress_State as State,BillingAddress_Country as Country,BillingAddress_PostalCode as zipCode,ShipAddress_Addr1 as ship_address1,ShipAddress_Addr2 as ship_address2,ShipAddress_City as ship_city,ShipAddress_State as ship_state,ShipAddress_Country as ship_country,ShipAddress_PostalCode as ship_zipcode');
			$this->db->from('chargezoom_test_customer');
			$this->db->where('ListID',$customerID);
			$this->db->where('qbmerchantID',$merchantID);
		}else{

		}
		$query = $this->db->get();
     
		if($query->num_rows()>0)
        {
        	$data = $query->row_array();
        	
            return $data;  
        }
        else
        {
           return [];
        }				
	}
	
	public function get_invoice_details_data($args)
	{
		$user_id = false;
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		} else {
			$user_id = $args['merchantID'];
		}

		if(!isset($args['invID']) && !isset($args['customerListId']) && !isset($args['itemId'])){
			return false;
		}

		$res=array();
		$this->db->select('inv.*, inv.TxnID as invoiceID, Item_ListID');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_invoice_lineitem invItem','invItem.TxnID = inv.TxnID AND invItem.inv_itm_merchant_id = inv.qb_inv_merchantID','INNER');
		$this->db->join('qb_test_customer invCust','invCust.ListID = inv.Customer_ListID AND invCust.qbmerchantID = inv.qb_inv_merchantID','INNER');
		$this->db->where('qb_inv_merchantID',$user_id);
		if(isset($args['invID'])  && !empty($args['invID'])){
			$invID = $args['invID'];
			$this->db->where('inv.TxnID',"$invID");
		}

		if(isset($args['customerListId']) && !empty($args['customerListId'])){
			$customerListId = $args['customerListId'];
			$this->db->where('inv.Customer_ListID',"$customerListId");
		}

		if(isset($args['itemId']) && !empty($args['itemId'])){
			$itemId = $args['itemId'];
			$this->db->where('invItem.Item_ListID', "$itemId");
		}
		
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
	}
	
	public function qbo_payment_method($paymentMethod, $merchantID, $isACH = true){
		$where = [
			'payment_type' => ($isACH) ? 'NON_CREDIT_CARD' : 'CREDIT_CARD',
			'merchantID' =>  $merchantID
		];

	    $query  = $this->db->select('*')->from('QBO_payment_method')->where($where)->order_by('payment_name')->get();
		if ($query->num_rows() > 0) {
			$data = $query->result_array();
			foreach ($data as $key => $val) {
				if (strtoupper($val['payment_name']) === strtoupper($paymentMethod)) {
					return $val;
				}
			}

			return $data[0];
		}

		return false;
	}
	public function getTransactionByInvoiceID($invoiceID,$integrationType,$merchantID,$transactionType=1)
  	{
		if($transactionType == 1){
			if($integrationType == 2){
				$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.id as txnRowID, inv.*,inv.TxnID as txnInvoiceID,inv.RefNumber as RefNumber');

				$this->db->join('qb_test_invoice inv','tr.invoiceTxnID = inv.TxnID','INNER');
				$this->db->where("inv.qb_inv_merchantID ", $merchantID);
				$this->db->where("tr.invoiceTxnID ", $invoiceID);
			}elseif($integrationType == 5){
				$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.id as txnRowID, inv.*,inv.TxnID as txnInvoiceID,inv.RefNumber as RefNumber');

				$this->db->join('chargezoom_test_invoice inv','tr.invoiceTxnID = inv.TxnID','INNER');
				$this->db->where("tr.invoiceTxnID ", $invoiceID);
				
			}elseif($integrationType == 1){
				$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

				$this->db->join('QBO_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
				$this->db->where("inv.merchantID ", $merchantID);
				$this->db->where("tr.invoiceID ", $invoiceID);
				
			}elseif($integrationType == 3){
				$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

				$this->db->join('Freshbooks_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
				$this->db->where("inv.merchantID ", $merchantID);
				$this->db->where("tr.invoiceID ", $invoiceID);
			}elseif($integrationType == 4){
				$this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

				$this->db->join('Xero_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
				$this->db->where("inv.merchantID ", $merchantID);
				$this->db->where("tr.invoiceID ", $invoiceID);
			}
		}else{
			$this->db->select('tr.*,tr.id as txnRowID');
		}
		$this->db->where("tr.merchantID", $merchantID);
		$this->db->from('customer_transaction tr');
		$this->db->where('tr.transaction_user_status',1);
		$this->db->where('tr.transactionType !=','refund');
		$this->db->where_in('tr.transactionCode', [100,200,1,111,120]);

	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
     	if($query->num_rows() >0 )
        {
        	$obj = [];
        	$data = $query->result_array();
        	$totalAmount = 0;
        	foreach ($data as $value) {
        		# code...
        		$txnData = $this->getRefundTransactionByID($value['txnRowID']);
        		$calAmount = $value['transactionAmount'] - $txnData['Refundamount'];
        		$totalAmount = $totalAmount + $calAmount;
        		$obj = $value;
        		
        	}
        	$obj['transactionAmount'] = $totalAmount;
        	
            return $obj;  
        }
        else
        {
           return [];
        }				

	}
	public function getInvoiceSuccessTransaction($invoiceID,$merchantID,$integrationType)
  	{

		if($integrationType == 2){
			$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.TxnID as txnInvoiceID,inv.RefNumber as RefNumber');

			$this->db->join('qb_test_invoice inv','tr.invoiceTxnID = inv.TxnID','INNER');
			$this->db->where("inv.qb_inv_merchantID ", $merchantID);
			$this->db->where("tr.invoiceTxnID ", $invoiceID);
		}elseif($integrationType == 5){
			$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.TxnID as txnInvoiceID,inv.RefNumber as RefNumber');

			$this->db->join('chargezoom_test_invoice inv','tr.invoiceTxnID = inv.TxnID','INNER');
			$this->db->where("tr.invoiceTxnID ", $invoiceID);
			
		}elseif($integrationType == 1){
			$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');
			$this->db->join('QBO_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
			$this->db->where("inv.merchantID ", $merchantID);
			$this->db->where("tr.invoiceID", $invoiceID);
		}elseif($integrationType == 3){
			$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

			$this->db->join('Freshbooks_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
			$this->db->where("inv.merchantID ", $merchantID);
			$this->db->where("tr.invoiceID", $invoiceID);
		}elseif($integrationType == 4){
			$this->db->select('tr.*,tr.id as txnRowID, inv.*,inv.invoiceID as txnInvoiceID,inv.refNumber as RefNumber');

			$this->db->join('Xero_test_invoice inv','tr.invoiceID = inv.invoiceID','INNER');
			$this->db->where("inv.merchantID ", $merchantID);
			$this->db->where("tr.invoiceID", $invoiceID);
		}else{
			$this->db->select('tr.*,tr.id as txnRowID');
		}
		$this->db->where("tr.merchantID", $merchantID);
		$this->db->from('customer_transaction tr');
		$this->db->where('tr.transaction_user_status',1);
		$this->db->where('tr.transactionType !=','refund');
		
		$this->db->where_in('tr.transactionCode', [100,200,1,111,120]);

	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() >0 )
        {
        	$obj = [];
        	$data = $query->result_array();
        	$totalAmount = 0;
        	foreach ($data as $value) {
        		# code...
        		$txnData = $this->getRefundTransactionByID($value['txnRowID']);
        		$calAmount = $value['transactionAmount'] - $txnData['Refundamount'];
        		$value['transactionAmount'] = $calAmount;
        		$totalAmount = $totalAmount + $calAmount;
        		$obj[] = $value;
        		
        	}
        	
            return $obj;  
        }
        else
        {
           return [];
        }		
  	}
  	public function getTemplateForInvoice($user_id) {
			
		$data=array();
		
		$this->db->select('*');
		$this->db->from('tbl_email_template');
		$this->db->where(['merchantID' => $user_id]);
		$this->db->where_in('templateType',[1,2,3]);
		$query = $this->db->get();
		if($query->num_rows()>0 ) {
			$data = $query->result_array();
			return $data;
		} else {
			return $data;
		}				

	}
	public function getInvoiceBatchTransactionList($invoiceID,$customerID,$transactionRowID,$type) {
		if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');
            $user_id                = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];
        }	
		$data=array();
		if($type == 5){
			$this->db->select('tr.id,tr.transactionStatus,tr.transactionID,tr.transactionCode, tr.invoiceID, tr.transactionAmount,cust.FullName as customerName,cust.ListID as customerID,inv.TxnID as invoiceID, inv.RefNumber as invoiceRefNumber');
			$this->db->from('customer_transaction tr');
			$this->db->join('chargezoom_test_invoice inv','inv.TxnID = tr.invoiceTxnID','INNER');
			$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
			$this->db->where('inv.TxnID',$invoiceID);
			$this->db->where('inv.Customer_ListID',$customerID);
			$this->db->where('tr.customerListID',$customerID);
			$this->db->where('cust.ListID',$customerID);

		}else if($type == 1){
			$this->db->select('tr.id,tr.transactionStatus,tr.transactionID,tr.transactionCode, tr.invoiceID, tr.transactionAmount,cust.fullName as customerName,cust.Customer_ListID as customerID,inv.invoiceID as invoiceID, inv.refNumber as invoiceRefNumber');
			$this->db->from('customer_transaction tr');
			$this->db->join('QBO_test_invoice inv','inv.invoiceID = tr.invoiceID','INNER');
			$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
			$this->db->where('inv.invoiceID',$invoiceID);
			$this->db->where('inv.CustomerListID',$customerID);
			$this->db->where('tr.customerListID',$customerID);
			$this->db->where('cust.Customer_ListID',$customerID);
			$this->db->where('cust.merchantID',$user_id);

		}else if($type == 2){
			$this->db->select('tr.id,tr.transactionStatus,tr.transactionID,tr.transactionCode, tr.invoiceID, tr.transactionAmount,cust.FullName as customerName,cust.ListID as customerID,inv.TxnID as invoiceID, inv.RefNumber as invoiceRefNumber');
			$this->db->from('customer_transaction tr');
			$this->db->join('qb_test_invoice inv','inv.TxnID = tr.invoiceTxnID','INNER');
			$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
			$this->db->where('inv.TxnID',$invoiceID);
			$this->db->where('inv.Customer_ListID',$customerID);
			$this->db->where('tr.customerListID',$customerID);
			$this->db->where('cust.ListID',$customerID);
			$this->db->where('cust.qbmerchantID',$user_id);
		}else if($type == 3){
			$this->db->select('tr.id,tr.transactionStatus,tr.transactionID,tr.transactionCode, tr.invoiceID, tr.transactionAmount,cust.fullName as customerName,cust.Customer_ListID as customerID,inv.invoiceID as invoiceID, inv.refNumber as invoiceRefNumber');
			$this->db->from('customer_transaction tr');
			$this->db->join('Freshbooks_test_invoice inv','inv.invoiceID = tr.invoiceID','INNER');
			$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
			$this->db->where('inv.invoiceID',$invoiceID);
			$this->db->where('inv.CustomerListID',$customerID);
			$this->db->where('tr.customerListID',$customerID);
			$this->db->where('cust.Customer_ListID',$customerID);
			$this->db->where('cust.merchantID',$user_id);
		}else if($type == 4){
			$this->db->select('tr.id,tr.transactionStatus,tr.transactionID,tr.transactionCode, tr.invoiceID, tr.transactionAmount,cust.fullName as customerName,cust.Customer_ListID as customerID,inv.invoiceID as invoiceID, inv.refNumber as invoiceRefNumber');
			$this->db->from('customer_transaction tr');
			$this->db->join('Xero_test_invoice inv','inv.invoiceID = tr.invoiceID','INNER');
			$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
			$this->db->where('inv.invoiceID',$invoiceID);
			$this->db->where('inv.CustomerListID',$customerID);
			$this->db->where('tr.customerListID',$customerID);
			$this->db->where('cust.Customer_ListID',$customerID);
			$this->db->where('cust.merchantID',$user_id);
		}
		$this->db->where('tr.id',$transactionRowID);
		$query = $this->db->get();
		if($query->num_rows()>0 ) {
			$data = $query->row_array();
		
			return $data;
		} else {
			return $data;
		}				
	}
  	public function chartDashboardDateVolumn($mID,$NumberOfDay,$startDay,$endDate,$type)
    {   
        $res= array();
        $days=array();
        $resObj = array(); 
        if($NumberOfDay == 0){
        	$days[] = $startDay;
        }else{
        	if($type == 2){
        		for ($i = 0; $i <= $NumberOfDay ; $i++) {
		            $days[] = date("Y-m-d", strtotime($startDay." +$i days"));
		        }  
        	}else{
        		for ($i = 1; $i <= $NumberOfDay ; $i++) {
		            $days[] = date("Y-m-d", strtotime($startDay." +$i days"));
		        }  
        	}
        	
        }
        $chart_arr = array();
        $totalRevenue = 0;
        $totalRevenue = $totalCCA = $totalECLA = 0;
        foreach($days as $key=> $day)
        { 
            $earn = $this->get_recent_volume_dashboard_day($mID,$day);
            $res[$key]['revenu_volume'] = $earn;
            $res[$key]['revenu_Month'] =$day;

            $earn2 = $this->get_creditcard_payment_day($mID,$day);
            
            $res[$key]['online_volume'] =$earn2;
            $res[$key]['online_Month'] = $day;
                
            $earn4 = $this->get_echeck_payment_day($mID,$day);
            $res[$key]['eCheck_volume'] =$earn4;
            $res[$key]['eCheck_Month'] =$day;

            $totalRevenue = $totalRevenue + $earn;
            $totalCCA = $totalCCA + $earn2;
            $totalECLA = $totalECLA + $earn4;
        }  


        $resObj['data'] = $res;
        $resObj['totalRevenue'] = $totalRevenue;
        $resObj['totalCCA'] = $totalCCA;
        $resObj['totalECLA'] = $totalECLA;
        return  $resObj; 
    } 
      
    public function get_recent_volume_dashboard_day($mID,$day)
     {   
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        $sql = 'SELECT   COALESCE(SUM(tr.transactionAmount),0) as volume FROM 
           customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID WHERE tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
           date_format(tr.transactionDate, "%Y-%m-%d") ="'.$day.'"  and    (tr.transactionType ="Sale" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or tr.transactionType ="ECheck" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
            or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
         tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
         and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")    GROUP BY date_format(transactionDate, "%Y-%m-%d") ' ;
         
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
             $row = $query->row_array();
             $earn = $row['volume']; 
        }
        $res = $earn - $refund;
        return  $res; 
    }    
    public function get_creditcard_payment_day($mID,$day)
    {   

        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        
        $sql = 'SELECT   COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%Y-%m-%d") ="'.$day.'" and  tr.gateway NOT LIKE "%echeck%"  and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%Y-%m-%d") ' ;
        $query = $this->db->query($sql);
        
        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
          $res = $earn; 
        }


        
        return  $res; 
    } 
    public function get_echeck_payment_day($mID,$day)
    {   
       
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        $refund1 = 0.00;
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%Y-%m-%d") ="'.$day.'"  and  tr.gateway LIKE "%echeck%"  and (tr.transactionType ="Sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%Y-%m-%d") ' ;
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
        }
        if($earn >= $refund){
            $earn = $earn - $refund;
        }
        $res = $earn;
        return  $res;
            
    }
    public function get_hours_range( $start = 0, $end = 86400, $step = 3600, $format = '' ) {
        $times = array();
        $data = range( $start, $end, $step );
        $inc = 0;
        $res = [];
        foreach ( $data as $timestamp ) {

            $hour_mins = date('Y-m-d').' '.gmdate( 'H:i:s', $timestamp );
            
            if($inc != 24){
            	if($inc == 23){
            		$hour_minEnd = date('Y-m-d').' 23:59:59';
            	}else{
            		$hour_minEnd = date('Y-m-d').' '.gmdate( 'H:i:s', $data[$inc+1] );
            	}
        		
        		$times['startTime'] = $hour_mins;
        		$times['endTime'] = $hour_minEnd;
        		$res[] = $times;
        	}
            $inc++;
        }
        return $res;
	}
    public function getHourlyRevenueVolumn($mID)
    {   
        $res= array();
        $hours=array();
        $resObj = array(); 
        $times = $this->get_hours_range();

        $chart_arr = array();
        $totalRevenue = 0;
        $totalRevenue = $totalCCA = $totalECLA = 0;
        foreach($times as $key => $time)
        { 
        	$startTime = $time['startTime'];
        	$endTime = $time['endTime'];

            $earn = $this->get_recent_volume_dashboard_hour($mID,$startTime,$endTime);
            
            $res[$key]['revenu_volume'] = $earn;
            $res[$key]['revenu_Month'] = $time;

            $earn2 = $this->get_creditcard_payment_hour($mID,$startTime,$endTime);
            
            $res[$key]['online_volume'] =$earn2;
            $res[$key]['online_Month'] = $time;
                
            $earn4 = $this->get_echeck_payment_hour($mID,$startTime,$endTime);
            $res[$key]['eCheck_volume'] =$earn4;
            $res[$key]['eCheck_Month'] =$time;

            $totalRevenue = $totalRevenue + $earn;
            $totalCCA = $totalCCA + $earn2;
            $totalECLA = $totalECLA + $earn4;
        }  
        

        $resObj['data'] = $res;
        $resObj['totalRevenue'] = $totalRevenue;
        $resObj['totalCCA'] = $totalCCA;
        $resObj['totalECLA'] = $totalECLA;
        return  $resObj; 
    } 
    public function get_recent_volume_dashboard_hour($mID,$startTime,$endTime)
    {   
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        $sql = 'SELECT   COALESCE(SUM(tr.transactionAmount),0) as volume FROM 
           customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID WHERE tr.merchantID = "'.$mID.'"  and mr1.merchID = "'.$mID.'" and  
           date_format(tr.transactionDate, "%Y-%m-%d %H:%i:%s") BETWEEN "'.$startTime.'" and "'.$endTime.'"  and    (tr.transactionType ="Sale" or tr.transactionType ="Offline Payment" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or tr.transactionType ="ECheck" or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
            or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
         tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
         and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")    GROUP BY date_format(transactionDate, "%Y-%m-%d") ' ;
         
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
             $row = $query->row_array();
             $earn = $row['volume']; 
        }
        $res = $earn - $refund;
        return  $res; 
    }   
    public function get_creditcard_payment_hour($mID,$startTime,$endTime)
    {   

        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        
        $sql = 'SELECT   COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%Y-%m-%d %H:%i:%s") BETWEEN "'.$startTime.'" and "'.$endTime.'" and  tr.gateway NOT LIKE "%echeck%"  and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%Y-%m-%d") ' ;
        $query = $this->db->query($sql);
        
        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
          $res = $earn; 
        }


        
        return  $res; 
    } 
    public function get_echeck_payment_hour($mID,$startTime,$endTime)
    {   
       
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        $refund1 = 0.00;
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
            customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE tr.merchantID = "'.$mID.'" and mr1.merchID = "'.$mID.'"  and  
            date_format(tr.transactionDate, "%Y-%m-%d %H:%i:%s") BETWEEN "'.$startTime.'" and "'.$endTime.'" and  tr.gateway LIKE "%echeck%"  and (tr.transactionType ="Sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
              or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
              tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
              and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  GROUP BY date_format(transactionDate, "%Y-%m-%d") ' ;
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
          $row = $query->row_array();
          $earn = $row['volume'];
        }
        if($earn >= $refund){
            $earn = $earn - $refund;
        }
        $res = $earn;
        return  $res;
            
    } 
    public function getDayRevenue($user_id,$startDate,$endDate,$type){
		
		$ts1 = strtotime($startDate);
		$ts2 = strtotime($endDate);
		$seconds_diff = $ts2 - $ts1;
		$day_diff = $seconds_diff/3600/24;

		$get_result = $this->chartDashboardDateVolumn($user_id,$day_diff,$startDate,$endDate,$type);
		if ($get_result['data']) {
			
			$result_set = array();
			$result_value1 = array();
			$result_value2 = array();
			$result_online_value = array();
			$result_online_month = array();
			$result_eCheck_value = array();
			$result_eCheck_month = array();
			foreach ($get_result['data'] as $count_merch) {

				array_push($result_value1, $count_merch['revenu_Month']);
				array_push($result_value2, (float) $count_merch['revenu_volume']);
				array_push($result_online_month, $count_merch['revenu_Month']);
				array_push($result_online_value, (float) $count_merch['online_volume']);
				array_push($result_eCheck_month, $count_merch['revenu_Month']);
				array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
			}
			
			$ob[] = [];
			$obRevenu = [];
			$obOnline = [];
			$obeCheck = [];
			$in = 0;

			foreach ($result_value1 as $value) {
				$custmonths = date("M-d", strtotime($value));
				$ob1 = [];
				$obR = [];
				$obON = [];
				$obEC = [];
				$inc = strtotime($value);
				$ob1[0] = $inc;
				$obR[] = $inc;
				$obR[] = $custmonths;
				$ob1[1] = $result_value2[$in];

				$ob[] = $ob1;

				$obON[0] = $inc;
				$obON[1] = $result_online_value[$in];
				$obOnline[] = $obON;


				$obEC[0] = $inc;
				$obEC[1] = $result_eCheck_value[$in];
				$obeCheck[] = $obEC;

				$obRevenu[] = $obR; 
				$in++;
			}



			$opt_array['revenu_month'] =   $obRevenu;
			$opt_array['revenu_volume'] =   $ob;
			
			$opt_array['online_month'] =   $result_online_month;
			$opt_array['online_volume'] =   $obOnline;
			
			$opt_array['eCheck_month'] =   $result_eCheck_month;
			$opt_array['eCheck_volume'] =   $obeCheck;

			$opt_array['totalRevenue'] =   $get_result['totalRevenue'];
			$opt_array['totalCCA'] =   $get_result['totalCCA'];
			$opt_array['totalECLA'] =   $get_result['totalECLA'];
			return ($opt_array);
			
		}
		return [];
	}
	public function getHourlyRevenue($user_id){
		$get_result = $this->getHourlyRevenueVolumn($user_id);
		
		if ($get_result['data']) {
			
			$result_set = array();
			$result_value1 = array();
			$result_value2 = array();
			$result_online_value = array();
			$result_online_month = array();
			$result_eCheck_value = array();
			$result_eCheck_month = array();
			foreach ($get_result['data'] as $count_merch) {

				array_push($result_value1, $count_merch['revenu_Month']);
				array_push($result_value2, (float) $count_merch['revenu_volume']);
				array_push($result_online_month, $count_merch['revenu_Month']);
				array_push($result_online_value, (float) $count_merch['online_volume']);
				array_push($result_eCheck_month, $count_merch['revenu_Month']);
				array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
			}
			
			$ob[] = [];
			$obRevenu = [];
			$obOnline = [];
			$obeCheck = [];
			$in = 0;

			
			foreach ($result_value1 as $value) {
				if($in == 23){
					$custmonths = '11:59 PM';
				}else{
					$custmonths = date("h A", strtotime($value['endTime']));
				}
				
				$ob1 = [];
				$obR = [];
				$obON = [];
				$obEC = [];
				$inc = strtotime($value['endTime']);
				$ob1[0] = $inc;
				$obR[] = $inc;
				$obR[] = $custmonths;
				$ob1[1] = $result_value2[$in];

				$ob[] = $ob1;

				$obON[0] = $inc;
				$obON[1] = $result_online_value[$in];
				$obOnline[] = $obON;


				$obEC[0] = $inc;
				$obEC[1] = $result_eCheck_value[$in];
				$obeCheck[] = $obEC;

				$obRevenu[] = $obR; 
				$in++;
			}



			$opt_array['revenu_month'] =   $obRevenu;
			$opt_array['revenu_volume'] =   $ob;
			
			$opt_array['online_month'] =   $result_online_month;
			$opt_array['online_volume'] =   $obOnline;
			
			$opt_array['eCheck_month'] =   $result_eCheck_month;
			$opt_array['eCheck_volume'] =   $obeCheck;

			$opt_array['totalRevenue'] =   $get_result['totalRevenue'];
			$opt_array['totalCCA'] =   $get_result['totalCCA'];
			$opt_array['totalECLA'] =   $get_result['totalECLA'];
			return ($opt_array);
			
		}
		return [];
	}

	public function generate_invoice_pdf_xero($user_id, $customer_data, $invoice_data, $invoice_items, $mode)
    {

        $condition4   = array('merchID' => $user_id);
        $company_data = $this->get_select_data('tbl_merchant_data', array('firstName', 'lastName', 'companyName', 'merchantAddress1', 'merchantEmail', 'merchantContact', 'merchantAddress2', 'merchantCity', 'merchantState', 'merchantCountry', 'merchantZipCode'), $condition4);
        $config_data  = $this->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID' => $user_id));
        if ($config_data['ProfileImage'] != "")
        //$logo = base_url().LOGOURL.$config_data['ProfileImage'];
        {
            $logo = LOGOURL1 . $config_data['ProfileImage'];
        } else {
            $logo = CZLOGO;
        }

        $invoiceID = $invoice_data['invoiceID'] . '-' . $invoice_data['refNumber'];
        $no        = $invoiceID;
        $path      = '';

        if ($mode == 'D') {
            $pdfFilePath = "$no.pdf";
        }
        if ($mode == 'F') {
            $path        = FCPATH . 'uploads/invoice_pdf/';
            $pdfFilePath = $path . "$no.pdf";
        }

		$payamount = $invoice_data['AppliedAmount'];

        ini_set('memory_limit', '320M');
        $this->load->library("TPdf");

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        define(PDF_HEADER_TITLE, 'Invoice');
        define(PDF_HEADER_STRING, '');
        $pdf->SetPrintHeader(false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Chargezoom 1.0');
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetFont('dejavusans', '', 10, '', true);
        
        // set cell padding
        $pdf->setCellPaddings(1, 1, 1, 1);

        // set cell margins
        $pdf->setCellMargins(1, 1, 1, 1);
        // Add a page

        $pdf->AddPage();

        $y        = 20;
        $logo_div = '<div style="text-align:left; float:left ">
        <img src="' . $logo . '"  border="0" height="50"/>
        </div>';

        // set color for background
        $pdf->SetFillColor(255, 255, 255);
        // write the first column
        $pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);

        if ($company_data['companyName'] != '') {
            $tt = $company_data['companyName'] . "<br/>";
        }

        if ($company_data['merchantAddress1'] != '') {
            $tt .= ($company_data['merchantAddress1']) . "<br/>";
        }

        if ($company_data['merchantAddress2'] != '') {
            $tt .= ($company_data['merchantAddress2']) . "<br/>";
        }

        if ($company_data['merchantCity'] != '' || $company_data['merchantState'] != '' || $company_data['merchantZipCode']) {
            $tt .= ($company_data['merchantCity']) . ", " . ($company_data['merchantState']) . " " . ($company_data['merchantZipCode']) . '<br/>';
        }

        if ($company_data['merchantCountry'] != '') {
            $tt .= ($company_data['merchantCountry']);
        }

        $y = 50;
        // set color for background
        $pdf->SetFillColor(255, 255, 255);

        // set color for text
        $pdf->SetTextColor(51, 51, 51);

        // write the first column
        $pdf->writeHTMLCell(80, 50, '', $y, "<b>From:</b><br/>" . $tt, 0, 0, 1, true, 'J', true);

        // set color for background
        //    $pdf->SetFillColor(215, 235, 255);

        // set color for text
        $pdf->SetTextColor(51, 51, 51);
        if ($invoice_data['refNumber'] != '') {
            $invoice = $invoice_data['refNumber'] . "<br/><br/>";
        }

        if ($invoice_data['DueDate'] != '') {

            $date = date("m/d/Y", strtotime($invoice_data['DueDate']));
        }

        $pdf->writeHTMLCell(80, 50, '', '', '<b>Invoice: </b>' . $invoice . '<b>Due Date: </b>' . $date, 0, 1, 1, true, 'J', true);

        $lll         = '';
        $BillingAdd  = 0;
        $ShippingAdd = 0;

        $isAdd = 0;
        if ($customer_data['address1'] || $customer_data['address2'] || $customer_data['City'] || $customer_data['State'] || $customer_data['zipCode'] || $customer_data['Country']) {
            $BillingAdd = 1;
            $isAdd      = 1;
        }
        if ($customer_data['ship_address1'] || $customer_data['ship_address2'] || $customer_data['ship_city'] || $customer_data['ship_state'] || $customer_data['ship_zipcode']) {
            $ShippingAdd = 1;
            $isAdd       = 1;
        }

        $customerName = '';
        if (isset($customer_data['firstName']) && $customer_data['firstName'] != '') {
            $customerName = $customer_data['firstName'] . ' ' . $customer_data['lastName'];
        } else if (isset($invoice_data['CustomerFullName']) && $invoice_data['CustomerFullName'] != '') {
            $customerName = $invoice_data['CustomerFullName'];
        }

        $customerPhone = '';
        if (isset($customer_data['phoneNumber']) && $customer_data['phoneNumber'] != '') {
            $customerPhone = $customer_data['phoneNumber'];
        }

        $customerEmail = '';
        if (isset($customer_data['userEmail']) && $customer_data['userEmail'] != '') {
            $customerEmail = $customer_data['userEmail'];
        }

        if ($BillingAdd) {
            $lll .= "$customerName<br>";
            $lll .= ($customer_data['address1']) ? $customer_data['address1'] . '<br>' : '';
            $lll .= ($customer_data['address2']) ? $customer_data['address2'] . '<br>' : '';
            $lll .= ($customer_data['City']) ? $customer_data['City'] . ', ' : '';
            $lll .= ($customer_data['State']) ? $customer_data['State'] . ' ' : '' . ' ';
            $lll .= ($customer_data['zipCode']) ? $customer_data['zipCode'] : '';
            $lll .= '<br>';
            $lll .= (isset($customer_data['Country']) && $customer_data['Country']) ? $customer_data['Country'] : '';
            $lll .= '<br>';
            $lll .= "<span>&#9742;<span>:  $customerPhone<br>";
            $lll .= "<span>&#9993;<span>:  $customerEmail<br>";
        }
        
        $lll_ship = '';

        if ($ShippingAdd) {
            $lll_ship .= "$customerName<br>";
            $lll_ship .= ($customer_data['ship_address1']) ? $customer_data['ship_address1'] . '<br>' : '';
            $lll_ship .= ($customer_data['ship_address2']) ? $customer_data['ship_address2'] . '<br>' : '';
            $lll_ship .= ($customer_data['ship_city']) ? $customer_data['ship_city'] . ', ' : '';
            $lll_ship .= ($customer_data['ship_state']) ? $customer_data['ship_state'] . ' ' : '' . ' ';
            $lll_ship .= ($customer_data['ship_zipcode']) ? $customer_data['ship_zipcode'] : '';
            $lll_ship .= '<br>';
            $lll_ship .= (isset($customer_data['ship_country']) && $customer_data['ship_country']) ? $customer_data['ship_country'] : '';
            $lll_ship .= '<br>';
            $lll_ship .= "<span>&#9742;<span>:  $customerPhone<br>";
            $lll_ship .= "<span>&#9993;<span>:  $customerEmail<br>";
        }

        $y = $pdf->getY();

        if ($BillingAdd) {
            $pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>" . $lll . '<br/>', 0, 0, 1, true, 'J', true);
        } else {
            $pdf->writeHTMLCell(80, 0, '', $y, '<b></b><br/><br/>', 0, 0, 1, true, 'J', true);
        }
        $pdf->SetTextColor(51, 51, 51);
        if ($ShippingAdd) {
            $pdf->writeHTMLCell(80, 0, '', '', '<b>Shipping Address</b>:<br/>' . $lll_ship . "<br/>", 0, 1, 1, true, 'J', true);
        } else {
            $pdf->writeHTMLCell(80, 0, '', '', '<b></b><br/><br/>', 0, 1, 1, true, 'J', true);
        }

        $pdf->setCellPaddings(1, 1, 1, 1);

        // set cell margins
        $pdf->setCellMargins(0, 1, 0, 0);
        // $pdf->writeHTML($tt1, true, false, true, false, '');
        $html = "\n\n\n" . '<h3></h3><table style="border: 1px solid #333333;" cellpadding="4"  >
            <tr style="border: 1px solid #333333;background-color:#3f3f3f;color:#FFFFFF;">

                <th style="border: 1px solid black;"colspan="2" align="center"><b>Product/Services</b></th>
                <th style="border: 1px solid black;" align="center"><b>Qty</b></th>
                <th style="border: 1px solid black;" align="center"><b>Unit Rate</b></th>
                    <th style="border: 1px solid black;" align="center"><b>Amount</b></th>

            </tr>';
        $html1     = '';
        $total_val = 0;
        $totaltax  = 0;
        $total     = 0;
        $tax       = 0;

        foreach ($invoice_items as $key => $item) {

            $total += $item['itemQty'] * $item['itemPrice'];

            $html .= '<tr style="border: 1px solid black;">

					 <td  style="border: 1px solid black;" colspan="2">' . $item['Name'] . '<br><br>' . $item['itemDescription'] . '</td>
					 <td  style="border: 1px solid black;"  align="center" >' . $item['itemQty'] . '</td>
					 <td  style="border: 1px solid black;" align="right">' . number_format($item['itemPrice'], 2) . '</td>
					 <td  style="border: 1px solid black;" align="right">' . number_format($item['itemQty'] * $item['itemPrice'], 2) . '</td>
				 </tr>';
        }
        $total_val = $total + $invoice_data['totalTax'];
        $total_tax = ($invoice_data['totalTax']) ? $invoice_data['totalTax'] : 0;
        $trate     = ($invoice_data['taxRate']) ? $invoice_data['taxRate'] : '0.00';
        $html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right">Tax ' . $trate . '%</td><td style="border: 1px solid black;" align="right">' . number_format($total_tax, 2) . '</td></tr>';
        $html .= '<tr><td colspan="4" align="right">Subtotal</td><td style="border: 1px solid black;" align="right">' . number_format($total, 2) . '</td></tr>';
        $html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right">Total</td><td style="border: 1px solid black;"  align="right">' . number_format($total_val, 2) . '</td></tr>';
        $html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right">Payment</td><td style="border: 1px solid black;" align="right">' . number_format($payamount, 2) . '</td></tr>';
        $html .= '<tr><td   colspan="4" align="right">Balance</td><td align="right">' . number_format(($invoice_data['BalanceRemaining']), 2) . '</td></tr> ';

        $email1 = ($company_data['merchantEmail']) ? $company_data['merchantEmail'] : '#';

        $html .= '</table>';

        $pdf->writeHTML($html, true, false, true, false, '');

        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.

        $pdf->Output($pdfFilePath, $mode);
        if ($mode == 'F') {
            return true;
        }

    }
	public function check_customer_merchant_data($customer_table, $args){
		
		$merchantColumn = $args[0];
		$where = $args['where'];
	    $this->db->select();
	    $this->db->from("$customer_table CT");
		$this->db->join("tbl_merchant_data TMD", "CT.$merchantColumn = TMD.merchID", 'INNER');
	    $this->db->where($where);
	    $this->db->where("TMD.isSuspend = 0 AND TMD.isDelete = 0 AND TMD.isEnable = 1");

	    $result = $this->db->get()->row_array();
		if($result && !empty($result)){
			return $result;
		}
		return false;
	}
	/**
     * Update Invoice on Surcharge Apply
     *
     * @param array $data
     * @param int
     * @return
    */
    public function updateSurchargeInvoice(array $data = [],int $type)
    {
        $merchID = $data['merchantID'];
        $condition = array('merchantID' => $merchID);
        $amount = $data['amount'];
        $merchant_surcharge = $this->get_row_data('tbl_merchant_surcharge', $condition);
        if($type == 1){
            $this->load->model('QBO_models/qbo_invoices_model');
	        
	        $accessToken = $data['accessToken'];
	        $refreshToken = $data['refreshToken'];
	        $realmID      = $data['realmID'];
	        
	        $inv_id = $data['inID'];
	        $invoice = $this->get_row_data('QBO_test_invoice', array('invoiceID' => $inv_id, 'merchantID' => $merchID, 'companyID' => $realmID));
	        $invoice_items = $this->qbo_invoices_model->get_item_data_new(array('invoiceID' => $inv_id, 'merchantID' => $merchID, 'releamID' => $realmID));

	        $targetInvoiceArray = $data['targetInvoiceArray'];
	        $dataService = $data['dataService'];
	        $total = 0;
	        $lineArray = [];
	        $addSurchargeItem = true;
	        foreach ($invoice_items as $key => $item) {
	            $amount = $item['itemPrice'] * $item['itemQty'];
	             
	            if($item['itemDescription'] == "Surcharge Fees"){
	                $addSurchargeItem = false;
	                $item['itemPrice'] = $item['itemPrice'] + $data['amount'];
	                $amount += $data['amount'];
	            }
	            $total += $amount;

	            $LineObj = Line::create([
	                "Amount" => $amount,
	                "DetailType" => "SalesItemLineDetail",
	                "Description" => $item['itemDescription'],
	                "SalesItemLineDetail" => [
	                    "ItemRef" => $item['itemRefID'],
	                    "Qty" => $item['itemQty'],
	                    "UnitPrice" => $item['itemPrice'],
	                    "TaxCodeRef" => [
	                        "value" => (isset($item['itemTax']) && $item['itemTax']) ? "TAX" : "NON",
	                    ]

	                ],
	            ]);
	            $lineArray[] = $LineObj;
	        }
	        if($addSurchargeItem){
	            $LineObj = Line::create([
	                "Amount" => $data['amount'],
	                "DetailType" => "SalesItemLineDetail",
	                "Description" => 'Surcharge Fees',
	                "SalesItemLineDetail" => [
	                    "ItemRef" => $merchant_surcharge['defaultItem'],
	                    "Qty" => 1,
	                    "UnitPrice" => $data['amount'],
	                    "TaxCodeRef" => [
	                        "value" => "NON",
	                    ]
	                ],
	            ]);
	            $lineArray[] = $LineObj;
	        }
	        
	        if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
	            $theInvoice = current($targetInvoiceArray);

	            $theResourceObj = Invoice::update($theInvoice, [
	                "Balance" => $total,
	                "TotalAmt" => $total,
	                "Line" => $lineArray,
	                "sparse" => true,
	                "MetaData" => [
	                    "LastUpdatedTime" => date('Y-m-d') . 'T' . date('H:i:s'),
	                ]
	            ]);
	            $resultingObj = $dataService->Update($theResourceObj);
	            $error = $dataService->getLastError();        
	        }
        }else if($type == 2){
        	$item_where = ['inv_itm_merchant_id' => $merchID, 'Descrip' => 'Surcharge Fees', 'TxnID' => $data['inID']];
	        $invoice_lineitem = $this->get_select_data('qb_test_invoice_lineitem', ['Rate'], $item_where);
	        $qb_test_item = $this->general_model->get_select_data('qb_test_item', ['FullName'], ['ListID' => $merchant_surcharge['defaultItem']]);

	        $invoice_lineitem_data = [
	        	'TxnID' => $data['inID'],
	        	'TxnLineID' => mt_rand(5000500, 9000900),
	        	'Item_ListID' => $merchant_surcharge['defaultItem'],
	        	'Item_FullName' =>  $qb_test_item['FullName'],
	        	'Descrip' => 'Surcharge Fees',
	        	'Quantity' => 1,
	        	'Rate' => $amount,
	        	'taxListID' => 0,
	        	'item_tax' => 0,
	        	'inv_itm_merchant_id' => $merchID,
	        ];
	        if($invoice_lineitem){
	        	$amount = $data['amount'] + $invoice_lineitem['Rate'];
	        	$invoice_lineitem_data['Rate'] = $amount;
	        	$this->update_row_data('qb_test_invoice_lineitem', $item_where, $invoice_lineitem_data);
	        }else{

	        	$this->insert_row('qb_test_invoice_lineitem', $invoice_lineitem_data);
	        }

	        $item_where = ['itemListID' => $merchant_surcharge['defaultItem'], 'itemDescription' => 'Surcharge Fees', 'invoiceDataID' => $data['inID']];
	        $subscription_invoice_item = $this->get_select_data('tbl_subscription_invoice_item', ['itemRate'], $item_where);

	        $invoice_item = [
	        	'itemListID' => $merchant_surcharge['defaultItem'],
	        	'itemFullName' => $qb_test_item['FullName'],
	        	'itemDescription' => 'Surcharge Fees',
	        	'itemQuantity' => 1,
	        	'itemRate' => $amount,
	        	'itemTax' => 0,
	        	'oneTimeCharge' => 0,
	        	'subscriptionID' => 0,
	        	'invoiceDataID' => $data['inID'],
	        	'itemLineID' => null,
	        ];

	        if($subscription_invoice_item){
	        	$invoice_item['itemRate'] = $amount;
	        	$this->update_row_data('tbl_subscription_invoice_item', $item_where, $invoice_item);
	        }else{
	        	$this->insert_row('tbl_subscription_invoice_item', $invoice_item);
	        }

	        $invdata = $this->get_select_data('qb_test_invoice', ['BalanceRemaining'], array('TxnID' => $data['inID'], 'qb_inv_merchantID' => $merchID));
	        $totalamount = $invdata['BalanceRemaining'] + $data['amount'];
	        $this->update_row_data('tbl_custom_invoice', array('insertInvID' => $data['inID'], 'merchantID' => $merchID), ['BalanceRemaining' => $totalamount]);

	        $this->update_row_data('qb_test_invoice', array('TxnID' => $data['inID'], 'qb_inv_merchantID' => $merchID), ['BalanceRemaining' => $totalamount]);

			include_once APPPATH .'libraries/QBD_Sync.php';

	        $QBD_Sync = new QBD_Sync($merchID);
			$QBD_Sync->invoiceSync([
				'invID' => $data['inID']
			]);
        }else if($type == 5){

	        $item_where = ['Item_ListID' => $merchant_surcharge['defaultItem'], 'Item_FullName' => 'Surcharge Fees', 'TxnID' => $data['inID']];
	        $invoice_lineitem = $this->get_select_data('chargezoom_test_invoice_lineitem', ['Rate'], $item_where);
	        $chargezoom_test_item = $this->general_model->get_select_data('chargezoom_test_item', ['FullName'], ['ListID' => $merchant_surcharge['defaultItem']]);
	        $invoice_item = [
	            'TxnID' => $data['inID'],
	            'Item_ListID' => $merchant_surcharge['defaultItem'],
	            'Item_FullName' => $chargezoom_test_item['FullName'],
	            'Descrip' => 'Surcharge Fees',
	            'Quantity' => 1,
	            'Rate' => $data['amount'],
	            'taxListID' => 0
	        ];
	        if($invoice_lineitem){
	            
	            $invoice_item['Rate'] = $data['amount'] + $invoice_lineitem['Rate'];
	            $this->update_row_data('chargezoom_test_invoice_lineitem', $item_where, $invoice_item);
	        }else{
	            $itemline = mt_rand(5000500, 9000900);
	            $invoice_item['TxnLineID'] = $itemline;
	            $this->insert_row('chargezoom_test_invoice_lineitem', $invoice_item);
	        }

	        $invdata = $this->get_select_data('chargezoom_test_invoice', ['BalanceRemaining'], array('TxnID' => $data['inID']));
	        $totalamount = $invdata['BalanceRemaining'] + $data['amount'];
	        $this->update_row_data('chargezoom_test_invoice', array('TxnID' => $data['inID']), ['BalanceRemaining' => $totalamount]);
        }
        
    }
    /**
     * Description: Send notification on success
     * @param double | $payAmount
     * @param string | $customerName
     * @param string | $customerID
     * @param int | $merchantID
     * @param string | $invoiceNumber
     * @return bool
     * */
    public function addNotificationForMerchant(float $payAmount,string $customerName,string $customerID,int $merchantID,string $invoiceNumber = null)
    {
    	
        /*Notification Saved*/
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
       
        if($invoiceNumber == null){
        	$title = 'Sale Payments';
        	$nf_desc = 'A payment for <b>'.$customerName.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'.';
        	$type = 1;
        }else{
        	$title = 'Invoice Checkout Payments';
        	$in_data =    $this->get_row_data('QBO_test_invoice', array('invoiceID'=>$invoiceNumber,'merchantID' => $merchantID));
            if(isset($in_data['refNumber'])){
                $invoiceRefNumber = $in_data['refNumber'];
            }else{
                $invoiceRefNumber = $invoiceNumber;
            }
        	$nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'';
        	$type = 2;
        }
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 2,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }   
    /**
     * Description: Send notification on failed
     * @param double | $payAmount
     * @param string | $customerName
     * @param string | $customerID
     * @param int | $merchantID
     * @param string | $invoiceNumber
     * @return bool
     * */
	public function failedNotificationForMerchant(double $payAmount,string $customerName,string $customerID,int $merchantID,string $invoiceNumber = null): bool
	{
		
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title = 'Failed Invoice Checkout payments';
        $in_data =    $this->get_row_data('QBO_test_invoice', array('invoiceID'=>$invoiceNumber,'merchantID' => $merchantID));
        if(isset($in_data['refNumber'])){
            $invoiceRefNumber = $in_data['refNumber'];
        }else{
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was attempted on '.$payDateTime.' but failed';
        $type = 2;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 2,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
}
