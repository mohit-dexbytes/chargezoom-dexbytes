<?php

    function create_card_customer($customer, $integrationType=1){
        $CI = &get_instance();
        
        if ($CI->session->userdata('logged_in')) {
			$user_id 				= $CI->session->userdata('logged_in')['merchID'];
		} else if ($CI->session->userdata('user_logged_in')) {
			$user_id 				= $CI->session->userdata('user_logged_in')['merchantID'];
        }

        $customerName = explode(' ', $customer['friendlyname'], 2);

        $fullname = $customer['friendlyname'];
        if(isset($customer['firstName']) && !empty($customer['firstName']) && isset($customer['lastName']) && !empty($customer['lastName'])){
            $fullname = $customer['firstName'] . ' ' .$customer['lastName'];
        }

        $input_data['firstName']   = (isset($customer['firstName']) && !empty($customer['firstName'])) ? $customer['firstName'] : $customerName[0];
        $input_data['lastName']	   = (isset($customer['lastName']) && !empty($customer['lastName'])) ? $customer['lastName'] : $customerName[1];
        $input_data['fullName']	   = $fullname;
        $input_data['userEmail']   = $customer['email'];
        $input_data['phoneNumber'] = (isset($customer['phone']) && $customer['phone']) ? $customer['phone'] : '';

        $input_data['address1']	   = (isset($customer['address1']) && $customer['address1']) ? $customer['address1'] : '';
        $input_data['address2']    = (isset($customer['address2']) && $customer['address2']) ? $customer['address2'] : '';
        $input_data['country']	   = (isset($customer['country']) && $customer['country']) ? $customer['country'] : '';
        $input_data['state']	   = (isset($customer['state']) && $customer['state']) ? $customer['state'] : '';
        $input_data['city']	       = (isset($customer['city']) && $customer['city']) ? $customer['city'] : '';
        $input_data['zipcode']	   = (isset($customer['zipcode']) && $customer['zipcode']) ? $customer['zipcode'] : '';

        $input_data['ship_country'] = (isset($customer['bcountry']) && $customer['bcountry']) ? $customer['bcountry'] : '';
        $input_data['ship_state']	= (isset($customer['bstate']) && $customer['bstate']) ? $customer['bstate'] : '';
        $input_data['ship_city']	= (isset($customer['bcity']) && $customer['bcity']) ? $customer['bcity'] : '';
        $input_data['ship_address1'] = (isset($customer['baddress1']) && $customer['baddress1']) ? $customer['baddress1'] : '';
        $input_data['ship_address2'] = (isset($customer['baddress2']) && $customer['baddress2']) ? $customer['baddress2'] : '';
        $input_data['ship_zipcode']	 = (isset($customer['bzipcode']) && $customer['bzipcode']) ? $customer['bzipcode'] : '';
        
        $input_data['companyName']	 = (isset($customer['companyName']) && !empty($customer['companyName'])) ? $customer['companyName'] : $fullname;
        $input_data['isActive']                = 'true';
        
        $comp_data = $CI->general_model->get_row_data('tbl_company', array('merchantID' =>	$user_id));
        $user = 	$comp_data['qbwc_username'];
        $input_data['companyID']        = 	$comp_data['id'];

        $input_data['merchantID']        = $user_id;
        $input_data['createdat']         = date('Y-m-d H:i:s');
        
        $comp_data = $CI->general_model->get_row_data('tbl_company', array('merchantID' =>	$user_id));

        if($integrationType == 1) {
            $user = 	$comp_data['qbwc_username'];
            
            $CI->load->config('quickbooks');
            $CI->load->model('quickbooks');
    
            $CI->quickbooks->dsn('mysqli://' . $CI->db->username . ':' . $CI->db->password . '@' . $CI->db->hostname . '/' . $CI->db->database);

            $input_data['qb_action'] = "Add Customer";
            $customerID = $c_List = mt_rand(4000000, 9000000);
            $input_data['customListID'] = $c_List;

            $cusID = 	 $CI->general_model->insert_row('tbl_custom_customer', $input_data);
            $login = array(
                'customerID' => $cusID, 'merchantID' => $user_id, 'createdAt' => date('Y-m-d H:i:s'),
                'customerEmail' => $input_data['userEmail'], 'overDue' => 0,
                'customerPassword' => md5($input_data['userEmail']), 'isEnable' => '1', 'customerUsername' => $input_data['userEmail']
            );
            $cus111ID = 	 $CI->general_model->insert_row('tbl_customer_login', $login);


            $CI->quickbooks->enqueue(QUICKBOOKS_ADD_CUSTOMER, $cusID, '1', '', $user);

            $cusom_data = array(
                'ListID' => $c_List, 'FirstName' => $input_data['firstName'], 'LastName' => $input_data['lastName'], 'FullName' => $input_data['fullName'],
                'Contact' => $input_data['userEmail'], 'companyName' => $input_data['companyName'], 'companyID' => $input_data['companyID'],

                'ShipAddress_Addr1' => $input_data['ship_address1'], 'ShipAddress_Addr2' => $input_data['ship_address2'], 'ShipAddress_City' => $input_data['ship_city'], 'ShipAddress_State' => $input_data['ship_state'],
                'ShipAddress_PostalCode' => $input_data['ship_zipcode'], 'ShipAddress_Country' => $input_data['ship_country'],
                'BillingAddress_Addr1' => $input_data['address1'], 'BillingAddress_Addr2' => $input_data['address2'], 'BillingAddress_State' => $input_data['state'],
                'BillingAddress_City' => $input_data['city'], 'BillingAddress_Country' => $input_data['country'], 'BillingAddress_PostalCode' => $input_data['zipcode'],



                'qbmerchantID' => $user_id, 'qb_status' => 0, 'customerStatus' => 1, 'IsActive' => 'true', 'qb_status' => 0, 'qbAction' => $input_data['qb_action'],
                'Phone' => $input_data['phoneNumber'], 'TimeCreated' => date('Y-m-d H:i:s'), 'TimeModified' => date('Y-m-d H:i:s')
            );

            $CI->general_model->insert_row('qb_test_customer', $cusom_data);
        }

        return $customerID;
    }

    function check_free_plan_transactions(){
        $CI = &get_instance();
        
        if ($CI->session->userdata('logged_in')) {
			$merchantID 				= $CI->session->userdata('logged_in')['merchID'];
		} else if ($CI->session->userdata('user_logged_in')) {
			$merchantID 				= $CI->session->userdata('user_logged_in')['merchantID'];
        }

        $successTransaction = $CI->general_model->success_transaction_count($merchantID);
        $plantype = $CI->general_model->chk_merch_plantype_status($merchantID);
        if(!$plantype || strtolower($plantype['merchant_plan_type']) != 'free'){
            return true;
        }
        
        $allowedTransaction = $plantype['allowed_transactions'];
        
        if($successTransaction >= $allowedTransaction){
            return false;
        } else {
            return true;
        }
    }

    function replace_email_tags($args){
        $CI = &get_instance();
        
        $currency            = "$";
        $customer            = '';
        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $tr_date             = '';
        $update_link         = '';
        $cust_company        = '';
        
        #Merchant Data Changes
        $merchantName = $merchantCompany = $merchantEmail = $merchantPhone = $config_email = ''; 
        $logo_url         = '';

        $customerName = $customerCompany = $customerEmail = $customerPhone = ''; 

        if(isset($args['merchantID'])){
            $merchantID = $args['merchantID'];

            $merchant_data = $CI->general_model->get_merchant_data(array('merchID' => $merchantID));
            $config_data   = $CI->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchantID));

            $config_email        = $merchant_data['merchantEmail'];
            $merchantCompany       = $merchant_data['companyName'];
            $merchantName       = $merchant_data['firstName'] . ' ' .$merchant_data['lastName'];
            $logo_url            = $merchant_data['merchantProfileURL'];
            $merchantPhone              = $merchant_data['merchantContact'];
            $update_link         = $config_data['customerPortalURL'];

            if (!empty($config_data['ProfileImage'])) {
                $logo_url = base_url() . LOGOURL . $config_data['ProfileImage'];
            } else {
                $logo_url = CZLOGO;
            }

            $appIntegration   = $CI->general_model->get_row_data('app_integration_setting', array('merchantID' => $merchantID));
            $active_app = $appIntegration['appIntegration'];

            #Customer Data Changes
            if(isset($args['customerID']) && $args['customerID']){
                $customerID = $args['customerID'];

                if($active_app == 1){
                    $customerData = $CI->general_model->get_select_data('QBO_custom_customer', array('fullName', 'companyName','userEmail', 'phoneNumber'), array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

                    $customerCompany = $customerData['companyName'];
                    $customerName = $customerData['fullName'];
                    $customerEmail = $customerData['userEmail'];
                    $customerPhone = $customerData['phoneNumber'];
                } else if($active_app == 2){
                    $customerData = $CI->general_model->get_select_data('qb_test_customer', array('companyName', 'FullName', 'Contact', 'Phone'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
        
                    $customerCompany = $customerData['companyName'];
                    $customerName = $customerData['FullName'];
                    $customerEmail = $customerData['Contact'];
                    $customerPhone = $customerData['Phone'];

                } else if($active_app == 3){
                    $customerData = $CI->general_model->get_select_data('Freshbooks_custom_customer', array('fullName', 'companyName','userEmail', 'phoneNumber'), array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

                    $customerCompany = $customerData['companyName'];
                    $customerName = $customerData['fullName'];
                    $customerEmail = $customerData['userEmail'];
                    $customerPhone = $customerData['phoneNumber'];
                } else if($active_app == 4){

                } else if($active_app == 5){
                    $customerData = $CI->general_model->get_select_data('qb_test_customer', array('companyName', 'FullName', 'Contact', 'Phone'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                    $customerCompany = $customerData['companyName'];
                    $customerName = $customerData['FullName'];
                    $customerEmail = $customerData['Contact'];
                    $customerPhone = $customerData['Phone'];
                }
            }
        }

        $mailText = $args['mailText'];

        #Default Data
        $mailText = stripslashes(str_ireplace('{{transaction.currency_symbol}}', $currency, $mailText));
        $mailText = stripslashes(str_ireplace('{{current.date}}', $cur_date, $mailText));

        #Merchant Data
        $mailText = stripslashes(str_ireplace('{{merchant_name}}', $merchantName, $mailText));
        $mailText = stripslashes(str_ireplace('{{merchant_email}}', $config_email, $mailText));
        $mailText = stripslashes(str_ireplace('{{merchant_phone}}', $mphone, $mailText));
        $mailText = stripslashes(str_ireplace('{{logo}}', "<img src='" . $logo_url . "'>", $mailText));

        #Customer Data
        $mailText = stripslashes(str_ireplace('{{customer.company}}', $customerCompany, $mailText));

        #Invoice Data
        $mailText = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $mailText));
        $mailText = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $mailText));

        #Card Detail
        $mailText = stripslashes(str_ireplace('{{creditcard.mask_number}}', $cardno, $mailText));
        $mailText = stripslashes(str_ireplace('{{creditcard.type_name}}', $friendly_name, $mailText));
        $mailText = stripslashes(str_ireplace('{{creditcard.url_updatelink}}', $update_link, $mailText));

        #Transaction Details
        $mailText = stripslashes(str_ireplace('{{transaction.amount}}', ($balance) ? ('$' . number_format($balance, 2)) : '0.00', $mailText));
        $mailText = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $mailText));

        #Invoice Details
        $mailText = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $mailText));
        $mailText = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $mailText));
        $mailText = stripslashes(str_ireplace('{{invoice.balance}}', '$' . number_format($balance, 2), $mailText));
        $mailText = stripslashes(str_ireplace('{{invoice.due_date|date("F j, Y")}}', $duedate, $mailText));
        $mailText = stripslashes(str_ireplace('{{invoice.days_overdue}}', $overday, $mailText));
        $mailText = stripslashes(str_ireplace('{{invoice.url_permalink}}', $in_link, $mailText));
        $mailText = stripslashes(str_ireplace('{{invoice_payment_pagelink}}', $in_link, $mailText));

        return $mailText;
    }

    function prorataCalculation($args){
        $paycycle = $args['paycycle'];
        $in_num = $args['in_num'];
        if($in_num === false){
            $in_num = 0;
        }
        
        $date = $next_date = $args['date'];
        $proRate = $args['proRate'];
        $nextInvoiceDate = $args['nextInvoiceDate'];
        $proRateday = $args['proRateday'];

        $total_amount = (isset($args['total_amount'])) ? $args['total_amount'] : 0;
        $prorataCalculatedRate = 1;

        switch ($paycycle) {
            case 'dly':
                $in_num    = ($in_num !== false) ? $in_num + 1 : '1';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
                break;

            case '1wk':
                $in_num    = ($in_num !== false) ? $in_num + 1 : '1';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                break;

            case '2wk':
                $in_num    = ($in_num !== false) ? $in_num + 1 : '1';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                break;

            case '2wk':
                $in_num    = ($in_num !== false) ? $in_num + 2 : '1';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                break;

            case 'mon':
                if ($proRate == 0 || $in_num != 0) {
                    $in_num    = ($in_num !== false) ? $in_num : '1';
                    $in_num    = $in_num + 1;
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                } else {
                    $first_invoice_day = date('d', strtotime($date));
                    $monthdays = date("t",strtotime($date));
                    $first_invoice_last_day = date('Y-m-t', strtotime($date));
                    $ndate2        = new DateTime($first_invoice_last_day);
                    $includeNextMonth = 0;
                    if($first_invoice_day < $proRateday){
                        $ndate2        = new DateTime(date("Y-m-$proRateday"));
                        $next_date = date('Y-m-' . $proRateday);
                    } else if($first_invoice_day > $nextInvoiceDate){
                        $in_num = 2;
                        $includeNextMonth = 1;
                        $next_date = date('Y-m-' . $proRateday, strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    } else {
                        $in_num = 1;
                        $next_date = date('Y-m-' . $proRateday, strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
    
                    $ndate1        = new DateTime($date);
                    $diff          = $ndate2->diff($ndate1);
                    $remainingdays = $diff->format("%a") + 1;
                    
                    $prorataCalculatedRate = ( 1 / $monthdays) * $remainingdays;
                    $prorataCalculatedRate = round($prorataCalculatedRate,2);
                    $total_amount                 = $args['recurring'] * $prorataCalculatedRate;
                    if($includeNextMonth == 1){
                        $total_amount += $args['recurring'];
                        $prorataCalculatedRate = $prorataCalculatedRate + 1;
                    }
                    $total_amount += $args['onetime'];
                }
                break;

            case '2mn':
                $in_num    = ($in_num !== false) ? $in_num + 2 * 1 : '1';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                break;

            case 'qtr':
                $in_num    = ($in_num !== false) ? $in_num + 3 * 1 : '1';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                break;

            case 'six':
                $in_num    = ($in_num !== false) ? $in_num + 6 * 1 : '1';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                break;

            case 'yrl':
                $in_num    = ($in_num !== false) ? $in_num + 12 * 1 : '0';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                break;

            case '2yr':
                $in_num    = ($in_num !== false) ? $in_num + 2 * 12 * 1 : '0';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                break;

            case '3yr':
                $in_num    = ($in_num !== false) ? $in_num + 3 * 12 * 1 : '0';
                $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                break;
            
            default:
                $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
                break;
        }

        $response = [
            'total_amount' => round($total_amount,2),
            'next_date' => $next_date,
            'prorataCalculatedRate' => $prorataCalculatedRate
        ];

        return $response;
    }

    function xero_sync_log($args, $merchantID){
        $CI = &get_instance();

        $input_data = [
            'xeroAction' => $args['action'],
            'xeroStatus' => $args['status'],
            'xeroText' => $args['message'],
            'xeroActionID' => (isset($args['xeroActionID'])) ? $args['xeroActionID'] : '',
            'merchantID' => $merchantID,
            'createdAt' => date("Y-m-d H:i:s"),
            'updatedAt' => date("Y-m-d H:i:s"),
        ];
        
        $logID = 	 $CI->general_model->insert_row('tbl_xero_log', $input_data);
        if($logID){
            $actionID = 1000 + $logID;
            if($args['syncFromCZ']){
                $xeroActionID = "CX#$actionID";
            } else {
                $xeroActionID = "XC#$actionID";
            }
            $CI->general_model->update_row_data('tbl_xero_log', [ 'id' => $logID ], [ 'xeroSyncID' => $xeroActionID ]);
        }
        
        return true;
    }

    function is_a_valid_customer($args=[]){
		
        if(empty($args['email']) || !filter_var($args['email'], FILTER_VALIDATE_EMAIL)){
              return false;
        }
        $searchCustomer = [
            'QBO_custom_customer' => [
                'merchantID',
                'where' => ['userEmail' => $args['email']]
            ],
            'qb_test_customer' => [
                'qbmerchantID',
                'where' => ['Contact' => $args['email']]
            ],
            'Freshbooks_custom_customer' => [
                'merchantID',
                'where' => ['userEmail' => $args['email']]
            ],
            'Xero_custom_customer' => [
                'merchantID',
                'where' => ['userEmail' => $args['email']]
            ],
            'chargezoom_test_customer' => [
                'qbmerchantID',
                'where' => ['Contact' => $args['email']]
            ],
        ];
        
        $isValid = false;
        $CI = &get_instance();
		foreach($searchCustomer as $table_name => $value){

            $customer = $CI->general_model->check_customer_merchant_data("$table_name", $value);
            if($customer){
                $isValid = true;
                break;
            }
        }

		return $isValid;
	}

    /**
     * Trim and remove white space
     *
     * @param string $name
     * @return string
     */
    function getSanitizedName(string $name) : string
    {
        $name = preg_replace('/\s+/', ' ', $name);
        $name = trim($name);

        return $name;
    }
?>