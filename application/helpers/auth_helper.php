<?php
/**
 * Provides authorized access to the system for a user, based on the provided
 * credentials, using a query to the database. If the authorization is
 * successful, a unique JSON Web Token is generated and stored in a cookie.
 * @param  $table - The database table to query.
 * @param $fields - An array of names for the fields to be requested.
 * @param $username_field - The name of the username field.
 * @param $password_field - The name of the password field.
 * @param $id_field - The name of the id field.
 * @param $username_value - The value of the username field.
 * @param $password_value - The value of the password field.
 * @param $service_name - The name of the service.
 * @param $cookie_name - The name of the cookie used to store the authorization
 *  token.
 * @return associative array
 */
 
/**************************Self Created Code******************/


function auth_api( $username_value, $password_value,$service,$domain, $sc_key,$mID){
  // Load the appropriate helpers
  $ci =& get_instance(); 
  $ci->load->model('Auth_api_model','auth_api_model');
  $ci->load->helper('error_code');
  $user= $ci->auth_api_model->get_user_all_login($username_value,$password_value,$mID);
  if($user == false)
    return array(
      "code" => BAD_CREDENTIALS,
      "message"=>"No user with this username."
    );

  if($user){
	   $company=0;
		
	   $token =generate_jwt_token($username_value, $user['id'],$mID, $user['role_type'],$service, $company, $domain, $sc_key);
    return  array('code'=>SUCCESS,'token'=>$token, "message" => "Token generated successfully.");
  }
  else
    return array(
      "code" => BAD_CREDENTIALS,
      "message"=>"Password is incorrect."
    );
}


function generate_jwt_token($username_value, $id_value,$mID, $role, $service, $company, $domain, $sckey)
{
  $secret = $sckey;

  $timestamp = date_timestamp_get(date_create());
  mt_srand(intval(substr($timestamp,-16,12)/substr(join(array_map(function ($n) { return sprintf('%03d', $n); }, unpack('C*', $secret))),0,2)));
  $stamp_validator = mt_rand();
  $time =time();
  $exp = $time+(2*60*60);
 
  $token = array(
    "iat" => $timestamp,
    "chk" => $stamp_validator,
	"start"=>$time,
	"expire"=>$exp,
    "username" => $username_value,
	'mid'=>$mID,
    "id" => $id_value,
    "service" => $service,
	 "company" => $company,
	"domain" => $domain,
	'role'=>$role,
  );
  
  return jwt_encode($token, $secret);
 
}


function regenerate_jwt_token($token, $secret)
{
  // Load the appropriate helpers
  $ci =& get_instance(); $ci->load->helper('jwt'); $ci->load->helper('error_code');

  if($token=="")
    return array(
      "code" => BAD_DATA,
      "message" => "Token not found."
    );

     $token = (array)jwt_decode($token, $secret);
      if($token["role"]=='m_customer')
	  {
         $con =array('loginID'=>$token["id"]);		  
		 $table ='tbl_customer_login'; 
      }
      if($token["role"]=='m_user')
	  {
         $con   = array('merchantUserID'=>$token["id"]);	
		 $table = 'tbl_merchant_user'; 
      }
	    if($token["role"]=='merchant')
	  {
         $con   = array('merchID'=>$token["mid"]);	
		 $table = 'tbl_merchant_data'; 
      }
	  $status = $ci->auth_api_model->check_existing_user($table, $con);
	 if($status)
	 {	 
 
       $new_token = generate_jwt_token($token["username"], $token["id"],$token["mid"], $token["role"],$token["service"], $token["company"],  $token["domain"], $secret);
      return  array('code'=>SUCCESS,'token'=>$new_token, "message" => "Token regenerated successfully.");
	 }else{
		  return  array('code'=>BAD_DATA, "message" => "Invalid request"); 
	 }
 
}




/**
 * Gets the data stored in a unique JSON Web Token.
 * @param $token - The name of the token used to store the authorization
 *  token.
 * @return associative array
 */
function get_jwt_data_api($token, $secret)
{
  // Load the appropriate helpers
  $ci =& get_instance(); 
  $ci->load->helper('jwt'); 
  $ci->load->helper('error_code');
  $ci->load->model('Auth_api_model','auth_api_model');
 
  if($token=='')
    return array(
      "code" => UNAUTHORIZED,
      "message" => "Token not found."
    );

  
 $t_api =  (array)jwt_decode($token, $secret);
 if(time()< $t_api['expire'])
 {
	 
	 if($t_api['role']=='m_user')
	 {	 
	   $auth = $ci->auth_api_model->get_user_auth_data($t_api['id'],$t_api['mid']);
	   $t_api['auth'] = $auth;
	 }
	 if($t_api['role']=='merchant')
		  $t_api['auth'] = "Merchant";
	 if($t_api['role']=='m_customer')
		  $t_api['auth'] = "Customer";
	 $t_api['code']=SUCCESS;
	return  $t_api;
 }else{
return  array(
      "code" => BAD_CREDENTIALS,
      "message"=>"Token has expired."
    );
 } 
}


?>
