<?php
	
	include_once APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
	use iTransact\iTransactSDK\iTTransaction;

	function mymessage($message)
	{
    echo '
		<div class="alert alert-success">
			<button data-dismiss="alert" class="close" type="button">×</button> '.$message.'
		</div>
		';
		
	}
	function getmessage($error)
	{
		echo '
		<div class="alert alert-danger">
			<button data-dismiss="alert" class="close" type="button">×</button> '.$error.'
		</div>
		';
	}

	function get_names(){
		$CI = &get_instance();
		$resellerID = 0;
		$companyName = $firstName = $lastName = $userEmail = $resellerID = $notificationRead = '';
		if ($CI->session->userdata('logged_in')) {
			//Merchant Details
			$user_info2	= $CI->session->userdata('logged_in');
			
			$user_id = $user_info2['merchID'];
			$firstName = $user_info2['firstName'];
			$lastName = $user_info2['lastName'];
			$resellerID = $user_info2['resellerID'];
			$companyName = $user_info2['companyName'];
			$userEmail = $user_info2['merchantEmail'];

			$con = array('merchID' => $user_id);
			$userData = $CI->general_model->get_row_data('tbl_merchant_data', $con);
			if ($userData) {
				$firstName = $userData['firstName'];
				$lastName = $userData['lastName'];
				$companyName = $userData['companyName'];
				$notificationRead = $userData['notification_read'];
				$userEmail = $userData['merchantEmail'];
			}
		}
		else if ($CI->session->userdata('user_logged_in')) {
			//Merchant User Details
			$user_info2		= $CI->session->userdata('user_logged_in');

			$user_id = $user_info2['merchantUserID'];
			$firstName = $user_info2['userFname'];
			$lastName = $user_info2['userLname'];
			$userEmail = $user_info2['userEmail'];
			
			$con = array('merchantUserID' => $user_id);
			$userData = $CI->general_model->get_row_data('tbl_merchant_user', $con);
			if ($userData) {
				$firstName = $userData['userFname'];
				$lastName = $userData['userLname'];
				$userEmail = $userData['userEmail'];
			}

			$con1 = array('merchID' => $user_info2['merchantID']);
			$userData1 = $CI->general_model->get_row_data('tbl_merchant_data', $con1);
			if ($userData1) {
				$resellerID = $userData1['resellerID'];
				$companyName = $userData1['companyName'];
				$notificationRead = $userData1['notification_read'];
			}
		}

		return ['firstName' => $firstName, 'lastName' => $lastName, 'userEmail' => $userEmail, 'resellerID' => $resellerID, 'companyName' => $companyName,'notification_read' => $notificationRead];
	}
	
	
	
	function get_services()
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_table('*', 'services');
		return $service;
	}
	
	



	function get_services_by_id($id)
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_allservices_by_id($id);
		return $service;
	}
	
	
	function get_uni_data($tbl,$col,$val,$return)
	{
		$CI =& get_instance();
		$con = array($col => $val);
		$CI->load->model('general_model');
		$service = $CI->general_model->get_row_data($tbl,$con);
		return $service["$return"];
	}

	function get_merchant_plan($merchID){
		$CI = &get_instance();
		$plantype = $CI->general_model->chk_merch_plantype_status($merchID);
		return $plantype;
	}

	function prepareMailList($templates, $merchID) {
		$merchant_plan = get_merchant_plan($merchID);
		if (!empty($merchant_plan) && !empty($templates)) {
			foreach ($templates as $key => $value) {
				if($merchant_plan['merchant_plan_type'] == 'VT')
					$exists = ['msgtemplate_refund', 'msgtemplate_receipt'];
				else
					$exists = ['msgtemplate_invoicepastdue', 'msgtemplate_invoice_upcoming', 'msgtemplate_failedreceipt', 'msgtemplate_receipt', 'msgtemplate_refund'];
				if(!in_array($value['typeName'], $exists)) {
					unset($templates[$key]);
				}
			}
		} 
		return $templates;
	}

	function is_email_exists($args){
		$CI = &get_instance();
		$isExists = false;
		
		$merchant_condition = [
			'merchantEmail' => $args['email']
		];

		if (isset($args['merchID']) && !empty($args['merchID'])) {
			$merchant_condition['merchID != '] = $args['merchantID'];
		}

		$userData = $CI->general_model->get_row_data('tbl_merchant_data', $merchant_condition);
		if ($userData) {
			return 'Email already registered as merchant.';
		}

		$merchant_user_condition = [
			'userEmail' => $args['email']
		];

		if (isset($args['merchantUserID']) && !empty($args['merchantUserID'])) {
			$merchant_user_condition['merchantUserID != '] = $args['merchantUserID'];
		}

		$userData = $CI->general_model->get_row_data('tbl_merchant_user', $merchant_user_condition);
		if ($userData) {
			return 'Email already registered with another merchant.';
		}

		return $isExists;
	}

	function merchant_gateway_allowed($args){
		$CI = &get_instance();
		$isAllowed = false;
		
		$merchant_condition = [
			'merchID' => $args['merchID'],
			'allowGateway' => 1
		];

		$userData = $CI->general_model->get_row_data('tbl_merchant_data', $merchant_condition);
		if ($userData) {
			$isAllowed = true;
		}

		return $isAllowed;
	}
	
	function getGatewayName($args){
		$CI = &get_instance();
		$gateway = $CI->general_model->get_row_data('tbl_master_gateway', array('gateID' => $args));
		return $gateway;
	}

	function getGatewayController($args, $appType = 1){
		
		$url = '';
		switch ($appType) {
			case 1:
				$url = 'QBO_controllers/';
				break;
			case 2:
				$url = '';
				break;
			case 3:
				$url = 'FreshBooks_controllers/';
				break;
			case 4:
				$url = 'Xero_controllers';
				break;
			case 5:
				$url = 'company/';
				break;
			default:
				$url = '';
				break;
		}

		$gatewayType = 1;
		if(isset($args['gatewayType'])){
			$gatewayType = $args['gatewayType'];
		}

		$gatewayContorllers = 'Payments/';
		switch ($gatewayType) {
			case 1:
				$gatewayContorllers = 'Payments/';
				break;
			case 2:
				$gatewayContorllers = 'AuthPayment/';
				break;
			case 3:
				$gatewayContorllers = 'PaytracePayment/';
				break;
			case 4:
				$gatewayContorllers = 'PaypalPayment/';
				break;
			case 5:
				$gatewayContorllers = 'StripePayment/';
				break;
			case 6:
				$gatewayContorllers = 'UsaePay/';
				break;
			case 7:
				$gatewayContorllers = 'GlobalPayment/';
				break;
			case 8:
				$gatewayContorllers = 'CyberSource/';
				break;
			case 9:
				$gatewayContorllers = 'Payments/';
				break;
			case 10:
				$gatewayContorllers = 'iTransactPayment/';
				break;
			case 11:
				$gatewayContorllers = 'FluidpayPayment/';
				break;
			case 12:
				$gatewayContorllers = 'TSYSPayment/';
				break;
			case 13:
				$gatewayContorllers = 'BasysIQProPayment/';
				break;
			case 14:
				$gatewayContorllers = 'CardPointe/';
				break;
			case 15:
				$gatewayContorllers = 'PayarcPayment/';
				break;
			case 16:
				$gatewayContorllers = 'EPXPayment/';
				break;	
			default:
				$gatewayContorllers = 'Payments/';
				break;
		}

		return [
			$url,
			$url.$gatewayContorllers
		];
	}

	function alterTransactionCode($args){
		switch ($args['transactionGateway']) {
			case 1:
				if($args['transactionCode'] == 200) {
					$args['transactionCode'] = 401;
				}
				break;
			case 11:
				if($args['transactionCode'] == 200) {
					$args['transactionCode'] = 401;
				}
				break;
			default:
				
				break;
		}

		return $args;
	}

	function getGatewayNames($gatewayType){
		$gateway = 'Unknown';
		switch ($gatewayType) {
			case 1:
				$gateway = 'NMI';
				break;
			case 2:
				$gateway = 'Authorize.Net';
				break;
			case 3:
				$gateway = 'Paytrace';
				break;
			case 4:
				$gateway = 'Paypal';
				break;
			case 5:
				$gateway = 'Stripe';
				break;
			case 6:
				$gateway = 'USAePay';
				break;
			case 7:
				$gateway = 'Heartland';
				break;
			case 8:
				$gateway = 'Cybersource';
				break;
			case 9:
				$gateway = 'Chargezoom';
				break;
			case 10:
				$gateway = iTransactGatewayName;
				break;
			case 11:
				$gateway = FluidGatewayName;
				break;
			case 12:
				$gateway = TSYSGatewayName;
				break;
			case 13:
				$gateway = BASYSGatewayName;
				break;
			case 14:
				$gateway = 'CardPointe';
				break;
			case 15:
				$gateway = PayArcGatewayName;
			break;
			case MAVERICKGATEWAYID:
				$gateway = MaverickGatewayName;
				break;
			case 16:
				$gateway = EPXGatewayName;
				break;
			default:
				$gateway;
				break;
		}
		return $gateway;
	}

	function addlevelThreeDataInTransaction($data)
	{
		$CI = &get_instance();
		
		// check level three data enable or not
		$checkLevelEnable = $CI->general_model->get_select_data('tbl_merchant_gateway', array('enable_level_three_data'), array('merchantID' => $data['merchID'] , 'gatewayType' => $data['gateway']));
		$enable_level_three = (isset($checkLevelEnable['enable_level_three_data'])) ? $checkLevelEnable['enable_level_three_data'] : 0;
		
		$transaction = isset($data['transaction']) ? $data['transaction'] : [];

		$request_data = [];
		if($enable_level_three){
			if(isset($data['card_type']) && !empty($data['card_type']) && empty($data['card_no'])){
				$card_type = $data['card_type'];
			}else{
				// check card type
				$card_type  = $CI->general_model->getType($data['card_no']);
			}
			$card_type = strtolower($card_type);
			
			$level_three_data = $CI->general_model->get_row_data('merchant_level_three_data', array('merchant_id'=> $data['merchID'], 'card_type' => ($card_type == 'visa') ? 'visa' : 'master'));
			if(!empty($level_three_data)){
				// if card type visa and master then add level three data
				if($data['gateway'] == 3){
					if($card_type == 'visa'){
						$url = 'https://api.paytrace.com/v1/level_three/visa';
						$request_data = [
							'transaction_id' => $data['transaction_id'],
							'integrator_id' => $data['integrator_id'],
							'invoice_id' => $data['invoice_id'],
							'customer_reference_id' => $level_three_data['customer_reference_id'],
							'tax_amount' => $level_three_data['local_tax'],
							'national_tax_amount' => $level_three_data['national_tax'],
							'merchant_tax_id' => $level_three_data['merchant_tax_id'],
							'customer_tax_id' => $level_three_data['customer_tax_id'],
							'commodity_code' => $level_three_data['commodity_code'],
							'discount_amount' => $level_three_data['discount_rate'],
							'freight_amount' => $level_three_data['freight_amount'],
							'duty_amount' => $level_three_data['duty_amount'],
							'source_address' => [
								'zip' => $level_three_data['source_zip']
				 			],
				 			'shipping_address' => [
								'zip' => $level_three_data['destination_zip'],
								'country' => $level_three_data['destination_country'],
				 			],
				 			'additional_tax_amount' => $level_three_data['addtnl_tax_freight'],
				 			'additional_tax_rate' => $level_three_data['addtnl_tax_rate'],
				 			'line_items' =>  [[
			 					'additional_tax_amount' => $level_three_data['addtnl_tax_amount'],
			 					'additional_tax_rate' => $level_three_data['line_item_addtnl_tax_rate'],
			 					'amount' => $data['amount'],
			 					'description' => $level_three_data['description'],
			 					'commodity_code' => $level_three_data['line_item_commodity_code'],
			 					'discount_amount' => $level_three_data['discount'],
			 					'product_id' => $level_three_data['product_code'],
			 					'quantity' => 1,
			 					'unit_of_measure' => $level_three_data['unit_measure_code'],
			 					'unit_cost' => $data['amount']
			 				]]
						];
					}else if($card_type == 'mastercard'){

						$url = 'https://api.paytrace.com/v1/level_three/mastercard';
						$request_data = [
							'transaction_id' => $data['transaction_id'],
							'integrator_id' => $data['integrator_id'],
							'invoice_id' => $data['invoice_id'],
							'customer_reference_id' => $level_three_data['customer_reference_id'],
							'tax_amount' => $level_three_data['local_tax'],
							'national_tax_amount' => $level_three_data['national_tax'],
							'freight_amount' => $level_three_data['freight_amount'],
							'duty_amount' => $level_three_data['duty_amount'],
							'source_address' => [
								'zip' => $level_three_data['source_zip']
				 			],
				 			'shipping_address' => [
								'zip' => $level_three_data['destination_zip'],
								'country' => $level_three_data['destination_country'],
				 			],
				 			'additional_tax_amount' => $level_three_data['addtnl_tax_amount'],
				 			'additional_tax_included' => ($level_three_data['addtnl_tax_indicator'] == 'Y') ? true : false,
				 			'line_items' => [
				 				[
				 					'additional_tax_amount' => $level_three_data['line_item_addtnl_tax_rate'],
				 					'additional_tax_included' => ($level_three_data['net_gross_indicator'] == 'Y') ? true : false,
				 					'additional_tax_rate' => $level_three_data['addtnl_tax_rate'],
				 					'amount' => $data['amount'],
				 					'debit_or_credit' => ($level_three_data['debit_credit_indicator'] == 'C') ? 'C' : 'D',
				 					'description' => $level_three_data['description'],
				 					'discount_amount' => $level_three_data['discount'],
				 					'discount_rate' => $level_three_data['discount_rate'],
				 					'discount_included' => true,
				 					'merchant_tax_id' => $level_three_data['merchant_tax_id'],
				 					'product_id' => $level_three_data['product_code'],
				 					'quantity' => 1,
				 					'tax_included' => true,
				 					'unit_of_measure' => $level_three_data['unit_measure_code'],
				 					'unit_cost' => $data['amount']
				 				]
				 			]
						];
					}

					if($request_data){
						$ch = curl_init();
				        curl_setopt($ch, CURLOPT_URL, $url);
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				        curl_setopt($ch, CURLOPT_HEADER, FALSE);

				        curl_setopt($ch, CURLOPT_POST, TRUE);

				        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_data));

				        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				            "Content-Type: application/json",
				            "Authorization: ".$data['token']
				        ));

				        $response = json_decode(curl_exec($ch), 1);
				        curl_close($ch);
				        return $response;
					}
		    	}elseif ($data['gateway'] == 1) {
		    		// nmi gateway code
	    			$level_three_data['unit_measure_code'] = (strtolower($level_three_data['unit_measure_code']) == 'each') ? 'EAC' : $level_three_data['unit_measure_code'];
	    			if($card_type == 'visa' || $card_type == 'mastercard'){

		    			if(isset($data['invoice_no']) && !empty($data['invoice_no'])){
		    				$transaction->addQueryParameter('ponumber', $data['invoice_no']);
		    			}
		    		}
		    		// add level three data in visa card
		    		if($card_type == 'visa'){
		    			$transaction->addQueryParameter('shipping', $level_three_data['freight_amount']);
		    			$transaction->addQueryParameter('shipping_country', $level_three_data['destination_country']);
		    			$transaction->addQueryParameter('shipping_postal', $level_three_data['destination_zip']);
						$transaction->addQueryParameter('ship_from_postal', $level_three_data['source_zip']);
						$transaction->addQueryParameter('summary_commodity_code', $level_three_data['commodity_code']);
		    			$transaction->addQueryParameter('duty_amount', $level_three_data['duty_amount']);
		    			$transaction->addQueryParameter('discount_amount', $level_three_data['discount_rate']);
		    			$transaction->addQueryParameter('national_tax_amount', $level_three_data['national_tax']);

		    			$transaction->addQueryParameter('vat_tax_amount', $level_three_data['addtnl_tax_freight']);
		    			$transaction->addQueryParameter('vat_tax_rate', $level_three_data['addtnl_tax_rate']);

		    			$transaction->addQueryParameter('customer_vat_registration', $level_three_data['customer_tax_id']);
		    			$transaction->addQueryParameter('merchant_vat_registration', $level_three_data['merchant_tax_id']);

						$transaction->addQueryParameter('item_product_code_1', $level_three_data['product_code']);
						$transaction->addQueryParameter('item_description_1', $level_three_data['description']);
						$transaction->addQueryParameter('item_commodity_code_1', $level_three_data['commodity_code']);
						$transaction->addQueryParameter('item_unit_of_measure_1', $level_three_data['unit_measure_code']);
						$transaction->addQueryParameter('item_unit_cost_1', $data['amount']);
						$transaction->addQueryParameter('item_quantity_1', 1);
						$transaction->addQueryParameter('item_total_amount_1', $data['amount']);

						$transaction->addQueryParameter('item_tax_amount_1', $level_three_data['addtnl_tax_amount']);
						$transaction->addQueryParameter('item_tax_rate_1', $level_three_data['line_item_addtnl_tax_rate']);
						
						$transaction->addQueryParameter('item_discount_amount_1', $level_three_data['discount']);

		    		}elseif ($card_type == 'mastercard') {
		    			// add level three data in master card
		    			$transaction->addQueryParameter('shipping', $level_three_data['freight_amount']);
		    			$transaction->addQueryParameter('shipping_country', $level_three_data['destination_country']);
		    			$transaction->addQueryParameter('shipping_postal', $level_three_data['destination_zip']);
						$transaction->addQueryParameter('ship_from_postal', $level_three_data['source_zip']);
		    			$transaction->addQueryParameter('duty_amount', $level_three_data['duty_amount']);
		    			$transaction->addQueryParameter('national_tax_amount', $level_three_data['national_tax']);

		    			$transaction->addQueryParameter('alternate_tax_amount', $level_three_data['addtnl_tax_amount']);
		    			$transaction->addQueryParameter('merchant_vat_registration', $level_three_data['merchant_tax_id']);

						$transaction->addQueryParameter('item_product_code_1', $level_three_data['product_code']);
						$transaction->addQueryParameter('item_description_1', $level_three_data['description']);
						$transaction->addQueryParameter('item_unit_of_measure_1', $level_three_data['unit_measure_code']);
						$transaction->addQueryParameter('item_unit_cost_1', $data['amount']);
						$transaction->addQueryParameter('item_quantity_1', 1);
						
						$transaction->addQueryParameter('item_total_amount_1', $data['amount']);
						$transaction->addQueryParameter('item_tax_amount_1', $level_three_data['line_item_addtnl_tax_rate']);
						$transaction->addQueryParameter('item_tax_rate_1', $level_three_data['addtnl_tax_rate']);
						
						$transaction->addQueryParameter('item_discount_amount_1', $level_three_data['discount']);
						$transaction->addQueryParameter('item_discount_rate_1', $level_three_data['discount_rate']);

						$transaction->addQueryParameter('item_tax_type_1', $level_three_data['addtnl_tax_type']);
		    		}
		    	}elseif ($data['gateway'] == 7){
			    	// add level three data in master card
		    		if($card_type == 'mastercard'){
			    	
		    			$commercialData = $data['levelCommercialData'];

						$lineItem = (object) [
						    'description' => $level_three_data['description'],
						    'productCode' => $level_three_data['product_code'],
						    'quantity' => 1,
						    'unitOfMeasure' => strtolower($level_three_data['unit_measure_code']),
						    'unitCost' => $data['amount'],
						];
						$commercialData->addLineItems($lineItem); 

						$transaction->transactionReference->transactionId = $data['transaction_id'];
						$transaction->commercialIndicator = 1;
						$transaction->cardType = 'MC';

		                $response = $transaction->edit()
		                ->withCommercialData($commercialData)
		                ->execute();
		                return $response;
		    		}else if($card_type == 'visa'){
			    		// add level three data in visa card
		    			$commercialData = $data['levelCommercialData'];
						$commercialData->freightAmount = $level_three_data['freight_amount'];
						$commercialData->dutyAmount =  $level_three_data['duty_amount'];
						$commercialData->discountAmount =  $level_three_data['discount_rate'];
						$commercialData->vatInvoiceNumber =  $data['invoice_id'];
						$commercialData->taxAmount =  $level_three_data['addtnl_tax_freight'];
						$commercialData->taxRate =  $level_three_data['addtnl_tax_rate'];

						$commercialData->summaryCommodityCode = $level_three_data['commodity_code'];
						$commercialData->destinationPostalCode = $level_three_data['destination_zip'];
						$commercialData->originPostalCode = $level_three_data['source_zip'];
						$commercialData->destinationCountryCode = $level_three_data['destination_country'];

						$lineItem = [
						    'commodityCode' => $level_three_data['line_item_commodity_code'],
						    'description' => $level_three_data['description'],
						    'productCode' => $level_three_data['product_code'],
						    'quantity' => 1,
						    'unitOfMeasure' => strtolower($level_three_data['unit_measure_code']),
						    'unitCost' => $data['amount'],
						    'taxAmount' => $level_three_data['addtnl_tax_amount'],
						    'taxPercentage' => $level_three_data['line_item_addtnl_tax_rate'],
						    // 'DiscountPerLineItem' => $level_three_data['discount'],
						    'totalAmount' => $data['amount'],
						];
						$commercialData->addLineItems($lineItem); 

						$transaction->transactionReference->transactionId = $data['transaction_id'];
						$transaction->commercialIndicator = 1;
						$transaction->cardType = 'Visa';

		                $response = $transaction->edit()
		                ->withCommercialData($commercialData)
		                ->execute();
		    			return $response;
		    		}
		    	}
		    }
		}
		if($data['gateway'] == 1){
			return $transaction;
		}else{
    		return false;
		}
    }
    
	// function for convert time 
	function getTimeBySelectedTimezone($timezone)
	{
		if($timezone['new_format'] == null || $timezone['new_format'] == ''){
			$timezone['new_format'] = 'America/Los_Angeles';
		}

		$date = new DateTime($timezone['time'], new DateTimeZone($timezone['current_format']));
		$date->setTimezone(new DateTimeZone($timezone['new_format']));
		return $date->format('Y-m-d H:i:s');
	}
	
	function get_merchant_notification($merchID,$limit){
		$CI = &get_instance();
		$condition  = array('receiver_id' => $merchID,'status' => 1);
		$orderBy = array('name' => 'created_at','type' => 'desc');
		$notification = $CI->general_model->get_table_data('tbl_merchant_notification',$condition,$orderBy,$limit);
		return $notification;
	}
	function get_merchant_notification_count($merchID){
		$CI = &get_instance();
		$condition  = array('receiver_id' => $merchID,'status' => 1);
		$orderBy = array('name' => 'created_at','type' => 'desc');
		$notification = $CI->general_model->get_table_data('tbl_merchant_notification',$condition,$orderBy);
		return count($notification);
	}
	function get_merchant_unread_notification($merchID){
		$CI = &get_instance();
		$condition  = array('receiver_id' => $merchID,'is_read' => 1);

		$notification = $CI->general_model->get_table_data('tbl_merchant_notification',$condition);
		
		return count($notification);
	}
	function time_elapsed_string($datetime, $full = false, $current_time = '') {
	    $now = new DateTime;
	    if($current_time){
	    	$now = new DateTime($current_time);	
	    }
	    
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}


	// function for sendEmailUsign Send Grid API
	if (!function_exists('sendEmailUsingSendGrid')) {
	    /**
	     * @param $request_payload
	     */
	    function sendEmailUsingSendGrid($request_payload = [])
	    {
	    	// create request for sendgrid V3 api for send email
			$request_array = [
				"personalizations" => [
				    [
				      "to" => [
				        [
				          "email" => $request_payload['to']
				        ]
				      ],
				      "subject" => $request_payload['subject']
				    ]
				],
				"from" => [
				    "email" => $request_payload['from'],
				    "name" => $request_payload['fromname']
				],
				"reply_to" => [
					"email" => $request_payload['replyto']
				],
				"content" => [
				    [
				      "type" => "text/html",
				      "value" => $request_payload['html']
				    ]
				]
			];

			if(isset($request_payload['addCC']) && !empty($request_payload['addCC'])){
				$addCC = $request_payload['addCC'];
				if(!is_array($addCC)){
					$addCC = explode(';', $addCC);
				}

				$toaddCCArrEmail = [];
				foreach ($addCC as $value) {
					$toaddCCArrEmail[]['email'] = trim($value);
				}
				$request_array['personalizations'][0]['cc'] = $toaddCCArrEmail;
			}

			if(isset($request_payload['addBCC']) && !empty($request_payload['addBCC'])){
				$addBCC = $request_payload['addBCC'];
				if(!is_array($addBCC)){
					$addBCC = explode(';', $addBCC);
				}

				$toaddBCCArrEmail = [];
				foreach ($addBCC as $value) {
					$toaddBCCArrEmail[]['email'] = trim($value);
				}
				$request_array['personalizations'][0]['bcc'] = $toaddBCCArrEmail;
			}
			
			if(isset($request_payload['attachments']) && !empty($request_payload['attachments'])){
				$attachments = [];
				foreach ($request_payload['attachments'] as $attachment) {
					$attachments[] = [
						'filename' => $attachment['filename'],
						'content' => base64_encode(file_get_contents($attachment['filepath']))
					];
				}
				$request_array['attachments'] = $attachments;
			}
			// get api key for send grid
	    	$api_key = SEND_GRID_API_KEY;
  			$url = 'https://api.sendgrid.com/v3/mail/send';

  			// set authorization header
			$headerArray = [
				'Authorization: Bearer '.$api_key,
				'Content-Type: application/json',
				'Accept: application/json'
			];
			
			$ch = curl_init($url);
			curl_setopt ($ch, CURLOPT_POST, true);
			curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, true);

			// add authorization header
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

			$response = curl_exec($ch);
		    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			
			// parse header data from response
			$header_text = substr($response, 0, $header_size);
			// parse response body from curl response
			$body = substr($response, $header_size);

			$header = createSendGridHeaders($header_text);

			// if mail sent success
			if($httpcode == '202' || $httpcode == '200'){
				$success = true;
			}else{
				$success = false;
			}
		    return ['success' => $success, 'response' => ['header' => $header, 'body' => $body]];
	    }

	}

	function getiTransactTransactionDetails($merchID,$transaction_id){
		$CI = &get_instance();
		$gt_result = $CI->general_model->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchID,'gatewayType' => 10));

        $apiUsername  = $gt_result['gatewayUsername'];
        $apiKey  = $gt_result['gatewayPassword']; 
        $isSurcharge  = $gt_result['isSurcharge']; 
        $surCharge = 0;
		$totalAmount =0;
		$payAmount = 0;
		$data = array('surCharge' => 0,'totalAmount' => 0,'payAmount' => 0,'isSurcharge' => $isSurcharge );
        if($isSurcharge){
        	$sdk = new iTTransaction();
			$payload = [];

	        $result1 = $sdk->getTransactionByID($apiUsername, $apiKey, $payload,$transaction_id);
	        
	        if ($result1['status_code'] == '200' || $result1['status_code'] == '201') {
	        	if(isset($result1['surcharge_amount']) && $result1['surcharge_amount'] != ''){
	        		$data['surCharge'] = $result1['surcharge_amount'] / 100;
	        	}
	        	$data['totalAmount'] = $result1['authorized_amount'] / 100;
	        	$data['payAmount'] = $result1['amount'] / 100;
	        }
        }

		return $data;
	}

	function getInvoiceOriginalID($invoice_ids, $merchantID, $merchantType)
	{
		$invoiceIDs = explode(',', $invoice_ids);
		$CI = &get_instance();
		$new_invoice = [];
		if($merchantType == 1){
			// QBO
			$CI->db->select('refNumber')->from('QBO_test_invoice');
			$CI->db->where('merchantID', $merchantID);
			$CI->db->where_in('invoiceID', $invoiceIDs);
			$result = $CI->db->get()->result_array();
			if($result){
				foreach ($result as $key => $value) {
					$new_invoice[] = $value['refNumber'];
				}
			}
		}else if($merchantType == 2){
			// QBD
			$CI->db->select('RefNumber')->from('qb_test_invoice');
			$CI->db->where('qb_inv_merchantID', $merchantID);
			$CI->db->where_in('TxnID', $invoiceIDs);
			$result = $CI->db->get()->result_array();
			if($result){
				foreach ($result as $key => $value) {
					$new_invoice[] = $value['RefNumber'];
				}
			}

		}else if($merchantType == 3){
			// FB
			$CI->db->select('refNumber')->from('Freshbooks_test_invoice');
			$CI->db->where('merchantID', $merchantID);
			$CI->db->where_in('invocieID', $invoiceIDs);
			$result = $CI->db->get()->result_array();
			if($result){
				foreach ($result as $key => $value) {
					$new_invoice[] = $value['refNumber'];
				}
			}
		}
		if($new_invoice){
			$invoices = implode(',', $new_invoice);
		}else{
			$invoices = implode(',', $invoiceIDs);
		}
		return str_replace(',', 'COMMA', $invoices);
	}
	function get_merchant_data($merchID){
		$CI = &get_instance();
		if($merchID != ''){
			$condition  = array('merchID' => $merchID);
		
			$merchantData = $CI->general_model->get_row_data('tbl_merchant_data',$condition);
		}else{
			$merchantData = [];
		}
		return $merchantData;
	}

	function getClientIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	if (!function_exists('priceFormat')) {
	    function priceFormat($price, $with_currency = false, $currency_pre = true)
	    {
	        if(!is_numeric($price)){
	            $price = (float)$price;
	        }
	        // If price is empty then set zero
	        if (empty($price)) $price = 0;

	        if ($with_currency) {
	            if ($currency_pre) {
	                if ($price < 0) {
	                    $price = abs($price);
	                    $price = number_format($price, 2, '.', ',');
	                    $price = '-$' . $price;

	                } else {
	                    $price = number_format($price, 2, '.', ',');
	                    $price = '$' . $price;
	                }
	            } else {
	                $price = number_format($price, 2, '.', ',');
	                $price = $price . '$';
	            }
	        } else {
	            $price = number_format($price, 2, '.', '');
	        }

	        return $price;

	    }
	}
	function getTableRowLengthValue($length)
	{
		$returnValue = 10;
		if($length >= 0){
			$returnValue = $length;
		}
		return $returnValue;
	}
	function getTableRowOrderColumnValue($columnNumber)
	{
		$returnValue = 0;
		if($columnNumber >= 0){
			$returnValue = $columnNumber;
		}
		return $returnValue;
	}
	function getTableRowStartValue($start)
	{
		$returnValue = 0;
		if($start >= 0){
			$returnValue = $start;
		}
		return $returnValue;
	}
	function getTableRowOrderByValue($orderBy)
	{
		$returnValue = 'desc';
		if($orderBy == 'desc' || $orderBy == 'asc'){
			$returnValue = $orderBy;
		}
		return $returnValue;
	}
	/*
	* function to set email headers
	* @param: string 
	* @return: array()
	*/
	function createSendGridHeaders($header_string)
	{
		$headers = [];
		foreach (explode("\r\n", $header_string) as $i => $line){
	        if ($i === 0){
	            $headers['http_code'] = $line;
	       
	        }else{
	        	$explode_value = explode(': ', $line);
	        	$key = isset($explode_value[0]) ? $explode_value[0] : ''; 
	        	$value = isset($explode_value[1]) ? $explode_value[1] : '';
	        	if($key){
	            	$headers[$key] = $value;
	        	}
	        }
		}
		return $headers;
	}
	/*
	* function to convert and manage message response
	* @param: array $transaction, string $message
	* @return: array()
	*/
	function messageConversionObj(array $transaction,string $message)
	{
		$tcode = array('100','200', '120','111','1');
		$newMessage = 'Transaction Failed - Declined';
		$status = 'failed';
		$returnClass = 'faild';
		$returnObj = [];
		$returnColorCode = '#cf4436';
		$isSuccess = 0;
		

		if(isset($transaction) && !empty($transaction)){
			if(in_array($transaction['transactionCode'], $tcode)){
				$isSuccess = 1;
			}

			if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'refund') !== false) && $isSuccess == 1 && ($transaction['transaction_user_status'] != 3))
			{
				$newMessage = 'Transaction Refunded';
				$status = 'success';
			}else if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'auth') !== false) && $isSuccess == 1 && ($transaction['transaction_user_status'] != 3) && ($transaction['gateway'] != 'Auth') )
			{
				$newMessage = 'Authorization Successful';
				$returnClass = 'success';
				$status = 'success';
				$returnColorCode = '#259F9F';
			}else if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'auth') !== false) && $isSuccess == 0 && ($transaction['transaction_user_status'] != 3) && ($transaction['gateway'] != 'Auth'))
			{
				$newMessage = 'Authorization Failed';
				$status = 'success';
			}else if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'auth') !== false) && $isSuccess == 1 && ($transaction['transaction_user_status'] != 3) && ($transaction['gateway'] == 'Auth') )
			{
				$newMessage = 'Authorization Successful';
				$returnClass = 'success';
				$status = 'success';
				$returnColorCode = '#259F9F';
			}else if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'auth') !== false) && $isSuccess == 1 && ($transaction['transaction_user_status'] != 3) && ($transaction['gateway'] == 'Auth') )
			{
				$newMessage = 'Transaction Successful';
				$returnClass = 'success';
				$status = 'success';
				$returnColorCode = '#259F9F';
			}else if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'capture') !== false)  && $isSuccess == 1 && ($transaction['transaction_user_status'] != 3) && ($transaction['gateway'] != 'Auth') )
			{
				$newMessage = 'Capture Successful';
				$returnClass = 'success';
				$status = 'success';
				$returnColorCode = '#259F9F';
			}else if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'capture') !== false)  && $isSuccess == 1 && ($transaction['transaction_user_status'] != 3) && ($transaction['gateway'] == 'Auth') )
			{
				$newMessage = 'Capture Successful';
				$returnClass = 'success';
				$status = 'success';
				$returnColorCode = '#259F9F';
			}else if(isset($transaction['transactionType']) && (strpos(strtolower($transaction['transactionType']), 'capture') !== false )  && $isSuccess == 0 && ($transaction['transaction_user_status'] != 3)  && ($transaction['gateway'] != 'Auth'))
			{
				$newMessage = 'Capture Failed';
				$status = 'success';
			}else if(isset($transaction['transaction_user_status']) && ($transaction['transaction_user_status'] == 3)){
				$newMessage = 'Transaction Voided';
				$status = 'success';
			}else if($isSuccess == 1 && ($transaction['transaction_user_status'] != 3)){
				$newMessage = 'Transaction Successful';
				$returnClass = 'success';
				$status = 'success';
				$returnColorCode = '#259F9F';
			}else{
				if(!empty($message)){
					$newMessage = $message;
				}else if(!empty($transaction['transactionStatus'])){
					$newMessage = 'Transaction Failed - '.$transaction['transactionStatus'];
				}
			}
		}
		$returnObj['message'] = $newMessage;
		$returnObj['returnClass'] = $returnClass;
		$returnObj['returnColorCode'] = $returnColorCode;
		$returnObj['status'] = $status;
		return $returnObj;
	}
	/**
	 * Description: Convert customer data field name 
	 * 
	 * @param array | $customer_data, Int | $type
	 *
	 * @return array
	**/
	function convertCustomerDataFieldName($customer_details,$integrationType): array
	{
		$receipt_data = [];
		if($integrationType == 1 || $integrationType == 3 || $integrationType == 4){
			$receipt_data['billing_name'] = $customer_details['fullName'];
            $receipt_data['billing_address1'] = $customer_details['address1'];
            $receipt_data['billing_address2'] = $customer_details['address2'];
            $receipt_data['billing_city'] = $customer_details['City'];
            $receipt_data['billing_zip'] = $customer_details['zipCode'];
            $receipt_data['billing_state'] = $customer_details['State'];
            $receipt_data['billing_country'] = $customer_details['Country'];
            $receipt_data['shipping_name'] = $customer_details['fullName'];
            $receipt_data['shipping_address1'] = $customer_details['ship_address1'];
            $receipt_data['shipping_address2'] = $customer_details['ship_address2'];
            $receipt_data['shipping_city'] = $customer_details['ship_city'];
            $receipt_data['shipping_zip'] = $customer_details['ship_zipcode'];
            $receipt_data['shipping_state'] = $customer_details['ship_state'];
            $receipt_data['shiping_counry'] = $customer_details['ship_country'];
            $receipt_data['Phone'] = $customer_details['phoneNumber'];
            $receipt_data['Contact'] = $customer_details['userEmail'];
		}else if($integrationType == 2 || $integrationType == 5){
			$receipt_data['billing_name'] = $customer_details['FullName'];
            $receipt_data['billing_address1'] = $customer_details['BillingAddress_Addr1'];
            $receipt_data['billing_address2'] = $customer_details['BillingAddress_Addr2'];
            $receipt_data['billing_city'] = $customer_details['BillingAddress_City'];
            $receipt_data['billing_zip'] = $customer_details['BillingAddress_PostalCode'];
            $receipt_data['billing_state'] = $customer_details['BillingAddress_State'];
            $receipt_data['billing_country'] = $customer_details['BillingAddress_Country'];
            $receipt_data['shipping_name'] = $customer_details['FullName'];
            $receipt_data['shipping_address1'] = $customer_details['ShipAddress_Addr1'];
            $receipt_data['shipping_address2'] = $customer_details['ShipAddress_Addr2'];
            $receipt_data['shipping_city'] = $customer_details['ShipAddress_City'];
            $receipt_data['shipping_zip'] = $customer_details['ShipAddress_PostalCode'];
            $receipt_data['shipping_state'] = $customer_details['ShipAddress_State'];
            $receipt_data['shiping_counry'] = $customer_details['ShipAddress_Country'];
            $receipt_data['Phone'] = $customer_details['Phone'];
            $receipt_data['Contact'] = $customer_details['Contact'];

		}
		return $receipt_data;
	}
	/**
     * Save log data for request and response
     * 
     * @param string $str  xml data
     * @param string $title title of log 
     * @param string $fileName save file name of log 
     * @return None
    */
    function logData(string $str,string $title=null,string $fileName) : void
    {
        // Log for few days only
        $log = (date('Y-m-d') <= '2021-10-25');
        if($log){
            $fp = fopen('./uploads/'.$fileName.'.txt', 'a');

            fwrite($fp, ($title ? $title : '').':'."\r\n"."\r\n");
            fwrite($fp, $str);
            fwrite($fp, "\r\n".'===================================='.date('Y-m-d H:i:s').'=============================='."\r\n"."\r\n");
            fclose($fp);
        }
    }
	
