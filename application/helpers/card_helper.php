<?php
function save_qbo_card($cardDetails, $user_id)
{
    $CI = &get_instance();
    $card_type      = $CI->general_model->getType($CI->input->post('card_number'));
    $friendlyname   = $card_type . ' - ' . substr($CI->input->post('card_number'), -4);
    $card_condition = array(
        'customerListID'           => $cardDetails['customerID'],
        'customerCardfriendlyName' => $friendlyname,
    );

    $crdata = $CI->card_model->check_friendly_name($cardDetails['customerID'], $friendlyname);

    if (!empty($crdata)) {

        $card_data = array(
            'cardMonth'       => $cardDetails['expmonth'],
            'cardYear'        => $cardDetails['exyear'],
            'companyID'       => $cardDetails['companyID'],
            'merchantID'      => $user_id,
            'CardType'        => $CI->general_model->getType($cardDetails['card_no']),
            'CustomerCard'    => $CI->card_model->encrypt($cardDetails['card_no']),
            'CardCVV'         => '',
            'updatedAt'       => date("Y-m-d H:i:s"),
            'Billing_Addr1'   => $cardDetails['address1'],

            'Billing_City'    => $cardDetails['city'],
            'Billing_State'   => $cardDetails['state'],
            'Billing_Country' => $cardDetails['country'],
            'Billing_Contact' => $cardDetails['phone'],
            'Billing_Zipcode' => $cardDetails['zipcode'],
        );

        $id1 = $CI->card_model->update_card_data($card_condition, $card_data);
    } else {
        $card_data = array(
            'cardMonth'                => $cardDetails['expmonth'],
            'cardYear'                 => $cardDetails['exyear'],
            'CardType'                 => $CI->general_model->getType($cardDetails['card_no']),
            'CustomerCard'             => $CI->card_model->encrypt($cardDetails['card_no']),
            'CardCVV'                  => '', 
            'customerListID'           => $cardDetails['customerID'],
            'companyID'                => $cardDetails['companyID'],
            'merchantID'               => $user_id,
            'customerCardfriendlyName' => $friendlyname,
            'createdAt'                => date("Y-m-d H:i:s"),
            'Billing_Addr1'            => $cardDetails['address1'],

            'Billing_City'             => $cardDetails['city'],
            'Billing_State'            => $cardDetails['state'],
            'Billing_Country'          => $cardDetails['country'],
            'Billing_Contact'          => $cardDetails['phone'],
            'Billing_Zipcode'          => $cardDetails['zipcode'],
        );

        $id1 = $CI->card_model->insert_card_data($card_data);
    }

    return $id1;
}

function checkCustomerCard($customerID,$merchID)
{
    $CI = &get_instance();
    
    $crdata = $CI->card_model->get_customer_card_data($customerID);

    if(count($crdata) > 0){
        $card_data = array('is_default'=>0);
        $card_condition = array('customerListID' =>$customerID,'merchantID'=>$merchID );
        $CI->card_model->update_card_data($card_condition, $card_data);

        return 0;

    }else{
        return 0;
    }
    die;
    

    
}
