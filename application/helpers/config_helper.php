<?php


function template_variable()
{
	$re_name = '';
	$CI = &get_instance();
	$user_data = [];
	if ($CI->session->userdata('logged_in')) {
	
		$u_id 				=  $CI->session->userdata('logged_in')['merchID'];
		$CI->load->model('general_model');
		$re_name = $CI->general_model->get_reseller_name_data($u_id);
		
		
	}
	if ($CI->session->userdata('user_logged_in')) {
	
		$u_id 				= $CI->session->userdata('user_logged_in')['merchantID'];
		$CI->load->model('general_model');
		$re_name = $CI->general_model->get_reseller_name_data($u_id);
		
		
	}
	

	$template = array(
		'name'              => 'Chargezoom',
		'version'           => '0.1',
		'author'            => 'Chargezoom',
		'robots'            => 'noindex, nofollow',
		'title'             => ucfirst($re_name) . ' PayPortal - Cloud Accounting Automation Software',
		'description'       => ucfirst($re_name) . ' Payment Portal automates your payment processing and accounting reconciliation so you can focus on your business',
		// true                     enable page preloader
		// false                    disable page preloader
		'page_preloader'    => true,
		// true                     enable main menu auto scrolling when opening a submenu
		// false                    disable main menu auto scrolling when opening a submenu
		'menu_scroll'       => true,
		// 'navbar-default'         for a light header
		// 'navbar-inverse'         for a dark header
		'header_navbar'     => 'navbar-default',
		// ''                       empty for a static layout
		// 'navbar-fixed-top'       for a top fixed header / fixed sidebars
		// 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
		'header'            => '',
		// ''                                               for a full main and alternative sidebar hidden by default (> 991px)
		// 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
		// 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
		// 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
		// 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
		// 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
		// 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
		'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
		// ''                       empty for a static footer
		// 'footer-fixed'           for a fixed footer
		'footer'            => '',
		// ''                       empty for default style
		// 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
		'main_style'        => '',
		// ''                           Disable cookies (best for setting an active color theme from the next variable)
		// 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
		'cookies'           => '',
		// 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
		// 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
		'theme'             => 'night',
		// ''                       for default content in header
		// 'horizontal-menu'        for a horizontal menu in header
		// This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
		'header_content'    => '',
		'active_page'       => basename($_SERVER['REQUEST_URI'])
	);

	
	return $template;
}


function primary_nav()
{
	$CI = &get_instance();
	$status = $CI->session->userdata('logged_in');

	if (!isset($status['active_app'])) {

		@$status['active_app'] = 0;
	}

	$u_id = '';
	$service = '';
	$plantype = '';
	$gmode = 1;
	$gateway_count = 0;
	$r_id = 0;
	$p_id = 0;
	$gateWayMode = 0;
	if ($CI->session->userdata('logged_in')) {

		$status = $CI->session->userdata('logged_in');

		if (!isset($status['active_app'])) {
			@$status['active_app'] = 0;
		}

		$gmode = isset($CI->session->userdata('user_logged_in')['allowGateway']) ? $CI->session->userdata('user_logged_in')['allowGateway']: 1;
		$u_id 				=  $CI->session->userdata('logged_in')['merchID'];
		$r_id 				=  $CI->session->userdata('logged_in')['resellerID'];
		$p_id 				=  $CI->session->userdata('logged_in')['plan_id'];
		$CI->load->model('general_model');
		$service = $CI->general_model->chk_echeck_gateway_status($u_id);

		$plantype = $CI->general_model->chk_merch_plantype_status($u_id);
		$planData = $CI->general_model->chk_merch_plantype_data($u_id);

		$cond = array('merchID' => $u_id );
		$merchData = $CI->general_model->get_row_data('tbl_merchant_data',$cond);
		
		if($merchData['freeTrial']){
			$trial_expiry_date = $merchData['trial_expiry_date'];
			$currentDate = date('Y-m-d');

			if($trial_expiry_date >= $currentDate){
				$plantype = null;
				$gateWayMode = 1;
			}
		}
	}
	if ($CI->session->userdata('user_logged_in')) {

		$status = $CI->session->userdata('user_logged_in');

		if (!isset($status['active_app'])) {
			@$status['active_app'] = 0;
		}
		
		$gmode = isset($CI->session->userdata('user_logged_in')['allowGateway']) ? $CI->session->userdata('user_logged_in')['allowGateway']: 1;
		$u_id 				= $CI->session->userdata('user_logged_in')['merchantID'];
		$r_id 				=  $CI->session->userdata('logged_in')['resellerID'];
		$p_id 				=  $CI->session->userdata('logged_in')['plan_id'];
		$CI->load->model('general_model');
		$service = $CI->general_model->chk_echeck_gateway_status($u_id);
		$plantype = $CI->general_model->chk_merch_plantype_status($u_id);
		$planData = $CI->general_model->chk_merch_plantype_data($u_id);
		if($plantype == ''){
			$CI->session->set_userdata('vt_plan', '0');
		}else {
			$CI->session->set_userdata('vt_plan', '1');

		}
		$cond = array('merchID' => $u_id );
		$merchData = $CI->general_model->get_row_data('tbl_merchant_data',$cond);
		
		if($merchData['freeTrial']){
			$trial_expiry_date = $merchData['trial_expiry_date'];
			$currentDate = date('Y-m-d');
			if($currentDate >= $trial_expiry_date){
				$plantype = null;
				$gateWayMode = 1;
			}
		}
	}

	$showCCList = true;
	$cond = array('merchantID' => $u_id, 'creditCard' => 1 );
	$ccData = $CI->general_model->get_row_data('tbl_merchant_gateway',$cond);
	if(!$ccData){
		$showCCList = false;
	}
	
	$mode =   $CI->general_model->get_select_data('plan_friendlyname', array('gatewayAccess'), array('reseller_id' => $r_id, 'plan_id' => $p_id));
	if (!empty($mode))
		$gmode = $mode['gatewayAccess'];
	else
		$gmode = 0;

	if($gateWayMode == 1){
		$gmode = 1;
	}
	
	$gateway_count = $CI->general_model->get_num_rows('tbl_merchant_gateway', array('merchantID' => $u_id));
	
	

	$primary_nav = array();


	$electronic_chk = array();
	$plan_chk_menu  = array();
	$gateway 		= array();
	$subscriptions  = array();
	$credit   		= array();
	$invoices 		= array();
	$level_three	= array();
	$credits	= array();
	$transaction_history	= array();
	$surChargeObj = [];
	if (isset($planData) && !empty($planData)) {
		if($planData->merchant_plan_type == 'SS' || $planData->merchant_plan_type == 'ES'){
			$surChargeObj = array(
								'name'  => 'Surcharging',
								'url'   =>  base_url('surcharging'),
								'page_name' => 'surcharging'
							);
		}
	}		
	if ($status['active_app'] == '1') {
		
		// if enable level three
		if(!empty($merchData['enable_level_three'])){
			$level_three = array(
				'name'  => 'Level 3',
				'url'   => base_url('QBO_controllers/home/level_three'),
				'page_name' => 'level_three'
			);
		}
		if ($gmode == 1) {
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   => base_url('QBO_controllers/home/company'),
						'page_name' => 'company'
					),
					array(
						'name'  => 'Merchant Gateway',
						'url'   =>  base_url('QBO_controllers/home/gateway'),
						'page_name' => 'gateway'
					),
				)
			);
		} else {
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   => base_url('QBO_controllers/home/company'),
						'page_name' => 'company'
					),

				)
			);
		}
		

		if ($gateway_count > 0) {
			
			$invoices = array(
				'name'  => 'Invoices',
				'icon'  => 'gi gi-list',
				'url'   => base_url('QBO_controllers/Create_invoice/Invoice_details'),
				'page_name' => 'Invoice_details'
			);

			$transaction_history = array(
				'name'  => 'Transactions',
				'icon'  => 'gi gi-calendar',
				'url'   => base_url('QBO_controllers/Payments/payment_transaction'),
				'page_name' => 'payment_transaction'
			);
			
			$credit = array(
				'name'  => 'Credit Card',
				'icon'  => 'fa fa-credit-card',
				'sub'   => array(

					array(
						'name'  => 'Sale',
						'url'   => base_url('QBO_controllers/Payments/create_customer_sale'),
						'page_name' => 'create_customer_sale'
					),
					array(
						'name'  => 'Authorize',
						'url'   => base_url('QBO_controllers/Payments/create_customer_auth'),
						'page_name' => 'create_customer_auth'
					),

					array(
						'name'  => 'Capture / Void',
						'url'   =>  base_url('QBO_controllers/Payments/payment_capture'),
						'page_name' => 'payment_capture'
					),


					array(
						'name'  => 'Refund',
						'url'   => base_url('QBO_controllers/Payments/payment_refund'),
						'page_name' => 'payment_refund'
					)
				)
			);

			$subscriptions = array(
				'name'  => 'Subscriptions & Plans',
				'icon'  => 'gi gi-refresh',
				'sub'   => array(
					array(
						'name'  => 'Subscriptions',
						'url'   =>  base_url('QBO_controllers/SettingSubscription/subscriptions'),
						'page_name' => 'subscriptions',
					),
					array(
						'name'  => 'Plans',
						'url'   => base_url('QBO_controllers/SettingPlan/plans'),
						'page_name' => 'plans'
					),
				)
			);
		} else {
			$subscriptions  = array();
			$credit   		= array();
			$invoices 		= array();
		}

		if ($plantype) {

			if ($service) {
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('QBO_controllers/Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),


						array(
							'name'  => 'Void',
							'url'   =>  base_url('QBO_controllers/Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('QBO_controllers/Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);
			}

			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('QBO_controllers/home/index'),
					'page_name' => 'index'
				),
				$credit,

				$electronic_chk,

				$invoices,
				$transaction_history,
				$credits,
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('QBO_controllers/MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),
						array(
							'name'  => 'Customer Portal',
							'url' => '',
							'url'   => base_url('QBO_controllers/SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),
						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('QBO_controllers/Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('QBO_controllers/SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three
					)
				),

				$gateway,
				

			);
		} else {

			if ($service) {
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('QBO_controllers/Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),


						array(
							'name'  => 'Void',
							'url'   =>  base_url('QBO_controllers/Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('QBO_controllers/Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);
			}


			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('QBO_controllers/home/index'),
					'page_name' => 'index'
				),
				$credit,


				$electronic_chk,


				array(
					'name'  => 'Customers',
					'icon'  => 'fa fa-users',
					'url'   => base_url('QBO_controllers/Customer_Details/customer_details'),
					'page_name' => 'customer_details'
				),
				$subscriptions,
				$invoices,
				$transaction_history,
				$credits,
				array(
					'name'  => 'Products & Services',
					'icon'  => 'gi gi-calendar',
					'url'   => base_url('QBO_controllers/Plan_Product/Item_detailes'),
					'page_name' => 'Item_detailes'
				),
				array(
					'name'  => 'Reports',
					'icon'  => 'gi gi-signal',
					'url'   => base_url('QBO_controllers/report/reports'),
					'page_name' => 'reports'
				),
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(




						array(
							'name'  => 'User Management',
							'url'   => base_url('QBO_controllers/MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),
						array(
							'name'  => 'Customer Portal',
							'url' => '',
							'url'   => base_url('QBO_controllers/SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),

						array(
							'name'  => 'Taxes',
							'url'   => base_url('QBO_controllers/Tax/tax_list'),
							'page_name' => 'tax_list'
						),
						array(
							'name'  => 'Payment Terms',
							'url'   => base_url('QBO_controllers/NetTerms/terms'),
							'page_name' => 'terms'
						),
						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('QBO_controllers/Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('QBO_controllers/SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three,
						array(
							'name'  => 'API',
							'url'   => base_url('QBO_controllers/SettingConfig/apikey'),
							'page_name' => 'apikey'
						)
					)
				),

				$gateway,
				
			);
		}

		
	} else  if ($status['active_app'] == '2') {
		
		// if enable level three
		if(!empty($merchData['enable_level_three'])){
			$level_three = array(
				'name'  => 'Level 3',
				'url'   => base_url('home/level_three'),
				'page_name' => 'level_three'
			);
		}
		if ($gmode == 1)
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   => base_url('home/company'),
						'page_name' => 'company'
					),
					array(
						'name'  => 'Merchant Gateway',
						'url'   =>  base_url('home/gateway'),
						'page_name' => 'gateway'
					),
				)
			);
		else
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   => base_url('home/company'),
						'page_name' => 'company'
					),

				)
			);

		
		if ($gateway_count > 0) {

			$invoices = array(
				'name'  => 'Invoices',
				'icon'  => 'gi gi-list',
				'url'   => base_url('home/invoices'),
				'page_name' => 'invoices'
			);

			$transaction_history = array(
				'name'  => 'Transactions',
				'icon'  => 'gi gi-calendar',
				'url'   => base_url('Payments/payment_transaction'),
				'page_name' => 'payment_transaction'
			);

			$credit = array(
				'name'  => 'Credit Card',
				'icon'  => 'fa fa-credit-card',
				'sub'   => array(

					array(
						'name'  => 'Sale',
						'url'   => base_url('Payments/create_customer_sale'),
						'page_name' => 'create_customer_sale'
					),
					array(
						'name'  => 'Authorize',
						'url'   => base_url('Payments/create_customer_auth'),
						'page_name' => 'create_customer_auth'
					),

					array(
						'name'  => 'Capture / Void',
						'url'   =>  base_url('Payments/payment_capture'),
						'page_name' => 'payment_capture'
					),


					array(
						'name'  => 'Refund',
						'url'   => base_url('Payments/payment_refund'),
						'page_name' => 'payment_refund'
					)
				)
			);

			$subscriptions = array(
				'name'  => 'Subscriptions & Plans',
				'icon'  => 'gi gi-refresh',
				'sub'   => array(
					array(
						'name'  => 'Subscriptions',
						'url'   =>  base_url('SettingSubscription/subscriptions'),
						'page_name' => 'subscriptions',
					),
					array(
						'name'  => 'Plans',
						'url'   => base_url('SettingPlan/plans'),
						'page_name' => 'plans'
					),
				)
			);
		} else {
			$subscriptions  = array();
			$credit   		= array();
			$invoices 		= array();
		}


		if ($plantype) {

			if ($service)
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),

						array(
							'name'  => 'Void',
							'url'   =>  base_url('Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);




			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('home/index'),
					'page_name' => 'index'
				),

				$credit,

				$electronic_chk,
				
				$invoices,
				$transaction_history,
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),

						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),

						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three
					)
				),
				$gateway,
				

			);
		} else {


			if ($service)
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),

						array(
							'name'  => 'Void',
							'url'   =>  base_url('Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);

			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('home/index'),
					'page_name' => 'index'
				),
				$credit,

				$electronic_chk,
				array(
					'name'  => 'Customers',
					'icon'  => 'fa fa-users',
					'url'   => base_url('home/customer'),
					'page_name' => 'customer',
					'id' => 'customer_menu'
				),
				$subscriptions,
				$invoices,
				$transaction_history,
				$credits,
				array(
					'name'  => 'Products & Services',
					'icon'  => 'gi gi-calendar',
					'url'   => base_url('home/plan_product'),
					'page_name' => 'plan_product',
					'id' => 'product_service'
				),
				array(
					'name'  => 'Reports',
					'icon'  => 'gi gi-signal',
					'url'   => base_url('report/reports'),
					'page_name' => 'reports'
				),
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),
						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),

						array(
							'name'  => 'Taxes',
							'url'   => base_url('Tax/tax_list'),
							'page_name' => 'tax_list'
						),
						array(
							'name'  => 'Payment Terms',
							'url'   => base_url('NetTerms/terms'),
							'page_name' => 'terms'
						),
						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three,
						array(
							'name'  => 'API',
							'url'   => base_url('SettingConfig/apikey'),
							'page_name' => 'apikey'
						)
					)
				),
				$gateway,
				

			);
		}

		
	} else  if ($status['active_app'] == '3') {

		// if level three data enable
		if(!empty($merchData['enable_level_three'])){
			$level_three = array(
				'name'  => 'Level 3',
				'url'   => base_url('FreshBooks_controllers/home/level_three'),
				'page_name' => 'level_three'
			);
		}
		if ($gmode == 1)
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   =>  base_url('FreshBooks_controllers/home/company'),
						'page_name' => 'company'
					),
					array(
						'name'  => 'Merchant Gateway',
						'url'   =>  base_url('FreshBooks_controllers/home/gateway'),
						'page_name' => 'gateway'
					),
				)
			);
		else
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   =>  base_url('FreshBooks_controllers/home/company'),
						'page_name' => 'company'
					),

				)
			);


		if ($gateway_count > 0) {

			$invoices = array(
				'name'  => 'Invoices',
				'icon'  => 'gi gi-list',
				'url'   => base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'),
				'page_name' => 'Invoice_details'
			);

			$transaction_history = array(
				'name'  => 'Transactions',
				'icon'  => 'gi gi-calendar',
				'url'   => base_url('FreshBooks_controllers/Transactions/payment_transaction'),
				'page_name' => 'payment_transaction'
			);

			$credit = array(
				'name'  => 'Credit Card',
				'icon'  => 'fa fa-credit-card',
				'sub'   => array(

					array(
						'name'  => 'Sale',
						'url'   => base_url('FreshBooks_controllers/Transactions/create_customer_sale'),
						'page_name' => 'create_customer_sale'
					),

					array(
						'name'  => 'Authorize',
						'url'   => base_url('FreshBooks_controllers/Transactions/create_customer_auth'),
						'page_name' => 'create_customer_auth'
					),

					array(
						'name'  => 'Capture / Void',
						'url'   =>  base_url('FreshBooks_controllers/Transactions/payment_capture'),
						'page_name' => 'payment_capture'
					),

					array(
						'name'  => 'Refund',
						'url'   => base_url('FreshBooks_controllers/Transactions/payment_refund'),
						'page_name' => 'payment_refund'
					)
				)
			);

			$subscriptions = array(
				'name'  => 'Subscriptions & Plans',
				'icon'  => 'gi gi-refresh',
				'sub'   => array(
					array(
						'name'  => 'Subscriptions',
						'url'   => base_url('FreshBooks_controllers/SettingSubscription/subscriptions'),
						'page_name' => 'subscriptions',
					),
					array(
						'name'  => 'Plans',
						'url'   => base_url('FreshBooks_controllers/SettingPlan/plans'),
						'page_name' => 'plans'
					),
				)
			);
		} else {
			$subscriptions  = array();
			$credit   		= array();
			$invoices 		= array();
		}




		if ($plantype) {
			if ($service)
				$electronic_chk =
					array(
						'name'  => 'Electronic Check',
						'icon'  => 'gi gi-lock',
						'sub'   => array(

							array(
								'name'  => 'Sale',
								'url'   => base_url('FreshBooks_controllers/Transactions/create_customer_esale'),
								'page_name' => 'create_customer_esale'
							),


							array(
								'name'  => 'Void',
								'url'   =>  base_url('FreshBooks_controllers/Transactions/evoid_transaction'),
								'page_name' => 'evoid_transaction'
							),

							array(
								'name'  => 'Refund',
								'url'   => base_url('FreshBooks_controllers/Transactions/echeck_transaction'),
								'page_name' => 'echeck_transaction'
							)
						)
					);



			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('FreshBooks_controllers/home/index'),
					'page_name' => 'index'
				),

				$credit,
				$electronic_chk,

				$invoices,
				$transaction_history,
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(


						array(
							'name'  => 'User Management',
							'url'   => base_url('FreshBooks_controllers/MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),

						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('FreshBooks_controllers/SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),
						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('FreshBooks_controllers/Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('FreshBooks_controllers/SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three
					)
				),

				$gateway,
				

			);
		} else {
			if ($service)
				$electronic_chk =
					array(
						'name'  => 'Electronic Check',
						'icon'  => 'gi gi-lock',
						'sub'   => array(

							array(
								'name'  => 'Sale',
								'url'   => base_url('FreshBooks_controllers/Transactions/create_customer_esale'),
								'page_name' => 'create_customer_esale'
							),


							array(
								'name'  => 'Void',
								'url'   =>  base_url('FreshBooks_controllers/Transactions/evoid_transaction'),
								'page_name' => 'evoid_transaction'
							),

							array(
								'name'  => 'Refund',
								'url'   => base_url('FreshBooks_controllers/Transactions/echeck_transaction'),
								'page_name' => 'echeck_transaction'
							)
						)
					);
			if ($gmode == 1)
				$gateway =	array(
					'name'  => 'Integrations',
					'icon'  => 'gi gi-bookmark',
					'sub'   => array(
						array(
							'name'  => 'Accounting Package',
							'url'   =>  base_url('FreshBooks_controllers/home/company'),
							'page_name' => 'company'
						),
						array(
							'name'  => 'Merchant Gateway',
							'url'   =>  base_url('FreshBooks_controllers/home/gateway'),
							'page_name' => 'gateway'
						),
					)
				);
			else
				$gateway =	array(
					'name'  => 'Integrations',
					'icon'  => 'gi gi-bookmark',
					'sub'   => array(
						array(
							'name'  => 'Accounting Package',
							'url'   =>  base_url('FreshBooks_controllers/home/company'),
							'page_name' => 'company'
						),
					)
				);
			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('FreshBooks_controllers/home/index'),
					'page_name' => 'index'
				),

				$credit,
				$electronic_chk,

				array(
					'name'  => 'Customers',
					'icon'  => 'fa fa-users',
					'url'   => base_url('FreshBooks_controllers/Freshbooks_Customer/get_customer_data'),
					'page_name' => 'customer'
				),
				$subscriptions,
				$invoices,
				$transaction_history,
				$credits,
				array(
					'name'  => 'Products & Services',
					'icon'  => 'gi gi-calendar',
					'url'   =>  base_url('FreshBooks_controllers/Freshbooks_plan_product/Item_detailes'),
					'page_name' => 'plan_product'
				),
				array(
					'name'  => 'Reports',
					'icon'  => 'gi gi-signal',
					'url'   =>  base_url('FreshBooks_controllers/report/reports'),
					'page_name' => 'reports'
				),
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('FreshBooks_controllers/MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),
						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('FreshBooks_controllers/SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),
						array(
							'name'  => 'Taxes',
							'url'   => base_url('FreshBooks_controllers/Tax/tax_list'),
							'page_name' => 'tax_list'
						),
						array(
							'name'  => 'Payment Terms',
							'url'   => base_url('FreshBooks_controllers/NetTerms/terms'),
							'page_name' => 'terms'
						),
						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('FreshBooks_controllers/Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('FreshBooks_controllers/SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three,
						array(
							'name'  => 'API',
							'url'   => base_url('FreshBooks_controllers/SettingConfig/apikey'),
							'page_name' => 'apikey'
						)
					)
				),

				$gateway,
				

			);
		}

		
	} else	  if ($status['active_app'] == '4') {

		// if level three data enable
		if(!empty($merchData['enable_level_three'])){
			$level_three = array(
				'name'  => 'Level 3',
				'url'   => base_url('Integration/home/level_three'),
				'page_name' => 'level_three'
			);
		}

		if ($service)
			$electronic_chk = array(
				'name'  => 'Electronic Check',
				'icon'  => 'gi gi-lock',
				'sub'   => array(

					array(
						'name'  => 'Sale',
						'url'   => base_url('Integration/Payments/create_customer_esale'),
						'page_name' => 'create_customer_esale'
					),

					array(
						'name'  => 'Void',
						'url'   =>  base_url('Integration/Payments/evoid_transaction'),
						'page_name' => 'evoid_transaction'
					),


					array(
						'name'  => 'Refund',
						'url'   => base_url('Integration/Payments/echeck_transaction'),
						'page_name' => 'echeck_transaction'
					)
				)
			);
		
		if ($gmode == 1)
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   =>  base_url('Integration/home/company'),
						'page_name' => 'company'
					),
					array(
						'name'  => 'Merchant Gateway',
						'url'   =>  base_url('Integration/home/gateway'),
						'page_name' => 'gateway'
					),
				)
			);
		else
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   =>  base_url('Integration/home/company'),
						'page_name' => 'company'
					),

				)
			);

		$invoices = array(
			'name'  => 'Invoices',
			'icon'  => 'gi gi-list',
			'url'   => base_url('Integration/Invoices/invoice_list'),
			'page_name' => 'Invoice_details'
		);

		$transaction_history = array(
			'name'  => 'Transactions',
			'icon'  => 'gi gi-calendar',
			'url'   => base_url('Integration/Payments/payment_transaction'),
			'page_name' => 'payment_transaction'
		);

		$primary_nav = array(
			array(
				'name'  => 'Dashboard',
				'icon'  => 'gi gi-home',
				'url'   => base_url('Integration/home/index'),
				'page_name' => 'index'
			),
			array(
				'name'  => 'Credit Card',
				'icon'  => 'fa fa-credit-card',
				'sub'   => array(

					array(
						'name'  => 'Sale',
						'url'   => base_url('Integration/Payments/create_customer_sale'),
						'page_name' => 'create_customer_sale'
					),
					array(
						'name'  => 'Authorize',
						'url'   => base_url('Integration/Payments/create_customer_auth'),
						'page_name' => 'create_customer_auth'
					),

					array(
						'name'  => 'Capture / Void',
						'url'   =>  base_url('Integration/Payments/payment_capture'),
						'page_name' => 'payment_capture'
					),


					array(
						'name'  => 'Refund',
						'url'   => base_url('Integration/Payments/payment_refund'),
						'page_name' => 'payment_refund'
					)
				)
			),
			$electronic_chk,
			array(
				'name'  => 'Customers',
				'icon'  => 'fa fa-users',
				'url'   =>  base_url('Integration/Customers/customer_list_all'),
				'page_name' => 'customer'
			),
			array(
				'name'  => 'Subscriptions & Plans',
				'icon'  => 'gi gi-refresh',
				'sub'   => array(
					array(
						'name'  => 'Subscriptions',
						'url'   =>  base_url('Integration/SettingSubscription/subscriptions'),
						'page_name' => 'subscriptions'
					),
					array(
						'name'  => 'Plans',
						'url'   => base_url('Integration/SettingPlan/plans'),
						'page_name' => 'plans'
					),
				)
			),
			$invoices,
			$transaction_history,
			array(
				'name'  => 'Products & Services',
				'icon'  => 'gi gi-calendar',
				'url'   =>  base_url('Integration/Items/item_list'),
				'page_name' => 'item_detailes'
			),
			array(
				'name'  => 'Reports',
				'icon'  => 'gi gi-signal',
				'url'   =>  base_url('Integration/Settings/reports'),
				'page_name' => 'reports'
			),
			array(
				'name'  => 'Configuration',
				'icon'  => 'gi gi-settings',
				'sub'   => array(
					array(
						'name'  => 'User Management',
						'url'   => base_url('Integration/Settings/admin_user'),
						'page_name' => 'admin_user'
					),
					array(
						'name'  => 'Customer Portal',
						'url'   => base_url('Integration/Settings/setting_customer_portal'),
						'page_name' => 'setting_customer_portal'
					),

					array(
						'name'  => 'Taxes',
						'url'   => base_url('Integration/Settings/tax_list'),
						'page_name' => 'tax_list'
					),
					array(
						'name'  => 'Payment Terms',
						'url'   => base_url('Integration/Settings/terms'),
						'page_name' => 'terms'
					),
					array(
						'name'  => 'Email Templates',
						'url'   =>  base_url('Integration/Settings/email_temlate'),
						'page_name' => 'email_temlate'
					),
					
					array(
						'name'  => 'General Settings',
						'url'   => base_url('Integration/Settings/profile_setting'),
						'page_name' => 'profile_setting'
					),
					$surChargeObj,
					$level_three,
					array(
						'name'  => 'API',
						'url'   => base_url('Integration/Settings/apikey'),
						'page_name' => 'apikey'
					)
				)
			),
			$gateway,
			

		);
	} else  if ($status['active_app'] == '5') {
		$isPlanVT = 0;
		
		if(!empty($planData)){
			if($planData->merchant_plan_type == 'VT'){
				$isPlanVT = 1;
			}
		}
		
		// if level three data enable
		if(!empty($merchData['enable_level_three'])){
			$level_three = array(
				'name'  => 'Level 3',
				'url'   => base_url('company/home/level_three'),
				'page_name' => 'level_three'
			);
		}

		
		if ($gateway_count > 0) {
			if($isPlanVT != 1){
				$invoices = 	array(
					'name'  => 'Invoices',
					'icon'  => 'gi gi-list',
					'sub'   => array(
						array(
							'name'  => 'Invoices',
							'url'   => base_url('company/home/invoices'),
							'page_name' => 'invoices'
						),
						array(
							'name'  => 'Payments',
							'url'   => base_url('company/Payments/payment_transaction'),
							'page_name' => 'payment_transaction'
						),
						array(
							'name'  => 'Refunds',
							'url'   => base_url('company/Payments/refund_transaction'),
							'page_name' => 'refund_transaction'
						),
						
					)
				);
			
				$invoices = array(
					'name'  => 'Invoices',
					'icon'  => 'gi gi-list',
					'url'   => base_url('company/home/invoices'),
					'page_name' => 'invoices'
				);
			}
			

			$transaction_history = array(
				'name'  => 'Transactions',
				'icon'  => 'gi gi-calendar',
				'url'   => base_url('company/Payments/payment_transaction'),
				'page_name' => 'payment_transaction'
			);

			$credit = array(
				'name'  => 'Credit Card',
				'icon'  => 'fa fa-credit-card',
				'sub'   => array(

					array(
						'name'  => 'Sale',
						'url'   => base_url('company/Payments/create_customer_sale'),
						'page_name' => 'create_customer_sale'
					),
					array(
						'name'  => 'Authorize',
						'url'   => base_url('company/Payments/create_customer_auth'),
						'page_name' => 'create_customer_auth'
					),

					array(
						'name'  => 'Capture / Void',
						'url'   =>  base_url('company/Payments/payment_capture'),
						'page_name' => 'payment_capture'
					),


					array(
						'name'  => 'Refund',
						'url'   => base_url('company/Payments/payment_refund'),
						'page_name' => 'payment_refund'
					)
				)
			);

			$subscriptions = array(
				'name'  => 'Subscriptions & Plans',
				'icon'  => 'gi gi-refresh',
				'sub'   => array(
					array(
						'name'  => 'Subscriptions',
						'url'   =>  base_url('company/SettingSubscription/subscriptions'),
						'page_name' => 'subscriptions',
					),
					array(
						'name'  => 'Plans',
						'url'   => base_url('company/SettingPlan/plans'),
						'page_name' => 'plans'
					),
				)
			);

			// if level three data enable
			if(!empty($merchData['enable_level_three'])){
				$level_three = array(
					'name'  => 'Level 3',
					'url'   => base_url('company/home/level_three'),
					'page_name' => 'level_three'
				);
			}
		} else {
			$subscriptions  = array();
			$credit   		= array();
			$invoices 		= array();
			
		}

		if ($plantype) {
			
			if ($service)
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('company/Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),

						array(
							'name'  => 'Void',
							'url'   =>  base_url('company/Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('company/Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);

			if ($gmode == 1)
				$gateway =	array(
					'name'  => 'Integrations',
					'icon'  => 'gi gi-bookmark',
					'sub'   => array(
						
						array(
							'name'  => 'Merchant Gateway',
							'url'   =>  base_url('company/SettingConfig/gateway'),
							'page_name' => 'gateway'
						),
					)
				);
			else
				$gateway =	array(); 	

			$primary_nav = array(

				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('company/home/index'),
					'page_name' => 'index'
				),

				$credit,

				$electronic_chk,
				array(
					'name'  => 'Customers',
					'icon'  => 'fa fa-users',
					'url'   => base_url('company/home/customer'),
					'page_name' => 'customer',
					'id' => 'customer_menu'
				),

				$invoices,
				$transaction_history,
				$credits,
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('company/MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),

						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('company/SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),

						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('company/Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						array(
							'name'  => 'General Settings',
							'url'   => base_url('company/SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three
					)
				),

				$gateway,
			);
		} else {


			if ($service)
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('company/Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),

						array(
							'name'  => 'Void',
							'url'   =>  base_url('company/Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('company/Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);
			
			if ($gmode == 1)
				$gateway = array(
					'name'  => 'Integrations',
					'icon'  => 'gi gi-bookmark',
					'sub'   => array(
						
						array(
							'name'  => 'Merchant Gateway',
							'url'   =>  base_url('company/SettingConfig/gateway'),
							'page_name' => 'gateway'
						),
					)
				);

			else
				$gateway = array();

			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('company/home/index'),
					'page_name' => 'index'
				),

				$credit,

				$electronic_chk,
				array(
					'name'  => 'Customers',
					'icon'  => 'fa fa-users',
					'url'   => base_url('company/home/customer'),
					'page_name' => 'customer',
					'id' => 'customer_menu'
				),
				$subscriptions,
				$invoices,
				$transaction_history,
				$credits,
				array(
					'name'  => 'Products & Services',
					'icon'  => 'gi gi-calendar',
					'url'   => base_url('company/home/plan_product'),
					'page_name' => 'plan_product',
					'id' => 'product_service'
				),
				array(
					'name'  => 'Reports',
					'icon'  => 'gi gi-signal',
					'url'   => base_url('company/report/reports'),
					'page_name' => 'reports'
				),
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('company/MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),
						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('company/SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),


						array(
							'name'  => 'Payment Terms',
							'url'   => base_url('company/NetTerms/terms'),
							'page_name' => 'terms'
						),
						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('company/Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						$surChargeObj,
						$level_three,
						array(
							'name'  => 'API',
							'url'   => base_url('company/SettingConfig/apikey'),
							'page_name' => 'apikey'
						)
					)
				),
				$gateway,
			);
		}
	} else {
		
		// if level three data enable
		if(!empty($merchData['enable_level_three'])){
			$level_three = array(
				'name'  => 'Level 3',
				'url'   => base_url('home/level_three'),
				'page_name' => 'level_three'
			);
		}
		

		if ($gmode == 1)
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   => base_url('home/company'),
						'page_name' => 'company'
					),
					array(
						'name'  => 'Merchant Gateway',
						'url'   =>  base_url('home/gateway'),
						'page_name' => 'gateway'
					),
				)
			);
		else
			$gateway =	array(
				'name'  => 'Integrations',
				'icon'  => 'gi gi-bookmark',
				'sub'   => array(
					array(
						'name'  => 'Accounting Package',
						'url'   => base_url('home/company'),
						'page_name' => 'company'
					),

				)
			);


		if ($gateway_count > 0) {
			$invoices = 	array(
				'name'  => 'Invoices & Payments',
				'icon'  => 'gi gi-list',
				'sub'   => array(
					array(
						'name'  => 'Invoices',
						'url'   => base_url('home/invoices'),
						'page_name' => 'invoices'
					),
					array(
						'name'  => 'Payments',
						'url'   => base_url('Payments/payment_transaction'),
						'page_name' => 'payment_transaction'
					),
					array(
						'name'  => 'Refunds',
						'url'   => base_url('Payments/refund_transaction'),
						'page_name' => 'refund_transaction'
					),
				)
			);

			$credit = array(
				'name'  => 'Credit Card',
				'icon'  => 'fa fa-credit-card',
				'sub'   => array(

					array(
						'name'  => 'Sale',
						'url'   => base_url('Payments/create_customer_sale'),
						'page_name' => 'create_customer_sale'
					),
					array(
						'name'  => 'Authorize',
						'url'   => base_url('Payments/create_customer_auth'),
						'page_name' => 'create_customer_auth'
					),

					array(
						'name'  => 'Capture / Void',
						'url'   =>  base_url('Payments/payment_capture'),
						'page_name' => 'payment_capture'
					),


					array(
						'name'  => 'Refund',
						'url'   => base_url('Payments/payment_refund'),
						'page_name' => 'payment_refund'
					)
				)
			);

			$subscriptions = array(
				'name'  => 'Subscriptions & Plans',
				'icon'  => 'gi gi-refresh',
				'sub'   => array(
					array(
						'name'  => 'Subscriptions',
						'url'   =>  base_url('SettingSubscription/subscriptions'),
						'page_name' => 'subscriptions',
					),
					array(
						'name'  => 'Plans',
						'url'   => base_url('SettingPlan/plans'),
						'page_name' => 'plans'
					),
				)
			);
		} else {
			$subscriptions  = array();
			$credit   		= array();
			$invoices 		= array();
		}


		if ($plantype) {

			if ($service)
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),

						array(
							'name'  => 'Void',
							'url'   =>  base_url('Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);




			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('home/index'),
					'page_name' => 'index'
				),

				$credit,

				$electronic_chk,

				$invoices,

				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),

						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),

						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						array(
							'name'  => 'Surcharging',
							'url'   =>  base_url('SettingConfig/surcharging'),
							'page_name' => 'surcharging'
						),
						$level_three
					)
				),
				$gateway,
				

			);
		} else {


			if ($service)
				$electronic_chk =	array(
					'name'  => 'Electronic Check',
					'icon'  => 'gi gi-lock',
					'sub'   => array(

						array(
							'name'  => 'Sale',
							'url'   => base_url('Payments/create_customer_esale'),
							'page_name' => 'create_customer_esale'
						),

						array(
							'name'  => 'Void',
							'url'   =>  base_url('Payments/evoid_transaction'),
							'page_name' => 'evoid_transaction'
						),


						array(
							'name'  => 'Refund',
							'url'   => base_url('Payments/echeck_transaction'),
							'page_name' => 'echeck_transaction'
						)
					)
				);

			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'icon'  => 'gi gi-home',
					'url'   => base_url('home/index'),
					'page_name' => 'index'
				),
				$credit,

				$electronic_chk,
				array(
					'name'  => 'Customers',
					'icon'  => 'fa fa-users',
					'url'   => base_url('home/customer'),
					'page_name' => 'customer',
					'id' => 'customer_menu'
				),
				$subscriptions,
				$invoices,

				array(
					'name'  => 'Products & Services',
					'icon'  => 'gi gi-calendar',
					'url'   => base_url('home/plan_product'),
					'page_name' => 'plan_product',
					'id' => 'product_service'
				),
				array(
					'name'  => 'Reports',
					'icon'  => 'gi gi-signal',
					'url'   => base_url('report/reports'),
					'page_name' => 'reports'
				),
				array(
					'name'  => 'Configuration',
					'icon'  => 'gi gi-settings',
					'sub'   => array(

						array(
							'name'  => 'User Management',
							'url'   => base_url('MerchantUser/admin_user'),
							'page_name' => 'admin_user'
						),
						array(
							'name'  => 'Customer Portal',
							'url'   => base_url('SettingConfig/setting_customer_portal'),
							'page_name' => 'setting_customer_portal'
						),

						array(
							'name'  => 'Taxes',
							'url'   => base_url('Tax/tax_list'),
							'page_name' => 'tax_list'
						),
						array(
							'name'  => 'Payment Terms',
							'url'   => base_url('NetTerms/terms'),
							'page_name' => 'terms'
						),
						array(
							'name'  => 'Email Templates',
							'url'   =>  base_url('Settingmail/email_temlate'),
							'page_name' => 'email_temlate'
						),
						
						array(
							'name'  => 'General Settings',
							'url'   => base_url('SettingConfig/profile_setting'),
							'page_name' => 'profile_setting'
						),
						$surChargeObj,
						$level_three,
						array(
							'name'  => 'API',
							'url'   => base_url('SettingConfig/apikey'),
							'page_name' => 'apikey'
						)
					)
				),
				$gateway,
				

			);
		}
	}
	if(!$showCCList){
		unset($primary_nav[1]);
	}

	return $primary_nav;
}



function get_chat_script($rID)
{
	$script = array();
	$CI = &get_instance();
	$CI->load->model('general_model');
	$rs  =  $CI->general_model->get_select_data('tbl_reseller', array('ProfileURL', 'Chat'), array('resellerID' => $rID));


	if (!empty($rs)) {
		$script   = $rs;
	}

	return $script;
}

function get_default_gateway_data($mID)
{
	$stripeKey = '';
	$CI = &get_instance();
	$CI->load->model('general_model');
	$gts  =  $CI->general_model->get_select_data('tbl_merchant_gateway', array('gatewayUsername'), array('merchantID' => $mID, 'gatewayType' => '5', 'set_as_default' => '1'));

	if (!empty($gts)) {
		$stripeKey = $gts['gatewayUsername'];
	}

	return $stripeKey;
}


function checkAuthPermission($args) {
	$CI = &get_instance();

	$auth = true;
	if ($CI->session->userdata('user_logged_in')) {
		$authName = $CI->session->userdata('user_logged_in')['authName'];
	} else{
		return $auth;
	}

	if(!in_array($args['Link'],$authName)) {
		$auth = false;
	}

	return $auth;
}

function restoreAuth() {
	$CI = &get_instance();

	$auth = true;
	if ($CI->session->userdata('user_logged_in')) {
		$user_data = $CI->session->userdata('user_logged_in');
		$CI->load->model('general_model');

		$user1=  $CI->general_model->get_merchant_user_data($user_data['merchantID'], $user_data['merchantUserID']);
		if(!empty($user1)){
			$auth = $CI->general_model->get_auth_data($user1['authID']); 
			if ($auth) {
				$user_data['authName'] = $auth; 
				$CI->session->set_userdata('user_logged_in',$user_data);
			}
		}
	} 
	return true;
}
