



<!-- Page content -->
<div id="page-content">
    
	<!-- Customer Content -->
    <div class="row">
        <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        <div class="col-lg-4">
            <!-- Customer Info Block -->
			
			
            <div class="block">
                <!-- Customer Info Title -->
                <div class="block-title">
                    <h2><i class="fa fa-file-o"></i> <strong>My</strong> Details</h2>
                </div>
                <!-- END Customer Info Title --> 

                <!-- Customer Info -->
                <div class="block-section text-center">
                    <a href="javascript:void(0)">
                        <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar4@2x.jpg" alt="avatar" class="img-circle">
                    </a>
                    <h3>
                        <strong><?php echo $customer->FirstName.' '.$customer->LastName; ?></strong>
                    
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                       <tr>
                            <td class="text-right"><strong>Primary Contact</strong></td>
                            <td><?php echo $customer->FullName; ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Added On</strong></td>
                            <td><?php echo date('F d, Y - h:m', strtotime($customer->TimeCreated)); ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Last Visit</strong></td>
                            <td><?php echo date('F d, Y - h:m', strtotime($customer->TimeModified)); ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Main Email</strong></td>
                            <td><?php echo $customer->Contact; ?></td>
                        </tr>
						<tr>
                            <td class="text-right"><strong>Main Phone</strong></td>
                            <td><?php echo $customer->Phone; ?></td>
                        </tr>
						<tr>
                              <td class="text-right"><strong>Credit Card Info</strong></td>
                            <td> <a href="#card_data_process" class="btn btn-xs btn-info"  onclick="set_card_user_data('<?php  echo $customer->ListID; ?>', '<?php  echo $customer->FullName; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
							 <a href="#card_edit_data_process" class="btn btn-xs btn-default"   data-backdrop="static" data-keyboard="false" data-toggle="modal">View/Update</a>
							</td>
                        </tr>
						<tr>
                            <td class="text-right"><strong>Status</strong></td>
                            <td><i class="fa fa-check"></i> Active</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Customer Info -->
                
                
                
                
            </div>
            <!-- END Customer Info Block -->

            <!-- Quick Stats Block -->
            <div class="block">
                <!-- Quick Stats Title -->
                <div class="block-title">
                    <h2><i class="fa fa-line-chart"></i> <strong>Quick</strong> Stats</h2>
                </div>
                <!-- END Quick Stats Title -->

                <!-- Quick Stats Content -->
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background">
                            <i class="gi gi-list"></i>
                        </div>
                        <h4 class="text-left">
                            <strong><?php echo $invoices_count; ?></strong><br><small>Invoices in Total</small>
                        </h4>
                    </div>
                </a>
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background-success">
                            <i class="fa fa-usd"></i>
                        </div>
                        <h4 class="text-left text-success">
                            <strong>$ <?php echo $sum_invoice; ?></strong><br><small>Invoice Value</small>
                        </h4>
                    </div>
                </a>
                <!-- END Quick Stats Content -->
            </div>
            <!-- END Quick Stats Block -->
        </div>
        <div class="col-lg-8">
            <!-- Orders Block -->
            <div class="block">
                <!-- Orders Title -->
                <div class="block-title">
				
				
				
				
				
                    <div class="block-options pull-right">
                        <span class="btn btn-alt1 btn-info"><strong>$ <?php echo $pay_upcoming; ?></strong></span>
						
                    </div>
                    <h2><strong>Upcoming</strong> Invoices</h2>
                </div>
                <!-- END Orders Title -->

                <!-- Orders Content -->
                <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
                        <tr>
                            <td class="text-right" ><strong>Invoice</strong></td>
                            <td class="hidden-xs text-right"><strong>Due Date</strong></td>
                          
							  <td class="text-right"><strong>Amount</strong></td>
                            <td class="text-center"><strong>Action</strong></td>
                        </tr>
						
						<?php   if(!empty($invoices)){     foreach($invoices as $invoice){
						
						  
						 ?>
                        <tr>
                            <td class="text-right"><a href="<?php echo base_url();?>customer/home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
                            <td class="hidden-xs text-right"><?php  echo  date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
                          
							 <td class="text-right"><strong><?php if($invoice['IsPaid']=='false'){   echo  $invoice['BalanceRemaining']; }else{ echo $invoice['AppliedAmount']; } ?></strong></td>
                            <td class="text-center">						
								
								 <?php 	 if($invoice['status']=='Upcoming'){   ?>
									 
									 	<a href="#invoice_customer_process" class="btn btn-xs btn-success"  onclick="set_invoice_customer_process_id('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
									 <?php }else if($invoice['status']=='Paid') { ?><a href="javascript:void(0)"  class="btn btn-alt1 btn-success">Paid</a>
									 
									 
									 
									 <?php   } else{ ?> <a href="javascript:void(0)" disabled="disabled" class="btn btn-xs btn-primary">Canceled</a>
									  
									 <?php } ?>
							</td>
                        </tr>
						   <?php  } }else { ?>	
						
						<tr>
                            <td colspan="5"><strong>No record founds!</strong></td>
                         
                        </tr>
						
						   <?php } ?>
                      
                        
                    </tbody>
                </table>
                <!-- END Orders Content -->
            </div>
            <!-- END Orders Block -->

            <!-- Products in Cart Block -->
            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
						
                        <span class="btn btn-alt1 btn-success"><strong>$ <?php echo $pay_invoice; ?></strong></span>
                        <span class="btn btn-alt1 btn-danger"><strong>$ <?php echo $pay_remaining; ?></strong></span>
					
                    </div>
                    <h2><strong>Past</strong> Invoices</h2>
                </div>
                <!-- END Products in Cart Title -->

                <!-- Products in Cart Content -->
                 <table id="ecom-orders" class="table table-bordered table-striped ecom-orders table-vcenter">
				 
				  <thead>
                 <tr>
                            <td class="text-right"><strong>Invoice</strong></td>
                            
                            <td class="hidden-xs text-right"><strong>Due/Paid Date</strong></td>
                            <td class="text-right hidden-xs"><strong>Payments</strong></td>
							  <td class="text-right"><strong>Balance</strong></td>
                            <td class="text-center"><strong>Status</strong></td>
                            <td class="text-center">Action</td>
                        </tr>
                </thead>
                    <tbody>
                      
						
						<?php   if(!empty($latest_invoice)){   

						foreach($latest_invoice as $invoice){
						  
						  
						  
						    if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }   
						else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   } else
						   
						   if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
						 ?>
                        <tr>
                            <td class="text-right"><a href="<?php echo base_url();?>customer/home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
                           <td class="hidden-xs text-right"><?php  echo  $invoice['TimeModifiedinv']; ?></td>
                            <?php if($invoice['AppliedAmount']!="0.00"){ ?>
                            <td class="hidden-xs text-right"><a href="#pay_data_process"   onclick="set_payment_data('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php  echo  $invoice['AppliedAmount']; ?></a></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right"><?php  echo  $invoice['AppliedAmount']; ?></td>   
						   <?php } ?>
							 <td class="text-right"><strong><?php  echo  $invoice['BalanceRemaining']; ?></strong></td>
                              <td class="text-center"><span <?php echo $disabled; ?> class="btn btn-alt1 btn-<?php echo $lable; ?>"><?php  echo  $invoice['status']; ?></span></td>
                                 <td class="text-center">						
								
								 <?php 	 if($invoice['status']=='Past Due' || $invoice['status']=='Failed' ){   ?>
									 
									 	<a href="#invoice_customer_process" class="btn btn-xs btn-success"  onclick="set_invoice_customer_process_id('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
									 <?php }else if($invoice['status']=='Canceled' ) {  ?>
									 <a href="javascript:void(0)" disabled="disabled" class="btn btn-xs btn-primary">Canceled</a>
									 <?php }else{ echo ""; } ?>
							</td>
                               
                        </tr>
						   <?php } }else { ?>	
						
						<tr>
                            <td colspan="6"><strong>No record founds!</strong></td>
                         
                        </tr>
						
						   <?php } ?>
                       
                        
                    </tbody>
                </table>
                <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->

            <!-- Customer Addresses Block -->
            <div class="block">
                <!-- Customer Addresses Title -->
                <div class="block-title">
                    <h2><strong>Addresses</strong></h2>
                </div>
                <!-- END Customer Addresses Title -->

                <!-- Customer Addresses Content -->
                <div class="row">
                    <div class="col-lg-6">
                        <!-- Billing Address Block -->
                        <div class="block">
                            <!-- Billing Address Title -->
                            <div class="block-title">
                                <h2>My Address</h2>
                            </div>
                            <!-- END Billing Address Title -->

                            <!-- Billing Address Content -->
                            <h4><strong><?php echo $customer->FullName; ?></strong></h4>
                            <address>
								<strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                <?php echo $customer->ShipAddress_Addr2; ?>, <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo $customer->ShipAddress_State; ?>, <?php echo $customer->ShipAddress_PostalCode; ?><br>
                            </address>
                            <!-- END Billing Address Content -->
                        </div>
                        <!-- END Billing Address Block -->
                    </div>
                    <div class="col-lg-6">
                        <!-- Shipping Address Block -->
                        <div class="block">
                            <!-- Shipping Address Title -->
                            <div class="block-title">
                                <h2>Invoice Address</h2>
                            </div>
                            <!-- END Shipping Address Title -->

                            <!-- Shipping Address Content -->
                            <h4><strong><?php echo $customer->FullName; ?></strong></h4>
                          <address>
								<strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                <?php echo $customer->ShipAddress_Addr2; ?>, <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo $customer->ShipAddress_State; ?>, <?php echo $customer->ShipAddress_PostalCode; ?><br>
                            </address>
                            <!-- END Shipping Address Content -->
                        </div>
                        <!-- END Shipping Address Block -->
                    </div>
                </div>
                <!-- END Customer Addresses Content -->
            </div>
            <!-- END Customer Addresses Block -->
         
            
            
            <!-- END Private Notes Block -->
        </div>
    </div>
    <!-- END Customer Content -->

	
<!-- END Page Content -->
<div id="card_edit_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit/Delete  Card</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
		
				
				 	 
				 
     <table  class="table table-bordered table-striped table-vcenter">
	 
         <?php
            if(!empty($card_data_array)){
     		 foreach($card_data_array as $cardarray){ ?>
           
                    <tr>
                    <td class="text-left hidden-xs"><?php echo $cardarray['customerCardfriendlyName'];  ?></td>
                    <td class="text-right visible-lg"><?php echo $cardarray['CardNo'];  ?></td>
                    <td class="text-right hidden-xs"><div class="btn-group btn-group-xs">
							<a href="#" data-toggle="tooltip" title="" class="btn btn-default" onclick="set_edit_card('<?php echo $cardarray['CardID'] ; ?>');" data-original-title="Edit Card"><i class="fa fa-edit"></i></a>
							<a href="<?php echo base_url(); ?>customer/Payments/delete_card_data/<?php echo $cardarray['CardID'] ; ?>" onClick="if(confirm('do you really want this')) return true; else return false;"  data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete Card"><i class="fa fa-times"></i></a>
                          
						</div> </td>
                </tr>  
            
		<?php } 
		
			}else{ ?>   
		 <tr>
                    <td colspan="3">No Card Available!</td>
               
                </tr> 
		
		<?php } ?>
       </table>	

		<form id="thest_form" method="post" style="display:none;" action='<?php echo base_url(); ?>customer/Payments/update_card_data' class="form-horizontal" >
				 <fieldset>
			        
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_card_number" name="edit_card_number" class="form-control" placeholder="Enter Card Number .." autocomplete="off">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                   	<select id="edit_expiry" name="edit_expiry" class="form-control">
                                                        <option value="1">JAN</option>
                                                        <option value="2">FEB</option>
                                                        <option value="3">MAR</option>
                                                        <option value="4">APR</option>
                                                        <option value="5">MAY</option>
													    <option value="6">JUN</option>
                                                        <option value="7">JUL</option>
                                                        <option value="8">AUG</option>
                                                        <option value="9">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
												
												    <label class="col-md-3 control-label" for="edit_expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                   	<select id="edit_expiry_year" name="edit_expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+15;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                                </div>
												
												
                                            </div>     
    				<div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_cvv" name="edit_cvv" class="form-control" placeholder="Security Code (CVV)" autocomplete="off" />
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="edit_friendlyname">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_friendlyname" name="edit_friendlyname" class="form-control" placeholder="Enter Friendly Name" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                                          <input type="hidden" id="edit_cardID" name="edit_cardID"  value="" />
                  <div class="pull-right">
				     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Edit"  />
                     <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			    </form>		
	 <!-- panel-group -->
				
				
                	
				<div id="can_div">
				 <div class="pull-right">
				    
                     <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
				 </div>
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>  

	
	
	
	
	
<!-- END Page Content -->
<div id="card_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add Card</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest" method="post" action='<?php echo base_url(); ?>customer/Payments/insert_new_data' class="form-horizontal" >
                     
                 
				 <fieldset>
                    
                    
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="card_number" name="card_number" class="form-control" placeholder="Enter Card Number .." autocomplete="off">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                   	<select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
													    <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
												
												    <label class="col-md-3 control-label" for="expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                   	<select id="expiry_year" name="expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+15;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                                </div>
												
												
                                            </div>     
    				<div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="cvv" name="cvv" class="form-control" placeholder="Security Code (CVV)" autocomplete="off" />
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Enter Friendly Name" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                     <input type="hidden" id="customerID11" name="customerID11"  value="" />
                  <div class="pull-right">
                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                    <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- Load and execute javascript code used only in this page -->


 
<script>
        
$(function(){    nmiValidation.init();  nmiValidation1.init();
		
					
	
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');

});
	

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
                   		
					 cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	
	
	
	

 function set_card_user_data(id, name){
      
	   $('#customerID11').val(id);
 }
     

 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 edit_expiry_year: {
							  CCExp: {
									month: '#edit_expiry',
									year: '#edit_expiry_year'
							  }
						},
					
                   		
					 edit_cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();


	function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();		
		 
		if(cardID!=""){
			
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>customer/Payments/get_card_edit_data",
				data : {'cardID':cardID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					       if(data['status']=='success'){
							
                                $('#edit_cardID').val(data['card']['CardID']);
							    $('#edit_card_number').val(data['card']['CardNo']);
								document.getElementById("edit_cvv").value =data['card']['CardCVV'];
								document.getElementById("edit_friendlyname").value =data['card']['customerCardfriendlyName'];
								$('select[name="edit_expiry"]').find('option[value="'+data['card']['cardMonth']+'"]').attr("selected",true);
								$('select[name="edit_expiry_year"]').find('option[value="'+data['card']['cardYear']+'"]').attr("selected",true);
								
							
					   }	   
					
				}
				
				
			});
			
		}	  
		   
	}	

</script>

</div>
