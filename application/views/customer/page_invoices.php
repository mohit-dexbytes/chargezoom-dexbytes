

<!-- Page content -->
<div id="page-content">
    <!-- eCommerce Orders Header -->
    
    <!-- END eCommerce Orders Header -->

    <!-- Quick Stats -->
  
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Invoices</h2>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-right" >Invoice</th>
                    
                    <th class="hidden-xs text-right">Due Date</th>
                      
                     <th class="text-right hidden-xs">Payment</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-right">Status</th>
                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices)){

						foreach($invoices as $invoice){
						     $lable='';
						          $statusnew='';
							           if($invoice['leftDay'] < 0 &&  $invoice['BalanceRemaining']=='0.00' ){
										 
										 	 $statusnew = "Close";			  
										}else if($invoice['leftDay'] < 0  && $invoice['userStatus']!='' ){
										 $statusnew = "Close";	
										}else if($invoice['leftDay'] < 0 &&  $invoice['BalanceRemaining']!='0.00'  && $invoice['userStatus']==''){ 
										 $statusnew = "Past Due";	
										
										}else if($invoice['leftDay'] >=0 &&  $invoice['BalanceRemaining']!='0.00' && $invoice['userStatus']=='cancel' ){
										
										 $statusnew = "Close";	
										}
										
										else if($invoice['leftDay'] >=0 &&  $invoice['BalanceRemaining']!='0.00' && $invoice['userStatus']=='' ){
										
										 $statusnew = "Open";	
										}
					
						   if($invoice['status']=='Upcoming' && $invoice['userStatus']=='' ){
						   $invoice['status']='Scheduled';
							    $lable ="info";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							     $invoice['status']='Paid';
						   }else  if($invoice['status']=='Failed' && $invoice['userStatus']==''){
							    $lable ="danger";
						   } else  if($invoice['status']=='Cancel'  ){
						 			 
								    $lable ="primary";
						   }else  if($invoice['userStatus']!=''){
						           $invoice['userStatus']='Canceled';
								    $lable ="primary";
						   }
						    
						    
			   ?>
			
				<tr>
					  <td class="text-right"><a href="<?php echo base_url();?>customer/home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
			
                    <td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
                             <?php if((-$invoice['AppliedAmount'])!="0.00"){ ?>
                            <td class="hidden-xs text-right"><a href="#pay_data_process"   onclick="set_payment_data('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo number_format((-$invoice['AppliedAmount']),2); ?></a></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right"><?php echo number_format( (-$invoice['AppliedAmount']),2); ?></td>   
						   <?php } ?>
            	
					<td class="text-right hidden-xs"><strong><?php echo $invoice['BalanceRemaining']; ?></strong></td>
					<td class="text-right">
					<?php  if($invoice['userStatus']!=''){ echo $invoice['userStatus']; }else{ echo $invoice['status']; } ?></td>
					
					<td class="text-center">
						<div class="btn-group btn-group-xs">
						    <?php if($invoice['DueDate'] > date('Y-m-d') && $invoice['userStatus']=='' && $invoice['IsPaid']=='false' ){ ?>
						
						
						<a href="#invoice_customer_process" class="btn btn-xs btn-success"  onclick="set_invoice_customer_process_id('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
							   <?php } else  if($invoice['DueDate'] > date('Y-m-d') && $invoice['userStatus']!='' ) { ?>
							  	<a href="javascript:void(0);" class="btn btn-xs btn-primary">Canceled</a>
							    <?php }else if( $invoice['BalanceRemaining']!='0.00' && $invoice['userStatus']=='' ){ ?>
								<a href="#invoice_process" class="btn btn-xs btn-success"  onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
							<?php 	}else{  echo "";  } ?>
							    	
							    	
						</div>
					</td>
				</tr>
				
				<?php 
				  }
			   }	
				?>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->



<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ EcomOrders.init(); });</script>
</div>
