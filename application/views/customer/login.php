 
<!-- Login Alternative Row -->
<div class="container">
	<div class="row">
		
		<div class="col-md-4 col-md-offset-4">
		      <h1 class="push-top-bottom">
					<img src="<?php echo base_url(IMAGES); ?>/logo_new.png" alt="avatar"><br>
					<small>Welcome to Customer Portal!</small>
				</h1
			<!-- Login Container -->
			<div id="login-container">
				<!-- Login Title -->
				<div class="login-title text-center">
					<h1><strong>Login</strong> or <strong>Register</strong></h1>
				</div>
				<!-- END Login Title -->

				<!-- Login Block -->
				<div class="block push-bit">
				
					<?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
					?> 
					
					<!-- Login Form -->
					<form action="<?php echo base_url('customer/login/user_login'); ?>" method="post" id="form-login" class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="login-email" name="login-email"  class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" autocomplete="off" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-4">
							</div>
							<div class="col-xs-8 text-right">
								<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Portal</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<a href="javascript:void(0)" id="link-reminder-login"><small>Forgot password?</small></a> -
								<a href="javascript:void(0)" id="link-register-login"><small>Create a new Login</small></a>
							</div>
						</div>
					</form>
					<!-- END Login Form -->

					<!-- Reminder Form -->
					<form action="<?php echo base_url('customer/login/recover_password'); ?>#reminder" method="post" id="form-reminder" class="form-horizontal display-none">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Reset Password</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
							</div>
						</div>
					</form>
					<!-- END Reminder Form -->

					<!-- Register Form -->
					<form action="<?php echo base_url('customer/login/user_register'); ?>#register" method="post" id="form-register" class="form-horizontal display-none">

						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="register-email" name="register-email" class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
					
						<div class="form-group form-actions">
							<div class="col-xs-6">
							</div>
							<div class="col-xs-6 text-right">
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Register Account</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<small>Do you have an account?</small> <a href="javascript:void(0)" id="link-register"><small>Login</small></a>
							</div>
						</div>
					</form>
					<!-- END Register Form -->
				</div>
				<!-- END Login Block -->
			</div>
			<!-- END Login Container -->
		</div>
	</div>
</div>
<!-- END Login Alternative Row -->


<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>

