
<!-- Page content -->
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Customers Transactions</h2>
        </div>
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-right" style="width: 100px;">Txn ID</th>

                    <th class="text-right">Invoice</th>
                     <th class="text-right hidden-xs">Amount</th>
					 
                    
                    <th class="text-right visible-lg">Date</th>
                     <th class="text-right hidden-xs">Type</th>
                    
                    <th class="hidden-xs text-center">Status</th>
                   
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-right"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					
				
					<td class="text-right hidden-xs"><?php echo $transaction['RefNumber']; ?></td>
					<td class="hidden-xs text-right"><?php echo number_format($transaction['transactionAmount'],2); ?></td>
					
				
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($transaction['transactionDate'])); ?></td>
						<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
					<td class="text-center visible-lg"><?php if($transaction['transactionCode']=='300'){ ?> <span class="btn btn-alt1 btn-danger">Failed</span> <?php }else if($transaction['transactionCode']=='100'){ ?> <span class="btn btn-alt1 btn-success">Success</span><?php } ?></td>
					
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ EcomOrders.init(); });</script>

</div>
<!-- END Page Content -->