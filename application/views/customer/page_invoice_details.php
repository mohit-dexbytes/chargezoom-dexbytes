

<!-- Page content -->
<div id="page-content">
    <!-- Order Status -->
    <div class="row text-center">
        <div class="col-sm-6 col-lg-4">
            <div class="widget">
                <div class="widget-extra themed-background-muted">
                    <h4 class="widget-content-light"><strong>Invoice <?php echo  $invoice_data['RefNumber']; ?></strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-muted animation-expandOpen"><?php  echo  date('m-d-Y', strtotime($invoice_data['DueDate'])); ?></span></div>
            </div>
        </div>
<?php  if($invoice_data['BalanceRemaining'] =="0.00" and $invoice_data['IsPaid']=='true'){  ?>
        <div class="col-sm-6 col-lg-4">
            <div class="widget">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Status</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-check"></i> Paid</span></div>
            </div>
        </div>
        <?php } 
		  if($invoice_data['AppliedAmount'] !="0.00" and $invoice_data['IsPaid']=='false'){  ?>
                  <div class="col-sm-6 col-lg-4">
            <div class="widget">
                <div class="widget-extra themed-background-warning">
                    <h4 class="widget-content-light"><strong>Status</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-warning animation-expandOpen"><i class="fa fa-info-circle"></i> Partial</span></div>
            </div>
        </div>

		<?php }	if($invoice_data['AppliedAmount'] =="0.00" and $invoice_data['IsPaid']=='false'){	?>
         <div class="col-sm-6 col-lg-4">
                    <div class="widget">
                        <div class="widget-extra themed-background-danger">
                            <h4 class="widget-content-light"><strong>Status</strong></h4>
                        </div>
                        <div class="widget-extra-full"><span class="h2 text-danger animation-expandOpen"><i class="fa fa-times-circle"></i> Unpaid</span></div>
                    </div>
                </div>
        
        <?php } ?>
          
        <?php  if($invoice_data['BalanceRemaining'] =="0.00" and $invoice_data['IsPaid']=='true'){  ?>
        <div class="col-sm-6 col-lg-4">
            <div class="widget">
                <div class="widget-extra themed-background-muted">
                    <h4 class="widget-content-light"><strong>Mode</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-muted animation-pulse">Auto - NMI</span></div>
            </div>
        </div>
          <?php } 
		  if($invoice_data['AppliedAmount'] !="0.00" and $invoice_data['IsPaid']=='false'){  ?>
          <div class="col-sm-6 col-lg-4">
            <div class="widget">
                <div class="widget-extra themed-background-muted">
                    <h4 class="widget-content-light"><strong>Mode</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-muted animation-pulse">Sale</span></div>
            </div>
        </div>
        		<?php }	if($invoice_data['AppliedAmount'] =="0.00" and $invoice_data['IsPaid']=='false'){	?>
                  <div class="col-sm-6 col-lg-4">
                    <div class="widget">
                        <div class="widget-extra themed-background-muted">
                            <h4 class="widget-content-light"><strong>Mode</strong></h4>
                        </div>
                        <div class="widget-extra-full"><span class="h2 text-muted animation-pulse">--</span></div>
                    </div>
                </div>
                
                <?php } ?>
        
    </div>
    <!-- END Order Status -->

    <!-- Products Block -->
    <div class="block">
        <!-- Products Title -->
        <div class="block-title">
			<div class="block-options pull-right">
				<a href="<?php echo base_url(); ?>customer/home/invoice_details_print/<?php echo  $invoice_data['TxnID']; ?>" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Print Invoice"><i class="fa fa-print"></i></a>
             	</div>
            <h2><strong>Invoice</strong> Details</h2>
			
        </div>
        <!-- END Products Title -->

        <!-- Products Content -->
         <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-right">S.N.</th>
						<th>Product/Service Name</th>
                        <th class="text-right">Qty</th>
                        <th class="text-right">Unit Rate</th>
						<th class="text-right">Amount</th>
						<th class="text-right">Tax</th>
                    </tr>
                </thead>
                <tbody>
				        <?php  
						
					
						$totaltax =0 ; $total=0; $tax=0;
						foreach($invoice_items as $key=> $item){
						?>
				   
                    <tr>
                        <td class="text-right"><?php echo $key+1; ?></td>
						<td><?php echo $item['Item_FullName']; ?></td>
                        <td class="text-right"><?php echo $item['Quantity']; ?></td>
                        <td class="text-right"><?php echo number_format($item['Rate'],2); ?></td>
						<td class="text-right"><?php  $total+=  $item['Quantity']*$item['Rate']; echo number_format($item['Quantity']*$item['Rate'],2);  ?></td>
						<th class="text-right"><strong><?php  if($item['TaxRate']){ $tax= ($item['TaxRate']*100)/($item['Quantity']*$item['Rate']);    $totaltax+= $tax ; }  echo ($tax)?$tax:'0.00'; ?> <a href="#" data-toggle="tooltip" title="<?php echo $item['SalesTaxCode_FullName']; ?> @ <?php echo ($tax)? $tax:'0.00'; ?>%"><i class="fa fa-exclamation-circle text-danger"></i></a></strong></th>
					
                    </tr>
						<?php } ?>
                  
					<tr class="active">
                        <td colspan="4" class="text-right text-uppercase"><strong>Total</strong></td>
                        <td class="text-right"><strong><?php  echo number_format( $total,2) ; ?></strong></td>
						<td class="text-right"><strong><?php echo  number_format($totaltax,2) ; ?></strong></td>
                    </tr>
					<tr>
                        <td colspan="5" class="text-right text-uppercase"><strong>Any Other Tax</strong></td>
                        <td class="text-right"><strong>00.00 <a href="#" data-toggle="tooltip" title="No Other Tax"><i class="fa fa-exclamation-circle text-danger"></i></a></strong></td>
                    </tr>
					<tr class="info">
                        <td colspan="5" class="text-right text-uppercase"><strong>Total Payable</strong></td>
                        <td class="text-right"><strong><?php echo  number_format($total+$totaltax,2) ;  ?></strong></td>
                    </tr>
                    <tr class="success">
                        <td colspan="5" class="text-right text-uppercase"><strong>Paid</strong></td>
                        <td class="text-right"><strong><?php $paid = number_format(-$invoice_data['AppliedAmount'],2); echo ($paid)?$paid:'0.00';  ?></strong></td>
                    </tr>
					<tr class="danger">
                        <td colspan="5" class="text-right text-uppercase"><strong>Balance</strong></td>
                        <td class="text-right"><strong><?php echo ($invoice_data['BalanceRemaining'])?$invoice_data['BalanceRemaining']:'0.00';  ?></strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- END Products Content -->
    </div>  
    <!-- END Products Block -->

    <!-- Addresses -->
    <div class="row">
        <div class="col-sm-12">
            <!-- Billing Address Block -->
            <div class="block">
                <!-- Billing Address Title -->
                <div class="block-title">
                    <h2><strong>Billing</strong> Address</h2>
                </div>
                <!-- END Billing Address Title -->

                <!-- Billing Address Content -->
                <h4><strong><?php echo $invoice_data['Customer_FullName']; ?></strong></h4>
				<address>
					<strong><?php echo $customer_data['companyName']; ?></strong><br>
					<?php echo $customer_data['ShipAddress_Addr1']; ?><br>
					<?php echo $customer_data['ShipAddress_City'] .','. $invoice_data['ShipAddress_State'];  ?><br>
					<?php echo 'United States' .', '. $customer_data['ShipAddress_PostalCode'];  ?><br><br>
					<i class="fa fa-phone"></i> <?php echo $customer_data['Phone']    ?><br>
					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
				</address>
                <!-- END Billing Address Content -->
            </div>
            <!-- END Billing Address Block -->
        </div>
     
    </div>
    <!-- END Addresses -->
</div>

