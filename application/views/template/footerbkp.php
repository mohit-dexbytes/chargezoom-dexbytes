<?php
/**
 * page_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
?>


<style>
.btn-success.btn-alt1 {
    background-color: #ffffff;
    color: #7db831;
}

.btn-danger.btn-alt1{
	  background-color: #ffffff;
    color: #e74c3c;
	
}

.btn-info.btn-alt1{
	  background-color: #ffffff;
    color: #3498db;
	
}

.btn-warning.btn-alt1{
	  background-color: #ffffff;
    color: #f39c12;
	
}
#qb-generate{
    
    font-size:14px;
}
</style>

   <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"> </script>
   <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
   
   
    <script>

   var card_daata=' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="Credit Card Number"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+     <?php 
														$cruy = date('y');
														$dyear = $cruy+25;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        '<option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>'+
													<?php } ?>
                                                       '</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="1234" /></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Friendly Name" /></div></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="" placeholder="Address 1"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="" placeholder="Address 2"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="" placeholder="City"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="" placeholder="State/Province"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder="ZIP Code"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="" placeholder="Country"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="" placeholder="Phone Number"></div></div></fieldset>';

  function roundN(num,n){
  return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
}  


    function set_template_data(c_id,cmp_name,cmp_id,cust_Email ){
		
		    //alert(c_id);
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
    }		    
        
	 var gtype ='';
	  function stripeResponseHandler_res(status, response)
	  {
	  
          
                        $("#thest_pay").find('input[name="stripeToken"]').remove();
           
                if (response.error) {
                    // Re-enable the submit button
                    $('#submit_btn').removeAttr("disabled");
                    // Show the errors on the form
//                    stripeErrorDisplayHandler(response);
                    $('#payment_error').text(response.error.message);
//                    $('.subscribe_process').hide();
                } else {
                    var form = $("#thest_pay");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

                    // Doing AJAX form submit to your server.
                 //   form.get(0).submit();
                 //   return false;
                 $("#btn_process").attr("disabled", false);	
                }
            }
	
 function create_Token_stripe()
 {
   
		//alert("HELLO");
       // console.log("start");
       // var $form = $("#payment_form");
        // Disable the submit button to prevent repeated clicks
     //   $form.find('#submit_btn').prop('disabled', true);
    
		var pub_key = $('#stripeApiKey').val();
        if(pub_key!=''){
		 Stripe.setPublishableKey(pub_key);
         Stripe.createToken({
                        number: $('#card_number11').val(),
                        cvc: $('#ccv11').val(),
                        exp_month: $('#expiry11').val(),
                        exp_year: $('#expiry_year11').val()
                    }, stripeResponseHandler_res);
   
    }
        // Prevent the form from submitting with the default action
        return false;
		
     
 }
 
 
 	
 function create_multi_Token_stripe()
 {
   
		//alert("HELLO");
       // console.log("start");
       // var $form = $("#payment_form");
        // Disable the submit button to prevent repeated clicks
     //   $form.find('#submit_btn').prop('disabled', true);
    
		var pub_key = $('#stripeApiKey').val();
        if(pub_key!=''){
           // alert($('#thest_pay1').find('#card_number11').val());
		 Stripe.setPublishableKey(pub_key);
         Stripe.createToken({
                        number: $('#thest_pay1').find('#card_number11').val(),
                        cvc: $('#thest_pay1').find('#ccv11').val(),
                        exp_month: $('#thest_pay1').find('#expiry11').val(),
                        exp_year:$('#thest_pay1').find('#expiry_year11').val()
                    }, stripeResponseHandler_res);
   
    }
        // Prevent the form from submitting with the default action
        return false;
		
     
 }

function set_subs_id(sID)
{
	
	  $('#subscID').val(sID);
	
}

function set_qbo_subs_id(sID)
{
	
	  $('#qbosubscID').val(sID);
	
}

function set_fb_subs_id(sID)
{
	
	  $('#fbsubscID').val(sID);
	
}

function set_xero_subs_id(sID)
{
	
	  $('#xerosubscID').val(sID);
	
}

function set_customerList_active_id(sID)
{
	
	  $('#custactiveID').val(sID);
	
}
  

function set_customerList_id(sID)
{
	
	  $('#custID').val(sID);
	
}


function set_productList_active_id(sID)
{
	
	  $('#proactiveID').val(sID);
	
}
  

function del_product_id(sID)
{
	
	  $('#prolistID').val(sID);
	
}





function del_role_id(id)
{
	
	     $('#merchantroleID').val(id);
}



function del_credit_id(id){
	
	     $('#invoicecreditid').val(id);
}

function qbo_del_credit_id(id){
	
	     $('#qbo_invoicecreditid').val(id);
}
function fb_del_credit_id(id){
	
	     $('#fb_invoicecreditid').val(id);
}

function xero_del_credit_id(id){
	
	     $('#xero_invoicecreditid').val(id);
}


function del_user_id(id){
	
	     $('#merchantID').val(id);
}



function set_company_id(id){
	
	     $('#companyID').val(id);
}	        
  
function set_invoice_id(id){
	
	     $('#invoiceID').val(id);
}	      

	
	
	

function set_invoice_schedule_id(id, ind_date){
	$('#scheduleID').val(id);

	var nowDate = new Date(ind_date);
	
	
      var inv_day = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
	 
            $("#schedule_date").datepicker({ 
              format: 'dd-mm-yyyy',
              
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
    
}	




function create_card_data()
{

 
	
   if($('#CardID').val()=='new1')
   {
    $('.card_div').html('');
    $('.card_div').html(card_daata);
   }else{
      $('.card_div').html('');
   }
}


function create_card_multi_data()
{

   var card_daata1=' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="Credit Card Number"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+     <?php 
														$cruy = date('y');
														$dyear = $cruy+25;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        '<option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>'+
													<?php } ?>
                                                       '</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11"  onblur="create_multi_Token_stripe();" name="cvv" class="form-control" placeholder="1234" /></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Friendly Name" /></div></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="" placeholder="Address 1"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="" placeholder="Address 2"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="" placeholder="City"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="" placeholder="State/Province"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder="ZIP Code"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="" placeholder="Country"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="" placeholder="Phone Number"></div></div></fieldset>';

	
   if($('#CardID1').val()=='new1')
   {
    $('.card_div').html('');
    $('.card_div').html(card_daata1);
   }else{
      $('.card_div').html('');
   }
}



/********************Show The Payment Data**************/
 
function set_payment_data(id){
	
    if(id !=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>Payments/view_transaction",
			data:{ 'invoiceID':id},
			success : function(response){
			
				$('#pay-content-data').html(response);
				
			}
			
		});
		
	}  
  
}

</script>  <!-- Footer -->

          <?php /*  <footer class="clearfix">
                <div class="pull-left">
                    <span style="color:#888888">2018 &copy;</span> <a href="http://localhost/Chargezoom" target="_blank"><?php echo $template['name'] . ' ' . $template['version']; ?></a>
                </div>
            </footer> */ ?>
            
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->




<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<?php $session = $this->session->userdata('logged_in'); ?>
<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->



<div id="del_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck11" method="post" action='<?php echo base_url(); ?>SettingSubscription/delete_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="subscID" name="subscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="del_qbo_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>QBO_controllers/SettingSubscription/delete_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="qbosubscID" name="qbosubscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
 
 <div id="del_fb_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckqbd" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/SettingSubscription/delete_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="fbsubscID" name="fbsubscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="del_xero_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckqbo" method="post" action='<?php echo base_url(); ?>Xero_controllers/SettingSubscription/delete_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="xerosubscID" name="xerosubscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="deactive_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Deactivate Product & Service</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckxero" method="post" action='<?php echo base_url(); ?>MerchantUser/change_product_status' class="form-horizontal" >
                     
                 
					<p>Do you really want to deactivate this Product & Service?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="prolistID" name="prolistID" class="form-control"  value="" />
							 <input type="hidden" id="status" name="status" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Deactivate Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="active_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Active Product</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckqq" method="post" action='<?php echo base_url(); ?>MerchantUser/change_product_status' class="form-horizontal" >
                     
                 
					<p>Do you really want to activate this product?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="proactiveID" name="proactiveID" class="form-control"  value="" />
                              <input type="hidden" id="status" name="status" class="form-control"  value="1" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" name="btn_cancel" class="btn btn-sm btn-success" value="Active Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="active_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Activate Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckde" method="post" action='<?php echo base_url(); ?>MerchantUser/delete_customer' class="form-horizontal" >
                     
                 
					<p>Do you really want to activate this Customer?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="custactiveID" name="custactiveID" class="form-control"  value="" />
                              <input type="hidden" id="status" name="status" class="form-control"  value="1" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Activate Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<div id="del_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Deactivate Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckre" method="post" action='<?php echo base_url(); ?>MerchantUser/delete_customer' class="form-horizontal" >
                     
                 
					<p>Do you really want to deactivate this Customer?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="custID" name="custID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Deactivate Now"  />
                    <button type="button" class="btn btn-sm btn-warning close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

 <!----------------- to view email popup modal --------------------->


<!-------------------------------  END  --------------------------------->









<!-------Delete Admin role------>

<!-------Delete Invoice Credit------>

<div id="del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_credit" method="post" action='<?php echo base_url(); ?>Payments/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoicecreditid" name="invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="qbo_del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_creditee" method="post" action='<?php echo base_url(); ?>QBO_controllers/Payments/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="qbo_invoicecreditid" name="qbo_invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="fb_del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_creditww" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/Transactions/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="fb_invoicecreditid" name="fb_invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="xero_del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_creditss" method="post" action='<?php echo base_url(); ?>Xero_controllers/Payments/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="xero_invoicecreditid" name="xero_invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="del_role" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Role</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_roleww" method="post" action='<?php echo base_url(); ?>MerchantUser/delete_role' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Role?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantroleID" name="merchantroleID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<!-------Delete Admin Users------>


<div id="del_user" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete User</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_user" method="post" action='<?php echo base_url(); ?>MerchantUser/delete_user' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this User?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantID" name="merchantID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>











<div id="pay_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Invoice Payment Details</h2>
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			 <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left" >Transaction ID</th>
                     <th class="text-right hidden-xs">Amount</th>
                    <th class="text-right visible-lg">Date</th>
                     <th class="text-right hidden-xs">Type</th>
                    <th class="hidden-xs text-right">Status</th>
                   
                </tr>
            </thead>
			  <tbody id="pay-content-data">
                
			
				
			</tbody>
        </table>
                  <div class="pull-right">
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Close</button>
        			</div>
                     <br />
                    <br />
			   
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>







<div id="del_company" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Application</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_comp" method="post" action='<?php echo base_url(); ?>home/delete_company' class="form-horizontal" >
					<p>All data will be lost, do you really want to delete this Application?</p> 
				    <div class="form-group">
                        <div class="col-md-8">
                            <input type="hidden" id="companyID" name="companyID" class="form-control"  value="" />
                        </div>
                    </div>
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete Now"  />
                    <button type="button" class="btn btn-sm close1" data-dismiss="modal" style="background-color:#ffc000; color:#ffffff;">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="invoice_schedule" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Schedule Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="Qbwc-form_schedule" method="post" action='<?php echo base_url(); ?>Payments/update_invoice_date' class="form-horizontal" >
                     
                 
					<p>Do you really want to schedule this invoice? Please select next Due Date.</p> 
					
					<div class="form-group">
                     <label class="col-md-4 control-label" for="card_list">Schedule Date</label>
                        <div class="col-md-8">
                      <div class="input-group input-date" >
                            <input type="text" id="schedule_date" name="schedule_date" class="form-control input-date " data-date-format="mm-dd-yyyy" placeholder="mm-dd-yyyy"  value="" />
                        </div>
					  </div>	
                    </div>
					
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="scheduleID" name="scheduleID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Schedule Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>





<div id="invoice_cancel" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                       <form id="Qbwc-form" method="post" action='<?php echo base_url(); ?>SettingSubscription/update_customer_invoice_cancel' class="form-horizontal" >
               <?php /*  <form id="Qbwc-form" method="post" action='<?php echo base_url(); ?>/update_customer_invoice_cancel' class="form-horizontal" >  */?>
                     
                 
					<p>Do you really want to delete this Invoice?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceID" name="invoiceID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-success close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"><i class="fa fa-pencil"></i> Change Password</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return checkForm(this);">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Name</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $session['firstName'].' '.$session['lastName']; ?></p>
								<input type="hidden" value="<?php echo $session['merchID'];  ?>"  name="user_id" id="user_id" />
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $session['merchantEmail'];  ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-currentpassword">Current Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-currentpassword" name="user-settings-currentpassword" class="form-control" placeholder="Enter Current Password">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Enter New Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="Confirm New Password">
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                             <button type="submit" class="btn btn-sm btn-success">Save</button>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
                           
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="qb-connecter-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Setup QuickBooks Desktop</h2>
                
                
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="Qbwc_form" method="post" action='<?php echo base_url(); ?>QuickBooks/config' class="form-horizontal" >
                     
                        <?php 
							// $username  = $qbusername;
							$username = $this->config->item('quickbooks_user');	
							$randomNum =substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 5);
							$username  = $username.$randomNum;
						?>
                        
                      <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-currentpassword">Application Name</label>
                            <div class="col-md-8">
                                <input type="text" id="qbcompany_name" name="qbcompany_name" class="form-control" readonly value="PayPortal" placeholder="Application Name"  >
                                <!--p class="form-control-static">Must match Company Name in QuickBooks</p-->
                            </div>
                        </div> 
                        
                          <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-currentpassword">Web Connector Username</label>
                            <div class="col-md-8">
                                <input type="text" id="qbusername" name="qbusername" class="form-control" readonly  value="<?php echo  $username ?>" />
                                
                            </div>
                        </div> 
						 
						
					    <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-password">Web Connector Password</label>
                            <div class="col-md-8">
                                <input type="password" id="qbpassword" name="qbpassword" class="form-control" placeholder="Web Connector Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-repassword">Confirm Password</label>
                            <div class="col-md-8">
                                <input type="password" id="qbrepassword" name="qbrepassword" class="form-control" placeholder="Confirm Password">
                            </div>
                        </div>
                      <div class="form-group form-actions">
                        <div class="col-xs-12 text-center">
                            <input type="submit" id="qb-generate" name="qb-generate" class="btn btn-sm btn-success" value="Generate Web Connector File"  />
                           
                        </div>
                    </div>
					
			   </form>		
                
            </div>
            <div class="modal-footer text-right">
            <!--button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Close</button-->
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="qb-connecter-info" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">QuickBooks Setup Instructions</h2>
                
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="" method="post"  class="form-horizontal" >
                     	<div class="form-group">
                            <label class="col-md-2 control-label">1</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Install QuickBooks Web Connector at the machine where your QuickBooks is installed.</p>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-2 control-label">2</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Configure your Web Connector using: Start->QuickBooks->QuickBooks Webconnector.</p>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-md-2 control-label">3</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Add the downloaded .qwc file using Add Application button at the QuickBooks Webconnector UI. </p>
                            </div>
                        </div>
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label">4</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Enter the password which you setup at step 4.</p>
                            </div>
                        </div>
						
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label">5</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Click on update button to import data from QuickBooks</p>
                            </div>
                        </div>
					
						
					 
					
			   </form>		
                
            </div>
            <div class="modal-footer text-right">
            <button type="button" class="btn btn-sm btn-danger " data-dismiss="modal">Close</button>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="invoice_modal_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Processed Today</h2>                
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
            	<div class="table-wrapper" style="width: 100%;height: 300px;overflow: auto;">
            	<table width="100%" class="table processed_table">
            		<thead>
            		<tr>
            			<th>ID</th>
            			<th>Name</th>
            			<th>Invoice</th>
            			<th>Amount</th>
            			<th>Type</th>
            			<th>Status</th>
            		</tr>
            	</thead>
                <tbody id="processed_server_data">
                	
                </tbody>
            </table>
            </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="scheduled_invoice_modal_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Scheduled Invoices</h2>                
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
            	<div class="table-wrapper" style="width: 100%;height: 300px;overflow: auto;">
            	<table width="100%" class="table scheduled_table">
            		<thead>
            		<tr>
            			<th style="font-size:14px !important">Customer Name</th>
            			<th style="font-size:14px !important">Invoice</th>
            			<th style="font-size:14px !important">Due Date</th>
            			<th style="font-size:14px !important">Payment</th>
            			<th style="font-size:14px !important">Balance</th>
            			<th style="font-size:14px !important">Status</th>
            		</tr>
            	</thead>
                <tbody id="scheduled_server_data">
                	
                </tbody>
            </table>
            </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="failed_invoice_modal_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Failed Invoices</h2>                
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
            	<div class="table-wrapper" style="width: 100%;height: 300px;overflow: auto;">
            	<table width="100%" class="table failed_table">
            		<thead>
            		<tr>
            			<th style="font-size:14px !important">Customer Name</th>
            			<th style="font-size:14px !important">Invoice</th>
            			<th style="font-size:14px !important">Due Date</th>
            			<th style="font-size:14px !important">Payment</th>
            			<th style="font-size:14px !important">Balance</th>
            			<th style="font-size:14px !important">Status</th>
            		</tr>
            	</thead>
                <tbody id="failed_server_data">
                	
                </tbody>
            </table>
            </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 <!----------------- to view email popup modal --------------------->

<!-------------------------------  END  --------------------------------->
<?php    if(isset($page_num) && $page_num=='customer_xero')
{ 

?>                            
                                
  
<script>


function set_xero_invoice_process_multiple(cid)
{

	    $("#btn_process").attr("disabled", true);
	    // $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay1');
	      // $('#thest_pay #inv_amount').val(in_val);
		if(cid!=""){
		     $('#inv_div').html('');
		     $('.card_div').html('');
		     
			$('#CardID1').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Xero_controllers/Customer/get_xero_customer_invoices",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
                         console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID1');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
							    
							  var invoices = data['invoices'];
							    
							   $('#inv_div').html(invoices);
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}
                                         

 function set_xero_url_multi_pay()
 {
 
          
		    var gateway_value =$("#gateway1").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 //  console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>Xero_controllers/PaytracePayment/pay_multi_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/pay_multi_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>Xero_controllers/Payments/pay_multi_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>Xero_controllers/PaypalPayment/pay_multi_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>Xero_controllers/StripePayment/pay_multi_invoice";
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							      	 var form = $("#thest_pay1");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay1").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }	
    	

function set_xero_invoice_process_id(id, cid, in_val)
{

	    $("#btn_process").attr("disabled", true);
	     $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay');
	      $('#thest_pay #inv_amount').val(in_val);
		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Xero_controllers/Payments/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
                            //console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
							  $(s).append('<option value="new1">New Card</option>');	
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}

 

function set_xero_url(){
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 //  console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>Xero_controllers/PaytracePayment/pay_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/pay_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>Xero_controllers/Payments/pay_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>Xero_controllers/PaypalPayment/pay_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>Xero_controllers/StripePayment/pay_invoice";
							   $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }


	function set_template_data(c_id,cmp_name,cmp_id,cust_Email ){
		
		    //alert(c_id);
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
    }		
	
	
	 $(function(){  
     
     
	$('#CardID').change(function(){
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						 //console.log(data);
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
                         $('#thest_pay #number').remove();	
                         $('#thest_pay #exp_year').remove();	
                         $('#thest_pay #exp_month').remove();	
                         $('#thest_pay #cvc').remove();	
						 var form = $("#thest_pay");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);

									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
				
				
			});
		  }else{
         
           $("#card_loader").hide();
           $("#thest_pay #btn_process").attr("disabled", false);
          }
		
	});		
       
     
			      	CKEDITOR.replace( 'textarea-ckeditor', {
				    toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				
				});
			
			
					$('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
				});
					$('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
				});
					$('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
				}); 
				
				
				
				
				
		  $('#type').change(function(){
			var typeID = $(this).val();

			var companyID  =  $('#tempCompanyID').val();

			var customer   = $('#tempCompanyName').val();

			var customerID =  $('#customertempID').val();
		    
		  
			if(typeID!=""){
		 
			$.ajax({
			type:"POST",
			url : '<?php echo base_url(); ?>Settingmail/set_template',
			data : {'typeID':typeID,'companyID':companyID,'customerID':customerID, 'customer':customer},
			success: function(data){
			         console.log(data);
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			    // $('#textarea-ckeditor').html(data['message']);
				$('#emailSubject').val(data['emailSubject']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
		  
		  }
	   });	 
	   
	 } 
 }); 



     
	
				
			
	 });    
	 
</script>
                                
<?php } 
if(isset($page_num) && $page_num=='customer_freshbook')
{ ?>


 <div id="set_tempemail_freshbook" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title pull-left">Send Email</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"> </label>
				
                  <form id="form-validation1" action="<?php echo base_url(); ?>FreshBooks_controllers/Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
				  
				  
			   <input type="hidden" id="customertempID" name="customertempID" value=""> 
				
				<input type="hidden" id="tempCompanyID" name="tempCompanyID" value=""> 
				
				<input type="hidden" id="tempCompanyName" name="tempCompanyName" value=""> 
                                
										
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                    
                                          <select id="type" name="type" class="form-control">
										  
                                            <option value="">Choose Template</option>
                                                <option value="1">Invoice due</option>
                                                <option value="2">Invoice past due/overdue</option>
                                                <option value="3">Invoice due soon/upcoming</option>
                                                <option value="12">A credit card will expire soon</option>
                                                <option value="13">A credit card has expired recently</option>
                                            </select>  
                                            
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                <?php /*    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">From Email Address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="fromEmail" name="fromEmail"  value="<?php  if($this->session->userdata('logged_in')) echo $login_info['merchantEmail'];  ?>" class="form-control" placeholder="Enter the email">
                                        </div>
                                    </div> */ ?>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);"  id ="open_reply">Set Reply-To</a>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">Cc these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="Cc email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">Bcc these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="Bcc Email">
                                        </div>
                                    </div>
                                     <div class="form-group" id="reply_div" style="display:none">
                                        <label class="col-md-3 control-label" for="replyEmail">Set the "Reply-To" to this address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="replyEmail" name="replyEmail" class="form-control"  value="<?php if(isset($templatedata)) echo ($templatedata['replyTo'])?$templatedata['replyTo']:''; ?>"  placeholder="Email">
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="form-group">
									
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
										
										<input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="Email Subject">
                                    </div>
										
                                    </div>
                                     
                                      <div class="form-group">
									  
									  
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success">Send </button>
                                        <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                                        
                                      
                                    </div>
                                </div>  
                           </form>
		    	
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<script>
function set_template_data_freshbook(c_id,cmp_name,cmp_id,cust_Email )
{
		
		    //alert(c_id);
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
    }	
    
    


function set_fb_invoice_process_multiple(cid)
{

	    $("#btn_process").attr("disabled", true);
	    // $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay1');
	      // $('#thest_pay #inv_amount').val(in_val);
		if(cid!=""){
		     $('#inv_div').html('');
		     $('.card_div').html('');
		     
			$('#CardID1').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_Customer/get_fb_customer_invoices",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
                         console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID1');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
							    
							  var invoices = data['invoices'];
							    
							   $('#inv_div').html(invoices);
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}
                                         

 function set_fb_url_multi_pay()
 {
 
          
		    var gateway_value =$("#gateway1").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 //  console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>FreshBooks_controllers/PaytracePayment/pay_multi_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>FreshBooks_controllers/AuthPayment/pay_multi_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>FreshBooks_controllers/Transactions/pay_multi_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>FreshBooks_controllers/PaypalPayment/pay_multi_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>FreshBooks_controllers/StripePayment/pay_multi_invoice";
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							      	 var form = $("#thest_pay1");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay1").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }	
    
	   
function set_fb_invoice_process_id(id, cid, in_val){
	    $("#btn_process").attr("disabled", true);
	     $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay');
	      $('#thest_pay #inv_amount').val(in_val);
		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>FreshBooks_controllers/Transactions/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
                            //console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
								 $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}

function set_fb_url(){
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 //  console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>FreshBooks_controllers/PaytracePayment/pay_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>FreshBooks_controllers/AuthPayment/pay_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>FreshBooks_controllers/Transactions/pay_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>FreshBooks_controllers/PaypalPayment/pay_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>FreshBooks_controllers/StripePayment/pay_invoice";
							     $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }
	
	 $(function(){  
		
				 	CKEDITOR.replace( 'textarea-ckeditor', {
				    toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
				});
			
					$('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
				});
					$('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
				});
					$('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
				}); 
				
				
				
				
				
		  $('#type').change(function(){
			var typeID = $(this).val();

			var companyID  =  $('#tempCompanyID').val();

			var customer   = $('#tempCompanyName').val();

			var customerID =  $('#customertempID').val();
		    
		  $('#emailSubject').val(customer);
			if(typeID!=""){
		 
			$.ajax({
			type:"POST",
			url : '<?php echo base_url(); ?>FreshBooks_controllers/Settingmail/set_template',
			data : {'typeID':typeID,'companyID':companyID,'customerID':customerID, 'customer':customer},
			success: function(data){
			         console.log(data);
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			    // $('#textarea-ckeditor').html(data['message']);
				$('#emailSubject').val(data['emailSubject']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
		  
		  }
	   });	 
	   
	 } 
 }); 



     
	$('#CardID').change(function(){
		var cardlID =  $(this).val();
	
		  if(cardlID!='' && cardlID !='new1' )
          {
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>FreshBooks_controllers/Transactions/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						 //console.log(data);
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
						 var form = $("#thest_pay");	
                         $('#thest_pay #number').remove();	
                         $('#thest_pay #exp_year').remove();	
                         $('#thest_pay #exp_month').remove();	
                         $('#thest_pay #cvc').remove();	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);

								
					        }	   
					
				}
				
				
			});
		  }else{
         
           $("#card_loader").hide();
           $("#thest_pay #btn_process").attr("disabled", false);
          }
		
	});		
       
	
				
			
	 });  
</script>
<?php } 

if(isset($page_num) && $page_num=='customer_qbd')
{ ?>

 <div id="set_tempemail" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title pull-left">Send Email</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"> </label>
				
                  <form id="form-validation1" action="<?php echo base_url(); ?>Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
				  
				  
			   <input type="hidden" id="customertempID" name="customertempID" value=""> 
				
				<input type="hidden" id="tempCompanyID" name="tempCompanyID" value=""> 
				
				<input type="hidden" id="tempCompanyName" name="tempCompanyName" value=""> 
                                
										
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                    
                                          <select id="type" name="type" class="form-control">
										  
                                            <option value="">Choose Template</option>
                                                <option value="1">Invoice due</option>
                                                <option value="2">Invoice past due/overdue</option>
                                                <option value="3">Invoice due soon/upcoming</option>
                                                <option value="12">A credit card will expire soon</option>
                                                <option value="13">A credit card has expired recently</option>
                                            </select>  
                                            
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                <?php /*    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">From Email Address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="fromEmail" name="fromEmail"  value="<?php  if($this->session->userdata('logged_in')) echo $login_info['merchantEmail'];  ?>" class="form-control" placeholder="Enter the email">
                                        </div>
                                    </div>  */ ?>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);"  id ="open_reply">Set Reply-To</a>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="CC Email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="BCC Email">
                                        </div>
                                    </div>
                                     <div class="form-group" id="reply_div" style="display:none">
                                        <label class="col-md-3 control-label" for="replyEmail">Set the "Reply-To" to this address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="replyEmail" name="replyEmail" class="form-control"  value="<?php if(isset($templatedata)) echo ($templatedata['replyTo'])?$templatedata['replyTo']:''; ?>"  placeholder="Email">
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="form-group">
									
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
										
										<input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="Email Subject">
                                    </div>
										
                                    </div>
                                     
                                      <div class="form-group">
									  
									  
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                        <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                                        
                                      
                                    </div>
                                </div>  
                           </form>
		    	
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<div id="invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Payments/pay_invoice' class="form-horizontal card_form" autocomplete="off" >
                     
                 
					 <!--<p>Do you really want to process this invoice before due date?</p> --> 
					
									<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_url();"  class="form-control">
                                                        <option value="" >Select Gateway</option>
														<?php if(isset($gateway_datas) && !empty($gateway_datas) ){
																foreach($gateway_datas as $gateway_data){
																?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
																<?php } } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
												
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
														
                                                </div>
                                            </div>
                                                
                                     <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>    
                       <div class="card_div"></div>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

                                                
                                                
<script>
	
	function set_template_data(c_id,cmp_name,cmp_id,cust_Email ){
		
		    //alert(c_id);
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
    }		
	
    	

function set_qbd_invoice_process_multiple(cid)
{

	    $("#btn_process").attr("disabled", true);
	    // $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay1');
	      // $('#thest_pay #inv_amount').val(in_val);
		if(cid!=""){
		     $('#inv_div').html('');
		     $('.card_div').html('');
		     
			$('#CardID1').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_qbd_customer_invoices",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
                         console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID1');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
							    
							  var invoices = data['invoices'];
							    
							   $('#inv_div').html(invoices);
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}
                                         

 function set_qbd_url_multi_pay()
 {
 
          
		    var gateway_value =$("#gateway1").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 //  console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>PaytracePayment/pay_multi_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>AuthPayment/pay_multi_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>Payments/pay_multi_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>PaypalPayment/pay_multi_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>StripePayment/pay_multi_invoice";
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							      	 var form = $("#thest_pay1");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay1").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }	

 
	
function set_invoice_process_id(id, cid, in_val)
{
	
	     $('#invoiceProcessID').val(id);
         $('#thest_pay #qbo_check').remove();

	     $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbd_pay'
			}).appendTo('#thest_pay');

      $('#thest_pay #inv_amount').val(in_val);
		 
		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
 console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}


 function set_url(){  
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>PaytracePayment/pay_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>AuthPayment/pay_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>Payments/pay_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>PaypalPayment/pay_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>StripePayment/pay_invoice";
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }
	

	 $(function(){  
		
				 	CKEDITOR.replace( 'textarea-ckeditor', {
				    toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
				});
			
					$('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
				});
					$('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
				});
					$('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
				}); 
				
				
				
				
				
		  $('#type').change(function(){
			var typeID = $(this).val();

			var companyID  =  $('#tempCompanyID').val();

			var customer   = $('#tempCompanyName').val();

			var customerID =  $('#customertempID').val();
		    
		  
			if(typeID!=""){
		 
			$.ajax({
			type:"POST",
			url : '<?php echo base_url(); ?>Settingmail/set_template',
			data : {'typeID':typeID,'companyID':companyID,'customerID':customerID, 'customer':customer},
			success: function(data){
			         console.log(data);
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			    // $('#textarea-ckeditor').html(data['message']);
				$('#emailSubject').val(data['emailSubject']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
		  
		  }
	   });	 
	   
	 } 
 }); 

/*
$('#CardID').change(function(){
        
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						 //console.log(data);
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
						 var form = $("#thest_pay");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);

									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
				
				
			});
		  }
		
	});		*/
     
	$('#CardID').change(function(){
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						 //console.log(data);
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
						 var form = $("#thest_pay");	
                        
                         $('#thest_pay #number').remove();	
                         $('#thest_pay #exp_year').remove();	
                         $('#thest_pay #exp_month').remove();	
                         $('#thest_pay #cvc').remove();	
                        
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
                             if(pub_key){
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);
                        }
									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
				
				
			});
		  }
		
	});		
       
	
				
			
	 });    


</script>


<!----------------- to view email popup modal --------------------->
<?php } 
if(isset($page_num) && $page_num=='customer_qbo')
{ ?>
 <div id="set_tempemail_QBO" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title pull-left">Send Email</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"> </label>
				
                  <form id="form-validation1" action="<?php echo base_url(); ?>QBO_controllers/Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
				  
				  
			   <input type="hidden" id="customertempID" name="customertempID" value=""> 
				
				<input type="hidden" id="tempCompanyID" name="tempCompanyID" value=""> 
				
				<input type="hidden" id="tempCompanyName" name="tempCompanyName" value=""> 
                                
										
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                    
                                          <select id="type" name="type" class="form-control">
										  
                                            <option value="">Choose Template</option>
                                                <option value="1">Invoice due</option>
                                                <option value="2">Invoice past due/overdue</option>
                                                <option value="3">Invoice due soon/upcoming</option>
                                                <option value="12">A credit card will expire soon</option>
                                                <option value="13">A credit card has expired recently</option>
                                            </select>  
                                            
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                               <?php /*     <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">From Email Address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="fromEmail" name="fromEmail"  value="<?php  if($this->session->userdata('logged_in')) echo $login_info['merchantEmail'];  ?>" class="form-control" placeholder="Enter the email">
                                        </div>
                                    </div>  */ ?>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);"  id ="open_reply">Set Reply-To</a>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">Cc these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="Cc Email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">Bcc these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="Bcc Email">
                                        </div>
                                    </div>
                                     <div class="form-group" id="reply_div" style="display:none">
                                        <label class="col-md-3 control-label" for="replyEmail">Set the "Reply-To" to this address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="replyEmail" name="replyEmail" class="form-control"  value="<?php if(isset($templatedata)) echo ($templatedata['replyTo'])?$templatedata['replyTo']:''; ?>"  placeholder="Email">
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="form-group">
									
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
										
										<input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="Email Subject">
                                    </div>
										
                                    </div>
                                     
                                      <div class="form-group">
									  
									  
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor1" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success">Send </button>
                                        <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                                        
                                      
                                    </div>
                                </div>  
                           </form>
		    	
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-------------------------------  END  --------------------------------->

<script>
                                            
   	

function set_qbo_invoice_process_multiple(cid)
{

	    $("#btn_process").attr("disabled", true);
	    // $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay1');
	      // $('#thest_pay #inv_amount').val(in_val);
		if(cid!=""){
		     $('#inv_div').html('');
		     $('.card_div').html('');
		     
			$('#CardID1').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/home/get_qbo_customer_invoices",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
                         console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID1');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
							    
							  var invoices = data['invoices'];
							    
							   $('#inv_div').html(invoices);
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}
                                         

 function set_qbo_url_multi_pay()
 {
 
          
		    var gateway_value =$("#gateway1").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 //  console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>QBO_controllers/PaytracePayment/pay_multi_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>QBO_controllers/AuthPayment/pay_multi_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>QBO_controllers/Payments/pay_multi_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>QBO_controllers/PaypalPayment/pay_multi_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>QBO_controllers/StripePayment/pay_multi_invoice";
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							      	 var form = $("#thest_pay1");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay1").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }	

function set_qbo_invoice_process_id(id, cid, in_val)
{

	    $("#btn_process").attr("disabled", true);
	     $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay');
	       $('#thest_pay #inv_amount').val(in_val);
		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/check_qbo_vault",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
                            //console.log (data);
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}

	
 function set_qbo_url()
 {
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 //  console.log(response);
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>QBO_controllers/PaytracePayment/pay_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>QBO_controllers/AuthPayment/pay_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>QBO_controllers/Payments/pay_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>QBO_controllers/PaypalPayment/pay_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>QBO_controllers/StripePayment/pay_invoice";
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }

function set_template_data_qbo(c_id,cmp_name,cmp_id,cust_Email )
{
		
		//    alert(cust_Email);
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
    }		
	
	
	 $(function(){  
     
 
				 	CKEDITOR.replace( 'textarea-ckeditor', {
				    toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
				});
     
                         
					$('#open_cc').click(function(){
                      //       alert("DDDD2222");
			          $('#cc_div').show();
					  $(this).hide();
				});
					$('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
				});
					$('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
				}); 
				
				
				
				
				
		  $('#type').change(function(){
			var typeID = $(this).val();

			var companyID  =  $('#tempCompanyID').val();

			var customer   = $('#tempCompanyName').val();

			var customerID =  $('#customertempID').val();
		    
		  
			if(typeID!=""){
		 
			$.ajax({
			type:"POST",
			url : '<?php echo base_url(); ?>QBO_controllers/Settingmail/set_template',
			data : {'typeID':typeID,'companyID':companyID,'customerID':customerID, 'customer':customer},
			success: function(data){
			         console.log(data);
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			    // $('#textarea-ckeditor').html(data['message']);
				$('#emailSubject').val(data['emailSubject']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
		  
		  }
	   });	 
	   
	 } 
 }); 



     
	$('#CardID').change(function()
	{
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						 $('#thest_pay #number').remove();	
                         $('#thest_pay #exp_year').remove();	
                         $('#thest_pay #exp_month').remove();	
                         $('#thest_pay #cvc').remove();	
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
						 var form = $("#thest_pay");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);

									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
				
				
			});
		  }else{
           $("#card_loader").hide();
           $("#btn_process").attr("disabled", false);	
          }
		
	});		
       
	
				
			
	 });  

</script>
<?php } ?>
<!-- END User Settings -->

<script type="text/javascript">

            
  function checkForm(form)
  {
	var curr_pass = $('#user-settings-currentpassword').val();
	var new_pass = $('#user-settings-password').val();
	var confirm_pass = $('#user-settings-repassword').val();
	
	if(curr_pass == '') {
		alert("Please enter current password!");
        $('#user-settings-currentpassword').focus();
        return false;
	}
	else if(new_pass == '') {
		alert("Please enter new password!");
        $('#user-settings-password').focus();
        return false;
	}
    else if( new_pass != "" && new_pass == confirm_pass ) {
      if(new_pass.length < 5){
        alert("Password must contain at least Five characters!");
        $('#user-settings-password').focus();
        return false;
      }
    } 
	else {
      alert("Confirm Password dismatched.!");
      $('#user-settings-repassword').focus();
      return false;
    } 
    return true;
  }

 $(document).ready(function(){
  

 	$(document).on('click','.processed_trans',function(){
 		$('#invoice_modal_popup').modal('show');
 		$.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_process_trans'); ?>",
        dataType: 'json',
        success:function(data){
			//console.log(data);
			 var s = $('#processed_server_data');
			 $(s).empty();
			 if(data != 2){

			for(var val in  data) {
				if(data[val]['transactionCode']=='300' || data[val]['transactionCode']=='' || data[val]['transactionCode']=='400' ||data[val]['transactionCode']=='401' || data[val]['transactionCode']=='3'){
					var status = '<span class="btn btn-sm btn-danger remove-hover">Failed</span>';
				}else if(data[val]['transactionCode']=='100'|| data[val]['transactionCode']=='200' || data[val]['transactionCode']=='111'|| data[val]['transactionCode']=='1'){
					var status = '<span class="btn btn-sm btn-success remove-hover">Success</span>';
				}
				$(s).append('<tr><td>'+data[val]['id']+'</td><td>'+data[val]['FullName']+'</td><td class="hidden-xs text-right">'+data[val]['RefNumber']+'</td><td>$'+data[val]['transactionAmount']+'</td><td>'+data[val]['transactionType']+'</td><td>'+status+'</td></tr>');
         }

     	}else{
     		$('.processed_table').empty();
     		$('.processed_table').append("<h2>No data...</h2>");
     	}
		}
    });
 	});


 	$(document).on('click','.scheduled_invoice',function(){
 		$('#scheduled_invoice_modal_popup').modal('show');
 		$.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_scheduled_incoice'); ?>",
        dataType: 'json',
        success:function(data){
			console.log(data);
			 var s = $('#scheduled_server_data');
			 $(s).empty();
			 if(data != 2){
			for(var val in  data) {
				if(data[val]['userStatus'] == 'Paid'){
					var status = "Schedule";
				}else if(data[val]['userStatus'] == ''){
					var status = "Schedule";
				}
					else{
					var status = "Past Due";
				}
			
			 $(s).append('<tr><td>'+data[val]['Customer_FullName']+'</td><td class="hidden-xs text-right">'+data[val]['RefNumber']+'</td><td class="hidden-xs text-right">'+data[val]['DueDate']+'</td><td>$'+data[val]['AppliedAmount']+'</td><td>$'+data[val]['BalanceRemaining']+'</td><td><strong>'+status+'</strong></td></tr>');
         }
     	}else{
     		$('.scheduled_table').empty();
     		$('.scheduled_table').append("<h2>No data...</h2>");
     	}
		}
    });
 	});

 	$(document).on('click','.failed_invoice',function(){
 		$('#failed_invoice_modal_popup').modal('show');
 		$.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_failed_incoice'); ?>",
        dataType: 'json',
        success:function(data){
			console.log(data);
			 var s = $('#failed_server_data');
			 $(s).empty();
			 if(data != 2){

			for(var val in  data) {
				
				 $(s).append('<tr><td>'+data[val]['Customer_FullName']+'</td><td class="hidden-xs text-right">'+data[val]['RefNumber']+'</td><td class="hidden-xs text-right">'+data[val]['DueDate']+'</td><td>$'+data[val]['AppliedAmount']+'</td><td>$'+data[val]['BalanceRemaining']+'</td><td><strong>Failed</strong></td></tr>');
         }

     	}else{
     		$('.failed_table').empty();
     		$('.failed_table').append("<h2>No data...</h2>");
     	}
		}
    });
 	});
 

 
     $('.card_form').validate({
         
         
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
						  CCExp1: {
								month: '#expiry11',
								year: '#expiry_year11'
						  }
					},
					
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
					friendlyname:{
						required: true,
						minlength: 3,
					},
                   
					 inv_amount: {
                        required: true
                    },
					
					 address: {
                        required: true
                    },
					country:{
						  required: true,
					},
					city:{
						required: true,
						minlength: 3,
					},
                    state:{
                         required: true,
                    },
					 email: {
                        required: true,
						email:true
                    },
					phone: {
                        required: true,
                        minlength:10,
						maxlength:12,
						digits:true,
                    },
                zipcode:{
					 required: true,
					  number: true,
						minlength: 5,
                        maxlength: 6,
				},
				
				baddress1: {
                        required: true
                    },
					baddress2: {
                        required: true
                    },
					bcountry:{
						  required: true,
					},
					bcity:{
						required: true,
						minlength: 3,
					},
                    bstate:{
                         required: true,
                    },
					 bemail: {
                        required: true,
						email:true
                    },
					bphone: {
                        required: true,
                        minlength:10,
						maxlength:12,
						digits:true,
                    
                        
                    },
                bzipcode:{
					 required: true,
					  number: true,
						minlength: 5,
                        maxlength: 6,
				}
				
				
                   
                  
              },
                messages: {
					  customerID: {
                        required: 'Please select a customer',
                      
                    },
                   
                    expry: {
                        required: 'Please select a valid month',
                         minlength: 'Please select a valid month',
                      
                    },
					amount:{
						  required: 'Please enter the amount',
					},
					check_status:{
						 required: 'Please select the option',
					}
                  
                   
                }
            });
       
       $.validator.addMethod('CCExp1', function(value, element, params) {
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  
		  console.log(params)
    
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
    //  alert(minMonth);alert(minYear);alert(month);alert(year);
		  return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');

          
    
 
     $('#thest_pay1').validate({
         
         
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
						  CCExp2: {
								month: '#expiry11',
								year: '#expiry_year11'
						  }
					},
					
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
					friendlyname:{
						required: true,
						minlength: 3,
					},
                   
					 inv_amount: {
                        required: true
                    },
					
					 address: {
                        required: true
                    },
					country:{
						  required: true,
					},
					city:{
						required: true,
						minlength: 3,
					},
                    state:{
                         required: true,
                    },
					 email: {
                        required: true,
						email:true
                    },
					phone: {
                        required: true,
                        minlength:10,
						maxlength:12,
						digits:true,
                    },
                zipcode:{
					 required: true,
					  number: true,
						minlength: 5,
                        maxlength: 6,
				},
				
				baddress1: {
                        required: true
                    },
					baddress2: {
                        required: true
                    },
					bcountry:{
						  required: true,
					},
					bcity:{
						required: true,
						minlength: 3,
					},
                    bstate:{
                         required: true,
                    },
					 bemail: {
                        required: true,
						email:true
                    },
					bphone: {
                        required: true,
                        minlength:10,
						maxlength:12,
						digits:true,
                    
                        
                    },
                bzipcode:{
					 required: true,
					  number: true,
						minlength: 5,
                        maxlength: 6,
				}
				
				
                   
                  
              },
                messages: {
					  customerID: {
                        required: 'Please select a customer',
                      
                    },
                   
                    expry: {
                        required: 'Please select a valid month',
                         minlength: 'Please select a valid month',
                      
                    },
					amount:{
						  required: 'Please enter the amount',
					},
					check_status:{
						 required: 'Please select the option',
					}
                  
                   
                }
            });
        
    $.validator.addMethod('CCExp2', function(value, element, params) {
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  
	//	  console.log(params)
   // alert(minMonth);alert(minYear);
		  var month = parseInt($('#thest_pay1').find(params.month).val(), 10);
		  var year = parseInt($('#thest_pay1').find(params.year).val(), 10);
    // alert(month);alert(year);
		  return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');

      
     
	$('#CardID1').change(function()
	{
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						 $('#thest_pay1 #number').remove();	
                         $('#thest_pay1 #exp_year').remove();	
                         $('#thest_pay1 #exp_month').remove();	
                         $('#thest_pay1 #cvc').remove();	
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
						 var form = $("#thest_pay1");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
							var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);
                      
									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
				
				
			});
		  }else{
           $("#card_loader").hide();
           $("#btn_process").attr("disabled", false);	
          }
		
	});		
       
	  

 });                             

 
 
</script>

