<?php
/**
 * template_start.php
 *
 * Author: pixelcave
 *
 * The first block of code used in every page of the template
 *
 */

?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <style type="text/css" media="print">
            @page 
            {
                size:  auto;   /* auto is the initial value */
                margin-bottom: 0;  /* this affects the margin in the printer settings */
            }
        </style>
        <meta charset="UTF-8">
        <meta name="google" content="notranslate">
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title><?php echo $template['title'] ?></title>

        <meta name="description" content="<?php echo $template['description'] ?>">
        <meta name="author" content="<?php echo $template['author'] ?>">
        <meta name="robots" content="<?php echo $template['robots'] ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo base_url(IMAGES); ?>/merch_icon.png">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon180.png" sizes="180x180">
        <!-- END Icons -->
		
		<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
		
         <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.4.6/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet"/>
        <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.4.6/bootstrap-editable/js/bootstrap-editable.min.js"></script>

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
       
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/main.css">
        <!-- Include a specific file here from <?php echo base_url(CSS); ?>/themes/ folder to alter the default theme of the template -->
        <?php if ($template['theme']) { ?><link id="theme-link" rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes/<?php echo $template['theme']; ?>.css"><?php } ?>

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes.css">
        
         <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/header_style_sheet.css">
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/footer_style.css?v=1.0">
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/custom.css?v=1.1">
        <link rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/common/common.css">
        <!-- END Stylesheets -->
	
        
        <!-- Modernizr (browser feature detection library) -->
		<script src="<?php echo base_url(JS); ?>/vendor/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/plugins.js"></script>
        <script src="<?php echo base_url(JS); ?>/app.js"></script>
        <script src="<?php echo base_url(JS); ?>/custom.js"></script>
        
        <script src="<?php echo base_url(JS); ?>/pages/formsWizard.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/modernizr-respond.min.js"> </script>
        <script src="<?php echo base_url(JS); ?>/pages/custom_footer.js?v=1.8"> </script>
        <script>
            var logoutURL = "<?php echo base_url('logout'); ?>";
            var timer = 0;
            function set_interval() {
            
                timer = setInterval(function(){ auto_logout(); }, 1800000);
            
            }

            function reset_interval() {
        
                if (timer != 0) {
                    clearInterval(timer);
                    timer = 0;
                    timer = setInterval(function(){ auto_logout(); }, 1800000);
                }
            }

            function auto_logout() {
                window.location = logoutURL;
            }
           
        </script>
        <style>
		.remove-hover {
         pointer-events: none;
		 
		}
		</style>
        
        
    </head>

    <body onload="set_interval()"
onmousemove="reset_interval()"
onclick="reset_interval()"
onkeypress="reset_interval()"
onscroll="reset_interval()">