<?php

/**
 * page_head.php
 *
 * Author: Chargezoom
 *
 * Header and Sidebar of each page
 *
 */
?>
<style>
    #dis_sidebar, #dis_my_account {
        pointer-events: none;
    }

    .mt-1 {
        margin-top: 10px;
        color: gray;
        position: fixed;
        left: 15px;
        bottom: 20px;
    }

    .searchBtnHeight{
        height: 32px !important;
    }

    @media (max-width: 767px){
        .search_head{
            width: 120px !important;
        }
    }

    #btn{
        background-color: #A5A5A5 !important;
    }
</style>


<script>
    $(function() {
        FormsWizard.init();
    });
    
</script>

<script>
    $(document).ready(function() {
        $('[data-toggle="modal"]').tooltip();

    });
</script>



<script>
    function searchbtn() {

        if (document.getElementById("search").value === "") {
            document.getElementById('btn').disabled = true;
        } else {
            document.getElementById('btn').disabled = false;
        }



    }
</script>



<?php
$CI = &get_instance();
$user_info = $CI->session->userdata('user_logged_in');
$user_info2 = $CI->session->userdata('logged_in');
$names = get_names();
$notificationReadMerchant = $names['notification_read'];

$firstName = $names['firstName'];
$lastName = $names['lastName'];
$resellerID = $names['resellerID'];
$script = '';
$logoimg = '';
$script_data     = get_chat_script($resellerID);

if (!empty($script_data['ProfileURL'])) {
    $logoimg = $script_data['ProfileURL'];
} else {
    $logoimg = CZHEADERLOGO;
}

if (!empty($script_data['Chat'])) {

    $script = $script_data['Chat'];
} else {

    $script  = '';
}

echo '<div style="display:none;">'.$script.'</div>';

$active_app = $firstLogin = 1;
if(isset($user_info2) && !empty($user_info2)){

  
    $active_app = $user_info2['active_app'];
    $firstLogin = $user_info2['firstLogin'];;
} else if(isset($user_info) && !empty($user_info)) {
    $active_app = $user_info['active_app'];
}


$base_path = '';
$searchURL = base_url('home/invoices');
if ($active_app == 1) {
    $base_path = 'QBO_controllers';
    $searchURL = base_url('QBO_controllers/Create_invoice/Invoice_details');
} else if ($active_app == 3) {
    $base_path = 'FreshBooks_controllers';
    $searchURL = base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details');
} else if ($active_app == 4) {
    $base_path = 'Integration';
    $searchURL = base_url('Xero_controllers/Xero_invoice/Invoice_detailes');
} else if ($active_app == 5) {
    $base_path = 'company';
    $searchURL = base_url('company/home/invoices');
}

restoreAuth();


?>
<div id="page-wrapper" <?php if ($template['page_preloader']) {
                            echo ' class="page-loading"';
                        } ?>>
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader themed-background-pre">
        <div class="inner">
            <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="spinner">
                <img id="img-spinner" src="<?php echo base_url(IMAGES); ?>/loading.gif" alt="loading">
            </div>
        </div>
    </div>


    <?php
    $page_classes = '';

    if ($template['header'] == 'navbar-fixed-top') {
        $page_classes = 'header-fixed-top';
    } else if ($template['header'] == 'navbar-fixed-bottom') {
        $page_classes = 'header-fixed-bottom';
    }

    if ($template['sidebar']) {
        $page_classes .= (($page_classes == '') ? '' : ' ') . $template['sidebar'];
    }

    if ($template['main_style'] == 'style-alt') {
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'style-alt';
    }

    if ($template['footer'] == 'footer-fixed') {
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'footer-fixed';
    }

    if (!$template['menu_scroll']) {
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'disable-menu-autoscroll';
    }

    if ($template['cookies'] === 'enable-cookies') {
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'enable-cookies';
    }
    ?>
    <div id="page-container" <?php if ($page_classes) {
                                    echo ' class="' . $page_classes . '"';
                                } ?>>

        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <div class="sidebar-brand">
                        <img style="width:203px; height:70px;" src="<?php echo $logoimg; ?>" class="icon" alt="avatar">
                    </div>
                    <!-- END Brand -->


                    <?php if ($primary_nav) { ?>
                        <?php if (isset($firstLogin) && $firstLogin == 0) { ?>
                            <!-- Sidebar Navigation -->
                            <ul class="sidebar-nav" id="dis_sidebar">
                                <?php foreach ($primary_nav as $key => $link) {

                                    if (!empty($link)) {
                                        $link_class = '';
                                        $li_active  = '';
                                        $menu_link  = '';
                                        $name = "";
                                        // Get 1st level link's vital info
                                        $name        = (isset($link['name']) && $link['name']) ? $link['name'] : '#';
                                        $url          = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
                                        if ($name == "Dashboard") {

                                            if ($firstLogin == 0) {

                                                $url  =    base_url('firstlogin/dashboard_first_login');
                                                $link['page_name'] = "dashboard_first_login";
                                            } else if ($firstLogin == '1' && $this->session->userdata('logged_in')['active_app'] == '1') {
                                                // $url  =    base_url("$base_path/home/index");
                                                $link['page_name'] = "index";
                                            } else {
                                                $url  =    base_url('home/index');
                                                $link['page_name'] = "index";
                                            }
                                        }

                                       
                                        $active     = (isset($link['url']) && ($template['active_page'] == $link['page_name'])) ? ' active' : '';
                                        $icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . ' sidebar-nav-icon"></i>' : '';


                                        if (isset($link['sub']) && $link['sub']) {
                                            // Since it has a submenu, we need to check if we have to add the class active
                                            // to its parent li element (only if a 2nd or 3rd level link is active)

                                            foreach ($link['sub'] as $sub_link) {


                                                if (in_array($template['active_page'], $sub_link)) {
                                                    $li_active = ' class="active"';
                                                    break;
                                                }

                                                // 3rd level links
                                                if (isset($sub_link['sub']) && $sub_link['sub']) {
                                                    foreach ($sub_link['sub'] as $sub2_link) {
                                                        if (in_array($template['active_page'], $sub2_link)) {
                                                            $li_active = ' class="active"';
                                                            break;
                                                        }
                                                    }
                                                }
                                            }

                                            $menu_link = 'sidebar-nav-menu';
                                        }

                                        // Create the class attribute for our link
                                        if ($menu_link || $active) {

                                            $link_class = ' class="' . $menu_link . $active . '"';
                                        }
                                ?>
                                        <?php if ($url == 'header') {      // if it is a header and not a link 
                                        ?>
                                            <li class="sidebar-header">
                                                <?php if (isset($link['opt']) && $link['opt']) { // If the header has options set
                                                ?>
                                                    <span class="sidebar-header-options clearfix"><?php echo $link['opt']; ?></span>
                                                <?php } ?>
                                                <span class="sidebar-header-title"><?php echo $link['name']; ?></span>
                                            </li>
                                        <?php } else { // If it is a link 
                                        ?>
                                            <li<?php echo $li_active; ?>>
                                                <a id="<?php if (isset($link['id']) && $link['id'] != "") {
                                                            echo $link['id'];
                                                        } ?>" href="<?php echo $url; ?>" <?php echo $link_class; ?>><?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu 
                                                                                                                                                                                        ?><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><?php }
                                                                                                                                                                                        echo $icon; ?><span class="sidebar-nav-mini-hide"><?php echo $link['name']; ?></span></a>
                                                <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu 
                                                ?>
                                                    <ul>
                                                        <?php foreach ($link['sub'] as $sub_link) {
                                                            $link_class = '';
                                                            $li_active = '';
                                                            $submenu_link = '';

                                                            // Get 2nd level link's vital info
                                                            $url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                                                           
                                                            $active     = (isset($sub_link['url']) && ($template['active_page'] == $sub_link['page_name'])) ? ' active' : '';

                                                            // Check if the link has a submenu
                                                            if (isset($sub_link['sub']) && $sub_link['sub']) {
                                                                // Since it has a submenu, we need to check if we have to add the class active
                                                                // to its parent li element (only if a 3rd level link is active)
                                                                foreach ($sub_link['sub'] as $sub2_link) {
                                                                    if (in_array($template['active_page'], $sub2_link)) {
                                                                        $li_active = ' class="active"';
                                                                        break;
                                                                    }
                                                                }

                                                                $submenu_link = 'sidebar-nav-submenu';
                                                            }

                                                            if ($submenu_link || $active) {
                                                                $link_class = ' class="' . $submenu_link . $active . '"';
                                                            }
                                                        ?>
                                                            <li<?php echo $li_active; ?>>
                                                                <a href="<?php echo $url; ?>" <?php echo $link_class; ?>><?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?><i class="fa fa-angle-left sidebar-nav-indicator"></i><?php }
                                                                                                                                                                                                                                        echo $sub_link['name']; ?></a>
                                                                <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
                                                                    <ul>
                                                                        <?php foreach ($sub_link['sub'] as $sub2_link) {
                                                                            // Get 3rd level link's vital info
                                                                            $url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                                                                            $active = (isset($sub2_link['url']) && ($template['active_page'] == $sub2_link['url'])) ? ' class="active"' : '';
                                                                        ?>
                                                                            <li>
                                                                                <a href="<?php echo $url; ?>" <?php echo $active ?>><?php echo $sub2_link['name']; ?></a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>
                                                                </li>
                                                            <?php } ?>
                                                    </ul>
                                                <?php } ?>
                                                </li>
                                    <?php }
                                    }
                                } ?>
                            </ul>
                            <!-- END Sidebar Navigation -->

                        <?php } else { // Default Header Content  
                        ?>

                            <ul class="sidebar-nav">
                                <?php foreach ($primary_nav as $key => $link) {

                                    if (!empty($link)) {
                                        $link_class = '';
                                        $li_active  = '';
                                        $menu_link  = '';
                                        $name = "";
                                        // Get 1st level link's vital info
                                        $name        = (isset($link['name']) && $link['name']) ? $link['name'] : '#';
                                        $url          = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
                                        if ($name == "Dashboard") {

                                            if ($firstLogin == 0) {

                                                $url  =    base_url('firstlogin/dashboard_first_login');
                                                $link['page_name'] = "dashboard_first_login";
                                            } else if ($firstLogin == '1') {
                                                // $url  =    base_url("$base_path/home/index");
                                                $link['page_name'] = "index";
                                            } else {
                                                $url  =    base_url('home/index');
                                                $link['page_name'] = "index";
                                            }
                                        }

                                       
                                        $active     = (isset($link['url']) && ($template['active_page'] == $link['page_name'])) ? ' active' : '';
                                        $icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . ' sidebar-nav-icon"></i>' : '';


                                        if (isset($link['sub']) && $link['sub']) {
                                            // Since it has a submenu, we need to check if we have to add the class active
                                            // to its parent li element (only if a 2nd or 3rd level link is active)

                                            foreach ($link['sub'] as $sub_link) {


                                                if (in_array($template['active_page'], $sub_link)) {
                                                    $li_active = ' class="active"';
                                                    break;
                                                }

                                                // 3rd level links
                                                if (isset($sub_link['sub']) && $sub_link['sub']) {
                                                    foreach ($sub_link['sub'] as $sub2_link) {
                                                        if (in_array($template['active_page'], $sub2_link)) {
                                                            $li_active = ' class="active"';
                                                            break;
                                                        }
                                                    }
                                                }
                                            }

                                            $menu_link = 'sidebar-nav-menu';
                                        }

                                        // Create the class attribute for our link
                                        if ($menu_link || $active) {

                                            $link_class = ' class="' . $menu_link . $active . '"';
                                        }
                                ?>

                                        <?php if ($url == 'header') {      // if it is a header and not a link 
                                        ?>
                                            <li class="sidebar-header">
                                                <?php if (isset($link['opt']) && $link['opt']) { // If the header has options set
                                                ?>
                                                    <span class="sidebar-header-options clearfix"><?php echo $link['opt']; ?></span>
                                                <?php } ?>
                                                <span class="sidebar-header-title"><?php echo $link['name']; ?></span>
                                            </li>
                                        <?php } else { // If it is a link 
                                        ?>
                                            <li<?php echo $li_active; ?>>
                                                <a id="<?php if (isset($link['id']) && $link['id'] != "") {
                                                            echo $link['id'];
                                                        } ?>" href="<?php echo $url; ?>" <?php echo $link_class; ?>><?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu 
                                                                                                                                                                                        ?><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><?php }
                                                                                                                                                                                        echo $icon; ?><span class="sidebar-nav-mini-hide"><?php echo $link['name']; ?></span></a>
                                                <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu 
                                                ?>
                                                    <ul>
                                                        <?php foreach ($link['sub'] as $sub_link) {
                                                            if(empty($sub_link)){
                                                                continue;
                                                            }
                                                            $link_class = '';
                                                            $li_active = '';
                                                            $submenu_link = '';

                                                            // Get 2nd level link's vital info
                                                            $url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                                                            
                                                            $active     = (isset($sub_link['url']) && ($template['active_page'] == $sub_link['page_name'])) ? ' active' : '';

                                                            // Check if the link has a submenu
                                                            if (isset($sub_link['sub']) && $sub_link['sub']) {
                                                                // Since it has a submenu, we need to check if we have to add the class active
                                                                // to its parent li element (only if a 3rd level link is active)
                                                                foreach ($sub_link['sub'] as $sub2_link) {
                                                                    if (in_array($template['active_page'], $sub2_link)) {
                                                                        $li_active = ' class="active"';
                                                                        break;
                                                                    }
                                                                }

                                                                $submenu_link = 'sidebar-nav-submenu';
                                                            }

                                                            if ($submenu_link || $active) {
                                                                $link_class = ' class="' . $submenu_link . $active . '"';
                                                            }
                                                        ?>
                                                            <li<?php echo $li_active; ?>>
                                                                <a href="<?php echo $url; ?>" <?php echo $link_class; ?>><?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?><i class="fa fa-angle-left sidebar-nav-indicator"></i><?php } echo $sub_link['name']; ?></a>
                                                                <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
                                                                    <ul>
                                                                        <?php foreach ($sub_link['sub'] as $sub2_link) {
                                                                            // Get 3rd level link's vital info
                                                                            $url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                                                                            $active = (isset($sub2_link['url']) && ($template['active_page'] == $sub2_link['url'])) ? ' class="active"' : '';
                                                                        ?>
                                                                            <li>
                                                                                <a href="<?php echo $url; ?>" <?php echo $active ?>><?php echo $sub2_link['name']; ?></a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>
                                                                </li>
                                                            <?php } ?>
                                                    </ul>
                                                <?php } ?>
                                                </li>
                                    <?php }
                                    }
                                } ?>
                               
                            </ul>


                        <?php } ?>
                    <?php } ?>

                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">

            <header class="navbar<?php if ($template['header_navbar']) {
                                        echo ' ' . $template['header_navbar'];
                                    } ?><meta http-equiv=" Content-Type" content="text/html; charset=utf-8"><?php if ($template['header']) {
                                                                                                                                                                                            echo ' ' . $template['header'];
                                                                                                                                                                                        } ?>
                <?php if ($template['header_content'] == 'horizontal-menu') { // Horizontal Menu Header Content 
                ?>

                    <!-- END Horizontal Menu + Search -->
                <?php } else { // Default Header Content  
                ?>
                    <!-- Left Header Navigation -->
                    <ul class="nav navbar-nav-custom">
                        <!-- Main Sidebar Toggle Button -->
                        <li style="padding-left: 4px;">
                            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                <i class="fa fa-bars fa-fw"></i>
                            </a>
                        </li>

                    </ul>

                    <!-- END Left Header Navigation -->
                    <div class="top-header pull-center">
                        <?php if ($firstLogin == 1) { ?>
                            <form class="fom-horizontal" method="post" action="<?php echo $searchURL?>" name="Form" onkeyup="searchbtn()" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control search_head" placeholder="Search" name="search_data" id="search">
                                    <div class="input-group-btn">
                                        <button class="btn btn btn-search searchBtnHeight" id="btn"  type="submit"><i class="gi gi-search icon-color"></i></button>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>
                    </div>


                    <!-- Right Header Navigation -->
                    <ul class="nav navbar-nav-custom pull-right">
                        
                        <!-- Notification alert -->
                        <?php 
                        if(isset($user_info2) && !empty($user_info2)){
                            $notification = array();
                            $notificationAllCount = get_merchant_notification_count($user_info2['merchID']);
                            $unReadCount = get_merchant_unread_notification($user_info2['merchID']);
                            $merchantID = $user_info2['merchID'];
                            

                        }else if(isset($user_info) && !empty($user_info)){
                            $notification = array();
                            $notificationAllCount = get_merchant_notification_count($user_info2['merchID']);
                            $unReadCount = get_merchant_unread_notification($user_info['merchantUserID']);
                            $merchantID = $user_info['merchantUserID'];
                        }else{
                            $notification =array();
                            $unReadCount = 0;
                            $notificationAllCount = 0;
                            $merchantID = 0;
                        }

                       
                        ?>
                        <input type="hidden" id="row" value="0">
                        <input type="hidden" id="all" value="5">
                        <input type="hidden" id="merchantID" value="<?php echo $merchantID; ?>">
                        <input type="hidden" id="base_path" value="<?php echo $base_path; ?>">
                        <input type="hidden" id="unReadCount" value="<?php echo $unReadCount; ?>">
                        <input type="hidden" id="notificationAllCount" value="<?php echo $notificationAllCount; ?>">
                        
                        
                        <li class="alert-notification dropdown clearNotification"> 
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                
                                
                                <span class="logo-alert" role="img" aria-label="Notifications"><i class="fa fa-bell" aria-hidden="true"></i></span>
                            </a>
                            <?php 
                                if($unReadCount > 0){
                                    echo '<div id="navigation-notification-count" aria-hidden="true" class="css-1v5plmp">'.$unReadCount.'</div>';
                                }
                                 
                           
                            $heightClass = '';
                            if($notificationAllCount > 5){
                               $heightClass = 'loadHeight';
                            }
                                     ?>
                            

                             
                                <?php 
                                echo '<div class="country dropdown-menu '.$heightClass.' dropdown-custom dropdown-menu-right"><ul id="results" class=" noscroll">';
                                if(count($notification) > 0){
                                    
                                    foreach ($notification as $nf) {
                                        if($nf['type'] == 1){
                                            $url = base_url(''.$base_path.'/Payments/payment_transaction');
                                        }else if($nf['type'] == 2){
                                            if($nf['recieverType'] == 1){
                                                $url = base_url(''.$base_path.'/home/invoice_details/'.$nf['typeID'].'');
                                            }elseif($nf['recieverType'] == 2){
                                                $url = base_url(''.$base_path.'/Create_invoice/invoice_details_page/'.$nf['typeID'].'');
                                            }elseif($nf['recieverType'] == 3){
                                                $url = base_url(''.$base_path.'/home/invoice_details/'.$nf['typeID'].'');
                                            }elseif($nf['recieverType'] == 4){
                                                $url = base_url(''.$base_path.'/Freshbooks_invoice/invoice_details_page/'.$nf['typeID'].'');
                                            }elseif($nf['recieverType'] == 5){
                                                $url = base_url(''.$base_path.'/Integration/Invoices/invoice_details/'.$nf['typeID'].'');
                                            }else{
                                                $url = '';
                                            }
                                            
                                        }else if($nf['type'] == 3){

                                            if($nf['recieverType'] == 1){
                                                $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                                            }elseif($nf['recieverType'] == 2){
                                                $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                                            }elseif($nf['recieverType'] == 3){
                                                $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                                            }elseif($nf['recieverType'] == 4){
                                                $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                                            }elseif($nf['recieverType'] == 5){
                                                $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                                            }else{
                                                $url = '';
                                            }
                                           
                                        }else{
                                            $url = base_url(''.$base_path.'/home/index');
                                        }
                                        if($nf['is_read'] == 1){
                                            $dis_color = 'black_show';
                                        }else{
                                            $dis_color = 'grey_show';
                                        }

                                        echo '<li id="nf'.$nf['id'].'" class="dropdown-header-li ">
                                            <div class="single-nf">
                                                <div id="read_nf'.$nf['id'].'" data-count="'.$unReadCount.'" data-read="'.$nf['is_read'].'" data-nfID="'.$nf['id'].'" class="nf-sub readNotification">
                                                    <a href="'.$url.'">
                                                        <div class="nf-message '.$dis_color.' ">
                                                            
                                                            <div class="nf-message-desc">
                                                                '.$nf['description'].'
                                                            </div>
                                                            <div class="nfCreatedTime">'.time_elapsed_string($nf['created_at']).'</div>
                                                        </div>
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                        </li>';
                                        
                                    }
                                    
                                }else{
                                 
                                } ?>
                            </ul>
                            <span id="loading"></span>
                            </div>
                            
                        </li>
                        <!-- User Dropdown -->
                        <li class="user-name"> 
                            <?php 
                            $get_merchant_data = [];
                            $get_merchant_data['merchantEmail'] = '';
                            $get_merchant_data['firstName'] = '';
                            $get_merchant_data['lastName'] = '';
                            $get_merchant_data['companyName'] = '';
                            if($this->session->userdata('logged_in')!="")
                             {
                        
                                $user_id                = $this->session->userdata('logged_in')['merchID'];
                               
                                
                                $get_merchant_data['merchantEmail'] = $this->session->userdata('logged_in')['merchantEmail'];
                                $get_merchant_data['firstName'] = $this->session->userdata('logged_in')['firstName'];
                                $get_merchant_data['lastName'] = $this->session->userdata('logged_in')['lastName'];
                                $get_merchant_data['companyName'] = $this->session->userdata('logged_in')['companyName'];

                             }
                             if( $this->session->userdata('user_logged_in')!="")
                             {
                            
                                $user_id                = $this->session->userdata('user_logged_in')['merchantID'];


                                $get_merchant_data['merchantEmail'] = $this->session->userdata('user_logged_in')['userEmail'];
                                $get_merchant_data['firstName'] = $this->session->userdata('user_logged_in')['userFname'];
                                $get_merchant_data['lastName'] = $this->session->userdata('user_logged_in')['userLname'];
                                $get_merchant_data['companyName'] = $this->session->userdata('user_logged_in')['merchant_data']['companyName'];
                             }
                            
                             ?>
                            <strong><?php echo $get_merchant_data['firstName']. ' '.$get_merchant_data['lastName']; ?></strong></li>

                        <li class="dropdown" style="padding-right: 10px;">

                            <?php
                            $email = $this->session->userdata('logged_in')['merchantEmail'];
                            $url = md5($email);
                           
                            $default = base_url(IMAGES) . '/placeholders/avatars/avatar1.jpg';
                            $size = 40;
                            $grav_url = "https://www.gravatar.com/avatar/" . md5(strtolower(trim($email))) . "?d=" . urlencode($default) . "&s=" . $size;

                            ?>

                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                               
                                <img src="<?php echo $grav_url; ?>" alt="user"> <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                <li>
                                    
                                    <?php if(isset($user_info2) && !empty($user_info2)){ ?>
                                    <a href="<?php echo base_url(''.$base_path.'/home/my_account'); ?>" data-toggle="tooltip" data-placement="bottom" title="My Account" id="<?php echo (isset($firstLogin) && $firstLogin == 0)?'dis_my_account':'ena_my_account'; ?>">
                                        <i class="fa fa-user fa-fw pull-right"></i>
                                        My Account
                                    </a>
                                    <?php } ?> 

                                    <?php if ($user_info2['merchID']) { ?>
                                        <a href="#modal-user-settings" data-toggle="modal">
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                            Change Password
                                        </a>
                                    <?php }
                                    if ($user_info['merchantUserID']) { ?>
                                        <a href="#modal-user-settings" data-toggle="modal">
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                            Change Password
                                        </a>
                                        <a href="<?php echo base_url('logout/index2'); ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit pull-right"></i>Logout</a>
                                    <?php }  ?>
                                    <?php if ($user_info2['merchID']) { ?>
                                        <a href="<?php echo base_url('logout'); ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit pull-right"></i>Logout</a><?php } ?>

                                </li>

                            </ul>
                        </li>
                        <!-- END User Dropdown -->
                    </ul>
                    <!-- END Right Header Navigation -->
                <?php } ?>
            </header>
            <!-- END Header -->


            <input type="hidden" id="js_base_url" value="<?php echo base_url(); ?>" />