<?php
/**
 * page_footer.php
 *
 * Author: Quadrish 
 *
 * The footer of each page
 *
 */
    $CI = &get_instance();

?>

<style>
#closeMsgData {
    float:right;
    display:inline-block;
    padding:0px 5px;
    font-size:20px; 
    color:#ff0000a8;
}

#closeMsgData:hover {
    cursor: pointer;
}
</style>

    <!-- Footer Starts Here -->
   <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"> </script>
   <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
   <script>
    function hideMsg() {
        $(".msg_data").fadeOut('slow');
        $(".msg_data1").fadeOut('slow');
    }
    $('.select-chosen').chosen({search_contains:true}) ;
    $('#cvv').keyup(function(event) {
        if (gtype == '5' || $('#stripeApiKey').val() != "") {
            var cvv = $('#cvv').val();
            if(cvv.length > 2){
                var pub_key = $('#stripeApiKey').val();
                Stripe.setPublishableKey(pub_key);
                Stripe.createToken({
                    number: $('#card_number').val(),
                    cvc: cvv,
                    exp_month: $('#expiry').val(),
                    exp_year: $('#expiry_year').val()
                }, stripeResponseHandler);
            }

            // Prevent the form from submitting with the default action
            return false;
        }
    });
     $('.clearNotification').click(function(event) {
        var merchID = $('#merchantID').val();
        $.ajax({
            type:"POST",
            url : base_url+'ajaxRequest/clearNotification',
            data : {'merchID':merchID},
            success : function(response){                    
                data=$.parseJSON(response);    
                if(data['status']=='success'){
                   
                    $('#navigation-notification-count').remove();
                    $('.nf-message').addClass('grey_show');
                    $('.nf-message').removeClass('black_show');

                }else{
                  
                }           
            }
        });
        
    });
    $(document).on("click", ".readNotification", function() {
        
        var nfID = $(this).attr('data-nfID');
        var count = $('#read_nf'+nfID).attr('data-count');
        var is_read = $('#read_nf'+nfID).attr('data-read');
        var updateCount = parseInt(count) - 1;
        
        if(is_read == 1){
            if(nfID != ''){
              
            }
        }
        
        
    });
    $(document).on("change", "#defaultMethod", function() {
        if($(this).is(":checked")){
            $(this).val(1);
        }else{
            $(this).val(0);
        }
        
    });
    
   </script>

    <script>
        $(function() {
           loadResults(0);
            $('.country').scroll(function() {
              if($("#loading").css('display') == 'none') {
                if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                   var limitStart = $("#results li").length;
                   loadResults(limitStart); 
                }
              }
            }); 
         
            function loadResults(limitStart) {

                var merchID = $('#merchantID').val();
                var base_path = $('#base_path').val();
                var unReadCount = $('#unReadCount').val();
                var notificationAllCount = $('#notificationAllCount').val();

                if(notificationAllCount != limitStart){
                    $("#loading").show();
            
                    $.ajax({
                        url : base_url+'ajaxRequest/loadNotification',
                        type: "post",
                        data: {
                            merchID:merchID,
                            base_path:base_path,
                            unReadCount:unReadCount,
                            notificationAllCount:notificationAllCount,
                            limitStart: limitStart
                        },
                        success: function(response) {
                            data=$.parseJSON(response);  
                              
                                $("#loading").hide();    
                                if(data['status'] == 'success'){

                                    $('#results').append(data['data']);
                                }else{
                                    if(notificationAllCount == 0){
                                        var html = '<li class="dropdown-header-li-blank "><div class="single-nf"><div class="nf-sub"><div class="nf-message"><div class="nf-message-head"><span></span></div><div class="nf-message-desc"><p class="alert_notification_p">No Notification Found</p></div></div></div></div></li>';
                                        $('#results').append(html);
                                    }
                                    return true;
                                }   
                                
                        }
                    });
                }else if(notificationAllCount == 0 && limitStart == 0){
                    $("#loading").hide();  
                    var html = '<li class="dropdown-header-li-blank "><div class="single-nf"><div class="nf-sub"><div class="nf-message"><div class="nf-message-head"><span></span></div><div class="nf-message-desc"><p class="alert_notification_p">No Notification Found</p></div></div></div></div></li>';
                    $('#results').append(html);
                }else{
                    $("#loading").hide();  
                  
                }
                
            };

            var CloseButton = `<i class="fa fa-times-circle " id="closeMsgData" data-dismiss="alert" onclick="hideMsg()"></i>`;
            var msg_data = $('.alert-danger').html();
            $('.alert-danger').html(msg_data+CloseButton);
        });
        </script>
    <script src="<?php echo base_url(JS); ?>/pages/custom_footer.js?version=1.6"> </script>
   
 
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->




<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
  <?php
            $stripeKey = $user_id = '';
            $changePasswordURL = base_url().'firstlogin/change_password';
             if($this->session->userdata('logged_in')!="")
             {
			$session =	$this->session->userdata('logged_in');
		
			    $user_id 				= $this->session->userdata('logged_in')['merchID'];
			 }
			 if( $this->session->userdata('user_logged_in')!="")
			 {
                $changePasswordURL = base_url().'user/login/change_password';
			    $session =	$this->session->userdata('user_logged_in');
			    $user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			 }
            if($user_id!=""){
                $get_merchant_data = get_merchant_data($user_id);
            }else{
                $get_merchant_data = [];
                $get_merchant_data['merchantEmail'] = '';
                $get_merchant_data['firstName'] = '';
                $get_merchant_data['lastName'] = '';
                $get_merchant_data['companyName'] = '';
            }
             
			 if($user_id!="")
			 $stripeKey = get_default_gateway_data($user_id);
			 if($stripeKey!="")
			 {
			 echo '<input type="hidden" name="stripeApiKey" id="stripeApiKey" value="'.$stripeKey.'"  />';
			 echo '<input type="hidden" name="gtType" id="gtType" value="5"  />';
			 }

        $names = get_names();
        $firstName = $names['firstName'];
        $lastName = $names['lastName'];
        $userEmail = $names['userEmail'];
        $companyName = $names['companyName'];
?>

 <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  id="custom-modal">  
     
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Card</h2>
                   <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span> </button>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
            
            <p>Are you sure you want to delete this card?</p>
                
                 <form id="thest_del" method="post" action='<?php echo base_url(); ?>QBO_controllers/Payments/delete_card_data' class="form-horizontal" >
              
                    
                   
                  <div class="pull-right">
                     <input type="submit"  name="btn_process" class="btn btn-sm btn-danger" value="Delete"  />
                     <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
                </form>     
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>







<div id="recurring_payment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="hd_title" >Setup Recurring Payments</h2>
                  <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
               
                <div id="sch_div">
                   
                </div>
		
			  <div id="recurring-content-data" >
                <form action="" id="recurringform" method="post" action="<?php echo base_url() ?>"  class="form-horizontal" >
                    
                    <div class="form-group ">
                      
                        <label class="col-md-4 control-label" for="card_list">Select Payment Method</label>
                        <div class="col-md-8">
                            <select  id="rec_cardID" name="rec_cardID"   class="form-control">
                                <option value="" >Please Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="Automatic">Run Automatic Payment</label>
                        <div class="col-md-8">
                            <select id="recurAuto" name="recurAuto"   class="form-control">
                                <option  value="1" >On Due Date</option>
                                 <option value="2" >On Invoice Date</option>
                                <option  value="3" >1 Day after Invoice Date</option>
                                <option  value="4" >Calendar Date</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="calendar_date_day_div">
                        <label class="col-md-4 control-label" for="calendar_date_day">Day of Month</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="calendar_date_day" id="calendar_date_day" value="">
                        </div>
                    </div> 
                    
                    <div class="form-group ">
                      
                        <label class="col-md-4 control-label" for="recurring">When Invoice Amount is less than</label>
                        <div class="col-md-8">
                           <input type="text" id="recAmount" name="recAmount"   class="form-control" value="" placeholder="1000.00" />
                        </div>
                    </div>
                    
                    
                   <div class="form-group">
                        <label class="col-md-4 control-label" for="reference"></label>
                        <div class="col-md-6">
                            <input type="checkbox" id="setMailRecurring" name="setMail" class="set_checkbox"   /> Send Customer Receipt
                        </div>
                    </div>
                    
                    <div class="pull-right">
        			    <input type="submit"  id="qb_recc"  name="btn_sub" class="btn btn-sm btn-info" value="Save"  />
                        <button type="button" id="qb_recc11"   class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>
			</div>
            <br />
            <br />
			   
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="del_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck11" method="post"  onsubmit="return false;" class="form-horizontal" enctype='multipart/form-data' >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="subscID1" name="subscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="button" id="qbd_btsub" onclick="delete_qbd_subscription();"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="del111_qbo_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" enctype='multipart/form-data'>
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del111_jkck" method="post"  onsubmit="return false;" class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="qbosubscID" name="qbosubscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="button"  id="qbo111_btsub" onclick="delete_qbo_subscription();"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
 
 <div id="del_fb_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckfb" method="post" onsubmit="return false;" class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="fbsubscID" name="fbsubscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="button"  id="fb_btsub"  onclick="delete_fb_subscription();"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="del_xero_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Subscription</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckqbo" method="post" action='<?php echo base_url(); ?>Xero_controllers/SettingSubscription/delete_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Subscription?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="xerosubscID" name="xerosubscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="deactive_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Deactivate Product / Service</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckxero" method="post" action='<?php echo base_url(); ?>MerchantUser/change_product_status' class="form-horizontal" >
                     
                 
					<p>Do you really want to deactivate this Product / Service?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="prolistID" name="prolistID" class="form-control"  value="" />
							 <input type="hidden" id="status" name="status" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="active_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Activate Product / Service</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckqq" method="post" action='<?php echo base_url(); ?>MerchantUser/change_product_status' class="form-horizontal" >
                     
                 
					<p>Do you really want to activate this product / service?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="proactiveID" name="proactiveID" class="form-control"  value="" />
                              <input type="hidden" id="status" name="status" class="form-control"  value="1" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" name="btn_cancel" class="btn btn-sm btn-info" value="Activate"  />
                    <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="active_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Activate Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckde" method="post" action='<?php echo base_url(); ?>MerchantUser/delete_customer' class="form-horizontal" >
                     
                 
					<p>Do you really want to activate this Customer?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="custactiveID" name="custactiveID" class="form-control"  value="" />
                              <input type="hidden" id="status" name="status" class="form-control"  value="1" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-info" value="Activate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="del_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Deactivate Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkckre" method="post" action='<?php echo base_url(); ?>MerchantUser/delete_customer' class="form-horizontal" >
                     
                 
					<p>Do you really want to deactivate this Customer?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="custID" name="custID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

 

<div id="del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_credit" method="post" action='<?php echo base_url(); ?>Payments/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoicecreditid" name="invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="qbo_del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_creditee" method="post" action='<?php echo base_url(); ?>QBO_controllers/Payments/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="qbo_invoicecreditid" name="qbo_invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="fb_del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_creditww" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/Transactions/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="fb_invoicecreditid" name="fb_invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="xero_del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_creditss" method="post" action='<?php echo base_url(); ?>Xero_controllers/Payments/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="xero_invoicecreditid" name="xero_invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<?php
if(!isset($deleteRoleURL)){
    $deleteRoleURL = "MerchantUser/delete_role";
}
?>

<div id="del_role" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Role</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_roleww" method="post" action='<?php echo base_url($deleteRoleURL); ?>' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Role?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantroleID" name="merchantroleID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="pay_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="h_title">Invoice Payment Details</h2>
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			 <table id="pay-content-data" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left" >Transaction ID</th>
                     <th class="text-right hidden-xs">Amount</th>
                    <th class="text-right visible-lg">Date</th>
                     <th class="text-right hidden-xs">Gateway</th>
                    <th class="hidden-xs text-right">Status</th>
                   
                </tr>
            </thead>
			  <tbody >
                
			
				
			</tbody>
        </table>
                  <div class="pull-right">
                    <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Close</button>
        			</div>
                     <br />
                    <br />
			   
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>









<div id="del_company" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Application</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_comp" method="post" action='<?php echo base_url(); ?>home/delete_company' class="form-horizontal" >
					<p>All data will be lost, do you really want to delete this Application?</p> 
				    <div class="form-group">
                        <div class="col-md-8">
                            <input type="hidden" id="companyID" name="companyID" class="form-control"  value="" />
                        </div>
                    </div>
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="invoice_cancel" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Void Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                       <form id="Qbwc-form" method="post" action='<?php echo base_url(); ?>SettingSubscription/update_customer_invoice_cancel' class="form-horizontal" >
                 
                 
					<p>Do you really want to void this Invoice?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceID" name="invoiceID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                  
                    <div class="modal-header text-center">
                        <h2 class="modal-title">Change Password</h2>
                    </div>
                   
        
                    <div class="modal-body">
                        <div id="expiryNotes" >
                            Your passwords has been expired no later than one hundred twenty (120) consecutive days after issuance please change you current password.
                        </div>
                        <form id="change_merch_pwd" action="<?= $changePasswordURL; ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                           
                            <input type="hidden" value="<?= $userEmail;  ?>"   id="merchantEmail" />
                            <input type="hidden" value="<?= $firstName;  ?>"   id="firstName" />
        
                            <input type="hidden" value="<?= $companyName;  ?>"   id="companyName" />
                            <fieldset>
        
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Name</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?= $firstName.' '.$lastName; ?></p>
                                        <input type="hidden" value="<?= $user_id;  ?>"  name="user_id" id="user_id" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Email</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?= $userEmail;  ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-currentpassword">Current Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user_settings_currentpassword" name="user_settings_currentpassword" class="form-control" placeholder="Enter Current Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user_settings_password" name="user_settings_password" class="form-control" placeholder="Enter New Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user_settings_repassword" name="user_settings_repassword" class="form-control" placeholder="Confirm New Password">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group form-actions transparentBackground">
                                <div class="col-xs-12 text-right">
                                     <button type="submit" class="btn btn-sm btn-success">Save</button>
                                    <button id="pswd_chng_close"  type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Close</button>
                                   
                                </div>
                            </div>
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>





<div id="qb-connecter-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Setup QuickBooks Desktop</h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span> </button>
                
                
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="Qbwc_form" method="post" action='<?php echo base_url(); ?>QuickBooks/config' class="form-horizontal" >
                     
                        <?php 
							// $username  = $qbusername;
							$username = $this->config->item('quickbooks_user');	
							$randomNum =substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 5);
							$username  = $username.$randomNum;
						?>
                        
                      <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-currentpassword">Application Name</label>
                            <div class="col-md-8">
                                <input type="text" id="qbcompany_name" name="qbcompany_name" class="form-control" readonly value="PayPortal" placeholder="Application Name"  >
                               
                            </div>
                        </div> 
                        
                          <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-currentpassword">Web Connector Username</label>
                            <div class="col-md-8">
                                <input type="text" id="qbusername" name="qbusername" class="form-control" readonly  value="<?php echo  $username ?>" />
                                
                            </div>
                        </div> 
						 
						
					    <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-password">Web Connector Password</label>
                            <div class="col-md-8">
                                <input type="password" id="qbpassword" name="qbpassword" class="form-control" placeholder="Web Connector Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-repassword">Confirm Password</label>
                            <div class="col-md-8">
                                <input type="password" id="qbrepassword" name="qbrepassword" class="form-control" placeholder="Confirm Password">
                            </div>
                        </div>
                      <div class="form-group form-actions">
                        <div class="col-xs-12 text-center">
                            <input type="submit" id="qb-generate" name="qb-generate" class="btn btn-sm btn-success" value="Generate Web Connector File"  />
                           
                        </div>
                    </div>
					
			   </form>		
                
            </div>
            <div class="modal-footer text-right">
           
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="qb-connecter-info" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">QuickBooks Setup Instructions</h2>
                
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="" method="post"  class="form-horizontal" >
                     	<div class="form-group">
                            <label class="col-md-2 control-label">1</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Install QuickBooks Web Connector at the machine where your QuickBooks is installed.</p>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-2 control-label">2</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Configure your Web Connector using: Start->QuickBooks->QuickBooks Webconnector.</p>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-md-2 control-label">3</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Add the downloaded .qwc file using Add Application button at the QuickBooks Webconnector UI. </p>
                            </div>
                        </div>
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label">4</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Enter the password which you setup at step 4.</p>
                            </div>
                        </div>
						
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label">5</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Click on update button to import data from QuickBooks</p>
                            </div>
                        </div>
					
						
					 
					
			   </form>		
                
            </div>
            <div class="modal-footer text-right">
            <button type="button" class="btn btn-sm btn-danger " data-dismiss="modal">Close</button>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="invoice_modal_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Processed Today</h2>                
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
            	<div class="table-wrapper" style="width: 100%;height: 300px;overflow: auto;">
            	<table width="100%" class="table processed_table">
            		<thead>
            		<tr>
            			<th>ID</th>
            			<th>Name</th>
            			<th>Invoice</th>
            			<th>Amount</th>
            			<th>Type</th>
            			<th>Status</th>
            		</tr>
            	</thead>
                <tbody id="processed_server_data">
                	
                </tbody>
            </table>
            </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="scheduled_invoice_modal_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Scheduled Invoices</h2>                
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
            	<div class="table-wrapper" style="width: 100%;height: 300px;overflow: auto;">
            	<table width="100%" class="table scheduled_table">
            		<thead>
            		<tr>
            			<th style="font-size:14px !important">Customer Name</th>
            			<th style="font-size:14px !important">Invoice</th>
            			<th style="font-size:14px !important">Due Date</th>
            			<th style="font-size:14px !important">Payment</th>
            			<th style="font-size:14px !important">Balance</th>
            			<th style="font-size:14px !important">Status</th>
            		</tr>
            	</thead>
                <tbody id="scheduled_server_data">
                	
                </tbody>
            </table>
            </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="failed_invoice_modal_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Failed Invoices</h2>                
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
            	<div class="table-wrapper" style="width: 100%;height: 300px;overflow: auto;">
            	<table width="100%" class="table failed_table">
            		<thead>
            		<tr>
            			<th style="font-size:14px !important">Customer Name</th>
            			<th style="font-size:14px !important">Invoice</th>
            			<th style="font-size:14px !important">Due Date</th>
            			<th style="font-size:14px !important">Payment</th>
            			<th style="font-size:14px !important">Balance</th>
            			<th style="font-size:14px !important">Status</th>
            		</tr>
            	</thead>
                <tbody id="failed_server_data">
                	
                </tbody>
            </table>
            </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 <!----------------- to view email popup modal --------------------->

<!-------------------------------  END  --------------------------------->
<!----------------- Refund  popup modal --------------------->
 <div id="refundAmountModel" class="modal fade" role="dialog">
  <div class="modal-dialog custom-modal-width">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <h2 class="modal-title">Refund Payment</h2>
      </div>
      <div class="modal-body modal-body-bottom-padding clearfix">
            <div id="refundAmountTable"></div>
            
            <div class="form-group alignTableInvoiceListTotal">
                   
                <div class="col-md-2 text-center"></div>
                <div class="col-md-2 text-left"></div>
                <div class="col-md-3 text-right"></div>
                <div class="col-md-2 text-right totalTextLeftRefund" >Total</div>
                <div class="col-md-3 text-left totalTextRightRefund" >
                    $<span id="refundTotalamount">0.00</span>
                </div>
            </div>
      </div>
      <div class="modal-footer modal-footer-witout">
        <form id="refundForm" method="post" action='<?php echo base_url(); ?>Common_controllers/refundInvoicePartial/commanRefundInvoice' class="form-horizontal" >
            
          <input type="hidden" name="refundTotalamountInput" id="refundTotalamountInput" value="0.00">
          <input type="hidden" name="refundInvoiceIDs" id="refundInvoiceIDs" value="">
          <input type="hidden" name="integrationType" id="integrationType" value="5">
          <input type="hidden" name="refundInvoiceAmount" id="refundInvoiceAmount" value="">

          <input type="hidden" name="refundRemainingAmount" id="refundRemainingAmount" value="">

          <input type="hidden" name="txnRowIDs" id="txnRowIDs" value="">

          <input type="hidden" name="transactionID" id="transactionID" value="">
          <input type="hidden" name="gatewaytype" id="gatewaytype" value="">
          <input type="hidden" name="gatewayName" id="gatewayName" value="">
          
          <div class="pull-right">
            <input type="submit" id="refundBtn" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund">
            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>

<!----------------- Invoice total amount Refund  popup modal --------------------->
 <div id="refundInvoiceAmountModel" class="modal fade" role="dialog">
  <div class="modal-dialog custom-modal-width">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header text-center">
            <h2 class="modal-title">Refund Payment</h2>
        </div>
        <form id="refundForm" method="post" action='<?php echo base_url(); ?>Common_controllers/refundInvoicePartial/totalToPartialRefundInvoice' class="form-horizontal" >
          <div class="modal-body modal-body-bottom-padding clearfix">
                <div id="refundTotalAmountTable"></div>
                
                <div class="form-group alignTableInvoiceListTotal">
                       
                    <div class="col-md-2 text-center"></div>
                    <div class="col-md-2 text-left"></div>
                    <div class="col-md-3 text-right"></div>
                    <div class="col-md-2 text-right totalTextLeftRefund" >Total</div>
                    <div class="col-md-3 text-left totalTextRightRefund" >
                        $<span id="refundTotalInvoiceAmount">0.00</span>
                    </div>
                </div>
          </div>
          <div class="modal-footer modal-footer-witout">
            
                
               <input type="hidden" name="refundTotalInvoiceAmountInput" id="refundTotalInvoiceAmountInput" value="0.00">
                
              <input type="hidden" name="refundInvoiceID" id="refundInvoiceID" value="">
              <input type="hidden" name="refundIntegrationType" id="refundIntegrationType" value="5">

              <input type="hidden" name="refundInvoiceRemainingAmount" id="refundInvoiceRemainingAmount" value="">

              
              <div class="pull-right">
                <input type="submit" id="refundInvoiceBtn" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund">
                <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
              </div>
            
          </div>
        </form>
    </div>

  </div>
</div>
<div id="sendgrid_email_detail_popup" class="modal fade" role="dialog">
    <div class="modal-dialog custom-modal-width">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h2 class="modal-title">Email Information</h2>
            </div>
            <div class="modal-body modal-body-bottom-padding clearfix">
                <div id="sendgrid-data-loader" class="text-center">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div id="sendgrid-email-detail-information">
                    <table class="table table-bordered table-vcenter">
                        <h4><strong>Details</strong></h4>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4"><strong>To</strong></div>
                                        <div class="col-md-8"><span class="send-grid-to-email"></span></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4"><strong>From</strong></div>
                                        <div class="col-md-8"><span id="send-grid-from-email"></span></div>
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Subject</strong></div>
                                        <div class="col-md-8"><span id="send-grid-email-subject"></span></div>
                                    </div> 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br><br>
                    <h4><strong>Event History</strong></h4>
                    <table class="table table-bordered table-vcenter">
                        <thead>
                            <th>Received by Sendgrid</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4"><strong><i class="fa fa-check-circle" style="color: rgb(4, 170, 109);font-size: 20px;"></i></strong> <span style="margin-left: 10px;">Processed</span></div>
                                        <div class="col-md-7"><span id="send-grid-received-date" class="pull-right"></span></div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-vcenter">
                        <thead>
                            <th>Received by <span id="sendgrid_mx_server"></span></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4"><strong><i class="fa fa-check-circle" style="color: rgb(4, 170, 109);font-size: 20px;"></i></strong> <span style="margin-left: 10px;">Delivered</span></div>
                                        <div class="col-md-7"><span id="send-grid-delivered-date" class="pull-right"></span></div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-vcenter" id="send-grid-opened-event-details">
                        <thead>
                            <th>Received by <span class="send-grid-to-email"></span></th>
                        </thead>
                        <tbody id="sendgrid_open_event_details">
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer modal-footer-witout">
                <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).on("click",".refunTotalInvoiceAmountCustom",function() {
    
  var invoiceID = $(this).attr('invoice-id');
  var integrationType = $(this).attr('integration-type');
  var URL = $(this).attr('data-url');
  
  $('#refundInvoiceID').val(invoiceID);
  $('#refundIntegrationType').val(integrationType);


  if (invoiceID != "") {
      $.ajax({
          type: "POST",
          url: URL,
          data: {
              'invoiceID': invoiceID,
              'integrationType': integrationType
          },
          success: function(response) {
            data = $.parseJSON(response);
                    
            if (data['status'] == 'success') {
              $('#refundTotalAmountTable').html('');
              
              if (!jQuery.isEmptyObject(data['invoices'])) {  
                $('#refundTotalAmountTable').html(data['invoices']);
                $('#refundTotalInvoiceAmount').html(parseFloat(data['totalInvoiceAmount']).toFixed(2));
                $('#refundTotalInvoiceAmountInput').val(parseFloat(data['totalInvoiceAmount']).toFixed(2));
                $('#refundIntegrationType').val(data['integrationType']);
               
                if(parseFloat(data['totalInvoiceAmount']) > 0){
                    $('#refundInvoiceBtn').show();
                }else{
                  $('#refundInvoiceBtn').hide();
                }

                $("#refundInvoiceAmountModel").modal("show");
              }
              var tmp = [];
              var payAmount = [];
              var tmpRowID = [];
              var sum = 0;
              $('.chk_pay_invoice_refund').each(function () {
                  var r_id = $(this).attr('id');
                  var inv_id = $(this).val();
                  
                  if ($(this).is(':checked')) {
                      sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
                      $('#test-inv-checkbox-'+$(this).val()).prop('checked', true);
                      if(data.applySurcharge != true){
                        $('.'+r_id).attr('readonly', false);
                      }

                      tmp.push(inv_id);
                      payAmount.push(parseFloat($('.' + r_id).val()));
                  }else{
                      $('#test-inv-checkbox-'+$(this).val()).prop('checked', false);
                      $('.'+r_id).attr('readonly', true);
                  }
              });  

              $('#refundInvoiceID').val(tmp);
              $('#refundTotalInvoiceAmountInput').val(payAmount);
              
              $('#refundInvoiceRemainingAmount').val(payAmount);
            }
          }
      });
  }
});
setInterval(function(){
    if ($(".dataTable")[0]){
        $.ajax({
            type: 'POST',
            url : base_url+'ajaxRequest/check_session',
            success: function(response){
                data=$.parseJSON(response);    

                if(data['status']=='failed'){
                    location.reload(true);
                }
                
            }
        });
    } 
},50000);
var customerInvoiceDisplayArray = [];
$(function() {
    $("#invoice_number_capture").chosen();
    $("#invoice_number_capture_chosen").on("focusout", "input", function (event)
    {

        var serachinputvalue = $(this).val();
        if(serachinputvalue != ''){

            var serachselectedvalue = '';
            var checkCUstomORnot = jQuery.inArray( serachinputvalue,  customerInvoiceDisplayArray);
            if(checkCUstomORnot < 0){
                $('#invoice_number_capture').append('<option value="'+serachinputvalue+'" selected>'+serachinputvalue+'</option>');
            }
            $("#invoice_number_capture > option").each(function () {
                if(this.text == ''){
                    $(this).remove();
                }else{
                    if(serachinputvalue == this.text){
                        $(this).remove();
                        $('#invoice_number_capture').trigger('chosen:updated');
                        serachselectedvalue = this.value;
                        $('#invoice_number_capture').append('<option value="'+this.value+'" selected>'+this.text+'</option>');
                    }
                }
            });
            var already_added = {};
            $("#invoice_number_capture > option").each(function () {
                if(already_added[this.text]) {
                    $(this).remove();
                    $('#invoice_number_capture').trigger('chosen:updated');
                } else {
                    already_added[this.text] = this.value;
                }
            });
            
            $('#amount').focus();
            $('#invoice_number_capture').trigger('chosen:updated');

            var oldInvoice = $('#invoice_ids').val();
            var invoiceStr = '';
            var sumCal = 0;
            var selectInvoiceTotal = 0;
            var oldAmountTotal = $('#totalamount1').html();
            
            if(oldInvoice != ''){
                var oldInvoiceArr = oldInvoice.split(",");
                
                var index = oldInvoiceArr.length;
                
                oldInvoiceArr[index] = serachselectedvalue;
                invoiceStr =  oldInvoiceArr.toString(); 
            }else{
                 invoiceStr = serachselectedvalue;
            }
           
            $('#multiinv'+serachselectedvalue).prop('checked', true);
            $('#test-inv-checkbox-'+serachselectedvalue).prop('checked', true);
            if(customerInvoiceArray){
                for (var i = 0; i < customerInvoiceArray.length; i++) {
                    var checkCUstomORnot = jQuery.inArray( customerInvoiceArray[i], $("#invoice_number_capture").chosen().val() ); 
                    if(checkCUstomORnot < 0){
                        show_invoice_select_popup = false;
                    }else{
                        show_invoice_select_popup = true;
                        break;
                    }
                }
            }

            $('#invoice_ids').val(invoiceStr);
           
            $('#amount').focus();
            calculateInvoiceNumberPriceUpdate();
            $('#invoice_number_capture').valid();

            if(!show_invoice_select_popup){
                $('#amount').val(auth_org_amount);
            }
        }
    });
    $.validator.setDefaults({ ignore: ":hidden:not(#invoice_number_capture)" });
    $('#data_form').validate({
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function(error, e) {
            e.parents('.form-group > div').append(error);
        },
        rules: {
            "invoice_number_capture[]":{
            },
            "amount":{
                required: true,
            }
        },
        messages: {
        }
    });

    $(document).on('click','.cvvMask',function(){
        $(this).attr('type','password');
        $(this).attr('maxlength','4');
    });

    $(document).on('click','.CCMask',function(){
        $(this).attr('type','password');
    });

    $('#amountModel').on('hidden.bs.modal', function () {
        $('#payment_capturemod').modal({
            show: true,
            backdrop: 'static',
            keyboard: false
        });
    });
    
    
});
var auth_org_amount = 0;
function set_capture_pay(txnid, txntype, cid, amount, intersectionType)
{
    auth_org_amount = amount;
    $('#customerID').val(cid);
    $('#amount').val(amount);
    var baseURL = base_url;
    $('#captureIntegrationType').val(intersectionType);
    if(intersectionType == 1){
        var ajaxUrl = base_url+"QBO_controllers/Payments/check_vault";
    }else if(intersectionType == 2){
        var ajaxUrl = base_url+"Payments/check_vault";
    }else if(intersectionType == 3){
        var ajaxUrl = base_url+"FreshBooks_controllers/Transactions/check_vault";
    }else if(intersectionType == 4){
        var ajaxUrl = base_url+"Xero_controllers/Payments/check_vault";
    }else if(intersectionType == 5){
        var ajaxUrl = base_url+"company/Payments/check_vault";
    }else{
        var ajaxUrl = base_url+"company/Payments/check_vault";
    }
    
    if(txnid !=""){
        $('#captureTxnID').val(txnid);
        if(txntype=='1'){
            $('#txnID').val(txnid);
            var gatewaytype = 1;     
            
        } else if(txntype=='2'){
            $('#txnID1').val(txnid);
            var gatewaytype = 2;
        } else if(txntype=='3'){
            $('#txnID2').val(txnid);
            var gatewaytype = 3;
        } else if(txntype=='4'){
            var gatewaytype = 4;
            $('#paypaltxnID').val(txnid);
        } else if(txntype=='5'){
            var gatewaytype = 5;
            $('#strtxnID').val(txnid);
        } else if(txntype=='6'){
            var gatewaytype = 6;
            $('#useatxnID').val(txnid);
        } else if(txntype=='7'){
            var gatewaytype = 7;
            $('#hearttxnID').val(txnid);
        } else if(txntype=='8'){
            var gatewaytype = 8;
            $('#strtxnID').val(txnid);
        } else if(txntype=='9'){
            var gatewaytype = 9;
            $('#txnID').val(txnid);  
        } else if(txntype=='10'){
            var gatewaytype = 10;
            $('#txnID').val(txnid);  
        } else if(txntype=='11'){
            var gatewaytype = 11;
            $('#txnID').val(txnid);  
        }else if(txntype=='12'){
            var gatewaytype = 12;
            $('#txnID').val(txnid);  
        }else if(txntype=='15'){
            var gatewaytype = 15;
            $('#txnID').val(txnid);  
        } else if(txntype=='13'){
            var gatewaytype = 13;
            $('#txnID').val(txnid);  
        } else if(txntype =='17'){
            var gatewaytype = 17;
            $('#txnID').val(txnid);
        } else if(txntype=='16'){
            var gatewaytype = 16;
            $('#txnID').val(txnid);  
        } else if(txntype=='14'){
            var gatewaytype = 14;
            $('#txnID').val(txnid);  
        }
        $('#original_auth_amount').html(format2(amount));
    }

    $('#invoice_ids').val('');
    $('#invoice_number_capture').empty(); //remove all child nodes
    $('#invoice_number_capture').append('');
    $('#invoice_number_capture').trigger("chosen:updated");
    

    if (cid != "") {

        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: {
                'customerID': cid
            },
            success: function(response) {
                data = $.parseJSON(response);
                show_invoice_select_popup = false;
                if (data['status'] == 'success') {

                    var s = $('#card_list');
                    var optionselect_invoice = '';
                    $('#table2').html('');
                    if (!jQuery.isEmptyObject(data['invoices'])) {
                        $('#table2').html(data['invoices']);
                        $('#totalamount1').html(amount);
                        $('#totalamount').val(amount);
                        $('#amount').val(amount);
                        $('#isCustomize').val(1);
                        for (var valinv in data['invoiceIDs']) {
                            optionselect_invoice += '<option value="'+valinv+'">'+data['invoiceIDs'][valinv]+'</option>';
                            customerInvoiceArray.push(valinv);
                            customerInvoiceDisplayArray.push(data['invoiceIDs'][valinv]);
                        }
                    }else{
                        customerInvoiceArray = [];
                        customerInvoiceDisplayArray = [];
                    }
                    $('#invoice_number_capture').empty(); //remove all child nodes
                    $('#invoice_number_capture').append('<option>Please Select Invoice Number</option>');
                    $('#invoice_number_capture').append(optionselect_invoice);
                    $('#invoice_number_capture').trigger("chosen:updated");
                    $('.chk_pay').each(function () {
                        $(this).prop('checked', false);
                        var r_id = $(this).attr('id');
                        var inv_id = $(this).val();
                        var sum = 0;
                        var tmp = [];
                        var payAmount = [];
                        $('#test-inv-checkbox-'+$(this).val()).prop('checked', false);
                        $('.'+r_id).attr('readonly', true);

                    });
                    $('#totalamount1').html('0.00');
                }
            }

        });

    }
}   
function getSendGridEmailStatusDetail(mailID){
    $('#sendgrid-email-detail-information').hide();
    $('#sendgrid-data-loader').show();
    $('#send-grid-opened-event-details').hide();
    $.ajax({
        url: base_url+'Common_controllers/sendGrid/getSendGridEmailStatusDetail',
        type: 'POST',
        data:{mailID:mailID},
        dataType: 'json',
        success: function(response){
            $('.send-grid-to-email').html(response.mailData.emailto);
            $('#send-grid-from-email').html(response.mailData.emailfrom);
            $('#send-grid-email-subject').html(response.mailData.emailSubject);
            var table_tr_data = '';
            if(response.data){
                $.each(response.data, function(index, item) {
                    if(item.event_name == 'processed'){
                        $('#send-grid-received-date').html(item.date);
                    }else if(item.event_name == 'delivered'){
                        $('#send-grid-delivered-date').html(item.date);
                        $('#sendgrid_mx_server').html(item.mx_server);
                    }else{
                        
                        $('#send-grid-opened-event-details').show();
                        table_tr_data += '<tr><td><div class="row">';
                        table_tr_data += '<div class="col-md-4"><strong><i class="fa fa-check-circle" style="color: rgb(4, 170, 109);font-size: 20px;"></i></strong> <span style="margin-left: 10px;">Opened</span></div>';
                        table_tr_data += '<div class="col-md-7"><span class="pull-right">'+item.date+'</span></div>';
                        table_tr_data += '</div></td></tr>';
                    }
                });
            }
            $('#sendgrid_open_event_details').html(table_tr_data);
            $('#sendgrid-data-loader').hide();
            $('#sendgrid-email-detail-information').show();

        }   
    }); 
}
</script>