<!DOCTYPE html>
<html>
<head>
	<title>QuickBooks Disconnected</title>
	<meta charset="UTF-8">
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

    <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
	<link rel="shortcut icon" href="<?php echo base_url(IMAGES); ?>/merch_icon.png">
</head>
<style type="text/css">
	body {
		font-family: 'Poppins', sans-serif;
	}
	.quickbooks-dis-page{
		margin: 120px 0 30px 0;
		padding: 0 20px;
	}

	.quickbooks-dis-page h2{
		font-size: 32px;
	    border-bottom: 6px solid #0c7bff;
	    width: 420px;
	    margin: 50px auto;
	    margin-bottom: 65px;
	    padding-bottom: 15px;
	}

	.quickbooks-dis-page p{
		font-size: 16px;
		margin-bottom: 50px;
	}

	.quickbooks-dis-page p a{
		color: #0078ff;
	}

	@media (max-width: 767px){
		.quickbooks-dis-page h2{
			font-size: 22px;
			width: 285px;
		}
	}
</style>
<body>
	<div class="text-center quickbooks-dis-page">
		<img src="<?php echo CZLOGO; ?>">
		<h2>QuickBooks Disconnected</h2>
		<p>Your QuickBooks integration has been disconnected. You will no longer to be able to sync data to QuickBooks from Chargezoom.</p>
		<p>If you’d like to re-connect Chargezoom and your QuickBooks account, <a href="https://support.chargezoom.com/support/solutions/articles/44002028310-how-do-i-reconnect-quickbooks-online-" target="_blank">click here to view our help guide.</a></p>
	</div>
</body>
</html>