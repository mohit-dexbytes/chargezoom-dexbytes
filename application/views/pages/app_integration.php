   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>
	<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
	<div class="header-section">
		<h1>
		<i class="gi gi-settings"> </i> <?php echo "Apps and Integrations";?> 
		</h1>
	</div>
	</div>
	
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	            
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	
  <div class="col-md-12">	
     <div class="col-md-6">
        <div class="widget">
           <div class="widget-header">
				<img class="image" src="https://assets-prod-a.chargeover.com/admin/integrations/qbo_logo.png">
		   </div>
       <h3 class="text">QuickBooks Online</h3>
		 
		   <div class="widget-content">
 
           <?php 
				if(isset($get_app_type) && !empty($get_app_type))
				{
					if($get_app_type['appIntegration']=='1'){
					?>
							 <button class="btn btn-sm btn-success display">Activated</button>
							 <?php } else { ?>
						   <a href="#qb"  id="qb_online" data-backdrop="static" data-keyboard="false" data-toggle="modal" class="btn btn-sm btn-info display">Switch</a>
				<?php }} ?> 
				<br>
		 </div>
		 
	   </div>
     </div>
   
	
     <div class="col-md-6">
       <div class="widget">
         <div  class="widget-header">
				<img class="image" src="https://assets-prod-a.chargeover.com/admin/integrations/quickbooks_logo.png">
		</div>
                <h3 class="text">QuickBooks Desktop</h3>
         <div class="widget-content">
			      <?php 
					if(isset($get_app_type) && !empty($get_app_type))
					{
						if($get_app_type['appIntegration']=='2'){
						?>
								 <button class="btn btn-sm btn-success display">Activated</button>
								 <?php } else { ?>
							   <a href="#qb_desktop" data-toggle="modal" class="btn btn-sm btn-info display">Switch</a>
					<?php }} ?> 
				<br>
		 </div>
	   </div>
	 </div>
	 
	  <div class="col-md-6">
       <div class="widget">
         <div  class="widget-header">
				<img class="image" src="<?php echo base_url(IMAGES); ?>/FreshBooks1.png">
		</div>
                <h3 class="text">FreshBooks Integration</h3>
         <div class="widget-content">
   
		 <?php 
			if(isset($get_app_type) && !empty($get_app_type))
			{
				if($get_app_type['appIntegration']=='3'){
				?>
						 <button class="btn btn-sm btn-success display">Activated</button>
						 <?php } else { ?>
					  <a href="#freshbooks" data-toggle="modal" id="freshbook" class="btn btn-sm btn-info display">Switch</a>
			<?php }} ?> 
		
			    
				<br>
		 </div>
	   </div>
	 </div>	
	 
	 <div class="col-md-6">
       <div class="widget">
         <div  class="widget-header">
				<img class="image" src="<?php echo base_url(IMAGES); ?>/xero.png">
		</div>
                <h3 class="text">Xero Integration</h3>
         <div class="widget-content">
      <?php 
				if(isset($get_app_type) && !empty($get_app_type))
				{
				if($get_app_type['appIntegration']=='4'){
				?>
						 <button class="btn btn-sm btn-success display">Activated</button>
						 <?php } else { ?>
					    <a href="<?php echo base_url(); ?>Xero_controllers/Xero/xero_int" id="xero" class="btn btn-sm btn-info display">Switch</a>
			<?php }} ?> 
			   
				<br>
		 </div>
	   </div>
	 </div>	 
	 
  </div>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
   
</div>
<?php 
 
				if(isset($gt_result))
				{
					foreach($gt_result as $gt)
					{
						
			 $redirect_uri = $gt['oauth_redirect_uri']; 
						
					}
					
				}


?>

<script
      type="text/javascript"
      src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere-1.3.3.js">
 </script>

 <script type="text/javascript">
 
	var redirectUrl = "<?php echo $redirect_uri ?>";

     intuit.ipp.anywhere.setup({
             grantUrl:  redirectUrl,
             datasources: {
                  quickbooks : true,
                  payments : true
            },
             paymentOptions:{
                   intuitReferred : true
            }
     });

 </script>

<div id="qb" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Connect to QuickBooks</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             	
                <?php

  if(isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])){
    echo "<h3>Retrieve OAuth 2 Tokens from Sessions:</h3>";
    $tokens = array(
       'access_token' => $_SESSION['access_token'],
       'refresh_token' => $_SESSION['refresh_token']
    );
    var_dump($tokens);
    echo "<br /> <a href='" .$refreshTokenPage . "'>
          Refresh Token
    </a> <br />";
    echo "<br /> <a href='" .$refreshTokenPage . "?deleteSession=true'>
          Clean Session
    </a> <br />";
  }else{
    echo "<h3>Please Complete the \"Connect to QuickBooks\" Authorization Process.</h3>";
    echo '
    <div> Click on the button below to start "Connect to QuickBooks"</div>';
    echo "<br /> <ipp:connectToIntuit></ipp:connectToIntuit><br />";
  }

 ?>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
 
 

<style>
.widget{
	
	display: block;
	margin-left: auto; 
	margin-right: auto; 
	border:1px solid #d1d1e0;
}
.widget-header
{
	height: auto; 
	background-color:#d1d1e0;
}
.text
{
	text-align: center; 
	margin-top: 10px;
}
.display
{
	display: block; 
	margin-left: auto; 
	margin-right: auto;
	width:120px;
}
.image
{
	display: block; 
	margin-left: auto; 
	margin-right: auto; 
	height:110px;
}

</style>

<div id="freshbooks" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">FreshBooks</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="freshbooks" class="form form-horizontal" action="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_integration/user">
			
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Sub Domain</label>
							<div class="col-md-8">
								<input type="text" id="subdomain"  name="subdomain" class="form-control"  value="" placeholder="Enter Your Sub Domain.."></div>
						</div>
						
						
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Secret Key</label>
							<div class="col-md-8">
								<input type="text" id="secretKey"  name="secretKey" class="form-control"  value="" placeholder="Enter Your Secret Key.."></div>
						</div>
						
                       </div>      
		      
			  <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>
					
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					
                    </div>
                    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="qb_desktop" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" data-toggle="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Quickbooks Webconnector File</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			
             <?php if(isset($login_info['fileID']) && !empty($login_info['fileID']) ){  ?>
					
						
					
					 
                
						<h4><b> Click "Download Web Connector File" to begin automatically syncing QuickBooks data."</b></h4>
						<br>
				<a href="<?php echo base_url().'QuickBooks/your_function/my-quickbooks-wc-file'.$file_user.'.qwc'; ?>"    class="btn btn-sm btn-info donfile" id="dowlloadID1">Download Web Connector File</a>
						<br>
						<hr>
						<br>
						
						
						<h4><b> To import data from your QuickBooks (Clients, Products & Invoices), follow QuickBooks setup instructions given below: <b></h4>
						<br>
						  
					 <form id="" method="post"  class="form-horizontal" >
                     	<div class="form-group">
                            <label class="col-md-2 control-label">1</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Install QuickBooks Web Connector at the machine where your QuickBooks is installed.</p>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-2 control-label">2</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Configure your Web Connector using: Start->QuickBooks->QuickBooks Webconnector.</p>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-md-2 control-label">3</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Add the downloaded .qwc file using Add Application button at the QuickBooks Webconnector UI. </p>
                            </div>
                        </div>
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label">4</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Enter the password which you setup at step 4.</p>
                            </div>
                        </div>
						
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label">5</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Click on update button to import data from QuickBooks.</p>
                            </div>
                        </div>
					
						
					 
					
			   </form>	
							
					
						
					  </div>
					
				
					
					<?php }else{  ?>
					
					
					
					   <div class="block" id="qbwwcid" style="display:none;" >
                
						
						<h4><b>Click "Download Web Connector File" to begin automatically syncing QuickBooks data."</b></h4>
						<br>
						<a href="<?php echo base_url() ?>QuickBooks/your_function/my-quickbooks-wc-file<?php echo $this->session->userdata('logged_in')['merchID'].'.qwc'; ?>"    class="btn btn-sm btn-info donfile" id="dowlloadID2" > Download Web Connector File</a>
						<br>
						<hr>
						<br>
						
						
									<h4><b> To import data from your QuickBooks (Clients, Products & Invoices), follow QuickBooks setup instructions given below: <b></h4>
								     <br>
									 <form id=""   class="form-horizontal" >
									<div class="form-group">
										<label class="col-md-2">1</label>
										<div class="col-md-10">
											<p class="form-control-static para">Install QuickBooks Web Connector at the machine where your QuickBooks is installed.</p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">2</label>
										<div class="col-md-10">
											<p class="form-control-static">Configure your Web Connector using: Start->QuickBooks->QuickBooks Webconnector.</p>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">3</label>
										<div class="col-md-10">
											<p class="form-control-static">Add the downloaded .qwc file using Add Application button at the QuickBooks Webconnector UI. </p>
										</div>
									</div>
									
									
										<div class="form-group">
										<label class="col-md-2 control-label">4</label>
										<div class="col-md-10">
											<p class="form-control-static">Enter the password which you setup at step 4.</p>
										</div>
									</div>
									
									
									
										<div class="form-group">
										<label class="col-md-2 control-label">5</label>
										<div class="col-md-10">
											<p class="form-control-static">Click on update button to import data from QuickBooks.</p>
										</div>
									</div>
								
									
								 
								
								   </form>	
						
					  </div>
					
					 <div id="qbwwcid1"  >
						<h4>You can create a webconnector file to get QuickBooks data.</h4>
					
						  <a href="#qb-connecter-settings" class="btn btn-sm btn-info" id="dddddc"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Setup QuickBooks</a>
						<br>
						<br>
						<br>
					</div>
					
					
					<?php } ?>	
                <div class="form-group text-right">
				<a href="<?php echo base_url(); ?>home/index" class="btn btn-sm btn-success bttn">OK</a>
				<br>
                <br>
            </div>
            </div>
			
			</div>
            <!-- END Modal Body -->
        </div>

