
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" >

<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">

<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
		<div class="row invoice_quick">


			<div class="col-sm-6 col-lg-4">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-success rounded-circle mr-2">

                            <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold">
                                	<span data-plugin="counterup">
                                	<?php echo '$'.number_format($revenue, 2); ?>	
                                	</span></h4>
                                <p class="text-truncate">Revenue</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-info rounded-circle mr-2">

                            <i class="fas fa-file-invoice-dollar ion-logo-usd avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php echo $totalInvoice; ?></span></h4>
                                <p class="text-truncate">Invoices</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>	
            <div class="col-sm-6 col-lg-4">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-purple rounded-circle mr-2">
                            <i class="far fa-calendar-alt ion-logo-usd avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php echo date('M d, Y', strtotime($subs['firstDate'])); ?></span></h4>
                                <p class="text-truncate">Subscription Start Date</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>
			
	</div>
    <!-- All Orders Block -->
 	<legend class="leg"><a href="<?php echo base_url('SettingSubscription/subscriptions'); ?>" style="color: #167bc4;">Subscriptions</a><span> / <?php echo $planName; ?></span> <span class="pull-right"><?php echo $customerName; ?></span></legend>      
    <div class="block-main full"  style="position: relative;margin-top: -18px;">
    	
        <!-- All Orders Title -->
        <div class="block-options">
			<a class="btn btn-sm  btn-info" title="Edit Subscription" href="<?php echo base_url(); ?>SettingSubscription/create_subscription/<?php echo $subs['subscriptionID'];?>">Edit Subscription</a>
		</div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="sub_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
				
					<th class="text-left">Invoice</th>

					<th class="hidden-xs text-right">Due Date</th>

					<th class="text-right hidden-xs">Payment</th>
					<th class="text-right hidden-xs">Balance</th>
					<th class="text-right">Status</th>
                </tr>
            </thead>
            <tbody>
			
				<?php if (!empty($invoices)) {

					foreach ($invoices as $invoice) {
						$lable = '';

						if ($invoice['status'] == 'Upcoming') {
							$status   = $invoice['status'] = 'Open';
							$lable = "info";
						} else   if ($invoice['status'] == 'Past Due') {
							$status   = $invoice['status'] = 'Past Due';
							$lable = "info";
						} else  if ($invoice['status'] == 'Success') {
							$lable = "success";
							$status   =   $invoice['status'] = 'Paid';
						} else  if ($invoice['status'] == 'Failed') {
							$status   = $invoice['status'];
							$lable = "danger";
						} else  if ($invoice['status'] == 'Cancel') {
							$status   = 'Cancelled';
							$lable = "primary";
						}


						if ($invoice['TxnID'] == '') {
							$invoice['userStatus'] = 'Pending';
						}
					?>
				<tr>
							

							<?php if ($plantype) { ?>
								<td class="text-left cust_view"><?php echo $invoice['RefNumber']; ?></td>
							<?php } else { ?>
								<td class="text-left cust_view"><a href="<?php echo base_url(); ?>home/invoice_details/<?php echo $invoice['TxnID']; ?>"><strong><?php echo $invoice['RefNumber']; ?></strong></a></td>

							<?php } ?>
							<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>


							<?php 
							
							if($invoice['AppliedAmount'] < 0){
								$AppliedAmount = -$invoice['AppliedAmount'];
							}else{
								$AppliedAmount = $invoice['AppliedAmount'];
							}

								if ( true ){ 		
								?>
								<td class="hidden-xs text-right cust_view"><a href="#pay_data_process" onclick="set_payment_data('<?php echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$' . number_format(($AppliedAmount), 2); ?></a></td>
							<?php } else { ?>
								<td class="hidden-xs text-right cust_view"><a href="#"><?php echo  '$' . number_format(($invoice['AppliedAmount']), 2); ?></a></td>
							<?php } ?>
							<td class="text-right hidden-xs"><?php echo '$' . number_format($invoice['BalanceRemaining'], 2); ?></td>
							<td class="text-right">
              <?php  echo ucfirst($invoice['status']); ?></td>
						</tr>
				
				  <?php } }
							else { echo '<tr>
							<td colspan="5"> No Records Found </td>
							<td style="display: none;"> No Records Found </td>
							<td style="display: none;"> No Records Found </td>
							<td style="display: none;"> No Records Found </td>
							<td style="display: none;"> No Records Found </td>
							</tr>'; }  
                           ?>	
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    

</div>   <script src="<?php echo base_url(JS); ?>/pages/customer_details_qbd.js"></script>
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script src="<?php echo base_url(JS); ?>/pages/qbd_invoice_payment.js"></script>

<script>
	$(function() {
		Pagination_view.init();
	});
</script>
<script>
	var Pagination_view = function() {

		return {
			init: function() {
				/ Extend with date sort plugin /
				$.extend($.fn.dataTableExt.oSort, {

				});

				/ Initialize Bootstrap Datatables Integration /
				App.datatables();

				/ Initialize Datatables /
				$('#sub_page').dataTable({
					columnDefs: [{
							type: 'date',
							targets: [-1]
						},
						{
							orderable: false,
							targets: [4]
						}
					],
					order: [
						[1, "desc"]
					],
					pageLength: 10,
					searching: false,
					lengthMenu: [
						[10, 25, 50, 100, 500],
						[10, 25, 50, 100, 500]
					]
				});

				/ Add placeholder attribute to the search input /
				$('.dataTables_filter input').attr('placeholder', 'Search');
			}
		};
	}();
</script>

<style>
	.table.dataTable {
		width: 100% !important;
	}

	@media screen and (max-width:352px) {
		.table.dataTable {
			width: 100% !important;
		}

		table.table thead .sorting_desc {
			padding-right: 0px !important;
		}

		table.dataTable thead>tr>th {
			padding-left: 5px !important;
			padding-right: 5px !important;
		}
	}
	.block-options{
		float: right;
    	margin-top: 10px;
	}
</style>




<div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Refund Payment</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<form id="data_form" method="post" action='<?php echo base_url(); ?>PaypalPayment/create_customer_refund' class="form-horizontal">
					<p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p>
					<div class="form-group">
						<div class="col-md-8">
							<input type="hidden" id="txnstrID" name="txnstrID" class="form-control" value="" />
							<input type="hidden" id="txnpaypalID" name="txnpaypalID" class="form-control" value="" />
							<input type="hidden" id="paytxnID" name="paytxnID" class="form-control" value="" />
							<input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control" value="" />
							<input type="hidden" id="txnID" name="txnID" class="form-control" value="" />
						</div>
					</div>
					<div class="pull-right">
						<input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund" />
						<button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					</div>
					<br />
					<br />
				</form>
			</div>
			<!-- END Modal Body -->
		</div>
	</div>
</div>

<form id="snk_form" method="post" action="<?php echo base_url(); ?>SettingSubscription/sync_invoice">
	<input type="hidden" id="inv_id" name="inv_id" value="">
	<input type="hidden" id="inv_lsid" name="inv_lsid" value="">
	<input type="hidden" id="inv_edid" name="inv_edid" value="">
</form>
<script>
	function set_refund_pay(txnid) {

		if (txnid != "") {



			$.ajax({

				type: "POST",
				url: '<?php echo base_url() ?>Payments/get_invoice_pay_data',
				data: {
					txnid: txnid
				},
				success: function(data) {
					var data = $.parseJSON(data);
					if (!jQuery.isEmptyObject(data)) {

						if (data['transactionGateway'] == '1') {
							$('#txnID').val(txnid);
							var url = "<?php echo base_url() ?>Payments/create_customer_refund";
						} else if (data['transactionGateway'] == '2') {
							$('#txnIDrefund').val(txnid);
							var url = "<?php echo base_url() ?>AuthPayment/create_customer_refund";
						} else if (data['transactionGateway'] == '3') {
							$('#paytxnID').val(txnid);
							var url = "<?php echo base_url() ?>PaytracePayment/create_customer_refund";
						} else if (data['transactionGateway'] == '4') {
							$('#txnpaypalID').val(txnid);
							var url = "<?php echo base_url() ?>PaypalPayment/create_customer_refund";
						} else if (data['transactionGateway'] == '5') {
							$('#txnstrID').val(txnid);
							var url = "<?php echo base_url() ?>StripePayment/create_customer_refund";
						}

						$("#data_form").attr("action", url);
					}
				}
			});
		}
	}



	window.setTimeout("fadeMyDiv();", 2000);

	function fadeMyDiv() {
		$(".msg_data").fadeOut('slow');
	}

	$(function() {
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() + 1, 0, 0, 0, 0);
		$('#payment_date').datepicker({
			format: 'mm-dd-yyyy',
			startDate: today,
			autoclose: true
		});


		$('.pay_chk').click(function() {
			if ($(this).is(':checked') && $(this).val() == '1') {
				customer = $('#c_user').val();

				$.ajax({

					type: "POST",
					url: '<?php echo base_url() ?>Payments/get_credit_data',
					data: {
						customerID: customer
					},
					success: function(data) {
						$('#cr_div').html(data);
					}
				});
				$('#credit_div').show();
				$('#chk_div').hide();
			} else {

				$('#chk_div').show();
				$('#cr_div').html('');
				$('#credit_div').hide();
			}

		});

	});
</script>