
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <style type="text/css" media="print">
            @page 
            {
                size:  auto;   /* auto is the initial value */
                margin-bottom: 0;  /* this affects the margin in the printer settings */
            }
        </style>
        <meta charset="UTF-8">
        <meta name="google" content="notranslate">
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title> PayPortal - Cloud Accounting Automation Software</title>

        <meta name="description" content=" Payment Portal automates your payment processing and accounting reconciliation so you can focus on your business">
        <meta name="author" content="Chargezoom">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?= base_url() ?>resources/img/merch_icon.png">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="<?= base_url() ?>resources/img/icon180.png" sizes="180x180">
        <!-- END Icons -->
		
		<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?= base_url() ?>resources/css/bootstrap.min.css">
		<!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<?= base_url() ?>resources/css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
       
        <link rel="stylesheet" href="<?= base_url() ?>resources/css/main.css">
        <!-- Include a specific file here from /resources/css/themes/ folder to alter the default theme of the template -->
        <link id="theme-link" rel="stylesheet" href="<?= base_url() ?>resources/css/themes/night.css">
        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?= base_url() ?>resources/css/themes.css">
        
         <link rel="stylesheet" href="<?= base_url() ?>resources/css/header_style_sheet.css">
        <link rel="stylesheet" href="<?= base_url() ?>resources/css/footer_style.css">
        <link rel="stylesheet" href="<?= base_url() ?>resources/css/custom.css">

        <!-- END Stylesheets -->
	
        
        <!-- Modernizr (browser feature detection library) -->
        <!-- <script src="js/vendor/modernizr.min.js"></script> -->
		<script src="<?= base_url() ?>resources/js/vendor/jquery-1.11.3.min.js"></script>
        <script src="<?= base_url() ?>resources/js/vendor/bootstrap.min.js"></script>
       
        
    </head>

    <body><!-- Error Container -->
<div id="error-container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <h1 class="animation-pulse"><i class="fa fa-exclamation-circle text-warning"></i> 404</h1>
            <h2 class="h3">Oops, we are sorry but the page you are looking for was not found..<br>But do not worry, we will have a look into it..</h2>
        </div>
       </div>
</div>
    </body>
</html>