
<!-- Page content -->
<div id="page-content">
    
    

    
    <div class="row">
        <div class="col-lg-12">
           
            <div class="block">
                
                <div class="block-title">
                    <h2><strong>Quickbooks Webconnector File</strong></h2>
                </div>
                
				<h4>Click "Download Web Connector File" to begin automatically syncing QuickBooks data."</h4>
				<br>
		     	<a href="<?php echo base_url(); ?>QuickBooks/your_function/my-quickbooks-wc-file<?php echo $this->session->userdata('logged_in')['id'].'.qwc'; ?>"    class="btn btn-sm btn-primary donfile"><i class="fa fa-download"></i> Download Web Connector File</a>
				<br>
				<br>
                
            </div>
           
        </div>
        
    </div>
    




<script src="<?php echo base_url(JS); ?>/pages/ecomDashboard.js"></script>
<script>$(function(){ EcomDashboard.init(); });</script>
</div>