<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">

<style>
.status_filter{
  width: 150px;
    position: absolute;
    top: 9px;
    left: 275px;
    z-index: 1;
}
.action_option{
    width: 150px;
    position: absolute;
    top: 9px;
    left: 430px;
    z-index: 1;
}

@media (max-width: 767px){
	div#invoice_page_wrapper {
		clear: both;
	}

	.list-add-btn {
		position: initial !important;
		float: right;
	}
	.dataTables_wrapper{
		background-color: transparent !important;
	}
	.status_filter {
    	position: absolute;
    	top: 50px;
      left: 300px !important;
	}
	#invoice_page{
	    margin-top: 70px !important;
	}
	.dropdown-menu {
    position: unset !important;
  }
  .action_option{
    top: 50px !important;
    left: 400px !important;
  }
}
.chk_pay_invoice_refund {
    display: none;
}
</style>
<div id="page-content">
	<!-- eCommerce Orders Header -->
	<?php echo $this->session->flashdata('message');   ?>
	<!-- END eCommerce Orders Header -->
	<div class="row invoice_quick">

		<div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-success rounded-circle mr-2">
                        <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup">
                                <?php echo ($success_invoice) ? ($success_invoice) : '0'; ?></span></h4>
                            <p class="text-truncate">Paid Invoices</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>
		
		<div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-info rounded-circle mr-2">
                        <i class="fa fa-credit-card ion-logo-usd avatar-title font-26 text-white"></i>
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold">
                              <span data-plugin="counterup">
                                <?php echo ($upcomming_inv) ? ($upcomming_inv) : '0';  ?>
                              </span>
                            </h4>
                            <p class="text-truncate">Scheduled Payments</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-purple rounded-circle mr-2">
                        <i class="fa fa-calendar ion-logo-usd avatar-title font-26 text-white"></i>
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold">
                              <span data-plugin="counterup">
                                <?php echo ($cancel_inv) ? ($cancel_inv) : '0';  ?>
                                  
                              </span>
                            </h4>
                            <p class="text-truncate">Overdue Invoices</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-primary rounded-circle mr-2">
                        <i class="fa fa-thumbs-o-down ion-logo-usd avatar-title font-26 text-white"></i>
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold">
                              <span data-plugin="counterup">
                                <?php echo ($failed_inv) ? ($failed_inv) : '0'; ?>
                              </span>
                            </h4>
                            <p class="text-truncate">Failed Invoices</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>
	</div>
	<!-- END Quick Stats -->

	<!-- All Orders Block -->
	<legend class="leg"> Invoices </legend>
  <div class="full">

    <!-- All Orders Title -->
    <div style="position: relative">
		

					<?php if (!$plantype) { ?>
						<a class="btn pull-right  btn-sm btn-success subs-btn list-add-btn addNewFixRight" title="Create New" href="<?php echo base_url(); ?>SettingSubscription/create_invoice">Add New</a>
					<?php } ?>

				
			
		
		<!-- END All Orders Title -->
		<form class="filter_form" action="<?php echo base_url();?>home/invoices" method="post">
            <select class="form-control status_filter" name="status_filter">
              <option value="Current" <?php if($filter == 'Current'){ echo 'Selected';}?>>Current</option>
              <option value="Past Due" <?php if($filter == 'Past Due'){ echo 'Selected';}?>>Overdue</option>
              <option value="Partial" <?php if($filter == 'Partial'){ echo 'Selected';}?>>Partial</option>
              <option value="Open" <?php if($filter == 'Open'){ echo 'Selected';}?>>Open</option>
              <option value="Failed" <?php if($filter == 'Failed'){ echo 'Selected';}?>>Failed</option>
              <option value="Paid" <?php if($filter == 'Paid'){ echo 'Selected';}?>>Paid</option>
			  <option value="Cancelled" <?php if($filter == 'Cancelled'){ echo 'Selected';}?>>Voided</option>
			  <option value="All" <?php if($filter == 'All'){ echo 'Selected';}?>>All</option>
			</select>
		</form>
		<?php  
    if ($plantype == '' || $plantype == null) { 
      $filter = ucfirst($filter);
      if($filter == 'Current' || $filter == 'Past Due' || $filter == 'Open' || $filter == 'Failed' || $filter == null){ ?>
	    <div class="action_option" >
	      <select class="form-control action_filter" id="action_filter" name="action_filter">
	        <option style="display: none;" value="" >Batch Actions</option>
	        <option value="1" >Process Selected</option>
	        <option value="2" >Email Selected</option>
	      </select>
	    </div>
		<?php }
    } ?>	
		<!-- All Orders Content -->
		<table id="invoice_page" class="table table-bordered table-striped table-vcenter">
			<thead>
				<tr>
					<th class="text-center visible-lg" style="padding-left: 26px !important;"><input type="checkbox" name="bulkCheckAll" id="bulkCheckAll" ></th>
					<th class="text-left visible-lg">Customer Name</th>
					<th class="text-left">Invoice</th>

					<th class="hidden-xs text-right">Due Date</th>

					<th class="text-right hidden-xs">Balance</th>
					<th class="text-right">Status</th>
				
					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody>


			</tbody>
		</table>
		<!-- END All Orders Content -->
	</div>
  </div>
	<!-- END All Orders Block -->

	<!-- END Page Content -->


</div>
<div id="bulkProcessInvoice" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header text-center">
        <h2 class="modal-title">Process Invoices</h2>
      </div>
      <!-- END Modal Header -->

      <!-- Modal Body -->
      <div class="modal-body">

        

        <form id="process_invoice_form" method="post" action='<?php echo base_url(); ?>CommanGateway/processMultipleInvoices' class="form-horizontal">
          <p id="message_data">Are you sure you want to Process the selected Invoices?</p>
          <input type="hidden" name="processInvoiceIDS" id="processInvoiceIDS" value="">
          <input type="hidden" name="integrationMerchantType" id="integrationMerchantType" value="2">

          <input type="hidden" name="electronic_check_gateway" id="echeckMerchantGatewayID" value="0">
          <input type="hidden" name="credit_card_gateway" id="creditCardMerchantGatewayID" value="0">

          <div class="pull-right">
            <input type="submit" id="process_btn" name="btn_process" class="btn btn-sm btn-success" value="Process" />
            <button type="button" id="process_btn_cancel" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
          </div>
          <br />
          <br />
        </form>

      </div>
      <!-- END Modal Body -->
    </div>
  </div>
</div>
<div id="bulkProcessGatewayInvoice" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header text-center">
        <h2 class="modal-title">Process Invoices</h2>
      </div>
      <!-- END Modal Header -->

      <!-- Modal Body -->
      <div class="modal-body">

        

        <form id="process_invoice_gateway_form" method="post" action='<?php echo base_url(); ?>CommanGateway/processMultipleInvoices' class="form-horizontal">
          <!--  -->
          <input type="hidden" name="processInvoiceIDS" id="processGatewayInvoiceIDS" value="">
          <input type="hidden" name="integrationMerchantType" id="integrationMerchantType" value="2">

          

          <div id="gateway_data">
            <div class="form-group">
              <label class="col-md-4 control-label" for="credit_card_gateway">Credit Card</label>
              <div class="col-md-6">
                <select id="credit_card_gateway" name="credit_card_gateway" class="form-control">
                    <option value="">Select Gateway</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="electronic_check_gateway">Electronic Check</label>
              <div class="col-md-6">
                <select id="electronic_check_gateway" name="electronic_check_gateway" class="form-control">
                    <option value="">Select Gateway</option>
                </select>
              </div>
            </div>
          </div>

          <div class="pull-right">
            <input type="submit" id="process_btn" name="btn_process" class="btn btn-sm btn-success" value="Process" />
            <button type="button" id="process_btn_cancel" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
          </div>
          <br />
          <br />
        </form>

      </div>
      <!-- END Modal Body -->
    </div>
  </div>
</div>
<div id="set_tempemail_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->

            <div class="modal-header ">
                <h2 class="modal-title text-center">Email Invoices</h2>
            </div>

            <div class="modal-body">
                <div id="data_form_template">
                    <label class="label-control" id="template_name"> </label>

                    <form id="email-validation" action="<?php echo base_url(); ?>CommanGateway/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="emailProcessInvoiceIDS" id="emailProcessInvoiceIDS" value="">
                        <input type="hidden" name="integrationType" id="integrationType" value="2">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="type">Template</label>
                            <div class="col-md-7">
                                
                                <select class="template_select form-control" name="templateID" id="templateID" >
                                  <?php 
                                  $templatedata = [];
                                  if(isset($templateList)){ 
                                    $inc = 0;
                                    foreach ($templateList as $value) {
                                      if($inc == 0){
                                         $templatedata = $value;
                                      }
                                      echo '<option value="'.$value['templateID'].'">'.$value['templateName'].'</option>';
                                      $inc++;
                                    }
                                  } ?>
                                  
                                </select>

                            </div>
                        </div>

                        <div class="form-group"  style="display: none;" id="reply_div">
                            <label class="col-md-3 control-label" for="replyEmail">Reply-To Email</label>
                            <div class="col-md-7">
                                <input type="text" id="replyEmail" name="replyEmail" class="form-control" value="<?php if (isset($templatedata) && $templatedata['replyTo'] != ''){
                                            echo $templatedata['replyTo'];
                                        }else{  echo $merchantEmail;} ?>" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group" style="display:none" id="from_email_div">
                            <label class="col-md-3 control-label" for="templteName">From Email</label>
                            <div class="col-md-7">
                                <input type="text" id="fromEmail" name="fromEmail"  value="<?php  if(isset($from_mail) && $from_mail != '') echo $from_mail ? $from_mail : 'donotreply@payportal.com'; ?>" class="form-control" placeholder="From Email">
                            </div>
                        </div>
                        <div class="form-group" id='display_name_div' style='display:none'>
                            <label class="col-md-3 control-label" for="templteName">Display Name</label>
                            <div class="col-md-7">
                                <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="<?php echo $mailDisplayName; ?>" placeholder="Display Name">
                            </div>
                        </div>

                        <div class="form-group"  style="font-size: 12px;">
                            <label class="col-md-3 control-label" for="templteName"></label>
                            <div class="col-md-7">
                                <a href="javascript:void(0);" id="open_reply">Add Reply-To<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);" id="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                               
                             </div>
                         </div>
 
                        <div class="form-group" id="cc_div" style="display:none">
                            <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                            <div class="col-md-7">
                                <input type="text" id="ccEmail" name="ccEmail" value="<?php if (isset($templatedata)) echo ($templatedata['addCC']) ? $templatedata['addCC'] : ''; ?>" class="form-control" placeholder="CC Email">
                            </div>
                        </div>
                        <div class="form-group" id="bcc_div" style="display:none">
                            <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                            <div class="col-md-7">
                                <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if (isset($templatedata)) echo ($templatedata['addBCC']) ? $templatedata['addBCC'] : ''; ?>" placeholder="BCC Email">
                            </div>
                        </div>
                        

                        <div class="form-group">

                            <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                            <div class="col-md-7">

                                <input type="text" id="emailSubject" name="emailSubject" value="<?php if (isset($templatedata)) echo ($templatedata['emailSubject']) ? $templatedata['emailSubject'] : ''; ?>" class="form-control" placeholder="Email Subject">
                            </div>

                        </div>
                        <div class="form-group ">
                            <label class="col-md-3 control-label" >Attach Invoice PDF</label>
                            <div class="col-md-7">
                                <label class="switch switch-info"><input type="checkbox" name="add_attachment"<?php  echo "checked";  ?> id="add_attachment"><span></span></label>
                            </div>
                        </div>

                        <div class="form-group">


                            <label class="col-md-3 control-label">Email Body</label>
                            <div class="col-md-7">
                                <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if (isset($templatedata)) echo ($templatedata['message']) ? $templatedata['message'] : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>


                            </div>
                        </div>
                    </form>


                </div>


            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<script src="<?php echo base_url(JS); ?>/pages/customer_details_qbd.js"></script>
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script src="<?php echo base_url(JS); ?>/pages/qbd_invoice_payment.js"></script>

<script>

  	$('.status_filter').change(function(){
  		
  	});
  	$('#action_filter').change(function(){
      var value = $(this).val();

      if(value == 1){
          if($('#processInvoiceIDS').val() == ''){
            alert('Please select at least one invoice.');
            $('#action_filter').val('');
          }else{

            $.ajax({

                type: "POST",
                url: '<?php echo base_url() ?>ajaxRequest/getMerchantDefaultGatewaySupport',
                data: {
                },
                success: function(data) {
                  var data = $.parseJSON(data);
                  if (!jQuery.isEmptyObject(data)) {
                    if(data.status == 'success'){
                      if(data.data.creditCard == 1 || data.data.echeckStatus == 1){
                        $('#echeckMerchantGatewayID').val(data.data.gatewayID);
                        $('#creditCardMerchantGatewayID').val(data.data.gatewayID);
                        $('#bulkProcessInvoice').modal('show');
                      }else{
                        var creditCardHtml = '<option value="">Select Gateway</option>';
                        var eCheckHtml = '<option value="">Select Gateway</option>';

                        $.each( data.creditCardData, function( key, value ) {
                          if(value.gatewayID == data.data.gatewayID){
                             creditCardHtml += '<option selected value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                          }else{
                               creditCardHtml += '<option value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                          }
                         
                        });

                        $.each( data.eCheckData, function( key, value ) {
                          if(value.gatewayID == data.data.gatewayID){
                             eCheckHtml += '<option selected value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                          }else{
                               eCheckHtml += '<option value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                          }
                         
                        });

                        $('#credit_card_gateway').html(creditCardHtml);
                        $('#electronic_check_gateway').html(eCheckHtml);

                        $('#bulkProcessGatewayInvoice').modal('show');
                      }
                    }
                  }
                }
              });


            
          }
          
      }else if(value == 2){
          
          if($('#processInvoiceIDS').val() == ''){
            alert('Please select at least one invoice.');
            $('#action_filter').val('');
          }else{
            $('#set_tempemail_data').modal('show');
          }
      }else{
            
      }

      
    });
	  $('#templateID').change(function(){
	      getEmailTemplate($(this).val());
	  });
	  
	  $('.close1').click(function(){
	    $('#action_filter').val('');
	  });
	  
	  $("#bulkCheckAll").click(function(){
    var checkVal = '';
    if($(this).prop('checked') == true){
      $(".singleCheck").prop('checked', $(this).prop('checked'));
    }else{
      $(".singleCheck").prop('checked', false);
    }
    $('input[name="selectRow[]"]:checked').each(function() {
      if(checkVal == ''){
        checkVal = this.value;
      }else{
        checkVal = checkVal+','+this.value;
      }
    });
    $('#processInvoiceIDS').val(checkVal);
    $('#processGatewayInvoiceIDS').val(checkVal);
    $('#emailProcessInvoiceIDS').val(checkVal);
  });
  function resetCheckBulk(){
    $("#bulkCheckAll").prop('checked', false);
    $(".singleCheck").prop('checked', false);
    var  checkVal = '';
    $('#processInvoiceIDS').val(checkVal);
    $('#processGatewayInvoiceIDS').val(checkVal);
    
    $('#emailProcessInvoiceIDS').val(checkVal);
  }
    $(document).on("click", ".singleCheck",function(){
    var checkVal = '';
    
    var totalInvoice = $(".singleCheck").length;
    var countCheck = 0;
    $('input[name="selectRow[]"]:checked').each(function() {
      if(checkVal == ''){
        checkVal = this.value;
      }else{
        checkVal = checkVal+','+this.value;
      }
      countCheck = countCheck + 1;
    });

    if(countCheck == totalInvoice){
      $("#bulkCheckAll").prop('checked', true);
    }else{
      $("#bulkCheckAll").prop('checked', false);
    }
    
    $('#processInvoiceIDS').val(checkVal);
    $('#processGatewayInvoiceIDS').val(checkVal);
    $('#emailProcessInvoiceIDS').val(checkVal);
  });

	$(function() {
	});
	
</script>
<script>
var table;

$(document).ready(function() {
  	var serachString = '<?php echo isset($serachString) ? $serachString : '';?>';
	var status_filter = $('.status_filter').val();
  	table = $('#invoice_page').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "language": {
          "processing": "<div class='tableLoader'><i class='fa fa-refresh fa-spin'></i>" 
      },
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
          "pageLength": 10,
        "ajax": {
                "url": "<?php echo site_url('/home/invoice_ajax')?>",
                "type": "POST" ,
                
                "data":function(data) {
                	data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                	data.status_filter = status_filter;
                  resetCheckBulk();
            	},
                    
           },

          "columnDefs": [
            { className: "hidden-xs text-left cust_view", "targets": [ 0 ] },
            { className: "text-left cust_view", "targets": [ 1 ] },
            { className: "hidden-xs text-right", "targets": [ 2 ] },
            { className: "hidden-xs text-right cust_view", "targets": [ 3 ] },
            { className: "hidden-xs text-right", "targets": [ 4 ] },
            { className: "text-right", "targets": [ 5 ] },
            { orderable: false, targets: [0] },  

        ],
        "order": [[3, 'desc']],        
        "sPaginationType": "bootstrap",
        "stateSave": false 

    });
	$('.status_filter').change(function(){
      	var status_filter = $(this).val();
		if(status_filter == 'Current'){
			$('.action_option').show();
		}else if(status_filter == 'Past Due'){
			$('.action_option').show();
		}else if(status_filter == 'Partial'){
			$('.action_option').show();
		}else if(status_filter == 'Open'){
			$('.action_option').show();
		}else if(status_filter == 'Failed'){
			$('.action_option').show();
		}else if(status_filter == ''){
			$('.action_option').show();
		}else{
			$('.action_option').hide();
		}
    	table.destroy();
    	table = $('#invoice_page').DataTable({ 
          "processing": true, //Feature control the processing indicator.
          "language": {
              "processing": "<div class='tableLoader'><i class='fa fa-refresh fa-spin'></i>" 
          },
          "serverSide": true, //Feature control DataTables' server-side processing mode.
           "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
            "pageLength": 10,
          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('/home/invoice_ajax')?>",
              "type": "POST" ,
              
              "data":function(data) {
            data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            data.status_filter = status_filter;
            resetCheckBulk();
          },
               
          },

         
          "columnDefs": [
            { className: "hidden-xs text-left cust_view", "targets": [ 0 ] },
            { className: "text-left cust_view", "targets": [ 1 ] },
            { className: "hidden-xs text-right", "targets": [ 2 ] },
            { className: "hidden-xs text-right cust_view", "targets": [ 3 ] },
            { className: "hidden-xs text-right", "targets": [ 4 ] },
            { className: "text-right", "targets": [ 5 ] },
            { orderable: false, targets: [0] },  
          ],
         "order": [[2, 'desc']],
          
              "sPaginationType": "bootstrap",
      "stateSave": false 

        });
    });
    $('#invoice_page_wrapper .dataTables_filter input').addClass("form-control ").attr("placeholder", "Search");
   	// modify table search input
   	$('#invoice_page_wrapper .dataTables_length select').addClass("m-wrap form-control ");
   	// modify table per page dropdown
   	$('#invoice_page_wrapper .dataTables_length dataTables_length select').select2();
   	// initialzie select2 dropdown

   	$("#search").on('keyup', function (){
      table.search( this.value ).draw();
  	});

  	table.search( serachString ).draw();
});
</script>

<style>
	.table.dataTable {
		width: 100% !important;
	}

	@media screen and (max-width:352px) {
		.table.dataTable {
			width: 100% !important;
		}

		table.table thead .sorting_desc {
			padding-right: 0px !important;
		}

		table.dataTable thead>tr>th {
			padding-left: 5px !important;
			padding-right: 5px !important;
		}
	}
	
</style>

<script>

</script>


<div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Refund Payment</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<form id="data_form" method="post" action='<?php echo base_url(); ?>PaypalPayment/create_customer_refund' class="form-horizontal">
					<p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p>
					<div class="form-group">
						<div class="col-md-8">
							<input type="hidden" id="txnstrID" name="txnstrID" class="form-control" value="" />
							<input type="hidden" id="txnpaypalID" name="txnpaypalID" class="form-control" value="" />
							<input type="hidden" id="paytxnID" name="paytxnID" class="form-control" value="" />
							<input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control" value="" />
							<input type="hidden" id="txnID" name="txnID" class="form-control" value="" />
						</div>
					</div>
					<div class="pull-right">
						<input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund" />
						<button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					</div>
					<br />
					<br />
				</form>
			</div>
			<!-- END Modal Body -->
		</div>
	</div>
</div>

<form id="snk_form" method="post" action="<?php echo base_url(); ?>SettingSubscription/sync_invoice">
	<input type="hidden" id="inv_id" name="inv_id" value="">
	<input type="hidden" id="inv_lsid" name="inv_lsid" value="">
	<input type="hidden" id="inv_edid" name="inv_edid" value="">
</form>
<script>
	function set_refund_pay(txnid) {

		if (txnid != "") {



			$.ajax({

				type: "POST",
				url: '<?php echo base_url() ?>Payments/get_invoice_pay_data',
				data: {
					txnid: txnid
				},
				success: function(data) {
					var data = $.parseJSON(data);
					if (!jQuery.isEmptyObject(data)) {

						if (data['transactionGateway'] == '1') {
							$('#txnID').val(txnid);
							var url = "<?php echo base_url() ?>Payments/create_customer_refund";
						} else if (data['transactionGateway'] == '2') {
							$('#txnIDrefund').val(txnid);
							var url = "<?php echo base_url() ?>AuthPayment/create_customer_refund";
						} else if (data['transactionGateway'] == '3') {
							$('#paytxnID').val(txnid);
							var url = "<?php echo base_url() ?>PaytracePayment/create_customer_refund";
						} else if (data['transactionGateway'] == '4') {
							$('#txnpaypalID').val(txnid);
							var url = "<?php echo base_url() ?>PaypalPayment/create_customer_refund";
						} else if (data['transactionGateway'] == '5') {
							$('#txnstrID').val(txnid);
							var url = "<?php echo base_url() ?>StripePayment/create_customer_refund";
						}

						$("#data_form").attr("action", url);
					}
				}
			});
		}
	}
	$('#email-validation').validate({

	    rules: {
	      'replyEmail': {
	        required: true,
	      },

	    },
	    submitHandler: function(form) {
	      
	      $('.preloader.themed-background-pre').show();
	      // do other things for a valid form
	      form.submit();
	    }
	  });

	$('#process_invoice_form').validate({

	    rules: {
	      'processInvoiceIDS': {
	        required: true,
	      },

	    },
	    submitHandler: function(form) {
	      
	      $('.preloader.themed-background-pre').show();
	      // do other things for a valid form
	      form.submit();
	    }
	 });
	window.setTimeout("fadeMyDiv();", 2000);

	function fadeMyDiv() {
		$(".msg_data").fadeOut('slow');
	}

	$(function() {
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() + 1, 0, 0, 0, 0);
		$('#payment_date').datepicker({
			format: 'mm-dd-yyyy',
			startDate: today,
			autoclose: true
		});


		$('.pay_chk').click(function() {
			if ($(this).is(':checked') && $(this).val() == '1') {
				customer = $('#c_user').val();

				$.ajax({

					type: "POST",
					url: '<?php echo base_url() ?>Payments/get_credit_data',
					data: {
						customerID: customer
					},
					success: function(data) {
						$('#cr_div').html(data);
					}
				});
				$('#credit_div').show();
				$('#chk_div').hide();
			} else {

				$('#chk_div').show();
				$('#cr_div').html('');
				$('#credit_div').hide();
			}

		});

	});
	function getEmailTemplate(templateID) {

    if (templateID != "") {



      	$.ajax({

	        type: "POST",
	        url: '<?php echo base_url() ?>CommanGateway/getEmailTemplate',
	        data: {
	          templateID: templateID
	        },
	        success: function(data) {
	          var data = $.parseJSON(data);
	         
	          if (!jQuery.isEmptyObject(data.data)) {
	            var obj = data.data;
	            if(obj.addBCC != null){
	              $('#bccEmail').val(obj.addBCC);
	            }
	            if(obj.addCC != null){
	              $('#ccEmail').val(obj.addCC);
	            }
	            if(obj.fromEmail != null){
	              $('#fromEmail').val(obj.fromEmail);
	            }
	            if(obj.mailDisplayName != ''){
	              $('#mailDisplayName').val(obj.mailDisplayName);
	            }
	            if(obj.emailSubject != ''){
	              $('#emailSubject').val(obj.emailSubject);
	            }
	            
	            if(obj.replyTo != null){
	              $('#replyEmail').val(obj.replyTo);
	            }
	            CKEDITOR.instances['textarea-ckeditor'].setData(obj.message);
	          }
	        }
      });
    }
  }
  $('#process_invoice_gateway_form').validate({

    rules: {
      'credit_card_gateway': {
        required: true,
      },
      'electronic_check_gateway': {
        required: false,
      },

    },
    submitHandler: function(form) {
      $("#submit_btn").attr("disabled", true);
      $('.preloader.themed-background-pre').show();
      // do other things for a valid form
      form.submit();
    }
  });
</script>