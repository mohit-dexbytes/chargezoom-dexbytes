   <!-- Page content -->
<?php
	$this->load->view('alert');
?>
	<div id="page-content">
	 
	  
    <!-- END Wizard Header -->  <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;
					?>

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	             
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form  id="customer_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>MerchantUser/edit_customer">
			
			 <input type="hidden"  id="customerID" name="customerID" value="<?php if(isset($customer)){echo $customer['customerID']; } ?>" /> 
			    <div class="form-group">
							<label class="col-md-3 control-label" for="example-username">Company Name</label>
							<div class="col-md-7">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($customer)){echo $customer['companyName']; } ?>" placeholder="Company Name"><?php echo form_error('companyName'); ?>
							</div>
						</div>
						
                            <div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Customer Name </label>
					<div class="col-md-7">
					<input type="text" id="fullName" name="fullName" class="form-control"  value="<?php if(isset($customer)){echo $customer['fullName']; } ?>" placeholder="Customer Name"><?php echo form_error('fullName'); ?> </div>
					</div>
                            
				
			   
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">First Name </label>
					<div class="col-md-7">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($customer)){echo $customer['firstName']; } ?>" placeholder="First Name"><?php echo form_error('firstName'); ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Last Name </label>
					<div class="col-md-7">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($customer)){echo $customer['lastName']; } ?>" placeholder="Last Name"><?php echo form_error('lastName'); ?> </div>
					</div>					
					
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Email Address</label>
					<div class="col-md-7">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($customer)){echo $customer['userEmail']; } ?>" placeholder="Email Address"><?php echo form_error('fullName'); ?> </div>
					</div>
					<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">Phone Number</label>
							<div class="col-md-7">
								<input type="number" id="phone" name="phone" class="form-control" value="<?php if(isset($customer)){echo $customer['phoneNumber']; } ?>" placeholder="Phone Number" >
							</div>
						</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 1</label>
						<div class="col-md-7">
							<input type="text" id="address1" name="address1"   value="<?php if(isset($customer)){echo $customer['address1']; } ?>" class="form-control" placeholder="Address Line 1"><?php echo form_error('companyAddress1'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 2</label>
						<div class="col-md-7">
							<input type="text" id="address2" name="address2"  class="form-control"  value="<?php if(isset($customer)){echo $customer['address2']; } ?>"  placeholder="Address Line 2">
						</div>
					</div>
					
					
					
                        
                        <div class ="form-group">
						   <label class="col-md-3 control-label" name="city">City</label>
						   <div class="col-md-7">
						       <input type="text" id="city" class="form-control " name="companyCity" placeholder="City"  value="<?php if(isset($customer)){ echo $customer['city']; } ?>">
								
							</div>
                        </div>
                        <div class ="form-group">
						   <label class="col-md-3 control-label"  name="state" for="example-typeahead"> State</label>
						   
						 	  <div class="col-md-7">
						 	      <input type="text" id="state" class="form-control " name="companyState" placeholder="State" value="<?php if(isset($customer)){ echo $customer['state']; } ?>" >
								
							</div>
                        </div>		
			
                        

						<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">ZIP Code</label>
							<div class="col-md-7">
								<input type="text" id="zipCode" name="zipCode" class="form-control" value="<?php if(isset($customer)){echo $customer['zipcode']; } ?>" placeholder="ZIP Code"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
					
						<div class ="form-group">
						   <label class="col-md-3 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-7">
								<select id="country" class="form-control " name="companyCountry" >
								<?php foreach($country_datas as $country){  ?>
								   <option value="<?php echo $country['country_id']; ?>" <?php if(isset($customer)&& $country['country_id']==$customer['country'] ){echo 'selected'; } ?>   > <?php echo $country['country_name']; ?> 	</option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
                  
	                <div class="form-group">
					<div class="col-md-4 col-md-offset-9">
					  
					
					 <button type="submit" class="submit btn btn-sm btn-success">Save</button>
                      <a href="<?php echo base_url(); ?>home/customer" class=" btn btn-sm btn-primary1">Cancel</a>		
					</div>
				    </div>					
					
		
		
  	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>

$(document).ready(function(){
$('#customer_menu').addClass('active');
    $.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
      


 
    $('#customer_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'firstName': {
                        required: true,
                        minlength: 2
                    },
					 
					 'lastName': {
                        required: true,
                       minlength: 2
                    },
					
					'userEmail':{
					      required:true,
						  email:true,
						  
					},
					'phone':{
					      maxlength:10,
                          required: true,
						  
					},
					  'companyCountry': {
                        required: true,
                        
                    },
                    'companyState': {
                        required: true,
                        
                    },
					  'companyCity': {
                        required: true,
                         
                    },
					 'companyAddress1':{
						  required: true,
					 },
                    'zipCode': {
                       	minlength:3,
						maxlength:10,
						ProtalURL:true,
						validate_addre:true
                    },
                    
               		
					'portal_url':{
                        minlength:2,
                        maxlength: 20,
                        ProtalURL:true,
                        "remote" :function(){return Chk_url('portal_url');} 
					},
					
			},
    });
    
    
    $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_states'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){

			var s = $('#state');
			 $('#state').append('<option value="Select State">Select State</option>');
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			$('#city').append('<option value="Select State">Select city</option>');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	
	
    
});	

</script>

</div>


