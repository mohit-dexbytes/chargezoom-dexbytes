<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">

    <!-- Progress Bar Wizard Block -->
    <div class="block">
       <div class="block-title">
            <h2><strong><?php if(isset($company)){ echo "Edit App";}else{ echo "Create New App"; }?></strong></h2>
            
        </div>
        
        <!-- Progress Bar Wizard Content -->
        <div class="row">
			<?php if(!isset($company)) { ?>
		 	<form method="POST" id="form_new" class="form form-horizontal"   >
		         <input type="hidden" name="companyID" value="<?php if(isset($company)){echo $company['id']; } ?>"  />
                 
                  
                    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="app-username">App Name</label>
							<div class="col-md-4">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($company)){echo $company['companyName']; }else{ echo "PayPortal"; } ?>" placeholder="Application Name"><?php echo form_error('companyName'); ?>
							</div>
						</div>
                         
                    <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">QWBC Username</label>
							<div class="col-md-4">
								<input type="text" id="qwbc_username" name="qwbc_username" class="form-control"  readonly="readonly" value="<?php if(isset($company)){echo $company['qbwc_username']; }else{ echo ($qb_username)?$qb_username:'';} ?>"  placeholder="Username">
							</div>
						</div>
                      
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">QWBC Password</label>
							<div class="col-md-4">
								<input type="password" id="qwbc_password" name="qwbc_password"   class="form-control" placeholder="Password">
							</div>
						</div>
                      
                         
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Confirm Password</label>
							<div class="col-md-4">
								<input type="password" id="confirm_password" name="confirm_password"   class="form-control" placeholder="Confirm Password">
							</div>
						</div>
                        </div>
                   
				  	
	               <div class="form-group">
	                  	<div class="col-md-4">
					</div><div class="col-md-4 text-right">
					     
							<button type="button" id="btn_qbd" class=" btn btn-sm  btn-success" >Save</button>
					         <a href="<?php echo base_url(); ?>home/company" class="btn  btn-sm btn-primary1" >Cancel</a>
					</div>
					<div class="col-md-4 ">
					    
					</div>
					
				</div>					
					
		
		
  	</form>
  	<?php } if(isset($company)) { ?>
  		<form method="POST" id="form_edit" class="form form-horizontal" >
		         <input type="hidden" name="companyID" value="<?php if(isset($company)){echo $company['id']; } ?>"  />
                 
                  
                    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="app-username">App Name</label>
							<div class="col-md-4">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($company)){echo $company['companyName']; }else{ echo "PayPortal";} ?>" placeholder="Application Name"><?php echo form_error('companyName'); ?>
							</div>
						</div>
                         
                    <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">QWBC Username</label>
							<div class="col-md-4">
								<input type="text" id="qwbc_username" name="qwbc_username" class="form-control"  readonly="readonly" value="<?php if(isset($company)){echo $company['qbwc_username']; }else{ echo ($qb_username)?$qb_username:'';} ?>"  placeholder="Username">
							</div>
						</div>
                      
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">QWBC Password</label>
							<div class="col-md-4">
								<input type="text" id="qwbc_password" name="qwbc_password"   class="form-control" placeholder="Password"  value="<?php if(isset($company)){echo $company['qbwc_password']; } ?>"  >
							</div>
						</div>
                      
                        </div>
                   
				  	
	              <div class="form-group">
	                  	<div class="col-md-4 ">
						
							
					</div>
					<div class="col-md-4 text-right">
					    
							<button type="button" id="btn_qbd" class="submit btn btn-sm  btn-success" >Save</button>
							 <a href="<?php echo base_url(); ?>home/company" class="btn  btn-sm btn-primary1" >Cancel</a>
					    </div>
					<div class="col-md-4 ">
					      
					
					</div>
					
				</div>					
					
		
		
  	</form>
  	
  	<?php } ?>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->

</div>



<!-- END Page Content -->

<script>


$(document).ready(function(){
    $.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
      


    $('#form_new').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'companyAddress1': {
                        required: true,
                        minlength: 5
                    },
					
					'companyEmail':{
					      required:true,
						  email:true,
						  
					},
					  'companyCountry': {
                        required: true,
                        
                    },
                    'companyState': {
                        required: true,
                        
                    },
					  'companyCity': {
                        required: true,
                         
                    },
					  'companyContact': {
                        required: true,
						 minlength: 10,
						 number : true,
						 maxlength: 12,
                       
                    },
                     'zipCode': {
                       	minlength:3,
						maxlength:10,
						ProtalURL:true,
						validate_addre:true,
                    },
                    
               		
					'portal_url':{
                        minlength:2,
                        maxlength: 20,
                        ProtalURL:true,
                        "remote" :function(){return Chk_url('portal_url');} 
					},
					'qwbc_password':{
					    required: true,
					   minlength: 6,
					   },
					'confirm_password':{
					    required: true,
					   equalTo: '#qwbc_password',
                    }
			
			},
    });
    
    
    
    $('#form_edit').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'companyName': {
                        required: true,
                         minlength: 3
                    },
                   
				
			
			},
    });
    
    $('#btn_qbd').click(function(){
     
       var forid = $(this).closest("form").attr('id');
         
         if(forid=='form_edit')
       var formdata=  $('#form_edit').serialize();
       else
        var formdata=  $('#form_new').serialize();
   
            $.ajax({
           beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },      
                
        type: "POST",
        url: "<?php echo base_url('QuickBooks/create_new_company'); ?>",
        data: formdata,
        dataType: 'json',
        success:function(data){
			if(data.status="Success" && data.file_name!='')
			{ 
			    var uri ='<?php echo  base_url().'uploads/'; ?>'+data.file_name;
		    	downloadURI(uri,data.file_name);
		     
			}
			setInterval(location.href = "<?php echo base_url('home/company'); ?>" ,1000);
        }
    });
    });
    
   
    
    
    $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QuickBooks/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QuickBooks/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	

	
		
});	
      
function downloadURI(uri, name) 
{
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}	

</script>

