<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #f07746; color: #fff; }
	::-moz-selection { background-color: #f07746; color: #fff; }

	body {
		background-color: #fff;
		margin: 40px auto;
		max-width: 1024px;
		font: 16px/24px normal "Helvetica Neue",Helvetica,Arial,sans-serif;
		color: #808080;
	}

	a {
		color: #dd4814;
		background-color: transparent;
		font-weight: normal;
		text-decoration: none;
	}

	a:hover {
	   color: #97310e;
	}

	h1 {
		color: #fff;
		background-color: #dd4814;
		border-bottom: 1px solid #d0d0d0;
		font-size: 22px;
		font-weight: bold;
		margin: 0 0 14px 0;
		padding: 5px 10px;
		line-height: 40px;
	}

	h1 img {
		display: block;
	}

	h2 {
		color:#404040;
		margin:0;
		padding:0 0 10px 0;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 13px;
		background-color: #f5f5f5;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
		min-height: 96px;
	}

	p {
		 margin: 0 0 10px;
		 padding:0;
	}

	p.footer {
		text-align: right;
		font-size: 12px;
		border-top: 1px solid #d0d0d0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
		background:#8ba8af;
		color:#fff;
	}

	#container {
		margin: 10px;
		border: 1px solid #d0d0d0;
		box-shadow: 0 0 8px #d0d0d0;
		border-radius: 4px;
	}
	
	.lab_width{
	    
	    width:300px;
	}
	</style>
	
</head>
<body>
		<div id="container">
	
	<div id="body">
			<p>This is the User page to display and add the Quick book users.</p>

			<h1>Adding Users to Quick book </h1>
			<code>application/views/user.php <?php  $base_url= base_url();  echo  "Base URL".base_url() ; ?></code>
            <h2>Add User </h2> 
			 <fieldset>
			  <form  action="<?php echo $base_url.'NMICron/do_something'; ?>"  method="post" >
			   <label class="lab_width"> Name</label>
			   <input type="text" name="Name" class="" placeholder="Enter your Name" ><br/><br>
			   <label class="lab_width">First Name</label>
			   <input type="text" name="FirstName" class="" placeholder="Enter your FirstName" ><br/><br>
			   <label class="lab_width">Last Name</label>
			   <input type="text" name="LastName" class="" placeholder="Enter your Last Name" ><br/><br>
			   <label class="lab_width">Contact</label>
			   <input type="text" name="Contact" class="" placeholder="Enter your Contact" ><br/><br>
			   
			     <label class="lab_width">ShipAddress_Addr1</label>
			   <input type="text" name="ShipAddress_Addr1" class="" placeholder="Enter your ShipAddress Addr1" ><br/><br>
			   <label class="lab_width">ShipAddress City</label>
			   <input type="text" name="ShipAddress_City" class="" placeholder="Enter your City" ><br/><br>
			   <label class="lab_width">ShipAddress State</label>
			   <input type="text" name="ShipAddress_State" class="" placeholder="Enter your City" ><br/><br>
			   
			   
			   <label class="lab_width">ShipAddress Postal </label>
			   <input type="text" name="ShipAddress_PostalCode" class="" placeholder="Enter your Postal" ><br/>
			   
			   <input type="submit" name="btnSubmit" value="save user"  />
			   
			  </form>
			 
			 </fieldset>
			
			<code>List Of Users of Quickbooks</code>
			
			<table>
			 <thead>
			 <th>Serial No.</th>
			 <th>Customer Name</th>
			 <th>FullName </th>
			 <th>FullName </th>
			 <th>City </th>
			 <th>State </th>
			 </thead>
			 
			 <tbody>
			   <?php if(!empty($users)){   foreach( $users as $user){ ?>
			   <tr>
			   <td><?php echo $user['ListID']; ?></td>
			   <td><?php echo $user['FirstName']; ?></td>
			   <td><?php echo $user['FullName']; ?></td>
			   <td><?php echo $user['Contact']; ?></td>
			   <td><?php echo $user['ShipAddress_City']; ?></td>
			   <td><?php echo $user['ShipAddress_State']; ?></td>
			   
			   </tr>
			   <?php } } ?>
			 <tbody>
			  
			</table>
			
	
			
		</div>

		<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  '<strong> 000000000000000Quickbook Soap Integration </strong>' : '' ?></p>
	</div>
</body>
</html>
	