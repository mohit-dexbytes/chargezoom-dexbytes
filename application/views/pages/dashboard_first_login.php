<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<style>
#upload-file-selector{
    width:90px;
    color:transparent;
	float: left;
}
#fileLabel{
	font-size:13px;
	font-weight: 600;
	padding-top: 7px;
}
.recomand-size{
	color: #888888 !important;
}
.termAndConditionDiv{
	padding: 0 0 10px 0px;
    float: right;
    width: 100%;
    text-align: right;
}
.anchorTerms{
	color: #1185fe;
}
.continue_later{
	margin-top: 20px;
	text-decoration: underline;
}
button.action.next.btn.btn-sm.btn-success.customSkip {
    color: rgb(136, 136, 136) !important;
    background-color: rgb(242, 242, 242) !important;
    border-color: rgb(170, 170, 170) !important;
}
#termsAndCondition-error{
	clear: both;
    text-align: right;
    margin-right: 50px;
}
</style>
<div id="page-content">
   
<style> .error{color:red; }
.btn-coming-soon{
	background-color: #808080 !important;
	border-color: #808080 !important;
	cursor: auto !important;
}
.btn-coming-soon:hover{
	background-color: #808080 !important;
	border-color: #808080 !important;
}
.intuitPlatformConnectButton{
	display: none;
    margin-left: auto;
    margin-right: auto;
    width: 120px;
	line-height: 29px;
	margin-bottom: 5px;
}
</style>    
    <!-- END Wizard Header -->
                        <?php $dis=''; if($gmode==0){ $dis='disabled '; } ?>
    <!-- Progress Bar Wizard Block -->
    <input type="hidden" name="accessMode" id="accessMode" value="<?php echo $gmode; ?>">
    <div class="block">
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		 <div class="progress" style="display:none;">
             <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
		 	<form method="POST" id="stepFormManage" class="form form-horizontal"  enctype="multipart/form-data"  action="<?php echo base_url(); ?>firstlogin/dashboard_first_login">
		       <div class="step" id="step1">
		
				
					<h3><small class="setup-text"><strong>Setup Wizard</strong> </small><br><hr></h3>
					<div class="col-sm-6">
					
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Company Name <span class="text-danger">*</span></label>
							<div class="col-md-8">
								<input type="text" id="companyName" name="companyName" class="form-control" value="<?php echo ($login_info['companyName'])?$login_info['companyName']:''; ?>" placeholder="Company Name"><?php echo form_error('companyName'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">First Name <span class="text-danger">*</span></label>
							<div class="col-md-8">
								<input type="text" id="firstName" name="firstName" class="form-control" value="<?php echo ($login_info['firstName'])?$login_info['firstName']:''; ?>" placeholder="First Name"><?php echo form_error('firstName'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address 1</label>
							<div class="col-md-8">
								<input type="text" id="companyAddress1" name="companyAddress1"   value="<?php echo ($login_info['merchantAddress1'])?$login_info['merchantAddress1']:''; ?>" class="form-control" placeholder="Address 1"><?php echo form_error('companyAddress1'); ?>
							</div>
						</div>
						
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="city" for="example-typeahead">City</label>
						   <div class="col-md-8">
							    <input type="text" id="city" class="form-control" name="companyCity" value="<?php echo ($login_info['merchantCity'])?$login_info['merchantCity']:''; ?>" placeholder="City">
							
							</div>
                        </div>	
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-email">Zip Code</label>
							<div class="col-md-8">
								<input type="text" id="zipCode" name="zipCode" class="form-control" value="<?php echo ($login_info['merchantZipCode'])?$login_info['merchantZipCode']:''; ?>" placeholder="Zip Code"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-email">Email Address <span class="text-danger">*</span></label>
							<div class="col-md-8">
								<input type="text" id="companyEmail" readonly name="companyEmail" class="form-control" value="<?php echo ($login_info['merchantEmail'])?$login_info['merchantEmail']:''; ?>"  placeholder="Email Address">
							</div>
						</div>
					
					</div>
				
					<div class="col-sm-6">
				
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-password">Phone Number</label>
							<div class="col-md-8">
								<input type="text" id="companyContact" name="companyContact" class="form-control" value="<?php echo ($login_info['merchantContact'])?$login_info['merchantContact']:''; ?>" placeholder="Phone Number">
							</div>
						</div>
							<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Last Name <span class="text-danger">*</span></label>
							<div class="col-md-8">
								<input type="text" id="lastName" name="lastName" class="form-control" value="<?php echo ($login_info['lastName'])?$login_info['lastName']:''; ?>" placeholder="Last Name"><?php echo form_error('lastName'); ?>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address 2</label>
							<div class="col-md-8">
								<input type="text" id="companyAddress2" name="companyAddress2"  class="form-control"  value="<?php echo ($login_info['merchantAddress2'])?$login_info['merchantAddress2']:''; ?>"  placeholder="Address 2 (Optional)">
							</div>
						</div>
					
                        
                        	<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state" for="example-typeahead">State</label>
						   <div class="col-md-8">
						        <input type="text" id="state" class="form-control" name="companyState" value="<?php echo ($login_info['merchantState'])?$login_info['merchantState']:''; ?>" placeholder="State">
							
							</div>
                        </div>	
                        
					
							
					<div class ="form-group">
						   <label class="col-md-4 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-8">
							 <input type="text" id="country" class="form-control" name="companyCountry" value="<?php echo ($login_info['merchantCountry'])?$login_info['merchantCountry']:''; ?>" placeholder="Country">
						
							
							</div>
                        </div>	
						
						<div class ="form-group" >
						   <label class="col-md-4 control-label" for="example-typeahead"></label>
						   <div class="col-md-8">
							 
							</div>
                        </div>
                        <div class ="form-group" >
	                        
	                        	<label class="col-md-4 control-label"></label>
	                        	<div class="col-md-8">
	                        		<div class="termAndConditionDiv">
	                        			<?php if(ENVIRONMENT == 'development'){
					          				$urlLink = 'https://payportaltest.com/terms';
					          			}else{
					          				$urlLink = 'https://payportal.com/terms';
					          			} ?>
					          			<input type="checkbox" name="termsAndCondition" id="termsAndCondition" >  Must accept <a href="<?php echo $urlLink; ?>" class="anchorTerms" target="_blank">Terms of Service</a>
	                        		</div>
	                        	</div>
			          			
			          		
			          	</div>	
					
					</div>  
					
			</div>
			<div  class="step" id="step2">
					
					<h3><small class="setup-text"><strong>Setup Wizard</strong> </small><br><hr></h3>
						<div class="col-sm-6">
       
         
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-lastname">Portal URL</label>
							
							<div class="col-md-8">
								<div class="input-group ">
								<input type="text" id="portal_url" name="portal_url" value="<?php  echo ($login_info['portalprefix'])?$login_info['portalprefix']:''; ?>" class="form-control" placeholder="">
								<span class="input-group-btn"><a class="btn btn-primary"><?php echo CUS_PORTAL ;?></a> </span>
									
                                   </div>
                              </div>
						
						
						</div>   
                             
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Customer Help Text</label>
							<div class="col-md-8">
								<input type="text" id="tagline" name="tagline"  value="<?php  echo ($login_info['merchantTagline'])?$login_info['merchantTagline']:''; ?>" class="form-control" placeholder="Customer Help Text">
							</div>
						</div>
						
					</div>
					<div class="col-sm-6">
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-file-input">Upload Logo</label>
							<div class="col-md-8">
							<?php if(isset($login_info)){ $mgurl = $login_info['ProfileImage'];  } ?>
							
							 <span id="fileselector">
								
								<input type='file' id="upload-file-selector" name="picture" onchange="pressed()"><label id="fileLabel">(Optional)</label>
							</span>
								<br>
								<span class="text-danger"><strong class="recomand-size">Recommended Logo Size is <?php echo LOGOWIDTH; ?> x <?php echo LOGOHEIGHT; ?></strong></span>
								<br>
								<?php
									$logoImage = (isset($login_info['ProfileImage']) && !empty($login_info['ProfileImage'])) ? base_url().LOGOURL.$login_info['ProfileImage'] : '';
									if(!empty($logoImage)){ ?>
										<img src="<?php echo $logoImage; ?>" height="<?php echo LOGOHEIGHT; ?>" width="<?php echo LOGOWIDTH; ?>">
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
		
				<div class="step" id="step3">
					
					
					<h3><small class="setup-text"><strong>Setup Wizard</strong> </small><br><hr></h3>	 	
					  <div class="col-md-12">	
						
					   
						
						 <div class="col-md-6 no-pad">
						   <div class="widget">
							 
							<div class="widget-content" >
								 <a href="#qb-connecter-settings" id="dddddc" data-backdrop="static" data-keyboard="false" data-toggle="modal"><img class="image" src="<?php echo base_url(IMAGES); ?>/quickbooks_desktop.png"></a>
							    	
									<br>
							 </div>
						   </div>
						 </div>
						 
                     <div class="col-md-6 no-pad">
							<div class="widget">
							 
						    	
							   <div class="widget-content" >
					 	
							   
								  <img id="qbo-image" style="cursor: pointer;" class="image" src="<?php echo base_url(IMAGES); ?>/quickbooks_online.png">
								  <ipp:connectToIntuit></ipp:connectToIntuit>
									<br>
							 </div>
							 
						   </div>
						 </div>
                    
						  <div class="col-md-6 no-pad">
						   <div class="widget">
							 <!-- <div  class="widget-header">
									
							</div> -->
							
							 <div class="widget-content" id="#connect_fb" onclick="connectFB(this)">
								<img style="cursor: pointer; " class="image" src="<?php echo base_url(IMAGES); ?>/freshbooks.png">
									<br>
							 </div>
						   </div>
						 </div>	
						 
						 <div class="col-md-6 no-pad" id="xero_integration_image">
						   <div class="widget">
						   <!-- <div  class="widget-header">
							</div> -->
							
							 <div class="widget-content">
								<img style="cursor: pointer; " class="image" src="<?php echo base_url(IMAGES); ?>/xero_logo.png">
						
						
									<br>
							 </div>
						   </div>
						 </div>	 
						 <div class="col-md-6 no-pad">
						   	<div class="widget">
							
								<div class="widget-content">
									<img  id="" style="cursor: pointer; " class="image" src="<?php echo base_url(IMAGES); ?>/netsuite.png">
									<br>
							 	</div>
						   </div>
						 </div>
						 <div class="col-md-6 no-pad">
						   	<div class="widget">
							
								<div class="widget-content" >
									<img  id="" style="cursor: pointer; " class="image" src="<?php echo base_url(IMAGES); ?>/sage_intacct.png">
									<br>
							 	</div>
						   </div>
						 </div>	
					  </div>
				</div>
				<div class="step" id="step4">
		
		
	
		<h3><small class="setup-text"><strong>Setup Wizard</strong> </small><br><hr></h3>
					<div class="col-sm-6" id="nmitest">
					
			     	<div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt"  <?php echo $dis; ?> class="form-control">
						       
								   <option value="" >Select Gateway</option>
								   <?php foreach($all_gateway as $gat_data){ 
								       $sel='';
								       if(isset($gateway_data) && $gateway_data['gatewayType']==$gat_data['gateID']){  $sel = ' selected="selected" '; } 
								   		echo '<option value="'.$gat_data['gateID'].'"  '.$sel.' >'.$gat_data['gatewayName'].'</option>';
								   } 
								   ?>
								  
							</select>
							</div>
					</div>
					
		    	<div class="gateway-div" id="nmi_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1' ){ echo ' style="display:block" ';}else{ echo ' 					style="display:none" ';} ?> >			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">NMI
                        
                        </label>
                        <div class="col-md-6">
                             <input type="text" id="nmi_user" name="nmi_user" class="form-control" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="NMI Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="password" id="nmiPassword" name="nmi_password" class="form-control"   <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="NMI Password">
                           
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?> >
                           
                        </div>
                    </div>
				</div>

				<div class="gateway-div" id="cz_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='9' ){ echo ' style="display:block" ';}else{ echo ' 					style="display:none" ';} ?> >			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Chargezoom
                        
                        </label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="czUser" class="form-control" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='9'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="password" id="czPassword" name="czPassword" class="form-control"   <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='9'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="Password">
                           
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='9' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='9' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?> >
                           
                        </div>
                    </div>
				</div>
				
               <div class="gateway-div" id="auth_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?> >					
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="API LoginID">
                        </div>
                    </div>
					
				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="Transaction Key">
                           
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?> >
                           
                        </div>
                    </div>
			 </div>	
			
			
				<div class="gateway-div" id="pay_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='3' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?> >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control"   <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='3'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="PayTrace Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="password" id="paytracePassword" name="paytracePassword"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='3'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?> class="form-control"  placeholder="PayTrace Password">
                           
                        </div>
                    </div>
     
					<div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_cr_status" name="paytrace_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='3' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_ach_status" name="paytrace_ach_status"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='3' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?>  >
                           
                        </div>
                    </div>
					
				</div>	
				
				<div class="gateway-div" id="paypal_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='4' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?> >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='4'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="password" id="paypalPassword" name="paypalPassword" class="form-control" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='4'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="Password">
                           
                        </div>
                    </div>
					
					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='4'){ ?>  value="<?php echo  $gateway_data['gatewaySignature'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="Signature">
                           
                        </div>
                    </div>
					
				</div>	
				
				
				
				<div class="gateway-div" id="stripe_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='5' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?> >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='5'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="Publishable Key">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="password" id="stripePassword" name="stripePassword" class="form-control" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='5'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="Secret API Key">
                           
                        </div>
                    </div>
					
				</div>	
				
				<div class="gateway-div" id="usaepay_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='6' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?> >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  placeholder="USAePay PIN">
                           
                        </div>
                    </div>
				</div>
				 <div class="gateway-div" id="heartland_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='7' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?> >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; placeholder="Public Key" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='7'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> >
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  placeholder="Secret Key" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='7'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>>
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_cr_status">Credit Card</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="heart_cr_status" name="heart_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='7' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_ach_status">Electronic Check</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="heart_ach_status" name="heart_ach_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='7' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?> >
                            
                        </div>
                    </div>
				</div>
				
				
				<div class="gateway-div" id="cyber_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='8' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>  >	
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID" name="cyberMerchantID" class="form-control"  placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
				
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber" name="apiSerialNumber" class="form-control" placeholder="Enter API Key ID">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey" name="secretKey" class="form-control"  placeholder="Enter  Secret Key">
                           
                        </div>
                    </div>
					
					 
					
				</div>	
				
				<div class="gateway-div" id="iTransact_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='10' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactUsername">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="iTransactUsername" name="iTransactUsername" class="form-control"  placeholder="API Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactAPIKIEY">API Key</label>
                       <div class="col-md-6">
                            <input type="text" id="iTransactAPIKIEY" name="iTransactAPIKIEY" class="form-control"  placeholder="API Key">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_cr_status" name="iTransact_cr_status" checked="checked" value='1' <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='10' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_ach_status" name="iTransact_ach_status" value='1'  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='10' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?>  >
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="add_surcharge_box">Surcharge</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="add_surcharge_box" name="add_surcharge_box" value='1' onchange="surchageCheckChange(this)">
                        </div>
                    </div>
					<div class="form-group" id='surchargePercentageBox' style="display: none">
                        <label class="col-md-4 control-label" for="surchargePercentage">Surcharge Rate</label>
                        <div class="col-md-6">
							<input type="text" id="surchargePercentage" name="surchargePercentage" class="form-control"  placeholder="Surcharge Rate" value='0'>
                        </div>
                    </div>
				</div>

				<div class="gateway-div" id="fluid_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='11' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>         
					<div class="form-group">
						<label class="col-md-4 control-label" for="fluidUser">API KEY</label>
						<div class="col-md-8">
							<input type="text" id="fluidUser" name="fluidUser" class="form-control"  placeholder="API KEY">
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="fluid_cr_status">Credit Card</label>
						<div class="col-md-8">
							<input type="checkbox" id="fluid_cr_status" name="fluid_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='11' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="fluid_ach_status">Electronic Check</label>
						<div class="col-md-8">
							<input type="checkbox" id="fluid_ach_status" name="fluid_ach_status"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='11' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?>  >
							
						</div>
					</div>
				</div>

				<div class="gateway-div" id="basys_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='13' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>         
					<div class="form-group">
						<label class="col-md-4 control-label" for="basysUser">API KEY</label>
						<div class="col-md-8">
							<input type="text" id="basysUser" name="basysUser" class="form-control"  placeholder="API KEY" value="<?php if(isset($gateway_data) && $gateway_data['gatewayType']=='13' ){ echo  $gateway_data['gatewayUsername']; } ?>">
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="basys_cr_status">Credit Card</label>
						<div class="col-md-8">
							<input type="checkbox" id="basys_cr_status" name="basys_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='13' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="basys_ach_status">Electronic Check</label>
						<div class="col-md-8">
							<input type="checkbox" id="basys_ach_status" name="basys_ach_status"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='13' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?>  >
							
						</div>
					</div>
				</div>

				<div class="gateway-div" id="payarc_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='15' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>         
					<div class="form-group">
						<label class="col-md-4 control-label" for="payarcUser">Secret KEY</label>
						<div class="col-md-8">
							<input type="text" id="payarcUser" name="payarcUser" class="form-control"  placeholder="Secret KEY" value="<?php if(isset($gateway_data) && $gateway_data['gatewayType']=='15' ){ echo  $gateway_data['gatewayUsername']; } ?>">
						</div>
					</div>
				</div>
				
				
				<div class="gateway-div" id="TSYS_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='12' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>         
					<div class="form-group">
						<label class="col-md-4 control-label" for="TSYSUser">User ID</label>
						<div class="col-md-8">
							<input type="text" id="TSYSUser" name="TSYSUser" class="form-control"  placeholder="User ID">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="TSYSPassword">Password</label>
                        <div class="col-md-6">
                            <input type="password" id="TSYSPassword" name="TSYSPassword" class="form-control" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='12'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="Password">
                           
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="TSYSMerchantID">TSYS MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="TSYSMerchantID" name="TSYSMerchantID" class="form-control"  placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="TSYS_cr_status">Credit Card</label>
						<div class="col-md-8">
							<input type="checkbox" id="TSYS_cr_status" name="TSYS_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='12' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="TSYS_ach_status">Electronic Check</label>
						<div class="col-md-8">
							<input type="checkbox" id="TSYS_ach_status" name="TSYS_ach_status"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='12' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?>  >
							
						</div>
					</div>
				</div>
				<div class="gateway-div" id="EPX_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='16' ){ echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>    
				          
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXCustNBR">CUST NBR</label>
                        <div class="col-md-8">
                                <input type="text" id="EPXCustNBR" name="EPXCustNBR" class="form-control" value="<?php if(isset($gateway_data) && $gateway_data['gatewayType']=='16' ){ echo  $gateway_data['gatewayUsername']; } ?>" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXMerchNBR">MERCH NBR</label>
                        <div class="col-md-8">
                            <input type="text" id="EPXMerchNBR" name="EPXMerchNBR" class="form-control" value="<?php if(isset($gateway_data) && $gateway_data['gatewayType']=='16' ){ echo  $gateway_data['gatewayPassword']; } ?>" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXDBANBR">DBA NBR</label>
                        <div class="col-md-8">
                            <input type="text" id="EPXDBANBR" name="EPXDBANBR" class="form-control" value="<?php if(isset($gateway_data) && $gateway_data['gatewayType']=='16' ){ echo  $gateway_data['gatewaySignature']; } ?>">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXterminal">TERMINAL NBR</label>
                        <div class="col-md-8">
                            <input type="text" id="EPXterminal" name="EPXterminal" class="form-control" value="<?php if(isset($gateway_data) && $gateway_data['gatewayType']=='16' ){ echo  $gateway_data['extra_field_1']; } ?>" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPX_cr_status">Credit Card</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="EPX_cr_status" name="EPX_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='16' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPX_ach_status">Electronic Check</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="EPX_ach_status" name="EPX_ach_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='16' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?> >
                            
                        </div>
                    </div>
                </div>

				<?php include(APPPATH."/views/gateways/maverick_add.php"); ?>
				
				<div class="gateway-div" id="cardpointe_div" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='14' ){ echo ' style="display:block" ';}else{ echo ' 					style="display:none" ';} ?> >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Site Variable</label>
                        <div class="col-md-6">
                        	<input type="text" id="cardpointeSiteName" name="cardpointeSiteName" class="form-control"  placeholder="fts (default) if you any custom variable name please insert here" value="<?php echo  $gateway_data['gatewaySignature'];  ?>">
                            <i class="fa fa fa-info-circle gatewayInformationIcon" aria-hidden="true" data-toggle="tooltip" title="fts (default) if you any custom variable name please insert here"></i>
                        </div>
                    </div>  				
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">CardPointe User Name
                        
                        </label>
                        <div class="col-md-6">
                             <input type="text" id="cardpointe_user" name="cardpointe_user" class="form-control" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='14'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="CardPointe Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="cardpointePassword">Password</label>
                       <div class="col-md-6">
                            <input type="password" id="cardpointePassword" name="cardpointe_password" class="form-control"   <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='14'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="CardPointe Password">
                           
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="cardpointeMerchID">Merchant ID</label>
                       <div class="col-md-6">
                            <input type="password" id="cardpointeMerchID" name="cardpointeMerchID" class="form-control"   <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='14'){ ?>  value="<?php echo  $gateway_data['gatewayMerchantID'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="CardPointe Merchant ID ">
                           
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="cardpointe_cr_status">Credit Card</label>
                        <div class="col-md-6">
                    
                            <input type="checkbox" id="cardpointe_cr_status" name="cardpointe_cr_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType'] =='14' && $gateway_data['creditCard'] == 1){ echo "checked"; } ?> >
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cardpointe_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cardpointe_ach_status" name="cardpointe_ach_status" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='14' && $gateway_data['echeckStatus'] == 1){ echo "checked"; } ?> >
                        
                        </div>
                    </div>
				</div>      
	</div>		
				
                        </div>
				<!-- END Fourth Step -->
				
				
						
				<div class="col-md-12 row text-right">
	              <div class="form-group ">
	              		

						<button type="button" class="action back btn btn-sm" style="color: #888888; background-color: #f2f2f2; border-color: #aaaaaa;">Back</button>
						<button type="button" class="action next btn btn-sm btn-success ">Next</button>
						<button type="button" class="action skip-btn btn btn-sm" style="font-size: 14px; color: #888888; background-color: #f2f2f2; border-color: #aaaaaa;">Skip</button>
					<a href="<?php echo base_url('company/setup/index'); ?>" id="skipbtn" class="action btn btn-sm" style="font-size: 14px; color: #888888; background-color: #f2f2f2; border-color: #aaaaaa;">Skip</a>
					</div>
				</div>		
		
		
  	</form>
  	
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->

    </div>

	<div id="first_login_integration_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Body -->
				<div class="modal-body">
					<p>Have your Accounting Software login information ready to proceed.</p>
					<p>Or, schedule an <a href="<?php echo FIRST_LOGIN_SUPPORT_URL; ?>" target="_blank" style="text-decoration: underline !important;"> onboarding session</a> and we will walk you through it.</p>
					<p>If you do not use an Accounting Software and don't plan on integrating one, select No Accounting Software below</p>
					<div class="col-md-12">
						<div class="col-md-6 text-right">
							<?php
								if($gmode)
								{
							?>
								<button type="button" id="nap_first_login" class="action next nap btn btn-sm btn-default" data-dismiss="modal">No Accounting Software</button>
							<?php
								} else {
							?>
								<a href="<?php echo base_url('company/setup/index'); ?>" id="nap_first_login" class="btn btn-sm btn-default">No Accounting Software</a>
							<?php
								}
							?>
						</div>
						
						<div class="col-md-6">
							<button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Connect Accounting Software</button>
						</div>
					</div>
					<div class="continue_later col-md-12 text-center">
						<a href="<?php echo base_url(); ?>logout?lr=1&t=<?php echo time(); ?>">Continue Later</a>
					</div>
				</div>
				<!-- END Modal Body -->
				<div class="modal-header">
					
				</div>
			</div>
		</div>
	</div>

<!-- END Page Content -->
<style>
.overlay {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: transparent;
    background-color: transparent;
    overflow-x: hidden;
    transition: 0.5s;
}
.widget{
	
	display: block;
	margin-left: auto; 
	margin-right: auto; 
	margin-bottom: 0px;
}

.text
{
	text-align: center; 
	margin-top: 10px;
}
.display
{
	display: block; 
	margin-left: auto; 
	margin-right: auto;
	width:120px;
}
.image
{
	display: block; 
	margin-left: auto; 
	margin-right: auto; 
}

.form-control-static{
	font-size:14px;
	padding-top: 5px;
    padding-bottom: 7px;
    margin-bottom: 0;
    min-height: 34px;
	
}
h4, .h4 {
    font-size: 17px;
}

.form-group {
    margin-bottom: 5px;
}
button.action.btn {
    font-size: 14px;
}
.bttn
{
     margin-right:15px;
	 font-size: 14px;
}
label {
	padding-top: 2px;
    padding-bottom: 7px;
    margin-bottom: 0;
    display: inline-block;
    max-width: 100%;
   font-size:14px;
    font-weight: 5;
}
.control-label.txt{
   font-weight: 500;
}
.action.back.btn:hover, .action.back.btn:focus, .action.back.btn:focus{
    border-color: #888888 !important;
}
@media screen and (min-width: 767px){

.no-pad {
	padding-left: 0px;
}}
</style>
 <?php  $complete =  $this->session->userdata('logged_in');
        
		  $completed =0;
		 $finalcompleted =0;
	    
	
       
	
        if($complete['companyName']!="" && $complete['merchantAddress1'] !=""){
			  $completed = 0;
			  $finalcompleted =1;
		}	 
	   if($complete['companyName']!="" && $complete['portalprefix'] !=""  ){
			 
			  $completed = 0;
			  $finalcompleted =2;
		  }		
	    if($complete['companyName']!="" && $complete['portalprefix'] !=""  && $complete['is_integrate'] =="1" ){
			 
				$finalcompleted =3;	
			  $completed = 0;
		  }
	
   
?>

<script>
window.pressed = function(){
    var a = document.getElementById('upload-file-selector');
    if(a.value == "")
    {
        fileLabel.innerHTML = "(Optional)";
    }
    else
    {
        var theSplit = a.value.split('\\');
        fileLabel.innerHTML = theSplit[theSplit.length-1];
    }
};

$(document).ready(function(){
	$('#qbo-image').click(function(){
		$('.intuitPlatformConnectButton').click();
	});

    $('#xero_integration_image').click(function(){
		var base_url = $('#js_base_url').val();
	    window.open(`${base_url}Integration/Xero/authenticate`, '_blank', 'width=650,height=650');
    	return false;
    });

	$('#gateway_opt').change(function(){
		var gateway_value =$(this).val();

		$('.gateway-div').hide();

		if(gateway_value=='3'){			
			$('#pay_div').show();
		}
		else if(gateway_value=='2'){
			$('#auth_div').show();
		} 
		else if(gateway_value=='1'){
			$('#nmi_div').show();	
		}
		else if(gateway_value=='4'){
			$('#paypal_div').show();
			
		}
		else if(gateway_value=='5'){
			$('#stripe_div').show();
			
		}	
		else   if(gateway_value=='6'){
			$('#usaepay_div').show();
			
		}
		else   if(gateway_value=='7'){
			$('#heartland_div').show();
			
		}
		else  if(gateway_value=='8'){
			$('#cyber_div').show();
			
		}
		else if(gateway_value=='9'){
			$('#cz_div').show();
			
		}
		else if(gateway_value=='10'){
			$('#iTransact_div').show();
			
		}
		else if(gateway_value=='11'){
			$('#fluid_div').show();
		}else if(gateway_value=='12'){
			$('#TSYS_div').show();
		}else if(gateway_value=='13'){
			$('#basys_div').show();

		}else if(gateway_value=='14'){
			$('#cardpointe_div').show();
		}else if(gateway_value=='15'){
			$('#payarc_div').show();

		} else if(gateway_value=='17'){
			$('#maverick_div').show();
		} 
	}); 
	  

	var current = <?php echo $completed ?>;
	var accessMode = <?php echo $gmode ?>;
	var finalcompleted = <?php echo $finalcompleted ?>;
	widget      = $(".step");
	btnnext     = $(".next");
	btnback     = $(".back"); 
	btnsubmit   = $(".submit");
	btnfinalskip = $(".skip-btn");
	// Init buttons and UI
	widget.not(':eq(0)').hide();

	if(finalcompleted == 3){
		current = 4;		
	}

	 if(current == 3)
			       hideButtons(current+1);
	else hideButtons(current);
	setProgress(current);

     if(current < widget.length){
			// Check validation
			 
				widget.show();
				widget.not(':eq('+(current++)+')').hide();
				setProgress(current);
			     hideButtons(current);
			     
			     if(current == 4)
			     {
					btnnext.show();
					btnback.hide();
				
			     }
		}
	if(finalcompleted == 3){
		widget.hide();
		$('#step4').show();
	}	
	// Next button click action
	btnnext.click(function(){
		
		
		if(current < widget.length){
			// Check validation
			if($(".form").valid()){
				insert_form_data(current);
				widget.show();
				widget.not(':eq('+(current++)+')').hide();
				setProgress(current);
			}
		}else{
			if($(".form").valid()){
				insert_form_data(current);
				
			}
		}
		if(current == 3){
			if(accessMode == 1){
				$('.action.next').text('Skip');
				$('.action.next').addClass('customSkip');
				
			}else{
				$('.action.next').text('Next');
				$('.action.next').removeClass('customSkip');
				hideButtons(current);
			}

			$('.action.next').addClass('hidden');
			$('.action.back').addClass('hidden');
			$('#skipbtn').addClass('hidden');
			$('#first_login_integration_popup').modal('show');
			$('#nap_first_login').removeClass('hidden');
			$('#nap_first_login').show();
			$('#nap_first_login').text('No Accounting Software');

			var modalDialog = $("#first_login_integration_popup").find(".modal-dialog");
			modalDialog.css("margin-top", Math.max(10, ($(window).height() - modalDialog.height()) / 3));
		}else{
			$('.action.next').text('Next');
			$('.action.next').removeClass('customSkip');
			$('.action.next').removeClass('hidden');
			hideButtons(current);
		}
				
	})
	btnfinalskip.click(function(){
		
		insert_form_data(current);
	
		hideButtons(current);
	})
	// Back button click action
	btnback.click(function(){
		if(current > 1){
			current = current - 2;
			
			if(current < widget.length){
				
				widget.show();
				widget.not(':eq('+(current++)+')').hide();
				setProgress(current);
				btnnext.prop("disabled", false);
			        $(".back").css("position","relative").html("Back");
					
			}
			if(current == 3){
				if(accessMode == 1){
					$('.action.next').text('Skip');
					$('.action.next').addClass('customSkip');
					
				}else{
					$('.action.next').text('Next');
					$('.action.next').removeClass('customSkip');
					hideButtons(current);
				}
			}else{
				$('.action.next').text('Next');
				$('.action.next').removeClass('customSkip');
				hideButtons(current);
			}
		}else{
			hideButtons(current);
		}
	
		
	})
 
   $.validator.addMethod("phoneUS", function(phone_number, element) {
         if(phone_number=="")return true;
            return phone_number.match(/^[+]{0,1}[0-9]{1}[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/);
        }, "Please specify a valid phone number like as +X(XXX) XXX-XXXX");
         
          
      $.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
               $.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );     


	var surchangeMessage = 'Invalid request';

	$.validator.addMethod('surcharge_valid_rate', function(value, element, param) {
		var result = true;
		if($('#'+param).prop('checked') == true){
			surchangeMessage = 'Please enter valid surcharge rate';
			if(value > 0) {
				var regexp = /^[0-9]+([,.][0-9]+)?$/g;
				return regexp.test(value);
			} else {
				return false;
			}
		}
		return this.optional(element) || result;
	}, function() { return surchangeMessage; });                              
                                             
                                         
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
  
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/,. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
     $.validator.addMethod("validate_company", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9][a-zA-Z0-9-_,'&. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore or apostraphe or  &.");
    
 var message1 ="This field is requied";
    $('#stepFormManage').validate({ // initialize plugin
		ignore:":not(:visible)",			
        errorClass: 'help-block animation-slideDown', 
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                 
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
    
		            rules: {
		       'companyName': {
		               minlength: 2,
		               required: true,
                        maxlength: 41,
                        validate_company: true
                    },
                    'firstName': {
		                minlength: 2,
		                required: true,
                        maxlength: 100,
                        validate_char: true
                    },
                    'lastName': {
                       required: true,
		               minlength: 2,
                       maxlength: 100,
                       validate_char: true
                    },
                    'termsAndCondition': {
                    	required: true,
                    },
                    'companyAddress1': {
                        minlength: 2,
                        maxlength: 41,
                        validate_addre: true
                    },
        			
                    'companyState': {
                        minlength: 2,
                       maxlength: 21,
                        validate_addre: true
                    },
					  'companyCity': {
					     minlength: 2,
                        maxlength: 31,
				    	validate_addre: true
                         
                    },
					
                    'zipCode': {
                       	minlength:3,
						maxlength:10,
						
                       ZIPCode :true,
						validate_addre:true
                    },
                    
                   'companyFullAddress':{
                        minlength: 3,
						 maxlength: 41,
                        validate_addre: true
					},
					'portal_url':{
						 required : true,
						 minlength: 3,
						 maxlength: 20,
						 remote: {
						     
						        beforeSend:function() { 
					      	 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					       },
					           complete:function() {
						              $(".overlay1").remove();
					             },
                                url: "<?php echo base_url(); ?>firstlogin/check_url",
                                type: "POST",
                                
                                dataType: 'json',
                                dataFilter: function(response) {
                                    
                                    var rsdata = jQuery.parseJSON(response);
                                 
                               
                                     if(rsdata.status=='success')
                                     return true;
                                     else{
                                          message1 = rsdata.portal_url;
                                       
                                   return false;
                                     }
                                }
                            },
					},
				
                    
				'nmi_user': {
					required: true,
					minlength: 3
                },
                'nmiUser': {
					required: true,
					minlength: 3
                },
				'iTransactUsername':{
					required: true,
				},
				'cardpointe_user' : {
					required: true,
				},
				'cardpointeSiteName' : {
					required: true,
				},
				'iTransactAPIKIEY':{
					required: true,
				},	
				'surchargePercentage':{
					surcharge_valid_rate: 'add_surcharge_box',
				},
				'nmi_password':{
					required : true,
					minlength: 3,
				},	
				'nmiPassword':{
					required : true,
					minlength: 3,
				},	
				'cardpointePassword':{
					required : true,
					minlength : 3,
				},
				'czUser': {
					required: true,
					minlength: 3
                },
				
				'czPassword':{
					required : true,
					minlength: 3,
				},	
        
               
			'apiloginID':{
				required: true,
                 minlength: 3
			},
			'transactionKey':{
				required: true,
                 minlength: 3
			},		 
		 
			'fluidUser':{
				required: true,
                minlength: 5
			},
			
			'basysUser':{
				required: true,
                minlength: 5
			},

			'paytracePassword':{
				required: true,
                minlength: 5
			},
			
			'paytraceUser':{
				required: true,
                 minlength: 3
			},
		 
		
			
			'paypalPassword':{
				required: true,
                minlength: 5
			},
			
			'paypalUser':{
				required: true,
                 minlength: 3
			},
		 
			'paypalSignature':{
				required: true,
                minlength: 5
			},
			
		    
			
				'cyberMerchantID':{
				required: true,
                minlength: 5
			},
			'cardpointeMerchID':{
				required : true,
				minlength : 5,
			},			
			'apiSerialNumber':{
				required: true,
                 minlength: 3
			},
		 
			'secretKey':{
				required: true,
                minlength: 5
			},
        	'TSYSUser':{
				required: true
			},
			'TSYSPassword':{
				required: true
			},
			'TSYSMerchantID':{
				required: true
			},
        
           'stripeUser': {
                 required: true,
                 minlength: 3
                },
				
					'stripePassword':{
						 required : true,
						 minlength: 3,
						
					  },	
        
        	'heartsecretkey':{
				required: true,
                 minlength: 3
			},
		 
			'heartpublickey':{
				required: true,
                minlength: 5
			},
				'transtionPin':{
				required: true,
                 minlength: 3
			},
		 
			'transtionKey':{
				required: true,
                minlength: 5
			},
		
				
				  'qbcompany_name':{
				         required : true,
						 minlength: 3
				  
				  }	  	
		   
			
			},
    
        messages:{
        	
					'portal_url':
                     {
                       required: "This is required",
                     
                      remote: function(){   return  message1; }
                    
                   },
        },
   
    });
	
	

	
	
	    var insert_form_data= function(current){
	        var app_type = '<?php echo $this->session->userdata('logged_in')['active_app']; ?>';
	        var finalcompletedUpdate = <?php echo $finalcompleted ?>;
	             if(current=='2')
	             {
	             var file_data = $("#upload-file-selector").prop("files")[0]; 
	             var form_data = new FormData();                 
	              form_data.append("picture", file_data)    
	               form_data.append("portal_url", $("#portal_url").val());  
	                form_data.append("tagline", $("#tagline").val());  
	                   form_data.append("action",current);  
            
				var dadadad  =form_data;
                 
                 	
			$.ajax({
		       beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
					
					 },
			 complete:function() {
				   $(".overlay1").remove();
			 },
			 url: '<?php echo base_url(); ?>firstlogin/dashboard_first_login_ajaxdata',
			
			 processData: false,
             contentType: false,
             type: "post",
			 data: dadadad,
			 success: function(response){
				   data=$.parseJSON(response);
                             if (data['status'] === 'success') {
				
					        return true;
				     }
             }			 
			});
	             }else{
	             	if(finalcompletedUpdate == 3){
	             		current = 4;
	                 
	              	
	             	}
	         var dadadad  = $('.form').serialize()+'&action='+current;   
				
			$.ajax({
		       beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
					
					 },
			 complete:function() {
				   $(".overlay1").remove();
			 },
			 url: '<?php echo base_url(); ?>firstlogin/dashboard_first_login_ajaxdata',
			
		
             type: "post",
			 data: dadadad,
			 success: function(response){
				   data=$.parseJSON(response);
                             if (data['status'] === 'success') {
				
					        return true;
				     }
             }			 
			});
            if(current == '4'){
			
				if (app_type == '1') {
					window.location =	'<?php echo base_url(); ?>QBO_controllers/QuickBooks_online/final_auth_success';
				}
				else if (app_type == '3') {
					window.location = '<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_integration/final_success_auth';
				}
				else if (app_type == '4') {
					window.location = '<?php echo base_url(); ?>Xero_controllers/Freshbooks_integration/final_success_auth';
				}
				else if (app_type == '2') {
					window.location = '<?php echo base_url(); ?>QuickBooks/final_auth_success';
				}
				else if (app_type == '5') {
					window.location = '<?php echo base_url(); ?>company/setup/final_auth_index';
				}
				
				
			}     
                     }
		}
	
	
	
	 var Chk_url=function(element_name){ 
       
            remote={
				
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>firstlogin/check_url',
                    type: "post",
					data:
                        {
                            portal_url: function(){return $('input[name=portal_url]').val();},
                        },
                        dataFilter:function(response){
							console.log(response);
						   data=$.parseJSON(response);
						
                             if (data['status'] == 'true') {
                                  
							
							    
						     	$('#portal_url').removeClass('error');	 
								 $('#portal_url-error').hide();	 
								 
								 
									   return data['status'] ;				   
									 
                              } else {
                               
								   if(data['status'] =='false'){
								      
                                       $('.form').validate().showErrors(function(){
										   
                                                  return {key:'portal_url'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
	
	
       var Chk_fname=function(element_name){ 
           
           
            remote={
				
				     beforeSend:function() { 
					 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>firstlogin/chk_friendly_name',
                    type: "post",
                    dataType:'json',
					data:
                        {
                            frname: function(){return $('input[name=gfrname]').val();},
                        },
                        dataFilter:function(response){
							
                             if (response.status == 'true') {
                                  
                                alert(response.status);
							
							    
						     	$('#gfrname').removeClass('error');	 
								 $('#gfrname-error').hide();	 
								  $('.next').removeAttr('disabled');
								 
									   return response.status ;				   
									 
                              } else {
                                $('.next').attr("disabled", true); 
								   if(response.status =='false'){
                                       $('.form').validate().showErrors(function(){
										   
                                                       return {key:'gfrname'}; 
                                                       })
								   }					   
                               }
							   
                              return response.element_name;
                         }
                    }
                                             
            return remote;
        }		
		
	
	
       var GetMyRemote=function(element_name){
            $('.next').attr("disabled", true); 
           
            remote={
				
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>firstlogin/testNMI',
                    type: "post",
					data:
                        {
                            nmiuser: function(){return $('input[name=nmiUser]').val();},
                            nmipassword:  function(){return $('input[name=nmiPassword]').val();}
                        },
                        dataFilter:function(response){
							console.log(response);
						   data=$.parseJSON(response);
                             if (data['status'] == 'true') {
                                  
									
							     $('.next').removeAttr('disabled');
						     	$('#nmiPassword').removeClass('error');	 
								 $('#nmiPassword-error').hide();	 
								  
									   return data['status'] ;				   
									 
                              } else {
                                
								   if(data['status'] =='false'){
                                       $('.form').validate().showErrors(function(){
										   
                                                       return {key:'nmiPassword'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
		
	
	$('#qb-generate').click( function(){ 
	
	  
	      if($('#qbpassword').val()!="" && $('#qbpassword').val()===$('#qbrepassword').val() ) { 
	         
	      var uname = $('#qbusername').val(); 
			var url   = "<?php echo base_url().'QuickBooks/your_function/my-quickbooks-wc-file';?>"+uname+'.qwc';
			$("#dowlloadID1").attr("href",url);
			$("#dowlloadID2").attr("href",url);
			
		  $('#qb-connecter-settings').modal('hide');
		  $('.next').removeAttr('disabled');
           $('#qb_desktop').modal('show');
		  $('#qbwwcid').show();
		  $('#qbwwcid1').hide();
		  }
		  
	});
	
	
	
    $('#qb-info').click(function(){
	      
	     $('#qb-connecter-info').modal('show');
		  $('.submit').removeAttr('disabled');
		
	});	
	
	$('.donfile').click(function(){
	
		$('.next').removeAttr('disabled');
		
		
	});	
	
	nmiValidation222.init();
	
 
});

function connectFB(e){
	var base_url = $('#js_base_url').val();
	var newFormFB = jQuery('<form>', {
		'action': `${base_url}firstlogin/freshbooks_user`,
		'method': 'post',
	}).append(jQuery('<input>', {
		'name': 'typetest',
		'value': '1',
		'type': 'hidden'
	}));
	$(document.body).append(newFormFB);
	newFormFB.submit();
}

function surchageCheckChange(e, divId = ''){
	if($('#'+e.id).prop('checked') == true){
		$('#surchargePercentageBox'+divId).show();
	} else {
		$('#surchargePercentageBox'+divId).hide();
	}
}


function fbs_check(typ)
{
    
 
    var tbl=''; var typ_val='';
    if(typ==1)
    {
         $('#frb_div').css('display','block');
         $('#fbs_btn').val('Save');
          tbl = '<p><b><span id="ms_box">If you are using FreshBooks New Account</span></b><span ><a href="JavaScript:Void(0)" onclick="fbs_check(2);"> Click Here</a></span></p>';
           $('#fdiv').html(tbl);
           typ_val=2;
             $('#typetest').val(typ_val);
    }
    
      if(typ==2)
    {
         
        $('#frb_div').css('display','none');
       $('#fbs_btn').val('Connect');
         tbl = '<p><b><span id="ms_box">If you are using FreshBooks Classic Account</span></b><span ><a href="JavaScript:Void(0)" onclick="fbs_check(1);"  >Click Here</a></span></p>';
          $('#fdiv').html(tbl);  
          typ_val=1;
              $('#typetest').val(typ_val);
    }
    
    
   
                             
              
    
}







 
// Change progress bar action
setProgress = function(currstep){
	var percent = parseFloat(100 / widget.length) * currstep;
	percent = percent.toFixed();
	if(percent==25)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                  
                        .attr('aria-valuenow', '25')
                        .removeClass('progress-bar-warning progress-bar-info progress-bar-primary progress-bar-danger')
                        .addClass('progress-bar-success');
   if(percent==50)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                     .attr('aria-valuenow', '50')
                        .removeClass('progress-bar-danger progress-bar-info progress-bar-primary progress-bar-warning')
                        .addClass('progress-bar-success');		
  if(percent==75)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                     .attr('aria-valuenow', '75')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-primary progress-bar-info')
                        .addClass('progress-bar-success');		
if(percent==100)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                   .attr('aria-valuenow', '100')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-info progress-bar-primary')
                        .addClass('progress-bar-success');								

						
}
 
// Hide buttons according to the current step
hideButtons = function(current){
	var limit = parseInt(widget.length); 
 
	$(".action").hide();
 	$('#skipbtn').hide();
	if(current < limit) btnnext.show();
	if(current > 1) btnback.show();
	if (current == 3) { 
		btnnext.hide(); 
		btnsubmit.show();
		btnsubmit.prop("disabled", true);
			$('#skipbtn').show();
		
	}
	if (current == 4) { 
		btnback.hide();
		
	}
		if(current == limit){
		    	
						$("#skipbtn").css("position","relative").html("Skip");
					$("#skipbtn").css("left","-13px").html("Skip");
					btnback.hide();
					btnnext.show();
		}
}
var nmiValidation222 = function() {  
    
    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#Qbwc_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    
					qbcompany_name:{
					    	  required: true,
                        minlength: 3
					    
					},
				    qbpassword: {
							  required: true,
                        minlength: 5
						},
				
					qbrepassword:{
					    
					       required: true,
                         equalTo: '#qbpassword'
					},
                    	
					
                },
               
            });
					
		
						

        }
    };
}();



</script>

<?php 
 
				if(isset($gt_result))
				{
					foreach($gt_result as $gt)
					{
						
		         	 $redirect_uri = $gt['oauth_redirect_uri']; 
						
					}
					
				}


?>
<script
      type="text/javascript"
      src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere-1.3.3.js">
 </script>

 <script type="text/javascript">
 
	var redirectUrl = "<?php echo $redirect_uri ?>";

     intuit.ipp.anywhere.setup({
             grantUrl:  redirectUrl,
             datasources: {
                  quickbooks : true,
                  payments : true
            },
             paymentOptions:{
                   intuitReferred : true
            }
     });
	 
	 

 </script>

<div id="qb" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Connect to QuickBooks</h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             	
                <?php

  if(isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])){
    echo "<h3>Retrieve OAuth 2 Tokens from Sessions:</h3>";
    $tokens = array(
       'access_token' => $_SESSION['access_token'],
       'refresh_token' => $_SESSION['refresh_token']
    );
    var_dump($tokens);
    echo "<br /> <a href='" .$refreshTokenPage . "'>
          Refresh Token
    </a> <br />";
    echo "<br /> <a href='" .$refreshTokenPage . "?deleteSession=true'>
          Clean Session
    </a> <br />";
  }else{
    echo "<h3>Please Complete the \"Connect to QuickBooks\" Authorization Process.</h3>";
    echo '
    <div> Click on the button below to start "Connect to QuickBooks"</div>';
    echo "<br /> <ipp:connectToIntuit></ipp:connectToIntuit><br />";
  }

 ?>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="freshbooks" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">FreshBooks</h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="freshbooks" class="form form-horizontal" action="<?php echo base_url(); ?>firstlogin/freshbooks_user">
			  <div id="fdiv" >
			      <p><b><span id="ms_box">If you are using FreshBooks Classic Account</span></b><span ><a href="JavaScript:Void(0);" >Click Here</a></span></p> 
			  </div>
			  <input type="hidden" id="typetest" name="typetest" value="1" />
		        <div id="frb_div" style="display:none" class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Sub Domain</label>
							<div class="col-md-8">
								<input type="text" id="subdomain"  name="subdomain" class="form-control"  value="" placeholder="Enter Your Sub Domain"></div>
						</div>
						
						
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Secret Key</label>
							<div class="col-md-8">
								<input type="text" id="secretKey"  name="secretKey" class="form-control"  value="" placeholder="Enter Your Secret Key"></div>
						</div>
						
                         
		      
             
                 </div> 
                         <div class="form-group">
        					<div class="col-md-4 pull-right">
        			     	<input type="submit" id="fbs_btn" class="btn btn-sm btn-success" value="Connect" />
        			     	
                            <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
        		         </div>
        		      </div>  
        		  
        		
        		      
	    </form>		
                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>

</div>








<div id="qb_desktop" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" data-toggle="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">QuickBooks Setup Guide</h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			
             <?php if(isset($login_info['fileID']) && !empty($login_info['fileID']) ){    ?>
					
						
					
					
                
					
					
					 <form id="" method="post"  class="form-horizontal" >
                     	<div class="form-group">
                            <label class="col-md-1"></label>
                            <div class="col-md-11">
                                	<h4>Almost done! Please read all instructions before proceeding. Let's sync all our QuickBooks data (Clients, Products & Invoices).</h4>
                            </div>
                        </div>
                     	
                     	<div class="form-group">
                            <label class="col-md-2 control-label txt">1.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Make sure Web Connector is installed on the same computer as QuickBooks and you are logged in with the Admin account.</p>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-2 control-label txt">2.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Open Web Connector here: Start->QuickBooks->Web Connector</p>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-md-2 control-label txt">3.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Select "Add an Application" from the "File" menu and select the .qwc file that was just downloaded </p>
                            </div>
                        </div>
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label txt">4.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Enter the "Web Connector Password" from Step 4</p>
                            </div>
                        </div>
						
						
						
							<div class="form-group">
                            <label class="col-md-2 control-label txt">5.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Check the box under "Auto-Run" and set the "Every_Min" field to "10"</p>
                            </div>
                        </div>
					
					
						<div class="form-group">
                            <label class="col-md-2 control-label txt">6.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Click "Select All" and then "Update Selected" button to begin syncing data from QuickBooks</p>
                            </div>
                        </div>
                        
                        	<div class="form-group">
                            <label class="col-md-2 control-label txt">7.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">Minimize Web Connector application. Allow up to 30 minutes to sync all data</p>
                            </div>
                        </div>
                        
						
					 
					
			   </form>	
							
				         <div class="form-group text-center">
				<a href="<?php echo base_url(); ?>QuickBooks/QB_firstlogin" class="btn btn-sm btn-success bttn">I Understand, Let's Get Started!</a>
				<br>
                <br>
            </div>    	
						
					  </div>
					
				
					
					<?php }else{  ?>
					
					
					
					  <div id="qbwwcid" style="display:none;" >
                
				
						
									 <form id=""   class="form-horizontal" >
            						<div class="form-group">
                                        <label class="col-md-1"></label>
                                        <div class="col-md-11">
                                            	<h4>Almost done! Please read all instructions before proceeding. Let's sync all our QuickBooks data (Clients, Products & Invoices).</h4>
                                        </div>
                                    </div>
								
									<div class="form-group">
										<label class="col-md-2 control-label txt">1.</label>
										<div class="col-md-10">
											<p class="form-control-static para">Make sure Web Connector is installed on the same computer as QuickBooks and you are logged in with the Admin account.</p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label txt">2.</label>
										<div class="col-md-10">
											<p class="form-control-static">Open Web Connector here: Start->QuickBooks->Web Connector</p>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label txt">3.</label>
										<div class="col-md-10">
											<p class="form-control-static">Select "Add an Application" from the "File" menu and select the .qwc file that was just downloaded</p>
										</div>
									</div>
									
									
										<div class="form-group">
										<label class="col-md-2 control-label txt">4.</label>
										<div class="col-md-10">
											<p class="form-control-static">Enter the "Web Connector Password" from Step 4</p>
										</div>
									</div>
									
									
        							<div class="form-group">
                                    <label class="col-md-2 control-label txt">5.</label>
                                    <div class="col-md-10">
                                        <p class="form-control-static">Check the box under "Auto-Run" and set the "Every_Min" field to "10"</p>
                                    </div>
                                </div>
        					
        					
        						<div class="form-group">
                                    <label class="col-md-2 control-label txt">6.</label>
                                    <div class="col-md-10">
                                        <p class="form-control-static">Click "Select All" and then "Update Selected" button to begin syncing data from QuickBooks</p>
                                    </div>
                                </div>
                                
                                	<div class="form-group">
                                    <label class="col-md-2 control-label txt">7.</label>
                                    <div class="col-md-10">
                                        <p class="form-control-static">Minimize Web Connector application. Allow up to 30 minutes to sync all data</p>
                                    </div>
                                </div>
									
								 
								
								   </form>	
                        <div class="form-group text-center">
			           	<a href="<?php echo base_url(); ?>QuickBooks/QB_firstlogin" class="btn btn-sm btn-success bttn">I Understand, Let's Get Started!</a>
				<br>
                <br>
            </div>                      
						
					  </div>
					
					 <div id="qbwwcid1"  >
						<h4>You can create a webconnector file to get QuickBooks data.</h4>
					
						  <a href="#qb-connecter-settings" class="btn btn-sm btn-info" id="dddddc"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Setup QuickBooks</a>
						<br>
						<br>
						<br>
					</div>
					
					
					<?php } ?>	
              
            </div>
			
			</div>
            <!-- END Modal Body -->
        </div>

		<?php if(!isset($gateway_data)){ ?>
			<script>
				$("#gateway_opt").val("1");
				$("#nmi_cr_status").prop( "checked", true );
				$("#nmi_div").show();
			</script>
		<?php } ?>