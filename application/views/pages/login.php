 
<!-- Login Alternative Row -->
<?php
	$this->load->view('alert');
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- Login Container -->
			<div id="login-container">
				<!-- Login Title -->
				<div class="push-top-bottom text-center">
				<div id="logo"><img src="<?php echo ($logo)?$logo:base_url(IMAGES).'/logo_neww.png'; ?>" alt="avatar">
				</div>
				</div>
			
				
				<!-- END Login Title -->

				<!-- Login Block -->
				<div class="block push-bit">
				<div class="msg_data ">
					<?php 
						$message = $this->session->flashdata('message');
						echo $message;

						if ($error = $this->session->flashdata('error')) { ?>
							<div class="alert alert-danger">
								<button data-dismiss="alert" class="close" type="button">×</button>
								<strong>Error:</strong> <?= $error ?>
							</div>
						<?php }
					?>
				</div>

					<!-- Login Form -->
					<form action="<?php echo base_url('login/user_login'); ?>" method="post" id="form-login" class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="login-email" name="login-email"  class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" autocomplete="off" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 col-sm-12 text-right">
								<a href="javascript:void(0)" id="link-reminder-login"><small>Forgot Password?</small></a> 
							
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 col-sm-12 text-center">
								<button type="submit" class="btn login_click_btn redbutton"> Login to Dashboard</button>
							</div>
						</div>
						<?php echo $this->czsecurity->appendFormCsrfToken(); ?>
					</form>
					<!-- END Login Form -->

					<!-- Reminder Form -->
					<form action="<?php echo base_url('login/recover_password'); ?>#reminder" method="post" id="form-reminder" class="form-horizontal display-none">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-center">
								<button type="submit" class="btn login_click_btn redbutton"> Reset Password</button>
							</div>
						</div>
						<?php echo $this->czsecurity->appendFormCsrfToken(); ?>
					</form>
					<!-- END Reminder Form -->

					<!-- Register Form -->
					<form action="<?php echo base_url('login/user_register'); ?>#register" method="post" id="form-register" class="form-horizontal display-none">
						<div class="form-group">
							<div class="col-xs-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-user"></i></span>
									<input type="text" id="register-firstname" name="register-firstname" class="form-control input-lg" placeholder="Firstname">
								</div>
							</div>
							<div class="col-xs-6">
								<input type="text" id="register-lastname" name="register-lastname" class="form-control input-lg" placeholder="Lastname">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="register-email" name="register-email" class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" id="register-password" name="register-password" class="form-control input-lg" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" id="register-password-verify" name="register-password-verify" class="form-control input-lg" placeholder="Verify Password">
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-xs-6">
								
							<select id="reseller" class="form-control " name="reseller" Placeholder="Select Reseller">
								<option>Select Reseller</option>
							 <?php foreach($reseller_list as $reseller){  ?>
							
								   <option value="<?php echo $reseller['resellerID']; ?>"> <?php echo $reseller['resellerfirstName']; ?> </option>
								   
								<?php } ?>  
							</select>
								
							</div>
							<div class="col-xs-6">
								<select id="Plans" class="form-control " name="Plans" >
								<option value=" ">Select Plans..</option>
								
								</select>
							</div>
						</div>
						
						<div class="form-group form-actions">
							<div class="col-xs-6">
							</div>
							<div class="col-xs-6 text-right">
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Register Account</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<small>Do you have an account?</small> <a href="javascript:void(0)" id="link-register"><small>Login</small></a>
							</div>
						</div>
						<?php echo $this->czsecurity->appendFormCsrfToken(); ?>
					</form>
					<!-- END Register Form -->
				</div>
				<!-- END Login Block -->
			</div>
			<!-- END Login Container -->
			<!-- Footer -->
			<div class="footer">
					<footer class="text-muted push-top-bottom">
						
					<?php echo $merchantHelpText; ?>
					</footer>
					<!-- END Footer -->
			</div>
		</div>
	</div>
</div>
<!-- END Login Alternative Row -->


<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>
<script>
$(document).ready(function(){base_url('

$('#reseller').change(function(){
    var reID = $(this).val();
    $("#Plans > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('login/get_reseller_plan_id'); ?>",
        data: {reID: reID},
        dataType: 'json',
        success:function(data){
			
			var s = $('#Plans');
			 
			   for(var val in  data) {
         
              $("<option />", {value: data[val]['plan_id'], text: data[val]['plan_name'] }).appendTo(s);
           }
			
        }
    });
});

});
</script>
<style>
.error{color:red; }
.footer {
    position: relative;
    top: 100px;
    text-align: center;
}
.redbutton {
    background: #FF4612 !important;
    color: #FFF !important;
    outline: 0 !important;
    height: 32px;
    width: 255px;
    border-radius: 3px;
}
body {
    background: #F0EFEF !important;
}
.push-bit {
    padding: 50px 20px 30px;
}

#logo {
    max-width: 200px;
    overflow: hidden;
    margin: 0 auto;
}
#logo img{
    width:100%;
    height:100%;
}
</style>
<script>
    $(function(){
    FormsWizard.init(); });
	$(function(){
        setTimeout(function() {
        }, 2000); // <-- time in milliseconds
    });
</script>
