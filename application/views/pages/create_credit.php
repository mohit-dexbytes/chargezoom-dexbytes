<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data ">
			    <?php echo $this->session->flashdata('message');   ?>
		    </div>
    
    
    
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
        <div class="col-md-12">
		<div class="block">
		 <div class="block-title">
             <h2><strong><?php if(isset($subs)){ echo "Edit"; }else{ echo "Create"; } ?> Credit </strong>  </h2>
        </div>
		   
		    <form id="form-validation" action="<?php echo base_url('MerchantUser/create_credit'); ?>" method="post" class="form-horizontal form-bordered">
                
				
				
                 <fieldset>  
				 <div class="col-md-12">
				   <div class="col-md-4 form-group">
						<label class="control-label" for="customerID">Customer</label>
						<div >
						
							<select id="customerID" name="customerID"  class="form-control select-chosen">
								<option  >Select Customer</option>
								<?php   foreach($customers as $customer){       ?>
								 <option value="<?php echo $customer['ListID']; ?>"  
								 <?php if(isset($subs) &&  $subs['customerListID']==$customer['ListID']){  echo "Selected" ;} ?> ><?php echo $customer['FullName']; ?></option>
								<?php }    ?>
								   
							</select>
						
						</div>
                   </div>
			      
                    
                   <div class="col-md-4 form-group">   
                       
                       
						<label class="control-label" for="customerID">Description</label>
                           <div>  
                              <textarea name="cr_description" id="cr_description" class="form-control"></textarea>
                         </div>
                     </div>
                             
                  
                  
					 
				  <div class="col-md-4  form-group">
				   <div class="">
						 
					 <label class="control-label ">Credit Note</label>
					
					  <textarea name="cr_note" id="cr_note" class="form-control"></textarea>
					
				  </div>
				</div> 		
			
				
				</div>	 
				
			
					 
			</fieldset>
                    
					<fieldset><legend> Plan Details</legend>	
					<div class="form-group form-actions">
					<div class="col-sm-3">
					    <div class="form-group"><label class="control-label">Products & Services</label> </div>
			        	</div> 
					
					<div class="col-sm-3"><div class="form-group"><label class="control-label">Description </label></div> </div>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Price </label> </div></div>
					<div class="col-sm-2"><div class="form-group text-center"><label class="control-label">Quantity </label></div> </div>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Total</label></div> </div>
					
					</div>
										
						  <div id="item_fields">
						 <?php 
						       if(isset($items) && !empty($items)){
							     foreach($items as $k=>$item ){
									$rate = $item['itemRate'];
									$qnty = $item['itemQuantity'];
								
									$total_amt = ( $rate * $qnty ) ;
								 
								 ?>
								  <div class="form-group removeclass<?php echo $k+1; ?>">
								<div class="col-sm-3 nopadding"><div class="form-group">
								  <select class="form-control"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="productID<?php echo $k+1; ?>" name="productID[]">
								<option value="">Select Product & Service</option>
								
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>" <?php if($plan['ListID']==$item['itemListID']){ echo "selected";} ?>  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div>
								    <div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" placeholder="Description "></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control float" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo number_format($item['itemRate'],2,'.',''); ?>" onblur="set_unit_val('<?php echo $k+1; ?>');" placeholder="Price"></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group "><input type="text" maxlength="4" onkeypress="return isNumberKey(event)" class="form-control quantity_input_box"  onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity<?php echo $k+1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>" placeholder="Quantity"></div></div>
								   
								   
								   
								   <div class="col-sm-2 nopadding"><div class="form-group" ><div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k+1; ?>" name="total[]" value="<?php echo sprintf('%0.2f', $total_amt); ?>" placeholder="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div></div>

						<?php		 }								 
							   }else{  
						 ?>
						   <div class="form-group removeclass1">
							<div class="col-sm-3 nopadding"><div class="form-group ">
								  <select class="form-control"  onchange="select_plan_val('1');"  id="productID1" name="productID[]">
								<option value="">Select Product & Service</option>		
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>"  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div>
								   
								   <div class="col-sm-3 nopadding">
								     <div class="form-group"> <input type="text" class="form-control" id="description1" name="description[]" value="" placeholder="Description ">
									 </div>
								   </div>
								   <div class="col-sm-2 nopadding"><div class="form-group" >
								   <input type="text" class="form-control force-numeric" id="unit_rate1" name="unit_rate[]" value="" onblur="set_unit_val('1');" placeholder="Price"></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group"> <input type="text"  maxlength="4"  onkeypress="return isNumberKey(event)" class="form-control quantity_input_box" onblur="set_qty_val('1');" id="quantity1" name="quantity[]" value="" placeholder="Qty"></div></div>
								   
								   
								   
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total1" name="total[]" value="" placeholder="Total"> 
								  </div></div> </div> <div class="clear"></div>
							   
							</div>   <?php } ?>
								  
						 </div>
						
				 
				       <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label "></label>						  
						 <div class="group-btn">
						 
							<button class="btn btn-sm btn-success" type="button"  onclick="item_fields();"> Add More</button>
						 
							<label class="btn btn-sm pull-right remove-hover"><strong>Total: $<span id="grand_total"><?php echo '0.00'; ?></span> </strong> </label>
						  </div>
				        </div>
						</div>
						
					</div>	
					
					
			 
                     <input type="hidden" name="planID" value="<?php if(isset($subs)){  echo $subs['planID']; }?>" />   
                 <div class="form-group pull-right">
					<div class="col-md-12">
					   
				
					  <button type="submit" class="submit btn btn-sm btn-success">Save</button>	
						 <a href="<?php echo base_url(); ?>Payments/credit" class=" btn btn-sm btn-primary1">Cancel</a>		
					</div>
				    </div>		
			 

						</fieldset>
                    
			
               
				
                  
            </form>
          </div>      
        </div>
            
   


    <script>
	
	
        $(function(){

			
            nmiValidation.init();
		  
	      var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
			 $("#invoice_date").datepicker({ 
              format: 'yyyy-mm-dd',
              autoclose: true
            });  
			
			$('#sub_start_date').datepicker({
			format: 'yyyy-mm-dd',
			startDate:today,
			autoclose: true
			});
			$('#subsamount').blur(function(){
				
				var dur = $('#duration_list').val();
              var subsamount = $('#subsamount').val();
			  var tot_amount = subsamount*dur ;
			  $('#total_amount').val(tot_amount.toFixed(2));
			  $('#total_invoice').val(dur);
				
			});
			
			

	});
	


  <?php  if(isset($k) && $k >=1){  ?>

     var room ='<?php echo $k+1; ?>';
     <?php }else{ ?>
  var room=1;
  <?php } ?>
     function item_fields()
	{ 
	   
       var type='group';
                
          $.ajax({
           
          url:'<?php echo base_url(); ?>MerchantUser/get_plan_data_item',
          type:"POST",
          data:{type:type},
          success:function(data){
          var plan_data = $.parseJSON(data); 
        
        
        
		room++;
		
		var plan_html ='<option val="">Select Product or Service</option>';
		for(var val in  plan_data) 
        { 
           plan_html+='<option value="'+ plan_data[val]['ListID']+'">'+plan_data[val]['Name']+'</option>'; 
        }
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="Description "></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text"  onkeypress="return isNumberKeys(event)" onblur="set_unit_val('+room+');"  class="form-control float" id="unit_rate'+room+'" name="unit_rate[]" value=""  placeholder="Price"></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text"  maxlength="4"  onkeypress="return isNumberKey(event)" class="form-control quantity_input_box"  id="quantity'+room+'" name="quantity[]" value="" onblur="set_qty_val('+room+');" placeholder="Qty"></div></div>  	    <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+room+'" name="total[]" value="" placeholder="Total"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest);
        
       }
          
            }); 
  }
  
  
   function remove_education_fields(rid)
   {
	  
	   var rid_val = $('#total'+rid).val();
	   
	   var gr_val  = $('#grand_total').html();
	    if(rid_val){
	   var dif     = parseFloat(gr_val)-parseFloat(rid_val);
	   $('#grand_total').html(dif.toFixed(2));
		}
	   $('.removeclass'+rid).remove();
	   
	    
	   
	   
  }
	

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                     customerID:{
						  required:true,
						 
					 },
					 cr_description: {
						 required:true,
                          minlength: 3
						},
					cr_note:{
					  required:true,
                      minlength: 3
					},
					'productID[]': {
					    required: true,
					    minlength: 1
					},
					'description[]': { 
				     minlength: 1,
				      maxlength: 31
			    	}
                    	
					
                }
               
            });
					
		 
	
						

        }
    };
}();
   
/**********Check the Validation for free trial**********************/

/************End*******************/	   
	
   
   function chk_payment(r_val)  {  
   
     
      if(r_val=='1'){
		  $('#set_pay_data').show();
	  }else{
		$('#set_pay_data').hide();
      }	  

   }   
 
   
	function select_plan_val(rid){
		
		
		
		var itemID = $('#productID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>SettingSubscription/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['FullName']);
             $('#unit_rate'+rid).val(item_data['SalesPrice']);
		   $('#quantity'+rid).val(1);
               
            $('#total'+rid).val(roundN(($('#quantity'+rid).val()*$('#unit_rate'+rid).val()),2));
            
            var grand_total=0;
	    	$( ".total_val" ).each(function(){
            var tval = $(this).val() != '' ? $(this).val() : 0;
            grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
	    	$('#grand_total').html(format22(grand_total));
             
			}	
		});
		
	}
	
	function set_unit_val(rid){
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
		$('#total'+rid).val(total.toFixed(2));
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
			
              var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format22(grand_total));
	}
	
	function set_qty_val(rid){
       
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val(); 
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax;   
		$('#total'+rid).val(total.toFixed(2));
	
		var grand_total=0;
		$( ".total_val" ).each(function(){ 
				var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(grand_total.toFixed(2));
		
	}	
	
	 
$(document).ready(function() {

        $('.force-numeric').keydown(function(e)
        {
            
   var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
		
		
		$('input.float').bind('keypress', function() {
			  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
			});
  

});


	
	 function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
	  
	  
function IntegerAndDecimal(e,obj,isDecimal)
{
    if ([e.keyCode||e.which]==8) //this is to allow backspace
    return true;

    if ([e.keyCode||e.which]==46) //this is to allow decimal point
    {
      if(isDecimal=='true')
      {
        var val = obj.value;
        if(val.indexOf(".") > -1)
        {
            e.returnValue = false;
            return false;
        }
        return true;
      }
      else
      {
        e.returnValue = false;
        return false;
      }
    }

    if ([e.keyCode||e.which] < 48 || [e.keyCode||e.which] > 57)
    e.preventDefault? e.preventDefault() : e.returnValue = false; 
}



function isNumberKeys(evt)
           {
               var charCode = (evt.which) ? evt.which : event.keyCode
 
               if (charCode == 46)
               {
                   var inputValue = $("#inputfield").val()
                   if (inputValue.indexOf('.') < 1)
                   {
                       return true;
                   }
                   return false;
               }
               if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
               {
                   return false;
               }
               return true;
           }

function format22(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

} 

	
	</script>
	
	<style>

	input[type="checkbox"] {
    margin: 12px 0 0;
    margin-top: 1px \9;
    margin-left: 45;
    margin-left: 10px;
    line-height: normal;
}
	
	</style>
</div>
</div>