<style>
    .cust-lab{
        text-align:justify !important;
    }
    .cust-field{
        width:65% !important;
    }
    
</style>

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <?php echo $this->session->flashdata('message');   ?> 
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong> Billing</strong></h2>
            
             <div class="block-options pull-right">
                   
                       <a class="btn btn-sm  btn-success"  href="#credit_card_info" onclick="get_merchant_card('<?php echo $login_info['merchID']; ?>')"  data-toggle="modal">Credit Card Info</a>
                     
                </div>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">Invoice</th>
                    <th class="hidden-xs text-right">Date</th>
                     <th class="text-right hidden-xs">Amount</th>
                    <th class="text-right">Status</th>
                
                    
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices)){

						foreach($invoices as $invoice){
						     
						    
			   ?>
			
				<tr>
					
					<td class="text-left"><strong><a target="_blank" href="<?php echo base_url(); ?>MerchantNMI/billing_details/<?php echo $invoice['invoice']; ?>"><?php  echo $invoice['invoice']; ?></a></strong></td>
				
                    <td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
					

					   <?php if($invoice['AppliedAmount']!=""){ ?>
                            <td class="hidden-xs text-right">$<?php echo number_format((int)$invoice['AppliedAmount'],2); ?></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right">$<?php  echo (number_format($invoice['BalanceRemaining'],2)); ?></td>   
						   <?php } ?>
				
					<td class="text-right">
					<?php  if($invoice['status']=='Completed'){ echo 'Paid'; }else{ echo 'Unpaid'; } ?></td>
					
				
				
				</tr>
				
				<?php 
				  }
			   }	
				else { echo'<tr><td colspan="4"> No Records Found </td></tr>'; }  
				?>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->


<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 

<!-- Load and execute javascript code used only in this page -->
 <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"> </script>
 <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
 <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){   <?php if(!empty($invoices)){  ?>
         Pagination_view31.init(); 
                                             <?php } ?>
                                                });</script>
    <script>
	
 var gtype ='';
	  function stripeResponseHandler_res(status, response) {

               
                if (response.error) {
                    // Re-enable the submit button
                    $('#submit_btn').removeAttr("disabled");
                    // Show the errors on the form

                    $('#payment_error').text(response.error.message);

                } else {
                    var form = $("#thest_pay");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

                }
            }
			
			
$(document).ready(function(){
    
    $('#btn_mask').click(function(){
    
    
  $('#m_card_id').hide();  
  $('#card_number').removeAttr('disabled');
  $('#card_number').attr('type','text');
});
    
    
    
    
 $('#merchantCardID').change(function(){
			
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			  
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>MerchantNMI/get_card_data",
				data : {'merchantCardID':cardlID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					    if(data['status']=='success'){
						 var form = $("#thest_pay");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['CardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['CardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);

									// Prevent the form from submitting with the default action
									
					        }	   
					
				}
				
				
			});
		  }
		
	});		
     

    
});



  


function set_url_billing(){  
 
           var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>MerchantNMI/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>MerchantNMI/pay_trace_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>MerchantNMI/pay_auth_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>MerchantNMI/pay_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>MerchantNMI/pay_paypal_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>MerchantNMI/pay_stripe_invoice";
							   
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }
	
	
var Pagination_view = function() 
{

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

function set_invoice_process_billing_id(id, mid)
{
	
	     $('#invoiceProcessID').val(id);
	     
	     
		 
		if(mid!=""){
			$('#merchantCardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>MerchantNMI/check_vault",
				data : {'merchantID':mid},
				success : function(response){
					
					     data=$.parseJSON(response);

					     if(data['status']=='success'){
						
                              var s=$('#merchantCardID');
							
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['merchantCardID'], text: card1[val]['merchantFriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}	      

function  get_merchant_card(mid)
{ 
    	if(mid!=""){
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>MerchantNMI/get_merchant_card_data",
				data : {'merchantID':mid},
				success : function(response){
					
					     data=$.parseJSON(response);
					     if(data['status']=='success')
					     {
						
                            
							       $('#mr_cradID').val(data['card']['merchantCardID']);
							    $('#card_number').val(data['card']['MerchantCard']);
							   $('#m_card').html(data['card']['MerchantCard']);
								document.getElementById("cvv").value =data['card']['CardCVV'];
					              $('#m_card_id').show();  
                                  $('#card_number').attr('disabled','disabled');
                                  $('#card_number').attr('type','hidden');	
								$('select[name="expiry"]').find('option[value="'+data['card']['CardMonth']+'"]').attr("selected",true);
								$('select[name="expiry_year"]').find('option[value="'+data['card']['CardYear']+'"]').attr("selected",true);
								$('#address1').val(data['card']['Billing_Addr1']);
							    $('#address2').val(data['card']['Billing_Addr2']);
							    $('#city').val(data['card']['Billing_City']);
							    $('#state').val(data['card']['Billing_State']);
							    $('select[name="country"]').find('option[value="'+data['card']['Billing_Country']+'"]').attr("selected",true);
							 
							    $('#zipcode').val(data['card']['Billing_Zipcode']);
							  
						
							 
					   }else{
					       $('#m_card_id').hide();  
                                  $('#card_number').removeAttr('disabled');
                                  $('#card_number').attr('type','text');	 
					   }	   
					
				}
				
				
			});
			
		}	
}
</script>





</div>

<div id="invoice_process_billing" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>MerchantNMI/pay_invoice' class="form-horizontal" autocomplete="off" >
                     
                 
					<p>Do you really want to process this invoice before due date?</p> 
					
									<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_url_billing();"  class="form-control">
                                                        <option value="" >Select Gateway</option>
														<?php if(isset($gateway_datas) && !empty($gateway_datas) ){
																foreach($gateway_datas as $gateway_data){
																?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
																<?php } } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
												
                                                    <select id="merchantCardID" name="merchantCardID"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
														
                                                </div>
                                            </div>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-warning" value="Process Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="credit_card_info" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Credit Card Info</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="bill_credit_card" method="post" action='
                 <?php echo base_url('MerchantNMI/addcredit_card_info'); ?>' class="form-horizontal">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                                 <div id="m_card_id"><span id="m_card"></span> <input type='button' id="btn_mask" class="btn btn-default btn-sm" value="Edit Card" /></div>
                                <input type="hidden" disabled  id="card_number" name="card_number" class="form-control" placeholder="Credit Card Number">
                               
                            
                        </div>
                    </div>
									
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Expiry Month</label>
                                                <div class="col-md-3">
												
                                                    <select id="expiry" name="expiry" class="form-control">
                                                        <option value="1">JAN</option>
                                                        <option value="2">FEB</option>
                                                        <option value="3">MAR</option>
                                                        <option value="4">APR</option>
                                                        <option value="5">MAY</option>
													    <option value="6">JUN</option>
                                                        <option value="7">JUL</option>
                                                        <option value="8">AUG</option>
                                                        <option value="9">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                </select>
														
                                                </div>
                                                
                                                	<label class="col-md-2 control-label" for="card_list">Expiry Year</label>
                                                <div class="col-md-3">
												
                                                    <select id="expiry_year" name="expiry_year"  class="form-control"><?php 
														$cruy = date('y');
														$dyear = $cruy+25;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php  echo "20".$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                </select>
														
                                                </div>
                                            </div>
					                    
					                   	<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Security Code(CVV) </label>
                                                <div style="width:25% !important;" class="col-md-6">
                                                     <input type="text"  placeholder="CVV" id="cvv" name="cvv" size="40"  class="form-control">
                                                </div>
                                            </div>
                                            
                                             
					                   	<div class="form-group ">
                                              
												<label class="col-md-6 control-label cust-lab" for="card_list">Billing Address</label>
                                                
                                            </div>
                                             
					                   	<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Address 1</label>
                                                <div class="col-md-8">
                                                     <input type="text" id="address1" name="address1" placeholder="Address 1" size="40"  class="form-control">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Address 2</label>
                                                <div class="col-md-8">
                                                     <input type="text" id="address2" name="address2" placeholder="Address 2" size="40"  class="form-control">
                                                </div>
                                            </div>
                                         
                                           <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">City </label>
                                                <div class="col-md-8">                                                  
								<input type="text" id="city" class="form-control" name="city" placeholder="City" value="">			
                                                </div>
                                            </div>
                                          
                                                  
                                        <div class="form-group ">                                              
												<label class="col-md-4 control-label" for="card_list">State </label>
                                                <div class="col-md-8">
									<input type="text" id="state" class="form-control " name="state" placeholder="State" value="">                                                   
                                                </div>
                                            </div>  
                                            
                                           
                                         
                                         <div class="form-group ">
                                              
												<label class="col-md-4 control-label " for="card_list">ZIP Code</label>
                                                <div class="col-md-3 ">
                                                     <input id="zipcode" name="zipcode" type="text"  placeholder="ZIP Code" size="40"  class="form-control">
                                                </div>
                                                
                                               
                                            </div>  
											   
                                         <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Country </label>
                                                <div class="col-md-8">
                                                     <input type="text" id="country" class="form-control" name="country" placeholder="Country"> 
                                              
                                                </div>
                                            </div>   
                       
                                
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="mr_cradID" name="mr_cradID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  class="btn btn-sm btn-success" value="Save"  />
                    <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<script>

    $('#bill_credit_card').validate({  // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'card_number': {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 'expiry_year': {
							  'CCExp': {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
                   		
					 'cvv': {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
					'address1':{
					    required: true,
					    maxlength: 41,
                        validate_addre:true,
					},
                    'address2':{
                        required: true,
					    maxlength: 41,
                        validate_addre:true,
					},
					'country':{
					    required: true,
						  maxlength: 21,
                        validate_addre:true,
					},
					'city':{
					    required: true,
					    maxlength: 21,
                        validate_addre:true,
					},
                    'state':{
                        required: true,
                          maxlength: 21,
                        validate_addre:true,
                    },
					'phone': {
                        required: true,
                         minlength: 10,
                         maxlength: 15,
                         phoneUS:true,
                    },
                    
				    'zipcode': {
                       	minlength:3,
						maxlength:10,
						ProtalURL:true,
						validate_addre:true,
                    },
                    
               		
					'portal_url':{
                        minlength:2,
                        maxlength: 20,
                        ProtalURL:true,
                        "remote" :function(){return Chk_url('portal_url');} 
					}
					
			}
		
    });
   
   
   $.validator.addMethod("phoneUS", function(phone_number, element) {
         
            return phone_number.match(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/);
        }, "Please specify a valid phone number like as (XXX) XX-XXXX");
         
         
         $.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
      

       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/, ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
          
$.validator.addMethod('CCExp', function(value, element, params) {  
	  var minMonth = new Date().getMonth() + 1;
	  var minYear = new Date().getFullYear();
	  var month = parseInt($(params.month).val(), 10);
	  var year = parseInt($(params.year).val(), 10);
	  
	  

	  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');

window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }
     
    $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_states'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){

			var s = $('#state');
			 $('#state').append('<option value="Select State">State</option>');
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			$('#city').append('<option value="Select State">City</option>');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
}); 
     
</script>
