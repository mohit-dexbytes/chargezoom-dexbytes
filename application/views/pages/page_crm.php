<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>CRM & Marketing</strong> </h2>
            <div class="block-options pull-right">
                            <div class="">
                               
							<a href="#create_crm" class="btn btn-sm btn-success"   data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
                           
                            </div> 
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">CRM Name</th>
                 
                    <th class=" text-left hidden-xs">CRM User</th>
                     <th class="hidden-xs text-right">Password</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($companies) && $companies)
				{
					foreach($companies as $company)
					{
				?>
				<tr>
					
				   	<td class="text-left hidden-xs"><?php echo $company['crmName']; ?></td>
					<td class="text-left hidden-xs"><?php echo $company['crmUserName']; ?></td>
					<td class="text-right hidden-xs"><?php echo $company['crmPassword']; ?></td>
					<td>	<a href="#" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <a href="#del_crm" onclick="set_crm_id('<?php  echo $company['crmID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a></td>
				
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#company').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_reseller(id){
	
	  $('#resellerID').val(id);
	
}


function view_company_details(id){
	 
	
	 if(id!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>home/get_company_details',
		  data : {'id':id},
		  success: function(data){
			  
			     $('#data_form_company').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 }


</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 

</div>

<div id="company_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">App Details</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_company"  style="height: 300px; min-height:300px;  overflow: auto; " >
			
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- END Page Content -->
<div id="create_crm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Create New CRM</h2>
               
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->                           
            <div class="modal-body">
                
                 <form id="crm_form" method="post" action="<?php echo base_url('SettingConfig/create_crm'); ?>" class="form-horizontal card_form" >
                     
                       <input type="hidden" id="crmID" name="crmID"  class="form-control" value="" />
					
									<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">CRM Name</label>
                                                <div class="col-md-6">
                                                     <input type="text" id="crmName" name="crmName"   class="form-control" value="" />
                                                      
													
                                                </div>
                                            </div>	
                                            	<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">CRM User</label>
                                                <div class="col-md-6">
                                                     <input type="text" id="crmUserName" name="crmUserName"   class="form-control" value="" />
                                                      
													
                                                </div>
                                            </div>
											<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">CRM Password</label>
                                                <div class="col-md-6">
                                                     <input type="text" id="crmPassword" name="crmPassword"  class="form-control" value="" />
                                                      
													
                                                </div>
                                            </div>
									
                                                
                                       
                      
				
                    
        		
                    <div class="pull-right">
        			 <input type="submit" id="qbd_process" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<script>
  
$(document).ready(function(){


    $('#create_crm').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'crmName': {
                        required: true,
                         minlength: 2
                    },
                   'crmUserName': {
                        required: true,
                         minlength: 2
                    },
					
				'crmPassword': {
                        required: true,
                         minlength: 3
                    },
					 
			},
    });
    
    
    
    $('#form_edit').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'crmName': {
                        required: true,
                         minlength: 3
                    },
                   
				
			
			},
    });
    
    $('#btn_qbd').click(function(){
     
       var forid = $(this).closest("form").attr('id');
         
         if(forid=='form_edit')
       var formdata=  $('#form_edit').serialize();
       else
        var formdata=  $('#form_new').serialize();
   
            $.ajax({
           beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },      
                
        type: "POST",
        url: "<?php echo base_url('SettingCongig/create_crm'); ?>",
        data: formdata,
        dataType: 'json',
        success:function(data){
        
			if(data.status="Success" && data.file_name!='')
			{ 
			    var uri ='<?php echo  base_url().'uploads/'; ?>'+data.file_name;
		    	downloadURI(uri,data.file_name);
		     
			}
			setInterval(location.href = "<?php echo base_url('home/setup_crm'); ?>" ,1000);
	
        }
    });
    });
    
   
    
    
    $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QuickBooks/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QuickBooks/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	

	
		
});	
    
</script>