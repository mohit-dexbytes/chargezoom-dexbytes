<!-- Page content -->
<?php
    $this->load->view('alert');
?>

<div id="page-content">
    <span id="flashdata"> 
        <?php echo $this->session->flashdata('message');   ?>
    </span>
    <legend class="leg">Sync Log</legend>
    <!-- All Orders Block -->
    <div class="block-main full" style="position: relative;">
        
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left hidden">Sort Date</th>
                    <th class="text-left hidden-xs">Time</th>
                    <th class="text-left">ID</th>
                    <th class="hidden-xs hidden-sm text-right">Message</th>
                    <th class="hidden-xs hidden-sm text-right">Status</th>
                 
                </tr>
            </thead>
            <tbody>
            
                <?php 
                if(isset($alls) && $alls)
                {
                    foreach($alls as $k=>$all)
                    {
                      
                      $actionType = $all['actionType'];
                      $identIDShow = $all['identIDShow'];
                      
                ?>
                <tr>
                    <td class="text-right hidden">
                        <?php
                            echo $all['enqueue_datetime'];
                        ?>
                    </td>
                    
                    <td class="text-left ">
                      <?php
                          $date = $all['enqueue_datetime'];
                          if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                              $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                              $date = getTimeBySelectedTimezone($timezone);
                          } 
                          echo date('M d, Y h:i A', strtotime($date));
                      ?>
                    </td>

        
          
                    <td class="text-left ">
                      <?php if($all['qbSyncID'] == null || $all['qbSyncID'] == ''){
                          echo"#".$all['ident'];
                      }else{
                        echo"#".$all['qbSyncID'];
                      }   ?>
                        
                    </td>
          
            
          
                   <td class="text-right hidden-xs">
                      <?php 
                          
                          $qbdStatusCode = isset($all['qbdStatusCode']) ? $all['qbdStatusCode'] : '';
                          $qbdStatusMessage = isset($all['qbdStatusMessage']) ? $all['qbdStatusMessage'] : '';

                          $msgShow = 'Failed';
                          $oldMsg = $all['msg'];

                          if($all['qb_status']=='q'){  
                            $msgShow = "Not Synced";
                            
                          }else if($all['qb_status']=='s'){ 
                            $msgShow = 'Sync Successfully';
                            
                          }else if($all['qb_status']=='e'){ 
                            $msgShow = 'Failed';

                            if(!empty($all['msg'])){
                              list($qbdStatusCode, $qbdStatusMessage) = explode(':', $all['msg'], 2);
                            }
                          }else{ 
                            $msgShow = 'Failed';
                            
                          }
                        if($all['qbSyncStatus'] == null || $all['qbSyncStatus'] == ''){
                          if($all['ident'] == 1 || $all['ident'] == 0){
                            echo $oldMsg.' '.$msgShow;
                          }else{
                            echo $oldMsg.' '.'<span class="cust_view">'.$identIDShow .'</span> '.$msgShow;
                          }
                          
                        }else{
                          if($all['ident'] == 1 || $all['ident'] == 0){
                            echo $all['qbSyncStatus'].' '.$msgShow;
                          }else{
                            echo $all['qbSyncStatus'].' '.'<span class="cust_view">'.$identIDShow .'</span> '.$msgShow;
                          }
                          
                        }
                      
                      // Show verbose message
                      if($qbdStatusCode > 0 && !empty($qbdStatusMessage))
                      {
                          $severity = '';
                          if($qbdStatusCode != ''){
                            if($qbdStatusCode == 0){
                              $severity = 'alert-success';
                            }
                            if($qbdStatusCode > 0 && $qbdStatusCode < 500){
                              $severity = 'alert-info';
                            }
                            if($qbdStatusCode > 500 && $qbdStatusCode < 1000){
                              $severity = 'alert-warning';
                            }
                            if($qbdStatusCode > 999){
                              $severity = 'alert-danger';
                            }
                          }
                          echo '<div><label style="padding:5px 2px; margin-bottom:0px;" class="alert '.$severity.'">'.$qbdStatusMessage.'</label></div>';
                      }
                      
                    ?> 
                          
                    </td>

                    <td class="text-right hidden-xs"><?php 
                      if($all['qb_status']=='q'){  echo "Not Synced"; }
                             
                      else if($all['qb_status']=='s' ){ echo "Synced"; }
                      else  { ?>
                        Error <br>
                        <input type="button" name="snk" id="snk_btn" class="btn btn-danger btn-sm" value="Resync"  onclick="set_seenk_data('<?php echo $all['ident'];  ?>','<?php echo $actionType;  ?>','<?php echo $all['quickbooks_queue_id'];  ?>');"   />
                        

                       <?php }
                             ?> 
                            
                            
                    </td>
                </tr>
                
                <?php } }else{ echo'<tr>
                <td colspan="5"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
                
            </tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ 
Pagination_view111.init(); });



var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#ecom-orders111').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: true, targets: [4] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script type="text/javascript">
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 3000); //call fade in 2 seconds
 })

function fadeMyDiv() {
}



function set_seenk_data(tid='',  cltype='',logID='')
{

$("#snk_form").remove();
var f = document.createElement("form");
f.setAttribute('method',"post");

f.setAttribute('id',"snk_form");

var j = document.createElement("input"); //input element, text
j.setAttribute('type',"hidden");
j.setAttribute('name',"logID");
j.setAttribute('value', logID);

if(cltype=='Customer' || cltype=='CustomerAdd' || cltype=='CustomerMod'){
  f.setAttribute('action',"<?php echo base_url(); ?>MerchantUser/sync_customer");

  var i = document.createElement("input"); //input element, text
  i.setAttribute('type',"hidden");
  i.setAttribute('name',"cust_id");
  i.setAttribute('value', tid);

} else if(cltype=='Invoice' || cltype=='InvoiceAdd' || cltype=='InvoiceMod'){
  f.setAttribute('action',"<?php echo base_url(); ?>SettingSubscription/sync_invoice");
  var i = document.createElement("input"); //input element, text
  i.setAttribute('type',"hidden");
  i.setAttribute('name',"inv_id");
  i.setAttribute('value', tid);
} else if(cltype=='Product' || cltype=='ItemServiceAdd' || cltype=='ItemServiceMod'){
  f.setAttribute('action',"<?php echo base_url(); ?>MerchantUser/sync_product");
  var i = document.createElement("input"); //input element, text
  i.setAttribute('type',"hidden");
  i.setAttribute('name',"prod_id");
  i.setAttribute('value', tid);
}else{
  f.setAttribute('action',"<?php echo base_url(); ?>MerchantUser/sync_event");

  var i = document.createElement("input"); //input element, text
  i.setAttribute('type',"hidden");
  i.setAttribute('name',"event_id");
  i.setAttribute('value', tid);

}

f.appendChild(i);
f.appendChild(j);

//and some more input elements here
//and dont forget to add a submit button

document.getElementsByTagName('body')[0].appendChild(f);

$('#snk_form').submit();

}
</script>
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>


</div>
<!-- END Page Content -->