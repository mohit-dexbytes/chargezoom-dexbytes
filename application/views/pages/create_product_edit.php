   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>
	<div id="page-content">
	    
	<?php /* ?>     <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <strong><a href="<?php echo base_url(); ?>home/index">Dashboard</a></strong>
        </li>
        <li class="breadcrumb-item"><small><?php if(isset($item_pro)){ echo "Edit Product";}else{ echo "Create New Product"; }?></small></li>
      </ol> <?php */ ?>
	
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	               <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;
					?>
       <div class="block-title">
       <h2><strong><?php if(isset($item_pro)){ echo "Edit Product";}else{ echo "Create New Product"; }?></strong></h2>
       </div>
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form  id="product_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>MerchantUser/edit_product">
			
			 <input type="hidden"  id="productID" name="productID" value="<?php if(isset($item_pro)){echo $item_pro['productID']; } ?>" /> 
			    <div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Name</label>
					<div class="col-md-7">
						<input type="text" id="productName" name="productName" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['productName']; } ?>"  placeholder="Product Name"><?php echo form_error('productName'); ?>
					</div>
				</div>
			   
					
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Full Name </label>
					<div class="col-md-7">
					<input type="text" id="fullName" name="fullName" class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['productFullName']; } ?>"  placeholder="Product Full Name"> <?php echo form_error('fullName'); ?></div>
					</div>
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-type">Type </label>
					<div class="col-md-7">
							<select id="type" class="form-control " name="type">
							<option value="">Select Type</option>
						
						   <option value="Service" <?php  if(isset($item_pro) && strtoupper($item_pro['productType'])==strtoupper('Service') ) echo"Selected" ?> >Service</option>
						   
							</select><?php echo form_error('type'); ?>
						</div>
					</div>
					
			
					
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Product Price </label>
						<div class="col-md-7">
							<input type="text" id="averageCost" name="averageCost"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['productPrice']; } ?>"  placeholder="Product Price" ><?php echo form_error('averageCost'); ?>
						</div>
					</div>
							<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Quantity</label>
						<div class="col-md-7">
							<input type="text" id="pquantity" name="pquantity"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['QuantityOnHand']; } ?>"  placeholder="Quantity" ><?php echo form_error('pquantity'); ?>
						</div>
					</div>
					<div class ="form-group">
					   <label class="col-md-3 control-label" for="example-typeahead">Select Parent</label>
					   <div class="col-md-7">
							<select id="productParent" class="form-control " name="productParent">
							<option value="">Select Parent ..</option>
							<?php foreach($products as $product){  ?>
							   <option value="<?php echo $product['ListID']; ?>" <?php  if(isset($item_pro) && $product['ListID']=$item_pro['parentID'] ) echo"Selected" ?> > <?php echo $product['Name']; ?> </option>
							   
							<?php } ?>  
							
							</select><?php echo form_error('productParent'); ?>
						</div>
					</div>	
				   
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-type">Product Description </label>
					<div class="col-md-7">
					<textarea id="proDescription" name="proDescription" class="form-control"  ><?php if(isset($item_pro)){echo $item_pro['productFullName']; } ?><?php echo form_error('proDescription'); ?> </textarea></div>
					</div>
						
	                <div class="form-group">
					<div class="col-md-4 col-md-offset-9">
					
					
						<button type="submit" class="submit btn btn-sm btn-success">Save</button>	
                      <a href="<?php echo base_url(); ?>home/plan_product"  class="submit btn btn-sm btn-primary1">Cancel</a>		
					</div>
				    </div>	
          	</form>
		
        </div>
     
    </div>
    
<script>

$(document).ready(function(){
 $('#product_service').addClass('active');
 
    $('#product_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'fullName': {
                        required: true,
                         minlength: 2
                    },
                    'company': {
                        required: true,
                       
                    },
					 'type': {
                        required: true,
                       
                    },
					 
					 'productName': {
                        required: true,
                        minlength: 3
                    },
					
					'averageCost':{
					      required:true,
						  number: true,
						  
					},
					'quantityonhand':{
						required:true,
						  digits: true,
					},
					'proDescription':{
						  required: true,
                        minlength: 3
					},
					'productParent':{
                     required: false
                     }
					
			},
    });
    
  
	
    
});	

</script>

</div>


