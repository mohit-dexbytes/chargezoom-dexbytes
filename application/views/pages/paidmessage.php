<!-- Error Container -->
<style>
#error-container h1 {
    font-size: 60px !important;
    color: #ffffff;
    margin-bottom: 40px;
}
#error-container h1 i {
    font-size: 85px !important; 
}
</style>
<div id="error-container">
    <div class="error-options">
        <h3><i class="fa fa-chevron-circle-left text-muted"></i> <a href="<?php echo base_url(); ?>">Go Back</a></h3>
    </div>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
			<h1 class="animation-pulse"><i class="fa fa-exclamation-circle text-warning"></i></h1>
            <h1 class="animation-pulse">This Invoice is already Paid.</h1>
      
        </div>
       </div>
</div>
<!-- END Error Container -->
