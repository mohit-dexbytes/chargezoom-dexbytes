<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
<span class="flashdata"> 
        <?php echo $this->session->flashdata('message');   ?>
        </span>
    <!-- All Orders Block -->
    <legend class="leg">Products & Services</legend>
    <div class="block-main full" style="position: relative;">
        
        <!-- All Orders Title -->
        <div class="addNewFixRight">
            
             <?php if($this->session->userdata('logged_in')){ ?>
                        
                   <a class="btn btn-sm  btn-success"  id="new_service"  href="<?php echo base_url(); ?>MerchantUser/create_product">Add New</a>
                 
            
            <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="page_plan" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr> 
                   
                    <th class= "text-left">Item Name</th>
                    <th class="text-left hidden-xs">Description</th>
                    <th class="text-left hidden-xs">Quantity On Hand</th>
                    <th class="text-left">Type</th>
                    <th class="hidden-xs text-right">Price</th>
                </tr>
            </thead>
            <tbody>
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<div id="plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">View Product</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_product"  style="height: 300px; min-height:300px;  overflow: auto; " >
			
			<tr colspan="2">No data found! </tr>
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
 
 function view_plan_details(planID){
	 
	
	 if(planID!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>home/plan_product_details',
		  data : {'planID':planID},
		  success: function(data){
			  
			     $('#data_form_product').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 }


</script>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

return {
    init: function() {
        / Extend with date sort plugin /
        $.extend($.fn.dataTableExt.oSort, {
       
        } );

        / Initialize Bootstrap Datatables Integration /
        App.datatables();

        / Initialize Datatables /
        $('#page_plan').dataTable({
            
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
                "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('home/plan_product_ajax')?>",
                "type": "POST" ,
                
                "data":function(data) {
                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            },
                
            },
            "order": [[0, 'asc']],
            
                "sPaginationType": "bootstrap",
        "stateSave": false 

        });
        / Add placeholder attribute to the search input /
       $('.dataTables_filter input').attr('placeholder', 'Search');
    }
};
}();




</script>
<script type="text/javascript">
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 2000); //call fade in 10 seconds
 })

function fadeMyDiv() {
   $(".flashdata").fadeOut('slow');
}


function set_seenk_data(cID='', cls='', ced='')
{

   if(cID!='')
   {
    $('#pro_id').val(cID);

   
   document.getElementById('pro_lsid').value='';
   document.getElementById('pro_edid').value='';
    $('#snk_form').submit();
   }

else if(cls!='' && ced!='' )
   {
 
  $('#pro_lsid').val(cls);
  $('#pro_edid').val(ced);
 document.getElementById('pro_id').value='';
$('#snk_form').submit();
   
   }else{
  return false;
}
}
</script>
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
	
 <form id="snk_form" method="post" action="<?php echo base_url(); ?>MerchantUser/sync_product" >
  <input type="hidden" id="pro_id" name="pro_id" value="" >
 <input type="hidden" id="pro_lsid" name="pro_lsid" value="" >
 <input type="hidden" id="pro_edid" name="pro_edid" value="" >
</form>
</div>
<!-- END Page Content -->