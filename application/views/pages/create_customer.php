   <!-- Page content -->
<?php
	$this->load->view('alert');
?>
	<div id="page-content">
	     <?php
				echo		$message = $this->session->flashdata('message');
						
					?>
	    
	  
	
<style> .error{color:red; }</style>    
    <!-- END Wizard Header --> 
    <legend class="leg"><?php if(isset($customer)){ echo "Edit Customer";}else{ echo "Create New Customer"; }?></legend>
    <!-- Progress Bar Wizard Block -->
    
	             
       
    
        <!-- Progress Bar Wizard Content -->
        
		<form  id="customer_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>MerchantUser/create_customer">
			<div class="block">
			 <input type="hidden"  id="customerListID" name="customerListID" value="<?php if(isset($customer)){echo $customer['ListID']; } ?>" /> 
			 	 <input type="hidden"  id="EditSequence" name="EditSequence" value="<?php if(isset($customer)){echo $customer['EditSequence']; } ?>" /> 
			 	 	
			
			    <div class="form-group">
							<label class="col-md-3 control-label" for="example-username">Company Name</label>
							<div class="col-md-7">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($customer)){echo $customer['companyName']; } ?>" ><?php echo form_error('companyName'); ?>
							</div>
						</div>
						
                            <div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Customer Name<span class="text-danger">*</span> </label>
					<div class="col-md-7">
					<input type="text" id="fullName" name="fullName" class="form-control"  value="<?php if(isset($customer)){echo $customer['FullName']; } ?>" ><?php echo form_error('fullName'); ?> </div>
					</div>
                            
			
                            
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">First Name </label>
					<div class="col-md-7">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($customer)){echo $customer['FirstName']; } ?>" ><?php echo form_error('firstName'); ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Last Name </label>
					<div class="col-md-7">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($customer)){echo $customer['LastName']; } ?>" ><?php echo form_error('lastName'); ?> </div>
					</div>					
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Email Address</label>
					<div class="col-md-7">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($customer)){echo $customer['Contact']; } ?>"><?php echo form_error('fullName'); ?> </div>
					</div>
					<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">Phone Number</label>
							<div class="col-md-7">
								<input type="text" id="phone" name="phone" class="form-control" value="<?php if(isset($customer)){echo $customer['Phone']; } ?>">
							</div>
					</div>
			    	
				
				
			 <fieldset>
			</div>
			<legend class="leg"> Billing Address</legend>
			    <div class="block" id="set_bill_data">
                                            
                                       	
										
				
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 1</label>
						<div class="col-md-7">
							<input type="text" id="address1" name="address1"  autocomplete="nope" value="<?php if(isset($customer)){echo $customer['BillingAddress_Addr1']; } ?>" class="form-control"><?php echo form_error('companyAddress1'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 2</label>
						<div class="col-md-7">
							<input type="text" id="address2" name="address2"  class="form-control" autocomplete="nope" value="<?php if(isset($customer)){echo $customer['BillingAddress_Addr2']; } ?>" >
						</div>
					</div>
					   <div class ="form-group">
						   <label class="col-md-3 control-label" name="city">City</label>
						   <div class="col-md-7">
						       	<input type="text" id="city" class="form-control" name="companyCity" autocomplete="nope" value="<?php if(isset($customer)){ echo $customer['BillingAddress_City']; } ?>">
								
							</div>
                        </div>

					
                        <div class ="form-group">
						   <label class="col-md-3 control-label"  name="state">State</label>
						   <div class="col-md-7">
						       <input type="text" id="state" class="form-control " name="companyState" autocomplete="nope" value="<?php if(isset($customer)){ echo $customer['BillingAddress_State']; } ?>">
							
							</div>
                        </div>		
			
                     
						<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">ZIP Code</label>
							<div class="col-md-7">
								<input type="text" id="zipCode" name="zipCode" class="form-control" value="<?php if(isset($customer)){echo $customer['BillingAddress_PostalCode']; } ?>" autocomplete="nope"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
					
						<div class ="form-group">
						   <label class="col-md-3 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-7">
						      
								<input type="text" id="country" class="form-control " name="companyCountry" autocomplete="nope" value="<?php if(isset($customer)){ echo $customer['BillingAddress_Country']; } ?>">
							</div>
                        </div>	
                        					
					</fieldset>
				 
			</div>
           	<legend class="leg">Shipping Address</legend>
           	<div class="block">
                        <input type="checkbox" id="chk_add_copy"> Same as Billing Address              	
                
					<div style="margin-top: 16px;"  class="form-group ">
						<label class="col-md-3 control-label" for="example-username">Address Line 1</label>
						<div class="col-md-7">
							<input type="text" id="saddress1" name="saddress1" autocomplete="nope"  value="<?php if(isset($customer)){echo $customer['ShipAddress_Addr1']; } ?>" class="form-control"><?php echo form_error('companyAddress1'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 2</label>
						<div class="col-md-7">
							<input type="text" id="saddress2" name="saddress2"  class="form-control"  value="<?php if(isset($customer)){echo $customer['ShipAddress_Addr2']; } ?>" autocomplete="nope">
						</div>
					</div>
					   <div class ="form-group">
						   <label class="col-md-3 control-label" name="scity">City</label>
						   <div class="col-md-7">
						       	<input type="text" id="scity" class="form-control" name="sCity" autocomplete="nope" value="<?php if(isset($customer)){ echo $customer['ShipAddress_City']; } ?>">
								
							</div>
                        </div>

					
                        <div class ="form-group">
						   <label class="col-md-3 control-label"  name="sstate">State</label>
						   <div class="col-md-7">
						       <input type="text" id="sstate" class="form-control " name="sState" autocomplete="nope" value="<?php if(isset($customer)){ echo $customer['ShipAddress_State']; } ?>">
							
							</div>
                        </div>		
			
						<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">ZIP Code</label>
							<div class="col-md-7">
								<input type="text" id="szipCode" name="szipCode" class="form-control" value="<?php if(isset($customer)){echo $customer['ShipAddress_PostalCode']; } ?>"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
					
						<div class ="form-group">
						   <label class="col-md-3 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-7">
						 
								
								<input type="text" id="scountry" class="form-control" autocomplete="nope" name="scountry" value="<?php if(isset($customer)){ echo $customer['ShipAddress_Country']; } ?>">
							</div>
                        </div>	
                        
				</fieldset>
					
                  	<div class="form-group">
	                	<div class="col-md-3"></div>
						<div class="col-md-7 text-right">
					  		
							<?php 
								if(isset($customer)){ $url = base_url()."home/view_customer/".$customer['ListID']; } 
								else { $url = base_url()."home/customer"; } 
							?>
							<button type="submit" class="submit btn btn-sm btn-success">Save</button>
                      		<a href="<?php echo $url; ?>" class=" btn btn-sm btn-primary1">Cancel</a>
						</div>
				    </div>	
  		</form>
		
   
        <!-- END Progress Bar Wizard Content -->
    
    <!-- END Progress Bar Wizard Block -->


</div>

<!-- END Page Content -->


<script>

$(document).ready(function(){
 
    $('#customer_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'companyName': {
		                minlength: 2,
                        maxlength: 41,
                    },
                    'firstName': {
                         minlength: 2,
                         maxlength: 100,
                         validate_char:true,
                    },
					 
					 'lastName': {
					     minlength: 2,
                       maxlength: 100,
                     validate_char:true,
                    },
					
					'userEmail':{
						  email:true,
						  
					},
				 'fullName': {
                        required: true,
                        minlength: 2,
                        maxlength: 41,
                        validate_char:true,
                     
                        
                         remote: {
                                      url: base_url+'ajaxRequest/check_customer_data',
                                    type: "POST",
                                    cache: false,
                                    dataType: 'json',
                                    data: {
                                        customer: function(){ return $("#fullName").val(); },
                                         customerID: function(){ return $("#customerListID").val(); }
                                    },
                                    dataFilter: function(response) {
                                     
                                       var rsdata = jQuery.parseJSON(response)
                                      
                                         if(rsdata.status=='success')
                                         return true;
                                         else
                                         return false;
                                    }
                                },
                        
                    },
                    'address1': {
                        minlength: 2,
			    	    maxlength: 41,
                    },
                    'saddress1': {
                        minlength: 2,
                        maxlength: 41,
                    },
                    'address2': {
                        minlength: 2,
                        maxlength: 41,
                    },
                    'saddress2': {
                        maxlength: 41,
                    },
					'companyCountry': {
					        minlength: 2,
					        maxlength: 31,
					},
					'scountry': {
					       minlength: 2,
					      maxlength: 31,
					},
					'companyCity': {
					     minlength: 2,
					     maxlength: 31,
					},
					'sCity': {
					    minlength: 2,
					    maxlength: 31,
					},
                    'companyState': {
                        minlength: 2,
                        maxlength: 21,
                    }, 
                    'sState': {
                        minlength: 2,
                         maxlength: 21,
                    },
					'phone': {
                         minlength: 10,
                         maxlength: 15,
                         phoneUS:true,
						  
					},
				
                    'zipCode': {
                        
						minlength:3,
						maxlength:10,
                    },
					 'szipCode': {
							minlength:3,
						maxlength:10,
                    },
                   
                    'portal_url':{
                        minlength:2,
                        maxlength: 20,
                        ProtalURL:true,
                        "remote" :function(){return Chk_url('portal_url');} 
					},
					
			},
				messages: {
                   companyCountry: {
                    },
                   companyState: {
                    },
                    companyCity: {
                    },
                    fullName:{
        					remote:"This customer name already exist"
        			}
                    
			}
    });
    
   $.validator.addMethod("phoneUS", function(phone_number, element) {
         
            return phone_number.match(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/);
        }, "Please specify a valid phone number like as (XXX) XX-XXXX");
         
    $.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
      
   
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[A-Za-z0-9-&,'._* ]*$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/,. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
         
    
    $('#chk_add_copy').click(function(){
   
     
     if($('#chk_add_copy').is(':checked')){
        
                            	$('#saddress1').val($('#address1').val());
								$('#saddress2').val($('#address2').val());
								$('#scity').val($('#city').val());
								$('#sstate').val($('#state').val());
								$('#szipCode').val($('#zipCode').val());
								$('#scountry').val($('#country').val());
     }
    else{
    	 
         var val_sp='';
                            	$('#saddress1').val(val_sp);
								$('#saddress2').val(val_sp);
								$('#scity').val(val_sp);
								$('#sstate').val(val_sp);
								$('#szipCode').val(val_sp);
								$('#scountry').val(val_sp);
    }
     
 });
    
  
	
	
    
});	

</script>



