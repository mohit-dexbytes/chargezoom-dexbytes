<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
    
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i><?php if(isset($subs)){ echo "Edit"; }else{ echo "Create"; } ?> Invoice<br><small>You can create a new invoice here.</small>
            </h1>
	        <div class="msg_data ">
			    <?php echo $this->session->flashdata('message');   ?>
		    </div>
        </div>
    </div>
  
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
        <div class="col-md-12">
		<div class="block">
		
		    <form id="form-validation" action="<?php echo base_url(); ?>SettingSubscription/edit_invoice" method="post" class="form-horizontal form-bordered">
                
				 <input type="hidden" name="txnID" name="txnID" value="<?php echo $invoice['TxnID']; ?>"  />
				  <input type="hidden" name="refNumber" name="refNumber" value="<?php echo $invoice['RefNumber']; ?>"  />
				  <input type="hidden" name="edit_sequence" name="edit_sequence" value="<?php echo $invoice['EditSequence']; ?>"  />
				
                 <fieldset>  
					
				   
			        <div class="col-md-3 form-group">
						 <label class="control-label" for="customerID">Customer</label>
						  <div>
                            <select id="customerID" name="customerID" class="form-control">
                                <option value>Select Customer</option>
						        <?php   foreach($customers as $customer){       ?>
						        <option value="<?php echo $customer['ListID']; ?>"
								<?php if(isset($invoice) &&  $invoice['Customer_ListID']==$customer['ListID']){  echo "selected" ;} ?>  ><?php echo  $customer['companyName'] ; ?></option>
						        <?php } ?>
                            </select>
						 </div>	
                   </div>    
                    
                 
                   <div class="col-md-3  form-group">   
                        <label class=" control-label" for="firstName">Due Date</label>
                           <div>
                            <div class="input-group input-date">
						
                                <input type="text" id="duedate" name="duedate" class="form-control"  value="<?php if(isset($invoice)){ echo date('m-d-Y', strtotime($invoice['DueDate'])); } ?>" data-date-format="mm-dd-yyyy" placeholder="mm-dd-yyyy">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                       </div>
                    
                    </div>
					
					 
			</fieldset>
                    
					<fieldset><legend> Item Details</legend>	
					<div class="form-group form-actions">
					<div class="col-sm-3">
					    <div class="form-group">	<label class="control-label">Products & Services</label> </div>
			        	</div> 
					<div class="col-sm-4">  <div class="form-group"><label class="control-label">Description </label></div> </div>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Unit Rate </label> </div></div>
					<div class="col-sm-1"><div class="form-group"><label class="control-label">Quantity </label></div> </div>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Total</label></div> </div>
					
					</div>
					
											
						  <div id="item_fields">
						 <?php 
						       if(isset($items) && !empty($items)){
							     foreach($items as $k=>$item ){  ?>
								  <div class="form-group removeclass<?php echo $k+1; ?>">
								<div class="col-sm-3 nopadding"><div class="form-group">
								  <select class="form-control"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="productID<?php echo $k+1; ?>" name="productID[]">
								<option>Select Plans</option>		
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>" <?php if($plan['ListID']==$item['Item_ListID']){ echo "selected";} ?>  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div><div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo $item['Item_FullName']; ?>" placeholder="Description "></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo $item['Rate']; ?>" onblur="set_unit_val('<?php echo $k+1; ?>');" placeholder="Unit Rate"></div></div>
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control" onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity<?php echo $k+1; ?>" name="quantity[]" value="<?php echo $item['Quantity']; ?>" placeholder="Quantity"></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k+1; ?>" name="total[]" value="<?php echo ($item['Quantity']*$item['Rate']); ?>" placeholder="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>
							</div>
						<?php		 }								 
							   }else{  
						 ?>
						   <div class="form-group removeclass1">
							<div class="col-sm-3 nopadding"><div class="form-group ">
								  <select class="form-control"  onchange="select_plan_val('1');"  id="productID1" name="productID[]">
								<option>Select Plans</option>		
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>"  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div><div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description1" name="description[]" value="" placeholder="Description "></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control" id="unit_rate1" name="unit_rate[]" value="" onblur="set_unit_val('1');" placeholder="Unit Rate"></div></div>
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control" onblur="set_qty_val('1');" id="quantity1" name="quantity[]" value="" placeholder="Quantity"></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total1" name="total[]" value="" placeholder="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('1');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>
							   
							</div>   <?php } ?>
								  
						 </div>
						
				 
				       <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label "></label>						  
						 <div class="group-btn">
							<button class="btn btn-sm btn-success" type="button"  onclick="item_fields();"> Add New </button>
						 
							<label class="btn btn-sm btn-success pull-right remove-hover"  ><strong>Total :</strong></strong> <span class="fa fa-usd" aria-hidden="true"></span> <span id="grand_total"> <strong>0.00</strong></span>  </label>
						  </div>
				        </div>
						</div>
						
					</div>	
					
				 <div class="col-md-12"	>
			
			  
						</fieldset>
                    
					<fieldset>
						<div id="set_bill_data">
							<legend>Billing Address</legend>						
									 <div class="col-sm-12 form-group">										
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 1</label>
                                              
                                                        <input type="text" id="address1" name="address1" class="form-control " value="<?php if(isset($subs)) echo $subs['address1']; ?>"  placeholder="Address...">
                                                  
                                          </div>
										</div>	
										 <div class="col-sm-12 form-group">					
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 2</label>
                                              
                                                        <input type="text" id="address2" name="address2" class="form-control "  value="<?php if(isset($subs))echo $subs['address2']; ?>" placeholder="Address...">
                                                  
                                             </div>
										   </div>
								<div class="col-sm-3 form-group">	   
								 <div class="">
										
											
                                              
												<label class="control-label" for="example-typeahead">Country</label>
												
													<input type="text" id="country" name="country" class="form-control input-typeahead" autocomplete="off" value="<?php if(isset($invoice)) echo $invoice['ShipAddress_Country']; ?>" placeholder="Search Country...">
													
											
										</div>	
									</div>
										<div class="form-group col-sm-3">	
										<div class=" ">
											
                                                <label class=" control-label" for="val_username">State/Province</label>
                                               
                                                        <input type="text" id="state" name="state" class="form-control input-typeahead"  value="<?php if(isset($invoice)) echo $invoice['ShipAddress_State']; ?>" autocomplete="off" placeholder="Search State...">
                                                   
                                        
										 </div>
										</div>
										<div class="form-group col-sm-2">
										 <div class="">
                                         
                                                <label class=" control-label" for="val_username">City</label>
                                              
                                                        <input type="text" id="city" name="city" class="form-control input-typeahead" autocomplete="off" value="<?php  if(isset($invoice))echo $invoice['ShipAddress_City']; ?>" placeholder="enter City...">
                                                    
                                           
										</div>
										</div>
										<div class="form-group col-sm-2">
										 <div class="">		  
										
                                                <label class=" control-label" for="val_username">Zip/Postal Code</label>
                                              
                                                        <input type="text" id="zipcode" name="zipcode" class="form-control" value="<?php if(isset($invoice)) echo $invoice['ShipAddress_PostalCode']; ?>" placeholder="Zipcode..">
                                                     
	
										  </div>
										</div>
										<div class="form-group col-sm-2">		
										 <div class="">
											  
                                                <label class=" control-label" for="phone">Contact Number</label>
                                               
                                                        <input type="text" id="phone" name="phone" class="form-control"  placeholder="Contact Number...">
                                                     
                                         
										   </div>
									</div>
								
				           </div>
				
			</fieldset>
               
				
				
				<div class="form-group pull-right">
                    
            <a href="<?php echo base_url();?>home/invoices"class="btn btn-sm btn-danger">Cancel</a>            
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                      
                    
                </div>
            </form>
          </div>      
        </div>
            
    </div>


    <script>
	
        $(function(){
		
			
           nmiValidation.init();
		  
	      var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
			 $("#invoice_date").datepicker({ 
              format: 'yyyy-mm-dd',
              autoclose: true
            });  
			
			$('#duedate').datepicker({
			format: 'yyyy-mm-dd',
			startDate:today,
			autoclose: true
			});
			$('#subsamount').blur(function(){
				
				var dur = $('#duration_list').val();
              var subsamount = $('#subsamount').val();
			  var tot_amount = subsamount*dur ;
			  $('#total_amount').val(tot_amount.toFixed(2));
			  $('#total_invoice').val(dur);
				
			});
			
			
					
		  $('#card_list').change( function(){
			   if($(this).val()=='new1'){
			   $('#set_credit').show();
			   }else{
					  $('#card_number').val('');
			$('#set_credit').hide();
			   }
		  });
					
		$('#customerID').change(function(){
			
			var cid  = $(this).val();
		
			if(cid!=""){
				$('#card_list').find('option').not(':first').remove();
				$.ajax({
					type:"POST",
					url : "<?php echo base_url(); ?>Payments/check_vault",
					data : {'customerID':cid},
					success : function(response){
						
							 data=$.parseJSON(response);
							
							 if(data['status']=='success'){
							
								  var s=$('#card_list');
								 
								  var card1 = data['card'];
									 $(s).append('<option value="new1">New Card</option>');
									for(var val in  card1) {
										
									  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
									}
																   
									$('#address1').val(data['ShipAddress_Addr1']);
									$('#address2').val(data['ShipAddress_Addr2']);
									$('#city').val(data['ShipAddress_City']);
									$('#state').val(data['	ShipAddress_State']);
									$('#zipcode').val(data['ShipAddress_PostalCode']);
									$('#phone').val(data['Phone']);
									
								  
						   }	   
						
					}
					
					
				});
				
			}	
		});		
				
	

	});
	
	
	var jsdata = '<?php if(isset($items)){ echo json_encode($items);}?>';
	
	if(jQuery.isEmptyObject(jsdata))
   {
	  
		var room = 1;
	  
		
	}else{
		
		var grand_total1=0;
		var room = '<?php if(isset($k)){ echo $k+1; }else{ echo "1";} ?>';
		$( ".total_val" ).each(function(){
			
               grand_total1+= parseFloat($(this).val());
			});
			
		$('#grand_total').html(grand_total1);

	}
	function item_fields()
	{
		room++;
		
		var  jsplandata = '<?php echo json_encode($plans); ?>';
		
		var plan_data = $.parseJSON(jsplandata); 
		var plan_html ='<option val="">Select Plan</option>';
		for(var val in  plan_data) {         plan_html+='<option value="'+ plan_data[val]['ListID']+'">'+plan_data[val]['FullName']+'</option>'; }
		
		
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="Description "></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="unit_rate'+room+'" name="unit_rate[]" value="" onblur="set_unit_val('+room+');" placeholder="Unit Rate"></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control" onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" placeholder="Quantity"></div></div>	<div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+room+'" name="total[]" value="" placeholder="Total"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest)
  }
  
  
   function remove_education_fields(rid)
   {
	  
	   var rid_val = $('#total'+rid).val();
	   
	   var gr_val  = $('#grand_total').html();
	    if(rid_val){
	   var dif     = parseFloat(gr_val)-parseFloat(rid_val);
	   $('#grand_total').html(dif.toFixed(2));
		}
	   $('.removeclass'+rid).remove();
	   
	    
	   
	   
  }
	
	
	
	
	
	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                     sub_name:{
						  required:true,
						  minlength:3,
					 },
					 sub_start_date: {
							 required:true,
						},
					autopay:{
							 required:true,
					},
					gateway_list:{
							 required:true,
					},
					card_list:{
							 required:true,
					},
					paycycle:{
							 required:true,
					},		
			    	 duration_list: {
                        required: true,
                        number: true,
						maxlength:2,
						
                    },
					invoice_date: {
                        required: true,
                        
                    },
                    customerID: {
                         required: true,
                       
                    },
					 subsamount: {
                        required: true,
						number:true,
						
                    },
					friendlyname:{
						 required: true,
						 minlength: 3,
					},
					  card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
					 cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },	
					
					address1:{
						required:true,
					},
					address2:{
						required:true,
					},
                  	country:{
						required:true,
					},
					state:{
						required:true,
					},
					city:{
						required:true,
					},
					zipcode:{
							minlength:3,
						maxlength:10,
						ProtalURL:true,
						validate_addre:true,
					},
					'portal_url':{
                        minlength:2,
                        maxlength: 20,
                        ProtalURL:true,
                        "remote" :function(){return Chk_url('portal_url');} 
					},
                  freetrial:{
						required: true,
						digits:true,
						check_free:{
							sub_start_date:'#sub_start_date',
							paycycle:'#paycycle',
							duration_list:'#duration_list',
						}
					},
                    	
					
                },
               
            });
					
		 $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
		
		
		
		$.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
      

					
				
		 $.validator.addMethod('check_free', function(value, element, params) {  
			
		  var duration = $(params.duration_list).val();
		  
		  if(duration =='0')return true;
		  
		  var frequency = $(params.paycycle).val();
		  var stdate     = new  Date( Date.parse($(params.sub_start_date).val()));
		  
		  var free = value;
	
		  return (duration > free);
		}, 'Value should be less than Recurrence value');
						

        }
    };
}();
   
/**********Check the Validation for free trial**********************/
	function weeksBetween(d1, d2) {
		
     return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
	
   }
   function daysbeween(d1, d2) {
		
     return Math.round((d2 - d1) / (24 * 60 * 60 * 1000));
	
   }
   
   function get_months(d1, d2){
	 

return difference = (d2.getFullYear()*12 + d2.getMonth()) - (d1.getFullYear()*12 + d1.getMonth()); 
	   
   }  
   
       function get_frequncy_val(du, new1date, fr){
		   var res='';
		 
		       var CurrentDate =  new Date(new1date.getFullYear(), new1date.getMonth(), new1date.getDate(), 0, 0, 0, 0); 
				  CurrentDate.setMonth(CurrentDate.getMonth() + parseInt(du));
                  var newdate = new Date(CurrentDate);
				
		 if(fr=='dly'){
           res= daysbeween(new Date(new1date), new Date(newdate));
		 }else if(fr=='1wk'){
			  res= weeksBetween(new Date(new1date), new Date(newdate));
		 }else if(fr=='2wk'){
           res= weeksBetween(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='mon'){
           res= get_months(new Date(new1date), new Date(newdate));
		 }else if(fr=='2mn'){
         res= get_months(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='qtr'){
          res= get_months(new Date(new1date), new Date(newdate))/3;
		 }else if(fr=='six'){
           res= get_months(new Date(new1date), new Date(newdate))/6;
		 }else if(fr=='yr1'){
          res= get_months(new Date(new1date), new Date(newdate))/12;
		 }else if(fr=='yr2'){
		 res= get_months(new Date(new1date), new Date(newdate))/24;
		 }else if(fr=='yr3'){
			 res= get_months(new Date(new1date), new Date(newdate))/36;
		 }	
           return res;    			 
		   
	   } 
	   
/************End*******************/	   
	
   
   function chk_payment(r_val)  {  
   
     
      if(r_val=='1'){
		  $('#set_pay_data').show();
	  }else{
		$('#set_pay_data').hide();
      }	  

   }   
   
	function select_plan_val(rid){
		
		
		
		var itemID = $('#productID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>SettingSubscription/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['FullName']);
             $('#unit_rate'+rid).val(item_data['AverageCost']);
			 $('#quantity'+rid).val(item_data['QuantityOnHand']);
			
			}	
		});
		
	}
	
	function set_unit_val(rid){
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var total  = qty*rate; 
		$('#total'+rid).val(total.toFixed(2));
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
			
               grand_total+=parseFloat($(this).val());
			});
			
		$('#grand_total').html(grand_total);
	}
	function set_qty_val(rid){
       
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var total  = qty*rate; 
		$('#total'+rid).val(total.toFixed(2));
		var grand_total=0;
		$( ".total_val" ).each(function(){
               grand_total=parseFloat(grand_total)+parseFloat($(this).val());
			});
			
		$('#grand_total').html(grand_total.toFixed(2));
		
	}	
	
	
	</script>
</div>