<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">

	<div class="msg_data ">
		<?php echo $this->session->flashdata('message');   ?>
	</div>
	

	<!-- END Forms General Header -->
	<div class="row">
		<!-- Form Validation Example Block -->
		<div class="col-md-12">
			<!-- <div class="block"> -->
				
				<legend class="leg"><?php if (isset($subs)) {
									echo "Edit";
								} else {
									echo "Create";
								} ?> Invoice
				</legend> 
				
				
				<form id="form-validation" action="<?php echo base_url(); ?>SettingSubscription/create_invoice" method="post" class="form form-horizontal form-bordered">
					<div class="block">
					<fieldset>
						<div class="col-md-12 no-pad">
							<div class="col-md-4 form-group">
								<label class="control-label" for="customerID">Customer</label>
								<div>
									<select id="customerID" name="customerID" class="form-control select-chosen">
										<option value="">Select Customer</option>
										<?php foreach ($customers as $customer) {       ?>
											<option value="<?php echo $customer['ListID']; ?>" <?php if (isset($Invcustomer) &&  $Invcustomer['ListID'] == $customer['ListID']) {
																									echo "selected";
																								} ?>><?php echo  $customer['FullName']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-md-4  form-group">
								<label class=" control-label" for="firstName">Invoice Date</label>
								<div>
									<div class="input-group input-date">

										<input type="text" id="invdate" name="invdate" class="form-control input-datepicker" value="<?php echo date('m/d/Y'); ?>" data-date-format="mm/dd/yyyy" placeholder="Invoice Date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-4 form-group">
								<label class="control-label" for="taxes">Tax</label>
								<div>
									<select name="taxes" id="taxes" class="form-control tax_div">
										<option value="">Select Tax</option>
										<?php foreach ($taxes as $tax) { ?>
											<option value="<?php echo $tax['taxID']; ?>" <?php if (isset($subs) && $tax['taxID'] == $subs['taxID']) {
																								echo "selected";
																							} ?>><?php echo $tax['friendlyName']; ?> </option>

										<?php } ?>
									</select>

								</div>
							</div>
							
						</div>
						<div class="col-md-12 no-pad">
							<div class="col-md-3 form-group">
								<label class="control-label" for="pay_terms">Payment Term</label>
								<div>
									<select name="pay_terms" id="pay_terms" class="form-control ">
										<option value="">Select Term</option>
										<?php foreach ($netterms as $terms) {
											$selected = '';

											if ($terms['pt_netTerm'] != "") {
												$termSet = $terms['pt_netTerm'];
											} else {
												$termSet = $terms['netTerm'];
											}

											if($terms['id'] == $selectedID){
												$selected = 'Selected';
											}else{
												$selected = '';
											}
											if ($terms['enable'] != 1) { ?>
												<option <?php echo $selected;  ?>  value="<?php echo $termSet; ?>"><?php if ($terms['pt_name'] != "") {
																																											echo $terms['pt_name'];
																																										} else {
																																											echo $terms['name'];
																																										} ?></option>
										<?php }
										} ?>
									</select>

								</div>
							</div>

							

						</div>
						<div class="col-sm-12 ">
							<div id="set_credit" style="display:none;">

								<div class="col-sm-4 form-group">
									<div>
										<label class="control-label">Credit Card Number</label>

										<input type="text" id="card_number" name="card_number" class="form-control" placeholder="Credit Card Number"  autocomplete="off">
									</div>
								</div>
							
								<div class="col-sm-3 form-group">
									<div>
										<label class="control-label" for="expry">Expiry Month </label>

										<select id="expiry" name="expiry" class="form-control">
											<option value="">Select Month</option>
											<option value="01">JAN</option>
											<option value="02">FEB</option>
											<option value="03">MAR</option>
											<option value="04">APR</option>
											<option value="05">MAY</option>
											<option value="06">JUN</option>
											<option value="07">JUL</option>
											<option value="08">AUG</option>
											<option value="09">SEP</option>
											<option value="10">OCT</option>
											<option value="11">NOV</option>
											<option value="12">DEC</option>
										</select>
									</div>
								</div>
								<div class="col-sm-3 form-group">
									<div>
										<label class=" control-label" for="expry_year">Expiry Year</label>

										<select id="expiry_year" name="expiry_year" class="form-control">
											<option value="">Select Year</option>
											<?php
											$cruy = date('y');
											$dyear = $cruy + 25;
											for ($i = $cruy; $i < $dyear; $i++) {  ?>
												<option value="<?php echo "20" . $i;  ?>"><?php echo "20" . $i;  ?> </option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-sm-2 form-group">
									<div>
										<label class="control-label" for="cvv">Card Security Code (CVV)</label>

										<input type="text" id="cvv" name="cvv" class="form-control" placeholder="Card Security Code (CVV)">

									</div>
								</div>


							</div>
						</div>

						<div class="col-sm-12 "></div>





					</fieldset>
                   </div>


                   	<legend class="leg"> Products & Services</legend>
                   <div class="block">

					<fieldset>
					
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group"><label class="control-label">Products & Services</label> </div>
							</div>
							<div class="col-sm-3">
								<div class="form-group"><label class="control-label">Description </label></div>
							</div>
							<div class="col-sm-2">
								<div class="form-group text-left"><label class="control-label">Price</label> </div>
							</div>
							<div class="col-sm-1">
								<div class="form-group text-left"><label class="control-label">Quantity </label></div>
							</div>
							<div class="col-sm-1">
								<div class="set_taxes" style="display:none;">
									<div class="form-group text-center"><label class="control-label">Tax </label></div>
								</div>
							</div>
							<div class="col-sm-2 row">
								<div class="form-group"><label class="control-label">Total</label></div>
							</div>

						</div>


						<div id="item_field_ins" class="row">
							<?php
							if (isset($items) && !empty($items)) {
								foreach ($items as $k => $item) {
									$rate = $item['itemRate'];
									$qnty = $item['itemQuantity'];
									if (!empty($item['itemTax'])) {
										$tax  = $item['itemTax'];
										echo "<script> $('.set_taxes').css('display','block'); </script>";
									} else {
										$tax = 0;
									}

									$total_amt = ($rate * $qnty) + (($rate * $qnty) * $tax / 100);

							?>

									<div class="form-group removeclass<?php echo $k + 1; ?>">
										<div class="col-sm-3 nopadding">
											<div class="form-group">
												<select class="form-control" onchange="select_plan_val('<?php echo $k + 1; ?>');" id="productID<?php echo $k + 1; ?>" name="productID[]">
													<option value="">Select Product & Service</option>
													<?php foreach ($plans as $plan) { ?>
														<option value="<?php echo $plan['ListID']; ?>" <?php if ($plan['ListID'] == $item['itemListID']) {
																											echo "selected";
																										} ?>>
															<?php echo $plan['FullName']; ?> </option> <?php }  ?>
												</select></div>
										</div>

										<div class="col-sm-3 nopadding">
											<div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k + 1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" placeholder="Description "></div>
										</div>

										<div class="col-sm-2 nopadding">
											<div class="form-group">
												<input type="text" class="form-control" id="unit_rate<?php echo $k + 1; ?>" name="unit_rate[]" value="<?php echo $item['itemRate']; ?>" onblur="set_unit_val('<?php echo $k + 1; ?>');" placeholder="Price"></div>
										</div>

										<div class="col-sm-1 nopadding">
											<div class="form-group"> <input type="text" class="form-control text-center" maxlength="4" onblur="set_qty_val('<?php echo $k + 1; ?>');" id="quantity<?php echo $k + 1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>" placeholder="Qty"></div>
										</div>

										<div class="col-sm-1 nopadding">
											<div class="set_taxes">
												<div class="form-group text-center"> <input type="checkbox" id="tax_check<?php echo $k + 1; ?>" <?php if ($item['itemTax']) echo "checked"; ?> name="tax_check[]" class="tax_checked" onchange="set_tax_val(this, '<?php echo $k + 1; ?>')" value="<?php echo $item['itemTax']; ?>"></div>
											</div>
										</div>

										<div class="col-sm-2 row nopadding">
											<div class="form-group">
												<div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k + 1; ?>" name="total[]" value="<?php echo ($item['itemQuantity'] * $item['itemRate']); ?>" placeholder="Total">
													<div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k + 1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div>
												</div>
											</div>
										</div>
										<div class="clear"></div>
									</div>
								<?php		 }
							} else {
								?>
								 <?php } ?>

						</div>


						<div class="col-md-12 no-pad">
							<div class="form-group">

								<div class=" form-actions">
									<label class="control-label "></label>
									<div class="group-btn">
										<button class="btn btn-sm btn-success" type="button" onclick="item_invoice_fields();"> Add More </button>

										<label class="btn btn-sm pull-right remove-hover"><strong>Total: $<span id="grand_total"><?php echo '0.00'; ?></span> </strong> </label>
									</div>
								</div>
							</div>

						</div>






					</fieldset>
                    </div>



                    <legend class="leg">Billing Address</legend>
                    <div class="block">
					<fieldset>

						<div id="set_bill_data" style="">
							
							<div class="col-md-12 no-pad">
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 1</label>

										<input type="text" id="baddress1" name="baddress1" class="form-control " <?php if (isset($Invcustomer)) {
																														echo 'value="' . $Invcustomer['BillingAddress_Addr1'] . '" ';
																													} ?> autocomplete="nope">

									</div>
								</div>
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 2</label>

										<input type="text" id="baddress2" name="baddress2" class="form-control " <?php if (isset($Invcustomer)) {
																														echo 'value="' . $Invcustomer['BillingAddress_Addr2'] . '" ';
																													} ?> autocomplete="nope">

									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">City</label>

										<input type="text" id="bcity" name="bcity" class="form-control input-typeahead-city" <?php if (isset($Invcustomer)) {
																												echo 'value="' . $Invcustomer['BillingAddress_City'] . '" ';
																											} ?> autocomplete="nope">


									</div>
								</div>
								<div class="form-group col-sm-3">
									<div class=" ">

										<label class=" control-label" for="val_username">State/Province</label>

										<input type="text" id="bstate" name="bstate" class="form-control input-typeahead-state" <?php if (isset($Invcustomer)) {
																												echo 'value="' . $Invcustomer['BillingAddress_State'] . '" ';
																											} ?> autocomplete="nope">


									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">ZIP Code</label>

										<input type="text" id="bzipcode" name="bzipcode" class="form-control" <?php if (isset($Invcustomer)) {
																													echo 'value="' . $Invcustomer['BillingAddress_PostalCode'] . '" ';
																												} ?> autocomplete="nope">


									</div>
								</div>
								<div class="col-sm-3 form-group">
									<div>


										<label class="control-label">Country</label>



										<input type="text" id="bcountry" name="bcountry" class="form-control input-typeahead-country" <?php if (isset($Invcustomer)) {
																													echo 'value="' . $Invcustomer['BillingAddress_Country'] . '" ';
																												} ?> autocomplete="nope">
									</div>
								</div>
								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="phone">Phone Number</label>

										<input type="text" id="bphone" name="bphone" class="form-control" <?php if (isset($Invcustomer)) {
																												echo 'value="' . $Invcustomer['Phone'] . '" ';
																											} ?> autocomplete="nope">


									</div>
								</div>

							</div>
						</div>

					</fieldset>
				</div>
				
                <legend class="leg">Shipping Address</legend>
				<div class="block">
					<fieldset>
						<div id="set_bill_data1">
							
							<div class="col-sm-12 form-group">
								<div class="col-md-12 no-pad">
									<input type="checkbox" id="chk_add_copy"> Same as Billing Address
								</div>
							</div>
							<div class="col-md-12 no-pad">
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 1</label>

										<input type="text" id="address1" name="address1" class="form-control " value="<?php if (isset($subs)) echo $subs['address1']; ?>" autocomplete="nope">

									</div>
								</div>
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 2</label>

										<input type="text" id="address2" name="address2" class="form-control " value="<?php if (isset($subs)) echo $subs['address2']; ?>" autocomplete="nope">

									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">City</label>

										<input type="text" id="city" name="city" class="form-control input-typeahead-city" autocomplete="nope" value="<?php if (isset($subs)) echo $subs['city']; ?>">


									</div>
								</div>
								<div class="form-group col-sm-3">
									<div class=" ">

										<label class=" control-label" for="val_username">State/Province</label>

										<input type="text" id="state" name="state" class="form-control input-typeahead-state" value="<?php if (isset($subs)) echo $subs['state']; ?>" autocomplete="noope">


									</div>
								</div>
								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">ZIP Code</label>

										<input type="text" id="zipcode" name="zipcode" class="form-control" value="<?php if (isset($subs)) echo $subs['zipcode']; ?>" autocomplete="nope">


									</div>
								</div>

								<div class="col-sm-3 form-group">
									<div class="">


										
										<label class="control-label" for="example-contry">Country</label>

										

										<input type="text" id="country" name="country" class="form-control input-typeahead-country" value="<?php if (isset($subs)) echo $subs['country']; ?>" autocomplete="nope">

									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="phone">Phone Number</label>

										<input type="text" id="phone" name="phone" class="form-control" autocomplete="nope">


									</div>
								</div>

							</div>

						</div>

						<div class="form-group pull-right">
							<div class="col-md-12 no-pad">


								<button type="submit" class="submit btn btn-sm btn-success">Save</button>
								<a href="<?php echo base_url(); ?>home/invoices" class=" btn btn-sm btn-primary1">Cancel</a>
							</div>
						</div>
						<div class="col-md-12 no-pad"></div>
						<div class="form-group pull-right">
							<div class="col-md-12 no-pad">

								<input type="checkbox" id="setMail" name="setMail" class="set_checkbox" /> Send Email
							</div>
						</div>
					</fieldset>


				</form>
			</div>
		
	</div>
</div>
<script src="<?php echo base_url(JS); ?>/pages/qbd_subscription.js"></script>

<script>
	$(document).ready(function() {

		$('#chk_add_copy').click(function() {
			
			if ($('#chk_add_copy').is(':checked')) {
			
				$('#address1').val($('#baddress1').val());
				$('#address2').val($('#baddress2').val());
				$('#city').val($('#bcity').val());
				$('#state').val($('#bstate').val());
				$('#zipcode').val($('#bzipcode').val());
				$('#country').val($('#bcountry').val());
				$('#phone').val($('#bphone').val());
			} else {

				var val_sp = '';
				$('#address1').val(val_sp);
				$('#address2').val(val_sp);
				$('#city').val(val_sp);
				$('#state').val(val_sp);
				$('#zipcode').val(val_sp);
				$('#country').val(val_sp);
				$('#phone').val(val_sp);
			}

		});


	});
</script>