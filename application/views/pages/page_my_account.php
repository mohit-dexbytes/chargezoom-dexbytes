<script src="<?php echo base_url(JS); ?>/pages/merchant_my_account_page.js"> </script>
<style type="text/css">
    .exist_add.colMD6.no-pad {
        width: 49%;
        float: left;
    }
    .exist_payment_method_new, .payment_info_new {
        padding: 3% 0% 0% 0%;
    }
    .cardDataView{
        margin-bottom: 2%;
    }
    .exist_payment_method_new .col-md-5.col-sm-5{
        text-align: right;
    }
    div.cardSection1 {
        width: 37%;
        float: left;
    }
    .bottom_right_section_new {
        background: #fff;
        margin-bottom: 2%;
        display: none;
    }
    .bottom_right_section_new .pull-right {
        position: relative;
        right: 7%;
        bottom: 22px;
    }
    .exist_payment_method_new .pay_value_label {
        width: 94%;
    }
    .exist_payment_method_new .edit_pay_option {
        left: 3%;
    }
    .payment_info_new .single-row {
        margin: 2% 0% 4% 3%;
    }
    .payment_info_new .col-md-4.col-sm-4{
        text-align: right;
    }
    .dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 270px !important;
	}

</style>
<?php
    $this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>

    <!-- Payment Info Block -->
    <legend class="leg">Payment Info</legend>
    <div class="full">
        <!-- Payment Info Title -->
        <div class="block-title">
            
        
        </div>
        
        <!-- END Payment Info Title -->

        <!-- Payment Info Content -->
        <div id="content-part" class="<?php echo (isset($carddata->merchantCardID) && $carddata->merchantCardID > 0)?'exist_record':'new_record'; ?>">
            <form id="thest" method="post" action='<?php echo base_url(); ?>home/update_card_data' class="form-horizontal" >

            <div  class="col-md-12 col-sm-12 no-pad payment_info_section">
                
                <?php if(isset($carddata->merchantCardID) && $carddata->merchantCardID > 0){ ?>
                    <div class="col-md-12 col-sm-12 no-pad cardDataView">
                        <input type="hidden" name="is_address_update" value="1" id="is_address_update">
                        
                        <input type="hidden" id="resellerID" name="resellerID" value="<?php echo $plan->resellerID ?>">

                        <input type="hidden" id="cardID" name="cardID" value="<?php echo $carddata->merchantCardID; ?>">

                        <input type="hidden" id="payOption" name="payOption" value="<?php echo $plan->payOption; ?>">

                        <div class="exist_payment_method_new cardSection1" id="view_mode">
                            <div class="col-md-5 col-sm-5">
                                <label class="left_pay_label">
                                    <?php 
                                    if($plan->payOption == 1){
                                        echo 'Credit Card';
                                    }else if($plan->payOption == 2){
                                        echo 'Checking Account';
                                    }else{

                                    }

                                     ?>

                                </label>
                            </div>
                            <div class="col-md-7 col-sm-7 no-pad">
                                <div class="pay_value_label"> 
                                    <?php 
                                    echo $carddata->merchantFriendlyName;
                                     ?>
                                        
                                </div>
                                <span id="viewEditModeNew" class="edit_pay_option">
                                    <a href="#card_data_process"  data-backdrop="static" data-keyboard="false" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </span>
                            </div>
                        </div>
                         <!-- Edit mode form manage -->
                        
                        <div class="col-md-4 col-sm-4 payment_info_new">
                            <div class="display-plan" id="plan_view">
                                <div class="single-row col-md-12 col-sm-12 no-pad ">
                                    <div class="col-md-4 col-sm-4 ">
                                        <label class="label_left planMethodLabel"  >Plan</label>
                                    </div>
                                    <div class="col-md-7 col-sm-7 no-pad">
                                        <span class="span_right  planMethodValue"><?php  echo $planname; ?></span>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            
                        </div>


                        <div class="col-md-3 col-sm-3 payment_info_new">
                            <div class="display-plan" id="plan_view">
                                
                                <div class="single-row col-md-12 col-sm-12">

                                    <div class="col-md-5 col-sm-5 ">
                                        <label class="label_left planMethodLabel">Price</label>
                                    </div>
                                    <div class="col-md-7 col-sm-7 no-pad">
                                        <span class="span_right planMethodValue"><?php echo "$".number_format($plan->subscriptionRetail,2); ?></span>
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="paymethodSection no-pad">
                        <input type="hidden" name="is_address_update" value="2" id="is_address_update">
                        <input type="hidden" id="resellerID" name="resellerID" value="<?php echo $plan->resellerID ?>">
                        <input type="hidden" id="cardID" name="cardID" value="">
                        <input type="hidden" id="payOption" name="payOption" value="2">
                        
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-7 text-right">
                                    <input value="2" checked type="radio" name="formselector" class="radio_pay"></input> Checking Account 
                                </label>
                                <label class="col-md-5 ">
                                    <input value="1"  type="radio" name="formselector" class="radio_pay"></input>  Credit Card
                                </label>      
                                
                            </div>
                        </fieldset>
                        <div style="display:none;" id="ccform"> 
                            <fieldset>
                                
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="card_number">Credit Card Number<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                   
                                        <input type="text" id="card_number" name="card_number" class="form-control  CCMask" placeholder="Credit Card Number"  autocomplete="off">
                                       
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="expry">Expiry Month<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <select id="expiry" name="expiry" class="form-control">
                                            <option value="01">JAN</option>
                                            <option value="02">FEB</option>
                                            <option value="03">MAR</option>
                                            <option value="04">APR</option>
                                            <option value="05">MAY</option>
                                            <option value="06">JUN</option>
                                            <option value="07">JUL</option>
                                            <option value="08">AUG</option>
                                            <option value="09">SEP</option>
                                            <option value="10">OCT</option>
                                            <option value="11">NOV</option>
                                            <option value="12">DEC</option>
                                        </select>
                                    </div>
                                                        
                                                   
                                </div>   
                                <div class="form-group">
                                    
                                                        
                                    <label class="col-md-5 control-label" for="expiry_year">Expiry Year<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <select id="expiry_year" name="expiry_year" class="form-control">
                                            <?php 
                                                $cruy = date('y');
                                                $dyear = $cruy+15;
                                            for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                                        
                                                        
                                </div>   
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="cvv">Security Code (CVV)</label>
                                    <div class="col-md-6">
                                   
                                        <input type="text" id="cvv" name="cvv" class="form-control"  autocomplete="off" placeholder="Security Code (CVV)" />
                                        
                                    
                                    </div>
                                </div>
                            
                          
                            </fieldset>
                            
                        </div>
                                  
                        <div  id="checkingform"> 
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="customerID">Account Number<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                           <input type="text" id="acc_number" name="acc_number" class="form-control" value="" placeholder="Account Number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="route_number">Routing Number<span class="text-danger">*</span></label>
                                    <div class="col-md-6">                           
                                            <input type="text" id="route_number" name="route_number" class="form-control" placeholder="Routing Number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="customerID">Account Name<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                           <input type="text" id="acc_name" name="acc_name" class="form-control" value="" placeholder="Account Name">
                                    </div>
                                </div>
                                <input type="hidden" id="secCode" name="secCode" value="WEB">
                                
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="acct_holder_type">Account Type</label>
                                    <div class="col-md-6">
                                
                                        <select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false">
                                            <option value="checking" >Checking</option>
                                            <option value="saving"  >Saving</option>
                                            

                                        </select>
                                    </div>
                                </div>
                         
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="acct_holder_type">Account Holder Type</label>
                                    <div class="col-md-6">
                                
                                        <select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false">
                                   
                                            <option value="business"  >Business</option>
                                            <option value="personal"  >Personal</option>
                                   
                                        </select>
                                    </div>
                                </div>
                            </fieldset> 
                    
                        </div>
                     
                            
                        <input type="hidden" id="customerID11" name="customerID11"  value="" />

                        
                        
                    
                    <div class="col-md-12 col-sm-12 ">
                        <div class="display-plan">
                            <div class="single-row col-md-12 col-sm-12  no-pad">
                              
                                <div class="col-md-5 col-sm-5 ">
                                    <label class="label_left planMethodLabel"  >Plan</label>
                                </div>
                                <div class="col-md-7 col-sm-7 no-pad">
                                    <span class="span_right  planMethodValue"><?php  echo $planname; ?></span>
                                </div>
                                
                            </div>
                            <div class="single-row col-md-12 col-sm-12  no-pad">
                                <div class="col-md-5 col-sm-5 ">
                                    <label class="label_left planMethodLabel"  >Price</label>
                                </div>
                                <div class="col-md-7 col-sm-7 no-pad">
                                    <span class="span_right  planMethodValue"><?php echo "$".number_format($plan->subscriptionRetail,2); ?></span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                    
                
                
                <?php if(isset($carddata->merchantCardID) && $carddata->merchantCardID > 0){ ?>
                    <div class="exist_add colMD6 no-pad">

                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-4 col-md-4 no-pad b_align_field">
                                <label class=" control-label" for="val_username">First Name </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                <input type="text" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" placeholder="First Name" name="billing_first_name" old-value="<?php echo $carddata->billing_first_name; ?>" id="b_firstName" value="<?php echo $carddata->billing_first_name; ?>">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-4 col-md-4 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Last Name </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                <input type="text" placeholder="Last Name" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_last_name" old-value="<?php echo $carddata->billing_last_name; ?>" id="b_lastName" value="<?php echo $carddata->billing_last_name; ?>">
                                
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-4 col-md-4 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Phone Number </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                
                                <input type="text" placeholder="Phone Number" onchange="changeValue();" class="noneBorder billing_value billing_phone_number inputBoxBorder form-control" name="billing_phone_number" old-value="<?php echo $carddata->billing_phone_number; ?>" id="b_phoneNumber" value="<?php echo $carddata->billing_phone_number; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-4 col-md-4 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Email Address </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                <input type="text" placeholder="Email Address" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_email" old-value="<?php echo $carddata->billing_email; ?>" id="b_email" value="<?php echo $carddata->billing_email; ?>">
                                
                            </div>
                        </div>
                    </div> 
                    <div class="exist_add colMD6 no-pad">
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-3 col-md-3 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Address </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                
                                <input type="text" placeholder="Address" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_address" old-value="<?php echo $carddata->Billing_Addr1; ?>" id="b_address" value="<?php echo $carddata->Billing_Addr1; ?>">
                            </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-3 col-md-3 no-pad b_align_field">
                                <label class=" control-label" for="val_username">City </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                <input type="text" placeholder="City" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_city" old-value="<?php echo $carddata->Billing_City; ?>" id="b_city" value="<?php echo $carddata->Billing_City; ?>">
                                
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-3 col-md-3 no-pad b_align_field">
                                <label class=" control-label" for="val_username">State </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                <input type="text" placeholder="State" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_state" old-value="<?php echo $carddata->Billing_State; ?>" id="b_state" value="<?php echo $carddata->Billing_State; ?>">
                                
                            </div>
                        </div> 
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-3 col-md-3 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Zip Code </label>
                            </div>
                            <div class="col-sm-8 col-md-8 no-pad ">
                                
                                <input type="text" placeholder="Zip Code" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_zipcode" old-value="<?php echo $carddata->Billing_Zipcode; ?>" id="b_zipcode" value="<?php echo $carddata->Billing_Zipcode; ?>">
                            </div>
                        </div>   

                    </div>

                    <?php }else{ ?>
                    <div class="billing_section onViewBill billing_part view_mode_address new_add no-pad">
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">First Name </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                <input type="text" placeholder="First Name" class="noneBorder billing_value inputBoxBorder form-control" old-value="" id="b_firstName" name="billing_first_name" value="">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Last Name </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                <input type="text" placeholder="Last Name" class="noneBorder billing_value inputBoxBorder form-control" old-value="" id="b_lastName" name="billing_last_name" value="">
                                
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Phone Number </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                
                                 <input type="text" placeholder="Phone Number" class="noneBorder billing_value billing_phone_number inputBoxBorder form-control" name="billing_phone_number" old-value="" id="b_phoneNumber" value="">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Email Address </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                <input type="text" placeholder="Email Address" class="noneBorder billing_value inputBoxBorder form-control" old-value="" id="b_email" name="billing_email" value="">
                                
                            </div>
                        </div>
                    </div>

                    <div class="billing_section onViewBill billing_part view_mode_address new_add no-pad">
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Address </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                
                                <input type="text" placeholder="Address" class="noneBorder billing_value inputBoxBorder form-control" old-value="" id="b_address" name="billing_address" value="">
                            </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">City </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                <input type="text" placeholder="City" class="noneBorder billing_value inputBoxBorder form-control" name="billing_city" old-value="" id="b_city" value="">
                                
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">State </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                <input type="text" placeholder="State" class="noneBorder billing_value inputBoxBorder form-control" name="billing_state" old-value="" id="b_state" value="">
                                
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 form-group single_add">
                            <div class="col-sm-5 col-md-5 no-pad b_align_field">
                                <label class=" control-label" for="val_username">Zip Code </label>
                            </div>
                            <div class="col-sm-7 col-md-7 no-pad ">
                                
                                <input type="text" placeholder="Zip Code" class="noneBorder billing_value inputBoxBorder form-control" old-value="" id="b_zipcode" name="billing_zipcode" value="">
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                     
                </div>
                <?php if((isset($carddata->merchantCardID) && $carddata->merchantCardID > 0)){ 
                    echo '<div class="col-sm-12 col-md-12 bottom_right_section_exists" ></div>';
                }?>
                
                <?php if((isset($carddata->merchantCardID) && $carddata->merchantCardID > 0)){
                            echo '<div class="col-sm-12 col-md-12 bottom_right_section_new" >
                                    <div class="pull-right"><input type="submit" name="btn_process" class="btn btn-sm btn-success" value="Update"  />
                                        <button type="button" id="cancelEditMode" class="btn btn-sm btn-primary1 close1" >Cancel</button></div>
                                </div>';
                        }else{
                            echo '<div class="col-sm-12 col-md-12 bottom_right_section" >
                                    <div class="pull-right"><input type="submit" name="btn_process" class="btn btn-sm btn-success" value="Save"  /></div>
                                </div>';
                        } ?>

                    
                </form>     
                
            </div>
        <!--END Payment Info Content-->
    </div>
    <!-- END Payment Info Block -->

    <!-- Invoices Block -->
    <legend class="leg">Invoices</legend>
    <div class="full">
        <!-- Invoices Title -->
        <div class="block-title">
            
        
        </div>
        
        <!-- END Invoices Title -->

        <!-- Invoices Content -->
        <table id="company" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Invoice</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Amount</th>
                    <th class="text-center">Status</th>
                   
                </tr>
            </thead>
            <tbody>
            
                <?php 
                if(isset($invoices) && $invoices)
                {
                  foreach($invoices as $invoice)
                  {
                ?>
               
                <tr>
                    
                    <td class="text-left cust_view"><strong><a  href="<?php echo base_url(); ?>MerchantNMI/billingDetailsDownload/<?php echo $invoice['invoice']; ?>"><?php  echo ($invoice['invoiceNumber'] == null || empty($invoice['invoiceNumber']))? $invoice['invoice'] :$invoice['invoiceNumber']; ?></a></strong></td>
                        
                    <td class="text-center"><?php echo $invoice['DueDate']; ?></td>

                    <td class=" text-center">
                        <?php
                        $total = $invoice['BalanceRemaining'] + $invoice['AppliedAmount'];
                        

                       echo '<div class="hidden-xs text-center cust_view"> <a href="#pay_data_process" onclick="get_payment_data(\'' . $invoice['invoice'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">' . '$' . (number_format($total,2)). ' </a> </div>';

                           ?>
                            
                        </td>
                    
                    <td class="text-center"><?php  if($invoice['status']=='paid'){ echo 'Paid'; }else{ echo 'Unpaid'; } ?></td></td>
                </tr>
                
                <?php  }
                } ?>
                
            </tbody>
        </table>
        <!--END Invoices Content-->
    </div>
    <!-- END Invoices Block -->

<?php if(isset($carddata->merchantCardID) && $carddata->merchantCardID > 0){ ?>
<div id="card_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit Payment Info</h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span> </button>
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="form-horizontal">  
                 
                </form> 
                <form id="thestModal" method="post" action='<?php echo base_url(); ?>home/update_card_data' class="form-horizontal" >
                    <input type="hidden" name="is_address_update" value="2" >
                    <input type="hidden"  name="resellerID" value="<?php echo $plan->resellerID ?>">
                    <input type="hidden" name="cardID" value="<?php echo $carddata->merchantCardID; ?>">
                    <input type="hidden" id="payOptionModal" name="payOption" value="<?php echo $plan->payOption; ?>">
                    
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-6 text-right">
                                <input value="1" <?php echo ($plan->payOption == 1)?'checked':''; ?>  type="radio" name="formselector" class="radio_pay"></input>  Credit Card
                            </label>      
                            <label class="col-md-6">
                                <input value="2" <?php echo ($plan->payOption == 2)?'checked':''; ?> type="radio" name="formselector" class="radio_pay"></input> Checking Account 
                            </label>
                        </div>
                    </fieldset>
                    <div id="ccform" class="<?php echo ($plan->payOption == 1)?'displayPayForm':'hidePayForm'; ?>" > 
                        <fieldset>
                            
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="card_number">Credit Card Number<span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    
                                    

                                    <input type="text" id="card_number" name="card_number" class="form-control form_field CCMask" placeholder="Credit Card Number" value="" >
                                   
                                </div>
                            </div>
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="expry">Expiry Month<span class="text-danger">*</span></label>
                                <div class="col-md-2">
                                    <select id="expiry" name="expiry" class="form-control ">
                                        <option <?php echo ($carddata->CardMonth == '01' || $carddata->CardMonth == '1')?'':''; ?> value="01">JAN</option>
                                        <option <?php echo ($carddata->CardMonth == '02' || $carddata->CardMonth == '2')?'':''; ?> value="02">FEB</option>
                                        <option <?php echo ($carddata->CardMonth == '03' || $carddata->CardMonth == '3')?'':''; ?> value="03">MAR</option>
                                        <option <?php echo ($carddata->CardMonth == '04' || $carddata->CardMonth == '4')?'':''; ?> value="04">APR</option>
                                        <option <?php echo ($carddata->CardMonth == '05' || $carddata->CardMonth == '5')?'':''; ?> value="05">MAY</option>
                                        <option <?php echo ($carddata->CardMonth == '06' || $carddata->CardMonth == '6')?'':''; ?> value="06">JUN</option>
                                        <option <?php echo ($carddata->CardMonth == '07' || $carddata->CardMonth == '7')?'':''; ?> value="07">JUL</option>
                                        <option <?php echo ($carddata->CardMonth == '08' || $carddata->CardMonth == '8')?'':''; ?> value="08">AUG</option>
                                        <option <?php echo ($carddata->CardMonth == '09' || $carddata->CardMonth == '9')?'':''; ?> value="09">SEP</option>
                                        <option <?php echo ($carddata->CardMonth == '10')?'':''; ?> value="10">OCT</option>
                                        <option <?php echo ($carddata->CardMonth == '11')?'':''; ?> value="11">NOV</option>
                                        <option <?php echo ($carddata->CardMonth == '12')?'':''; ?> value="12">DEC</option>
                                    </select>
                                </div>
                                                    
                                <label class="col-md-3 control-label" for="expiry_year">Expiry Year<span class="text-danger">*</span></label>    
                                <div class="col-md-3">
                                    <select id="expiry_year" name="expiry_year" class="form-control ">
                                        <?php 
                                            $cruy = date('y');
                                            $dyear = $cruy+15;
                                        for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                            <option <?php echo ($carddata->CardYear == '20'.$i)?'':''; ?> value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>              
                            </div>  
                               
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label>
                                <div class="col-md-8">
                               
                                    <input type="text" id="cvv" name="cvv" class="form-control  form_field " value="" placeholder="Security Code (CVV)" />
                                    
                                
                                </div>
                            </div>
                        
                      
                        </fieldset>
                        
                    </div>
                              
                    <div class="<?php echo ($plan->payOption == 2)?'displayPayForm':'hidePayForm'; ?>" id="checkingform"> 
                        <fieldset>
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="customerID">Account Number<span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="text"  id="acc_number" name="acc_number" class="form-control form_field" value="" placeholder="Account Number" >
                                </div>
                            </div>
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="route_number">Routing Number<span class="text-danger">*</span></label>
                                <div class="col-md-8">                           
                                        <input type="text" id="route_number" name="route_number" class="form-control form_field" value="" placeholder="Routing Number">
                                </div>
                            </div>
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="customerID">Account Name<span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                       <input type="text" id="acc_name" name="acc_name" class="form-control form_field" value="" placeholder="Account Name">
                                </div>
                            </div>
                        
                            <input type="hidden" name="secCode" value="WEB">
                     
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="acct_holder_type">Account Type</label>
                                <div class="col-md-8">
                            
                                    <select id="acct_type" name="acct_type" class="form-control valid " aria-invalid="false">
                               
                                        <option  value="checking" <?php if(isset($carddata->accountType) && $carddata->accountType == 'checking'){ echo ""; } ?> >Checking</option>
                                        <option value="saving" <?php if(isset($carddata->accountType) && $carddata->accountType == 'saving'){ echo ""; } ?> >Saving</option>
                                    </select>
                                </div>
                            </div>
                     
                            <div class="form-group payFormGroup">
                                <label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label>
                                <div class="col-md-8">
                            
                                    <select id="acct_holder_type" name="acct_holder_type" class="form-control valid " aria-invalid="false">
                               
                                        <option value="business" <?php if(isset($carddata->accountHolderType) && $carddata->accountHolderType == 'business'){ echo ""; } ?> >Business</option>
                                        <option value="personal" <?php if(isset($carddata->accountHolderType) && $carddata->accountHolderType == 'personal'){ echo ""; } ?> >Personal</option>
                               
                                    </select>
                                </div>
                            </div>
                        </fieldset> 
                        
                    </div>
                    <fieldset>
                        <legend>Billing Address</legend>
                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">First Name </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                <input type="text" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" placeholder="First Name" name="billing_first_name" old-value="<?php echo $carddata->billing_first_name; ?>" id="b_firstName" value="<?php echo $carddata->billing_first_name; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">Last Name </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                <input type="text" placeholder="Last Name" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_last_name" old-value="<?php echo $carddata->billing_last_name; ?>" id="b_lastName" value="<?php echo $carddata->billing_last_name; ?>">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">Phone Number </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                
                                <input type="text" placeholder="Phone Number" onchange="changeValue();" class="noneBorder billing_value billing_phone_number inputBoxBorder form-control" name="billing_phone_number" old-value="<?php echo $carddata->billing_phone_number; ?>" id="b_phoneNumber" value="<?php echo $carddata->billing_phone_number; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">Email Address </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                <input type="text" placeholder="Email Address" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_email" old-value="<?php echo $carddata->billing_email; ?>" id="b_email" value="<?php echo $carddata->billing_email; ?>">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">Address </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                
                                <input type="text" placeholder="Address" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_address" old-value="<?php echo $carddata->Billing_Addr1; ?>" id="b_address" value="<?php echo $carddata->Billing_Addr1; ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">City </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                <input type="text" placeholder="City" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_city" old-value="<?php echo $carddata->Billing_City; ?>" id="b_city" value="<?php echo $carddata->Billing_City; ?>">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">State </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                <input type="text" placeholder="State" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_state" old-value="<?php echo $carddata->Billing_State; ?>" id="b_state" value="<?php echo $carddata->Billing_State; ?>">
                                
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-md-4 control-label">
                                <label class=" control-label" for="val_username">Zip Code </label>
                            </div>
                            <div class="col-sm-8 col-md-8 ">
                                
                                <input type="text" placeholder="Zip Code" onchange="changeValue();" class="noneBorder billing_value inputBoxBorder form-control" name="billing_zipcode" old-value="<?php echo $carddata->Billing_Zipcode; ?>" id="b_zipcode" value="<?php echo $carddata->Billing_Zipcode; ?>">
                            </div>
                        </div> 
                    </fieldset>
                        
                    <input type="hidden" id="customerID11" name="customerID11"  value="" />
                    <div class="pull-right">
                     <input type="submit" name="btn_process" class="btn btn-sm btn-success" value="Update">
                     <button type="button" id="cancelEditModeModal" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>

                  
                    </div><br><br>
                </form>
            </div>          
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<?php } ?>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#company').dataTable({
                columnDefs: [
                    
                ],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

$('#viewEditMode').click(function(){
    
   
    $('#is_address_update').val(2);


});
function changeValue(){
    $('.bottom_right_section_new').show();
     $('.bottom_right_section_exists').hide();
}
$('#cancelEditMode').click(function(){

    $('#view_mode').show();
    $('#plan_view').show();
    $('#plan_edit').hide();
    $('.bottom_right_section_new').hide();
     $('.bottom_right_section_exists').show();
    $('.billing_section').removeClass('onEditBill');
    $('.billing_section').addClass('onViewBill');

    $('.form_field').val('');

    $('#is_address_update').val(1);

    $('.single_add').removeClass('has-error');
    $('.payFormGroup').removeClass('has-error');

    $('.help-block').remove();

    /*Billing infor return back*/
    var b_firstName = $('#b_firstName').attr('old-value');
    $('#b_firstName').val(b_firstName);

    var b_lastName = $('#b_lastName').attr('old-value');
    $('#b_lastName').val(b_lastName);

    var b_phoneNumber = $('#b_phoneNumber').attr('old-value');
    $('#b_phoneNumber').val(b_phoneNumber);

    var b_email = $('#b_email').attr('old-value');
    $('#b_email').val(b_email);

    var b_address = $('#b_address').attr('old-value');
    $('#b_address').val(b_address);

    var b_zipcode = $('#b_zipcode').attr('old-value');
    $('#b_zipcode').val(b_zipcode);

    var b_city = $('#b_city').attr('old-value');
    $('#b_city').val(b_city);

    var b_state = $('#b_state').attr('old-value');
    $('#b_state').val(b_state);

    /**/

    $('.planMethodLabel').removeClass('edit_plan_name');
    $('.planMethodValue').removeClass('edit_plan_price');
    $('#edit_mode').hide();
});
$('#cancelEditModeModal').click(function(){

    $('#view_mode').show();
    $('#plan_view').show();
    $('#plan_edit').hide();
    $('.bottom_right_section_new').hide();
     $('.bottom_right_section_exists').show();
    $('.billing_section').removeClass('onEditBill');
    $('.billing_section').addClass('onViewBill');

    $('.form_field').val('');

    $('#is_address_update').val(1);


    $('.single_add').removeClass('has-error');
    $('.payFormGroup').removeClass('has-error');

    $('.help-block').remove();

    $('.planMethodLabel').removeClass('edit_plan_name');
    $('.planMethodValue').removeClass('edit_plan_price');
    $('#edit_mode').hide();
});

$('.radio_pay').click(function(){
  var method = $(this).val(); 
   if(method==1)
   {
       $('#checkingform').hide();
       $('#payOption').val(1);
       $('#payOptionModal').val(1);
       $('#ccform').show();
   }else if (method==2){
         $('#checkingform').show();
         $('#payOption').val(2);
         $('#payOptionModal').val(2);
       $('#ccform').hide();
   }else {
       
     }         
});
function get_payment_data(invoice) {



    if (invoice != "") {


        $.ajax({

            type: 'POST',
            url: '<?php echo  base_url(); ?>ajaxRequest/view_invoice_transaction',
            data: { 'invoiceID': invoice, 'action': 'invoice' },
            dataType: 'json',
            success: function (response) {
                
                $('#pay-content-data').html(response.transaction);

            }

        });

    }

}
</script>



<style>
.bottom_right_section_exists{
    margin-bottom: 2%;
}
.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 

</div>
<!-- Load and execute javascript code used only in this page -->

<div id="company_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            
            <div class="modal-header text-center">
                <h2 class="modal-title">App Details</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_company"  style="height: 300px; min-height:300px;  overflow: auto; " >
            
            </div>
            <hr>
                <div class="pull-right">
                
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                                    
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- END Page Content