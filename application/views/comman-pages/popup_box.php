<style type="text/css">
    #invoice_number_chosen{
        width: 100% !important;
    }
</style>
<?php 

    $surchargePercentage = 0;
    if (!isset($merchID) || !$merchID) {
        $CI = &get_instance();
        if ($CI->session->userdata('logged_in')) {
			$user_info 	= $CI->session->userdata('logged_in');
			$merchID 				= $user_info['merchID'];
		}
		if ($CI->session->userdata('user_logged_in')) {
			$user_info 	= $CI->session->userdata('user_logged_in');
			$merchID 				= $user_info['merchantID'];
		}
    }
    if(merchant_gateway_allowed(['merchID' => $merchID])){
        $isGatewayAllowedStyle = "display : block";
    } else {
        $isGatewayAllowedStyle = "display : none";
    }

    if (!isset($isCardGatewayExists)) {
        $isCardGatewayExists = 1;
    }

    if (!isset($isEcheckGatewayExists)) {
        $isEcheckGatewayExists = 1;
    }

    if($isCardGatewayExists && $isEcheckGatewayExists){
        $showAllGateway = 0; // i.e Credit card and Echeck both
    } elseif($isCardGatewayExists){
        $showAllGateway = 1; // i.e Credit card only
    } else {
        $showAllGateway = 2; // i.e Echeck Only
    }
?>
<div id="invoice_schedule" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Schedule Payment</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <div id="schDiv"></div>
                 <form id="Qbd_form1_schedule" method="post" onsubmit="return false;" class="form-horizontal" >
                     
                 
				 <?php /* <p>Do you really want to schedule this invoice? Please schedule date to automatically process payment.</p>  */ ?>
                    
                    <div class="form-group">
                     <label class="col-md-4 control-label" for="card_list">Schedule Date</label>
                        <div class="col-md-8">
                      <div class="input-group  input-date">
                            <input type="text" id="schedule_date1" name="schedule_date" class="form-control col-md-8 input-datepicker " data-date-format="mm/dd/yyyy" placeholder="Schedule Date" value="">
                        </div>
                      </div>    
                    </div>
                    <?php if($showAllGateway == 0){ ?>

                    	 <div class="form-group">
                    	  <label class="col-md-4 control-label" for="card_list">Payment Method</label>
                    	  <div class="col-md-8">
                          <label class="col-md-6 row text-left"><input value="1" checked onclick="get_ach_details(this);" checked  type="radio" name="sch_method" class="radio_pay"></input>  Credit Card </label>      
                                            <label class="col-md-6 row"><input value="2" type="radio" onclick="get_ach_details(this);" name="sch_method" class="radio_pay"></input> Checking Account </label>
		               </div>
                    </div>
                    <?php } else{ ?>
                        <input type="hidden" name="sch_method" value="<?php echo $showAllGateway;  ?>">
                    <?php } ?>

                    <div id="pay_data">
                      <div class="form-group ">
                      
                        <label class="col-md-4 control-label" for="card_list">Gateway</label>
                        <div class="col-md-8">
                            <select id="sch_gateway" name="sch_gateway"   class="form-control">
                                <option value="" >Select Gateway</option>
                                <?php if(isset($gateway_datas) && !empty($gateway_datas) ){
                                        foreach($gateway_datas['gateway'] as $gateway_data){
                                        ?>
                                     <option value="<?php echo $gateway_data['gatewayID'];  ?>" <?php if($gateway_data['set_as_default']=='1')echo "selected ='selected' ";  ?> ><?php echo $gateway_data['gatewayFriendlyName']; ?></option>
                                        <?php } } ?>
                            </select>
                            
                        </div>
                    </div> 
                    
                    <div class="form-group ">
                      
                        <label class="col-md-4 control-label" for="card_list">Card</label>
                        <div class="col-md-8">
                            <select id="schCardID" name="schCardID"  onchange="create_card_schedule_data();" class="form-control">
                                <!-- <option value="" >Select Card</option> -->
                              
                            </select>
                            
                        </div>
                    </div> 
                    </div>
                    <div class="form-group ">
                      
                        <label class="col-md-4 control-label" for="card_list">Amount</label>
                        <div class="col-md-8">
                           <input type="text" id="schAmount" name="schAmount"   class="form-control" value="" placeholder="0.00" />
                              
                            
                        </div>
                         
                    </div>
                         <div class="card_div_csh" style="display:none;">
                           
                               
                       </div>
                         <div class="form-group">
								<label class="col-md-4 control-label" for="reference"></label>
                                 <div class="col-md-6">
								<input type="checkbox" id="setMail" name="setMail" class="set_checkbox"   />  Send Customer Receipt
								 </div>
						</div> 
					
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="scheduleID" name="scheduleID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="qbd_schedule"  name="btn_cancel" onclick="qbd_schedule_payment();" class="btn btn-sm btn-info" value="Schedule"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                  </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<?php if($page_num!="invoices_xero") 
{ ?>

<div id="set_tempemail" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title text-center custom-modal-head-title">Send Email</h2>
           <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"></label>
                   <form id="form-validation1" action="<?php echo base_url(); ?>Integration/Settings/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
				  
				  
			   <input type="hidden" id="customertempID" name="customertempID" value=""> 
					<input type="hidden" id="tempCompanyID" name="tempCompanyID" value=""> 
				<input type="hidden" id="tempCompanyName" name="tempCompanyName" value=""> 
				
			
			
                                
										
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                             <?php
                                                      $option='<option value="">Select Template</option>';
                                                        foreach($template_data as $template)
                                                    {
                                                        $option.='<option value="'.$template['templateID'].'" >'.$template['templateName'].'</option>';
                                                    }    
                                                ?>
                                            <select id="type" name="type" class="form-control">
                                                <?php echo $option; ?> 
                                            </select> 
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group" id="reply_div">
                                        <label class="col-md-3 control-label" for="replyEmail">Reply To Email</label>
                                        <div class="col-md-7">
                                            <input type="text" id="replyEmail" name="replyEmail" class="form-control" value="<?php if (isset($templatedata)) echo ($templatedata['replyTo']) ? $templatedata['replyTo'] : ''; ?>" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group" style="display:none" id="from_email_div">
                                        <label class="col-md-3 control-label" for="templteName">From Email</label>
                                        <div class="col-md-7">
                                            <input type="text" id="fromEmail" name="fromEmail"  value="<?php  echo $from_mail; ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group" id='display_name_div' style='display:none'>
                                        <label class="col-md-3 control-label" for="templteName">Display Name</label>
                                        <div class="col-md-7">
                                            <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="<?php echo $mailDisplayName; ?>" placeholder="">
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                           <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                           <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                                            <!-- <a href="javascript:void(0);" id="open_reply">Set Reply-To</a> -->
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">Cc these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">Bcc these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
									
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
										
										<input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="">
                                    </div>
										
                                    </div>
                                     
                                      <div class="form-group">
									  
									  
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-7 col-md-offset-3 align-right">
                                        
                                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                      
                                    </div>
                                </div>  
                           </form>
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>  


<?php } ?>




<div id="invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span> 
                </button>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->                           
            <div class="modal-body">
                 <form id="thest_pay" method="post" action="<?php echo base_url().'Integration/Payments/pay_invoice'; ?>" class="form-horizontal card_form" >
                     
                 
                     <!--<p>Do you really want to process this invoice before due date?</p> -->
                    <?php if($showAllGateway == 0){ ?>
                     <div class="form-group">
                    	                <label class="col-md-4 control-label" for="card_list">Payment Method</label>
                    	                <div class="col-md-8">
                                            <label class="col-md-6 row text-left"><input value="1" onclick="get_ach_details_pay(this);" checked type="radio" name="sch_method" class="radio_pay"></input>  Credit Card </label>      
                                            <label class="col-md-6 row"><input value="2" type="radio" onclick="get_ach_details_pay(this);" name="sch_method" class="radio_pay"></input> Checking Account </label>
                                        </div>
                                    </div>
                    <?php } else{ ?>
                        <input type="hidden" name="sch_method" value="<?php echo $showAllGateway;  ?>">
                    <?php } ?>

                                    <div id="pay_data_1">
                                    <?php 
                                        if(!isset($defaultGateway) || !$defaultGateway){
                                    ?>
                                   
                                    
									<div class="form-group">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_url();"  class="form-control">
                                                        <option value="" >Select Gateway</option>
														<?php if(isset($gateway_datas) && !empty($gateway_datas) ){
																foreach($gateway_datas ['gateway'] as $gateway_data){
                                                                    if($gateway_data['set_as_default']=='1'){
                                                                        $surchargePercentage = ($gateway_data['isSurcharge'] == 1) ? $gateway_data['surchargePercentage'] : 0 ;
                                                                    }
																?>
                                                            <option value="<?php echo $gateway_data['gatewayID'];  ?>" <?php if($gateway_data['set_as_default']=='1')echo "selected ='selected' ";  ?> ><?php echo $gateway_data['gatewayFriendlyName']; ?></option>
																<?php } } ?>
                                                    </select>
													
                                                </div>
                                            </div>
                                            <?php 
                                                } else {
                                                    $surchargePercentage = ($defaultGateway['isSurcharge'] == 1) ? $defaultGateway['surchargePercentage'] : 0 ;
                                                ?>
                                                    <input type="hidden" name="gateway" value="<?php echo $defaultGateway['gatewayID'];  ?>">
                                            <?php }  ?>	
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
												
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <!-- <option value="" >Select Card</option> -->

                                                        
                                                    </select>
														
                                                </div>
                                            </div>
                                    </div>    
                                     <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>
                                            <div class="panel panel-info notice_box surchargeNoticeClass1" style="display:none"> <div class="panel-heading"> <h3 class="panel-title">Surcharge Notice</h3> </div> <div class="panel-body surchargeNoticeText"></div> </div>    
                       <div class="card_div"style="display:none" ></div>
					
				    
                     <div class="form-group">
					     <label class="col-md-4 control-label" for="reference"></label>
                        <div class="col-md-8 align-right reciptCheckboxGroup">
				            <span>Send Customer Receipt</span><input type="checkbox" id="setMail" name="setMail" class="set_checkbox"   />
				        </div>
			       	</div> 
					
			 
                    <div class="pull-right">
                        <input type="hidden" id="invoiceProcessID" name="invoiceProcessID"   value="" />
                        <input type="hidden" id="customerProcessID" name="customerProcessID"  value="" />
        			    <input type="submit" id="qbd_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="card_edit_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit/Delete Payment Info</h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
		
				
				 	 
				 
     <table  class="table table-bordered table-striped table-vcenter">
	 <tr>
	     <th class="text-left hidden-xs">
	         Payment Type 
	     </th>
	      <th class="text-right hidden-xs">
	          Friendly Name
	     </th>
	      <th class="text-right hidden-xs">
	         Last 4 Digits 
	     </th>
	      <th class="text-right hidden-xs">
	         Action
	     </th>
	 </tr>
	 
         <?php
            if(!empty($card_data_array)){
     		 foreach($card_data_array as $cardarray){ 
     		     $link = base_url().'Integration/Payments/delete_card_data/';
     		     $link1="#custom-modal";
                 $classDefault = '';
                if($cardarray['is_default'] == 1){
                    $classDefault = 'boldText';
                }
             $mss= "do you really want this";
                     if(!empty($cardarray['CardNo'])){
                    echo '<tr class="'.$classDefault .'">'.
                    '<td class="text-left visible-lg">'.$cardarray['CardType'].'</td>'.
                    '<td class="text-right hidden-xs">'.$cardarray['customerCardfriendlyName'].'</td>'.
                    '<td class="text-right visible-lg">'.$cardarray['CardNo'].'</td>'.
                    
                    '<td class="text-right hidden-xs"><div class="btn-group btn-group-xs">'.
							'<a href="javascript:void(0);" data-toggle="tooltip" title="" class="btn btn-default" onclick="set_edit_card('.$cardarray['CardID'].');" data-original-title="Edit Card"><i class="fa fa-edit"></i></a>'.
								'<a href="'.$link1.'"  onclick="delete_card(\''.$link.'\',\''.$cardarray['CardID'].'\',\''.$cardarray['customerListID'].'\');"    data-backdrop="static" data-keyboard="false" data-toggle="modal"  class="btn btn-danger"  data-original-title="Delete Card"><i class="fa fa-times"></i></a>'.
                          
                          
						'</div> </td>'.
                '</tr>';  
            
		 }else{         echo '<tr class="'.$classDefault .'">'.
		            '<td class="text-left visible-lg">'.$cardarray['CardType'].'</td>'.
                    '<td class="text-right hidden-xs">'.$cardarray['accountName'].'</td>'.
                    '<td class="text-right visible-lg">'.$cardarray['accountNumber'].'</td>'.
                   
                    '<td class="text-right hidden-xs"><div class="btn-group btn-group-xs">'.
							'<a href="#" data-toggle="tooltip" title="" class="btn btn-default" onclick="set_edit_card('.$cardarray['CardID'].');" data-original-title="Edit Card"><i class="fa fa-edit"></i></a>'.
								'<a href="'.$link1.'"  onclick="delete_card(\''.$link.'\',\''.$cardarray['CardID'].'\',\''.$cardarray['customerListID'].'\');"    data-backdrop="static" data-keyboard="false" data-toggle="modal"  class="btn btn-danger"  data-original-title="Delete Card"><i class="fa fa-times"></i></a>'.
                          
                          
						'</div> </td>'.
                '</tr>';  
		
			} } 
			}else{ 
		echo  '<tr><td colspan="4">No Payment Data</td></tr>'; 
		
		 } ?>
       </table>	

		<form id="thest_form" method="post" style="display:none;" action='<?php echo base_url(); ?>Integration/Payments/update_card_data' class="form-horizontal" >
				 	<div style="display:none;" id="editccform"> 
				 <fieldset>
			        
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                                 <div id="m_card_id"><span id="m_card"></span> <input type='button' id="btn_mask" class="btn btn-default btn-sm" value="Edit Card" /></div>
                                <input type="hidden" disabled  id="edit_card_number" name="edit_card_number" class="form-control">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                   	<select id="edit_expiry" name="edit_expiry" class="form-control">
                                                        <option value="1">JAN</option>
                                                        <option value="2">FEB</option>
                                                        <option value="3">MAR</option>
                                                        <option value="4">APR</option>
                                                        <option value="5">MAY</option>
													    <option value="6">JUN</option>
                                                        <option value="7">JUL</option>
                                                        <option value="8">AUG</option>
                                                        <option value="9">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
												
												    <label class="col-md-3 control-label" for="edit_expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                   	<select id="edit_expiry_year" name="edit_expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+25;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                                </div>
												
												
                                            </div>     
    				<div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_cvv" name="edit_cvv" class="form-control" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                 </div>   
                    
                    	<div style="display:none;" id="editcheckingform"> 
			  <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Account Number</label>
                        <div class="col-md-8">
                               <input type="text" id="edit_acc_number" name="edit_acc_number" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Routing Number</label>
                        <div class="col-md-8">                           
                                <input type="text" id="edit_route_number" name="edit_route_number" class="form-control">
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Account Name</label>
                        <div class="col-md-8">
                            <input type="text" id="edit_acc_name" name="edit_acc_name" class="form-control" value="">
                            <input type="hidden" id="edit_secCode" name="edit_secCode" class="form-control" value="ACK">
                        </div>
                    </div>
					
					
                <div class="form-group">
                    <label class="col-md-4 control-label" for="acct_type">Account Type</label>
                    <div class="col-md-6">
                        
                            <select id="edit_acct_type" name="edit_acct_type" class="form-control valid" aria-invalid="false">
                                <option value="checking" <?php if(isset($reseller['accountType']) && $reseller['accountType']=='checking'){ echo "Seleced"; } ?> >Checking</option>
                                <option value="saving" <?php if(isset($reseller['accountType']) && $reseller['accountType']=='saving'){ echo "Seleced"; } ?> >Saving</option>
                        </select>
                    </div>
                </div>
				 
				 	<div class="form-group">
                    <label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label>
                    <div class="col-md-6">
                        
                            <select id="edit_acct_holder_type" name="edit_acct_holder_type" class="form-control valid" aria-invalid="false">
                                <option value="business" <?php if(isset($reseller['accountHolderType']) && $reseller['accountHolderType']=='bussiness'){ echo "Seleced"; } ?> >Business</option>
                                <option value="personal" <?php if(isset($reseller['accountHolderType']) && $reseller['accountHolderType']=='personal'){ echo "Seleced"; } ?> >Personal</option>                           
                        </select>
                    </div>
                </div>
                
			  
				</fieldset>	
					
			
			
			</div>
			
          
                    <fieldset>
                        <legend>Billing Address</legend>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                        <div class="col-md-8">
                       		   <input type="text" id="baddress1" name="baddress1" class="form-control" value="" placeholder="Address Line 1">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                        <div class="col-md-8">
                       		   <input type="text" id="baddress2" name="baddress2" class="form-control" value="" placeholder="Address Line 2">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">City</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcity" name="bcity" class="form-control" value="" placeholder="City">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">State/Province</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bstate" name="bstate" class="form-control" value="" placeholder="State/Province">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bzipcode" name="bzipcode" class="form-control" value="" placeholder="ZIP Code">
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Country</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcountry" name="bcountry" class="form-control" value="" placeholder="Country">
                        </div>
                        </div>
                        
                          <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Phone Number</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcontact" name="bcontact" class="form-control" value="" placeholder="Phone Number">
                        </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username"></label>
                            <div class="col-md-8">
                                <div class="defaultAlign">
                                    <input type="checkbox" name="defaultMethod" id="defaultMethod" value="0"> Set as Default
                                </div>
                               
                            </div>
                        </div>
                    </fieldset>
                                          <input type="hidden" id="edit_cardID" name="edit_cardID"  value="" />
                  <div class="pull-right">
				     <input type="submit" id="edtbtn_process" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                     <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			    </form>		
	 <!-- panel-group -->
				
				
                	
				<div id="can_div">
				 <div class="pull-right">
				    
                     <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
				 </div>
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!------Show Payment Data------------------->

<!-- Load and execute javascript code used only in this page -->


<div id="card_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add Payment Info</h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
        <form class="form-horizontal">	
         
		</form>	
                
                 <form id="thest" method="post" action='<?php echo base_url(); ?>Integration/Payments/add_card_data' class="form-horizontal" >
         <fieldset>
		 <div class="form-group">
		<label class="col-md-6 text-right"><input value="1" checked type="radio" name="formselector" class="radio_pay"></input>  Credit Card </label>      
        <label class="col-md-6"><input value="2" type="radio" name="formselector" class="radio_pay"></input> Checking Account </label>
		</div>
		</fieldset>
				 <div id="ccform">    
				 <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Customer Name</label>
                        <div class="col-md-8">
                               <input type="text" id="customername" name="customername" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="card_number" name="card_number" class="form-control">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                    <select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
                                                        <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
                                                        <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
                                                
                                                    <label class="col-md-3 control-label" for="expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                    <select id="expiry_year" name="expiry_year" class="form-control">
                                                    <?php 
                                                        $cruy = date('y');
                                                        $dyear = $cruy+25;
                                                    for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
                                                    <?php } ?>
                                                       </select>
                                                </div>
                                                
                                                
                                            </div>     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="cvv" name="cvv" class="form-control" />
                                
                            
                        </div>
                    </div>
                    
               
                    </fieldset>
					
					  </div> 
			<div style="display:none;" id="checkingform"> 
			  <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Account Number</label>
                        <div class="col-md-8">
                               <input type="text" id="acc_number" name="acc_number" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Routing Number</label>
                        <div class="col-md-8">                           
                                <input type="text" id="route_number" name="route_number" class="form-control">
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Account Name</label>
                        <div class="col-md-8">
                            <input type="text" id="acc_name" name="acc_name" class="form-control" value="">
                            <input type="hidden" id="secCode" name="secCode" class="form-control" value="ACK">
                        </div>
                    </div>
					
				 
                <div class="form-group">
                    <label class="col-md-4 control-label" for="acct_holder_type">Account Type</label>
                    <div class="col-md-6">
                        <select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false">
                            
                            <option value="checking" <?php if(isset($reseller['accountHolderType']) && $reseller['accountHolderType']=='checking'){ echo "Seleced"; } ?> >Checking</option>
                            <option value="saving" <?php if(isset($reseller['accountHolderType']) && $reseller['accountHolderType']=='saving'){ echo "Seleced"; } ?> >Saving</option>
                            
                        </select> 
                        
                    </div>
                </div>
				 
				 	<div class="form-group">
                    <label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label>
                    <div class="col-md-6">
                        <select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false">
                            
                            <option value="business" <?php if(isset($reseller['accountType']) && $reseller['accountType']=='bussiness'){ echo "Seleced"; } ?> >Business</option>
                            <option value="personal" <?php if(isset($reseller['accountType']) && $reseller['accountType']=='personal'){ echo "Seleced"; } ?> >Personal</option>
                            
                        </select>
                            
                    </div>
                </div>
                
			  
				</fieldset>	
					
			
			
			</div>
			
                  
                    <fieldset>
                        <legend>Billing Address</legend>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                        <div class="col-md-8">
                       		   <input type="text" id="address1" name="address1" class="form-control" value="<?php if(isset($customer) && !empty($customer->BillingAddress_Addr1)){ echo $customer->BillingAddress_Addr1; }  ?>">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                        <div class="col-md-8">
                       		   <input type="text" id="address2" name="address2" class="form-control" value="<?php if(isset($customer) && !empty($customer->BillingAddress_Addr2)){ echo $customer->BillingAddress_Addr2; }  ?>">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">City</label>
                        <div class="col-md-8">
                       		   <input type="text" id="city" name="city" class="form-control" value="<?php if(isset($customer) && !empty($customer->BillingAddress_City)){ echo $customer->BillingAddress_City; }  ?>">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">State/Province</label>
                        <div class="col-md-8">
                       		   <input type="text" id="state" name="state" class="form-control" value="<?php if(isset($customer) && !empty($customer->BillingAddress_State)){ echo $customer->BillingAddress_State; }  ?>">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                        <div class="col-md-8">
                       		   <input type="text" id="zipcode" name="zipcode" class="form-control" value="<?php if(isset($customer) && !empty($customer->BillingAddress_PostalCode)){ echo $customer->BillingAddress_PostalCode; }  ?>">
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Country</label>
                        <div class="col-md-8">
                       		   <input type="text" id="country" name="country" class="form-control" value="<?php if(isset($customer) && !empty($customer->BillingAddress_Country)){ echo $customer->BillingAddress_Country; }  ?>">
                        </div>
                        </div>
                        
                          <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Phone Number</label>
                        <div class="col-md-8">
                       		   <input type="text" id="contact" name="contact" class="form-control" value="<?php if(isset($customer) && !empty($customer->Phone)){ echo $customer->Phone; }  ?>">
                        </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username"></label>
                            <div class="col-md-8">
                                <div class="defaultAlign">
                                    <input type="checkbox" name="defaultMethod" id="defaultMethod" value="0"> Set as Default
                                </div>
                               
                            </div>
                        </div>
                    </fieldset>  
                    
                        
                        
          
                     <input type="hidden" id="customerID11" name="customerID11"  value="" />
                  <div class="pull-right">
                     <input type="submit"  name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                     <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			
			
			</form>
			
			
			
			
</div>			
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!------------------  View email history Popup popup ------------------>

<div id="view_history" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> View Details </h2>
                 <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> </button>
				<div class="modal-body">
        
			<div id="data_history" style=" overflow: auto; ">
			</div>	
                 
            </div>
            
	</div>

               <div class="modal-footer">
                 <button type="button"  align="right" class="btn btn-sm pull-right btn-primary1 close1" data-dismiss="modal"> Close </button>
				 
			</div>
			
					
            </div>
			
			
	</div>
			
            <!-- END Modal Body -->
        </div>
<div id="qbd_invoice_multi_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoices</h2>
                <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span> 
                </button>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">  
                 <form id="thest_pay1" method="post" action="<?php echo base_url().'Integration/Payments/pay_multi_invoice'; ?>" class="form-horizontal " >
                     <input type="hidden" id="customermultiProcessID" name="customermultiProcessID" class="form-control"  value="<?php echo $this->uri->segment('4'); ?>" />
                    
                                    
                                    <?php 
                                        if(!isset($defaultGateway) || !$defaultGateway){
                                    ?>
                                    
                                    <div class="form-group">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway1" name="gateway1" onchange="set_common_url_multi_pay();"  class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if(isset($gateway_datas) && !empty($gateway_datas) ){
                                                                foreach($gateway_datas ['gateway'] as $gateway_data){
                                                                    if($gateway_data['set_as_default']=='1'){
                                                                        $surchargePercentage = ($gateway_data['isSurcharge'] == 1) ? $gateway_data['surchargePercentage'] : 0 ;
                                                                    } 
                                                                ?>
                                                            <option value="<?php echo $gateway_data['gatewayID'];  ?>" <?php if($gateway_data['set_as_default']=='1')echo "selected ='selected' ";  ?> ><?php echo $gateway_data['gatewayFriendlyName']; ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            
                                            <?php 
                                                } else { 
                                                    $surchargePercentage = ($defaultGateway['isSurcharge'] == 1) ? $defaultGateway['surchargePercentage'] : 0 ;
                                                ?>
                                                    <input type="hidden" name="gateway1" value="<?php echo $defaultGateway['gatewayID'];  ?>">
                                            <?php }  ?>	

                                          <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
                                                
                                                    <select id="CardID1" name="CardID1"  onchange="create_card_multi_data();"  class="form-control">
                                                        <!-- <option value="" >Select Card</option> -->

                                                        
                                                    </select>
                                                        
                                                </div>
                                            </div>
                                            <input type="hidden" id="totalPay" name="totalPay" class="form-control" />
                                             <!-- <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Total Payment</label>
                                                <div class="col-md-6">
                                                
                                                    <input type="text" id="totalPay" name="totalPay" class="form-control" />
                                                      
                                                        
                                                </div>
                                            </div> -->
                                                
                                          
                        <div id="inv_div" style="padding-top: 10px;"></div>
                        <div class="invoiceTotal" style="padding-bottom: 10px;">
                            <div class="form-group">
                                <div class="col-md-2 text-center"></div>
                                <div class="col-md-2 text-left"></div>
                                
                                <div class="col-md-5 text-right"><b>Total Payment</b></div>
                                <div class="col-md-3 text-left"><b>$<span id="totalMultiInvoiceTotal">0.00</span></b>
                            </div>
                           </div>
                        </div>                        
                       <div class="card_div" style="display:none"></div>
                                                    
                       <div class="panel panel-info notice_box surchargeNoticeClass3" style="display:none"> <div class="panel-heading"> <h3 class="panel-title">Surcharge Notice</h3> </div> <div class="panel-body surchargeNoticeText"></div> </div>    
                 
                    
                    
                    <div class="alignRIghtReciept">
                     
                        <div class="col-md-12 secReciept">
                            <span>Send Customer Receipt</span><input type="checkbox" id="setMail" name="setMail" class="set_checkbox"   />  
                        </div>
                    </div> 
                    <div class="pull-right">
                     <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">
   
                     <input type="submit"  name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<div id="set_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header text-center">
        <h2 class="modal-title">Add Offline Payment</h2>


      </div>
      <!-- END Modal Header -->

      <!-- Modal Body -->
      <div class="modal-body">
        <form id="del_ccjkck_o" method="post" action='<?php echo base_url(); ?>Integration/Payments/offline_payments' class="form-horizontal">
          <div class="form-group">
            <label class="col-md-4 control-label">Payment Mode</label>
            <div class="col-md-8">  
              <div class="input-group">
                <label class="control-label"><input type="radio" name="pay_type" value="0" class="radio  radio-inline" checked="checked"> Check</label>
              </div>
            </div>
          </div>
          <div id="chk_div">
            <div class="form-group">
              <label class="col-md-4 control-label">Date</label>
              <div class="col-md-8">
                <div class="input-group input-date">
                  <input type="text" id="payment_date" name="payment_date" class="form-control input-datepicker" data-date-format="mm/dd/yyyy" placeholder="" value="">

                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Check Number</label>
              <div class="col-md-8">
                <div class="input-group">
                  <input type="text" id="check_number" placeholder="" size="40" name="check_number" class="form-control">
                </div>
              </div>
            </div>

          </div>
          <div id="credit_div" style="display:none;">
            <?php if (isset($credits) &&  !empty($credits)) { ?>
              <div class="form-group">
                <label class="col-md-4 control-label">Credit</label>

                <div class="col-md-8">
                  <table id="cr_div" class="table table-stripe">

                  </table>
                </div>

              </div>

            <?php } ?>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Amount</label>
            <div class="col-md-8">
              <div class="input-group">
                <input type="text" id="inv_amount" placeholder="" size="40" name="inv_amount" class="form-control">
              </div>
            </div>
          </div>

          <?php /* <div class="form-group">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-8">
                      <div class="input-group">
                          <input type="checkbox" id="sendemail" name="sendemail" value="send_mail">  Tick to Send Confirmation Email
                        </div>
                      </div>
                      
                    </div> */ ?>


          <div class="form-group">

            <div class="col-md-8">

              <input type="hidden" id="subscID" name="subscID" class="form-control" value="" />
              <input type="hidden" id="status" name="status" class="form-control" value="Paid" />
            </div>
          </div>



          <div class="pull-right">
            <input type="submit" class="btn btn-sm btn-success" value="Add Payment" />
            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
          </div>
          <br />
          <br />

        </form>

      </div>
      <!-- END Modal Body -->
    </div>
  </div>
</div>

<div id="amountModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header text-center">
                <h2 class="modal-title">Amount</h2>
            </div>
            <div class="modal-body clearfix">
                
                <div id="table2">
                </div>
                <div class="form-group alignTableInvoiceListTotal">
               
                    <div class="col-md-2 text-center"></div>
                    <div class="col-md-2 text-left"></div>
                    <div class="col-md-3 text-right"></div>
                    <div class="col-md-2 text-right totalTextLeft" >Total</div>
                    <div class="col-md-3 text-left totalTextRight" >
                        $<span id="totalamount1">0.00</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Select</button>
                <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="invDefaultsurchargeRate" value='<?php echo $surchargePercentage; ?>'>
<input type="hidden" id="invDefaultcardSurchargeValue" value='0'>
<input type="hidden" id="ccChecked" value='<?php echo $showAllGateway; ?>'>