<style>
	.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 270px !important;
	}
</style>
<?php
	$this->load->view('alert');
?>
<div id="page-content">
	
	<legend class="leg"> Capture / Void</legend>
	<div class="full">
       
        <!-- Form Validation Example Title -->
         <table id="capture_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Customer Name</th>
                    <th class="text-right">Amount</th>
                    <th class="text-right hidden-xs">Date</th>
                    <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{  
				?>
				<tr>
			        <?php if($plantype) { ?>	
					<td class="text-left"><?php echo $transaction['fullName']; ?></a></td>
					<?php } else { ?>
					<td class="text-left cust_view"><a href="<?php echo base_url('Integration/Customers/customer_detail/'.$transaction['Customer_ListID']); ?>"><?php echo $transaction['fullName']; ?></a></td>
					<?php } ?>
					
					<td class="text-right cust_view"><a href="#pay_data_process"   onclick="set_common_transaction_data('<?php  echo $transaction['transactionID']; ?>','title');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$<?php echo ($transaction['transactionAmount'])?number_format($transaction['transactionAmount'], 2):'0.00'; ?></a></td>
					
				
					
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					
                    <td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					
				   
					<td class="text-center">
                        <div class="">
                            <div class="btn-group dropbtn">
                                <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                <ul class="dropdown-menu text-left">
                                    <li><a href="#payment_capturemod" class=""  onclick="set_capture_pay('<?php  echo $transaction['transactionID']; ?>','<?php  echo $transaction['transactionGateway']; ?>' );"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Capture</a></li>
                                    <?php if($transaction['transactionGateway'] != 5){ ?>
                                    <li><a href="#payment_voidmod" class=""  onclick="set_void_pay('<?php  echo $transaction['id']; ?>','<?php  echo $transaction['transactionGateway']; ?>' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>
                                <?php } ?>
                                </ul>
                            </div>
					    </div>
					</td>
				</tr>
				
				<?php } }
					else {echo'<tr><td colspan="5"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                
                </tr>'; }  
			
				?>
				
			</tbody>
        </table>
    </div>
    
   
	 <div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>  
     <div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>   
   </div> 
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#capture_page').dataTable({
                columnDefs: [
                    { type: 'date', targets: [2] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', '');
        }
    };
}();



</script>

    <script>
	 //	$(function(){  nmiValidation.init(); });
	  	function set_capture_pay(txnid, txntype){
    		if(txnid !=""){
                $('#txnID').val(txnid);	 
            }
    	}
    
    function set_void_pay(txnid, txntype){
        if(txnid !=""){
            $('#voidPaytxnID').val(txnid);
        }
    }
    </script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
 
	
    <div id="payment_capturemod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Capture Authorized Transaction</h2>
                       
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action="<?php echo base_url()?>Integration/Payments/create_customer_capture" class="form-horizontal" >
                    	<p id="message_data"> Do you wish to proceed with capture for this transaction? The payment will be captured if you click "Capture" below.</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
								<input type="hidden" id="usatxnID" name="usatxnID" class="form-control"  value="" />
                                <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
								 <input type="hidden" id="txnID1" name="txnID1" class="form-control"  value="" />
								 <input type="hidden" id="txnID2" name="txnID2" class="form-control"  value="" />
								 <input type="hidden" id="paypaltxnID" name="paypaltxnID" class="form-control"  value="" />
								 <input type="hidden" id="strtxnID" name="strtxnID" class="form-control"  value="" />
                            </div>
                        </div>
                       <div class="col-md-12">
                        <div class="col-md-8 pull-left">
                         <input type="checkbox" id="setMail" name="setMail" class="set_checkbox"   /> Send Customer Receipt
                        </div>
    					 <div class="col-md-4 pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Capture"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
<div id="payment_voidmod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Void Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form11" method="post" action='<?php echo base_url(); ?>Integration/Payments/delete_pay_transaction' class="form-horizontal" >
                         
                        <p id="message_data">Do you really want to void this transaction? The payment will be dropped if you click "Void" below.</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
                                <input type="hidden" id="voidPaytxnID" name="paytxnID" class="form-control"  value="" />
								<input type="hidden" name="payment_capture_page" class="form-control"  value="1" />
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-md-8 pull-left">
                         <input type="checkbox" id="setMail" name="setMailVoid" class="set_checkbox"   /> Send Customer Receipt
                        </div>
    					<div class="col-md-4 pull-right">
            			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        </div>
                        <br />
                        <br />
                
    			    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
</div>
