<!-- Page content -->
<?php
    $this->load->view('alert');
    $formURL = base_url('Common_controllers/surcharging/surchargingSaved');
?>
<div id="page-content">
    <!-- Surcharging -->
    <legend class="leg">Surcharging</legend>
    <div class="surchargeBtnGrp">
        <?php if($isEnable){ ?>
            <form id="surchargeDisableForm" method="post" action='<?php echo base_url(); ?>Common_controllers/surcharging/surchargeStatusUpdate' class="form-horizontal" >
                <input type="hidden" id="merchantID" name="merchantID" value="<?php echo $merchantID; ?>">
                <input type="hidden" id="appIntegration" name="appIntegration" value="<?php echo $appIntegration; ?>">
                <input type="hidden" id="surchargeStatus" name="surchargeStatus" value="<?php echo ($isEnable == 1)?0:1; ?>">
                <input type="submit" id="surchargeDisableBtn" name="btn_disable" class="btn btn-danger btn_can surchargeBtn pull-right" value="Disable"  />
            </form>
        <?php }else{ ?>
            <a href="#surchargeEnablePopup" id="surchargeEnableBtn" class="btn btn-sm btn-success pull-right mg-block-5px surchargeBtn" data-backdrop="static" data-keyboard="false" data-toggle="modal">Enable</a>
        <?php } ?>
    </div>
    <div class="block">
        <!-- Wizard Content -->
        <div class="alignSection">
            <?php if($isEnable){ ?>
                <form id="saveSurchargeForm" action="<?php echo $formURL; ?>" method="post" class="form form-horizontal">
                    <input type="hidden" class="form-control" name="surchargeRowID" value="<?php echo $surchargeData['id']; ?>" >
                    <div class="row" id="content-div">
                        <div class="form-group" id="sp_1">
                            <label class="col-md-3 control-label text-right" for="surcharge_percent"> Surcharge Percentage</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="surcharge_percent" name="surcharge_percent" value="<?php echo $surchargeData['surchargePercent']; ?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right" for="default_item_id"> Default Item</label>
                            <div class="col-md-7">
                                <?php if($appIntegration != 0){ ?>
                                    <select id="default_item_id" class="form-control" name="default_item_id">
                                        <option value="">Select Default Item</option>
                                        <?php if($items){
                                            foreach ($items as $item) {
                                                if($appIntegration == 1){
                                                    $itemID = $item['productID'];
                                                    $itemName = $item['Name'];
                                                }elseif ($appIntegration == 2) {
                                                    $itemID = $item['ListID'];
                                                    $itemName = $item['Name'];
                                                }elseif ($appIntegration == 5) {
                                                    $itemID = $item['ListID'];
                                                    $itemName = $item['Name'];
                                                }
                                        ?>
                                            <option value="<?php echo $itemID; ?>" <?php echo (isset($surchargeData['defaultItem']) && $surchargeData['defaultItem'] == $itemID) ? 'selected' : ''; ?> ><?php echo  $itemName ; ?></option>
                                        <?php }
                                        } ?>
                                    </select>
                                <?php }else{ ?>
                                    <input type="text" class="form-control" id="default_item_id" name="default_item_id" value="<?php echo (isset($surchargeData['defaultItem']) && !empty($surchargeData['defaultItem']))?$surchargeData['defaultItem']:'Surcharge Fees'; ?>">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right" for="default_account_id"> Default Item Account</label>
                            <div class="col-md-7">
                                <?php if($appIntegration != 0){ ?>    
                                    <select id="default_account_id" class="form-control" name="default_account_id">
                                        <option value="">Select Default Item Account</option>
                                        <?php if($account_lists){
                                            foreach ($account_lists as $account_list) {
                                                if($appIntegration == 1){
                                                    $accountID = $account_list['accountID'];
                                                    $accountName = $account_list['accountName'].' / '.$account_list['accountType'];
                                                }elseif ($appIntegration == 2) {
                                                    $accountID = $account_list['ListID'];
                                                    $accountName = $account_list['Name'];
                                                    
                                                }elseif ($appIntegration == 5) {
                                                    $accountID = 'Surcharge Income';
                                                    $accountName = 'Surcharge Income';
                                                }
                                        ?>
                                            <option value="<?php echo $accountID; ?>" <?php echo (isset($surchargeData['defaultItemAccount']) && $surchargeData['defaultItemAccount'] == $accountID) ? 'selected' : ''; ?>><?php echo  $accountName; ?></option>
                                        <?php } } ?>
                                    </select>
                                <?php }else{ ?>
                                    <input type="text" class="form-control" id="default_account_id" name="default_account_id"  value="<?php echo (isset($surchargeData['defaultItemAccount']) && !empty($surchargeData['defaultItemAccount']))?$surchargeData['defaultItemAccount']:'Surcharge Income'; ?>">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 text-right">
                                <button type="submit" class="submit btn btn-sm btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            <?php }else{ ?>
                <form action="<?php echo $formURL; ?>" method="post" class="form form-horizontal">
                    <div class="row" id="content-div">
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right" for="default_item_id"> Default Item</label>
                            <div class="col-md-7">
                                <input type="text" id="default_item_id" class="disabledView form-control" name="default_item_id" value="" >
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right" for="default_account_id"> Default Item Account</label>
                            <div class="col-md-7">
                                <input type="text" id="default_account_id" class="form-control disabledView" name="default_account_id" value="" >
                                
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <div class="col-md-10 text-right">
                                <button type="button" class="submit disabledSurchargeBtn btn btn-sm btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            <?php } ?>
            
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->
</div>
<!--======================================== Modal for Enable Surcharge ================================================------>

<div id="surchargeEnablePopup" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Surcharge Disclaimer</h2>
                <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span> 
                </button>
            </div>
            <!-- END Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="surchargeEnableForm" method="post" action='<?php echo base_url(); ?>Common_controllers/surcharging/surchargeStatusUpdate' class="form-horizontal" >
                     <input type="hidden" id="merchantID" name="merchantID" value="<?php echo $merchantID; ?>">
                     <input type="hidden" id="appIntegration" name="appIntegration" value="<?php echo $appIntegration; ?>">
                     <input type="hidden" id="surchargeStatus" name="surchargeStatus" value="<?php echo ($isEnable == 1)?0:1; ?>">
                    <div class="contentPopup">
                        <p> Merchants who intend to surcharge Visa credit transactions must provide notice directly to Visa 
                        and the merchant's acquirer 30 days before beginning to surcharge. Submit the form below to 
                        notify Visa of your intent to surcharge along with notifying your acquirer. Additional information 
                        about the merchant surcharging guidelines is available on the link below. </p> 
                        <p><a class="linkColorVisa" href="https://usa.visa.com/Forms/merchant-surcharge-notification-form.html" target="_blank">Click Here to register with Visa (required)</a></p>
                        <div class="col-md-12 col-sm-12 no-pad alignWidth">
                            <input type="checkbox" class="col-md-1 col-sm-1 no-pad isAgree" name="isAgree" id="isAgree">
                            <div class="isAgreeContent col-md-11 col-sm-11 no-pad">I understand by enabling surcharging, that I am responsible for compliance with Visa's     
                        requirements for disclosure and for compliance with any relevant State laws.</div> 
                        </div>
                    </div>
                    
                    <div class="pull-right">
                        <input type="submit" id="btn_enable" name="btn_enable" class="btn btn-success btn_can" value="Enable"  />
                    
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(JS); ?>/pages/surcharge.js"></script>