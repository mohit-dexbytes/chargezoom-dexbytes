<?php

$status = '';
$today = date('Y-m-d');
$due_date = date('Y-m-d', strtotime($invoice_data['DueDate']));
if ($due_date < $today && $invoice_data['IsPaid'] == 'false') {
    $status = 'Pastdue';
}
$type = '';
$date1 = strtotime($due_date);
$date2 = strtotime($today);

$type_text = '';

if ($date1 >= $date2) {
    $diff = $date2 - $date1;
    $diff = floor($diff / (60 * 60 * 24));
    $type = 3;
    $type_text = "Due Amount";
} else {
    $type = 2;
    $type_text = "Invoice Past Due";
}
if ($this->session->userdata('logged_in')) {
    $merchantEmail = $this->session->userdata('logged_in')['merchantEmail'];
}
if ($this->session->userdata('user_logged_in')) {
    $merchantEmail = $this->session->userdata('user_logged_in')['userEmail'];
}
$colSpanStyle = '';
if ($invoice_data['BalanceRemaining'] > 0) {
    $displayStatus = 1;
    $readonly = '';
}else{
    $displayStatus = 0;
    $readonly = 'readonly';
}

if(!isset($customer_data['Phone'])){
    $customer_data['Phone'] = '';
}

if(!isset($customer_data['Contact'])){
    $customer_data['Contact'] = '';
}

?>
<?php
	$this->load->view('alert');
?>
<style>
.block-options{
    margin-top: 10px;
}
</style>
<div id="page-content">






    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Products Block -->
    <legend class="leg">Invoice Details</legend>
    <div class="block">
        <!-- Products Title -->
        
            <div class="block-options pull-right">
                <a href="<?php echo base_url(); ?>Integration/Invoices/invoice_details_print/<?php echo  $invoice_data['invoiceID']; ?>" class="btn btn-alt btn-sm btn-danger" data-toggle="tooltip" title="Print Invoice">Download PDF</a>

                <?php if ($invoice_data['BalanceRemaining'] == "0.00" and $invoice_data['IsPaid'] == 'true') {  ?>
                    <a href="javascript:void(0);" title="Send Email" disabled class="btn btn-sm btn-info" data-backdrop="static" data-keyboard="false" data-toggle="modal">Email Invoice</a>
                <?php } else {   ?>
                    <a href="#set_tempemail_data" title="Send Email" onclick="set_template_data_temp('<?php echo  $invoice_data['invoiceID']; ?>','<?php echo  $invoice_data['CustomerListID']; ?>','<?php echo $type; ?>');" class="btn btn-sm btn-info" data-backdrop="static" data-keyboard="false" data-toggle="modal">Email Invoice</a>

                <?php } ?>

                <div class="btn-group dropbtn">
                    <?php if ($invoice_data['BalanceRemaining'] == "0.00" and $invoice_data['IsPaid'] == 'true') {  ?>
                        <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a> 
                        <ul class="dropdown-menu text-left">
                            <li>
                                
                                <a href="javascript:void(0);" id="txnRefund<?php echo $invoice_data["invoiceID"]; ?>" invoice-id="<?php echo $invoice_data["invoiceID"]; ?>" integration-type="4" data-url="<?php echo base_url(); ?>ajaxRequest/getTotalRefundOfInvoice" class="refunTotalInvoiceAmountCustom">Refund</a>
                            </li>
                        </ul>
                    <?php } else { ?> <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a> 
                    <ul class="dropdown-menu text-left">
                        <li> <a href="#invoice_process" class="" onclick="set_invoice_process_id('<?php echo $invoice_data['invoiceID']; ?>','<?php echo $invoice_data['CustomerListID']; ?>','<?php echo $invoice_data['BalanceRemaining']; ?>',1);" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a></li>
                        <li> <a href="#invoice_schedule" class="" onclick="set_invoice_schedule_id('<?php echo $invoice_data['invoiceID']; ?>','<?php echo $invoice_data['CustomerListID']; ?>','<?php echo $invoice_data['BalanceRemaining']; ?>','<?php echo date('F d Y', strtotime($invoice_data['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>

                        <li> <a href="#set_subs" class="" onclick="set_sub_status_id('<?php echo $invoice_data['invoiceID']; ?>', '<?php echo $invoice_data['CustomerListID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>
                        <?php if ($invoice_data['AppliedAmount'] == 0 || $invoice_data['AppliedAmount'] == '0.00') {  ?>
                            <li><a href="#invoice_cancel" onclick="set_common_invoice_id('<?php echo $invoice_data['invoiceID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>
                        <?php } 
                    }?>

                    </ul>
                </div>
            </div>


           



        <form id="form-validation" action="<?php echo base_url(); ?>Integration/Invoices/create_invoice" method="post" enctype="multipart/form-data" class="form-horizontal ">


            <h4>Invoice Number: <strong><?php echo  $invoice_data['refNumber']; ?></strong></h4>
            <h5>Customer Name: <strong><?php echo  $invoice_data['CustomerFullName']; ?></strong></h5>
            <input type="hidden" name="customerID" value="<?php echo $invoice_data['CustomerListID']; ?>" />

            <h5>Invoice Date: <div class="input-group input-date col-md-3">

                    <input type="text" id="invDate" name="invDate" class="form-control input-datepicker" value="<?php echo date('m/d/Y', strtotime($invoice_data['TimeCreated'])); ?>" data-date-format="mm/dd/yyyy" placeholder="Invoice Date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </h5>
            <h5>Invoice Due Date: <div class="input-group input-date col-md-3">

                    <input type="text" id="dueDate" name="dueDate" class="form-control input-datepicker" value="<?php echo date('m/d/Y', strtotime($invoice_data['DueDate'])); ?>" data-date-format="mm/dd/yyyy" placeholder="Invoice Due Date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </h5>

            <h5>Invoice Status: <strong> <?php if ($invoice_data['BalanceRemaining'] == "0.00" and $invoice_data['IsPaid'] == 'true') {  ?> Paid <?php } else
                  if ($invoice_data['IsPaid'] == 'false' && ($invoice_data['BalanceRemaining'] != "0.00" and  $invoice_data['AppliedAmount'] == "0.00")) {    ?> Unpaid <?php } else
                 if (($invoice_data['BalanceRemaining'] != "0.00" and $invoice_data['AppliedAmount'] != "0.00") && $invoice_data['IsPaid'] == 'false') {  ?> Partial <?php } ?></h5>

            <h5>Payment Method: <strong>
                    <?php
                        if(!empty($transaction['transactionType']) && $transaction['transactionType'] =='Offline Payment'){
                            echo 'Check'; 
                        }else if((isset($transaction['paymentType']) && $transaction['paymentType'] == 2  )){ 
                            echo 'eCheck';
                        }else if(empty($transaction['transactionType']  )){ 
                            echo '--';
                        }else{ 
                            echo'Credit Card';
                        }   
                    ?>
                </strong></h5>
            <?php if($ur_data){ ?>
                <h5>Preview Invoice Page: <strong class="cust_view"><?php echo '<a target="_blank" href="' . $paylink . '">' . $paylink . '</a>'; ?></strong></h5>
        
            <?php }else{ ?>
                <h5>Preview Invoice Page: <strong class="cust_view"><?php echo '<a href="javascript:void(0);" rel="txtTooltip" data-toggle="tooltip" title="To activate this page, you must enable the Customer Portal for your account.">' . $paylink . '</a>'; ?></strong></h5>
            <?php } ?>
            <div class="table-responsive">
                <table class="table table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th>Product / Service</th>
                            <th class="text-left"> Description</th>
                            <th class="text-right">Unit Rate</th>
                            <th class="text-right">Qty</th>
                            <th class="text-right set_taxes" <?php echo $colSpanStyle; ?>>Tax</th>
                            <th class="text-right">Amount</th>

                        </tr>
                    </thead>
                    <tbody id="item_fields">
                        <?php


                        $totalTax = 0;
                        $total = 0;
                        $tax = 0;

                        $key = 0;
                        foreach ($invoice_items as $key => $item) {

                        ?>

                            <tr class="removeclass<?php echo $key + 1; ?>  rd ">
                                <td>
                                    <select class="form-control" onchange="select_plan_val('<?php echo $key + 1; ?>', this);" id="productID<?php echo $key + 1; ?>" name="productID[]">

                                        <?php foreach ($plans as $plan) {
                                            $sel = "";
                                            if ($plan['productID'] == $item['productID'])  $sel = "selected"; ?>
                                            <option value="<?php echo $plan['Code']; ?>" <?php echo $sel; ?>>
                                                <?php echo $plan['Name']; ?> </option> <?php }  ?>
                                    </select>
                                </td>
                                <td><input type="text" <?php echo $readonly; ?> id="description<?php echo $key + 1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" class="form-control" /> </td>
                                <td class="text-right">$<span id="rate<?php echo $key + 1; ?>"> <?php echo number_format($item['itemPrice'], 2); ?> </span> <input type="hidden" id="unit_rate<?php echo $key + 1; ?>" name="unit_rate[]" value="<?php echo $item['itemPrice']; ?>" /></td>
                                <td class="text-right">
                                    <div class="input-group pull-right"> <input type="text" <?php echo $readonly; ?> onkeypress="return isNumberKey(event)" size="6" class="form-control text-right" maxlength="4" onblur="set_qty_val('<?php echo $key + 1; ?>');" id="quantity<?php echo $key + 1; ?>" name="quantity[]" value="<?php echo $item['itemQty']; ?>" class="form-control" placeholder="Qty"></div>
                                </td>
                                <td>
                                    <select class="form-control ptaxSelect" onchange="calculateAmount()" id="ptaxID<?php echo $key + 1; ?>" name="ptaxID[]" <?php if($invoice_data['BalanceRemaining'] == '0' || $invoice_data['lineAmountType'] == 'NoTax'){ echo 'disabled'; }?>>
                                    <?php foreach ($taxes as $taxSingle) {
                                        $sel = "";
                                        if ($taxSingle['taxID'] == $item['taxType'])  $sel = "selected"; ?>
                                        <option value="<?php echo $taxSingle['taxID']; ?>" <?php echo $sel; ?>>
                                            <?php echo $taxSingle['friendlyName']; ?> </option> <?php }  ?>
                                    </select>
                                
                                </td>
                                <td class="text-right"><?php $total +=  $item['itemQty'] * $item['itemPrice'];
                                                        echo '<input type="hidden" class="form-control total_val" id="total' . ($key + 1) . '" name="total[]" value="' . ($item['itemQty'] * $item['itemPrice']) . '"/>';  ?> $<?php echo '<span id="total11' . ($key + 1) . '" >' . number_format($item['itemQty'] * $item['itemPrice'], 2) . '</span>'; ?> </td>


                        <input type="hidden" id="AccountCode<?php echo $key+1; ?>" name="AccountCode[]" value="<?php echo $item['AccountCode']; ?>">
                         <input type="hidden" name="xeroitemID[]" value="<?php echo $item['itemID']; ?>">
                         <input type="hidden" id="txn_line_id<?php echo $key+1; ?>" name="txn_line_id[]" value="<?php echo $item['itemID']; ?>">

                            </tr>
                        <?php } ?>



                        <tr class="active">
                            <td>
                                <?php if($displayStatus){ ?>
                                    <button class="btn btn-sm btn-success" type="button" onclick="item_invoice_details_fields();"> Add More </button>
                                    <input type="hidden" id="roomCount" value="<?php echo $key+1; ?>">
                                <?php } ?>
                            </td>
                            <td colspan="4" class="text-right text-uppercase"><strong>SUBTOTAL</strong></td>
                            <td class="text-right">$<?php if ($invoice_data['IsPaid'] == 'true') {
                                                    $balance = $invoice_data['BalanceRemaining'] + abs($invoice_data['AppliedAmount']);
                                    
                                                        // $balance = (-$invoice_data['AppliedAmount']);
                                                    } else {
                                                        $balance = $invoice_data['BalanceRemaining'] + abs($invoice_data['AppliedAmount']);

                                                        // $balance = $invoice_data['BalanceRemaining'];
                                                    }
                                                    echo '<span id="sub_total" >' . number_format(($balance - $invoice_data['totalTax']), 2) . '</span>'; ?></td>

                        </tr>

                        <tr>
                            <td class="text-left">
                                <select name="taxes" id="taxes" class="form-control " <?php if($invoice_data['BalanceRemaining'] == '0'){ echo 'disabled'; }?>>
                                    <option <?php if($invoice_data['lineAmountType'] == 'NoTax'){ echo 'selected'; }?> value="">No Tax</option>
                                    <option <?php if($invoice_data['lineAmountType'] == 'Exclusive'){ echo 'selected'; }?> value="1">Tax Exclusive</option>
                                    <option <?php if($invoice_data['lineAmountType'] == 'Inclusive'){ echo 'selected'; }?> value="2">Tax Inclusive</option>
                                </select>
                            </td>
                            <td colspan="4" class="text-right text-uppercase"><span id="taxv_rate"><strong>TAX Amount </strong></span></td>
                            <td class="text-right"><?php echo '$' . '<span id="tax_val" >' . number_format($invoice_data['totalTax'], 2) . '</span>'; ?></td>
                        </tr>
                        <tr class="info">
                            <td colspan="5" class="text-right text-uppercase"><strong>TOTAL</strong></td>
                            <td class="text-right">$<?php if ($invoice_data['IsPaid'] == 'true') {
                                                        // $balance = (-$invoice_data['AppliedAmount']);
                                                    } else {
                                                        // $balance = $invoice_data['BalanceRemaining'];
                                                    }
                                                    echo '<span id="total_amt">' . number_format(($balance), 2) . '</span>';    ?></td>
                        </tr>

                        <tr class="success">
                            <td colspan="5" class="text-right text-uppercase"><strong>PAID</strong></td>
                            <td class="text-right">$<?php echo number_format(($invoice_data['AppliedAmount']), 2);  ?></td>
                        </tr>

                        <tr class="danger">
                            <td colspan="5" class="text-right text-uppercase"><strong>BALANCE</strong></td>
                            <td class="text-right"><strong>$<?php $total = ($invoice_data['BalanceRemaining']) ? $invoice_data['BalanceRemaining'] : '0.00';
                                                            echo '<span id="grand_total">' . $total . '</span>'; ?></strong></td>
                        </tr>
                    </tbody>

                </table>
            </div>
            <input type="hidden" id="txID" id="txID" value="<?php echo $invoice_data['taxRate']; ?>" />
            <div class="col-md-12">

                <div class="pull-right">
                    <?php if ($invoice_data['BalanceRemaining'] != '0.00') {
                        $dis = ''; ?>
                        <a href="#invoice_process" class="btn btn-sm btn-success" onclick="set_invoice_process_id('<?php echo $invoice_data['invoiceID']; ?>', '<?php echo $invoice_data['CustomerListID']; ?>','<?php echo $invoice_data['BalanceRemaining']; ?>',3);" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
                        <?php if ($invoice_data['IsPaid'] == 'false' && ($invoice_data['BalanceRemaining']) > 0 &&  (-$invoice_data['AppliedAmount']) > 0) {
                            $dis = 'disabled';
                        } ?>
                        <button name="inv_exit" class="btn testbtn btn-sm btn-primary" <?php echo $dis; ?>>Save & Exit</button>
                        <button name="inv_save" class="btn testbtn btn-sm btn-primary" <?php echo $dis; ?>>Save</button>

                    <?php  }    ?><br><br>
                </div>

            </div>


            <input type="hidden" id="invNo" name="invNo" value="<?php echo $invoice_data['invoiceID']; ?>" />
        </form>

        <div>
        </div>
        <?php  
            $BillingAdd = 0;
            $ShippingAdd = 0;

            $isAdd = 0;
            if($customer_data['address1']  || $customer_data['address2'] || $customer_data['City'] || $customer_data['State'] || $customer_data['zipCode'] || $customer_data['Country']){
                $BillingAdd = 1;
                $isAdd = 1;
            }
            if($customer_data['ship_address1']  || $customer_data['ship_address2'] || $customer_data['ship_city'] || $customer_data['ship_state'] || $customer_data['ship_zipcode'] || $customer_data['ship_country'] ){
                $ShippingAdd = 1;
                $isAdd = 1;
            }

        ?>
        <!-- Addresses -->
        <div class="row">
        <?php if($isAdd){ ?>
            <?php if($BillingAdd){ ?>
            <div class="col-sm-6">
                <!-- Billing Address Block -->
                <div class="block">
                    <!-- Billing Address Title -->
                    <div class="block-title">
                        <h2><strong>Billing</strong> Address</h2>
                    </div>


                    <h4><strong>
                            <?php if (isset($customer_data['firstName']) && $customer_data['firstName'] != '') {
                                echo $customer_data['firstName'] . ' ' . $customer_data['lastName'];
                            } else {
                                echo $invoice_data['CustomerFullName'];
                            }
                            ?>
                        </strong></h4>
                    <address>
                        
                        <?php echo ($customer_data['address1']) ? $customer_data['address1']. '<br>'  : ''; ?>
                        <?php echo ($customer_data['address2']) ? $customer_data['address2']. '<br>'  : ''; ?>

                        <?php echo ($customer_data['City']) ? $customer_data['City'] . ',' : ''; ?>
                        <?php echo ($customer_data['State']) ? $customer_data['State'] : ''; ?>
                        <?php echo ($customer_data['zipCode']) ? $customer_data['zipCode']. '<br>' : ''; ?> 
                        <?php echo ($customer_data['Country']) ? $customer_data['Country']. '<br>' : ''; ?> 
                       
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Billing Address Content -->
                </div>
                <!-- END Billing Address Block -->
            </div>
            <?php }
            if($ShippingAdd){ ?>
            <div class="col-sm-6">
                <!-- Shipping Address Block -->
                <div class="block">
                    <!-- Shipping Address Title -->
                    <div class="block-title">
                        <h2><strong>Shipping </strong> Address</h2>
                    </div>
                    <!-- END Shipping Address Title -->

                    <h4><strong> <?php if (isset($customer_data['firstName']) && $customer_data['firstName'] != '') {
                                        echo $customer_data['firstName'] . ' ' . $customer_data['lastName'];
                                    } else {
                                        echo $invoice_data['CustomerFullName'];
                                    }
                                    ?></strong></h4>
                    <address>
                        

                        <?php echo ($customer_data['ship_address1']) ? $customer_data['ship_address1']. '<br>'  : ''; ?>
                        <?php echo ($customer_data['ship_address2']) ? $customer_data['ship_address2']. '<br>'  : ''; ?>

                        <?php echo ($customer_data['ship_city']) ? $customer_data['ship_city'] . ',' : ''; ?>
                        <?php echo ($customer_data['ship_state']) ? $customer_data['ship_state'] : ''; ?>
                        <?php echo ($customer_data['ship_zipcode']) ? $customer_data['ship_zipcode']. '<br>' : ''; ?>
                        <?php echo ($customer_data['ship_country']) ? $customer_data['ship_country']. '<br>' : ''; ?>
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Shipping Address Content -->
                </div>
                <!-- END Shipping Address Block -->
            </div>
            <?php } ?>
        <?php } ?>
        </div>
        <!-- END Addresses -->

        <!-- Log Block -->
        <div class="block full">
            <!-- Private Notes Title -->
            <div class="block-title">
                <h2><strong>Private</strong> Notes</h2>
            </div>
            <!-- END Private Notes Title -->

            <!-- Private Notes Content -->
            <div class="alert alert-info">
                <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
            </div>
            <form method="post" id="pri_form" onsubmit="return false;">
                <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="Your note.."></textarea>
                <input type="hidden" name="customerID" id="customerID" value="<?php echo $invoice_data['CustomerListID']; ?>" />
                <br>
                <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
            </form>
            <hr>

            <?php
            if (!empty($notes)) {
                foreach ($notes as $note) { 
                        if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                            $timezone = ['time' => $note['privateNoteDate'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                            $note['privateNoteDate'] = getTimeBySelectedTimezone($timezone);
                        }
                    ?>

                    <div>
                        <?php echo $note['privateNote']; ?>
                    </div>
                    <div class="pull-right">
                        <span>Added on <strong><?php echo date('M d, Y - h:i A', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
                        <span><a href="javascript:void(0)" onclick="delele_notes('<?php echo $note['noteID']; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
                    </div>
                    <br>
                    <hr>
            <?php }
            }
            ?>


            <br>
            <!-- END Private Notes Content -->
        </div>
        <!-- END Log Block -->
    </div>


    <!-- <script src="<?php echo base_url(JS); ?>/pages/qbd_invoice_payment.js"></script> -->
    <script src="<?php echo base_url(JS); ?>/common-integrations/customer_detail.js"></script>
    <script src="<?php echo base_url(JS); ?>/common-integrations/products.js"></script>
    <script>
        $('.testbtn').click(function() {
            var form_data = $('#form-validation').serialize();
            var index = '';
            if ($(this).val() == 'Save') {
                index = "self";
            } else {
                index = "other";
            }
            $('#index').remove();


            $('<input>', {
                'type': 'hidden',
                'id': 'index',
                'name': 'index',

                'value': index,
            }).appendTo($('#form-validation'));

            $('#form-validation').submit();

        });
    </script>
    <div id="set_tempemail_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->

                <div class="modal-header ">
                    <h2 class="modal-title text-center">Send Email</h2>



                </div>

                <div class="modal-body">
                    <div id="data_form_template">
                        <label class="label-control" id="template_name"> </label>

                        <form id="form-validation1" action="<?php echo base_url(); ?>Integration/Settings/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">

                            <input type="hidden" id="invoicetempID" name="invoicetempID" value="">
                            <input type="hidden" id="customertempID" name="customertempID" value="">
                            <input type="hidden" id="invoiceCode" name="invoiceCode" value="">
                            <input type="hidden" id="sendmailbyinvdtl" name="sendmailbyinvdtl" value="1">
                            <input type="hidden" name="type" id="type" value="<?php echo $type ?>" />
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="type">Template</label>
                                <div class="col-md-7">
                                    <input type="text" name="type_text" class="form-control" readonly='readonly' id="type_text" value="<?php echo $type_text ?>" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label" for="templteName">To Email</label>
                                <div class="col-md-7">
                                    <input type="text" id="toEmail" name="toEmail" value="" class="form-control" placeholder="Email">
                                </div>
                            </div>

                            
                            <div class="form-group" id="reply_div">
                                <label class="col-md-3 control-label" for="replyEmail">Reply To Email</label>
                                <div class="col-md-7">
                                    <input type="text" id="replyTo" name="replyEmail" class="form-control" value="<?php if (isset($templatedata) && $templatedata['replyTo'] != ''){
                                                echo $templatedata['replyTo'];
                                            }else{  echo $merchantEmail;} ?>" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group" style="display:none" id="from_email_div">
                                <label class="col-md-3 control-label" for="templteName">From Email</label>
                                <div class="col-md-7">
                                    <input type="text" id="fromEmail" name="fromEmail"  value="<?php   if(isset($from_mail) && $from_mail != '') echo $from_mail ? $from_mail : 'donotreply@payportal.com'; ?>" class="form-control" placeholder="From Email">
                                </div>
                            </div>
                            <div class="form-group" id='display_name_div' style='display:none'>
                                <label class="col-md-3 control-label" for="templteName">Display Name</label>
                                <div class="col-md-7">
                                    <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="<?php echo $mailDisplayName; ?>" placeholder="Display Name">
                                </div>
                            </div>
                                    

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="templteName"></label>
                                <div class="col-md-7">
                                    <a href="javascript:void(0);" id="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                    <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                           <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                                    <!-- <a href="javascript:void(0);" id="open_reply">Set Reply-To</a> -->
                                </div>
                            </div>

                            <div class="form-group" id="cc_div" style="display:none">
                                <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                                <div class="col-md-7">
                                    <input type="text" id="ccEmail" name="ccEmail" value="<?php if (isset($templatedata)) echo ($templatedata['addCC']) ? $templatedata['addCC'] : ''; ?>" class="form-control" placeholder="CC Email">
                                </div>
                            </div>
                            <div class="form-group" id="bcc_div" style="display:none">
                                <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                                <div class="col-md-7">
                                    <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if (isset($templatedata)) echo ($templatedata['addBCC']) ? $templatedata['addBCC'] : ''; ?>" placeholder="BCC Email">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-md-3 control-label" >Attach Invoice PDF</label>
                                <div class="col-md-7">
                                    <label class="switch switch-info"><input type="checkbox" name="add_attachment"<?php  echo "checked";  ?> id="add_attachment"><span></span></label>
                                </div>
                            </div>

                            <div class="form-group">

                                <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                <div class="col-md-7">

                                    <input type="text" id="emailSubject" name="emailSubject" value="<?php if (isset($templatedata)) echo ($templatedata['emailSubject']) ? $templatedata['emailSubject'] : ''; ?>" class="form-control" placeholder="Email Subject">
                                </div>

                            </div>

                            <!-- <div class="form-group">

                                <label class="col-md-3 control-label" for="templteName">Email Attachment</label>
                                <div class="col-md-7">

                                    <input type="file" id="mail_attachment" name="mail_attachment">
                                </div>

                            </div> -->

                            <div class="form-group">


                                <label class="col-md-3 control-label">Email Body</label>
                                <div class="col-md-7">
                                    <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if (isset($templatedata)) echo ($templatedata['message']) ? $templatedata['message'] : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-7 col-md-offset-3 align-right">
                                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                    


                                </div>
                            </div>
                        </form>


                    </div>


                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>