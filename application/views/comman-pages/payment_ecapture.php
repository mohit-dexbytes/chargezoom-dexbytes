<style>
	.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 270px !important;
	}
</style>
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <legend class="leg">Void</legend>
	<div class="full">
        <!-- Form Validation Example Title -->
        <table id="page_ecapture" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs">Invoice</th>
                    <th class="text-right">Amount</th>					
                    <th class="text-right hidden-xs hidden-sm">Date</th>
                    <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
    			<?php 
    			if(isset($transactions) && $transactions)
    			{
    				foreach($transactions as $transaction)
    				{
    			?>
				<tr>
					
					<?php if($plantype) { ?>
					<td class="text-left cust_view"><?php echo $transaction['fullName']; ?></td>
					<?php } else {?>
						<td class="text-left cust_view"><a href="<?php echo base_url('Integration/Customers/customer_detail/'.$transaction['Customer_ListID']); ?>"><?php echo $transaction['fullName']; ?></a></td>
					<?php } ?>	
				
					<?php if(!empty($transaction['invoiceID'])) { ?>
				    <td class="text-right hidden-xs cust_view"><a href="<?php echo base_url('Integration/Invoices/invoice_details/'.$transaction['invoiceID']); ?> "><?php echo ($transaction['refNumber'])?$transaction['refNumber']:'-----'; ?></a></td>
					<?php } else { ?>
					<td class="text-right hidden-xs cust_view"><a href="javascript:void(0)"><?php echo ($transaction['refNumber'])?$transaction['refNumber']:'-----'; ?></a></td>
					<?php } ?>
					<td class="text-right"><?php echo($transaction['transactionAmount'])? '$'.$transaction['transactionAmount']:'0.00'; ?></td>			
					
					<td class="hidden-xs hidden-sm text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
				
					
					<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'';   ?></td>
					
					
				    <td class="text-center">
                        <a href="#payment_voidmod" class="btn btn-sm btn-danger"  onclick="set_void_pay('<?php  echo $transaction['transactionID']; ?>','<?php  echo $transaction['transactionGateway']; ?>' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>                           
					</td>
				</tr>
				<?php } }
				else { echo'<tr><td colspan="6"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                </tr>'; }  
				?>
			</tbody>
        </table>
    </div>
    
   <div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
</div>

    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
      <script>$(function(){ Pagination_view.init(); });</script>
<script>
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#page_ecapture').dataTable({
                columnDefs: [
                    { type: 'date', targets: [3] },
                   { orderable: false, targets: [5] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', '');
        }
    };
}();
</script>

	<script>
    //	$(function(){  nmiValidation.init(); });
    	
 
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
           $(".msg_data").fadeOut('slow');
        }
		
		
    
    function set_void_pay(txnid, txntype){
    		
   
        if(txnid !=""){
            $('#txnvoidID').val(txnid);
            $('#paytxnID').val(txnid);
            var url   = "<?php echo base_url()?>Integration/Payments/delete_pay_transaction";
			
            $("#data_form11").attr("action",url);	
        }
			
			
    }
	    
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
           $(".msg_data").fadeOut('slow');
        }
        
	</script>
	
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
	
	
	
<div id="payment_voidmod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Void Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form11" method="post" action='<?php echo base_url(); ?>Integration/Payments/delete_pay_transaction' class="form-horizontal" >
                         
                        <p id="message_data">Do you really want to void this transaction? The payment will be dropped if you click "Void" below.</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
								  <input type="hidden" id="txnvoidID" name="txnvoidID" class="form-control"  value="" />
                                  <input type="hidden" id="txnvoidID1" name="txnvoidID1" class="form-control"  value="" />
								  <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
								
                            </div>
                        </div>
                        
    					<div class="pull-right">
            			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                
    			    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
		
