<!-- Page content -->

<?php
	$this->load->view('alert');
?>

<div id="page-content">
   

    <!-- All Orders Block -->
    <legend class="leg">Accounting Package</legend>
    <div class="full">
        <!-- All Orders Title -->
        
        <!-- END All Orders Title -->
        <div class="new_btn">
            <a href="<?php echo base_url('Integration/home/xero_log'); ?>" data-toggle="tooltip" class="btn btn-info btn-sm" data-original-title="" title=""> View Log</a>
        </div>
        <!-- All Orders Content -->
        <table id="company" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Accounting Package</th>
                    <th class=" text-left hidden-xs">Company ID</th>
                     <th class="hidden-xs text-right">Refresh Token</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($companies) && $companies)
				{
					foreach($companies as $company)
					{
				?>
				<tr>
					<td class="text-left cust_view"><?php echo "Xero Connect"; ?></td>
				    <td class="text-left hidden-xs"><?php echo $company['tenant_id']; ?></td>
					<td class="text-right hidden-xs"><?php echo $company['refresh_token']; ?></td>
					<td class="text-center">
                        <?php
                            if($isConnected){ ?>
                                <a href="#del_pack"  data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Disconnect" class="btn btn-danger disconnect-app"><i class="fa fa-times"></i></a> 
                            <?php } else { ?>
                                <input type="submit" id="reconnect_xero" class="btn btn-sm btn-info" value="Reconnect"  />
                            <?php }
                        ?>
                    </td>
				
				</tr>
				
				<?php } }else { echo'<tr>
                <td colspan="4"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->
 <div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
        <div id="del_pack" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Disconnect Account</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                
                <p>Do you really want to disconnect ?</p> 
                <form id="form-validation" action="<?php echo base_url(); ?>Integration/RevokeToken" method="post" enctype="multipart/form-data" class="form-horizontal ">
                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="hidden" id="appID" name="appID" class="form-control"  value="" />
                        </div>
                    </div>
                    <div class="pull-right">
                    <input type="submit" id="disconnect-ajax" name="btn_cancel" class="btn btn-sm btn-danger" value="Disconnect"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>
$(document).ready(function(){
    $('#reconnect_xero').click(function(){
        var base_url = $('#js_base_url').val();
        window.open(`${base_url}Integration/Xero/authenticate`, '_blank', 'width=650,height=650');
        return false;
    });
});
   
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#company').dataTable({
                columnDefs: [
                  
                    { orderable: false, targets: [0] }
                ],
                searching: false,
                bInfo: false,
                paging: false,
                //order: [[ 3, "desc" ]],
                // pageLength: 10,
                // lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
            // $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}
.disconnect-app{
    line-height: 0px !important;
    
}
.intuitPlatformConnectButton{
	display: none;
    margin-left: auto;
    margin-right: auto;
    width: 120px;
	line-height: 29px;
	margin-bottom: 5px;
}

#qbo-reconnect{
    width: 100px;
    cursor: pointer;
}
.disconnect-msg{
    background: #fff;
    padding: 100px;
    text-align: center;
    font-size: 15px
}
.disconnect-msg > p> a {
    color: #1bbae1;
}
.new_btn{
    text-align: right;
    margin-top: -15px;
    margin-bottom: -10px;
}
</style>
 

</div>


<!-- END Page Content -->