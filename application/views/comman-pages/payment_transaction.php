<style>
	.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 270px !important;
	}
</style>
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- All Orders Block -->
    <legend class="leg">Transactions</legend>
    <div class="full">
        <?php  //  print_r($transactions); die; 
        ?>
        <table id="pay_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Customer Name</th>
                    <th class="text-right">Invoice</th>
                    <th class="text-right hidden-xs">Amount</th>
                    <th class="text-right ">Date </th>
                    <th class="text-right hidden-xs">Type</th>
                    <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                    <!--th class="hidden-xs text-right">Status</th>-->

                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                if (isset($transactions) && $transactions) {
                    foreach ($transactions as $transaction) {

                        $inv_url1 = '';
                        if (!empty($transaction['invoice_id']) && !empty($transaction['invoice_no'])) {
                            $invs = explode(',', $transaction['invoice_id']);

                            $invoice_no = explode(',', $transaction['invoice_no']);

                            foreach ($invs as $k => $inv) {
                                $inv_url = base_url() . 'Integration/Invoices/invoice_details/' . trim($inv);
                                if (isset($invoice_no[$k]))
                                    if ($plantype) {
                                        $inv_url1 .= $invoice_no[$k].',';
                                    } else {
                                        $inv_url1 .= ' <a href="' . $inv_url . '">' . $invoice_no[$k] . '</a>,';
                                    }
                            }

                            $inv_url1 = substr($inv_url1, 0, -1);
                        } else
                            $inv_url1 .= '<a href="javascript:void(0);">---</a> ';
                        $gateway = ($transaction['gateway']) ? $transaction['gateway'] : $transaction['transactionType'];



                ?>
                        <tr>

                            <?php if ($plantype) { ?>
                                <td class="text-left visible-lg"><?php echo $transaction['fullName']; ?></td>
                            <?php } else { ?>
                                <td class="text-left visible-lg cust_view"><a href="<?php echo base_url('Integration/Customers/customer_detail/' . $transaction['customerListID']); ?>"><?php echo $transaction['fullName']; ?></a></td>
                            <?php } ?>
                            <td class="text-right cust_view"><?php echo $inv_url1; ?></td>


                            <td class="hidden-xs text-right cust_view"><a href="#pay_data_process" onclick="set_common_transaction_data('<?php echo $transaction['transactionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$' . number_format($transaction['transactionAmount'], 2); ?></a></td>

                            <?php
                                $transactionDate = $transaction['transactionDate'];
                                if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                                    $timezone = ['time' => $transactionDate, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                                    $transactionDate = getTimeBySelectedTimezone($timezone);
                                }
                            ?>
                            <td class=" text-right"><?php echo date('M d, Y h:i A', strtotime($transactionDate)); ?></td>
                            <td class="hidden-xs text-right">
                                <?php
                                $showRefund = 0;
                                if ($transaction['partial'] == $transaction['transactionAmount']) {
                                    echo "<label class='label label-success'>Fully Refunded: " . '$' . number_format($transaction['partial'], 2) . "</label><br/>";
                                } else if ($transaction['partial'] != '0') {
                                    $showRefund = 1;
                                    echo "<label class='label label-warning'>Partially Refunded: " . '$' . number_format($transaction['partial'], 2) . " </label><br/>";
                                } else {
                                    $showRefund = 1;
                                    echo "";
                                }
                                if(strpos(strtolower($transaction['transactionType']), 'refund') !== false){
                                    $showRefund=0;
                                    echo "Refund";
                                }else if (strpos($transaction['transactionType'], 'sale') !== false || strtoupper($transaction['transactionType']) == 'AUTH_CAPTURE') {
                                    echo "Sale";
                                } else if (strpos($transaction['transactionType'], 'Offline Payment') !== false) {
                                    echo "Offline Payment";
                                } else if (strpos($transaction['transactionType'], 'capture') !== false || strtoupper($transaction['transactionType']) != 'AUTH_CAPTURE') {
                                    echo "Capture";
                                }


                                ?>

                            </td>
                            <td class="text-right hidden-xs hidden-sm"><?php echo $transaction['transactionID']; ?></td>

                            <td class="text-center hidden-xs">

                                <div class="btn-group dropbtn">
                                    <?php if($showRefund == 0) { 
                                            $disabled_select = 'disabled'; 
                                        }else{
                                            $disabled_select = '';
                                        } 
                                    ?>

                                    <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm  <?php echo $disabled_select; ?> dropdown-toggle">Select <span class="caret"></span></a>
                                    <ul class="dropdown-menu text-left">

                                        <?php
                                        if ($transaction['transaction_user_status'] == '1' || $transaction['transaction_user_status'] == '2') {
                                            if (
                                                in_array($transaction['transactionCode'], array('100', '200', '111', '1')) &&
                                                in_array(strtoupper($transaction['transactionType']), array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'OFFLINE PAYMENT', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
                                            ) {
                                                if (($transaction['tr_Day'] == 0 &&  ($transaction['transactionGateway'] == '2' || $transaction['transactionGateway'] == '3')) ||  strtoupper($transaction['transactionType']) == 'OFFLINE PAYMENT') {      ?>


                                                    <li> <a href="javascript:void(0);" class="" data-title="Pending Transaction" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
                                                    <?php } else {
                                                    if ($transaction['partial'] == $transaction['transactionAmount']) { ?>
                                                        <li> 
                                                            <a href="javascript:void(0)" id="txnRefund<?php  echo $transaction['transactionID']; ?>" transaction-id="<?php  echo $transaction['transactionID']; ?>" transaction-gatewayType="<?php echo $transaction['transactionGateway'];?>" transaction-gatewayName="<?php echo $transaction['gateway'];?>" integration-type="4" class="refunAmountCustom" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
                                                        </li>
                                                    <?php } else {
                                                        if(strtolower($gateway) != 'heartland echeck'){
                                                    ?>         
                                                    
                                                        <li> 
                                                            <a href="javascript:void(0)" id="txnRefund<?php  echo $transaction['transactionID']; ?>" transaction-id="<?php  echo $transaction['transactionID']; ?>" transaction-gatewayType="<?php echo $transaction['transactionGateway'];?>" transaction-gatewayName="<?php echo $transaction['gateway'];?>" integration-type="4" class="refunAmountCustom" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
                                                        </li>
                                                    <?php    }
                                                    ?>
                                                <?php }
                                                }
                                            }
                                            if($transaction['transactionGateway'] != 5){
                                                if ($transaction['transaction_user_status'] != '2') {    ?>
                                                    <li><a href="#payment_delete" class="" onclick="set_transaction_pay('<?php echo $transaction['id']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>
                                                <?php  }
                                            }
                                        } else {  ?>
                                            <li><a href="javascript:void(0);" data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>
                                        <?php } ?>
                                        <?php 
                                            $transaction_auto_id = $transaction['transactionID'];
                                        ?>
                                        <li><a href="javascript:void('0');" onclick="getPrintTransactionReceiptData('<?php echo $transaction_auto_id; ?>', 1)">Print</a></li>
                                    </ul>
                                </div>

                            </td>

                        </tr>

                <?php }
                } else {
                    echo '<tr><td colspan="7"> No Records Found </td></tr>';
                }
                ?>

            </tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div>
            </div>
            <div class="col-md-4"> </div>
        </div>
    </div>
</div>

<div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Refund Payment</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <div id="refund_msg"></div>


                <form id="data_form" method="post" class="form-horizontal">
                    <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p>

                    <div id="sigle_tr">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_list">Refund Amount:</label>
                            <div class="col-md-8">
                                <label class="control-label"><span id="ref_amount"></span></label>
                                <?php /*	   <input type="text" name="ref_amount" id="ref_amount" value="" class="form-control" /> */ ?>
                            </div>
                        </div>
                    </div>
                    <div id="multi_tr">

                    </div>
                    <div class="pull-right">
                        <input type="submit" id="rf_btn" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund" />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="payment_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Void Payment</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form id="data_formpay" method="post" action='<?php echo base_url(); ?>Integration/Payments/delete_pay_transaction' class="form-horizontal">
                    <p id="message_data">Do you really want to "Void" this payment?</p>
                    <div class="form-group">
                        <div class="col-md-8">

                        </div>
                    </div>
                    <div class="pull-right">
                        <input type="submit" name="btn_cancel" class="btn btn-sm btn-danger" value="Void" />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="print-transaction-div" style="display: none;">

</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>
    $(function() {
        Pagination_view.init();
    });

    var Pagination_view = function() {

        return {
            init: function() {
                / Extend with date sort plugin /
                $.extend($.fn.dataTableExt.oSort, {

                });

                / Initialize Bootstrap Datatables Integration /
                App.datatables();

                / Initialize Datatables /
                $('#pay_page').dataTable({
                    columnDefs: [{
                            type: 'date',
                            targets: [3]
                        },
                        {
                            orderable: false,
                            targets: [6]
                        }
                    ],
                    order: [
                        [3, "desc"]
                    ],
                    pageLength: 10,
                    lengthMenu: [
                        [10, 25, 50, 100, 500],
                        [10, 25, 50, 100, 500]
                    ]
                });

                / Add placeholder attribute to the search input /
                $('.dataTables_filter input').attr('placeholder', 'Search');
            }
        };
    }();

    function set_transaction_pay(t_id) {
        var form = $("#data_formpay");
        $('<input>', {
            'type': 'hidden',
            'id': 'paytxnID',
            'name': 'paytxnID'

        }).remove();
        $('<input>', {
            'type': 'hidden',
            'id': 'paytxnID',
            'name': 'paytxnID',
            'value': t_id,
        }).appendTo(form);
    }
</script>


<style>
    .table.dataTable {
        width: 100% !important;
    }

    @media screen and (max-width:352px) {
        .table.dataTable {
            width: 100% !important;
        }

        table.table thead .sorting_desc {
            padding-right: 0px !important;
        }

        table.dataTable thead>tr>th {
            padding-left: 5px !important;
            padding-right: 5px !important;
        }
    }
</style>



<!-- END Page Content -->