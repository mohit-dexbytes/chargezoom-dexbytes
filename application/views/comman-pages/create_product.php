   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>
	<div id="page-content">
    <!-- END Wizard Header -->
    <!-- Progress Bar Wizard Block -->
   	<legend class="leg"> <?php if(isset($item_pro)){ echo "Edit Product";}else{ echo "Create New Product"; }?></legend>
 
    <div class="block">
	               <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;

					?>
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form  id="product_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>Integration/Items/create_product">
			
			 <input type="hidden"  id="productID" name="productID" value="<?php if(isset($item_pro)){echo $item_pro['productID']; } ?>" /> 

			 	<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Code</label>
					<div class="col-md-7">
						<input type="text" id="productCode" name="productCode" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['Code']; } ?>"><?php echo form_error('productCode'); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Name</label>
					<div class="col-md-7">
						<input type="text" id="productName" name="productName" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['Name']; } ?>"><?php echo form_error('productName'); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="example-type">Product Description </label>
					<div class="col-md-7">
						<textarea id="proDescription" name="proDescription" class="form-control"  ><?php if(isset($item_pro)){echo $item_pro['SalesDescription']; } ?><?php echo form_error('proDescription'); ?> </textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Price </label>
					<div class="col-md-7">
						<input type="text" id="averageCost" name="averageCost"  class="form-control" value="<?php if(isset($item_pro)){ 
						echo  sprintf('%0.2f',($item_pro['saleCost']) ) ;
						} ?>"><?php echo form_error('averageCost'); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="example-type">Account</label>
					<div class="col-md-7">
						<select id="type" class="form-control " name="AccountCode">
						<option value="">Select Account</option>
						<?php
							foreach ($accounts as $keyAccount => $accountValue) {
								?>
									<option value="<?php echo $accountValue['accountCode']; ?>" <?php  if(isset($item_pro) && strtoupper($item_pro['AccountCode'])== $accountValue['accountCode'] ) echo"Selected" ?> ><?php echo $accountValue['accountName']; ?></option>
								<?php
							}
							?>
						</select><?php echo form_error('AccountCode'); ?>
					</div>
				</div>
			   
					   
				<div class="form-group">
					<div class="col-md-10 text-right">
						<button type="submit" class="submit btn btn-sm btn-success">Save</button>				
    	                <a href="<?php echo base_url(); ?>Integration/Items/item_list"  class="submit btn btn-sm btn-primary1">Cancel</a>	
					</div>
				</div>
          	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->
</div></div>


<!-- END Page Content -->


<script>

$(document).ready(function(){
                    
/*    var spacesToAdd =25;
var biggestLength = 0;
$("#productAccount option").each(function(){
 if( $(this).val()!=''){
var len = $(this).text().length;
    if(len > biggestLength){
        biggestLength = len;
    }
 }
});

var padLength = biggestLength + spacesToAdd;
 
                    var      strLength1 = [];
$("#productAccount option").each(function(){
   if( $(this).val()!=''){
    var parts = $(this).text().split(':');
    var strLength = parts[0].length;
   strLength1.push(strLength);
    
    for(var x=0; x<(padLength-strLength); x++){
        parts[0] = parts[0]+' '; 
    }
  // $(this).text(parts[0]+':'+parts[1]).text; 
    $(this).text(parts[0].replace(/ /g, '\u00a0')+'  '+parts[1]).text; 
   }
});      
            */        
     /*               console.log(strLength1);
         var padlength = Math.max.apply(Math,strLength1);       
                    alert(padlength);
   $("#productAccount option").each(function(){
   if( $(this).val()!=''){
    var parts = $(this).text().split(':');
    var strLength = parts[0].length;
  
   }
});       */             
                    
                    //Math.max(strLength1);
                     //console.log(strLength1);
                    
                    
 $('#product_service').addClass('active');
 
    $('#product_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			'productCode': {
				required: true,
				minlength: 3,
				maxlength: 25,
				validate_char:true,
				remote: {
					url: base_url+'Integration/Items/is_product_exists',
					type: "POST",
					cache: false,
					dataType: 'json',
					data: {
						productID: function(){ return $("#productID").val(); },
						productCode: function(){ return $("#productCode").val(); }
					},
					dataFilter: function(response) {
						var rsdata = jQuery.parseJSON(response)
						if(rsdata.status)
							return true;
						else
							return false;
					}
				},
			},
			'productName': {
				required: true,
				minlength: 3,
				maxlength: 25,
				validate_char:true,
			},
			'averageCost':{
				required:true,
				number: true,
			},
			'quantityonhand':{
				required:true,
					digits: true,
			},
			'proDescription':{
				required: false,
				minlength: 3,
				maxlength: 41,
			},
		},
		messages : {
			productCode : {
				remote: 'Product code already exists'
			}
		}
    });
    
        $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
    
         
	$('#type').change(function(){
    
    
      if($(this).val()=='Discount'){
      $('#discount_div').show();
    $('#averageCost_div').hide();
      $('#account_div').show();
       $('#parent_div').show();
        $('#group_div').hide();
      }else if($(this).val()=='SubTotal' || $(this).val()=='Payment' ){
        $('#averageCost_div').hide();
        $('#discount_div').hide();
       $('#account_div').hide();
       $('#parent_div').hide();
        $('#group_div').hide();
      }else if($(this).val()=='Group'){
       $('#group_div').show();
       $('#averageCost_div').hide();
        $('#discount_div').hide();
       $('#account_div').hide();
       $('#parent_div').hide();
      }else{
    $('#discount_div').hide();
        $('#group_div').hide();
    $('#averageCost_div').show();
       $('#account_div').show();
       $('#parent_div').show();
    }
    
    });
                    
                    
                    
                    
     
    
});	

     function item_fields()
	{ 
	   
       var type='group';
                
          $.ajax({
           
          url:'<?php echo base_url(); ?>MerchantUser/get_plan_data_item',
          type:"POST",
          data:{type:type},
          success:function(data){
           console.log(data);
          var plan_data = $.parseJSON(data); 
          console.log(plan_data);
        
        
        
		room++;
		//var  jsplandata = '';
		//console.log(jsplandata);
	//	var plan_data = $.parseJSON(jsplandata); 
		//var plan_data = jsplandata;
		var plan_html ='<option val="">Select Product or Service</option>';
		for(var val in  plan_data) 
        {   //   console.log(plan_data[val]);  
           plan_html+='<option value="'+ plan_data[val]['ListID']+'">'+plan_data[val]['Name']+'</option>'; 
        }
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class=""></div></div><div class="col-sm-2 nopadding"><div class=""><select class="form-control"  onchange="select_plan_val('+room+');"  id="prodID'+room+'" name="prodID[]">'+ plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class=""> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" ></div></div><div class="col-sm-1 nopadding"><div class=""> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate'+room+'" name="unit_rate[]" value=""  ></div></div><div class="col-sm-1 nopadding"><div class=""> <input type="text" onkeypress="return isNumberKey(event)" class="form-control"  id="quantity'+room+'" name="quantity[]" value="" ></div></div>  	     <div class="col-sm-2 nopadding"><div class=""><div class="input-group"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_product_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest);
        
       }
          
            }); 
  }
  
  
   function remove_product_fields(rid)
   {
	
	   $('.removeclass'+rid).remove();
	   
   }
	function select_plan_val(rid){
		
		
		
		var itemID = $('#prodID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>SettingSubscription/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['Name']);
             $('#unit_rate'+rid).val(item_data['SalesPrice']);
			 $('#quantity'+rid).val(item_data['QuantityOnHand']);
			
			}	
		});
		
	}

</script>

</div>


