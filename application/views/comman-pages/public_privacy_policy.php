<style type="text/css">
	body{
		background: #fff;
	}
	.row {
	    margin-left: 100px;
	    margin-right: 100px;
	}
	h1 {
	    font-family: 'Poppins', sans-serif;
	    font-size: 32px;
	    margin: 0px;
	    color: #2f2f2f;
	    font-weight: 500;
	    line-height: 34px;
	    position: relative;
	    padding-bottom: 30px;
	}
	h1:after {
	    left: 0px;
	    height: 6px;
	    bottom: 0px;
	    width: 140px;
	    content: " ";
	    position: absolute;
	    background-color: #1c60fc;
	}
	.title_holder{
		margin: 4% 0%;
	}
	.wpb_wrapper p {
	    line-height: 1.7;
	}
	p {
	    font-family: 'Raleway', sans-serif;
	    font-weight: 300;
	    font-size: 12px;
	}
	.wpb_wrapper{
		color: #00000099;
	}
</style>
<!-- Error Container -->
<div id="container">
    <div class="row">
    	
    
		<div class="title_holder" >
			<h1><span>Privacy Policy</span></h1>
		</div>

		<div class="wpb_wrapper">
			<p><strong>Privacy Policy</strong></p>
			<p>Protecting your private information is our priority. This Statement of Privacy applies to https://chargezoom.com, https://payportal.com and Chargezoom, Inc. and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to Chargezoom. include https://chargezoom.com, https://payportal.com and Chargezoom, Inc. The Chargezoom website is an accounting services automation site. By using the Chargezoom website, you consent to the data practices described in this statement.</p>
		</div>
		<div class="wpb_wrapper">
			<p><strong>COLLECTION OF YOUR PERSONAL INFORMATION</strong></p>
			<p>In order to better provide you with products and services offered on our Site, Chargezoom may collect personally identifiable information, such as your:</p>
			<p style="padding-left: 40px;">First and Last Name</p>
			<p style="padding-left: 40px;">Mailing Address</p>
			<p style="padding-left: 40px;">Company Name</p>
			<p style="padding-left: 40px;">E-mail Address</p>
			<p style="padding-left: 40px;">Phone Number</p>
			<p>If you purchase Chargezoom’s products and services, we collect billing and credit card information. This information is used to complete the purchase transaction.</p>
			<p>Please keep in mind that if you directly disclose personally identifiable information or personally sensitive data through Chargezoom’s public message boards, this information may be collected and used by others.</p>
			<p>We do not collect any personal information about you unless you voluntarily provide it to us. However, you may be required to provide certain personal information to us when you elect to use certain products or services available on the Site. These may include: (a) registering for an account on our Site; (b) entering a sweepstakes or contest sponsored by us or one of our partners; (c) signing up for special offers from selected third parties; (d) sending us an email message; (e) submitting your credit card or other payment information when ordering an purchasing products and services on our Site. To wit, we will use your information for, but not limited to, communicating with you in relation to services and/or products you have requested from us. We also may gather additional personal or non-personal information in the future.</p>
			<p>&nbsp;</p>
			<p><strong>USE OF YOUR PERSONAL INFORMATION</strong></p>
			<p>Chargezoom collects and uses your personal information to operate its website(s) and deliver the services you have requested.</p>
			<p>Chargezoom may also use your personally identifiable information to inform you of other products or services available from Chargezoom and its affiliates.</p>
			<p>&nbsp;</p>
			<p><strong>SHARING INFORMATION WITH THIRD PARTIES</strong></p>
			<p>Chargezoom does not sell, rent or lease its customer lists to third parties.</p>
			<p>Chargezoom may share data with trusted partners to help perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to Chargezoom, and they are required to maintain the confidentiality of your information.</p>
			<p>Chargezoom may disclose your personal information, without notice, if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on Chargezoom or the site; (b) protect and defend the rights or property of Chargezoom; and/or (c) act under exigent circumstances to protect the personal safety of users of Chargezoom, or the public.</p>
			<p>&nbsp;</p>
			<p><strong>AUTOMATICALLY COLLECTED INFORMATION</strong></p>
			<p>Information about your computer hardware and software may be automatically collected by Chargezoom. This information can include: your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the Chargezoom website.</p>
			<p>&nbsp;</p>
			<p><strong>USE OF COOKIES</strong></p>
			<p>The Chargezoom website may use “cookies” to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.</p>
			<p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the Web server that you have returned to a specific page. For example, if you personalize Chargezoom pages, or register with Chargezoom site or services, a cookie helps Chargezoom to recall your specific information on subsequent visits. This simplifies the process of recording your personal information, such as billing addresses, shipping addresses, and so on. When you return to the same Chargezoom website, the information you previously provided can be retrieved, so you can easily use the Chargezoom features that you customized.</p>
			<p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the Chargezoom services or websites you visit.</p>
			<p>&nbsp;</p>
			<p><strong>SECURITY OF YOUR PERSONAL INFORMATION</strong></p>
			<p>Chargezoom secures your personal information from unauthorized access, use, or disclosure. Chargezoom uses the following methods for this purpose:</p>
			<p style="padding-left: 40px;">SSL Protocol</p>
			<p>When personal information (such as a credit card number) is transmitted to other websites, it is protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol.</p>
			<p>We strive to take appropriate security measures to protect against unauthorized access to or alteration of your personal information. Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we strive to protect your personal information, you acknowledge that: (a) there are security and privacy limitations inherent to the Internet which are beyond our control; and (b) security, integrity, and privacy of any and all information and data exchanged between you and us through this Site cannot be guaranteed.</p>
			<p>&nbsp;</p>
			<p><strong>CHILDREN UNDER THIRTEEN</strong></p>
			<p>Chargezoom does not knowingly collect personally identifiable information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use this website.</p>
			<p>&nbsp;</p>
			<p><strong>E-MAIL COMMUNICATIONS</strong></p>
			<p>From time to time, Chargezoom may contact you via email for the purpose of providing announcements, promotional offers, alerts, confirmations, surveys, and/or other general communication. In order to improve our Services, we may receive a notification when you open an email from Chargezoom or click on a link therein.</p>
			<p>If you would like to stop receiving marketing or promotional communications via email from Chargezoom, you may opt out of such communications by unchecking the notifications option in your profile.</p>
			<p>&nbsp;</p>
			<p><strong>CHANGES TO THIS STATEMENT</strong></p>
			<p>Chargezoom reserves the right to change this Privacy Policy from time to time. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your account, by placing a prominent notice on our site, and/or by updating any privacy information on this page. Your continued use of the Site and/or Services available through this Site after such modifications will constitute your: (a) acknowledgment of the modified Privacy Policy; and (b) agreement to abide and be bound by that Policy.</p>
			<p>&nbsp;</p>
			<p><strong>CONTACT INFORMATION</strong></p>
			<p>Chargezoom welcomes your questions or comments regarding this Statement of Privacy. If you believe that Chargezoom has not adhered to this Statement, please contact Chargezoom at:</p>
			<p>&nbsp;</p>
			<p><strong><span style="color: #003b9d;">Chargezoom</span></strong><br>
			17192 Murphy Ave<br>
			Unit 16366<br>
			Irvine, CA 92623</p>
			<p>&nbsp;</p>
			<p>Email Address:<br>
			<span style="color: #003b9d;"><a style="color: #003b9d;" href="mailto:support@chargezoom.com">support@chargezoom.com</a></span></p>
			<p>&nbsp;</p>
			<p>Telephone number:<br>
			(800) 369-1160</p>
			<p>&nbsp;</p>
			<p>Effective as of January 01, 2020</p>
		</div>
		
	</div>
</div>
<!-- END Error Container -->
