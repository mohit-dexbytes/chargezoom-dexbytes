<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
	<div class="msg_data"><?php echo $this->session->flashdata('message'); ?> </div>

    <?php /* ?><ol class="breadcrumb">
        <li class="breadcrumb-item">
          <strong><a href="<?php echo base_url(); ?>Integration/home/index">Dashboard</a></strong>
        </li>
        <li class="breadcrumb-item"><small>Merchant Gateway</small></li>
      </ol><?php */ ?>
   
    <!-- END Forms General Header -->
  
        <!-- Form Validation Example Block -->
		<legend class="leg">Merchant Gateway </legend>
		<div class="block-main" style="position: relative;">  
					    
					          
					       
		<div class="addNewFixRight" >
            <a class="btn btn-sm btn-info"  data-backdrop="static" data-keyboard="false" data-toggle="modal" href="#set_def_gateway">Set Default Gateway</a>
            <a href="#add_gateway" class="btn btn-sm btn-success"  onclick="" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Add New</a>
        </div>
                         
					
				
					
					<!-- All Orders Content -->
        <table id="merch_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class=" text-left">Gateway </th>
                    <!-- <th class="hidden-xs text-left">Merchant ID</th> -->
					<th class=" text-left">Friendly Name</th>
					<th class="hidden-xs text-left">User Name</th>
					<th class="text-center"> Action </th>
                </tr>
            </thead>
            <tbody>
			
			
			
			<?php 
				if(isset($gateways) && $gateways)
				{
					foreach($gateways as $gateway)
					{
						
						
				?>
				<tr>
					
					<td class="text-left cust_view">
						<a href="#edit_gateway" onclick="set_edit_gateway('<?php echo $gateway['gatewayID'];  ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo getGatewayNames($gateway['gatewayType']); ?></a> 
					</td> 
					
					<td class="text-left "><?php echo $gateway['gatewayFriendlyName']; ?> </a> </td>
					
                    <td class="text-left hidden-xs"><?php echo  (strlen($gateway['gatewayUsername']) > 50 ? substr($gateway['gatewayUsername'], 0, 50).'...': $gateway['gatewayUsername']); ?> </a> </td>
				
					<?php 
						if ($gateway['set_as_default']) { 
							$delURL = '';
							$disbaled = 'disabled';
						} else {
							$delURL = 'del_gateway';
							$disbaled = 'onclick="del_gateway_id('.$gateway['gatewayID'].');"';
						} 
					?>
					
				<td class="text-center">
					<div class="btn-group btn-group-xs">
						<a href="#<?php echo $delURL; ?>" <?php echo $disbaled; ?> data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
					</div>
					
		   </td>
		</tr>
				
				<?php } }
				else { echo'<tr><td colspan="4"> No Records Found </td>
					<td style="display:none;"> No Records Found </td>
					<td style="display:none;"> No Records Found </td>
					<td style="display:none;"> No Records Found </td></tr>'; }  
				 ?>
				
			</tbody>
        </table>
        </br>
    
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>


<!-- END Page Content -->
<div id="set_def_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Set Default Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Integration/home/set_gateway_default' class="form-horizontal card_form" >
                     
                 
                    
                                                <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gatewayid" name="gatewayid" class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if(isset($gateways) && !empty($gateways) ){
                                                                foreach($gateways as $gateway_data){
                                                                ?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>"  <?php if($gateway_data['set_as_default']=='1'){ echo 'selected'; } ?>   ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            
                    
                 
                    <div class="pull-right">
                         <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">  
                         
                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-info" value="Save"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

     <!------------ Add popup for gateway   ------->

 <div id="add_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New </h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			<form method="POST" id="nmiform" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>Integration/home/create_gateway">
			  
				<input type="hidden" id="gatewayID" name="creditID" value=""  />
			
		       <div class="form-group ">
                                              
				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
				<div class="col-md-6">
					<input type="text" id="frname" name="frname"  class="form-control " />
						
				</div>	
				</div>

				<div class="form-group ">                    
					<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
					<div class="col-md-6">
						<select id="gateway_opt" name="gateway_opt"  class="form-control">
							<option value="" >Select Gateway</option>
							<?php foreach($all_gateway as $gat_data){ 
							echo '<option value="'.$gat_data['gateID'].'" >'.$gat_data['gatewayName'].'</option>';
							} 
							?>
							
						</select>
					</div>
				</div>
					
		    	<div class="gateway-div" id="nmi_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">NMI Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="NMI Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">NMI Password</label>
                       <div class="col-md-6">
                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="NMI Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >
                           
                        </div>
                    </div>
				</div>

				<div class="gateway-div" id="cz_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="czUser" class="form-control"  placeholder="Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="czPassword" name="czPassword" class="form-control"  placeholder="Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status"  >
                           
                        </div>
                    </div>
				</div>
				
                <div class="gateway-div" id="auth_div" style="display:none">					
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="API LoginID">
                        </div>
                    </div>
					
				 	<div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="Transaction Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >
                           
                        </div>
                    </div>
			 	</div>	

				<div class="gateway-div" id="pay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="PayTrace  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="PayTrace  password">
                           
                        </div>
                    </div>

					<!-- <div class="form-group" style="display:none" id='paytraceIntegratorIdDiv'>
                        <label class="col-md-4 control-label" for="paytraceIntegratorId">Integrator Id</label>
                        <div class="col-md-6">
                            <input type="text" id="paytraceIntegratorId" name="paytraceIntegratorId" class="form-control"  placeholder="Enter Integrator Id">
                        </div>
                    </div> -->

					<div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_cr_status" value='1' name="paytrace_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_ach_status" value='1' name="paytrace_ach_status"  >
                           
                        </div>
                    </div>
					
				</div>	
				
				<div class="gateway-div" id="paypal_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control" placeholder="Enter  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  placeholder="Enter  password">
                           
                        </div>
                    </div>
					
					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  placeholder="Enter  Signature">
                           
                        </div>
                    </div>
					
				</div>	
			
				<div class="gateway-div" id="stripe_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control" placeholder="Publishable Key">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  placeholder="Secret API Key">
                           
                        </div>
                    </div>
					
				</div>	
				
				 <div class="gateway-div" id="usaepay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  placeholder="USAePay PIN">
                           
                        </div>
                    </div>
				</div>
				 <div class="gateway-div" id="heartland_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  placeholder="Secret Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="heart_cr_status" value='1' name="heart_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="heart_ach_status" value='1' name="heart_ach_status"  >
                           
                        </div>
                    </div>
				</div>
				
				<div class="gateway-div" id="cyber_div" style="display:none" >	
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID" name="cyberMerchantID" class="form-control"  placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
				
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber" name="apiSerialNumber" class="form-control" placeholder="Enter API Key ID">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey" name="secretKey" class="form-control"  placeholder="Enter  Secret Key">
                           
                        </div>
                    </div>
					
					 
					
				</div>	
				
				<div class="gateway-div" id="iTransact_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactUsername">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="iTransactUsername" name="iTransactUsername" class="form-control"  placeholder="API Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactAPIKIEY">API Key</label>
                       <div class="col-md-6">
                            <input type="text" id="iTransactAPIKIEY" name="iTransactAPIKIEY" class="form-control"  placeholder="API Key">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_cr_status" name="iTransact_cr_status" checked="checked" value='1' >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_ach_status" name="iTransact_ach_status" value='1'>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="add_surcharge_box">Surcharge</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="add_surcharge_box" name="add_surcharge_box" value='1' onchange="surchageCheckChange(this)">
                        </div>
                    </div>
					<div class="form-group" id='surchargePercentageBox' style="display: none">
                        <label class="col-md-4 control-label" for="surchargePercentage">Surcharge Rate</label>
                        <div class="col-md-6">
							<input type="text" id="surchargePercentage" name="surchargePercentage" class="form-control"  placeholder="Surcharge Rate" value='0'>
                        </div>
                    </div>
				</div>

				<div class="gateway-div" id="fluid_div" style="display:none">         
					<div class="form-group">
						<label class="col-md-4 control-label" for="customerID">API KEY</label>
						<div class="col-md-8">
							<input type="text" id="fluidUser" name="fluidUser" class="form-control"  placeholder="API KEY">
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="fluid_cr_status">Credit Card</label>
						<div class="col-md-8">
							<input type="checkbox" id="fluid_cr_status" name="fluid_cr_status" checked="checked" >
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="fluid_ach_status">Electronic Check</label>
						<div class="col-md-8">
							<input type="checkbox" id="fluid_ach_status" name="fluid_ach_status"  >
							
						</div>
					</div>
				</div>
				<div class="gateway-div" id="basys_div" style="display:none">         
					<div class="form-group">
						<label class="col-md-4 control-label" for="customerID">API KEY</label>
						<div class="col-md-8">
							<input type="text" id="basysUser" name="basysUser" class="form-control"  placeholder="API KEY">
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="basys_cr_status">Credit Card</label>
						<div class="col-md-8">
							<input type="checkbox" id="basys_cr_status" name="basys_cr_status" checked="checked" >
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="basys_ach_status">Electronic Check</label>
						<div class="col-md-8">
							<input type="checkbox" id="basys_ach_status" name="basys_ach_status"  >
							
						</div>
					</div>
				</div>
				<div class="gateway-div" id="payarc_div" style="display:none">         
					<div class="form-group">
						<label class="col-md-4 control-label" for="payarcUser">Secret KEY <span class="text-danger">*</span></label>
						<div class="col-md-8">
								<textarea id="payarcUser" name="payarcUser" class="form-control"  placeholder="Secret KEY"></textarea>
						</div>
					</div>
				</div>
                <div class="gateway-div" id="TSYS_div" style="display:none" >     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsysUserID">API User ID</label>
                        <div class="col-md-6">
                             <input type="text" id="tsysUserID" name="tsysUserID" class="form-control" placeholder="Enter User ID">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsysPassword">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="tsysPassword" name="tsysPassword" class="form-control"  placeholder="Enter Password">
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsysMerchID">Gateway Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="tsysMerchID" name="tsysMerchID" class="form-control"  placeholder="Enter Gateway Merchant ID">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsys_cr_status">Credit Card</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="tsys_cr_status" name="tsys_cr_status" checked="checked" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsys_ach_status">Electronic Check</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="tsys_ach_status" name="tsys_ach_status"  >
                            
                        </div>
                    </div>
                </div>
				<div id="manageMID" class="form-group hidden">
					<label class="col-md-4 control-label" for="card_number">Merchant ID</label>
					<div class="col-md-6">
						<input type="text" id="gatewayMerchantID" name="gatewayMerchantID" class="form-control"  placeholder="Merchant ID (optional)">						
					</div>
					
				</div>

				<div class="form-group">
						<div class="col-md-4 pull-right">
						<button type="submit" class="submit btn btn-sm btn-success">Add</button>
						
						<button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
						
						</div>
				</div> 
			
	   		</form>		    
		</div>
			
            <!-- END Modal Body -->
        </div>
     </div>
	 
  </div>





<!-------------------------- Modal for Edit Gateway ------------------------------>

<div id="edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
			 <div class="modal-header text-center">
                <h2 class="modal-title">Edit Gateway</h2> </div>
                
            	<div class="modal-body">    
			<form method="POST" id="nmiform1" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>Integration/home/update_gateway">
			 
			<input type="hidden" id="gatewayEditID" name="gatewayEditID" value=""  />
			
			
                     <div class="form-group">
						<label class="col-md-4 control-label" for="example-username"> Friendly Name</label>
						<div class="col-md-6">
								<input type="text" id="fname"  name="fname" class="form-control"  value="" placeholder="">
							</div>
			     </div>
  
                    <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <input type="text" name="gateway" id="gateway" readonly class="form-control" />
						</div>
					</div>
					
		  
				<div class="gateway-div1" id="nmi_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser1" name="nmiUser1" class="form-control"; placeholder="Username..">
                        </div>
                   </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="text" id="nmiPassword1" name="nmiPassword1" class="form-control"  placeholder="password..">
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status1" name="nmi_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status1" name="nmi_ach_status1"  >
                           
                        </div>
                    </div>
				</div>

				<div class="gateway-div1" id="cz_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser1" name="czUser1" class="form-control"; placeholder="Username..">
                        </div>
                   </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="text" id="czPassword1" name="czPassword1" class="form-control"  placeholder="password..">
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status1" name="cz_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status1" name="cz_ach_status1"  >
                           
                        </div>
                    </div>
				</div>	
                 <div class="gateway-div1" id="auth_div1" style="display:none">					
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID1" name="apiloginID1" class="form-control" placeholder="API LoginID..">
                        </div>
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey1" name="transactionKey1" class="form-control"  placeholder="Transaction Key..">
                           
                        </div>
                    </div>
                                        <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status1" name="auth_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status1" name="auth_ach_status1"   >
                           
                        </div>
                    </div>
                    
                    
				</div>	
				
				<div class="gateway-div1" id="pay_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser1" name="paytraceUser1" class="form-control"; placeholder="PayTrace  Username..">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword1" name="paytracePassword1" class="form-control"  placeholder="PayTrace  password..">
                           
                        </div>
                    </div>

					<!-- <div class="form-group" style="display:none" id='paytraceIntegratorIdDiv1'>
                        <label class="col-md-4 control-label" for="paytraceIntegratorId1">Integrator Id</label>
                        <div class="col-md-6">
                            <input type="text" id="paytraceIntegratorId1" name="paytraceIntegratorId1" class="form-control"  placeholder="Enter Integrator Id">
                        </div>
                    </div> -->

					<div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_cr_status1" value='1' name="paytrace_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_ach_status1" value='1' name="paytrace_ach_status1"  >
                           
                        </div>
                    </div>
				</div>	
				
				
				<div class="gateway-div1" id="paypal_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser1" name="paypalUser1" class="form-control" placeholder="Enter  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword1" name="paypalPassword1" class="form-control"  placeholder="Enter  password">
                           
                        </div>
                    </div>
					
					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature1" name="paypalSignature1" class="form-control"  placeholder="Enter  Signature">
                           
                        </div>
                    </div>
					
				</div>
				
				<div class="gateway-div1" id="cyber_div1" style="display:none" >	
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID1" name="cyberMerchantID1" class="form-control"  placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
				
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber1" name="apiSerialNumber1" class="form-control" placeholder="Enter API Key ID">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey1" name="secretKey1" class="form-control"  placeholder="Enter  Secret Key">
                           
                        </div>
                    </div>
					
					 
					
				</div>
				
				<div class="gateway-div1" id="stripe_div1" style="display:none" >		
					
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser1" name="stripeUser1" class="form-control" placeholder="Publishable Key..">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword1" name="stripePassword1" class="form-control"  placeholder="Secret API Key..">
                           
                        </div>
                    </div>
					
				</div>
				 
				<div class="gateway-div1" id="usaepay_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey1" name="transtionKey1" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin1" name="transtionPin1" class="form-control"  placeholder="USAePay PIN">
                           
                        </div>
                    </div>
				</div>	
				<div class="gateway-div1" id="heartland_div1" style="display:none" >		
				   
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey1" name="heartpublickey1" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey1" name="heartsecretkey1" class="form-control"  placeholder="Secret Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="heart_cr_status1" value='1' name="heart_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="heart_ach_status1" value='1' name="heart_ach_status1"  >
                           
                        </div>
                    </div>
				</div>

				<div class="gateway-div1" id="iTransact_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactUsername1">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="iTransactUsername1" name="iTransactUsername1" class="form-control"  placeholder="API Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactAPIKIEY1">API Key</label>
                       <div class="col-md-6">
                            <input type="text" id="iTransactAPIKIEY1" name="iTransactAPIKIEY1" class="form-control"  placeholder="API Key">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_cr_status1" name="iTransact_cr_status1" checked="checked" value='1' >
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_ach_status1" name="iTransact_ach_status1" value='1'>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="add_surcharge_box1">Surcharge</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="add_surcharge_box1" name="add_surcharge_box1" value='1' onchange="surchageCheckChange(this, 1)">
                        </div>
                    </div>
					<div class="form-group" id='surchargePercentageBox1' style="display: none">
                        <label class="col-md-4 control-label" for="surchargePercentage1">Surcharge Rate</label>
                        <div class="col-md-6">
							<input type="text" id="surchargePercentage1" name="surchargePercentage1" class="form-control"  placeholder="Surcharge Rate">
                        </div>
                    </div>
				</div>
				<div class="gateway-div1" id="fluid_div1" style="display:none">         
					<div class="form-group">
						<label class="col-md-4 control-label" for="customerID">API KEY</label>
						<div class="col-md-8">
							<input type="text" id="fluidUser1" name="fluidUser1" class="form-control"  placeholder="API KEY">
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="fluid_cr_status1">Credit Card</label>
						<div class="col-md-8">
							<input type="checkbox" id="fluid_cr_status1" name="fluid_cr_status1" checked="checked" >
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="fluid_ach_status1">Electronic Check</label>
						<div class="col-md-8">
							<input type="checkbox" id="fluid_ach_status1" name="fluid_ach_status1"  >
							
						</div>
					</div>
				</div>
				<div class="gateway-div1" id="basys_div1" style="display:none">         
					<div class="form-group">
						<label class="col-md-4 control-label" for="customerID">API KEY</label>
						<div class="col-md-8">
							<input type="text" id="basysUser1" name="basysUser1" class="form-control"  placeholder="API KEY">
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="basys_cr_status1">Credit Card</label>
						<div class="col-md-8">
							<input type="checkbox" id="basys_cr_status1" name="basys_cr_status1" checked="checked" >
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="basys_ach_status1">Electronic Check</label>
						<div class="col-md-8">
							<input type="checkbox" id="basys_ach_status1" name="basys_ach_status1"  >
							
						</div>
					</div>
				</div>
				<div class="gateway-div1" id="payarc_div1" style="display:none">         
					<div class="form-group">
						<label class="col-md-4 control-label" for="payarcUser1">Secret KEY <span class="text-danger">*</span></label>
						<div class="col-md-8">
								<textarea id="payarcUser1" name="payarcUser1" class="form-control"  placeholder="Secret KEY"></textarea>
						</div>
					</div>
				</div>
				<div class="gateway-div1" id="TSYS_div1" style="display:none" >        
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsysUserID1">API UserID</label>
                        <div class="col-md-6">
                             <input type="text" id="tsysUserID1" name="tsysUserID1" class="form-control" placeholder="Enter User ID">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsysPassword1">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="tsysPassword1" name="tsysPassword1" class="form-control"  placeholder="Enter password">
                           
                        </div>
                    </div>
                    
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="tsysMerchID1">Gateway Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="tsysMerchID1" name="tsysMerchID1" class="form-control"  placeholder="Enter Gateway Merchant ID">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsys_cr_status1">Credit Card</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="tsys_cr_status1" name="tsys_cr_status1" checked="checked" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="tsys_ach_status1">Electronic Check</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="tsys_ach_status1" name="tsys_ach_status1"  >
                            
                        </div>
                    </div>
                </div>
                     <div class="form-group hidden" id="manageMID1">
							<label class="col-md-4 control-label" for="example-username"> Merchant ID</label>
						<div class="col-md-6">
							<input type="text" id="mid"  name="mid" class="form-control"  value="" placeholder="MerchantID (optional)"> 
						</div>
			      </div>
							
	            <div class="form-group">
					<div class="col-md-4 pull-right">
					
					<button type="submit" class="submit btn btn-sm btn-success"> Save </button>
					
					<button  type="button" align="right" class="btn btn-sm  btn-primary1 close1" data-dismiss="modal"> Cancel </button>
                   
                   </div>
            </div>
	 </form>
	 </div>
	<!--------- END ---------------->
		</div>

	</div>
 
 </div>

     <!------- Modal for Delete Gateway ------>

  <div id="del_gateway" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_gateway" method="post" action='<?php echo base_url(); ?>Integration/home/delete_gateway' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Gateway? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantgatewayid" name="merchantgatewayid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<style>

.table.dataTable {
  width:100% !important;
 }
 .block-title h1{
font-size: 16px;
}


@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){  
				   Pagination_view.init(); 
 });

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#merch_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();





function del_gateway_id(id){
	
	     $('#merchantgatewayid').val(id);
}


function set_edit_gateway(gatewayid)
{
	
	if(gatewayid !=""){
		//$("#btnclose").hide();
	  //$('#edit_gateway_data').css('display','block');
	
    
     	$.ajax({
			url: '<?php echo base_url("Integration/home/get_gatewayedit_id")?>',
			type: 'POST',
			data:{gatewayid:gatewayid},
			dataType: 'json',
			success: function(data){
			
				$('#gatewayEditID').val(data.gatewayID);		
				
				$('.gateway-div1').hide();
				
				if(data.gatewayType=='1'){
					$('#nmi_div1').show();
                    $('#manageMID1').show();
					$('#nmiUser1').val(data.gatewayUsername);
					$('#nmiPassword1').val(data.gatewayPassword);
					if(data.creditCard=='1'){
						$('#nmi_cr_status1').attr('checked','checked');  
						
					} else {
						$('#nmi_cr_status1').removeAttr('checked');    
					}
					if(data.echeckStatus=='1'){
						$('#nmi_ach_status1').attr('checked','checked');
					} else {
						$('#nmi_ach_status1').removeAttr('checked');
					}
				} else if(data.gatewayType=='2'){
					$('#auth_div1').show();
					$('#manageMID1').show();
					$('#apiloginID1').val(data.gatewayUsername);
					$('#transactionKey1').val(data.gatewayPassword);
					if(data.creditCard==1){
						$('#auth_cr_status1').attr('checked','checked');  
					}else{
						$('#auth_cr_status1').removeAttr('checked');    
					}

					if(data.echeckStatus==1)
						$('#auth_ach_status1').attr('checked','checked');
					else
						$('#auth_ach_status1').removeAttr('checked');
				} else if(data.gatewayType=='3'){
					$('#pay_div1').show();
					$('#manageMID1').show();
					$('#paytraceUser1').val(data.gatewayUsername);
					$('#paytracePassword1').val(data.gatewayPassword);
					// $('#paytraceIntegratorId1').val(data.gatewaySignature);
					if(data.creditCard==1){
						$('#paytrace_cr_status1').attr('checked','checked');  
					}else{
						$('#paytrace_cr_status1').removeAttr('checked');    
					}
					if(data.echeckStatus==1){
						$('#paytrace_ach_status1').attr('checked','checked');
						// $('#paytraceIntegratorIdDiv1').show();
					}
					else {
						$('#paytrace_ach_status1').removeAttr('checked');
						// $('#paytraceIntegratorIdDiv1').hide();
					}
				} else if(data.gatewayType=='4'){
					$('#paypal_div1').show();
					$('#manageMID1').show();
					$('#paypalUser1').val(data.gatewayUsername);
					$('#paypalPassword1').val(data.gatewayPassword);
					$('#paypalSignature1').val(data.gatewaySignature);
				} else if(data.gatewayType=='5'){
					$('#stripe_div1').show();
					$('#manageMID1').show();
					$('#stripeUser1').val(data.gatewayUsername);
					$('#stripePassword1').val(data.gatewayPassword);
				} else if(data.gatewayType=='6') {
					$('#usaepay_div1').show();
					$('#manageMID1').show();
					$('#transtionKey1').val(data.gatewayUsername);
					$('#transtionPin1').val(data.gatewayPassword);
				} else if(data.gatewayType=='7'){
					$('#heartland_div1').show(); 
					$('#manageMID1').show();
					$('#heartpublickey1').val(data.gatewayUsername);
					$('#heartsecretkey1').val(data.gatewayPassword);
                    if(data.creditCard==1){
                        $('#heart_cr_status1').attr('checked','checked');  
                    }else{
                        $('#heart_cr_status1').removeAttr('checked');    
                    }
                    if(data.echeckStatus==1){
                        $('#heart_ach_status1').attr('checked','checked');
                    }
                    else {
                        $('#heart_ach_status1').removeAttr('checked');
                    }
				} else if(data.gatewayType=='8'){
					
					$('#cyber_div1').show();
					$('#manageMID1').show();
					$('#cyberMerchantID1').val(data.gatewayUsername);
				
					$('#apiSerialNumber1').val(data.gatewayPassword);
					$('#secretKey1').val(data.gatewaySignature);
				} else if(data.gatewayType=='9'){
					$('#cz_div1').show();
					$('#manageMID1').show();
					$('#czUser1').val(data.gatewayUsername);
					$('#czPassword1').val(data.gatewayPassword);
					
					if(data.creditCard=='1'){
						$('#cz_cr_status1').attr('checked','checked');  
						
					}else{
						$('#cz_cr_status1').removeAttr('checked');    
					}
					if(data.echeckStatus=='1')
					{
						
					$('#cz_ach_status1').attr('checked','checked');
					}
					else{
					
					$('#cz_ach_status1').removeAttr('checked');
					}
				} else if(data.gatewayType=='10'){
					$('#iTransact_div1').show();
					$('#manageMID1').show();
					$('#iTransactUsername1').val(data.gatewayUsername);
					$('#iTransactAPIKIEY1').val(data.gatewayPassword);
					$('#surchargePercentage1').val(data.surchargePercentage);
					
					if(data.creditCard=='1'){
						$('#iTransact_cr_status1').attr('checked','checked');  
						
					}else{
						$('#iTransact_cr_status1').removeAttr('checked');    
					}
					if(data.echeckStatus=='1')
					{
						$('#iTransact_ach_status1').attr('checked','checked');
					}
					else{
						$('#iTransact_ach_status1').removeAttr('checked');
					}

					if(data.isSurcharge=='1')
					{
						$('#add_surcharge_box1').attr('checked','checked');
						$('#surchargePercentageBox1').show();
					}
					else{
						$('#add_surcharge_box1').removeAttr('checked');
						$('#surchargePercentageBox1').hide();
					}
				} else if(data.gatewayType=='11'){
					$('#fluid_div1').show();
					$('#manageMID1').show();
					if(data.creditCard=='1'){
						$('#fluid_cr_status1').attr('checked','checked');  
						
					}else{
						$('#fluid_cr_status1').removeAttr('checked');    
					}
					if(data.echeckStatus=='1')
					{
						$('#fluid_ach_status1').attr('checked','checked');
					}
					else{
						$('#fluid_ach_status1').removeAttr('checked');
					}
				}else if(data.gatewayType=='12'){
                    $('#TSYS_div1').show();
                    $('#manageMID1').hide();
                    $('#tsysUserID1').val(data.gatewayUsername);
                    $('#tsysPassword1').val(data.gatewayPassword);
                    $('#tsysMerchID1').val(data.gatewayMerchantID);
                    if(data.creditCard=='1'){
                        $('#tsys_cr_status1').attr('checked','checked');  
                    }else{
                        $('#tsys_cr_status1').removeAttr('checked');    
                    }
                    if(data.echeckStatus=='1')
                    {
                        $('#tsys_ach_status1').attr('checked','checked');
                    }
                    else{
                        $('#tsys_ach_status1').removeAttr('checked');
                    }
                } else if(data.gatewayType=='13'){
					$('#basys_div1').show();
					$('#basysUser1').val(data.gatewayUsername);
					$('#manageMID1').show();
					if(data.creditCard=='1'){
						$('#basys_cr_status1').attr('checked','checked');  
						
					}else{
						$('#basys_cr_status1').removeAttr('checked');    
					}
					if(data.echeckStatus=='1')
					{
						$('#basys_ach_status1').attr('checked','checked');
					}
					else{
						$('#basys_ach_status1').removeAttr('checked');
					}
				} else if(data.gatewayType=='15'){
					$('#payarc_div1').show();
					$('#payarcUser1').val(data.gatewayUsername);
					$('#manageMID1').show();
				}

				$('#fname').val(data.gatewayFriendlyName);
				$('#gateway').val(data.gateway);
				$('#mid').val(data.gatewayMerchantID);
			}	
		}); 
	
  	}

}

function surchageCheckChange(e, divId = ''){
	if($('#'+e.id).prop('checked') == true){
		$('#surchargePercentageBox'+divId).show();
	} else {
		$('#surchargePercentageBox'+divId).hide();
	}
}

$(function(){  
     
	$('#gateway_opt').change(function(){
		var gateway_value =$(this).val();

		$('.gateway-div').hide();

		if(gateway_value=='3'){			
			$('#pay_div').show();
            $('#manageMID').show();
            
		} else if(gateway_value=='2'){
			$('#auth_div').show();
            $('#manageMID').show();
		} else if(gateway_value=='1'){
			$('#nmi_div').show();	
			$('#manageMID').show();
		} else if(gateway_value=='4'){
			$('#paypal_div').show();	
			$('#manageMID').show();
		} else if(gateway_value=='5'){
			$('#stripe_div').show();
			$('#manageMID').show();
		} else if(gateway_value=='6'){
			$('#usaepay_div').show();
			$('#manageMID').show();
		} else if(gateway_value=='7'){
			$('#heartland_div').show();
			$('#manageMID').show();
		} else if(gateway_value=='8'){	
			$('#cyber_div').show();
			$('#manageMID').show();
		} else if(gateway_value=='9'){
			$('#cz_div').show();
			$('#manageMID').show();
		} else if(gateway_value=='10'){
			$('#iTransact_div').show();
            $('#manageMID').show();
		} else if(gateway_value=='11'){
			$('#fluid_div').show();
			$('#manageMID').show();
		}  else if(gateway_value=='12'){
            $('#TSYS_div').show();
            $('#manageMID').hide();
        }else if(gateway_value=='13'){
			$('#basys_div').show();
            $('#manageMID').show();
		}else if(gateway_value=='15'){
			$('#payarc_div').show();
            $('#manageMID').show();
		}  else{
			$('#manageMID').show();
		}
			
	}); 
	  
	// $('#gateway_opt option[value="9"]').attr("selected",true);
	// $('#cz_div').show();
	  
	$('#nmiform').validate({ // initialize plugin
			ignore:":not(:visible)",			
			rules: {
				
			'gateway_opt':{
				required: true,
			},
			'iTransactUsername':{
				required: true,
			},
			'iTransactAPIKIEY':{
				required: true,
			},	
			'surchargePercentage':{
				surcharge_valid_rate: 'add_surcharge_box',
			},
			'fluidUser':{
				required: true,
			},
			'tsysUserID':{
                required: true,
            },
            'tsysPassword':{
                required: true,
            },
            'tsysMerchID':{
                required: true,
            },
			'nmiUser': {
					required: true,
					minlength: 3
					},
				'nmiPassword':{
					required : true,
					minlength: 5,
					},
			
				'czUser': {
					required: true,
					minlength: 3
                },
				
				'czPassword':{
					required : true,
					minlength: 3,
				},
			
			
			
				'apiloginID':{
					required: true,
					minlength: 3
				},
				'transactionKey':{
					required: true,
					minlength: 3
				},		 
			
				'frname':{
					required: true,
					minlength: 3
				},	
			
				'nmiUser1': {
					required: true,
					minlength: 3
				},
				'nmiPassword1':{
					required : true,
					minlength: 5,
				},
			
				'czUser1': {
					required: true,
					minlength: 3
                },
				
				'czPassword1':{
					required : true,
					minlength: 3,
				},
			
				'apiloginID1':{
					required: true,
					minlength: 3
				},
				
				'transactionKey1':{
					required: true,
					minlength: 3
				},
			
			
				'paytracePassword':{
					required: true,
					minlength: 5
				},

				// 'paytraceIntegratorId': {
				// 	required: true,
				// },
				
				'paytraceUser':{
					required: true,
					minlength: 3
				},
			
				'paytracePassword1':{
					required: true,
					minlength: 5
				},
				'paytraceUser1':{
					required: true,
					minlength: 3
				},
				
				'paypalPassword':{
					required: true,
					minlength: 5
				},
				
				'paypalUser':{
					required: true,
					minlength: 3
				},
			
				'paypalSignature':{
					required: true,
					minlength: 5
				},
				'paypalUser1':{
					required: true,
					minlength: 3
				},
				'paypalPassword1':{
					required: true,
					minlength: 5
				},
				
				'paypalSignature1':{
					required: true,
					minlength: 5
				},
				
				
					'cyberMerchantID':{
					required: true,
					minlength: 5
				},
				
				'apiSerialNumber':{
					required: true,
					minlength: 3
				},
			
				'secretKey':{
					required: true,
					minlength: 5
				},
			
				
				
				
				
				
				'fname':{
				required:true,
				minlength:3,
				}
			
				
		},
	});

  
	$('#nmiform1').validate({ // initialize plugin
			ignore:":not(:visible)",			
			rules: {
			
			'gateway_opt':{
				required: true,
			},
			'iTransactUsername1':{
				required: true,
			},
			'iTransactAPIKIEY1':{
				required: true,
			},
			'surchargePercentage1':{
				surcharge_valid_rate: 'add_surcharge_box1',
			},
			'fluidUser1':{
				required: true,
			},
			'tsysUserID1':{
                required: true,
            },
            'tsysPassword1':{
                required: true,
            },
            'tsysMerchID1':{
                required: true,
            },
			'nmiUser': {
					required: true,
					minlength: 3,
					maxlength: 25,
					validate_char: true
			},
			
			'nmiPassword':{
					required : true,
					minlength: 5,
			},
			
			'apiloginID':{
				required: true,
				minlength: 3
			},
			
			'transactionKey':{
				required: true,
				minlength: 3
			},		 
			
			'frname':{
					required: true,
					minlength: 3,
					maxlength: 25,
					validate_char: true
			},	
			
			'nmiUser1': {
					required: true,
					minlength: 3,
					maxlength:25,
					validate_char:true
			},
			
			'nmiPassword1':{
					required : true,
					minlength: 5,
			},
			
			'apiloginID1':{
				required: true,
					minlength: 3
			},
			
		'transactionKey1':{
				required: true,
					minlength: 3
			},
			
		'paytracePassword':{
				required: true,
					minlength: 3
			},
			
		'paytraceUser':{
				required: true,
					minlength: 3,
					maxlength:25,
				validate_char:true
			},
			
		'paytracePassword1':{
				required: true,
					minlength: 5
			},
		// 'paytraceIntegratorId1': {
		// 	required: true,
		// },
			
		'paytraceUser1':{
				required: true,
					minlength: 3,
					maxlength:25,
					// validate_char:true
			},
			
			'cyberMerchantID1':{
				required: true,
				minlength: 5
			},
			
			'apiSerialNumber1':{
				required: true,
					minlength: 3
			},
			
			'secretKey1':{
				required: true,
				minlength: 5
			},
			
			
			'fname':{
				required:true,
				minlength:3,
				maxlength:25,
				validate_char:true
			}
			
			
		},
	});

	$.validator.addMethod("validate_char", function(value, element) {

		return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);

	}, "Please enter only letters, numbers, space, hyphen or underscore.");

	var surchangeMessage = 'Invalid request';

	$.validator.addMethod('surcharge_valid_rate', function(value, element, param) {
		var result = true;
		if($('#'+param).prop('checked') == true){
			surchangeMessage = 'Please enter valid surcharge rate';
			if(value > 0) {
				var regexp = /^[0-9]+([,.][0-9]+)?$/g;
				return regexp.test(value);
			} else {
				return false;
			}
		}
		return this.optional(element) || result;
	}, function() { return surchangeMessage; });
	
    
	var GetMyRemote=function(element_name){
		$('#update_btn').attr("disabled", true); 
		remote={
					beforeSend:function() { 
						$("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
					
					},
					complete:function() {
						$(".overlay1").remove();
					},
					url: '<?php echo base_url(); ?>Payments/testNMI',
				type: "post",
				data:
					{
						nmiuser: function(){return $('input[name=nmiUser]').val();},
						nmipassword:  function(){return $('input[name=nmiPassword]').val();}
					},
						dataFilter:function(response){
						
						data=$.parseJSON(response);
							if (data['status'] === 'true') {
								
									
							$('#update_btn').removeAttr('disabled');
							$('#nmiPassword').removeClass('error');	 
								$('#nmiPassword-error').hide();	 
								
									return data['status'] ;				   
									
							} else {
							
								if(data['status'] =='false'){
									$('#nmiform').validate().showErrors(function(){
										
													return {key:'nmiPassword'}; 
													})
								}					   
							}
							
							return JSON.stringify(data[element_name]);
						}
				}
											
		return remote;
	}

	// $('#paytrace_ach_status').bind('change', function () {

	// 	if ($(this).is(':checked'))
	// 		$('#paytraceIntegratorIdDiv').show();
	// 	else
	// 		$('#paytraceIntegratorIdDiv').hide();
	// });

	// $('#paytrace_ach_status1').bind('change', function () {

	// 	if ($(this).is(':checked'))
	// 		$('#paytraceIntegratorIdDiv1').show();
	// 	else
	// 		$('#paytraceIntegratorIdDiv1').hide();
	// });
});
 
</script>

