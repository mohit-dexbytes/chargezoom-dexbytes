<!-- Page content -->
<?php
    $this->load->view('alert');
?>
<style type="text/css">
#transactioList_wrapper .row {
    display: none;
}
</style>

<div id="page-content">
    
    <legend class="leg"> Batch Process </legend>
    <!-- All Orders Block -->
    <div class="block full" style="position: relative;">
        
        

        <h5 style="font-weight: 600;">Transaction Receipt</h5>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="transactioList" class="table dataTable  compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">Customer Name</th>
                    <th class="text-right ">Invoice</th>
                    <th class="hidden-xs hidden-sm text-right">Amount</th>
                    <th class="hidden-xs hidden-sm text-right">Transaction ID</th>
                    <th class="hidden-xs hidden-sm text-right">Response</th>
                    <th class="text-right hidden-xs">Status</th>
                 
                </tr>
            </thead>
            <tbody>
            
                <?php 
                if(count($allTransaction) > 0)
                {
                    foreach($allTransaction as $txn)
                    {
                      
                      if(isset($txn) && $txn)
                      {
                         if($integrationType == 5){ 
                            $customerNameHref = base_url('company/home/view_customer/' . $txn['customerID']);
                            $invoiceHref  = base_url() . 'company/home/invoice_details/' . trim($txn['invoiceID']);
                         }else if($integrationType == 1){

                            $customerNameHref = base_url('QBO_controllers/home/view_customer/' . $txn['customerID']);
                            $invoiceHref  = base_url() . 'QBO_controllers/Create_invoice/invoice_details_page/' . trim($txn['invoiceID']);
                         
                         }else if($integrationType == 2){

                            $customerNameHref = base_url('home/view_customer/' . $txn['customerID']);
                            $invoiceHref  = base_url() . 'home/invoice_details/' . trim($txn['invoiceID']);
                         
                         }else if($integrationType == 3){

                            $customerNameHref = base_url('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $txn['customerID']);
                            $invoiceHref  = base_url() . 'FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/' . trim($txn['invoiceID']);
                         
                         }else if($integrationType == 4){

                            $customerNameHref = base_url('Xero_controllers/home/view_customer/' . $txn['customerID']);
                            $invoiceHref  = base_url() . 'Xero_controllers/Create_invoice/invoice_details_page/' . trim($txn['invoiceID']);
                         
                         }else{
                            $customerNameHref = '#';
                            $invoiceHref = '#';
                         }
                      
                        ?>
                        <tr>
                            
                            <td class="text-left cust_view">
                              <a href="<?php echo $customerNameHref; ?>"><?php echo $txn['customerName']; ?></a>
                            </td>
                
                  
                            <td class="hidden-sm text-right cust_view">

                              <a href="<?php echo $invoiceHref; ?>"><?php echo $txn['invoiceRefNumber']; ?></a>
                            </td>
                  
                    
                  
                            <td class="text-right cust_view">
                              <?php 
                              if($txn['transactionID'] ==''){
                                  echo '<a href="javascript:void(0)">$'.number_format($txn['transactionAmount'], 2).'</a>';
                              }else{
                                if($integrationType == 5){
                                  echo '<a href="#pay_data_process" onclick="set_company_payment_transaction_data(\'' . $txn['transactionID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$'.number_format($txn['transactionAmount'], 2).'</a>';
                                  
                                }else if($integrationType == 1){

                                  echo '<a href="#pay_data_process" onclick="set_qbo_payment_transaction_data(\'' . $txn['transactionID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$'.number_format($txn['transactionAmount'], 2).'</a>';

                                }else if($integrationType == 2){

                                  echo '<a href="#pay_data_process" onclick="set_payment_transaction_data(\'' . $txn['transactionID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$'.number_format($txn['transactionAmount'], 2).'</a>';


                                }else if($integrationType == 3){

                                  echo '<a href="#pay_data_process" onclick="set_fb_payment_transaction_data(\'' . $txn['transactionID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$'.number_format($txn['transactionAmount'], 2).'</a>';

                                }else if($integrationType == 4){

                                  echo '<a href="#" >$'.number_format($txn['transactionAmount'], 2).'</a>';

                                }else{
                                    echo '<a href="#">$'.number_format($txn['transactionAmount'], 2).'</a>';
                                }  
                              }
                              
                            ?>
                               
                            </td>
                            <td class="text-right ">
                              <?php 
                              if($txn['transactionID'] == ''){
                                echo '--';
                              }else{
                                echo $txn['transactionID'];
                              }
                               ?>
                            </td>
                            <td class="text-right ">
                              <?php echo ucfirst(strtolower($txn['transactionStatus'])); ?>
                            </td>
                            <td class="text-right hidden-xs">

                              <?php  
                              if (in_array($txn['transactionCode'], $statusCode)){
                                  echo 'Paid';
                              }else{
                                  echo 'Failed';
                              }
                              ?>
                            </td>
                        </tr>
                
                <?php
                  }
                 } }else{ echo'<tr>
                <td colspan="6"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
                
            </tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ 
Pagination_view111.init(); });



var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#transactioList').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: true, targets: [-1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 500,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>


	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>


</div>
<!-- END Page Content -->