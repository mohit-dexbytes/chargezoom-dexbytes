<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
	
    
    <div class="msg_data"><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- All Orders Block -->
    <legend class="leg"> Plans</legend>
    <div class="block-main full" style="position: relative;">
        <!-- All Orders Title -->
            <!-- <h2><strong> Plans</strong></h2> -->
            <div class="addNewFixRight">
	            <a class="btn pull-lft  btn-sm btn-success subs-btnbk list-add-btnbk"  href="<?php echo base_url(); ?>Integration/SettingPlan/create_plan">Add New </a>
            </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="sub_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   <th class=" text-left">Plan Name</th>
					<th class="hidden-xs hidden-sm text-left">Frequency</th>
                    <th class="hidden-xs hidden-sm text-left">Free Trial</th>
                    <th class="hidden-xs hidden-sm text-left">Recurrence</th>
                    <th class="text-right">Amount</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
				
				
				if(isset($plandata) && $plandata)
				{
					foreach($plandata as $plans)
					{ 
					
				?>
				<tr>
					<td class="text-left cust_view "><a href="<?php echo base_url('Integration/SettingPlan/create_plan/'.$plans['planID']); ?>" data-toggle="tooltip" title="Edit Plan" ><?php echo $plans['planName']; ?> </a></td>
                    <td class="text-left hidden-xs">
                        <?php 
                            if($plans['invoiceFrequency'] == 'dly'){ echo "Daily"; }
                            else if($plans['invoiceFrequency'] == '1wk'){ echo "Every Week"; } 
                            else if($plans['invoiceFrequency'] == '2wk'){ echo "Every Other Week"; }
                            else if($plans['invoiceFrequency'] == 'mon'){ echo "Monthly"; }
                            else if($plans['invoiceFrequency'] == '2mn'){ echo "Every other month"; }
                            else if($plans['invoiceFrequency'] == 'qtr'){ echo "Quarterly"; }
                            else if($plans['invoiceFrequency'] == 'six'){ echo "Every six month"; }
                            else if($plans['invoiceFrequency'] == 'yrl'){ echo "Yearly"; }
                            else if($plans['invoiceFrequency'] == '2yr'){ echo "Every 2 years"; }
                            else if($plans['invoiceFrequency'] == '3yr'){ echo "Every 3 years"; }
                        ?>
                    </td>
					
				    <td class="text-left hidden-xs hidden-sm"> <?php echo $plans['freeTrial']; ?> </td>
				    
				    <td class="text-left hidden-xs hidden-sm"> <?php echo $plans['subscriptionPlan']; ?> </td>
                    
					<td class="text-right ">$<?php echo ($plans['subscriptionAmount'])?number_format($plans['subscriptionAmount'], 2):'0.00'; ?></td>
					
					<td class="text-center">
						<div class="btn-group btn-group-xs">
                            <a href="#del_sub_plan" onclick="set_subplan_id('<?php  echo $plans['planID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
						</div>
					</td>
				</tr>
				
				<?php } }
				else { echo'<tr>
                <td colspan="6"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; }  
					 ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->
    <style>
        @media screen and (max-width:400px){
         .block-title-xs {
          height:85px !important;
         }
        }
        
        

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

	
        
    </style>
  
  <div id="del_sub_plan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Plan</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>Integration/SettingPlan/delete_plan' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Plan?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="plancID" name="plancID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- Load and execute javascript code used only in this page -->


<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){  Pagination_view.init();
 });</script>
<script>



	
 

	

  

  
 
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#sub_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [5] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>
<script>
	  
		

	  
var nmiValidation1 = function() {   

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#gatw').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                     
					 gateway_old: {
							 required:true,
						},
					gateway_new:{
							 required:true,
					},
					
					
                },
               
            });
					
		
		
        }
    };
}();
function set_subplan_id(sID){
	
	  $('#plancID').val(sID);
	
}

window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
		   $(".msg_data").fadeOut('slow');
		}
</script>



</div>
<!-- END Page Content