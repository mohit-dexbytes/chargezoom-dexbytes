

<style>
.recieptPage label{
    font-weight: 800 !important;
}
.recieptPage .row{
    padding-bottom: 10px !important;
}
.recieptPage .first-row{
    padding-bottom: 0px !important;
}
.recieptPage .success{
    color: #3FBF3F;
}
.recieptPage .faild{
    color: #cf4436;
}
.recieptPage a{
    color: #167bc4;
}
.recieptPage hr{
    margin: 10px;
}
.recieptPage p {
    margin-bottom: 0px !important;
    color: #000;
}
.transaction-print-btn{
    font-size: 13px !important;
    color: #ffffff !important;
}
@media print {
   #DivIdToPrint {
        font-size: 11pt;     
       font-family: Consolas;
       padding: 0px;
       margin: 0px;
    }
}
.newBtn{
    position: absolute;
    right: 14px;
    bottom: 30px;
    z-index: 9;
}
.recieptPage .row.first-row{
    margin-top: 10px;
}
</style>
<?php 

    $transactionAmount = isset($transactionAmount) && ($transactionAmount != null)?number_format($transactionAmount, 2):number_format(0,2);
    $surchargeAmount = isset($surchargeAmount) && ($surchargeAmount != null)?number_format($surchargeAmount, 2):number_format(0, 2);

    $totalAmount = isset($totalAmount) && ($totalAmount != null)?number_format($totalAmount, 2):number_format(0, 2);

    $isSurcharge = isset($isSurcharge) && ($isSurcharge != null)?$isSurcharge:0;

    $showPrintButton = true;
    if (strpos($customer_data['proccess_url'], 'create_customer_auth') !== false) {
        $showPrintButton = false;
    }
    $url = $this->uri->segment('1');
    if($url == 'QBO_controllers'){
       $messageLimitEnd = 'Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.';
    }else if($url == 'company'){
        $messageLimitEnd = 'Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.';
    }else if($url == 'FreshBooks_controllers'){
        $messageLimitEnd = 'Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.';
    }else if($url == 'Integration'){
        $messageLimitEnd = 'Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'Integration/home/my_account">Click here</a> to upgrade.';
    }else{
        $messageLimitEnd = 'Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'home/my_account">Click here</a> to upgrade.';
    }

?>
<div id="page-content" class="recieptPage">
 
    <!-- Products Block -->
        <div class="block">
        <!-- Products Title -->
            
            <div class="row first-row">
                <div class="col-md-12">
                    <div class="newBtn">
                        <a class="btn btn-sm btn-info pull-right" href="<?php echo base_url().$customer_data['proccess_url'];?>"><?php echo $customer_data['proccess_btn_text'];?></a>
                    </div>
                    <legend class="leg">Transaction Receipt</legend>

                </div>
                
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <?php 
                    $message = '';
                    if(!empty($this->session->flashdata('success'))){
                       
                        $message = strip_tags($this->session->flashdata('success'));
                        
                    }else if(!empty($this->session->flashdata('message'))){
                    
                        $message = strip_tags($this->session->flashdata('message'));
                    }
                    $returnMessageObj = messageConversionObj($transactionDetail,$message);
                    if(isset($returnMessageObj) && !empty($returnMessageObj)){
                        if(isset($customer_data['checkPlan']) && !$customer_data['checkPlan']){
                                
                            echo '<h5><strong class="faild">'.$messageLimitEnd.'</strong></h5>';
                        }else{
                            echo '<h5><strong class="'.$returnMessageObj['returnClass'].'">'.$returnMessageObj['message'].'</strong></h5>';
                        }
                        
                    }else{
                        echo '<h5><strong class="faild">Transaction Failed - Declined</strong></h5>';
                    }
                    
                   ?>
                
                </div>
            </div> 
            <div class="row">
                <div class="col-md-4 font-14">
                    <label class="amount_label_font">Amount: </label>
                    <?php if(isset($transactionDetail['transactionType']) && strpos(strtolower($transactionDetail['transactionType']), 'refund') !== false){ ?>
                        <span class="amount_label_font"> ($<?php echo $transactionAmount; ?>)</span>
                    <?php }else{ ?>
                        <span class="amount_label_font"> $<?php echo $transactionAmount; ?></span>
                    <?php } ?>
                </div>
            </div>
            <?php if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-8 font-14">
                    
                    <label>Surcharge Amount: </label><span> $<?php echo  $surchargeAmount; ?></span><br>

                </div>
            </div>
            <?php }
            if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-8 font-14">
                    
                     <label>Total Amount: </label><span> $<?php echo $totalAmount;?></span>

                </div>
            </div>
            <?php } ?>
            <div class="row"> 
                <div class="col-md-4">
                    <?php 
                    $date = date("Y-m-d H:i:s");
                        if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                            $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                            $date = getTimeBySelectedTimezone($timezone);
                        }
                    ?>
                    <label>Date: </label> <span><?php echo date("m/d/Y h:i A", strtotime($date)); ?></span>
                </div>
                <div class="col-md-4">
                    <label>Transaction ID:  </label> <span> <?php echo $customer_data['transaction_id'];?></span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <label>IP Address: </label><span> <?php echo $Ip;?></span>
                </div>
                <div class="col-md-8">
                    <label>User: </label><span> <?php echo $name.' ('.$email.')';?></span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <label>Invoice(s): </label>

                    <?php 
                    $invoice_id = '';
                    $custom_data = (isset($transactionDetail) && isset($transactionDetail['custom_data_fields'])) ? $transactionDetail['custom_data_fields'] : false;
                    $po_number = '';
                    $payment_type = '';
                    if($custom_data){
                        $json_data = json_decode($custom_data, 1);
                        if(isset($json_data['invoice_number'])){
                            $invoice_id = $json_data['invoice_number'];
                        }
                        if(isset($json_data['po_number'])){
                            $po_number = $json_data['po_number'];
                        }

                        if(isset($json_data['payment_type'])){
                            $payment_type = $json_data['payment_type'];
                        }
                    }
                    
                    
                    
                    if(empty($invoice_id)){
                        $url = $this->uri->segment('1');
                        $invoiceCount = count($invoice_data);
                        $inc = 1;
                        
                        foreach($invoice_data as $invoice){
                            
                            if($url == 'QBO_controllers'){
                                $linkURL = base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$invoice['invoiceID'];
                                $linInvoiceRef = $invoice['refNumber'];
                            }else if($url == 'company'){
                                $linkURL = base_url().'company/home/invoice_details/'.$invoice['TxnID'];
                                $linInvoiceRef = $invoice['RefNumber'];
                            }else if($url == 'FreshBooks_controllers'){
                                $linkURL = base_url().'FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/'.$invoice['invoiceID'];
                                $linInvoiceRef = $invoice['refNumber'];
                            }else if($url == 'Integration'){
                                $linkURL = base_url().'Integration/Invoices/invoice_details/'.$invoice['invoiceID'];
                                $linInvoiceRef = $invoice['refNumber'];
                            }else{
                                $linkURL = base_url().'home/invoice_details/'.$invoice['TxnID'];
                                $linInvoiceRef = $invoice['RefNumber'];
                            }
                         ?>
                            <?php if($this->session->userdata('vt_plan') == '0'){?>
                                <span>
                                    <a href="<?php echo $linkURL;?>"><?php echo $linInvoiceRef;?></a>
                                </span>
                                <?php if($inc < $invoiceCount){
                                    echo ',';
                                } ?>
                                
                            <?php } else { ?>
                                    <span><?php echo $linInvoiceRef;?></span>
                                    <?php if($inc < $invoiceCount){
                                        echo ',';
                                    } ?>
                            <?php } 
                            $inc++;
                        }
                    }else{
                        echo $invoice_id;
                    }
                    ?>
                    
                </div>
                <div class="col-md-8">
                    <label>PO Number: </label><span> <?php echo $po_number;?></span><br>

                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <label>Payment Type: </label><span> <?php echo $payment_type; ?></span><br>
                </div>
                <div class="col-md-8">
                    
                </div>
            </div>
            
        <!-- Addresses -->
        <?php  
            $BillingAdd = 0;
            $ShippingAdd = 0;

            $isAdd = 0;
            if($customer_data['billing_address1']  || $customer_data['billing_address2'] || $customer_data['billing_city'] || $customer_data['billing_state'] || $customer_data['billing_zip'] || $customer_data['billing_country']){
                $BillingAdd = 1;
                $isAdd = 1;
            }
            if($customer_data['shipping_address1']  || $customer_data['shipping_address2'] || $customer_data['shipping_city'] || $customer_data['shipping_state'] || $customer_data['shipping_zip'] || $customer_data['shiping_counry'] ){
                $ShippingAdd = 1;
                $isAdd = 1;
            }

        ?>

        <div class="row">
        <?php if($isAdd){ ?>
            <?php if($BillingAdd){ ?>
            <div class="col-sm-4">
                <!-- Billing Address Block -->
                <div class="block">
                    <!-- Billing Address Title -->
                    <div class="block-title">
                        <h2>Billing Address</h2>
                    </div>
                    <address>
                        <span>
                            <?php if ($customer_data['billing_name'] != '') {
                                echo $customer_data['billing_name'] ;
                            } 
                            ?>
                        </span>
                        <br>
                        <span>
                            <?php if (isset($customer_data['FullName']) && !empty($customer_data['FullName'])) {
                                echo $customer_data['FullName'];
                            }
                            ?>
                        </span>
                        <br><br>
                        <?php if ($customer_data['billing_address1'] != '') {
                            echo $customer_data['billing_address1'].'<br>'; } ?> 
                            
                            <?php if ($customer_data['billing_address2'] != '') {
                              echo $customer_data['billing_address2'].'<br>'; } else{
                                  echo '';
                              } ?>
                        
                        <?php echo ($customer_data['billing_city']) ? $customer_data['billing_city'] . ',' : ''; ?>
                        <?php echo ($customer_data['billing_state']) ? $customer_data['billing_state'] : ''; ?>
                        <?php echo ($customer_data['billing_zip']) ? $customer_data['billing_zip'].'<br>' : ''; ?> 
                        <?php echo ($customer_data['billing_country']) ? $customer_data['billing_country'].'<br>' : ''; ?> 
                        
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Billing Address Content -->
                </div>
                <!-- END Billing Address Block -->
            </div>
            <?php }
            if($ShippingAdd){ ?>
            <div class="col-sm-4">
                <!-- Shipping Address Block -->
                <div class="block">
                    <!-- Shipping Address Title -->
                    <div class="block-title">
                        <h2>Shipping Address</h2>
                    </div>
                    <!-- END Shipping Address Title -->

                    <address>
                        <span>
                            <?php if ($customer_data['shipping_name'] != '') {
                                    echo $customer_data['shipping_name']; 
                                }
                            ?>
                        </span>
                        <br>
                        <span>
                            <?php if (isset($customer_data['FullName']) && !empty($customer_data['FullName'])) {
                                echo $customer_data['FullName'];
                            }
                            ?>
                        </span>
                        <br><br>
                        <?php if ($customer_data['shipping_address1'] != '') {
                            echo $customer_data['shipping_address1'].'<br>'; } ?> 
                            
                            <?php if ($customer_data['shipping_address2'] != '') {
                              echo $customer_data['shipping_address2'].'<br>'; } else{
                                  echo '';
                              } ?>
                        


                        <?php echo ($customer_data['shipping_city']) ? $customer_data['shipping_city'] . ',' : ''; ?>
                        <?php echo ($customer_data['shipping_state']) ? $customer_data['shipping_state'] : ''; ?>
                        <?php echo ($customer_data['shipping_zip']) ? $customer_data['shipping_zip'].'<br>' : ''; ?> 
                         <?php echo ($customer_data['shiping_counry']) ? $customer_data['shiping_counry'].'<br>' : ''; ?> 
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Shipping Address Content -->
                </div>
                <!-- END Shipping Address Block -->
            </div>
            <?php } ?>
        <?php } ?>
        </div>

        <?php if(isset($referenceMemo) && !empty($referenceMemo)){ ?>
            <div class="row" id="reference_memo_id">
                <div class="col-md-12">
                    <label>Reference Memo: </label> <?php echo $referenceMemo; ?>            
                </div>
            </div>
        <?php } ?>

        <?php if($showPrintButton){ ?>
        <div class="row">
            <div class="col-md-4">
                <a href="<?php echo base_url('ajaxRequest/printTransactionReceiptPDF') ?>" class="btn btn-primary transaction-print-btn" target="_blank">Print Receipt</a>
            </div>
        </div>
        <?php } ?>

        <!-- END Addresses -->
        

