   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>
	<div id="page-content">
	     <div class="msg_data">
        <?php echo $this->session->flashdata('message');   ?>
        </div>
	   
	
<style> .error{color:red; }</style>  
	<legend class="leg"><?php if(isset($customer)){echo "Edit Customer";} else {echo "Create New Customer";} ?></legend>
    <!-- Progress Bar Wizard Block -->
    
	           
      
        <!-- Progress Bar Wizard Content -->
		
		<form  id="customer_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_Customer/create_customer">
			<div class="block">
			 <input type="hidden"  id="customerListID" name="customerListID" value="<?php if(isset($customer)){echo $customer['Customer_ListID']; } ?>" /> 
			 	
			
			    <div class="form-group">
							<label class="col-md-3 control-label" for="example-username">Company Name</label>
							<div class="col-md-7">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($customer)){echo $customer['companyName']; } ?>" placeholder="" data-args="Company Name"><?php echo form_error('companyName'); ?>
							</div>
					</div>
                            
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Customer Name<span class="text-danger">*</span> </label>
					<div class="col-md-7">
					<input type="text" id="fullName" name="fullName" class="form-control"  value="<?php if(isset($customer)){echo $customer['fullName']; } ?>" placeholder="" data-args="Customer Name"><?php echo form_error('fullName'); ?> </div>
					</div>	
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">First Name </label>
					<div class="col-md-7">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($customer)){echo $customer['firstName']; } ?>" placeholder="" data-args="First Name"><?php echo form_error('firstName'); ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Last Name </label>
					<div class="col-md-7">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($customer)){echo $customer['lastName']; } ?>" placeholder="" data-args="Last Name"><?php echo form_error('lastName'); ?> </div>
					</div>
					
					
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Email Address</label>
					<div class="col-md-7">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($customer)){echo $customer['userEmail']; } ?>" placeholder="" data-args="Email Address"><?php echo form_error('fullName'); ?> </div>
					</div>
					<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">Phone Number</label>
							<div class="col-md-7">
								<input type="text" id="phone" name="phone" class="form-control" value="<?php if(isset($customer)){echo $customer['phoneNumber']; } ?>" placeholder="" data-args="Phone Number">
							</div>
						</div>
						
					
				
				</div>
				<legend class="leg"> Billing Address</legend>
			    <div class="block" id="set_bill_data">
                    
				
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 1</label>
						<div class="col-md-7">
							<input type="text" id="address1" name="address1"   value="<?php if(isset($customer)){echo $customer['address1']; } ?>" class="form-control" placeholder="" data-args="Address Line 1"><?php echo form_error('companyAddress1'); ?>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 2</label>
						<div class="col-md-7">
							<input type="text" id="address2" name="address2"  class="form-control"  value="<?php if(isset($customer)){echo $customer['address2']; } ?>"  placeholder="" data-args="Address Line 2">
						</div>
					</div>
					
					
                        
                       <div class ="form-group">
						   <label class="col-md-3 control-label" name="city"> City</label>
						   <div class="col-md-7">
						       <input type="text" id="city" class="form-control" name="companyCity" placeholder="" data-args="City" value="<?php if(isset($customer)){ echo $customer['City']; } ?>">
							
							</div>
                        </div>
						
                        
                        <div class ="form-group">
						   <label class="col-md-3 control-label"  name="state">State</label>
						   <div class="col-md-7">
						       <input type="text" id="state" class="form-control" name="companyState" placeholder="" data-args="State" value="<?php if(isset($customer)){ echo $customer['State']; } ?>">
							
							</div>
                        </div>	
						<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">ZIP Code</label>
							<div class="col-md-7">
								<input type="text" id="zipCode" name="zipCode" class="form-control" value="<?php if(isset($customer)){echo $customer['zipCode']; } ?>" placeholder="" data-args="ZIP Code"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
						<div class ="form-group">
						   <label class="col-md-3 control-label">Country</label>
						   <div class="col-md-7">
						     <input type="text" id="country" class="form-control" name="companyCountry" placeholder="" data-args="Country" value="<?php if(isset($customer)){ echo $customer['Country']; } ?>">
						    
							</div>
                        </div>
                   </div>
                 	
                    <legend>Shipping Address</legend>
                    <div class="block">       
                     	 <div class="form-group"> <div class="col-md-12">
						  <input type="checkbox" id="chk_add_copy"> Same as Billing Address 
						</div></div> 
					<div style="margin-top: 16px;" class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 1</label>
						<div class="col-md-7">
							<input type="text" id="saddress1" name="saddress1"   value="<?php if(isset($customer)){echo $customer['ship_address1']; } ?>" class="form-control" placeholder="" data-args="Address Line 1">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 2</label>
						<div class="col-md-7">
							<input type="text" id="saddress2" name="saddress2"  class="form-control"  value="<?php if(isset($customer)){echo $customer['ship_address2']; } ?>"  placeholder="" data-args="Address Line 2">
						</div>
					</div>
					   <div class ="form-group">
						   <label class="col-md-3 control-label" name="scity">City</label>
						   <div class="col-md-7">
						       	<input type="text" id="scity" class="form-control" name="sCity" placeholder="" data-args="City" value="<?php if(isset($customer)){ echo $customer['ship_city']; } ?>">
								
							</div>
                        </div>
                        <div class ="form-group">
						   <label class="col-md-3 control-label"  name="sstate">State</label>
						   <div class="col-md-7">
						       <input type="text" id="sstate" class="form-control " name="sState" placeholder="" data-args="State" value="<?php if(isset($customer)){ echo $customer['ship_state']; } ?>">
							
							</div>
                        </div>
				
						<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">ZIP Code</label>
							<div class="col-md-7">
								<input type="text" id="szipCode" name="szipCode" class="form-control" value="<?php if(isset($customer)){echo $customer['ship_zipcode']; } ?>" placeholder="" data-args="ZIP Code"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
					
						<div class ="form-group">
						   <label class="col-md-3 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-7">
						     
								<input type="text" id="scountry" class="form-control " name="sCountry" placeholder="" data-args="Country" value="<?php if(isset($customer)){ echo $customer['ship_country']; } ?>">
							</div>
                        </div>	
                        
				</fieldset>
                <div class="form-group">
                	<div class="col-md-3"></div>
					<div class="col-md-7 text-right">
				  		
						<button type="submit" class="submit btn btn-sm btn-success">Save</button>
                     	<a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_Customer/get_customer_data" class=" btn btn-sm btn-primary1">Cancel</a>
					</div>
			    </div>  
	              					
					
		
		
  	</form>
		
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>


$(document).ready(function(){
 
    $('#customer_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'companyName': {
                     minlength: 2,
                     maxlength: 41,
                    },
                    'fullName': {
                         required: true,
                        minlength: 2,
                        maxlength: 41,
                        validate_char:true,
                         remote: {
                                      url: base_url+'ajaxRequest/check_customer_data',
                                    type: "POST",
                                    cache: false,
                                    dataType: 'json',
                                    data: {
                                        customer: function(){ return $("#fullName").val(); },
                                         customerID: function(){ return $("#customerListID").val(); }
                                    },
                                    dataFilter: function(response) {
                                    
                                       var rsdata = jQuery.parseJSON(response)
                                      
                                         if(rsdata.status=='success')
                                         return true;
                                         else
                                         return false;
                                    }
                                },
                               
                    },
					  'firstName': {
                        minlength: 2,
                        maxlength: 100,
                        validate_char:true,
                    },
					 'lastName': {
                    minlength: 2,
                    maxlength: 100,
                    validate_char:true,
                    },
					
					'userEmail':{
						  email:true,
						  
					},
			
					  
                     'address1': {
			    	   minlength: 2,
			    	    maxlength: 41,
                    },
                    'saddress1': {
                        minlength: 2,
                        maxlength: 41,
                    },
                    'address2': {
                        minlength: 2,
                        maxlength: 41,
                    },
                    'saddress2': {
                        maxlength: 41,
                    },
					'companyCountry': {
					         minlength: 2,
					        maxlength: 31,
					},
					'sCountry': {
					      minlength: 2,
					      maxlength: 31,
					},
					'companyCity': {
                         minlength: 2,
					     maxlength: 31,
					},
					'sCity': {
                        minlength: 2,
					    maxlength: 31,
					},
                    'companyState': {
                       minlength: 2,
                        maxlength: 21,
                    }, 
                    'sState': {
                        minlength: 2,
                        maxlength: 21,
                    },
					'phone': {
                       minlength: 10,
                         maxlength: 17,
                         phoneUS:true,
						  
					},
				
                    'zipCode': {
						minlength:3,
						maxlength:10,
                    },
					 'szipCode': {
					minlength:3,
						maxlength:10,
                    }
					
			},
			messages: {
           companyCountry: {
            },
           companyState: {
            },
            companyCity: {
            },
            
            	fullName:{
						
                        remote:"This customer name already exist"
					},
            
			}
    });
    
    
    
   		$.validator.addMethod("phoneUS", function(value, element) {
         
         if(value=='')
         return true;
        return  value.match(/^\d{10}$/) || 
    value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
   
        }, "Please specify a valid phone number");
                           
          $.validator.addMethod("ZIPCode", function(value, element) {
         return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
           },"Only alphanumeric and hyphen is allowed" );
                           
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[A-Za-z0-9-&,'._* ]*$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9][a-zA-Z0-9-_/,.# ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
         
    
    
    $('#chk_add_copy').click(function(){

     
     if($('#chk_add_copy').is(':checked')){
                            	$('#saddress1').val($('#address1').val());
								$('#saddress2').val($('#address2').val());
								$('#scity').val($('#city').val());
								$('#sstate').val($('#state').val());
								$('#szipCode').val($('#zipCode').val());
								$('#scountry').val($('#country').val());
     }
    else{
    	 
         var val_sp='';
                            	$('#saddress1').val(val_sp);
								$('#saddress2').val(val_sp);
								$('#scity').val(val_sp);
								$('#sstate').val(val_sp);
								$('#szipCode').val(val_sp);
								$('#scountry').val(val_sp);
    }
     
 });
    
    $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_states'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){

			var s = $('#state');
			 $('#state').append('<option value="Select State">Select State </option>');
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/get_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			$('#city').append('<option value="Select State">Select city</option>');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	
	
    
});	

</script>

</div>


