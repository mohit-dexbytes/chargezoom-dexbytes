
<?php
	$this->load->view('alert');
?>
<style type="text/css">
    .dataTables_wrapper >.row >.col-sm-6.col-xs-7{
        left: 275px !important;
    }
</style>
<div id="page-content">
    	<div class="msg_data ">
			    <?php echo $this->session->flashdata('message');   ?>
			</div>
	
            <!-- Form Validation Example Block -->
		
    <legend class="leg">Refund </legend>
	<div class="full">
	
        <!-- Form Validation Example Title -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs">Invoice</th>
                    <th class="text-right">Amount</th>					
                    <th class="text-right hidden-xs hidden-sm">Date</th>
					<th class="text-right hidden-xs">Type</th>
					<th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
    			<?php 
    			if(isset($transactions) && $transactions)
    			{
			
    				foreach($transactions as $transaction)
    				{
                      $inv_url1='';
                      if(!empty($transaction['invoice_id']) && !empty($transaction['invoice_no']) )
                      {
                        $invs = explode(',',$transaction['invoice_id']);
              
                        $invoice_no = explode(',',$transaction['invoice_no']);
                    
                        foreach($invs as $k=> $inv)
                        {
                          $inv_url = base_url().'FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/'.trim($inv);
                          if(isset($invoice_no[$k])){
                              if($plantype){
                                  $inv_url1.= $invoice_no[$k].',';  

                              }else{
                                  $inv_url1.=' <a href="'.$inv_url.'">'.$invoice_no[$k]. '</a>,';  

                              }
                               
                          }    
                        }
                        $inv_url1 = substr($inv_url1, 0, -1);
                     
                      }else{
                          $inv_url1.='<a href="javascript:void(0);">---</a> ';
                      }
    			?>
				<tr>
				
				<?php if($plantype) { ?>
					<td class="text-left"><?php echo $transaction['fullName']; ?></td>
				    <?php } else {?>
				    	<td class="text-left cust_view"><a href="<?php echo base_url() ;?>FreshBooks_controllers/Freshbooks_Customer/view_customer/<?php echo $transaction['customerListID'];?> "><?php echo $transaction['fullName']; ?></a></td>
				    <?php } ?>
					
				    <td class="text-right hidden-xs cust_view"><?php echo $inv_url1; ?></td>
					
					<td class="text-right">
                        <?php
                          $showRefund = 0;
                          if($transaction['partial']==$transaction['transactionAmount'])
                          {

                              echo "<label class='label label-success'>Fully Refunded: ".'$'.number_format($transaction['partial'],2)."</label><br/>";
                          }
                          else if($transaction['partial'] !='0' )
                          { 
                            $showRefund = 1;
                            echo "<label class='label label-warning'>Partially Refunded: ".'$'.number_format($transaction['partial'],2)." </label><br/>";
                          }
                          else
                          { 
                            $showRefund = 1;
                            echo "";
                             
                          }

                          ?>
                        $<?php echo($transaction['transactionAmount'])? number_format($transaction['transactionAmount'], 2):'0.00'; ?>
                            
                    </td>		
					
					
					<td class="hidden-xs hidden-sm text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
                <td class="hidden-xs text-right">
					     <?php if (strpos($transaction['transactionType'], 'sale') !== false) { 
                            echo "Sale";
                        }else if(strpos($transaction['transactionType'], 'auth') !== false){
                             echo "Authorization";
                         }else if(strpos($transaction['transactionType'], 'capture') !== false){
                             echo "Authorization";
                         }
                         ?>
                            
					</td>
						<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'';   ?></td>
					
					
					
				    <td class="text-center">
                        <div class="btn-group dropbtn ">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle <?php if( $showRefund=='0' ) echo "disabled"; ?>">Select <span class="caret"></span></a>
                            <?php if($transaction['transactionID'] != '' && $transaction['transactionGateway'] > 0){
                                $transactionGateway = $transaction['transactionGateway'];
                                $transactionGatewayName = $transaction['gateway'];
                                $transactionID = $transaction['transactionID'];
                            ?>
                            <ul class="dropdown-menu text-left">
                                <li>
                                    <a href="javascript:void(0)" id="txnRefund<?php  echo $transactionID; ?>" transaction-id="<?php  echo $transactionID; ?>" transaction-gatewayType="<?php echo $transactionGateway;?>" transaction-gatewayName="<?php echo $transactionGatewayName;?>" integration-type="3" class="refunAmountCustom"  data-backdrop="static" data-keyboard="false" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice" data-toggle="modal">Refund</a>

                                  
                                </li>
                                <?php if($transactionGateway != 5){ ?>
                                    <li>
                                        <a href="#payment_voidmod" class=""  onclick="set_void_pay('<?php  echo $transactionID; ?>', <?php echo $transactionGateway;?> );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </div>
                    </td>
				</tr>
				<?php } }
					else { echo'<tr><td colspan="8"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                </tr>'; }  
				?>
			</tbody>
        </table>
    </div>
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js?v=1.0"></script>
    <script>$(function(){ EcomOrders.init(); });</script>

	<script>
    	$(function(){  nmiValidation.init(); });
    	
    	function set_refund_pay(txnid, txntype){
    	
			
    		if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnID').val(txnid);	 
				 var url   = "<?php echo base_url()?>FreshBooks_controllers/Transactions/create_customer_refund";
				}	
			else if(txntype=='2'){
    		    $('#txnIDrefund').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/AuthPayment/create_customer_refund";
			}	
			 else if(txntype=='3'){
				$('#paytxnID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/PaytracePayment/create_customer_refund";
			 }
			 else if(txntype=='4'){
				$('#txnpaypalID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/PaypalPayment/create_customer_refund";
			 }
			 else if(txntype=='5'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/StripePayment/create_customer_refund";
			 } else if(txntype=='6'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/UsaePay/create_customer_refund";
			 } else if(txntype=='7'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/GlobalPayment/create_customer_refund";
			 } else if(txntype=='8'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/CyberSource/create_customer_refund";
			 } else if(txntype=='9'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/Transactions/create_customer_refund";
			 } else if(txntype=='10'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/iTransactPayment/create_customer_refund";
			 } else if(txntype=='11'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/FluidpayPayment/create_customer_refund";
			 } else if(txntype=='12'){
                $('#txnstrID').val(txnid);
                var url   = "<?php echo base_url()?>FreshBooks_controllers/TSYSPayment/create_customer_refund";
             } else if(txntype=='15'){
                $('#txnstrID').val(txnid);
                var url   = "<?php echo base_url()?>FreshBooks_controllers/PayarcPayment/create_customer_refund";
             
             } else if(txntype=='13'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/BasysIQProPayment/create_customer_refund";
			 } else if(txntype=='17'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/MaverickPayment/create_customer_refund";
			 
			 } else if(txntype=='16'){
                $('#txnstrID').val(txnid);
                var url   = "<?php echo base_url()?>FreshBooks_controllers/EPXPayment/create_customer_refund";
             }
			 
			 $("#data_form").attr("action",url);	
    		}
    	}   

        function set_void_pay(txnid, txntype){
            
            if(txnid !=""){
            
                if(txntype=='1'){
                    $('#txnvoidID').val(txnid);	 
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/Transactions/create_customer_void";
                } else if(txntype=='2'){
                    $('#txnvoidID1').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/AuthPayment/create_customer_void";
                } else if(txntype=='3'){
                    $('#txnvoidID2').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/PaytracePayment/create_customer_void";
                } else if(txntype=='4'){
                        $('#paypaltxnvoidID').val(txnid);
                        var url   = "<?php echo base_url()?>FreshBooks_controllers/PaypalPayment/create_customer_void";
                } else if(txntype=='5'){
                    $('#strtxnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/StripePayment/create_customer_void";
                } else if(txntype=='6'){
                    $('#usavoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/UsaePay/create_customer_void";
                } else if(txntype=='7'){
                    $('#strtxnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/GlobalPayment/create_customer_void";
                } else if(txntype=='8'){
                    $('#cybervoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/CyberSource/create_customer_void";
                } else if(txntype=='9'){
                    $('#txnvoidID').val(txnid);	 
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/Transactions/create_customer_void";
                } else if(txntype=='10'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/iTransactPayment/create_customer_void";
                } else if(txntype=='11'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/FluidpayPayment/create_customer_void";
                } else if(txntype=='12'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/TSYSPayment/create_customer_void";
                } else if(txntype=='15'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/PayarcPayment/create_customer_void";
                } else if(txntype=='13'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/BasysIQProPayment/create_customer_void";
                } else if(txntype=='17'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/MaverickPayment/create_customer_void";
                } else if(txntype=='16'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/EPXPayment/create_customer_void";
                } else if(txntype=='14'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>FreshBooks_controllers/CardPointePayment/create_customer_void";
                }
                $("#data_form11").attr("action",url);	
            }
        }
	    
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
           $(".msg_data").fadeOut('slow');
        }
        
	</script>
	
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
    <div id="payment_voidmod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Void Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form11" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/PaypalPayment/create_customer_void' class="form-horizontal" >
                         
                        <p id="message_data">Do you really want to void this transaction? The payment will be dropped if you click "Void" below.</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
							      <input type="hidden" id="usavoidID" name="usavoidID" class="form-control"  value="" />
								  <input type="hidden" id="txnvoidID" name="txnvoidID" class="form-control"  value="" />
								  <input type="hidden" id="txnvoidID1" name="txnvoidID1" class="form-control"  value="" />
								  <input type="hidden" id="txnvoidID2" name="txnvoidID2" class="form-control"  value="" />
								  <input type="hidden" id="paypaltxnvoidID" name="paypaltxnvoidID" class="form-control"  value="" />
								  <input type="hidden" id="strtxnvoidID"    name="strtxnvoidID" class="form-control"  value="" />
								    
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-md-8 pull-left">
                         <input type="checkbox" id="setMail" name="setMail" class="set_checkbox"   /> Send Customer Receipt
                        </div>
    					<div class="col-md-4 pull-right">
            			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        </div>
                        <br />
                        <br />
                
    			    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	 <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/PaypalPayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
							    <input type="hidden" id="txnstrID" name="txnstrID" class="form-control"  value="" />
                                <input type="hidden" id="txnpaypalID" name="txnpaypalID" class="form-control"  value="" />
								 <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
								 <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
								 <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
</div>
