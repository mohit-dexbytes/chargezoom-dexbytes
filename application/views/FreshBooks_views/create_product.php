   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>
	<div id="page-content">

<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->
    <!-- Progress Bar Wizard Block -->
   <legend class="leg"> <?php if(isset($item_pro)){ echo "Edit Product";}else{ echo "Create New Product"; }?></legend> 
    <div class="block">
	               <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;
					?>
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form  id="product_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_plan_product/create_product_services">
			
			 <input type="hidden"  id="productID" name="productID" value="<?php if(isset($item_pro)){echo $item_pro['productID']; } ?>" /> 
			 
				<div class="form-group">
					<label class="col-md-3 control-label" for="example-type">Product Type</label>
					<div class="col-md-7">
						<select id="isService" class="form-control " name="isService">
							<option value="0" <?php  if(isset($item_pro) && $item_pro['isService'] == 0) echo"Selected" ?>>Item</option>
							<option value="1" <?php  if(isset($item_pro) && $item_pro['isService'] == 1) echo"Selected" ?>>Service</option>
						</select>
					</div>
				</div>
			    <div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Name</label>
					<div class="col-md-7">
						<input type="text" id="productName" name="productName" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['Name']; } ?>"  placeholder="" data-args="Product Name"><?php echo form_error('productName'); ?>
					</div>
				</div>
					
					
				<div class="form-group hideFields" style="<?php  if(isset($item_pro) && $item_pro['isService'] == 1) echo"display: none" ?>">
					<label class="col-md-3 control-label" for="example-username"> Sales Product Description </label>
					<div class="col-md-7">
					<input type="text" id="productDesc" name="productDesc" class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['SalesDescription']; } ?>"  placeholder="" data-args="Product Description"> </div>
				</div>
				
				 <div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Sales Price </label>
						<div class="col-md-7">
							<input type="text" id="salePrice" name="salePrice"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['saleCost']; } ?>"  placeholder="" data-args="Product Sale Price" >
						</div>
				</div>

				<div class="form-group hideFields" style="<?php  if(isset($item_pro) && $item_pro['isService'] == 1) echo"display: none" ?>">
						<label class="col-md-3 control-label" for="example-username">Quantity </label>
						<div class="col-md-7">
							<input type="text" id="quantity" name="quantity"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['QuantityOnHand']; } ?>"  placeholder="" data-args="Product Quantity" >
						</div>
				</div>


					<div class="form-group hideFields" style="<?php  if(isset($item_pro) && $item_pro['isService'] == 1) echo"display: none" ?>">
					<label class="col-md-3 control-label" for="example-type">Type </label>
					<div class="col-md-7">
							<select id="ser_type" class="form-control " name="ser_type">
							<option value="">Select Type</option>

						    <option value="Inventory" <?php  if(isset($item_pro) && $item_pro['Inventory'] != 0) echo"Selected" ?>>Inventory</option>
						   </select>
						</div>
					</div>
					
					<div class="form-group" id="stock" style="<?php  if(isset($item_pro) && $item_pro['Inventory'] != 0) { echo "display:block"; } else { echo "display:none"; } ?> ">
						<label class="col-md-3 control-label" for="example-username">Current stock </label>
						<div class="col-md-7">
							<input type="text" id="stock" name="stock"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['Inventory']; } ?>"  placeholder="" data-args="Stock" >
						</div>
					</div>
					
					 <div class="form-group pull-right">
						<div class="col-md-12">
                        
					 <button type="submit" class="submit btn btn-sm btn-success">Save</button>
					    <a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_plan_product/Item_detailes" class=" btn btn-sm btn-primary1">Cancel</a>		
					
					</div>
				    </div>
					
          	</form>
			
				    </div>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->
<script type="text/javascript">
    $('.datepicker').datepicker({
    format: 'mm/dd/yyyy'
});
</script>
<script>

$(document).ready(function(){

$('#ser_type').change( function(){
				if ( $(this).val() == "Inventory")
				{ 
				  $("#stock").show();
				}else{
					$("#stock").hide();	
				}
		  });


		  	$('#isService').change( function(){
				if ( $(this).val() == "0"){
					$(".hideFields").show();
				}else{
					$(".hideFields").hide();	
				}
				// alert($(this).val());
		  	});


    $('#product_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
					 'productDesc': {
                        required: true,
                       
                    },
					 
					 'productName': {
                        required: true,
                        minlength: 3
                    },
					
					'quantity':{
						required: true,
						  digits: true,
					},
					'salePrice':{
					    required: true,
						 
					}					
			},
    });
    
  
	
    
});	

</script>

</div>


