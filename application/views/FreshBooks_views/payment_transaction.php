<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<style>
	.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 270px !important;
	}
</style>
<div id="page-content">

	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
	<legend class="leg">Transactions</legend>
    <div class="full">

		<table id="pay_page" class="table table-bordered table-striped table-vcenter">
			<thead>
				<tr>

					<th class="text-left">Customer Name</th>
					<th class="text-right">Invoice</th>
					<th class="text-right hidden-xs">Amount</th>


					<th class="text-right ">Date </th>
					<th class="text-right hidden-xs">Type</th>
					<th class="text-right hidden-xs hidden-sm">Transaction ID</th>

					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody>

				<?php
				if (isset($transactions) && $transactions) {
					foreach ($transactions as $transaction) {
						$inv_url1 = '';
						if ($transaction['invoice_id'] != "") {
							$invs = explode(',', $transaction['invoice_id']);
							$invoice_no = explode(',', $transaction['invoice_no']);

							foreach ($invs as $k => $inv) {
								$inv_url = base_url() . 'FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/' . $inv;
								if ($plantype) {
									$inv_url1 .= $invoice_no[$k] . ',';
								} else {
									$inv_url1 .= ' <a href="' . $inv_url . '" >' . $invoice_no[$k] . '</a>,';
								}
							}
							$inv_url1 = substr($inv_url1, 0, -1);

						} else
							$inv_url1 .= '<a href="javascript:void(0);">---</a> ';
				?>
						<tr>
							<?php if ($plantype) { ?>
								<td class="text-left visible-lg"><?php echo $transaction['fullName']; ?></td>
							<?php } else { ?>
								<td class="test-left cust_view"><a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_Customer/view_customer/<?php echo $transaction['customerListID']; ?>"> <?php echo $transaction['fullName']; ?></a></td>
							<?php } ?>


							<td class="text-right cust_view "><?php echo $inv_url1; ?></td>
							<td class="hidden-xs text-right cust_view"><a href="#pay_data_process" onclick="set_fb_payment_transaction_data('<?php echo $transaction['transactionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$' . number_format($transaction['transactionAmount'], 2); ?></a></td>


							<?php
                                $transactionDate = $transaction['transactionDate'];
                                if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                                    $timezone = ['time' => $transactionDate, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                                    $transactionDate = getTimeBySelectedTimezone($timezone);
                                }
                            ?>
							<td class=" text-right"><?php echo date('M d, Y h:i A', strtotime($transactionDate)); ?></td>
							<td class="hidden-xs text-right">
								<?php
								$showRefund = 0;
								if ($transaction['partial'] == $transaction['transactionAmount']) {
									echo "<label class='label label-success'>Fully Refunded: " . '$' . number_format($transaction['partial'], 2) . "</label><br/>";
								} else if ($transaction['partial'] != '0') {
									$showRefund = 1;
									echo "<label class='label label-warning'>Partially Refunded: " . '$' . number_format($transaction['partial'], 2) . " </label><br/>";
								} else {
									$showRefund = 1;
									echo "";
								}
								if(strpos(strtolower($transaction['transactionType']), 'refund') !== false){
									$showRefund=0;
                                    echo "Refund";
                                }else if (strpos($transaction['transactionType'], 'sale') !== false || strtoupper($transaction['transactionType']) == 'AUTH_CAPTURE') {
									echo "Sale";
								} else if (strpos($transaction['transactionType'], 'capture') !== false || strtoupper($transaction['transactionType']) != 'AUTH_CAPTURE') {
									echo "Capture";
								} else if (strpos($transaction['transactionType'], 'Offline Payment') !== false) {
									echo "Offline Payment";
								}


								?>

							</td>
							<td class="text-right hidden-xs hidden-sm"><?php echo $transaction['transactionID']; ?></td>


							

							<td class="text-center hidden-xs">
								<div class="btn-group dropbtn">
									<?php if($showRefund == 0) { 
		                                    $disabled_select = 'disabled'; 
		                                }else{
		                                    $disabled_select = '';
		                                } 
		                            ?>
									<a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm  <?php echo $disabled_select; ?> dropdown-toggle">Select <span class="caret"></span></a>
									<ul class="dropdown-menu text-left">

										<?php
										if ($transaction['transaction_user_status'] == '1' || $transaction['transaction_user_status'] == '2') {
											if (
												in_array($transaction['transactionCode'], array('100', '200', '111', '1')) &&
												in_array(strtoupper($transaction['transactionType']), array('SALE', 'AUTH_CAPTURE', 'OFFLINE PAYMENT', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
											) {
												if (($transaction['tr_Day'] == 0 &&  ($transaction['transactionGateway'] == '2' || $transaction['transactionGateway'] == '3')) ||  strtoupper($transaction['transactionType']) == 'OFFLINE PAYMENT') {      ?>


													<li> <a href="javascript:void(0);" class="" data-title="Pending Transaction" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
												<?php } else {  
													if(strtolower($transaction['gateway']) != 'heartland echeck'){
												?>
												
													<li> 
														<a href="javascript:void(0)" id="txnRefund<?php  echo $transaction['transactionID']; ?>" transaction-id="<?php  echo $transaction['transactionID']; ?>" transaction-gatewayType="<?php echo $transaction['transactionGateway'];?>" transaction-gatewayName="<?php echo $transaction['gateway'];?>" integration-type="3" class="refunAmountCustom" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
													</li>

												<?php	}
												?>
												<?php }
											}
											if ($transaction['transaction_user_status'] != '2') {    ?>
												<li><a href="#payment_delete" class="" onclick="set_transaction_pay('<?php echo $transaction['id']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>
											<?php }
										} else {  ?>
											<li><a href="javascript:void(0);" data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>
										<?php } ?>
										<?php 
                                            $transaction_auto_id = $transaction['transactionID'];
                                        ?>
                                        <li><a href="javascript:void('0');" onclick="getPrintTransactionReceiptData('<?php echo $transaction_auto_id; ?>', 3)">Print</a></li>
									</ul>
								</div>

							</td>
						</tr>

				<?php }
				} else {
					echo'<tr><td colspan="7"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                </tr>';
				}
				?>

			</tbody>
		</table>
		<!-- END All Orders Content -->
	</div>
	<!-- END All Orders Block -->


</div>






<div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Refund Payment</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">



				<form id="data_form" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/PaypalPayment/create_customer_refund' class="form-horizontal">
					<p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p>

					<div class="form-group">
						<label class="col-md-4 control-label" for="card_list11">Refund Amount</label>
						<div class="col-md-8">
							<input type="text" name="ref_amount" id="ref_amount" value="" class="form-control" />
						</div>
					</div>
					<div class="pull-right">
						<input type="submit" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund" />
						<button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					</div>
					<br />
					<br />
				</form>

			</div>
			<!-- END Modal Body -->
		</div>
	</div>
</div>

<div id="payment_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Delete Payment</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<form id="data_formpay" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/Payments/void_transaction' class="form-horizontal">
					<p id="message_data">Do you really want to "Void" this payment?</p>
					<div class="form-group">
						<div class="col-md-8">

						</div>
					</div>
					<div class="pull-right">
						<input type="submit" name="btn_cancel" class="btn btn-sm btn-danger" value="Void" />
						<button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					</div>
					<br />
					<br />
				</form>
			</div>
			<!-- END Modal Body -->
		</div>
	</div>
</div>
<div id="print-transaction-div" style="display: none;"></div>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>
	$(function() {
		Pagination_view.init();
	});
</script>
<script>
	var Pagination_view = function() {

		return {
			init: function() {
				/ Extend with date sort plugin /
				$.extend($.fn.dataTableExt.oSort, {

				});

				/ Initialize Bootstrap Datatables Integration /
				App.datatables();

				/ Initialize Datatables /
				$('#pay_page').dataTable({
					columnDefs: [{
							type: 'date',
							targets: [3]
						},
						{
							orderable: false,
							targets: [6]
						}
					],
					order: [
						[3, "desc"]
					],
					pageLength: 10,
					lengthMenu: [
						[10, 25, 50, 100, 500],
						[10, 25, 50, 100, 500]
					]
				});

				/ Add placeholder attribute to the search input /
				$('.dataTables_filter input').attr('placeholder', 'Search');
			}
		};
	}();


	function set_refund_pay(txnid, txntype) {


		if (txnid != "") {

			if (txntype == '1') {
				$('#txnID').val(txnid);
				var url = "<?php echo base_url() ?>FreshBooks_controllers/Transactions/create_customer_refund";
			} else if (txntype == '2') {
				$('#txnIDrefund').val(txnid);
				var url = "<?php echo base_url() ?>FreshBooks_controllers/AuthPayment/create_customer_refund";
			} else if (txntype == '3') {
				$('#paytxnID').val(txnid);
				var url = "<?php echo base_url() ?>FreshBooks_controllers/PaytracePayment/create_customer_refund";
			} else if (txntype == '4') {
				$('#txnpaypalID').val(txnid);
				var url = "<?php echo base_url() ?>FreshBooks_controllers/PaypalPayment/create_customer_refund";
			} else if (txntype == '5') {
				$('#txnstrID').val(txnid);
				var url = "<?php echo base_url() ?>FreshBooks_controllers/StripePayment/create_customer_refund";
			} else if(txntype=='6'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/UsaePay/create_customer_refund";
			} else if(txntype=='7'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/GlobalPayment/create_customer_refund";
			} else if(txntype=='8'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/CyberSource/create_customer_refund";
			} else if(txntype=='9'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/Transactions/create_customer_refund";
			} else if(txntype=='10'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/iTransactPayment/create_customer_refund";
			} else if(txntype=='11'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/FluidpayPayment/create_customer_refund";
			} else if(txntype=='15'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/PayarcPayment/create_customer_refund";
			}else if(txntype=='13'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/BasysIQProPayment/create_customer_refund";
			}else if(txntype=='17'){
				$('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>FreshBooks_controllers/MaverickPayment/create_customer_refund";
			}

			$("#data_form").attr("action", url);
		}
	}



	function set_refund_pay(id, txnid, txntype, refamoutn) {


		var form = $("#data_form");
		$('#ref_amount').val(roundN(refamoutn, 2));
		$('.ref').remove();


		$('<input>', {
			'type': 'hidden',
			'id': 'trID',
			'name': 'trID',
			'class': 'ref',
			'value': id,
		}).appendTo(form);

		if (txnid != "") {

			if (txntype == '1') {
				var url = "<?php echo base_url() ?>FreshBooks_controllers/Transactions/create_customer_refund";


				$('<input>', {
					'type': 'hidden',
					'id': 'txnID',
					'name': 'txnID',
					'class': 'ref',
					'value': txnid,
				}).appendTo(form);

			} else if (txntype == '2') {
				var url = "<?php echo base_url() ?>FreshBooks_controllers/AuthPayment/create_customer_refund";
				$('<input>', {
					'type': 'hidden',
					'id': 'txnIDrefund',
					'name': 'txnIDrefund',
					'value': txnid,
				}).appendTo(form);
			} else if (txntype == '3') {
				var url = "<?php echo base_url() ?>FreshBooks_controllers/PaytracePayment/create_customer_refund";
				$('<input>', {
					'type': 'hidden',
					'id': 'paytxnID',
					'name': 'paytxnID',
					'value': txnid,
				}).appendTo(form);
			} else if (txntype == '4') {
				var url = "<?php echo base_url() ?>FreshBooks_controllers/PaypalPayment/create_customer_refund";

				$('<input>', {
					'type': 'hidden',
					'id': 'txnpaypalID',
					'name': 'txnpaypalID',
					'value': txnid,
				}).appendTo(form);
			} else if (txntype == '5') {
				var url = "<?php echo base_url() ?>FreshBooks_controllers/StripePayment/create_customer_refund";
				$('<input>', {
					'type': 'hidden',
					'id': 'txnstrID',
					'name': 'txnstrID',
					'value': txnid,
				}).appendTo(form);
			}

			$("#data_form").attr("action", url);
		}
	}





	$('#data_form').validate({

		rules: {
			ref_amount: {
				required: true,
				number: true,
				remote: {
					url: "<?php echo base_url(); ?>FreshBooks_controllers/Transactions/check_transaction_payment",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						trID: function() {
							return $("#trID").val();
						},
						
					},
					dataFilter: function(response) {

						var rsdata = jQuery.parseJSON(response)
						
						if (rsdata.status == 'success')
							return true;
						else
							return false;
					}
				},


			},

		},
		messages: {
			ref_amount: {
				required: "Please enter amount",
				number: "Please enter valid amount",
				remote: "Your are not allowed to refund more than actual amount"
			}
		},
	});


	function set_transaction_pay(t_id) {
		var form = $("#data_formpay");
		$('<input>', {
			'type': 'hidden',
			'id': 'paytxnID',
			'name': 'paytxnID'

		}).remove();
		$('<input>', {
			'type': 'hidden',
			'id': 'paytxnID',
			'name': 'paytxnID',
			'value': t_id,
		}).appendTo(form);



	}
</script>


<style>
	.table.dataTable {
		width: 100% !important;
	}

	@media screen and (max-width:352px) {
		.table.dataTable {
			width: 100% !important;
		}

		table.table thead .sorting_desc {
			padding-right: 0px !important;
		}

		table.dataTable thead>tr>th {
			padding-left: 5px !important;
			padding-right: 5px !important;
		}
	}
</style>


<!-- END Page Content -->