  <!-- Page content -->
  <?php
	$this->load->view('alert');
?>
	<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
	<div class="header-section">
		
	</div>
	</div>
	
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	               
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		<h2><strong>Authentication Successful!</strong></h2>
            <div class="text-left col-md-12"><strong>Please check details at Configuration -> Accounting Packages -> FreshBooks Integration</strong> <a class="btn btn-sm  btn-success" href="<?php echo base_url(); ?>FreshBooks_controllers/home/index">OK</a></div></br>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



</div>
