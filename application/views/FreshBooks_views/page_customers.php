<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- All Orders Block -->
    <legend class="leg">Customers</legend>
    <div class="block-main full page_customer" style="position: relative">
        <!-- All Orders Title -->
        <span class="" ><a class="btn btn-sm addNewBtnCustom btn-success" href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_Customer/create_customer">Add New</a></span>
        <div class="block-options show_hide_cust_btn">
                         <?php if($activeval  == 'Active'){  ?>
                               <a id="sh_cust" class="btn btn-sm btn-primary1" href="<?php echo base_url('FreshBooks_controllers/Freshbooks_Customer/get_customer_data').'/'.$activeval ?>">Show/Hide Inactive</a>
                        <?php }  else { ?>
                             <a id="sh_cust" class="btn btn-sm btn-primary1" href="<?php echo base_url('FreshBooks_controllers/Freshbooks_Customer/get_customer_data').'/'.$activeval ?>">Show/Hide Active</a>
                       <?php } ?>
                             <input type="hidden" name="customer_type" id="customer_type"  value="<?php 	if(isset($activeval) && $activeval){ echo $activeval; } ?>">
         </div> 
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">Customer Name</th>
                    <th class="text-left ">Full Name</th>
                    <th class="text-left hidden-xs">Email Address</th>
                   <th class="text-right hidden-xs">Balance</th>
                    <!-- <th class="text-center" style="width:85px !important;">Action</th> -->
                </tr>
            </thead>
            <tbody>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
         
    </div>
    <?php if($activeval == "Active"){ ?>
        <input type="hidden" id="data_type" value="0">
     <?php } else { ?>
        <input type="hidden" id="data_type" value="1">
  <?php  } ?> 
    
    
    <!-- END All Orders Block -->
<div id="qbo_del_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="st_head" >Deactivate Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck_qbo" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_Customer/delete_customer' class="form-horizontal" >
                     
                 
                    <p id="st_msg">Do you really want to deactivate this Customer?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="qbocustID" name="qbocustID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="qbo_btn_cancel" name="qbo_btn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

 <div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script src="<?php echo base_url(JS); ?>/freshbook/customer.js" ></script>
<script>

$(document).ready(function(){
    $(function(){ // EcomOrders.init(); 
Pagination_view111.init(); });

var type = $('#data_type').val();
var table_datatable = '';
var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            table_datatable = $('.compamount').dataTable({
              "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
             "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
              "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('FreshBooks_controllers/Freshbooks_Customer/get_customer_data_ajax')?>",
                "type": "POST" ,
                
                "data":function(data) {
							data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
							data.type = type;
						},
                 
            },

           
            
           "order": [[0, 'asc']],
           
           "sPaginationType": "bootstrap",
				"stateSave": false
               
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
            $('#ecom-orders111_filter').remove();
            $('#ecom-orders111_length').after('<div id="ecom-orders111_filter" class="dataTables_filter"><label><div class="input-group"><input type="search" class="form-control" placeholder="Search" aria-controls="ecom-orders111"><span class="input-group-addon"><i class="fa fa-search"></i></span></div></label></div>');
            $('#ecom-orders111_filter').addClass('col-md-10');
            $('#ecom-orders111_length').addClass('col-md-2');
        }
    };
}();
$(document).on('keyup','.dataTables_filter input', function (){
    table_datatable.fnFilter( this.value ).draw();
});
		  
         $('.compamount .dataTables_filter input').addClass("form-control ").attr("placeholder", "Search");
         // modify table search input
         $('.compamount .dataTables_length select').addClass("m-wrap form-control ");
         // modify table per page dropdown
         $('.compamount .dataTables_length dataTables_length select').select2();
         // initialzie select2 dropdown

       });

</script>



	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

.custom-datatable-filter-class{
    width: 100% !important;
}

#ecom-orders111_length {
    padding-left: 0;
    padding-right: 0;
    width: 75px;
}

#ecom-orders111_filter{
    padding-left: 0;
}

#ecom-orders111_filter label{
    float: left;
}
.addNewBtnCustom{
    right: 0 !important;
}
@media only screen and (min-width: 992px) and (max-width: 1279px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }
}
@media only screen and (min-width: 1280px) and (max-width: 1439px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }   
}

@media only screen and (min-width: 1440px) and (max-width: 1679px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }   
}

@media only screen and (min-width: 1680px) and (max-width: 2180px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }   
}
</style>

 

</div>
<script type="text/javascript">
    function delete_qbo_customer(cust_id, st){
        
        var base_url =$('#base_url').val(); 
        if(st==1)
        {
             $('#qbo_btn_cancel').val("Activate");
             $('#qbo_btn_cancel').removeClass("btn-danger");
              $('#qbo_btn_cancel').addClass("btn-success");
             $('#st_head').html("Activate Customer");
              $('#st_msg').html("Do you really want to activate this Customer?");
        }else{
             $('#qbo_btn_cancel').val("Deactivate");
              $('#qbo_btn_cancel').removeClass("btn-success");
              $('#qbo_btn_cancel').addClass("btn-danger");
             $('#st_head').html("Deactivate Customer");
              $('#st_msg').html("Do you really want to deactivate this Customer?");
              
              
         
        }
        
          $('<input>', {
                            'type': 'hidden',
                            'name': 'st_act',
                             'id': 'st_act',
                           
                        }).remove();
        
       $('<input>', {
                            'type': 'hidden',
                            'name': 'st_act',
                             'id': 'st_act',
                            'value': st,
                        }).appendTo($('#del_ccjkck_qbo'));
 
     $('#qbocustID').val(cust_id);
    }
    window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }
</script>
<!-- END Page Content -->