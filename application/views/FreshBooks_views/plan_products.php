
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<style type="text/css">
    .addNewBtnCustom{
        right: 0 !important;
    }
</style>
<div id="page-content">
    
<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- All Orders Block -->
     <legend class="leg">Products & Services</legend>
    <div class="block-main full" style="position: relative;">
        <!-- All Orders Title -->
        
        <div class="addNewBtnCustom">
             <?php if($this->session->userdata('logged_in')){ ?>
                          
                   <a class="btn btn-sm  btn-success" href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_plan_product/create_product_services">Add New</a>
                 
            
            <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="product" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr> 
                   
                    <th class= "text-left">Item Name</th>
                      <th class="text-left hidden-xs">Description</th>
                    <th class="text-left">Type</th>
                    <th class="text-right hidden-xs">Quantity</th>
                    <th class="text-right hidden-xs">Sale Cost</th>
                    <th class=" text-center" style="width:85px !important;">Action</th>
                   
                </tr>
            </thead>
            <tbody>
            
                
                
            </tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<div id="plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            
            <div class="modal-header text-center">
                <h2 class="modal-title">View Product</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_product"  style="height: 300px; min-height:300px;  overflow: auto; " >
        
            </div>
            <hr>
                <div class="pull-right">
                
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />                  
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
 
 function view_plan_details(productID){
     
    
     if(productID!=""){
         $('#data_form_product').empty();
       $.ajax({
          type:"POST",
          url : '<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_plan_product/plan_product_details',
          data : {'productID':productID},
          success: function(data){
              
                 $('#data_form_product').html(data);
              
              
          }
       });     
         
    
     
     } 
 }


</script>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#product').dataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
                    "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('FreshBooks_controllers/Freshbooks_plan_product/product_ajax')?>",
                    "type": "POST" ,
                    
                    "data":function(data) {
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    //data.status_filter = status_filter;
                },
                    
                },

                
                "columnDefs": [
            { 
                "type": "date",
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
                ],
                "order": [[0, 'asc']],
                
                    "sPaginationType": "bootstrap",
            "stateSave": false 

            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_plan_product(id){
    
      $('#productID').val(id);
    
}

 window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
           $(".msg_data").fadeOut('slow');
        }
        
</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>

</div>

<div id="del_plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Deactivate Product / Service</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_plan_product" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_plan_product/delete_item' class="form-horizontal" >
                     
                 
                    <p>Do you really want to deactivate this Product / Service?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="productID" name="productID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- END Page Content -->