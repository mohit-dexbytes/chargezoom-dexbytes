<!DOCTYPE html>> <html class="no-js" lang="en"> 
    <head>
        <meta charset="utf-8">

        <title><?php echo $template['title'] ?></title>

      
    </head>
	
    <body style="">
	
<!-- Page content -->
<div id="page-content">
    
	<!-- Products Block -->
    <div class="block">
         <?php 
	   
	    switch($type){
			case "1":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Top 10 Due by Customer</strong></h2>
				</div>';
			break;
			case "2":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Top 10 Past Due by Customer</strong></h2>
				</div>';
			break;
			
			case "3":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Top 10 Past Due Invoices by Amount</strong></h2>
				</div>';
			break;
			
			case "4":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Top 10 Past Due Invoices by Days</strong></h2>
				</div>';
			break;
			
			case "5":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Failed Transactions in Last 30 Days</strong></h2>
				</div>';
			break;
			case "6":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Accounts with Credit Cards Expiring</strong></h2>
				</div>';
			break;
			case "7":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Transaction Report - Based on Date Range</strong></h2>
				</div>';
			break;
			case "8":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Open Invoices Report</strong></h2>
				</div>';
			break;
			case "9":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;All Invoices Report</strong></h2>
				</div>';
			break;
			case "10":
			echo '<div class="block-title">
					<h3 style="clear:left;"><strong>&nbsp;&nbsp;Scheduled Payments Report</strong></h2>
				</div>';
			break;
			
			
			default:
			echo'No Records Found';
			break;
			
		}
	   ?>
        <!-- Products Content -->
         <div class="table-responsive">
         
        
       <?php  if(isset($report1)){  ?>
        
         <table class="table table-bordered table-vcenter">
            <thead>
                <tr>
                    <th class="text-left visible-lg">Customer Name</th>
                    <th class="text-left hidden-xs">Full Name</th>
                    <th class="text-left hidden-xs">Email Address</th>
                    <th class="text-right">Amount</th>
                    <th class="hidden-xs text-right">Status</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report1))
				{
					foreach($report1 as $key=> $invoice)
					{
				?>
				<tr>
					 
					<td class="text-left"><?php echo $invoice['firstName'].' '.$invoice['lastName']; ?></td>
					<td class="text-left visible-lg"><?php echo $invoice['fullName']; ?></a></td>
					<td class="hidden-xs text-left"><?php echo $invoice['userEmail']; ?></td>
					<td class="text-right">$<?php echo number_format($invoice['balance'],2); ?></td>
					
				
			     	
					
					<td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
					
				   
					
				</tr>
				
				<?php } }
				else {
				    echo '<tr><td colspan="5">No Records Found</td></tr>';
				}
				?>
				
			</tbody>
        </table>
       <?php }  
       
        if(isset($report2)){        ?> 
        
          <table id="ecom-orders"  class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="visible-lg">Customer</th>
                    <th class="text-right hidden-xs">Full Name</th>
                    <th class="text-right">Amount</th>
					<th class="text-right hidden-xs">Email Address</th>
                    <th class="hidden-xs text-right">Status</th>
                   
                </tr>
            </thead>
            <tbody>
             <?php
				if(!empty($report2))
				{
					foreach($report2 as $key=>$invoice)
					{
				?>
				<tr>
					 
					<td class="text-center"><?php echo $invoice['firstName'].' '.$invoice['lastName']; ?></td>
					<td class="visible-lg"><?php echo $invoice['fullName']; ?></a></td>
				
					<td class="text-right">$<?php echo number_format($invoice['balance'],2); ?></td>
					
					<td class="hidden-xs text-right"><?php echo $invoice['userEmail']; ?></td>
					
					<td class="text-right hidden-xs"><span class="label label-success"><?php  echo $invoice['status']; ?></span></td>
					
				</tr>
				
				<?php } }
				else {echo '<tr><td colspan="5">No Records Found</td></tr>';}
				?>
				
			</tbody>
        </table>
       <?php } 
	   
	    if(isset($report3)){  ?> 
         <table id="ecom-orders"  class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center">Invoice</th>
                    <th class="visible-lg">Customer Name</th>
                    <th class="text-right">Email Address</th>
                    <th class="text-right hidden-xs">DueDate</th>
                    <th class="text-right">Amount</th>
                    <th class="hidden-xs text-right">Status</th>
                  
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report3))
				{
					foreach($report3 as $invoice)
					{
					  if($invoice['status']=='Scheduled'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-center"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:'----'; ?></td>
					<td class="visible-lg"><?php echo $invoice['fullName']; ?></a></td>
					<td class="hidden-xs text-right"><?php echo $invoice['userEmail']; ?></td>
					<td class="hidden-xs text-center"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
					<td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
				
				</tr>
				
				<?php } } 
				else { echo '<tr><td colspan="6">No Records Found</td></tr>'; }
				?>
				
			</tbody>
        </table>
        
          <?php } 
	   
	    if(isset($report4)){  ?> 
          <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center">TxnID</th>
                    <th class="visible-lg">Customer Name</th>
                     <th class="text-right">Email Address</th>
                    <th class="text-right hidden-xs">DueDate</th>
                    <th class="text-right">Amount</th>
				
                    <th class="hidden-xs text-right">Status</th>
                   
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report4))
				{
					foreach($report4 as $invoice)
					{
					
					  if($invoice['status']=='Scheduled'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-center"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:'----'; ?></td>
					<td class="visible-lg"><?php echo $invoice['fullName']; ?></a></td>
				    <td class="hidden-xs text-right"><?php echo $invoice['userEmail']; ?></td>
					<td class="hidden-xs text-center"><?php echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
					
					<td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
				</tr>
				
				<?php } } 
				
					else { echo '<tr><td colspan="6">No Records Found</td></tr>'; }
				?>
				
			</tbody>
        </table>
        
       <?php } 
	   
	    if(isset($report5)){  ?> 
          <table id="ecom-orders"  class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center">TxnID</th>
                    <th class="visible-lg">Customer Name</th>
                    <th class="text-right hidden-xs">Txn Date</th>
                    <th class="text-right">Amount</th>
				    <th class="hidden-xs text-right">Invoice</th>
                     <th class="text-right">Status</th>
                     <th class="text-right">Type</th>
                     
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report5))
				{
				
					foreach($report5 as $invoice)
					{
					if($invoice['transactionCode']!='100'){
					 $lable ="danger";
						$labeltext ="Failed";	   
					}else{
					 $lable ="success";
					 $labeltext ="Success";
							  
					}
				?>
				<tr>
					<td class="text-center"><?php echo ($invoice['transactionID'])?$invoice['transactionID']:$invoice['id']; ?></td>
					<td class="visible-lg"><?php echo $invoice['fullName']; ?></a></td>
				
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>
					<td class="text-right">$<?php echo number_format($invoice['transactionAmount'],2); ?></td>
					
					<td class="hidden-xs text-right"><?php echo $invoice['RefNumber']; ?></td>
					<td class="text-right hidden-xs"><?php echo $labeltext; ?></td>
					<td class="text-right">
						<?php echo ucfirst($invoice['transactionType']); ?>
					</td>
                  
				</tr>
				
				<?php } } 
					else { echo '<tr><td colspan="7">No Records Found</td></tr>'; }
				?>
				
			</tbody>
        </table>
         <?php } 
	   
	    if(isset($report6)){ 
	    ?> 
          <table id="ecom-orders" class="table table-bordered table-striped table-vcenter ">
            <thead>
                <tr>
                    <th class="visible-lg">Customer Name</th>
                    <th class="text-right hidden-xs">Email Address</th>
                    <th class="text-right">Card Number</th>
					<th class="text-right">Card Name</th>
                    <th class="text-right visible-lg">Expired Date</th>
                    <th class="hidden-xs text-right">Status</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report6))
				{
					foreach($report6 as $invoice)
					{
				?>
				<tr>
					
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></a></td>
					<td class="hidden-xs text-right"><?php echo $invoice['Contact']; ?></td>
                    <td class="text-center"><?php echo ($invoice['CardNo'])?$invoice['CardNo']:''; ?></td>
			     	<td class="hidden-xs text-right"><?php echo $invoice['customerCardfriendlyName']; ?></td>
					<td class="text-right"><?php echo date('M, Y', strtotime($invoice['expired_date'])); ?></td>
					<td class="text-right hidden-xs"><span class="label label-danger">Expired</span></td>
					
				   
					<td class="text-center">
							<div class="btn-group btn-group-xs">
							<a href="<?php echo base_url('home/view_customer/'.$invoice['Customer_ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-default"><i class="fa fa-eye"></i></a></div>
					</td>
				</tr>
				
				<?php } } 
				else {echo '<tr><td colspan="6">No Records Found</tr>';}
				?>
				
			</tbody>
        </table>
        
         <?php } 
		
	    if(isset($report7)){ 
	    ?> 
            <table id="ecom-orders"  class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center">Txn ID</th>
                     <th class="hidden-xs text-right">Invoice</th>
					<th class="visible-lg">Customer Name</th>
                    <th class="text-right">Amount</th>
				   <th class="text-right hidden-xs">Date</th>
				    <th class="text-right">Remarks</th>
                     <th class="text-right">Status</th>
                     <th class="text-right">Type</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report7))
				{
				
					foreach($report7 as $invoice)
					{
				
				
					if($invoice['transactionCode'] == 200 || $invoice['transactionCode'] == 100 || $invoice['transactionCode'] == 1 || $invoice['transactionCode'] == 111){
						$lable ="success";
					 	$labeltext ="Success";
					}else{
					 	$lable ="danger";
						$labeltext ="Failed";	   
					}
				?>
				<tr>
					<td class="text-center"><?php echo ($invoice['transactionID'])?$invoice['transactionID']:$invoice['id']; ?></td>
					<td class="hidden-xs text-right"> <?php echo $invoice['RefNumber']; ?></td>
					<td class="visible-lg"><?php echo $invoice['fullName']; ?></td>
					<td class="text-right">
						<?php if(isset($invoice['transactionType']) && strpos(strtolower($invoice['transactionType']), 'refund') !== false){
							echo '('.priceFormat($invoice['transactionAmount'], true).')';
						}else{ 
							echo priceFormat($invoice['transactionAmount'], true);
						} ?>
					</td>
					<td class="hidden-xs text-right"> <?php echo date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>
					 <td class="hidden-xs text-right"> <?php echo $invoice['transactionStatus']; ?></td>
					<td class="text-right hidden-xs"> <?php echo $labeltext; ?></td>
				   
					<td class="text-right">
						<?php echo ucfirst($invoice['transactionType']); ?>
					</td>
				</tr>
				
				<?php } }
				else { echo '<tr><td colspan="8">No Records Found</td></tr>'; }
				?>
				
			</tbody>
        </table>
        
           <?php } 
	   
	    if(isset($report89) &&  !empty($report89) ){ 
	    ?> 
        
        <table id="ecom-orders"  class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center">Invoice</th>
                    <th class="visible-lg">Customer Name</th>
                    <th class="text-right hidden-xs">Paid</th>
                    <th class="text-right">Balance</th>
				    <th class="hidden-xs text-right">DueDate</th>
                     <th class="text-right">Status</th>
                     <th class="text-center">AddedAt</th>
                      
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report89))
				{
				
					foreach($report89 as $invoice)
					{
				
				
					  if($invoice['status']=='Scheduled'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   } else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-center"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:'----'; ?></td>
					<td class="visible-lg"><?php echo $invoice['fullName']; ?></td>
					<td class="hidden-xs text-center">$<?php echo number_format($invoice['AppliedAmount'],2); ?></td>
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
				    <td class="hidden-xs text-center"><?php echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
                    <td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
					
					<td class="text-center">
						<?php echo date('M d, Y', strtotime($invoice['addOnDate'])); ?>
					</td>
				     
				
				</tr>
				
				<?php } }
				else { echo '<tr><td colspan="7">No Records Found</td></tr>';}
				
				?>
				
			</tbody>
        </table>
        
        
         <?php } 
	   
	    if(isset($report10)){ 
	    ?> 
          
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                     <th class="text-left hidden-xs hidden-sm">Invoice</th>
                    <th class="text-left">Customer Name</th>
                    
                    <th class="text-right">Balance</th>
                    <th class="text-right hidden-xs">Due Date</th>
				   <th class="text-right hidden-xs">Amount</th>
				     <th class="hidden-xs text-right">Scheduled Date</th>
                  
                       
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report10))
				{
				
					foreach($report10 as $invoice)
					{
				
				
					
				?>
				<tr>
					<td class="text-left"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:'----'; ?></td>
					<td class="visible-lg text-left"><?php echo $invoice['fullName']; ?></td>
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
					
					<td class="text-right hidden-xs"><?php echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
				   	<td class="hidden-xs text-right">$<?php echo number_format($invoice['Total_payment'],2); ?></td>
				    <td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($invoice['scheduleDate'])); ?></td>
					
					
                 
                     
				</tr>
				
				<?php } }
				else { echo '<tr><td colspan="6">No records Found</td></tr>'; }
				?>
				
			</tbody>
        </table>
          <?php } ?>
         
         
        </div>
	
        <!-- END Products Content -->
    </div>
    <!-- END Products Block -->

    
</div>
<!-- END Page Content -->


<style type="text/css">


body{margin:0;}
.title{
    Position:relative;
    top: 8px;
}
a{background-color:transparent;}
a:active,a:hover{outline:0;}
strong{font-weight:bold;}
table{border-collapse:collapse;border-spacing:0;}
td,th{padding:0;}

h2{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
h2{margin-top:20px;margin-bottom:10px;}
h2{font-size:30px;}
.text-right{text-align:right;}
.text-uppercase{text-transform:uppercase;}
.text-danger{color:#a94442;}
address{margin-bottom:20px;font-style:normal;line-height:1.42857143;}
.row{margin-left:-15px;margin-right:-15px;}

 
table{background-color:transparent;}
th{text-align:left;}
.table{width:100%;max-width:100%;margin-bottom:20px;}
.table>thead>tr>th,.table>tbody>tr>th,.table>tbody>tr>td{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd;}
.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd;}
.table>thead:first-child>tr:first-child>th{border-top:0;}
.table-bordered{border:1px solid #ddd;}
.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tbody>tr>td{border:1px solid #ddd;}
.table-bordered>thead>tr>th{border-bottom-width:2px;}
.table>tbody>tr.active>td{background-color:#f5f5f5;}
.table>tbody>tr.success>td{background-color:#dff0d8;}
.table>tbody>tr.info>td{background-color:#d9edf7;}
.table>tbody>tr.danger>td{background-color:#f2dede;}
.table-responsive{overflow-x:auto;min-height:0.01%;}
@media screen and (max-width:767px){
.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;-ms-overflow-style:-ms-autohiding-scrollbar;border:1px solid #ddd;}
.table-responsive>.table{margin-bottom:0;}
.table-responsive>.table>thead>tr>th,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tbody>tr>td{white-space:nowrap;}
.table-responsive>.table-bordered{border:0;}
.table-responsive>.table-bordered>thead>tr>th:first-child,.table-responsive>.table-bordered>tbody>tr>td:first-child{border-left:0;}
.table-responsive>.table-bordered>thead>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>td:last-child{border-right:0;}
.table-responsive>.table-bordered>tbody>tr:last-child>td{border-bottom:0;}
}
.row:before,.row:after{content:" ";display:table;}
.row:after{clear:both;}

/*! CSS Used from: http://localhost/quickbook/resources/css/main.css */
body{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;color:#394263;font-size:13px; }
#page-content{padding:10px 5px 1px;background-color:#eaedf1;}
.block{margin:0 0 10px;padding:20px 15px 1px;background-color:#ffffff;border:1px solid #dbe1e8;}
h2{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:300;}
h2{margin-bottom:15px;}
a,a:hover,a:focus{color:#1bbae1;}
.text-danger,.text-danger:hover{color:#e74c3c;}
strong{font-weight:600;}
.table.table-vcenter th,.table.table-vcenter td{vertical-align:middle;}
.table thead > tr > th{font-size:18px;font-weight:600;}
.table thead > tr > th{padding-top:14px;padding-bottom:14px;}
.table thead > tr > th,.table tbody > tr > th,.table tbody > tr > td,.table-bordered,.table-bordered > thead > tr > th,.table-bordered > tbody > tr > th,.table-bordered > tbody > tr > td{border-color:#eaedf1;}


.table thead > tr > th,.table tbody > tr > th,.table tbody > tr > td,.table-bordered,.table-bordered > thead > tr > th,.table-bordered > tbody > tr > th,.table-bordered > tbody > tr > td{border-color:#f2f2f2;}
.block{border-color:#e8e8e8;}
a,a:hover,a:focus{color:#888888;}
/*! CSS Used from: Embedded */
body{margin:0;}
strong{font-weight:bold;}
table{border-collapse:collapse;border-spacing:0;}
td,th{padding:0;}

thead{display:table-header-group;}
tr{page-break-inside:avoid;}
h2{orphans:3;widows:3;}
h2{page-break-after:avoid;}
.table{border-collapse:collapse!important;}
.table td,.table th{background-color:#fff!important;}
.table-bordered th,.table-bordered td{border:1px solid #ddd!important;}
}

body{
	    font-size: 14px; 
    color: #333;
}
.div2{
	width:46%; float:left; padding:10px;margin:0px;
}

.row{
	width:100%;
	clear:both;
		    font-size: 14px; 
    color: #333;margin:0px;padding:0px;
}
.block-title{
         padding : 6px 8px; 
}


tr{background-color:#d9edf7;}

 th , td{
    padding : 6px 8px; 
}
 
  

</style>

    </body>
</html>