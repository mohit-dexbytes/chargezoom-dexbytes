<!-- Page content -->
<?php
	$this->load->view('alert');
	$isShipping = $isBilling = $isService = 0;
	if (isset($subs) && $subs['isShipping'] == '1') {
		$isShipping = 1;
	}
	if (isset($subs) && $subs['isBilling'] == '1') {
		$isBilling = 1;
	}
	if (isset($subs) && $subs['isService'] == '1') {
		$isService = 1;
	}
?>


<style type="text/css">
	.selectPayOption input[type="checkbox"] {
	    height: 12px;
	    width: 12px;
	    vertical-align: sub;
	    margin-right: 5px;
	    border-radius: 50% !important;
	    margin-left: 3%;
	}
	.selectPayOptionHide1,.selectPayOptionHide2{
		display: none;
	}
	/* The container */
	.container {
	    
	    position: relative;
	    padding: 0px 0 0 15px;
	    margin-bottom: 16px;
	    cursor: pointer;
	    font-size: 11px;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	}

	/* Hide the browser's default radio button */
	.container input {
	  position: absolute;
	  opacity: 0;
	  cursor: pointer;
	}

	/* Create a custom radio button */
	.checkmark {
	    position: absolute;
	    top: 2px;
	    left: 0;
	    height: 12px;
	    width: 12px;
	    background-color: #ffffff;
	    border: 1px solid #818181;
	    border-radius: 50%;
	}

	/* On mouse-over, add a grey background color */
	.container:hover input ~ .checkmark {
	    background-color: #fff;
	    border: 1px solid #818181;
	}

	/* When the radio button is checked, add a blue background */
	.container input:checked ~ .checkmark {
	    background-color: #ffffff;
	    border: 1px solid #2196F3;
	    padding: 2px;
	}

	/* Create the indicator (the dot/circle - hidden when not checked) */
	.checkmark:after {
	  content: "";
	  position: absolute;
	  display: none;
	}

	/* Show the indicator (dot/circle) when checked */
	.container input:checked ~ .checkmark:after {
	  display: block;
	}

	/* Style the indicator (dot/circle) */
	.container .checkmark:after {
	 	top: 1px;
	    left: 1px;
	    width: 8px;
	    height: 8px;
	    border-radius: 50%;
	    background: #0075ff;
	}
	.step1{
		width: 82px;
	}
	.step2{
		width: 60px;
	}
</style>
<div id="page-content">
	<div class="msg_data">
		<?php echo $this->session->flashdata('message');   ?>
	</div>
	


	<div class="row">
		<!-- Form Validation Example Block -->
		<div class="col-md-12">
				<legend class="leg">Configurable Options</legend>

			
				<form id="form-validation" action="<?php echo base_url('FreshBooks_controllers/SettingPlan/create_plan'); ?>" method="post" class="form-horizontal form-bordered">
				<div class="block">
					<fieldset>
						<div class="col-md-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<label class="control-label" for="customerID">Plan Name</label>
								<div>

									<input type="text" id="plan_name" name="plan_name" onblur="check_subs_link(this);" class="form-control" value="<?php if (isset($subs)) {
																																						echo  $subs['planName'];
																																					}; ?>" placeholder="" data-args="Plan Name" />

									
								</div>
							</div>





							<div class="col-md-3 form-group">


								<label class="control-label" for="customerID">Frequency</label>
								<div>
									<select name="paycycle" id="paycycle" class="form-control">
										<option value="">Select Frequency</option>
										<?php foreach ($frequencies as $frequecy) { ?>
											<option value="<?php echo $frequecy['frequencyValue']; ?>" <?php if (isset($subs) && $frequecy['frequencyValue'] == $subs['invoiceFrequency']) {
																											echo "selected";
																										} ?>><?php echo $frequecy['frequencyText']; ?> </option>
										<?php } ?>
									</select>
								</div>
							</div>



							<div class="col-md-3  form-group">
								<label class="control-label" for="customerID">Recurrence</label>
								<div>

									<input type="text" id="duration_list" name="duration_list" class="form-control" value="<?php if (isset($subs)) {
																																echo  $subs['subscriptionPlan'];
																															}; ?>" placeholder="" data-args="0 for Unlimited" />

									
								</div>
							</div>

							<div class="form-group col-md-3">
								<div class="">

									<label class="control-label ">Free Trial Recurrence</label>

									<input type="text" class="form-control" id="freetrial" name="freetrial" value="<?php if (isset($subs)) echo $subs['freeTrial']; ?>" placeholder="" data-args="Free Trial Recurrence">

								</div>
							</div>
								</div>
						</div>

						<div class="col-md-12 prorata_billing_block" id="MhiddenDiv" <?php if (isset($subs) && $subs['invoiceFrequency'] == 'mon') {  ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							<div class="form-group col-md-12">
								<div class="">
									<label class="col-md-2 prorata-label">Prorata Billing</label>
									<div class="col-md-10"><input type="checkbox" id="prorate_bill_check" name="pro_rate" <?php if (isset($subs) && $subs['proRate'] == '1') {
																																echo "checked";
																															} ?>>
																																																					</div>
								</div>
							</div>
							<div id="pro_div" <?php if (!isset($subs) || $subs['proRate'] != '1') { echo 'style="display:none;"' ;} ?>>
								<div class="form-group col-md-12">
									<div class="">
										<label class="col-md-2 prorata-label">Prorata Date</label>
										<div class="col-md-2"> <input type="text" class="form-control" id="prorata_date" name="pro_billing_date" value="<?php if (isset($subs)) echo $subs['proRateBillingDay'];  else echo 1; ?>" placeholder="" data-args="Prorata Date"> </div>
										<span class="prorata_cont">Enter the day of the month you want to charge on </span>
									</div>
								</div>
								<div class="form-group col-md-12">
									<div class="">
										<label class="col-md-2 prorata-label">Charge Next Month</label>
										<div class="col-md-2"> <input type="text" class="form-control" id="prorata_date_next" name="pro_next_billing_date" value="<?php if (isset($subs)) echo $subs['nextMonthInvoiceDate'];  else echo 25; ?>" placeholder="" data-args="Charge"> </div>
										<span class="prorata_cont">Enter the day of the month after which the following month will also be included on the first invoice </span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
						<div class="row">
							<div class="form-group col-sm-3">
								<div class="">

									<label class=" control-label">Email Recurring Invoices</label>
								</div>
								<div class="">
									<label class="radio-inline control-label">
										<input type="radio" name="email_recurring" <?php if (isset($subs) && $subs['emailRecurring'] == '1') {
																						echo "checked";
																					} ?> value="1">Yes</label><br>
									<label class="radio-inline control-label">
										<input type="radio" name="email_recurring" <?php if (!isset($subs) || $subs['emailRecurring'] == '0') echo "checked"; ?> value="0">No</label>

								</div>
							</div>
							<div class="form-group col-sm-3">
								<div class="">

									<label style="padding:5px;" class=" control-label">Automatic Payment</label>
								</div>
								<div class="">
									<label class="radio-inline control-label">
										<input type="radio" name="autopay" onclick="chk_payment(this.value);" <?php if (isset($subs) && $subs['automaticPayment'] == '1') {
																													echo "checked";
																												} ?> value="1" checked>Yes</label><br>
									<label class="radio-inline control-label">
										<input type="radio" name="autopay" onclick="chk_payment(this.value);" <?php if (isset($subs) && $subs['automaticPayment'] == '0') echo "checked"; ?> value="0">No</label>

								</div>
							</div>

							<div id="set_pay_data" <?php if (isset($subs) && $subs['automaticPayment'] == '0') { ?> style="display:none;" <?php } else if (isset($subs) && $subs['automaticPayment'] == '1') { ?> style="display:block;" <?php } else { ?> style="display:block;" <?php } ?>>
								<div class="form-group col-sm-3">
									<div class="">

										<label style="padding:5px;" class=" control-label">Auto-Rebilling</label>
									</div>
									<div class="">
										<label class="radio-inline control-label">
											<input type="radio" name="rebilling" value="1" checked>Yes</label><br>
										<label class="radio-inline control-label">
											<input type="radio" name="rebilling" value="0">No</label>

									</div>
								</div>

								<?php 
									if(!isset($defaultGateway) || !$defaultGateway){
								?>
								<div class="form-group col-sm-3">
									<div class="">



										<label class=" control-label" for="card_list" style="margin-right: 4%;">Gateway</label>
										<?php 
										$showClass1 = 'selectPayOptionHide1';
										$showClass2 = 'selectPayOptionHide2';
										$checkconditionIsEnableCount = 0;
										if (isset($isCreditCard) && $isCreditCard){
											$showClass1 = 'selectPayOptionShow1';
											$checkconditionIsEnableCount = $checkconditionIsEnableCount + 1;
										}
										if (isset($isEcheck) && $isEcheck){
											$showClass2 = 'selectPayOptionShow2';
											$checkconditionIsEnableCount = $checkconditionIsEnableCount + 1;
										}

										?>
										<input type="hidden" id="checkconditionIsEnableCount" value="<?php echo $checkconditionIsEnableCount; ?>">
										<label class="<?php echo $showClass1; ?> selectPayOption container step1">Credit Card
										  <input type="checkbox" name="cc" id="cc" <?php if(isset($subs['creditCard']) && $subs['creditCard']){ echo "checked"; }  ?> value="<?php echo (isset($subs['creditCard']) && $subs['creditCard'])?$subs['creditCard']:1;  ?>">
						 				  <span class="checkmark"></span>
										</label>
										<label class="<?php echo $showClass2; ?> selectPayOption container step2">eCheck
										  <input type="checkbox" name="ec" id="ec" <?php if(isset($subs) && $subs['eCheck']) echo "checked"; ?> value="<?php echo (isset($subs['eCheck']) && $subs['eCheck'])?$subs['eCheck']:1;  ?>" >
										  <span class="checkmark"></span>
										</label>
										<select id="gateway_list" name="gateway_list" class="form-control">
											<option value="">Select Gateway</option>
											<?php foreach ($gateways as $gateway) { ?>
												<option value="<?php echo $gateway['gatewayID'];  ?>" <?php if (isset($subs) && $subs['paymentGateway'] == $gateway['gatewayID']) {
																											echo "selected";
																										} else if (isset($subs) && ($subs['paymentGateway'] == 0) && $gateway['set_as_default'] == 1) echo 'selected';
																										else if ($gateway['set_as_default'] == 1)     ?>><?php echo $gateway['gatewayFriendlyName']; ?></option>
											<?php } ?>


										</select>
									</div>
								</div>
								<?php } else { ?>
									<input type="hidden" name="gateway_list" value="<?php echo $defaultGateway['gatewayID'];  ?>">
								<?php }  ?>
							</div>
						</div>
					</fieldset>
				</div>
				<legend class="leg"> Products & Services</legend>
				<div class="block">
					<fieldset>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group"> <label class="control-label">Products & Services</label> </div>
							</div>
							<div class="col-sm-2">
								<div class="form-group no-pad-left"><label class="control-label">Recurring / One Time</label></div>
							</div>
							<div class="col-sm-2">
								<div class="form-group"><label class="control-label">Description </label></div>
							</div>
							<div class="col-sm-2">
								<div class="form-group"><label class="control-label">Price </label> </div>
							</div>
							<div class="col-sm-1">
								<div class="form-group text-center"><label class="control-label">Quantity </label></div>
							</div>
							
							<div class="col-sm-2">
								<div class="form-group"><label class="control-label">Total</label></div>
							</div>

						</div>
						<?php
						if (isset($items) && !empty($items)) { ?>
							<input type="hidden" id="item-exist" value="1">
						<?php }else{ ?>
							<input type="hidden" id="item-exist" value="0">

						<?php } ?>
						<div id="item_fields">
							<?php 
							if (isset($items) && !empty($items)) {
								foreach ($items as $k => $item) {
									$rate = $item['itemRate'];
									$qnty = $item['itemQuantity'];
									if (!empty($item['itemTax'])) {
										$tax  = $item['itemTax'];
										echo "<script> $('.set_taxes').css('display','block'); </script>";
									} else {
										$tax = 0;
									}

									$total_amt = ($rate * $qnty) + (($rate * $qnty) * $tax / 100);

							?>
									<div id="danny" class="removeclass<?php echo $k + 1; ?>">
										<div class="col-sm-3 nopadding no-pad-left">
											<div class="form-group">
												<select class="form-control select-chosen" onchange="select_plan_val('<?php echo $k + 1; ?>');" id="productID<?php echo $k + 1; ?>" name="productID[]">
													<option>Select Product & Service</option>

													<?php foreach ($plans as $plan) { ?>
														<option value="<?php echo $plan['productID']; ?>" <?php if ($plan['productID'] == $item['itemListID']) {
																												echo "selected";
																											} ?>>
															<?php echo $plan['Name']; ?> </option> <?php }  ?>
												</select></div>
										</div>
										<div class="col-sm-2 nopadding no-pad-left">
											<div class="form-group ">
												<select class="form-control" id="onetime_charge" name="onetime_charge[]">
													<option>Select Product & Service</option>
													<option value="0" <?php if ($item['oneTimeCharge'] == '0') {
																			echo "selected";
																		} ?>>Recurring</option>
													<option value="1" <?php if ($item['oneTimeCharge'] == '1') {
																			echo "selected";
																		} ?>>One Time Charge</option>
												</select></div>
										</div>
										<div class="col-sm-2 nopadding">
											<div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k + 1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" placeholder="" data-args="Description "></div>
										</div>

										<div class="col-sm-2 nopadding">
											<div class="form-group">
												<input type="text" class="form-control float" id="unit_rate<?php echo $k + 1; ?>" name="unit_rate[]" value="<?php echo ($item['itemRate']); ?>" onblur="set_unit_val('<?php echo $k + 1; ?>');" placeholder="" data-args="Price"></div>
										</div>

										<div class="col-sm-1 nopadding">
											<div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val('<?php echo $k + 1; ?>');" id="quantity<?php echo $k + 1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>" placeholder="" data-args="Qty"></div>
										</div>

										
										<div class="col-sm-2 nopadding">
											<div class="form-group">
												<div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k + 1; ?>" name="total[]" value="<?php echo sprintf('%0.2f', $total_amt); ?>" placeholder="" data-args="Total">
													<div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k + 1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div>
												</div>
											</div>
										</div>
										<div class="clear"></div>
									</div>

								<?php		 }
							} else {
								?>
								
							<?php } ?>

						</div>


						
							<div class="form-group">

								<div class=" form-actions">
									<label class="control-label "></label>
									<div class="group-btn">

										<button class="btn btn-sm btn-success" type="button" onclick="item_fields();"> Add More </button>

										<label class="btn btn-sm pull-right remove-hover"><strong>Total: $<span id="grand_total"> <?php echo '0.00'; ?></span> </strong> </label>
									</div>
								</div>
							</div>

						
					</fieldset>
				</div>
				<legend class="leg"> Checkout Page</legend>
				<div class="block">
					<fieldset>
							<div class="form-group col-sm-4">
								<div class="">
									<label class=" control-label">Require Billing Address</label>
								</div>
								<div class="">
									<label class="radio-inline control-label">
										<input type="radio" name="require_billing" <?php if ($isBilling == '1') {echo "checked";} ?> value="1">Yes</label><br>
									<label class="radio-inline control-label">
										<input type="radio" name="require_billing" <?php if ($isBilling == '0') echo "checked"; ?> value="0">No</label>
								</div>
							</div>
							<div class="form-group col-sm-4">
								<div class="">
									<label  class=" control-label">Require Shipping Address</label>
								</div>
								<div class="">
									<label class="radio-inline control-label">
										<input type="radio" name="require_shipping" <?php if ($isShipping == '1') {
																						echo "checked";
																					} ?> value="1">Yes</label><br>
									<label class="radio-inline control-label">
										<input type="radio" name="require_shipping" <?php if ($isShipping == '0') echo "checked"; ?> value="0">No</label>
								</div>
							</div>
							<div class="form-group col-sm-4">
								<div class="">
									<label class=" control-label">Must Accept Terms of Service</label>
								</div>
								<div class="">
									<label class="radio-inline control-label">
										<input type="radio" name="require_service" <?php if ($isService == '1') {
																						echo "checked";
																					} ?> value="1">Yes</label><br>
									<label class="radio-inline control-label">
										<input type="radio" name="require_service" <?php if ($isService == '0') echo "checked"; ?> value="0">No</label>
								</div>
							</div><br>
						<div class="col-md-12">
						<div class="row">
							<div class="form-group">
								<label class="" stye="text-align:left" for="plan_link">Plan Link</label>

								<div class="" id="m_link_id"> <?php if (isset($subs) && !empty($subs['merchantPlanURL'])) { ?>


										<div class="input-group">
											<a href="<?php echo $subs['merchantPlanURL'];
											 ?>" id='pl_href_link'>
												<span class="input-group-addon plan_cust_url"><?php echo $ur_data['customerPortalURL'] . 'fb_check_out/' . $base_id . '/'; ?></span>
											</a>
											<div class="abctest">
												<input type="text" id="portal_urlll" name="plan_url" size="30" <?php if (isset($subs) && !empty($subs['merchantPlanURL'])) {
																													echo ' value ="' . $subs['postPlanURL'] . '" ' . ' readonly ';
																												} ?> class="form-control valid">
												<?php if (isset($subs)) { ?> <input type='button' id="btn_mask_link" class="btn btn-sm btn-default" value="Edit Link" />
													<input type="button" class="btn btn-sm btn-info" onclick="myFunction()" value="Copy" /><?php }  ?>

											</div>
										</div>
									<?php } else {  ?>
										<?php
																			if ($this->session->userdata('logged_in')) {
																				$data['login_info'] = $this->session->userdata('logged_in');
																				$base_id = base64_encode($data['login_info']['merchID']);
																			}
										?>
										<div class="input-group">
											<span class="input-group-addon plan_cust_url"><?php echo $ur_data['customerPortalURL'] . 'fb_check_out/' . $base_id . '/';   ?></span>
											<div class="abctests">
												<input type="text" id="newplan" name="short_url" class="form-control valid">
											</div>
										</div>

									<?php  }


									?>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
						<div class="row">
							<div class="form-group">
								<label class="" stye="text-align:left" for="plan_link">Confirmation Page</label>


								

								<div class="" id="m_link_id">
									<input type="text" id="confirm_page_url" name="confirm_page_url" value="<?php if (isset($subs) && !empty($subs)) {
																												echo $subs['confirm_page_url'];
																											} ?>" class="form-control" placeholder="" data-args="Confirmation Page">

								</div>

																														</div>
							</div>
						</div>






						<input type="hidden" name="planID" id="planID" value="<?php if (isset($subs)) {
																		echo $subs['planID'];
																	} ?>" />


						<div class="form-group pull-right">
							<div class="col-md-12">

								
								<button type="submit" class="submit btn btn-sm btn-success">Save</button>
								<a href="<?php echo base_url(); ?>FreshBooks_controllers/SettingPlan/plans" class=" btn btn-sm btn-primary1">Cancel</a>
							</div>
						</div>
				


			</fieldset>
			</div>
			</form>
		</div>
	</div>
</div>
<div id="itemJSON" class="hidden" style="display:none"><?php echo $jsPlans ?></div>

	<script>
		$(document).ready(function() {

			var maxdays = daysInThisMonth();
			$('#prorate_bill_check').click(function() {

				if ($(this).is(':checked'))
					$('#pro_div').show();
				else
					$('#pro_div').hide();

			});

			$('#btn_mask_link').click(function() {
				$('#portal_urlll').removeAttr('readonly');
				$("#portal_urlll").css({
					"background-color": "#f2f2f2",
					"border": "1px solid #ddd",
					"border-radius": "0px"
				});
			});
			$('#taxes').change(function() {
				if ($(this).val() != "") {
					$('.set_taxes').show();
					$('.show_check').show();
				} else {
					$('#tax_check').val('');
					$('.set_taxes').hide();
				}
			});

			$('#form-validation').validate({
				errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
				errorElement: 'div',
				errorPlacement: function(error, e) {
					e.parents('.form-group > div').append(error);
				},
				highlight: function(e) {
					$(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
					$(e).closest('.help-block').remove();
				},
				success: function(e) {
					// You can use the following if you would like to highlight with green color the input after successful validation!
					e.closest('.form-group').removeClass('has-success has-error');
					e.closest('.help-block').remove();
				},
				rules: {
					plan_name: {
						required: true,
						minlength: 3,
						maxlength: 25,
						validate_char: true,
					},
					sub_name: {
						required: true,
						minlength: 3,
					},
					short_url: {
						validPlanurl: true,
						remote: {
							url: base_url+'user/Utils/is_plan_exists',
							type: 'POST',
							data:{
								plan_url: function() {
									return $('#newplan').val();
								},
								planID: function() {
									return $('#planID').val();
								},
							},
							dataFilter: function(response) {
								response = jQuery.parseJSON(response)
								return !response.success;
							}
						}
					},
					plan_url: {
						validPlanurl: true,
						remote: {
							url: base_url+'user/Utils/is_plan_exists',
							type: 'POST',
							data:{
								plan_url: function() {
									return $('#portal_urlll').val();
								},
								planID: function() {
									return $('#planID').val();
								},
							},
							dataFilter: function(response) {
								response = jQuery.parseJSON(response)
								return !response.success;
							}
						}
					},
					sub_start_date: {
						required: true,
					},
					autopay: {
						required: true,
					},
					gateway_list: {
						required: true,
					},
					card_list: {
						required: true,
					},
					paycycle: {
						required: true,
					},
					duration_list: {
						required: true,
						number: true,
						maxlength: 2,

					},
					invoice_date: {
						required: true,

					},
					customerID: {
						required: true,

					},
					subsamount: {
						required: true,
						number: true,

					},
					friendlyname: {
						required: true,
						minlength: 3,
					},
					card_number: {
						required: true,
						minlength: 13,
                        maxlength: 16,
						number: true
					},
					expiry_year: {
						CCExp: {
							month: '#expiry',
							year: '#expiry_year'
						}
					},

					cvv: {
						number: true,
						minlength: 3,
						maxlength: 4,
					},

					address1: {
						required: true,
					},
					address2: {
						required: true,
					},
					country: {
						required: true,
					},
					state: {
						required: true,
					},
					city: {
						required: true,
					},
					zipcode: {
						minlength: 3,
						maxlength: 10,
						ZIPCode: true,

					},
					'description[]': {
						minlength: 1,
						maxlength: 31
					},
					'productID[]': {
						minlength: 1,
						required: true
					},
					confirm_page_url: {
						confirm_url: true
					},
					freetrial: {
						required: true,
						digits: true,
						check_free: {
							sub_start_date: '#sub_start_date',
							paycycle: '#paycycle',
							duration_list: '#duration_list',
						}
					},
					pro_billing_date: {
						required: true,
						min: 1,
						max: maxdays,
						digits: true,
					},
					pro_next_billing_date: {
						required: true,
						min: 1,
						max: maxdays,
						digits: true,
					},


					require_service: {
						remote: {
							url: base_url + 'ajaxRequest/chk_portal_url',
							type: "POST",
							cache: false,
							dataType: "json",
							data: {
								inp: function() {
									return $('input[name=require_service]:checked').val();
								},
							},
							dataFilter: function(response) {
							
								var rsdata = jQuery.parseJSON(response)

								if (rsdata.status == 'success')
									return true;
								else
									return false;
							}
						},

					}

				},
				messages: {
					require_service: {
						remote: 'Error: No link detected. Add a Terms of Service URL on the Customer Portal page. Configuration -> Customer Portal -> Terms of Service URL',
					},
					short_url: {
						remote: 'This link currently exists, please change the Plan Link',
					},
					plan_url: {
						remote: 'This link currently exists, please change the Plan Link',
					},
				}

			});

			$.validator.addMethod("validate_char", function(value, element) {

				return this.optional(element) || /^[a-zA-Z0-9-_ ][a-zA-Z0-9-_ ]+$/i.test(value);

			}, "Please enter only letters, numbers, space, hyphen or underscore.");


			$.validator.addMethod('CCExp', function(value, element, params) {
				var minMonth = new Date().getMonth() + 1;
				var minYear = new Date().getFullYear();
				var month = parseInt($(params.month).val(), 10);
				var year = parseInt($(params.year).val(), 10);



				return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
			}, 'Your Credit Card Expiration date is invalid.');


			$.validator.addMethod("phoneUS", function(value, element) {

				if (value == '')
					return true;
				return value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);

			}, "Please specify a valid phone number");

			$.validator.addMethod("ZIPCode", function(value, element) {

				return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
			}, "Only alphanumeric and hyphen is allowed");


			$.validator.addMethod('check_free', function(value, element, params) {

				var duration = $(params.duration_list).val();

				
				if (duration == '0') return true;

				var frequency = $(params.paycycle).val();
				var stdate = new Date(Date.parse($(params.sub_start_date).val()));

				var free = value;
				duration = parseInt(duration, 10);
				free = parseInt(free, 10);
				

				return (duration > free);
			}, 'Value should be less than Recurrence value');

			$.validator.addMethod("validPlanurl", function(value, element) {
				return /^[a-zA-Z0-9_]+$/.test(value);
			}, "Please enter a valid plan url");
			$.validator.addMethod("confirm_url", function(value, element) {
				if (value == '') return true;
				return /^(ftp|http|https):\/\/[^ "]+$/.test(value);
			}, "Please enter a valid url like as: https://merchantsite.com");

		});

		$('.tax_div').change(function() {
			var tax_id = $(this).val();
			var res = tax_id.split(",");
			var rateid = res[0];
			$.ajax({
				url: '<?php echo base_url("FreshBooks_controllers/SettingSubscription/get_tax_id") ?>',
				type: 'POST',
				data: {
					tax_id: rateid
				},
				dataType: 'json',
				success: function(data) {

					$('.tax_checked').val(data.taxRate);


				}
			});

			var nowDate = new Date();
			var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() + 1, 0, 0, 0, 0);
			$("#invoice_date").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			});

			$('#sub_start_date').datepicker({
				format: 'yyyy-mm-dd',
				startDate: today,
				autoclose: true
			});
			$('#subsamount').blur(function() {

				var dur = $('#duration_list').val();
				var subsamount = $('#subsamount').val();
				var tot_amount = subsamount * dur;
				$('#total_amount').val(tot_amount.toFixed(2));
				$('#total_invoice').val(dur);

			});



			$('#card_list').change(function() {
				if ($(this).val() == 'new1') {
					$('#set_credit').show();
				} else {
					$('#card_number').val('');
					$('#set_credit').hide();
				}
			});

			$('#customerID').change(function() {

				var cid = $(this).val();

				if (cid != "") {
					$('#card_list').find('option').not(':first').remove();
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>FreshBooks_controllers/Transactions/check_vault",
						data: {
							'customerID': cid
						},
						success: function(response) {

							data = $.parseJSON(response);

							if (data['status'] == 'success') {

								var s = $('#card_list');
								var card1 = data['card'];
								$(s).append('<option value="new1">New Card</option>');
								for (var val in card1) {

									$("<option />", {
										value: card1[val]['CardID'],
										text: card1[val]['customerCardfriendlyName']
									}).appendTo(s);
								}

								$('#address1').val(data['ShipAddress_Addr1']);
								$('#address2').val(data['ShipAddress_Addr2']);
								$('#city').val(data['ShipAddress_City']);
								$('#state').val(data['	ShipAddress_State']);
								$('#zipcode').val(data['ShipAddress_PostalCode']);
								$('#phone').val(data['Phone']);


							}

						}


					});

				}
			});



		});

		var jsdata = '<?php if (isset($items)) {
							echo json_encode($items);
						} ?>';

		if (jQuery.isEmptyObject(jsdata)) {

			var room = 1;


		} else {

			var grand_total1 = 0;
			var room = '<?php if (isset($k)) {
							echo $k + 1;
						} else {
							echo "1";
						} ?>';
			$(".total_val").each(function() {

				grand_total1 += parseFloat($(this).val());
			});

			$('#grand_total').html(format22(grand_total1));

		}

		function item_fields(first=false) {
			room++;
			var jsplandata = $('#itemJSON').html();

			var plan_data = $.parseJSON(jsplandata);
			var plan_html = '<option val="">Select Product and Service</option>';
			for (var val in plan_data) {
				plan_html += '<option value="' + plan_data[val]['productID'] + '">' + plan_data[val]['Name'] + '</option>';
			}

			var deleteButton = '<div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <span class="fa fa-times" aria-hidden="true"></span></button></div> ';
			if(first) {
				deleteButton = '';
			}

			var onetime_html = '<option value="0">Recurring</option><option value="1">One Time Charge</option>';
			var objTo = document.getElementById('item_fields')
			var divtest = document.createElement("div");
			divtest.setAttribute("class", "form-group removeclass" + room);
			var rdiv = 'removeclass' + room;
			var show_tax = '';

			if ($('#taxes').val() == '') {
				var show_tax = 'style="display:none;"';
				var tax_val = 0;
			} else {
				var tax_val = $('.tax_checked').val();
			}

			divtest.innerHTML = '<div class="row"><div class="col-sm-3 nopadding"><div class="form-group no-pad-left"><select class="form-control select-chosen"  onchange="select_plan_val(' + room + ');"  id="productID' + room + '" name="productID[]">' + plan_html + '</select></div></div><div class="col-sm-2 nopadding"><div class="form-group no-pad-left"><select class="form-control"   id="onetime_charge' + room + '" name="onetime_charge[]">' + onetime_html + '</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description' + room + '" name="description[]" value="" placeholder="" data-args="Description "></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate' + room + '" name="unit_rate[]" value="" onblur="set_unit_val(' + room + ');" placeholder="" data-args="Price"></div></div>	<div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val(' + room + ');" id="quantity' + room + '" name="quantity[]" value="" placeholder="" data-args="Qty"></div></div>		<div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total' + room + '" name="total[]" value="" placeholder="" data-args="Total"> '+ deleteButton +' </div></div> </div></div><div class="clear"></div>';

			objTo.appendChild(divtest)
			$(".select-chosen").chosen();
		}
		
		var item = $('#item-exist').val();
		if(item == 0){
			item_fields(true);
		}


		function remove_education_fields(rid) {

			var rid_val = $('#total' + rid).val();

			var gr_val = 0;
			$(".total_val").each(function() {

				gr_val += parseFloat($(this).val());
			});


			if (rid_val) {
				var dif = parseFloat(gr_val) - parseFloat(rid_val);

				$('#grand_total').html(format22(dif));
			}
			$('.removeclass' + rid).remove();




		}



		var nmiValidation = function() {

			return {
				init: function() {
					/*
					 *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
					 */

					/* Initialize Form Validation */

					$.validator.addMethod('CCExp', function(value, element, params) {
						var minMonth = new Date().getMonth() + 1;
						var minYear = new Date().getFullYear();
						var month = parseInt($(params.month).val(), 10);
						var year = parseInt($(params.year).val(), 10);



						return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
					}, 'Your Credit Card Expiration date is invalid.');


					$.validator.addMethod('check_free', function(value, element, params) {

						var duration = $(params.duration_list).val();

					
						if (duration == '0') return true;

						var frequency = $(params.paycycle).val();
						var stdate = new Date(Date.parse($(params.sub_start_date).val()));

						var free = value;
						duration = parseInt(duration, 10);
						free = parseInt(free, 10);
					
						return (duration > free);
					}, 'Value should be less than Recurrence value');

					$.validator.addMethod("validPlanurl", function(value, element) {
						return /^[a-zA-Z0-9_]+$/.test(value);
					}, "Please enter a valid plan url");


				}
			};
		}();

		/**********Check the Validation for free trial**********************/
		function weeksBetween(d1, d2) {

			return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));

		}

		function daysbeween(d1, d2) {

			return Math.round((d2 - d1) / (24 * 60 * 60 * 1000));

		}

		function get_months(d1, d2) {


			return difference = (d2.getFullYear() * 12 + d2.getMonth()) - (d1.getFullYear() * 12 + d1.getMonth());

		}

		function get_frequncy_val(du, new1date, fr) {
			var res = '';

			var CurrentDate = new Date(new1date.getFullYear(), new1date.getMonth(), new1date.getDate(), 0, 0, 0, 0);
			CurrentDate.setMonth(CurrentDate.getMonth() + parseInt(du));
			var newdate = new Date(CurrentDate);

			if (fr == 'dly') {
				res = daysbeween(new Date(new1date), new Date(newdate));
			} else if (fr == '1wk') {
				res = weeksBetween(new Date(new1date), new Date(newdate));
			} else if (fr == '2wk') {
				res = weeksBetween(new Date(new1date), new Date(newdate)) / 2;
			} else if (fr == 'mon') {
				res = get_months(new Date(new1date), new Date(newdate));
			} else if (fr == '2mn') {
				res = get_months(new Date(new1date), new Date(newdate)) / 2;
			} else if (fr == 'qtr') {
				res = get_months(new Date(new1date), new Date(newdate)) / 3;
			} else if (fr == 'six') {
				res = get_months(new Date(new1date), new Date(newdate)) / 6;
			} else if (fr == 'yr1') {
				res = get_months(new Date(new1date), new Date(newdate)) / 12;
			} else if (fr == 'yr2') {
				res = get_months(new Date(new1date), new Date(newdate)) / 24;
			} else if (fr == 'yr3') {
				res = get_months(new Date(new1date), new Date(newdate)) / 36;
			}
			return res;

		}

		/************End*******************/



		function chk_payment(r_val) {



			if (r_val == '1') {
				$('#set_pay_data').show();
				$("input[name='rebilling'][value='1']").prop('checked', true);
			} else {
				$('#set_pay_data').hide();
				$("input[name='rebilling'][value='0']").prop('checked', true);
			}

		}


		function select_plan_val(rid) {



			var itemID = $('#productID' + rid).val();

			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>FreshBooks_controllers/Freshbooks_invoice/get_item_data",
				data: {
					'itemID': itemID
				},
				success: function(data) {

					var item_data = $.parseJSON(data);
					$('#description' + rid).val(item_data['SalesDescription']);
					$('#unit_rate' + rid).val(item_data['saleCost'], 2);
					$('#quantity' + rid).val(1);

				


					$('#total' + rid).val(roundN(($('#quantity' + rid).val() * $('#unit_rate' + rid).val()), 2));

					var grand_total = 0;
					$(".total_val").each(function() {
						var tval = $(this).val() != '' ? $(this).val() : 0;
						grand_total = parseFloat(grand_total) + parseFloat(tval);
					});
					$('#grand_total').html(format22(grand_total));


				}

			});

		}

		function set_unit_val(rid) {
			var qty = $('#quantity' + rid).val();
			var rate = $('#unit_rate' + rid).val();
			var tax = 0;

			if ($('input#tax_check' + rid).is(':checked')) {
				tax = $('#tax_check' + rid).val();
			}
			var total_tax = (qty * rate) * tax / 100;
			var total = qty * rate + total_tax;
			$('#total' + rid).val(total.toFixed(2));

			var grand_total = 0;
			$(".total_val").each(function() {

				var tval = $(this).val() != '' ? $(this).val() : 0;
				grand_total = parseFloat(grand_total) + parseFloat(tval);
			});

			$('#grand_total').html(format22(grand_total));
		}

		function set_qty_val(rid) {

			var qty = $('#quantity' + rid).val();
			var rate = $('#unit_rate' + rid).val();
			var tax = 0;

			if ($('input#tax_check' + rid).is(':checked')) {
				tax = $('#tax_check' + rid).val();
			}

			var total_tax = (qty * rate) * tax / 100;
			var total = qty * rate + total_tax;
			$('#total' + rid).val(total.toFixed(2));
			
			var grand_total = 0;
			$(".total_val").each(function() {
				var tval = $(this).val() != '' ? $(this).val() : 0;
				grand_total = parseFloat(grand_total) + parseFloat(tval);
			});

			$('#grand_total').html(format22(grand_total));

		}

		function set_tax_val(mythis, rid) {

			var qty = $('#quantity' + rid).val();
			var rate = $('#unit_rate' + rid).val();
			var tax = $('#tax_check' + rid).val();

			if (mythis.checked) {
				var total_tax = (qty * rate) * tax / 100;
				var total = qty * rate + total_tax;
				$('#total' + rid).val(total.toFixed(2));
			} else {
				var total = qty * rate;
				$('#total' + rid).val(total.toFixed(2));
			}

			var grand_total = 0;
			$(".total_val").each(function() {
				var tval = $(this).val() != '' ? $(this).val() : 0;
				grand_total = parseFloat(grand_total) + parseFloat(tval);
			});

			$('#grand_total').html(format22(grand_total));
		}




		$(document).ready(function() {

			$('.force-numeric').keydown(function(e) {

				var key = e.charCode || e.keyCode || 0;
				// allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
				// home, end, period, and numpad decimal
				return (
					key == 8 ||
					key == 9 ||
					key == 13 ||
					key == 46 ||
					key == 110 ||
					key == 190 ||
					(key >= 35 && key <= 40) ||
					(key >= 48 && key <= 57) ||
					(key >= 96 && key <= 105));
			});


			$('input.float').bind('keypress', function() {
				this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
			});


		});



		function isNumberKey(evt) {
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;

			return true;
		}


		function IntegerAndDecimal(e, obj, isDecimal) {
			if ([e.keyCode || e.which] == 8) //this is to allow backspace
				return true;

			if ([e.keyCode || e.which] == 46) //this is to allow decimal point
			{
				if (isDecimal == 'true') {
					var val = obj.value;
					if (val.indexOf(".") > -1) {
						e.returnValue = false;
						return false;
					}
					return true;
				} else {
					e.returnValue = false;
					return false;
				}
			}

			if ([e.keyCode || e.which] < 48 || [e.keyCode || e.which] > 57)
				e.preventDefault ? e.preventDefault() : e.returnValue = false;
		}



		function isNumberKeys(evt) {
			var charCode = (evt.which) ? evt.which : event.keyCode

			if (charCode == 46) {
				var inputValue = $("#inputfield").val()
				if (inputValue.indexOf('.') < 1) {
					return true;
				}
				return false;
			}
			if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
			return true;
		}

		function check_subs_link(eve) {
			var url = $(eve).val();
			url = url.replace(/[&\/\\#,+()$~%.'":;*?<>{}]/g, '');
			url = url.replace(" ", "_");
			$('#plan_url').val(url);
			url = '<?php echo base_url(); ?>' + 'FreshBooks_controllers/PlanCheck/' + url;
			$('#m_link').html('<a href=' + url + '>' + url + '</a>');
		}




		function myFunction() {

			var form = $("#form-validation");
			// Getting token from the response json.
			var pl_url = $('#pl_href_link').attr("href");
			$('<input>', {
				'type': 'text',
				'id': 'myInput',
				'value': pl_url,
			}).appendTo(form);

			if ($('#form-validation').find('#myInput')) {
				var copyText = $('#form-validation').find('#myInput');

				copyText.select();
				document.execCommand("Copy");
				$('#form-validation #myInput').remove();
			}
		}

		function roundN(num, n) {
			return parseFloat(Math.round(num * Math.pow(10, n)) / Math.pow(10, n)).toFixed(n);
		}

		function format22(num) {

			var p = parseFloat(num).toFixed(2).split(".");
			return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
				return num == "-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
			}, "") + "." + p[1];

		}
		$('#paycycle').change(function() {
			var pay_type = $(this).val();
			if (pay_type == 'mon') {
				$('#MhiddenDiv').show();
			} else {
				$('#MhiddenDiv').hide();
			}
		});


		function daysInThisMonth() {
			var now = new Date();
			return new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
		}


		$("#plan_name").blur(function() {
			var str = $(this).val();
			str = str.trim();
			str = str.replace(/\s+/g, '_');
			$('#newplan').val(str);

		});


		
		$('#gateway_list').change(function() {
			var gatewayID = $(this).val();
			$.ajax({
				url: '<?php echo base_url() ?>FreshBooks_controllers/home/get_gateway_data',
				type: 'POST',
				data: {
					gatewayID: gatewayID
				},
				dataType: 'json',
				success: function(data) {
					var checkconditionIsEnableCount = 0;
					if(data.creditCard == 1){
						$('#cc').prop('checked',true);

						$('.selectPayOptionShow1').show();
						$('.selectPayOptionHide1').show();

						checkconditionIsEnableCount = checkconditionIsEnableCount + 1;
					}else{
						$('.selectPayOptionShow1').hide();
						$('.selectPayOptionHide1').hide();
					}
					if(data.echeckStatus == 1){
						$('#ec').prop('checked',true);
						checkconditionIsEnableCount = checkconditionIsEnableCount + 1;
						$('.selectPayOptionShow2').show();
						$('.selectPayOptionHide2').show();
					}else{
						$('.selectPayOptionShow2').hide();
						$('.selectPayOptionHide2').hide();
					}

					$('#checkconditionIsEnableCount').val(checkconditionIsEnableCount);

				}
			});
		});
		$('#cc').click(function() {

			if ($(this).is(':checked')){

				$(this).val(1);
			}else{
				if($('#checkconditionIsEnableCount').val() == 1){
					$(this).prop('checked',true);
					$(this).val(1);
				}else{
					if ($('#ec').is(':checked')){
						$(this).val(0);
					}else{
						$(this).prop('checked',true);
						$(this).val(1);
					}
				}
				
				
			}

		});
		$('#ec').click(function() {

			if ($(this).is(':checked')){

				$(this).val(1);
			}else{
				if($('#checkconditionIsEnableCount').val() == 1){
					$(this).prop('checked',true);
					$(this).val(1);
				}else{
					if ($('#cc').is(':checked')){
						$(this).val(0);
					}else{
						$(this).prop('checked',true);
						$(this).val(1);
					}
				}
				
			}

		});
	</script>