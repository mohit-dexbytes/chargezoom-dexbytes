<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data">
		<?php echo $this->session->flashdata('message');   ?>
	</div>
	        
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
        <div class="col-md-12">
		
		
		  
			<legend class="leg" ><?php if (isset($subs)) {
									echo "Edit";
								} else {
									echo "Create";
								} ?> Invoice</legend>    
		
		   <?php //echo "<pre>"; print_r($subs); ?>
		    <form id="form-validation" action="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_invoice/invoice_create" method="post" class="form-horizontal form-bordered">
                
				
				<div class="block">
                 <fieldset>  
					
				<div class="col-md-12 no-pad">
			        <div class="col-md-4 form-group">
						 <label class="control-label" for="customerID">Customers</label>
						  <div>
                            <select id="customerID" name="customerID" class="form-control select-chosen">
                                <option value>Select Customer</option>
						      <?php   foreach($customers as $customer){       ?>
						        <option value="<?php echo $customer['Customer_ListID']; ?>" <?php if(isset($Invcustomer) &&  $Invcustomer['Customer_ListID']==$customer['Customer_ListID']){  echo "selected" ;} ?>  ><?php echo  $customer['fullName'] ; ?></option>
						        <?php } ?>
                            </select>
						 </div>	
                   </div>    
                    
                 
                   <div class="col-md-4  form-group">   
                        <label class=" control-label" for="firstName">Invoice Date</label>
                           <div>
                            <div class="input-group input-date">
                                <input type="text" id="invdate" name="invdate" class="form-control input-datepicker"  value="<?php echo date('m/d/Y'); ?>" data-date-format="mm/dd/yyyy">
                                <span class="input-group-addon due_date"><i class="fa fa-calendar"></i></span>
                            </div>
                       </div>
                    
                    </div>
					
					<div class="col-md-4 form-group">   
						<label class="control-label" for="pay_terms">Payment Term</label>
						<div>  
							<select name="pay_terms" id="pay_terms" class="form-control ">
							 	<option value="" >Select Term</option>

								<?php  foreach($netterms as $terms){
									if ($terms['pt_netTerm'] != "") {
										$termSet = $terms['pt_netTerm'];
									} else {
										$termSet = $terms['netTerm'];
									}
									if($terms['id'] == $selectedID){
										$selected = 'Selected';
									}else{
										$selected = '';
									} 
									if($terms['enable'] != 1){
										?>
										
										<option  <?php echo $selected; ?> value="<?php echo $termSet; ?>"><?php if($terms['pt_name'] != ""){ echo $terms['pt_name']; }else{ echo $terms['name']; } ?></option>
									<?php } 
								}?>
							</select>
						</div>
					</div>
				</div>
			</fieldset>
			</div>

            <legend class="leg"> Products & Services</legend>
			<div class="block">			
				<fieldset>	
						<div class="row">
						<div class="col-sm-3">
						    <div class="form-group"><label class="control-label">Products & Services</label> </div>
				        	</div> 
						<div class="col-sm-3">  <div class="form-group"><label class="control-label">Description </label></div> </div>
						<div class="col-sm-2"><div class="form-group"><label class="control-label">Price </label> </div></div>
						<div class="col-sm-1"><div class="form-group text-center"><label class="control-label">Quantity </label></div> </div>
						<div class="col-sm-1"><div class="set_taxes"><div class="form-group"><label class="control-label">Tax </label></div></div> </div> 
						<div class="col-sm-2 row"><div class="form-group"><label class="control-label">Total</label></div> </div>
						
						</div>
						
												
							  <div id="item_fields_inv" class="row">
							 <?php 
							       if(isset($items) && !empty($items)){
								     foreach($items as $k=>$item ){
										$rate = $item['itemRate'];
										$qnty = $item['itemQuantity'];
										if(!empty($item['itemTax'])) {
											$tax  = $item['itemTax'];
											echo "<script> $('.set_taxes').css('display','block'); </script>";
										}else{	$tax = 0; }
										
										$total_amt = ( $rate * $qnty ) + ( ( $rate * $qnty ) * $tax / 100 );
									 
									 ?>
									 
								   <div class="form-group removeclass<?php echo $k+1; ?>">
									<div class="col-sm-3 nopadding"><div class="form-group no-pad-left">
									  <select class="form-control select-chosen"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="productID<?php echo $k+1; ?>" name="productID[]">
									<option value="">Select Product & Service</option>		
									 <?php foreach($plans as $plan){ ?>
									 <option value="<?php echo $plan['productID']; ?>"  > 
									 <?php echo $plan['Name']; ?> </option> <?php }  ?>
									   </select></div></div>
									   <input type="hidden" id="productName<?php echo $k+1; ?>" name="plan_name[]" value="">
									   <div class="col-sm-3 nopadding"><div class="form-group no-pad-left"> <input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>"></div></div>
									   
									   <div class="col-sm-2 nopadding"><div class="form-group">
									   <input type="text" class="form-control" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo $item['itemRate']; ?>" onblur="set_unit_val('<?php echo $k+1; ?>');"></div></div>
									   
									   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control text-center" maxlength="4" onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity<?php echo $k+1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>"></div></div>
									   
									    <div class="col-sm-1 nopadding"><div class="set_taxes"><div class="form-group"> <input type="checkbox" id="tax_check<?php echo $k+1; ?>" <?php if($item['itemTax']) echo "checked"; ?> name="tax_check[]" class="tax_checked" onchange="set_tax_val(this, '<?php echo $k+1; ?>')" value="<?php echo $item['itemTax']; ?>"></div></div></div>
									   
									   <div class="col-sm-2 nopadding row"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k+1; ?>" name="total[]" value="<?php echo ($item['itemQuantity']*$item['itemRate']); ?>"> 
									   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>
	                                       </div>
							<?php		 }								 
								   } ?>
									  
							 </div>
							
					 
					    <div class="col-md-12 no-pad">
						   <div class="form-group">
							
							<div class=" form-actions">		
								<label class="control-label "></label>						  
							  <div class="group-btn">
								 <button class="btn btn-sm btn-success" type="button"  onclick="item_invoice_fields();"> Add More </button>
							 
								 <label class="btn btn-sm pull-right remove-hover"><strong>Total: $<span id="grand_total"><?php echo '0.00'; ?></span></strong>  </label>
							  </div>
					        </div>
						</div>
							
						</div>	
			
			


				</fieldset>
			</div>

           	<legend class="leg">Billing Address</legend>
            <div class="block">        
            	<fieldset>
			     	<div id="set_bill_data" style="">
							
							<div class="col-md-12 no-pad">
									 <div class="col-sm-12 form-group">										
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 1</label>
                                              
                                                        <input type="text" id="baddress1" name="baddress1" class="form-control " <?php  if(isset($Invcustomer)){ echo 'value="'.$Invcustomer['address1'].'" '; } ?>  >
                                                  
                                          </div>
										</div>	
										 <div class="col-sm-12 form-group">					
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 2</label>
                                              
                                                        <input type="text" id="baddress2" name="baddress2" class="form-control " <?php  if(isset($Invcustomer)){ echo 'value="'.$Invcustomer['address2'].'" '; } ?>  >
                                                  
                                             </div>
										   </div>
								
										<div class="form-group col-sm-2">
										 <div class="">
                                         
                                                <label class=" control-label" for="val_username">City</label>
                                              
                                                        <input type="text" id="bcity" name="bcity" class="form-control input-typeahead" <?php  if(isset($Invcustomer)){ echo 'value="'.$Invcustomer['City'].'" '; } ?> autocomplete="off" >
                                                    
                                           
										</div>
										</div>
										<div class="form-group col-sm-3">	
										<div class=" ">
											
                                                <label class=" control-label" for="val_username">State/Province</label>
                                               
                                                        <input type="text" id="bstate" name="bstate" class="form-control input-typeahead" <?php  if(isset($Invcustomer)){ echo 'value="'.$Invcustomer['State'].'" '; } ?> autocomplete="off" >
                                                   
                                        
										 </div>
										</div>
									
										<div class="form-group col-sm-2">
										 <div class="">		  
										
                                                <label class=" control-label" for="val_username">ZIP Code</label>
                                              
                                                        <input type="text" id="bzipcode" name="bzipcode" class="form-control" <?php  if(isset($Invcustomer)){ echo 'value="'.$Invcustomer['zipCode'].'" '; } ?> >
                                                     
	
										  </div>
										</div>
										<div class="col-sm-3 form-group">	   
								 <div class="">
										
											
												
												<label class="control-label" for="example-typeahead">Country</label>
											
												<input type="text" id="bcountry" name="bcountry" class="form-control input-typeahead" <?php  if(isset($Invcustomer)){ echo 'value="'.$Invcustomer['Country'].'" '; } ?> autocomplete="off" > 
										</div>	
									</div>
										<div class="form-group col-sm-2">		
										 <div class="">
											  
                                                <label class=" control-label" for="phone">Phone Number</label>
                                               
                                                        <input type="text" id="bphone" name="bphone" class="form-control" <?php  if(isset($Invcustomer)){ echo 'value="'.$Invcustomer['phoneNumber'].'" '; } ?> >
                                                     
                                         
										   </div>
									</div>
								
				           </div>
				           
				
                    </div>
                 </fieldset>
            </div>

            <legend class="leg">Shipping Address</legend>
            <div class="block">        
            	<fieldset>
                      <div id="set_bill_data1">
								<div class="col-sm-12 form-group">					
									     <div class="col-md-12 no-pad">
							    <input type="checkbox" id="chk_add_copy"> Same as Billing Address
							    </div></div>
							    	<div class="col-md-12 no-pad">
									<div class="col-sm-12 form-group">										
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 1</label>
                                              
                                                        <input type="text" id="address1" name="address1" class="form-control " value="<?php if(isset($subs)) echo $subs['address1']; ?>"  >
                                                  
                                          </div>
									</div>	
								    <div class="col-sm-12 form-group">					
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 2</label>
                                              
                                                        <input type="text" id="address2" name="address2" class="form-control "  value="<?php if(isset($subs))echo $subs['address2']; ?>" >
                                                  
                                         </div>
									</div>
								  
								  	<div class="form-group col-sm-2">
										 <div class="">
                                         
                                                <label class=" control-label" for="val_username">City</label>                                              
                                                        <input type="text" id="city" name="city" class="form-control input-typeahead" autocomplete="off" value="<?php  if(isset($subs))echo $subs['city']; ?>">
                                                    
                                           
										</div>
									</div>
								  
									<div class="form-group col-sm-3">	
										<div class=" ">
											
                                                <label class=" control-label" for="val_username">State/Province</label>
                                               
                                                        <input type="text" id="state" name="state" class="form-control input-typeahead"  value="<?php if(isset($subs)) echo $subs['state']; ?>" autocomplete="off" >
                                                   
                                        
										 </div>
									</div>
								
									<div class="form-group col-sm-2">
										 <div class="">		  
										
                                                <label class=" control-label" for="val_username">ZIP Code</label>
                                              
                                                        <input type="text" id="zipcode" name="zipcode" class="form-control" value="<?php if(isset($subs)) echo $subs['zipcode']; ?>" >
                                                     
	
										  </div>
									</div>
									  <div class="col-sm-3 form-group">	   
								       <div class="">
										
											
												
												<label class="control-label" for="example-typeahead">Country</label>
											
												
							<input type="text" id="country" name="country" class="form-control input-typeahead" autocomplete="off" value="<?php if(isset($subs)) echo $subs['country']; ?>">
													
											
										</div>	
									</div>
									<div class="form-group col-sm-2">		
										 <div class="">
											  
                                                <label class=" control-label" for="phone">Phone Number</label>
                                               
                                                        <input type="text" id="phone" name="phone" class="form-control" >
                                                     
                                         
										   </div>
									</div>
								
				           </div>
				           </div>
					<div class="form-group pull-right">
                    
                   
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                    <a href="<?php echo base_url();?>FreshBooks_controllers/Freshbooks_invoice/Invoice_details"class="btn btn-sm btn-primary1">Cancel</a>   
                    
                </div>
                     <div class="col-md-12 no-pad"></div>
				    <div class="form-group pull-right">
					<div class="col-md-12 no-pad">
					    
                  	<input type="checkbox" id="setMail" name="setMail" class="set_checkbox"   /> Send Email
				    </div>
				</div>
				</fieldset>
               
				<input type="hidden" name="subID"   id="subID" value="" /> 
			<div>	
				
			
            </form>
          </div>      
        </div>
    </div>
</div>
<script src="<?php echo base_url(JS); ?>/pages/fb_subscription.js" ></script> 
  