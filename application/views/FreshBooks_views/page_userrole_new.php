<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
     <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
	           ?>

    <!-- All Orders Block -->
    <legend class="leg">User Roles</legend>
    <div class="full">
	        
        <!-- All Orders Title -->
       
        <div style="position: relative">
                              
                    <a href="#add_role" class="btn pull-lft btn-sm btn-success subs-btn list-add-btn addNewFixRight"  onclick="set_addrole();" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Add New</a>
              
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="user_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class=" text-left">Role Name</th>
                    <th class="text-left hidden-xs">Powers</th>
                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($roles_data) && $roles_data)
				{
					foreach($roles_data as $role)
					{
				?>
				<tr>
				
					<td class="text-left "><?php echo $role['roleName']; ?></a></td>
                    <td class="text-left hidden-xs"><?php echo  implode(', ',$role['authName']); ?></td>
				
					
					<td class="text-center">
						<div class="btn-group btn-group-xs">
						
							 <a href="#add_role" onclick="set_edit_role('<?php  echo $role['roleID']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Edit Role" class="btn btn-alt btn-sm btn-default"  > <i class="fa fa-edit"></i></a>
							
                            <a href="#del_role" onclick="del_freshBooks_role_id('<?php  echo $role['roleID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Role" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
                            
						</div>
					</td>
				</tr>
				
				<?php } } 
			
				?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
        </div>
    </div>
    <!-- END All Orders Block -->
</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>


<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#user_page').dataTable({
                columnDefs: [
                  
                    { orderable: false, targets: [2] }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_addrole(id){
	$('#roleName').val(id);
          	
	 $("input[type=checkbox]").each(function() {
				
				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = false;
				
			});
}	
function set_edit_role(role_id){
              	
    	 $("input[type=checkbox]").each(function() {				
				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = false;				
			});
    
    $.ajax({
    url: '<?php echo base_url("FreshBooks_controllers/MerchantUser/get_role_id")?>',
    type: 'POST',
	data:{role_id:role_id},
	dataType: 'json',
    success: function(data){
		
             $('#roleID').val(data.roleID);		
			 $('#roleName').val(data.roleName);

			 var newarray = JSON.parse("[" + data.authID + "]");
			
			 for(var val in  newarray) {
			  
			 $("input[type=checkbox]").each(function() {
				
			if($(this).val()==newarray[val]) {

				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = true;
				}
			});
			 
			 }			 
	}	
});	
}


$(document).ready(function(){


    
    $('#form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'roleName': {
                        required: true,
                        minlength: 3,
                        maxlength: 25
                         
                    },
                   'role[]': {
                        required: true,
                        minlength: 1
                    }
			
		
    },	
    errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
         error.appendTo('.sele');
    } else {
          error.insertAfter(element);
    }
    }
    });	
    

 $.validator.addMethod("validate_char", function(value, element) {

      return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);

  }, "This field contain only letters, numbers, or underscore."); 
  

});

</script>
<!------    Add popup form    ------->


<div id="add_role" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add/Edit Role</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="form" class="form form-horizontal" action="<?php echo base_url(); ?>FreshBooks_controllers/MerchantUser/create_role">
			<input type="hidden" id="roleID" name="roleID" value=""  />
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Role Name</label>
							<div class="col-md-8">
								<input type="text" id="roleName"  name="roleName" class="form-control"  value="" placeholder="" data-args="Role Name"></div>
						</div>
                </div>      
               
		    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Select Powers</label>
							<div class="col-md-8"></div>
							 <br>
							  <div class="sele"></div>
						</div>
               </div>      
               	
						 <div class="col-md-12">
						    
					<?php    foreach ($auths as $key=>$auth ){  ?>     
						   
					  
					   <div class="col-md-6 form-group">
					   <label class="col-md-5 control-label" for="example-username">  </label>
					  
					   <input type="checkbox" name="role[]" id="role<?php echo $key; ?>"  value="<?php echo $auth['authID']; ?>" >  <?php echo $auth['authName'] ; ?>
					  </div>
					  
					  
					  <?php } ?>
                    </div>
                    
                  
	                <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>
					
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					
                    </div>
                    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<!-- END Page Content -->