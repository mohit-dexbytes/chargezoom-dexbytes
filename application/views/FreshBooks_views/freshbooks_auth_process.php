   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>
	<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
	<div class="header-section">
		<h1>
		<i class="gi gi-settings"> </i> <?php echo "FreshBooks Authentication";?> 
		</h1>
	</div>
	</div>
	

    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	            
        <!-- Progress Bar Wizard Content -->
        <div class="row">
	   
		<div> <h3>Click on the button below to start "Authentication Process"</h3></div><br><br>
	
		
		<a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_integration/auth "
       data-toggle="modal" class="btn btn-lg btn-success">Get Authentication</a>
 
			
				<br><br>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
   
</div>
