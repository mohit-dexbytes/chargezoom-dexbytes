<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <span id="flashdata"> 
     
    </span>
    <legend class="leg">Sync Log</legend>
    <!-- All Orders Block -->
    <div class="block-main full" style="position: relative;">
        
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left hidden">Sort Date</th>
                    <th class="text-left hidden-xs">Time</th>
                    <th class="text-left">ID</th>
                    <th class="text-right ">Message</th>
                    <th class="hidden-xs hidden-sm text-right" style="width:225px !important;">Status</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($alls) && $alls)
				{
					foreach($alls as $k=>$all)
					{
                     
				?>
				<tr> 
          <td class="text-right hidden">
              <?php
                  echo $all['createdAt'];
              ?>
          </td>
          <td class="text-left ">
                <?php
                    $date = $all['createdAt'];
                    if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                        $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                        $date = getTimeBySelectedTimezone($timezone);
                    } 
                    echo date('M d, Y h:i A', strtotime($date));
                ?>
          </td>
            <td class="text-left ">
                <?php echo $all['xeroSyncID'];
                ?>
               
            </td>
        
          
           <td class="hidden-sm text-right ">
             <?php 
                if($all['xeroStatus']==0 ){
                    echo $all['xeroText'].' Not Synced';
                  }else{
                    echo $all['xeroText']. ' Successfully';
                  }
             ?>
               
           </td>

          <td class="text-right hidden-xs" style="width:225px !important;"> 
            <?php 
                if($all['xeroStatus']==0 ){
                    echo '<span class="submit btn btn-sm btn-danger">Error</span>';
                  }else{
                    echo '<span class="submit btn btn-sm btn-success">Synced</span>
                    ';
                  }
             ?>
          </td>
          
					
				</tr>
				
				<?php } }else{ echo'<tr>
                <td colspan="5"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
        <!-- </div> -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ // EcomOrders.init(); 
Pagination_view111.init(); });



var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#ecom-orders111').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: true, targets: [-1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script type="text/javascript">
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 3000); //call fade in 2 seconds
 })

function fadeMyDiv() {
   // $("#flashdata").fadeOut('slow');
}


function set_seenk_data(logID='')
{

$("#snk_form").remove();
var f = document.createElement("form");
f.setAttribute('method',"post");

f.setAttribute('id',"snk_form");

f.setAttribute('action',"<?php echo base_url(); ?>QBO_controllers/home/sync_event");
var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"logID");
i.setAttribute('value', logID);


f.appendChild(i);
//f.appendChild(s);

//and some more input elements here
//and dont forget to add a submit button

document.getElementsByTagName('body')[0].appendChild(f);

$('#snk_form').submit();

}
</script>
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>

<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
</div>
<!-- END Page Content -->