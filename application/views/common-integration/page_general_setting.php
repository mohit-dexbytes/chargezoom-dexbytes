	<!-- Page content -->
	<?php
	$this->load->view('alert');
?>
		<div id="page-content">
								   <!-- Forms General Header -->
          <?php /* ?> <ol class="breadcrumb">
                    <li class="breadcrumb-item">
          <strong><a href="<?php echo base_url(); ?>home/index">Dashboard</a></strong>
        </li>
        <li class="breadcrumb-item"><small><?php  echo "General Settings";?></small></li>  
      </ol> <?php */ ?>
	
    <!-- END Wizard Header -->

	
	
	
	
    <!-- Progress Bar Wizard Block -->
    <legend class="leg"><?php  echo "General Settings";?> </legend>
	<div class="block">       
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="form_new" class="form form-horizontal"  enctype="multipart/form-data" action="<?php echo base_url(); ?>Integration/Settings/profile_setting">
		         <input type="hidden" id ="merchID" name="merchID" value="<?php if(isset($invoice)){echo $invoice['merchID']; } ?>"  />
                 
				   <div class="col-md-6"> 
				   
				       <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">First Name</label>
							<div class="col-md-8">
								<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['firstName']; } ?>" >
							</div>
						</div>
						
						 <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Last Name</label>
							<div class="col-md-8">
								<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['lastName']; } ?>" >
							</div>
						</div>
						
						
						 <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Email Address</label>
							<div class="col-md-8">
								<input type="text" readonly="readonly" id="merchantEmail" name="merchantEmail" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantEmail']; } ?>" >
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Company Name</label>
							<div class="col-md-8">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['companyName']; } ?>" >
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Website</label>
							<div class="col-md-8">
								<input type="text" id="weburl" name="weburl"  value="<?php if(isset($invoice) && !empty($invoice)){ echo $invoice['weburl']; } ?>" class="form-control">
							</div>
						</div>
						
						
					<?php /* ?>			<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Tagline</label>
							<div class="col-md-8">
								<input type="text" id="tagline" name="tagline"  value="<?php if(isset($invoice)){echo $invoice['merchantTagline']; } ?>" class="form-control">
							</div>
						</div>
						
					
						<?php /* ?>	<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Full Address</label>
							<div class="col-md-8">
								<input type="text" id="merchantFullAddress" name="merchantFullAddress"  value="<?php if(isset($invoice)){echo $invoice['merchantFullAddress']; } ?>" class="form-control">
							</div>
						</div><?php */ ?>
				   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Invoice Prefix </label>
							<div class="col-md-8">
							<input type="text" id="prefix" name="prefix" class="form-control" 
							value="<?php if(isset($prefix) && !empty($prefix)){ echo $prefix['prefix']; }else{ echo "CZ-";} ?>">
							
							</div>
						   </div>
                       
							<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Invoice Start Number </label>
							<div class="col-md-8">
							<input type="text" id="postfix" name="postfix" class="form-control" 
							value="<?php if(isset($prefix) && !empty($prefix)){ echo $prefix['postfix']; }else{echo "10000";} ?>"></div>
							</div>
                    		<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Timezone</label>
								<?php
									$timezone = '';
									if(isset($invoice['merchant_default_timezone']) && !empty($invoice['merchant_default_timezone']))
									{ 
										$timezone = $invoice['merchant_default_timezone']; 
									}
								?>
								<div class="col-md-8">
									<select class="form-control" name="merchant_default_timezone">
										<!-- <option value="">Select Timezone</option> -->
										<option value="America/Adak" <?php echo ($timezone == "America/Adak") ? "selected" : ''; ?> > HST - Hawaii Standard Time - GMT-10:00</option>
										<option value="America/Anchorage" <?php echo ($timezone == "America/Anchorage") ? "selected" : ''; ?> > AST - Alaska Standard Time - GMT-9:00</option>
										<option value="America/Los_Angeles" <?php echo ($timezone == "America/Los_Angeles") ? "selected" : ''; ?> > PST - Pacific Standard Time - GMT-8:00</option>
										<option value="America/Denver" <?php echo ($timezone == "America/Denver") ? "selected" : ''; ?> > MST - Mountain Standard Time - GMT-7:00</option>
										<option value="America/Chicago" <?php echo ($timezone == "America/Chicago") ? "selected" : ''; ?> > CST - Central Standard Time - GMT-6:00</option>
										<option value="America/New_York" <?php echo ($timezone == "America/New_York") ? "selected" : ''; ?> > EST - Eastern Standard Time - GMT-5:00</option>
									</select>
								</div>
							</div>
						     
                            
                    </div>  
                    
                  
					    
				<div class="col-md-6">  
				
				 	<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 1</label>
							<div class="col-md-8">
								<input type="text" id="merchantAddress1" name="merchantAddress1" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAddress1']; } ?>">
							</div>
						</div>
						
							<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 2</label>
							<div class="col-md-8">
								<input type="text" id="merchantAddress2" name="merchantAddress2" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAddress2']; } ?>">
							</div>
						</div>
				
						
					
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="city">City</label>
						   <div class="col-md-8">
							<input type="text" id="city" class="form-control" name="city" value="<?php if(isset($invoice)){ echo $invoice['merchantCity']; } ?>">
								<?php /* ?><select id="city" class="form-control input-typeahead" name="city">
								<option value="Select City"> City</option>
								   <?php foreach($city_datas as $city){  ?>
								   <option value="<?php echo $city['city_id']; ?>" <?php if(isset($invoice)){if($invoice['merchantCity']==$city['city_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $city['city_name']; ?> </option>
								   <?php } ?> 
								</select> <?php */ ?>
							</div>
                        </div>
                                   
						<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state">State</label>
						   <div class="col-md-8">
					<input type="text" id="state" class="form-control " name="state" value="<?php if(isset($invoice)){ echo $invoice['merchantState'];} ?>">
								<?php /* ?><select id="state" class="form-control input-typeahead" name="state">
								<option value="Select State"> State </option>
								   <?php foreach($state_datas as $state){  ?>
								   <option value="<?php echo $state['state_id']; ?>"<?php if(isset($invoice)){if($invoice['merchantState']==$state['state_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $state['state_name']; ?> </option>
								   
								<?php } ?>  
								</select><?php */ ?>
							</div>
                        </div>	
                                   
                        <div class ="form-group">
						   <label class="col-md-4 control-label" name="country" for="example-typeahead">Country</label>
						   <div class="col-md-8">
						  <input type="text" id="country" class="form-control" name="country" value="<?php if(isset($invoice)){ echo $invoice['merchantCountry']; } ?>">
							<?php /*	<select id="country" class="form-control " name="country" >
								<option value="Country"> Country </option>
								<?php foreach($country_datas as $country){  ?>
								   <option value="<?php echo $country['country_id']; ?>" <?php if(isset($invoice)){ if($invoice['merchantCountry']==$country['country_id'] ){ ?> selected="selected" <?php }  ?> <?php }?>> <?php echo $country['country_name']; ?> </option>
								   
								<?php } ?>  
								</select> */ ?>
							</div>
                        </div>	
                                
                                
                                
                        <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Phone Number</label>
							<div class="col-md-8">
								<input type="text" id="merchantContact" name="merchantContact" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantContact']; } ?>">
							</div>
						</div>
                        
                         <div class="form-group">
    							<label class="col-md-4 control-label" for="example-username">Alternate Number</label>
    							<div class="col-md-8">
    								<input type="text"  id="merchantAlternateContact" name="merchantAlternateContact" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAlternateContact']; } ?>">
    							</div>
    						</div>
    						
    						 <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">ZIP Code</label>
							<div class="col-md-8">
								<input type="text" id="merchantZipCode" name="merchantZipCode" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantZipCode']; } ?>">
							</div>
						</div>
                      </div>
					  
									  
					
					<?php /* <div class="col-sm-6">
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-file-input">Upload Logo</label>
							<div class="col-md-8">
								<input type="file" id="upload_logo" name="picture" class="upload" ><img src="<?php if(isset($invoice)){echo $invoice['merchantProfileURL']; } ?>" height="80px" width="80px">
							</div>
						</div>
					</div> */ ?>
		
				  	
	              <div class="form-group">
					<div class="col-md-8 col-md-offset-11">
						
						<button type="submit" class="submit btn btn-sm btn-success" >Save </button>	
					</div>
				</div>					
					
		
		
  	</form>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>
$(document).ready(function(){

   
  
    $('#form_new').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'merchantAddress1': {
                       validate_addre:true,
                        minlength: 5
                    },
					
					'city':{
					    validate_char:true,
						  
						  
					},
					'state':{
					     validate_char:true,
						  
						  
					},
					'country':{
					    validate_char:true,
					},
					'firstName':{
						    required:true,
                    validate_char:true,
							minlength: 3
					},
					'lastName':{
						    required:true,
                    validate_char:true,
							minlength: 3
					},
					
					  'merchantContact': {
                      
						 minlength: 10,
						 phoneUS : true,
						 maxlength: 17,
                       
                    },
                    'merchantZipCode': {
                       ZIPCode:true,
                         minlength: 3,
                         maxlength:10
                    },
					'prefix':{
					 /*   required: true, */
					   minlength: 3,
					   },
					'postfix':{
					    required: true,
					    digits:true,
                    }
			
			},
    });

  $.validator.addMethod("phoneUS", function(value, element) {
         
         if(value=='')
         return true;
           return value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
   
        }, "Please specify a valid phone number");
    $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-._ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/,. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
     $.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );   
                                            
                                            
              
 

 
 
   $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Integration/Settings/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			// data=$.parseJSON(res);
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Integration/Settings/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});   


});	
   
    
		
	

</script>

</div>