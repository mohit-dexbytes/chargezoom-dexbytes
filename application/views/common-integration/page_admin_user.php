<style>
	.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 50px !important;
	}
</style>
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
	           ?>

    <!-- All Orders Block -->
    <legend class="leg">User Management</legend>
    <div class="full">
	           
        <!-- All Orders Title -->
        <div style="position: relative">
							
							<!--<a href="#add_role" onclick="set_addrole_id('<?php  ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Create User" class="btn btn-alt btn-sm btn-success"  > <i class="fa fa-plus"> </i> </a>-->
                        <div class="addNewFixRight width160FixRight">	
                            <a class="btn btn-sm btn-primary" title="Create User" href="<?php echo base_url(); ?>Integration/Settings/admin_role">User Roles</a>
							<?php if($plantype){ 
                                if(isset($user_data) && $user_data)
				                {

                                    if($plantype['merchant_plan_type'] == 'AS' && count($user_data) < 10){
                                        echo '<a class="btn pull-lft btn-sm btn-success" title="Create User"   href="'.base_url().'Integration/Settings/create_user">Add New</a>';

                                    }else{ 
                                        echo '<a class="btn pull-lft btn-sm btn-success" style="display:none;" title="Create User"  href="'.base_url().'Integration/Settings/create_user">Add New</a>';
                                        
                                    } 
                               }else{ ?>
                                    <a class="btn pull-lft btn-sm btn-success" title="Create User"   href="<?php echo base_url(); ?>Integration/Settings/create_user">Add New</a>
                                
                               <?php } ?>
                            <?php }else{ ?>
                                
                                <a class="btn pull-lft btn-sm btn-success" title="Create User"  href="<?php echo base_url(); ?>Integration/Settings/create_user">Add New</a>
                                
                            <?php }?>
                                
                        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="admin_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
					<th class="text-left"> Name </th>
					<th class="text-left"> Role Name </th>
					<th class="hidden-xs text-left"> Email Address</th>
                   <!-- <th class="text-left hidden-xs "> Powers </th>-->
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($user_data) && $user_data)
				{
					foreach($user_data as $user)
					{
					 /* print_r($user_data); die;	 */

				?>
				<tr>
				
					<td class="text-left"> <?php echo $user['userFname'].' '.$user['userLname']; ?></td>
					<td class="text-left"> <?php echo $user['roleName']; ?></td>
					<td class="text-left hidden-xs"> <?php echo $user['userEmail']; ?></td>
					
                   <?php /* <td class="text-left hidden-xs"><?php echo implode(', ',$user['authName']); ?> </td> */ ?>
					
					
					<td class="text-center">
						<div class="btn-group btn-group-xs">
						
						<a href="<?php echo base_url('Integration/Settings/create_user/'.$user['merchantUserID']); ?>" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"> </i> </a>
							
                        <a href="#del_user" onclick="del_user_id('<?php  echo $user['merchantUserID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
							
					</div>
					</td>
				</tr>
				
				<?php } }
				else { echo'<tr><td colspan="4"> No Records Found </td>
                    
                    <td style="display: none"></td>
                    <td style="display: none"></td>
                    <td style="display: none"></td></tr>'; }  
				?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    </div>
    <!-- END All Orders Block -->
    </div>

<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#admin_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>	