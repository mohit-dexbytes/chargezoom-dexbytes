<!--Page content -->
<?php
	$this->load->view('alert');
    if(!isset($activeval)){
        $activeval = 'Active';
    }

    if(!isset($type)){
        $type = '1';
    }
?>
<div id="page-content">
    <div class="msg_data"> 
        <?php echo $this->session->flashdata('message');   ?>
    </div>
    <legend class="leg">Customers </legend>
    <!-- All Orders Block -->
    <div class="block-main full page_customer" style="position: relative">
        
        <!-- All Orders Title -->
        <span class="" ><a class="btn btn-sm btn-success addNewBtnCustom" href="<?php echo base_url(); ?>Integration/Customers/create_customer">Add New</a></span>
        <!-- END All Orders Title -->
       
        <div class="block-options show_hide_cust_btn">
            <?php if($activeval  == 'Active'){  ?>
                <a id="sh_cust" class="btn btn-sm btn-primary1" href="<?php echo base_url('Integration/Customers/customer_list_all').'/'.$activeval ?>">Show/Hide Inactive</a>
            <?php } else { ?>
                <a id="sh_cust" class="btn btn-sm btn-primary1" href="<?php echo base_url('Integration/Customers/customer_list_all').'/'.$activeval ?>">Show/Hide Active</a>
            <?php  } ?>             
            <input type="hidden" name="customer_type" id="customer_type"  value="<?php 	if(isset($activeval) && $activeval){ echo $activeval; } ?>">
        </div> 
          
         
        <!-- All Orders Content -->
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <!--th class="text-center">Customer ID</th-->
                    <th class="text-left">Customer Name</th>
                    <th class="text-left ">Full Name</th>
                    <th class="text-left hidden-xs">Email Address</th>
                    <!-- <th class="hidden-xs hidden-sm text-right">Phone Number</th> -->
                    <th class="text-right hidden-xs">Balance</th>
                </tr>
            </thead>
            <tbody>
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
    <?php if($type == 0){ ?>
        <input type="hidden" id="data_type" value="0">
     <?php } else { ?>
        <input type="hidden" id="data_type" value="1">
  <?php  } ?> 
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>
    $(document).ready(function () {
        $(function () { // EcomOrders.init(); 
            Pagination_view111.init();
        });

        var type = $('#data_type').val();
        var table_datatable = '';
        var Pagination_view111 = function () {

            return {
                init: function () {
                    /* Extend with date sort plugin */
                    $.extend($.fn.dataTableExt.oSort, {

                    });

                    /* Initialize Bootstrap Datatables Integration */
                    App.datatables();

                    /* Initialize Datatables */
                    table_datatable = $('.compamount').dataTable({
                        "processing": true, //Feature control the processing indicator.
                        "serverSide": true, //Feature control DataTables' server-side processing mode.
                        "lengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                        "pageLength": 10,
                        // Load data for the table's content from an Ajax source
                        "ajax": {
                            "url": "<?php echo site_url('Integration/Customers/customer_list_ajax')?>",
                            "type": "POST",

                            "data": function (data) {
                                data.<?php echo $this -> security -> get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                                data.type = type;
                            },

                        },



                        "order": [[1, 'desc']],

                        "sPaginationType": "bootstrap",
                        "stateSave": false

                    });
                    /* Add placeholder attribute to the search input */
                    $('.dataTables_filter input').attr('placeholder', 'Search');
                    $('#ecom-orders111_filter').remove();
                    $('#ecom-orders111_length').after('<div id="ecom-orders111_filter" class="dataTables_filter"><label><div class="input-group"><input type="search" class="form-control" placeholder="Search" aria-controls="ecom-orders111"><span class="input-group-addon"><i class="fa fa-search"></i></span></div></label></div>');
                    $('#ecom-orders111_filter').addClass('col-md-10');
                    $('#ecom-orders111_length').addClass('col-md-2');

                }
            };
        }();
        $(document).on('keyup', '.dataTables_filter input', function () {
            table_datatable.fnFilter(this.value).draw();
        });
    });



</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

.custom-datatable-filter-class{
    width: 100% !important;
}

#ecom-orders111_length {
    padding-left: 0;
    padding-right: 0;
    width: 75px;
}

#ecom-orders111_filter{
    padding-left: 0;
}

#ecom-orders111_filter label{
    float: left;
}
.addNewBtnCustom{
    right: 0 !important;
}
@media only screen and (min-width: 992px) and (max-width: 1279px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }
}
@media only screen and (min-width: 1280px) and (max-width: 1439px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }   
}

@media only screen and (min-width: 1440px) and (max-width: 1679px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }   
}

@media only screen and (min-width: 1680px) and (max-width: 2180px){
    .block-options.show_hide_cust_btn a#sh_cust {
        left: 300px;
    }   
}
</style>

<script src="<?php echo base_url(JS); ?>/qbd/customer.js?v=1.0"></script>

</div>
<!-- END Page Content