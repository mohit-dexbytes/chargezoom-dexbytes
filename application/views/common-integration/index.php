<style>

#chart-classic2 g text tspan {
    visibility: visible;
}
#chart-classic2 g {
    /* opacity: 0.5; */
	border-width:2px;
}

#chart-classic2 text tspan {
    visibility: hidden;
}

	#chart-pie-customers g text tspan {
    visibility: visible;
}
#chart-pie-customers g {
    opacity: 1.5;
	borderWidth:2px;
}

#chart-pie text tspan {
    visibility: hidden;
}

	#chart-pie g text tspan {
    visibility: visible;
}
#chart-pie g {
    opacity: 1.5;
	borderWidth:2px;
}

#chart-pie-customers text tspan {
    visibility: hidden;
}
.shield-container{
    margin-top: -40px;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">
<input type="hidden" id="currentDate" value="<?php echo date("Y-m-d"); ?>">
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- eCommerce Dashboard Header -->
  
    <!-- END eCommerce Dashboard Header -->

    <!-- Quick Stats -->
   
	<div class="row ">
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
      
        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-success rounded-circle mr-2">
                        <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                        <!-- <i class="ion-logo-usd avatar-title font-26 text-white"></i> -->
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                    $volume =$recent_pay; 
                                ?>$<?php echo $volume?number_format($volume,2):'0.00'; ?></span></h4>
                            <p class=""><?php echo date('F');?> Revenue</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-info rounded-circle">
                        <i class="fa fa-credit-card ion-md-cart avatar-title font-26 text-white"></i>
                        
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                    $card_payment =$card_payment; 
                                ?>$<?php echo $card_payment?number_format($card_payment,2):'0.00'; ?></span></h4>
                            <p class="">Credit Card Payments</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-purple rounded-circle">
                        <i class=""></i>
                        <i class="fa fa-credit-card custom-warning-icon ion-md-contacts avatar-title font-26 text-white"></i>
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                    $eCheck_payment =$eCheck_payment; 
                                ?>$<?php echo $eCheck_payment?number_format($eCheck_payment,2):'0.00'; ?></span></h4>
                            <p class=" ">eCheck Payments</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-primary rounded-circle">
                       
                         <i class="fa fa-calendar custom-danger-icon ion-md-eye avatar-title font-26 text-white"></i>
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                    $outstanding_total =$outstanding_total; 
                                ?>$<?php echo $outstanding_total?number_format($outstanding_total,2):'0.00'; ?></span></h4>
                            <p class="">Accounts Receivable</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>
    </div>
    
    <!-- END Quick Stats -->

    <!-- eShop Overview Block -->
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="card-header py-3 bg-transparent">
                    
                    <h5 class="card-title">
                        Revenue History: <span id="totalPayment">$0.00</span>
                        <select class="revenue_filter" name="revenue_filter" id="revenue_filter">
                            <option <?php if($rhgraphOption == 3){ echo 'selected'; } ?> value="3">Hourly</option>
                            <option <?php if($rhgraphOption == 2){ echo 'selected'; } ?> value="2">Month-to-Date</option>
                            <option <?php if($rhgraphOption == 1){ echo 'selected'; } ?> value="1">30 Days</option>
                            <option <?php if($rhgraphOption == 0){ echo 'selected'; } ?> value="0">Annual</option>
                            <option <?php if($rhgraphOption == 4){ echo 'selected'; } ?> value="4">Custom</option>
                        </select>
                        <div class="col-md-4" id="rangeDataDashboard" <?php  if($rhgraphOption != 4){ ?> style="display:none" <?php  } ?> >
                             <div class="input-group input-daterange" data-date-format="yyyy-mm-dd">
                                    <input type="text" id="startDate" name="startDate" class="form-control text-center" value="<?php echo (isset($rhgraphFromDate) && $rhgraphFromDate != '0000-00-00' )?($rhgraphFromDate):date('Y-m-d'); ?>" placeholder="From">
                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                                    <input type="text" id="endDate" name="endDate" class="form-control text-center"  value="<?php echo (isset($rhgraphToDate) && $rhgraphToDate != '0000-00-00' )?($rhgraphToDate):date('Y-m-d'); ?>"  placeholder="To">
                             </div>
                       </div>
                    </h5>
                </div>
                <!-- <div class="row"> -->
                <!-- <div id="chart-classic1" style="height: 390px;" class="chart"></div> -->
                <img id="graphloader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">
                <div class="revenueGraph">
                    <div id="placeholder" style="height:300px;" class="demo-placeholder"></div>
                    <div id="choices" >
                        
                    </div>

                    <div id="legendContainer" class="legend" ></div>

                </div>
                <!-- </div> -->
            </div>
        </div>
        
    </div>
	<div class="row">
        <div class="col-sm-6">
            
            <div class="block full">
                <div class="card-header py-3 bg-transparent">
                    <h5 class="card-title">Top 5 Due By Customer</span></h5>
                </div>
                <div id="chart-pie-customers" class="chart_pie_sm"></div>

            </div>    
        </div>
        <div class="col-sm-6">
            
            <div class="block full">
                <div class="card-header py-3 bg-transparent">
                    <h5 class="card-title">Top 5 Overdue By Customer</span></h5>
                </div>
                <div id="chart-pie" class="chart_pie_sm"></div>
            </div> 
        </div>
        
    </div>
   
    <div class="row">
        <div class="col-sm-6">
            <!-- Latest Orders Block -->
            
            <div class="block-white full">
                <!-- Latest Orders Title -->
                <div class="card-header py-3 bg-transparent pad-20">
                    <h5 class="card-title">Recent Payments</span></h5>
                </div>
                <!-- END Latest Orders Title -->
			    <table class="table table-bordered table-striped table-vcenter dashboard-table">
                   <thead>
                        <tr>
                            <!-- <td class="text-center"><strong>Full Name</strong></td> -->
                             <?php if($plantype) { ?>
                            <td class="text-left custom-head" style="display:block"><strong>Customer Name</strong></td>
                             <?php } else { ?>
                             <td class="text-left custom-head"><strong>Customer Name</strong></td>
                             <?php } ?>
							<td class="text-left"><strong>Invoice</strong></td>
                            <td class="text-right"><strong>Payment Date</strong></td>
                            <td class="text-right"><strong>Amount</strong></td>
							
                           
                        </tr>
                        </thead>
                        <tbody>
                       	<?php   if(!empty($recent_paid)){   
 
						foreach($recent_paid as $invoice){
                            $inv_url1 = '';
                            if (!empty($invoice['invoice_id']) && !empty($invoice['invoice_no'])) {
                                $invs = explode(',', $invoice['invoice_id']);

                                $invoice_no = explode(',', $invoice['invoice_no']);

                                foreach ($invs as $k => $inv) {
                                    $inv_url = base_url() . 'Integration/Invoices/invoice_details/' . trim($inv);
                                    if (isset($invoice_no[$k]))
                                        if ($plantype) {
                                            $inv_url1 .= $invoice_no[$k].',';
                                        } else {
                                            $inv_url1 .= ' <a href="' . $inv_url . '"><strong>' . $invoice_no[$k] . '</strong></a>,';
                                        }
                                }

                                $inv_url1 = substr($inv_url1, 0, -1);
                            } else{
                                $inv_url1 .= '<a href="javascript:void(0);">---</a> ';
                            }

					 ?>
                        <tr>
                        <?php if($plantype) { ?>
                            <td class="text-left"  style="display:block"><?php echo $invoice['FullName']; ?></td>
						 <?php } else {?>
						  <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Integration/Customers/customer_detail/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>
						 <?php } ?>

                         <?php if ($plantype_vs) { ?>
                        <td class="text-left cust_view"><?php echo $invoice['refNumber']; ?></td>
                        <?php } else { ?>
                        <td class="text-left cust_view"><?php echo $inv_url1; ?></td>
                        <?php } ?> 
                        
                            <td class="text-right"><?php  echo  date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>

                            <td class="text-right cust_view"><a href="#pay_data_process"   onclick="get_payment_transaction_data('<?php  echo $invoice['TxnID']; ?>','comman');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$'.number_format($invoice['balance'], 2); ?></a></td>
							 
                              
                        </tr>
						   <?php } }else { ?>	
						
						<tr>
                            <td colspan="4"><strong>No Records Found</strong></td>
                         
                        </tr>
						
						   <?php } ?>
						
						   
                       
                        
                    </tbody>
                </table>
              
                <!-- END Latest Orders Content -->
            </div>
            <!-- END Latest Orders Block -->
        </div>
        <div class="col-sm-6">
            <!-- Top Products Block -->
            
            <div class="block-white full">
                <!-- Top Products Title -->
                <div class="card-header py-3 bg-transparent pad-20">
                    <h5 class="card-title">Oldest Invoices</span></h5>
                </div>
                <!-- END Top Products Title -->

                <!-- Top Products Content -->
               <table class="table table-bordered table-striped table-vcenter dashboard-table">
                   <thead>
                        <tr>
                             <?php if($plantype) { ?>
                            <td class="text-left custom-head" style="display:block"><strong>Customer Name</strong></td>
                             <?php } else { ?>
                             <td class="text-left custom-head"><strong>Customer Name</strong></td>
                             <?php } ?>
                            <td class="text-left custom-head" ><strong>Invoice</strong></td>
							<!-- <td class="text-left" ><strong>Full Name</strong></td> -->
							  
                            <td class="text-right"><strong>Aging</strong></td>
                           
							  <td class="text-right"><strong>Amount</strong></td>

                        </tr>
                        </thead>
                        <tbody>
						<?php   if(!empty($oldest_invs)){   

                       //   print_r($invoices); 
						foreach($oldest_invs as $invoice){
							
						 ?>
                        <tr>
                            <?php if($plantype) { ?>
                                <td class="text-left"  style="display:block"><?php echo $invoice['FullName']; ?></td>
                             <?php } else { ?>
                            <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Integration/Customers/customer_detail/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>
                             <?php } ?>
                            
                            
                            <!-- <td class="text-right cust_view"><a href="<?php echo base_url(); ?>Integration/Invoices/invoice_details/<?php echo $invoice['invoiceID']; ?>"><?php  echo $invoice['refNumber']; ?></a></td> -->
                            <?php if ($plantype_vs) { ?>
                            <td class="text-left cust_view"><?php echo $invoice['refNumber']; ?></td>
                            <?php } else { if($plantype_as){ ?>
                            <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Integration/Invoices/invoice_details/<?php echo $invoice['invoiceID']; ?>"><strong><?php echo $invoice['refNumber']; ?></strong></a></td>
                            <?php } else{ ?>
                                <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Integration/Invoices/invoice_details/<?php echo $invoice['invoiceID']; ?>"><strong><?php echo $invoice['refNumber']; ?></strong></a></td>
                            <?php } } ?>
                            <!-- <td class="text-left cust_view"><?php  echo $invoice['refNumber']; ?></td> -->

                            <td class="text-right"><?php  echo  $invoice['aging_days']; ?></td>
						   <td class="text-right">$<?php  echo  number_format($invoice['BalanceRemaining'],2); ?></td>
                            
                        </tr>
						   <?php  } }else { ?>	
						<tr>
                            <td colspan="4"><strong>No Records Found</strong></td>
                        </tr>
						
						   <?php } ?>
						
						   
                       
                        
                    </tbody>
                </table>
                <!-- END Top Products Content -->
            </div>
            <!-- END Top Products Block -->
        </div>
    </div>
    <!-- Live Chart Block -->
<?php /*	<section style="position:absolute; top:0;">
		<div class="block full" style="padding: 0; margin: 0; border: none;">
		<div class="row chart-area" style="">
			<div id="chart-live" class="chart ls" style="visibility:hidden;"></div>
			<div id="chart-classic" class="chart ls" style="visibility:hidden;"></div>
		</div>		
		</div>   
	</section>  <?php */ ?>
	
	
	</div>
	

<link id="themecss" rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/shieldui/all.min.css" />          
<script src="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/js/shieldui/shieldui-all.min.js"></script>	
<script>$(function(){  //CompCharts.init(); 
});</script>
<script>
  /*
 *  Document   : compCharts.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Charts page
 */
                        
                
                        
                        
                        
var general_volume = 0;
var pchart_color_customer = []; 
var pchart_past_due = []; 

 
 $.ajax({
			type:"POST",
				url : "<?php echo base_url(); ?>Integration/home/get_invoice_due_company",
					dataType: 'json',
            success: function (data) {
                var plancharge  = data;
              //  console.log(plancharge);
                var chartPie    = $('#chart-pie-customers'); 
                var color =['#28a745', '#007bff', '#ffc107', '#e67e22', '#e74c3c', '#34dbcb', '#db7734', '#3abc','#ff1a75', '#8c1aff'];
                showjsdataLegend = true;
                if (plancharge.length == 0){
                    plancharge = [['No Data', 0.0001]];
                    color = ['#7F7F7F'];
                    showjsdataLegend = false;
                }
                CompCharts.init(plancharge, chartPie, color, showjsdataLegend);

                }
    		});	   
    		
    $.ajax({
			type:"POST",
				url : "<?php echo base_url(); ?>Integration/home/get_invoice_Past_due_company",
				dataType: 'json',
            success: function (data) {
           
           var plancharge =data;
         //   console.log(plancharge);
           var chartPie    = $('#chart-pie'); 
            var color = ['#28a745', '#007bff', '#ffc107', '#e67e22', '#e74c3c', '#34dbcb', '#db7734', '#3abc','#ff1a75', '#8c1aff'];
            showjsdataLegend = true;
            if (plancharge.length == 0){
                plancharge = [['No Data', 0.0001]];
                color = ['#7F7F7F'];
                showjsdataLegend = false;
            }

           CompCharts.init(plancharge, chartPie, color, showjsdataLegend);
            $('.chart_pie_sm').css('height', '350px');
                }
    		});


    
    		
var CompCharts = function() {

    return {
        init: function(pchart_data,char_id, color, showLegend = true) {
       
       //     var chartPiecustomer = $('#chart-pie-customers');
       var colorPalette = color;
        char_id.shieldChart({
            events: {
                legendSeriesClick: function (e) {
                    // stop the series item click event, so that 
                    // user clicks do not toggle visibility of the series
                    e.preventDefault();
                }
            },
            theme: "bootstrap",
            seriesPalette: colorPalette,
            seriesSettings: {
                    donut: {
                        enablePointSelection: false,
                        addToLegend: showLegend,
                        activeSettings: {
                            pointSelectedState: {
                                enabled: false
                            }
                        },
                        enablePointSelection: false,
                       
                        dataPointText: "",
                        borderColor: '#ffffff',
                        borderWidth:2
                    }
                },
                
                chartLegend: {
                    align: "right",
                    renderDirection: 'vertical',
                    verticalAlign: "middle",
                    legendItemSettings: {
                        bottomSpacing: 7
                    }
                },
            exportOptions: {
                image: false,
                print: false
            },
            primaryHeader: {
                text: ""
            },
           
                        
            
           
            tooltipSettings: {
                 customHeaderText:"{point.collectionAlias}", 
              
            
                
           customPointText: function (point, chart) {
                           
            return shield.format(
                '<span>{value}</span>',
                {
                    value: format2(point.y)
                }
            );
        }
              
            },
            axisY: {
                title: {
                    text: ""
                }
            },
            dataSeries: [{
                seriesType: "donut",
                collectionAlias: "Customers",
                data: pchart_data
            }]
        });
             
            
            
            
            
            
        /*    $.plot(chartPie, pchart_past_due,
                {
                    colors: ['#28a745', '#007bff', '#ffc107', '#e67e22', '#e74c3c', '#34dbcb', '#db7734', '#3abc','#ff1a75', '#8c1aff'],
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                             enablePointSelection: true,
                            label: {
                                show: true,
                                radius: 3 / 4,
                                formatter: function(label, pieSeries) { 
                                    
                                 //   console.log(pieSeries.data[0][1]);
                                 //   return '<div class="chart-pie-label">' + label + '<br>' + format2(pieSeries.data) + '</div>';
                                    return '<div class="chart-pie-label">' + label + '<br>' + format2(pieSeries.data[0][1]) + '</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );  */
			
            // Pie Chart
           /* $.plot(chartPiecustomer, pchart_color_customer,
                {
                    colors: ['#28a745', '#007bff', '#ffc107', '#e67e22', '#e74c3c', '#34dbcb', '#db7734', '#3abc','#ff1a75', '#8c1aff'],
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 3 / 4,
                                formatter: function(label, pieSeries) { 
                                    return '<div class="chart-pie-label">' + label + '<br>' + format2(pieSeries.data[0][1]) + '</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );  */

        }
    };
}();

    
</script>


<!-- flot chart -->
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.time.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.selection.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.stack.js"></script>

<link href="<?php echo base_url(CSS); ?>/moltran/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />
<script type="text/javascript">


        

    function revenueData(){
        $('#graphloader').show(); 
        var filterOption = $('#revenue_filter').val();
        if(filterOption == 4){
          var startDate = $('#startDate').val();
          var endDate = $('#endDate').val();
        }else{
          var startDate = $('#startDate').val();
          var endDate = $('#endDate').val();
        }   
        return $.ajax({
            type:"POST",
            url : "<?php echo base_url(); ?>Integration/home/dashboardReport",
            data: {
                revenue_filter: filterOption,
                startDate: startDate,
                endDate: endDate,
            },
            success: function (data) {
                var general_volume = $.parseJSON(data);
                $('#totalPayment').html(format2(general_volume.totalRevenue));  
                $('#RA').html(format2(general_volume.totalRevenue)); 
                $('#CCA').html(format2(general_volume.totalCCA));   
                $('#ECLA').html(format2(general_volume.totalECLA)); 
                var x1 = general_volume.revenu_volume;
                var x2 = general_volume.online_volume;
                var x3 = general_volume.eCheck_volume;

                var y1 = general_volume.revenu_month;
                var legendContainer = document.getElementById("legendContainer");


                var datasets = {
                    "Revenue": {
                        data: x1, label: "Revenue",color:'#33b86c',hoverable:true, shadowSize: 2,highlightColor: '#007bff',clickable: true
                     },
                    "Credit Card Payments": {
                            data: x2, label: "Credit Card Payments",color:'#007bff',hoverable:true,clickable: true
                        },
                    "eCheck Payments": {
                         data: x3, label: "eCheck Payments",color:'#7e57c2',hoverable:true,clickable: true
                    }
                };
                var i = 1;
                $("#choices").html('');

                var choiceContainer = $("#choices");

                $.each(datasets, function(key, val) {
                    var checkM = '';
                    var classOpacity = '';
                    if(key != 'Revenue'){
                         checkM = "checked='checked'";
                    }else{
                         classOpacity = 'opacityManage';
                    }
                    
                    choiceContainer.append("<div class='displayLegend " + classOpacity + " ' id='color" + i + "'><input class='checkboxNone' type='checkbox' name='" + key +
                        "' " + checkM + " data-id='" + i + "' id='id" + key + "'></input> <label id='color" + i + "' style='padding:1px;width: 12px;height: 12px;background:"+ val.color +" ;' for='id" + key + "' id='colorSet" + i + "'></label>" +
                        "<label class='legendLabel' for='id" + key + "'>"
                        + val.label + "</label> </div>");
                    ++i;
                    
                });

                var data = [];

                choiceContainer.find("input:checked").each(function () {
                    var key = $(this).attr("name");
                    if (key && datasets[key]) {
                        data.push(datasets[key]);
                    }
                });
                choiceContainer.find("input").click(plotAccordingToChoices);
                $('.checkboxNone').change(function() {
                    var id = $(this).attr("data-id");
                    
                    var checkGraph = choiceContainer.find("input:checked").length;
                    if(checkGraph != 0){
                      if ($(this).prop('checked') == true){
                          $("#color"+id).removeClass('opacityManage');
                      } else {
                          $("#color"+id).addClass('opacityManage');
                      }
                    }
                });
                function plotAccordingToChoices() {

                    var data = [];

                    choiceContainer.find("input:checked").each(function () {
                        var key = $(this).attr("name");
                        
                        if(key != 'Revenue'){
                            if (key && datasets[key]) {
                                data.push(datasets[key]);
                            }
                        }else{
                            if ($('#idRevenue').prop('checked') == true){
                                if (key && datasets[key]) {
                                    data.push(datasets[key]);
                                }
                            }
                        
                        }
                        
                    });

                    if (data.length > 0) {
                        $.plot("#placeholder", data, {
                            series: { lines: { show: !0, fill: !0, lineWidth: 1, fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.9 }] } }, points: { show: !0 }, shadowSize: 0 },
                            legend: {
                                position: "nw",
                                margin: [0, -24],
                                noColumns: 0,
                                backgroundColor: "transparent",
                                labelBoxBorderColor: null,
                                labelFormatter: function (o, t) {
                                    return o + "&nbsp;&nbsp;";
                                },
                                width: 30,
                                height: 2,
                                container: legendContainer,
                                onItemClick: {
                                      toggleDataSeries: true
                                  },
                            },
                            grid: { hoverable: !0, clickable: !0, borderColor: i, borderWidth: 0, labelMargin: 10, backgroundColor: 'transparent' },
                            yaxis: { min: 0, max: 15, tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            xaxis: { ticks: y1,tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            tooltip: !0,
                            tooltipOpts: { content: function(data, x, y, dataObject) {

                                            var XdataIndex = dataObject.dataIndex;
                                            var XdataLabel = dataObject.series.xaxis.ticks[XdataIndex].label;
                                            return format2(y);
                                        }, shifts: { x: -60, y: 25 }, defaultTheme: !1 },

                            
                            
                            yaxis: {
                                autoScale:"exact"
                            }
                        }); 
                    }
                }
                plotAccordingToChoices();
                $('#graphloader').hide();
            }
        });
    }

    $('#revenue_filter').change(function(){
        var filterOption = $('#revenue_filter').val();
        if(filterOption == 4){
          $('#rangeDataDashboard').show();
        }else{
          $('#rangeDataDashboard').hide();
        }
        revenueData();
    });
    $('#startDate').on('changeDate', function() {
        var filterOption = $('#revenue_filter').val();  
        if(filterOption == 4){
          revenueData();
        }
    });
    $('#endDate').on('changeDate', function() {
        var filterOption = $('#revenue_filter').val();  
        if(filterOption == 4){
          revenueData();
        }
    });
    var filterOption = $('#revenue_filter').val()
    if(filterOption != 4){
      revenueData();
    }

    function getTooltip(label, x, y) {
        console.log(x);
            return "Your sales for " + x + " was $" + y; 
    }

</script>