<div class="gateway-div1" id="maverick_div1" style="display:none">         
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverickAccessToken1">Access Token</label>
        <div class="col-md-8">
            <input type="text" id="maverickAccessToken1" name="maverickAccessToken1" class="form-control"  placeholder="Access Token">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverickTerminalId1">Terminal ID</label>
        <div class="col-md-8">
            <input type="text" id="maverickTerminalId1" name="maverickTerminalId1" class="form-control"  placeholder="Terminal ID">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverick_cr_status1">Credit Card</label>
        <div class="col-md-8">
            <input type="checkbox" id="maverick_cr_status1" name="maverick_cr_status1" checked="checcked" >
            
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverick_ach_status1">Electronic Check</label>
        <div class="col-md-8">
            <input type="checkbox" id="maverick_ach_status1" name="maverick_ach_status1"  >
            
        </div>
    </div>
</div>