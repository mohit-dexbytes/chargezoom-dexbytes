<div class="gateway-div" id="maverick_div" style="display:none">         
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverickAccessToken">Access Token</label>
        <div class="col-md-8">
            <input type="text" id="maverickAccessToken" name="maverickAccessToken" class="form-control"  placeholder="Access Token" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverickTerminalId">Terminal ID</label>
        <div class="col-md-8">
            <input type="text" id="maverickTerminalId" name="maverickTerminalId" class="form-control"  placeholder="Terminal ID" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverick_cr_status">Credit Card</label>
        <div class="col-md-8">
            <input type="checkbox" id="maverick_cr_status" name="maverick_cr_status" checked />
            
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="maverick_ach_status">Electronic Check</label>
        <div class="col-md-8">
            <input type="checkbox" id="maverick_ach_status" name="maverick_ach_status" >
            
        </div>
    </div>
</div>