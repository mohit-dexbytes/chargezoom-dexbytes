<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">

	

	<div class="msg_data ">
		<?php echo $this->session->flashdata('message');   ?>
	</div>
	<!-- END Forms General Header -->
	<div class="row">
		<!-- Form Validation Example Block -->
		<div class="col-md-12">
			<div class="block">
				<div class="block-title">
					<h2><strong><?php if (isset($subs)) {
									echo "Edit";
								} else {
									echo "Create";
								} ?> Subscription </strong> </h2>
				</div>
				

				<form id="form-validation" action="<?php echo base_url(); ?>company/SettingSubscription/create_subscription" method="post" class="form-horizontal form-bordered">
					
					<input type="hidden" name="cardID" id="cardID" value="<?php if (isset($subs)) {
																			echo $subs['cardID'];
																		} ?>" />
					<input type="hidden" name="subID" id="subID" value="<?php if (isset($subs)) {
																			echo $subs['subscriptionID'];
																		} ?>" />
					<fieldset>
						<div class="col-md-12">

							<div class="col-md-3 form-group">
								<label class="control-label" for="customerID">Customer</label>
								<div>
									<select id="customerID" name="customerID" class="form-control select-chosen">
										<option value>Select Customer</option>
										<?php foreach ($customers as $customer) {       ?>
											<option value="<?php echo $customer['ListID']; ?>" <?php if (isset($subs) &&  $subs['customerID'] == $customer['ListID']) {
																									echo "selected";
																								} ?>><?php echo  $customer['FullName']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-3 form-group">


								<label class="control-label" for="taxes">Plan</label>
								<div>
									<select name="sub_plan" id="sub_plan" onchange="set_plan_change();" class="form-control">
										<option value="0">Select Plan</option>
										<?php foreach ($subplans as $subplan) { ?>
											<option value="<?php echo $subplan['planID']; ?>" <?php if (isset($subs) && $subplan['planID'] == $subs['planID']) {
																									echo "selected";
																								} ?>><?php echo  $subplan['planName']; ?></option>

										<?php } ?>
									</select>

								</div>
							</div>

							<div class="col-md-3 form-group">


								<label class="control-label" for="customerID">Frequency</label>
								<div>
									<select name="paycycle" id="paycycle" class="form-control">
										<option value="">Select Frequency</option>
										<?php foreach ($frequencies as $frequecy) { ?>
											<option value="<?php echo $frequecy['frequencyValue']; ?>" <?php if (isset($subs) && $frequecy['frequencyValue'] == $subs['invoiceFrequency']) {
																											echo "selected";
																										} ?>><?php echo $frequecy['frequencyText']; ?> </option>
										<?php } ?>
									</select>
								</div>
							</div>



							<div class="col-md-3  form-group">
								<label class="control-label" for="Recurrence">Recurrence</label>
								<div>

									<input type="text" id="duration_list" name="duration_list" class="form-control" value="<?php if (isset($subs)) {
																																echo  $subs['subscriptionPlan'];
																															}; ?>" placeholder="0 for Unlimited" />

								</div>
							</div>
							<div class="form-group col-md-3 ">
								<div class="">

									<label class="control-label ">Free Trial Recurrence</label>

									<input type="text" class="form-control" id="freetrial" name="freetrial" value="<?php if (isset($subs)) echo $subs['freeTrial']; ?>" placeholder="Free Trial Recurrence">

								</div>
							</div>


							<div class="col-md-3  form-group">
								<label class=" control-label" for="firstName">Subscription Start Date</label>
								<div>
									<div class="input-group input-date">

										<input type="text" id="sub_start_date" name="sub_start_date" class="form-control input-datepicker" value="<?php if (isset($subs)) {
																																						echo date('m/d/Y', strtotime($subs['startDate']));
																																					} else {
																																						echo date('m/d/Y');
																																					} ?>" data-date-format="mm/dd/yyyy" placeholder="Subscription Start Date">
										<span class="input-group-addon sub_date"><i class="fa fa-calendar"></i></span>
									</div>
								</div>

							</div>




							<div class="col-md-3 form-group">


								<label class="control-label" for="taxes">Tax</label>
								<div>
									<select name="taxes" id="taxes" class="form-control tax_div">
										<option value="">Select Tax</option>
										<?php foreach ($taxes as $tax) { ?>
											<option value="<?php echo $tax['taxID']; ?>" <?php if (isset($subs) && $tax['taxID'] == $subs['taxID']) {
																								echo "selected";
																							} ?>><?php echo $tax['friendlyName']; ?> </option>

										<?php } ?>
									</select>

								</div>
							</div>
						</div>



					</fieldset>

					<fieldset>
						<legend> Plan Details</legend>
						<div class="form-group form-actions">
							<div class="col-sm-3">
								<div class="form-group"> <label class="control-label">Products & Services</label> </div>
							</div>
							<div class="col-sm-2">
								<div class="form-group"><label class="control-label">Recurring / One Time</label></div>
							</div>
							<div class="col-sm-2">
								<div class="form-group"><label class="control-label">Description </label></div>
							</div>
							<div class="col-sm-1">
								<div class="form-group"><label class="control-label">Price</label> </div>
							</div>
							<div class="col-sm-1">
								<div class="form-group"><label class="control-label">Quantity </label></div>
							</div>
							<div class="col-sm-1">
								<div class="set_taxes" style="display:none;">
									<div class="form-group"><label class="control-label">Tax </label></div>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group"><label class="control-label">Total</label></div>
							</div>

						</div>
						<?php
						if (isset($items) && !empty($items)) { ?>
							<input type="hidden" id="item-exist" value="1">
						<?php }else{ ?>
							<input type="hidden" id="item-exist" value="0">

						<?php } ?>
						<div id="item_fields">
							<?php
							if (isset($items) && !empty($items)) {
								foreach ($items as $k => $item) {
									$rate = $item['itemRate'];
									$qnty = $item['itemQuantity'];
									if (!empty($item['itemTax'])) {
										$tax  = $item['itemTax'];
										echo "<script> $('.set_taxes').css('display','block'); </script>";
									} else {
										$tax = 0;

										echo "<script> $('.set_taxes').css('display','none'); </script>";
									}

									$total_amt = ($rate * $qnty) + (($rate * $qnty) * $tax / 100);

							?>
									<div class="form-group removeclass<?php echo $k + 1; ?>">
										<div class="col-sm-3 nopadding">
											<div class="form-group">
												<select class="form-control select-chosen" onchange="select_plan_val('<?php echo $k + 1; ?>');" id="productID<?php echo $k + 1; ?>" name="productID[]">
													<option value="">Select Product & Service</option>

													<?php foreach ($plans as $plan) { ?>
														<option value="<?php echo $plan['ListID']; ?>" <?php if ($plan['ListID'] == $item['itemListID']) {
																											echo "selected";
																										} ?>>
															<?php echo $plan['FullName']; ?> </option> <?php }  ?>
												</select></div>
										</div>
										<div class="col-sm-2 nopadding">
											<div class="form-group ">
												<select class="form-control" id="onetime_charge<?php echo $k + 1; ?>" name="onetime_charge[]">
													<option>Select Product & Service</option>
													<option value="0" <?php if ($item['oneTimeCharge'] == '0') {
																			echo "selected";
																		} ?>>Recurring</option>
													<option value="1" <?php if ($item['oneTimeCharge'] == '1') {
																			echo "selected";
																		} ?>>One Time</option>
												</select></div>
										</div>
										<div class="col-sm-2 nopadding">
											<div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k + 1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" placeholder="Description "></div>
										</div>

										<div class="col-sm-1 nopadding">
											<div class="form-group">
												<input type="text" class="form-control float text-left" id="unit_rate<?php echo $k + 1; ?>" name="unit_rate[]" value="<?php echo number_format($item['itemRate'], 2, '.', ''); ?>" onblur="set_unit_val('<?php echo $k + 1; ?>');" placeholder="Price"></div>
										</div>

										<div class="col-sm-1 nopadding">
											<div class="form-group text-center"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val('<?php echo $k + 1; ?>');" id="quantity<?php echo $k + 1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>" placeholder="Qty"></div>
										</div>

										<div class="col-sm-1 nopadding">
											<div class="set_taxes" <?php if (empty($item['itemTax'])) echo "style=display:none"; ?>>
												<div class="form-group"> <input type="checkbox" id="tax_check<?php echo $k + 1; ?>" <?php if ($item['itemTax']) echo "checked"; ?> name="tax_check[]" class="tax_checked" onchange="set_tax_val(this, '<?php echo $k + 1; ?>')" value="<?php echo $item['itemTax']; ?>"></div>
											</div>
										</div>

										<div class="col-sm-2 nopadding">
											<div class="form-group">
												<div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k + 1; ?>" name="total[]" value="<?php echo sprintf('%0.2f', $total_amt); ?>" placeholder="Total">
													<div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k + 1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div>
												</div>
											</div>
										</div>
										<div class="clear"></div>
									</div>

								<?php		 }
							} else {
								?>
								
								<?php } ?>

						</div>


						<div class="col-md-12">
							<div class="form-group">

								<div class=" form-actions">
									<label class="control-label "></label>
									<div class="group-btn">

										<button class="btn btn-sm btn-success" type="button" onclick="item_fields();"> Add More </button>

										<label class="pull-right remove-hover"><strong>Total: $<span id="grand_total"><?php echo '0.00'; ?></span></strong> </label>
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-12">


							<div class="form-group col-sm-2">
								<div class="col-sm-12 no-pad">

									<label style="padding: 6px 0 0 0;" class=" control-label">Email Recurring Invoices</label>
								</div>
								<div class="col-sm-12 no-pad">
									<label class="radio-inline control-label">
										<input type="radio" name="email_recurring" <?php if (isset($subs) && $subs['emailRecurring'] == '1') {
																						echo "checked";
																					} ?> value="1">Yes</label>
									<label class="radio-inline control-label">
										<input type="radio" name="email_recurring" <?php if (!isset($subs) || $subs['emailRecurring'] == '0') echo "checked"; ?> value="0">No</label>

								</div>
							</div>
							<div class="form-group col-sm-2" id="automatic-payment">
								<div class="col-sm-12">

									<label style="padding: 6px 0 0 0;" class=" control-label">Automatic Payment</label>
								</div>
								<div class="col-sm-12">
									<label class="radio-inline control-label">
										<input type="radio" name="autopay" id="true-autopay" onclick="chk_payment(this.value);" <?php if (isset($subs) && $subs['automaticPayment'] == '1') {
																													echo "checked";
																												} ?> value="1" checked>Yes</label>
									<label class="radio-inline control-label">
										<input type="radio" name="autopay" id="false-autopay" onclick="chk_payment(this.value);" <?php if (isset($subs) && $subs['automaticPayment'] == '0') echo "checked"; ?> value="0">No</label>

								</div>
							</div>

							<div id="set_pay_data" <?php if (isset($subs) && $subs['automaticPayment'] == '0') { ?> style="display:none;" <?php } else if (isset($subs) && $subs['automaticPayment'] == '1') { ?> style="display:block;" <?php } else { ?> style="display:block;" <?php } ?>>
							<?php 
								if(!isset($defaultGateway) || !$defaultGateway){
									
							?>
								<div class="form-group col-sm-3">
									<div class="">
										<label class=" control-label" for="card_list">Gateway</label>
										<?php $selectedOptions = '';$isSelected=0; ;?>
										<select id="gateway_list" name="gateway_list" class="form-control">
											<option value="">Select Gateway</option>
											<?php foreach ($gateways as $gateway) { 
												
												if (isset($subs) && ($subs['paymentGateway'] == $gateway['gatewayID']) && $isSelected == 0) {
													$selectedOptions = 'selected';
													$isSelected = 1;
												} else if (isset($subs) && ($subs['paymentGateway'] == 0) && ($gateway['set_as_default'] == 1) && $isSelected == 0){
													$selectedOptions = 'selected';
													$isSelected = 1;
												} else if ($gateway['set_as_default'] == 1 && $isSelected == 0){
													$selectedOptions = 'selected';
													$isSelected = 1;
												}else{
													$selectedOptions = '';
												}
											?>
												<option value="<?php echo $gateway['gatewayID'];  ?>" <?php echo $selectedOptions;   ?>><?php echo $gateway['gatewayFriendlyName']; ?></option>
												<?php } ?>


										</select>
									</div>
								</div>

							<?php 
								} else { ?>
								<input type="hidden" name="gateway_list" value="<?php echo $defaultGateway['gatewayID'];  ?>">
							<?php }  ?>	
								<div class="form-group col-sm-3">
									<div class=" ">
										<label class=" control-label" for="card_list">Payment Method</label>
										<select id="card_list" name="card_list" class="form-control">
											<option value="">Select Payment Method</option>
											<option value="new1"><strong>New Card</strong></option>
											<?php if (isset($c_cards) && !empty($c_cards)) {
												foreach ($c_cards as $c_card) {
											?>
													<option value="<?php echo $c_card['CardID']; ?>" <?php if ($c_card['CardID'] == $subs['cardID']) echo "selected"; ?>><?php echo $c_card['customerCardfriendlyName']; ?></option>
											<?php }
											} ?>

										</select>

									</div>

								</div>
							</div>
						</div>

						<div class="col-sm-12 ">
							<div id="set_credit" style="display:none;">

								<div class="col-sm-3 form-group">
									<div>
										<label class="control-label">Credit Card Number</label>

										<input type="text" id="card_number" name="card_number" class="form-control" placeholder="Credit Card Number" autocomplete="off">
									</div>
								</div>

								<div class="col-sm-3 form-group">
									<div>
										<label class="control-label" for="friendlyname"> Card Friendly Name</label>

										<input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Card Friendly Name">
									</div>
								</div>

								<div class="col-sm-2 form-group">

									<label class="control-label" for="expry">Expiry Month </label>

									<select id="expiry" name="expiry" class="form-control">
										<option value="01">JAN</option>
										<option value="02">FEB</option>
										<option value="03">MAR</option>
										<option value="04">APR</option>
										<option value="05">MAY</option>
										<option value="06">JUN</option>
										<option value="07">JUL</option>
										<option value="08">AUG</option>
										<option value="09">SEP</option>
										<option value="10">OCT</option>
										<option value="11">NOV</option>
										<option value="12">DEC</option>
									</select>
								</div>

								<div class="col-sm-2 form-group">
									<div>
										<label class=" control-label" for="expry_year">Expiry Year</label>

										<select id="expiry_year" name="expiry_year" class="form-control">
											<?php
											$cruy = date('y');
											$dyear = $cruy + 25;
											for ($i = $cruy; $i < $dyear; $i++) {  ?>
												<option value="<?php echo "20" . $i;  ?>"><?php echo "20" . $i;  ?> </option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-sm-2 form-group">
									<div>
										<label class="control-label" for="cvv">Card Security Code (CVV)</label>

										<input type="text" id="cvv" name="cvv" class="form-control" placeholder="Security Code (CVV)" autocomplete="off">

									</div>
								</div>


							</div>

							<div id="set_eCheck" style="display:none;">

								<div class="col-sm-3 form-group">
									<div>
										<label class="control-label">Account Number</label>

										<input type="text" id="acc_number" name="acc_number" class="form-control" placeholder="Account Number">
									</div>
								</div>

								<div class="col-sm-3 form-group">
									<div>
										<label class="control-label" for="route_number"> Route Number</label>

										<input type="text" id="route_number" name="route_number" class="form-control" placeholder="Route Number">
									</div>
								</div>
								<div class="col-sm-2 form-group">
									<div>
										<label class="control-label" for="acc_name"> Account Name</label>

										<input type="text" id="acc_name" name="acc_name" class="form-control" placeholder="Account Name">
									</div>
								</div>

								<div class="col-sm-2 form-group">

									<label class="control-label" for="acct_type">Account Type</label>
									<select id="acct_type" name="acct_type" class="form-control" >

										<option value="checking">Checking</option>
										<option value="saving">Saving</option> 

									</select>
									
								</div>

								<div class="col-sm-2 form-group">
									<div>
										<label class=" control-label" for="acct_holder_type">Account Holder Type</label>

										<select id="acct_holder_type" name="acct_holder_type" class="form-control" >

											<option value="business">Business</option>
											<option value="personal">Personal</option>

										</select>
									</div>
								</div>
								


							</div>
						</div>



					</fieldset>


					<fieldset>
						<div id="set_bill_data">
							<legend>Billing Address</legend>

							<div class="col-md-12">
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 1</label>

										<input type="text" id="baddress1" name="baddress1" class="form-control " value="<?php if (isset($subs)) {
																															echo $subs['baddress1'];
																														} ?>" placeholder="Address Line 1">

									</div>
								</div>
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 2</label>

										<input type="text" id="baddress2" name="baddress2" class="form-control" value="<?php if (isset($subs)) {
																															echo $subs['baddress2'];
																														} ?>" placeholder="Address Line 2">

									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">City</label>

										<input type="text" id="bcity" name="bcity" class="form-control input-typeahead-city" value="<?php if (isset($subs)) {
																																	echo $subs['bcity'];
																																} ?>" autocomplete="off" placeholder="City">


									</div>
								</div>
								<div class="form-group col-sm-3">
									<div class=" ">

										<label class=" control-label" for="val_username">State/Province</label>

										<input type="text" id="bstate" name="bstate" class="form-control input-typeahead-state" value="<?php if (isset($subs)) {
																																		echo $subs['bstate'];
																																	} ?>" autocomplete="off" placeholder="State/Province">


									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">ZIP Code</label>

										<input type="text" id="bzipcode" name="bzipcode" class="form-control" value="<?php if (isset($subs)) {
																															echo $subs['bzipcode'];
																														} ?>" placeholder="ZIP code">


									</div>
								</div>
								<div class="col-sm-3 form-group">
									<div class="">



										<label class="control-label" for="example-typeahead">Country</label>

										<input type="text" id="bcountry" name="bcountry" class="form-control input-typeahead-country" value="<?php if (isset($subs)) {
																																			echo $subs['bcountry'];
																																		} ?>" autocomplete="off" placeholder="Country">


									</div>
								</div>
								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="phone">Phone Number</label>

										<input type="text" id="bphone" name="bphone" class="form-control" value="<?php if (isset($subs)) {
																														echo $subs['bphone'];
																													} ?>" placeholder="(XXX) XXX-XXXX">


									</div>
								</div>

							</div>
						</div>
					</fieldset>
					<fieldset>
						<div>
							<legend>Shipping Address</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input type="checkbox" id="chk_add_copy"> Copy from Billing Address
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 1</label>

										<input type="text" id="address1" name="address1" class="form-control " value="<?php if (isset($subs)) echo $subs['address1']; ?>" placeholder="Address Line 1">

									</div>
								</div>
								<div class="col-sm-12 form-group">
									<div class="">
										<label class=" control-label" for="val_username">Address Line 2</label>

										<input type="text" id="address2" name="address2" class="form-control " value="<?php if (isset($subs)) echo $subs['address2']; ?>" placeholder="Address Line 2">

									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">City</label>

										<input type="text" id="city" name="city" class="form-control input-typeahead-city" autocomplete="off" value="<?php if (isset($subs)) echo $subs['city']; ?>" placeholder="City">


									</div>
								</div>
								<div class="form-group col-sm-3">
									<div class=" ">

										<label class=" control-label" for="val_username">State/Province</label>

										<input type="text" id="state" name="state" class="form-control input-typeahead-state" value="<?php if (isset($subs)) echo $subs['state']; ?>" autocomplete="off" placeholder="State/Province">


									</div>
								</div>

								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="val_username">ZIP Code</label>

										<input type="text" id="zipcode" name="zipcode" class="form-control" value="<?php if (isset($subs)) echo $subs['zipcode']; ?>" placeholder="ZIP Code">


									</div>
								</div>
								<div class="col-sm-3 form-group">
									<div class="">


										<label class="control-label" for="example-typeahead">Country</label>

										<input type="text" id="country" name="country" class="form-control input-typeahead-country" autocomplete="off" value="<?php if (isset($subs)) echo $subs['country'];
																																						else echo "United States"; ?>" placeholder="Country">


									</div>
								</div>
								<div class="form-group col-sm-2">
									<div class="">

										<label class=" control-label" for="phone">Phone Number</label>

										<input type="text" id="phone" name="phone" class="form-control" value="<?php if (isset($subs)) echo $subs['contactNumber']; ?>" placeholder="(XXX) XXX-XXXX">


									</div>
								</div>

							</div>
						</div>
						<div class="form-group pull-right">
							<div class="col-md-12">

								<button type="submit" class="submit btn btn-sm btn-success">Save</button>
								<a href="<?php echo base_url(); ?>company/SettingSubscription/subscriptions" class=" btn btn-sm btn-primary1">Cancel</a>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(JS); ?>/pages/chargezoom_subscription.js"></script>

<script type="text/javascript">
	var gatewayID = $('#gateway_list').val();
	var cid = $('#customerID').val();
	var cardID = $('#cardID').val();
	if(gatewayID != ''){
		$('#card_list').find('option').not(':first').remove();
		$.ajax({
			type: "POST",
			url: base_url + "company/Payments/check_vault",
			data: { 'customerID': cid,'gatewayID': gatewayID },
			success: function (response) {

				data = $.parseJSON(response);
				if (data['status'] == 'success') {

					var s = $('#card_list');
					var card1 = data['card'];
					var cardTypeOption = data['cardTypeOption'];
					if(cardTypeOption == 1){
			            $(s).append('<option value="new1">New Card</option>');
			            $(s).append('<option value="new2">New eCheck</option>');
			        }else if(cardTypeOption == 2){
			            $(s).append('<option value="new1">New Card</option>');
			        }else if(cardTypeOption == 3){
			            $(s).append('<option value="new2">New eCheck</option>');
			        }else if(cardTypeOption == 4){
			            
			        }else{
			           
			        }
					for (var val in card1) {
						if(cardID == card1[val]['CardID']){
							$("<option />", { value: card1[val]['CardID'],selected:true, text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
						}else{
							$("<option />", { value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
						}
						
					}
					$('#baddress1').val(data['BillingAddress_Addr1']);
					$('#baddress2').val(data['BillingAddress_Addr2']);
					$('#bcity').val(data['BillingAddress_City']);
					$('#bstate').val(data['BillingAddress_State']);
					$('#bzipcode').val(data['BillingAddress_PostalCode']);
					$('#bcountry').val(data['BillingAddress_Country']);
					$('#bphone').val(data['Phone']);
					$('#email').val(data['Contact']);
					$('#address1').val(data['ShipAddress_Addr1']);
					$('#address2').val(data['ShipAddress_Addr2']);
					$('#city').val(data['ShipAddress_City']);
					$('#state').val(data['ShipAddress_State']);
					$('#zipcode').val(data['ShipAddress_PostalCode']);
					$('#country').val(data['ShipAddress_Country']);
					$('#phone').val(data['Phone']);
					if ($('#chk_add_copy').is(':checked')) {

						$('#address1').val($('#baddress1').val());
						$('#address2').val($('#baddress2').val());
						$('#city').val($('#bcity').val());
						$('#state').val($('#bstate').val());
						$('#zipcode').val($('#bzipcode').val());
						$('#country').val($('#bcountry').val());
						$('#phone').val($('#bphone').val());


					}




				}

			}


		});
	}

	$('#gateway_list').change(function () {
		var gatewayID = $(this).val();
		var cid = $('#customerID').val();
		if(gatewayID != ''){
			$('#card_list').find('option').not(':first').remove();
			$.ajax({
				type: "POST",
				url: base_url + "company/Payments/check_vault",
				data: { 'customerID': cid,'gatewayID': gatewayID },
				success: function (response) {

					data = $.parseJSON(response);
					if (data['status'] == 'success') {

						var s = $('#card_list');
						var card1 = data['card'];
						var cardTypeOption = data['cardTypeOption'];
						if(cardTypeOption == 1){
				            $(s).append('<option value="new1">New Card</option>');
				            $(s).append('<option value="new2">New eCheck</option>');
				        }else if(cardTypeOption == 2){
				            $(s).append('<option value="new1">New Card</option>');
				        }else if(cardTypeOption == 3){
				            $(s).append('<option value="new2">New eCheck</option>');
				        }else if(cardTypeOption == 4){
				            
				        }else{
				           
				        }
						for (var val in card1) {

							$("<option />", { value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
						}
						$('#baddress1').val(data['BillingAddress_Addr1']);
						$('#baddress2').val(data['BillingAddress_Addr2']);
						$('#bcity').val(data['BillingAddress_City']);
						$('#bstate').val(data['BillingAddress_State']);
						$('#bzipcode').val(data['BillingAddress_PostalCode']);
						$('#bcountry').val(data['BillingAddress_Country']);
						$('#bphone').val(data['Phone']);
						$('#email').val(data['Contact']);
						$('#address1').val(data['ShipAddress_Addr1']);
						$('#address2').val(data['ShipAddress_Addr2']);
						$('#city').val(data['ShipAddress_City']);
						$('#state').val(data['ShipAddress_State']);
						$('#zipcode').val(data['ShipAddress_PostalCode']);
						$('#country').val(data['ShipAddress_Country']);
						$('#phone').val(data['Phone']);
						if ($('#chk_add_copy').is(':checked')) {

							$('#address1').val($('#baddress1').val());
							$('#address2').val($('#baddress2').val());
							$('#city').val($('#bcity').val());
							$('#state').val($('#bstate').val());
							$('#zipcode').val($('#bzipcode').val());
							$('#country').val($('#bcountry').val());
							$('#phone').val($('#bphone').val());


						}




					}

				}


			});
		}
	});
	
</script>