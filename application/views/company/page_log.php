<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <span id="flashdata"> 
        <?php echo $this->session->flashdata('message');   ?>
    </span>
    <legend class="leg">Sync Log</legend>
    <!-- All Orders Block -->
    <div class="block-main full" style="position: relative;">
        
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">ID</th>
                    <th class="text-right ">Data</th>
                    
                    <th class="hidden-xs hidden-sm text-right">Status</th>
                    <th class="text-right hidden-xs">Time</th>
                 
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($alls) && $alls)
				{
					foreach($alls as $k=>$all)
					{
                       if($all['c_type']=='Invoice')
                       {
                     $align='  text-right';
                       }else{
                        $align='  text-left';
                       }
				?>
				<tr>
					
				
				
			
					
					<td class="text-left "><?php echo"#".$all['ID'];  ?></td>
				
					
                    <td class="hidden-sm text-right "><?php echo $all['qb_action'];  ?></td>
					
						
					
                    <td class="text-right hidden-xs"><?php 
                    if($all['qb_status']==0 || $all['qb_status']==1 || $all['qb_status']==2 ){  echo "Not Synced"; }
                     if($all['qb_status']==3 ){ echo "Error"; }
                     if($all['qb_status']==5 ){ echo "Synced"; }
                    
                 
                    if($all['qb_status']=='3' ){    ?> 
                          <input type="button" name="snk" id="snk_btn" class="btn btn-warning btn-sm" value="Sync Again"  onclick="set_seenk_data('<?php echo $all['ID'];  ?>','<?php echo $all['c_type'];  ?>');"   />
                          <?php }  ?>
                    </td>
					<td class="text-right "><?php echo date('M d, Y H:i A', strtotime($all['time']));  ?></td>
				</tr>
				
				<?php } }else{ echo'<tr>
                <td colspan="5"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ 
Pagination_view111.init(); });



var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#ecom-orders111').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [-1] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script type="text/javascript">
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 3000); //call fade in 2 seconds
 })

function fadeMyDiv() {
   // $("#flashdata").fadeOut('slow');
}


function set_seenk_data(tid='',  cltype='')
{

$("#snk_form").remove();
var f = document.createElement("form");
f.setAttribute('method',"post");

f.setAttribute('id',"snk_form");

if(cltype=='Customer'){
f.setAttribute('action',"<?php echo base_url(); ?>MerchantUser/sync_customer");

var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"cust_id");
i.setAttribute('value', tid);

}
if(cltype=='Invoice'){
f.setAttribute('action',"<?php echo base_url(); ?>SettingSubscription/sync_invoice");
var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"inv_id");
i.setAttribute('value', tid);
}
if(cltype=='Product'){
f.setAttribute('action',"<?php echo base_url(); ?>MerchantUser/sync_product");
var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"prod_id");
i.setAttribute('value', tid);
}

f.appendChild(i);


//and some more input elements here
//and dont forget to add a submit button

document.getElementsByTagName('body')[0].appendChild(f);

$('#snk_form').submit();

}
</script>
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>


</div>
<!-- END Page Content -->