                <!-- Page content -->
                <?php
	$this->load->view('alert');
?>
<div id="page-content"> 
        
            <!-- Form Validation Example Block -->
			<div class="msg_data ">
			    <?php echo $this->session->flashdata('message');   ?>
			</div>
							   <!-- Forms General Header -->
     
    <!-- END Forms General Header -->
    <legend class="leg">Refunded Transactions</legend>
	<div class="full">
	    <input type="hidden" id="countTxn" value="<?php echo count($transactions); ?>">
        <!-- Form Validation Example Title -->
        <table id="refund_page" class="table table-bordered dataTable  table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">Customer Name</th>
                   
                    <th class="text-left">Amount</th>
					

                    <th class="text-right hidden-xs">Date</th>
					<th class="text-right hidden-xs">Gateway</th>
					<th class="text-right  hidden-xs">Transaction ID</th>
                    <th class=" text-right">Status</th>

                </tr>
            </thead>
            <tbody>
			
    			<?php 
    			if(isset($transactions) && $transactions)
    			{
					
					
    				foreach($transactions as $transaction)
    				{

    			?>
				<tr>
					<?php if($plantype) { ?>
					<td class="text-left"><?php echo $transaction['FullName']; ?></td>
				     <?php } else { ?>
				    	<td class="text-left cust_view"><a href="<?php echo base_url(); ?>home/view_customer/<?php echo $transaction['customerListID']; ?>"  ><?php echo $transaction['FullName']; ?></a></td>	

				    <?php } ?>
                    <?php if($transaction['transactionID'] == ''){ ?>
                        <td class="hidden-xs text-right ">$<?php echo ($transaction['transactionAmount'])?number_format($transaction['transactionAmount'], 2):'0.00'; ?></td>

                    <?php }else{ ?>
                        <td class="hidden-xs text-right cust_view"><a href="#pay_data_process"   onclick="set_company_payment_transaction_data('<?php  echo $transaction['transactionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$<?php echo ($transaction['transactionAmount'])?number_format($transaction['transactionAmount'], 2):'0.00'; ?></a></td>
                    <?php } ?> 	
					
				
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo getGatewayNames($transaction['transactionGateway']); ?></td>
					<td class="text-right  hidden-xs"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'';   ?></td>
					<?php if( in_array( $transaction['transactionCode'], array('100','200','111','1'))){ ?>
					<td class="text-left "><span style="color:#888888;"><?php echo "Success"; ?></span></td>
					<?php }else{ ?>
						<td class="text-left"><span style="color:#888888;">
						<?php echo "Failed"; ?></span></td>
				    <?php } ?>	
				    
				</tr>
				<?php } } 
				else { echo'<tr><td colspan="6"> No Records Found </td></tr>'; }  	
				?>
			</tbody>
        </table>
    </div>
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
    <script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            if($('#countTxn').val() > 0){
                $('#refund_page').dataTable({
                    columnDefs: [
                        { type: 'date-custom', targets: [3] },
                        { orderable: false, targets: [] }
                    ],
                    order: [[ 3, "desc" ]],
                    pageLength: 10,
                    lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
                });
            }
            

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

	<script>
    	$(function(){ 
         });
    	
    	function set_refund_pay(txnid, txntype){
    		if(txnid !=""){
    		    $('#txnID').val(txnid);	  
    		    $('#txnIDrefund').val(txnid);
				$('#paytxnID').val(txnid);
    		}
    	}   
	
	</script>
	
	
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
 
	
	  <div id="paytrace_payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>PaytracePayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Yes" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                                <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                            <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
	

    <div id="auth_payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>AuthPayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Yes" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                                <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                            <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>

    <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>Payments/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Yes" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                                <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                            <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
</div>
