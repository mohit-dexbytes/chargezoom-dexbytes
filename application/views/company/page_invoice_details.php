<?php

$status = '';
$today = date('Y-m-d');
$due_date = date('Y-m-d', strtotime($invoice_data['DueDate']));
if ($due_date < $today && $invoice_data['IsPaid'] == 'false') {
    $status = 'Pastdue';
}
$type = '';
$date1 = strtotime($due_date);
$date2 = strtotime($today);

$type_text = '';

if ($date1 >= $date2) {
    $diff = $date2 - $date1;
    $diff = floor($diff / (60 * 60 * 24));
    $type = 3;
    $type_text = "Due Amount";
} else {
    $type = 2;
    $type_text = "Invoice Past Due";
}

if ($this->session->userdata('logged_in')) {
    $merchantEmail = $this->session->userdata('logged_in')['merchantEmail'];
}
if ($this->session->userdata('user_logged_in')) {
    $merchantEmail = $this->session->userdata('user_logged_in')['userEmail'];
}

if ($invoice_data['BalanceRemaining'] > 0) {
    $displayStatus = 1;
    $readonly = '';
}else{
    $displayStatus = 0;
    $readonly = 'readonly';
}
?>
<?php
	$this->load->view('alert');
?>
<style>
.block-options{
    margin-top: 10px;
}
</style>
<div id="page-content">






    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Products Block -->
    <legend class="leg">Invoice Details</legend>
    <div class="block">
        <!-- Products Title -->
       
            <div class="block-options pull-right">
                <a href="<?php echo base_url(); ?>company/home/invoice_details_print/<?php echo  $invoice_data['TxnID']; ?>" class="btn btn-alt btn-sm btn-danger" data-toggle="tooltip" title="">Download PDF</a>

                <input type="hidden" name="taxes" id="taxes" value="<?php echo $invoice_data['TaxListID']; ?>" />
                <input type="hidden" name="tax_rate" id="tax_rate" value="<?php echo $invoice_data['TaxRate']; ?>" />
                <?php if ($invoice_data['BalanceRemaining'] == "0.00" and $invoice_data['IsPaid'] == 'true') {  ?>
                    <a href="javascript:void(0);" title="Send Email" disabled class="btn btn-sm btn-info" data-backdrop="static" data-keyboard="false" data-toggle="modal">Email Invoice</a>
                <?php } else {   ?>
                    <a href="#set_tempemail_data" title="Send Email" onclick="set_template_data_temp('<?php echo  $invoice_data['TxnID']; ?>','<?php echo  $invoice_data['Customer_ListID']; ?>','<?php echo $type; ?>');" class="btn btn-sm btn-info" data-backdrop="static" data-keyboard="false" data-toggle="modal">Email Invoice</a>

                <?php } ?>

                <div class="btn-group dropbtn">
                    <?php if ($invoice_data['BalanceRemaining'] == "0.00" and $invoice_data['IsPaid'] == 'true') {  ?>
                        <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                        <ul class="dropdown-menu text-left">
                            <li>
                                <a href="javascript:void(0);" id="txnRefund<?php echo $invoice_data["TxnID"]; ?>" invoice-id="<?php echo $invoice_data["TxnID"]; ?>" integration-type="5" data-url="<?php echo base_url(); ?>ajaxRequest/getTotalRefundOfInvoice" class="refunTotalInvoiceAmountCustom">Refund</a>
                            </li>
                    </ul>
                    <?php } else { ?> <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                    <ul class="dropdown-menu text-left">
                        
                        <li> <a href="#invoice_schedule" class="" onclick="set_invoice_schedule_id('<?php echo $invoice_data['TxnID']; ?>','<?php echo $invoice_data['Customer_ListID']; ?>','<?php echo $invoice_data['BalanceRemaining']; ?>','<?php echo date('F d Y', strtotime($invoice_data['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>

                        <li> <a href="#set_subs" class="" onclick="set_sub_status_id('<?php echo $invoice_data['TxnID']; ?>', '<?php echo $invoice_data['Customer_ListID']; ?>','<?php echo $invoice_data['BalanceRemaining']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>
                        <?php if ($invoice_data['AppliedAmount'] == 0 || $invoice_data['AppliedAmount'] == '0.00') {  ?>
                            <li><a href="#invoice_cancel" onclick="set_company_invoice_id('<?php echo $invoice_data['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>
                        <?php } } ?>

                    </ul>
                </div>
            </div>




        <form id="form-validation" action="<?php echo base_url(); ?>company/SettingSubscription/edit_custom_invoice" method="post" enctype="multipart/form-data" class="form-horizontal ">


            <h4>Invoice Number: <strong><?php echo  $invoice_data['RefNumber']; ?></strong></h4>
            <h5>Customer Name: <strong><?php echo  $invoice_data['Customer_FullName']; ?></strong></h5>
            <h5>Invoice Date: <div class="input-group input-date col-md-3">

                    <input type="text" id="invDate" name="invDate" class="form-control input-datepicker" value="<?php echo date('m/d/Y', strtotime($invoice_data['TimeCreated'])); ?>" data-date-format="mm/dd/yyyy" placeholder="Invoice Date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </h5>
            <h5>Invoice Due Date: <div class="input-group input-date cust_view col-md-3">

                    <input type="text" id="dueDate" name="dueDate" class="form-control input-datepicker" value="<?php echo date('m/d/Y', strtotime($invoice_data['DueDate'])); ?>" data-date-format="mm/dd/yyyy" placeholder="Invoice Due Date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </h5>

            <h5>Invoice Status: <strong> <?php if ($invoice_data['BalanceRemaining'] == "0.00" and $invoice_data['IsPaid'] == 'true') {  ?> Paid <?php } else
                  if ($invoice_data['IsPaid'] == 'false' && ($invoice_data['BalanceRemaining'] != "0.00" and  $invoice_data['AppliedAmount'] == "0.00")) {    ?> Unpaid <?php } else
                 if (($invoice_data['BalanceRemaining'] != "0.00" and $invoice_data['AppliedAmount'] != "0.00") && $invoice_data['IsPaid'] == 'false') {  ?> Partial <?php } ?></strong></h5>

            <h5>Payment Method: <strong>
                    <?php
                        if(!empty($transaction['transactionType']) && $transaction['transactionType'] =='Offline Payment'){
                            echo 'Check'; 
                        }else if((isset($transaction['paymentType']) && $transaction['paymentType'] == 2  )){ 
                            echo 'Electronic Check';
                        }else if(empty($transaction['transactionType']  )){ 
                            echo '--';
                        }else{ 
                            echo'Credit Card';
                        }   
                    ?>
                </strong></h5>
            <?php if($ur_data){ ?>
                <h5>Preview Invoice Page: <strong class="cust_view"><?php echo '<a target="_blank" href="' . $paylink . '">' . $paylink . '</a>'; ?></strong></h5>
        
            <?php }else{ ?>
                <h5>Preview Invoice Page: <strong class="cust_view"><?php echo '<a href="javascript:void(0);" rel="txtTooltip" data-toggle="tooltip" title="To activate this page, you must enable the Customer Portal for your account.">' . $paylink . '</a>'; ?></strong></h5>
            <?php } ?>    
            <div class="table-responsive">
                <table class="table table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th>Product / Service</th>
                            <th class="text-left"> Description</th>
                            <th class="text-right">Unit Rate</th>
                            <th class="text-right">Qty</th>

                            <th class="text-right">Amount</th>

                        </tr>
                    </thead>
                    <tbody id="item_fields">
                        <?php


                        $totaltax = 0;
                        $total = 0;
                        $tax = 0;


                        foreach ($invoice_items as $key => $item) {

                        ?>

                            <tr class="removeclass<?php echo $key + 1; ?>  rd ">
                                <td>
                                    <select class="form-control" onchange="select_plan_val('<?php echo $key + 1; ?>');" id="productID<?php echo $key + 1; ?>" name="productID[]">

                                        <?php foreach ($plans as $plan) {
                                            $sel = "";
                                            if ($plan['ListID'] == $item['Item_ListID'])  $sel = "selected"; ?>
                                            <option value="<?php echo $plan['ListID']; ?>" <?php echo $sel; ?>>
                                                <?php echo $plan['FullName']; ?> </option> <?php }  ?>
                                    </select>
                                </td>
                                <td>
                                    <?php 
                                        $descrip_readonly = '';
                                        if($item['Descrip'] == 'Surcharge Fees'){
                                            $descrip_readonly = 'readonly';
                                        } 
                                    ?>
                                    <input <?php echo $readonly; ?> type="text" id="description<?php echo $key + 1; ?>" name="description[]" value="<?php echo $item['Descrip']; ?>" class="form-control" <?php echo $descrip_readonly; ?> /> 
                                </td>
                                    
                                    
                                <td class="text-right">$<span id="rate<?php echo $key + 1; ?>"> <?php echo number_format($item['Rate'], 2); ?> </span> <input type="hidden" id="unit_rate<?php echo $key + 1; ?>" name="unit_rate[]" value="<?php echo $item['Rate']; ?>" /></td>
                                <td class="text-right">
                                    <div class="input-group pull-right"> 
                                        
                                        <input <?php echo $readonly; ?> type="text" size="6" onkeypress="return isNumberKey(event)" class="form-control text-right" maxlength="4" onblur="set_qty_val('<?php echo $key + 1; ?>');" id="quantity<?php echo $key + 1; ?>" name="quantity[]" value="<?php echo $item['Quantity']; ?>" class="form-control" placeholder="Qty">
                                        
                                    </div>
                                </td>
                                <td class="text-right"><?php $total +=  $item['Quantity'] * $item['Rate'];
                                                        echo '<input type="hidden" class="form-control total_val" id="total' . ($key + 1) . '" name="total[]" value="' . ($item['Quantity'] * $item['Rate']) . '"/>';  ?> $<?php echo '<span id="total11' . ($key + 1) . '" >' . number_format($item['Quantity'] * $item['Rate'], 2) . '</span>'; ?> </td>



                            </tr>
                        <?php } ?>


                        <tr class="active">
                            <td> 
                                <?php if($displayStatus){ ?>
                                    <button class="btn btn-sm btn-success" type="button" onclick="item_invoice_fields();"> Add More </button>
                                <?php } ?>
                                
                            </td>
                            <td colspan="3" class="text-right text-uppercase"><strong>SUBTOTAL</strong></td>
                            <td class="text-right">$
                                <?php 
                                    $balance = $invoice_data['BalanceRemaining'] + abs($invoice_data['AppliedAmount']);
                                  
                                    echo '<span id="sub_total" >' . number_format(($balance - $invoice_data['TotalTax']), 2) . '</span>';
                                ?>  
                            </td>

                        </tr>

                        <tr>
                            <td class="text-left"><select name="taxes" id="taxesee" onchange="set_tax_valnew();" class="form-control ">
                                    <option value="">Select Tax</option>
                                    <?php foreach ($taxes as $tax) { ?>
                                        <option value="<?php echo $tax['taxID']; ?>" <?php if (isset($invoice_data) && $tax['taxListID'] == $invoice_data['TaxListID']) {
                                                                                            echo "selected";
                                                                                        } ?>><?php echo $tax['friendlyName']; ?> </option>

                                    <?php } ?>
                                </select> </td>
                            <td colspan="3" class="text-right text-uppercase"><strong>TAX (<?php echo $invoice_data['TaxRate']; ?>%) </strong></td>
                            <td class="text-right"><?php echo '$' . '<span id="tax_val" >' . number_format($invoice_data['TotalTax'], 2) . '</span>'; ?></td>
                        </tr>
                        <tr class="info">
                            <td colspan="4" class="text-right text-uppercase"><strong>TOTAL</strong></td>
                            <td class="text-right">$
                                <?php 
                                   
                                    echo '<span id="total_amt">' . number_format(($balance), 2) . '</span>';
                                ?>
                            </td>
                        </tr>

                        <tr class="success">
                            <td colspan="4" class="text-right text-uppercase"><strong>PAID</strong></td>
                            <td class="text-right">$<?php echo number_format((-$invoice_data['AppliedAmount']), 2);  ?></td>
                        </tr>

                        <tr class="danger">
                            <td colspan="4" class="text-right text-uppercase"><strong>BALANCE</strong></td>
                            <td class="text-right"><strong>$<?php $total = ($invoice_data['BalanceRemaining']) ? $invoice_data['BalanceRemaining'] : '0.00';
                                                            echo '<span id="grand_total">' . $total . '</span>'; ?></strong></td>
                        </tr>
                    </tbody>

                </table>
            </div>
            <input type="hidden" id="txID" id="txID" value="<?php echo $invoice_data['TaxRate']; ?>" />
            <div class="col-md-12">

                <div class="pull-right">
                    <?php if ($invoice_data['BalanceRemaining'] != '0.00') { ?>
                        <a href="#invoice_process" class="btn btn-sm btn-success" onclick="set_invoice_process_id('<?php echo $invoice_data['TxnID']; ?>', '<?php echo $invoice_data['Customer_ListID']; ?>','<?php echo $invoice_data['BalanceRemaining']; ?>',3);" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
                        <?php $dis = '';
                        if ($invoice_data['IsPaid'] == 'false' && ($invoice_data['BalanceRemaining']) > 0 &&  (-$invoice_data['AppliedAmount']) > 0) {
                            $dis = 'disabled';
                        }
                        if ($invoice_data['IsPaid'] == 'true') {
                            $dis = 'disabled';
                        }

                        ?>
                        <input type="button" name="inv_exit" class="btn  testbtn btn-sm btn-primary" <?php echo $dis; ?> value="Save & Exit" />
                        <input type="button" name="inv_save" class="btn testbtn btn-sm btn-primary" <?php echo $dis; ?> value="Save" />

                    <?php }   ?><br><br>
                </div>

            </div>


            <input type="hidden" id="invNo" name="invNo" value="<?php echo $invoice_data['TxnID']; ?>" />
        </form>

        <div>
        </div>
        <?php  
            $BillingAdd = 0;
            $ShippingAdd = 0;

            $isAdd = 0;
            if($invoice_data['Billing_Addr1']  || $invoice_data['Billing_Addr2'] || $invoice_data['Billing_City'] || $invoice_data['Billing_State'] || $invoice_data['Billing_PostalCode'] || $invoice_data['Billing_Country']){
                $BillingAdd = 1;
                $isAdd = 1;
            }
            if($invoice_data['ShipAddress_Addr1']  || $invoice_data['ShipAddress_Addr2'] || $invoice_data['ShipAddress_City'] || $invoice_data['ShipAddress_State'] || $invoice_data['ShipAddress_PostalCode'] || $invoice_data['ShipAddress_Country'] ){
                $ShippingAdd = 1;
                $isAdd = 1;
            }

        ?>
        
        <!-- Addresses -->
        <div class="row">
        <?php if($isAdd){ ?>
            <?php if($BillingAdd){ ?>
            <div class="col-sm-6">
                <!-- Billing Address Block -->
                <div class="block">
                    <!-- Billing Address Title -->
                    <div class="block-title">
                        <h2><strong>Billing</strong> Address</h2>
                    </div>


                    <h4><strong>
                            <?php if ($customer_data['FirstName'] != '') {
                                echo $customer_data['FirstName'] . ' ' . $customer_data['LastName'];
                            } else {
                                echo $invoice_data['Customer_FullName'];
                            }
                            ?>
                        </strong></h4>
                    <address>
                        
                        <?php echo ($invoice_data['Billing_Addr1']) ? $invoice_data['Billing_Addr1'] . '<br>' : ''; ?>
                        <?php echo ($invoice_data['Billing_Addr2']) ? $invoice_data['Billing_Addr2'] . ',<br>' : ''; ?>

                        <?php echo ($invoice_data['Billing_City']) ? $invoice_data['Billing_City'] . ',' : ''; ?>
                        <?php echo ($invoice_data['Billing_State']) ? $invoice_data['Billing_State'] : ''; ?>
                        <?php echo ($invoice_data['Billing_PostalCode']) ? $invoice_data['Billing_PostalCode']. '<br>' : ''; ?>
                        <?php echo ($invoice_data['Billing_Country']) ? $invoice_data['Billing_Country']. '<br>' : ''; ?> 
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Billing Address Content -->
                </div>
                <!-- END Billing Address Block -->
            </div>
            <?php }
            if($ShippingAdd){ ?>
            <div class="col-sm-6">
                <!-- Shipping Address Block -->
                <div class="block">
                    <!-- Shipping Address Title -->
                    <div class="block-title">
                        <h2><strong>Shipping </strong> Address</h2>
                    </div>
                    <!-- END Shipping Address Title -->

                    <h4><strong> <?php if ($customer_data['FirstName'] != '') {
                                        echo $customer_data['FirstName'] . ' ' . $customer_data['LastName'];
                                    } else {
                                        echo $invoice_data['Customer_FullName'];
                                    }
                                    ?></strong></h4>
                    <address>
                        
                        <?php echo ($invoice_data['ShipAddress_Addr1']) ? $invoice_data['ShipAddress_Addr1']. '<br>,' : ''; ?>
                        <?php echo ($invoice_data['ShipAddress_Addr2']) ? $invoice_data['ShipAddress_Addr2'] . '<br>' : ''; ?>

                        <?php echo ($invoice_data['ShipAddress_City']) ? $invoice_data['ShipAddress_City'] . ',' : ''; ?>
                        <?php echo ($invoice_data['ShipAddress_State']) ? $invoice_data['ShipAddress_State'] : ''; ?>
                        <?php echo ($invoice_data['ShipAddress_PostalCode']) ? $invoice_data['ShipAddress_PostalCode']. '<br>' : ''; ?> 
                        <?php echo ($invoice_data['ShipAddress_Country']) ? $invoice_data['ShipAddress_Country']. '<br>' : ''; ?> 
                       
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Shipping Address Content -->
                </div>
                <!-- END Shipping Address Block -->
            </div>
            <?php } ?>
        <?php } ?>
        </div>
        
        <!-- END Addresses -->

        <!-- Log Block -->
        <div class="block full">
            <!-- Private Notes Title -->
            <div class="block-title">
                <h2><strong>Private</strong> Notes</h2>
            </div>
            <!-- END Private Notes Title -->

            <!-- Private Notes Content -->
            <div class="alert alert-info">
                <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
            </div>
            <form method="post" id="pri_form" onsubmit="return false;">
                <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="Your note.."></textarea>
                <input type="hidden" name="customerID" id="customerID" value="<?php echo $customer_data['ListID']; ?>" />
                <br>
                <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
            </form>
            <hr>

            <?php
            if (!empty($notes)) {
                foreach ($notes as $note) { 
                    if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                        $timezone = ['time' => $note['privateNoteDate'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                        $note['privateNoteDate'] = getTimeBySelectedTimezone($timezone);
                    }
                ?>

                    <div>
                        <?php echo $note['privateNote']; ?>
                    </div>
                    <div class="pull-right">
                        <span>Added on <strong><?php echo date('M d, Y - h:i A', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
                        <span><a href="javascript:void(0)" onclick="delele_notes('<?php echo $note['noteID']; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
                    </div>
                    <br>
                    <hr>
            <?php }
            }
            ?>


            <br>
            <!-- END Private Notes Content -->
        </div>
        <!-- END Log Block -->
    </div>


    <script src="<?php echo base_url(JS); ?>/pages/customer_details_company.js"></script>
    <script>
        $('.testbtn').click(function() {
            var form_data = $('#form-validation').serialize();
            var index = '';
            if ($(this).val() == 'Save') {
                index = "self";
            } else {
                index = "other";
            }
            $('#index').remove();


            $('<input>', {
                'type': 'hidden',
                'id': 'index',
                'name': 'index',

                'value': index,
            }).appendTo($('#form-validation'));

            $('#form-validation').submit();

        });
    </script>
    <div id="set_tempemail_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->

                <div class="modal-header ">
                    <h2 class="modal-title text-center">Send Email</h2>



                </div>

                <div class="modal-body">
                    <div id="data_form_template">
                        <label class="label-control" id="template_name"> </label>

                        <form id="form-validation1" action="<?php echo base_url(); ?>company/Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">

                            <input type="hidden" id="invoicetempID" name="invoicetempID" value="">
                            <input type="hidden" id="customertempID" name="customertempID" value="">
                            <input type="hidden" id="invoiceCode" name="invoiceCode" value="">
                            <input type="hidden" id="sendmailbyinvdtl" name="sendmailbyinvdtl" value="1">
                            
                            <input type="hidden" name="type" id="type" value="<?php echo $type ?>" />
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="type">Template</label>
                                <div class="col-md-7">
                                    <input type="text" name="type_text" class="form-control" readonly='readonly' id="type_text" value="<?php echo $type_text ?>" />


                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label" for="templteName">To Email</label>
                                <div class="col-md-7">
                                    <input type="text" id="toEmail" name="toEmail" value="" class="form-control" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group" style="display:none;" id="reply_div">
+                               <label class="col-md-3 control-label" for="replyEmail">Reply-To Email</label>
                                <div class="col-md-7">
                                    <input type="text" id="replyEmail" name="replyEmail" class="form-control" value="<?php if (isset($templatedata) && $templatedata['replyTo'] != ''){
                                                echo $templatedata['replyTo'];
                                            }else{  echo $merchantEmail;} ?>" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group" style="display:none" id="from_email_div">
                                <label class="col-md-3 control-label" for="templteName">From Email</label>
                                <div class="col-md-7">
                                    <input type="text" id="fromEmail" name="fromEmail"  value="<?php  if(isset($from_mail) && $from_mail != '') echo $from_mail ? $from_mail : 'donotreply@payportal.com'; ?>" class="form-control" placeholder="From Email">
                                </div>
                            </div>
                            <div class="form-group" id='display_name_div' style='display:none'>
                                <label class="col-md-3 control-label" for="templteName">Display Name</label>
                                <div class="col-md-7">
                                    <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="<?php echo $mailDisplayName; ?>" placeholder="Display Name">
                                </div>
                            </div>

                            <div class="form-group" style="font-size: 12px;">
                                <label class="col-md-3 control-label" for="templteName"></label>
                                <div class="col-md-7">
                                    <a href="javascript:void(0);" id="open_reply">Add Reply-To<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                    <a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                    <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                    <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                                    
                                </div>
                            </div>

                            <div class="form-group" id="cc_div" style="display:none">
                                <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                                <div class="col-md-7">
                                    <input type="text" id="ccEmail" name="ccEmail" value="<?php if (isset($templatedata)) echo ($templatedata['addCC']) ? $templatedata['addCC'] : ''; ?>" class="form-control" placeholder="CC Email">
                                </div>
                            </div>
                            <div class="form-group" id="bcc_div" style="display:none">
                                <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                                <div class="col-md-7">
                                    <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if (isset($templatedata)) echo ($templatedata['addBCC']) ? $templatedata['addBCC'] : ''; ?>" placeholder="BCC Email">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-md-3 control-label" >Attach Invoice PDF</label>
                                <div class="col-md-7">
                                    <label class="switch switch-info"><input type="checkbox" name="add_attachment"<?php  echo "checked"; ?> id="add_attachment"><span></span></label>
                                </div>
                            </div>

                            <div class="form-group">

                                <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                <div class="col-md-7">

                                    <input type="text" id="emailSubject" name="emailSubject" value="<?php if (isset($templatedata)) echo ($templatedata['emailSubject']) ? $templatedata['emailSubject'] : ''; ?>" class="form-control" placeholder="Email Subject">
                                </div>

                            </div>

                            

                            <div class="form-group">


                                <label class="col-md-3 control-label">Email Body</label>
                                <div class="col-md-7">
                                    <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if (isset($templatedata)) echo ($templatedata['message']) ? $templatedata['message'] : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-7 col-md-offset-3 align-right">
                                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                </div>
                            </div>
                        </form>


                    </div>


                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>