<html>
<head>
       <title><?php echo $template['title'] ?></title>
    <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/main.css">
    <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
    <style>
    body{background:#ffffff!important;
      font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
    }
    .backg{background:#f2f2f3;min-height:400px;border-top:1px solid #DADADA;}
    .back{background:#ffffff;width: 100%;float: left;border:1px solid #DADADA;}
    .nopadding{padding:0px;}
    .heading {padding: 8px 0 0 15px;}
    .padding{padding:15px 0px!important;}
    .myblock{background:#f2f2f3;width: 100%;float: left;}
    .myblock2{background:#ffffff;width: 100%;float: left;;border:1px solid #DADADA;margin-top:40px;}
    .policy{margin:3px 5px;}
    .myblock3 {float: left;width: 100%;margin: 30px 0px 50px 0px;}
    .btn-primary{background:#2676A4;border:1px solid #2676A4;}
    .error{color:red;font-size:11px;}
    .servererror{color:red;font-size:11px;width:100%;}
    .form-group {margin-bottom: 15px; min-height: 50px;}
    @media (min-width:992px)
    {
        .container{width:786px}
    }
@media (min-width:1200px) and (max-width:1400px)
    {
        .container{width:786}
    }
    .invoice_h3{
		padding-right: 0px !important;
		padding-left: 0px !important; 	
	}
    .invoice_h3>h3{
      	color: #38485b !important;
	  	margin-bottom: 20px !important;
    margin-top: 20px !important;
    font-weight: 300 !important;
    }
	.form-control{
		border-color: #e8e8e8 !important;
		height: 44px !important;
		font-size: 14px !important;
		border-top: 0px !important;
	}
	label {
		font-size: 13px;
		color: #38485b;
		font-weight: 600;
		margin-bottom: 7px;
	}
  .grey
	{
		background: #F8F8F8;
		clear: both;
		display: inline-block;
		padding: 15px 0 5px 0px;
	}
	.cards
	{
		padding-top: 3.4rem;
  }
  h5>strong{
    font-weight: 600 !important;
  }
  .submit-btn{
	  margin-top: -75px;
  }
</style>

</head>
<body>
<div class="container">
    <header>
        <div class="col-md-12">
            <div class="navbar-brand-centered" style="padding: 20px 0px;">
               <?php if(!empty($mr_config['ProfileURL'])){?>
                    <img src="<?php echo $mr_config['ProfileURL'];  ?>" class="img-responsive center-block" />
               <?php }else{ ?>
                    <img src="https://https://demo.payportal.com//admin/uploads/reseller_logo/1536147116logo_neww1.png" class="img-responsive center-block"  />
               <?php } ?>
            </div>
        </div>
        
     </header>
</div>

<div class="container-fluid backg">
    <div class="container">
        <div class="row">
            <div class="msg_data">
                <?php echo $this->session->flashdata('message');   ?>
            </div>
        </div>
         
        <?php
        
        $protocol = $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
        $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    
        $val = round($get_invoice['BalanceRemaining']);
        if($val > 0 || $get_invoice['IsPaid'] == false){
        ?>
        <div class="row">
          
            <div class="col-md-5 invoice_h3">
                <h3>Invoice No: <?php if(!empty($get_invoice['RefNumber'])){ echo $get_invoice['RefNumber']; }else{ echo $get_invoice['invoiceID'];} ?></h3>
            </div>
             <div class="col-md-7 invoice_h3">
                <h3 class="pull-right">Amount: <?php if(!empty($get_invoice['BalanceRemaining'])){ echo '$'. number_format($get_invoice['BalanceRemaining'],2); } ?></h3>
            </div>
        </div>
        
        <div class="row">
            <form method="post" id="thest_pay" action='<?php echo $action; ?>'>
                <input type="hidden" value="<?php echo $mid; ?>" name="mid" />
                <input type="hidden" value="<?php echo $invoice_no ?>" name="invid" />
                <input type="hidden" value="<?php echo $token ?>" name="token" />
                 <input type="hidden" value="<?php echo $protocol ?>" name="requrl" />
            <div class="back">
                <div class="col-md-12 nopadding">
                    
                  <div class="heading">
                       <h5> <strong>Cardholder Information</strong></h5>
                  </div>
                  <hr/>
                  
                      <!--personal detail-->
                      <div class="container">
                           <div class="row nopadding">
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">FIRST NAME</label>
            						<input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name">
            						<?php echo form_error('first_name', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label  class="control-label">LAST NAME</label>
            						<input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name">
            						<?php echo form_error('last_name', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                               
                          </div>
                      </div>
                       
                      <hr/>
                      
                      <!--card detail-->
                      <div class="container">
                           <div class="row nopadding">
                              
                               
							<div class="grey">
								<div class="col-md-12">
                            
                           
                              
                               <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">CARD NUMBER</label>
            						<input type="text" name="cardNumber" id="card_number11" class="form-control" autocomplete="off">
            						<?php echo form_error('cardNumber', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
							</div>
							<div class="col-md-12">
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">EXPIRATION MONTH</label>
            						<input type="text" name="cardExpiry" id="expiry11" class="form-control" placeholder="12">
            						<?php echo form_error('cardExpiry', '<div class="servererror">', '</div>'); ?>  
            					 </div>
                              </div>
                              
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">EXPIRATION YEAR</label>
            						<input type="text" name="cardExpiry2" id="expiry_year11" class="form-control" placeholder="2020">
            						<?php echo form_error('cardExpiry2', '<div class="servererror">', '</div>'); ?> 
            					 </div>
                              </div>
                              
                              <div class="col-md-3 col-md-offset-3">
                                  <div class="form-group">
                                  <label class="control-label">CVV</label>
                                  <input type="text" onblur="create_Token_stripe();" id="ccv11" name="cardCVC"  class="form-control" placeholder="123" autocomplete="off">
                                  <?php echo form_error('cardCVC', '<div class="servererror">', '</div>'); ?>
                                </div>
                              </div>
                                <div class="cards">
                                <img src="<?php echo base_url();?>resources/img/amex.png">
                              <img src="<?php echo base_url();?>resources/img/visa.png" />
                              <img src="<?php echo base_url();?>resources/img/master.png" />
                              <img src="<?php echo base_url();?>resources/img/discover.png" />
                                </div>
                                  </div>
                                  </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label><input type="hidden" value="savepaymentinfo" id="savepaymentinfo" name="savepaymentinfo" /> </label>
            						
            					 </div>
                              </div>
                              
                         </div>
                    </div>
                            
                  
                    <div class="container">
                           <div class="row">          
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">STREET ADDRESS</label>
            						<input type="text" name="address" id="address" class="form-control" placeholder="Street Address">
            						<?php echo form_error('address', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">ADDRESS 2</label>
            						<input type="text" name="address2" id="address2" class="form-control" placeholder="Address 2">
            					 </div>
                              </div>
                           </div> 
						   <div class="row">   						   
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">CITY</label>
            						<input type="text" name="city" id="city" class="form-control" placeholder="city">
            						<?php echo form_error('city', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">STATE</label>
            						<input type="text" name="state" id="state" class="form-control" placeholder="State">
            						<?php echo form_error('state', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">ZIP/POSTAL</label>
            						<input type="text" name="zip" id="zip" class="form-control" placeholder="Zip code">
            						<?php echo form_error('zip', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                             
                             
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">COUNTRY</label>
            						<input type="text" name="country" id="country" class="form-control" placeholder="Country">
            					 </div>
                              </div>
                              
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label><input type="hidden" style="margin-top: 1rem" value="sendrecipt" id="sendrecipt" name="sendrecipt" /> </label>
            						
            					 </div>
                              </div>
                              
						  </div>
						  <input type="hidden" id="stripeApiKey" name="stripe" value="<?php if(!empty($userName)){ echo $userName;} ?>" />
						  <div class="col-md-6 submit-btn pull-right">
							<input type="submit" name="pay" value="Pay Now" class="btn btn-success pull-right" />
						</div>
                      </div>
                  
                      
                </div>  
              
             </div>
             
             
                 <div class="myblock3">
                    
                    <div class="col-md-12">
                      
                         <p style="margin-top: 23px;" class="text-right">Powered by <a target="_blank" href="<?php if(!empty($mr_config['webURL'])){ echo $mr_config['webURL']; } else{ echo'#';}  ?>"><?php if(!empty($mr_config['resellerCompanyName'])){ echo $mr_config['resellerCompanyName']; }  ?></a></p>   
                    </div>
                    
                </div>
              </form>
         </div>
        <?php  }
        
        else{
         
             header("location:".base_url()."page_404/msg_paid");
        
            
            
        }  ?>
         
     </div>
</div>
<script src="https://testreseller.https://demo.payportal.com//resources/js/vendor/jquery-1.11.3.min.js"></script>
        <script src="https://testreseller.https://demo.payportal.com//resources/js/vendor/bootstrap.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>

$(document).ready(function () {
    $("#card_number11").attr("maxlength", 16);
    $("#expiry11").attr("maxlength", 2);
    $("#expiry_year11").attr("maxlength", 4);
    $("#ccv11").attr("maxlength", 4);
    
  $("#thest_pay").validate({
    rules: {
    
       first_name: {
        required: true,
        minlength: 1,
        maxlength: 100,
       validate_char: true
      },
       last_name: {
        required: true,
         minlength: 1,
         maxlength: 100,
         validate_char: true
      },
            country: {
                 maxlength: 31,
                 validate_addre: true
              },
              zip: {
                required: true,
                 minlength: 4,
                 maxlength: 6,
                 validate_char: true
              },
              address: {
                maxlength: 41,
                validate_addre: true
              },
              address2: {
                maxlength: 41,
                validate_addre: true
              },
              city: {
                   maxlength: 31,
                 validate_addre: true
              },
              state: {
                   maxlength: 31,
                   validate_addre: true
              }, 
   
       cardNumber: {
        required: true,
        number:true,
        minlength: 13,
         maxlength: 16
      },
       cardExpiry: {
        required: true,
         number:true,
        minlength: 2,
         min: 1, max: 12,
      },
       cardExpiry2: {
        required: true,
        number:true,
        minlength: 4,
         maxlength: 4,
         CCExp: {
        				month: '#expiry11',
        				year: '#expiry_year11'
        		  }
      },
       cardCVC: {
        number:true,
        minlength: 3,
        maxlength: 4
      }
    },
    messages: {
        
      cardCVC: {
        minlength: "Must be min 3 number"
      },
      cardExpiry2: {
        required: "Enter Expiry Year",
        minlength: "Expiry year must be min 4 number. exp:2000"
      },
      cardExpiry: {
        required: "Enter Expiry Month",
        minlength: "Expiry month must be min 2 number"
      },
      cardfriendlyname: {
        required: "Enter Card Friendly Name"
       
      },
      cardNumber: {
        required: "Enter Card Number",
        minlength: "Must be min 13 digits",
         maxlength: "Must be max 16 digits"
      },
    
    
      
    },
    submitHandler: function (form) {
        $("#qbd_process").attr("disabled", true);
        return true;
    }
    
  });
  
  
     $.validator.addMethod("phoneUS", function(phone_number, element) {
         
            return phone_number.match(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/);
        }, "Please specify a valid phone number like as (XXX) XX-XXXX");
         
          
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/, ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
   $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
					
});



  function stripeResponseHandler_res(status, response)
	  {
	  
          
                        $("#thest_pay").find('input[name="stripeToken"]').remove();
           
                if (response.error) {
                  
                    $('#submit_btn').removeAttr("disabled");
               
                    $('#payment_error').text(response.error.message);

                } else {
                    var form = $("#thest_pay");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

               
                 $("#btn_process").attr("disabled", false);	
                }
            }
            
            
            
	
 function create_Token_stripe()
 {
   
    
		var pub_key = $('#stripeApiKey').val();
        if(pub_key!=''){
		 Stripe.setPublishableKey(pub_key);
         Stripe.createToken({
                        number: $('#card_number11').val(),
                        cvc: $('#ccv11').val(),
                        exp_month: $('#expiry11').val(),
                        exp_year: $('#expiry_year11').val(),
                        name: $('#first_name').val()+''+$('#last_name').val()
                    }, stripeResponseHandler_res);
   
    }
        // Prevent the form from submitting with the default action
        return false;
		
     
 }
 
 
 

</script>

</body>
</html>