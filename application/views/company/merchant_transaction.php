<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Merchant Transactions</strong></h2>
        </div>
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="pay_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-right  hidden-xs " style="width: 100px;">Trans ID</th>
                   

                    <th class="text-right">Invoice</th>
                     <th class="text-right">Amount</th>
					 
                    
                    <th class="text-right hidden-xs">Date </th>
                    
                    <th class=" text-center">Status</th>
                   
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-right hidden-xs"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					
				
					<td class="text-right"><?php echo ($transaction['invoice'])?$transaction['invoice']:'--'; ?></td>
					<td class=" text-right">$<?php echo ($transaction['transactionAmount'])?$transaction['transactionAmount']:'0.00'; ?></td>
					
				
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($transaction['transactionDate'])); ?></td>
					
					<td class="text-center "><?php   if($transaction['transactionCode']=='300' || $transaction['transactionID']=='' || $transaction['transactionCode']=='400' ||$transaction['transactionCode']=='401' || $transaction['transactionCode']=='3'){ ?> 
					<span class="btn btn-sm btn-danger remove-hover">Failed</span> <?php }else if($transaction['transactionCode']=='100'|| trim($transaction['transactionCode'])=='200' || $transaction['transactionCode']=='111'|| $transaction['transactionCode']=='1'){ ?> <span class="btn btn-sm btn-success remove-hover">Success</span><?php } ?></td>
					
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="5"> No Records Found </td></tr>'; }  
				?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#pay_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>




</div>
<!-- END Page Content -->