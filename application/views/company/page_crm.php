<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>CRM & Marketing</strong> </h2>
            <div class="block-options pull-right">
                       
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Accounting Package</th>
                    <th class=" text-left hidden-xs">Username</th>
                     <th class="hidden-xs text-right">Password</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($companies) && $companies)
				{
					foreach($companies as $company)
					{
				?>
				<tr>
					<td class="text-left cust_view"><a href="#company_data" onclick="view_company_details('<?php echo $company['id']; ?>');" data-toggle="modal" data-backdrop="static" data-keyboard="false"><?php echo "QuickBooks Desktop"; // $company['companyName']; ?></a></td>
						
					<td class="text-left hidden-xs"><?php echo $company['qbwc_username']; ?></td>
					<td class="text-right hidden-xs"><?php echo $company['qbwc_password']; ?></td>
					
					<td class="text-center">
						<div class="btn-group btn-group-xs">
							<a href="<?php if($company['qbwc_username']!=""){ echo base_url().'QuickBooks/your_function/my-quickbooks-wc-file'.$company['qbwc_username'].'.qwc'; } ?>" data-toggle="tooltip" title="Download QWC File" class="btn btn-success"><i class="fa fa-download"></i></a>
							<a href="<?php echo base_url('QuickBooks/create_company/'.$company['id']); ?>" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <a href="#del_company" onclick="set_company_id('<?php  echo $company['id']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a>
                
                          
						</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#company').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_reseller(id){
	
	  $('#resellerID').val(id);
	
}


function view_company_details(id){
	 
	
	 if(id!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>home/get_company_details',
		  data : {'id':id},
		  success: function(data){
			  
			     $('#data_form_company').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 }


</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 

</div>

<div id="company_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">App Details</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_company"  style="height: 300px; min-height:300px;  overflow: auto; " >
			
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- END Page Content -->