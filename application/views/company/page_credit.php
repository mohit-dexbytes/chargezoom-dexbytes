<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <div>
  <?php echo $this->session->flashdata('message');   ?> 
	</div>		   
    <!-- All Orders Block -->
   <legend class="leg"> Credits</legend> 
    <div class="block-main full" style="position: relative;">
	        
        <!-- All Orders Title -->
        
        <div class="addNewBtnCustom">
               
            <?php if($plantype) { ?>          
            <a href="<?php echo base_url() ; ?>company/Credit/create_credit" class="btn btn-sm btn-success" style="display:none"   data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Add New</a>
            <?php } else { ?>
             <a href="<?php echo base_url() ; ?>company/Credit/create_credit" class="btn btn-sm btn-success"   data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Add New</a>
            <?php } ?>
                
                       
        </div>
       
        
        <!-- END All Orders Title -->
       
        <!-- All Orders Content -->
        <table id="credit_page" class="table dataTable table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                 
                    <th class="text-left">Customer Name</th>
					<th class="text-right ">Amount</th>		
					<th class="text-right hidden-xs">Date</th>					
					<th class="text-right hidden-xs">Status</th>
                    <th class="text-center" style="width:85px !important;">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($credits) && $credits)
				{
					foreach($credits as $credit)
					{
						
						
				?>
				<tr>
					
                    <td class="text-left "><?php echo $credit['CustomerFullName']; ?> </a> </td>
                	<td class="text-right ">$<?php echo number_format($credit['CreditRemaining'], 2); ?> </a> </td>
					
					<td class="text-right hidden-xs"><?php echo date('M d, Y', strtotime($credit['TimeModified'])); ?> </a> </td>
					
					<td class="text-right hidden-xs">
					<?php if( $credit['IsPending']=="true"){ ?>
					<span class="btn btn-sm btn-alt1 btn-success remove-hover"><?php echo "Processed"; ?></span> 
					<?php }else{ ?>
							<span class="btn btn-sm btn-alt1 btn-danger remove-hover">
						<?php echo "Pending"; ?> </span> 
				    <?php } ?>	</a> 
					
					</td>
					
					
					
					<td class="text-center">
						<div class="btn-group btn-group-sm">
						<a href="#view_credit"  class="btn btn-default"  onclick="set_view_credit('<?php echo $credit['CustomerListID'];  ?>');" title="Preview"  data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-eye"> </i> </a>
						
						
						</div>
					</td>
				</tr>
				
				<?php } } 
				else { echo'<tr>
                <td colspan="5"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; }  
				?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->


<!------    Add popup for credit   ------->

<div id="add_credit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add Credit </h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="form111" class="form  form-horizontal" action="<?php echo base_url(); ?>company/Credit/create_credit">
			  
		
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username"> Select Customer </label>
							
							<div class="col-md-8">
							
								<select id="creditname" name="creditname" class="form-control">
                                                   
										<option value>Choose Customer</option>
										
										<?php   foreach($customers as $customer){ ?>
														
											<option value="<?php echo $customer['ListID']; ?>"><?php echo  $customer['FullName'] ; ?> </option>
											
											<?php } ?>
									</select>
	
						</div>
					</div>
                </div>      
		    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Amount ($)</label>
							<div class="col-md-8">
								<input type="text" id="creditamount"  name="creditamount" class="form-control"  value="" placeholder="Amount"> </div>
						</div>
                       </div>      
							
					<div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Description</label>
							<div class="col-md-8">
								<input type="text" id="creditdescription"  name="creditdescription" class="form-control"  value="" placeholder="Description"> </div>
						</div>
                       </div>
                    
                  
	                <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>
					
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
					
                    </div>
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



</div>
<!-- END Page Content -->




<!------------------  View credit popup ------------------>

<div id="view_credit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> View Credit </h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
        
			<div id="data_credit"  style="height: 200px; min-height:200px;  overflow: auto; " >
			</div>	
			<div id="edit_credit_data" style="display:none;" >
			 <h2 class="modal-title text-center"> Edit Below Credit </h2>
			 <hr/>
			<form method="POST" id="form222" class="form form-horizontal" action="<?php echo base_url(); ?>company/Credit/update_credit">
			 
			<input type="hidden" id="creditEditID" name="creditEditID" value=""  />
			
               
				
		    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Amount ($)</label>
							<div class="col-md-8">
								<input type="text" id="amount"  name="amount" class="form-control"  value="" placeholder=""> </div>
			     </div>
          </div>  

		  <div class="col-md-12"> 
                    <div class="form-group">
							<label class="col-md-4 control-label" for="example-username"> Description </label>
							
							<div class="col-md-8">
							
								<input type="text" id="description"  name="description" class="form-control"  value="" placeholder="">
                                                   
					    </div>
					</div>
                </div> 

							
				
							
	            <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success"> Update </button>
					
                   <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button> 
					
                    </div>
                    
            </div>
			    </form>	
			
			</div>

               
					
            </div>
			    
			<div class="modal-footer">
                 <button type="button" id="vbtn" align="right" class="btn btn-sm pull-right btn-danger close1" data-dismiss="modal"> Cancel </button>
				 
			</div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            
            $('#credit_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [-1] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });
            

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script>$(function(){   

nmiValidation11.init(); 
nmiValidation22.init();    });


 
var nmiValidation11 = function() {
    
    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form111').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    
					  
					 creditname: {
							 required:true,
							
						},
				    creditamount: {
							 required:true,
							 number:true,
						},
					 creditdescription:{
						  required:true,
						  minlength:3,
					 },
					description:{
					    
					      required:true,
						  minlength:3,
					},
                    	
					
                },
               
            });
					
		
						

        }
    };
}();


var nmiValidation22 = function() {
    
    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form222').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    
					
				    amount: {
							 required:true,
							 number:true,
						},
				
					description:{
					    
					      required:true,
						  minlength:3,
					},
                    	
					
                },
               
            });
					
		
						

        }
    };
}();





function set_view_credit(cID){
	if(cID!=""){
    	$('#vbtn').show();
     $.ajax({
    url: '<?php echo base_url("company/Credit/get_credit_id")?>',
    type: 'POST',
	data:{customerID:cID},
    success: function(data){
		
		$('#data_credit').html(data);
			  
			 
			 
	}	
}); 
	
}

}



function set_edit_credit(credit_id){
	
	
	if(credit_id !=""){
		$('#vbtn').hide();
		
	  $('#edit_credit_data').css('display','block');
	
    
     $.ajax({
    url: '<?php echo base_url("company/Credit/get_creditedit_id")?>',
    type: 'POST',
	data:{credit_id:credit_id},
	dataType: 'json',
    success: function(data){
		
		$('#creditEditID').val(data.creditID);		
			 
			  $('#amount').val(data.creditAmount);
			  $('#description').val(data.creditDescription);
			 
			 
	}	
  }); 
	
  }

}


</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	
