<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo $template['title'] ?></title>

        <meta name="description" content="<?php echo $template['description'] ?>">
        <meta name="author" content="<?php echo $template['author'] ?>">
        <meta name="robots" content="<?php echo $template['robots'] ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

       
		
		<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="http://localhost/quickbook/resources/css/bootstrap.min.css">
		

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="http://localhost/quickbook/resources/css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="http://localhost/quickbook/resources/css/main.css">

        <!-- Include a specific file here from <?php echo base_url(CSS); ?>/themes/ folder to alter the default theme of the template -->
        <?php if ($template['theme']) { ?><link id="theme-link" rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes/<?php echo $template['theme']; ?>.css"><?php } ?>

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes.css">
        <!-- END Stylesheets -->
		

        <!-- Modernizr (browser feature detection library) -->
		<script src="<?php echo base_url(JS); ?>/vendor/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/plugins.js"></script>
        <script src="<?php echo base_url(JS); ?>/app.js"></script>
        <script src="<?php echo base_url(JS); ?>/custom.js"></script>
        <script src="<?php echo base_url(JS); ?>/pages/formsWizard.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/modernizr-respond.min.js"></script>
    </head>
	
    <body style="">
	
<!-- Page content -->
<div id="page-content">
    
	<!-- Products Block -->
    <div class="block">
        <!-- Customer Addresses Content -->
		<h2 style=" clear:left"><strong>&nbsp;&nbsp;Billing Report</strong></h2>
		<br>
		
		
			<div class="row" > 
		
			<div class="div2" style="margin-left:30px;" >
				<!-- Shipping Address Block -->
				<div class="blo ck">
					<!-- Shipping Address Content -->
				
					<strong>Invoice ID : </strong> <?php echo $invoice['invoice'] ?><br><br>
					<strong>Due Date : </strong><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?><br><br><br><br>
				</div>
				<!-- END Shipping Address Block -->
			</div>
		</div>
		<br>
		
		
        <!-- Products Content -->
         <div class="table-responsive">
         
        
       <?php  if(isset($invoice) &&  !empty($invoice) ){  ?>
        
         <table class="table table-bordered table-vcenter">
            <thead>
                <tr>
                    <th class="visible-lg">Items</th>
                   
					<th class="text-right hidden-xs">Amount,<b>$</b></th>
                  
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($items))
				{  
				    $total=0;
					foreach($items as $key=> $item)
					{
					    $total=$total+$item['itemPrice'];
				?>
				<tr>
					
					<td class=""><strong><?php  echo $item['itemName']; ?></strong></td>
				
                    <td class="hidden-xs text-right"><?php echo number_format($item['itemPrice'],2); ?></td>
				</tr>
				
				<?php } } ?>
				
					<tr class="active">
                        <td  class="text-right text-uppercase"><strong>Total</strong></td>
                        <td class="text-right"><strong><?php echo  number_format($total, 2) ; ?></strong></td>
						
                    </tr>
				
					<tr class="info">
                        <td colspan="" class="text-right text-uppercase"><strong>Total Payable</strong></td>
                        <td class="text-right"><strong><?php echo number_format($total, 2) ;  ?></strong></td>
                    </tr>
                    <tr class="success">
                        <td colspan="" class="text-right text-uppercase"><strong>Paid</strong></td>
                        <td class="text-right"><strong><?php echo ucfirst($invoice['status']); ?></strong></td>
                    </tr>
					<tr class="danger">
                        <td colspan="" class="text-right text-uppercase"><strong>Balance</strong></td>
                        <td class="text-right"><strong><?php echo ($invoice['BalanceRemaining'])?$invoice['BalanceRemaining']:'0.00';  ?></strong></td>
                    </tr>
				
			</tbody>
        </table>
       <?php }  ?>
        </div>
	
        <!-- END Products Content -->
    </div>
    <!-- END Products Block -->

    
</div>
<!-- END Page Content -->

<style type="text/css">






/*! CSS Used from: http://localhost/quickbook/resources/css/bootstrap.min.css */
body{margin:0;}
a{background-color:transparent;}
a:active,a:hover{outline:0;}
strong{font-weight:bold;}
table{border-collapse:collapse;border-spacing:0;}
td,th{padding:0;}
@media print{
*,*:before,*:after{background:transparent!important;color:#000!important;-webkit-box-shadow:none!important;box-shadow:none!important;text-shadow:none!important;}
a,a:visited{text-decoration:underline;}
a[href]:after{content:" (" attr(href) ")";}
a[href^="#"]:after{content:"";}
thead{display:table-header-group;}
tr{page-break-inside:avoid;}
h2{orphans:3;widows:3;}
h2{page-break-after:avoid;}
.table{border-collapse:collapse!important;}
.table td,.table th{background-color:#fff!important;}
.table-bordered th,.table-bordered td{border:1px solid #ddd!important;}
}
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff;}
a{color:#337ab7;text-decoration:none;}
a:hover,a:focus{color:#23527c;text-decoration:underline;}
a:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
h2{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
h2{margin-top:20px;margin-bottom:10px;}
h2{font-size:30px;}
.text-right{text-align:right;}
.text-uppercase{text-transform:uppercase;}
.text-danger{color:#a94442;}
address{margin-bottom:20px;font-style:normal;line-height:1.42857143;}
.row{margin-left:-15px;margin-right:-15px;}
.col-lg-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px;}
 
.col-lg-6{float:left;}
.col-lg-6{width:50%;}
 
table{background-color:transparent;}
th{text-align:left;}
.table{width:100%;max-width:100%;margin-bottom:20px;}
.table>thead>tr>th,.table>tbody>tr>th,.table>tbody>tr>td{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd;}
.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd;}
.table>thead:first-child>tr:first-child>th{border-top:0;}
.table-bordered{border:1px solid #ddd;}
.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tbody>tr>td{border:1px solid #ddd;}
.table-bordered>thead>tr>th{border-bottom-width:2px;}
.table>tbody>tr.active>td{background-color:#f5f5f5;}
.table>tbody>tr.success>td{background-color:#dff0d8;}
.table>tbody>tr.info>td{background-color:#d9edf7;}
.table>tbody>tr.danger>td{background-color:#f2dede;}
.table-responsive{overflow-x:auto;min-height:0.01%;}
@media screen and (max-width:767px){
.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;-ms-overflow-style:-ms-autohiding-scrollbar;border:1px solid #ddd;}
.table-responsive>.table{margin-bottom:0;}
.table-responsive>.table>thead>tr>th,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tbody>tr>td{white-space:nowrap;}
.table-responsive>.table-bordered{border:0;}
.table-responsive>.table-bordered>thead>tr>th:first-child,.table-responsive>.table-bordered>tbody>tr>td:first-child{border-left:0;}
.table-responsive>.table-bordered>thead>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>td:last-child{border-right:0;}
.table-responsive>.table-bordered>tbody>tr:last-child>td{border-bottom:0;}
}
.row:before,.row:after{content:" ";display:table;}
.row:after{clear:both;}
/*! CSS Used from: http://localhost/quickbook/resources/css/plugins.css */
.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.fa-exclamation-circle:before{content:"\f06a";}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media screen and (max-width: 800px) and (orientation: landscape),
@media only screen and (-Webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
/*! CSS Used from: http://localhost/quickbook/resources/css/main.css */
body{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;color:#394263;font-size:13px;background-color:#222222;}
#page-content{padding:10px 5px 1px;background-color:#eaedf1;}
.block{margin:0 0 10px;padding:20px 15px 1px;background-color:#ffffff;border:1px solid #dbe1e8;}
h2{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:300;}
h2{margin-bottom:15px;}
a,a:hover,a:focus{color:#1bbae1;}
.text-danger,.text-danger:hover{color:#e74c3c;}
strong{font-weight:600;}
.table.table-vcenter th,.table.table-vcenter td{vertical-align:middle;}
.table thead > tr > th{font-size:18px;font-weight:600;}
.table thead > tr > th{padding-top:14px;padding-bottom:14px;}
.table thead > tr > th,.table tbody > tr > th,.table tbody > tr > td,.table-bordered,.table-bordered > thead > tr > th,.table-bordered > tbody > tr > th,.table-bordered > tbody > tr > td{border-color:#eaedf1;}
.table{margin-bottom:20px;}
:focus{outline:0!important;}
@media screen and (min-width: 768px){
#page-content{padding:20px 20px 1px;}
.block{padding-left:20px;padding-right:20px;}
.block{margin-bottom:20px;}
}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media print{
#page-content{min-height:0!important;height:auto!important;padding:0!important;}
.block{border:none!important;padding:0!important;}
}
/*! CSS Used from: http://localhost/quickbook/resources/css/themes/night.css */
body{color:#333333;}
#page-content{background-color:#f2f2f2;}
.table thead > tr > th,.table tbody > tr > th,.table tbody > tr > td,.table-bordered,.table-bordered > thead > tr > th,.table-bordered > tbody > tr > th,.table-bordered > tbody > tr > td{border-color:#f2f2f2;}
.block{border-color:#e8e8e8;}
a,a:hover,a:focus{color:#888888;}
/*! CSS Used from: Embedded */
body{margin:0;}
strong{font-weight:bold;}
table{border-collapse:collapse;border-spacing:0;}
td,th{padding:0;}
@media print{
*,*:before,*:after{background:transparent!important;color:#000!important;-webkit-box-shadow:none!important;box-shadow:none!important;text-shadow:none!important;}
thead{display:table-header-group;}
tr{page-break-inside:avoid;}
h2{orphans:3;widows:3;}
h2{page-break-after:avoid;}
.table{border-collapse:collapse!important;}
.table td,.table th{background-color:#fff!important;}
.table-bordered th,.table-bordered td{border:1px solid #ddd!important;}
}
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff;}
h2{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
h2{margin-top:20px;margin-bottom:10px;}
h2{font-size:30px;}
.text-right{text-align:right;}
.text-uppercase{text-transform:uppercase;}
address{margin-bottom:20px;font-style:normal;line-height:1.42857143;}
.row{margin-left:-15px;margin-right:-15px;}
.col-lg-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px;}
@media (min-width:1200px){
.col-lg-6{float:left;}
.col-lg-6{width:50%;}
}
table{background-color:transparent;}
th{text-align:left;}
.table{width:100%;max-width:100%;margin-bottom:20px;}
.table>thead>tr>th,.table>tbody>tr>th,.table>tbody>tr>td{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd;}
.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd;}
.table>thead:first-child>tr:first-child>th{border-top:0;}
.table-bordered{border:1px solid #ddd;}
.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tbody>tr>td{border:1px solid #ddd;}
.table-bordered>thead>tr>th{border-bottom-width:2px;}
.table-responsive{overflow-x:auto;min-height:0.01%;}
 
.row:before,.row:after{content:" ";display:table;}
.row:after{clear:both;}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media screen and (max-width: 800px) and (orientation: landscape),
@media only screen and (-Webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
#page-content{padding:10px 5px 1px;background-color:#eaedf1;}
.block{margin:0 0 10px;padding:20px 15px 1px;background-color:#ffffff;border:1px solid #dbe1e8;}
h2{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:300;}
h2{margin-bottom:15px;}
strong{font-weight:600;}
 
@media screen and (min-width: 768px){
#page-content{padding:20px 20px 1px;}
.block{padding-left:20px;padding-right:20px;}
.block{margin-bottom:20px;}
}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media print{
#page-content{min-height:0!important;height:auto!important;padding:0!important;}
.block{border:none!important;padding:0!important;}
} </style>



<style>
body{
	    font-size: 14px; 
    color: #333;
}
.div2{
	width:46%; float:left; padding:10px;margin:0px;
}

.row{
	width:100%;
	clear:both;
		    font-size: 14px; 
    color: #333;margin:0px;padding:0px;
}



tr{background-color:#d9edf7;}

 th , td{
    padding : 6px 8px; 
}
 
  

</style>


<script>$(function(){ FormsWizard.init(); });</script>
    </body>
</html>