<style>
	.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 270px !important;
	}
</style>
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">

    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <legend class="leg">Transactions</legend>
    <div class="full">

        <!-- All Orders Title -->
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="pay_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>

                    <th class="text-left">Customer Name</th>
                    <th class="text-right">Invoice</th>
                    <th class="text-right hidden-xs">Amount</th>

                    <th class="text-right ">Date </th>
                    <th class="text-right hidden-xs">Type</th>
                    <th class="text-right hidden-xs hidden-sm">Transaction ID</th>

                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

</div>

<div id="print-transaction-div" style="display: none;">
    
</div>

<div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Refund Payment</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <div id="refund_msg"></div>


                <form id="data_form" method="post" action='<?php echo base_url(); ?>company/refundInvoice/create_payment_refund' class="form-horizontal">
                    <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p>

                    <div id="sigle_tr">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_list">Refund Amount:</label>
                            <div class="col-md-8">
                                <label class="control-label" id="ref_amount"></label>
                               
                            </div>
                        </div>
                    </div>
                    <div id="multi_tr11">

                    </div>
                    <div class="pull-right">
                       
                        <input type="submit" id="rf_btn" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund" />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="payment_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Void Payment</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form id="data_formpay" method="post" action='<?php echo base_url(); ?>company/AuthPayment/delete_pay_transaction' class="form-horizontal">
                    <p id="message_data">Do you really want to "Void" this payment?</p>
                    <div class="form-group">
                        <div class="col-md-8">

                        </div>
                    </div>
                    <div class="pull-right">
                        <input type="submit" name="btn_cancel" class="btn btn-sm btn-danger" value="Void" />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $(function(){ 
            Pagination_view.init(); 
        });

        var table_datatable = '';
        var Pagination_view = function(){
            return {
                init: function() {
                    /* Extend with date sort plugin */
                    $.extend($.fn.dataTableExt.oSort, {
                
                    } );

                    /* Initialize Bootstrap Datatables Integration */
                    App.datatables();

                    /* Initialize Datatables */
                    table_datatable = $('#pay_page').dataTable({
                        "processing": true, //Feature control the processing indicator.
                        "serverSide": true, //Feature control DataTables' server-side processing mode.
                        "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
                        "pageLength": 10,
                        // Load data for the table's content from an Ajax source
                        "ajax": {
                            "url": "<?php echo site_url('company/Payments/ajaxPaymentTransactions')?>",
                            "type": "POST" ,
                            
                            "data":function(data) {
                                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                            },
                        },
                        
                        "order": [[3, 'desc']],
                        "sPaginationType": "bootstrap",
                        "stateSave": false
                    
                    });

                    /* Add placeholder attribute to the search input */
                    $('.dataTables_filter input').attr('placeholder', 'Search');
                    $('#pay_page_filter').remove();
                    $('#pay_page_length').after('<div id="pay_page_filter" class="dataTables_filter"><label><div class="input-group"><input type="search" class="form-control" placeholder="Search" aria-controls="pay_page"><span class="input-group-addon"><i class="fa fa-search"></i></span></div></label></div>');
                    $('#pay_page_filter').addClass('col-md-10');
                    $('#pay_page_length').addClass('col-md-2');
                }
            };
        }();

        $(document).on('keyup','.dataTables_filter input', function (){
            table_datatable.fnFilter( this.value ).draw();
        });
            
        $('#pay_page .dataTables_filter input').addClass("form-control ").attr("placeholder", "Search");
        $('#pay_page .dataTables_length select').addClass("m-wrap form-control ");
        $('#pay_page .dataTables_length dataTables_length select').select2();
    });
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>
    $(function() {
        $('#data_form').validate({

            rules: {

                'ref_amount': {
                    required: true,
                    number: true,
                    remote: {
                        url: "<?php echo base_url(); ?>Payments/check_transaction_payment",
                        type: "POST",
                        cache: false,
                        dataType: "json",
                        data: {
                            trID: function() {
                                return $("#trID").val();
                            },

                        },
                        dataFilter: function(response) {

                            var rsdata = jQuery.parseJSON(response)

                            if (rsdata.status == 'success')
                                return true;
                            else
                                return false;
                        }
                    },


                },

                'multi_inv': {
                    required: true,
                },

            },

        });
    });

    function set_refund_pay(id, txnid, txntype, refamoutn) {


        var form = $("#data_form");
        $('#ref_amount').html(roundN(refamoutn, 2));
        $('.ref').remove();


        $('<input>', {
            'type': 'hidden',
            'id': 'trID',
            'name': 'trID',
            'class': 'ref',
            'value': id,
        }).appendTo(form);

        if (txnid != "") {
            $('#multi_tr').html('');
            $('#txnID').remove();
            $('<input>', {
                'type': 'hidden',
                'id': 'txnID',
                'name': 'txnID',
                'class': 'ref',
                'value': txnid,
            }).appendTo(form);

        }
    }

    function set_transaction_pay(t_id) {
        var form = $("#data_formpay");
        $('<input>', {
            'type': 'hidden',
            'id': 'paytxnID',
            'name': 'paytxnID'

        }).remove();
        $('<input>', {
            'type': 'hidden',
            'id': 'paytxnID',
            'name': 'paytxnID',
            'value': t_id,
        }).appendTo(form);



    }

</script>


<style>
    
.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

.custom-datatable-filter-class{
    width: 100% !important;
}

#pay_page_length {
    padding-left: 0;
    padding-right: 0;
    width: 75px;
}

#pay_page_filter{
    padding-left: 0;
}

#pay_page_filter label{
    float: left;
}
</style>



<!-- END Page Content -->