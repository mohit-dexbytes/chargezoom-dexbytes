<div id="page-content">
    
    <?php /* ?> <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <strong><a href="<?php echo base_url(); ?>home/index">Dashboard</a></strong>
        </li>
        <li class="breadcrumb-item"><small><?php if(isset($subs)){ echo "Edit"; }else{ echo "Create"; } ?> Plan</small></li>
      </ol> <?php */ ?>
    
  
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
        <div class="col-md-12">
		<div class="block">
		 <div class="block-title">
             <h2><strong><?php if(isset($subs)){ echo "Edit"; }else{ echo "Create"; } ?> Plan </strong>  </h2>
        </div>
		   <?php //echo "<pre>"; print_r($subs); ?>
		    <form id="form-validation" action="<?php echo base_url('company/SettingPlan/create_plan'); ?>" method="post" class="form-horizontal form-bordered">
                
				
				
                 <fieldset> 
                 <legend><strong>Configurable Options</strong></legend>
				 <div class="col-md-12">
				   <div class="col-md-3 form-group">
						<label class="control-label" for="customerID">Plan Name</label>
						<div >
						
						  <input type="text" id="plan_name" name="plan_name"   class="form-control" value="<?php if(isset($subs)){ echo  $subs['planName']; }; ?>" placeholder="Plan Name" />
						 
						  <?php /* ?>
							<select id="duration_list" name="duration_list"  class="form-control">
								<option value="" >Select Duration</option>
								<?php   foreach($durations as $duration){       ?>
								 <option value="<?php echo $duration['planValue']; ?>"  
								 <?php if(isset($subs) &&  $subs['subscriptionPlan']==$duration['planValue']){  echo "Selected" ;} ?> ><?php echo $duration['planName']; ?></option>
								<?php }    ?>
								   
							</select>
						<?php */ ?>	
						</div>
                   </div>
			      
                    
                   <div class="col-md-3 form-group">   
                       
                       
						<label class="control-label" for="customerID">Frequency</label>
                           <div>  
                               <select name="paycycle" id="paycycle" class="form-control">
							 <option value="" >Select Frequency</option>
							 <?php  foreach($frequencies as $frequecy){ ?>
							 <option value="<?php echo $frequecy['frequencyValue']; ?>"
							 <?php if(isset($subs) && $frequecy['frequencyValue']==$subs['invoiceFrequency'] ){echo "selected";} ?>><?php echo $frequecy['frequencyText']; ?> </option>
							 <?php } ?>
							   </select>
                         </div>
                     </div>
                             
                   <div class="col-md-3  form-group">
						<label class="control-label" for="customerID">Recurrence</label>
						<div >
						
						  <input type="text" id="duration_list" name="duration_list"  class="form-control" value="<?php if(isset($subs)){ echo  $subs['subscriptionPlan']; }; ?>" placeholder="0 for Unlimited" />
						 
						  <?php /* ?>
							<select id="duration_list" name="duration_list"  class="form-control">
								<option value="" >Select Duration</option>
								<?php   foreach($durations as $duration){       ?>
								 <option value="<?php echo $duration['planValue']; ?>"  
								 <?php if(isset($subs) &&  $subs['subscriptionPlan']==$duration['planValue']){  echo "Selected" ;} ?> ><?php echo $duration['planName']; ?></option>
								<?php }    ?>
								   
							</select>
						<?php */ ?>	
						</div>	
					 </div>
                  
					 
				  <div class="col-md-3  form-group">
				   <div class="">
						 
					 <label class="control-label ">Free Trial Recurrence</label>
					
					 <input type="text" class="form-control" id="freetrial" name="freetrial" value="<?php if(isset($subs))echo $subs['freeTrial']; ?>" placeholder="Free Trial">
					
				  </div>
				</div> 
				
				
				
				
            </div>
				
			
				 <div class="col-md-12 prorata_billing_block" id="MhiddenDiv"   <?php if(isset($subs)&& $subs['invoiceFrequency']=='mon'){  ?> style="display:block;" <?php }else{ ?>  style="display:none;" <?php } ?>>
				     <div class="form-group col-md-12">
				   <div class="">
				    <label class="col-md-2 prorata-label">Prorata Billing</label>		
				     <div class="col-md-10"><input type="checkbox" id="prorate_bill_check" name="pro_rate"  <?php if(isset($subs)&& $subs['proRate']=='1'){ echo "checked"; } ?> ><span class="prorata_cont"> Tick this box to enable</span></div>
				     </div>
				     </div>
				     
				     <div class="form-group col-md-12">
				   <div class="">
				    	<label class="col-md-2 prorata-label">Prorata Date</label>		
				     <div class="col-md-2"> <input type="text" class="form-control" id="prorata_date" name="pro_billing_date" value="<?php if(isset($subs))echo $subs['proRateBillingDay']; ?>" placeholder="Prorata Date"> </div>
				      <span class="prorata_cont">Enter the day of the month you want to charge on </span>
				     </div>
				     </div>
				      <div class="form-group col-md-12">
				      <div class="">
				    	<label class="col-md-2 prorata-label">Charge Next Month</label>		
				       <div class="col-md-2"> <input type="text" class="form-control" id="prorata_date_next" name="pro_next_billing_date" value="<?php if(isset($subs))echo $subs['nextMonthInvoiceDate']; ?>" placeholder="Charge"> </div>
				       <span class="prorata_cont">Enter the day of the month after which the following month will also be included on the first invoice </span>
				     </div>
				     </div>
				     
				</div>
				 	 <div class="col-md-12"	>
				 	
				<div class="form-group col-sm-3">	
						<div class="col-sm-12">
						  
						  <label style="padding:5px;" class=" control-label">Email Recurring Invoices</label>
						</div>	
						<div class="col-sm-12">
							<label class="radio-inline control-label">
							<input type="radio"   name="email_recurring"  <?php if(isset($subs)&& $subs['emailRecurring']=='1'){ echo "checked"; } ?>  value="1" >Yes</label>
							<label class="radio-inline control-label">
							<input type="radio"   name="email_recurring"  <?php if(isset($subs)&& $subs['emailRecurring']=='0')echo "checked"; ?>   value="0" >No</label>
						 
						 </div> 
				</div> 			
				<div class="form-group col-sm-3">	
						<div class="col-sm-12">
						  
						     <label style="padding:5px;" class=" control-label">Automatic Payment</label>
						</div>	
						<div class="col-sm-12">
							<label class="radio-inline control-label">
							<input type="radio"   name="autopay" onclick="chk_payment(this.value);" <?php if(isset($subs)&& $subs['automaticPayment']=='1'){ echo "checked"; } ?>  value="1" checked>Yes</label>
							<label class="radio-inline control-label">
							<input type="radio"   name="autopay" onclick="chk_payment(this.value);" <?php if(isset($subs)&& $subs['automaticPayment']=='0')echo "checked"; ?>   value="0" >No</label>
						 
						 </div> 
				</div> 	
				<div class="form-group col-sm-3">	
						<div class="col-sm-12">
						  
						     <label style="padding:5px;" class=" control-label">Auto-Rebilling</label>
						</div>	
						<div class="col-sm-12">
							<label class="radio-inline control-label">
							<input type="radio"   name="rebilling"  value="1" checked>Yes</label>
							<label class="radio-inline control-label">
							<input type="radio"   name="rebilling"  value="0" >No</label>
						 
						 </div> 
				</div> 	

				  	<div class="form-group col-sm-3">	
					<div class="">	
					                                            
						<label class=" control-label" for="card_list">Gateway</label>
						
							<select id="gateway_list" name="gateway_list"  class="form-control">
								<option value="" >Select Gateway</option>
								  <?php foreach($gateways as $gateway){ ?>
								   <option value="<?php echo $gateway['gatewayID'];  ?>" <?php if(isset($subs) && $subs['paymentGateway']==$gateway['gatewayID'])echo "selected"; ?> ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								
								
							</select>
					</div>		
					
				</div>	
			
						 
				
			
					 
			</fieldset>
                    
					<fieldset><legend> Products & Services</legend>	
					<div class="form-group">
					<div class="col-sm-3">
					    <div class="form-group"><label class="control-label">Products & Services</label> </div>
			        	</div> 
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Recurring / One Time</label></div> </div>	
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Description </label></div> </div>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Price </label> </div></div>
					<div class="col-sm-1"><div class="form-group text-center"><label class="control-label">Quantity </label></div> </div>
				<?php /*	<div class="col-sm-1"><div class="set_taxes" style="display:none;"><div class="form-group"><label class="control-label">Tax </label></div></div> </div> */ ?>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Total</label></div> </div>
					
					</div>
										
						  <div id="item_fields">
						 <?php 
						       if(isset($items) && !empty($items)){
							     foreach($items as $k=>$item ){
									$rate = $item['itemRate'];
									$qnty = $item['itemQuantity'];
									if(!empty($item['itemTax'])) {
										$tax  = $item['itemTax'];
										echo "<script> $('.set_taxes').css('display','block'); </script>";
									}else{	$tax = 0; }
									
									$total_amt = ( $rate * $qnty ) + ( ( $rate * $qnty ) * $tax / 100 );
								 
								 ?>
								  <div class="form-group removeclass<?php echo $k+1; ?>">
								<div class="col-sm-3 nopadding"><div class="form-group">
								  <select class="form-control"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="productID<?php echo $k+1; ?>" name="productID[]">
								<option value="">Select Product & Service</option>
								
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>" <?php if($plan['ListID']==$item['itemListID']){ echo "selected";} ?>  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div><div class="col-sm-2 nopadding"><div class="form-group ">
								  <select class="form-control"  id="onetime_charge" name="onetime_charge[]">
								<option value="">Select Product & Service</option>
								   <option value="0" <?php if($item['oneTimeCharge']=='0'){ echo "selected";} ?>  >Recurring</option> 
								  <option value="1"  <?php if($item['oneTimeCharge'] =='1'){ echo "selected";} ?>  >One Time</option> 
								   </select></div></div> <div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" placeholder="Description "></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control float" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo number_format($item['itemRate'],2,'.',''); ?>" onblur="set_unit_val('<?php echo $k+1; ?>');" placeholder="Price"></div></div>
								   
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity<?php echo $k+1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>" placeholder="Qty"></div></div>
								   
								   <?php /*<div class="col-sm-1 nopadding"><div class="set_taxes"><div class="form-group"> <input type="checkbox" style="display:none;" id="tax_check<?php echo $k+1; ?>" <?php if($item['itemTax']) echo "checked"; ?> name="tax_check[]" class="tax_checked" onchange="set_tax_val(this, '<?php echo $k+1; ?>')" value="<?php echo $item['itemTax']; ?>"></div></div></div> */ ?>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k+1; ?>" name="total[]" value="<?php echo sprintf('%0.2f', $total_amt); ?>" placeholder="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div></div>

						<?php		 }								 
							   }else{  
						 ?>
						   <div class="form-group removeclass1">
							<div class="col-sm-3 nopadding"><div class="form-group ">
								  <select class="form-control"  onchange="select_plan_val('1');"  id="productID1" name="productID[]">
								<option value="">Select Product & Service</option>		
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>"  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div><div class="col-sm-2 nopadding"><div class="form-group ">
								  <select class="form-control"  id="onetime_charge" name="onetime_charge[]">
								
								   <option value="0" >Recurring</option> 
								  <option value="1" >One Time</option> 
								   </select></div></div>   <div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description1" name="description[]" value="" placeholder="Description "></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control force-numeric" id="unit_rate1" name="unit_rate[]" value="" onblur="set_unit_val('1');" placeholder="Price"></div></div>
								   
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val('1');" id="quantity1" name="quantity[]" value="" placeholder="Qty"></div></div>
								   
								 <?php /*  <div class="col-sm-1 nopadding"><div class="set_taxes" style="display:none;"><div class="form-group"> <input type="checkbox" id="tax_check1" onchange="set_tax_val(this, '1')" name="tax_check[]" class="tax_checked" value=""></div></div></div> */ ?>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total1" name="total[]" value="" placeholder="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('1');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>
							   
							</div>   <?php } ?>
								  
						 </div>
						
				 
				       <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label "></label>						  
						 <div class="group-btn">
						 
							<button class="btn btn-sm btn-success" type="button"  onclick="item_fields();"> Add More</button>
						 
							<label class="btn btn-sm pull-right remove-hover"><strong>Total: $<span id="grand_total"><?php echo '0.00'; ?></span></strong>  </label>
						  </div>
				        </div>
						</div>
						
					</div>	
					
				</fieldset>
			<fieldset>		
			<legend> Checkout Page</legend>
				 <div class="col-md-12"	>
				
					<div class="form-group col-sm-3">	
						<div class="col-sm-12">
						     <label style="padding:5px;" class=" control-label">Require Shipping Address</label>
						</div>	
						<div class="col-sm-12">
							<label class="radio-inline control-label">
							<input type="radio"   name="require_shipping" <?php if(isset($subs)&& $subs['isShipping']=='1'){ echo "checked"; } ?>  value="1" >Yes</label>
							<label class="radio-inline control-label">
							<input type="radio"   name="require_shipping" <?php if(isset($subs)&& $subs['isShipping']=='0')echo "checked"; ?>   value="0" >No</label>
						 </div> 
				</div> 		 
			    	
			 <div class="form-group col-sm-3">	
						<div class="col-sm-12">
						     <label style="padding:5px;" class=" control-label">Must Accept of Terms of Service</label>
						</div>	
						<div class="col-sm-12">
							<label class="radio-inline control-label">
							<input type="radio"   name="require_service" <?php if(isset($subs)&& $subs['isService']=='1'){ echo "checked"; } ?>  value="1" >Yes</label>
							<label class="radio-inline control-label">
							<input type="radio"   name="require_service" <?php if(isset($subs)&& $subs['isService']=='0')echo "checked"; ?>   value="0" >No</label>
						 </div> 
				</div> 	
			 
			   </div>	
			   
			   	<div class="col-md-12"	>
				    
				     <div class="form-group">
                        <label class="col-md-2  " stye="text-align:left" for="plan_link">Plan Link</label>
                    
                                 <div class="col-md-10" id="m_link_id"> <?php if(isset($subs) && !empty($subs['merchantPlanURL'])){ ?> 
                                       
                                       
                                    <div class="input-group">
                                         <a  href="<?php echo $subs['merchantPlanURL']; ?>" id='pl_href_link'>
                                            <span class="input-group-addon plan_cust_url"><?php echo $ur_data['customerPortalURL'].'company_check_out/'.$base_id.'/'; ?></span> 
                                         </a>
                                         <div class="abctest">
                                              <input type="text" id="portal_urlll" name="plan_url" size="30" <?php if(isset($subs) && !empty($subs['merchantPlanURL'])){  echo ' value ="'.$subs['postPlanURL'].'" '.' readonly '; } ?> class="form-control valid">
                                              <?php if(isset($subs)){ ?>  <input type='button' id="btn_mask_link" class="btn btn-sm btn-default" value="Edit Link" /> 
                                              <input type="button"  class="btn btn-sm btn-info" onclick="myFunction()" value="Copy" /><?php }  ?> 
                                              
                                          </div>
                                    </div>
                                 <?php }else{  ?>
                                 <?php
                                       if($this->session->userdata('logged_in')){
				                            $data['login_info'] = $this->session->userdata('logged_in');
				                            	$base_id =base64_encode( $data['login_info']['merchID']);
			                        	}
                                 ?>
                                    <div class="input-group">
                                         <span class="input-group-addon plan_cust_url"><?php echo $ur_data['customerPortalURL'].'qbd_check_out/'.$base_id.'/';   ?></span> 
                                        <div class="abctests">
                                         <input type="text" id="newplan" name="short_url"  class="form-control valid">
                                         </div>
                                    </div>
                                 
                               <?php  } 
                              
                         
                               ?>
                               </div>
                          </div>
                    </div>
			     
				  	<div class="col-md-12"	>
				    
				     <div class="form-group">
                        <label class="col-md-2" stye="text-align:left" for="plan_link">Confirmation Page</label>
                    
                              <div class="col-md-6" id="m_link_id">
                              <input type="text" id="confirm_page_url" name="confirm_page_url"  value="<?php if(isset($subs) && !empty($subs)){ echo $subs['confirm_page_url']; } ?>"   class="form-control" placeholder="Confirmation Page">
                                 </div> 
                               </div>
                          </div>
                    
				  
                     <input type="hidden" name="planID" value="<?php if(isset($subs)){  echo $subs['planID']; }?>" />   
                	<div class="col-md-12">
                 <div class="form-group pull-right">
				
					   
					<?php /* if(!isset($subs)) {?>		
                  				
					   <button type="submit" class="submit btn btn-sm btn-success">Save</button>					
					<?php }else {?>										
					   <button type="submit" class="submit btn btn-sm btn-success">Update</button>					
					<?php } */  ?>
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>	
						 <a href="<?php echo base_url(); ?>company/SettingPlan/plans" class=" btn btn-sm btn-primary1">Cancel</a>		
					</div>
				    </div>		
			 </div>

						</fieldset>
                    
			
               
				
                  
            </form>
          </div>      
        </div>
            
       </div>      
        </div>


    <script>
	
	  $(document).ready(function() {
	      
	       
	    
	        $('#btn_mask_link').click(function(){
	        $('#portal_urlll').removeAttr('readonly');
	        $("#portal_urlll").css({"background-color": "#f2f2f2", "border": "1px solid #ddd","border-radius":"0px"});
	    });  
		$('#taxes').change( function(){
			   if($(this).val() != "" ){
			   $('.set_taxes').show();
			   $('.show_check').show();
			   }else{
					$('#tax_check').val('');
					$('.set_taxes').hide();
			   }
		  });
		  
		
    $('#paycycle').change( function(){
	    var pay_type = $(this).val();
			   	if(pay_type == 'mon'){
			   //	$(this).attr("selected","selected");      
			   $('#MhiddenDiv').show();
			   }else{
					$('#MhiddenDiv').hide();
			   }
		  });
		  
  $('.tax_div').change( function(){
    var tax_id = $(this).val();
    $.ajax({
    url: '<?php echo base_url("company/SettingSubscription/get_tax_id")?>',
    type: 'POST',
	data:{tax_id:tax_id},
	dataType: 'json',
    success: function(data){
	//	console.log(data);
             		
			 $('.tax_checked').val(data.taxRate);
			 
			 
	}	
});
	
});


  
$("#plan_name").blur(function(){
    var str =  $(this).val(); 
    str = str.replace(/\s+/g, '_');
    $('#newplan').val(str);
    
});

			
            nmiValidation.init();
		  
	      var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
			 $("#invoice_date").datepicker({ 
              format: 'yyyy-mm-dd',
              autoclose: true
            });  
			
			$('#sub_start_date').datepicker({
			format: 'yyyy-mm-dd',
			startDate:today,
			autoclose: true
			});
			$('#subsamount').blur(function(){
				
				var dur = $('#duration_list').val();
              var subsamount = $('#subsamount').val();
			  var tot_amount = subsamount*dur ;
			  $('#total_amount').val(tot_amount.toFixed(2));
			  $('#total_invoice').val(dur);
				
			});
			
			
					
		  $('#card_list').change( function(){
			   if($(this).val()=='new1'){
			   $('#set_credit').show();
			   }else{
					  $('#card_number').val('');
			$('#set_credit').hide();
			   }
		  });
	/*				
		$('#customerID').change(function(){
			
			var cid  = $(this).val();
		
			if(cid!=""){
				$('#card_list').find('option').not(':first').remove();
				$.ajax({
					type:"POST",
					url : "<?php echo base_url(); ?>company/Payments/check_vault",
					data : {'customerID':cid},
					success : function(response){
						
							 data=$.parseJSON(response);
							
							 if(data['status']=='success'){
							
								  var s=$('#card_list');
								 console.log (data['card']);
								  var card1 = data['card'];
									 $(s).append('<option value="new1">New Card</option>');
									for(var val in  card1) {
										
									  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
									}
																   
									$('#address1').val(data['ShipAddress_Addr1']);
									$('#address2').val(data['ShipAddress_Addr2']);
									$('#city').val(data['ShipAddress_City']);
									$('#state').val(data['ShipAddress_State']);
									$('#zipcode').val(data['ShipAddress_PostalCode']);
									$('#phone').val(data['Phone']);
									
								  
						   }	   
						
					}
					
					
				});
				
			}	
		});		
				
	 */

	});
	
	  
	      
	 var jsdata = '<?php if(isset($items)){ echo json_encode($items);}?>';
	
	if(jQuery.isEmptyObject(jsdata))
   {
	  
		var room = 1;
	  
		
	}else{
		
		var grand_total1=0;
		var room = '<?php if(isset($k)){ echo $k+1; }else{ echo "1";} ?>';
		$( ".total_val" ).each(function(){
			
            grand_total1+= parseFloat($(this).val());
			});
			
		$('#grand_total').html(format22(grand_total1));

	}
	function item_fields()
	{
	   <?php
	        $arrplan = json_encode($plans);
	        $newdata = str_replace("'", "", $arrplan);
	        ?>
		room++;
		var  jsplandata = '<?php echo $newdata ?>';
	//	var jsplandata = jsplandata1.replace(/'/g, '');
		
		var plan_data = $.parseJSON(jsplandata); 
		var plan_html ='<option val="">Select Product or Service</option>';
		for(var val in  plan_data) {      console.log(plan_data[val]);   plan_html+='<option value="'+ plan_data[val]['ListID']+'">'+plan_data[val]['FullName']+'</option>'; }
		
		var onetime_html ='<option val="0">Recurring</option><option val="1">One Time</option>';
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		var show_tax = '';
		
		if($('#taxes').val() == ''){
			var show_tax = 'style="display:none;"';
			var tax_val = 0;
		}else {
			var tax_val = $('.tax_checked').val();
		}
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"   id="onetime_charge'+room+'" name="onetime_charge[]">'+ onetime_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="Description "></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate'+room+'" name="unit_rate[]" value="" onblur="set_unit_val('+room+');" placeholder="Price"></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" placeholder="Qty"></div></div>   <div class="col-sm-1 nopadding"><div style="display:none;" class="set_taxes" ><div class="form-group"> <input type="checkbox" id="tax_check'+room+'" onchange="set_tax_val(this, '+room+')" name="tax_check[]" '+show_tax+' class="show_check tax_checked" value="'+tax_val+'"></div></div></div> 	     <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+room+'" name="total[]" value="" placeholder="Total"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest)
  }
  
  
   function remove_education_fields(rid)
   {
	  
	   var rid_val = $('#total'+rid).val();
	   
	  // var gr_val  = $('#grand_total').html();
	    var gr_val =0;
	   $( ".total_val" ).each(function(){
			
            gr_val+= parseFloat($(this).val());
			});
	   
	    if(rid_val){
	   var dif     = parseFloat(gr_val)-parseFloat(rid_val);
	   $('#grand_total').html(format22(dif));
		}
	   $('.removeclass'+rid).remove();
	   
	    
	   
	   
  }
var maxdays = daysInThisMonth(); 

function daysInThisMonth() {
  var now = new Date();
  return new Date(now.getFullYear(), now.getMonth()+1, 0).getDate();
}

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                     plan_name:{
						  required:true,
						  minlength:3,
						  maxlength: 25,
						  validate_char:true,
					 },
					  pro_billing_date:{
                          required:true,
                          min:1,
                          max:maxdays,
                          digits:true,
                    },
                      pro_next_billing_date:{
                      required:true,
                      min:1,
                      max:maxdays,
                      digits:true,
                    },
                   
					 sub_start_date: {
							 required:true,
					},
					short_url:{
					   validPlanurl: true  
					 },
					autopay:{
							 required:true,
					},
					gateway_list:{
							 required:true,
					},
					card_list:{
							 required:true,
					},
					paycycle:{
							 required:true,
					},		
			    	 duration_list: {
                        required: true,
                        number: true,
						maxlength:2,
						
                    },
					invoice_date: {
                        required: true,
                        
                    },
                    customerID: {
                         required: true,
                       
                    },
					 subsamount: {
                        required: true,
						number:true,
						
                    },
					friendlyname:{
						 required: true,
						 minlength: 3,
					},
					  card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },	
					
					address1:{
						required:true,
					},
					address2:{
						required:true,
					},
                  	country:{
						required:true,
					},
					state:{
						required:true,
					},
					city:{
						required:true,
					},
					zipcode:{
						//required:true,
						minlength:3,
						maxlength:10,
						ProtalURL:true,
						validate_addre:true
                    },

					'description[]': { 
				     minlength: 1,
				      maxlength: 31
				    },
				'productID[]':{
				    required: true,
				    minlength: 1
				},
				confirm_page_url: {
				    confirm_url: true
				},
                  freetrial:{
						required: true,
						digits:true,
						check_free:{
							sub_start_date:'#sub_start_date',
							paycycle:'#paycycle',
							duration_list:'#duration_list',
						}
					}
                    	
					
                }
               
            });
	
		
		   
         $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
         }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    			
		 $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
		
		
		$.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
      

					
				
		 $.validator.addMethod('check_free', function(value, element, params) {  
			
		  var duration = $(params.duration_list).val();
		  
		 // alert(duration);
		  if(duration =='0')return true;
		  
		  var frequency = $(params.paycycle).val();
		  var stdate     = new  Date( Date.parse($(params.sub_start_date).val()));
		  
		  var free = value;
		//  var result  =  get_frequncy_val(duration,new Date(stdate), frequency);
			
		  return (duration > free);
		}, 'Value should be less than Recurrence value');
		$.validator.addMethod("validPlanurl", function (value, element) {
                return /^[a-zA-Z0-9_]+$/.test(value);
             }, "Please enter a valid plan url");					
          $.validator.addMethod("confirm_url", function (value, element) {
                return /^(ftp|http|https):\/\/[^ "]+$/.test(value);
             }, "Please enter a valid url like as: https://merchantsite.com");					
							

        }
    };
}();
   
   
   
/**********Check the Validation for free trial**********************/
	function weeksBetween(d1, d2) {
		
     return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
	
   }
   function daysbeween(d1, d2) {
		
     return Math.round((d2 - d1) / (24 * 60 * 60 * 1000));
	
   }
   
   function get_months(d1, d2){
	 

return difference = (d2.getFullYear()*12 + d2.getMonth()) - (d1.getFullYear()*12 + d1.getMonth()); 
	   
   }  
   
       function get_frequncy_val(du, new1date, fr){
		   var res='';
		 
		       var CurrentDate =  new Date(new1date.getFullYear(), new1date.getMonth(), new1date.getDate(), 0, 0, 0, 0); 
				  CurrentDate.setMonth(CurrentDate.getMonth() + parseInt(du));
                  var newdate = new Date(CurrentDate);
				
		 if(fr=='dly'){
           res= daysbeween(new Date(new1date), new Date(newdate));
		 }else if(fr=='1wk'){
			  res= weeksBetween(new Date(new1date), new Date(newdate));
		 }else if(fr=='2wk'){
           res= weeksBetween(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='mon'){
           res= get_months(new Date(new1date), new Date(newdate));
		 }else if(fr=='2mn'){
         res= get_months(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='qtr'){
          res= get_months(new Date(new1date), new Date(newdate))/3;
		 }else if(fr=='six'){
           res= get_months(new Date(new1date), new Date(newdate))/6;
		 }else if(fr=='yr1'){
          res= get_months(new Date(new1date), new Date(newdate))/12;
		 }else if(fr=='yr2'){
		 res= get_months(new Date(new1date), new Date(newdate))/24;
		 }else if(fr=='yr3'){
			 res= get_months(new Date(new1date), new Date(newdate))/36;
		 }	
           return res;    			 
		   
	   } 
	   
/************End*******************/	   
	
   
   function chk_payment(r_val)  {  
   
     
      if(r_val=='1'){
		  $('#set_pay_data').show();
	  }else{
		$('#set_pay_data').hide();
      }	  

   }   
 
   
	function select_plan_val(rid){
		
		
		
		var itemID = $('#productID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>company/SettingSubscription/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['FullName']);
             $('#unit_rate'+rid).val(roundN(item_data['SalesPrice'],2));
             $('#quantity'+rid).val(1);
             $('#total'+rid).val(roundN(($('#quantity'+rid).val()*$('#unit_rate'+rid).val()),2));
             
	    	var grand_total=0;
		$( ".total_val" ).each(function(){
			
              var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format22(grand_total));
			}	
		});
		
	}
	
	function set_unit_val(rid){
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
		$('#total'+rid).val(total.toFixed(2));
		
		//$(total_val)
		var grand_total=0;
		$( ".total_val" ).each(function(){
			
              var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format22(grand_total));
	}
	
	function set_qty_val(rid){
       
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val(); 
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax;   
		$('#total'+rid).val(total.toFixed(2));
			//$(total_val)
		var grand_total=0;
		$( ".total_val" ).each(function(){ 
				var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format22(grand_total));
		
	}	
	
	function set_tax_val(mythis, rid){
		
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = $('#tax_check'+rid).val();
  
		if(mythis.checked){
			var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
			$('#total'+rid).val(total.toFixed(2));
		}
		else {
			var total  = qty*rate; 
			$('#total'+rid).val(total.toFixed(2));
		}
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
		   var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
		});
			
		$('#grand_total').html(format22(grand_total));
	}
	
	
	 
$(document).ready(function(){

        $('.force-numeric').keydown(function(e)
        {
            
   var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
		
		
		$('input.float').bind('keypress', function() {
			  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
			});
  

});


	
	 function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
	  
	  
function IntegerAndDecimal(e,obj,isDecimal)
{
    if ([e.keyCode||e.which]==8) //this is to allow backspace
    return true;

    if ([e.keyCode||e.which]==46) //this is to allow decimal point
    {
      if(isDecimal=='true')
      {
        var val = obj.value;
        if(val.indexOf(".") > -1)
        {
            e.returnValue = false;
            return false;
        }
        return true;
      }
      else
      {
        e.returnValue = false;
        return false;
      }
    }

    if ([e.keyCode||e.which] < 48 || [e.keyCode||e.which] > 57)
    e.preventDefault? e.preventDefault() : e.returnValue = false; 
}



function isNumberKeys(evt)
           {
               var charCode = (evt.which) ? evt.which : event.keyCode
 
               if (charCode == 46)
               {
                   var inputValue = $("#inputfield").val()
                   if (inputValue.indexOf('.') < 1)
                   {
                       return true;
                   }
                   return false;
               }
               if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
               {
                   return false;
               }
               return true;
           }



  function roundN(num,n){
  return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
}  
   
function format22(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

 // return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
} 


function myFunction() {
    
      var form = $("#form-validation");
                    // Getting token from the response json.
                 var pl_url = $('#pl_href_link').attr("href");
                    $('<input>', {
                            'type': 'text',
                            'id': 'myInput',
                            'value': pl_url,
                        }).appendTo(form);
                        
                 if($('#form-validation').find('#myInput'))
                 {
                    var copyText =     $('#form-validation').find('#myInput');
                    
//  var copyText = document.getElementById("myInput");
     copyText.select();
     document.execCommand("Copy");
     $('#form-validation #myInput').remove();
   }
}
   
	
	</script>
