        <!-- Page content -->
<?php
	$this->load->view('alert');
    
    if($isPlanVT){
        $displayClass = 'hidden';
    }else{
        $displayClass = '';
    }
?>
			<div id="page-content">
			       <div><?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
		?> 
			 <?php 
						$error = $this->session->flashdata('error');
						if(isset($error) && $error != ""){
						echo getmessage($error);
						}
					?>
		
        </div>
		<!-- Forms General Header -->
            
    <legend class="leg">Customer Portal <span style="font-weight: 400;">(coming soon)</span></legend>
    <div class="block full">
        <!-- Working Tabs Title --> 
        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                <div class="">
                    <!-- Block Tabs Title -->
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    <div class="">
                        <div class="tab-pane active" id="portal_setting">
                            <form id="form-portal" action="<?php echo base_url(); ?>company/SettingConfig/setting_customer_portal" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="type">Enable Customer Portal?</label>
                                    <div class="col-md-7">
                                
                                        <select id="enable" name="enable" class="form-control">
                                            <option value="0" <?php if(isset($setting) && !empty($setting) && $setting['customerPortal']=='0'){ echo "selected"; } ?>  >No</option>
                                            <option value="1" <?php if(isset($setting) && !empty($setting) && $setting['customerPortal']=='1'){ echo "selected"; } ?> >Yes</option>
                                            
                                        </select>  
                                        
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="portal_url">Your Customer Portal URL</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                        
                                            <input type="text" id="portal_url" name="portal_url"  value="<?php if(isset($setting) && !empty($setting)){ echo $setting['portalprefix']; } ?>"   class="form-control">
                                            
                                            <span class="input-group-addon"> <?php echo CUS_PORTAL; ?> </span>
                                    
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="customer_help_text">Customer Help Text</label>
                                    <div class="col-md-7">
                                        <textarea id="customer_help_text" name="customer_help_text"  class="form-control"><?php if(isset($setting) && !empty($setting) && $setting['customerHelpText']!=''){ echo $setting['customerHelpText'];  } ?></textarea>
                                        
                                    </div>
                                </div>
                                <div class="form-group <?php echo $displayClass; ?>">
                                    <label class="col-md-4 control-label" for="type">Show "My Info" tab</label>
                                    <div class="col-md-7">
                                        <select id="myInfo" name="myInfo" class="form-control">
                                            <option value="1"  <?php if(isset($setting) && !empty($setting) && $setting['showInfoTab']=='1'){ echo "selected"; } ?> >Yes</option>
                                            <option value="0"  <?php if(isset($setting) && !empty($setting) && $setting['showInfoTab']=='0'){ echo "selected"; } ?> >No</option>
                                              
                                        </select>  
                                            
                                    </div>
                                </div>
                                <div class="form-group <?php echo $displayClass; ?>">
                                    <label class="col-md-4 control-label" for="type">Show "My Password" tab</label>
                                    <div class="col-md-7">
                                  
                                        <select id="myPass" name="myPass" class="form-control">
                                            <option value="1"  <?php if(isset($setting) && !empty($setting) && $setting['showPassword']=='1'){ echo "selected"; } ?> >Yes</option>
                                            <option value="0"  <?php if(isset($setting) && !empty($setting) && $setting['showPassword']=='0'){ echo "selected"; } ?>  >No</option>
                                              
                                        </select>  
                                            
                                    </div>
                                </div>
                                <div class="form-group <?php echo $displayClass; ?>">
                                    <label class="col-md-4 control-label" for="type">Show "My Subscriptions" tab</label>
                                    <div class="col-md-7">
                                  
                                        <select id="mySubscriptions" name="mySubscriptions" class="form-control">
                                            <option value="1"  <?php if(isset($setting) && !empty($setting) && $setting['showSubscription']=='1'){ echo "selected"; } ?> >Yes</option>
                                            <option value="0"  <?php if(isset($setting) && !empty($setting) && $setting['showSubscription']=='0'){ echo "selected"; } ?> >No</option>
                                              
                                        </select>  
                                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="type">Terms of Service URL</label>
                                    <div class="col-md-7">
                                        <input type="url" id="service_url" name="service_url"  value="<?php if(isset($setting) && !empty($setting)){ echo $setting['serviceurl']; } ?>"   class="form-control">
                                        
                                    </div>
                                </div> 
                    
                
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="example-file-input">Upload Logo (Recommended size: <?php echo  LOGOWIDTH; ?>px x <?php echo  LOGOHEIGHT; ?>px)</label>
                                    <div class="col-md-7">
                                        <input type="file" id="upload_logo" name="picture" class="upload" ><img src="<?php if(isset($setting) && !empty($setting['ProfileImage'])){ echo  base_url().LOGOURL.$setting['ProfileImage'];   }else{ echo CZLOGO; } ?>" >
                                            
                                    </div>
                                </div>                
                                
                                <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                                    
                                    </div>
                                </div>  
                                
                           </form>
                        </div>
                    </div>
                    <!-- END Tabs Content -->
                </div>
                <!-- END Block Tabs -->
            </div>
           
        </div>
        <!-- END Working Tabs Content -->
    </div>
     
   </div> 
   <br>
   <br>
   
<script>


$(function(){       
   var message ='This field is required'
    $('#form-portal').validate({ // initialize plugin
		ignore:":not(:visible)",

       errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
		
		rules: {
		       'customer_help_text': {
                         minlength: 3,
                         validate_char:true,
                     },
                    'service_url': {
                         required: false,
	                	},	
					'portal_url':{
						 required : true,
						 minlength: 3,
					
						 remote : {
						     
						        beforeSend:function() { 
					      	 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					       },
					           complete:function() {
						              $(".overlay1").remove();
					             },
                                url: "<?php echo base_url(); ?>firstlogin/check_url",
                                type: "POST",
                                
                                dataType: 'json',
                                dataFilter: function(response) {
                                    
                                    var rsdata = jQuery.parseJSON(response);
                                 
                                     if(rsdata.status=='success')
                                     return true;
                                     else{
                                          message = rsdata.portal_url;
                                     return false;
                                     }
                                }
                            },
					}			
				 
					
				 
			
			},
				messages:
            {
                portal_url:
                {
                       required: "This is required",
                      minlength: "Please enter minimun 3 characters",
                      remote: function(){ return message; }
                },
                service_url: {
                  required: "Please enter valid URL.(i.e. https://)" ,
	        	}
            },
			
			
    });
    
    
    
 $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-._@()+, ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
  
});  
</script>

