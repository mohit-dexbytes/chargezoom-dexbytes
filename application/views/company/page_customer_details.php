<!-- Page content -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">

<?php
    $this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data1"><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Customer Content -->
    <div class="row">
        <div class="col-lg-4">
            <!-- Customer Info Block -->
            <legend class="leg">Customer Profile</legend>
            <div class="block">
                <!-- Customer Info Title -->
              
                <!-- END Customer Info Title -->

                <!-- Customer Info -->
                <div class="block-section text-center">
                    <div class="user-info pull-left">
                        <div class="profile-pic">
                            <?php if ($customer->profile_picture != "") { ?>

                                <img src="<?php echo base_url(); ?>uploads/customer_pic/<?php echo $customer->profile_picture; ?>" />
                            <?php } else { ?>
                                <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar4@2x.jpg" />
                            <?php } ?>
                            <div class="layer">
                                <div class="loader"></div>
                            </div>
                            <a class="image-wrapper" href="#">
                                <input class="hidden-input" id="changePicture" name="profile_picture" type="file">
                                <label class="edit fa fa-pencil" for="changePicture" type="file" title="Change picture"></label>

                            </a>
                        </div>

                        <input type="hidden" id="cr_url" value="<?php echo current_url(); ?>" />
                    </div>
                    <h3 class="customer-name">
                        <strong><?php echo $customer->FullName; ?></strong>

                    </h3>
                    <a href="<?php echo base_url('company/MerchantUser/create_customer/' . $customer->ListID); ?>" class="btn pull-lft btn-sm btn-success btn-edit-customer batch_button_color"> Edit Customer </a>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>

                        
                       
                        <?php if ($this->session->userdata('logged_in') || in_array('Send Email', $this->session->userdata('user_logged_in')['authName'])) { ?>
                            <tr>
                                <td class="text-left"><strong>Email</strong></td>

                                <td class="cust_view"><a href="#set_tempemail" onclick="set_template_data_company('<?php echo $customer->ListID ?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->Contact; ?>','<?php echo $merchantEmailID; ?>')" title="Send Email" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo $customer->Contact; ?></a></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td class="text-left"><strong>Phone</strong></td>
                            <td><?php echo $customer->Phone; ?></td>
                        </tr>
                        <tr>
                            <td class="text-left"><strong>Primary Contact</strong></td>
                            <td><?php echo $customer->FirstName . ' ' . $customer->LastName; ?></td>
                        </tr> 

                        <tr>
                            <td class="text-left"><strong>Added On</strong></td>
                            <td><?php echo date('M d, Y - h:i A', strtotime($customer->TimeCreated)); ?></td>
                        </tr>
                        <tr>
                            <td class="text-left"><strong>Status</strong></td>
                            <td>
                                <?php
                                if ($customer->IsActive == 'true') {
                                ?>
                                    <a href="#del_cust" onclick="company_customerList_id('<?php echo $customer->ListID; ?>')" data-backdrop="static" data-keyboard="false" data-toggle="modal" class="active_btn common_btn fs-12p">Active</a>
                                <?php
                                } else {
                                ?>
                                    <a href="#active_cust" onclick="company_customerList_active_id('<?php echo $customer->ListID; ?>')" data-backdrop="static" data-keyboard="false" data-toggle="modal" class="deactive common_btn fs-12p">Inactive</i></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left"><strong>Payment Info</strong></td>
                            <?php if ($this->session->userdata('logged_in') || in_array('Create Card', $this->session->userdata('user_logged_in')['authName'])) { ?>
                                <td> <a href="#card_data_process" class="btn btn-sm btn-success" onclick="set_card_user_data('<?php echo $customer->ListID; ?>', '<?php echo $customer->FullName; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
                                <?php }
                            if ($this->session->userdata('logged_in') || in_array('Modify Card', $this->session->userdata('user_logged_in')['authName'])) {   ?>
                                    <a href="#card_edit_data_process" class="btn btn-sm btn-info" data-backdrop="static" data-keyboard="false" data-toggle="modal">View/Update</a>
                                <?php } ?>
                                </td>
                        </tr>

                        
                    </tbody>
                </table>
                <!-- END Customer Info -->
            </div>
            <?php  
                $BillingAdd = 0;
                $ShippingAdd = 0;

                $isAdd = 0;
                if($customer->BillingAddress_Addr1  || $customer->BillingAddress_Addr2 || $customer->BillingAddress_City|| $customer->BillingAddress_State|| $customer->BillingAddress_PostalCode || $customer->BillingAddress_Country){
                    $BillingAdd = 1;
                    $isAdd = 1;
                }
                if($customer->ShipAddress_Addr1  || $customer->ShipAddress_Addr2 || $customer->ShipAddress_City|| $customer->ShipAddress_State|| $customer->ShipAddress_PostalCode || $customer->ShipAddress_Country){
                    $ShippingAdd = 1;
                    $isAdd = 1;
                }

            ?>
            <?php if($isAdd){ ?>
            <legend class="leg">Addresses</legend>
            <div class="">
                <!-- Customer Addresses Title -->
                
                <!-- END Customer Addresses Title -->
                
                <!-- Customer Addresses Content -->
                <div class="row">
                    <?php if($BillingAdd){ ?>
                    <div class="col-lg-12">
                        <!-- Shipping Address Block -->
                        <div class="block">
                            <!-- Shipping Address Title -->
                            <div class="block-title" id="add_title">
                                <h2 class="address-heading-h2">Billing Address</h2>
                            </div>
                            <!-- END Shipping Address Title -->

                            <!-- Shipping Address Content -->
                            <h5><strong><?php echo $customer->FirstName . ' ' . $customer->LastName; ?></strong></h5>
                            <address>
                                <strong></strong>
                                <?php echo ($customer->BillingAddress_Addr1) ? $customer->BillingAddress_Addr1.'<br>' : ''; ?>
                                <?php echo ($customer->BillingAddress_Addr2) ? $customer->BillingAddress_Addr2.'<br>' : ''; ?>
                                <?php echo ($customer->BillingAddress_City) ? $customer->BillingAddress_City . ',' : ''; ?>
                                <?php echo ($customer->BillingAddress_State) ? $customer->BillingAddress_State : ''; ?>
                                <?php echo ($customer->BillingAddress_PostalCode) ? $customer->BillingAddress_PostalCode.'<br>' : ''; ?>
                                 <?php echo ($customer->BillingAddress_Country) ? $customer->BillingAddress_Country.'<br>' : ''; ?>
                                
                                
                            </address>
                            <!-- END Shipping Address Content -->
                        </div>
                        <!-- END Shipping Address Block -->
                    </div>
                <?php }
                if($ShippingAdd){ ?>

                    <div class="col-lg-12">
                        <!-- Billing Address Block -->
                        <div class="block">
                            <!-- Billing Address Title -->
                            <div class="block-title" id="add_title">
                                <h2 class="address-heading-h2">Shipping Address</h2>
                            </div>
                            <!-- END Billing Address Title -->

                            <!-- Billing Address Content -->
                            <h5><strong><?php echo $customer->FirstName . ' ' . $customer->LastName; ?></strong></h5>
                            <address>
                                <strong></strong>
                                <?php echo ($customer->ShipAddress_Addr1) ? $customer->ShipAddress_Addr1.'<br>'  : ''; ?>
                                <?php echo ($customer->ShipAddress_Addr2) ? $customer->ShipAddress_Addr2.'<br>'  : ''; ?> 
                                <?php echo ($customer->ShipAddress_City) ? $customer->ShipAddress_City . ',</br>' : ''; ?>
                                <?php echo ($customer->ShipAddress_State) ? $customer->ShipAddress_State : ''; ?>
                                <?php echo ($customer->ShipAddress_PostalCode) ? $customer->ShipAddress_PostalCode.'<br>'  : ''; ?>
                                <?php echo ($customer->ShipAddress_Country) ? $customer->ShipAddress_Country.'<br>'  : ''; ?>
                                
                            </address>
                            <!-- END Billing Address Content -->
                        </div>
                        <!-- END Billing Address Block -->
                    </div>
                 <?php } ?>   
                </div>
            </div>
        <?php } ?>
            
                <!-- END Customer Addresses Content -->
            </div>

      



        
        <div class="col-lg-8">
            <div class="block-options pull-right">
                <a href="#recurring_payment" class="btn btn-sm btn-info" data-backdrop="static" data-keyboard="false" data-toggle="modal" onclick="set_recurring_payment('<?php echo $customer->ListID; ?>',5);">Recurring Payments</a>
            </div>
        </div>
        <div class="col-lg-8">
            <!-- Orders Block -->
            <legend class="leg">Invoices</legend>
           
            <div class="outstand_invoice">
                <!-- Orders Title -->
               
                <table id="invoice_page" class="table ecom-orders table-bordered table-striped table-vcenter">

                    <thead>
                        <tr>
                            <th class="text-left">Invoice</th>

                            <th class="hidden-xs text-right">Due Date</th>
                            <?php if(empty(getenv('SHOW_INVOICE_CHECKBOX'))){ ?>
                                <th class="text-right hidden-xs">Payment</th>
                            <?php } ?>
                            <th class="text-right hidden-xs">Balance</th>
                            <th class="text-right">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <br>
                <!-- END Orders Content -->
            </div>
            <!-- END Orders Block -->

            <!-- load transaction history table Start -->

            <?php 
                $history_data = [
                    'transactions' => isset($transaction_history_data) ? $transaction_history_data : [],
                    'void_url' => 'company/AuthPayment/delete_pay_transaction',
                    'refund_url' => 'company/refundInvoice/create_payment_refund',
                    'customer_url' => 'company/home/view_customer/',
                    'invoice_url' => 'company/home/invoice_details/',
                    'merchant_type' => 5,
                    'plantype' => $plantype,
                    'login_info' => $login_info,
                    'isCustomerPage' => true
                ]; 
                $this->load->view('common_transaction_history', $history_data); 
            ?>
            <br>
            <!-- load transaction history table End -->

            <?php if($showSubscriptionList){ ?>
            <legend class="leg">Subscriptions</legend> 
           
            <div class="" style="position: relative">
                <!-- Products in Cart Title -->
                
                <a href="<?php echo base_url(); ?>company/SettingSubscription/create_subscription" class="btn pull-lft  btn-sm btn-success subs-btn" > Add New </a>
              


                <!-- All Orders Content -->
                <table id="subs" class="table subs table-bordered ecom-orders table-striped table-vcenter">
                    <thead>
                        <tr>

                            <th class="text-left hidden-xs">Plan</th>
                            <th style="display:none;" class="text-left hidden-xs">Card</th>
                            <th class="text-right ">Amount</th>
                            <th class="text-right hidden-xs">Next Charge</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($getsubscriptions)) {


                            foreach ($getsubscriptions as $getsubscription) {

                        ?>

                                <tr>

                                    
                                    <td class="text-left hidden-xs"><?php echo $getsubscription['sub_planName']; ?></td>

                                    <td style="display:none;" class="text-left hidden-xs"><?php echo $getsubscription['customerCardfriendlyName']; ?> </td>

                                    

                                    <td class="text-right ">$<?php echo ($getsubscription['subscriptionAmount']) ? number_format($getsubscription['subscriptionAmount'], 2) : '0.00'; ?></td>

                                    <td class="hidden-xs text-right"> <span class="hidden"><?php echo $getsubscription['nextGeneratingDate']; ?></span> <?php echo date('M d, Y', strtotime($getsubscription['nextGeneratingDate'])); ?></td>


                                    <td class="text-center">
                                       
                                        <div class="btn-group dropbtn">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                            <ul class="dropdown-menu text-left">
                                                <li><a href="<?php echo base_url('company/SettingSubscription/create_subscription/' . $getsubscription['subscriptionID']); ?>" data-toggle="tooltip" title="Edit Subscription" class=""> Edit</a></li>
                                                <li> <a href="#del_subs" onclick="set_subs_id('<?php echo $getsubscription['subscriptionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Delete Subscription" class="">Delete</a>
                                            </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                        <?php  }
                        }         ?>

                    </tbody>

                </table>
                <br>

            </div>
            <?php } ?>

            <!-----------------  END --------------------->

            <?php if ($this->session->userdata('logged_in') || in_array('Send Email', $this->session->userdata('user_logged_in')['authName'])) { ?>


                <?php if($showEmailList){ ?>

                <!-----------------   to view email history  ------------------------->
                <legend class="leg">Email History</legend>
               
                <div class="" style="position: relative">
                    <a href="#set_tempemail" class="btn btn-sm btn-info subs-btn" onclick="set_template_data_company('<?php echo $customer->ListID ?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->Contact; ?>')" title="" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Send Email</a>
                    <table id="email_hist" class="table table-bordered table-striped ecom-orders table-vcenter">

                            <thead>
                                
                                <th class="text-left"><strong>Subject</strong></th>
                                <th class="text-right"><strong>Date</strong> </th>
                               <th class="text-right"><strong>Status</strong> </th>
                            </thead>
                            <tbody>

                                <?php if (!empty($editdatas)) {


                                    foreach ($editdatas as $editdata) {
                                        if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                                        $timezone = ['time' => $editdata['emailsendAt'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                                        $editdata['emailsendAt'] = getTimeBySelectedTimezone($timezone);
                                    }
                                ?>
                                        <tr>
                                            
                                            
                                            <td class="text-left cust_view"> <a href="#view_history" title="View Details" onclick="set_view_history('<?php echo $editdata['mailID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <?php echo $editdata['emailSubject']; ?> </a> </td>
                                            
                                            <td class="text-right"><span class="hidden"><?php echo $editdata['emailsendAt']; ?></span><?php echo date('M d, Y h:i A', strtotime($editdata['emailsendAt'])); ?> </td>
                                            
                                            <td class="text-right cust_view">
                                                <span>
                                                    <?php
                                                        if($editdata['send_grid_email_status'] != 'Delivered'){
                                                            echo $editdata['send_grid_email_status'];
                                                        }else{
                                                    ?>
                                                            <a href="#sendgrid_email_detail_popup" onclick="getSendGridEmailStatusDetail('<?php echo $editdata['mailID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <?php echo $editdata['send_grid_email_status']; ?> </a>
                                                    <?php } 
                                                    ?>
                                                </span>
                                            </td>
                                        </tr>
                                <?php }
                                }

                                ?>

                            </tbody>
                        </table>
                        <br>
                        
                    </div>
                <!-- END Products in Cart Block -->

                <?php } ?>
            <?php } ?>
            
            <!-- END Customer Addresses Block -->

            
            <?php if ($this->session->userdata('logged_in') || in_array('Add Note', $this->session->userdata('user_logged_in')['authName'])) { ?>

                <!-- Private Notes Block -->
                <legend class="leg">Private Notes</legend>
                <div class="block full">
                    <!-- Private Notes Title -->
                   
                    <!-- END Private Notes Title -->

                    <!-- Private Notes Content -->
                    <div class="alert alert-info">
                        <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
                    </div>
                    <label for="">Add Note</label>
                    <form method="post" id="pri_form" onsubmit="return false;">
                        <div class="form-group">
                            <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="Your Note."></textarea>
                            <input type="hidden" name="customerID" id="customerID" value="<?php echo $customer->ListID; ?>" />
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-12 padding-right-0px">
                                <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success pull-right">Add Note</button>
                            </div>
                        </div>
                        
                        <br>
                        <br>
                        <br>
                    </form>
                   
                    <?php if (!empty($notes)) {
                        foreach ($notes as $note) { 
                            if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                                $timezone = ['time' => $note['privateNoteDate'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                                $note['privateNoteDate'] = getTimeBySelectedTimezone($timezone);
                            }
                    ?>
                        <div class="singlenotes">
                            <div>
                                <?php echo $note['privateNote']; ?>
                            </div>
                            <div class="align-right">
                                <span>Added on <strong><?php echo date('M d, Y - h:i A', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
                                <span><a href="javascript:void(0);" onclick="delele_notes('<?php echo $note['noteID']; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
                            </div>
                        </div>
                    <?php }
                    } ?>


                   
                    <!-- END Private Notes Content -->
                </div>

            <?php } else{ ?>
                <input type="hidden" name="customerID" id="customerID" value="<?php echo $customer->Customer_ListID ; ?>" />
             <?php } ?>
            <!-- END Private Notes Block -->
        </div>
    </div>
    <!-- END Customer Content -->
</div>
<style>
    #invoice_page_filter label{
        position: absolute;
        z-index: 9;
        left: -5px !important;
    }
    #invoice_page_filter label > select{
        margin-left: 5px;
    }
    #transaction_history_pay_page_wrapper >.row >.col-sm-6.col-xs-7 {
        left: 65px !important;
    }
    #email_hist_wrapper >.row >.col-sm-6.col-xs-7 {
        left: 50px !important;
    }
    #subs_wrapper >.row >.col-sm-6.col-xs-7 {
        left: 50px !important;
    }
    #invoice_page_wrapper .col-xs-5, #invoice_page_wrapper .col-sm-11{
        padding: 0;
    }
    
    @media screen and  (max-width : 1199px){
        #invoice_page_filter label{
            left: -10px !important;
        }
    }

    @media screen and  (max-width : 1500px){
        #invoice_page_filter label{
            left: 2px !important;
        }
    }

    @media screen and  (min-width : 1600px){
        #invoice_page_filter label{
            left: -25px !important;
        }
    }
</style>
<!-- END Page Content -->
<script type="text/javascript">
    var InvoiceCount = "<?php echo count($invoices); ?>";
</script>
<script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(JS); ?>/pages/customer_details_company.js?v=1.0"></script>
<script src="<?php echo base_url(JS); ?>/pages/chargezoom_subscription.js"></script>
<script src="<?php echo base_url(JS); ?>/company/customer.js?v=1.0"></script>