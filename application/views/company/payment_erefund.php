<style>
	.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
		left: 270px !important;
	}
</style>
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- Form Validation Example Block -->
			    <?php echo $this->session->flashdata('message');   ?>
  
    <!-- END Forms General Header -->

     
        
            
	
    <legend class="leg">Refund</legend>
	<div class="full">
	
	    
        <!-- Form Validation Example Title -->
        <table id="erefund_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs">Invoice</th>
                    <th class="text-right">Amount</th>					
                    <th class="text-right hidden-xs hidden-sm">Date</th>
		
					 <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                   
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
    			<?php 
    			if(isset($transactions) && $transactions)
    			{
				
    				foreach($transactions as $transaction)
    				{
                        $inv_url1='';
                        if(!empty($transaction['invoice_id']) && !empty($transaction['invoice_no']) )
                        {
                            $invs = explode(',',$transaction['invoice_id']);

                            $invoice_no = explode(',',$transaction['invoice_no']);

                            foreach($invs as $k=> $inv)
                            {
                              $inv_url = base_url().'home/invoice_details/'.trim($inv);
                              if(isset($invoice_no[$k])){
                                  if($plantype){
                                      $inv_url1.= $invoice_no[$k].',';  

                                  }else{
                                      $inv_url1.=' <a href="'.$inv_url.'">'.$invoice_no[$k]. '</a>,';  

                                  }
                                   
                              }    
                            }
                        $inv_url1 = substr($inv_url1, 0, -1);

                        }else{
                          $inv_url1.='<a href="javascript:void(0);">---</a> ';
                        }
    			?>
				<tr>
					
					<?php if($plantype) { ?>
					<td class="text-left"><?php echo $transaction['FullName']; ?></td>
    				<?php } else { ?>
    					<td class="text-left cust_view"><a href="<?php echo base_url();?>company/home/view_customer/<?php echo $transaction['customerListID'] ; ?> "><?php echo $transaction['FullName']; ?></a></td>
    				<?php } ?>
                    <td class="text-right cust_view"><?php echo $inv_url1; ?></td>
				    
					<td class="hidden-xs text-right cust_view">
                        <?php
                          $showRefund = 0;
                          if($transaction['partial']==$transaction['transactionAmount'])
                          {

                              echo "<label class='label label-success'>Fully Refunded: ".'$'.number_format($transaction['partial'],2)."</label><br/>";
                          }
                          else if($transaction['partial'] !='0' )
                          { 
                            $showRefund = 1;
                            echo "<label class='label label-warning'>Partially Refunded: ".'$'.number_format($transaction['partial'],2)." </label><br/>";
                          }
                          else
                          { 
                            $showRefund = 1;
                            echo "";
                             
                          }

                          ?>
                        <a href="#pay_data_process"   onclick="get_payment_transaction_data('<?php  echo $transaction['transactionID']; ?>','company');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$<?php echo($transaction['transactionAmount'])? number_format($transaction['transactionAmount'], 2):'0.00'; ?></a>
                    </td>			
					
					<td class="hidden-xs hidden-sm text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					
					<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'';   ?></td>
						
					
				    <td class="text-center">
                        <a href="javascript:void(0)" id="txnRefund<?php  echo $transaction['transactionID']; ?>" transaction-id="<?php  echo $transaction['transactionID']; ?>" transaction-gatewayType="<?php echo $transaction['transactionGateway'];?>" transaction-gatewayName="<?php echo $transaction['gateway'];?>" integration-type="5" class="refunAmountCustom btn btn-sm btn-warning"   data-backdrop="static" data-keyboard="false" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice" data-toggle="modal">Refund</a>
           
					</td>
				</tr>
				<?php } }
				else { echo'<tr><td colspan="6"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                </tr>'; }  
				?>
			</tbody>
        </table>
    </div>
    
    	
	
</div>

    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
     <script>$(function(){ Pagination_view.init(); });</script>
    <script>
    var Pagination_view = function() {
    
        return {
            init: function() {
                / Extend with date sort plugin /
                $.extend($.fn.dataTableExt.oSort, {
               
                } );
    
                / Initialize Bootstrap Datatables Integration /
                App.datatables();
    
                / Initialize Datatables /
                $('#erefund_page').dataTable({
                    columnDefs: [
                        { type: 'date-custom', targets: [4] },
                        { orderable: false, targets: [5] }
                    ],
                    order: [[ 3, "desc" ]],
                    pageLength: 10,
                    lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
                });
    
                / Add placeholder attribute to the search input /
               $('.dataTables_filter input').attr('placeholder', '');
            }
        };
    }();
    
    
    
    </script>
	<script>
    	
        function set_refund_pay_common(txnid, txntype){
    		if(txnid !=""){
                $('#txnID').val(txnid);	 
    		}
    	}   
	    
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
        }
		
		
    
    function set_void_pay(txnid, txntype){
    		
   
          if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnvoidID').val(txnid);	 
				 var url   = "<?php echo base_url()?>company/Payments/payment_evoid";
				}	
                else if(txntype=='2'){
                    $('#txnvoidID1').val(txnid);
                    var url   = "<?php echo base_url()?>company/AuthPayment/payment_evoid";
                }	
                else if(txntype=='3'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/PaytracePayment/payment_evoid";
                }	
                else if(txntype=='10'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/iTransactPayment/payment_evoid";
                }
                else if(txntype=='11'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/FluidpayPayment/payment_evoid";
                }
                else if(txntype=='12'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/TSYSPayment/payment_evoid";
                }
                else if(txntype=='15'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/PayarcPayment/payment_evoid";
                } else if(txntype=='13'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/BasysIQProPayment/payment_evoid";
                } else if(txntype=='17'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/MaverickPayment/payment_evoid";
                
                } else if(txntype=='16'){
                    $('#txnvoidID').val(txnid);
                    var url   = "<?php echo base_url()?>company/EPXPayment/payment_evoid";
                }   
			
			 $("#data_form11").attr("action",url);	
    		}
			
			
    	}
	    
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
        }
        
	</script>
	
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
	
	 <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>company/refundInvoice/create_payment_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
							   
								 <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
								 <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
<div id="payment_voidmod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Void Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form11" method="post" action='<?php echo base_url(); ?>company/Payments/payment_evoid' class="form-horizontal" >
                         
                        <p id="message_data">Do you really want to void this transaction? The payment will be dropped if you click "Void" below.</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
								  <input type="hidden" id="txnvoidID" name="txnvoidID" class="form-control"  value="" />
								  <input type="hidden" id="txnvoidID1" name="txnvoidID1" class="form-control"  value="" />
								
                            </div>
                        </div>
                        
    					<div class="pull-right">
            			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                        <button type="button" class="btn btn-sm btn-priamry1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                
    			    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
		
