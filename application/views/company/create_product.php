   <!-- Page content -->
<?php
	$this->load->view('alert');
?>
	<div id="page-content">
	
    <!-- END Wizard Header -->
    <!-- Progress Bar Wizard Block -->
    <legend class="leg"> <?php if(isset($item_pro)){ echo "Edit Product";}else{ echo "Create New Product"; }?></legend>
    <div class="block">
	               <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;

					?>
        
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form  id="product_form" method="POST" 
			class="form form-horizontal" action="<?php echo base_url(); ?>company/MerchantUser/create_product">
			
			 <input type="hidden"  id="productID" name="productID" value="<?php if(isset($item_pro)){echo $item_pro['ListID']; } ?>" /> 
			  <input type="hidden"  id="EditSequence" name="EditSequence" value="<?php if(isset($item_pro)){echo $item_pro['EditSequence']; } ?>" /> 
			
			    <div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Name</label>
					<div class="col-md-7">
						<input type="text" id="productName" name="productName" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['Name']; } ?>"><?php echo form_error('productName'); ?>
					</div>
				</div>
			   
					
					
				<?php $k=0; ?>
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-type">Type </label>
					<div class="col-md-7">
							<select id="type" class="form-control " name="type">
							<option value="">Select Type</option>
						
						   <option value="Service" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Service') ) echo"Selected" ?> >Service</option>
						   <option value="NonInventory" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('NonInventory') ) echo"Selected" ?> >Non Inventory</option>
                           <option value="OtherCharge" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('OtherCharge') ) echo"Selected" ?> >Other Charge</option>
                           <option value="Group" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Group') ) echo"Selected" ?> >Group</option>
                           <option value="SubTotal" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('SubTotal') ) echo"Selected" ?> >Sub Total</option>
                           <option value="Discount" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Discount') ) echo"Selected" ?> >Discount</option>
                           <option value="Payment" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Payment') ) echo"Selected" ?> >Payment</option>
							</select><?php echo form_error('type'); ?>
						</div>
					</div>
					
			
					
					<div class="form-group" id="averageCost_div" <?php if(isset($item_pro) && (strtoupper($item_pro['Type'])==strtoupper('Discount') ||  strtoupper($item_pro['Type'])==strtoupper('Payment') || strtoupper($item_pro['Type'])==strtoupper('Group'))){  ?> style="display:none;" <?php } ?>  >
						<label class="col-md-3 control-label" for="example-username">Product Price </label>
						<div class="col-md-7">
							<input type="text" id="averageCost" name="averageCost"  class="form-control" value="<?php if(isset($item_pro)){ 
                            echo  sprintf('%0.2f',($item_pro['SalesPrice']) ) ;
                            } ?>"><?php echo form_error('averageCost'); ?>
						</div>
					</div>
						<div class="form-group" id="discount_div"  <?php if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Discount')  ){  ?> style="display:block;" <?php }else{ ?>  style="display:none;" <?php } ?> >
						<label class="col-md-3 control-label" for="example-username">Discount</label>
						<div class="col-md-7">
		<input type="text" id="discount" name="discount"  class="form-control" value="<?php if(isset($item_pro['Discount'])) echo $item_pro['Discount']; ?>" ><?php echo form_error('discount'); ?>
						</div>
					</div>
						<div class="form-group" id="qty" style="display:none;">
						<label class="col-md-3 control-label" for="example-username">Quantity</label>
						<div class="col-md-7">
							<input type="text" id="pquantity" name="pquantity"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['QuantityOnHand']; } ?>"><?php echo form_error('pquantity'); ?>
						</div>
					</div>
					<div class ="form-group"  id="parent_div" <?php if(isset($item_pro) && (strtoupper($item_pro['Type'])==strtoupper('Discount') ||  strtoupper($item_pro['Type'])==strtoupper('Payment')|| strtoupper($item_pro['Type'])==strtoupper('Group') )){  ?> style="display:none;" <?php } ?> >
					   <label class="col-md-3 control-label" for="example-typeahead">Select Parent</label>
					   <div class="col-md-7">
							<select id="productParent" class="form-control " name="productParent">
							<option value="">Select Parent</option>
							<?php foreach($products as $product){  ?>
							   <option value="<?php echo $product['ListID']; ?>" <?php  if(isset($item_pro) && $product['ListID']=$item_pro['Parent_ListID'] ) echo"Selected" ?> > <?php echo $product['FullName']; ?> </option>
							   
							<?php } ?>  
							
							</select><?php echo form_error('productParent'); ?>
						</div>
					</div>	
				    	
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-type">Product Description </label>
					<div class="col-md-7">
					<textarea id="proDescription" name="proDescription" class="form-control"  ><?php if(isset($item_pro)){echo $item_pro['SalesDesc']; } ?><?php echo form_error('proDescription'); ?> </textarea></div>
					</div>
					<div class ="form-group" id="account_div" <?php if(isset($item_pro) && (strtoupper($item_pro['Type'])==strtoupper('Discount') ||  strtoupper($item_pro['Type'])==strtoupper('Payment')|| strtoupper($item_pro['Type'])==strtoupper('Group')) ){  ?> style="display:none;" <?php } ?> >
						   <label class="col-md-3 control-label" for="productAccount">Select Account</label>
						   <div class="col-md-7">
								<select id="productAccount" name="productAccount" class="form-control"  >
								<option value="">Select Account</option>
								<?php foreach($accounts as $account){  ?>
								   <option value="<?php echo $account['ListID']; ?>" ><?php echo $account['Name'].' / '. $account['AccountType'] ; ?></option>
								   
								<?php } ?>  
								</select><?php echo form_error('productAccount'); ?>
							</div>
                        </div>		
							
							<!-- Group Type Product Begins -->
                            <div  id="group_div"   <?php if(isset($item_pro) && (strtoupper($item_pro['Type'])==strtoupper('Group')) ){   ?> style="display:block;" <?php }else{ ?> style="display:none;" <?php } ?> >
                                 <div id="item_fields">
						
                               	<?php

								// SHow Existing Group Prroducts
							   	if(isset($item_data) && !empty($item_data))
                                {
									foreach($item_data as $k=>$group_d)
									{  
								?>
								  	<div class="form-group  removeclass<?php echo $k+1; ?>">
                                 		<div class="col-sm-3 ">
                                 			<div class="nopadding">&nbsp;</div>
										</div>
										<div class="col-sm-2 nopadding">
										    <div class="">
												<select class="form-control"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="prodID<?php echo $k+1; ?>" name="prodID[]">
								  					<option>Select Product & Service</option>
													<?php foreach ($items as $k1=> $item){  ?> 
													<option  <?php if($group_d['itemListID']==$item['ListID']){ echo "selected='selected'"; } ?> value="<?php echo $item['ListID'] ?>"><?php echo $item['Name'];  ?></option> 
													<?php } ?>
								   				</select>
											</div>
                                 		</div>
                                 		<div class="col-sm-2 nopadding">
										 	<div class="">
											 	<input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo  $group_d['FullName']; ?>" >
											</div>
										</div>
										<div class="col-sm-1 nopadding">
											<div class=" text-center">
								   				<input type="text" class="form-control float text-center" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo  $group_d['Price']; ?>"  >
											</div>
										</div>
										<div class="col-sm-1 nopadding">
											<div class=" text-center">
												<input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity" name="quantity[]" value="<?php  echo $group_d['Quantity']; ?>" >
											</div>
										</div>							   
								   
								   
								   		<div class="col-sm-2 nopadding">
								   			<div class="">
									   			<div class="input-group">
								   					<div class="input-group-btn">
													   <button class="btn btn-danger" type="button" onclick="remove_product_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button>
													</div>
												</div>
											</div>
										</div> 
										<div class="clear"></div>
									</div>
                                 
                                <?php  
									} 
								} else { 
								?>
                                	<div class="form-group  removeclass1">
                                 		<div class="col-sm-3 ">
                                 			<div class="nopadding">
                                 			&nbsp;
                                 			</div>
										</div>
										<div class="col-sm-2 nopadding">
                                 			<div class="">
								  				<select class="form-control"  onchange="select_plan_val('1');"  id="prodID1" name="prodID[]">
								  					<option>Select Product & Service</option>
													<?php foreach ($items as $k1=> $item){  ?>
													<option value="<?php echo $item['ListID'] ?>"><?php echo $item['Name'];  ?></option>
													<?php } ?>
								   				</select>
								   			</div>
                                 		</div>
                                 
								 		<div class="col-sm-2 nopadding">
										 	<div class="">
											 	<input type="text" class="form-control" id="description1" name="description[]" value="" >
											</div>
										</div>
									    <div class="col-sm-1 nopadding">
											<div class=" text-center">
								   				<input type="text" class="form-control float text-center" id="unit_rate1" name="unit_rate[]" value="" >
											</div>
										</div>
								   	    <div class="col-sm-1 nopadding">
										   	<div class=" text-center">
											   	<input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center"  id="quantity1" name="quantity[]" value="" >
											</div>
										</div>
										<div class="col-sm-2 nopadding">
											<div class="">
												<div class="input-group">
													<div class="input-group-btn">
														<button class="btn btn-danger" type="button" onclick="remove_product_fields('1');"> <span class="fa fa-times" aria-hidden="true"></span></button>
													</div>
												</div>
											</div>
										</div>
										<div class="clear"></div></div>
                                 </div>
                                <?php } ?>
                               
                                <div class="form-group">
									<label class="control-label "></label>						  
									<div class="col-md-9 col-md-offset-3">
										<button class="btn btn-sm btn-success" type="button"  onclick="item_fields();"> Add More </button>
						       		</div>
                        		</div>
                            </div>
							<!-- Group Type Product Ends -->
					<div class="form-group">
						<div class="col-md-10 text-right">
							<button type="submit" class="submit btn btn-sm btn-success">Save</button>
							<?php 
								if(isset($item_pro)){
									$product_id = $item_pro['ListID'];
									if($item_pro['IsActive'] == 'false'){
							?>
										<a href='#active_product' onclick='company_set_productList_active_id("<?php echo $product_id; ?>");' data-backdrop='static' data-keyboard='false' data-toggle='modal' class='btn btn-sm btn-info'>Activate</a>
							<?php	}else{ ?>
										<a href='#deactive_product' class='btn btn-sm btn-danger' onclick='company_del_product_id("<?php echo $product_id; ?>");'  data-backdrop='static' data-keyboard='false' data-toggle='modal'>Deactivate</a>
							<?php	}
								}
							?>
											
		                    <a href="<?php echo base_url(); ?>company/home/plan_product"  class="submit btn btn-sm btn-primary1">Cancel</a>	
						</div>
				    </div>		
          	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->
</div></div>


<!-- END Page Content -->


<script>

$(document).ready(function(){

                   
                    
 $('#product_service').addClass('active');
 
    $('#product_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'fullName': {
                        required: true,
                         minlength: 2,
                         maxlength: 100,
                         validate_char:true,
                    },
                    'company': {
                        required: true,
                        maxlength: 41,
                        validate_char:true,
                    },
					 'type': {
                        required: true,
                       
                    },
					 
					 'productName': {
                        required: true,
                        minlength: 3,
                        maxlength: 25,
                        validate_char:true,
                    },
					
					'averageCost':{
					      required:true,
						  number: true,
						  
					},
					'quantityonhand':{
						required:true,
						  digits: true,
					},
					'proDescription':{
					  required: true,
                        minlength: 3,
                        maxlength: 41,
					},
					'productParent':{
                     required: false
                     }
					
			},
    });
    
        $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
    
         
	$('#type').change(function(){
    
    
      if($(this).val()=='Discount'){
      $('#discount_div').show();
    $('#averageCost_div').hide();
      $('#account_div').show();
       $('#parent_div').show();
        $('#group_div').hide();
      }else if($(this).val()=='SubTotal' || $(this).val()=='Payment' ){
        $('#averageCost_div').hide();
        $('#discount_div').hide();
       $('#account_div').hide();
       $('#parent_div').hide();
        $('#group_div').hide();
      }else if($(this).val()=='Group'){
       $('#group_div').show();
       $('#averageCost_div').hide();
        $('#discount_div').hide();
       $('#account_div').hide();
       $('#parent_div').hide();
      }else{
    $('#discount_div').hide();
        $('#group_div').hide();
    $('#averageCost_div').show();
       $('#account_div').show();
       $('#parent_div').show();
    }
    
    });
                    
                    
                    
                    
     
    
});	

  <?php  if($k >=1){  ?>

     var room ='<?php echo $k+1; ?>';
     <?php }else{ ?>
  var room=1;
  <?php } ?>
     function item_fields()
	{ 
	   
       var type='group';
                
          $.ajax({
           
          url:'<?php echo base_url(); ?>company/MerchantUser/get_plan_data_item',
          type:"POST",
          data:{type:type},
          success:function(data){
        
          var plan_data = $.parseJSON(data); 
        
        
        
		room++;
	
		var plan_html ='<option val="">Select Product or Service</option>';
		for(var val in  plan_data) 
        {  
           plan_html+='<option value="'+ plan_data[val]['ListID']+'">'+plan_data[val]['Name']+'</option>'; 
        }
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class=""></div></div><div class="col-sm-2 nopadding"><div class=""><select class="form-control"  onchange="select_plan_val('+room+');"  id="prodID'+room+'" name="prodID[]">'+ plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class=""> <input type="text" class="form-control" id="description'+room+'" name="description[]" value=""></div></div><div class="col-sm-1 nopadding"><div class=""> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate'+room+'" name="unit_rate[]" value="" ></div></div><div class="col-sm-1 nopadding"><div class=""> <input type="text" onkeypress="return isNumberKey(event)" class="form-control"  id="quantity'+room+'" name="quantity[]" value=""></div></div>  	     <div class="col-sm-2 nopadding"><div class=""><div class="input-group"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_product_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest);
        
       }
          
            }); 
  }
  
  
   function remove_product_fields(rid)
   {
	
	   $('.removeclass'+rid).remove();
	   
   }
	function select_plan_val(rid)
	{
		
		
		
		var itemID = $('#prodID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>company/SettingSubscription/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['Name']);
             $('#unit_rate'+rid).val(item_data['SalesPrice']);
			 $('#quantity'+rid).val(item_data['QuantityOnHand']);
			
			}	
		});
		
	}

</script>

</div>


