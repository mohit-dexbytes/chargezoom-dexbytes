

<style>
.recieptPage label{
    font-weight: 800 !important;
}
.recieptPage .row{
    padding-bottom: 10px !important;
}
.recieptPage .first-row{
    padding-bottom: 0px !important;
}
.recieptPage .success{
    color: #3FBF3F;
}
.recieptPage .faild{
    color: #cf4436;
}
.recieptPage a{
    color: #167bc4;
}
.recieptPage hr{
    margin: 10px;
}
.recieptPage p {
    margin-bottom: 0px !important;
    color: #000;
}
.transaction-print-btn{
    font-size: 13px !important;
}
@media print {
   #DivIdToPrint {
        font-size: 11pt;     
       font-family: Consolas;
       padding: 0px;
       margin: 0px;
    }
}
</style>
<?php 
    $transactionAmount = isset($transactionAmount) && ($transactionAmount != null)?number_format($transactionAmount, 2):number_format(0,2);
    $surchargeAmount = isset($surchargeAmount) && ($surchargeAmount != null)?number_format($surchargeAmount, 2):number_format(0, 2);

    $totalAmount = isset($totalAmount) && ($totalAmount != null)?number_format($totalAmount, 2):number_format(0, 2);

    $isSurcharge = isset($isSurcharge) && ($isSurcharge != null)?$isSurcharge:0;
?>
<div id="page-content" class="recieptPage">
    
    <!-- Products Block -->
        <div class="block">
        <!-- Products Title -->
            <div class="block-title">
                <h2><?php echo $page_data['sub_header'];?></h2>
            </div>
            <div class="row first-row">
                <div class="col-md-6">
                    <h4><strong>Transaction Receipt</strong></h4>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-sm btn-info pull-right" href="<?php echo base_url().$page_data['proccess_url'];?>"><?php echo $page_data['proccess_btn_text'];?></a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <?php if($this->session->flashdata('success') != null){
                        ?><h5><strong class="success">Transaction Successful</strong></h5>
                    <?php }else{ 
                        $message = strip_tags($this->session->flashdata('message'));
                        if(isset($page_data['checkPlan']) && !$page_data['checkPlan']){
                            $message = "Transaction Failed - Transaction limit has been reached. <a href= '".base_url()."company/home/my_account'>Click here</a> to upgrade.";
                        }
                        ?>

                        <h5><strong class="faild"><?php echo $message;?></strong></h5>
                 <?php   } ?>
                    
                </div>
            </div> 
            <div class="row">
                <div class="col-md-4 font-14">
                    <label class="amount_label_font">Amount: </label><span class="amount_label_font"> $<?php echo $transactionAmount; ?></span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <?php 
                    $date = date("Y-m-d H:i:s");
                    if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                        $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                        $date = getTimeBySelectedTimezone($timezone);
                    
                    } ?>
                    <label>Date: </label> <strong><?php echo date("m/d/Y h:i A", strtotime($date));?></strong>
                </div>
                <div class="col-md-4">
                    <label>Transaction ID:  </label> <span> <?php echo $transaction_id;?></span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <label>IP Address: </label><span> <?php echo $Ip;?></span>
                </div>
                <div class="col-md-8">
                    <label>User: </label><span> <?php echo $name.' ('.$email.')';?></span>
                </div>
            </div>
            <?php 
                $invoice_id = '';
                $custom_data = (isset($transactionDetail) && isset($transactionDetail['custom_data_fields'])) ? $transactionDetail['custom_data_fields'] : false;
                $po_number = '';
                $payment_type = '';
                if($custom_data){
                    $json_data = json_decode($custom_data, 1);
                    if(isset($json_data['invoice_number'])){
                        $invoice_id = $json_data['invoice_number'];
                    }
                    if(isset($json_data['po_number'])){
                        $po_number = $json_data['po_number'];
                    }

                    if(isset($json_data['payment_type'])){
                        $payment_type = $json_data['payment_type'];
                    }
                }
            ?>
            <div class="row"> 
                <div class="col-md-4">
                    
                    <?php if($this->session->userdata('vt_plan') == '0'){?>
                            <label>Invoice(s): </label><span><a href="<?php echo base_url()?>company/home/invoice_details/<?php echo $invoice;?>"><?php echo $invoice_number;?></a></span>
                        <?php } else { ?>
                            <label>Invoice(s): </label><span><?php echo $invoice_number;?></span>
                        <?php } ?>
                </div>
                <div class="col-md-8">
                    <label>Payment Type: </label><span> <?php echo $payment_type; ?></span><br>
                </div>
            </div>
            <?php if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-8">
                    
                    <label>Surcharge Amount: </label><span> $<?php echo  $surchargeAmount; ?></span><br>

                </div>
            </div>
            <?php }
            if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-8">
                    
                     <label>Total Amount: </label><span> $<?php echo $totalAmount;?></span>

                </div>
            </div>
            <?php } ?>
        <?php  
            $BillingAdd = 0;
            $ShippingAdd = 0;

            $isAdd = 0;
            if($invoice_data['BillingAddress_Addr1']  || $invoice_data['BillingAddress_Addr2'] || $invoice_data['BillingAddress_City'] || $invoice_data['BillingAddress_State'] || $invoice_data['BillingAddress_PostalCode'] || $invoice_data['BillingAddress_Country']){
                $BillingAdd = 1;
                $isAdd = 1;
            }
            if($invoice_data['ShipAddress_Addr1']  || $invoice_data['ShipAddress_Addr2'] || $invoice_data['ShipAddress_City'] || $invoice_data['ShipAddress_PostalCode'] || $invoice_data['ShipAddress_State'] || $invoice_data['ShipAddress_Country'] ){
                $ShippingAdd = 1;
                $isAdd = 1;
            }

        ?>  
        <!-- Addresses -->
        <div class="row">
        <?php if($isAdd){ ?>
            <?php if($BillingAdd){ ?>
            <div class="col-sm-4">
                <!-- Billing Address Block -->
                <div class="block">
                    <!-- Billing Address Title -->
                    <div class="block-title">
                        <h2>Billing Address</h2>
                    </div>
                    <h4><span>
                            <?php if ($customer_data['FirstName'] != '') {
                                echo $customer_data['FirstName'] . ' ' . $customer_data['LastName'];
                            } else if(isset($invoice_data['Customer_FullName']) && $invoice_data['Customer_FullName'] != ''){
                                echo $invoice_data['Customer_FullName'];
                            } else {
                                echo '';
                            }
                            ?>
                        </span></h4>
                    <address>
                        

                        <?php if ($invoice_data['BillingAddress_Addr1'] != '') {
                            echo $invoice_data['BillingAddress_Addr1'].'<br>'; } ?> 
                            
                            <?php if ($invoice_data['BillingAddress_Addr2'] != '') {
                              echo $invoice_data['BillingAddress_Addr2'].'<br>'; } else{
                                  echo '';
                              } ?>
                        
                        <?php echo ($invoice_data['BillingAddress_City']) ? $invoice_data['BillingAddress_City'] . ',' : ''; ?>
                        <?php echo ($invoice_data['BillingAddress_State']) ? $invoice_data['BillingAddress_State'] : ''; ?>
                        <?php echo ($invoice_data['BillingAddress_PostalCode']) ? $invoice_data['BillingAddress_PostalCode'].'<br>' : ''; ?> <br>
                        <?php echo ($invoice_data['BillingAddress_Country']) ? $invoice_data['BillingAddress_Country'].'<br>' : ''; ?> <br>
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Billing Address Content -->
                </div>
                <!-- END Billing Address Block -->
            </div>
            <?php }
            if($ShippingAdd){ ?>
            <div class="col-sm-4">
                <!-- Shipping Address Block -->
                <div class="block">
                    <!-- Shipping Address Title -->
                    <div class="block-title">
                        <h2>Shipping Address</h2>
                    </div>
                    <!-- END Shipping Address Title -->

                    <h4><span> <?php if ($customer_data['FirstName'] != '') {
                                        echo $customer_data['FirstName'] . ' ' . $customer_data['LastName'];
                                    } else if(isset($invoice_data['Customer_FullName']) && $invoice_data['Customer_FullName'] != ''){
                                        echo $invoice_data['Customer_FullName'];
                                    } else {
                                        echo '';
                                    }
                                    ?></span></h4>
                    <address>
                        <?php if ($invoice_data['ShipAddress_Addr1'] != '') {
                            echo $invoice_data['ShipAddress_Addr1'].'<br>'; } ?> 
                            
                            <?php if ($invoice_data['ShipAddress_Addr2'] != '') {
                              echo $invoice_data['ShipAddress_Addr2'].'<br>'; } else{
                                  echo '';
                              } ?>
                        


                        <?php echo ($invoice_data['ShipAddress_City']) ? $invoice_data['ShipAddress_City'] . ',' : ''; ?>
                        <?php echo ($invoice_data['ShipAddress_State']) ? $invoice_data['ShipAddress_State'] : ''; ?>
                        <?php echo ($invoice_data['ShipAddress_PostalCode']) ? $invoice_data['ShipAddress_PostalCode'].'<br>' : ''; ?> 
                        <?php echo ($invoice_data['ShipAddress_Country']) ? $invoice_data['ShipAddress_Country'].'<br>' : ''; ?> 
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Shipping Address Content -->
                </div>
                <!-- END Shipping Address Block -->
            </div>
            <?php } ?>
        <?php } ?>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="<?php echo base_url('ajaxRequest/printTransactionReceiptPDF') ?>" class="btn btn-primary transaction-print-btn" target="_blank">Print Receipt</a>
            </div>
        </div>
        <!-- END Addresses -->
        <div id='DivIdToPrint' style="display:none;">
            <p>Transaction Receipt</p>
            <br>
            <p>Transaction Type: Sale</p>
            <p>Transaction Status: <?php if($this->session->flashdata('success')){
                        ?>Successful
                    <?php }else{ ?>
                        Decline
                 <?php   } ?></p>
            <p>Amount: $<?php echo  $transactionAmount;?></p>
            <?php if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
                <p>Surcharge Amount: $<?php echo  $surchargeAmount;?></p>
                <p>Total Amount: $<?php echo  $totalAmount;?></p>
            <?php } ?>
            <p>Date: <?php echo date("m/d/Y h:i A", strtotime($date)); ?></p>
            <p>Transaction ID: <?php echo $transaction_id;?></p>
            <p>Invoice(s): <?php echo $invoice_number;?></p>
            <br>
            <?php if($BillingAdd){ ?>
                <p>Billing Address</p>
                <p><?php if ($customer_data['FirstName'] != '') {
                    echo $customer_data['FirstName'] . ' ' . $customer_data['LastName'];
                } else if(isset($invoice_data['Customer_FullName']) && $invoice_data['Customer_FullName'] != ''){
                    echo $invoice_data['Customer_FullName'];
                } else {
                    echo '';
                }
                ?></p>
                <p> <?php if ($invoice_data['BillingAddress_Addr1'] != '') {
                    echo $invoice_data['BillingAddress_Addr1']; } ?> 
                </p>
                <p><?php echo ($invoice_data['BillingAddress_City']) ? $invoice_data['BillingAddress_City'] . ',' : ''; ?>
                <?php echo ($invoice_data['BillingAddress_State']) ? $invoice_data['BillingAddress_State'] : ''; ?>
                <?php echo ($invoice_data['BillingAddress_PostalCode']) ? $invoice_data['BillingAddress_PostalCode'] : ''; ?> </p>
                <p><?php echo ($invoice_data['BillingAddress_Country']) ? $invoice_data['BillingAddress_Country'] : ''; ?> </p>
                
                <p>
                <?php echo $customer_data['Phone'];    ?></p>
                <p><?php echo $customer_data['Contact'];    ?>
                </p>
                <br>
            <?php }
            if($ShippingAdd){ ?>

                <p>Shipping Address</p>
                <p><?php if ($customer_data['FirstName'] != '') {
                    echo $customer_data['FirstName'] . ' ' . $customer_data['LastName'];
                } else if(isset($invoice_data['Customer_FullName']) && $invoice_data['Customer_FullName'] != ''){
                    echo $invoice_data['Customer_FullName'];
                } else {
                    echo '';
                }
                ?></p>
                <p> <?php if ($invoice_data['ShipAddress_Addr1'] != '') {
                    echo $invoice_data['ShipAddress_Addr1']; } ?> 
                </p>
                <p><?php echo ($invoice_data['ShipAddress_City']) ? $invoice_data['ShipAddress_City'] . ',' : ''; ?>
                <?php echo ($invoice_data['ShipAddress_State']) ? $invoice_data['ShipAddress_State'] : ''; ?>
                <?php echo ($invoice_data['ShipAddress_PostalCode']) ? $invoice_data['ShipAddress_PostalCode'] : ''; ?> </p>
                <p><?php echo ($invoice_data['ShipAddress_Country']) ? $invoice_data['ShipAddress_Country'] : ''; ?> </p>
               
                <p>
                <?php echo $customer_data['Phone'];    ?></p>
                <p><?php echo $customer_data['Contact'];    ?>
                </p>
            <?php } ?> 
        </div>

