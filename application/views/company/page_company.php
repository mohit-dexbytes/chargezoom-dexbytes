<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>

    <!-- All Orders Block -->
    <legend class="leg">Accounting Package</legend>
    <div class="full">
        <!-- All Orders Title -->
        <div class="addNewFixRight viewLogOverride">
            
      
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="company" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">Accounting Package</th>
                 
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($companies) && $companies)
				{
					foreach($companies as $company)
					{
				?>
				<tr>
					<td class="text-left cust_view"><a href="#company_data" onclick="view_company_details('<?php echo $company['id']; ?>');" data-toggle="modal" data-backdrop="static" data-keyboard="false"><?php echo "Without Accounting Package"; // $company['companyName']; ?></a></td>
						
				
					
					<td class="text-center">
                    NA
					
					</td>
				</tr>
				
				<?php } }else { echo'<tr>
                <td colspan="2"> No Records Found </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#company').dataTable({
                columnDefs: [
                    { orderable: false, targets: [1] }
                ],
                searching: false,
                bInfo: false,
                paging: false,
              
            });

            / Add placeholder attribute to the search input /
        }
    };
}();


function set_del_reseller(id){
	
	  $('#resellerID').val(id);
	
}


function view_company_details(id){
	 
	
	 if(id!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>company/home/get_company_details',
		  data : {'id':id},
		  success: function(data){
			  
			     $('#data_form_company').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 }


</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 

</div>

<div id="company_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">App Details</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_company"  style="height: 300px; min-height:300px;  overflow: auto; " >
			
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- END Page Content -->