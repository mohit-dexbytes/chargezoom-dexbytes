<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <!-- Form Validation Example Block -->
			    <?php echo $this->session->flashdata('message');   ?>
 
    <!-- END Forms General Header -->

     
        
            
	

	<div class="block full">
	    <div class="block-title">
             <h2><strong>Refunds</strong> </h2>
             </div>
	    
        <!-- Form Validation Example Title -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs">Invoice</th>
                    <th class="text-right">Amount</th>					
                    <th class="text-right hidden-xs hidden-sm">Date</th>
					<th class="text-right hidden-xs">Type</th>
					 <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                    <th class="hidden-xs hidden-sm text-right">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
    			<?php 
    			if(isset($transactions) && $transactions)
    			{
					
					
    				foreach($transactions as $transaction)
    				{
    			?>
				<tr>
					
					
					<td class="text-left"><?php echo $transaction['fullName']; ?></td>
					
				    <td class="text-right hidden-xs"><?php echo ($transaction['invoiceID'])?$transaction['invoiceID']:'-----'; ?></td>
					
					<td class="text-right">$<?php echo($transaction['transactionAmount'])? number_format($transaction['transactionAmount'], 2):'0.00'; ?></td>			
					
					<td class="hidden-xs hidden-sm text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right">
					     <?php if (strpos($transaction['transactionType'], 'sale') !== false) { 
                            echo "Sale";
                        }else if(strpos($transaction['transactionType'], 'auth') !== false){
                             echo "Authorization";
                         }else if(strpos($transaction['transactionType'], 'capture') !== false){
                             echo "Authorization";
                         }
                         ?>
                            
					</td>
					<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'';   ?></td>
					<?php if( $transaction['transactionCode']=="1"  || $transaction['transactionCode']=="111"|| $transaction['transactionCode']=="100" || $transaction['transactionCode']=="200"  ){ ?>
					<td class="text-right hidden-xs hidden-sm"><span style="color:#888888;"><?php echo "Success"; ?></span></td>
					<?php }else{ ?>
					
						<td class="text-right hidden-xs hidden-sm"><span style="color:#888888;">
						<?php echo $transaction['transactionStatus']; ?></span></td>
				    <?php } ?>	
					
				    <td class="text-center">
					      <a href="javascript:void(0)" id="txnRefund<?php  echo $transaction['transactionID']; ?>" transaction-id="<?php  echo $transaction['transactionID']; ?>" transaction-gatewayType="<?php echo $transaction['transactionGateway'];?>" transaction-gatewayName="<?php echo $transaction['gateway'];?>" integration-type="4" class="refunAmountCustom btn btn-sm btn-warning"   data-backdrop="static" data-keyboard="false" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice" data-toggle="modal">Refund</a>                          
					</td>
				</tr>
				<?php } } ?>
			</tbody>
        </table>
    </div>
    
    	
	
</div>

    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
    <script>$(function(){ EcomOrders.init(); });</script>

	<script>
    	
    function set_refund_pay(txnid, txntype){
    	
			
    		if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnID').val(txnid);	 
				 var url   = "<?php echo base_url()?>Xero_controllers/Payments/payment_erefund";
				}	
			else if(txntype=='2'){
    		    $('#txnIDrefund').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/payment_erefund";
			}	
		
			 $("#data_form").attr("action",url);	
    		}
    	}   
	    
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
           $(".msg_data").fadeOut('slow');
        }
		
		
    
    function set_void_pay(txnid, txntype){
    		
   
          if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnvoidID').val(txnid);	 
				 var url   = "<?php echo base_url()?>Xero_controllers/Payments/payment_evoid";
				}	
			else if(txntype=='2'){
    		    $('#txnvoidID1').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/payment_evoid";
			}	
			
			 $("#data_form11").attr("action",url);	
    		}
			
			
    	}
	    
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
           $(".msg_data").fadeOut('slow');
        }
        
	</script>
	
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
	
	 <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>Xero_controllers/Payments/payment_erefund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund Now" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
							   
								 <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
								 <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
<div id="payment_voidmod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Void Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form11" method="post" action='<?php echo base_url(); ?>Xero_controllers/Payments/payment_evoid' class="form-horizontal" >
                         
                        <p id="message_data">Do you really want to void this transaction? The payment will be dropped if you click "Void Now" below.</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
								  <input type="hidden" id="txnvoidID" name="txnvoidID" class="form-control"  value="" />
								  <input type="hidden" id="txnvoidID1" name="txnvoidID1" class="form-control"  value="" />
								
                            </div>
                        </div>
                        
    					<div class="pull-right">
            			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                        <button type="button" class="btn btn-sm btn-priamry1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                
    			    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
		
