<!-- Page content -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    
    <!-- Customer Content -->
    <div class="row">
        <div class="col-lg-4">
            <!-- Customer Info Block -->
          
            <legend class="leg">Customer Profile</legend>
            
            <div class="block">
              
                <!-- Customer Info -->
                <div class="block-section text-center">
                   <div class="user-info pull-left">
						<div class="profile-pic">
								<?php if($customer->profile_picture!=""){ ?> 
          
								<img src="<?php echo base_url(); ?>uploads/customer_pic/<?php echo $customer->profile_picture; ?>" />
                                <?php }else{ ?>
                                <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar4@2x.jpg" />
                                <?php } ?>
							 <div class="layer">
								<div class="loader"></div>
							 </div>
							<a class="image-wrapper" href="#">
								<input class="hidden-input" id="changePicture" name="profile_picture" type="file">
								<label class="edit fa fa-pencil" for="changePicture" type="file" title="Change picture"></label>
							
							</a>
						</div>
						
						<input type="hidden" id="cr_url" value="<?php echo current_url(); ?>" />
						</div>   
                    <h3 class="customer-name">
                        <strong><?php echo $customer->fullName; ?></strong>
                     
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                        
                        <tr>
                            <td class="text-left">Primary Contact</td>
                            <td><?php echo $customer->firstName.' '.$customer->lastName; ?></td>
                        </tr>

                        <tr>
                            <td class="text-left">Added On</td>
                            <td><?php echo date('M d, Y - h:m A', strtotime($customer->updatedAt)); ?></td>
                        </tr>
                       
                       <?php if($this->session->userdata('logged_in')|| in_array('Send Email',$this->session->userdata('user_logged_in')['authName'])  ){ ?> 
                        <tr>
                            <td class="text-left">Email</td>
                           
                            <td class="cust_view"><a href="#set_tempemail" onclick="set_template_data('<?php echo $customer->Customer_ListID; ?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->userEmail; ?>')" title="Send mail" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo $customer->userEmail; ?></a></td>
                        </tr>
                             <?php } ?>
                        <tr>
                            <td class="text-left">Phone</td>
                            <td><?php echo $customer->phoneNumber; ?></td>
                        </tr>
                        <tr>
                            <td class="text-left">Status</td>
                            <td> Active</td>
                        </tr>
                        <tr>
                             <td class="text-left">Payment Info</td>
                             <?php if($this->session->userdata('logged_in')|| in_array('Create Card',$this->session->userdata('user_logged_in')['authName'])  ){ ?>
                            <td> <a href="#card_data_process" class="btn btn-sm btn-success"  onclick="set_card_user_data('<?php  echo $customer->Customer_ListID; ?>', '<?php  echo $customer->fullName; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
                             <?php }if($this->session->userdata('logged_in')|| in_array('Modify Card',$this->session->userdata('user_logged_in')['authName']) ){   ?>
                             <a href="#card_edit_data_process" class="btn btn-sm btn-info"   data-backdrop="static" data-keyboard="false" data-toggle="modal">View/Update</a>
                             <?php } ?>
                            </td>
                        </tr>

                        
                    </tbody>
                </table>
                <!-- END Customer Info -->
            </div>

            <?php  
                $BillingAdd = 0;
                $ShippingAdd = 0;

                $isAdd = 0;
                if($customer->address1  || $customer->address2 || $customer->City|| $customer->State|| $customer->zipCode || $customer->Country){
                    $BillingAdd = 1;
                    $isAdd = 1;
                }
                if($customer->ship_address1  || $customer->ship_address2 || $customer->ship_city|| $customer->ship_state || $customer->ship_zipcode || $customer->ship_country){
                    $ShippingAdd = 1;
                    $isAdd = 1;
                }

            ?>
            <?php if($isAdd){ ?>
            <legend class="leg">Addresses</legend>
            <div class="">
                <!-- Customer Addresses Title -->
               
                <!-- END Customer Addresses Title -->

                <!-- Customer Addresses Content -->
                <div class="row">
                    <?php if($BillingAdd){ ?>
                    <div class="col-lg-12">
                        <!-- Shipping Address Block -->
                        <div class="block">
                            <!-- Shipping Address Title -->
                            <div class="block-title" id="add_title">
                                <h2 class="address-heading-h2">Billing Address</h2>
                            </div>
                            <!-- END Shipping Address Title -->
                           <?php if(isset($customer->address1) && $customer->address1 != ""){ ?>    
                            <!-- Shipping Address Content -->
                            <h5><strong><?php echo $customer->fullName; ?></strong></h5>
                          <address>
                                <strong></strong>
                                <?php echo ($customer->address1)?$customer->address1.'<br>' :''; ?>
                                 <?php echo ($customer->address2)?$customer->address2.'<br>':''; ?> 
                                 <?php echo ($customer->City)?$customer->City.',':''; ?>
                                <?php echo ($customer->State)?$customer->State:''; ?> 
                                <?php echo ($customer->zipCode)?$customer->zipCode.'<br>':''; ?>
                                <?php echo ($customer->Country)?$customer->Country.'<br>':''; ?>
                                
                               
                            </address>
                            <?php } ?>
                            <!-- END Shipping Address Content -->
                        </div>
                        <!-- END Shipping Address Block -->
                    </div>
                    <?php }
                    if($ShippingAdd){ ?>
                    
                    <div class="col-lg-12">
                        <!-- Billing Address Block -->
                        <div class="block">
                            <!-- Billing Address Title -->
                            <div class="block-title" id="add_title">
                                <h2 class="address-heading-h2">Shipping Address</h2>
                            </div>
                            <!-- END Billing Address Title -->
                            <?php if(isset($customer->ship_address1) && $customer->ship_address1 != ""){ ?> 
                            <!-- Billing Address Content -->
                            <h5><strong><?php echo $customer->fullName; ?></strong></h5>
                             <address>
                                <strong></strong>
                                <?php echo ($customer->ship_address1)?$customer->ship_address1.'<br>':''; ?>
                                 <?php echo ($customer->ship_address2)?$customer->ship_address2.'<br>':''; ?>
                                 <?php echo ($customer->ship_city)?$customer->ship_city.',':'';?>
                                <?php echo ($customer->ship_state)?$customer->ship_state:'' ?> 
                                <?php echo ($customer->ship_zipcode)?$customer->ship_zipcode.'<br>':''; ?>
                                <?php echo ($customer->ship_country)?$customer->ship_country.'<br>':''; ?>
                                
                                
                               
                            </address>
                             <?php } ?>
                            <!-- END Billing Address Content -->
                        </div>
                        <!-- END Billing Address Block -->
                    </div>
                    <?php } ?>
                </div>
                <!-- END Customer Addresses Content -->
            </div>
            <?php } ?> 
             
        
            <!-- END Customer Info Block -->

            
            
        
        <div class="col-lg-8">
        <legend class="leg">Quick Stats</legend>



        <div class="quick_status">
            <div class="col-sm-6 col-lg-6 quick_1">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-success rounded-circle mr-2">
                            <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold">
                                    <span data-plugin="counterup">$
                                        <?php echo number_format($sum_invoice,2); ?>  
                                    </span>
                                </h4>
                                <p class="text-truncate">Invoice Value</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>
            <div class="col-sm-6 col-lg-6 quick_2">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-info rounded-circle mr-2">
                            <i class="fa fa-credit-card ion-logo-usd avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold">
                                    <span data-plugin="counterup">
                                        <?php echo $invoices_count;  ?>  
                                    </span>
                                </h4>
                                <p class="text-truncate">Invoices in Total</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>  
        </div>
        
        </div>
        <div class="col-lg-8">
            <!-- Orders Block -->
            <legend class="leg">Outstanding Invoices</legend>
            <div class="outstand_invoice">
                <!-- Orders Title -->
               
                 <?php   if(!empty($invoices)){  ?>
                     <div class=" block-options pull-right process_btn">
                        
                       <a href="#xero_invoice_multi_process" class="btn btn-sm btn-success batch_button_color"  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_xero_invoice_process_multiple('<?php echo $customer->Customer_ListID?>');">Batch Process</a>
                    
                    </div>
                    <?php } ?>
                
                
                <!-- Orders Content -->
                <table id="outstand" class="table table-bordered table-striped table-vcenter">
                    
                        <thead>
                            <th class="text-left" >Invoice</th>
                            <th class="hidden-xs text-right">Due Date</th>
                          
                              <th class="text-right">Amount</th>
                               <th class="text-right hidden-xs">Status</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php   if(!empty($invoices)){     foreach($invoices as $invoice){
                        
                         ?>
                           <tr>
                            <td class="text-left cust_view"> <a href="<?php echo base_url();?>Xero_controllers/Xero_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']?$invoice['refNumber']:'----'; ?></a></td>
                            <td class="hidden-xs text-right"><?php  echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
                          
                            <?php if($invoice['BalanceRemaining']!="0.00"){ ?>
                            <td class="hidden-xs text-right cust_view"><a href="#pay_data_process"   onclick="set_xero_payment_data('<?php  echo $invoice['invoiceID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$'.number_format(($invoice['BalanceRemaining']),2); ?></a></td>
						   <?php }
						   else{ ?>
							<td class="hidden-xs text-right cust_view"><a href="#"><?php  echo  '$'.number_format(($invoice['Total_payment']),2); ?></a></td>   
						   <?php } ?>
                            <td class="hidden-xs text-right">
                                <?php  echo $invoice['UserStatus']; ?>
                            </td>
                           
                             
                             
                             
                            <td class="text-center">                        
                                 <div class="">
                            <?php if($invoice['BalanceRemaining'] == 0){ ?>
                             <div class="btn-group dropbtn">
								     <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                   <ul class="dropdown-menu text-left">
                                  <li><a class="btn btn-sm btn-success" disabled>Process</a></li>
                                   <li> <a class="btn btn-sm btn-info" disabled>Schedule</a></li>
                                   </ul>
                                    </div>
                                   
                             <?php }else{ ?>
                             
                              <div class="btn-group dropbtn">
								     <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                   <ul class="dropdown-menu text-left">
                                    <li><a href="#fb_invoice_process" class=""  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_xero_invoice_process_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo
                             $invoice['CustomerListID']; ?>', '<?php echo $invoice['BalanceRemaining'] ; ?>');">Process</a></li>
                             <li> <a href="#xero_invoice_schedule" onclick="set_invoice_schedule_date_id('<?php  echo $invoice['refNumber']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                             <li> <a href="#xero_invoice_delete" onclick="get_invoice_id('<?php  echo $invoice['refNumber']; ?>','<?php  echo $invoice['UserStatus']; ?>')" data-keyboard="false" data-toggle="modal" class="">Void</a></li>
                             
                              </ul>
                                    </div>
                             <?php } ?>
                            
                     
                             

    
                                    
                        </div>          
                                 
                            </td>
                        </tr>
                           <?php  } } 
							
                        ?>

                        
                    </tbody>
                </table>
                <br>
                <!-- END Orders Content -->
            </div>
            <!-- END Orders Block -->

            <!-- Products in Cart Block -->
            <legend>Paid Invoices</legend>
            <div class="">
              
                <!-- Products in Cart Content -->
                 <table id="pinv" class="table  table-bordered table-striped table-vcenter">
                 
                  <thead>
                 
                        <th class="text-center"><strong>Invoice</strong></th>
                        
                        <th class="text-right hidden-xs">Amount</th>
                            <th style="display:none;" class="text-right hidden-xs">Payments</th>
                              <th style="display:none;" class="text-right">Balance</th>
                            <th class="text-right">Status</th>
                      
                </thead>
                    <tbody>
                      
                        
                        <?php   if(!empty($latest_invoice)){   

                        foreach($latest_invoice as $invoice){
                          
                         ?>
                        <tr>
                            	<td class="text-right cust_view"> <a href="<?php echo base_url();?>Xero_controllers/Xero_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']?$invoice['refNumber']:'----'; ?></a></td>
                           
                                <?php if($invoice['Total_payment']!="0.00"){ ?>
                            <td class="hidden-xs text-right cust_view"><a href="#pay_data_process"   onclick="set_xero_payment_data('<?php  echo $invoice['invoiceID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$'.number_format(($invoice['Total_payment']),2); ?></a></td>
						   <?php }
						   else{ ?>
							<td class="hidden-xs text-right cust_view"><a href="#"><?php  echo  '$'.number_format(($invoice['Total_payment']),2); ?></a></td>   
						   <?php } ?>
                            <td style="display:none;" class="hidden-xs text-right">$<?php  echo  number_format($invoice['Total_payment'],2); ?></td>   
                           
                             <td style="display:none;" class="text-right">$<?php  echo number_format($invoice['BalanceRemaining'],2); ?></td>
                             
                              <td class="text-right">
                           <?php if($invoice['BalanceRemaining'] == 0){ ?>
                            <span style="color:#23800f">Paid</span>
                        <?php } ?></td>
                        </tr>
                           <?php } }
							?>  
                        
                    
                        
                    </tbody>
                </table>
                <br>
                <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->


            
            <legend class="leg">Subscriptions<legend>

            <div class="" style="position: relative">
                <!-- Products in Cart Title -->
                <a href="#" class="btn pull-lft  btn-sm btn-success subs-btn"> Add New </a>
                    
               
                
                
        <!-- All Orders Content -->
         <table id="subs" class="table  table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                   
                    <th class="text-left hidden-xs">Gateway</th>
                    <th class="text-right ">Amount</th>
                    <th class="text-right hidden-xs">Next Charge</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
             <tbody>
             <?php   if(!empty($getsubscriptions)){   

                        
                        foreach($getsubscriptions as $getsubscription){ 
                          
                         ?>
            
                   <tr>
                  
                    
                  
                    <td class="text-left hidden-xs"><?php echo $getsubscription['gatewayFriendlyName']; ?>  </td> 
                    
                 
                    
                    <td class="text-right ">$<?php echo ($getsubscription['subscriptionAmount'])?number_format($getsubscription['subscriptionAmount'], 2):'0.00'; ?></td>
                   
                    <td class="hidden-xs text-right">    <span class="hidden"><?php echo $getsubscription['nextGeneratingDate']; ?></span><?php echo date('M d, Y', strtotime($getsubscription['nextGeneratingDate'])); ?></td>
                    
                
                    <td class="text-center">
                        <div class="btn-group btn-group-xs">
                            
                            
                          
                           
                        </div>
                        <div class="btn-group dropbtn">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                            <ul class="dropdown-menu text-left">
                                <li><a href="<?php echo base_url('Xero_controllers/SettingSubscription/create_subscription/'.$getsubscription['subscriptionID']); ?>" data-toggle="tooltip" title="Edit Subscription" class="">Edit</a></li>
                                <li>   <a href="#del_xero_subs" onclick="set_xero_subs_id('<?php  echo $getsubscription['subscriptionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Subscription" class="">Delete</a>
                            </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                
                <?php  } } 
			 ?>
                
            </tbody>
            
                </table>
                <br>
                
            </div>
<!-----------------  END --------------------->
            
<?php if($this->session->userdata('logged_in') || in_array('Send Email',$this->session->userdata('user_logged_in')['authName']) ) { ?>
             
             
            
             <!-----------------   to view email history  ------------------------->
             <legend class="leg">Email History</legend>
             <div style="position: relative">

                 <a href="#set_tempemail_xero" class="btn btn-sm btn-info subs-btn" onclick="set_template_data('<?php echo $customer->Customer_ListID?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->phoneNumber; ?>')" title="Send mail" data-backdrop="static" data-keyboard="false" data-toggle="modal">Send Mail</a>
             </div>
                     
             <div class="">
                 <!-- Products in Cart Title -->
                 
                 <!-- END Products in Cart Title -->
 
                 <!-- Products in Cart Content -->
                  <table id="email_hist11" class="table table-bordered table-striped ecom-orders table-vcenter">
                  
                   <thead>
                        <th class="text-left"><strong>Subject</strong></th>
                        <th class="text-right"><strong>Date</strong> </th>
                        <th class="text-right"><strong>Status</strong> </th>
                             
                 </thead>
                     <tbody>
                       
                         <?php   if(!empty($editdatas)){   
 
                         
                         foreach($editdatas as $editdata){ 
                            if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                                        $timezone = ['time' => $editdata['emailsendAt'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                                        $editdata['emailsendAt'] = getTimeBySelectedTimezone($timezone);
                            };
                          ?>
                         <tr>
                             
                            <td class="text-left cust_view"> <a href="#view_history" title="View Details" onclick="set_view_history('<?php echo $editdata['mailID'];?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <?php echo $editdata['emailSubject'];?> </a> </td>
                            <td class="text-right">     <span class="hidden"><?php echo $editdata['emailsendAt']; ?></span><?php echo date('M d, Y h:i A', strtotime($editdata['emailsendAt'])); ?> </td>

                            <td class="text-right cust_view">
                                <span>
                                    <?php
                                        if($editdata['send_grid_email_status'] != 'Delivered'){
                                            echo $editdata['send_grid_email_status'];
                                        }else{
                                    ?>
                                            <a href="#sendgrid_email_detail_popup" onclick="getSendGridEmailStatusDetail('<?php echo $editdata['mailID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <?php echo $editdata['send_grid_email_status']; ?> </a>
                                    <?php } 
                                    ?>
                                </span>
                            </td>
                            
                         </tr>
                            <?php } } 	?> 
                         
                     </tbody>
                 </table>
                 <br>
                 <!-- END Products in Cart Content -->
             </div>
             <!-- END Products in Cart Block -->
             
              <?php } ?>
            
            <!-- END Customer Addresses Block -->
            <?php if($this->session->userdata('logged_in')|| in_array('Add Note',$this->session->userdata('user_logged_in')['authName'] )  ) { ?>
            
            <!-- Private Notes Block -->
            <legend class="leg">Private Notes</legend>
            <div class="block full">
                <!-- Private Notes Title -->
                
                <!-- END Private Notes Title -->

                <!-- Private Notes Content -->
                <div class="alert alert-info">
                    <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
                </div>
                <label for="">Add Note</label>
                <form  method="post"  id="pri_form" onsubmit="return false;" >
                    <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="" data-args="Your note.."></textarea>
                    <input type="hidden" name="customerID" id="customerID" value="<?php echo $customer->Customer_ListID ; ?>" />
                    <br>
                    <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
                </form>
                        
                <?php if(!empty($notes)){  foreach($notes as $note){ 
                        if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                            $timezone = ['time' => $note['privateNoteDate'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                            $note['privateNoteDate'] = getTimeBySelectedTimezone($timezone);
                        }
                    ?>
                
                <div>
                   <?php echo $note['privateNote']; ?>
                </div>
                <div class="pull-right">
                    <span>Added on <strong><?php   echo date('M d, Y - h:i A', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
                    <span><a href="javascript:void(0);"  onclick="delele_notes('<?php echo $note['noteID'] ; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
                </div>
            
                <?php } } ?>
                
                
                <br>
                <!-- END Private Notes Content -->
            </div>
            
            <?php } ?>
            <!-- END Private Notes Block -->
        </div>
    </div>
    
     
</div>
    <!-- END Customer Content -->



 <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js" ></script>
  <script src="<?php echo base_url(JS); ?>/pages/customer_details_xero.js" ></script> 


</div>

