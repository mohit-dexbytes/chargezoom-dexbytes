<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?>	</div>
 
          <div class="row">  <div class="col-md-12">
	

          <legend class="leg">Refund </legend>
	<div class="full">
	
        <!-- Form Validation Example Title -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs">Invoice</th>
                    <th class="text-right">Amount</th>					
                    <th class="text-right hidden-xs hidden-sm">Date</th>
					<th class="text-right hidden-xs">Type</th>
					 <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                    <th class="hidden-xs hidden-sm text-right">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
    			<?php 
    			if(isset($transactions) && $transactions)
    			{
					
					
    				foreach($transactions as $transaction)
    				{
                        $inv_url1='';
                      if(!empty($transaction['invoice_id']) && !empty($transaction['invoice_no']) )
                      {
                        $invs = explode(',',$transaction['invoice_id']);
              
                        $invoice_no = explode(',',$transaction['invoice_no']);
                    
                        foreach($invs as $k=> $inv)
                        {
                          $inv_url = base_url().'Xero_controllers/Create_invoice/invoice_details_page/'.trim($inv);
                          if(isset($invoice_no[$k])){
                              if($plantype){
                                  $inv_url1.= $invoice_no[$k].',';  

                              }else{
                                  $inv_url1.=' <a href="'.$inv_url.'">'.$invoice_no[$k]. '</a>,';  

                              }
                               
                          }    
                        }
                        $inv_url1 = substr($inv_url1, 0, -1);
                     
                      }else{
                          $inv_url1.='<a href="javascript:void(0);">---</a> ';
                      }
    			?>
				<tr>
					
					
					<td class="text-left"><?php echo $transaction['fullName']; ?></td>
					
				    <td class="text-right hidden-xs cust_view"><?php echo $inv_url1; ?></td>
					
					<td class="text-right">
                        <?php
                          $showRefund = 0;
                          if($transaction['partial']==$transaction['transactionAmount'])
                          {

                              echo "<label class='label label-success'>Fully Refunded: ".'$'.number_format($transaction['partial'],2)."</label><br/>";
                          }
                          else if($transaction['partial'] !='0' )
                          { 
                            $showRefund = 1;
                            echo "<label class='label label-warning'>Partially Refunded: ".'$'.number_format($transaction['partial'],2)." </label><br/>";
                          }
                          else
                          { 
                            $showRefund = 1;
                            echo "";
                             
                          }

                          ?>
                        $<?php echo($transaction['transactionAmount'])?number_format($transaction['transactionAmount'], 2):'0.00'; ?>
                            
                    </td>
					
				
					
					<td class="hidden-xs hidden-sm text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
						<td class="hidden-xs text-right">
					     <?php if (strpos($transaction['transactionType'], 'sale') !== false) { 
                            echo "Sale";
                        }else if(strpos($transaction['transactionType'], 'auth') !== false){
                             echo "Authorization";
                         }else if(strpos($transaction['transactionType'], 'capture') !== false){
                             echo "Authorization";
                         }
                         ?>
                            
					</td>
					<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'';   ?></td>
					<?php if( $transaction['transactionCode']=="1"  || $transaction['transactionCode']=="111"|| $transaction['transactionCode']=="100" || $transaction['transactionCode']=="200"  ){ ?>
					<td class="text-right hidden-xs hidden-sm"><span style="color:#888888;"><?php echo "Success"; ?></span></td>
					<?php }else{ ?>
					
						<td class="text-right hidden-xs hidden-sm"><span style="color:#888888;">
						<?php echo $transaction['transactionStatus']; ?></span></td>
				    <?php } ?>	
					
				    <td class="text-center">
                        <div class="btn-group dropbtn ">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle <?php if( $showRefund=='0' ) echo "disabled"; ?>">Select <span class="caret"></span></a>
                            <?php if($transaction['transactionID'] != '' && $transaction['transactionGateway'] > 0){
                                $transactionGateway = $transaction['transactionGateway'];
                                $transactionGatewayName = $transaction['gateway'];
                                $transactionID = $transaction['transactionID'];
                            ?>
                            <ul class="dropdown-menu text-left">
                                <li>
                                    <a href="javascript:void(0)" id="txnRefund<?php  echo $transactionID; ?>" transaction-id="<?php  echo $transactionID; ?>" transaction-gatewayType="<?php echo $transactionGateway;?>" transaction-gatewayName="<?php echo $transactionGatewayName;?>" integration-type="1" class="refunAmountCustom"  data-backdrop="static" data-keyboard="false" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice" data-toggle="modal">Refund</a>

                                 
                                </li>
                                <?php if($transactionGateway != 5){ ?>
                                    <li>
                                        <a href="#payment_voidmod" class=""  onclick="set_void_pay('<?php  echo $transactionID; ?>', <?php echo $transactionGateway;?> );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </div>
                    </td>
				</tr>
				<?php } } 
			else {  echo'<tr><td colspan="8"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                </tr>'; }  
			?>
			</tbody>
        </table>
    </div>
    </div>
    </div>
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
    <script>$(function(){ EcomOrders.init(); });</script>

	<script>
    	$(function(){  nmiValidation.init(); });
    	
    	function set_refund_pay(txnid, txntype){
    	
			
    		if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnID').val(txnid);	 
				 var url   = "<?php echo base_url()?>Xero_controllers/Payments/create_customer_refund";
				}	
			else if(txntype=='2'){
    		    $('#txnIDrefund').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/create_customer_refund";
			}	
			 else if(txntype=='3'){
				$('#paytxnID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaytracePayment/create_customer_refund";
			 }
			 else if(txntype=='4'){
				$('#txnpaypalID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaypalPayment/create_customer_refund";
			 }
			 else if(txntype=='5'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/StripePayment/create_customer_refund";
			 }
			 
			 $("#data_form").attr("action",url);	
    		}
    	}   
	    
	    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
           $(".msg_data").fadeOut('slow');
        }
        
	</script>
	
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
	
	 <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>QBO_controllers/PaypalPayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
							    <input type="hidden" id="txnstrID" name="txnstrID" class="form-control"  value="" />
                                <input type="hidden" id="txnpaypalID" name="txnpaypalID" class="form-control"  value="" />
								 <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
								 <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
								 <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
 
	
	
</div>
