<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <div class="msg_data">
			    <?php echo $this->session->flashdata('message');   ?>
		    </div>
  

	      <div class="block-title">
             <h2><strong><?php if(isset($subs)){ echo "Edit"; }else{ echo "Create"; } ?> Invoice</strong> </h2>
             </div>   
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
        <div class="col-md-12">
			<legend class="leg" ><?php if (isset($subs)) {
									echo "Edit";
								} else {
									echo "Create";
								} ?> Invoice</legend>  
		
		    <form id="form-validation" action="<?php echo base_url(); ?>Xero_controllers/Xero_invoice/invoice_create" method="post" class="form-horizontal form-bordered">
                 <fieldset>  
                 <div  class="col-md-12 no-pad">
					     <div class="col-md-4 form-group">
						 <label class="control-label" for="customerID">Customer</label>
						  <div>
                            <select id="customerID" name="customerID" class="form-control">
                                <option value>Select Customer</option>
						      <?php   foreach($customers as $customer){       ?>
						        <option value="<?php echo $customer['Customer_ListID']; ?>"><?php echo  $customer['companyName'] ; ?></option>
						        <?php } ?>
                            </select>
                            </div>
						 </div>	
                    
                 
                   <div class="col-md-4  form-group">   
                        <label class=" control-label" for="firstName">Invoice Date</label>
                            <div class="input-group input-date">
                                <input type="text" id="invdate" name="invdate" class="form-control"   value="<?php echo date('Y-m-d'); ?>" data-date-format="mm-dd-yyyy" placeholder="" data-args="mm-dd-yyyy">
                                <span class="input-group-addon due_date"><i class="fa fa-calendar"></i></span>
                            </div>
                       </div>
					
					 <div class="col-md-4 form-group"> 
						<label class="control-label" for="taxes">Tax</label>
						    <div>
                               <select name="taxes" id="taxes" class="form-control tax_div">
							 <option value="" >Select Tax</option>
							 <?php  foreach($taxes as $tax){ ?>
							 <option  value="<?php echo $tax['taxID']; ?>"
							 <?php if(isset($subs) && $tax['taxID']==$subs['taxID'] ){echo "selected";} ?>><?php echo $tax['friendlyName']; ?> </option>
							 
							 <?php } ?>
							   </select>
							  </div>
                         </div>
                </div>
					
					<div class="col-md-12 no-pad"> 
					     <div class="col-md-3 form-group">
						    <label class="control-label" for="pay_terms">Payment Term</label>
						    <div>
						    <select name="pay_terms" id="pay_terms" class="form-control ">
							 <option value="" >Select Term</option>
							 <?php  foreach($netterms as $terms){ 
							 	if ($terms['pt_netTerm'] != "") {
									$termSet = $terms['pt_netTerm'];
								} else {
									$termSet = $terms['netTerm'];
								}
								if($terms['id'] == $selectedID){
									$selected = 'Selected';
								}else{
									$selected = '';
								} 
							 	if($terms['enable'] != 1){?>
								<option  <?php echo $selected; ?> value="echo $termSet;"><?php if($terms['pt_name'] != ""){ echo $terms['pt_name']; }else{ echo $terms['name']; } ?></option>
							 <?php } }?>
							   </select>
					        </div>
					     </div>
                        
			        
				 </div>
					 
					  
			<div class="col-sm-12 ">
			         <div id="set_credit" style="display:none;">	
                                         <div class="col-sm-3 form-group">
                                             <div>
                                                <label class="control-label" >Credit Card Number</label>
                                               
                                                        <input type="text" id="card_number" name="card_number" class="form-control" placeholder="" data-args="Credit Card Number">
                                                  </div>    
                                            </div>
											
											<div class="col-sm-3 form-group">	
                                            <div>
                                                <label  class="control-label" for="friendlyname"> Card Friendly Name</label>
                                               
                                                        <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" data-args="Card Friendly Name">
                                             </div>   
                                            </div>
                                        
										  <div class="col-sm-2 form-group">	
                                              
                                                <label class="control-label" for="expry">Expiry Month  </label>
                                                
                                                   	<select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
													    <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
											
											 <div class="col-sm-2 form-group">	
                                                <div>
												    <label class=" control-label" for="expry_year">Expiry Year</label>
                                               
                                                   	<select id="expiry_year" name="expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+15;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php  echo "20".$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                               </div>  
                                            </div>  
										 <div class="col-sm-2 form-group">					
											<div>
                                                <label class="control-label" for="cvv">Card Security Code (CVV)</label>

                                                        <input type="text" id="cvv" name="cvv" class="form-control" placeholder="" data-args="Card Security Code (CVV)">
                                                     
                                            </div>
                                            </div>
										 
									
                                    </div>
							</div>
			
					 
			</fieldset>
                    
					
			<fieldset><legend class="leg"> Item Details</legend>	
					<div class="col-md-12 no-pad form-group form-actions">
				 	<div class="col-md-3 form-group"><label class="control-label">Products & Services</label> </div>
					<div class="col-md-3 form-group"><label class="control-label">Description </label></div>
					<div class="col-md-2 form-group"><label class="control-label">Unit Rate </label> </div>
					<div class="col-md-1 form-group"><label class="control-label">Quantity </label></div>
					<div class="col-md-1 set_taxes form-group"><label class="control-label">Tax </label></div>
					<div class="col-md-2 form-group"><label class="control-label">Total</label></div>
					
					</div>
					
											
						  <div id="item_fields">
						 <?php 
						       if(isset($items) && !empty($items)){
							     foreach($items as $k=>$item ){
									$rate = $item['itemRate'];
									$qnty = $item['itemQuantity'];
									if(!empty($item['itemTax'])) {
										$tax  = $item['itemTax'];
										echo "<script> $('.set_taxes').css('display','block'); </script>";
									}else{	$tax = 0; }
									
									$total_amt = ( $rate * $qnty ) + ( ( $rate * $qnty ) * $tax / 100 );
								 
								 ?>
								 
							   <div class="form-group removeclass<?php echo $k+1; ?>">
								<div class="col-sm-3 nopadding"><div class="form-group">
								  <select class="form-control select-chosen"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="productID<?php echo $k+1; ?>" name="productID[]">
								<option>Select Plans</option>		
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['Code']; ?>"  > 
								 <?php echo $plan['Name']; ?> </option> <?php }  ?>
								   </select></div></div>
								   
								   <div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" placeholder="" data-args="Description "></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo $item['itemRate']; ?>" onblur="set_unit_val('<?php echo $k+1; ?>');" placeholder="" data-args="Unit Rate"></div></div>
								   
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control" maxlength="4"  onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity<?php echo $k+1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>" placeholder="" data-args="Quantity"></div></div>
								   
								    <div class="col-sm-1 nopadding"><div class="set_taxes"><div class="form-group"> <input type="checkbox" id="tax_check<?php echo $k+1; ?>" <?php if($item['itemTax']) echo "checked"; ?> name="tax_check[]" class="tax_checked" onchange="set_tax_val(this, '<?php echo $k+1; ?>')" value="<?php echo $item['itemTax']; ?>"></div></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k+1; ?>" name="total[]" value="<?php echo ($item['itemQuantity']*$item['itemRate']); ?>" placeholder="" data-args="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>
                                       </div>
						<?php		 }								 
							   }else{  
						 ?>
						   <div class="form-group removeclass1">
							<div class="col-sm-3 nopadding"><div class="form-group ">
								  <select class="form-control select-chosen"  onchange="select_plan_val('1');"  id="productID1" name="productID[]">
								<option>Select Plans</option>		
								  <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['Code']; ?>"  > 
								 <?php echo $plan['Name']; ?> </option> <?php }  ?>
								   </select></div></div>
								   
								   <div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description1" name="description[]" value="" placeholder="" data-args="Description "></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control force-numeric" id="unit_rate1" name="unit_rate[]" value="" onblur="set_unit_val('1');" placeholder="" data-args="Unit Rate"></div></div>
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control" maxlength="4"  onblur="set_qty_val('1');" id="quantity1" name="quantity[]" value="" placeholder="" data-args="Quantity"></div></div>
								   
								   <div class="col-sm-1 nopadding"><div class="set_taxes" style="display:none;"><div class="form-group"> <input type="checkbox"  id="tax_check1" onchange="set_tax_val(this, '1')" name="tax_check[]" class="tax_checked" value=""></div></div></div>
								   
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total1" name="total[]" value="" placeholder="" data-args="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('1');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>
							   
							</div>   <?php } ?>
								  
						 </div>
						
				 
				    <div class="col-md-12 no-pad">
					   <div class="form-group">
						
						<div class=" form-actions">		
							<label class="control-label "></label>						  
						  <div class="group-btn">
							 <button class="btn btn-sm btn-success" type="button"  onclick="item_fields();"> Add More </button>
						 
							 <label class="pull-right remove-hover"  ><strong>Total : <span id="grand_total"> <?php echo '$'.'0.00'; ?></span>  </strong></label>
						  </div>
				        </div>
					</div>
						
					</div>	
					
				 </fieldset>
                    
					<fieldset>
							<div id="set_bill_data">
							<legend class="leg">Billing Address</legend>						
									<div class="col-sm-12 form-group">										
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 1</label>
                                              
                                                        <input type="text" id="address1" name="address1" class="form-control " value="<?php if(isset($subs)) echo $subs['address1']; ?>"  placeholder="" data-args="Address Line 1">
                                                  
                                          </div>
									</div>	
								    <div class="col-sm-12 form-group">					
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 2</label>
                                              
                                                        <input type="text" id="address2" name="address2" class="form-control "  value="<?php if(isset($subs))echo $subs['address2']; ?>" placeholder="" data-args="Address Line 2">
                                                  
                                         </div>
									</div>
								    <div class="col-sm-3 form-group">	   
								       <div class="">
                                               
												<label class="control-label" for="example-typeahead">Country</label>
											
												<input type="text" id="country" name="country" class="form-control input-typeahead" autocomplete="off" value="<?php if(isset($subs)) echo $subs['country']; ?>" placeholder="" data-args="Country"> 
										</div>	
									</div>
									<div class="form-group col-sm-3">	
										<div class=" ">
                                                <label class=" control-label" for="val_username">State/Province</label>
                                                 <input type="text" id="state" name="state" class="form-control input-typeahead"  value="<?php if(isset($subs)) echo $subs['state']; ?>" autocomplete="off" placeholder="" data-args="State/Province">
                                        </div>
									</div>
									<div class="form-group col-sm-2">
										 <div class="">
                                                <label class=" control-label" for="val_username">City</label>
                                                 <input type="text" id="city" name="city" class="form-control input-typeahead" autocomplete="off" value="<?php  if(isset($subs))echo $subs['city']; ?>" placeholder="" data-args="City">
                                                    
                                           
										</div>
									</div>
									<div class="form-group col-sm-2">
										 <div class="">		  
										
                                                <label class=" control-label" for="val_username">ZIP Code</label>
                                              
                                                        <input type="text" id="zipcode" name="zipcode" class="form-control" value="<?php if(isset($subs)) echo $subs['zipcode']; ?>" placeholder="" data-args="ZIP Code">
                                                     
	
										  </div>
									</div>
									<div class="form-group col-sm-2">		
										 <div class="">
											  
                                                <label class=" control-label" for="phone">Contact Number</label>
                                               
                                                        <input type="text" id="phone" name="phone" class="form-control"  placeholder="" data-args="Contact Number">
                                                     
                                         
										   </div>
									</div>
								
				           </div>
				<div class="form-group pull-right">
					<div class="col-md-12 no-pad">
					    <button type="submit" class="submit btn btn-sm btn-success">Save</button>
					    <a href="<?php echo base_url(); ?>Xero_controllers/Xero_invoice/Invoice_detailes" class=" btn btn-sm btn-primary1">Cancel</a>	
                  				
					   					
					
					</div>
				    </div>	
			</fieldset>
               
				
             
				
            </form>
          </div>      
        
            
    </div>


    <script type="text/javascript">


	  $(document).ready(function() {
                                                
                                                
		$('#taxes').change( function(){
			   if($(this).val() != "" ){
			   $('.set_taxes').show();
			   $('.show_check').show();
			   }else{
					$('#tax_check').val('');
					$('.set_taxes').hide();
			   }
		  });

		window.setTimeout("fadeMyDiv();", 2000);
		
		});
		
		function fadeMyDiv() {
		   $(".msg_data").fadeOut('slow');
		}
  
  
  $('.tax_div').change( function(){
    var tax_id = $(this).val();
    
    $.ajax({
    url: '<?php echo base_url("Xero_controllers/SettingSubscription/get_tax_id")?>',
    type: 'POST',
	data:{tax_id:tax_id},
	dataType: 'json',
    success: function(data){
		
             		
			 $('.tax_checked').val(data.taxRate);
			 
			 
	}	
});
	
});
	
	
        $(function(){
		
		
           nmiValidation.init();
		  
	      var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
			 $("#invoice_date").datepicker({ 
              format: 'yyyy-mm-dd',
              autoclose: true
            });  
			
			$('#invdate').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true
			});
			
			 $('.due_date').click(function () {
                $('#invdate').datepicker('show');
            });
            
			$('#subsamount').blur(function(){
				
				var dur = $('#duration_list').val();
              var subsamount = $('#subsamount').val();
			  var tot_amount = subsamount*dur ;
			  $('#total_amount').val(tot_amount.toFixed(2));
			  $('#total_invoice').val(dur);
				
			});
			
			
					
		  $('#card_list').change( function(){
			   if($(this).val()=='new1'){
			   $('#set_credit').show();
			   }else{
					  $('#card_number').val('');
			$('#set_credit').hide();
			   }
		  });
					
		$('#customerID').change(function(){
			
			var cid  = $(this).val();
		
			if(cid!=""){
				$('#card_list').find('option').not(':first').remove();
				$.ajax({
					type:"POST",
					url : "<?php echo base_url(); ?>Xero_controllers/Payments/check_vault",
					data : {'customerID':cid},
					success : function(response){
						
							 data=$.parseJSON(response);
						
							 if(data['status']=='success'){
							
								  var s=$('#card_list');
								
								  var card1 = data['card'];
									 $(s).append('<option value="new1">New Card</option>');
									for(var val in  card1) {
										
									  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
									}
																   
									$('#address1').val(data['address1']);
									$('#address2').val(data['address2']);
									$('#city').val(data['City']);
									$('#state').val(data['State']);
									$('#country').val(data['Country']);
									$('#zipcode').val(data['zipCode']);
									$('#phone').val(data['phoneNumber']);
									
								  
						   }else{
						      
								 $('#card_list').append('<option value="new1">New Card</option>');
								 return false;
						   }	   
						
					}
					
					
				});
				
			}	
		});		
				
	

	});
	
	
	var jsdata = '<?php if(isset($items)){ echo json_encode($items);}?>';
	
	if(jQuery.isEmptyObject(jsdata))
   {
	  
		var room = 1;
	  
		
	}else{
		
		var grand_total1=0;
		var room = '<?php if(isset($k)){ echo $k+1; }else{ echo "1";} ?>';
		$( ".total_val" ).each(function(){
               grand_total1+= parseFloat($(this).val());
			});
			
		$('#grand_total').html(format2(grand_total1,2));

	}
	function item_fields(first=false)
	{
	<?php
	        $arrplan = json_encode($plans);
	        $newdata = str_replace("'", "", $arrplan);
	        ?>
		room++;
		var  jsplandata = '<?php echo $newdata ?>';
		
		var plan_data = $.parseJSON(jsplandata); 
		var plan_html ='<option val="">Select Plan</option>';
		for(var val in  plan_data) {      plan_html+='<option value="'+ plan_data[val]['Code']+'">'+plan_data[val]['Name']+'</option>'; }
		
		
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		
		var deleteButton = '<div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div>';
		if(first) {
			deleteButton = '';
		}
		
		if($('#taxes').val() == ''){
			var show_tax = 'style="display:none;"';
			var tax_val = 0;
		}else {
			var tax_val = $('.tax_checked').val();
		}
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control select-chosen"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="" data-args="Description "></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control" id="unit_rate'+room+'" name="unit_rate[]" value="" onblur="set_unit_val('+room+');" placeholder="" data-args="Unit Rate"></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control" maxlength="4"  onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" placeholder="" data-args="Quantity"></div></div>  <div class="col-sm-1 nopadding"><div class="set_taxes" ><div class="form-group"> <input type="checkbox" onkeypress="return isNumberKeys(event)" id="tax_check'+room+'" onchange="set_tax_val(this, '+room+')" name="tax_check[]" '+show_tax+' class="show_check tax_checked" value="'+tax_val+'"></div></div></div> 	<div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+room+'" name="total[]" value="" placeholder="" data-args="Total">'+deleteButton+'</div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest);
		$(".select-chosen").chosen();

  }

  item_fields(true);
  
  
   function remove_education_fields(rid)
   {
	   var rid_val = $('#total'+rid).val();
	   
	   var gr_val  = $('#grand_total').html();
	    if(rid_val){
	   var dif     = parseFloat(gr_val)-parseFloat(rid_val);
	   $('#grand_total').html(format2(dif,2));
		}
	   $('.removeclass'+rid).remove();
	   
	    
	   
	   
  }
	
	
	
	
	
	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                     sub_name:{
						  required:true,
						  minlength:3,
					 },
					 sub_start_date: {
							 required:true,
						},
					autopay:{
							 required:true,
					},
					gateway_list:{
							 required:true,
					},
					card_list:{
							 required:true,
					},
					paycycle:{
							 required:true,
					},		
			    	 duration_list: {
                        required: true,
                        number: true,
						maxlength:2,
						
                    },
					invoice_date: {
                        required: true,
                        
                    },
                    customerID: {
                         required: true,
                       
                    },
					 subsamount: {
                        required: true,
						number:true,
						
                    },
					friendlyname:{
						 required: true,
						 minlength: 3,
					},
					  card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },	
					
					address1:{
						required:true,
					},
					address2:{
						required:true,
					},
                  	country:{
						required:true,
					},
					state:{
						required:true,
					},
					city:{
						required:true,
					},
					zipcode:{
						required:true,
						minlength:5,
						maxlength:6,
						digits:true,
					},
					phone:{
						required:true,
						minlength:10,
						maxlength:14,
						digits:true,
					},
                  freetrial:{
						required: true,
						digits:true,
						check_free:{
							sub_start_date:'#sub_start_date',
							paycycle:'#paycycle',
							duration_list:'#duration_list',
						}
					},
                    	
					
                },
               
            });
					
		 $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
					
				
		 $.validator.addMethod('check_free', function(value, element, params) {  
			
		  var duration = $(params.duration_list).val();
		  
		  if(duration =='0')return true;
		  
		  var frequency = $(params.paycycle).val();
		  var stdate     = new  Date( Date.parse($(params.sub_start_date).val()));
		  
		  var free = value;
		
			
		  return (duration > free);
		}, 'Value should be less than Recurrence value');
						

        }
    };
}();
   
/**********Check the Validation for free trial**********************/
	function weeksBetween(d1, d2) {
		
     return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
	
   }
   function daysbeween(d1, d2) {
		
     return Math.round((d2 - d1) / (24 * 60 * 60 * 1000));
	
   }
   
   function get_months(d1, d2){
	 

return difference = (d2.getFullYear()*12 + d2.getMonth()) - (d1.getFullYear()*12 + d1.getMonth()); 
	   
   }  
   
       function get_frequncy_val(du, new1date, fr){
		   var res='';
		 
		       var CurrentDate =  new Date(new1date.getFullYear(), new1date.getMonth(), new1date.getDate(), 0, 0, 0, 0); 
				  CurrentDate.setMonth(CurrentDate.getMonth() + parseInt(du));
                  var newdate = new Date(CurrentDate);
				
		 if(fr=='dly'){
           res= daysbeween(new Date(new1date), new Date(newdate));
		 }else if(fr=='1wk'){
			  res= weeksBetween(new Date(new1date), new Date(newdate));
		 }else if(fr=='2wk'){
           res= weeksBetween(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='mon'){
           res= get_months(new Date(new1date), new Date(newdate));
		 }else if(fr=='2mn'){
         res= get_months(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='qtr'){
          res= get_months(new Date(new1date), new Date(newdate))/3;
		 }else if(fr=='six'){
           res= get_months(new Date(new1date), new Date(newdate))/6;
		 }else if(fr=='yr1'){
          res= get_months(new Date(new1date), new Date(newdate))/12;
		 }else if(fr=='yr2'){
		 res= get_months(new Date(new1date), new Date(newdate))/24;
		 }else if(fr=='yr3'){
			 res= get_months(new Date(new1date), new Date(newdate))/36;
		 }	
           return res;    			 
		   
	   } 
	   
/************End*******************/	   
	
   
   function chk_payment(r_val)  {  
   
     
      if(r_val=='1'){
		  $('#set_pay_data').show();
	  }else{
		$('#set_pay_data').hide();
      }	  

   }   
   
	function select_plan_val(rid){
		
		
		
		var itemID = $('#productID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>Xero_controllers/Xero_invoice/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['SalesDescription']);		
			 $('#quantity'+rid).trigger('blur');	 
			$('#unit_rate'+rid).val(roundN(item_data['SalesPrice'],2));
			 $('#quantity'+rid).val(item_data['QuantityOnHand']);
			
			if(item_data['QuantityOnHand']=='0'){
			 $('#quantity'+rid).val('1');
             }else{
                 $('#quantity'+rid).val(item_data['QuantityOnHand']);
             }
			}		
		});
		
	}
	
	function set_unit_val(rid){
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
		$('#total'+rid).val(total.toFixed(2));
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
			
              var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format2(grand_total,2));
	}
	
	function set_qty_val(rid){
       
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val(); 
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax;   
		$('#total'+rid).val(total.toFixed(2));
		var grand_total=0;
		$( ".total_val" ).each(function(){ 
				var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format2(grand_total,2));
		
	}	
	
	function set_tax_val(mythis, rid){
		
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = $('#tax_check'+rid).val();
 
		if(mythis.checked){
			var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
			$('#total'+rid).val(total.toFixed(2));
		}
		else {
			var total  = qty*rate; 
			$('#total'+rid).val(total.toFixed(2));
		}
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
		   var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
		});
			
		$('#grand_total').html(format2(grand_total,2));
	}
	
	$(document).ready(function() {

        $('.force-numeric').keydown(function(e)
        {
            
   var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
		
		
		$('input.float').bind('keypress', function() {
			  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
			});
  

});


	
	 function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
	  
	  
function IntegerAndDecimal(e,obj,isDecimal)
{
    if ([e.keyCode||e.which]==8) //this is to allow backspace
    return true;

    if ([e.keyCode||e.which]==46) //this is to allow decimal point
    {
      if(isDecimal=='true')
      {
        var val = obj.value;
        if(val.indexOf(".") > -1)
        {
            e.returnValue = false;
            return false;
        }
        return true;
      }
      else
      {
        e.returnValue = false;
        return false;
      }
    }

    if ([e.keyCode||e.which] < 48 || [e.keyCode||e.which] > 57)
    e.preventDefault? e.preventDefault() : e.returnValue = false; 
}



function isNumberKeys(evt)
           {
               var charCode = (evt.which) ? evt.which : event.keyCode
 
               if (charCode == 46)
               {
                   var inputValue = $("#inputfield").val()
                   if (inputValue.indexOf('.') < 1)
                   {
                       return true;
                   }
                   return false;
               }
               if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
               {
                   return false;
               }
               return true;
           }
	

	</script>
	
	<style>
	.form-bordered .form-group {
    margin: 0;
    border: none;
    padding: 10px;
   
}
	
	input[type="checkbox"] {
    margin: 12px 0 0;
    margin-top: 1px \9;
    margin-left: 45;
    margin-left: 10px;
    line-height: normal;
}

	</style>
</div>