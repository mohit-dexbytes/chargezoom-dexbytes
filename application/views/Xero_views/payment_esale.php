                <!-- Page content -->
<?php
	$this->load->view('alert');
?>
<style>

.has-error .help-block {
    display: table-footer-group;
}


</style>
                
				<div id="page-content">
				    	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?>	
				    	</div>
								 
							
                                <div class="block">
                                    <!-- Form Validation Example Title -->
                                     <div class="block-title">
                                        <h2><strong> Sale </strong></h2>
                                    </div>
                                                          
                                    <!-- END Form Validation Example Title -->
                                    
                             
                                    <!-- Form Validation Example Content -->  
							       		
						    			
									
                                    <form id="form-validation" action="<?php echo base_url(); ?>Xero_controllers/Payments/create_customer_esale" method="post" class="form-horizontal">
                                        <input type="hidden" id="invoiceTotalAmountCal" value="0.00">
                                        <input type="hidden" id="isCustomize" value="0">
                                        <fieldset>
                                            <legend> Payment Info</legend>
											 
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="customerID">Customer Name</label>
                                                <div class="col-md-6">
                                                    <select id="customerID" name="customerID" class="form-control">
                                                      
                                                        <option value>Choose Customer</option>
														<?php   foreach($customers as $customer){       ?>
														
                                                        <option value="<?php echo $customer['Customer_ListID']; ?>"><?php echo  $customer['fullName'] ; ?></option>
														<?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-1"><div class="block-options pull-right">
                                             <a class="btn btn-sm btn-success" title="Create New" href="<?php echo base_url(); ?>Xero_controllers/Customer/create_customer">Add New</a>
                                             </div></div>
                                            </div>
                                            <?php 
                                                if(!isset($defaultGateway) || !$defaultGateway){
                                            ?>
									   <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway_list" name="gateway_list"  class="form-control">
                                                        <option value="" >Select Gateway</option>
														  <?php foreach($gateways as $gateway){ ?>
                                                           <option value="<?php echo $gateway['gatewayID'];  ?>"  <?php if($gateway['set_as_default']=='1')echo "selected ='selected' ";  ?> ><?php echo $gateway['gatewayFriendlyName']; ?></option>
														   <?php } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
											<?php 
                                                } else { ?>
                                                <input type="hidden" name="gateway_list" value="<?php echo $defaultGateway['gatewayID'];  ?>">
                                            <?php }  ?>	
									
											<div class="form-group">
                                                <label class="col-md-4 control-label" for="account_name">Account Name<span class="text-danger"></span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="account_name"  data-stripe="account_name" name="account_name" class="form-control" placeholder="" data-args="Account Name">
                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
														
                                                    </div>
                                                </div>
                                            </div>								 
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="card_number">Account Number<span class="text-danger"></span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="account_number"  data-stripe="account_number" name="account_number" class="form-control" placeholder="" data-args="Account Number">
                                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
														
                                                    </div>
                                                </div>
                                            </div>
                                         <div class="form-group">
                                                <label class="col-md-4 control-label" for="route">Routing Number<span class="text-danger"></span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="route_number" name="route_number" data-stripe="route_number" class="form-control" placeholder="" data-args="Routing Number">
                                                        <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                                    </div>
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type<span class="text-danger"></span></label>
                                                <div class="col-md-6">
                                                    
                                                        <select id="acct_holder_type" name="acct_holder_type" class="form-control">
                                                       
                                                        <option value="business">Business</option>
                                                        <option value="personal">Personal</option>
                                                       
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="acct_type">Account Type<span class="text-danger"></span></label>
                                                <div class="col-md-6">
                                                    
                                                       <select id="acct_type" name="acct_type" class="form-control">
                                                       
                                                        <option value="checking">Checking</option>
                                                        <option value="savings">Saving</option>
                                                       
                                                    </select>
												
                                                </div>
                                            </div>
                                             
											 
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="friendlyname">Entry Method (Sec Code)<span class="text-danger"></span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                       <select id="sec_code" name="sec_code" class="form-control">
                                                       
                                                       
</option><option value="ACK">Acknowledgement Entry (ACK)</option><option value="ADV">Automated Accounting Advice (ADV)</option><option value="ARC">Accounts Receivable Entry (ARC)</option><option value="ATX">Acknowledgement Entry (ATX)</option><option value="BOC">Back Office Conversion (BOC)</option><option value="CBR">Corporate Cross-Border Payment (CBR)</option><option value="CCD">Corporate Cash Disbursement (CCD)</option><option value="CIE">Consumer Initiated Entry (CIE)</option><option value="COR">Automated Notification of Change (COR)</option><option value="CTX">Corporate Trade Exchange (CTX)</option><option value="DNE">Death Notification Entry (DNE)</option><option value="ENR">Automated Enrollment Entry (ENR)</option><option value="MTE">Machine Transfer Entry (MTE)</option><option value="PBR">Consumer Cross-Border Payment (PBR)</option><option value="POP">Point-Of-Presence (POP)</option><option value="POS">Point-Of-Sale Entry (POP)</option><option value="PPD">Prearranged Payment &amp; Deposit (PPD)</option><option value="RCK">Re-presented Check Entry (RCK)</option><option value="SHR">Shared Network Transaction (SHR)</option><option value="TEL">Telephone Initiated Entry (TEL)</option><option value="TRC">Truncated Entry (TRC)</option><option value="TRX">Truncated Entry (TRX)</option><option value="WEB">Web Initiated Entry (WEB)</option><option value="XCK">Destroyed Check Entry (XCK)</option>
                                                       
                                                    </select>
													</div>
                                                </div>
                                            </div>
									
								 </fieldset>	
								 
								
								 
							


									 <fieldset>			
                                             <div class="form-group">
                                                <label class="col-md-4 control-label" for="amount">Amount</label>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <img id="amount_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">
                                                        <input type="text" id="amount" name="amount" class="form-control" placeholder="" data-args="Amount">
                                                        <span class="input-group-addon"><i class="gi gi-usd"></i></span>
                                                    </div>
                                                </div>
												<label class="col-md-2 control-label" for="country_code">Currency</label>
                                                <div class="col-md-2">
                                                    <select id="country_code" name="country_code" class="form-control">
                                                       
                                                        <option value="USD">USD</option>
                                                        <option value="AUD">AUD</option>
                                                        <option value="CAD">CAD</option>
                                                        <option value="EUR">EUR</option>
                                                        <option value="NZD">NZD</option>
                                                    </select>
                                                </div>
                                            </div>
											
											 <div class="form-group hidden">
                                                <label class="col-md-4 control-label" for="val_skill">Surcharge Type</label>
                                                <div class="col-md-2">
                                                    <select id="surcharge_type" name="surcharge_type"  class="form-control">
                                                      
                                                        <option value="1">No Surcharge</option>
                                                        <option value="2">Fixed</option>
                                                        <option value="3">Percentage</option>
                                                    </select>
                                                </div>
												   <label class="col-md-2 control-label" for="val_digits">Surcharge</label>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" id="surchargeVal" name="surchargeVal" class="form-control" readonly  placeholder="" data-args="0.00">
                                                        <span class="input-group-addon"><i class="gi gi-usd"></i></span>
                                                    </div>
                                                </div>
												
                                            </div>
											
									 </fieldset>	
                                    <fieldset id="card_data">	
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_digits">Total Amount</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="totalamount" name="totalamount" class="form-control" placeholder="" data-args="0.00" readonly ='readonly' >
                                                        <span class="input-group-addon"><i class="gi gi-usd"></i></span>
                                                    </div>
                                                </div>
                                            </div>
											
											
											
											
                                        </fieldset>
                                        
                                        <fieldset>
			     	<div id="set_bill_data">
                                            <legend> Billing Address</legend>
                                       	
										
									     <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="baddress1" name="baddress1" class="form-control "  placeholder="" data-args="Address Line 1">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
                                          
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="baddress2" name="baddress2" class="form-control "  placeholder="" data-args="Address Line 2">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">City</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="bcity" name="bcity" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="City">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											<div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">State/Province</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="bstate" name="bstate" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="State/Province">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                          </div>	  
											  
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="bzipcode" name="bzipcode" class="form-control" placeholder="" data-args="ZIP Code">
                                                        <span class="input-group-addon"><i class="gi gi-direction"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											 <div class="form-group">
												
												<label class="col-md-4 control-label" for="example-typeahead">Country</label>
												<div class="col-md-6">
												  <div class="input-group">
													<input type="text" id="bcountry" name="bcountry" class="form-control input-typeahead" autocomplete="off" value="" placeholder="" data-args="Country">
													<span class="input-group-addon"><i class="gi gi-home"></i></span>
												</div>
												
                                            </div>
											</div>
										
                                          
                                        </div>
											
											 </fieldset>
											 <fieldset>
								               <legend> Shipping Address</legend>
													 <div class="form-group"> <div class="col-md-12">
												 <input type="checkbox" id="chk_add_copy"> Copy from Billing Address
												</div></div>
                                       
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="companyName">Company Name </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="companyName" name="companyName" class="form-control" placeholder="" data-args="Company Name">
                                                        <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
                                                
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="firstName">First Name</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="firstName" name="firstName" class="form-control" placeholder="" data-args="First Name">
                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="lastName">Last Name </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="lastName" name="lastName" class="form-control" placeholder="" data-args="Last Name">
                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                               	
									  							
                                         	
									     <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="address1" name="address1" class="form-control "  placeholder="" data-args="Address Line 1">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
                                                 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="address2" name="address2" class="form-control "  placeholder="" data-args="Address Line 2">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
										 
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">City</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="city" name="city" class="form-control" autocomplete="off" placeholder="" data-args="City">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">State/Province</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="state" name="state" class="form-control" autocomplete="off" placeholder="" data-args="State/Province">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                          </div>    
                                            
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="" data-args="ZIP Code">
                                                        <span class="input-group-addon"><i class="gi gi-direction"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											
												 <div class="form-group">
                                                												
												<label class="col-md-4 control-label" for="example-typeahead">Country</label>
												<div class="col-md-6">
												  <div class="input-group">
													<input type="text" id="country" name="country" class="form-control" autocomplete="off" value="" placeholder="" data-args="Country">
													<span class="input-group-addon"><i class="gi gi-home"></i></span>
												</div>
												
                                            </div>
											</div>
											  <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_email">Email Address</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="email" name="email" class="form-control" placeholder="" data-args="Email Address">
                                                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                                    </div>
                                                </div>
                                            </div>		
											  <div class="form-group">
                                                <label class="col-md-4 control-label" for="phone">Phone Number</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="phone" name="phone" class="form-control" placeholder="" data-args="Phone Number">
                                                        <span class="input-group-addon"><i class="gi gi-phone_alt"></i></span>
                                                    </div>
                                                </div>
                                          </div>
																				
                                          
                                        
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="reference">Reference Memo</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="reference" name="reference" class="form-control" placeholder="" data-args="Reference Memo">
                                                        <span class="input-group-addon"><i class="gi gi-notes"></i></span>
                                                    </div>
                                                </div>
                                            </div>
 											<div class="form-group">
											     <label class="col-md-4 control-label" for="reference"> Send Receipt Email</label>
                                                <div class="col-md-6">
										        <input type="checkbox" name="tr_checked" checked="checked" />
										        </div>
									       	</div>	
											 </fieldset>
											 
										
                                        <div class="form-group form-actions">
                                            <div class="col-md-8 col-md-offset-4">
                                                <input type="hidden" name="invoice_id" id="invoice_ids" value="" />
                                                <input type="hidden" name="invoice_pay_amount" id="invoice_pay_amount" value="" />      
                                                <input type="hidden" name="gatewaySurchargeRate" id="gatewaySurchargeRate" value="" />       
                                                <input type="hidden" name="cardSurchargeValue" id="cardSurchargeValue" value="" />     
                                                <button type="submit"  id="submit_btn"  class="btn btn-sm btn-success"> Submit</button>
												<button type="button"  onclick="this.form.reset()" class="btn btn-sm btn-info"> Reset</button>				
                                              
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Validation Example Content -->

                                
                                    <!-- END Terms Modal -->
                                </div>
                                <!-- END Validation Block -->
                            </div>
					  </div>

               <div id="amountModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <h2 class="modal-title">Amount</h2>
      </div>
      <div class="modal-body clearfix">
      
        <div id="table2">
            
            
            
        </div>
        <div class="form-group alignTableInvoiceListTotal">
               
            <div class="col-md-2 text-center"></div>
            <div class="col-md-2 text-left"></div>
            <div class="col-md-3 text-right"></div>
            <div class="col-md-2 text-right totalTextLeft" >Total</div>
            <div class="col-md-3 text-left totalTextRight" >
                $<span id="totalamount1">0.00</span>
            </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Select</button>
        <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>    

 <style>
.use_vault{display:none;}
.update_vault{display:none;}
.add_vault{display:none;}
#set_credit{display:none;}
#vaul_div{display:none;}
.vaul_div{display:none;}
 
</style> 
         

   <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
   
   
    <script>
        window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }

	 var gtype ='';
	  function stripeResponseHandler(status, response) {

                if (response.error) {
                    // Re-enable the submit button
                    $('#submit_btn').removeAttr("disabled");
                    // Show the errors on the form
                    $('#payment_error').text(response.error.message);
                } else {
                    var form = $("#form-validation");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

                }
            }
	
	$(function(){  nmiValidation.init(); 
	
	  $('#gateway_list').change(function(){
		    var gateway_value =$(this).val();
			
			if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				  
				              data = $.parseJSON(response);
							 gtype  = 	data['gatewayType'];
							  if(gtype=='3'){			
											var url   = "<?php echo base_url()?>Xero_controllers/Payments/create_customer_esale";
										}else if(gtype=='2'){
									var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/create_customer_esale";
									}
									
																			
				
				             $("#form-validation").attr("action",url);
					}   
				   
			   });
			}	
			
         
			
	  });
	
	$('#customerID').change(function(){
		
		$('#vaultID').val('');
                            
        $('#check_status').val('0');
        $('#invoice_ids').val('');
        $('#invoice_pay_amount').val('');
        $('#invoice_number').empty(); //remove all child nodes
        $('#invoice_number').append('');
        $('#invoice_number').trigger("chosen:updated");
        showHideSurchargeNotice(0);         
        
        var cid  = $(this).val();
        $(".new_esale_accounts").show();                   
	
		if(cid!=""){
			if (typeof focusAmountInputAuto !== 'undefined') {
                focusAmountInputAuto = true;
            }
            var amount = $('#amount').val(0.00);
            $('#totalamount').val(0.00);
            $('#amount').attr('readonly',true);
            $('#amount_loader').show();
            $('#card_list').find('option').not(':first').remove();

			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Xero_controllers/Payments/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
					    data=$.parseJSON(response);
                        $('#amount').attr('readonly',false);
                        $('#amount_loader').hide();
						
					    if(data['status']=='success'){
						
                              var s=$('#card_list');
							  var card1 = data['card'];
						      var optionselect_invoice = '';
                                $('#table2').html('');
                               
                                if (!jQuery.isEmptyObject(data['invoices'])) {
                                    $('#table2').html(data['invoices']);
                                    $('#totalamount1').html(parseFloat(data['totalInvoiceAmount']).toFixed(2));
                                    $('#totalamount').val(parseFloat(data['totalInvoiceAmount']).toFixed(2));
                                    $('#amount').val(parseFloat(data['totalInvoiceAmount']).toFixed(2));
                                    $('#isCustomize').val(1);
                                    $("#amountModel").modal("show");


                                    for (var valinv in data['invoiceIDs']) {
                                        optionselect_invoice += '<option selected value="'+valinv+'">'+data['invoiceIDs'][valinv]+'</option>';
                                    }
                                }

                                tmp = [];
                                $('#invoice_pay_amount').val('');
                                $('#invoice_ids').val('');

                                $('#invoice_number').empty(); //remove all child nodes
                                $('#invoice_number').append(optionselect_invoice);
                                $('#invoice_number').trigger("chosen:updated");
						
							 
							    $('#companyName').val(data['companyName']);
							    $('#firstName').val(data['firstName']);
								$('#lastName').val(data['lastName']);
								$('#baddress1').val(data['address1']);
								$('#baddress2').val(data['address2']);
								$('#bcity').val(data['City']);
								$('#bstate').val(data['State']);
								$('#bzipcode').val(data['zipCode']);
								$('#bcountry').val(data['Country']);
								$('#phone').val(data['phoneNumber']);
								$('#email').val(data['userEmail']);
								  if($('#chk_add_copy').is(':checked')){
                             	$('#address1').val($('#baddress1').val());
								$('#address2').val(	$('#baddress2').val());
								$('#city').val($('#bcity').val());
								$('#state').val($('#bstate').val());
								$('#zipcode').val($('#bzipcode').val());
								$('#country').val($('#bcountry').val());
					         
					     }	 
                         var sum = 0;
                            var tmp = [];
                            var payAmount = [];
                            $('.chk_pay').each(function () {
                                var r_id = $(this).attr('id');
                                var inv_id = $(this).val();

                                if ($(this).is(':checked')) {
                                    sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
                                    $('#test-inv-checkbox-'+$(this).val()).prop('checked', true);
                                    $('.'+r_id).attr('readonly', false);
                                    
                                    tmp.push(inv_id);
                                    payAmount.push(parseFloat($('.' + r_id).val()));
                                }else{
                                    $('#test-inv-checkbox-'+$(this).val()).prop('checked', false);
                                    $('.'+r_id).attr('readonly', true);
                                }
                            });  
                            $('#invoice_ids').val(tmp);
                            $('#invoice_pay_amount').val(payAmount);  
					   }	   
					
				}
				
				
			});
			
		}	
    });		
	
	    
	$('#card_list').change(function(){
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			  
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Xero_controllers/Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					    if(data['status']=='success'){
						 var form = $("#form-validation");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler);

									// Prevent the form from submitting with the default action
									
					        }	   
					
				}
				
				
			});
		  }
		
	});		
        $('#cvv').blur(function(event) {  
		if(gtype=='5'){	
       
        // Disable the submit button to prevent repeated clicks
		
		var pub_key = $('#stripeApiKey').val();
		 Stripe.setPublishableKey('pk_test_txAnDWmHlkcx7AUronv8mKUl');
         Stripe.createToken({
                        number: $('#card_number').val(),
                        cvc: $('#cvv').val(),
                        exp_month: $('#expiry').val(),
                        exp_year: $('#expiry_year').val()
                    }, stripeResponseHandler);

        // Prevent the form from submitting with the default action
        return false;
		}
      });
	
	
	var amount=0;
	$('#surcharge_type').change(function(){
		
		if($(this).val()=='1'){
			
			 $('#surchargeVal').attr('readonly','readonly');
			  $('#surchargeVal').val('0');
			var amount = $('#amount').val();
			$('#totalamount').val(amount.toFixed(2));
		}	
		if($(this).val()=='2'){
			    $('#surchargeVal').removeAttr('readonly');
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			    $('#totalamount').val(amount1.toFixed(2));

		}	
		if($(this).val()=='3'){
			 $('#surchargeVal').removeAttr('readonly');
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					     amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	});
	
	$('#surchargeVal').change(function(){
       
	  
	   	if( $('#surcharge_type').val()=='2'){
			
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
		if( $('#surcharge_type').val()=='3'){
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					    amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	   

	});
	
	
	$('#amount').change(function(){
       
	   
	   if($(this).val()=='1'){
			 $('#surchargeVal').removeAttr('readonly');
			  $('#surchargeVal').val('0');
			var amount = $('#amount').val();
			$('#totalamount').val(amount.toFixed(2));
		}	
	  
	   	if( $('#surcharge_type').val()=='2'){
			
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
		if( $('#surcharge_type').val()=='3');{
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	   

	});
	
		 $('#chk_add_copy').click(function(){
   
     
     if($('#chk_add_copy').is(':checked')){
                      	$('#address1').val($('#baddress1').val());
								$('#address2').val(	$('#baddress2').val());
								$('#city').val($('#bcity').val());
								$('#state').val($('#bstate').val());
								$('#zipcode').val($('#bzipcode').val());
								$('#country').val($('#bcountry').val());
     }
     
 });
	
		
  $('#card_list').change( function(){
	   if($(this).val()=='new1'){
	   $('#set_credit').show();
	    $('#set_bill_data').show();
	   }else{
		      $('#card_number').val('');
	$('#set_credit').hide();
	$('#set_bill_data').hide();
	   }
  });
	

	$.validator.addMethod('CCExp', function(value, element, params) {
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');


});
	
	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                rules: {
                    account_number: {
                        required: true,
						minlength: 3,
                        maxlength: 20,
                        number:true,
                        noDecimal:true,

                    },
				
					account_name:{
						required: true,
						minlength: 3,
					},
					route_number: {
                        required: true,
						minlength: 9,
                        maxlength: 9,
                        number:true,
                        noDecimal:true,

                    },
				
					account_type:{
						required: true,
						minlength: 3,
					},
                    customerID:{
                         required: true,
                       
                    },
					 amount: {
                        required: true,
                         number: true
                    },
                     totalamount: {
                        required: true,
                         number: true
                    },
					check_status:{
						  required: true,
					},
					fistName:{
						required: true,
						minlength: 2,
					},
                    lastName:{
                         required: true,
                       
                    },
					 address1: {
                        required: true
                    },
                 address2: {
                        required: true
                    },
					country:{
						  required: true,
					},
					city:{
						required: true,
						minlength: 3,
					},
                    state:{
                         required: true,
                    },
					 email: {
                        required: true,
						email:true
                    },
					phone: {
                        required: true,
						minlength:10,
						maxlength:12,
						digits:true,
                    },
                zipcode:{
					 required: true,
					  number: true,
						minlength: 5,
                        maxlength: 6,
				},
				
				baddress1: {
                        required: true
                    },
					bcountry:{
						  required: true,
					},
					bcity:{
						required: true,
						minlength: 3,
					},
                    bstate:{
                         required: true,
                    },
					 bemail: {
                        required: true,
						email:true
                    },
					bphone: {
                        required: true,
                        minlength:10,
						maxlength:12,
						digits:true,
                        
                    },
                bzipcode:{
					 required: true,
					  number: true,
						minlength: 5,
                        maxlength: 6,
				}
				
                   
                  
              },
                messages: {
					  customerID: {
                        required: 'Please select a customer',
                      
                    },
                   
                   
					amount:{
						  required: 'Please enter the amount',
					},
					check_status:{
						 required: 'Please select the option',
					},
                    account_number: {

                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits."
                    },
                    route_number: {

                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 9 digits."
                    },
                  
                   
                },
                submitHandler: function (form) {
                    $("#submit_btn").attr("disabled", true);
                    return true;
                }
            });
        }
    };
}();
$.validator.addMethod("noDecimal", function(value, element) {
    return !(value % 1);
}, "Please enter valid routing number.");
	
$('#amount').focus(function() {

    $('#vaultID').val('');

    $('#check_status').val('0');
    
    var cid = $('#customerID').val();
    var invoice_ids  = $('#invoice_ids').val();
    
    var isCustomize  = $('#isCustomize').val();

    if(cid!="" && isCustomize == 0 && invoice_ids != ''){
        $('#amount').attr('readonly',true);
        $('#amount_loader').show();
        $('#totalamount1').html($(this).val());
        $("#amountModel").modal("show");
    }else{
        if(cid!="" && isCustomize == 1  && invoice_ids != ''){
            $("#amountModel").modal("show");
        }
    }
});	
	
	</script>
</div>