   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>

	<div id="page-content">
	    

    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	               <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;
					?>
        <div class="block-title">
             <h2><strong><?php if(isset($item_pro)){ echo "Edit Product";}else{ echo "Create New Product"; }?></strong> </h2>
             </div>
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form  id="product_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>Xero_controllers/Xero_plan_product/create_product_services">
			
			 <input type="hidden"  id="productID" name="productID" value="<?php if(isset($item_pro)){echo $item_pro['Code']; } ?>" /> 
			 
			    <div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Code</label>
					<div class="col-md-7">
						<input type="text" id="productCode" name="productCode" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['Code']; } ?>"  placeholder="" data-args="Product Code" <?php if(isset($item_pro)){echo "disabled"; } ?>><?php echo form_error('productName'); ?>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Product Name</label>
					<div class="col-md-7">
						<input type="text" id="productName" name="productName" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['Name']; } ?>"  placeholder="" data-args="Product Name"><?php echo form_error('productName'); ?>
					</div>
				</div>	
				
		
					
				<div class="form-group">
					<label class="col-md-3 control-label" for="example-username"> Sales Product Description </label>
					<div class="col-md-7">
					<input type="text" id="productDesc" name="productDesc" class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['SalesDescription']; } ?>"  placeholder="" data-args="Product Description"> </div>
				</div>
				
					
				
				 <div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Sales Price </label>
						<div class="col-md-7">
							<input type="text" id="salePrice" name="salePrice"  class="form-control" value="<?php if(isset($item_pro)){echo number_format($item_pro['saleCost'],2, '.', ''); } ?>"  placeholder="" data-args="Product Sale Price" >
						</div>
				</div>

				
					
					 <div class="form-group">
					<div class="col-md-4 col-md-offset-9">
					 
					
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>
					 <a href="<?php echo base_url(); ?>Xero_controllers/Xero_plan_product/Item_detailes"  class="submit btn btn-sm btn-primary1 ">Cancel</a>		
					</div>
				    </div>	
          	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->
<script type="text/javascript">
    $('.datepicker').datepicker({
    format: 'mm/dd/yyyy'
});
</script>
<script>

$(document).ready(function(){


$("#track").change(function() {
    if(this.checked) {
         $('#track').val('1');
         $("#stock").show();
    }else{
         $('#track').val('');
        $("#stock").hide();
    }
});


$('#ser_type').change( function(){
				if ( $(this).val() == "Inventory")
				{ 
				  $("#stock").show();
				}else{
					$("#stock").hide();	
				}
		  });


    $('#product_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
					 'productDesc': {
                        required: true,
                       
                    },
					 
					 'productName': {
                        required: true,
                        minlength: 3
                    },
					
					'quantity':{
						required: true,
						  
					},
					'salePrice':{
					    required: true,
						 
					}					
			},
    });
    
  
	
    
});	

</script>

</div>


