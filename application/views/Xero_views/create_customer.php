   <!-- Page content -->
<?php
	$this->load->view('alert');
?>

	<div id="page-content">

	
    <!-- END Wizard Header -->  <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;
					?>
    <legend class="leg"><?php if(isset($customer)){ echo "Edit Customer";}else{ echo "Create New Customer"; }?></legend>                
    <!-- Progress Bar Wizard Block -->
    
	             
       
        <!-- Progress Bar Wizard Content -->
       
		
		 <form  id="customer_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>Xero_controllers/Customer/create_customer">
			<div class="block">
			 <input type="hidden"  id="customerListID" name="customerListID" value="<?php if(isset($customer)){echo $customer['Customer_ListID']; } ?>" /> 
			 	
			         <div class="form-group">
                                       <label class="col-md-4 control-label" for="companyName">Company Name </label>
                                     <div class="col-md-6">
                                      <div class="input-group">
                                                        <input type="text" id="companyName" name="companyName" class="form-control" placeholder="" data-args="Company Name">
                                     <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                </div>
                                     </div>
                    </div>	
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Customer Name<span class="text-danger">*</span> </label>
					<div class="col-md-6">
					<input type="text" id="fullName" name="fullName" class="form-control"  value="<?php if(isset($customer)){echo $customer['fullName']; } ?>" placeholder="" data-args="Customer Name"><?php echo form_error('fullName'); ?> </div>
					</div>	
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">First Name </label>
					<div class="col-md-6">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($customer)){echo $customer['firstName']; } ?>" placeholder="" data-args="First Name"><?php echo form_error('firstName'); ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Last Name </label>
					<div class="col-md-6">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($customer)){echo $customer['lastName']; } ?>" placeholder="" data-args="Last Name"><?php echo form_error('lastName'); ?> </div>
					</div>
					
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email Address</label>
					<div class="col-md-6">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($customer)){echo $customer['userEmail']; } ?>" placeholder="" data-args="Email Address"><?php echo form_error('fullName'); ?> </div>
					</div>
					<div class="form-group">
							<label class="col-md-4 control-label" for="example-email">Phone Number</label>
							<div class="col-md-6">
								<input type="text" id="phone" name="phone" class="form-control" value="<?php if(isset($customer)){echo $customer['phoneNumber']; } ?>" placeholder="" data-args="Phone Number">
							</div>
					</div>
				
                </div>
                <legend class="leg">Billing Address</legend>
			     	<div class="block" id="set_bill_data">
                                            
                                       	
											
                                         
										
									     <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="baddress1" name="address1" class="form-control "  placeholder="" data-args="Address Line 1">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
                                          
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="baddress2" name="address2" class="form-control " placeholder="" data-args="Address Line 2">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">City</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="bcity" name="city" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="City">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											<div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">State/Province</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="bstate" name="state" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="State/Province">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                          </div>	  
											  
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="bzipcode" name="zipcode" class="form-control" placeholder="" data-args="ZIP Code">
                                                        <span class="input-group-addon"><i class="gi gi-direction"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											 <div class="form-group">
												
												<label class="col-md-4 control-label" for="example-typeahead">Country</label>
												<div class="col-md-6">
												  <div class="input-group">
													<input type="text" id="bcountry" name="country" class="form-control input-typeahead" autocomplete="off" value="" placeholder="" data-args="Country">
													<span class="input-group-addon"><i class="gi gi-home"></i></span>
												</div>
												
                                            </div>
											</div>
                                        </div>
										 
                                         <legend class="leg">Shipping Address</legend>
                                         <div class="block">
											  <div class="col-md-12">
												 <input type="checkbox" id="chk_add_copy"> Same as Billing Address   
										      </div>	
									
										
									   
									     <div  style="margin-top: 16px;" class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="saddress1" name="saddress1" class="form-control "  placeholder="" data-args="Address Line 1">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="saddress2" name="saddress2" class="form-control "  placeholder="" data-args="Address Line 2">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
										   <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">City</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="scity" name="scity" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="City">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											 		<div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">State/Province</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="sstate" name="sstate" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="State/Province">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                          </div>
                                         
											  
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="szipcode" name="szipcode" class="form-control" placeholder="" data-args="ZIP Code">
                                                        <span class="input-group-addon"><i class="gi gi-direction"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
                                            
                                            	 <div class="form-group">												
												<label class="col-md-4 control-label" for="example-typeahead">Country</label>
												<div class="col-md-6">
												  <div class="input-group">
													<input type="text" id="scountry" name="scountry" class="form-control input-typeahead" autocomplete="off" value="" placeholder="" data-args="Country">
													<span class="input-group-addon"><i class="gi gi-home"></i></span>
												</div>												
                                            </div>
											</div>
											
									
                                        
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="reference">Reference Memo</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="reference" name="reference" class="form-control" placeholder="" data-args="Reference Memo">
                                                        <span class="input-group-addon"><i class="gi gi-notes"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
										
											 </fieldset>
                                             <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-7 text-right">
                                                    
                                            
                                                    <button type="submit" class="submit btn btn-sm btn-success">Save</button>   
                                                    <a href="<?php echo base_url(); ?>Xero_controllers/Customer/get_customer" class=" btn btn-sm btn-primary1">Cancel</a>   
                                                </div>
                                            </div>  
	                
		
		
  	</form>
		
        </div>
   
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>

$(document).ready(function(){

// alert ('aaaaa');
    $('#customer_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         
                    'firstName': {
                        minlength: 2
                    },
					 
					 'lastName': {
                        minlength: 2
                    },
					
					'userEmail':{
						  email:true,
						  
					},
				
					 'companyAddress1':{
					 },
                    'zipCode': {
                         minlength: 3
                    },
                     'phone': {
                       minlength: 10,
                         digits:true,
                         maxlength: 12,
                    },
					
			},
    });
    
 $('#chk_add_copy').click(function(){

     
     if($('#chk_add_copy').is(':checked')){
                            	$('#saddress1').val($('#baddress1').val());
								$('#saddress2').val($('#baddress2').val());
								$('#scity').val($('#bcity').val());
								$('#sstate').val($('#bstate').val());
								$('#szipcode').val($('#bzipcode').val());
								$('#scountry').val($('#bcountry').val());
     }
    else{
    	 
         var val_sp='';
                            	$('#saddress1').val(val_sp);
								$('#saddress2').val(val_sp);
								$('#scity').val(val_sp);
								$('#sstate').val(val_sp);
								$('#szipcode').val(val_sp);
								$('#scountry').val(val_sp);
    }
     
 });
    
    
    
    
    
    $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QBO_controllers/Customer_Details/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QBO_controllers/Customer_Details/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	
	
    
});	

</script>

</div>


