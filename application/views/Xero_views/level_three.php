<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
	<div class="full">
		<legend class="leg">Level 3</legend>
        <input type="hidden" name="tmpType" id="tmpType" value="<? echo $this->uri->segment(4); ?>" >
        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                <div class="block full tabpage-ul-li-div">
                    <!-- Block Tabs Title -->
                    <div class="block-title">
                    	<ul class="nav nav-tabs " data-toggle="tabs">
                           <li id="systemtmp" class="active"><a href="#visa_tab"><strong>Visa</strong></a></li>
                           <li id="customtmp" ><a href="#master_tab"><strong> Mastercard</strong></a></li>
                        </ul>
                    </div>
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="visa_tab">
                        	<div class="row">
						    	<form id="level_three_visa_form" action="<?php echo base_url('Integration/home/level_three_visa'); ?>" method="post" class="form-horizontal form-bordered">
							        <!-- Form Validation Example Block -->
							       	<div class="col-md-12">
							            <legend class="leg">Level 2 Data</legend>
								        <div class="">
								        	<fieldset>
								        		<div class="col-md-12">
								        			<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Customer Reference ID</label>
									                            <div class="col-md-8">
									                                <input type="text" id="customer_reference_id" name="customer_reference_id" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="We recommend leaving this field blank so that data from each unique customer and/or transaction may be used. Please provide an identifier for this Merchant of up to 15 characters." value="<?php echo (isset($level_three_visa_data['customer_reference_id']) && !empty($level_three_visa_data['customer_reference_id'])) ? $level_three_visa_data['customer_reference_id'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Local Tax (%)</label>
									                            <div class="col-md-8">
									                                <input type="text" id="local_tax" name="local_tax" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the local tax amount that is included in the transaction amount. The local tax amount may contain up to 12 numeric digits and a decimal. The default value is 5; however, it's strongly recommended to provide a local tax between 0.1% and 21% to ensure that the transaction qualifies." value="<?php echo (isset($level_three_visa_data['local_tax']) && !empty($level_three_visa_data['local_tax'])) ? $level_three_visa_data['local_tax'] : '5'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">National Tax</label>
									                            <div class="col-md-8">
									                                <input type="text" id="national_tax" name="national_tax" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the national tax amount that is included in the transaction amount. The national tax amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_visa_data['national_tax']) && !empty($level_three_visa_data['national_tax'])) ? $level_three_visa_data['national_tax'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
								        		</div>
								        	</fieldset>
								        </div>

								        <legend class="leg">Enhanced Data</legend>
								        <div class="">
								        	<fieldset>
								        		<div class="col-md-12">
								        			<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Merchant Tax ID</label>
									                            <div class="col-md-8">
									                                <input type="text" id="merchant_tax_id" name="merchant_tax_id" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="We recommend providing the tax ID for your business. If this is unavailable, we recommend leaving this field blank so that data from each unique transaction may be used.. Please provide your business Tax ID or other value that will help your customer identify your business. The merchant ID may contain up to 20 characters of free text" value="<?php echo (isset($level_three_visa_data['merchant_tax_id']) && !empty($level_three_visa_data['merchant_tax_id'])) ? $level_three_visa_data['merchant_tax_id'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
									                    </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Freight Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="freight_amount" name="freight_amount" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the freight amount that is included in the transaction amount. The freight amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_visa_data['freight_amount']) && !empty($level_three_visa_data['freight_amount'])) ? $level_three_visa_data['freight_amount'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Destination Country</label>
									                            <div class="col-md-8">
									                                <?php $destination_country =  (isset($level_three_visa_data['destination_country']) && !empty($level_three_visa_data['destination_country'])) ? $level_three_visa_data['destination_country'] : 'US';  ?>
									                                <select name="destination_country" rel="txtTooltip" data-toggle="tooltip" title="Please select the country where the product/order will be shipped" id="destination_country" class="form-control">
									                                	<option value="CA" <?php echo ($destination_country == 'CA') ? 'selected' : ''; ?> >Canada</option>
									                                	<option value="US" <?php echo ($destination_country == 'US') ? 'selected' : ''; ?>>United States</option>
									                                </select>
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Customer Tax ID</label>
									                            <div class="col-md-8">
									                                <input type="text" id="customer_tax_id" name="customer_tax_id" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="We recommend leaving this field blank so that data from each unique customer and/or transaction may be used. Please provide your customer's Tax ID or other value that will help you identify your customer. The customer ID may contain up to 13 characters of free text." value="<?php echo (isset($level_three_visa_data['customer_tax_id']) && !empty($level_three_visa_data['customer_tax_id'])) ? $level_three_visa_data['customer_tax_id'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Duty Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="duty_amount" name="duty_amount" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the duty amount that is included in the transaction amount. The duty amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_visa_data['duty_amount']) && !empty($level_three_visa_data['duty_amount'])) ? $level_three_visa_data['duty_amount'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax (Freight)</label>
									                            <div class="col-md-8">
									                                <input type="text" id="addtnl_tax_freight" name="addtnl_tax_freight" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the additional tax amount that was generated from freight costs. The additional tax amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_visa_data['addtnl_tax_freight']) && !empty($level_three_visa_data['addtnl_tax_freight'])) ? $level_three_visa_data['addtnl_tax_freight'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Commodity Code</label>
									                            <div class="col-md-8">
									                                <input type="text" id="commodity_code" name="commodity_code" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="We recommend providing the most descriptive commodity code that describes the majority of your products. Please provide the first 4 characters of the commodity code that best describes your business. Commodity codes are maintained by the International Standards Organization (ISO)." value="<?php echo (isset($level_three_visa_data['commodity_code']) && !empty($level_three_visa_data['commodity_code'])) ? $level_three_visa_data['commodity_code'] : '4900'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Destination Zip</label>
									                            <div class="col-md-8">
									                                <input type="text" id="destination_zip" name="destination_zip" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the destination zip where the product/order will be shipped." value="<?php echo (isset($level_three_visa_data['destination_zip']) && !empty($level_three_visa_data['destination_zip'])) ? $level_three_visa_data['destination_zip'] : '99212'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Rate</label>
									                            <div class="col-md-8">
									                                <input type="text" id="addtnl_tax_rate" name="addtnl_tax_rate" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the rate used to calculate the additional tax amount from freight costs. The additional tax rate may contain up to 4 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_visa_data['addtnl_tax_rate']) && !empty($level_three_visa_data['addtnl_tax_rate'])) ? $level_three_visa_data['addtnl_tax_rate'] : '0'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Discount Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" required id="discount_rate" name="discount_rate" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the discount amount that is included in the transaction amount. The discount amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_visa_data['discount_rate']) && !empty($level_three_visa_data['discount_rate'])) ? $level_three_visa_data['discount_rate'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
								                        	<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Source Zip</label>
									                            <div class="col-md-8">
									                                <input type="text" id="source_zip" name="source_zip" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the zip from where the product/order will be shipped." value="<?php echo (isset($level_three_visa_data['source_zip']) && !empty($level_three_visa_data['source_zip'])) ? $level_three_visa_data['source_zip'] : '99212'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
								        		</div>
								        	</fieldset>
								        </div>

								        <legend class="leg">Line Item Data</legend>
								        <div class="">
								        	<fieldset>
								        		<div class="col-md-12">
								        			<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Commodity Code</label>
									                            <div class="col-md-8">
									                                <input type="text" id="line_item_commodity_code" name="line_item_commodity_code" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="We recommend providing the most descriptive commodity code that describes the majority of your products. Please provide the first 4 characters of the commodity code that best describes your business. Commodity codes are maintained by the International Standards Organization (ISO)." value="<?php echo (isset($level_three_visa_data['line_item_commodity_code']) && !empty($level_three_visa_data['line_item_commodity_code'])) ? $level_three_visa_data['line_item_commodity_code'] : '4900'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="addtnl_tax_amount" name="addtnl_tax_amount" class="form-control" rel="txtTooltip" data-toggle="tooltip" title="Please provide any additional tax amount that was included in the order amount in reference to the product defined in this line item. The additional tax amount may contain up to 12 numeric digits and a decimal." value="<?php echo (isset($level_three_visa_data['addtnl_tax_amount']) && !empty($level_three_visa_data['addtnl_tax_amount'])) ? $level_three_visa_data['addtnl_tax_amount'] : '0'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Description</label>
									                            <div class="col-md-8">
									                                <input type="text" id="description" name="description" class="form-control" rel="txtTooltip" data-toggle="tooltip" title="Unless there is a description that applies to the majority of your products, we recommend leaving this field blank so that data from each unique transaction may be used. Please provide a description of the product(s) included in this line item. Descriptions may contain up to 35 characters of free text." value="<?php echo (isset($level_three_visa_data['description']) && !empty($level_three_visa_data['description'])) ? $level_three_visa_data['description'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
									                        
								                        </div>
								                        <div class="col-md-4">
								                        	<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Rate</label>
									                            <div class="col-md-8">
									                                <input type="text" id="line_item_addtnl_tax_rate" name="line_item_addtnl_tax_rate" class="form-control" rel="txtTooltip" data-toggle="tooltip" title="Please provide the rate that was used to calculate any additional tax amount that was included in the order amount in reference to the product defined in this line item. The additional tax rate may contain up to 4 numeric digits and a decimal." value="<?php echo (isset($level_three_visa_data['line_item_addtnl_tax_rate']) && !empty($level_three_visa_data['line_item_addtnl_tax_rate'])) ? $level_three_visa_data['line_item_addtnl_tax_rate'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Product Code</label>
									                            <div class="col-md-8">
									                                <input type="text" id="product_code" name="product_code" class="form-control" rel="txtTooltip" data-toggle="tooltip" title="Unless there is a product code that applies to the majority of your products, we recommend leaving this field blank so that data from each unique transaction may be used. Please provide a product code or ID that may uniquely identify this product in your inventory. Product Codes may contain up to 12 characters of free text." value="<?php echo (isset($level_three_visa_data['product_code']) && !empty($level_three_visa_data['product_code'])) ? $level_three_visa_data['product_code'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Discount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="discount" name="discount" class="form-control" rel="txtTooltip" data-toggle="tooltip" title="Please provide the discount amount that was applied to the order in reference to this product. The discount amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_visa_data['discount']) && !empty($level_three_visa_data['discount'])) ? $level_three_visa_data['discount'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
														<div class="col-md-4">
								                        	<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Unit Measure/Code</label>
									                            <div class="col-md-8">
									                                <input type="text" id="unit_measure_code" name="unit_measure_code" class="form-control" rel="txtTooltip" data-toggle="tooltip" title="Please provide the unit measure/code that is used to measure your products, such as pounds, gallons, liters, case, crate, etc. The unit measure/code may contain up to 12 characters of free text." value="<?php echo (isset($level_three_visa_data['unit_measure_code']) && !empty($level_three_visa_data['unit_measure_code'])) ? $level_three_visa_data['unit_measure_code'] : 'EACH'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
								        		</div>
								        	</fieldset>
								        </div>
								        <div class="col-md-12 bottom_right_mechant_btn">
						                    <div class="pull-right">
						                        <button type="submit" class="submit btn btn-sm btn-success">Update</button>
						                    </div>
						                </div>
								    </div>
							    </form>
						    </div>
                        </div>
                        <div class="tab-pane" id="master_tab">
                        	<div class="row">
						    	<form id="level_three_master_form" action="<?php echo base_url('Integration/home/level_three_master_card'); ?>" method="post" class="form-horizontal form-bordered">
							        <!-- Form Validation Example Block -->
							       	<div class="col-md-12">
							            <legend class="leg">Level 2 Data</legend>
								        <div class="">
								        	<fieldset>
								        		<div class="col-md-12">
								        			<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Customer Reference ID</label>
									                            <div class="col-md-8">
									                                <input type="text" id="customer_reference_id" name="customer_reference_id" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="We recommend leaving this field blank so that data from each unique customer and/or transaction may be used. Please provide an identifier for this Merchant of up to 15 characters." value="<?php echo (isset($level_three_master_data['customer_reference_id']) && !empty($level_three_master_data['customer_reference_id'])) ? $level_three_master_data['customer_reference_id'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Local Tax (%)</label>
									                            <div class="col-md-8">
									                                <input type="text" id="local_tax" name="local_tax" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the local tax amount that is included in the transaction amount. The local tax amount may contain up to 12 numeric digits and a decimal. The default value is 5; however, it's strongly recommended to provide a local tax between 0.1% and 21% to ensure that the transaction qualifies." value="<?php echo (isset($level_three_master_data['local_tax']) && !empty($level_three_master_data['local_tax'])) ? $level_three_master_data['local_tax'] : '5'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">National Tax</label>
									                            <div class="col-md-8">
									                                <input type="text" id="national_tax" name="national_tax" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the national tax amount that is included in the transaction amount. The national tax amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_master_data['national_tax']) && !empty($level_three_master_data['national_tax'])) ? $level_three_master_data['national_tax'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
								        		</div>
								        	</fieldset>
								        </div>

								        <legend class="leg">Enhanced Data</legend>
								        <div class="">
								        	<fieldset>
								        		<div class="col-md-12">
								        			<div class="row">
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Freight Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="freight_amount" name="freight_amount" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the freight amount that is included in the transaction amount. The freight amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_master_data['freight_amount']) && !empty($level_three_master_data['freight_amount'])) ? $level_three_master_data['freight_amount'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Destination Country</label>
									                            <div class="col-md-8">
									                            	<?php $destination_country =  (isset($level_three_master_data['destination_country']) && !empty($level_three_master_data['destination_country'])) ? $level_three_master_data['destination_country'] : 'US';  ?>
									                                <select name="destination_country"  rel="txtTooltip" data-toggle="tooltip" title="Please select the country where the product/order will be shipped." id="destination_country" class="form-control">
									                                	<option value="CA" <?php echo ($destination_country == 'CA') ? 'selected' : ''; ?> >Canada</option>
									                                	<option value="US" <?php echo ($destination_country == 'US') ? 'selected' : ''; ?>>United States</option>
									                                </select>
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Duty Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="duty_amount" name="duty_amount" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the duty amount that is included in the transaction amount. The duty amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_master_data['duty_amount']) && !empty($level_three_master_data['duty_amount'])) ? $level_three_master_data['duty_amount'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
								                        	<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="addtnl_tax_amount" name="addtnl_tax_amount" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the additional tax amount that may apply if the order is shipped to a country that has an additional tax amount. The additional tax amount may contain up to 9 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_master_data['addtnl_tax_amount']) && !empty($level_three_master_data['addtnl_tax_amount'])) ? $level_three_master_data['addtnl_tax_amount'] : '0'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Destination Zip</label>
									                            <div class="col-md-8">
									                                <input type="text" id="destination_zip" name="destination_zip" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the destination zip where the product/order will be shipped." value="<?php echo (isset($level_three_master_data['destination_zip']) && !empty($level_three_master_data['destination_zip'])) ? $level_three_master_data['destination_zip'] : '99212'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Indicator</label>
									                            <div class="col-md-8">
									                            	<?php
									                            		$addtnl_tax_indicator = 'N';
									                            		if(isset($level_three_master_data['addtnl_tax_indicator']) && $level_three_master_data['addtnl_tax_indicator'] == 'Y'){
									                            			$addtnl_tax_indicator = 'Y';
									                            		}
									                            	?>
									                            	<select name="addtnl_tax_indicator" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please indicate whether additional (international) tax was included in the original transaction amount.">
																		<option value="Y" <?php echo ($addtnl_tax_indicator == 'Y') ? 'selected' : '';  ?> >Addtnl tax is included</option>
																		<option value="N" <?php echo ($addtnl_tax_indicator == 'N') ? 'selected' : '';  ?> >Addtnl tax is NOT included</option>
																	</select>
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								                        <div class="col-md-4">
								                        	<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Source Zip</label>
									                            <div class="col-md-8">
									                                <input type="text" id="source_zip" name="source_zip" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the zip from where the product/order will be shipped." value="<?php echo (isset($level_three_master_data['source_zip']) && !empty($level_three_master_data['source_zip'])) ? $level_three_master_data['source_zip'] : '99212'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
								        		</div>
								        	</fieldset>
								        </div>

								        <legend class="leg">Line Item Data</legend>
								        <div class="">
								        	<fieldset>
								        		<div class="col-md-12">
								        			<div class="row">
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Description</label>
									                            <div class="col-md-8">
									                                <input type="text" id="description" name="description" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Unless there is a description that applies to the majority of your products, we recommend leaving this field blank so that data from each unique transaction may be used. Please provide a description of the product(s) included in this line item. Descriptions may contain up to 35 characters of free text." value="<?php echo (isset($level_three_master_data['description']) && !empty($level_three_master_data['description'])) ? $level_three_master_data['description'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
								                        	<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Rate</label>
									                            <div class="col-md-8">
									                                <input type="text" id="addtnl_tax_rate" name="addtnl_tax_rate" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the rate that was used to calculate any additional tax amount that was included in the order amount in reference to the product defined in this line item. The additional tax rate may contain up to 4 numeric digits and a decimal." value="<?php echo (isset($level_three_master_data['addtnl_tax_rate']) && !empty($level_three_master_data['addtnl_tax_rate'])) ? $level_three_master_data['addtnl_tax_rate'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-5 control-label" for="example-username">Debit/Credit Indicator</label>
									                            <div class="col-md-7">
									                            	<?php
									                            		$debit_credit_indicator = 'D';
									                            		if(isset($level_three_master_data['debit_credit_indicator']) && $level_three_master_data['debit_credit_indicator'] == 'C'){
									                            			$debit_credit_indicator = 'C';
									                            		}
									                            	?>
									                            	<select name="debit_credit_indicator" class="form-control SELECT " rel="txtTooltip" data-toggle="tooltip" title="Please select whether the net impact of this line item was a debit or a credit to the original transaction amount.">
																		<option value="C" <?php echo ($debit_credit_indicator == 'C') ? 'selected' : '';  ?> >Extended item amount is credit</option>
																		<option value="D" <?php echo ($debit_credit_indicator == 'D') ? 'selected' : '';  ?> >Extended item amount is debit</option>
																	</select>
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								                        
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Product Code</label>
									                            <div class="col-md-8">
									                                <input type="text" id="product_code" name="product_code" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title=" Unless there is a product code that applies to the majority of your products, we recommend leaving this field blank so that data from each unique transaction may be used. Please provide a product code or ID that may uniquely identify this product in your inventory. Product Codes may contain up to 12 characters of free text." value="<?php echo (isset($level_three_master_data['product_code']) && !empty($level_three_master_data['product_code'])) ? $level_three_master_data['product_code'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Type</label>
									                            <div class="col-md-8">
									                                <input type="text" maxlength="4" id="addtnl_tax_type" name="addtnl_tax_type" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the type of additional tax entered into the additional tax amount field. The additional tax type may contain up to 4 characters of free text." value="<?php echo (isset($level_three_master_data['addtnl_tax_type']) && !empty($level_three_master_data['addtnl_tax_type'])) ? $level_three_master_data['addtnl_tax_type'] : '0'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-5 control-label" for="example-username">Discount</label>
									                            <div class="col-md-7">
									                                <input type="text" id="discount" name="discount" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the discount amount that was applied to the order in reference to this product. The discount amount may contain up to 12 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_master_data['discount']) && !empty($level_three_master_data['discount'])) ? $level_three_master_data['discount'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
														<div class="col-md-4">
								                        	<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Unit Measure/Code</label>
									                            <div class="col-md-8">
									                                <input type="text" id="unit_measure_code" name="unit_measure_code" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide the unit measure/code that is used to measure your products, such as pounds, gallons, liters, case, crate, etc. The unit measure/code may contain up to 12 characters of free text." value="<?php echo (isset($level_three_master_data['unit_measure_code']) && !empty($level_three_master_data['unit_measure_code'])) ? $level_three_master_data['unit_measure_code'] : 'EACH'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Addtnl Tax Amount</label>
									                            <div class="col-md-8">
									                                <input type="text" id="line_item_addtnl_tax_rate" name="line_item_addtnl_tax_rate" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="Please provide any additional tax amount that was included in the order amount in reference to the product defined in this line item. The additional tax amount may contain up to 12 numeric digits and a decimal." value="<?php echo (isset($level_three_master_data['line_item_addtnl_tax_rate']) && !empty($level_three_master_data['line_item_addtnl_tax_rate'])) ? $level_three_master_data['line_item_addtnl_tax_rate'] : '0'; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
								                        </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-5 control-label" for="example-username">Discount Rate</label>
									                            <div class="col-md-7">
									                                <input type="text" id="discount_rate" name="discount_rate" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title=" Please provide the discount rate used to calculate the discount amount that was applied to the order in reference to this product. The discount rate amount may contain up to 4 numeric digits and a decimal. The default value is 0." value="<?php echo (isset($level_three_master_data['discount_rate']) && !empty($level_three_master_data['discount_rate'])) ? $level_three_master_data['discount_rate'] : '0'; ?>" placeholder="" data-args="">
									                            </div>
									                        </div>
								                        </div>
													</div>
													<div class="row">
								        				<div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Merchant Tax ID</label>
									                            <div class="col-md-8">
									                                <input type="text" id="merchant_tax_id" name="merchant_tax_id" class="form-control"  rel="txtTooltip" data-toggle="tooltip" title="We recommend providing the tax ID for your business. If this is unavailable, we recommend leaving this field blank so that data from each unique transaction may be used. Please provide your business Tax ID or other value that will help your customer identify your business. The merchant ID may contain up to 15 characters of free text." value="<?php echo (isset($level_three_master_data['merchant_tax_id']) && !empty($level_three_master_data['merchant_tax_id'])) ? $level_three_master_data['merchant_tax_id'] : ''; ?>" placeholder="" data-args="Show Hint">
									                            </div>
									                        </div>
									                    </div>
								                        <div class="col-md-4">
															<div class="form-group">
									                            <label class="col-md-4 control-label" for="example-username">Net/Gross Indicator</label>
									                            <div class="col-md-8">
									                            	<?php
									                            		$net_gross_indicator = 'N';
									                            		if(isset($level_three_master_data['net_gross_indicator']) && $level_three_master_data['net_gross_indicator'] == 'Y'){
									                            			$net_gross_indicator = 'Y';
									                            		}
									                            	?>
									                                <select name="net_gross_indicator" class="form-control" rel="txtTooltip" data-toggle="tooltip" title="Please select whether or not this line item amount includes tax." >
																		<option value="Y" <?php echo ($net_gross_indicator == 'Y') ? 'selected' : '';  ?>>Item amount does include tax</option>
																		<option value="N"  <?php echo ($net_gross_indicator == 'N') ? 'selected' : '';  ?> >Item amount does NOT include tax</option>
																	</select>
									                            </div>
									                        </div>
								                        </div>
													</div>
								        		</div>
								        	</fieldset>
								        </div>
								        <div class="col-md-12 bottom_right_mechant_btn">
						                    <div class="pull-right">
						                        <button type="submit" class="submit btn btn-sm btn-success">Update</button>
						                    </div>
						                </div>
								    </div>
							    </form>
						    </div> 
                        </div>
                    </div>
                    <!-- END Tabs Content -->
                </div>
                <!-- END Block Tabs -->
            </div>
           
        </div>
    <!-- END Working Tabs Content -->
	</div>
</div>
<link href="<?php echo base_url(CSS.'jquery-ui-1-12-1.css'); ?>" rel="stylesheet" />
<script src="<?php echo base_url(JS.'jquery-ui-1-12-1.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('input[rel="txtTooltip"]').tooltip({
		  	position: { my: "left+15 center", at: "right" }
		});
		$("#level_three_master_form").validate({
			ignoreTitle: true,
		    rules: {
		       	addtnl_tax_amount: {
		        	required: true,
		    	},
		    	description: {
		        	required: true,
		        	maxlength: 35
		    	},
		    	product_code: {
		        	required: true,
		    	},
		    	merchant_tax_id: {
		        	required: true,
		    	},
		    	destination_zip: {
		        	required: true,
		    	},
		    	source_zip: {
		        	required: true,
		    	},
		    	addtnl_tax_rate: {
		        	required: true,
		        	max: 99
		    	},
		    	addtnl_tax_type: {
		        	required: true,
		        	maxlength: 4
		    	},
		    	discount_rate: {
		    		max: 99 
		    	},
		       	local_tax: {
		        	required: true,
		      	},
		       	national_tax: {
		        	required: true,
		      	}
		    }
		});

		$("#level_three_visa_form").validate({
			ignoreTitle: true,
		    rules: {
		       	discount_rate: {
		        	required: true,
		    	},
		       	freight_amount: {
		        	required: true,
		      	},
		       	duty_amount: {
		        	required: true,
		      	},
		       	line_item_commodity_code: {
		        	required: true,
		      	},
		       	description: {
		        	required: true,
		        	maxlength: 35
		      	},
		       	discount: {
		        	required: true,
		      	},
		       	source_zip: {
		        	required: true,
		      	},
		       	destination_zip: {
		        	required: true,
		      	},
		       	product_code: {
		        	required: true,
		      	},
		       	addtnl_tax_freight: {
		        	required: true,
		      	},
		       	addtnl_tax_rate: {
		        	required: true,
		        	max: 99
		      	},
		       	line_item_addtnl_tax_rate: {
		        	required: true,
		        	max: 99
		      	},
		       	addtnl_tax_amount: {
		        	required: true,
		      	},
		       	commodity_code: {
		        	required: true,
		      	},
		       	local_tax: {
		        	required: true,
		      	},
		       	national_tax: {
		        	required: true,
		      	}
		    }
		});
	});
</script>