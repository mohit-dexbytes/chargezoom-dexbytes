<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    

    <!-- All Orders Block -->
   
        <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        <legend class="leg">Transaction History</legend>
    <div class="full">
		
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="pay_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class="text-left">Customer Name</th>
                    <th class="text-right">Invoice</th>
                     <th class="text-right hidden-xs">Amount</th>
                    
                    <th class="text-right ">Date </th>
                     <th class="text-right hidden-xs">Type</th>
                     <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                   
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					
					<td class="test-left"><?php echo $transaction['fullName']; ?></a></td>
				
					<td class="text-right "><?php echo ($transaction['invoiceID'])?$transaction['invoiceID']:'--'; ?></td>
					<td class="hidden-xs text-right">$<?php echo ($transaction['transactionAmount'])?number_format($transaction['transactionAmount'], 2):'0.00'; ?></td>
					
				
					<td class=" text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
						<td class="hidden-xs text-right">
						   <?php 
                           $showRefund = 0;
                           if($transaction['partial']==$transaction['transactionAmount']){echo "<label class='label label-success'>Fully Refunded: ".'$'.number_format($transaction['partial'],2)."</label><br/>"; }
                              
                            else if($transaction['partial']!='0') {
                            $showRefund = 1;
                             echo "<label class='label label-warning'>Partially Refunded: ".'$'.number_format($transaction['partial'],2)." </label><br/>";}
                              else{ $showRefund = 1;
                                echo "";
                                 }
                           if (strpos($transaction['transactionType'], 'sale') !== false) { 
                            echo "Sale";
                        }else if(strpos($transaction['transactionType'], 'auth') !== false){
                             echo "Authorization";
                         }else if(strpos($transaction['transactionType'], 'capture') !== false){
                             echo "Authorization";
                         }
                     else if(strpos($transaction['transactionType'], 'refund') !== false){
                            $showRefund = 0;
                             echo "Refund";
                         }
                         else if(strpos($transaction['transactionType'], 'void') !== false){
                             echo "Void";
                         }
                         else if(strpos($transaction['transactionType'], 'credit') !== false){
                             echo "Credit";
                         }
                         ?>						
						</td>
					<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>

				
                 
               <td class="text-center hidden-xs">
                      <div class="btn-group dropbtn">
                            <?php if($showRefund == 0) { 
                                    $disabled_select = 'disabled'; 
                                }else{
                                    $disabled_select = '';
                                } 
                            ?>
                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm  <?php echo $disabled_select; ?> dropdown-toggle">Select <span class="caret"></span></a>
                            <ul class="dropdown-menu text-left">
                     
                          <?php 
					    
					        if(in_array($transaction['transactionCode'], array('100','200','111','1')) && 
					        in_array(strtoupper($transaction['transactionType']), array('SALE','AUTH_CAPTURE','Offline Payment', 'PAYPAL_SALE','PRIOR_AUTH_CAPTURE' ,'PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE'))
					        ){     if( $transaction['tr_Day']==0 &&  ($transaction['transactionGateway']=='2' || $transaction['transactionGateway']=='3')  ){      ?>
					   
					    
					    <li> <a href="javascript:void(0);" class="" data-title="Pending Transaction" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
					    <?php }else{  ?>
					     <li> <a href="javascript:void(0)" id="txnRefund<?php  echo $transaction['transactionID']; ?>" transaction-id="<?php  echo $transaction['transactionID']; ?>" transaction-gatewayType="<?php echo $transaction['transactionGateway'];?>" transaction-gatewayName="<?php echo $transaction['gateway'];?>" integration-type="2" class="refunAmountCustom" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
					  
					 
					 <?php }  }?>
                      <li><a href="#payment_delete" class=""  onclick="set_transaction_pay('<?php  echo $transaction['id']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a></li>
                        <?php 
                            $transaction_auto_id = $transaction['transactionID'];
                        ?>
                        <li><a href="javascript:void('0');" onclick="getPrintTransactionReceiptData('<?php echo $transaction_auto_id; ?>', 4)">Print</a></li>
                      </ul>
                      </div>
                      
                      </td>        
				</tr>
				
                         
                         
				<?php } }
				else {  echo'<tr><td colspan="7"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                </tr>'; }  
					?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
<div id="print-transaction-div" style="display: none;"></div>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#pay_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [6] }
                ],
                order: [[ 5, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>


<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	


</div>
<!-- END Page Content -->