<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <div class="msg_data">
			    <?php echo $this->session->flashdata('message'); ?>
	</div>
   

    <!-- END Forms General Header -->

        <!-- Form Validation Example Block -->
		<legend class="leg">Merchant Gateways </legend>
		<div class="" style="position: relative;" >  
					    
					          
					       
		<div class="addNewBtnCustom" >
            <a class="btn btn-sm btn-info"  title="Set Default Gateway" data-backdrop="static" data-keyboard="false" data-toggle="modal" href="#set_def_gateway">Set Default Gateway</a>
            <a href="#add_gateway" class="btn btn-sm btn-success"  onclick="" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Create New" >Add New</a>
                         
        </div>



					<!-- All Orders Content -->
        <table id="merch_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class=" text-left">Gateway </th>
					<th class=" text-left">Friendly Name</th>
					<th class="hidden-xs text-left">User Name</th>
					<th class="text-center"> Action </th>
                </tr>
            </thead>
            <tbody>



			<?php
if (isset($gateways) && $gateways) {
    foreach ($gateways as $gateway) {

        ?>
				<tr>

					<td class="text-left "><?php if ($gateway['gatewayType'] == '5') {echo "Stripe";} else if ($gateway['gatewayType'] == '4') {echo "Paypal";} else if ($gateway['gatewayType'] == '1') {echo "NMI";} else if ($gateway['gatewayType'] == '2') {echo "Authorize.Net";} else if ($gateway['gatewayType'] == '6') {echo "USAePay";} else if ($gateway['gatewayType'] == '9') {echo "Chargezoom";} else {
            echo "Pay Trace";}?> </a> </td>


					<td class="text-left "><?php echo $gateway['gatewayFriendlyName']; ?> </a> </td>

                    <td class="text-left hidden-xs"><?php echo $gateway['gatewayUsername']; ?> </a> </td>

					<?php 
						if ($gateway['set_as_default']) { 
							$delURL = '';
							$disbaled = 'disabled';
						} else {
							$delURL = 'del_gateway';
							$disbaled = 'onclick="del_gateway_id('.$gateway['gatewayID'].');"';
						} 
					?>

				<td class="text-center">
					<div class="btn-group btn-group-xs">

						<a href="#edit_gateway" class="btn btn-default" onclick="set_edit_gateway('<?php echo $gateway['gatewayID']; ?>');" title="Edit Details" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-edit"> </i> </a>

						<a href="#<?php echo $delURL; ?>" <?php echo $disbaled; ?> data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
				</div>

		   </td>
		</tr>

				<?php }}?>

			</tbody>
        </table>
        </br>

        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>


<!-- END Page Content -->
<div id="set_def_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Set Default Gateway</h2>


            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Integration/home/set_gateway_default' class="form-horizontal card_form" autocomplete="off" >



                                                <div class="form-group ">

                                                <label class="col-md-4 control-label">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gatewayid" name="gatewayid" class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if (isset($gateways) && !empty($gateways)) {
    foreach ($gateways as $gateway_data) {
        ?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>"  <?php if ($gateway_data['set_as_default'] == '1') {echo 'selected';}?>   ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
                                                                <?php }}?>
                                                    </select>

                                                </div>
                                            </div>



                    <div class="pull-right">
                         <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">

                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-info" value="Save"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

     <!------------ Add popup for gateway   ------->

 <div id="add_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New </h2>


            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="nmiform" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>Integration/home/create_gateway">

			<input type="hidden" id="gatewayID" name="creditID" value=""  />

		       <div class="form-group ">

				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						<div class="col-md-6">
						   <input type="text" id="frname" name="frname"  class="form-control " />

						</div>

					</div>

					<div class="form-group ">

						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt"  class="form-control">

								   <?php foreach ($all_gateway as $gat_data) {
    echo '<option value="' . $gat_data['gateID'] . '" >' . $gat_data['gatewayName'] . '</option>';
}
?>

							</select>
							</div>
					</div>

				<div id="nmi_div" style="display:none">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="" data-args="Username">
                        </div>
                   </div>

				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="" data-args="Password">

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checcked" >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >

                        </div>
                    </div>
				</div>

				<div id="cz_div" style="display:none">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="czUser" class="form-control"  placeholder="" data-args="Username">
                        </div>
                   </div>

				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="czPassword" name="czPassword" class="form-control"  placeholder="" data-args="Password">

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" checked="checcked" >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status"  >

                        </div>
                    </div>
				</div>

               <div id="auth_div" style="display:none">

				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="" data-args="API LoginID">
                        </div>
                    </div>

				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="" data-args="Transaction Key">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checcked" >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >

                        </div>
                    </div>
			 </div>


				<div id="pay_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="" data-args="PayTrace  Username">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="" data-args="PayTrace  password">

                        </div>
                    </div>

				</div>

				<div id="paypal_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control" placeholder="" data-args="Enter  Username">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  placeholder="" data-args="Enter  password">

                        </div>
                    </div>

					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  placeholder="" data-args="Enter  Signature">

                        </div>
                    </div>

				</div>
				<div id="stripe_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control" placeholder="" data-args="Publishable Key">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  placeholder="" data-args="Secret API Key">

                        </div>
                    </div>

				</div>

				 <div id="usaepay_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; placeholder="" data-args="USAePay Transaction Key">
                        </div>
                  </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  placeholder="" data-args="USAePay PIN">

                        </div>
                    </div>
				</div>
                    <div class="form-group hidden">
                        <label class="col-md-4 control-label" for="card_number">Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="gatewayMerchantID" name="gatewayMerchantID" class="form-control"  placeholder="" data-args="Merchant ID (optional)">

                        </div>

                   </div>


	         <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Add</button>

                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>

                    </div>
             </div>

	   </form>

            </div>

            <!-- END Modal Body -->
        </div>
     </div>

  </div>





<!-------------------------- Modal for Edit Gateway ------------------------------>

<div id="edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
			 <div class="modal-header text-center">
                <h2 class="modal-title">Edit Gateway</h2> </div>

            	<div class="modal-body">
			<form method="POST" id="nmiform1" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>Integration/home/update_gateway">

			<input type="hidden" id="gatewayEditID" name="gatewayEditID" value=""  />


                     <div class="form-group">
						<label class="col-md-4 control-label" for="example-username"> Friendly Name</label>
						<div class="col-md-6">
								<input type="text" id="fname"  name="fname" class="form-control"  value="" placeholder="" data-args="">
							</div>
			     </div>

                    <div class="form-group ">

						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <input type="text" name="gateway" id="gateway" readonly class="form-control" />
						</div>
					</div>


				<div id="nmi_div1" style="display:none">
					<div class="form-group">
						<label class="col-md-4 control-label" for="customerID">Username</label>
						<div class="col-md-6">
								<input type="text" id="nmiUser1" name="nmiUser1" class="form-control"; placeholder="" data-args="Username..">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="card_number">Password</label>
						<div class="col-md-6">
							<input type="text" id="nmiPassword1" name="nmiPassword1" class="form-control"  placeholder="" data-args="password..">

						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="nmi_cr_status1">Credit Card</label>
						<div class="col-md-6">
							<input type="checkbox" id="nmi_cr_status1" name="nmi_cr_status1" checked="checked" >

						</div>
					</div>
						<div class="form-group">
						<label class="col-md-4 control-label" for="nmi_ach_status1">Electronic Check</label>
						<div class="col-md-6">
							<input type="checkbox" id="nmi_ach_status1" name="nmi_ach_status1"  >

						</div>
					</div>
				</div>

				<div id="cz_div1" style="display:none">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser1" name="czUser1" class="form-control"; placeholder="" data-args="Username..">
                        </div>
                   </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="text" id="czPassword1" name="czPassword1" class="form-control"  placeholder="" data-args="password..">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status1" name="cz_cr_status1" checked="checked" >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status1" name="cz_ach_status1"  >

                        </div>
                    </div>
				</div>
                 <div id="auth_div1" style="display:none">

					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID1" name="apiloginID1" class="form-control" placeholder="" data-args="API LoginID..">
                        </div>
                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey1" name="transactionKey1" class="form-control"  placeholder="" data-args="Transaction Key..">

                        </div>
                    </div>
                                        <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status1" name="auth_cr_status1" checked="checked" >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status1" name="auth_ach_status1"   >

                        </div>
                    </div>


				</div>

			  <div id="pay_div1" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser1" name="paytraceUser1" class="form-control"; placeholder="" data-args="PayTrace  Username">
                        </div>
                  </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword1" name="paytracePassword1" class="form-control"  placeholder="" data-args="PayTrace  password">

                        </div>
                    </div>
				</div>


				<div id="paypal_div1" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser1" name="paypalUser1" class="form-control" placeholder="" data-args="Enter  Username">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword1" name="paypalPassword1" class="form-control"  placeholder="" data-args="Enter  password">

                        </div>
                    </div>

					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature1" name="paypalSignature1" class="form-control"  placeholder="" data-args="Enter  Signature">

                        </div>
                    </div>

				</div>

					<div id="stripe_div1" style="display:none" >


					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser1" name="stripeUser1" class="form-control" placeholder="" data-args="Publishable Key">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword1" name="stripePassword1" class="form-control"  placeholder="" data-args="Secret API Key">

                        </div>
                    </div>

				</div>


				  <div id="usaepay_div1" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey1" name="transtionKey1" class="form-control"; placeholder="" data-args="USAePay Transaction Key">
                        </div>
                  </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin1" name="transtionPin1" class="form-control"  placeholder="" data-args="USAePay PIN">

                        </div>
                    </div>
				</div>

                     <div class="form-group hidden">
							<label class="col-md-4 control-label" for="example-username"> Merchant ID</label>
						<div class="col-md-6">
							<input type="text" id="mid"  name="mid" class="form-control"  value="" placeholder="" data-args="Merchant ID (optional)">
						</div>
			      </div>

	            <div class="form-group">
					<div class="col-md-4 pull-right">

					<button type="submit" class="submit btn btn-sm btn-success"> Save </button>

					<button  type="button" align="right" class="btn btn-sm  btn-primary1 close1" data-dismiss="modal"> Cancel </button>

                   </div>
            </div>
	 </form>
	 </div>
	<!--------- END ---------------->
		</div>

	</div>

 </div>

     <!------- Modal for Delete Gateway ------>

  <div id="del_gateway" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Gateway</h2>


            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_gateway" method="post" action='<?php echo base_url(); ?>Integration/home/delete_gateway' class="form-horizontal" >


					<p> Do you really want to delete this Gateway? </p>

				    <div class="form-group">

                        <div class="col-md-8">
                            <input type="hidden" id="merchantgatewayid" name="merchantgatewayid" class="form-control"  value="" />
                        </div>
                    </div>


                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />

			    </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<style>

.table.dataTable {
  width:100% !important;
 }
 .block-title h1{
font-size: 16px;
}


@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }

 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }

 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){
				   Pagination_view.init();
 });

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {

            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#merch_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 1, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();





function del_gateway_id(id){

	     $('#merchantgatewayid').val(id);
}


function set_edit_gateway(gatewayid)
{


	if(gatewayid !=""){
	
     $.ajax({
		url: '<?php echo base_url("Integration/home/get_gatewayedit_id") ?>',
		type: 'POST',
		data:{gatewayid:gatewayid},
		dataType: 'json',
		success: function(data){

		 $('#gatewayEditID').val(data.gatewayID);

		  if(data.gatewayType=='1')
			  {
				  $('#nmi_div1').show();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div').hide();
				  $('#heartland_div1').hide();
				  $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#nmiUser1').val(data.gatewayUsername);
			      $('#nmiPassword1').val(data.gatewayPassword);

			      if(data.creditCard=='1'){
			         $('#nmi_cr_status1').attr('checked','checked');

			      }else{
			        $('#nmi_cr_status1').removeAttr('checked');
			      }
			       if(data.echeckStatus=='1')
			       {

			     $('#nmi_ach_status1').attr('checked','checked');
			       }
			     else{

			     $('#nmi_ach_status1').removeAttr('checked');
			     }
              }

				if(data.gatewayType=='2')
				{
			      $('#nmi_div1').hide();
				  $('#auth_div1').show();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div').hide();
				  $('#heartland_div1').hide();
				  $('#cyber_div1').hide();
				  $('#cz_div1').hide();
				  $('#apiloginID1').val(data.gatewayUsername);
			      $('#transactionKey1').val(data.gatewayPassword);
			        if(data.creditCard==1){
			         $('#auth_cr_status1').attr('checked','checked');

			      }else{
			        $('#auth_cr_status1').removeAttr('checked');
			      }
			       if(data.echeckStatus==1)
			     $('#auth_ach_status1').attr('checked','checked');
			     else

			     $('#auth_ach_status1').removeAttr('checked');
				}

				if(data.gatewayType=='3')
				{

		       	   $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').show();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div').hide();
				  $('#heartland_div1').hide();
				   $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#paytraceUser1').val(data.gatewayUsername);
			      $('#paytracePassword1').val(data.gatewayPassword);
				}

              if(data.gatewayType=='4')
				{

		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').show();
				  $('#stripe_div1').hide();
				  $('#usaepay_div').hide();
				  $('#heartland_div1').hide();
				   $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#paypalUser1').val(data.gatewayUsername);
			      $('#paypalPassword1').val(data.gatewayPassword);
				  $('#paypalSignature1').val(data.gatewaySignature);
				}
               if(data.gatewayType=='5')
				{

		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').show();
				  $('#usaepay_div').hide();
				  $('#heartland_div1').hide();
				   $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#stripeUser1').val(data.gatewayUsername);
			      $('#stripePassword1').val(data.gatewayPassword);
				}
			   if(data.gatewayType=='6')
				{

		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div1').show();
				  $('#heartland_div1').hide();
				   $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#transtionKey1').val(data.gatewayUsername);
			      $('#transtionPin1').val(data.gatewayPassword);
				}
				if(data.gatewayType=='7')
				{

		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div1').hide();
				  $('#heartland_div1').show();
				   $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#heartpublickey1').val(data.gatewayUsername);
			      $('#heartsecretkey1').val(data.gatewayPassword);
				}

				if(data.gatewayType=='8')
				{

		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div1').hide();
				  $('#heartland_div1').hide();
				   $('#cyber_div1').show();
				  $('#cz_div1').hide();
		          $('#cyberMerchantID1').val(data.gatewayUsername);

			        $('#apiSerialNumber1').val(data.gatewayPassword);
			      $('#secretKey1').val(data.gatewaySignature);
				}

				if(data.gatewayType=='9')
				{
					$('#nmi_div1').hide();
					$('#auth_div1').hide();
					$('#pay_div1').hide();
					$('#paypal_div1').hide();
					$('#stripe_div1').hide();
					$('#usaepay_div').hide();
					$('#heartland_div1').hide();
					$('#cyber_div1').hide();
					$('#cz_div1').show();
					$('#czUser1').val(data.gatewayUsername);
					$('#czPassword1').val(data.gatewayPassword);

					if(data.creditCard=='1'){
						$('#cz_cr_status1').attr('checked','checked');

					}else{
						$('#cz_cr_status1').removeAttr('checked');
					}
					if(data.echeckStatus=='1')
					{
						$('#cz_ach_status1').attr('checked','checked');
					}
					else{
						$('#cz_ach_status1').removeAttr('checked');
					}
				}
				$('#fname').val(data.gatewayFriendlyName);
				$('#gateway').val(data.gateway);
				$('#mid').val(data.gatewayMerchantID);


	}
  });

  

  }

}


$(function(){
	$('#gateway_opt').change(function(){
		var gateway_value =$(this).val();
		if(gateway_value=='3'){
			$('#pay_div').show();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').hide();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		} else if(gateway_value=='2'){
			$('#auth_div').show();
			$('#pay_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').hide();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		} else if(gateway_value=='1'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').show();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').hide();
			$('#cz_div').hide();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
		} else if(gateway_value=='4'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').show();
			$('#usaepay_div').hide();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		} else if(gateway_value=='5'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').show();
			$('#paypal_div').hide();
			$('#usaepay_div').hide();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		} else if(gateway_value=='6'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').show();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		} else if(gateway_value=='7'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').hide();
			$('#heartland_div').show();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		} else if(gateway_value=='8'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').hide();
			$('#heartland_div').hide();
			$('#cyber_div').show();
			$('#cz_div').hide();
		} else if(gateway_value=='9'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').hide();
			$('#cz_div').show();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
		}

	});

	$('#nmiform').validate({ // initialize plugin
			ignore:":not(:visible)",
			rules: {

			'gateway_opt':{
					required: true,
			},

			'nmiUser': {
					required: true,
					minlength: 3
					},
				'nmiPassword':{
					required : true,
					minlength: 5,
					},
			'czUser': {
				required: true,
				minlength: 3
			},

			'czPassword':{
					required : true,
					minlength: 5,
			},



				'apiloginID':{
					required: true,
					minlength: 3
				},
				'transactionKey':{
					required: true,
					minlength: 3
				},

				'frname':{
					required: true,
					minlength: 2
				},

				'nmiUser1': {
					required: true,
					minlength: 3
				},
				'nmiPassword1':{
					required : true,
					minlength: 5,
					},
			'czUser1': {
					required: true,
					minlength: 3
			},

			'czPassword1':{
					required : true,
					minlength: 5,
			},

				'apiloginID1':{
					required: true,
					minlength: 3
				},

				'transactionKey1':{
					required: true,
					minlength: 3
				},


				'paytracePassword':{
					required: true,
					minlength: 5
				},

				'paytraceUser':{
					required: true,
					minlength: 3
				},

				'paytracePassword1':{
					required: true,
					minlength: 5
				},
				'paytraceUser1':{
					required: true,
					minlength: 3
				},

				'paypalPassword':{
					required: true,
					minlength: 5
				},

				'paypalUser':{
					required: true,
					minlength: 3
				},

				'paypalSignature':{
					required: true,
					minlength: 5
				},
				'paypalUser1':{
					required: true,
					minlength: 3
				},
				'paypalPassword1':{
					required: true,
					minlength: 5
				},

				'paypalSignature1':{
					required: true,
					minlength: 5
				},







				'fname':{
				required:true,
				minlength: 2,
				}


		},
	});

	$('#nmiform1').validate({ // initialize plugin
		 ignore:":not(:visible)",
		 rules: {

		  'gateway_opt':{
				 required: true,
		  },

		  'nmiUser': {
                 required: true,
                 minlength: 3
          },

		  'nmiPassword':{
				 required : true,
				 minlength: 5,
		  },

		  'czUser': {
				required: true,
				minlength: 3
		},

		'czPassword':{
				required : true,
				minlength: 5,
		},

		  'apiloginID':{
				required: true,
                minlength: 3
		  },

		  'transactionKey':{
				required: true,
                minlength: 3
		  },

		  'frname':{
				 required: true,
				 minlength: 2
		  },

		'nmiUser1': {
				required: true,
				minlength: 3
		},

		'nmiPassword1':{
				required : true,
				minlength: 5,
		},

		'czUser1': {
				required: true,
				minlength: 3
		},

		'czPassword1':{
				required : true,
				minlength: 5,
		},

         'apiloginID1':{
				required: true,
                 minlength: 3
		 },

		'transactionKey1':{
				required: true,
                 minlength: 3
		 },

		'paytracePassword':{
				required: true,
                 minlength: 3
		 },

		'paytraceUser':{
				required: true,
                 minlength: 3
		 },

        'paytracePassword1':{
				required: true,
                 minlength: 5
		 },

		'paytraceUser1':{
				required: true,
                 minlength: 3
		 },

		 'fname':{
			 required:true,
			 minlength: 2,
		 }


	 },
  	});

	var GetMyRemote=function(element_name){
		$('#update_btn').attr("disabled", true);
		remote={
					beforeSend:function() {
						$("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

					},
					complete:function() {
						$(".overlay1").remove();
					},
					url: '<?php echo base_url(); ?>Payments/testNMI',
				type: "post",
				data:
					{
						nmiuser: function(){return $('input[name=nmiUser]').val();},
						nmipassword:  function(){return $('input[name=nmiPassword]').val();}
					},
						dataFilter:function(response){

						data=$.parseJSON(response);
							if (data['status'] === 'true') {


							$('#update_btn').removeAttr('disabled');
							$('#nmiPassword').removeClass('error');
								$('#nmiPassword-error').hide();

									return data['status'] ;

							} else {

								if(data['status'] =='false'){
									$('#nmiform').validate().showErrors(function(){

													return {key:'nmiPassword'};
													})
								}
							}

							return JSON.stringify(data[element_name]);
						}
				}

		return remote;
	}
});

</script>

