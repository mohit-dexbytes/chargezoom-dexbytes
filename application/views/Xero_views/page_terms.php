<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    
    
               
        	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
         
  
        <!-- Form Validation Example Block -->
	
				
				<div class="block-main blk">
					     <div class="block-title blk-ttl">
					      
					        <div class="block-options termsBtnGrp">
							
                            
                    <a class="btn btn-sm btn-info default_gateway" data-backdrop="static" data-keyboard="false" data-toggle="modal" href="#set_def_terms" data-original-title="" title="">Set Default Payment Term</a>
                    <a href="#add_payment_terms" class="btn btn-sm btn-success"  onclick="reset_form();" data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Add New</a>
                            
                  </div>
                           
        </div> 
				
				
					
					<!-- All Orders Content -->
        <table id="merch_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="hidden-xs text-left">Name</th>
					<th class="text-left">Net Terms (Days)</th>
					<th class="text-center"> Action </th>
                </tr>
            </thead>
            <tbody>
			
			</tr>
			<?php 
			
				if(isset($netterms) && $netterms)
				{
					foreach($netterms as $terms)
					{
						
					if($terms['enable'] != 1){	
				?>
				<tr>
					
					<td class="text-left hidden-xs">
					    <?php 
					         if($terms['pt_name'] != ""){
					             $name = $terms['pt_name'];
					        echo $terms['pt_name'];
					    }else{
					        $name = $terms['name'];
					        echo $terms['name'];
					    }	
					    ?> </a> </td>
					
					<td class="text-left">
					<?php 
					   	 if($terms['pt_netTerm'] != ""){
					   	     $term = $terms['pt_netTerm'];
					        echo $terms['pt_netTerm'];
					    }else{
					         $term = $terms['netTerm'];
					        echo $terms['netTerm'];
					    }			
					?></a> </td>
				
					
				<td class="text-center">
				    <?php 
				    if( $terms['netTerm'] != "0" || is_null($terms['netTerm']) ){ ?>
					<div class="btn-group btn-group-xs">
						
						<a href="#add_payment_terms"  class="btn btn-default" onclick="set_edit_term('<?php echo $terms['pid'];  ?>','<?php echo $terms['dfid']; ?>','<?php echo $name; ?>','<?php echo $term; ?>','update');" title="Edit" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-edit"> </i> </a>  
              
						<a href="#del_term" onclick="del_term_id('<?php echo $terms['pid']; ?>','<?php echo $terms['dfid']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
				</div>
					<?php } ?>
		   </td>
		</tr>
				
				<?php } } } 
				else{
          echo'<tr>
                <td colspan="3"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>';
        }
				?>
				
			</tbody>
        </table>
        </br>
    
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>


     <!------------ Add popup for gateway   ------->

 <div id="add_payment_terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title pop_title"> Add New Payment Term</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
               <form method="POST" id="termform" class="form nmifrom form-horizontal" action="<?php echo base_url('Xero_controllers/NetTerms/create_terms');?>">
		     	<input type="hidden" id="termID" name="termID" value=""  />
				<input type="hidden" id="dfID" name="dfID" value=""  />
				<input type="hidden" id="term_status" name="term_status" class="form-control"  value="" />
		       <div class="form-group ">
		           
		       <div class="form-group ">
                                              
				 <label class="col-md-4 control-label" for="card_list">Name</label>
						<div class="col-md-6">
						   <input type="text" id="termname" name="termname"  class="form-control " />
								
						</div>
						
					</div>
                
                  <div class="form-group ">
                                              
				 <label class="col-md-4 control-label" for="card_list">Net Term</label>
						<div class="col-md-5">
						   
						       <input style="width:35% !important" type="" value="" name="netterm" id="netterm" class="form-control">
						       	  <div id="error"></div>
						</div>
                    
					</div>
                    
                  
	         <div class="form-group">
					<div class="col-md-12 text-right">
					    <div class="col-md-12">
					<button type="submit" class="submit btn btn-sm btn-success">Add</button>
					
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					</div>
                    </div>
             </div> 
			
	   </form>			
                
            </div>
			
            <!-- END Modal Body -->
        </div>
     </div>
	 
  </div>

</div>



<!-------------------------- Modal for Edit Gateway ------------------------------>


     <!------- Modal for Delete Gateway ------>

  <div id="del_term" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Payment Term</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_term_form" method="post" action='<?php echo base_url('Xero_controllers/NetTerms/delete_term');?>' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Payment Term? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                              <input type="hidden" id="dtermID" name="dtermID" class="form-control"  value="" />
                              <input type="hidden" id="del_dfID" name="del_dfID" class="form-control"  value="" />
                              
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-------------------- Modal for Default terms set ---------------------------->


<div id="set_def_terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Set Default Payment Term</h2>  
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Xero_controllers/NetTerms/set_gateway_default' class="form-horizontal card_form" autocomplete="off" >
                    <div class="form-group ">
                                              
                        <label class="col-md-4 control-label">Payment Term</label>
                        <div class="col-md-6">
                            <select id="termid" name="termid" class="form-control">
                                <option value="" >Select Payment Term</option>
                                <?php if(isset($netterms) && !empty($netterms) ){
                                        foreach($netterms as $term_data){


                                        ?>
                                            <option value="<?php echo $term_data['id'] ?>"  <?php if($term_data['set_as_default'] == '1'){ echo 'selected'; } ?>   ><?php echo $term_data['pt_name'] ?>
                                                
                                            </option>
                                        <?php } } ?>
                            </select>
                            
                        </div>
                    </div>      

                    <div class="pull-right">
                         <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">  
                         
                        <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-info" value="Save"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<style>

.table.dataTable {
  width:100% !important;
 }
 .block-title h1{
font-size: 16px;
}


@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#merch_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [2] }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();





function del_term_id(id,dfid){
	
	     $('#dtermID').val(id);
	     $('#del_dfID').val(dfid);
}


function set_edit_term(termid,dfID,tname,tvalue,term_status){
	 
	 $('#termID').val(termid);
	 $('#dfID').val(dfID);
	 $('#term_status').val(term_status);
	 
	 $('.pop_title').html("Edit Payment Term");
	 $('.submit').html("Save");

	var name = tname;
	var netTerm = tvalue;
	
     $('#termname').val(name);
     $('#netterm').val(netTerm);


}

 	  
  $('#termform').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			
		 'termname':{
				 required: true,
		 },	
		 
		  'netterm': {
                 required: true,
                },
		
	 },
  });
  

    
    $('#netterm').keydown(function(e){
          // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
      $("#netterm").on('input',function (e) {  
             var termval = $(this).val();
             if(termval > 180){
                 $("#netterm").val(termval.substring(0, termval.length - 1));
                $("#error").html('<label id="termname-error" class="error" for="termname">Please enter value between 0-180</label>');
                 return false;
             }else{
                 $("#error").html('');
             }
        
    });
</script>

<script>
function reset_form(term_status){
    $('#termform').trigger('reset');
    $('#termID').val("");
    $('#dfID').val("");
     $('#term_status').val('');
    
    $('#term_status').val(term_status);
    
    $('.pop_title').html("Add New Payment Term");
    $('.submit').html("Save");
    
}

window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }
</script>
