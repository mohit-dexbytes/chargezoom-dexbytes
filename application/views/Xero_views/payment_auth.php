<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<style type="text/css">
#expiry_year-error{
	clear: both;
    margin: 0;
    display: block !important;
    margin-left: 15px;
}
#amount-error, #card_number-error{
	display: table-footer-group;
}
</style>
<script src="<?php echo base_url(JS); ?>/pages/new-fontawesome-5-15-3.js"></script>
<div id="page-content">
		       <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo $message;
					?>
	 
    	<legend class="leg"> Payment Info</legend>
        <form id="form-validation" action="<?php echo base_url(); ?>Xero_controllers/Payments/create_customer_auth" method="post">
            <div class="block">
				<div class="row">
					<div class="col-md-6">
					    <div class="form-group">
						<label class="control-label" for="customerID">Customer Name</label>
                        <div class="row">
							<?php if(!empty($plantype) ){?>
								<div class="col-md-12">
                                <select id="customerID" name="customerID" class="form-control">
                                    <option value>Choose Customer</option>
                                    <?php  foreach($customers as $customer){       ?>
                                    <option value="<?php echo $customer['Customer_ListID']; ?>"><?php echo  $customer['fullName'] ; ?></option>
                                    <?php } ?>
                                </select>
								</div>
								
							<?php }else{?>
							<div class="col-md-9">
                                <select id="customerID" name="customerID" class="form-control">
                                    <option value>Choose Customer</option>
                                    <?php  foreach($customers as $customer){       ?>
                                    <option value="<?php echo $customer['Customer_ListID']; ?>"><?php echo  $customer['fullName'] ; ?></option>
                                    <?php } ?>
                                </select>
							</div>
							<div class="col-md-3">
								<a class="btn btn-md d-block btn-success w-100 mt-sm-1 add-new-sale-btn" href="<?php echo base_url(); ?>Xero_controllers/Customer/create_customer">Add New</a>
							</div>
							<?php

							}?>
							</div>
					  </div>
						<label>Confirm Payment Details</label>
						<div class="col-md-12 bg-dark py-1">
							
								<option  value="new1" ><strong>New Card</strong></option>
							</select>
						</div> -->
						<div id="set_credit">
							<div class="form-group clearfix mt-md-1" id="frdname">
								<label class="control-label" for="friendlyname"> Card Holder Name<span class="text-danger">*</span></label>
								<div class="input-group col-md-12">
									<input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" maxlength="30" autocomplete="off">
								</div>
                    		</div>
							<div class="form-group">
								<label class="control-label" for="card_number" style="position: relative;width: 100%">Credit Card Number<span class="text-danger">*</span>
								<div class="cards">
								<img src="<?php echo base_url();?>resources/img/amex.png">
										<img src="<?php echo base_url();?>resources/img/visa.png" />
										<img src="<?php echo base_url();?>resources/img/master.png" />
										<img src="<?php echo base_url();?>resources/img/discover.png" />
									</div>
								</label>
								
								<div class="input-group mtop-120">
									<input type="text" id="card_number" data-stripe="card_number" name="card_number" class="form-control" placeholder="" data-args="">
									<span class="input-group-addon border-none"><i class="fa fa-credit-card"></i></span>
								</div>
							</div>
                    		<div class="form-group clearfix">
								<div class="row">

									<div class="col-md-3 col-sm-3 text-xs-left">
									<br>
                        			<label style="vertical-align: sub" class="mt-md-2 mt-sm-1-7">Exp Date  <span class="text-danger">*</span></label>
									</div>
									<div class="col-md-3 col-sm-3">
									<label class="d-block text-center" for="expry">Month </label>
									<select id="expiry" name="expiry" data-stripe="expiry" class="form-control">
										<option value="01">JAN</option>
										<option value="02">FEB</option>
										<option value="03">MAR</option>
										<option value="04">APR</option>
										<option value="05">MAY</option>
										<option value="06">JUN</option>
										<option value="07">JUL</option>
										<option value="08">AUG</option>
										<option value="09">SEP</option>
										<option value="10">OCT</option>
										<option value="11">NOV</option>
										<option value="12">DEC</option>
									</select>
								</div>
									<div class="col-md-3 col-sm-3">
									<label class="d-block text-center" for="expry_year">Year</label>
									<select id="expiry_year" name="expiry_year" data-stripe="expiry_year" class="form-control">
										<?php 
											$cruy = date('y');
											$dyear = $cruy+25;
											for($i =$cruy; $i< $dyear ;$i++ ){  ?>
											<option value="<?php  echo "20".$i;  ?>"><?php echo "20".$i;  ?> </option>
										<?php } ?>
									</select>
									</div>

									<div class="col-md-1 col-sm-1 text-right text-xs-left">
										<label class="mt-md-3-8 mt-sm-3-8 mt-xs-1-5" for="cvv">CVV<span class="text-danger">*</span></label>
									</div>
									<div class="col-md-2 col-sm-2">
										<input type="text" id="cvv" name="cvv" data-stripe="cvv" class="form-control mt-md-2-5 mt-sm-2-5 mt-xs-1 " placeholder="" data-args=""> 
									</div>
									
								</div>
                    		</div>
                    		
                        </div>
						</div>
						<div class="form-group" id="tc_check">
							<input type="checkbox" name="tc" id="tc"  />
							<label for="tc"> Do not save Credit Card</label>
                    	</div>
					</div>
					<div class="col-md-6">
							
							<div class="row">
								<div class="col-md-6">
								<div class="form-group">
									<label class="control-label" for="amount">Amount</label>
									<div class="input-group">
										<input type="text" id="amount" name="amount" class="form-control" placeholder="" data-args="" onkeypress="return onlyNumberKeyAllow(event)">
										<span class="input-group-addon"><i class="gi gi-usd"></i></span>
									</div>	
								</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="country_code">Currency</label>
										<select id="country_code" name="country_code" class="form-control">
											<option value="USD">USD</option>
										
										</select>
									</div>
								</div>
							</div>
							<div class="row hidden">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="val_skill">Surcharge Type</label>
											<select id="surcharge_type" name="surcharge_type"  class="form-control">
											<option value="1">No Surcharge</option>
											<option value="2">Fixed</option>
											<option value="3">Percentage</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="val_digits">Surcharge</label>
										<div class="input-group">
											<input type="text" id="surchargeVal" name="surchargeVal" class="form-control" readonly  placeholder="" data-args="">
											<span class="input-group-addon"><i class="gi gi-usd"></i></span>
										</div>
									</div>
								</div>
							</div>
							<?php 
                                if(!isset($defaultGateway) || !$defaultGateway){
                            ?>
							<div class="form-group">
								<label class="control-label" for="card_list">Gateway</label>
								<div class="input-group col-md-12">
									<select id="gateway_list" name="gateway_list" onchange="get_change_gatway();"   class="form-control">
	                                    <option value="" >Select Gateway</option>
	                                        <?php foreach($gateways as $gateway){ ?>
	                                        <option value="<?php echo $gateway['gatewayID'];  ?>" <?php if($gateway['set_as_default']=='1')echo "selected ='selected' ";  ?> ><?php echo $gateway['gatewayFriendlyName']; ?></option>
	                                        <?php } ?>
	                                </select>
	                            </div>
							</div>
							<?php 
                                } else { ?>
                                <input type="hidden" name="gateway_list" value="<?php echo $defaultGateway['gatewayID'];  ?>">
                            <?php }  ?>	
						
						<input type="hidden" id="vaultcardID"  name="vaultcardID" class="form-control"  readonly ='readonly'>
						
						<input type="hidden" id="totalamount" name="totalamount" class="form-control" placeholder="" data-args="" readonly ='readonly' >
						<div class="form-group">
                            <label class="control-label" for="po_number">PO Number</label>
                            <div class="input-group">
                                <input type="text" id="po_number" name="po_number" class="form-control" placeholder="">
                                <span class="input-group-addon"><i class="fas fa-file-invoice-dollar"></i></span>
                            </div>
                        </div>
						<div class="form-group">
							<label class="control-label" for="val_email">Email Address</label>
							<div class="input-group">
								<input type="text" id="email" name="email" class="form-control" placeholder="" data-args="">
								<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
							</div>
						</div>		
						<div class="form-group">
							<label class="control-label" for="phone">Phone Number</label>
							<div class="input-group">
								<input type="text" id="phone" name="phone" class="form-control" placeholder="" data-args="" onkeypress="return onlyNumberKeyAllow(event)" onchange="formatPhoneNumber($(this).val())">
								<span class="input-group-addon"><i class="gi gi-phone_alt"></i></span>
							</div>
						</div>
						<div class="form-group">
	                        <label class="control-label" for="val_username">ZIP Code</label>
	                        <div class="">
	                            <div class="input-group">
	                                <input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="" data-args=""  maxlength="10" onblur="zipCodeAllowPermission($(this).val(),2)">
	                                <span class="input-group-addon"><i class="gi gi-direction"></i></span>
	                            </div>
	                        </div>
	                    </div>	
                	</div>
				</div>
			</div>
                
           
                                
            <legend class="leg"> Billing Address</legend>
            <div class="block">		
                
                  
                    <div class="row">
						<div class="form-group col-md-6">
							<label class="control-label" for="companyName">Company Name </label>
							<div class="">
								<div class="input-group">
									<input type="text" id="companyName" name="companyName" class="form-control" placeholder="" data-args="">
									<span class="input-group-addon"><i class="fa fa-university"></i></span>
								</div>
							</div>
                    	</div>	
						<div class="form-group col-md-3">
							<label class="control-label" for="firstName">First Name</label>
							<div class="">
								<div class="input-group">
									<input type="text" id="firstName" name="firstName" class="form-control" placeholder="" data-args="">
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
								</div>
							</div>
						</div>
						<div class="form-group col-md-3">
							<label class=" control-label" for="lastName">Last Name </label>
							<div class="">
								<div class="input-group">
									<input type="text" id="lastName" name="lastName" class="form-control" placeholder="" data-args="">
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
								</div>
							</div>
						</div>
					</div>
										   
                                                
                    <div class="form-group">
                        <label class="control-label" for="val_username">Address Line 1</label>
                        <div class="">
                            <div class="input-group">
                                <input type="text" id="address1" name="address1" class="form-control"  placeholder="" data-args="">
                                <span class="input-group-addon"><i class="gi gi-home"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                     
                        <div class="">
                            <div class="input-group">
                                <input type="text" id="address2" name="address2" class="form-control"  placeholder="" data-args="">
                                <span class="input-group-addon"><i class="gi gi-home"></i></span>
                            </div>
                        </div>
                    </div>
				
					<div class="row">
						<div class="form-group col-md-4">
                        <label class="control-label" for="val_username">City</label>
                        <div class="">
                            <div class="input-group">
                                <input type="text" id="city" name="city" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="">
                                <span class="input-group-addon"><i class="gi gi-road"></i></span>
                            </div>
                        </div>
                    </div>	
                    <div class="form-group col-md-4">
                        <label class="control-label" for="val_username">State/Province</label>
                        <div class="">
                            <div class="input-group">
                                <input type="text" id="state" name="state" class="form-control input-typeahead" autocomplete="off" placeholder="" data-args="">
                                <span class="input-group-addon"><i class="gi gi-road"></i></span>
                            </div>
                        </div>
                    </div>	  
					<div class="form-group col-md-4">
                        <label class="control-label" for="example-typeahead">Country</label>
                        <div class="">
                            <div class="input-group">
                                <input type="text" id="country" name="country" class="form-control" autocomplete="off" value="" placeholder="" data-args="">
                                <span class="input-group-addon"><i class="gi gi-home"></i></span>
                            </div>
                        </div>
                    </div>
					
					</div>
                    
					<div class="form-group ">
                        <label class="control-label" for="reference">Reference Memo</label>
                        <div class="">
                            <div class="input-group">
                                <input type="text" id="val_username" name="val_username" class="form-control" placeholder="" data-args="">
                                <span class="input-group-addon"><i class="gi gi-notes"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" control-label" for="reference"></label>
                        <div class="">
                        <label class="col-md-4 control-label" for="reference"></label>
                         <div class="col-md-6">
					            <input type="checkbox" name="tr_checked" checked="checked" />
						     </div>
                        </div>
                    </div>
                
            </div>
            <input type="hidden" name="invoice_id" id="invoice_ids" value="" />		 
			<input type="hidden" id='stripeApiKey' name='stripeApiKey' value="<?php if(isset($stp_user)){echo $stp_user; } ?>" />
            <div class="form-group form-actions">
            	<div class="text-right">
                	<button type="button" onclick="this.form.reset()" class="btn btn-danger px-md-4">Reset</button>
                    <button type="submit" id="submit_btn" class="btn btn-success px-md-4">Authorize</button>
                </div>
            </div>

        </form>  

</div> 

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>
        window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }
     
	var  gtype='';
	  function stripeResponseHandler(status, response) {

              
                if (response.error) {
                    // Re-enable the submit button
                    $('#submit_btn').removeAttr("disabled");
                    // Show the errors on the form
                    $('#payment_error').text(response.error.message);
                } else {
                    var form = $("#form-validation");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

                }
            }
        $(function(){
                
            nmiValidation.init();
			
		  $('#gateway_list').change(function(){
		    var gateway_value =$(this).val();
	
			if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
							gtype  = 	data['gatewayType'];
							  if(gtype=='1'){			
										var url   = "<?php echo base_url()?>Xero_controllers/Payments/create_customer_auth";	
										}else if(gtype=='2'){
									var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/create_customer_auth";
									}else if(gtype=='3'){
										var url   = "<?php echo base_url()?>Xero_controllers/PaytracePayment/create_customer_auth";
									
									}else if(gtype=='4'){
									var url   = "<?php echo base_url()?>Xero_controllers/PaypalPayment/create_customer_auth";
									}	
									else if(gtype=='5'){
									var url   = "<?php echo base_url()?>Xero_controllers/StripePayment/create_customer_auth";
									 var form = $("#form-validation");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
									
									
									}				
				
				             $("#form-validation").attr("action",url);
					}   
				   
			   });
			}	
			
         
			
	  });
			
			
			
			
			
	  $('#amount').blur(function(){
		var amount = $(this).val();
		if(amount !=''){
			$('#amount').val(parseFloat(amount).toFixed(2));
		}
	});
            var amount=0;
    	    $('#surcharge_type').change(function(){
    		
    		    if($(this).val()=='1'){
    			    $('#surchargeVal').attr('readonly','readonly');
        			$('#surchargeVal').val('0');
        			var amount = $('#amount').val();
        			$('#totalamount').val(amount.toFixed(2));
    		    }	
    		    
    		    if($(this).val()=='2'){
    			    $('#surchargeVal').removeAttr('readonly');
    				var amount = $('#amount').val();
    				var surcharge = $('#surchargeVal').val();
    				amount1=	parseFloat(amount)+ parseFloat(surcharge);
    			    $('#totalamount').val(amount.toFixed(2));
                }	
    		    
    		    if($(this).val()=='3'){
    			    $('#surchargeVal').removeAttr('readonly');
    		        var amount = $('#amount').val();
    				var surcharge = $('#surchargeVal').val();
    				surcharge = (amount*surcharge)/100;
    				amount1 = parseFloat(amount)+ parseFloat(surcharge);
    			    $('#totalamount').val(amount.toFixed(2));
                }	
    	    });
    	
    	    $('#surchargeVal').change(function(){
           	    if( $('#surcharge_type').val()=='2'){
    				var amount = $('#amount').val();
    				var surcharge = $('#surchargeVal').val();
    				amount1 = parseFloat(amount)+ parseFloat(surcharge);
    		        $('#totalamount').val(amount1.toFixed(2));
            	}	
    		    if( $('#surcharge_type').val()=='3'){
    		        var amount = $('#amount').val();
    				var surcharge = $('#surchargeVal').val();
    				surcharge = (amount*surcharge)/100;
    				amount1 = parseFloat(amount)+ parseFloat(surcharge);
    			    $('#totalamount').val(amount1.toFixed(2));
        		}	
    	    });
    	
            $('#amount').blur(function(){
		var amount = $(this).val();
		$('#amount').val(parseFloat(amount).toFixed(2));
	});
    	    $('#amount').change(function(){
                if($(this).val()=='1'){
        			$('#surchargeVal').removeAttr('readonly');
        			$('#surchargeVal').val('0');
        			var amount = $('#amount').val();
        			$('#totalamount').val(amount.toFixed(2));
        		}	
        	  
        	   	if( $('#surcharge_type').val()=='2'){
        			
            		var amount = $('#amount').val();
            		var surcharge = $('#surchargeVal').val();
            		amount1 = parseFloat(amount)+ parseFloat(surcharge);
            	    $('#totalamount').val(amount1.toFixed(2));
        		}	
        		if( $('#surcharge_type').val()=='3');{
        		    var amount = $('#amount').val();
        			var surcharge = $('#surchargeVal').val();
        			surcharge = (amount*surcharge)/100;
        			amount1 = parseFloat(amount)+ parseFloat(surcharge);
        		    $('#totalamount').val(amount1.toFixed(2));
                }	
        	});
    	
    	
        $('#cvv').blur(function(event) {  
		  
		if(gtype=='5'){
        var $form = $("#payment_form");
        // Disable the submit button to prevent repeated clicks
        $form.find('#submit_btn').prop('disabled', true);
		
		var pub_key = $('#stripeApiKey').val();
		 Stripe.setPublishableKey(pub_key);
         Stripe.createToken({
                        number: $('#card_number').val(),
                        cvc: $('#cvv').val(),
                        exp_month: $('#expiry').val(),
                        exp_year: $('#expiry_year').val()
                    }, stripeResponseHandler);

        // Prevent the form from submitting with the default action
        return false;
		}
      });
		
	
	$('#customerID').change(function(){
		
		             
		var cid  = $(this).val();
	
		if(cid!=""){
			
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Xero_controllers/Payments/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
				
					     data=$.parseJSON(response);
					     if(data['status']=='success'){
							 
							     $('#companyName').val(data['companyName']);
							    $('#firstName').val(data['FirstName']);
								$('#lastName').val(data['LastName']);
								$('#address1').val(data['address1']);
                         		$('#address2').val(data['address2']);
								$('#city').val(data['City']);
								$('#state').val(data['State']);
                         		$('#country').val(data['Country']);
								$('#zipcode').val(data['zipCode']);
								formatPhoneNumber(data['phoneNumber']);
								$('#email').val(data['userEmail']);
							 
					   }	   
					
				}
				
				
			});
			
		}	
    });		
	
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');


	});
	
	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
                   		
					 cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                    friendlyname:{
						required: true,
						minlength: 3,
						maxlength: 30
					},
                    customerID: {
                         required: true,
                       
                    },
						 amount: {
                        required: true,
                        number: true,
                        max: 99999.99
                    },
                    totalamount: {
                        required: true,
                        number: true
                    },
                     surchargeVal: {
                        required: true,
                        number: true
                    },
					check_status:{
						  required: true,
					},
				
                    lastName:{
                         required: true,
                       
                    },
                    email: {
                        email:true,
                    },
                    phone: {
                        maxlength: 12,
                    }, 
                  	po_number:{
                  		maxlength: 25
                  	},
                  	gateway_list:{
                  		required: true,
                  	},
                    bzipcode: {
                        required: function () {
                            let cardID = $('#card_list').val();
                            if(cardID == 'new1'){
                                return true;
                            }else{
                                return false;
                            }
                            
                        },
                    },
                  
                },
                messages: {
					  customerID: {
                        required: 'Please select a customer',
                      
                    },
                   
                   
					amount:{
						  required: 'Please enter the amount',
					},
					check_status:{
						 required: 'Please select the option',
					},
                  	phone:{
						maxlength: 'Phone number is not valid, can not be more than 10 digits.',
					},
                      gateway_list: {
                        required: 'Please select the gateway',
                    }
                  
                   
                },
                submitHandler: function (form) {
                    $("#submit_btn").attr("disabled", true);
                    return true;
                }
            });
			
				$.validator.addMethod('CCExp', function(value, element, params) {  
	  var minMonth = new Date().getMonth() + 1;
	  var minYear = new Date().getFullYear();
	  var month = parseInt($(params.month).val(), 10);
	  var year = parseInt($(params.year).val(), 10);
	  
	  

	  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');
			
			
        }
    };
}();
	
	
	
	
	</script>
</div>