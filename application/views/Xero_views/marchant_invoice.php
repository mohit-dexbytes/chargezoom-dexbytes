<?php

$status = '';
$today = date('Y-m-d');
$due_date = date('Y-m-d');
if ($due_date < $today && $invoice_data['IsPaid'] == 'false') {
    $status = 'Pastdue';
}
$type = '';
$date1 = strtotime($due_date);
$date2 = strtotime($today);

$type_text = '';

if ($date1 >= $date2) {
    $diff = $date2 - $date1;
    $diff = floor($diff / (60 * 60 * 24));
    $type = 3;
    $type_text = "Invoice due soon/upcoming";
} else {

    $diff = $date2 - $date1;
    $diff = floor($diff / (60 * 60 * 24));
    if ($diff > 7) {
        $type = 2;
        $type_text = "Invoice past due/overdue";
    } else {
        $type = 1;
        $type_text = "Invoice Due";
    }
}


?>
<?php
	$this->load->view('alert');
?>
<style>
.block-options{
    margin-top: 10px;
}
</style>
<div id="page-content">






    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Products Block -->
    <legend class="leg">Invoice Details</legend>
    <div class="block">
        <!-- Products Title -->
       
            <div class="block-options pull-right">
                <a href="#" class="btn btn-alt btn-sm btn-danger" data-toggle="tooltip" title="">Download PDF</a>

                <input type="hidden" name="taxes" id="taxes" value="" />
                <input type="hidden" name="tax_rate" id="tax_rate" value="" />
                
            </div>




        <form id="form-validation" action="<?php echo base_url(); ?>company/SettingSubscription/edit_custom_invoice" method="post" enctype="multipart/form-data" class="form-horizontal ">


            <h4>Invoice Number: <strong>CRZ-10023</strong></h4>
            <h5>Customer Name: <strong>Dummy Data User</strong></h5>
            <h5>Invoice Date: <div class="input-group input-date col-md-3">

                    <input type="text" id="invDate" name="invDate" class="form-control input-datepicker" value="<?php echo date('m/d/Y'); ?>" data-date-format="mm/dd/yyyy" placeholder="" data-args="Invoice Date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </h5>
            <h5>Invoice Due Date: <div class="input-group input-date cust_view col-md-3">

                    <input type="text" id="dueDate" name="dueDate" class="form-control input-datepicker" value="<?php echo date('m/d/Y'); ?>" data-date-format="mm/dd/yyyy" placeholder="" data-args="Invoice Due Date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </h5>

            <h5>Invoice Status: <strong> Pending</strong></h5>

            <h5>Payment Method: <strong>
                    <?php
                    	echo 'Credit Card';
                    ?>
                </strong></h5>

            <h5>Preview Invoice Page: <strong class="cust_view">Upcomming</strong></h5>


            <div class="col-md-12">

                <div class="pull-right">
                    <br><br>
                </div>

            </div>


            <input type="hidden" id="invNo" name="invNo" value="" />
        </form>

        <div class="pay_info">
        	<div class="table-responsive">
                <table class="table table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th>Plan Name</th>
                            <th class="text-left"> </th>
                            <th class="text-right"></th>
                            <th class="text-right"></th>

                            <th class="text-right">Amount</th>

                        </tr>
                    </thead>
                    <tbody id="item_fields">
                       	<tr class="">
                            <td>Demo For Dummy User</td>
                            <td colspan="3" class="text-right text-uppercase"><strong>Amount</strong></td>
                            <td class="text-right">$89.00</td>

                        </tr>
                        <tr class="active">
                            <td></td>
                            <td colspan="3" class="text-right text-uppercase"><strong>SUBTOTAL</strong></td>
                            <td class="text-right">$89.00</td>

                        </tr>

                        <tr>
                            <td class="text-left"></td>
                            <td colspan="3" class="text-right text-uppercase"><strong>TAX (0%) </strong></td>
                            <td class="text-right">$10.00</td>
                        </tr>
                        <tr class="info">
                            <td colspan="4" class="text-right text-uppercase"><strong>TOTAL</strong></td>
                            <td class="text-right">$99.00</td>
                        </tr>

                        <tr class="success">
                            <td colspan="4" class="text-right text-uppercase"><strong>PAID</strong></td>
                            <td class="text-right">$50.00</td>
                        </tr>

                        <tr class="danger">
                            <td colspan="4" class="text-right text-uppercase"><strong>BALANCE</strong></td>
                            <td class="text-right"><strong>$49.00</strong></td>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>

        
        <!-- END Addresses -->

        <!-- Log Block -->
        <div class="block full">
            <!-- Private Notes Title -->
            <div class="block-title">
                <h2><strong>Private</strong> Notes</h2>
            </div>
            <!-- END Private Notes Title -->

            <!-- Private Notes Content -->
            <div class="alert alert-info">
                <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to admin.
            </div>
            <form method="post" id="pri_form" onsubmit="return false;">
                <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="" data-args="Your note.."></textarea>
                
                <br>
                <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
            </form>
            <hr>

           
            <br>
            <!-- END Private Notes Content -->
        </div>
        <!-- END Log Block -->
    </div>


    <script src="<?php echo base_url(JS); ?>/pages/customer_details_company.js"></script>
    <script>
        $('.testbtn').click(function() {
            var form_data = $('#form-validation').serialize();
            var index = '';
            if ($(this).val() == 'Save') {
                index = "self";
            } else {
                index = "other";
            }
            $('#index').remove();


            $('<input>', {
                'type': 'hidden',
                'id': 'index',
                'name': 'index',

                'value': index,
            }).appendTo($('#form-validation'));

            $('#form-validation').submit();

        });
    </script>
    