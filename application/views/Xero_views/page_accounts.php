<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    
	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- All Orders Block -->
    <div class="block full">
	
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Accounts</strong> </h2>
          
        </div>
        
        
        <table id="acc_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                  
                    <th class="text-left">Name</th>
                    <th class="text-right ">Code</th>
                   <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($accounts) && $accounts)
				{
					foreach($accounts as $account)
					{
				?>
				<tr>
					
					<td class="text-left"><?php echo $account['accountName']; ?></td>
					<td class="text-right"><?php echo $account['accountCode']; ?></td>
                    <td class="text-right"><?php echo $account['accountDescription']; ?></td>
					
					
				
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="3"> No Records Found </td></tr>'; }  
				?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>

<script>$(function(){ Pagination_view.init(); 
                
});

var Pagination_view = function() {

    return {
        init: function() {
            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#acc_page').dataTable({
                columnDefs: [ { orderable: false, targets: [ 2 ] } ],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


</script>


<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 
<!------    Add popup form    ------->

<div id="add_tax" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Tax</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="tax_form" class="form form-horizontal" action="<?php echo base_url(); ?>Xero_controllers/Tax/create_taxes">
			<input type="hidden" id="taxID" name="taxID" value=""  />
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Friendly Name</label>
							<div class="col-md-8">
								<input type="text" id="friendlyName"  name="friendlyName" class="form-control"  value="" placeholder="" data-args="Friendly Name"><?php echo form_error('friendlyName'); ?></div>
						</div>
						
						
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Tax Rate (%)</label>
							<div class="col-md-8">
								<input type="text" id="taxRate"  name="taxRate" class="form-control"  value="" placeholder="" data-args="Tax Rate"><?php echo form_error('taxRate'); ?></div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Component Name</label>
							<div class="col-md-8">
								<input type="text" id="taxComponent"  name="taxComponent" class="form-control"  value="" placeholder="" data-args="Tax Component"><?php echo form_error('taxComponent'); ?></div>
						</div>
						
                       </div>      
		      
			  <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
					
                    </div>
                    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<!-- END Page Content -->

<!--------------------del admin plan------------------------>

<div id="del_tax" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Tax</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_tax" method="post" action='<?php echo base_url(); ?>Xero_controllers/Tax/delete_tax' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this tax?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="tax_id" name="tax_id" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>






