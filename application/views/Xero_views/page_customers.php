<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div class="msg_data">
<?php echo $this->session->flashdata('message');   ?>
</div>
<div id="page-content">
    <legend class="leg">Customers</legend>
    <!-- All Orders Block -->
    <div class="block-main full"  style="position: relative">
        
        <!-- All Orders Title -->
        <a class="btn btn-sm addNewBtnCustom btn-success" title="Create New"  href="<?php echo base_url(); ?>Xero_controllers/Customer/create_customer">Add New</a>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Customer Name</th>
                    <th class="text-left ">Full Name</th>
                    <th class="text-left hidden-xs">Email Address</th>
                    <th class="hidden-xs hidden-sm text-right">Phone Number</th>
                   <th class="text-right hidden-xs">Balance</th>
                    <th class="text-center" style="width:85px !important;">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($customers) && $customers)
				{
					foreach($customers as $customer)
					{
				?>
				<tr>
				    
				     <td class="text-left cust_view"><a href="<?php echo base_url('Xero_controllers/Customer/view_customer/'.$customer['Customer_ListID']); ?>"><?php echo $customer['firstName']; ?></a> </td>
			
					<td class="text-left"> <?php echo $customer['fullName']; ?></td>
					<td class="text-left hidden-xs"> <?php echo $customer['userEmail']; ?></td>
					<td class="text-right hidden-xs"> <?php echo $customer['phoneNumber']; ?></td>
					<td class="text-right hidden-xs">$<?php echo number_format($customer['balance'],2); ?></td>
				
					<td class="text-center">
						<div class="btn-group btn-group-xs">
						
					
						
					
						<a class="edit_customer" href="<?php echo base_url('Xero_controllers/Customer/create_customer/'.$customer['Customer_ListID']); ?>" data-toggle="tooltip" title="Edit User" class="btn btn-default"><i class="fa fa-pencil"> </i> </a>	
                        <a class="deactive_customer" href="#xero_del_cust" data-backdrop="static" onclick="delete_xero_customer('<?php echo $customer['Customer_ListID'];?>')" data-keyboard="false" data-toggle="modal"  title="Deactivate User" class="btn btn-danger"> <i class="fa fa-close"> </i> </a>
							
					</div>
					</td>
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="6"> No Records Found </td></tr>'; }  
				?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
<div id="xero_del_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Deactivate Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>Xero_controllers/Customer/delete_customer' class="form-horizontal" >
                     
                 
                    <p>Do you really want to deactivate this Customer?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="xerocustID" name="xerocustID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="qb_btn_cancel" name="qb_btn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ 
Pagination_view111.init(); });






var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>


	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>

 

</div>
<script type="text/javascript">
    function delete_xero_customer(cust_id){
     $('#xerocustID').val(cust_id);
    }
    window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }
</script>
<!-- END Page Content -->