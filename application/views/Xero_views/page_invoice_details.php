  
<?php
	$this->load->view('alert');
?>
	
<div id="page-content">
  
    <!-- Products Block -->
    <div class="block">
        <!-- Products Title -->
        <div class="block-title">
	<div class="block-options pull-right">
				<a href="<?php echo base_url(); ?>Xero_controllers/Xero_invoice/invoice_details_print/<?php echo  $invoice_data['invoiceID']; ?>" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Print Invoice"><i class="fa fa-print"></i></a>
              
           <a href="javascript:void(0);" title="Send Email"  class="btn btn-alt btn-sm btn-default"  data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-paper-plane-o"></i></a>
			</div>  
            <h2><strong>Invoice</strong> Details</h2>
			
        </div>
        <!-- END Products Title -->

        <!-- Products Content -->
         <div class="table-responsive">
           
             <div class="block">
                 
                <h4>Invoice Number: <strong><?php  echo  $invoice_data['refNumber']; ?></strong></h4>
                <h5>Customer Name:  <strong><?php echo ucwords( $invoice_data['CustomerFullName']); ?></strong></h5>
                <h5>Invoice Date:  <strong><?php  echo date('m/d/Y', strtotime($invoice_data['TimeCreated'])); ?></strong></h5>
                <h5>Invoice Due Date:  <strong><?php  echo date('m/d/Y', strtotime($invoice_data['DueDate'])); ?></strong></h5>
                <h5>Invoice Status:  <strong> <?php echo $invoice_data['UserStatus']; ?></h5>
                
                 <h5>Payment Method: <strong>
                     <?php
                        if(!empty($transaction['transactionType']) && $transaction['transactionType'] =='Offline Payment'){
                            echo 'Check'; 
                        }else if((isset($transaction['paymentType']) && $transaction['paymentType'] == 2  )){ 
                            echo 'Electronic Check';
                        }else if(empty($transaction['transactionType']  )){ 
                            echo '--';
                        }else{ 
                            echo'Credit Card';
                        }   
                    ?>
                 </strong></h5>
          	
                 
             </div>
             
             
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th>Product / Service</th>
                        <th class="text-right">Description</th>
                           <th class="text-right">Unit Rate</th>
                        <th class="text-right">Qty</th>
						<th class="text-right">Amount</th>
                    </tr>
                </thead>
                <tbody>
				 <?php foreach($invoice_data['l_items'] as $item){   ?>   
                    <tr>
                        <td class="text-left"><?php echo $item['Name'];  ?></td>
                        <td><?php echo  $item['itemDescription']; ?></td>
                        <td class="text-right">$<?php echo ($item['itemPrice'])?number_format($item['itemPrice'],2):'0.00';  ?></td>
						 <td class="text-right"><?php echo $item['itemQty'];  ?></td>
				    	<td class="text-right">$<?php echo ($item['itemPrice'])?number_format($item['totalAmount'],2):'0.00';  ?></td>
						
                    </tr>
					
                 <?php } ?> 
					<tr class="active">
                        <td colspan="3" class="text-right text-uppercase"><strong>Total</strong></td>
                        <td class="text-right"><strong></strong>$<?php if($invoice_data['IsPaid']=='1'){ $balance=$invoice_data['Total_payment'];}else{ $balance=$invoice_data['BalanceRemaining']; }  echo  number_format(($balance-$invoice_data['totalTax']), 2) ; ?></td>
						<td class="text-right"><strong><?php echo '$'.number_format($invoice_data['totalTax'])?'$'.number_format($invoice_data['totalTax'],2):'0.00';  ?></strong></td>
                    </tr>
					<tr>
                        <td colspan="4" class="text-right text-uppercase"><strong>Other Tax</strong></td>
                        <td class="text-right">$0.00</td>
                    </tr>
					<tr class="info">
                        <td colspan="4" class="text-right text-uppercase"><strong>Total Payable</strong></td>
                        <td class="text-right">$<?php echo number_format($invoice_data['BalanceRemaining'],2);  ?></td>
                    </tr>
                    <tr class="success">
                        <td colspan="4" class="text-right text-uppercase"><strong>Paid</strong></td>
                        <td class="text-right">$<?php echo number_format($invoice_data['AppliedAmount'],2);  ?></td>
                    </tr>
					<tr class="danger">
                        <td colspan="4" class="text-right text-uppercase"><strong>Balance</strong></td>
                        <td class="text-right">$<?php echo number_format($invoice_data['BalanceRemaining'],2);  ?></td>
                    </tr>
                </tbody>
				
            </table>
				<div class="col-md-12 ">
			<div class="pull-right">
			 <?php if($invoice_data['BalanceRemaining']!='0.00'){ ?>
	<br><br>
			 <?php } ?>						 
			  </div>						 
			 
			</div>
			
        </div>
        <!-- END Products Content -->
    </div>  
    <!-- END Products Block -->
    <?php  
        $BillingAdd = 0;
        $ShippingAdd = 0;

        $isAdd = 0;
        if($invoice_data['cust_data']['address1']  || $invoice_data['cust_data']['address2'] || $invoice_data['cust_data']['City'] || $invoice_data['cust_data']['State'] || $invoice_data['cust_data']['zipCode'] || $invoice_data['cust_data']['Country']){
            $BillingAdd = 1;
            $isAdd = 1;
        }
        if($invoice_data['ShipAddress_Addr1']  || $invoice_data['ShipAddress_Addr2'] || $invoice_data['ShipAddress_City'] || $invoice_data['ShipAddress_State'] || $invoice_data['ShipAddress_PostalCode'] || $invoice_data['ShipAddress_Country'] ){
            $ShippingAdd = 1;
            $isAdd = 1;
        }

    ?>
    <!-- Addresses -->
    <div class="row">
    <?php if($isAdd){ ?>
        <?php if($BillingAdd){ ?>
        <div class="col-sm-6">
            <!-- Billing Address Block -->
            <div class="block">
                <!-- Billing Address Title -->
                <div class="block-title">
                    <h2><strong>Billing</strong> Address</h2>
                </div>
                <!-- END Billing Address Title -->

                <!-- Billing Address Content -->
                <h4><strong><?php echo $invoice_data['cust_data']['fullName']; ?></strong></h4>
				<address>
                    <?php echo ($invoice_data['cust_data']['address1'])? $invoice_data['cust_data']['address1'].'<br>':'';?>
                    <?php echo ($invoice_data['cust_data']['address2'])?$invoice_data['cust_data']['address2'].'<br>':''; ?> 
                    <?php  echo ($invoice_data['cust_data']['City'])?$invoice_data['cust_data']['City'].',':''; ?>
                    <?php echo ($invoice_data['cust_data']['State'])?$invoice_data['cust_data']['State']:''; ?> 
                    <?php echo ($invoice_data['cust_data']['zipCode'])?$invoice_data['cust_data']['zipCode'].'<br>':''; ?> 
                    <?php echo ($invoice_data['cust_data']['Country'])?$invoice_data['cust_data']['Country'].'<br>':''; ?>
                    <br>
                            
                    <i class="fa fa-phone"></i> <?php echo $invoice_data['cust_data']['phoneNumber'] ;      ?><br>
                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $invoice_data['cust_data']['userEmail'];    ?></a>
                </address>
                <!-- END Billing Address Content -->
            </div>
            <!-- END Billing Address Block -->
        </div>
        <?php }
        if($ShippingAdd){ ?>
        <div class="col-sm-6">
            <!-- Shipping Address Block -->
            <div class="block">
                <!-- Shipping Address Title -->
                <div class="block-title">
                    <h2><strong>Shipping</strong> Address</h2>
                </div>
                <!-- END Shipping Address Title -->

                <!-- Shipping Address Content -->
                  <h4><strong><?php echo $invoice_data['cust_data']['fullName']; ?></strong></h4>
				<address>
                    <?php  echo  ( $invoice_data['ShipAddress_Addr1'])? $invoice_data['ShipAddress_Addr1'].'<br>':'';?>
                    <?php echo( $invoice_data['ShipAddress_Addr2'])?$invoice_data['ShipAddress_Addr2'].'<br>':''; ?> 
                    <?php echo ($invoice_data['ShipAddress_City'])?$invoice_data['ShipAddress_City'].',':''; ?>
                    <?php echo ($invoice_data['ShipAddress_State'])?$invoice_data['ShipAddress_State']:''; ?> 
                    <?php echo ($invoice_data['ShipAddress_PostalCode'])?$invoice_data['ShipAddress_PostalCode'].'<br>':''; ?> 
                    <?php echo ($invoice_data['ShipAddress_Country'])?$invoice_data['ShipAddress_Country'].'<br>':''; ?>
                    <br>

                    <i class="fa fa-phone"></i> <?php echo $invoice_data['cust_data']['phoneNumber'] ;      ?><br>
                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $invoice_data['cust_data']['userEmail'];    ?></a>
                </address>
                <!-- END Shipping Address Content -->
            </div>
            <!-- END Shipping Address Block -->
        </div>
        <?php } ?>
    <?php } ?>
    </div>
    <!-- END Addresses -->

    <!-- Log Block -->
    <div class="block full">
                <!-- Private Notes Title -->
                <div class="block-title">
                    <h2><strong>Private</strong> Notes</h2>
                </div>
                <!-- END Private Notes Title -->

                <!-- Private Notes Content -->
                <div class="alert alert-info">
                    <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
                </div>
                 <form  method="post"  id="pri_form" onsubmit="return false;" >
                    <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="" data-args="Your note.."></textarea>
                   <input type="hidden" name="customerID" id="customerID" value="<?php echo $invoice_data['cust_data']['Customer_ListID']; ?>" />
					<br>
                    <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
                </form>
				<hr>
				
		
                      <?php   if(!empty($notes)){  foreach($notes as $note){ 
                        if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                            $timezone = ['time' => $note['privateNoteDate'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                            $note['privateNoteDate'] = getTimeBySelectedTimezone($timezone);
                        }
                        ?>
                
                <div>
                   <?php echo $note['privateNote']; ?>
                </div>
                <div class="pull-right">
                    <span>Added on <strong><?php   echo date('M d, Y - h:i A', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
                    <span><a href="javascript:void(0);"  onclick="delele_notes('<?php echo $note['noteID'] ; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
                </div>
                <br>
                <hr>
                <?php } } ?>
		    
				<br>
				<hr>
				
				
				<br>
                <!-- END Private Notes Content -->
            </div>
 </div>



<div id="set_email" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title pull-left">Send Email</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"></label>
                  <form id="form-validation" action="<?php echo base_url(); ?>QBO_controllers/Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                   
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                      
                                          <select id="type" name="type" class="form-control">
                                            <option value="">Choose Template</option>
                                                <option value="1">Invoice due</option>
                                                <option value="2">Invoice past due/overdue</option>
                                                <option value="3">Invoice due soon/upcoming</option>
                                                  <option value="5">Payment Receipt</option>
                                            </select>  
                                            
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="" data-args="Enter the name">
                                        </div>
                                    </div>
                                    <div class="form-group" style="display:none;" id="reply_div">
                                       <label class="col-md-3 control-label" for="replyEmail">Reply-To Email</label>
                                        <div class="col-md-7">
                                            <input type="text" id="replyTo" name="replyEmail" class="form-control" value="<?php if (isset($templatedata)) echo ($templatedata['replyTo']) ? $templatedata['replyTo'] : ''; ?>" placeholder="" data-args="Email">
                                        </div>
                                    </div>

                                    <div class="form-group" style="display:none" id="from_email_div">
                                        <label class="col-md-3 control-label" for="templteName">From Email</label>
                                        <div class="col-md-7">
                                            <input type="text" id="fromEmail" name="fromEmail"  value="<?php  echo $from_mail; ?>" class="form-control" placeholder="" data-args="From Email">
                                        </div>
                                    </div>
                                    <div class="form-group" id='display_name_div' style='display:none'>
                                        <label class="col-md-3 control-label" for="templteName">Display Name</label>
                                        <div class="col-md-7">
                                            <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="<?php echo $mailDisplayName; ?>" placeholder="" data-args="Display Name">
                                        </div>
                                    </div>
                                      <div class="form-group"  style="font-size: 12px;">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
                                            <a href="javascript:void(0);" id="open_reply">Add Reply-To<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                           <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                            <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                                           
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="" data-args="Enter the cc email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="" data-args="Enter the bcc Email">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="" data-args="Enter the Subject">
                                        </div>
                                    </div>

                             
                                     
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-7 col-md-offset-3 align-right">
                                        <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Save </button>
                                      
                                    </div>
                                </div>  
                           </form>
		    	
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

 <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js" ></script>
        <script>
        
        $(function(){   
		
			 CKEDITOR.replace( 'textarea-ckeditor', {
				toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
			});
			CKEDITOR.config.allowedContent = true;
		    $('#open_cc').click(function(){
                $('#cc_div').show();
                $(this).hide();
            });
            $('#open_bcc').click(function(){
                $('#bcc_div').show();
                $(this).hide();
            });
            $('#open_reply').click(function(){
                $('#reply_div').show();
                $(this).hide();
            }); 
            
            $('#open_from_email').click(function(){
                $('#from_email_div').show();
                $(this).hide();
            });
            $('#open_display_name').click(function(){
                $('#display_name_div').show();
                $(this).hide();
            });
		
          /*********************Template Methods**************/ 
	 
 $('#type').change(function(){
 
    
	  var typeID = $(this).val();

	  var companyID = '<?php echo $invoice_data['cust_data']['companyID']; ?>';
	  var customer  = '<?php echo $invoice_data['cust_data']['fullName']; ?>';  
	  var customerID = '<?php echo $invoice_data['cust_data']['Customer_ListID']; ?>';  
	  var invoiceID  = '<?php echo $invoice_data['invoiceID']; ?>';
	 if(typeID !=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>QBO_controllers/Settingmail/set_template',
		  data : {'typeID':typeID,'companyID':companyID,'customerID':customerID, 'invoiceID':invoiceID,  'customer':customer },
		  success: function(data){
			     data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			   $('#emailSubject').val(data['emailSubject']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
		  }
	   });	 
	   
	 } 
 }); 


});

	
function delele_notes(note_id){
	
	if(note_id !=""){
		
		
		$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>home/delele_note",
			data:{ 'noteID': note_id},
			success : function(response){
				
				
				   data=$.parseJSON(response);
				  
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
	}
	
}

function add_notes(){
    
    var formdata= $('#pri_form').serialize();

    if($('#private_note').val()!=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>home/add_note",
			data:formdata,
			success : function(response){
				
				
		     	 data=$.parseJSON(response);
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
    }
    
}
    
</script>
    <!-- END Log Block -->

