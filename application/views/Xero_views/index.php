<style>
#chart-classic2 g text tspan {
    visibility: visible;
}
#chart-classic2 g {
    opacity: 0.7;
	borderWidth:2px;
}

#chart-classic2 text tspan {
    visibility: hidden;
}



</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">
<link rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/global.css">
<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
   

    <!-- Quick Stats -->
    <div class="row invoice_quick">
	
	   <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>

        <?php if (isset($passwordExpDays)) { ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert-non-fade alert-warning">
                        <strong>Password Update:</strong> &nbsp;
                        A required password change is due in <?= $passwordExpDays ?> days. &nbsp;Click
                        <a class="alert-link" href="#modal-user-settings" data-toggle="modal" data-original-title="" title="" style="font-weight:700;">here</a>
                        to change your password.
                    </div>
                </div>
            </div>
        <?php } ?>

       <div class="col-sm-6 col-lg-3">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-success rounded-circle mr-2">
                            <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                        $volume =$recent_pay; 
                                    ?>$<?php echo $volume?number_format($volume,2):'0.00'; ?></span></h4>
                                <p class=""><?php echo date('F');?> Revenue</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-info rounded-circle">
                            <i class="fa fa-credit-card ion-md-cart avatar-title font-26 text-white"></i>
                            
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                        $card_payment =$card_payment; 
                                    ?>$<?php echo $card_payment?number_format($card_payment,2):'0.00'; ?></span></h4>
                                <p class="">Credit Card Payments</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-purple rounded-circle">
                            <i class=""></i>
                            <i class="fa fa-credit-card custom-warning-icon ion-md-contacts avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                        $eCheck_payment =$eCheck_payment; 
                                    ?>$<?php echo $eCheck_payment?number_format($eCheck_payment,2):'0.00'; ?></span></h4>
                                <p class=" ">eCheck Payments</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card-box">
                    <div class="media">
                        <div class="avatar-md bg-primary rounded-circle">
                           
                             <i class="fa fa-calendar custom-danger-icon ion-md-eye avatar-title font-26 text-white"></i>
                        </div>
                        <div class="media-body align-self-center">
                            <div class="text-right">
                                <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php 
                                        $outstanding_total =$outstanding_total; 
                                    ?>$<?php echo $outstanding_total?number_format($outstanding_total,2):'0.00'; ?></span></h4>
                                <p class="">Accounts Receivable</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end card-box-->
            </div>

    </div>


    
    <!-- END Quick Stats -->

    <!-- eShop Overview Block -->
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="card-header py-3 bg-transparent">
                    
                    <h5 class="card-title">Revenue History: <span id="totalPayment">$0.00</span></h5>
                </div>
              
                <div class="revenueGraph">
                    <div id="placeholder" style="height:300px;" class="demo-placeholder"></div>
                    <div id="choices" >
                        
                    </div>

                    <div id="legendContainer" class="legend" ></div>

                </div>
            </div>
        </div>
        
    </div>
	<div class="row">
        <div class="col-sm-6">
            
            <div class="block full">
                <div class="card-header py-3 bg-transparent">
                    <h5 class="card-title">Top 10 Due By Customer</span></h5>
                </div>
                <div id="chart_pie_customer" class="chart_pie_sm"></div>

            </div>    
        </div>
        <div class="col-sm-6">
            
            <div class="block full">
                <div class="card-header py-3 bg-transparent">
                    <h5 class="card-title">Top 10 Past Due By Customer</span></h5>
                </div>
                <div id="chart-pie" class="chart_pie_sm"></div>
            </div> 
        </div>
        
    </div>
    <div class="row">
        <div class="col-sm-6">
            <!-- Latest Orders Block -->
           
                <!-- Latest Orders Title -->
                
                <div class="block-white full">
                    <div class="card-header py-3 bg-transparent pad-20">
                        <h5 class="card-title">Recent Payments</span></h5>
                    </div>
                <!-- END Latest Orders Title -->
			    <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
                        <tr>
                            <td class="text-left"><strong>Customer Name</strong></td>
							<td class="text-left"><strong>Invoice</strong></td>
                            <td class="text-right"><strong>Payment Date</strong></td>
                            <td class="text-right"><strong>Amount</strong></td>
							
                           
                        </tr>
						
                       	<?php   if(!empty($recent_paid)){   
 
						foreach($recent_paid as $invoice){

					 ?>
                        <tr>
                         <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Xero_controllers/Customer/view_customer/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>

                          

                           <td class="text-left cust_view"><?php echo $invoice['refNumber']; ?></td>
                         
                            <td class="text-right "><?php  echo  date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>
                            <td class="text-right">$<?php  echo  number_format($invoice['balance'],2); ?></td>
							 
                              
                        </tr>
						   <?php } }else { ?>	
						
						<tr>
                            <td colspan="4"><strong>No Records Found</strong></td>
                         
                        </tr>
						
						   <?php } ?>
						
						   
                       
                        
                    </tbody>
                </table>
              
                <!-- END Latest Orders Content -->
            </div>
            <!-- END Latest Orders Block -->
        </div>
        <div class="col-sm-6">
            <!-- Top Products Block -->
            
            <div class="block-white full">
                <!-- END Top Products Title -->
                <div class="card-header py-3 bg-transparent pad-20">
                    <h5 class="card-title">Recent Payments</span></h5>
                </div>
                <!-- Top Products Content -->
               <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
                        <tr>
                            <td class="text-left" ><strong>Customer Name</strong></td>
                            <td class="text-left" ><strong>Invoice</strong></td>
							  
                            <td class="text-right"><strong>Due Date</strong></td>
                           
							  <td class="text-right"><strong>Amount</strong></td>

                        </tr>
						
						<?php   if(!empty($oldest_invs)){   

						foreach($oldest_invs as $invoice){
						  
						      if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }   
						else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   } else
						   
						   if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
							
						 ?>
                        <tr>
                          <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Xero_controllers/Customer/view_customer/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>
                           <td class="text-right cust_view"> <a href="<?php echo base_url();?>Xero_controllers/Xero_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']; ?></a></td>
                          <td class="text-right"><?php  echo  date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
						   <td class="text-right">$<?php  echo  number_format($invoice['BalanceRemaining'],2); ?></td>
                            
                        </tr>
						   <?php  } }else { ?>	
						<tr>
                            <td colspan="5"><strong>No Records Found</strong></td>
                        </tr>
						
						   <?php } ?>
						
						   
                       
                        
                    </tbody>
                </table>
                <!-- END Top Products Content -->
            </div>
            <!-- END Top Products Block -->
        </div>
    </div>
    <!-- Live Chart Block -->

	
	
	</div>
	

<link id="themecss" rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/shieldui/all.min.css" />          
<script type="text/javascript" src="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/js/shieldui/shieldui-all.min.js"></script>	
<script>$(function(){   
});</script>
<script>
  /*
 *  Document   : compCharts.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Charts page
 */
                        
                
                        
                        
                        
var jsdata='',jsdata1='';
var pie ='pie_action';
var general_volume = 0;

var pchart_color_customer = []; 
var pdchartColor=[];
var pchart_color = []; 
var chartColor =[];
	
    $.ajax({
			type:"POST",
				url : "<?php echo base_url(); ?>Integration/home/get_invoice_due_company",
            success: function (data) {
            
				    jsdata1 = data.customer_due;
				    jsddd = data.customer_due;
           
                    for(var val in  jsddd) {
                        if(val==0){
                            chartColor.push('#007bff');
                        }else if(val==1){
                            chartColor.push('#28a745');
                        }else if(val==2){
                        chartColor.push('#ffc107');	 	
                                
                        }else if(val==3){
                            chartColor.push('#e67e22');
                        }else if(val==4){
                            chartColor.push('#e74c3c');	 
                        }else if(val==5){
                            chartColor.push('#34dbcb');
                        }else if(val==6){
                            chartColor.push('#db7734');	 
                        }else if(val==7){
                            chartColor.push('#3abc'); 
                        }else if(val==8){
                            chartColor.push('#ff1a75 ');	 
                        }else if(val==9){
                            chartColor.push('#8c1aff');
                        }	 
                        
                    }

                    showjsdataLegend1 = true;
                    if (jsdata1 == undefined || jsdata1.length == 0){
                        jsdata1 = [['No Data', 0.0001]];
                        chartColor = ['#7F7F7F'];
                        showjsdataLegend1 = false;
                    }

                    var chartPie1 =$('#chart_pie_customer');

                    CompCharts.init(jsdata1, chartPie1, chartColor, showjsdataLegend1); 

                }
    		});	
    		
    $.ajax({
			type:"POST",
				url : "<?php echo base_url(); ?>Integration/home/get_invoice_Past_due_company",
            success: function (data) {
                    jsdata = data.customer_due;
				    jsddd1 = data.customer_due;
           
                    for(var val in  jsddd1) {
                        if(val==0){
                            pchart_color.push('#007bff');
                        }else if(val==1){
                            pchart_color.push('#28a745');
                        }else if(val==2){
                        pchart_color.push('#ffc107');	 	
                                
                        }else if(val==3){
                            pchart_color.push('#e67e22');
                        }else if(val==4){
                            pchart_color.push('#e74c3c');	 
                        }else if(val==5){
                            pchart_color.push('#34dbcb');
                        }else if(val==6){
                            pchart_color.push('#db7734');	 
                        }else if(val==7){
                            pchart_color.push('#3abc'); 
                        }else if(val==8){
                            pchart_color.push('#ff1a75 ');	 
                        }else if(val==9){
                            pchart_color.push('#8c1aff');
                        }	 
                        
                    }

                    showjsdataLegend = true;
                    if (jsdata == undefined || jsdata.length == 0){
                        jsdata = [['No Data', 0.0001]];
                        pchart_color = ['#7F7F7F'];
                        showjsdataLegend = false;
                    }

                    var chartPie1 =$('#chart-pie');

                    CompCharts.init(jsdata, chartPie1, pchart_color, showjsdataLegend); 
                }
    		});


    
    		
        var CompCharts = function() {

            return {
                init: function(pchart_data,char_id, color, showLegend = true) {
            
            //     var chartPiecustomer = $('#chart-pie-customers');
            var colorPalette = color;
                    
                char_id.shieldChart({
                    events: {
                        legendSeriesClick: function (e) {
                            // stop the series item click event, so that 
                            // user clicks do not toggle visibility of the series
                            e.preventDefault();
                        }
                    },
                    theme: "bootstrap",
                    seriesPalette: colorPalette,
                    seriesSettings: {
                        donut: {
                            enablePointSelection: false,
                            addToLegend: showLegend,
                            activeSettings: {
                                pointSelectedState: {
                                    enabled: false
                                }
                            },
                            enablePointSelection: false,
                            
                            dataPointText: "",
                            borderColor: '#ffffff',
                            borderWidth:2
                        }
                    },
                    
                    chartLegend: {
                        align: "right",
                        renderDirection: 'vertical',
                        verticalAlign: "middle",
                        legendItemSettings: {
                            bottomSpacing: 7
                        }
                    },
                    exportOptions: {
                        image: false,
                        print: false
                    },
                    primaryHeader: {
                        text: " "
                    },
                    tooltipSettings: {
                        customHeaderText:"{point.collectionAlias}", 
                    
                    
                        
                customPointText: function (point, chart) {
                                
                    return shield.format(
                        '<span>{value}</span>',
                        {
                            value: format2(point.y)
                        }
                    );
                }
                    
                    },
                    axisY: {
                        title: {
                            text: ""
                        }
                    },
                    dataSeries: [{
                        seriesType: "donut",
                        collectionAlias: "Customers",
                        data: pchart_data
                    }]
                });
                    
                    
                

                }
            };
        }();



</script>

<style>
	#chart-classic1 g text tspan {
    visibility: visible;
}
#chart-classic1 g {
    /* opacity: 0.7; */
	border-width:2px;
}

#chart-classic1 text tspan {
    visibility: hidden;
}

	#chart_pie_customer g text tspan {
    visibility: visible;
}
#chart_pie_customer g {
    opacity: 1.5;
	border-width:2px;
}

#chart-pie text tspan {
    visibility: hidden;
}

	#chart-pie g text tspan {
    visibility: visible;
}
#chart-pie g {
    opacity: 1.5;
	border-width:2px;
}

#chart_pie_customer text tspan {
    visibility: hidden;
}
.shield-container{
    margin-top: -40px;
}
</style>

<!-- flot chart -->
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.time.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.selection.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.stack.js"></script>

<link href="<?php echo base_url(CSS); ?>/moltran/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />
<script type="text/javascript">


        

        $.ajax({
            type:"POST",
            url : "<?php echo base_url(); ?>Integration/home/dashboardReport",
            success: function (data) {
                var general_volume = $.parseJSON(data);
                $('#totalPayment').html(format2(general_volume.totalRevenue));  
                $('#RA').html(format2(general_volume.totalRevenue)); 
                $('#CCA').html(format2(general_volume.totalCCA));   
                $('#ECLA').html(format2(general_volume.totalECLA)); 
                var x1 = general_volume.revenu_volume;
                var x2 = general_volume.online_volume;
                var x3 = general_volume.eCheck_volume;

                var y1 = general_volume.revenu_month;
                var legendContainer = document.getElementById("legendContainer");


                var datasets = {
                    "Revenue": {
                        data: x1, label: "Revenue",color:'#33b86c',hoverable:true, shadowSize: 2,highlightColor: '#007bff',clickable: true
                     },
                    "Credit Card Payments": {
                            data: x2, label: "Credit Card Payments",color:'#007bff',hoverable:true,clickable: true
                        },
                    "eCheck Payments": {
                         data: x3, label: "eCheck Payments",color:'#7e57c2',hoverable:true,clickable: true
                    }
                };
                var i = 1;
                

                var choiceContainer = $("#choices");

                $.each(datasets, function(key, val) {
                    var checkM = '';
                    var classOpacity = '';
                    if(key != 'Revenue'){
                         checkM = "checked='checked'";
                    }else{
                         classOpacity = 'opacityManage';
                    }
                    
                    choiceContainer.append("<div class='displayLegend " + classOpacity + " ' id='color" + i + "'><input class='checkboxNone' type='checkbox' name='" + key +
                        "' " + checkM + " data-id='" + i + "' id='id" + key + "'></input> <label id='color" + i + "' style='padding:1px;width: 12px;height: 12px;background:"+ val.color +" ;' for='id" + key + "' id='colorSet" + i + "'></label>" +
                        "<label class='legendLabel' for='id" + key + "'>"
                        + val.label + "</label> </div>");
                    ++i;
                    
                });

                var data = [];

                choiceContainer.find("input:checked").each(function () {
                    var key = $(this).attr("name");
                    if (key && datasets[key]) {
                        data.push(datasets[key]);
                    }
                });
                choiceContainer.find("input").click(plotAccordingToChoices);
                $('.checkboxNone').change(function() {
                    var id = $(this).attr("data-id");
                    
                    var checkGraph = choiceContainer.find("input:checked").length;
                    if(checkGraph != 0){
                      if ($(this).prop('checked') == true){
                          $("#color"+id).removeClass('opacityManage');
                      } else {
                          $("#color"+id).addClass('opacityManage');
                      }
                    }
                });
                function plotAccordingToChoices() {

                    var data = [];

                    choiceContainer.find("input:checked").each(function () {
                        var key = $(this).attr("name");
                        
                        if(key != 'Revenue'){
                            if (key && datasets[key]) {
                                data.push(datasets[key]);
                            }
                        }else{
                            if ($('#idRevenue').prop('checked') == true){
                                if (key && datasets[key]) {
                                    data.push(datasets[key]);
                                }
                            }
                        
                        }
                        
                    });

                    if (data.length > 0) {
                        $.plot("#placeholder", data, {
                            series: { lines: { show: !0, fill: !0, lineWidth: 1, fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.9 }] } }, points: { show: !0 }, shadowSize: 0 },
                            legend: {
                                position: "nw",
                                margin: [0, -24],
                                noColumns: 0,
                                backgroundColor: "transparent",
                                labelBoxBorderColor: null,
                                labelFormatter: function (o, t) {
                                    return o + "&nbsp;&nbsp;";
                                },
                                width: 30,
                                height: 2,
                                container: legendContainer,
                                onItemClick: {
                                      toggleDataSeries: true
                                  },
                            },
                            grid: { hoverable: !0, clickable: !0, borderColor: i, borderWidth: 0, labelMargin: 10, backgroundColor: 'transparent' },
                            yaxis: { min: 0, max: 15, tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            xaxis: { ticks: y1,tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            tooltip: !0,
                            tooltipOpts: { content: function(data, x, y, dataObject) {

                                            var XdataIndex = dataObject.dataIndex;
                                            var XdataLabel = dataObject.series.xaxis.ticks[XdataIndex].label;
                                            return format2(y);
                                        }, shifts: { x: -60, y: 25 }, defaultTheme: !1 },

                            
                            
                            yaxis: {
                                autoScale:"exact"
                            }
                        }); 
                    }
                }
                plotAccordingToChoices();
                 
            }
        });

       function getTooltip(label, x, y) {
            return "Your sales for " + x + " was $" + y; 
        }

</script>