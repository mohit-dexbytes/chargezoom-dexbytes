<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>

    <!-- All Orders Block -->
    <legend class="leg">Accounting Package</legend>
    <div class="full">
        <!-- All Orders Title -->
        <div class="block-title">
           
            <div class="block-options pull-right">
                            <div class="">
							<a href="<?php echo base_url('Integration/home/xero_log'); ?>"  data-toggle="tooltip" class="btn btn-info btn-sm" > View Log</a>
              
                            </div>
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Accounting Package</th>
                 
                    <th class=" text-left hidden-xs">Company ID</th>
                     <th class="hidden-xs text-right">Refresh Token</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($companies) && $companies)
				{
					foreach($companies as $company)
					{
				?>
				<tr>
					<td class="text-left cust_view"><?php echo "QuickBooks Online"; ?></td>
				    <td class="text-left hidden-xs"><?php echo $company['realmID']; ?></td>
					<td class="text-right hidden-xs"><?php echo $company['refreshToken']; ?></td>
					
			 
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#company').dataTable({
                columnDefs: [
                  
                    { orderable: false, targets: [2] }
                ],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();




</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 

</div>

<div id="company_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">App Details</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_company"  style="height: 300px; min-height:300px;  overflow: auto; " >
			
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- END Page Content -->