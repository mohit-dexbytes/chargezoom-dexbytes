<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <span id="flashdata"> 
        <?php echo $this->session->flashdata('message');   ?>
    </span>

    <!-- All Orders Block -->
    <div class="block full">
        
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Sync Log</strong> </h2>
                     
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left">ID</th>
                    <th class="text-right ">Data</th>
                    
                    <th class="hidden-xs hidden-sm text-right" style="width:225px !important;">Status</th>
                    
                    <th class="text-right hidden-xs">Time</th>
                 
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($alls) && $alls)
				{
					foreach($alls as $k=>$all)
					{
                     
				?>
				<tr>
					
					<td class="text-left "><?php echo"#".$all['xeroActionID']; if($all['xeroStatus']==0 ){ echo '<br/> <a href="javascript:void(0)" class="btn btn-warning" >Sync Again</a>';  }  ?></td>
				    <td class="hidden-sm text-right "><?php echo $all['xeroAction'];  ?></td>
				    <td class="text-right hidden-xs" style="width:225px !important;"><?php 
                    if($all['xeroStatus']==0 ){  echo "Not Synced <br>". $all['xeroText']; }
                   if($all['xeroStatus']==1 ){ echo "Synced"; } ?>
                 </td>
					<td class="text-right "><?php echo date('m-d-Y H:i A', strtotime($all['createdAt']));  ?></td>
				</tr>
				
				<?php } }else{ echo "<tr><td colspan='4'>No Record Found</td></tr>"; } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ 
Pagination_view111.init(); });



var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: true, targets: [-1] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script type="text/javascript">
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 3000); //call fade in 2 seconds
 })

function fadeMyDiv() {
}


function set_seenk_data(tid='',  cltype='')
{

$("#snk_form").remove();
var f = document.createElement("form");
f.setAttribute('method',"post");

f.setAttribute('id',"snk_form");

if(cltype=='Customer'){
f.setAttribute('action',"<?php echo base_url(); ?>MerchantUser/sync_customer");

var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"cust_id");
i.setAttribute('value', tid);

}
if(cltype=='Invoice'){
f.setAttribute('action',"<?php echo base_url(); ?>SettingSubscription/sync_invoice");
var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"inv_id");
i.setAttribute('value', tid);
}
if(cltype=='Product'){
f.setAttribute('action',"<?php echo base_url(); ?>MerchantUser/sync_product");
var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"prod_id");
i.setAttribute('value', tid);
}

f.appendChild(i);

//and some more input elements here
//and dont forget to add a submit button

document.getElementsByTagName('body')[0].appendChild(f);

$('#snk_form').submit();

}
</script>
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>


</div>
<!-- END Page Content -->