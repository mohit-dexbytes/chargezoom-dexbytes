 <!-- Page content -->
 <?php
	$this->load->view('alert');
?>

	<div id="page-content">
	    	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- Wizard Header -->
     
	
	 
 
	
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->

	
	
	
	
    <!-- Progress Bar Wizard Block -->
    <legend class="leg"><?php  echo "General Settings";?> </legend>
	<div class="block">
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="form_new" class="form form-horizontal"  enctype="multipart/form-data" action="<?php echo base_url(); ?>Xero_controllers/SettingConfig/profile_setting">
		         <input type="hidden" id ="merchID" name="merchID" value="<?php if(isset($invoice)){echo $invoice['merchID']; } ?>"  />
                 
				 
				
                  
                    <div class="col-md-6">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">First Name</label>
							<div class="col-md-8">
								<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['firstName']; } ?>" placeholder="" data-args="First Name">
							</div>
						</div>
                       
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Last Name</label>
							<div class="col-md-8">
								<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['lastName']; } ?>" placeholder="" data-args="Last Name">
							</div>
						</div>
                      
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Email Address</label>
							<div class="col-md-8">
								<input type="text" readonly="readonly" id="merchantEmail" name="merchantEmail" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantEmail']; } ?>" placeholder="" data-args="Email Address ">
							</div>
						</div>
					   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Company Name</label>
							<div class="col-md-8">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['companyName']; } ?>" placeholder="" data-args="Company Name">
							</div>
						</div>
                      
					<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Website</label>
							<div class="col-md-8">
								<input type="text" id="weburl" name="weburl"  value="<?php if(isset($invoice) && !empty($invoice)){ echo $invoice['weburl']; } ?>" class="form-control" placeholder="" data-args="Website">
							</div>
					</div>
				
					   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Invoice Prefix </label>
							<div class="col-md-8">
							<input type="text" id="prefix" name="prefix" class="form-control" 
							value="<?php if(isset($prefix) && !empty($prefix)){ echo $prefix['prefix']; }else{ echo"CZ-";
							} ?>" placeholder="" data-args="Invoice Prefix">
							
							</div>
					  </div>
				 
					    <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Invoice Start Number </label>
							<div class="col-md-8">
							<input type="text" id="postfix" name="postfix" class="form-control" 
							value="<?php if(isset($prefix) && !empty($prefix)){ echo $prefix['postfix']; }else{echo "10000";} ?>" placeholder="" data-args="Invoice Start Number">
							</div>
							</div>
						
                    		<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Timezone</label>
								<?php
									$timezone = '';
									if(isset($invoice['merchant_default_timezone']) && !empty($invoice['merchant_default_timezone']))
									{ 
										$timezone = $invoice['merchant_default_timezone']; 
									}
								?>
								<div class="col-md-8">
									<select class="form-control" name="merchant_default_timezone">
										<option value="America/Adak" <?php echo ($timezone == "America/Adak") ? "selected" : ''; ?> > HST - Hawaii Standard Time - GMT-10:00</option>
										<option value="America/Anchorage" <?php echo ($timezone == "America/Anchorage") ? "selected" : ''; ?> > AST - Alaska Standard Time - GMT-9:00</option>
										<option value="America/Los_Angeles" <?php echo ($timezone == "America/Los_Angeles") ? "selected" : ''; ?> > PST - Pacific Standard Time - GMT-8:00</option>
										<option value="America/Denver" <?php echo ($timezone == "America/Denver") ? "selected" : ''; ?> > MST - Mountain Standard Time - GMT-7:00</option>
										<option value="America/Chicago" <?php echo ($timezone == "America/Chicago") ? "selected" : ''; ?> > CST - Central Standard Time - GMT-6:00</option>
										<option value="America/New_York" <?php echo ($timezone == "America/New_York") ? "selected" : ''; ?> > EST - Eastern Standard Time - GMT-5:00</option>
									</select>
								</div>
							</div>
					 </div>
					   <div class="col-md-6">  
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 1</label>
							<div class="col-md-8">
								<input type="text" id="merchantAddress1" name="merchantAddress1" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAddress1']; } ?>" placeholder="" data-args="Address Line 1">
							</div>
						</div>
					    
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 2</label>
							<div class="col-md-8">
								<input type="text" id="merchantAddress2" name="merchantAddress2" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAddress2']; } ?>" placeholder="" data-args="Address Line 2">
							</div>
						</div>
                         	<div class ="form-group">
						   <label class="col-md-4 control-label" name="city" for="example-typeahead">City</label>
						   <div class="col-md-8">
						      <input type="text" id="city" class="form-control" name="city" placeholder="" data-args="City" value="<?php if(isset($invoice)){ echo $invoice['merchantCity']; } ?>"> 
						       
							
							</div>
                        </div>
						<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state" for="example-typeahead">State</label>
						   <div class="col-md-8">
						       	<input type="text" id="state" class="form-control " name="state" placeholder="" data-args="State" value="<?php if(isset($invoice)){ echo $invoice['merchantState'];} ?>">
						       
							
							</div>
                        </div>	
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="country" for="example-typeahead">Country</label>
						   <div class="col-md-8">
						         <input type="text" id="country" class="form-control" name="country" placeholder="" data-args="Country" value="<?php if(isset($invoice)){ echo $invoice['merchantCountry']; } ?>">
							
							</div>
                        </div>
					   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Phone Number</label>
							<div class="col-md-8">
								<input type="text" id="merchantContact" name="merchantContact" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantContact']; } ?>" placeholder="" data-args="Phone Number">
							</div>
						</div>
						 <div class="form-group">
    							<label class="col-md-4 control-label" for="example-username">Alternate Contact</label>
    							<div class="col-md-8">
    								<input type="text"  id="merchantAlternateContact" name="merchantAlternateContact" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAlternateContact']; } ?>" placeholder="" data-args="Alternate Contact">
    							</div>
    						</div>
					 <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">ZIP Code</label>
							<div class="col-md-8">
								<input type="text" id="merchantZipCode" name="merchantZipCode" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantZipCode']; } ?>" placeholder="" data-args="ZIP Code">
							</div>
						</div>
						
				    </div>		
					
	              <div class="form-group">
					<div class="col-md-8 col-md-offset-11">
						
						<button type="submit" class="submit btn btn-sm btn-success" >Save</button>	
					</div>
				</div>					
					
		
		
		
  	</form>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>
$(document).ready(function(){

   
    $('#form_new').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'merchantAddress1': {
                        required: true,
                        minlength: 5
                    },
					
					'city':{
					      required:true,
						  
						  
					},
					'state':{
					      required:true,
						  
						  
					},
					'country':{
					      required:true,
					},
					'firstName':{
						    required:true,
							minlength: 2
					},
					'lastName':{
						    required:true,
							minlength: 2
					},
					
					  'merchantContact': {
                        required: true,
						 minlength: 10,
						 number : true,
						 maxlength: 12,
                       
                    },
                    'merchantZipCode': {
                        required: true,
                         minlength: 3
                    },
					'prefix':{
					    required: true,
					   minlength: 3,
					   },
					'postfix':{
					    required: true,
					    digits:true,
                    }
			
			},
    });

 
 
   $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Xero_controllers/SettingConfig/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('SettingConfig/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});   


});	
   
    
		
	

</script>

</div>
