<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo $template['title'] ?></title>

        <meta name="description" content="<?php echo $template['description'] ?>">
        <meta name="author" content="<?php echo $template['author'] ?>">
       
		
        <!-- Include a specific file here from <?php echo base_url(CSS); ?>/themes/ folder to alter the default theme of the template -->
        <?php if ($template['theme']) { ?><link id="theme-link" rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes/<?php echo $template['theme']; ?>.css"><?php } ?>

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes.css">
        <!-- END Stylesheets -->
		

        <!-- Modernizr (browser feature detection library) -->
	
    </head>
	
    <body style="">
	
<!-- Page content -->
<div id="page-content">
    
	<!-- Products Block -->
    <div class="block">
        <!-- Customer Addresses Content -->
		<h2 style=" clear:left"><strong>&nbsp;&nbsp;Invoice</strong></h2>
		<br>
		<div class="row" > 
			<div class="div2">
				<!-- Billing Address Block -->
				<div class="bl ock">
					
				<strong>From:</strong>
					<address>
						<?php echo  $login_info['firstName'].' '. $login_info['lastName']; ?><br>
						<?php echo  $login_info['companyName']; ?><br>
						<?php echo  $login_info['merchantAddress1'].' '.$login_info['merchantAddress2'].' '.$login_info['merchantState'].''.$login_info['merchantCity'].''.$login_info['merchantZipCode']	; ?><br>
						<?php echo  $login_info['merchantContact'].', '.$login_info['merchantEmail']; ?>
					</address>
					<!-- END Billing Address Content -->
				</div>
				<!-- END Billing Address Block -->
			</div>
			<div class="div2" style="margin-left:30px;" >
				<!-- Shipping Address Block -->
				<div class="blo ck">
					
					
					<!-- Shipping Address Content -->
					<strong>Invoice ID : </strong> <?php echo $invoice_data['refNumber'] ?><br><br>
					<strong>Due Date : </strong><?php echo date('F d, Y', strtotime($invoice_data['DueDate'])); ?><br><br><br><br>
					
				</div>
				<!-- END Shipping Address Block -->
			</div>
		</div>
		<br>
		<!-- Customer Addresses Content -->
		<div class="row">
			<div class="div2">
				<!-- Billing Address Block -->
				<div class="bl ock">
					
					<strong>Billing Address</strong>
					<address>
					<strong><?php echo $invoice_data['customer_data']['companyName']; ?></strong><br>
					<?php echo $invoice_data['customer_data']['address1']; ?><br>
					<?php echo  ($invoice_data['customer_data']['City'])?$invoice_data['customer_data']['City'].',':'' . $invoice_data['customer_data']['State'];  ?><br>
					<?php echo $invoice_data['customer_data']['Country'] .', '. $invoice_data['customer_data']['zipCode'];  ?><br><br>
					<i class="fa fa-phone"></i> <?php echo $invoice_data['customer_data']['phoneNumber'] ;   ?><br>
					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $invoice_data['customer_data']['userEmail'];    ?></a>
				</address>
					<!-- END Billing Address Content -->
				</div>
				<!-- END Billing Address Block -->
			</div>
			<div class="div2" style="margin-left:30px;">
				<!-- Billing Address Block -->
				<div class="blo ck">
					
					<strong>Shipping/Customer Address</strong>
						<address>
					<strong><?php echo $invoice_data['customer_data']['companyName']; ?></strong><br>
					<?php echo $invoice_data['customer_data']['address1']; ?><br>
					<?php echo  ($invoice_data['customer_data']['City'])?$invoice_data['customer_data']['City'].',':'' . $invoice_data['customer_data']['State'];  ?><br>
					<?php echo $invoice_data['customer_data']['Country'] .', '. $invoice_data['customer_data']['zipCode'];  ?><br><br>
					<i class="fa fa-phone"></i> <?php echo $invoice_data['customer_data']['phoneNumber'] ;   ?><br>
					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $invoice_data['customer_data']['userEmail'];    ?></a>
				</address>
					<!-- END Billing Address Content -->
				</div>
				<!-- END Billing Address Block -->
			</div>
		</div>
		<br>
		
        <!-- Products Content -->
         <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-right">S.N.</th>
						<th>Description</th>
                        <th class="text-right">Qty</th>
                        <th  width="100px" class="text-right">Unit Rate</th>
						<th class="text-right">Amount</th>
						<th class="text-right">Tax</th>
                    </tr>
                </thead>
                <tbody>
				        <?php  
						foreach($invoice_data['invoice_items'] as $key=>$item){
							
						?>
				   
                    <tr>
                        <td class="text-right"><?php echo $key+1; ?></td>
						<td><?php echo $item['itemDescription']; ?></td>
                        <td class="text-right"><?php echo $item['itemQty']; ?></td>
                        <td class="text-right"><?php echo number_format($item['itemPrice'],2); ?></td>
						<td class="text-right"><?php  echo number_format($item['totalAmount'], 2) ; ?></td>
						<th class="text-right"><strong><?php  echo ($invoice_data['taxRate'])?$invoice_data['taxRate']:'0.00'; ?> <a href="#" data-toggle="tooltip" title="<?php echo $item['SalesTaxCode_ListID']; ?> @ <?php echo ($tax)? $tax:'0.00'; ?>%"><i class="fa fa-exclamation-circle text-danger"></i></a></strong></th>
					
                    </tr>
						<?php } ?>
                  
					<tr class="active">
                        <td colspan="4" class="text-right text-uppercase"><strong>Total</strong></td>
                        <td class="text-right"><strong><?php if($invoice_data['IsPaid']=='1'){ $balance=$invoice_data['Total_payment'];}else{ $balance=$invoice_data['BalanceRemaining']; }  echo  number_format(($balance-$invoice_data['totalTax']), 2) ; ?></strong></td>
						<td class="text-right"><strong><?php echo  number_format($invoice_data['totalTax'], 2) ; ?></strong></td>
                    </tr>
					<tr>
                        <td colspan="5" class="text-right text-uppercase"><strong>Any Other Tax</strong></td>
                        <td class="text-right"><strong>00.00 <a href="#" data-toggle="tooltip" title="No Other Tax"><i class="fa fa-exclamation-circle text-danger"></i></a></strong></td>
                    </tr>
					<tr class="info">
                        <td colspan="5" class="text-right text-uppercase"><strong>Total Payable</strong></td>
                        <td class="text-right"><strong><?php echo  number_format($invoice_data['BalanceRemaining'], 2) ; ?></strong></td>
                    </tr>
                    <tr class="success">
                        <td colspan="5" class="text-right text-uppercase"><strong>Paid</strong></td>
                        <td class="text-right"><strong><?php if($invoice_data['BalanceRemaining']==$invoice_data['Total_payment']){  $paid ='0.00';}else{$paid =number_format(($invoice_data['Total_payment']),2);} echo ($paid)?$paid:'0.00';  ?></strong></td>
                    </tr>
					<tr class="danger">
                        <td colspan="5" class="text-right text-uppercase"><strong>Balance</strong></td>
                        <td class="text-right"><strong><?php echo ($invoice_data['BalanceRemaining'])?$invoice_data['BalanceRemaining']:'0.00';  ?></strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
		<strong>Important:</strong><br>
		1. Please ignore if already paid.<br>
		2. Please contact <?php echo  $login_info['merchantContact'].' or '.$login_info['merchantEmail']; ?> if you have any question.
		<br>
		<br>
        <!-- END Products Content -->
    </div>
    <!-- END Products Block -->

    
</div>
<!-- END Page Content -->

<style type="text/css">






/*! CSS Used from: http://localhost/quickbook/resources/css/bootstrap.min.css */
body{margin:0;}
a{background-color:transparent;}
a:active,a:hover{outline:0;}
strong{font-weight:bold;}
table{border-collapse:collapse;border-spacing:0;}
td,th{padding:0;}
@media print{
*,*:before,*:after{background:transparent!important;color:#000!important;-webkit-box-shadow:none!important;box-shadow:none!important;text-shadow:none!important;}
a,a:visited{text-decoration:underline;}
a[href]:after{content:" (" attr(href) ")";}
a[href^="#"]:after{content:"";}
thead{display:table-header-group;}
tr{page-break-inside:avoid;}
h2{orphans:3;widows:3;}
h2{page-break-after:avoid;}
.table{border-collapse:collapse!important;}
.table td,.table th{background-color:#fff!important;}
.table-bordered th,.table-bordered td{border:1px solid #ddd!important;}
}
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff;}
a{color:#337ab7;text-decoration:none;}
a:hover,a:focus{color:#23527c;text-decoration:underline;}
a:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
h2{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
h2{margin-top:20px;margin-bottom:10px;}
h2{font-size:30px;}
.text-right{text-align:right;}
.text-uppercase{text-transform:uppercase;}
.text-danger{color:#a94442;}
address{margin-bottom:20px;font-style:normal;line-height:1.42857143;}
.row{margin-left:-15px;margin-right:-15px;}
.col-lg-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px;}
 
.col-lg-6{float:left;}
.col-lg-6{width:50%;}
 
table{background-color:transparent;}
th{text-align:left;}
.table{width:100%;max-width:100%;margin-bottom:20px;}
.table>thead>tr>th,.table>tbody>tr>th,.table>tbody>tr>td{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd;}
.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd;}
.table>thead:first-child>tr:first-child>th{border-top:0;}
.table-bordered{border:1px solid #ddd;}
.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tbody>tr>td{border:1px solid #ddd;}
.table-bordered>thead>tr>th{border-bottom-width:2px;}
.table>tbody>tr.active>td{background-color:#f5f5f5;}
.table>tbody>tr.success>td{background-color:#dff0d8;}
.table>tbody>tr.info>td{background-color:#d9edf7;}
.table>tbody>tr.danger>td{background-color:#f2dede;}
.table-responsive{overflow-x:auto;min-height:0.01%;}
@media screen and (max-width:767px){
.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;-ms-overflow-style:-ms-autohiding-scrollbar;border:1px solid #ddd;}
.table-responsive>.table{margin-bottom:0;}
.table-responsive>.table>thead>tr>th,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tbody>tr>td{white-space:nowrap;}
.table-responsive>.table-bordered{border:0;}
.table-responsive>.table-bordered>thead>tr>th:first-child,.table-responsive>.table-bordered>tbody>tr>td:first-child{border-left:0;}
.table-responsive>.table-bordered>thead>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>td:last-child{border-right:0;}
.table-responsive>.table-bordered>tbody>tr:last-child>td{border-bottom:0;}
}
.row:before,.row:after{content:" ";display:table;}
.row:after{clear:both;}
/*! CSS Used from: http://localhost/quickbook/resources/css/plugins.css */
.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.fa-exclamation-circle:before{content:"\f06a";}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media screen and (max-width: 800px) and (orientation: landscape),
@media only screen and (-Webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
/*! CSS Used from: http://localhost/quickbook/resources/css/main.css */
body{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;color:#394263;font-size:13px;background-color:#222222;}
#page-content{padding:10px 5px 1px;background-color:#eaedf1;}
.block{margin:0 0 10px;padding:20px 15px 1px;background-color:#ffffff;border:1px solid #dbe1e8;}
h2{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:300;}
h2{margin-bottom:15px;}
a,a:hover,a:focus{color:#1bbae1;}
.text-danger,.text-danger:hover{color:#e74c3c;}
strong{font-weight:600;}
.table.table-vcenter th,.table.table-vcenter td{vertical-align:middle;}
.table thead > tr > th{font-size:18px;font-weight:600;}
.table thead > tr > th{padding-top:14px;padding-bottom:14px;}
.table thead > tr > th,.table tbody > tr > th,.table tbody > tr > td,.table-bordered,.table-bordered > thead > tr > th,.table-bordered > tbody > tr > th,.table-bordered > tbody > tr > td{border-color:#eaedf1;}
.table{margin-bottom:20px;}
:focus{outline:0!important;}
@media screen and (min-width: 768px){
#page-content{padding:20px 20px 1px;}
.block{padding-left:20px;padding-right:20px;}
.block{margin-bottom:20px;}
}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media print{
#page-content{min-height:0!important;height:auto!important;padding:0!important;}
.block{border:none!important;padding:0!important;}
}
/*! CSS Used from: http://localhost/quickbook/resources/css/themes/night.css */
body{color:#333333;}
#page-content{background-color:#f2f2f2;}
.table thead > tr > th,.table tbody > tr > th,.table tbody > tr > td,.table-bordered,.table-bordered > thead > tr > th,.table-bordered > tbody > tr > th,.table-bordered > tbody > tr > td{border-color:#f2f2f2;}
.block{border-color:#e8e8e8;}
a,a:hover,a:focus{color:#888888;}
/*! CSS Used from: Embedded */
body{margin:0;}
strong{font-weight:bold;}
table{border-collapse:collapse;border-spacing:0;}
td,th{padding:0;}
@media print{
*,*:before,*:after{background:transparent!important;color:#000!important;-webkit-box-shadow:none!important;box-shadow:none!important;text-shadow:none!important;}
thead{display:table-header-group;}
tr{page-break-inside:avoid;}
h2{orphans:3;widows:3;}
h2{page-break-after:avoid;}
.table{border-collapse:collapse!important;}
.table td,.table th{background-color:#fff!important;}
.table-bordered th,.table-bordered td{border:1px solid #ddd!important;}
}
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff;}
h2{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
h2{margin-top:20px;margin-bottom:10px;}
h2{font-size:30px;}
.text-right{text-align:right;}
.text-uppercase{text-transform:uppercase;}
address{margin-bottom:20px;font-style:normal;line-height:1.42857143;}
.row{margin-left:-15px;margin-right:-15px;}
.col-lg-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px;}
@media (min-width:1200px){
.col-lg-6{float:left;}
.col-lg-6{width:50%;}
}
table{background-color:transparent;}
th{text-align:left;}
.table{width:100%;max-width:100%;margin-bottom:20px;}
.table>thead>tr>th,.table>tbody>tr>th,.table>tbody>tr>td{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd;}
.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd;}
.table>thead:first-child>tr:first-child>th{border-top:0;}
.table-bordered{border:1px solid #ddd;}
.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tbody>tr>td{border:1px solid #ddd;}
.table-bordered>thead>tr>th{border-bottom-width:2px;}
.table-responsive{overflow-x:auto;min-height:0.01%;}
 
.row:before,.row:after{content:" ";display:table;}
.row:after{clear:both;}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media screen and (max-width: 800px) and (orientation: landscape),
@media only screen and (-Webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
#page-content{padding:10px 5px 1px;background-color:#eaedf1;}
.block{margin:0 0 10px;padding:20px 15px 1px;background-color:#ffffff;border:1px solid #dbe1e8;}
h2{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:300;}
h2{margin-bottom:15px;}
strong{font-weight:600;}
 
@media screen and (min-width: 768px){
#page-content{padding:20px 20px 1px;}
.block{padding-left:20px;padding-right:20px;}
.block{margin-bottom:20px;}
}
@media only screen and (-webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
@media print{
#page-content{min-height:0!important;height:auto!important;padding:0!important;}
.block{border:none!important;padding:0!important;}
} </style>



<style>
body{
	    font-size: 14px; 
    color: #333;
}
.div2{
	width:46%; float:left; padding:10px;margin:0px;
}

.row{
	width:100%;
	clear:both;
		    font-size: 14px; 
    color: #333;margin:0px;padding:0px;
}



tr{background-color:#d9edf7;}

 th , td{
    padding : 6px 8px; 
}
 
  

</style>


<script>$(function(){ FormsWizard.init(); });</script>
    </body>
</html>