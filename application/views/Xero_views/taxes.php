<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<style>
    .dataTables_wrapper >.row >.col-sm-6.col-xs-7{
	position: absolute;
    z-index: 9;
    top: 9px;
    left: 60px;
    width: 220px !important;
}

.addNewBtnCustom{
    right: 0px !important;
}

@media screen and (max-width: 767px){
    .addNewBtnCustom {
	     left: 280px !important;
	     text-align: right;
	}
	.dataTables_wrapper > div:first-child {
	     height: 80px!important;
	}
}
   
</style>
<div id="page-content">
    
	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <legend class="leg">Taxes</legend> 
    <!-- All Orders Block -->
    <div class="block full" style="position: relative;">
	
        <!-- All Orders Title -->
            <div class="addNewBtnCustom">      
                    <a href="#add_tax" class="btn btn-sm btn-success"  onclick="set_addtax();" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Create Tax" >Add New</a>
                           
            </div>
        
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="tax_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Tax Name</th>
                    <th class="text-right ">Tax Rate (%)</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($taxes) && $taxes)
				{
					foreach($taxes as $tax)
					{
				?>
				<tr>
					
					<td class="text-left"><?php echo $tax['friendlyName']; ?></td>
					<td class="text-right"><?php echo number_format($tax['taxRate'], 2); ?></td>
					
					
				
				</tr>
				
				<?php } } 
				else { echo'<tr>
                <td colspan="2"> No Records Found </td>
                <td style="display:none;">  </td>
                
                </tr>'; }  
				?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>

<script>$(function(){ Pagination_view.init(); 
$('#tax_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'friendlyName': {
                        required: true,
                        minlength: 3,
                         
                    },
                    'taxRate': {
                        required: true,
                        
                    },
                   'taxComponent': {
                        required: true,
                        
                    },
        
                   
			},
    });    


});</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#tax_page').dataTable({
                columnDefs: [ { orderable: true, targets: [ 0 ] } ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

function set_del_tax(id){
	
	  $('#tax_id').val(id);
	
}

function set_addtax(id){
	
	$('#friendlyName').val(id);
	$('#taxRate').val(id);
	
          	
}	
function set_edit_tax(taxID){
     $('.edit_save').html('Save');
    $.ajax({
    url: '<?php echo base_url("Xero_controllers/Tax/get_tax_id")?>',
    type: 'POST',
	data:{taxID:taxID},
	dataType: 'json',
    success: function(data){
			var num = parseFloat(data.taxRate).toFixed(2); 
             $('#taxID').val(data.taxID);		
			 $('#friendlyName').val(data.friendlyName);
			 $('#taxRate').val(num);
			 $('#taxComponent').val(data.componentName);
			
	}	
});
	
}

 	
  

</script>


<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 
<!------    Add popup form    ------->

<div id="add_tax" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Tax</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="tax_form" class="form form-horizontal" action="<?php echo base_url(); ?>Xero_controllers/Tax/create_taxes">
			<input type="hidden" id="taxID" name="taxID" value=""  />
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Friendly Name</label>
							<div class="col-md-8">
								<input type="text" id="friendlyName"  name="friendlyName" class="form-control"  value="" placeholder="" data-args="Friendly Name"><?php echo form_error('friendlyName'); ?></div>
						</div>
						
						
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Tax Rate (%)</label>
							<div class="col-md-8">
								<input type="text" id="taxRate"  name="taxRate" class="form-control"  value="" placeholder="" data-args="Tax Rate"><?php echo form_error('taxRate'); ?></div>
						</div>
						
				
                	  <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Component Name</label>
							<div class="col-md-8">
								<input type="text" id="taxComponent"  name="taxComponent" class="form-control"  value="" placeholder="" data-args="Tax Component"><?php echo form_error('taxComponent'); ?></div>
						</div> 
						
                       </div>      
		      
			     <div class="form-group">
					<div class="col-md-12 text-right">
				    <div class="col-md-12">
					<button type="submit" class="submit btn btn-sm btn-success edit_save">Save</button>
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
					 </div>
                    </div>
                </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<!-- END Page Content -->

<!--------------------del admin plan------------------------>

<div id="del_tax" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Tax</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_tax" method="post" action='<?php echo base_url(); ?>Xero_controllers/Tax/delete_tax' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Tax?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="tax_id" name="tax_id" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>






