<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?>	</div>
  
    <!-- END Forms General Header -->
  <div class="row"><div class="col-md-12">
  <legend class="leg"> Capture / Void</legend>
	<div class="full">
        <!-- Form Validation Example Title -->
         <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs hidden-sm">Invoice</th>
                    <th class="text-right">Amount</th>
					
                    <th class="text-right hidden-xs">Date</th>
                    <th class="text-right hidden-xs">Type</th>
                     <th class="text-left hidden-xs hidden-sm">Transaction ID</th>
                     
                    <th class="hidden-xs text-right">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{  
				?>
				<tr>
				
					<td class="text-left"><?php echo $transaction['fullName']; ?></a></td>
				
					<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['invoiceID'])?$transaction['invoiceID']:'-----'; ?></td>
					<td class="text-right">$<?php echo ($transaction['transactionAmount'])?number_format($transaction['transactionAmount'], 2):'0.00'; ?></td>
					
				
					
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
						<td class="hidden-xs text-right"><?php 
					
					 
					 if($transaction['transactionType'] == 'auth'){
                            	echo 'NMI';
                            }
                           else if($transaction['transactionType'] == 'stripe_auth'){
                            	echo 'Stripe';
                            }
                            else if($transaction['transactionType'] == 'Paypal_auth'){
                            	echo 'Paypal';
                            }
                        else if($transaction['transactionType'] == 'pay_auth'){
                            	echo 'Paytrace';
                            }
                            else{
                            	echo 'Auth';
                            }
                            ?></td>
					
					<td class="text-left hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					
					<?php if( $transaction['transactionCode']=="100" || $transaction['transactionCode']=="111" || $transaction['transactionCode']=="200" || $transaction['transactionCode']=="1"){ ?>
					
					<td class="text-right hidden-xs"><?php echo "Success" ?></td>
					<?php } ?>
					
				   
					<td class="text-center">
					   <?php if($transaction['transactionCode']=='100'){ ?>
						<a href="#payment_capturemod" class="btn btn-sm btn-success"  onclick="set_capture_pay('<?php  echo $transaction['transactionID']; ?>','1' );"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Capture</a>
                        <?php if($transaction['transactionGateway'] != 5){ ?>
						  <a href="#payment_voidmod" class="btn btn-sm btn-danger"  onclick="set_void_pay('<?php  echo $transaction['transactionID']; ?>','1' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>
                        
					   <?php }
                        } if($transaction['transactionCode']=='1') {?>
					    <a href="#payment_capturemod" class="btn btn-sm btn-success"  onclick="set_capture_pay('<?php  echo $transaction['transactionID']; ?>','2' );"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Capture</a>
                        <?php if($transaction['transactionGateway'] != 5){ ?>
						<a href="#payment_voidmod" class="btn btn-sm btn-danger"  onclick="set_void_pay('<?php  echo $transaction['transactionID']; ?>','2' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>
                      
					   <?php }
                        }
					   if($transaction['transactionCode']=='200' && $transaction['transactionType']!='stripe_auth') {?>
					    <a href="#payment_capturemod" class="btn btn-sm btn-success"  onclick="set_capture_pay('<?php  echo $transaction['transactionID']; ?>','3' );"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Capture</a>
                        <?php if($transaction['transactionGateway'] != 5){ ?>
						<a href="#payment_voidmod" class="btn btn-sm btn-danger"  onclick="set_void_pay('<?php  echo $transaction['transactionID']; ?>','3' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>
                        
					   <?php } 
                        }
					    if($transaction['transactionCode']=='111') {?>
					    <a href="#payment_capturemod" class="btn btn-sm btn-success"  onclick="set_capture_pay('<?php  echo $transaction['transactionID']; ?>','4' );"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Capture</a>
                        <?php if($transaction['transactionGateway'] != 5){ ?>
						<a href="#payment_voidmod" class="btn btn-sm btn-danger"  onclick="set_void_pay('<?php  echo $transaction['transactionID']; ?>','4' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>
					   <?php }
                        } 
					    if($transaction['transactionCode']=='200' && $transaction['transactionType']=='stripe_auth') {?>
					    <a href="#payment_capturemod" class="btn btn-sm btn-success"  onclick="set_capture_pay('<?php  echo $transaction['transactionID']; ?>','5' );"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Capture</a>
                        <?php if($transaction['transactionGateway'] != 5){ ?>
						  <a href="#payment_refunds" class="btn btn-sm btn-danger"  onclick="set_refund_pay('<?php  echo $transaction['transactionID']; ?>','5' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a>
					   <?php } 
                        }
					   
					   ?>
					</td>
				</tr>
				
				<?php } } 
					else { echo'<tr><td colspan="8"> No Records Found </td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                <td style="display: none"></td>
                </tr>'; }  
				?>
				
			</tbody>
        </table>
    </div>
    </div>
    </div>
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
    <script>$(function(){ EcomOrders.init(); });</script>

    <script>
        $(function(){  nmiValidation.init(); });
        
	window.setTimeout("fadeMyDiv();", 2000);
	function fadeMyDiv() {
		   $(".msg_data").fadeOut('slow');
		}
		
	  	function set_capture_pay(txnid, txntype){
    	
    	
			
    		if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnID').val(txnid);	 
				 var url   = "<?php echo base_url()?>Xero_controllers/Payments/create_customer_capture";
				}	
			else if(txntype=='2'){
    		    $('#txnID1').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/create_customer_capture";
			}	
			 else if(txntype=='3'){
				$('#txnID2').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaytracePayment/create_customer_capture";
			 }
			 else if(txntype=='4'){
				$('#paypaltxnID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaypalPayment/create_customer_capture";
			 }
			 else if(txntype=='5'){
			    $('#strtxnID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/StripePayment/create_customer_capture";
			 }
			 
			 $("#data_form").attr("action",url);	
    		}
			
			
    				
    	}
    
    function set_void_pay(txnid, txntype){
    		
        
          if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnvoidID').val(txnid);	 
				 var url   = "<?php echo base_url()?>Xero_controllers/Payments/create_customer_void";
				}	
			else if(txntype=='2'){
    		    $('#txnvoidID1').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/create_customer_void";
			}	
			 else if(txntype=='3'){
				$('#txnvoidID2').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaytracePayment/create_customer_void";
			 }
			 else if(txntype=='4'){
				$('#paypaltxnvoidID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaypalPayment/create_customer_void";
			 }
			 else if(txntype=='5'){
			    $('#strtxnvoidID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/StripePayment/create_customer_void";
			 }
			 
			 $("#data_form11").attr("action",url);	
    		}
			
			
    	}
    </script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
 
	
    <div id="payment_capturemod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Capture Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>QBO_controllers/Payments/create_customer_capture' class="form-horizontal" >
                    	<p id="message_data">Do you wish to proceed with capture for this transaction? The payment will be captured if you click "Capture" below.
</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
                                <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
								 <input type="hidden" id="txnID1" name="txnID1" class="form-control"  value="" />
								 <input type="hidden" id="txnID2" name="txnID2" class="form-control"  value="" />
								 <input type="hidden" id="paypaltxnID" name="paypaltxnID" class="form-control"  value="" />
								 <input type="hidden" id="strtxnID" name="strtxnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Capture"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
<div id="payment_voidmod" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Void Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form11" method="post" action='<?php echo base_url(); ?>QBO_controllers/PaypalPayment/create_customer_void' class="form-horizontal" >
                         
                        <p id="message_data">Do you really want to void this transaction? The payment will be dropped if you click "Void" below.

</p> 
    					
    				    <div class="form-group">
                         
                            <div class="col-md-8">
								  <input type="hidden" id="txnvoidID" name="txnvoidID" class="form-control"  value="" />
								  <input type="hidden" id="txnvoidID1" name="txnvoidID1" class="form-control"  value="" />
								  <input type="hidden" id="txnvoidID2" name="txnvoidID2" class="form-control"  value="" />
								  <input type="hidden" id="paypaltxnvoidID" name="paypaltxnvoidID" class="form-control"  value="" />
								  <input type="hidden" id="strtxnvoidID"    name="strtxnvoidID" class="form-control"  value="" />
								    
                            </div>
                        </div>
                        
    					<div class="pull-right">
            			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                
    			    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
	<div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Void Authorized Transaction</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>Xero_controllers/StripePayment/create_customer_refund' class="form-horizontal" >
                       <p id="message_data">Do you really want to void this transaction? The payment will be dropped if you click "Void" below.</p>
    					<div class="form-group">
                            <div class="col-md-8">
                                <input type="hidden" id="txnred" name="txnred" class="form-control"  value="" />
							    <input type="hidden" id="txnstrID" name="txnstrID" class="form-control"  value="" />
                                <input type="hidden" id="txnpaypalID" name="txnpaypalID" class="form-control"  value="" />
								 <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
								 <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
								 <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                       <div class="pull-right">
            			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Void"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
		<script>
	    	function set_refund_pay(txnid, txntype){
    	
			
    		if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnID').val(txnid);	 
				 var url   = "<?php echo base_url()?>Xero_controllers/Payments/create_customer_refund";
				}	
			else if(txntype=='2'){
    		    $('#txnIDrefund').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/AuthPayment/create_customer_refund";
			}	
			 else if(txntype=='3'){
				$('#paytxnID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaytracePayment/create_customer_refund";
			 }
			 else if(txntype=='4'){
				$('#txnpaypalID').val(txnid);
				var url   = "<?php echo base_url()?>Xero_controllers/PaypalPayment/create_customer_refund";
			 }
			 else if(txntype=='5'){
			    $('#txnstrID').val(txnid);
			    $('#txnred').val('void');
				var url   = "<?php echo base_url()?>Xero_controllers/StripePayment/create_customer_refund";
			 }
			 
			 $("#data_form").attr("action",url);	
    		}
    	}  
	</script>
	
	
	


</div>
