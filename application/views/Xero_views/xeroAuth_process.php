   <!-- Page content -->
   <?php
	$this->load->view('alert');
?>

	<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
	<div class="header-section">
		<h1>
		<i class="gi gi-settings"> </i> <?php echo "Xero Authentication";?> 
		</h1>
	</div>
	</div>
	
      <div class="msg_data "><?php echo $this->session->flashdata('message');   ?>	</div>
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	            
        <!-- Progress Bar Wizard Content -->
        <div class="row">
	
		<div> <h3>Click on the button below to start "Authentication Process"</h3></div><br><br>
				<a href="?authenticate=1" class="btn btn-lg btn-success">Authenticate</a>
				<br><br>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
   
</div>
 <script>

    
window.setTimeout("fadeMyDiv();", 2000);
	function fadeMyDiv() {
		   $(".msg_data").fadeOut('slow');
		}
		
		 </script>