

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<style type="text/css">
  .action_option{
    width: 150px;
    position: absolute;
    top: 9px;
    left: 230px;
    z-index: 1;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">

<div id="page-content">
    <!-- eCommerce Orders Header -->
        <?php echo $this->session->flashdata('message');   ?>
    <!-- END eCommerce Orders Header -->
	<div class="row invoice_quick">
    <div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-success rounded-circle mr-2">
                    <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup">
                            <?php echo $in_details->Paid; ?></span></h4>
                        <p class="text-truncate">Paid Invoices</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-info rounded-circle mr-2">
                    <i class="fa fa-credit-card ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold">
                          <span data-plugin="counterup">
                            <?php echo  $in_details->schedule;  ?>
                          </span>
                        </h4>
                        <p class="text-truncate">Scheduled Payments</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-purple rounded-circle mr-2">
                    <i class="fa fa-calendar ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold">
                          <span data-plugin="counterup">
                            <?php echo  $in_details->incount; ?>
                          </span>
                        </h4>
                        <p class="text-truncate">Total Invoices</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div>    
    <div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-primary rounded-circle mr-2">
                    <i class="fa fa-thumbs-o-down ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold">
                          <span data-plugin="counterup">
                            <?php echo  $in_details->Failed; ?>
                          </span>
                        </h4>
                        <p class="text-truncate">Failed Invoices</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div> 
	</div>
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <legend class="leg"> Invoices </legend>
  <div class="full">

    <!-- All Orders Title -->
    <div style="position: relative">
             <?php if($this->session->userdata('logged_in')){ ?>
              
                       <a href="<?php echo base_url(); ?>Xero_controllers/Xero_invoice/add_invoice" class="btn pull-lft  btn-sm btn-success subs-btn list-add-btn" title="Create New"  >Add New</a>
                     
             
                <?php } ?>
       
        <!-- END All Orders Title -->
        <!-- END All Orders Title -->
        <form class="filter_form" action="<?php echo base_url();?>home/invoices" method="post">
                <select class="form-control status_filter" name="status_filter">
                  <option value="open" <?php if($filter == 'open'){ echo 'Selected';}?>>Open</option>
                  <option value="Past Due" <?php if($filter == 'Past Due'){ echo 'Selected';}?>>Past Due</option>
                  <option value="Failed" <?php if($filter == 'Failed'){ echo 'Selected';}?>>Failed</option>
                  <option value="Paid" <?php if($filter == 'Paid'){ echo 'Selected';}?>>Paid</option>
            <option value="Cancelled" <?php if($filter == 'Cancelled'){ echo 'Selected';}?>>Cancelled</option>
            <option value="All" <?php if($filter == 'All'){ echo 'Selected';}?>>All</option>
          </select>
        </form>
        <?php
        if ($plantype == '' || $plantype == null) { 
          if($filter == 'open' || $filter == 'Past Due' || $filter == 'Failed' || $filter == null){ ?>
          <div class="action_option" >
            <select class="form-control action_filter" id="action_filter" name="action_filter">
              <option style="display: none;" value="" >Batch Actions</option>
              <option value="1" >Process Selected</option>
              <option value="2" >Email Selected</option>
            </select>
          </div>
        <?php }
        } ?>  
        <!-- All Orders Content -->
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left visible-lg">Customer Name</th>
                    <th class="text-right">Invoice</th>
                    <th class="hidden-xs text-right">Due Date</th>
                    <th class="text-right hidden-xs">Payment</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-right">Status</th>                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices)){

						foreach($invoices as $invoice){
						     
						    
			   ?>
			
				<tr>
          <td class="text-left visible-lg"> <?php echo ($invoice['CustomerFullName'])?($invoice['CustomerFullName']):'-----'; ?></td>
          
          	<?php if ($plantype_vs) { ?>
            	<td class="text-right cust_view"> <?php echo $invoice['refNumber']; ?></td>
          
            <?php  } else { if($plantype_as){ ?>?>
                <td class="text-right cust_view"> <a href="<?php echo base_url();?>Xero_controllers/Xero_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']; ?></a></td>
            <?php }else ?>
                <td class="text-right cust_view"> <a href="<?php echo base_url();?>Xero_controllers/Xero_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']; ?></a></td>
            
          	<?php } }?>
					<td class="text-right hidden-xs"> <?php echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
					
				<?php if($invoice['Total_payment']!="0.00"){ ?>
                            <td class="hidden-xs text-right cust_view"><a href="#pay_data_process"   onclick="set_xero_payment_data('<?php  echo $invoice['invoiceID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$'.number_format(($invoice['Total_payment']),2); ?></a></td>
						   <?php }
						   else{ ?>
							<td class="hidden-xs text-right cust_view"><a href="#"><?php  echo  '$'.number_format(($invoice['Total_payment']),2); ?></a></td>   
						   <?php } ?>
					
					
					<td class="text-right hidden-xs">$<?php echo ($invoice['BalanceRemaining'])?number_format($invoice['BalanceRemaining'], 2):'0.00'; ?></td>
					<td class="text-right hidden-xs"> 
                       <?php echo $invoice['UserStatus']; ?></td>
                    </td>
					
				<td class="text-center">
						<div class="">
                            
                             <?php if($invoice['BalanceRemaining'] == 0){ ?>
                                <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
                                <li><a href="#payment_refunds" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
                                <li><a href="#qbo_invoice_delete" onclick="get_invoice_id('<?php  echo $invoice['refNumber']; ?>','<?php  echo $invoice['UserStatus']; ?>')" data-keyboard="false" data-toggle="modal" class="">Void</a> </li>
                                </ul>
                               </div>
                             <?php }else{ ?>
                                <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
                            <li><a href="#xero_invoice_process" class=""  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_xero_invoice_process_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo
                             $invoice['CustomerListID']; ?>', '<?php echo $invoice['BalanceRemaining']; ?>');">Process</a></li>
                             <li><a href="#qbo_invoice_schedule" onclick="set_invoice_schedule_date_id('<?php  echo $invoice['refNumber']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                             <li><a href="#set_subs" class=""  onclick="set_sub_status_id('<?php  echo $invoice['invoiceID']; ?>','<?php echo $invoice['BalanceRemaining']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>
                              </ul>
                             </div>
                             <?php } ?>
							    	
						</div>
					</td>
				</tr>
				
				<?php 
				  }
		
				?>		
				
			</tbody>
        </table>
    </div>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
</div>
<!-- END Page Content -->

<!--- ******************************************** Process for Payment ************************************** ---->
<div id="xero_invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Payments/pay_invoice' class="form-horizontal" autocomplete="off" >
                                    <?php 
                                        if(!isset($defaultGateway) || !$defaultGateway){
                                    ?>
                                    <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_xero_url();"  class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if(isset($gateway_datas) && !empty($gateway_datas) ){
                                                                foreach($gateway_datas as $gateway_data){
                                                                ?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            <?php 
                                                } else { ?>
                                                <input type="hidden" name="gateway" value="<?php echo $defaultGateway['gatewayID'];  ?>">
                                            <?php }  ?>	
                                      <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
                                                
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
                                                        
                                                </div>
                                            </div>
                                             <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>    
                       <div id="card_div"></div>
                    
                   
                    
                    
                    
                    <div class="pull-right">
                        <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">
                        <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="qbo_invoice_schedule" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Schedule Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="Qbwc-form_schedule" method="post" action="<?php echo base_url();?>Xero_controllers/Xero_invoice/invoice_schedule" class="form-horizontal">                     
                 
                    <p>Do you really want to schedule this invoice? Please schedule date to automatically process payment.</p>                     
                    <div class="form-group">
                     <label class="col-md-4 control-label" for="card_list">Schedule Date</label>
                        <div class="col-md-8">
                      <div class="input-group input-date">
                            <input type="text" id="schedule_date" name="schedule_date" class="form-control input-datepicker " data-date-format="mm/dd/yyyy" placeholder="" data-args="Schedule Date" value="">
                        </div>
                      </div>    
                    </div>                   
                    
                    <div class="form-group">                     
                        <div class="col-md-8">
                            <input type="hidden" id="scheduleID" name="scheduleID" class="form-control" value="1-1501156111">
                        </div>
                    </div>       
                    
             
                    <div class="pull-right">
                     <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Schedule">
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br>
                    <br>
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="qbo_invoice_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Void Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="Qbwc-form" method="post" action='<?php echo base_url()?>Xero_controllers/Xero_invoice/delete_invoice' class="form-horizontal" >
                     
                 
                    <p>Do you really want to void this Invoice?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invID" name="invID" class="form-control"  value="" />
                            <input type="hidden" id="invSTATUS" name="invSTATUS" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Void"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="set_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Mark Invoice as Paid</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>Xero_controllers/SettingSubscription/status' class="form-horizontal" >
                     
                 
					 <div class="form-group">
                     <label class="col-md-4 control-label">Date</label>
                        <div class="col-md-8">
                      <div class="input-group input-date">
                            <input type="text" id="payment_date" name="payment_date" class="form-control input-datepicker " data-date-format="yyyy/mm/dd" placeholder="" data-args="yyyy/mm/dd" value="">
                        </div>
                      </div>    
                    </div>
                    
                     <div class="form-group">
                     <label class="col-md-4 control-label">Check Number</label>
                        <div class="col-md-8">
                      <div class="input-group">
                            <input type="text" id="check_number" placeholder="" data-args="Check Number" size="40" name="check_number" class="form-control">
                        </div>
                      </div>    
                    </div>
                    
                    <div class="form-group">
                     <label class="col-md-4 control-label" >Amount</label>
                        <div class="col-md-8">
                      <div class="input-group">
                            <input type="text" id="inv_amount" placeholder="" data-args="Amount" size="40" name="inv_amount" class="form-control">
                        </div>
                      </div>    
                    </div>
                    
                 
                    
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="subscID" name="subscID" class="form-control"  value="" />
							<input type="hidden" id="status" name="status" class="form-control"  value="Paid" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" class="btn btn-sm btn-success" value="Add Payment"  />
                    <button type="button" class="btn btn-sm btn-primary1 " data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="bulkProcessInvoice" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header text-center">
        <h2 class="modal-title">Process Invoices</h2>
      </div>
      <!-- END Modal Header -->

      <!-- Modal Body -->
      <div class="modal-body">

        

        <form id="process_invoice_form" method="post" action='<?php echo base_url(); ?>CommanGateway/processMultipleInvoices' class="form-horizontal">
          <p id="message_data">Are you sure you want to Process the selected Invoices?</p>
          <input type="hidden" name="processInvoiceIDS" id="processInvoiceIDS" value="">
          <input type="hidden" name="integrationMerchantType" id="integrationMerchantType" value="4">

          <input type="hidden" name="electronic_check_gateway" id="echeckMerchantGatewayID" value="0">
          <input type="hidden" name="credit_card_gateway" id="creditCardMerchantGatewayID" value="0">

          <div class="pull-right">
            <input type="submit" id="process_btn" name="btn_process" class="btn btn-sm btn-success" value="Process" />
            <button type="button" id="process_btn_cancel" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
          </div>
          <br />
          <br />
        </form>

      </div>
      <!-- END Modal Body -->
    </div>
  </div>
</div>
<div id="bulkProcessGatewayInvoice" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header text-center">
        <h2 class="modal-title">Process Invoices</h2>
      </div>
      <!-- END Modal Header -->

      <!-- Modal Body -->
      <div class="modal-body">

        

        <form id="process_invoice_gateway_form" method="post" action='<?php echo base_url(); ?>CommanGateway/processMultipleInvoices' class="form-horizontal">
          <!--  -->
          <input type="hidden" name="processInvoiceIDS" id="processGatewayInvoiceIDS" value="">
          <input type="hidden" name="integrationMerchantType" id="integrationMerchantType" value="4">

          

          <div id="gateway_data">
            <div class="form-group">
              <label class="col-md-4 control-label" for="credit_card_gateway">Credit Card</label>
              <div class="col-md-6">
                <select id="credit_card_gateway" name="credit_card_gateway" class="form-control">
                    <option value="">Select Gateway</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="electronic_check_gateway">Electronic Check</label>
              <div class="col-md-6">
                <select id="electronic_check_gateway" name="electronic_check_gateway" class="form-control">
                    <option value="">Select Gateway</option>
                </select>
              </div>
            </div>
          </div>

          <div class="pull-right">
            <input type="submit" id="process_btn" name="btn_process" class="btn btn-sm btn-success" value="Process" />
            <button type="button" id="process_btn_cancel" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
          </div>
          <br />
          <br />
        </form>

      </div>
      <!-- END Modal Body -->
    </div>
  </div>
</div>
<div id="set_tempemail_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->

            <div class="modal-header ">
                <h2 class="modal-title text-center">Email Invoices</h2>
            </div>

            <div class="modal-body">
                <div id="data_form_template">
                    <label class="label-control" id="template_name"> </label>

                    <form id="email-validation" action="<?php echo base_url(); ?>CommanGateway/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="emailProcessInvoiceIDS" id="emailProcessInvoiceIDS" value="">
                        <input type="hidden" name="integrationType" id="integrationType" value="3">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="type">Template</label>
                            <div class="col-md-7">
                                
                                <select class="template_select form-control" name="templateID" id="templateID" >
                                  <?php 
                                  $templatedata = [];
                                  if(isset($templateList)){ 
                                    $inc = 0;
                                    foreach ($templateList as $value) {
                                      if($inc == 0){
                                         $templatedata = $value;
                                      }
                                      echo '<option value="'.$value['templateID'].'">'.$value['templateName'].'</option>';
                                      $inc++;
                                    }
                                  } ?>
                                  
                                </select>

                            </div>
                        </div>

                        <div class="form-group" style="display:none;" id="reply_div">
+                           <label class="col-md-3 control-label" for="replyEmail">Reply-To Email</label>
                            <div class="col-md-7">
                                <input type="text" id="replyEmail" name="replyEmail" class="form-control" value="<?php if (isset($templatedata) && $templatedata['replyTo'] != ''){
                                            echo $templatedata['replyTo'];
                                        }else{  echo $merchantEmail;} ?>" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group" style="display:none" id="from_email_div">
                            <label class="col-md-3 control-label" for="templteName">From Email</label>
                            <div class="col-md-7">
                                <input type="text" id="fromEmail" name="fromEmail"  value="<?php  if(isset($from_mail) && $from_mail != '') echo $from_mail ? $from_mail : 'donotreply@payportal.com'; ?>" class="form-control" placeholder="From Email">
                            </div>
                        </div>
                        <div class="form-group" id='display_name_div' style='display:none'>
                            <label class="col-md-3 control-label" for="templteName">Display Name</label>
                            <div class="col-md-7">
                                <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="<?php echo $mailDisplayName; ?>" placeholder="Display Name">
                            </div>
                        </div>

                        <div class="form-group"  style="font-size: 12px;">
                            <label class="col-md-3 control-label" for="templteName"></label>
                            <div class="col-md-7">
                                <a href="javascript:void(0);" id="open_reply">Add Reply-To<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);" id="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                              
                            </div>
                        </div>

                        <div class="form-group" id="cc_div" style="display:none">
                            <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                            <div class="col-md-7">
                                <input type="text" id="ccEmail" name="ccEmail" value="<?php if (isset($templatedata)) echo ($templatedata['addCC']) ? $templatedata['addCC'] : ''; ?>" class="form-control" placeholder="CC Email">
                            </div>
                        </div>
                        <div class="form-group" id="bcc_div" style="display:none">
                            <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                            <div class="col-md-7">
                                <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if (isset($templatedata)) echo ($templatedata['addBCC']) ? $templatedata['addBCC'] : ''; ?>" placeholder="BCC Email">
                            </div>
                        </div>
                        

                        <div class="form-group">

                            <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                            <div class="col-md-7">

                                <input type="text" id="emailSubject" name="emailSubject" value="<?php if (isset($templatedata)) echo ($templatedata['emailSubject']) ? $templatedata['emailSubject'] : ''; ?>" class="form-control" placeholder="Email Subject">
                            </div>

                        </div>
                        <div class="form-group ">
                            <label class="col-md-3 control-label" >Attach Invoice PDF</label>
                            <div class="col-md-7">
                                <label class="switch switch-info"><input type="checkbox" name="add_attachment"<?php  echo "checked";  ?> id="add_attachment"><span></span></label>
                            </div>
                        </div>

                        <div class="form-group">


                            <label class="col-md-3 control-label">Email Body</label>
                            <div class="col-md-7">
                                <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if (isset($templatedata)) echo ($templatedata['message']) ? $templatedata['message'] : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>


                            </div>
                        </div>
                    </form>


                </div>


            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/customer_details_xero.js" ></script>
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

  var serachString = '<?php echo isset($serachString) ? $serachString : '';?>';

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                "processing": true, //Feature control the processing indicator.
                "language": {
                      "processing": "<div class='tableLoader'><i class='fa fa-refresh fa-spin'></i>" 
                  },
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [6] }
                ],
                order: [[3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');

            $("#search").on('keyup', function (){
              $('#invoice_page').dataTable().search( this.value ).draw();
            });

            $('#invoice_page').dataTable().search( serachString ).draw();
        }
    };
}();



</script>

<script type="text/javascript">
$('#action_filter').change(function(){
    var value = $(this).val();

    if(value == 1){
        if($('#processInvoiceIDS').val() == ''){
          alert('Please select at least one invoice.');
          $('#action_filter').val('');
        }else{

          $.ajax({

              type: "POST",
              url: '<?php echo base_url() ?>ajaxRequest/getMerchantDefaultGatewaySupport',
              data: {
              },
              success: function(data) {
                var data = $.parseJSON(data);
                if (!jQuery.isEmptyObject(data)) {
                  if(data.status == 'success'){
                    if(data.data.creditCard == 1 || data.data.echeckStatus == 1){
                      $('#echeckMerchantGatewayID').val(data.data.gatewayID);
                      $('#creditCardMerchantGatewayID').val(data.data.gatewayID);
                      $('#bulkProcessInvoice').modal('show');
                    }else{
                      var creditCardHtml = '<option value="">Select Gateway</option>';
                      var eCheckHtml = '<option value="">Select Gateway</option>';

                      $.each( data.creditCardData, function( key, value ) {
                        if(value.gatewayID == data.data.gatewayID){
                           creditCardHtml += '<option selected value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                        }else{
                             creditCardHtml += '<option value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                        }
                       
                      });

                      $.each( data.eCheckData, function( key, value ) {
                        if(value.gatewayID == data.data.gatewayID){
                           eCheckHtml += '<option selected value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                        }else{
                             eCheckHtml += '<option value="'+value.gatewayID+'">'+value.gatewayFriendlyName+'</option>';
                        }
                       
                      });

                      $('#credit_card_gateway').html(creditCardHtml);
                      $('#electronic_check_gateway').html(eCheckHtml);

                      $('#bulkProcessGatewayInvoice').modal('show');
                    }
                  }
                }
              }
            });


          
        }
        
    }else if(value == 2){
        
        if($('#processInvoiceIDS').val() == ''){
          alert('Please select at least one invoice.');
          $('#action_filter').val('');
        }else{
          $('#set_tempemail_data').modal('show');
        }
    }else{
          
    }

    
  });
function set_sub_status_id(sID,amount){
	
	  $('#subscID').val(sID);
	  $('#inv_amount').val(amount);
	
}


window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }

function get_invoice_id(inv_id,invSTATUS){
    $('#invID').val(inv_id);
    $('#invSTATUS').val(invSTATUS);
}

function set_invoice_schedule_date_id(id, ind_date){
    $('#scheduleID').val(id);
    $('#schedule_date').val('');

    var nowDate = new Date(ind_date);
   
      var inv_day = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
     
            $("#schedule_date").datepicker({ 
              format: 'dd-mm-yyyy',
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
    
}      
</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
<script>
    	

function set_xero_invoice_process_id(invoice, rid){
	
	     $('#invoice').val(invoice);
	
	
		 $('#invoiceProcessID').val(invoice);  
		  
		if(rid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Xero_controllers/Xero_invoice/check_vault",
				data : {'rid':rid,'invoice':invoice},
				error:function(error){
                				},
				success : function(response){
					
					     data=$.parseJSON(response);
						 
				  
						
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
							
							  var card1 = data['card'];
							  $(s).append('<option value="new1">New Card</option>'); 
							   
							    for(var val in  card1) {
									if(card1[val]['friendlyName'])
								  $("<option />", {value: card1[val]['cardResellerID'], text: card1[val]['friendlyName'] }).appendTo(s);
							    }
													var d = new Date();
													var i;
													var opt;
													var cruy = d.getFullYear();
						  	                     		var dyear = cruy + 25;
													for(i = cruy; i < dyear ;i++ ){  
                                                      opt+='<option value='+i+'>'+i+'</option>';
												 
													
							var card_daata='<fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="" data-args="Credit Card Number"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="" data-args="Security Code (CVV)" /></div></div> <div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div><div id="frdname" class="form-group"></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="'+data['address1']+'" placeholder="" data-args="Address Line 1"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="'+data['address2']+'" placeholder="" data-args="Address Line 2"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="'+data['City']+'" placeholder="" data-args="City"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="'+data['State']+'" placeholder="" data-args="State/Province"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="'+data['zipCode']+'" placeholder="" data-args="ZIP Code"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="'+data['Country']+'" placeholder="" data-args="Country"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="'+data['phoneNumber']+'" placeholder="" data-args="Phone Number"></div></div></fieldset>';
                   
                       $('#card_div').html(card_daata);
					  
					   }				   
				   }
				}
				
				
			});
			
		}	
		 
		 
}	

 
	$('#process_invoice_gateway_form').validate({

    rules: {
      'credit_card_gateway': {
        required: true,
      },
      'electronic_check_gateway': {
        required: false,
      },

    },
    submitHandler: function(form) {
      $("#submit_btn").attr("disabled", true);
      $('.preloader.themed-background-pre').show();
      // do other things for a valid form
      form.submit();
    }
  });
	

</script>
