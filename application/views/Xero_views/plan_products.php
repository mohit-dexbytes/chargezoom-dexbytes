
<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    
    <div class="msg_data">
          <?php echo $this->session->flashdata('message');   ?>
    </div>
    <!-- All Orders Block -->
    <div class="block-main full" style="position: relative;">
         
        <!-- All Orders Title -->
        <div class="addNewBtnCustom">
            <h2><strong>Products & Services</strong></h2>
             <?php if($this->session->userdata('logged_in')){ ?>
            
                           
                   <a class="btn btn-sm  btn-success" title="Create New" href="<?php echo base_url(); ?>Xero_controllers/Xero_plan_product/create_product_services">Add New</a>
                 
           
            <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="product" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr> 
                   <th class= "text-left">Item Code</th>
                    <th class= "text-left">Item Name</th>
                      <th class="text-left hidden-xs">Description</th>
                  
                    <th class="text-right hidden-xs">Sale Cost</th>
                    <th class=" text-center" style="width:85px !important;">Action</th>
                   
                </tr>
            </thead>
            <tbody>
            
                <?php 
                if(isset($plans) && $plans)
                {
                    foreach($plans as $plan)
                    {
                ?>
                <tr>
                     <td class="text-left"><?php echo $plan['Code']; ?></a></td>
                    <td class="text-left"><?php echo $plan['Name']; ?></a></td>
                    <td class="text-left hidden-xs"><?php echo $plan['SalesDescription']; ?></a></td>
                  
                     
                    <td class="hidden-xs text-right">$<?php echo number_format($plan['saleCost'],2); ?></td>
                
                     <td class="text-center">                       
                    <div class="btn-group btn-group-xs">    
                      <?php if($plan['IsActive']=='active'){  ?>
                         <a href="<?php echo base_url('Xero_controllers/Xero_plan_product/create_product_services/'.$plan['productID']); ?>" data-toggle="tooltip" title="Edit Product" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        
                        <a href="#plan_product" class="btn btn-sm btn-default" onclick="view_plan_details('<?php echo $plan['productID']; ?>');"  data-backdrop="static" data-keyboard="false" title="View" data-toggle="modal"><i class="fa fa-eye"></i></a>
                            
                            
                        <a href="#del_plan_product" onclick="set_del_plan_product('<?php  echo $plan['productID']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Deactivate" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
                        
                       <?php } if($plan['IsActive']=='false'){ ?>  
                          <a  title="Active Product"  data-backdrop="static" data-keyboard="false" data-toggle="modal"  class="btn btn-info"><i class="fa fa-check"></i></a>
                       <?php } ?>   
                    </div>
                    </td>
                
                
                    
                </tr>
                
                <?php } }
                else { echo'<tr>
                <td colspan="6"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; }  
                ?>
                
            </tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<div id="plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            
            <div class="modal-header text-center">
                <h2 class="modal-title">Products / Services</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_product"  style="height: 300px; min-height:300px;  overflow: auto; " >
        
            </div>
            <hr>
                <div class="pull-right">
                
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />                  
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
 
 function view_plan_details(productID){
     
    
     if(productID!=""){
         
       $.ajax({
          type:"POST",
          url : '<?php echo base_url(); ?>Xero_controllers/Xero_plan_product/plan_product_details',
          data : {'productID':productID},
          success: function(data){
              
                 $('#data_form_product').html(data);
              
              
          }
       });     
         
    
     
     } 
 }


</script>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#product').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [5] }
                ],
                order: [[1, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_plan_product(id){
    
      $('#productID').val(id);
    
}

window.setTimeout("fadeMyDiv();", 2000);
function fadeMyDiv() {
		   $(".msg_data").fadeOut('slow');
		}
  
</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>

</div>

<div id="del_plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Deactivate Product / Service</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_plan_product" method="post" action='<?php echo base_url(); ?>Xero_controllers/Xero_plan_product/det_plan_product' class="form-horizontal" >
                     
                 
                    <p>Do you really want to deactivate this Product / Service?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="productID" name="productID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- END Page Content -->