

<style>
.recieptPage label{
    font-weight: 800 !important;
}
.recieptPage .row{
    padding-bottom: 10px !important;
}
.recieptPage .first-row{
    padding-bottom: 0px !important;
}
.recieptPage .success{
    color: #3FBF3F;
}
.recieptPage .faild{
    color: #cf4436;
}
.recieptPage a{
    color: #167bc4;
}
.recieptPage hr{
    margin: 10px;
}
.recieptPage p {
    margin-bottom: 0px !important;
    color: #000;
}
.transaction-print-btn{
    font-size: 13px !important;
}
@media print {
   #DivIdToPrint {
        font-size: 11pt;     
       font-family: Consolas;
       padding: 0px;
       margin: 0px;
    }
}
</style>
<?php 

    $transactionAmount = isset($transactionAmount) && ($transactionAmount != null)?number_format($transactionAmount, 2):number_format(0,2);
    $surchargeAmount = isset($surchargeAmount) && ($surchargeAmount != null)?number_format($surchargeAmount, 2):number_format(0, 2);

    $totalAmount = isset($totalAmount) && ($totalAmount != null)?number_format($totalAmount, 2):number_format(0, 2);

    $isSurcharge = isset($isSurcharge) && ($isSurcharge != null)?$isSurcharge:0;
?>
<div id="page-content" class="recieptPage">
 
    <!-- Products Block -->
        <div class="block">
        <!-- Products Title -->
            <div class="block-title">
                <h2><?php echo $customer_data['sub_header'];?></h2>
            </div>
            <div class="row first-row">
                <div class="col-md-6">
                    <h4><strong>Transaction Receipt</strong></h4>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-sm btn-info pull-right" href="<?php echo base_url().$customer_data['proccess_url'];?>"><?php echo $customer_data['proccess_btn_text'];?></a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <?php if($this->session->flashdata('success') != null){
                        ?><h5><strong class="success">Transaction Successful</strong></h5>
                    <?php }else{ 
                         $message = strip_tags($this->session->flashdata('message'));
                            if(isset($customer_data['checkPlan']) && !$customer_data['checkPlan']){
                                $message = "Transaction Failed - Transaction limit has been reached. <a href= '".base_url()."QBO_controllers/home/my_account'>Click here</a> to upgrade.";
                            }
                         ?>
 
                         <h5><strong class="faild"><?php echo $message;?></strong></h5>
                 <?php   } ?>
                    
                </div>
            </div> 
            <div class="row"> 
                <div class="col-md-4">
                     <?php 
                        $date = date("Y-m-d H:i:s");
                            if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                                $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                                $date = getTimeBySelectedTimezone($timezone);
                            }
                         ?>
                        <label>Date: </label> <strong><?php echo date("m/d/Y h:i A", strtotime($date)); ?></strong>
                </div>
                <div class="col-md-4">
                    <label>Transaction ID:  </label> <span> <?php echo $customer_data['transaction_id'];?></span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <label>IP Address: </label><span> <?php echo $Ip;?></span>
                </div>
                <div class="col-md-8">
                    <label>User: </label><span> <?php echo $name.' ('.$email.')';?></span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-4">
                    <label>Invoice(s): </label>
                    <?php foreach($invoice_data as $invoice){?>
                        <?php if($this->session->userdata('vt_plan') == '0'){?>
                            <span><a href="<?php echo base_url()?>QBO_controllers/Create_invoice/invoice_details_page/<?php echo $invoice['invoiceID'];?>"><?php echo $invoice['refNumber'];?></a></span>,
                        <?php } else { ?>
                        <span><?php echo $invoice['refNumber'];?></span>,
                        <?php }
                    } ?>
                    
                </div>
                <div class="col-md-8">
                    <label>Amount: </label><span> $<?php echo $transactionAmount;?></span>
                </div>
            </div>
            <?php if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-8">
                    
                    <label>Surcharge Amount: </label><span> $<?php echo $surchargeAmount;?></span><br>

                </div>
            </div>
            <?php }
            if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-8">
                    
                     <label>Total Amount: </label><span> $<?php echo $totalAmount;?></span>

                </div>
            </div>
            <?php } ?>
        <?php  
            $BillingAdd = 0;
            $ShippingAdd = 0;

            $isAdd = 0;
            if($customer_data['billing_address1']  || $customer_data['billing_address2'] || $customer_data['billing_city'] || $customer_data['billing_state'] || $customer_data['billing_zip'] || $customer_data['billing_country']){
                $BillingAdd = 1;
                $isAdd = 1;
            }
            if($customer_data['shipping_address1']  || $customer_data['shipping_address2'] || $customer_data['shipping_city'] || $customer_data['shipping_state'] || $customer_data['shipping_zip'] || $customer_data['shiping_counry'] ){
                $ShippingAdd = 1;
                $isAdd = 1;
            }

        ?>  
        <!-- Addresses -->
        <div class="row">
        <?php if($isAdd){ ?>
            <?php if($BillingAdd){ ?>
            <div class="col-sm-4">
                <!-- Billing Address Block -->
                <div class="block">
                    <!-- Billing Address Title -->
                    <div class="block-title">
                        <h2>Billing Address</h2>
                    </div>
                    <h4><span>
                            <?php if ($customer_data['billing_name'] != '') {
                                echo $customer_data['billing_name'] ;
                            } 
                            ?>
                        </span></h4>
                    <address>
                        

                        <?php if ($customer_data['billing_address1'] != '') {
                            echo $customer_data['billing_address1'].'<br>'; } ?> 
                            
                            <?php if ($customer_data['billing_address2'] != '') {
                              echo $customer_data['billing_address2'].'<br>'; } else{
                                  echo '';
                              } ?>
                        
                        <?php echo ($customer_data['billing_city']) ? $customer_data['billing_city'] . ',' : ''; ?>
                        <?php echo ($customer_data['billing_state']) ? $customer_data['billing_state'] : ''; ?>
                        <?php echo ($customer_data['billing_zip']) ? $customer_data['billing_zip'].'<br>' : ''; ?> 
                        <?php echo ($customer_data['billing_country']) ? $customer_data['billing_country'].'<br>' : ''; ?> 
                         <br>
                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Billing Address Content -->
                </div>
                <!-- END Billing Address Block -->
            </div>
            <?php }
            if($ShippingAdd){ ?>
            <div class="col-sm-4">
                <!-- Shipping Address Block -->
                <div class="block">
                    <!-- Shipping Address Title -->
                    <div class="block-title">
                        <h2>Shipping Address</h2>
                    </div>
                    <!-- END Shipping Address Title -->

                    <h4><span> <?php if ($customer_data['shipping_name'] != '') {
                                        echo $customer_data['shipping_name']; }
                                    ?></span></h4>
                    <address>
                        <?php if ($customer_data['shipping_address1'] != '') {
                            echo $customer_data['shipping_address1'].'<br>'; } ?> 
                            
                        <?php if ($customer_data['shipping_address2'] != '') {
                              echo $customer_data['shipping_address2'].'<br>'; } else{
                                  echo '';
                        } ?>
                        


                        <?php echo ($customer_data['shipping_city']) ? $customer_data['shipping_city'] . ',' : ''; ?>
                        <?php echo ($customer_data['shipping_state']) ? $customer_data['shipping_state'] : ''; ?>
                        <?php echo ($customer_data['shipping_zip']) ? $customer_data['shipping_zip'].'<br>' : ''; ?> 
                        <?php echo ($customer_data['shipping_zip']) ? $customer_data['shipping_zip'].'<br>' : ''; ?> 
                        <br>

                        <i class="fa fa-phone"></i> <?php echo $customer_data['Phone'];    ?><br>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
                    </address>
                    <!-- END Shipping Address Content -->
                </div>
                <!-- END Shipping Address Block -->
            </div>
            <?php } ?>
        <?php } ?>
        </div>
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-primary transaction-print-btn" onclick="printDiv();">Print Receipt</button>
            </div>
        </div>
        <!-- END Addresses -->
        <div id='DivIdToPrint' style="display:none;">
            <p>Transaction Receipt</p>
            <br>
            <p>Transaction Type: Sale</p>
            <p>Transaction Status: <?php if($transactionCode == 200 || $transactionCode == 100 || $transactionCode == 1 || $transactionCode == 111){
                        ?>Successful
                    <?php }else{ ?>
                        Failed
            <?php   } ?></p>
            <p>Amount: $<?php echo $transactionAmount;?></p>
            <?php if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
                <p>Surcharge Amount: $<?php echo $surchargeAmount;?></p>
                <p>Total Amount: $<?php echo $totalAmount;?></p>
            <?php } ?>
            <p>Date: <?php echo date("m/d/Y h:i A", strtotime($date));?></p>
            <p>Transaction ID: <?php echo $customer_data['transaction_id'];?></p>
            
            <p>Invoice(s): <?php foreach($invoice_data as $invoice){?>
                        <?php echo $invoice['RefNumber'].', ';?>
                    <?php } ?></p>
            <br>
            <?php if($BillingAdd){ ?>
                <p>Billing Address</p>
                <p><?php if ($customer_data['billing_name'] != '') {
                    echo $customer_data['billing_name'];
                } 
                ?></p>
                <p> <?php if ($customer_data['billing_address1'] != '') {
                    echo $customer_data['billing_address1']; } ?> 
                </p>
                <p><?php echo ($customer_data['billing_city']) ? $customer_data['billing_city'] . ',' : ''; ?>
                <?php echo ($customer_data['billing_state']) ? $customer_data['billing_state'] : ''; ?>
                <?php echo ($customer_data['billing_zip']) ? $customer_data['billing_zip'] : ''; ?> </p>
                <p><?php echo ($customer_data['billing_country']) ? $customer_data['billing_country'] : ''; ?></p>
                <p>
                <?php echo $customer_data['Phone'];    ?></p>
                <p><?php echo $customer_data['Contact'];    ?>
                </p>
                <br>
            <?php } 
            if($ShippingAdd){ ?>
                <p>Shipping Address</p>
                <p><?php if ($customer_data['shipping_name'] != '') {
                    echo $customer_data['shipping_name'];
                } 
                ?></p>
                <p> <?php if ($customer_data['shipping_address1'] != '') {
                    echo $customer_data['shipping_address1']; } ?> 
                </p>
                <p><?php echo ($customer_data['shipping_city']) ? $customer_data['shipping_city'] . ',' : '' ; ?>
                <?php echo ($customer_data['shipping_state']) ? $customer_data['shipping_state'] : ''; ?>
                <?php echo ($customer_data['shipping_zip']) ? $customer_data['shipping_zip'] : ''; ?> </p>
                <p><?php echo ($customer_data['shiping_counry']) ? $customer_data['shiping_counry'] : ''; ?> </p>
                <p>
                <?php echo $customer_data['Phone'];    ?></p>
                <p><?php echo $customer_data['Contact'];    ?>
                </p>
            <?php } ?>
        </div>
<script>
    
        function printDiv() 
            {

                document.getElementById("DivIdToPrint").style.fontFamily = 'Consolas';
                
                var printContents = document.getElementById('DivIdToPrint').innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
                location.reload();
            }
    
</script>