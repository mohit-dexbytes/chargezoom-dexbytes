<style type="text/css">
    #invoice_number_chosen{
        width: 100% !important;
    }
</style>
<div id="payment_refund_popup_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Refund Payment</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">

                <div id="refund_msg"></div>

                <form id="refund_pay_popup" method="post" class="form-horizontal">
                    <p id="_message_data"></p>
                    <div id="_ref_id">

                    </div>
                    <div class="pull-right">
                        <input type="button" id="refund_transaction_btn" class="btn btn-sm btn-warning" value="Refund" />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="cusIDModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h2 class="modal-title">Pending Invoices</h2>
            </div>
            <div class="modal-body">
                <div id="table1">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Select</button>
                <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

<div id="amountModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header text-center">
                <h2 class="modal-title">Amount</h2>
            </div>
            <div class="modal-body clearfix">
                
                <div id="table2">
                </div>
                <div class="form-group alignTableInvoiceListTotal">
               
                    <div class="col-md-2 text-center"></div>
                    <div class="col-md-2 text-left"></div>
                    <div class="col-md-3 text-right"></div>
                    <div class="col-md-2 text-right totalTextLeft" >Total</div>
                    <div class="col-md-3 text-left totalTextRight" >
                        $<span id="totalamount1">0.00</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Select</button>
                <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    
    function set_refund_popup(reqObject,type)
    {
        var ajaxURL = ipHTML = _message_data = '';
        var form = $("#refund_pay_popup");
        var actionURL = '<?php echo base_url(); ?>';

        if(type== 'customer_detail_invoice' || type == 'page_invoices'){
            var ajaxURL = "ajaxRequest/get_invoice_transactions";
            var data = {
                invID: reqObject.id
            };

            actionURL += 'QBO_controllers/refundInvoice/create_customer_refund';
            $("#refund_transaction_btn").attr('onclick', 'check_transaction(this)');

        } else if(type== 'payment_refund'){
            $("#refund_transaction_btn").attr('onclick', 'submitfrom(this)');
//change for refund input
            var ipHTML = `<div class="form-group" >
                    <div class="col-md-4 text-left"><b>Transaction</b></div>
                    <div class="col-md-4 text-right"><b>Amount</b></div>
                    
                </div>
                <div class="form-group" >
                    <div class="col-md-4 text-left"><input checked type="radio" class="radio1"   data-id="1"   name="txnID" value="${reqObject.txnid}" /> ${reqObject.txnid}</div>
                    <div class="col-md-4 text-right">${reqObject.refAmount}</div>
                    <div class="col-md-3 text-left"><input type="hidden"  name="ref_amount"  id="${reqObject.txnid}"  data-id="100"    class="form-control source-val input_txt_pay"  value="${reqObject.refAmount}" /></div>
                </div>
            `;
            $('#_ref_id').html(ipHTML);
            
            actionURL += 'QBO_controllers/refundInvoice/create_payment_refund';
        } else if(type== 'payment_transaction'){
            actionURL += 'QBO_controllers/refundInvoice/create_payment_refund';
            
            $('<input>', {
                'type': 'hidden',
                'id'  : 'trID',
                'name': 'trID',
                'class':'ref',
                'value': reqObject.id,
            }).appendTo(form);

    		if(reqObject.txnid !="")
    		{
                $('#multi_tr').html('');
                $('#txnID').remove();
                $('<input>', {
                    'type': 'hidden',
                    'id'  : 'txnID',
                    'name': 'txnID',
                    'class':'ref',
                    'value': reqObject.txnid,
                }).appendTo(form);
            }

            var ipHTML = `<div class="form-group" >
                    <div class="col-md-4 text-left"><b>Transaction</b></div>
                    <div class="col-md-4 text-right"><b>Amount</b></div>
                    
                </div>
                <div class="form-group" >
                    <div class="col-md-4 text-left"><input checked type="radio" class="radio1"   data-id="1"   name="txnID" value="${reqObject.txnid}" /> ${reqObject.txnid}</div>
                    <div class="col-md-4 text-right">${reqObject.refAmount}</div>
                    <div class="col-md-3 text-left"><input type="hidden"  name="ref_amount"  id="${reqObject.txnid}"  data-id="100"    class="form-control source-val input_txt_pay"  value="${reqObject.refAmount}" /></div>
                </div>
            `;
            $('#_ref_id').html(ipHTML);

            $("#refund_transaction_btn").attr('onclick', 'submitfrom()');
        }
        
        form.attr('action', ''+actionURL);
        
        if(ajaxURL!=="")
        {
            $.ajax({
                type:"POST",
                url: base_url + ajaxURL,
                data,
                success:function(response){
                    data=$.parseJSON(response); 
                    if(data.status=='success')
                    {
                        if(type = 'customer_detail_invoice' || type == 'page_invoices'){
                            // _message_data = 'Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.<br>You can select only one transaction at a time.';
                            _message_data = '';
                            ipHTML = '<input type="hidden" id="ref_invID" name="ref_invID" value="'+reqObject.id+'" />'
                           
                            $('#_message_data').html(_message_data);
                            $('#_ref_id').html(ipHTML+data.transactions);
                            $('#refund_transaction_btn').removeAttr('disabled');
                        }
                    }else{
                        
                        $('#_ref_id').html('<span>N/A</span>');
                        
                        $('#refund_transaction_btn').attr('disabled','disabled');
                    }
                    
                }
                
            });
        
        }
        
    }

    function submitfrom(){
        $( "#refund_pay_popup" ).submit();
    }

    function check_transaction(el) {

        var st = '';
        var msg = '';
        var btn = '';

        btn = $(el).attr('id');


        if ($("input[name='multi_inv']").is(":checked")) {


            var trID = $("input[name='multi_inv']:checked").val();
            var trAmount = $("#" + trID).val();

            $.ajax({
                url: "<?php echo base_url(); ?>ajaxRequest/check_transaction_qbo_payment_data",
                type: "POST",
                aync: false,
                dataType: 'json',
                data: {
                    trID: trID,
                    pay_amount: trAmount,
                },
                dataFilter: function(response) {

                    var rsdata = JSON.parse(response);

                    if (rsdata.status == 'success') {
                        msg = "Success";
                        st = 'success';
                    } else {
                        msg = rsdata.message;
                        st = 'error';

                        msg = '<div class="alert alert-danger">  <strong>Error:</strong> ' + msg + '</div>'

                    }

                    if (st == 'error') {

                        $('#refund_msg').html(msg);
                        $('#rf_btn').attr("disabled", true);
                    } else {
                        msg = '';
                        $('refnd_msg').html(msg);
                        $('#rf_btn').attr("disabled", false);
                        if (btn == "rf_btn") {
                            var index = $("input[name='multi_inv']:checked").data("id");

                            $('#index').remove();


                            $('<input>', {
                                'type': 'hidden',
                                'id': 'index',
                                'name': 'index',

                                'value': index,
                            }).appendTo($('#data_form'));
                            $('#data_form').submit();
                        }
                    }
                }
            });
        } else {
            $('#data_form').submit();
        }

    }

    $(document).on('change', '.test', function() {
        totalCount = calculateAll();
        var price = totalCount.toFixed(2);
        $('#amount').val(price);
        $('#totalamount').val(price);


        var tmp = [];
        var payAmount = [];
        var totalInvoiceAmount = 0.00;
        $(".test").each(function() {
            if ($(this).is(':checked')) {

                var checked1 = $(this).attr("rel");
                var amount = $(this).val();
                totalInvoiceAmount = parseFloat(totalInvoiceAmount) + parseFloat(amount);
                tmp.push(checked1);
                payAmount.push($('#inv_original_amount_'+checked1).val());
            }
        });
        
        $('#invoiceTotalAmountCal').val(totalInvoiceAmount.toFixed(2));

        $('#invoice_ids').val(tmp);
        $('#invoice_pay_amount').val(payAmount);
    });

    
    function calculateAll() {
        count = 0;
        $("input[name='inv']").each(function(index, checkbox) {
            if (checkbox.checked)
                count += parseFloat(checkbox.value);
        })
        return count;
    }

    $(document).ready(function() {
       

        $(document).on('change', '.test', function() {
            totalCount = calculateAll();
            var price = totalCount.toFixed(2);
            $('#amount').val(price);
            $('#totalamount').val(price);


            var tmp = [];
            $(".test").each(function() {
                if ($(this).is(':checked')) {

                    var checked1 = $(this).attr("rel");
                    tmp.push(checked1);
                }
            });

            var invoice_number_options = '';
            $("#invoice_number option").each(function()
            {
                if($.inArray($(this).val(), tmp) !== -1){
                    invoice_number_options += '<option value="'+ $(this).val() +'" selected>'+ $(this).html() +'</option>';
                }else{
                    invoice_number_options += '<option value="'+ $(this).val() +'">'+ $(this).html() +'</option>';
                }
            });
            $('#invoice_number').empty(); //remove all child nodes
            $('#invoice_number').html(invoice_number_options);
            $('#invoice_number').trigger("chosen:updated");

            $('#invoice_ids').val(tmp);
        });
    });
    
</script>