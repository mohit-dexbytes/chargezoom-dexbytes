
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/merchantDashboard.css">

<div id="page-content">
 
<div class="row invoice_quick">
	<div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-success rounded-circle mr-2">
                    <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold">
                        	<span data-plugin="counterup">
                            	<?php echo ($subs_data['total_subcription'])?$subs_data['total_subcription']:'0'; ?>
                            		
                            </span>
                        </h4>
                        <p class="text-truncate">Subscribed Customers</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div>
	<div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-info rounded-circle mr-2">
                    <i class="fa fa-credit-card ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold">
                          <span data-plugin="counterup">
                            <?php echo ($subs_data['active_subcription'])?$subs_data['active_subcription']:'0'; ?>
                          </span>
                        </h4>
                        <p class="text-truncate">Active Subscriptions</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div>	
    <div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-purple rounded-circle mr-2">
                    <i class="fa fa-calendar ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold">
                          <span data-plugin="counterup">
                            <?php echo $subs_data['exp_subcription']; ?>
                          </span>
                        </h4>
                        <p class="text-truncate">Expiring Subscriptions</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div>    
        
    <div class="col-sm-6 col-lg-3">
        <div class="card-box">
            <div class="media">
                <div class="avatar-md bg-primary rounded-circle mr-2">
                    <i class="fa fa-thumbs-o-down ion-logo-usd avatar-title font-26 text-white"></i>
                </div>
                <div class="media-body align-self-center">
                    <div class="text-right">
                        <h4 class="font-20 my-0 font-weight-bold">
                          <span data-plugin="counterup">
                            <?php echo $subs_data['failed_count']; ?>
                          </span>
                        </h4>
                        <p class="text-truncate">Failed Subscriptions</p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end card-box-->
    </div> 
		
        
	</div>
    <!-- All Orders Block -->
   <legend class="leg"> Subscriptions</legend>
    <div class="block-main full" style="position: relative;">
        <div class="addNewFixRight width248FixRight" >
            <a class="btn  btn-sm btn-info" title=""  data-backdrop="static" data-keyboard="false" data-toggle="modal"  href="#Edit_gateway" style="margin-right: 4%;">Change Gateway in Bulk</a>
            <a class="btn  btn-sm btn-success"  href="<?php echo base_url(); ?>QBO_controllers/SettingSubscription/create_subscription">Add New </a>
        </div>
        
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="sub_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left hidden-xs">Subscription</th>
                    <th class=" text-left">Customer Name</th>
					          
                    <th class="text-right">Amount</th>
					          <th class="hidden-xs text-right">Next Charge Date</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
				
				
				if(isset($subscriptions) && $subscriptions)
				{
					foreach($subscriptions as $subs)
					{ 
						
				?>
				<tr>

          			<td class="text-left cust_view"><a href="<?php echo base_url('QBO_controllers/SettingSubscription/subscription_invoices/'.$subs['subscriptionID']); ?>"><?php echo $subs['sub_planName']; ?></a></td>

				  <td class="text-left cust_view"><a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$subs['customerID']); ?>"><?php echo $subs['fullName']; ?> </a> </td>
					
					<td class="text-right ">$<?php echo ($subs['subscriptionAmount'])?number_format($subs['subscriptionAmount'],2):'0.00'; ?></td>
				   
					<td class="hidden-xs text-right"><span class="hidden"><?php echo $subs['nextGeneratingDate']; ?></span><?php echo date('M d, Y', strtotime($subs['nextGeneratingDate'])); ?></td>
					
					<td class="text-center">
            <div class="btn-group dropbtn">
              <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
              <ul class="dropdown-menu text-left">

                <li> <a href="<?php echo base_url('QBO_controllers/SettingSubscription/create_subscription/'.$subs['subscriptionID']); ?>" data-toggle="tooltip" title="Edit"> Edit </a> </li>
                
                <li> <a href="<?php echo base_url('QBO_controllers/SettingSubscription/create_subscription/'.$subs['subscriptionID']); ?>" data-toggle="tooltip" title="Detail"> Detail </a> </li>

                <li> <a href="#del111_qbo_subs" onclick="set_qbo_subs_id('<?php  echo $subs['subscriptionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete"> Delete </a> </li>

              </ul>
            </div>
					
					</td>
				</tr>
				
				<?php } }
				else { echo'<tr><td colspan="5"> No Records Found </td>
                    <td style="display: none"></td>
                    <td style="display: none"></td>
                    <td style="display: none"></td>
                    <td style="display: none"></td>
                    </tr>'; }  
				?>
				
			</tbody >
        </table>
       
    </div>
    
<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>

</div>    
    <!-- END All Orders Block -->
    <style>
        @media screen and (max-width:400px){
         .block-title-xs {
          height:85px !important;
         }
        }
        
        

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

	
        
    </style>
  
  

<!-- Load and execute javascript code used only in this page -->

<div id="Edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Change Gateway in Bulk</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>QBO_controllers/SettingSubscription/update_gateway' class="form-horizontal" >
				 
                     				 
				   
				   
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Current Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_old" name="gateway_old"  class="form-control">
								   <option value="" >Select Current Gateway</option>
								    <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
				
					
						<div class="form-group ">
                        	<label class="col-md-4 control-label" for="card_list">Change to Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_new" name="gateway_new"  class="form-control">
								   <option  value="" >Select New Gateway</option>
								   <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						</div>
						
				<div class="form-group ">
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel1" name="btn_cancel1" class="btn btn-sm btn-info" value="Change"  disabled="true"/>
                    <button type="button"  id="btn_cancel" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
					</div>
					
					
					
					<table>
					   
				<tbody id="t_data">
						 
						 </tbody>
				 </table>
					
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit" id="btn" name="btn_cancel" class="btn btn-sm btn-info" value="Change"  />
                    <button type="button" id="btn" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script src="<?php echo base_url(JS); ?>/pages/qbo_subscription.js"></script>
<script>$(function(){  Pagination_view.init();
 });</script>
<script>
	window.setTimeout("fadeMyDiv();", 2000);
		
        function fadeMyDiv() {
		   $(".msg_data").fadeOut('slow');
		}
  
	
 $('#gateway_old').change(function(){
	 
	
    var gID =  $('#gateway_old').val();
    if(gID == ""){
        $('#btn_cancel1').prop('disabled', true);
    }else{
        $('#btn_cancel1').prop('disabled', false);
    }
	$.ajax({
    url: '<?php echo base_url("QBO_controllers/SettingSubscription/get_gateway_id")?>',
    type: 'POST',
	data:{gateway_id:gID, merchantID:'<?php echo $merchantID; ?>'},
	
    success: function(data){
		
		$('#t_data').html(data);
		
		
		
		var newarray = JSON.parse("[" + data.subscriptionID + "]");
	
			 for(var val in  newarray) {
			 $("input[type=checkbox]").each(function() {
				
			if($(this).val()==newarray[val]) {

				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = true;
				}
			});
			 
			 }
		
			
     }	
  });
	
});	
	

 
var Pagination_view = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#sub_page').dataTable({
                columnDefs: [
                    { type: 'date', targets: [-1] },
                    { orderable: true, targets: [3] }
                ],
                order: [[ 4, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


		

	  
var nmiValidation1 = function() {   

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#gatw').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                     
					 gateway_old: {
							 required:true,
						},
					gateway_new:{
							 required:true,
					},
					
					
                },
               
            });
					
		
		
        }
    };
}();





</script>


<!-- END Page Content -->