 <!-- Page content -->
   <!-- Page content -->
<?php
	$this->load->view('alert');
?>
	<div id="page-content" style="min-height: 584px;">
		<legend class="leg"><?php if(isset($user)){ echo "Edit User";}else{ echo "Create New User"; }?></legend>
    
		<form method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>QBO_controllers/MerchantUser/create_user">
		    <div class="block">
			
			 <input type="hidden"  id="userID" name="userID" value="<?php if(isset($user)){echo $user['merchantUserID']; } ?>"/>
			
			
			   
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">First Name </label>
					<div class="col-md-6">
					<input type="text" id="userFname" name="userFname" class="form-control"  value="<?php if(isset($user)){ echo $user['userFname']; } ?>"><?php echo form_error('userFname'); ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Last Name </label>
					<div class="col-md-6">
					<input type="text" id="userLname" name="userLname" class="form-control"  value="<?php if(isset($user)){ echo $user['userLname']; } ?>"><?php echo form_error('userLname'); ?> </div>
					</div>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email Address</label>
					<div class="col-md-6">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($user)){ echo $user['userEmail']; } ?>"></div>
					</div>
			    	<?php if(!isset($user)) {?>	
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Password </label>
					<div class="col-md-6">
					<input type="password" id="userPassword" name="userPassword" class="form-control" value="<?php if(isset($user)){ echo $user['userPassword']; } ?>"> </div>
					</div> 
			       	<?php }?>
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">
					Role Name  </label>
					<div class="col-md-6">
					
					     
						<select id="roleID" class="form-control" name="roleID">
						
						<option value=""> Select Role Name </option>
								
						<?php foreach($role_name as $role) { ?>
						<option  value="<?php echo $role['roleID'];?>" <?php if(isset($user) && $user['roleId']==$role['roleID']){ echo "selected"; } ?>  > <?php echo $role['roleName'];?> </option> 
						
								<?php } ?>
							</select>
						</div>			
					</div>
				 
    
                  
	             <div class="form-group">
					<label class="col-md-4 control-label" for="example-username"></label>
					<div class="col-md-6">
					    <div class="pull-right">
					<?php if(!isset($user)) {?>				
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
					<?php } else {?>
				     <a href="<?php echo base_url().'QBO_controllers/MerchantUser/recover_pwd/'.$this->uri->segment(4);  ?>" class="submit btn btn-sm btn-danger">Reset Password</a>
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
					<?php }?>
					</div>
				    </div>	
		            </div>	
		
        </div>
  	</form>
		
<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>


<!-- END Page Content -->


</div>

<script>
$(document).ready(function(){

	var error_message = 'Email already registered with another merchant.';
    $('#role_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		             'userFname': {
                        required: true,
                        minlength: 2,
                        maxlength: 100,
                        validate_char:true, 
                    },
                    'userLname': {
                        required: true,
                        minlength: 2,
                        maxlength: 100,
                        validate_char:true,
                    },
					
			    	'userEmail': {
                        required: true,
                        email: true,
						remote: {
							url: base_url+'ajaxRequest/check_merchant_user',
							type: 'POST',
							data:{
								userEmail: function() {
									return $('#userEmail').val();
								},
								userId: function() {
									return $('#userID').val();
								},
							},
							dataFilter: function (response) {
								var rsdata = jQuery.parseJSON(response);
								if (rsdata.success === true)
									return true;
								else {
									error_message = rsdata.message;
									return false;
								}
							}
						}
                    },
                    'userPassword': {
                        required: true,
                        minlength:5,
                    },
					
                    'userAddress': {
                        required: true,
                        minlength:5,
                        maxlength: 41,
                        validate_char:true,
                    },
                    'roleID': {
                        required: true,
                    
                    }
			
			},
			messages:{
				userEmail: {
					remote: function(){ return error_message; },
				},
			}
    });
});	    
		
	    $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    

</script>




