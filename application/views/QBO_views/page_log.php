<!-- Page content -->
<?php
	$this->load->view('alert');
?>

<div id="page-content">
    <span id="flashdata"> 
     
    </span>
    <legend class="leg">Sync Log</legend>
    <!-- All Orders Block -->
    <div class="block-main full">
        
        <!-- All Orders Title -->
        
        <!-- END All Orders Title -->

        <!-- <div style="position: relative"> -->

            <!-- END All Orders Title -->
            <?php 
            if($queueInProgress){
                 echo '<div class="alert alert-warning">Quickbook data sync is in progress. Please wait for atleast 5 Minutes.</div>'; 
            } 
            ?>
            <div style="position: relative;">
            <form class="filter_form" action="<?php echo base_url();?>QBO_controllers/home/Qbo_sync_data" method="post">
            
                <select class="form-control sync_filter" name="sync_filter">
                    <option value="0">All</option>
                    <option value="1">Customers</option>
                    <option value="2">Invoices</option>
                    <option value="3">Products & Services</option>
                    <option value="4">Payment Methods</option>
                </select>

                <button class="btn  btn-sm btn-success addNewFixRight" title="Create New">Force Pull</button>
            </form>
            </div>
            <!-- All Orders Content -->
            <div style="position: relative;">
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left hidden">Sort Date</th>
                    <th class="text-left hidden-xs">Time</th>
                    <th class="text-left">ID</th>
                    <th class="text-right ">Message</th>
                    <th class="hidden-xs hidden-sm text-right" style="width:225px !important;">Status</th>
                    
                 
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($alls) && $alls)
				{
					foreach($alls as $k=>$all)
					{
                     
				?>
				<tr> 
          <td class="text-right hidden">
              <?php
                  echo $all['createdAt'];
              ?>
          </td>
          <td class="text-left ">
                <?php
                    $date = $all['createdAt'];
                    if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                        $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                        $date = getTimeBySelectedTimezone($timezone);
                    } 
                    echo date('M d, Y h:i A', strtotime($date));
                ?>
          </td>
					 <td class="text-left ">
             <?php echo ($all['qbSyncID'] == null || $all['qbSyncID'] == '')?'#'.$all['qbActionID']:'#'.$all['qbSyncID'];
                ?>
               
           </td>
        
          
           <td class="hidden-sm text-right ">
             <?php if($all['qbSyncStatus'] == null || $all['qbSyncStatus'] == ''){
               echo $all['qbAction'];
             }else{
                if($all['qbStatus']==0 ){
                  echo $all['qbSyncStatus'].' Not Synced';
                }else{
                  echo $all['qbSyncStatus'].' Synced Successfully';
                }
               
             }  ?>
               
           </td>


    
          <td class="text-right hidden-xs" style="width:225px !important;"><?php 
            if($all['qbStatus']==0 ){  ?>

              Not Synced 
              <br><a href="javascript:void(0)" onclick="set_seenk_data('<?php echo $all['id'];  ?>');" class="btn btn-danger"  >Resync</a> 
              <?php }
            if($all['qbStatus']==1 ){ echo "Synced"; } ?>
          </td>
          
					
				</tr>
				
				<?php } }else{ echo'<tr>
                <td colspan="5"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
        </div>
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>
let queueInProgress = '<?php echo ($queueInProgress ? 'Yes' : 'No'); ?>';
$(function(){ 
Pagination_view111.init(); });



var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#ecom-orders111').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: true, targets: [-1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script type="text/javascript">
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 3000); //call fade in 2 seconds
 });

function fadeMyDiv() {
}


function set_seenk_data(logID='')
{

$("#snk_form").remove();
var f = document.createElement("form");
f.setAttribute('method',"post");

f.setAttribute('id',"snk_form");

f.setAttribute('action',"<?php echo base_url(); ?>QBO_controllers/home/sync_event");
var i = document.createElement("input"); //input element, text
i.setAttribute('type',"hidden");
i.setAttribute('name',"logID");
i.setAttribute('value', logID);


f.appendChild(i);

//and some more input elements here
//and dont forget to add a submit button

document.getElementsByTagName('body')[0].appendChild(f);

$('#snk_form').submit();

}

jQuery(document).ready(function(){
    if (queueInProgress == 'Yes'){
        setInterval(function(){
            $.ajax({
                type: 'GET',
                url : base_url+'QBO_controllers/home/checkQboQueue',
                dataType:'JSON',
                success: function(response){
                    if(response.status == 'success' && response.queue_updated > 0){
                        location.reload(true);
                    }
                }
            });
        
        },60000);
    } 
});
</script>
	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>

<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
</div>
<!-- END Page Content -->