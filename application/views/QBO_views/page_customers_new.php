<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <?php echo $this->session->flashdata('message');   ?>

    <!-- All Orders Block -->
    <div class="block full page_customer">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Customers</strong> </h2>
              <div class="block-options pull-right">
                           
                               <a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>QBO_controllers/Customer_Details/create_customer">Add New</a>
                             
                        </div>
        </div>
  
         
         <div class="block-options show_hide_cust_btn">
              <a id="sh_cust" class="btn btn-sm btn-primary1"> Show/Hide Inactive </a>
              
              <input type="hidden" name="status_value" id="status_value" value="1">
              
         </div>     
         
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Customer Name</th>
                    <th class="text-left ">Full Name</th>
                    <th class="text-left hidden-xs">Email Address</th>
                    <th class="hidden-xs hidden-sm text-right">Phone Number</th>
                   <th class="text-right hidden-xs">Balance</th>
                    <th class="text-center" style="width:85px !important;">Action</th>
                </tr>
            </thead>
            <tbody>
			
				
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
<div id="qbo_del_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="st_head" >Deactivate Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck_qbo" method="post" action='<?php echo base_url(); ?>QBO_controllers/home/delete_customer' class="form-horizontal" >
                     
                 
                    <p id="st_msg">Do you really want to deactivate this Customer?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="qbocustID" name="qbocustID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="qbo_btn_cancel" name="qbo_btn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>

<script>

var table;


$(document).ready(function() {
	
	
	   table = $('.compamount').DataTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
             "lengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
              "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('QBO_controllers/Customer_Details/ajax_all_customers')?>",
                "type": "POST" ,
                
                "data":function(data) {
                    
                            data.type 		= $('#status_value').val();
                           
							data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
						},
                 
            },

           
            "columnDefs": [
				{ 
				  "targets": [ -1 ], //last column
				  "orderable": false, //set not orderable
				},
            ],
           "order": [[0, 'asc']],
          	 "language": {
						"lengthMenu": " _MENU_ ",
					  "sSearch": "",
					  "paginate": {
                      "previous": "",
					   "next": "",
                        }
          
					},
                "sPaginationType": "bootstrap",
				"stateSave": true 

          });
         $('.compamount_wrapper .dataTables_filter input').addClass("form-control ").attr("placeholder", "Search");
         // modify table search input
         $('.compamount_wrapper .dataTables_length select').addClass("m-wrap form-control ");
         // modify table per page dropdown
         $('.compamount_wrapper .dataTables_length dataTables_length select').select2();
         // initialzie select2 dropdown   

	   
	   
	   
	   $('#sh_cust').click( function (event) {
		      if( $('#status_value').val()=='1') 
			  {
			  
				 $('#status_value').val('0');		
			   
        	     table.draw();  
				
			  } else {
			     
			       $('#status_value').val('1');	
			       table.draw(); 
			  }
			  
			  
		
		});		
	   
	   
	   
	
}); // Jquery End



</script>


	 <style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>

 

</div>
<script type="text/javascript">
    function delete_qbo_customer(cust_id, st){
        
        var base_url =$('#base_url').val(); 
        if(st==1)
        {
             $('#qbo_btn_cancel').val("Activate");
             $('#qbo_btn_cancel').removeClass("btn-danger");
              $('#qbo_btn_cancel').addClass("btn-success");
             $('#st_head').html("Activate Customer");
              $('#st_msg').html("Do you really want to activate this Customer?");
        }else{
             $('#qbo_btn_cancel').val("Deactivate");
              $('#qbo_btn_cancel').removeClass("btn-success");
              $('#qbo_btn_cancel').addClass("btn-danger");
             $('#st_head').html("Deactivate Customer");
              $('#st_msg').html("Do you really want to deactivate this Customer?");
              
              
         
        }
        
          $('<input>', {
                            'type': 'hidden',
                            'name': 'st_act',
                             'id': 'st_act',
                           
                        }).remove();
        
       $('<input>', {
                            'type': 'hidden',
                            'name': 'st_act',
                             'id': 'st_act',
                            'value': st,
                        }).appendTo($('#del_ccjkck_qbo'));
 
     $('#qbocustID').val(cust_id);
    }
    window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }
</script>
<!-- END Page Content -->