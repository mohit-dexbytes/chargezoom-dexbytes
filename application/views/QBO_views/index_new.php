<style>

.pd-top-10{
    padding-top:10px;
}

.pd-top-15{
    padding-top:15px;
}

.bck{
    
    background-color: #f2f2f2 !important; 
}
.btn-primary2 {
    background-color: #f2f2f2 !important;
    border-color: #e91111 !important;
    color: #888888 !important;
    border:2px solid;
}

.btn-21
{
    font-size:21px;
  
}
.btn-14
{
    font-size:14px;
  
}

.btn-font-21
{
    font-size:28px;
    
}
  .input-group-addon {
  
    background-color: #f4f1f1;
  
}

.head1{
    
    color:#418eff;
}
.under_line{
    
    border-bottom:2px solid #418eff !important;
}
.hr_line{
    
    margin:0px;
    border-top: 1px solid #cdcbcb !important;
}
.pd{
     font-size: 14px;
     margin: 10px 0px 10px 0px; 
}
.btn-font-16
{
 font-size:16px;    
}
    .plan{
        font-size: 18px;
       margin: 10px 14px 10px 14px;
    }

  .btn-size{
        
       padding:8px 35px 8px 35px;
    }
    
	#chart-classic2 g text tspan {
    visibility: visible;
}
#chart-classic2 g {
    opacity: 0.5;
	borderWidth:2px;
}

#chart-classic2 text tspan {
    visibility: hidden;
}

	#chart-pie-customers g text tspan {
    visibility: visible;
}
#chart-pie-customers g {
    opacity: 1.5;
	borderWidth:2px;
}

#chart-pie text tspan {
    visibility: hidden;
}

	#chart-pie g text tspan {
    visibility: visible;
}
#chart-pie g {
    opacity: 1.5;
	borderWidth:2px;
}

#chart-pie-customers text tspan {
    visibility: hidden;
}



</style>
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- eCommerce Dashboard Header -->
  
    <!-- END eCommerce Dashboard Header -->

    <!-- Quick Stats -->
   
	
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
     
        <div class="row">
    
      <div class="col-sm-6 col-lg-3">     
                <div class="widget  text-white bg-success">
                    <div class="widget-simple">
                        <div class="widget-icon pull-left">
                            <i><img src="<?php echo base_url().IMAGES?>payment.png"></i>
                        </div>
                        <div class="pd-top-15">
                             <span class="w_invoice">Revenue <strong><?php echo date('F');?></strong> </span> <br>
                           
                            <?php if(isset($recent_pay) && $recent_pay)
				                {
				                 $volume = $recent_pay['total_amount']-$recent_pay['refund_amount'];
		        		        ?>$
				            <?php echo ($volume)?number_format($volume,2):'0.00'; ?><?php } ?> 
                        </div>
                    </div>
                </div>
         </div>        
	  
        
        <div class="col-sm-6 col-lg-3">     
                <div class="widget  text-white bg-primary">
                    <div class="widget-simple">
                        <div class="widget-icon pull-left">
                            <i><img src="<?php echo base_url().IMAGES?>processed.png"></i>
                        </div>
                        <div class="pd-top-15">
                             <span class="w_invoice">Processed Today </span> <br>
                           
                              <?php echo $today_invoice; ?>
                        </div>
                    </div>
                </div>
         </div>        
	   
     
         <div class="col-sm-6 col-lg-3">     
                <div class="widget  text-white bg-warning">
                    <div class="widget-simple">
                        <div class="widget-icon pull-left">
                            <i><img src="<?php echo base_url().IMAGES?>scheduled.png"></i>
                        </div>
                        <div class="pd-top-15">
                             <span class="w_invoice">Scheduled Payments </span> <br>
                           
                              <?php echo $upcoming_inv; ?>
                        </div>
                    </div>
                </div>
         </div>        
	   
        
      <div class="col-sm-6 col-lg-3">     
                <div class="widget  text-white bg-danger">
                    <div class="widget-simple">
                        <div class="widget-icon pull-left">
                            <i><img src="<?php echo base_url().IMAGES?>failed.png"></i>
                        </div>
                        <div class="pd-top-15">
                             <span class="w_invoice">Failed Invoices </span> <br>
                           
                              <?php echo $failed_inv; ?>
                        </div>
                    </div>
                </div>
         </div> 
         
       </div>
    
       
<!-- END Quick Stats -->
       <div class="row pd-top-10">
       <div class="col-md-12">
          <div class="plan">
            <span class="under_line"> <strong>General Overview</strong></span>
             <hr class="hr_line">
         </div>
       </div> 
    </div>  
    <!-- eShop Overview Block -->
     <div class="block full">
        <!-- eShop Overview Title -->
       
        <div class="row">
            <div id="chart-classic2" style="height: 350px;"></div>
        </div>
    </div>    
		<hr>
	    <div class="row">
		 <div class="col-sm-6">
		    <div class="plan">
              <span class="under_line"> <strong>Top 10 Due By Customer</strong></span>
                <hr class="hr_line">
             </div>
            <!-- Bars Chart Block -->
            <div class="block full">
                <!-- Bars Chart Title -->
                <div id="chart-pie-customers" class="chart"></div>
             
            </div>
           
        </div>
        <div class="col-sm-6">
            <div class="plan">
              <span class="under_line"> <strong>Top 10 Past Due By Customer</strong></span>
                <hr class="hr_line">
             </div>
            <!-- Classic Chart Block -->
            <div class="block full">
                <!-- Classic Chart Title -->
              
              
                <div id="chart-pie" class="chart"></div>
              
            </div>
           
        </div>
       
    </div>
   

    <div class="row">
        <div class="col-lg-6">
          <div class="plan">
             <span class="under_line"> <strong>Recent Payments</strong></span>
             <hr class="hr_line">
          </div>
         
            <!-- Latest Orders Block -->
            <div class="block">
                <!-- Latest Orders Title -->
                <div class="block-title">
                  
                </div>
                <!-- END Latest Orders Title -->
			    <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
                        <tr>
                             <?php if($plantype) { ?>
                            <td class="text-left" style="display:none"><strong>Customer Name</strong></td>
                             <?php } else { ?>
                             <td class="text-left"><strong>Customer Name</strong></td>
                             <?php } ?>
							<td class="text-right"><strong>Invoice</strong></td>
                            <td class="text-right"><strong>Payment Date</strong></td>
                            <td class="text-right"><strong>Amount</strong></td>
							
                           
                        </tr>
						
                       	<?php   if(!empty($recent_paid)){   
 
						foreach($recent_paid as $invoice){

					 ?>
                        <tr>
                        <?php if($plantype) { ?>
                        <td class="text-left cust_view" style="display:none"><a href="<?php echo base_url(); ?>QBO_controllers/home/view_customer/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>
						 <?php } else {?>
						  <td class="text-left cust_view"><a href="<?php echo base_url(); ?>QBO_controllers/home/view_customer/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>
						 <?php } ?>
                            <td class="text-right cust_view"><a href="<?php echo base_url(); ?>QBO_controllers/Create_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php  echo $invoice['refNumber']; ?></a></td>
                            <td class="text-right"><?php  echo  date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>
                            <td class="text-right">$<?php  echo  number_format($invoice['balance'],2); ?></td>
							 
                              
                        </tr>
						   <?php } }else { ?>	
						
						<tr>
                            <td colspan="4"><strong>No Records Found</strong></td>
                         
                        </tr>
						
						   <?php } ?>
						
						   
                       
                        
                    </tbody>
                </table>
              
                <!-- END Latest Orders Content -->
            </div>
            <!-- END Latest Orders Block -->
        </div>
        <div class="col-lg-6">
            <!-- Top Products Block -->
            <div class="plan">
             <span class="under_line"> <strong>Oldest Invoices</strong></span>
             <hr class="hr_line">
            </div>
            <div class="block">
                <!-- Top Products Title -->
               
                <!-- END Top Products Title -->

                <!-- Top Products Content -->
               <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
                        <tr>
                             <?php if($plantype) { ?>
                            <td class="text-left" style="display:none"><strong>Customer Name</strong></td>
                             <?php } else { ?>
                             <td class="text-left"><strong>Customer Name</strong></td>
                             <?php } ?>
                            <td class="text-right" ><strong>Invoice</strong></td>
							  
                            <td class="text-right"><strong>Due Date</strong></td>
                           
							  <td class="text-right"><strong>Amount</strong></td>

                        </tr>
						
						<?php   if(!empty($oldest_invs)){   

						foreach($oldest_invs as $invoice){
						  
						      if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }   
						else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   } else
						   
						   if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
							
						 ?>
                        <tr>
                            <?php if($plantype) { ?>
                           <td class="text-left cust_view" style="display:none"><a href="<?php echo base_url(); ?>QBO_controllers/home/view_customer/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>
                             <?php } else { ?>
                            <td class="text-left cust_view"><a href="<?php echo base_url(); ?>QBO_controllers/home/view_customer/<?php echo $invoice['CustomerListID']; ?>"><?php  echo  $invoice['FullName']; ?></a></td>
                             <?php } ?>
                            
                            
						 
                            <td class="text-right cust_view"><a href="<?php echo base_url(); ?>QBO_controllers/Create_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php  echo $invoice['refNumber']; ?></a></td>
                            <td class="text-right"><?php  echo  date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
						   <td class="text-right">$<?php  echo  number_format($invoice['BalanceRemaining'],2); ?></td>
                            
                        </tr>
						   <?php  } }else { ?>	
						<tr>
                            <td colspan="4"><strong>No Records Found</strong></td>
                        </tr>
						
						   <?php } ?>
						
						   
                       
                        
                    </tbody>
                </table>
                <!-- END Top Products Content -->
            </div>
            <!-- END Top Products Block -->
        </div>
    </div>
    <!-- Live Chart Block -->

	
	</div>

	

<link id="themecss" rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/shieldui/all.min.css" />          
<script type="text/javascript" src="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/js/shieldui/shieldui-all.min.js"></script>	
<script>$(function(){ 
});</script>
<script>
  /*
 *  Document   : compCharts.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Charts page
 */
                        
                
                        
                        
                        
var general_volume = 0;
var pchart_color_customer = []; 
var pchart_past_due = []; 
	

$.ajax({
			type:"POST",
		url : "<?php echo base_url(); ?>QBO_controllers/home/general_volume",
                       success: function (data) {
              var general_volume = $.parseJSON(data);
               
 $("#chart-classic2").shieldChart({
            theme: "light",
            primaryHeader: {
                text: ""
            },
            exportOptions: {
                image: false,
                print: false
            },
            axisX: {
                categoricalValues: general_volume.month
            },
                 tooltipSettings: {
             chartBound: true,
            axisMarkers: {
                    enabled: true,
                    mode: 'xy'
                }, 
             customPointText: function (point, chart) {
            return shield.format(
                '<span>{value}</span>',
                {
                    value: format2(point.y)
                }
            );
        }
    },
			
            dataSeries: [{
                seriesType: 'splinearea',
                collectionAlias: "USD",
                data: general_volume.volume
            }]
			
        });   
                }
    });

 
 $.ajax({
			type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/home/get_invoice_due_company",
					dataType: 'json',
            success: function (data) {
                var plancharge  = data;
                var chartPie    = $('#chart-pie-customers'); 
                var color =['#28a745', '#007bff', '#ffc107', '#e67e22', '#e74c3c', '#34dbcb', '#db7734', '#3abc','#ff1a75', '#8c1aff'];
                CompCharts.init(plancharge, chartPie, color);
                }
    		});	   
    		
    $.ajax({
			type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/home/get_invoice_Past_due_company",
				dataType: 'json',
            success: function (data) {
           
           var plancharge =data;
           
           var chartPie    = $('#chart-pie'); 
           var color = ['#28a745', '#007bff', '#ffc107', '#e67e22', '#e74c3c', '#34dbcb', '#db7734', '#3abc','#ff1a75', '#8c1aff'];
              CompCharts.init(plancharge, chartPie, color);
           }
             
    		});


    
    		
var CompCharts = function() {

    return {
        init: function(pchart_data,char_id, color) {
       
       var colorPalette = color;
            
                  char_id.shieldChart({
            theme: "bootstrap",
            seriesPalette: colorPalette,
            exportOptions: {
                image: false,
                print: false
            },
            primaryHeader: {
                text: " "
            },
           
           
           seriesSettings: {
                    pie: {
                        enablePointSelection: true,
                        addToLegend: true,
                        dataPointText: {
                            enabled: false,
                           
                        }
                    }
                },
                chartLegend: {
                   width: 70,
                   align: 'right',
                   x:-80,
                },  
           
                  
            
           
            tooltipSettings: {
                 customHeaderText:"{point.collectionAlias}", 
              
            
                
           customPointText: function (point, chart) {
                           
            return shield.format(
                '<span>{value}</span>',
                {
                    value: format2(point.y)
                }
            );
        }
              
            },
            axisY: {
                title: {
                    text: ""
                }
            },
            dataSeries: [{
                seriesType: "pie",
                collectionAlias: "Customers",
                data: pchart_data
                 
            }]
        });
             
            
            
            
            
         
        }
    };
}();

    
</script>

