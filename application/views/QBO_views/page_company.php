<!-- Page content -->

<?php
	$this->load->view('alert');
?>

<div id="page-content">
   

    <!-- All Orders Block -->
    <legend class="leg">Accounting Package</legend>
    <div class="full">
        <!-- All Orders Title -->
        <div class="new_btn">
           
            <a href="<?php echo base_url('QBO_controllers/home/qbo_log'); ?>"  data-toggle="tooltip" class="btn btn-info btn-sm" > View Log</a>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="company" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Accounting Package</th>
                    <th class=" text-left hidden-xs">Company ID</th>
                     <th class="hidden-xs text-right">Refresh Token</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($companies) && $companies)
				{
					foreach($companies as $company)
					{
				?>
				<tr>
					<td class="text-left cust_view"><?php echo "QuickBooks Online"; ?></td>
				    <td class="text-left hidden-xs"><?php echo $company['realmID']; ?></td>
					<td class="text-right hidden-xs"><?php echo $company['refreshToken']; ?></td>
					<td class="text-center">
                    <?php if($this->session->userdata('disconnect') == '1'){?>
                        
                         <img id="qbo-reconnect" title="Reconnect" class="image" src="<?php echo base_url(IMAGES); ?>/qbo-reconnect.png">
                        <ipp:connectToIntuit></ipp:connectToIntuit>
                    <?php } elseif($connect == 'yes'){?>
                    <a href="#del_pack"  data-backdrop="static" data-keyboard="false" data-toggle="modal" data-token=<?php echo $company['refreshToken']; ?> title="Disconnect" class="btn btn-danger disconnect-app"><i class="fa fa-times"></i></a> 
                     <img style="display:none;" id="qbo-reconnect" title="Reconnect" class="image" src="<?php echo base_url(IMAGES); ?>/qbo-reconnect.png">
                        <ipp:connectToIntuit></ipp:connectToIntuit>
                    <?php } else { ?>
                        
                        <img id="qbo-reconnect" title="Reconnect" class="image" src="<?php echo base_url(IMAGES); ?>/qbo-reconnect.png">
                        <ipp:connectToIntuit></ipp:connectToIntuit>
                     <?php }
                    ?>
                    </td>
				
				</tr>
				
				<?php } }else { echo'<tr>
                <td colspan="4"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; } ?>
				
			</tbody>
        </table>
        <?php if($connect == 'yes'){?>
            <div class='disconnect-msg' style="display:none;">
                <p>Your QuickBooks integration has been disconnected. You will no longer to be able to sync data to QuickBooks from Chargezoom.</p>
                <p>If you’d like to reconnect Chargezoom and your QuickBooks account, <a href="https://support.chargezoom.com/support/solutions/articles/44002028310-how-do-i-reconnect-quickbooks-online-" target="_blank">click here to view our help guide.</a></p>
            </div>
        <?php } ?>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->
 <div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
        <div id="del_pack" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Disconnect Account</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                
					<p>Do you really want to disconnect ?</p> 
				    <div class="form-group">
                        <div class="col-md-8">
                            <input type="hidden" id="appID" name="appID" class="form-control"  value="" />
                        </div>
                    </div>
                    <div class="pull-right">
        			<input type="submit" id="disconnect-ajax" name="btn_cancel" class="btn btn-sm btn-danger" value="Disconnect"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>
 $(document).ready(function(){
    $('#disconnect-ajax').click(function(){
        $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/home/disconnet_account",
				data : {'customerID':''},
				success : function(response){
                    var obj = jQuery.parseJSON(response);
                    if(obj.status == 'success'){
                        $('#del_pack').modal('toggle');
                        $('.disconnect-app').css('display', 'none');
                        $('#qbo-reconnect').css('display','block');
                        $('.disconnect-msg').css('display','block');
                    }else{
                        alert('Something went wrong!');
                    }
                },
        });
     });
           
   
 });


</script>
<?php 
 
				if(isset($gt_result))
				{
					foreach($gt_result as $gt)
					{
						
		         	 $redirect_uri = $gt['oauth_redirect_uri']; 
						
					}
					
				}


?>
<script
      type="text/javascript"
      src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere-1.3.3.js">
 </script>

 <script type="text/javascript">
 
	var redirectUrl = "<?php echo $redirect_uri ?>";

     intuit.ipp.anywhere.setup({
             grantUrl:  redirectUrl,
             datasources: {
                  quickbooks : true,
                  payments : true
            },
             paymentOptions:{
                   intuitReferred : true
            }
     });
	 
	 

 </script>
<script>
    $(document).ready(function(){
	$('#qbo-reconnect').click(function(){
		 $('.intuitPlatformConnectButton').click();
        
	});
    });
$('.disconnect-app').click(function(){
    var token = $(this).data('token');
    $('appID').val(token);
});
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#company').dataTable({
                columnDefs: [
                  
                    { orderable: false, targets: [2] }
                ],
                searching: false,
                bInfo: false,
                paging: false,
              
            });

            / Add placeholder attribute to the search input /
        }
    };
}();




</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}
.disconnect-app{
    line-height: 0px !important;
    
}
.intuitPlatformConnectButton{
	display: none;
    margin-left: auto;
    margin-right: auto;
    width: 120px;
	line-height: 29px;
	margin-bottom: 5px;
}

#qbo-reconnect{
    width: 100px;
    cursor: pointer;
}
.disconnect-msg{
    background: #fff;
    padding: 100px;
    text-align: center;
    font-size: 15px
}
.disconnect-msg > p> a {
    color: #1bbae1;
}
.new_btn{
    text-align: right;
    margin-top: -15px;
    margin-bottom: -10px;
}
</style>
 

</div>

<div id="company_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">App Details</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_company"  style="height: 300px; min-height:300px;  overflow: auto; " >
			
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- END Page Content -->