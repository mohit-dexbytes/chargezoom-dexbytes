<style>

.nav.navbar-nav-custom > li.open > a, .nav.navbar-nav-custom > li > a:hover, .nav.navbar-nav-custom > li > a:focus, .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus, .navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus, .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus, .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus, a.sidebar-brand:hover, a.sidebar-brand:focus, a.sidebar-title:hover, a.sidebar-title:focus, #to-top:hover, .timeline-list .active .timeline-icon, .table-pricing.table-featured th, .table-pricing th.table-featured, .wizard-steps div.done span, .wizard-steps div.active span, .switch-primary input:checked + span, a.list-group-item.active, a.list-group-item.active:hover, a.list-group-item.active:focus, .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus, .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus, .nav .open > a, .nav .open > a:hover, .nav .open > a:focus, .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus, .pager > li > a:hover, .pagination > li > a:hover, .label-primary, .chosen-container .chosen-results li.highlighted, .chosen-container-multi .chosen-choices li.search-choice, .datepicker table tr td.active, .datepicker table tr td.active:hover, .datepicker table tr td.active.disabled, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled, .datepicker table tr td.active:hover.disabled, .datepicker table tr td.active.disabled.disabled, .datepicker table tr td.active.disabled:hover.disabled, .datepicker table tr td.active[disabled], .datepicker table tr td.active:hover[disabled], .datepicker table tr td.active.disabled[disabled], .datepicker table tr td.active.disabled:hover[disabled], .datepicker table tr td span.active:hover, .datepicker table tr td span.active:hover:hover, .datepicker table tr td span.active.disabled:hover, .datepicker table tr td span.active.disabled:hover:hover, .datepicker table tr td span.active:active, .datepicker table tr td span.active:hover:active, .datepicker table tr td span.active.disabled:active, .datepicker table tr td span.active.disabled:hover:active, .datepicker table tr td span.active.active, .datepicker table tr td span.active:hover.active, .datepicker table tr td span.active.disabled.active, .datepicker table tr td span.active.disabled:hover.active, .datepicker table tr td span.active.disabled, .datepicker table tr td span.active:hover.disabled, .datepicker table tr td span.active.disabled.disabled, .datepicker table tr td span.active.disabled:hover.disabled, .datepicker table tr td span.active[disabled], .datepicker table tr td span.active:hover[disabled], .datepicker table tr td span.active.disabled[disabled], .datepicker table tr td span.active.disabled:hover[disabled], .bootstrap-timepicker-widget table td a:hover, div.tagsinput span.tag, .slider-selection, .themed-background, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-results__option--highlighted[aria-selected], .nav-horizontal a:hover, .nav-horizontal li.active a {
    background-color: #16c443 !important;
}
.input-group-addon{
    background:#007aff !important;
}


.fa-search
{
    color:white !important;
}
  .w_invoice{
  
    font-size: 16px;
    font-weight: 500;
  }
  
 .b_white{
     background: white;
 }
 
 .wiget_green{

     background:#29a744;
 }
 
 .wiget_blue{

     background:#007aff;
 }
 
 
 .text_color{
     color:#ffffff;
 }
 
.block-options.process_btn {
    top:190px !important;
}
.mr-top-50{
    
    margin-top: 50px;
}

.btn_active{
    border-radius: 183px;
    width: 2px;
    height: 2px;
    padding: 4px 4px;
}
.image-wrapper .edit {
    top: 5px;
    left: 8px;
}

.image-wrapper {
    height:40px;
}
.profile-pic {
    left: 70%;
    margin-top:-30px;
    border: 20px solid white;
  }
 
 .pd-top-customer {
      padding: 30px 10px 10px 10px;
}

.bck{
    
    background-color: #f2f2f2 !important; 
}
.btn-primary2 {
    background-color: #f2f2f2 !important;
    border-color: #e91111 !important;
    color: #888888 !important;
    border:2px solid;
}

.btn-21
{
    font-size:21px;
  
}
.btn-14
{
    font-size:14px;
  
}

.btn-font-21
{
    font-size:28px;
    
}
  .input-group-addon {
  
    background-color: #f4f1f1;
  
}

.head1{
    
    color:#418eff;
}
.under_line{
    
    border-bottom:2px solid #418eff !important;
}
.hr_line{
    
    margin:0px;
    border-top: 1px solid #cdcbcb !important;
}
.pd{
     font-size: 14px;
     margin: 10px 0px 10px 0px; 
}
.btn-font-16
{
 font-size:16px;    
}
    .plan{
        font-size: 18px;
       margin: 10px 14px 10px 14px;
    }
    
    
    .btn-size{
        
       padding:8px 35px 8px 35px;
    }
</style>
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">  
 <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Customer Content -->
    
    
     <div class="row pd-top-customer">
        <div class="col-md-6">
              <!-- Customer Info Title -->
                <div class="pull-left">
                    <h4 class="head1"><strong>Customer Details </strong> </h4>
                </div>
                <!-- END Customer Info Title --> 
        </div>
        <div class="col-md-6">
              <!-- Customer Info Title -->
                <div class="pull-right">
                    <a class="btn btn-sm  btn-success" title="Create New"  href="<?php echo base_url().'QBO_controllers/Create_invoice/add_invoice/'.$customer->Customer_ListID; ?>">Add Invoice</a>
                </div>
                <!-- END Customer Info Title --> 
        </div>
    </div>    
    <div class="row">
    
        <div class="col-lg-4">
            <!-- Customer Info Block -->
            
               <div class="plan">
                 <span class="under_line"> <strong>Customer Profile</strong></span>
                 <hr class="hr_line">
                </div>
                
            <div class="block">
                
                <!-- Customer Info -->
                <div class="block-section text-center">
                    <div class="row">
                     
                     <div class="col-md-6">    
                    <div class="user-info">
						<div class="profile-pic">
								<?php if( $customer->profile_picture!=""){ ?> 
          
								<img src="<?php echo base_url(); ?>uploads/customer_pic/<?php echo  $customer->profile_picture; ?>" />
                                <?php }else{ ?>
                                <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar4@2x.jpg" />
                                <?php } ?>
							 <div class="layer">
								<div class="loader"></div>
							 </div>
							<a class="image-wrapper" href="#">
								<input class="hidden-input" id="changePicture" name="profile_picture" type="file">
								<label class="edit fa fa-pencil" for="changePicture" type="file" title="Change picture"></label>
							
							</a>
						</div>
						
						<input type="hidden" id="cr_url" value="<?php echo current_url(); ?>" />
				  </div>   
                  </div>  
                    
                  <div class="col-md-6">  
                     <h4 class="mr-top-50">
                        <strong><?php echo $customer->fullName; ?></strong>
                    </h4>
              	<a href="<?php echo base_url('QBO_controllers/Customer_Details/create_customer/'.$customer->Customer_ListID); ?>" class="btn pull-lft btn-md btn-success"> Edit Customer </a>
            
                </div>
                 </div>
                </div>
             
                
                
                
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                        
                          <?php if($this->session->userdata('logged_in')|| in_array('Send Email',$this->session->userdata('user_logged_in')['authName'])  ){ ?> 
                        <tr>
                            <td class="text-left"><strong>Email</strong></td>
                           
                            <td class="cust_view"><a href="#set_tempemail_QBO" onclick="set_template_data_qbo('<?php echo $customer->Customer_ListID; ?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->userEmail; ?>')" title="Send Email" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo $customer->userEmail; ?></a></td>
                        </tr>
                             <?php } ?>
                          
                        <tr>
                            <td class="text-left"><strong>Mobile</strong></td>
                            <td><?php echo $customer->phoneNumber; ?></td>
                        </tr>
                        
                        <tr>
                            <td class="text-left"><strong>Primary Contact</strong></td>
                            <td><?php echo $customer->firstName.' '.$customer->lastName; ?></td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Added On</strong></td>
                            <td><?php echo date('M d, Y - H:m a', strtotime($customer->createdAt)); ?></td>
                        </tr>
                         <tr>
                            <td class="text-left"><strong>Status</strong></td>
                            <td> <a href="javascript:void(0)" class="btn btn-success btn_active" data-toggle="tooltip" title="" data-original-title="Share on Twitter">
                                </a> Active</td>
                        </tr>
                       
                        <tr>
                             <td class="text-left"><strong>Payment Info</strong></td>
                             <?php if($this->session->userdata('logged_in')|| in_array('Create Card',$this->session->userdata('user_logged_in')['authName'])  ){ ?>
                            <td> <a href="#card_data_process" class="btn btn-sm btn-success"  onclick="set_card_user_data('<?php  echo $customer->Customer_ListID; ?>', '<?php  echo $customer->fullName; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
                             <?php }if($this->session->userdata('logged_in')|| in_array('Modify Card',$this->session->userdata('user_logged_in')['authName']) ){   ?>
                             <a href="#card_edit_data_process" class="btn btn-sm btn-info"   data-backdrop="static" data-keyboard="false" data-toggle="modal">View/Update</a>
                             <?php } ?>
                            </td>
                        </tr>

                     
                    </tbody>
                </table>
                <!-- END Customer Info -->
            </div>
            
             <?php if($this->session->userdata('logged_in') || in_array('Send Email',$this->session->userdata('user_logged_in')['authName']) ) { ?>
             
             
              <div class="plan">
                 <span class="under_line"> <strong>Addresses</strong></span>
                 <hr class="hr_line">
                </div>
            <!-----------------   to view email history  ------------------------->
            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                   <a href="#set_tempemail_QBO" class="btn btn-sm btn-info" onclick="set_template_data_qbo('<?php echo $customer->Customer_ListID?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->userEmail; ?>')" title="" data-backdrop="static" data-keyboard="false" data-toggle="modal">Send Email</a>
                   </div>
                    <h2>&nbsp;&nbsp;&nbsp;</h2>
                </div>
                <!-- END Products in Cart Title -->

                <!-- Products in Cart Content -->
                 <table id="email_hist11" class="table  table-bordered table-striped ecom-orders table-vcenter">
                 
                  <thead>
                        <th class="text-left"><strong>Date</strong> </th>
                        <th class="text-left"><strong>Subject</strong></th>
                            
                </thead>
                    <tbody>
                      
                        <?php   if(!empty($editdatas)){   

                        
                        foreach($editdatas as $editdata){ 
                          
                         ?>
                        <tr>
                            <td class="text-left"> <?php echo date('M d, Y', strtotime($editdata['emailsendAt'])); ?> </td>
                            
                           <td class="text-left cust_view"> <a href="#view_history" title="View Details" onclick="set_view_history('<?php echo $editdata['mailID'];?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <?php echo $editdata['emailSubject'];?> </a> </td>
                           
                           
                        </tr>
                           <?php } } ?> 
                        
                    </tbody>
                </table>
                <br>
                <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->
            
             <?php } ?>
             
            <!-- END Customer Info Block -->

            
            
            
            
            
        </div>
        
        
        
       
        <div class="col-lg-8">
                <div class="plan">
                 <span class="under_line"> <strong>Quick Stats</strong></span>
                 <hr class="hr_line">
              </div>  
            
<!-- Quick Stats Block -->
            <div class="col-md-12">
                <!-- Quick Stats Content -->
                
                 <div class="row">
                     
            <div class="col-md-6">     
                <div  class="widget  wiget_green">
                    <div class="widget-simple">
                        <div class="widget-icon pull-left">
                            <i><img src="<?php echo base_url().IMAGES; ?>payment.png"  /></i>
                        </div>
                        <h4 class="text-right  text_color">
                             <span class="w_invoice">Invoice Value</span> <br>
                            <strong>$<?php echo number_format($sum_invoice,2); ?></strong>
                           
                        </h4>
                    </div>
                </div>
             </div>
                
                
                  <div class="col-md-6">     
                    <div class="widget wiget_blue">
                        <div class="widget-simple">
                            <div class="widget-icon pull-left">
                                 <i><img src="<?php echo base_url().IMAGES; ?>bar.png"  /></i>
                            </div>
                            <h4 class="text-right text_color">
                               <span class="w_invoice">Invoices in Total</span>
                                <br>
                                <center><strong><?php echo $invoices_count; ?></strong></center>
                            </h4>
                        </div>
                    </a>
                </div>
                 
                <!-- END Quick Stats Content -->
            </div>
            </div>
            <!-- END Quick Stats Block -->
            
            
              <div class="plan">
                 <span class="under_line"> <strong>Outstanding Invoices</strong></span>
                 <hr class="hr_line">
              </div>
            <!-- Orders Block -->
            <div class="block outstand_invoice">
                <!-- Orders Title -->
                <div class="block-title">
                
                
                </div>
                <input type="hidden" id="subID" value="" >
                     
                  
                    
                     <div class=" block-options pull-right process_btn">
                            <?php   if(!empty($invoices)){  ?> 
                       <a href="#qbo_invoice_multi_process" class="btn btn-sm btn-success"  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_qbo_invoice_process_multiple('<?php echo $customer->Customer_ListID?>');">Batch Process</a>   <?php } ?>
                       <a href="#recurring_payment" class="btn btn-sm btn-info customerDetailsBtnAlingRight"  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_recurring_payment('<?php echo $customer->Customer_ListID; ?>','1');">Recurring Payments</a>
                    
                    
                    </div>
                 
                <table id="outstand" class="table cmptable  table-bordered table-striped ecom-orders table-vcenter">
                    
                        <thead>
                            <th class="text-center" >Invoice</th>
                            <th class="hidden-xs text-right">Due Date</th>
                            <th class="text-right">Amount</th>
                            <th class="text-right">Balance</th>
                            <th class="text-center hidden-xs">Status</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php   if(!empty($invoices)){     foreach($invoices as $invoice){
                        
                          
                         ?>
                        <tr>
                            <td class="text-center cust_view"><a href="<?php echo base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$invoice['invoiceID']; ?>"><strong><?php  echo $invoice['refNumber']; ?></strong></a></td>
                            <td class="hidden-xs text-right"><?php  echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
                            <td class="text-right">$<?php echo ($invoice['BalanceRemaining'])?number_format($invoice['BalanceRemaining'],2):'0.00'; ?></td>
                            <td class="text-right">$<?php  echo number_format($invoice['BalanceRemaining'],2); ?></strong></td>
                            <td class="hidden-xs text-center"><?php  echo $invoice['status'] ;  ?></td>
                           
                             
                             
                             
                            <td class="text-center">                        
                                 <div class="">
                            <?php if($invoice['BalanceRemaining'] == 0){ ?>
                            <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
                                   <li><a class="" disabled>Process</a></li>
                                      <li> <a class="" disabled>Schedule</a></li>
                                 </ul>    
                            </div>        
                             <?php }else{ ?>
                             
                             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
                                <li><a href="#qbo_invoice_process" class=""  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_qbo_invoice_process_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo
                             $invoice['CustomerListID']; ?>','<?php  echo $invoice['BalanceRemaining'] ; ?>',2);">Process</a></li>
                              <li> <a href="#qbo_invoice_schedule" onclick="set_invoice_schedule_date_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo
                             $invoice['CustomerListID']; ?>','<?php echo $invoice['BalanceRemaining']; ?>','<?php  echo date('M d Y',strtotime($invoice['DueDate'])); ?>');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                              </ul>    
                            </div>
                             <?php } ?>
                            </div>          
                            </td>
                        </tr>
                           <?php  } }
						
						 ?>    
                        

                        
                    </tbody>
                </table>
                <br>
                <!-- END Orders Content -->
            </div>
            <!-- END Orders Block -->

             <div class="plan">
                 <span class="under_line"> <strong>Paid Invoices</strong></span>
                 <hr class="hr_line">
              </div>
            <!-- Products in Cart Block -->
            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    
                    
                </div>
                <!-- END Products in Cart Title -->

                <!-- Products in Cart Content -->
                 <table  id="pinv" class="table cmptable compamount table-bordered table-striped ecom-orders table-vcenter">
                 
                  <thead>
                 
                        <th class="text-center"><strong>Invoice</strong></th>
                        <th class="hidden-xs text-right">Paid Date</th>
                        <th class="text-right hidden-xs">Amount</th>
                        <th class="text-right hidden-xs">Payments</th>
                        <th class="text-right">Balance</th>
                        <th class="text-right">Action</th>
                      
                </thead>
                    <tbody>
                      <?php   if(!empty($latest_invoice)){   
                        foreach($latest_invoice as $invoice){
                        ?>
                        <tr>
                            <td class="text-center cust_view"><a href="<?php echo base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$invoice['invoiceID']; ?>"><strong><?php  echo $invoice['refNumber']; ?></strong></a></td>
                            <td class="hidden-xs text-right"><?php   echo date('M d, Y', strtotime($invoice['TimeModified']));  ?></td>
                            <td class="hidden-xs text-right">$<?php echo number_format($invoice['Total_payment'],2); ?></td>
                            <td class="hidden-xs text-right">$<?php  echo  number_format(($invoice['Total_payment']-$invoice['BalanceRemaining']),2); ?></td>   
                            <td class="text-right">$<?php  echo number_format($invoice['BalanceRemaining'],2); ?></td>
                             <td class="text-center">
							     <div class="btn-group dropbtn">
									  <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                     <ul class="dropdown-menu text-left"> 
							             <li><a href="#payment_refunds"  onclick=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
				                	 </ul>
						         </div>
					 	    </td>
                        </tr>
                           <?php } }
					 ?>  
                        
                    
                        
                    </tbody>
                </table>
                <br>
                <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->


            
            
            <div class="plan">
                 <span class="under_line"> <strong>Subscriptions</strong></span>
                 <hr class="hr_line">
              </div>
            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        
                        <a href="<?php echo base_url(); ?>QBO_controllers/SettingSubscription/create_subscription" class="btn pull-lft  btn-sm btn-success"> Add New </a>
                    
                    </div>
                    <h2> &nbsp;&nbsp;&nbsp;&nbsp; </h2>
                </div>
                
                
       <div class="table-responsive">
        <table  id="sub_dta" class="table  table-bordered table-striped ecom-orders table-vcenter">
            <thead>
                   
                   <th class="text-left hidden-xs">Plan</th>
					 <th class="text-left hidden-xs">Card</th>
                    <th class="text-right ">Amount</th>
                    <th class="text-right hidden-xs">Next Charge</th>
                    <th class="text-center">Action</th>
            </thead>
             <tbody>
             <?php   if(!empty($getsubscriptions)){   
                        foreach($getsubscriptions as $getsubscription){ 
                         ?>
                   <tr>
                   
                    <td class="text-left hidden-xs"><?php echo $getsubscription['sub_planName']; ?>  </td> 
                    <td class="text-left hidden-xs"><?php echo $getsubscription['customerCardfriendlyName']; ?>  </td> 
                    
                 
                    
                    <td class="text-right ">$<?php echo ($getsubscription['subscriptionAmount'])?number_format($getsubscription['subscriptionAmount'],2):'0.00'; ?></td>
                   
                    <td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($getsubscription['nextGeneratingDate'])); ?></td>
                    
                
                    <td class="text-center">
                        <div class="btn-group btn-group-xs">
                            <a href="<?php echo base_url('QBO_controllers/SettingSubscription/create_subscription/'.$getsubscription['subscriptionID']); ?>" data-toggle="tooltip" title="Edit" class="btn btn-default"> <i class="fa fa-edit"></i></a>
                            
                            <a href="#del111_qbo_subs" onclick="set_qbo_subs_id('<?php  echo $getsubscription['subscriptionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a>
                           
                        </div>
                    </td>
                </tr>
                
                <?php  } }
			 ?>
                
            </tbody>
            
                </table>
                <br>
             </div>   
            </div>
<!-----------------  END --------------------->
           
           
             <div class="plan">
                 <span class="under_line"> <strong>Addresses</strong></span>
                 <hr class="hr_line">
              </div>
              
            <!-- Customer Addresses Block -->
            <div class="block">
                <!-- Customer Addresses Title -->
               
                <!-- END Customer Addresses Title -->

                <!-- Customer Addresses Content -->
                <div class="row">
                    
                    <div class="col-lg-6">
                        <!-- Shipping Address Block -->
                        <div class="block">
                            <!-- Shipping Address Title -->
                            <div class="block-title b_white">
                                <h2 >Billing Address</h2> 
                            </div>
                            <!-- END Shipping Address Title -->
                           <?php if(isset($customer->address1) && $customer->address1 != ""){ ?>    
                            <!-- Shipping Address Content -->
                            <h4><strong><?php echo $customer->fullName; ?></strong></h4>
                          <address>
                                <strong></strong>
                                <?php echo ($customer->address1)?$customer->address1:'--'; ?><br>
                                 <?php echo ($customer->address2)?$customer->address2:'--'; ?><br>
                                 <?php echo ($customer->City)?$customer->City.',':'--'; ?>
                                <?php echo ($customer->State)?$customer->State:'--'; ?>
                                <?php echo ($customer->zipCode)?$customer->zipCode:'--'; ?><br>
                                 <?php echo $customer->Country; ?><br>
                                 <br>
                                 
                                  <?php if($customer->phoneNumber!=""){ ?> 
                                <i><img src="<?php echo base_url().IMAGES; ?>phone.png"  /></i>&nbsp;&nbsp;<?php echo $customer->phoneNumber; }?><br>
                                 <?php if($customer->userEmail!=""){ ?> 
                                <i><img src="<?php echo base_url().IMAGES; ?>sms.png"  /></i>&nbsp;&nbsp;<a href="javascript:void(0)"><?php echo $customer->userEmail;} ?></a>
                                 
                                
                            </address>
                            <?php } ?>
                            <!-- END Shipping Address Content -->
                        </div>
                        <!-- END Shipping Address Block -->
                    </div>
                    
                    <div class="col-lg-6">
                        <!-- Billing Address Block -->
                        <div class="block">
                            <!-- Billing Address Title -->
                            <div class="block-title b_white">
                                <h2>Shipping Address</h2>
                            </div>
                            <!-- END Billing Address Title -->
                            <?php if(isset($customer->ship_address1) && $customer->ship_address1 != ""){ ?> 
                            <!-- Billing Address Content -->
                            <h4><strong><?php  echo $customer->fullName; ?></strong></h4>
                             <address>
                                <strong></strong>
                                <?php echo ($customer->ship_address1)?$customer->ship_address1:'--'; ?><br>
                                 <?php echo ($customer->ship_address2)?$customer->ship_address2:''; ?> <br>
                                 <?php echo ($customer->ship_city)?$customer->ship_city.',':'--'; ?>
                                <?php echo ($customer->ship_state)?$customer->ship_state:'--'; ?>
                                <?php echo ($customer->ship_zipcode)?$customer->ship_zipcode:'--'; ?><br>
                                 <?php echo $customer->ship_country; ?><br>
                                 <br>
                                <?php if($customer->phoneNumber!=""){ ?> 
                                 <i><img src="<?php echo base_url().IMAGES; ?>phone.png"  /></i>&nbsp;&nbsp;<?php echo $customer->phoneNumber; }?><br>
                                 <?php if($customer->userEmail!=""){ ?> 
                                 <i><img src="<?php echo base_url().IMAGES; ?>sms.png"  /></i>&nbsp;&nbsp;<a href="javascript:void(0)"><?php echo $customer->userEmail;} ?></a>
                            </address>
                             <?php } ?>
                            <!-- END Billing Address Content -->
                        </div>
                        <!-- END Billing Address Block -->
                    </div>
                    
                </div>
                <!-- END Customer Addresses Content -->
            </div>
            <!-- END Customer Addresses Block -->
            <?php if($this->session->userdata('logged_in')|| in_array('Add Note',$this->session->userdata('user_logged_in')['authName'] )  ) { ?>
            
            
              <div class="plan">
                 <span class="under_line"> <strong>Private Notes</strong></span>
                 <hr class="hr_line">
              </div>
              
            <!-- Private Notes Block -->
            <div class="block full">
                <!-- Private Notes Title -->
                <div class="block-title b_white">
                    <h2><strong>Add</strong> Note</h2>
                </div>
                <!-- END Private Notes Title -->

                <!-- Private Notes Content -->
                <div class="alert alert-info">
                    <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
                </div>
                <form  method="post"  id="pri_form" onsubmit="return false;" >
                    <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder=""></textarea>
                    <input type="hidden" name="customerID" id="customerID" value="<?php echo $customer->Customer_ListID ; ?>" />
                    <br>
                    <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
                </form>
                <hr>
                            
                <?php   if(!empty($notes)){  foreach($notes as $note){ ?>
                
                <div>
                   <?php echo $note['privateNote']; ?>
                </div>
                <div class="align-right">
                    <span>Added on <strong><?php   echo date('M d, Y - h:m A', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
                    <span><a href="javascript:void(0);"  onclick="delele_notes('<?php echo $note['noteID'] ; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
                </div>
                <br>
                <hr>
                <?php } } ?>
                
                
                <br>
                <!-- END Private Notes Content -->
            </div>
            
            <?php } ?>
            <!-- END Private Notes Block -->
        </div>
    </div>
    <!-- END Customer Content -->
</div>
<!-- END Page Content -->

 <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                   
                   <div id="refund_msg"></div>
                    
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>refundInvoice/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.<br>You can select only one transaction at a time.</p> 
                                           
                        <div id="ref_id">
                         
    					</div>
    					
    					<input type="hidden" id="ref_invID" name="ref_invID" value="" />
    					
                        <div class="pull-right">
            			    <input type="button" id="rf_btn" onclick="check_transaction(this);"  name="btn_cancel" class="btn btn-sm btn-warning" value="Refund"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>	

                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>

 <script src="<?php echo base_url(JS); ?>/pages/customer_details_qbo.js" ></script>
<script src="<?php echo base_url(JS); ?>/pages/qbo_subscription.js"></script>
  
                   