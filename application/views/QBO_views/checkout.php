<style>
.myblk{padding: 0px 15px;}
.top-bar {
    background: #555;
    color: #fff;
    font-size: 0.9rem;
    padding: 10px 0;
}

.top-bar .contact-info {
    margin-right: 20px;
}

.top-bar ul {
    margin-bottom: 0;
}

.top-bar .contact-info a {
    font-size: 0.8rem;
}

.top-bar ul.social-custom {
    margin-left: 20px;
}
.top-bar ul {
    margin-bottom: 0;
}

.top-bar a.login-btn i, .top-bar a.signup-btn i {
    margin-right: 10px;
}

.top-bar ul.social-custom a:hover {
    background: #4fbfa8;
    color: #fff;
}
.top-bar ul.social-custom a {
    text-decoration: none !important;
    font-size: 0.7rem;
    width: 26px;
    height: 26px;
    line-height: 26px;
    color: #999;
    text-align: center;
    border-radius: 50%;
    margin: 0;
}
a:focus, a:hover {
    color: #348e7b;
    text-decoration: underline;
}
.top-bar a.login-btn, .top-bar a.signup-btn {
    color: #eee;
    text-transform: uppercase;
    letter-spacing: 0.1em;
    text-decoration: none !important;
    font-size: 0.75rem;
    font-weight: 700;
    margin-right: 10px;
}

</style>
<div class="top-bar">
	<div class="container-fluid">
	  <div class="row d-flex align-items-center">
		<div class="col-md-6 d-md-block d-none">
		  <p><img src= "https://https://demo.payportal.com//admin/uploads/reseller_logo/1536147116logo_neww1.png" /></p>
		</div>
		
		</div>
	  </div>
	</div>
</div>
<?php
	$this->load->view('alert');
?>
<div id="page-content" style="min-height: 459px;float: left;">
  
    <form method="POST" id="thest_pay"  autocomplete="off">
	<div class="col-md-8">
	    
	    <div class="row">
		    <div class="blocker">
				<h5 class="titles">Personal Detail</h5>
				<hr>

			    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
						<label>Customer Name</label>
						<input type="text" name="cname" id="first_name" class="form-control input-sm" required>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 ">
					<div class="form-group">
						<label>Email</label><?php echo form_error('email', '<div class="servererror">', '</div>'); ?>
						<input type="email" name="email" id="last_name" class="form-control input-sm" required >
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
						<label>Username</label> <?php echo form_error('username', '<div class="servererror">', '</div>'); ?>
						<input type="text" name="username" id="first_name" class="form-control input-sm" required >
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 " >
					<div class="form-group">
						<label>Password</label>
						<input type="password"  autocomplete="off" name="password" id="last_name" class="form-control input-sm" required>
					</div>
				</div>
			</div>
		</div>
		
		
		 <div class="row">
		    <div class="blocker" style="float:left;">
				<h5 class="titles">Billing Address</h5>
				<hr>

			    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
						<label>First Name</label><?php echo form_error('first_name', '<div class="servererror">', '</div>'); ?>
						<input type="text" name="first_name" id="first_name" class="form-control input-sm" required>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
						<label>Last Name</label><?php echo form_error('last_name', '<div class="servererror">', '</div>'); ?>
						<input type="text" name="last_name" id="last_name" class="form-control input-sm" required>
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
					<div class="form-group">
						<label>Mobile No</label>
						<input type="text" name="mobile" id="last_name" class="form-control input-sm" required>
					</div>
				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-12">
					<p><hr></p>
				</div>
				
				
				<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
						<label>Country</label>
						<input type="text" name="country" id="country" class="form-control input-sm" required>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
						<label>Zip Code</label><?php echo form_error('zip', '<div class="servererror">', '</div>'); ?>
						<input type="text" name="zip" id="zip" class="form-control input-sm" required>
					</div>
				</div>
				
				
			   <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
					    <label>Street Address</label><?php echo form_error('address', '<div class="servererror">', '</div>'); ?>
						<input type="text" name="address" id="address" class="form-control input-sm" required>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
					    <label>Address Line 2</label>
						<input type="text" name="address2" id="address2" class="form-control input-sm" >
					</div>
				</div>
				
				<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
						<label>City</label><?php echo form_error('city', '<div class="servererror">', '</div>'); ?>
						<input type="text" name="city" id="city" class="form-control input-sm" required>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
					<div class="form-group">
					    <label>State</label><?php echo form_error('state', '<div class="servererror">', '</div>'); ?>
						<input type="text" name="state" id="state" class="form-control input-sm" required>
					</div>
				</div>
				
		    </div>
		</div>
		
		<!-- middle part-->
		
		<div class="row">
             <div class="blocker" >
				<h5 class="titles">Company Address</h5>
				<hr>
				
                  <p class="pgraph">Company address is used to determine applicable tax rates for your state. Is your business tax exempt? Before completing your purchase, contact Payportal Support and have us update your exemption status. </p>  
                <label>Company Address</label><br />
			<input type="checkbox" name="billingtoo" onclick="FillBilling(this.form)"><em>Check this box if Billing Address and Mailing Address are the same.</em>
				
				
				<div class="hform" id="hform" >
					<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
						<div class="form-group">
							<label>Country</label>
							<input type="text" name="scountry" id="scountry" class="form-control input-sm">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
						<div class="form-group">
							<label>Zip Code</label>
							<input type="text" name="szip" id="szip" class="form-control input-sm">
						</div>
					</div>
					
					
				   <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
						<div class="form-group">
							<label>Street Address</label>
							<input type="text" name="saddress" id="saddress" class="form-control input-sm" >
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
						<div class="form-group">
							<label>Address Line 2</label>
							<input type="text" name="saddress2" id="saddress2" class="form-control input-sm" >
						</div>
					</div>
					
					<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
						<div class="form-group">
							<label>City</label>
							<input type="text" name="scity" id="scity" class="form-control input-sm">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
						<div class="form-group">
							<label>State</label>
							<input type="text" name="sstate" id="sstate" class="form-control input-sm">
						</div>
					</div>
				
				</div>
            </div>            
            <!-- CREDIT CARD FORM ENDS HERE -->
          </div> 
		  
		  <!--midle part end-->
		
		
		
		<div class="row">
            <div class="blocker">
			
                <div class="panel-heading " >
                    <div class="row" >
                        <h3 class="panel-title pull-left" >Payment Details</h3>
                        <div class=" pull-right" >                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">
                    
                        <div class="row">
                            <div class="col-xs-12 nopadding">
                                <div class="form-group">
                                    <label for="cardNumber">Card Number</label><?php echo form_error('cardNumber', '<div class="servererror">', '</div>'); ?> 
                                    <div class="input-group">
                                        <input type="tel" class="form-control" name="cardNumber" id="card_number11" placeholder="Valid Card Number" autocomplete="cc-number" maxlength="16" required autofocus   />
                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 nopadding">
                                <div class="form-group">
                                    <label for="cardNumber">Card Friendly Name</label>
									<?php echo form_error('cardfriendlyname', '<div class="servererror">', '</div>'); ?> 
                                        <input type="text" class="form-control" name="cardfriendlyname"  placeholder="Card Friendly Name"  />
                                      
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-md-3 nopadding">
                                <div class="form-group">
                                    <label for="cardExpiry"><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">EXP</span> Month</label>
                                    <?php echo form_error('cardExpiry', '<div class="servererror">', '</div>'); ?>  
                                    <input  type="tel" class="form-control" name="cardExpiry" id="expiry11" placeholder="MM " maxlength="2" autocomplete="cc-exp" required />
                                </div>
                            </div>
							
							<div class="col-xs-3 col-md-3 nopadding">
                                <div class="form-group">
                                    <label for="cardExpiry"><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">EXP</span> Year</label>
                                    <?php echo form_error('cardExpiry2', '<div class="servererror">', '</div>'); ?> 
                                    <input type="tel" class="form-control" name="cardExpiry2" id="expiry_year11" placeholder="YY" maxlength="4" autocomplete="cc-exp" required />
                                </div>
                            </div>
							
                            <div class="col-xs-6 col-md-6 pull-right">
                                <div class="form-group">
                                    <label for="cardCVC">CV Code</label>
                                     <?php echo form_error('cardCVC', '<div class="servererror">', '</div>'); ?>
                                    <input type="tel" class="form-control" onblur="create_Token_stripe();" id="ccv11" name="cardCVC" placeholder="CVC" autocomplete="cc-csc" maxlength="4" required />
                                </div>
                            </div>
                        </div>
						<p>&nbsp;</p>
						<div class="col-xs-6 col-sm-6 col-md-6 nopadding">
						<div class="form-group">
							<input type="hidden" class="form-control" name="amount" value="<?php echo $selected_plan->subscriptionAmount; ?>" required />
								<input type="hidden" class="form-control" name="keyss" id="stripeApiKey" value="<?php echo $gateway['gatewayUsername']; ?>" required />
						</div>
					    </div>
					
					<p>&nbsp;</p>
                        <div class="row">
                            <div class="col-xs-3 nopadding">
                                <input class="subscribe btn btn-success btn-block" type="submit" value ="Pay Now" name="pay" />
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                   
                </div>
            </div>            
            <!-- CREDIT CARD FORM ENDS HERE -->
          </div>   
		</form>
		
		
	
	<div class="col-md-4">
		<div class="blocker">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="card">
					    
						<div class="header-block myblk">
						   
						  <div class="pull-left">
						  <h5 style="font-weight:bold;">Plan Summary</h5>
						  </div>
						  <div class="pull-right">
						  <h5 class="text-right"><a href="" class="clink">Change Plan</a></h5>
						  </div>
						  <div class="clearfix"></div>
						  <hr />
						</div>
						<article class="card-body">
							<div class="data-title">Plan Name</div>
							<div class="data"><?php echo $selected_plan->planName;  ?></div>
							<hr />
							
							<div class="data-title">Price</div>
							<div class="data"><?php echo '$'. $selected_plan->subscriptionAmount;  ?> <?php if( $selected_plan->freeTrial ==1){ echo '/ Free Trial'; }  ?></div>
							<p><hr /></p>
							<div class="data-title">Invoice Frequency</div>
							<div class="data"><?php echo get_uni_data('tbl_invoice_frequecy','frequencyValue',$selected_plan->invoiceFrequency,'frequencyText');  ?></div>
							<p><hr /></p>
						</article>
						
					</div> <!-- card.// -->
				</div> <!-- col.//-->
			</div> <!-- row.//-->
		</div>

		<div class="blocker">
			<div class="row justify-content-center" style="margin-top:20px;">
				<div class="col-md-12">
					<div class="card">
						<article class="card-body">
							<p class="pgraph">If you're interested in manual invoicing features, please sign up for PayPortal. </p>
							<p><hr /></p>
							
							<p class="pgraph">Looking for an annual contract? Get in touch to learn about our Enterprise Plan. </p>
							<p><hr /></p>
							
							<p class="pgraph">Feel free to contact our Support Team with any questions you may have. </p>
						</article>
					</div> <!-- card.// -->
				</div> <!-- col.//-->
			</div> <!-- row.//-->
			
		</div>
	</div>
<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
</div>

<script>


function FillBilling(f) {
  if(f.billingtoo.checked == true) {
    f.scountry.value = f.country.value;
    f.scity.value = f.city.value;
    f.sstate.value = f.state.value;
    f.szip.value = f.zip.value;
    f.saddress.value = f.address.value;
    f.saddress2.value = f.address.value;
  }
}

</script>