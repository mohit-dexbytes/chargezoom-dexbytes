<style>

.bck{
    
    background-color: #f2f2f2 !important; 
}
.btn-primary2 {
    background-color: #f2f2f2 !important;
    border-color: #e91111 !important;
    color: #888888 !important;
    border:2px solid;
}

.btn-21
{
    font-size:21px;
  
}
.btn-14
{
    font-size:14px;
  
}

.btn-font-21
{
    font-size:28px;
    
}
  .input-group-addon {
  
    background-color: #f4f1f1;
  
}

.head1{
    
    color:#418eff;
}
.under_line{
    
    border-bottom:2px solid #418eff !important;
}
.hr_line{
    
    margin:0px;
    border-top: 1px solid #cdcbcb !important;
}
.pd{
     font-size: 14px;
     margin: 10px 0px 10px 0px; 
}
.btn-font-16
{
 font-size:16px;    
}
    .plan{
        font-size: 18px;
       margin: 10px 14px 10px 14px;
    }
    
    /* remove the original arrow */
    select {
      -webkit-appearance: none;
      -moz-appearance: none;
      -o-appearance: none;
      /* no standardized syntax available, no ie-friendly solution available */
    }
    
    select + i.fa {
        float: right;
        margin-top: -27px;
        margin-right: 5px;
        font-size: 17px;
        pointer-events: none;
        background-color: #fff;
        padding-right: 5px;
      
    }
    
    /*radio css*/
    .radio-inline {
      position: relative;
      margin-bottom: 12px;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }
    
    /* Hide the browser's default radio button */
    .radio-inline input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }
    
    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 10px;
        left: 0;
        height: 13px;
        width: 13px;
        background-color: #bcb8b8;
        border-radius: 50%;
    }
    
    /* On mouse-over, add a grey background color */
    .radio-inline:hover input ~ .checkmark {
      background-color: #ccc;
    }
    
    /* When the radio button is checked, add a blue background */
    .radio-inline input:checked ~ .checkmark {
      background-color: #2196F3;
    }
    
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }
    
    /* Show the indicator (dot/circle) when checked */
    .radio-inline input:checked ~ .checkmark:after {
      display: block;
    }
    
    /* Style the indicator (dot/circle) */
    .radio-inline .checkmark:after {
     	top: 4px;
        left: 4px;
        width: 5px;
        height: 5px;
        border-radius: 50%;
        background: white;
    }
    .btn-size{
        
       padding:8px 35px 8px 35px;
    }
</style>
			
   
                            
				
 <style>
.use_vault{display:none;}
.update_vault{display:none;}
.add_vault{display:none;}
#vaul_div{display:none;}
.vaul_div{display:none;}
 
</style> 
     
	 <?php
	$this->load->view('alert');
?>
<div id="page-content">
       <div class="msg_data">
        <?php echo $this->session->flashdata('message');   ?>
        </div>
 
   <form id="form-validation" action="<?php echo base_url().'QBO_controllers/'.$gateway_url.'create_customer_sale'; ?>" method="post" >
     <div class="row">
      
          <div class="col-md-8">
         <div class="plan">
         <strong class="head1">Sale</strong>
         </div> 
         </div>
         <div class="col-md-4">
             
           
             <div class="block-options pull-right">
                                            <a href="javascript:void(0)"><img src="<?php echo base_url().IMAGES;?>cancel.png" /></a>
                                        </div>
              <div class="block-options pull-right">
                                            <a href="javascript:void(0)"><img src="<?php echo base_url().IMAGES;?>save.png" /></a>
                                        </div>
       </div>
       
       <div class="col-md-12">
        <div class="plan">
        <span class="under_line"> <strong>Payment Info</strong>
         </span>
         <hr class="hr_line">
        </div>
       </div> 
        
      
        <div class="col-md-12">
		<div class="block">
		        <div class="block-title">
             
             </div>
       
			<input type="hidden" name="invoice_id" id="invoice_ids" value="" />	
				
                 <fieldset> 
                 
				 <div class="col-md-12">
				   
				   <div class="col-md-5 form-group">
						<label class="control-label" for="customerID">Customer Name</label>
						<div >
						
						 <select id="customerID" name="customerID" class="form-control select-chosen">
                                                      
                                                        <option value>Choose Customer</option>
														<?php   foreach($customers as $customer){       ?>
														
                                                        <option value="<?php echo $customer['Customer_ListID']; ?>"><?php echo  $customer['fullName'] ; ?></option>
														<?php } ?>
                         </select>
					       
						</div>
                   </div>
                     <div class="col-md-1">
                         <label class="control-label" for="add"> </label>
                         <div class="block-options pull-right">
                                             <a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>QBO_controllers/Customer_Details/create_customer">Add New</a>
                        </div>
                    </div>
			      
                
                   <div class="col-md-6 form-group">   
                       
                       
						<label class="control-label" for="customerID">Gateway</label>
                         
                              <select id="gateway_list" name="gateway_list"  onchange="change_gatway_data();" class="form-control">
                                                        <option value="" >Select Gateway</option>
														  <?php foreach($gateways as $gateway){ ?>
                                                           <option value="<?php echo $gateway['gatewayID'];  ?>"  <?php if($gateway['set_as_default']=='1')echo "selected ='selected' ";  ?>    ><?php echo $gateway['gatewayFriendlyName']; ?></option>
														   <?php } ?>
                               </select>
							<i class="fa fa-angle-down"></i>
                         
                     </div>
			
					  <div class="col-md-12 pd"> 
				   	        <label class="control-label" for="Payment Details">Confirm Payment Details</label>
				   	</div>
				 
				
                   <div class="col-md-6 bck">
                        <div class="form-group">
                             <label class="control-label" for="customerID">Select Card</label>
						  <select id="card_list" name="card_list"  class="form-control">
                                                      
                                                        <option  value="new1" ><strong>New Card</strong></option>
                                                        
                           </select>
                           	<i class="fa fa-angle-down"></i>
					 </div>
				   </div>
				  	<div class="form-group col-md-6">
				   <div class="">
						 
					 <label class="control-label "></label>
					 
					
					
				  </div>
				</div> 
				
			</div>	
			 
				 <div class="col-md-12">
				    
				 
                   
				   <div class="col-md-6 bck">
				       <div class="form-group">
						<label class="control-label" for="customerID">Card Number
						    <img src="http://testreseller.https://demo.payportal.com//resources/img/card.png" class="img-responsive bck pull-right" style="width: 24%;">
						</label>
					     <div class="input-group">
                            
                             <input type="text" id="card_number"  data-stripe="card_number" name="card_number" class="form-control" placeholder=""  autocomplete="off">
                             <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
														
                         </div>
					
					     
                  
                   </div>
                 </div>
		
                   <div class="col-md-3">   
                       
                       <div class="form-group">
						<label class="control-label" for="customerID">Amount</label>
                         
                              <div class="input-group">
                                                        <input type="text" id="amount" name="amount" class="form-control" placeholder="">
                                                        <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>usd.png" /></span>
                             </div>
                       </div>   
                     </div>
				
				   <div class="col-md-3">
				       <div class="form-group">
						<label class="control-label" for="customerID">Currency</label>
						  
						<select id="country_code" name="country_code" class="form-control">
                                                       
                                                        <option value="USD">USD</option>
                                                        <option value="AUD">AUD</option>
                                                        <option value="CAD">CAD</option>
                                                        <option value="EUR">EUR</option>
                                                        <option value="NZD">NZD</option>
                       </select>
                           	<i class="fa fa-angle-down"></i>
					 </div>
				</div>	
				
				
		    	<div class="col-md-6 bck" > 
				  <div class="col-md-5">
						
						<label class="control-label "></label><br>
						   <span class="btn-14" ><strong> Expriry Date </strong> </span>
					 </div>
				  <div class="col-md-3">
				   <div class="form-group">
				   	 <label class="control-label ">MM </label>
					   <select id="expiry" name="expiry" data-stripe="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
													    <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                              
				   </div> 
				   </div>
				   <div class="col-md-1 bck ">
						<div class="form-group">
						<label class="control-label "></label>
						   <span class="btn-font-21" > /  </span>
					 </div>
				    </div>
				 	 
				  <div class="form-group col-md-3">
				 
						 
					 <label class="control-label "> YY</label>
					 
                    <select id="expiry_year" name="expiry_year"  data-stripe="expiry_year"  class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+25;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php  echo "20".$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>                                   
				   </div> 
				  </div>
				    <div class="form-group col-md-3">
				 
						 
					 <label class="control-label ">Surcharge Type</label>
					
					 <select id="surcharge_type" name="surcharge_type"  class="form-control">
                                                      
                                                        <option value="1">No Surcharge</option>
                                                        <option value="2">Fixed</option>
                                                        <option value="3">Percentage</option>
                      </select>
                      	<i class="fa fa-angle-down"></i>
				   </div> 
				      <div class="form-group col-md-3 ">
				 
						 
					 <label class="control-label ">Surcharge Type</label>
					
				            <div class="input-group">
                               <input type="text" id="surchargeVal" name="surchargeVal" class="form-control" readonly  placeholder="">
                                <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>usd.png" /></span>
                            </div>
				   </div> 
				   
				       	 <div class="col-md-6 bck">
				       	     <div class="form-group">
    				     	 <label class="control-label ">CARD HOLDER NAME</label>
    					          <input type="text" id="card_holder_name" name="card_holder_name" class="form-control"  autocomplete="off" placeholder=" ">
                         </div> 
                         </div>
                          <div class="form-group col-md-6">
                         <label class="control-label ">Total Amount</label>
                                                    <div class="input-group">
                                                        <input type="text" id="totalamount" name="totalamount" class="form-control" placeholder="" readonly ='readonly' >
                                                        <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>usd.png" /></span>
                                                    </div>
                      </div>
                      
                     <div class="col-md-6 bck"> 
				     <div class="col-md-5">
						<div class="form-group">
						<label class="control-label "></label><br>
						   <span class="" ><strong> </strong> </span>
					 </div>
				   </div>	 
				  <div class="col-md-2 bck">
				     <div class="form-group">
				   	 <label class="control-label "></label>
					  </div> 
                   </div> 
				   <div class="col-md-1 bck">
						<div class="form-group">
						<label class="control-label "></label>
						   <span class="btn-14" >CVV </span>
					 </div>
				 </div>
				 	 
				  <div class="col-md-3 bck">
				 
					 	 
					 <label class="control-label "> </label>
					 <input type="text" id="cvv" name="cvv" class="form-control"  autocomplete="off" placeholder="" style="margin-top:-7px;">
                                                  
				   </div> 
				    <div class="col-md-1 bck">
				     <div class="form-group">
				   	 <label class="control-label "></label>
				   	 <img src="<?php echo base_url().IMAGES;?>info.png" />
					  </div> 
                   </div> 
				  </div> 
				
				<div class="form-group col-md-6">
					 <label class="control-label ">Email Address </label>
				         <div class="input-group">
                            <input type="text" id="email" name="email" class="form-control" placeholder="">
                              <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>sms.png" /></span>
                          </div>
                   </div>  
                   
                   
                 	<div class="form-group col-md-6">
					 <label class="control-label "> </label>
				         <input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card
                   </div> 
                   
                  	<div class="form-group col-md-6">
					 <label class="control-label ">Phone Number </label>
				         <div class="input-group">
                              <input type="text" id="phone" name="phone" class="form-control" placeholder="">
                              <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>phone.png" /></span>
                          </div>
                   </div>  
                    
                   
				</div> 
		  
           </fieldset>      
        </div>
      </div>
    </div>
     <div class="row">
        <!-- Form Validation Example Block -->
        
         <div class="plan">
        <span class="under_line"> <strong>Billing Address</strong>
         </span>
         <hr class="hr_line">
        </div>
        
        <div class="col-md-12">
		<div class="block">
		   
       
	
             	<fieldset>       
				
				   
				    <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label ">Address Line 1</label>						  
						     <div class="input-group">
                                <input type="text" id="baddress1" name="baddress1" class="form-control "  placeholder="">
                                 <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>location.png" /></span>
                             </div>
				         </div>
						</div>
						
					</div>	
					
					 <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label ">Address Line 2</label>						  
						     <div class="input-group">
                                 <input type="text" id="baddress2" name="baddress2" class="form-control " placeholder="">
                                  <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>location.png" /></span>
                             </div>
				         </div>
						</div>
						
					</div>	
				
				
				<div class="form-group col-sm-3">	
						
					
							<label class="control-label"> City </label>
							 <div class="input-group">
                                  <input type="text" id="bcity" name="bcity" class="form-control input-typeahead" autocomplete="off" placeholder="">
                                   <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>city.png" /></span>
                             </div>
				</div> 	
				
				<div class="form-group col-sm-3">	
							<label class="control-label"> State/Province </label>
						        <div class="input-group">
                                                        <input type="text" id="bstate" name="bstate" class="form-control input-typeahead" autocomplete="off" placeholder="">
                                                        <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>building.png" /></span>
                            </div>
				</div>
				
				<div class="form-group col-sm-3">	
						
					
							<label class="control-label"> ZIP Code </label>
						 <div class="">
                                                        <input type="text" id="bzipcode" name="bzipcode" class="form-control" placeholder="">
                                                      
                         </div>
				</div>
				<div class="form-group col-sm-3">	
						
					
							<label class="control-label"> Country </label>
							<div class="input-group">
													<input type="text" id="bcountry" name="bcountry" class="form-control " autocomplete="off" value="" placeholder="">
													<span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>earth.png" /></span>
							</div>
				</div> 
				
			
								
						  
				 
				       
					
						</fieldset>
		
               
				
                  
                
                
                
                
                
         
          </div>      
        </div>
        </div>
        
        <div class="row">
        <!-- Form Validation Example Block -->
        
         <div class="plan">
        <span class="under_line"> <strong>Shipping Address</strong>
         </span>
         <hr class="hr_line">
        </div>
        
        
        <div class="col-md-12">
		<div class="block">
		     
               
			<fieldset>		
		        
		        
		           <div class="col-md-12 form-group ">
				      <input type="checkbox" id="chk_add_copy"> Copy from Billing Address
				   </div>
				 <br>
				 <br>
				<div class="form-group col-sm-6">	
					
				  <label  class="control-label">Company Name</label>
				   <div class="">
                      <input type="text" id="companyName" name="companyName" class="form-control" placeholder="">
                      
                  </div>
				</div>
				
				<div class="form-group col-sm-3">	
					
				  <label  class="control-label">First Name</label>
				    <div class="input-group">
                      <input type="text" id="firstName" name="firstName" class="form-control" placeholder="">
                      <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>user.png" /></span>
                     </div>
				</div>
				
			 <div class="form-group col-sm-3">	
					
				  <label  class="control-label">Last Name</label>
				    <div class="input-group">
                       <input type="text" id="lastName" name="lastName" class="form-control" placeholder="">
                       <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>user.png" /></span>
                    </div>
				</div>
				
			
				    <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label ">Address Line 1</label>						  
						     <div class="input-group">
                                 <input type="text" id="address1" name="address1" class="form-control "  placeholder="">
                                 <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>location.png" /></span>
                              </div>
				         </div>
						</div>
						
					</div>	
					
					 <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label ">Address Line 2</label>						  
						     <div class="input-group">
                                  <input type="text" id="address2" name="address2" class="form-control " placeholder="">
                                  <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>location.png" /></span>
                            </div>
				         </div>
						</div>
						
					</div>	
				  
				  
				  <div class="form-group col-sm-3">	
						
					
							<label class="control-label"> City </label>
							  <div class="input-group">
                                <input type="text" id="city" name="city" class="form-control" autocomplete="off" placeholder="">
                                 <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>city.png" /></span>
                           </div>
				</div> 	
				<div class="form-group col-sm-3">	
							<label class="control-label"> State/Province </label>
						        <div class="input-group">
                                  <input type="text" id="state" name="state" class="form-control input-typeahead" autocomplete="off" placeholder="">
                                  <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>building.png" /></span>
                            </div>
				</div>  	
				
				<div class="form-group col-sm-3">	
						
					
							<label class="control-label"> ZIP Code </label>
						 <div class="">
                             <input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="">
                            
                         </div>
				</div>
				<div class="form-group col-sm-3">	
						
					
							<label class="control-label"> Country </label>
							<div class="input-group">
								<input type="text" id="country" name="country" class="form-control " autocomplete="off" value="" placeholder="">
							    <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>earth.png" /></span>
							</div>
				</div> 
				
			 
			<div class="form-group col-sm-12">	
			<label class="control-label" for="reference">Reference Memo</label>
			 <div class="input-group">
                                                        <input type="text" id="reference" name="reference" class="form-control" placeholder="">
                                                        <span class="input-group-addon"><img src="<?php echo base_url().IMAGES;?>book.png" /></span>
                                                    </div>
				
                </div>
              <div class="form-group col-sm-12">	  
               <input type="checkbox" id="setMail" name="setMail" class="set_checkbox"   /> Send Customer Receipt   
             </div>
             
                 <div class="form-group pull-right">
					<div class="col-md-12">
					    	
					
					<button type="button"  onclick="this.form.reset()" class="btn btn-large btn-primary2 btn-size"> Reset</button>
					<button type="submit"  id="submit_btn"  class="btn btn-large btn-success btn-size"> Submit</button>
				
												
				
			
					</div>
				    </div>					
					 </div>
				</fieldset>
          </div>      
        </div>
     </div>   
   </div>
   </form>

<div id="cusIDModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <h2 class="modal-title">Pending Invoices</h2>
      </div>
      <div class="modal-body">
        <div id="table1">
        </div>
      </div>
      <div class="modal-footer">
          
          <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Select</button>
        <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>



   <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
   <script>
  
	$(document).on('change','.test',function(){
		totalCount = calculateAll();
		var price = totalCount.toFixed(2);
		$('#amount').val(price);
		$('#totalamount').val(price);
	       

	     var tmp = [];
			       $(".test").each(function(){
            		  if($(this).is(':checked'))
            		  {
            		    
            		      var checked1 = $(this).attr("rel");
            			  tmp.push(checked1);
            		  } 
		});
		
		
	       
	    
			
		$('#invoice_ids').val(tmp);
	});


        window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }

	 var gtype ='';
	
	$(function(){ 
	    nmiValidation.init(); 
	
 //This is for card saving 
    $('#tc').click(function(){
        
        if($(this).is(':checked'))
        {
          
       
        $('#frdname').hide();
        }else
         $('#frdname').show();
        
    });
	$('#customerID').change(function(){
		
		                    $('#vaultID').val('');
		                    
		                       $('#check_status').val('0');
						
		
		var cid  = $(this).val();
	
		if(cid!=""){
		    var amount = $('#amount').val(0 .toFixed(2));
			$('#totalamount').val(0 .toFixed(2));
			$('#card_list').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/Payments/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
					     if(data['status']=='success'){
						
                              var s=$('#card_list');
                               $('#table1').html('');
                               if(!jQuery.isEmptyObject(data['invoices']))
                              {
							 $('#table1').html(data['invoices']);
		                   	 $("#cusIDModel").modal("show");
                              }
		                   	 tmp=[];
		                   	 	$('#invoice_ids').val('');
							  var card1 = data['card'];
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
							    $('#companyName').val(data['companyName']);
							    $('#firstName').val(data['firstName']);
								$('#lastName').val(data['lastName']);
								$('#baddress1').val(data['address1']);
								$('#baddress2').val(data['address2']);
								$('#bcity').val(data['City']);
								$('#bstate').val(data['State']);
								$('#bzipcode').val(data['zipCode']);
								$('#bcountry').val(data['Country']);
								$('#phone').val(data['phoneNumber']);
								$('#email').val(data['userEmail']);
								$('#address1').val(data['ship_address1']);
								$('#address2').val(data['ship_address2']);
								$('#country').val(data['ship_country']);
								$('#state').val(data['ship_state']);
								$('#city').val(data['ship_city']);
								$('#zipcode').val(data['ship_zipcode']);
								
								  if($('#chk_add_copy').is(':checked')){
                             	$('#address1').val($('#baddress1').val());
								$('#address2').val(	$('#baddress2').val());
								$('#city').val($('#bcity').val());
								$('#state').val($('#bstate').val());
								$('#zipcode').val($('#bzipcode').val());
								$('#country').val($('#bcountry').val());
					         
					     }	   
					   }	   
					
				}
				
				
			});
			
		}	
    });		
	
	    
	$('#card_list').change(function(){
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			  
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					    if(data['status']=='success'){
                        
                       if(gtype==5|| ($('stripeApiKey').val()!="0")) 
                       {
                     
						 var form = $("#form-validation");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler); 
                       }
                      
		        }	   
					
				}
				
				
			});
		  }
		
	});		
        $('#friendlyname').blur(function(event) {  
	  if(gtype==5|| ($('stripeApiKey').val()!="0")) 
		{
    
		var pub_key = $('#stripeApiKey').val();
		 Stripe.setPublishableKey(pub_key);
         Stripe.createToken({
                        number: $('#card_number').val(),
                        cvc: $('#cvv').val(),
                        exp_month: $('#expiry').val(),
                        exp_year: $('#expiry_year').val()
                    }, stripeResponseHandler);

        // Prevent the form from submitting with the default action
		}
        return false;
		});
    
	
	
	var amount=0;
	$('#surcharge_type').change(function(){
		
		if($(this).val()=='1'){
			
			 $('#surchargeVal').attr('readonly','readonly');
			  $('#surchargeVal').val('0');
			var amount = $('#amount').val();
			$('#totalamount').val(amount.toFixed(2));
		}	
		if($(this).val()=='2'){
			    $('#surchargeVal').removeAttr('readonly');
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			    $('#totalamount').val(amount1.toFixed(2));

		}	
		if($(this).val()=='3'){
			 $('#surchargeVal').removeAttr('readonly');
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
				  amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	});
	
	$('#surchargeVal').change(function(){
       
	  
	   	if( $('#surcharge_type').val()=='2'){
			
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
		if( $('#surcharge_type').val()=='3'){
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					    amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	   

	});
	
	
	$('#amount').change(function(){
       
	   
	   if($(this).val()=='1'){
			 $('#surchargeVal').removeAttr('readonly');
			  $('#surchargeVal').val('0');
			var amount = $('#amount').val();
			$('#totalamount').val(amount.toFixed(2));
		}	
	  
	   	if( $('#surcharge_type').val()=='2'){
			
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
		if( $('#surcharge_type').val()=='3');{
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	   

	});
	
		 $('#chk_add_copy').click(function(){
   
     
     if($('#chk_add_copy').is(':checked')){
        
                      	$('#address1').val($('#baddress1').val());
								$('#address2').val(	$('#baddress2').val());
								$('#city').val($('#bcity').val());
								$('#state').val($('#bstate').val());
								$('#zipcode').val($('#bzipcode').val());
								$('#country').val($('#bcountry').val());
								$('#phone').val($('#bphone').val());
     }
     
 });
	
		
  $('#card_list').change( function(){
	   if($(this).val()=='new1'){
	   $('#set_credit').show();
	    $('#set_bill_data').show();
	   }else{
		      $('#card_number').val('');
	$('#set_credit').hide();
	   }
  });
	


});
	
	$.validator.addMethod('CCExp', function(value, element, params) {
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');

	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
             rules: {
                   card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
						  CCExp: {
								month: '#expiry',
								year: '#expiry_year'
						  }
					},
					
					 cvv: {
                       
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
					friendlyname:{
						required: true,
						minlength: 3,
					},
                    customerID:{
                         required: true,
                       
                    },
					 amount: {
                        required: true,
                        number:true,
                    },
                     totalamount: {
                        required: true,
                        number:true,
                    },
                    gateway_list: {
                        required: true,
                    },
                    card_list: {
                        required: true,
                    },
                     
					check_status:{
						  required: true,
					},
			    	phone: {
                        
                         minlength: 10,
                         maxlength: 15,
                         phoneUS:true,
                     },
                   
					firstName:{
					    maxlength: 100,
                        validate_char:true,
                         
					},
					lastName:{
					    maxlength: 100,
					    validate_char: true,
					},
			    	baddress1: {
			    	    maxlength: 41,
                        validate_addre:true,
                    },
                    address1: {
                        maxlength: 41,
                        validate_addre:true,
                    },
                    baddress2: {
                        maxlength: 41,
                        validate_addre:true,
                    },
                    address2: {
                        maxlength: 41,
                        validate_addre:true,
                    },
					country:{
					        maxlength: 31,
						  validate_addre:true,
					},
					bcountry:{
					      maxlength: 31,
						  validate_addre:true,
					},
					city:{
					    minlength:2,
					     maxlength: 31,
				    	validate_addre:true,
					},
					bcity:{
					     minlength:2,
					    maxlength: 31,
				    	validate_addre:true,
					},
                    state:{
                        maxlength: 21,
                        validate_addre:true,
                    }, 
                    bstate:{
                         maxlength: 21,
                        validate_addre:true,
                    },
					 email: {
                        
						email:true,
                    },
					phone: {
                       
                         minlength: 10,
                         maxlength: 15,
                         phoneUS:true,
						  
                    },
                    zipcode:{
    					
    				         	minlength:3,
						maxlength:10,
						ProtalURL:true,
						validate_addre:true
                    },
                    

                    bzipcode:{
    					 
    				         	minlength:3,
						maxlength:10,
						ProtalURL:true,
						validate_addre:true
                    },
                    

					companyName:{
    				    maxlength: 41,
                         validate_char:true,
    				    
    				}
                   
                   
                  
              },
                messages: {
					  customerID: {
                        required: 'Please select a customer',
                      
                    },
                      gateway_list: {
                        required: 'Please select the gateway',
                    },
                    card_list: {
                         required: 'Please select the card',
                        
                    },
                    expry: {
                        required: 'Please select a valid month',
                         minlength: 'Please select a valid month',
                      
                    },
					amount:{
						  required: 'Please enter the amount',
					},
					check_status:{
						 required: 'Please select the option',
					}
                  
                   
                }
            });
        }
    };
}();

  $.validator.addMethod("phoneUS", function(phone_number, element) {
         
            if(phone_number=='')
            true;
            return phone_number.match(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/);
        }, "Please specify a valid phone number");
        
        $.validator.addMethod("ProtalURL", function(value, element) {
       
        return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );                                   
      

         
          
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/,. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    

async function change_gatway_data()
{
		    var gateway_value = $('#gateway_list').val();
			
			if(gateway_value > 0){
			await  $.ajax({
              
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				  
				            data = $.parseJSON(response);
                
						 gtype  = 	data['gatewayType'];
             
							  if(gtype=='3'){			
											var url   = "<?php echo base_url()?>QBO_controllers/PaytracePayment/create_customer_sale";
										}else if(gtype=='2'){
									var url   = "<?php echo base_url()?>QBO_controllers/AuthPayment/create_customer_sale";
									}else if(gtype=='1'){
									var url   = "<?php echo base_url()?>QBO_controllers/Payments/create_customer_sale";
									}else if(gtype=='9'){
									var url   = "<?php echo base_url()?>QBO_controllers/Payments/create_customer_sale";
									}else if(gtype=='4'){
									var url   = "<?php echo base_url()?>QBO_controllers/PaypalPayment/create_customer_sale";
									}	
									else if(gtype=='5'){
									var url   = "<?php echo base_url()?>QBO_controllers/StripePayment/create_customer_sale";
									 var form = $("#form-validation");
									 
									$("#stripeApiKey").remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
									
									
									}else if(gtype=='6'){
									var url   = "<?php echo base_url()?>QBO_controllers/UsaePay/create_customer_sale";
									}										
				                     else if(gtype=='7'){
									var url   = "<?php echo base_url()?>QBO_controllers/GlobalPayment/create_customer_sale";
									}
									else if(gtype=='8'){
									var url   = "<?php echo base_url()?>QBO_controllers/CyberSource/create_customer_sale";
									}
				             $("#form-validation").attr("action",url);
					}   
				   
			   });
			}	
			
         
	
}
	
  function stripeResponseHandler(status, response) {

      
                if (response.error) {
                    // Re-enable the submit button
                    $('#submit_btn').removeAttr("disabled");
                    // Show the errors on the form

                    $('#payment_error').text(response.error.message);

                } else {
                    var form = $("#form-validation");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

                  
                }
            }
            
   	function calculateAll(){
		count = 0;
		$("input[name='inv']").each(function(index, checkbox){
		  if(checkbox.checked)
			count += parseFloat(checkbox.value);
		})
		return count;  
    }

  
		
	
	</script>