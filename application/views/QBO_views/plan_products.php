<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
 
    <!-- All Orders Block -->
         <legend class="leg">Products & Services</legend>

    <div class="block-main full" style="position: relative;">
        <!-- All Orders Title -->
        <div class="addNewFixRight">
          
             <?php if($this->session->userdata('logged_in')){ ?>
                          
                   <a class="btn btn-sm  btn-success" href="<?php echo base_url(); ?>QBO_controllers/Plan_Product/create_product_services">Add New</a>
                 
            
            <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="product" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr> 
                    <th class= "text-left">Item Name</th>
                    <th class="text-left hidden-xs">Description</th>
                    <th class="text-left">Type</th>
                    <th class="text-right hidden-xs">Purchase Cost</th>
                    <th class="text-right hidden-xs">Sale Cost</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($plans) && $plans)
				{
					foreach($plans as $plan)
					{
				?>
				<tr>
				
					<td class="text-left cust_view"><a href="<?php echo base_url('QBO_controllers/Plan_Product/create_product_services/'.$plan['productID']); ?>" data-toggle="tooltip" title="Edit" ><?php echo $plan['Name']; ?></a></td>
                    <td class="text-left hidden-xs"><?php echo $plan['SalesDescription']; ?></a></td>
					<td class="text-left"><?php echo $plan['Type']; ?></a></td>
					<td class="hidden-xs text-right">$<?php echo number_format($plan['purchaseCost'],2); ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($plan['saleCost'],2); ?></td>
				</tr>
				
				<?php } } 
				else { echo'<tr>
                <td colspan="6"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; }  
				?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<div id="plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">View Product</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_product"  style="height: 300px; min-height:300px;  overflow: auto; " >
		
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-primary1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
 
 function view_plan_details(productID){
	 
	
	 if(productID!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>QBO_controllers/Plan_Product/plan_product_details',
		  data : {'productID':productID},
		  success: function(data){
			  
			     $('#data_form_product').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 }


</script>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#product').dataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
                    "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('QBO_controllers/Plan_Product/product_ajax')?>",
                    "type": "POST" ,
                    
                    "data":function(data) {
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    
                },
                    
                },
                "order": [[0, 'asc']],
                
                    "sPaginationType": "bootstrap",
            "stateSave": false 

            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_plan_product(id,status){
    
     if(status==0)
        {
             $('#pbtn_cancel').val("Deactivate");
              $('#pbtn_cancel').removeClass("btn-success");
              $('#pbtn_cancel').addClass("btn-danger");
             $('#p_head').html("Deactivate Product / Service");
              $('#p_msg').html("Do you really want to deactivate this Product / Service?");
           
        }else{
            
              $('#pbtn_cancel').val("Activate");
             $('#pbtn_cancel').removeClass("btn-danger");
              $('#pbtn_cancel').addClass("btn-success");
             $('#p_head').html("Activate Product / Service");
              $('#p_msg').html("Do you really want to activate this Product / Service?");
            
              
              
         
        }
       $('<input>', {
                            'type': 'hidden',
                            'name': 'st_act',
                             'id': 'st_act',
                           
                        }).remove();
       $('<input>', {
                            'type': 'hidden',
                            'name': 'st_act',
                             'id': 'st_act',
                            'value': status,
                        }).appendTo($('#form_plan_product'));


	  $('#productID').val(id);
	
}


</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
</div>

<div id="del_plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="p_head">Deactivate Product & Service</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="form_plan_product" method="post" action='<?php echo base_url(); ?>QBO_controllers/Plan_Product/det_plan_product' class="form-horizontal" >
                     
                 
					<p id="p_msg">Do you really want to deactivate this Product and Service?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="productID" name="productID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="pbtn_cancel" name="pbtn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- END Page Content -->