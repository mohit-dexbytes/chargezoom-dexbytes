<style>
    body{background:#ffffff!important;}
    .backg{background:#f2f2f3;min-height:400px;border-top:1px solid #DADADA;}
    .back{background:#ffffff;width: 100%;float: left;border:1px solid #DADADA;}
    .nopadding{padding:0px;}
    .heading {padding: 8px 0 0 15px;}
    .padding{padding:15px 0px!important;}
    .myblock{background:#f2f2f3;width: 100%;float: left;}
    .myblock2{background:#ffffff;width: 100%;float: left;;border:1px solid #DADADA;margin-top:40px;}
    .policy{margin:3px 5px;}
    .myblock3 {float: left;width: 100%;margin: 30px 0px 50px 0px;}
    .btn-primary{background:#2676A4;border:1px solid #2676A4;}
    @media (min-width:992px)
    {
        .container{width:786px}
    }
@media (min-width:1200px) and (max-width:1400px)
    {
        .container{width:786}
    }
</style>

<div class="container">
    <header>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="navbar-brand-centered">
                <img src="<?php echo base_url().'resources/css/logo.png';  ?>" class="img-responsive center-block"  />
            </div>
        </div>
        <div class="col-md-4"></div>
     </header>
</div>

<div class="container-fluid backg">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Sean Hannity TV Show Monthly Podcast (Auto Renew)</h3>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <h3 class="text-right">$5.50 / 1 Month</h3>
            </div>
        </div>
        
        <div class="row">
            <form method="post">
            <div class="back">
                <div class="col-md-12 nopadding">
                    
                  <div class="heading">
                       <h5> <strong>Contact Information</strong></h5>
                  </div>
                  <hr/>
                  
                      <!--personal detail-->
                      <div class="container">
                           <div class="row nopadding">
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>FIRST NAME</label>
            						<input type="text" name="fname" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>LAST NAME</label>
            						<input type="text" name="lname" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>EMAIL</label>
            						<input type="text" name="email" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                          </div>
                      </div>
                       
                      <hr/>
                      
                      <!--card detail-->
                      <div class="container">
                           <div class="row nopadding">
                               <div class="col-md-12">
                                    <img src="<?php echo base_url().'resources/css/card.png';  ?>" class="img-responsive" style="width:30%;padding-bottom:20px;" />
                               </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>CARD NUMBER</label>
            						<input type="text" name="card" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>CARD FRIENDLY NAME</label>
            						<input type="text" name="cardfriendlyname" id="friendlyname" class="form-control input-sm">
            						<?php echo form_error('cardfriendlyname', '<div class="servererror">', '</div>'); ?> 
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>EXPIRATION MONTH</label>
            						<input type="text" name="exp" id="cname" class="form-control input-sm" placeholder="12">
            					 </div>
                              </div>
                              
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>EXPIRATION YEAR</label>
            						<input type="text" name="exp2" id="cname" class="form-control input-sm" placeholder="2020">
            					 </div>
                              </div>
                              
                              <div class="col-md-3 col-md-offset-3">
                                  <div class="form-group">
            						<label>CCV</label>
            						<input type="text" name="ccv" id="cname" class="form-control input-sm" placeholder="123">
            					 </div>
                              </div>
                         </div>
                    </div>
                            
                    <div class="container">
                          <div class="row nopadding">
                              <div class="col-md-6 col-md-offset-right-6">
                                  <div class="form-group">
            						<label>COUNTRY</label>
            						<input type="text" name="country" id="cname" class="form-control input-sm" >
            					 </div>
                              </div>
                          </div>
                    </div>
                    
                    <div class="container">
                           <div class="row nopadding">          
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>STREET ADDRESS</label>
            						<input type="text" name="add" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>ADDRESSS 2</label>
            						<input type="text" name="add2" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                              
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>CITY</label>
            						<input type="text" name="city" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>STATE</label>
            						<input type="text" name="state" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>ZIP/POSTAL</label>
            						<input type="text" name="zip" id="cname" class="form-control input-sm">
            					 </div>
                              </div>
                          </div>
                      </div>
                      
                      <hr/>
                      <!--shipping address-->
                      <div class="heading" style="padding-bottom:20px;">
                          <h5> <strong>Shipping Information</strong></h5>
                      </div>
                      <div class="shipping">
                         
                           <div class="container">  
                               <div class="row nopadding">
                                  <div class="col-md-6">
                                      <div class="form-group">
                						<label>STREET ADDRESS</label>
                						<input type="text" name="sadd" id="cname" class="form-control input-sm">
                					 </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                						<label>ADDRESSS 2</label>
                						<input type="text" name="sadd2" id="cname" class="form-control input-sm">
                					 </div>
                                  </div>
                                  
                                  <div class="col-md-6">
                                      <div class="form-group">
                						<label>CITY</label>
                						<input type="text" name="scity" id="cname" class="form-control input-sm">
                					 </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                						<label>STATE</label>
                						<input type="text" name="sstate" id="cname" class="form-control input-sm">
                					 </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                						<label>ZIP/POSTAL</label>
                						<input type="text" name="szip" id="cname" class="form-control input-sm">
                					 </div>
                                  </div>
                              </div>
                          </div>
                          
                          <div class="container">
                              <div class="row nopadding">
                                  <div class="col-md-6 col-md-offset-right-6">
                                      <div class="form-group">
                						<label>CONTACT</label>
                						<input type="text" name="contact" id="cname" class="form-control input-sm">
                					 </div>
                                  </div>
                              </div>
                         </div>
                         
                      </div>
                </div>  
                
                
                <div class="myblock">
                    <div class="col-md-6">
                        <p style="padding-top:10px">Sub Total</p>
                        <p><strong>Order Total</strong></p>
                    </div>
                     <div class="col-md-6">
                        <p class="text-right" style="padding-top:10px"><strong>$5.95 USD</strong></p>
                        <p class="text-right"><strong>$5.95 USD</strong></p>
                    </div>
                </div>
             </div>
             
             
                <div class="myblock2">
                    <div class="col-md-12">
                        <p style="padding-top:10px"><label>TERMS AND PRIVACY</label></p>
                        <p><input type="checkbox" name="accept" value="accept" /><span class="policy">I accept the <a href="">Privacy Policy</a> and <a href="">Terms of service.</a></span></p>
                    </div>
                    
                </div>
                 <div class="myblock3">
                    <div class="col-md-6">
                        <input type="submit" name="submit" value="Subscribe" class="btn btn-primary" />
                    </div>
                    <div class="col-md-6">
                        <p style="margin-top: 23px;" class="text-right">Powered by <a href="">ABC Company</a></p>
                    </div>
                    
                </div>
              </form>
         </div>
     </div>
</div>