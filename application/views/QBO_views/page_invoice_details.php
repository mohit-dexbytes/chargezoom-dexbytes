<?php 
  	  
  
	    $status ='';
        $today =date('Y-m-d');
        
        $due_date = date('Y-m-d', strtotime($invoice_data['DueDate'])); 
	
	    $type ='';
	
     $date1=strtotime($due_date);  
      $date2=strtotime($today); 
	  
	  $type_text='';
	  
	  if($date1 >= $date2)
	  {
	      $diff= $date2 - $date1;

	      $diff =floor($diff / (60*60*24) );
	      $type = 3;
	      $type_text = "Due Amount";
	  }
	  else
	  {
        $type = 2;
        $type_text = "Invoice Past Due";
      }
    
    $colSpan = 3;
    $colSpanStyle = 'style="display:none"';
    if(isset($invoice_data) && $invoice_data['taxID'] != '' ){
        $colSpan = 4;
        $colSpanStyle = '';
    }
	if ($this->session->userdata('logged_in')) {
        $merchantEmail = $this->session->userdata('logged_in')['merchantEmail'];
    }
    if ($this->session->userdata('user_logged_in')) {
        $merchantEmail = $this->session->userdata('user_logged_in')['userEmail'];
    }
    if ($invoice_data['BalanceRemaining'] > 0) {
        $displayStatus = 1;
        $readonly = '';
    }else{
        $displayStatus = 0;
        $readonly = 'readonly';
    }
?>
<?php
	$this->load->view('alert');
?>
<style>
.block-options{
    margin-top: 10px;
}
</style>
<div id="page-content">
     <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Order Status -->

    <!-- END Order Status -->

    <!-- Products Block -->
    <legend class="leg">Invoice Details</legend>
    <div class="block">
        <!-- Products Title -->
        
			 
         <div class="table-responsive">

	    <div class="block-options pull-right">
            <a href="<?php echo base_url(); ?>QBO_controllers/Create_invoice/invoice_details_print/<?php echo  $invoice_data['invoiceID']; ?>" class="btn btn-alt btn-sm btn-danger" data-toggle="tooltip" title="Print Invoice">Download PDF</a>
             <?php	 if($invoice_data['IsPaid']=='1'){  ?>  
            <a href="javascript:void(0);" title="Send Email" disabled    class="btn btn-sm btn-info"  data-backdrop="static" data-keyboard="false">Email Invoice</a>
			<?php } else {   ?>  
			  <a href="#set_tempemail_data_ttt" class="btn btn-sm btn-info"  onclick="set_template_data_temp('<?php echo  $invoice_data['invoiceID']; ?>','<?php echo  $invoice_data['CustomerListID']; ?>','<?php echo $type; ?>');"  data-backdrop="static" data-keyboard="false" data-toggle="modal" data-original-title="Send Email" >Email Invoice</a>
			
				<?php } ?>
				
				  <div class="btn-group dropbtn">
                        <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                        <ul class="dropdown-menu text-left">
                <?php	 if($invoice_data['IsPaid']=='0'){  ?>  
                    <li><a href="#qbo_invoice_schedule" onclick="set_invoice_schedule_date_id('<?php  echo $invoice_data['invoiceID']; ?>','<?php  echo
                    $invoice_data['CustomerListID']; ?>','<?php echo $invoice_data['BalanceRemaining']; ?>','<?php  echo date('M d Y',strtotime($invoice_data['DueDate'])); ?>');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                    <li> <a href="#invoice_cancel"  class=""  onclick="set_invoice_id('<?php  echo $invoice_data['invoiceID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>
                    <?php }else if ($invoice_data['BalanceRemaining'] == "0.00" and $invoice_data['IsPaid'] == '1') { ?>
                      <li>
                        <a href="javascript:void(0);" id="txnRefund<?php echo $invoice_data["invoiceID"]; ?>" invoice-id="<?php echo $invoice_data["invoiceID"]; ?>" integration-type="1" data-url="<?php echo base_url(); ?>ajaxRequest/getTotalRefundOfInvoice" class="refunTotalInvoiceAmountCustom">Refund</a>
                        </li>

                   <?php  } ?>
                </ul>
                </div>
			</div> 
              <form id="form-validation" action="<?php echo base_url(); ?>QBO_controllers/Create_invoice/edit_custom_invoice"  method="post" enctype="multipart/form-data" class="form-horizontal ">
	
             <div class="">
             
                <h4>Invoice Number: <strong><?php  echo  $invoice_data['refNumber']; ?></strong></h4>
                <h5>Customer Name:  <strong><?php echo  ucwords($invoice_data['cust_data']['fullName']); ?></strong></h5>
               <h5>Invoice Date:   <div class="input-group input-date col-md-3">
						
                                <input type="text" id="invDate" name="invDate" class="form-control input-datepicker"  value="<?php  echo date('m/d/Y', strtotime($invoice_data['TimeCreated'])); ?>" data-date-format="mm/dd/yyyy" placeholder="Invoice Date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div></h5>
                <h5>Invoice Due Date:  <div class="input-group input-date col-md-3">
						
                                <input type="text" id="dueDate" name="dueDate" class="form-control input-datepicker"  value="<?php  echo date('m/d/Y', strtotime($invoice_data['DueDate'])); ?>" data-date-format="mm/dd/yyyy" placeholder="Invoice Due Date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div></h5>
                </h5>
            
                 <h5>Invoice Status:  <strong> <?php  if($invoice_data['BalanceRemaining'] =="0.00" and $invoice_data['IsPaid']=='1'){  ?> Paid <?php }else 
                 if(($invoice_data['Total_payment'] !=  $invoice_data['BalanceRemaining']) && $invoice_data['IsPaid']!='1' ){  ?> Partial <?php }else	
                 if($invoice_data['IsPaid']=='0' && ($invoice_data['Total_payment'] ==  $invoice_data['BalanceRemaining'])){	?> Unpaid <?php } ?>
                 
                 </strong></h5>
                
                 <h5>Payment Method: <strong > 
                    <?php
                        if(!empty($transaction['transactionType']) && $transaction['transactionType'] =='Offline Payment'){
                            echo 'Check'; 
                        }else if((isset($transaction['paymentType']) && $transaction['paymentType'] == 2  )){ 
                            echo 'Electronic Check';
                        }else if(empty($transaction['transactionType']  )){ 
                            echo '--';
                        }else{ 
                            echo'Credit Card';
                        }   
                    ?>
                     </strong></h5>
                     <?php if($ur_data){ ?>
                        <h5>Preview Invoice Page: <strong class="cust_view"><?php    echo '<a target="_blank" href="'.$paylink.'">'.$paylink.'</a>'; ?> </strong></h5>
                
                    <?php }else{ ?>
                        <h5>Preview Invoice Page: <strong class="cust_view"><?php echo '<a href="javascript:void(0);" rel="txtTooltip" data-toggle="tooltip" title="To activate this page, you must enable the Customer Portal for your account.">' . $paylink . '</a>'; ?></strong></h5>
                    <?php } ?>
             </div>
             
            <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th style="width: 280px;">Product / Service</th>
                         <th class="text-left"> Description</th>
                          <th class="text-right">Unit Rate</th>
                        <th class="text-right">Qty</th>
                        <th class="text-right set_taxes" <?php echo $colSpanStyle; ?>>Tax</th>

						<th class="text-right">Amount</th>
					
                    </tr>
                </thead>
                <tbody id="item_fields">
                    
                    
				 <?php 	$totaltax =0 ; $total=0; $tax=0; 
				 foreach($invoice_data['l_items'] as $key=> $item){    $total+= $item['itemQty']*$item['itemPrice'];
                    
                    if($item['Type'] == 'Group'){
                        $readonly = 'readonly';
                    }
                   ?>   
                    <tr class="removeclass<?php echo $key+1; ?>  rd ">
                        <td>
								  <select class="form-control"  onchange="select_plan_val('<?php echo $key+1; ?>');"  id="productID<?php echo $key+1; ?>" name="productID[]">
									
								  <?php foreach($plans as $plan){
                                    $selectedOpt = '';
                                    if($plan['productID']==$item['itemRefID']){
                                        $selectedOpt = 'selected';
                                    }
                                    if( ($plan['IsActive'] == 'false') && ($plan['productID']==$item['itemRefID'])){
                                        
                                        echo '<option value="'.$plan['productID'].'" selected > '.$plan['Name'].' </option>';

                                    }else if($plan['IsActive'] == 'true'){
                                        echo '<option value="'.$plan['productID'].'" '.$selectedOpt.'  > '.$plan['Name'].' </option>';
                                    }
                                   ?>
								  <?php }  ?>
								   </select>
                        </td>
                        
                        <td>
                            <?php 
                                $descrip_readonly = '';
                                if($item['itemDescription'] == 'Surcharge Fees'){
                                    $descrip_readonly = 'readonly';
                                } 
                            ?>
                            <input type="text" <?php echo $readonly; ?>  id="description<?php echo $key+1; ?>" name="description[]" value="<?php echo  $item['itemDescription']; ?>" class="form-control" <?php echo $descrip_readonly; ?> /> 
                        </td>
                        <td class="text-right">$<span id="rate<?php echo $key+1; ?>"> <?php echo $item['itemPrice']; ?>  </span></td>
						<input type="hidden" id="unit_rate<?php echo $key+1; ?>"  name="unit_rate[]" value="<?php echo $item['itemPrice']; ?>" />
                        <td class="text-right"><div class="pull-right"><input <?php echo $readonly; ?> type="text" size="6"  onblur="set_qty_val('<?php echo $key+1; ?>');" id="quantity<?php echo $key+1; ?>" name="quantity[]" value="<?php echo $item['itemQty'];  ?>" class="form-control text-right" /></div> </td>
						
                        <td class='set_taxes text-right' <?php echo $colSpanStyle; ?>>
                            <label class="custom_checkbox">
                                <input type="checkbox" onkeypress="return isNumberKeys(event)" id="tax_check<?php echo $key+1; ?>" onchange="set_tax_val(this, <?php echo $key+1; ?>)" name="tax_check[]" class="tax_checked" value="" <?php if($invoice_data['BalanceRemaining'] == '0'){ echo 'disabled'; }?> <?php if($item['itemTax'] == '1'){ echo 'checked'; }?>>
                                <span class="geekmark"></span>
                            </label>
                            <div class="form-group"> 
                                <input type="hidden" id="is_tax_check<?php echo $key+1; ?>" name="is_tax_check[]" value="<?php echo $item['itemTax']; ?>">
                            </div>
                        </td>

                        <td class="text-right"><?php   echo '<input type="hidden" class="form-control total_val" id="total'.($key+1).'" name="total[]" value="'.($item['itemQty']*$item['itemPrice']).'"/>';  ?> $<span id="total11<?php echo $key+1; ?>"><?php echo number_format($item['itemQty']*$item['itemPrice'],2); ?> </span></td>
						
                        <input type="hidden" id="isEditableInvoice" value=" <?php if($invoice_data['BalanceRemaining'] == '0'){ echo '0'; } else { echo '1'; } ?>">
                   
					
                    </tr>
					
                 <?php } ?> 
                 
                 	<tr class="active">
                       <td> 
                            <?php if($displayStatus){ ?>
                                <button class="btn btn-sm btn-success" type="button"  onclick="item_invoice_fields();"> Add More </button>
                            <?php } ?>
                        </td>
                        <td colspan="<?php echo $colSpan; ?>" class="text-right text-uppercase colspan_d_3"><strong>SUBTOTAL</strong></td>
                        <td class="text-right">$<?php echo '<span id="sub_total" > '.number_format(($invoice_data['Total_payment']-$invoice_data['totalTax']), 2).'</span>' ; ?></td>
                    </tr>
                 
                 
                 <tr>
                        <td class="text-left"><select name="taxes" id="taxesee" onchange="set_tax_valnew();" class="form-control ">
							 <option value="" >Select Tax</option>
							 <?php  foreach($taxes as $tax){ ?>
							 <option  value="<?php echo $tax['taxID']; ?>"
							 <?php if(isset($invoice_data) && $tax['taxID']==$invoice_data['taxID'] ){echo "selected";} ?>><?php echo $tax['friendlyName']; ?> </option>
							 
							 <?php } ?>
							   </select> </td><td colspan="<?php echo $colSpan; ?>" class="text-right text-uppercase colspan_d_3" ><span id="taxv_rate"><strong>TAX (<?php echo $invoice_data['taxRate']; ?>%) </strong></span></td>
                        <td class="text-right"><?php echo '$'. '<span id="tax_val" >'.number_format($invoice_data['totalTax'],2).'</span>'; ?></td>
                    </tr>
               
				<tr class="info">
                        <td colspan="<?php echo $colSpan+1; ?>" class="text-right text-uppercase colspan_d_4"><strong>TOTAL</strong></td>
                        <td class="text-right">$<?php echo '<span id="total_amt">'.number_format($invoice_data['Total_payment'],2).'</span>';  ?></td>
                    </tr>
                    <tr class="success">
                        <td colspan="<?php echo $colSpan+1; ?>" class="text-right text-uppercase colspan_d_4"><strong>PAID</strong></td>
                        <td class="text-right">$<?php echo number_format(($invoice_data['Total_payment']-$invoice_data['BalanceRemaining']),2);  ?></td>
                    </tr>
					<tr class="danger">
                        <td colspan="<?php echo $colSpan+1; ?>" class="text-right text-uppercase colspan_d_4"><strong>BALANCE</strong></td>
                        <td class="text-right">$<?php  $total = ($invoice_data['BalanceRemaining'])?$invoice_data['BalanceRemaining']:'0.00';  echo '<span id="grand_total">'.$total.'</span>'; ?></td>
                    </tr>
                </tbody>
				
            </table>
            </div>
             <input type="hidden" id="invNo" name="invNo" value="<?php  echo $invoice_data['invoiceID']; ?>" />
              <input type="hidden" id="txID" value="<?php echo $invoice_data['taxRate']; ?>" />
				<div class="col-md-12">
					
					<div class="pull-right">
					 <?php if($invoice_data['BalanceRemaining']!='0.00'){ $dis=''; ?>
				
					<a href="#qbo_invoice_process" class="btn btn-sm btn-success"  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_qbo_invoice_process_id('<?php  echo $invoice_data['invoiceID']; ?>','<?php  echo
                             $invoice_data['CustomerListID']; ?>','<?php  echo $invoice_data['BalanceRemaining'] ; ?>',3);">Process</a>
					<?php  if($invoice_data['IsPaid']=='0' && ($invoice_data['Total_payment'] !=  $invoice_data['BalanceRemaining'])){  $dis='disabled';  } ?> 
					<input type="button" <?php echo $dis; ?> name="inv_exit" class="btn  testbtn btn-sm btn-primary"  value="Save & Exit" />
					<input type="button" <?php echo $dis; ?>  name="inv_save" class="btn testbtn btn-sm btn-primary" value="Save" />
       
					 <?php }    ?><br><br>
					  </div>	
				
				    
				</form>	  
					
			</div>
			
        </div>
        <!-- END Products Content -->
    </div>  
    <!-- END Products Block -->

    <!-- Addresses -->
    <?php  
        $BillingAdd = 0;
        $ShippingAdd = 0;

        $isAdd = 0;
        if($invoice_data['BillAddress_Addr1']  || $invoice_data['BillAddress_Addr2'] || $invoice_data['BillAddress_City'] || $invoice_data['BillAddress_State'] || $invoice_data['BillAddress_PostalCode'] || $invoice_data['BillAddress_Country']){
            $BillingAdd = 1;
            $isAdd = 1;
        }
        if($invoice_data['ShipAddress_Addr1']  || $invoice_data['ShipAddress_Addr2'] || $invoice_data['ShipAddress_City'] || $invoice_data['ShipAddress_State'] || $invoice_data['ShipAddress_PostalCode'] || $invoice_data['ShipAddress_Country'] ){
            $ShippingAdd = 1;
            $isAdd = 1;
        }

    ?>
    <div class="row">
      <?php if($isAdd){ ?>
        <?php if($BillingAdd){ ?>
        <div class="col-sm-6">
            <!-- Billing Address Block -->
            <div class="block">
                <!-- Billing Address Title -->
                <div class="block-title">
                    <h2><strong>Billing</strong> Address</h2>
                </div>
                <!-- END Billing Address Title -->

                <!-- Billing Address Content -->
                 <h4><strong>
                 <?php if($invoice_data['cust_data']['firstName']!=''){ echo $invoice_data['cust_data']['firstName'].' '.$invoice_data['cust_data']['lastName']; }else{
               echo ucwords($invoice_data['cust_data']['fullName']); 
                }
                ?>
                </strong></h4>
            		<address>
                                
                  <?php echo( $invoice_data['BillAddress_Addr1'])?$invoice_data['BillAddress_Addr1'].'<br>':''; ?>
                  <?php echo( $invoice_data['BillAddress_Addr2'])?$invoice_data['BillAddress_Addr2'].'<br>':''; ?> 
                  <?php echo ($invoice_data['BillAddress_City'])?$invoice_data['BillAddress_City'].',':''; ?>
                  <?php echo ($invoice_data['BillAddress_State'])?$invoice_data['BillAddress_State']:''; ?> 
                  <?php echo ($invoice_data['BillAddress_PostalCode'])?$invoice_data['BillAddress_PostalCode'].'<br>':''; ?> 
                  <?php echo ($invoice_data['BillAddress_Country'])?$invoice_data['BillAddress_Country'].'<br>':''; ?> 
                  <br>                  
            			<i class="fa fa-phone"></i> <?php echo $invoice_data['cust_data']['phoneNumber'] ;      ?><br>
            			<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $invoice_data['cust_data']['userEmail'];    ?></a>
            		</address>
               
                <!-- END Billing Address Content -->
            </div>
            <!-- END Billing Address Block -->
        </div>
        <?php }
        if($ShippingAdd){ ?>
        <div class="col-sm-6">
            <!-- Shipping Address Block -->
            <div class="block">
                <!-- Shipping Address Title -->
                <div class="block-title">
                    <h2><strong>Shipping </strong> Address</h2>
                </div>
                <!-- END Shipping Address Title -->

                <!-- Shipping Address Content -->
                
                
                  <h4><strong>
                <?php if($invoice_data['cust_data']['firstName']!=''){ echo $invoice_data['cust_data']['firstName'].' '.$invoice_data['cust_data']['lastName']; }else{
               echo ucwords($invoice_data['cust_data']['fullName']); 
                }
                ?>
                </strong></h4>
                <address>
        					
                      <?php echo( $invoice_data['ShipAddress_Addr1'])?$invoice_data['ShipAddress_Addr1'].'<br>':''; ?>
                      <?php echo( $invoice_data['ShipAddress_Addr2'])?$invoice_data['ShipAddress_Addr2'].'<br>':''; ?> 
                      <?php echo ($invoice_data['ShipAddress_City'])?$invoice_data['ShipAddress_City'].',':''; ?>
                      <?php echo ($invoice_data['ShipAddress_State'])?$invoice_data['ShipAddress_State']:''; ?> 
                      <?php echo ($invoice_data['ShipAddress_PostalCode'])?$invoice_data['ShipAddress_PostalCode'].'<br>':''; ?> 
                      <?php echo ($invoice_data['ShipAddress_Country'])?$invoice_data['ShipAddress_Country'].'<br>':''; ?> 
                      
                   <br>                  
        					<i class="fa fa-phone"></i> <?php echo $invoice_data['cust_data']['phoneNumber'] ;      ?><br>
        					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $invoice_data['cust_data']['userEmail'];    ?></a>
        				</address>
               
                <!-- END Shipping Address Content -->
            </div>
            <!-- END Shipping Address Block -->
        </div>
        <?php } ?>
    <?php } ?>
    </div>
    <!-- END Addresses -->

    <!-- Log Block -->
    <div class="block full">
                <!-- Private Notes Title -->
                <div class="block-title">
                    <h2><strong>Private</strong> Notes</h2>
                </div>
                <!-- END Private Notes Title -->

                <!-- Private Notes Content -->
                <div class="alert alert-info">
                    <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
                </div>
                <form  method="post"  id="pri_form" onsubmit="return false;" >
                    <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="Your note.."></textarea>
                   <input type="hidden" name="customerID" id="customerID" value="<?php echo $invoice_data['cust_data']['Customer_ListID']; ?>" />
					<br>
                    <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
                </form>
				<hr>
				
		
                      <?php   if(!empty($notes)){  foreach($notes as $note){ 
                        if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                            $timezone = ['time' => $note['privateNoteDate'], 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                            $note['privateNoteDate'] = getTimeBySelectedTimezone($timezone);
                        }
                        ?>
                
                <div>
                   <?php echo $note['privateNote']; ?>
                </div>
                <div class="pull-right">
                    <span>Added on <strong><?php   echo date('M d, Y - h:i A', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
                    <span><a href="javascript:void(0);"  onclick="delele_notes('<?php echo $note['noteID'] ; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
                </div>
                <br>
                <hr>
                <?php } } ?>

				<br>
				<hr>
				
				
				<br>
                <!-- END Private Notes Content -->
            </div>
 </div>



<script src="<?php echo base_url(JS); ?>/pages/customer_details_qbo.js?v=1.0" ></script>
<script>
 $('.testbtn').click(function(){
	 var form_data=$('#form-validation').serialize();
	var index = ''; 
	   if($(this).val()=='Save')
	   {
			index ="self";
	   }
		else{
		index ="other";	
		}
								$('#index').remove(); 
			 	 
                                    
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'index',
											'name': 'index',
										
											'value':index ,
											}).appendTo($('#form-validation'));
	
	$('#form-validation').submit();
	
 });
</script>

 <div id="set_tempemail_data_ttt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title text-center">Send Email</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"> </label>
				
                  <form id="form-validation1" action="<?php echo base_url(); ?>QBO_controllers/Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
				  
				   <input type="hidden" id="invoicetempID" name="invoicetempID" value=""> 
			    <input type="hidden" id="customertempID" name="customertempID" value=""> 
				<input type="hidden" id="invoiceCode" name="invoiceCode" value=""> 
					<input type="hidden" id="sendmailbyinvdtl" name="sendmailbyinvdtl" value="1">
			<input type="hidden" id="fromEmail" name="fromEmail" value=""> 
			
                   <input type="hidden" name="type" id="type" value="<?php echo $type ?>" />                
										
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                         <input type="text" name="type_text"  class="form-control"   readonly='readonly' id="type_text" value="<?php echo $type_text ?>"   />
                                          
                                            
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="form-group" style="display:none;" id="reply_div">
                                        <label class="col-md-3 control-label" for="replyEmail">Reply-To Email</label>
                                        <div class="col-md-7">
                                            <input type="text" id="replyTo" name="replyEmail" class="form-control" value="<?php if (isset($templatedata) && $templatedata['replyTo'] != ''){
                                                echo $templatedata['replyTo'];
                                            }else{  echo $merchantEmail;} ?>" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="form-group" style="display:none" id="from_email_div">
                                        <label class="col-md-3 control-label" for="templteName">From Email</label>
                                        <div class="col-md-7">
                                            <input type="text" id="fromEmail" name="fromEmail"  value="<?php if (isset($templatedata) && $templatedata['fromEmail'] != ''){
                                                echo $templatedata['fromEmail'];
                                            }else{  echo DEFAULT_FROM_EMAIL;} ?>" class="form-control" placeholder="From Email">
                                        </div>
                                    </div>
                                    <div class="form-group" id='display_name_div' style='display:none'>
                                        <label class="col-md-3 control-label" for="templteName">Display Name</label>
                                        <div class="col-md-7">
                                            <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="<?php echo $mailDisplayName; ?>" placeholder="Display Name">
                                        </div>
                                    </div>
                               
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7" style="font-size: 12px;">
                                            <a href="javascript:void(0);"  id="open_reply">Add Reply-To<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                            <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                            <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                            <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                                           
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="CC Email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="BCC Email">
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="col-md-3 control-label" >Attach Invoice PDF</label>
                                        <div class="col-md-7">
                                           <label class="switch switch-info"><input type="checkbox" name="add_attachment"<?php echo "checked"; ?> id="add_attachment"><span></span></label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
									
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
										
										<input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="Email Subject">
                                    </div>
										
                                    </div>

                                   
                                     
                                      <div class="form-group">
									  
									  
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-7 col-md-offset-3 align-right">
                                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                        
                                       
                                    </div>
                                </div>  
                           </form>
		    	
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>