   <!-- Page content -->
<?php
	$this->load->view('alert');
?>
	<div id="page-content">
	   
	
<style> .error{color:red; }</style>  
    <!-- Progress Bar Wizard Block -->
    <legend class="leg"><?php if(isset($customer)){echo "Edit Customer";} else {echo "Create New Customer";} ?></legend>
    
	         
        <!-- Progress Bar Wizard Content -->
		
		<form  id="customer_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>QBO_controllers/Customer_Details/create_customer">
			<div class="block">
			   <input type="hidden"  id="customerListID" name="customerListID" value="<?php if(isset($customer)){echo $customer['Customer_ListID']; } ?>" /> 
			 	
			
			    <div class="form-group">
							<label class="col-md-3 control-label" for="example-username">Company Name</label>
							<div class="col-md-7">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($customer)){echo $customer['companyName']; } ?>"><?php  if(form_error('companyName'))echo "<div class='error' style='color:red'> This field is required</div>"; ?>
							</div>
					</div>
                            
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Customer Name<span class="text-danger">*</span></label>
					<div class="col-md-7">
					<input type="text" id="fullName" name="fullName" class="form-control"  value="<?php if(isset($customer)){echo $customer['fullName']; } ?>"><?php if(form_error('fullName'))echo "<div class='error' style='color:red'> This field is required</div>";?> </div>
					</div>	
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">First Name </label>
					<div class="col-md-7">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($customer)){echo $customer['firstName']; } ?>"><?php if(form_error('firstName'))echo "<div class='error' style='color:red'> This field is required</div>"; ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Last Name </label>
					<div class="col-md-7">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($customer)){echo $customer['lastName']; } ?>"><?php if(form_error('lastName'))echo "<div class='error' style='color:red'> This field is required</div>"; ?> </div>
					</div>
					
					
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="example-username">Email Address</label>
					<div class="col-md-7">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($customer)){echo $customer['userEmail']; } ?>"><?php if(form_error('userEmail'))echo "<div class='error' style='color:red'> This field is required</div>"; ?> </div>
					</div>
					<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">Phone Number</label>
							<div class="col-md-7">
								<input type="text" id="phone" name="phone" class="form-control" value="<?php if(isset($customer)){echo $customer['phoneNumber']; } ?>">
							</div>
						</div>
						
				
				
			</div>
			<legend class="leg"> Billing Address</legend>	 
			    <div class="block" id="set_bill_data">
                    
				
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 1</label>
						<div class="col-md-7">
							<input type="text" id="address1" name="address1"   value="<?php if(isset($customer)){echo $customer['address1']; } ?>" class="form-control" ><?php if(form_error('companyAddress1'))echo "<div class='error' style='color:red'> This field is required</div>";?>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 2</label>
						<div class="col-md-7">
							<input type="text" id="address2" name="address2"  class="form-control"  value="<?php if(isset($customer)){echo $customer['address2']; } ?>"  >
						</div>
					</div>
					
					
                        
                       <div class ="form-group">
						   <label class="col-md-3 control-label" name="city"> City</label>
						   <div class="col-md-7">
						       <input type="text" id="city" class="form-control" name="companyCity" value="<?php if(isset($customer)){ echo $customer['City']; } ?>">
							
							</div>
                        </div>
						
                        
                        <div class ="form-group">
						   <label class="col-md-3 control-label"  name="state">State</label>
						   <div class="col-md-7">
						       <input type="text" id="state" class="form-control" name="companyState" value="<?php if(isset($customer)){ echo $customer['State']; } ?>">
							
							</div>
                        </div>	
						<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">ZIP Code</label>
							<div class="col-md-7">
								<input type="text" id="zipCode" name="zipCode" class="form-control" value="<?php if(isset($customer)){echo $customer['zipCode']; } ?>"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
						<div class ="form-group">
						   <label class="col-md-3 control-label">Country</label>
						   <div class="col-md-7">
						     <input type="text" id="country" class="form-control" name="companyCountry" value="<?php if(isset($customer)){ echo $customer['Country']; } ?>">
						    
							</div>
                        </div>
                    </div>
                 
                    <legend class="leg">Shipping Address</legend>
                    <div class="block">       
                     	 <div class="form-group"> <div class="col-md-12">
						  <input type="checkbox" id="chk_add_copy"> Same as Billing Address 
						</div></div> 
					<div style="margin-top: 16px;" class="form-group ">
						<label class="col-md-3 control-label" for="example-username">Address Line 1</label>
						<div class="col-md-7">
							<input type="text" id="saddress1" name="saddress1"   value="<?php if(isset($customer)){echo $customer['ship_address1']; } ?>" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="example-username">Address Line 2</label>
						<div class="col-md-7">
							<input type="text" id="saddress2" name="saddress2"  class="form-control"  value="<?php if(isset($customer)){echo $customer['ship_address2']; } ?>"  >
						</div>
					</div>
					   <div class ="form-group">
						   <label class="col-md-3 control-label" name="scity">City</label>
						   <div class="col-md-7">
						       	<input type="text" id="scity" class="form-control" name="sCity" value="<?php if(isset($customer)){ echo $customer['ship_city']; } ?>">
								
							</div>
                        </div>
                        <div class ="form-group">
						   <label class="col-md-3 control-label"  name="sstate">State</label>
						   <div class="col-md-7">
						       <input type="text" id="sstate" class="form-control " name="sState" value="<?php if(isset($customer)){ echo $customer['ship_state']; } ?>">
							
							</div>
                        </div>
				
						<div class="form-group">
							<label class="col-md-3 control-label" for="example-email">ZIP Code</label>
							<div class="col-md-7">
								<input type="text" id="szipCode" name="szipCode" class="form-control" value="<?php if(isset($customer)){echo $customer['ship_zipcode']; } ?>"><?php echo form_error('zipCode'); ?>
							</div>
						</div>
					
						<div class ="form-group">
						   <label class="col-md-3 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-7">
						    
								<input type="text" id="scountry" class="form-control " name="sCountry" value="<?php if(isset($customer)){ echo $customer['ship_country']; } ?>">
							</div>
                        </div>	
                        
				</fieldset>
                <div class="form-group">
                	<div class="col-md-3"></div>
					<div class="col-md-7 text-right">
				  		
				
						<button type="submit" class="submit btn btn-sm btn-success">Save</button>
                    	<a href="<?php echo base_url(); ?>QBO_controllers/Customer_Details/customer_details" class=" btn btn-sm btn-primary1">Cancel</a>
					</div>
			    </div>	  
	                			
					
		
		
  	</form>
		
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->
<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>


<!-- END Page Content -->


<script>

$(document).ready(function(){

 
    $('#customer_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'companyName': {
                     minlength: 2,
                     maxlength: 41,
                    },
                    'fullName': {
                         required: true,
                        minlength: 2,
                        maxlength: 41,
                        validate_char:true,
                         remote: {
                                      url: base_url+'ajaxRequest/check_customer_data',
                                    type: "POST",
                                    cache: false,
                                    dataType: 'json',
                                    data: {
                                        customer: function(){ return $("#fullName").val(); },
                                         customerID: function(){ return $("#customerListID").val(); }
                                    },
                                    dataFilter: function(response) {
                                     
                                       var rsdata = jQuery.parseJSON(response)
                                      
                                         if(rsdata.status=='success')
                                         return true;
                                         else
                                         return false;
                                    }
                                },
                               
                    },
					  'firstName': {
                        minlength: 2,
                        maxlength: 100,
                        validate_char:true,
                    },
					 'lastName': {
                    minlength: 2,
                    maxlength: 100,
                    validate_char:true,
                    },
					
					'userEmail':{
						  email:true,
						  
					},
			
					  
                     'address1': {
			    	   minlength: 2,
			    	    maxlength: 41,
                    },
                    'saddress1': {
                        minlength: 2,
                        maxlength: 41,
                    },
                    'address2': {
                        minlength: 2,
                        maxlength: 41,
                    },
                    'saddress2': {
                        maxlength: 41,
                    },
					'companyCountry': {
					         minlength: 2,
					        maxlength: 31,
					},
					'sCountry': {
					      minlength: 2,
					      maxlength: 31,
					},
					'companyCity': {
                         minlength: 2,
					     maxlength: 31,
					},
					'sCity': {
                        minlength: 2,
					    maxlength: 31,
					},
                    'companyState': {
                       minlength: 2,
                        maxlength: 21,
                    }, 
                    'sState': {
                        minlength: 2,
                        maxlength: 21,
                    },
					'phone': {
                       minlength: 10,
                         maxlength: 17,
                         phoneUS:true,
						  
					},
				
                    'zipCode': {
						minlength:3,
						maxlength:10,
                    },
					 'szipCode': {
					minlength:3,
						maxlength:10,
                    }
					
			},
			messages: {
           companyCountry: {
            },
           companyState: {
            },
            companyCity: {
            },
            
            	fullName:{
						
                        remote:"This customer name already exist"
					},
            
			}
    });
    
    
    
   		$.validator.addMethod("phoneUS", function(value, element) {
         
         if(value=='')
         return true;
        return  value.match(/^\d{10}$/) || 
    value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
   
        }, "Please specify a valid phone number");
                           
          $.validator.addMethod("ZIPCode", function(value, element) {
         return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
           },"Only alphanumeric and hyphen is allowed" );
                           
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[A-Za-z0-9-&,'._* ]*$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9][a-zA-Z0-9-_/,.# ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
         
    
    
      $('#chk_add_copy').click(function(){
   
     if($('#chk_add_copy').is(':checked')){
        
                            	$('#saddress1').val($('#address1').val());
								$('#saddress2').val($('#address2').val());
								$('#scity').val($('#city').val());
								$('#sstate').val($('#state').val());
								$('#szipCode').val($('#zipCode').val());
								$('#scountry').val($('#country').val());
     }
    else{
    	 
         var val_sp='';
                            	$('#saddress1').val(val_sp);
								$('#saddress2').val(val_sp);
								$('#scity').val(val_sp);
								$('#sstate').val(val_sp);
								$('#szipCode').val(val_sp);
								$('#scountry').val(val_sp);
    }
     
 });
    
 
    
});	

</script>

</div>


