   <!-- Page content -->
<?php
	$this->load->view('alert');
?>
   <div id="page-content">
	 <!-- END Wizard Header -->
	 <!-- Progress Bar Wizard Block -->
<legend class="leg"> <?php if(isset($item_pro)){ echo "Edit Product";}else{ echo "Create New Product"; }?></legend>	 
	 <div class="block">
					<?php
						 $message = $this->session->flashdata('message');
						 if(isset($message) && $message != "")
						 echo $message;
					 ?>
		 <!-- Progress Bar Wizard Content -->
		 <div class="row">
		 
			  <form  id="product_form" method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>QBO_controllers/Plan_Product/create_product_services">
			 
			  <input type="hidden"  id="productID" name="productID" value="<?php if(isset($item_pro)){echo $item_pro['productID']; } ?>" /> 
			  
				 <div class="form-group">
					 <label class="col-md-3 control-label" for="example-username">Product Name</label>
					 <div class="col-md-7">
						 <input type="text" id="productName" name="productName" class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['Name']; } ?>">
					 </div>
				 </div>
					 
					 
				 <div class="form-group">
					 <label class="col-md-3 control-label" for="example-username">SKU </label>
					 <div class="col-md-7">
					 <input type="text" id="sku" name="sku" class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['SKU']; } ?>"> </div>
				 </div>
				 
				 <div class ="form-group">
						<label class="col-md-3 control-label" for="example-typeahead">Parent Name</label>
						<div class="col-md-7">
							 <select id="productParent" class="form-control " name="productParent">
							 <option value="">Select Parent</option>
							 <?php foreach($parent_list as $parent){  ?>
								<option value="<?php echo $parent['productID']; ?>" <?php  if(isset($item_pro) && $parent['productID']==$item_pro['parent_ListID'] ) echo"Selected" ?> > <?php echo $parent['Name']; ?> </option>
								
							 <?php } ?>  
							 
							 </select>
						 </div>
					 </div>
					 
					 <div class="form-group">
					 <label class="col-md-3 control-label" for="example-type">Type </label>
					 <div class="col-md-7">
							 <select id="ser_type" class="form-control " name="ser_type">
							 <option value="">Select Type</option>
						 
							<option value="Service"<?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Service') ) echo"Selected" ?>>Service</option>
							 <option value="Inventory" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Inventory') ) echo"Selected" ?>>Inventory</option>
							 <option value="Group" <?php  if(isset($item_pro) && strtoupper($item_pro['Type'])==strtoupper('Group') ) echo"Selected" ?> >Group</option>
                           
							</select>
						 </div>
					 </div>
					 
					 
					<!-- Service Type Product Begins --> 
					 <div id="ServiceType" class="type productType" <?php if(isset($item_pro) && (strtoupper($item_pro['Type'])==strtoupper('Service')) ){  ?> style="display:block" <?php }else{ ?> style="display:none" <?php } ?> >
						  <div class="form-group">
							 <label class="col-md-3 control-label" for="example-username">Sales Price </label>
							 <div class="col-md-7">
								 <input type="text" id="salePrice_serv" name="salePrice_serv"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['saleCost']; } ?>">
							 </div>
						 </div>
					 
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">Income account</label>
							 <div class="col-md-7">
						 
						 <select id="incomeAccount_serv" class="form-control " name="incomeAccount_serv">
							 <option value="">Select Type </option>
							 <?php foreach($account_list as $account){  ?>
							 
								<option value="<?php echo $account['accountID']; ?>" <?php  if(isset($item_pro) && $account['accountID']==$item_pro['IncomeAccountRef'] ) echo"Selected" ?>> <?php echo $account['accountName'] .' / '.$account['accountType']; ?> </option>
								
							 <?php } ?>  
							 
							 </select>
						 </div>
				 </div>
					 
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-type">Sales Product Description </label>
						 <div class="col-md-7">
							 <textarea id="saleProDescription_serv" name="saleProDescription_serv" class="form-control"  ><?php if(isset($item_pro)){echo $item_pro['SalesDescription']; } ?> </textarea>
						 </div>
					 </div>
					 
				  <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">Purchase Price </label>
						 <div class="col-md-7">
							 <input type="text" id="purchasePrice_serv" name="purchasePrice_serv"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['saleCost']; } ?>" >
						 </div>
					 </div>
					 
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">Expense account </label>
						 <div class="col-md-7">
						 
						 <select id="expenseAccount_serv" class="form-control " name="expenseAccount_serv">
							 <option value="">Select Type</option>
							 <?php foreach($account_list as $account){  ?>
							 
								<option value="<?php echo $account['accountID']; ?>" <?php  if(isset($item_pro) && $account['accountID']==$item_pro['ExpenseAccountRef'] ) echo"Selected" ?>> <?php echo $account['accountName'].' / '.$account['accountType']; ?> </option>
								
							 <?php } ?>  
							 
							 </select>
						 </div>
						 </div>
					 
						 <div class="form-group">
							 <label class="col-md-3 control-label" for="example-type">Purchase Product Description </label>
							 <div class="col-md-7">
							 <textarea id="purProDescription_serv" name="purProDescription_serv" class="form-control"  ><?php if(isset($item_pro)){echo $item_pro['purchaseDesc']; } ?> </textarea></div>
						 </div>
					 </div>
					<!-- Service Type Product Begins -->
					<!-- Inventory Type Product Begins -->	 
					 <div id="qty" class="type productType" <?php if(isset($item_pro) && (strtoupper($item_pro['Type'])==strtoupper('Inventory')) ){  ?> style="display:block" <?php }else{ ?> style="display:none" <?php } ?> >
						 <div class="form-group">
							 <label class="col-md-3 control-label" for="example-username">Quantity On Hand</label>
							 <div class="col-md-7">
								 <input type="text" id="quantityonhand" name="quantityonhand"  class="form-control"  value="<?php if(isset($item_pro)){echo $item_pro['QuantityOnHand']; } ?>">
							 </div>
						 </div>
					 
				 <?php if(!isset($item_pro)){  ?>	
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">As of date</label>
						 <div class="col-md-7">
							 <input class="datepicker" id="datePick" name="datePick" data-date-format="mm/dd/yyyy">
						 </div>
					 </div>
					 <?php } ?>
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">Sales Price </label>
						 <div class="col-md-7">
							 <input type="text" id="salePrice" name="salePrice"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['saleCost']; } ?>">
						 </div>
					 </div>
					 
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">Inventory Asset Account </label>
						 <div class="col-md-7">
							 <select id="AssetAccountRef" class="form-control " name="AssetAccountRef">
							 <option value="">Select Type</option>
							 <?php foreach($inventory_account as $invAccount){  ?>
							 
								<option value="<?php echo $invAccount['accountID']; ?>" <?php  if(isset($item_pro) && $invAccount['accountID']==$item_pro['AssetAccountRef'] ) echo"Selected" ?>> <?php echo $invAccount['accountName']; ?> </option>
								
							 <?php } ?>  
							 
							 </select>
						 </div>
					 </div>
					 
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">Income Account</label>
							 <div class="col-md-7">
						 
						 <select id="incomeAccount" class="form-control " name="incomeAccount">
							 <option value="">Select Type </option>
							 <?php foreach($inventory_income as $income){  ?>
							 
								<option value="<?php echo $income['accountID']; ?>" <?php  if(isset($item_pro) && $income['accountID']==$item_pro['IncomeAccountRef'] ) echo"Selected" ?>> <?php echo $income['accountName']; ?> </option>
								
							 <?php } ?>  
							 
							 </select>
						 </div>
					 </div>
					 
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-type">Sales Product Description </label>
						 <div class="col-md-7">
							 <textarea id="saleProDescription" name="saleProDescription" class="form-control"  ><?php if(isset($item_pro)){echo $item_pro['SalesDescription']; } ?> </textarea>
						 </div>
					 </div>
					 
				  <div class="form-group">
					 <label class="col-md-3 control-label" for="example-username">Purchase Price </label>
					 <div class="col-md-7">
							 <input type="text" id="purchasePrice" name="purchasePrice"  class="form-control" value="<?php if(isset($item_pro)){echo $item_pro['saleCost']; } ?>">
					 </div>
				 </div>
					 
					 <div class="form-group">
						 <label class="col-md-3 control-label" for="example-username">Expense Account </label>
						 <div class="col-md-7">
						 
						 <select id="expenseAccount" class="form-control " name="expenseAccount">
							 <option value="">Select Type</option>
							 <?php foreach($inventory_expense as $expense){  ?>
							 
								<option value="<?php echo $expense['accountID']; ?>" <?php  if(isset($item_pro) && $expense['accountID']==$item_pro['ExpenseAccountRef'] ) echo"Selected" ?>> <?php echo $expense['accountName']; ?> </option>
								
							 <?php } ?>  
							 
							 </select>
						 </div>
					 </div>
					 
				 <div class="form-group">
					 <label class="col-md-3 control-label" for="example-type">Purchase Product Description </label>
					 <div class="col-md-7">
					 <textarea id="purProDescription" name="purProDescription" class="form-control"  ><?php if(isset($item_pro)){echo $item_pro['purchaseDesc']; } ?> </textarea></div>
				 </div>
				 
				 
				 </div>
				 <!-- Inventory Type Product Ends -->
				 <!-- Group Type Product Begins -->
				 <div  id="group_div" class="type productType"  <?php if(isset($item_pro) && (strtoupper($item_pro['Type'])==strtoupper('Group')) ){   ?> style="display:block;" <?php }else{ ?> style="display:none;" <?php } ?> >
					<?php

					// SHow Existing Group Prroducts
					if(isset($item_data) && !empty($item_data))
					{
						foreach($item_data as $k=>$group_d)
						{  
					?>
						<div class="form-group  removeclass<?php echo $k+1; ?>">
							<div class="col-sm-3 ">
								<div class="nopadding">&nbsp;</div>
							</div>
							<div class="col-sm-2 nopadding">
								<div class="">
									<select class="form-control"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="prodID<?php echo $k+1; ?>" name="prodID[]">
										<option>Select Product & Service</option>
										<?php foreach ($items as $k1=> $item){  ?> 
										<option  <?php if($group_d['itemListID']==$item['ListID']){ echo "selected='selected'"; } ?> value="<?php echo $item['ListID'] ?>"><?php echo $item['Name'];  ?></option> 
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-2 nopadding">
								<div class="">
									<input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo  $group_d['FullName']; ?>" >
								</div>
							</div>
							<div class="col-sm-1 nopadding">
								<div class=" text-center">
									<input type="text" class="form-control float text-center" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo  $group_d['Price']; ?>"  >
								</div>
							</div>
							<div class="col-sm-1 nopadding">
								<div class=" text-center">
									<input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity" name="quantity[]" value="<?php  echo $group_d['Quantity']; ?>" >
								</div>
							</div>							   
						
						
							<div class="col-sm-2 nopadding">
								<div class="">
									<div class="input-group">
										<div class="input-group-btn">
											<button class="btn btn-danger" type="button" onclick="remove_product_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button>
										</div>
									</div>
								</div>
							</div> 
							<div class="clear"></div>
						</div>
						
					<?php  
						} 
					} else { 
					?>
						<div class="form-group  removeclass1">
							<div class="col-sm-3 ">
								<div class="nopadding">
								&nbsp;
								</div>
							</div>
							<div class="col-sm-2 nopadding">
								<div class="">
									<select class="form-control"  onchange="select_plan_val('1');"  id="prodID1" name="prodID[]">
										<option>Select Product & Service</option>
										<?php foreach ($items as $k1=> $item){  ?>
										<option value="<?php echo $item['ListID'] ?>"><?php echo $item['Name'];  ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						
							<div class="col-sm-2 nopadding">
								<div class="">
									<input type="text" class="form-control" id="description1" name="description[]" value="" >
								</div>
							</div>
							<div class="col-sm-1 nopadding">
								<div class=" text-center">
									<input type="text" class="form-control float text-center" id="unit_rate1" name="unit_rate[]" value="" >
								</div>
							</div>
							<div class="col-sm-1 nopadding">
								<div class=" text-center">
									<input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center"  id="quantity1" name="quantity[]" value="" >
								</div>
							</div>
							<div class="col-sm-2 nopadding">
								<div class="">
									<div class="input-group">
										<div class="input-group-btn">
											<button class="btn btn-danger" type="button" onclick="remove_product_fields('1');"> <span class="fa fa-times" aria-hidden="true"></span></button>
										</div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					<?php } ?>
					<div id="item_fields"></div>
					<div class="form-group">
						<label class="control-label "></label>						  
						<div class="col-md-9 col-md-offset-3">
							<button class="btn btn-sm btn-success" type="button"  onclick="item_fields();"> Add More </button>
						</div>
					</div>
				</div>
				<!-- Group Type Product Ends -->
				
				 <div class="form-group text-right">
				 	<div class="col-md-10">
					   
					   <button type="submit" class="submit btn btn-sm btn-success">Save</button><?php 
							if(isset($item_pro)){
								$product_id = $item_pro['productID'];
							 	if($item_pro['IsActive'] == 'false'){ ?>
							 		<a href='#del_plan_product' onclick='set_del_plan_product("<?php echo $product_id; ?>", 1);' data-backdrop='static' data-keyboard='false' data-toggle='modal' class='btn btn-sm btn-info'>Activate</a>
					 	<?php	}else{ ?>
								 	<a href='#del_plan_product' onclick='set_del_plan_product("<?php echo $product_id; ?>", 0)' data-backdrop='static' data-keyboard='false' data-toggle='modal' class='btn btn-sm btn-danger'>Deactivate</a>
					 <?php		}
						 	} ?>		
						<a href="<?php echo base_url(); ?>QBO_controllers/Plan_Product/Item_detailes" class=" btn btn-sm btn-primary1">Cancel</a>		
				  	</div>
					 </div>					
					 
				 
			 </div>
			 
			   </form>
		 
		 </div>
		 <!-- END Progress Bar Wizard Content -->
	 </div>
	<!-- END Progress Bar Wizard Block -->
	<div id="del_plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header text-center">
					<h2 class="modal-title" id="p_head">Deactivate Product & Service</h2>
					
					
				</div>
				<!-- END Modal Header -->

				<!-- Modal Body -->
				<div class="modal-body">
					<form id="form_plan_product" method="post" action='<?php echo base_url(); ?>QBO_controllers/Plan_Product/det_plan_product' class="form-horizontal" >
						
					
						<p id="p_msg">Do you really want to deactivate this Product and Service?</p> 
						
						<div class="form-group">
						
							<div class="col-md-8">
								<input type="hidden" id="QBO_ProductID" name="productID" class="form-control"  value="" />
							</div>
						</div>
						
						
				
						<div class="pull-right">
						<input type="submit" id="pbtn_cancel" name="pbtn_cancel" class="btn btn-sm btn-danger" value="Deactivate"  />
						<button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
						</div>
						<br />
						<br />
				
					</form>		
					
				</div>
				<!-- END Modal Body -->
			</div>
		</div>
	</div>						 
 
 <!-- END Page Content -->
 <script type="text/javascript">
	 $('.datepicker').datepicker({
	 format: 'mm/dd/yyyy'
	 //startDate: '-3d'
 });
 </script>
 <script>
 
 $(document).ready(function(){
 
	$('#ser_type').change( function(){

		jQuery('.productType').hide();
		
		switch(jQuery(this).val()){
			case 'Group':
				$("#group_div").show();
			break;
			case 'Inventory':
				$("#qty").show();
			break;
			default:
				$("#ServiceType").show();
			break;
		}
	});
 
 
	 $('#product_form').validate({ // initialize plugin
		 ignore:":not(:visible)",			
		 rules: {
					  'sku': {
						 required: true,
						 validate_addre: true,
					 },
					  
					  'productName': {
						 required: true,
						 minlength: 3,
						 validate_char:true,
						 maxlength: 100,
					 },
					 
					 'averageCost':{
						   required:true,
						   number: true,
						   
					 },
					 
					 'quantityonhand':{
						 required: true,
						   digits: true,
					 },
					 'ser_type':{
						 required: true,
						   
					 },
					 'purProDescription_serv':{
						   required: true,
							  minlength: 3
					 },
					 'saleProDescription_serv':{
						   required: true,
							 minlength: 3
					 },
					 'purProDescription':{
						   required: true,
						 minlength: 3
					 },
					 'saleProDescription':{
						   required: true,
						  minlength: 3
					 },
					 'purchasePrice':{
						 required: true,
						 number: true
					 },
					 'salePrice':{
						 required: true,
						 number: true
					 },
					 'purchasePrice_serv':{
						 required: true,
						 number: true
					 },
					 'salePrice_serv':{
						 required: true,
						 number: true
					 },
					 
					 
			 },
	 });
	 
   
	 
	 
 });	
 
 $.validator.addMethod("validate_addre", function(value, element) {
 
	   return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/, ]+$/i.test(value);
 
   }, "This field contain only letters, numbers, or underscore.");
 
  $.validator.addMethod("validate_char", function(value, element) {
	 
		   return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
	 
	   }, "Please enter only letters, numbers, space, hyphen or underscore.");
	
	var room = <?php echo count($items); ?>;
  	function item_fields()
	{ 
	   
       var type='group';
                
        $.ajax({
			url:'<?php echo base_url(); ?>QBO_controllers/Plan_Product/get_plan_data_item',
			type:"POST",
			data:{type:type},
			success:function(data){
				var plan_data = $.parseJSON(data);
				
				room++;

				var plan_html ='<option val="">Select Product or Service</option>';
				for(var val in  plan_data) 
        		{
           			plan_html+='<option value="'+ plan_data[val]['ListID']+'">'+plan_data[val]['Name']+'</option>'; 
        		}
				var objTo = document.getElementById('item_fields')
				var divtest = document.createElement("div");
				divtest.setAttribute("class", "form-group removeclass"+room);
				var rdiv = 'removeclass'+room;
				divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class=""></div></div><div class="col-sm-2 nopadding"><div class=""><select class="form-control"  onchange="select_plan_val('+room+');"  id="prodID'+room+'" name="prodID[]">'+ plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class=""> <input type="text" class="form-control" id="description'+room+'" name="description[]" value=""></div></div><div class="col-sm-1 nopadding"><div class=""> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate'+room+'" name="unit_rate[]" value="" ></div></div><div class="col-sm-1 nopadding"><div class=""> <input type="text" onkeypress="return isNumberKey(event)" class="form-control"  id="quantity'+room+'" name="quantity[]" value=""></div></div>  	     <div class="col-sm-2 nopadding"><div class=""><div class="input-group"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_product_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
				objTo.appendChild(divtest);
        
       		}
          
        }); 
  	}
  
  
   function remove_product_fields(rid)
	{
		$('.removeclass'+rid).remove();
	}

	function select_plan_val(rid)
	{
		var itemID = $('#prodID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>QBO_controllers/Plan_Product/get_item_data",
			data: {'itemID':itemID },
			dataType:'JSON',
			success:function(itemData){
				if(itemData.status == 'success') {
				 	$('#description'+rid).val(itemData.data.Name);
             		$('#unit_rate'+rid).val(itemData.data.SalesPrice);
			 		$('#quantity'+rid).val(itemData.data.QuantityOnHand);
				
				}
			
			}	
		});
		
	}
	function set_del_plan_product(id,status){
    
		if(status==0)
		{
			$('#pbtn_cancel').val("Deactivate");
			$('#pbtn_cancel').removeClass("btn-info");
			$('#pbtn_cancel').addClass("btn-danger");
			$('#p_head').html("Deactivate Product / Service");
			$('#p_msg').html("Do you really want to deactivate this Product / Service?");
		}else{
				
			$('#pbtn_cancel').val("Activate");
			$('#pbtn_cancel').removeClass("btn-danger");
			$('#pbtn_cancel').addClass("btn-info");
			$('#p_head').html("Activate Product / Service");
			$('#p_msg').html("Do you really want to activate this Product / Service?");
		}

		$('<input>', {
			'type': 'hidden',
			'name': 'st_act',
			'id': 'st_act',
		}).remove();

		$('<input>', {
			'type': 'hidden',
			'name': 'st_act',
			'id': 'st_act',
			'value': status,
		}).appendTo($('#form_plan_product'));

		$('#QBO_ProductID').val(id);
	}
 </script>
 
 
 
 