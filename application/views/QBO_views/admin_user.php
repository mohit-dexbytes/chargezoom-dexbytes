
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">

    <!-- All Orders Block -->
    <div class="block-main full">
	    <?php
            $message = $this->session->flashdata('message');
            if(isset($message) && $message != "")
            echo ($message);
           ?>       
        <!-- All Orders Title -->
        <legend class="leg">User Management</legend>
        <div class="full">
	           
        <!-- All Orders Title -->
        <div style="position: relative">
							
							
                        <div class="addNewFixRight width160FixRight">
                            <a class="btn btn-sm btn-primary" title="Create User" href="<?php echo base_url(); ?>QBO_controllers/MerchantUser/admin_role">User Roles</a>
	
                            <?php if($plantype){ 
                                if(isset($user_data) && $user_data)
				                {

                                    if($plantype['merchant_plan_type'] == 'AS' && count($user_data) < 9){
                                        echo '<a class="btn pull-lft btn-sm btn-success" title="Create User"   href="'.base_url().'QBO_controllers/MerchantUser/create_user">Add User</a>';

                                    }else{ 
                                        echo '<a class="btn pull-lft btn-sm btn-success" style="display:none;" title="Create User"  href="'.base_url().'QBO_controllers/MerchantUser/create_user">Add User</a>';
                                        
                                    } 
                                }else{ ?>
                                    <a class="btn pull-lft btn-sm btn-success" title="Create User"   href="<?php echo base_url(); ?>QBO_controllers/MerchantUser/create_user">Add User</a>
                                
                               <?php } ?>
                            <?php }else{ ?>
                                
                                <a class="btn pull-lft btn-sm btn-success" title="Create User"  href="<?php echo base_url(); ?>QBO_controllers/MerchantUser/create_user">Add User</a>
                                
                            <?php }?>
                        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="admin_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
					<th class="text-left"> Name </th>
					<th class="text-left"> Role Name </th>
					<th class="hidden-xs text-left"> Email Address</th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($user_data) && $user_data)
				{
					foreach($user_data as $user)
					{
					
				?>
				<tr>
				
					<td class="text-left"> <?php echo $user['userFname'].' '.$user['userLname']; ?></td>
					<td class="text-left"> <?php echo $user['roleName']; ?></td>
					<td class="text-left hidden-xs"> <?php echo $user['userEmail']; ?></td>
					
                
					
					<td class="text-center">
						<div class="btn-group btn-group-xs">
						
						<a href="<?php echo base_url('QBO_controllers/MerchantUser/create_user/'.$user['merchantUserID']); ?>" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"> </i> </a>
							
                        <a href="#del_user_qbo" onclick="del_user_id2('<?php  echo $user['merchantUserID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
							
					</div>
					</td>
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="4"> No Records Found </td>
                    
                    <td style="display: none"></td>
                    <td style="display: none"></td>
                    <td style="display: none"></td></tr>'; }  
				?>
				
			</tbody>
        </table>
        </div>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->
    
    </div>

<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>
 
function del_user_id2(id){
	
	     $('#merchantID1').val(id);
}
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#admin_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [-1] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>	
<div id="del_user_qbo" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete User</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_user" method="post" action='<?php echo base_url(); ?>QBO_controllers/MerchantUser/delete_user' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this User?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantID1" name="merchantID1" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
