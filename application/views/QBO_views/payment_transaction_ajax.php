<!-- Page content -->
<div id="page-content">
   


    <!-- END Quick Stats -->


    <!-- All Orders Block -->
   <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Payment Transactions</strong> </h2>
        </div>
        <!-- All Orders Content -->
        <table id="tab_data" class="table table-bordered table-striped table-vcenter">
            <thead>
              <tr>
                    <th class="text-left visible-lg">Customer Name</th>
                    <th class="text-right">Invoice</th>
                     <th class="hidden-xs text-right">Amount</th>
					  <th class="text-right hidden-xs">Date </th>
                     <th class="text-right hidden-xs">Type</th>
                    <th class="text-right">Transaction ID</th> 
                    <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
			
			</tbody>
        </table>
        <!-- END All Orders Content -->
       
        
        
    </div>
    <!-- END All Orders Block -->
<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>
</div>



 <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                   
                        
                    
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>QBO_controllers/PaypalPayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p> 
                                           
                        <div class="form-group">
                           
                           <div class="col-md-8">
                           <input type="hidden" name="ref_amount" id="ref_amount" value="" class="form-control" />
                        </div>
    					</div>
                        <div class="pull-right">
            			    <input type="submit"  name="btn_cancel" class="btn btn-sm btn-warning" value="Refund"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>	

                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>

 <div id="payment_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Delete Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_formpay" method="post" action='<?php echo base_url(); ?>QBO_controllers/AuthPayment/delete_qbo_transaction' class="form-horizontal" >
                        <p id="message_data">Do you really want to delete this payment?</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                              
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                            <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
<!-- Load and execute javascript code used only in this page -->


<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>

<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"  <link rel="stylesheet" >
 
<script>

var table;
 
$(document).ready(function() {
	
	
	   table = $('#tab_data').DataTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
             "lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
              "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('QBO_controllers/Payments/ajax_payment_list')?>",
                "type": "POST" ,
                
                "data":function(data) {
							data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
						},
                 
            },

           
            "columnDefs": [
				{ 
				  "targets": [ -1 ], //last column
				  "orderable": false, //set not orderable
				},
            ],
           "order": [[4, 'desc']],
          	 "language": {
						"lengthMenu": " _MENU_ ",
					  "sSearch": "",
					  "paginate": {
                      "previous": "",
					   "next": "",
                        }
          
					},
                 "sPaginationType": "bootstrap",
			

          });
        
         // initialzie select2 dropdown
         
         
          $('#tab_data_wrapper .dataTables_filter input').addClass("form-control ").attr("placeholder", "Search");
         // modify table search input
         $('#tab_data_wrapper .dataTables_length select').addClass("m-wrap form-control ");
         // modify table per page dropdown
         $('#tab_data_wrapper .dataTables_length dataTables_length select').select2();
});


	function set_refund_pay(id, txnid, txntype,refamoutn)
	{
	    
	       
	         var form = $("#data_form");
	         $('#ref_amount').val(roundN(refamoutn,2));
			 	 $('.ref').remove(); 
			 	 
                                    
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'trID',
											'name': 'trID',
											'class':'ref',
											'value': id,
											}).appendTo(form);

    		if(txnid !=""){
			 
				if(txntype=='1'){
				 var url   = "<?php echo base_url()?>QBO_controllers/Payments/create_customer_refund";
				 
			
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'txnID',
											'name': 'txnID',
											'class':'ref',
											'value': txnid,
											}).appendTo(form);
				 
				}	
			else if(txntype=='2'){
				var url   = "<?php echo base_url()?>QBO_controllers/AuthPayment/create_customer_refund";
					 $('<input>', {
											'type': 'hidden',
											'id'  : 'txnIDrefund',
											'name': 'txnIDrefund',
											'value': txnid,
											}).appendTo(form);
			}	
			 else if(txntype=='3'){
				var url   = "<?php echo base_url()?>QBO_controllers/PaytracePayment/create_customer_refund";
				 $('<input>', {
											'type': 'hidden',
											'id'  : 'paytxnID',
											'name': 'paytxnID',
											'value': txnid,
											}).appendTo(form);
			 }
			 else if(txntype=='4'){
				var url   = "<?php echo base_url()?>QBO_controllers/PaypalPayment/create_customer_refund";
				
					 $('<input>', {
											'type': 'hidden',
											'id'  : 'txnpaypalID',
											'name': 'txnpaypalID',
											'value': txnid,
											}).appendTo(form);
			 }
			 else if(txntype=='5'){
				var url   = "<?php echo base_url()?>QBO_controllers/StripePayment/create_customer_refund";
					 $('<input>', {
											'type': 'hidden',
											'id'  : 'txnstrID',
											'name': 'txnstrID',
											'value': txnid,
											}).appendTo(form);
			 }
			 
			 $("#data_form").attr("action",url);	
    		}
    	}  





$('#data_form').validate({
    
     rules: {
    ref_amount: {
    required: true,
    number:true,
    remote: {
        url: "<?php echo base_url(); ?>QBO_controllers/Payments/check_transaction_payment",
        type: "POST",
        cache: false,
        dataType: "json",
        data: {
            trID: function(){ return $("#trID").val(); },
        },
        dataFilter: function(response) {
            
            var rsdata = jQuery.parseJSON(response)
        
             if(rsdata.status=='success')
             return true;
             else
             return false;
        }
    },
   

},
    
     },
      messages:
    {
        ref_amount:
        {
            required: "Please enter amount",
             number: "Please enter valid amount",
            remote:"Your are not allowed to refund more than actual amount"
        }
    },
});



   function set_transaction_pay(t_id)
   {
       	 var form = $("#data_formpay");
                                     $('<input>', {
											'type': 'hidden',
											'id'  : 'paytxnID',
											'name': 'paytxnID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'paytxnID',
											'name': 'paytxnID',
											'value': t_id,
											}).appendTo(form);
							
       
  
   }


</script>


<style>


</style>	
	


<!-- END Page Content -->