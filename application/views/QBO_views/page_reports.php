	<!-- Page content -->
	<?php
	$this->load->view('alert');
?>
	<div id="page-content">
								  
    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?></div>
	<legend class="leg">Reports </legend>
	<div class="block full">
	   
      <form method="post" action="<?php echo base_url().'QBO_controllers/report/transaction_reports' ?>" >
       
       <div class="col-md-3 no-left-padding">
           
         <select name="report_type" id="report_type"  class="form-control">
            <option value="1"    <?php if(isset($report_type) &&  $report_type=='1') echo "selected"; ?>  >Top 10 Due by Customer</option>
            <option value="2"  <?php if(isset($report_type) && $report_type=='2') echo "selected"; ?>>Top 10 Past Due by Customer</option>
            <option value="3"  <?php if(isset($report_type) && $report_type=='3') echo "selected"; ?>>Top 10 Past Due Invoices by Amount</option>
              <option value="4"  <?php if(isset($report_type) && $report_type=='4') echo "selected"; ?>>Top 10 Past Due Invoices by Days</option>
            <option value="5"  <?php if(isset($report_type) && $report_type=='5') echo "selected"; ?>>Failed Transactions in Last 30 Days</option>
            <option value="6"  <?php if(isset($report_type) && $report_type=='6') echo "selected"; ?>>Accounts with Credit Cards Expiring</option>
              <option value="7" <?php if(isset($report_type) && $report_type=='7') echo "selected"; ?> >Transaction Report - Based on Date Range </option>
            <option value="8"  <?php if(isset($report_type) && $report_type=='8') echo "selected"; ?>>Open Invoices Report</option>
         
              <option value="10"  <?php if(isset($report_type) && $report_type=='10') echo "selected"; ?>>Scheduled Payments Report</option>
          </select>
         
       </div> 
       <div class="col-md-4" id="range_data" <?php if($startdate==''){ ?> style="display:none" <?php  } ?> >
       
             <div class="input-group input-daterange" data-date-format="mm/dd/yyyy">
                    <input type="text" id="startDate" name="startDate" class="form-control text-center" value="<?php if(isset($startdate)) echo($startdate)?($startdate):''; ?>" placeholder="From">
                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                    <input type="text" id="endDate" name="endDate" class="form-control text-center"  value="<?php if(isset($enddate)) echo($enddate)?($enddate):''; ?>"  placeholder="To">
                </div>
             </div>
            
        
             <input type="submit" name="getData" class="btn btn-sm pull-left rp-btn btn-info" id="getData" value="Go" /> 
       
        <div class="btn-group pull-left"> 
        
      
	        <a  href="<?php echo base_url(); ?>QBO_controllers/report/export_csv/<?php echo $report_type; if(isset($startdate)&& $startdate!=''){echo '/'.date('Y-m-d',strtotime($startdate)); }if(isset($enddate)&& $enddate!=''){echo '/'.date('Y-m-d',strtotime($enddate)); } ?>" target="_blank" data-toggle="tooltip" title="" data-original-title="Download CSV" id="csv_btn"><b class="btn btn-sm btn-success">CSV</b></a>
           
			  <a   href="<?php echo base_url(); ?>QBO_controllers/report/report_details_pdf/<?php echo $report_type; if(isset($startdate)&& $startdate!=''){echo '/'.date('Y-m-d',strtotime($startdate)); }if(isset($enddate)&& $enddate!=''){echo '/'.date('Y-m-d',strtotime($enddate)); } ?>" target="_blank"   data-toggle="tooltip" title="" data-original-title="Download PDF"><b class="btn btn-sm btn-danger">PDF</b></a>
              
           </div>     
      
       
            
     </form>
   
      
     <br>
     <br>
     <br>
     
        
       <?php  if(isset($report1)){  ?>
        
         <table  class="table table-bordered table-striped table-vcenter table-align-padding-left">
            <thead>
                <tr>
				  
                    <th class="text-left">Customer Name</th>
                    <th class="text-left hidden-xs">Full Name</th>
                   
                    
					<th class="text-right hidden-xs">Email Address</th>
					<th class="text-right">Amount</th>
                   
                  
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report1))
				{
					
					
					foreach($report1 as $key=>$invoice)
					{
				?>
				<tr>
				   
					
					<td class="text-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" data-toggle="tooltip" title="View Details"  ><?php echo $invoice['fullName'] ; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" data-toggle="tooltip" title="View Details"  disabled><?php echo $invoice['fullName'] ; ?></a>
							<?php }?>
					  </td>
					<td class="text-left"><?php echo $invoice['firstName'].' '.$invoice['lastName']; ?></td>
				
					<td class="hidden-xs  hidden-sm text-right cust_view"><a href="mailto:<?php echo $invoice['userEmail']; ?>"><?php echo $invoice['userEmail']; ?></a></td>
			       <td class="text-right">$<?php echo number_format($invoice['balance'],2); ?></td>
					
				
					
				
				</tr>
				
				<?php } }
				else { echo'<tr><td colspan="5"> No Records Found </td></tr>'; }  
				
				?>
				
			</tbody>
        </table>
       <?php }  
	   
	 
	    if(isset($report2) &&  !empty($report2) ){        ?> 
        
          <table   class="table table-bordered table-striped table-vcenter table-align-padding-left">
            <thead>
                <tr>
                   
                    <th class="text-left">Customer Name</th>
                    <th class="text-left hidden-xs">Full Name</th>
                   
					<th class="text-right hidden-xs">Email Address</th>
					 <th class="text-right">Amount</th>
                
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report2))
				{
					foreach($report2 as $key=> $invoice)
					{
				?>
				<tr>
				    	<td class="text-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" data-toggle="tooltip" title="View Details"  ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" data-toggle="tooltip" title="View Details"  disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
				
					<td class="text-left hidden-xs"><?php echo $invoice['firstName'].' '.$invoice['lastName'] ; ?></a></td>
				
					
					<td class="hidden-xs  hidden-sm text-right cust_view"><a href="mailto:<?php echo $invoice['userEmail']; ?>"><?php echo $invoice['userEmail']; ?></a></td>
			     
						<td class="text-right">$<?php echo number_format($invoice['balance'],2); ?></td>
				
					
				
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="5"> No Records Found </td></tr>'; }  
				?>
				
			</tbody>
        </table>
       <?php } 
	   
	    if(isset($report3)){  ?> 
         <table   class="table table-bordered table-striped table-vcenter table-align-padding-left">
            <thead>
                <tr>
                    <th class="text-left hidden-xs  hidden-sm">Invoice</th>
                    <th class="text-left">Customer Name</th>
                      <th class="text-right hidden-xs  hidden-sm">Email Address</th>
                    <th class="text-right hidden-xs ">Days Delinquent</th>
                    <th class="text-right" >Amount</th>
                  
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report3))
				{
					foreach($report3 as $invoice)
					{
					  if($invoice['status']=='Scheduled'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-left hidden-sm hidden-xs cust_view"><a href="<?php echo base_url();?>QBO_controllers/Create_invoice/invoice_details_page/<?php  echo $invoice['invoiceID']; ?>"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:''; ?></a></td>
						<td class="text-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>"  disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
				
			<td class="hidden-xs  hidden-sm text-right cust_view"><a href="mailto:<?php echo $invoice['userEmail']; ?>"><?php echo $invoice['userEmail']; ?></a></td>
				
					
					 <td class="hidden-xs text-right"><?php echo $invoice['tr_Day']; ?></td> 
				 
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
					
				
					 
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="6"> No Records Found </td></tr>'; }  
				
				?>
				
			</tbody>
        </table>
        
          <?php } 
	   
	    if(isset($report4)){  ?> 
          <table  class="table table-bordered table-striped table-vcenter compamount4 table-align-padding-left">
            <thead>
                <tr>
                    <th class="text-left hidden-xs  hidden-sm">Invoice</th>
                    <th class="text-left">Customer Name</th>
                     <th class="text-right hidden-xs  hidden-sm">Email Address</th>
                    <th class="text-right hidden-xs">Days Delinquent</th>
                    <th class="text-right" >Amount</th>
                
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report4))
				{
					foreach($report4 as $invoice)
					{
					
					   if($invoice['status']=='Scheduled'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   } else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-left hidden-xs hidden-sm cust_view"><a href="<?php echo base_url();?>QBO_controllers/Create_invoice/invoice_details_page/<?php  echo $invoice['invoiceID']; ?>"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:''; ?></a></td>
						<td class="test-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
				
					<td class="hidden-xs  hidden-sm text-right cust_view"><a href="mailto:<?php echo $invoice['userEmail']; ?>"><?php echo $invoice['userEmail']; ?></a></td>
					
					 <td class="hidden-xs text-right"><?php echo $invoice['tr_Day']; ?></td> 
					
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
				
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="6"> No Records Found </td></tr>'; }  
				
				?>
				
			</tbody>
        </table>
        
       <?php } 
	   
	    if(isset($report5)){  ?> 
          <table id="ecom-orders"  class="table table-bordered table-striped table-vcenter compamount5 table-align-padding-left">
            <thead>
                <tr>
                    <th class="text-left hidden-xs  hidden-sm">Transaction ID</th>
                    <th class="text-left">Customer Name</th>
                   <th class="hidden-xs text-left">Invoice</th>
                    <th class="text-right">Amount</th>
                 
                     <th class="text-right hidden-xs">Type</th>
                      <th class="text-right hidden-xs">Date</th>
                     <th class="text-right hidden-xs hidden-sm">Reference</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report5))
				{
				
					foreach($report5 as $key=> $invoice)
					{
				
				
					if($invoice['transactionCode']!='100'){
					 $lable ="danger";
						$labeltext ="Failed";	   
					}else{
					 $lable ="success";
					 $labeltext ="Success";
							  
					}
				?>
				<tr>
				  
					<td class="text-left hidden-xs  hidden-sm"><?php echo ($invoice['transactionID'])?$invoice['transactionID']:$invoice['id']; ?></td>
					<td class="test-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['customerListID']); ?>" ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['customerListID']); ?>" disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
				
						<td class="text-left hidden-sm hidden-xs cust_view"><a href="<?php echo base_url();?>QBO_controllers/Create_invoice/invoice_details_page/<?php  echo $invoice['invoiceID']; ?>"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:''; ?></a></td>
					<td class="text-right">$<?php echo number_format($invoice['transactionAmount'],2); ?></td>
					
				
			     	
					
				
					
			
					<td class="text-right hidden-xs">
						<?php echo ucfirst($invoice['transactionType']); ?>
					</td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>
                    <td class="text-right hidden-xs">
						<?php echo $invoice['transactionStatus']; ?>
					</td>
				   
					
				</tr>
				
				<?php } }
				else { echo'<tr><td colspan="7"> No Records Found </td></tr>'; }  
				?>
				
			</tbody>
        </table>
         <?php } 
	   
	    if(isset($report6)){ 
	    ?> 
          <table id="ecom-orders" class="table table-bordered table-striped table-vcenter table-align-padding-left">
            <thead>
                <tr>
                   
                    <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs  hidden-sm">Email Address</th>
                     <th class="text-right hidden-xs">Card Number</th>
					
                    <th class="text-right ">Expiration Date</th>
                    <th class="text-center hidden-xs  hidden-sm">Status</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report6))
				{
					foreach($report6 as $invoice)
					{
				?>
				<tr>
					
					<td class="test-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" data-toggle="tooltip" title="View Details"  ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" data-toggle="tooltip" title="View Details"  disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
				
				
					<td class="hidden-xs hidden-sm text-right cust_view"><a href="mailto:<?php echo $invoice['Contact']; ?>"><?php echo $invoice['Contact']; ?></a></td>
                    <td class="text-right hidden-xs"><?php echo ($invoice['CardNo'])?$invoice['CardNo']:''; ?></td>
			     	
					<td class="text-right"><?php echo date('m/Y', strtotime($invoice['expired_date'])); ?></td>
				
					
					<td class="text-center hidden-xs">Expired</td>
					
				   
			
				</tr>
				
				<?php } } 
				else { echo'<tr><td colspan="5"> No Records Found </td></tr>'; }  
				
				?>
				
			</tbody>
        </table>
        
         <?php } 
	  
	    if(isset($report7)){ 
	    ?> 
             <table   class="table table-bordered table-striped table-vcenter compamount7 table-align-padding-left">
            <thead>
                <tr>
				   
                    <th class="text-left hidden-xs hidden-sm">Transaction ID</th>
                    	<th class="text-left">Customer Name</th>
					 <th class="hidden-xs hidden-sm text-left">Invoice</th>
				
					 <th class="text-right">Amount</th>
                    <th class="text-right hidden-xs">Date</th>
                     <th class="text-right">Type</th>
				     <th class="text-right hidden-xs">Remarks</th>
                     <th class="text-right hidden-xs">Status</th>
                    
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report7))
				{
				
					foreach($report7 as $key=> $invoice)
					{
						$inv_url1 = '';
						if (!empty($invoice['invoice_id']) && !empty($invoice['invoice_no'])) {
							$invs = explode(',', $invoice['invoice_id']);

							$invoice_no = explode(',', $invoice['invoice_no']);

							foreach ($invs as $k => $inv) {
								$inv_url = base_url() . 'QBO_controllers/Create_invoice/invoice_details_page/' . trim($inv);
								$inv_url1 .= ' <a href="' . $inv_url . '"><strong>' . $invoice_no[$k] . '</strong></a>,';
							}

							$inv_url1 = substr($inv_url1, 0, -1);
						} else{
							$inv_url1 .= '<a href="javascript:void(0);">---</a> ';
						}
				
						if($invoice['transactionCode'] == 200 || $invoice['transactionCode'] == 100 || $invoice['transactionCode'] == 1 || $invoice['transactionCode'] == 111){
							$lable ="success remove-hover";
						 	$labeltext ="Success";
						}else{
						 	$lable ="danger remove-hover";
							$labeltext ="Failed";	   
						}
				?>
				<tr>
				   
					<td class="text-left hidden-xs hidden-sm"><?php echo ($invoice['transactionID'])?$invoice['transactionID']:$invoice['id']; ?></td>
					<td class="test-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>"  ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
					<td class="hidden-xs hidden-sm text-left cust_view"><?php echo $inv_url1; ?></td>
					<td class="text-right">
						<?php if(isset($invoice['transactionType']) && strpos(strtolower($invoice['transactionType']), 'refund') !== false){
							echo '('.priceFormat($invoice['transactionAmount'], true).')';
						}else{ 
							echo priceFormat($invoice['transactionAmount'], true);
						} ?>
					</td>
					
					<td class="hidden-xs text-right"> <?php echo date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>
						<td class="text-right"><?php echo ucfirst($invoice['transactionType']); ?></td>
				    <td class="hidden-xs text-right" > <?php echo ucwords(strtolower($invoice['transactionStatus'])); ?></td>
					
					
					
			     	
					
			    
					
					<td class="text-right hidden-xs"> <?php echo $labeltext; ?></td>
					
				   
				
				</tr>
				
				<?php } }
				else { echo'<tr><td colspan="8"> No Records Found </td></tr>'; }  
				?>
				
			</tbody>
        </table>
        
           <?php } 
	   
	    if(isset($report89) &&  !empty($report89) ){ 
	    ?> 
        
        <table class="table table-bordered table-striped table-vcenter compamount table-align-padding-left">
            <thead>
                <tr>
                    <th class="text-left hidden-xs  hidden-sm">Invoice</th>
                    <th class="text-left">Customer Name</th>
                    <th class="text-right hidden-xs">Added On</th>
                    <th class="hidden-xs text-right">Days Delinquent</th>
                    <th class="text-right hidden-xs">Paid</th>
                    <th class="text-right">Balance</th>
				    
                     <th class="text-center">Status</th>
                   
                     
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report89))
				{
				
					foreach($report89 as $key=> $invoice)
					{
				
				
					  if($invoice['status']=='Scheduled'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   } else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-left hidden-xs hidden-sm cust_view"><a href="<?php echo base_url();?>QBO_controllers/Create_invoice/invoice_details_page/<?php  echo $invoice['invoiceID']; ?>"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:'----'; ?></a></td>
						<td class="test-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>"  ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['Customer_ListID']); ?>" disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
			        <td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($invoice['addOnDate'])); ?></td>
			    	 <td class="hidden-xs text-right"><?php echo $invoice['tr_Day']; ?></td> 
					<td class="hidden-xs text-right">$<?php echo number_format($invoice['AppliedAmount'],2); ?></td>
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
				    
                    <td class="text-center"><?php echo $invoice['status']; ?></td>
					
				
				
				</tr>
				
				<?php } }
				else { echo'<tr><td colspan="6"> No Records Found </td></tr>'; }  
				
				?>
				
			</tbody>
        </table>
        
        
         <?php } 
	   
	    if(isset($report10)    ){ 
	    ?> 
          
        <table id="ecom-orders"   class="table table-bordered table-striped table-vcenter table-align-padding-left">
            <thead>
                <tr>
                    <th class="text-left hidden-xs hidden-sm">Invoice</th>
                    <th class="text-left">Customer Name</th>
                 
                    <th class="text-right">Balance</th>
                    <th class="text-right hidden-xs">Due Date</th>
				    <th class="hidden-xs text-right">Amount</th>
				    <th class="hidden-xs text-right">Schedule Date</th>
                
                       
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report10))
				{
				
					foreach($report10 as $invoice)
					{
				
			
				?>
				<tr>
					<td class="text-left hidden-xs  hidden-sm cust_view"><a href="<?php echo base_url();?>QBO_controllers/Create_invoice/invoice_details_page/<?php  echo $invoice['invoiceID']; ?>"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:'----'; ?></a></td>
					<td class="test-left hidden-xs cust_view">
					    	<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['CustomerListID']); ?>"   ><?php echo $invoice['fullName']; ?></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('QBO_controllers/home/view_customer/'.$invoice['CustomerListID']); ?>" disabled><?php echo $invoice['fullName']; ?></a>
							<?php }?>
					  </td>
				
					<td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
					<td class="text-right hidden-xs"><?php echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
				     <td class="hidden-xs text-right"><?php echo $invoice['Total_payment']; ?></td>
				     <td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($invoice['scheduleDate'])); ?></td>
                
                     
				</tr>
				
				<?php } }
				else { echo'<tr><td colspan="7"> No Records Found </td></tr>'; }  
				?>
				
			</tbody>
        </table>
          <?php } ?>
    </div>
   
 <style>
    .rp-btn{
        
        margin-right:4px;
    } 
     
 </style>   
    
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
    <script>$(function(){   
	
	
	Pagination_view.init();

	
	
 $('#report_type').change(function(){

    var report_type = $(this).val();
	
	if(report_type=='7'){
	$('#range_data').show();
	}else{  $('#range_data').hide();  }

 });
 

 
 
 
  });
  
  
  
var Pagination_view = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            
            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



  
</script>
	

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	<div class="row">
           <div class="col-md-12">
              <div class="col-md-4"></div>    
                <div class="col-md-4">
                    <div class="msg_data"><?php echo $this->session->flashdata('message');   ?> </div> 
               </div>
               <div class="col-md-4">   </div>
             </div>  
        </div>

</div>
