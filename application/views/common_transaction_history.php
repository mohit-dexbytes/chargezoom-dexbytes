<?php 
    if(isset($isCustomerPage) && $isCustomerPage){
?>
<style>
.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
    position: absolute;
    z-index: 9;
    left: 65px;
    width: 220px !important;
}
</style>
    <?php }

?>
    <legend class="leg">Transactions</legend>
    <div class="full">

        <!-- All Orders Content -->
        <table id="transaction_history_pay_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Invoice</th>
                    <th class="text-right hidden-xs">Amount</th>
                    <th class="text-right ">Date </th>
                    <th class="text-right hidden-xs">Type</th>
                    <th class="text-right hidden-xs hidden-sm">Transaction ID</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($transactions) && $transactions) {
                    if($merchant_type == 1){
                        $view_function = 'set_qbo_payment_transaction_data';
                    }else if($merchant_type == 2){
                        $view_function = 'set_payment_transaction_data';
                    }else if($merchant_type == 3){
                        $view_function = 'set_fb_payment_transaction_data';
                    }else if($merchant_type == 4){
                        $view_function = 'set_common_transaction_data';
                    }else{
                        $view_function = 'set_company_payment_transaction_data';
                    }
                    foreach ($transactions as $transaction) {
                        $inv_url1 = '';
                        $invoice_id = '';
                        $custom_data = $transaction['custom_data_fields'];
                        if($custom_data){
                            $json_data = json_decode($custom_data, 1);
                            if(isset($json_data['invoice_number'])){
                                $invoice_id = $json_data['invoice_number'];
                            }
                        }
                        if(!empty($invoice_id)){
                            $transaction['invoice_id'] = $invoice_id;
                        }
                        if (!empty($transaction['invoice_id'])) {
                            $invs = explode(',', $transaction['invoice_id']);
                            if(isset($transaction['invoice_no'])){
                                $invoice_no = explode(',', $transaction['invoice_no']);
                            }
                            foreach ($invs as $k => $inv) {
                                if (isset($invoice_no[$k])) {
                                    $inv_url = base_url() . $invoice_url . trim($inv);
                                    if (isset($invoice_no[$k]))
                                        if ($plantype) {
                                            $inv_url1 .= $invoice_no[$k].',';
                                        } else {
                                            $inv_url1 .= ' <a href="' . $inv_url . '">' . $invoice_no[$k] . '</a>,';
                                        }
                                }
                            }
                            $inv_url1 = substr($inv_url1, 0, -1);
                        } else
                            $inv_url1 .= '<a href="javascript:void(0);">---</a> ';

                        $gateway = ($transaction['gateway']) ? $transaction['gateway'] : 'Offline Payment';
                ?>
                        <tr>
                            <td class="text-left cust_view"><?php echo $inv_url1; ?></td>
                            <td class="hidden-xs text-right cust_view">
                            <?php if(isset($transaction['transactionType']) && strpos(strtolower($transaction['transactionType']), 'refund') !== false){ ?>
                                <a href="#pay_data_process" onclick="<?php echo $view_function; ?>('<?php echo $transaction['transactionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">(<?php echo '$' . number_format($transaction['transactionAmount'], 2); ?>)</a>
                            <?php }else{ ?>
                                <a href="#pay_data_process" onclick="<?php echo $view_function; ?>('<?php echo $transaction['transactionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$' . number_format($transaction['transactionAmount'], 2); ?></a>
                            <?php } ?>
                            </td>
                            
                            <?php

                                $transactionDate = $transaction['transactionDate'];
                                if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){

                                    $timezone = ['time' => $transactionDate, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];

                                    $transactionDate = getTimeBySelectedTimezone($timezone);

                                }

                            ?>
                            <td class=" text-right"><?php echo date('M d, Y h:i A', strtotime($transactionDate)); ?></td>
                            <td class="hidden-xs text-right">
                                <?php
                                $showRefund = 0;
                                if ($transaction['partial'] == $transaction['transactionAmount']) {
                                    echo "<label class='label label-success'>Fully Refunded: " . '$' . number_format($transaction['partial'], 2) . "</label><br/>";
                                } else   if ($transaction['partial'] != '0') {
                                    $showRefund = 1;
                                    echo "<label class='label label-warning'>Partially Refunded: " . '$' . number_format($transaction['partial'], 2) . " </label><br/>";
                                } else {
                                    $showRefund = 1;
                                    echo "";
                                }

                                if(strpos(strtolower($transaction['transactionType']), 'refund') !== false){
                                    $showRefund = 0;
                                     echo "Refund";
                                }else if (strpos($transaction['transactionType'], 'sale') !== false || strtoupper($transaction['transactionType']) == 'AUTH_CAPTURE') {
                                    echo "Sale";
                                } else if (strpos($transaction['transactionType'], 'capture') !== false || strtoupper($transaction['transactionType']) != 'AUTH_CAPTURE') {
                                    echo "Capture";
                                } else if (strpos($transaction['transactionType'], 'Offline Payment') !== false) {
                                    echo "Offline Payment";
                                }
                                ?>

                            </td>
                            <td class="text-right hidden-xs hidden-sm"><?php echo $transaction['transactionID']; ?></td>

                            <td class="text-center hidden-xs">

                                <div class="btn-group dropbtn">
                                    <?php 
                                        if($showRefund == 0) { 
                                            $disabled_select = 'disabled'; 
                                        }else{
                                            $disabled_select = '';
                                        } 
                                    ?>
                                    <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default <?php echo $disabled_select; ?> btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                    <ul class="dropdown-menu text-left">

                                        <?php

                                        if ($transaction['transaction_user_status'] == '1' || $transaction['transaction_user_status'] == '2') {

                                            if (
                                                in_array($transaction['transactionCode'], array('100', '200', '111', '1')) &&
                                                in_array(strtoupper($transaction['transactionType']), array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'OFFLINE PAYMENT', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
                                            ) {
                                                if (($transaction['tr_Day'] == 0 &&  ($transaction['transactionGateway'] == '2' || $transaction['transactionGateway'] == '3')) ||  strtoupper($transaction['transactionType']) == 'OFFLINE PAYMENT') {      ?>


                                                    <li> <a href="javascript:void(0);" class="" data-title="Pending Transaction" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
                                                <?php } else { 
                                                        if(strtolower($gateway) != 'heartland echeck'){ 
                                                ?>

                                                    <li> 
                                                        
                                                        <a href="javascript:void(0)" id="txnRefund<?php  echo $transaction['transactionID']; ?>" transaction-id="<?php  echo $transaction['transactionID']; ?>" transaction-gatewayType="<?php echo $transaction['transactionGateway'];?>" transaction-gatewayName="<?php echo $transaction['gateway'];?>" integration-type="<?php echo $merchant_type; ?>" class="refunAmountCustom" data-url="<?php echo base_url(); ?>ajaxRequest/getRefundInvoice"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
                                                    </li>
                                                <?php   }
                                                ?>


                                                <?php }
                                            }
                                            if($transaction['transactionGateway'] != 5){
                                                if ($transaction['transaction_user_status'] != '2') {   ?>

                                                    <li><a href="#transaction_history_payment_delete" class="" onclick="set_transaction_pay_id('<?php echo $transaction['id']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>
                                                <?php }
                                            }
                                        } else {  ?>

                                            <li><a href="javascript:void('0');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>
                                        <?php     } ?>
                                        <?php 
                                            $transaction_auto_id = $transaction['transactionID'];
                                        ?>
                                        <li><a href="javascript:void('0');" onclick="getPrintTransactionReceiptData('<?php echo $transaction_auto_id; ?>', '<?php echo $merchant_type; ?>')">Print</a></li>
                                    </ul>
                                </div>
                            </td>

                        </tr>

                <?php }
                } 
                ?>

            </tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
<div id="print-transaction-div" style="display: none;">
    
</div>
<div id="transaction_history_payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Refund Payment</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <div id="refund_msg"></div>


                <form id="transaction_history_data_form" method="post" action='<?php echo base_url($refund_url); ?>' class="form-horizontal">
                    <p id="message_data">Do you really want to refund this payment? Clicking "Refund" will initiate the refund process.</p>

                    <div id="sigle_tr">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_list">Refund Amount:</label>
                            <div class="col-md-8">
                                <label class="control-label" id="ref_amount"></label>
                               
                            </div>
                        </div>
                    </div>
                    <div id="multi_tr11">

                    </div>
                    <div class="pull-right">
                        <input type="submit" id="rf_btn" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund" />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="transaction_history_payment_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Void Payment Record</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form id="transaction_history_data_formpay" method="post" action='<?php echo base_url($void_url); ?>' class="form-horizontal">
                    <p id="message_data">This will void the payment record in your accounting software only. If you are looking to void an unsettled payment, please go to "Capture/Void" page.</p>
                    <div class="form-group">
                        <div class="col-md-8">

                        </div>
                    </div>
                    <div class="pull-right">
                        <input type="submit" name="btn_cancel" class="btn btn-sm btn-danger" value="Void" />
                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<script>
    $(function() {

        Pagination_view_transaction_history.init();


        $('#transaction_history_data_form').validate({

            rules: {

                'ref_amount': {
                    required: true,
                    number: true,
                    remote: {
                        url: "<?php echo base_url(); ?>Payments/check_transaction_payment",
                        type: "POST",
                        cache: false,
                        dataType: "json",
                        data: {
                            trID: function() {
                                return $("#trID").val();
                            },

                        },
                        dataFilter: function(response) {

                            var rsdata = jQuery.parseJSON(response)

                            if (rsdata.status == 'success')
                                return true;
                            else
                                return false;
                        }
                    },


                },

                'multi_inv': {
                    required: true,
                },

            },

        });


    });



    window.setTimeout("fadeMyDiv();", 2000);

    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
    }


    function check_transaction(el) {

        var st = '';
        var msg = '';
        var btn = '';

        btn = $(el).attr('id');


        if ($("input[name='multi_inv']").is(":checked")) {


            var trID = $("input[name='multi_inv']:checked").val();
            var trAmount = $("#" + trID).val();

            $.ajax({
                url: "<?php echo base_url(); ?>ajaxRequest/check_transaction_qbo_payment_data",
                type: "POST",
                aync: false,
                dataType: 'json',
                data: {
                    trID: trID,
                    pay_amount: trAmount,
                },
                dataFilter: function(response) {

                    var rsdata = JSON.parse(response);

                    if (rsdata.status == 'success') {
                        msg = "Success";
                        st = 'success';
                    } else {
                        msg = rsdata.message;
                        st = 'error';

                        msg = '<div class="alert alert-danger">  <strong>Error:</strong> ' + msg + '</div>'

                    }

                    if (st == 'error') {

                        $('#refund_msg').html(msg);
                        $('#rf_btn').attr("disabled", true);
                    } else {
                        msg = '';
                        $('refnd_msg').html(msg);
                        $('#rf_btn').attr("disabled", false);
                        if (btn == "rf_btn") {
                            var index = $("input[name='multi_inv']:checked").data("id");

                            $('#index').remove();


                            $('<input>', {
                                'type': 'hidden',
                                'id': 'index',
                                'name': 'index',

                                'value': index,
                            }).appendTo($('#transaction_history_data_form'));
                            $('#transaction_history_data_form').submit();

                        }


                    }
                }
            });




        } else {

            var amt = $('#ref_amount').val();
            if (amt > 0)
                $('#transaction_history_data_form').submit();
            return false;
        }

    }


    function check_invoce_value() {
        $('#rf_btn').attr("disabled", false);
        $('#refund_msg').html('');
    }





















    var Pagination_view_transaction_history = function() {

        return {
            init: function() {
                / Extend with date sort plugin /
                $.extend($.fn.dataTableExt.oSort, {

                });

                / Initialize Bootstrap Datatables Integration /
                App.datatables();

                / Initialize Datatables /
                $('#transaction_history_pay_page').dataTable({
                    columnDefs: [{
                            type: 'date',
                            targets: [2]
                        },
                        {
                            orderable: false,
                            targets: [5]
                        }
                    ],
                    order: [
                        [2, "desc"]
                    ],
                    pageLength: 10,
                    lengthMenu: [
                        [10, 25, 50, 100, 500],
                        [10, 25, 50, 100, 500]
                    ]
                });

                / Add placeholder attribute to the search input /
                $('.dataTables_filter input').attr('placeholder', 'Search');
            }
        };
    }();


    function set_refund_pay_id(id, txnid, txntype, refamoutn) {


        var form = $("#transaction_history_data_form");
        $('#ref_amount').html(roundN(refamoutn, 2));
        $('.ref').remove();


        $('<input>', {
            'type': 'hidden',
            'id': 'trID',
            'name': 'trID',
            'class': 'ref',
            'value': id,
        }).appendTo(form);

        if (txnid != "") {
            $('#multi_tr').html('');
            $('#txnID').remove();
            $('<input>', {
                'type': 'hidden',
                'id': 'txnID',
                'name': 'txnID',
                'class': 'ref',
                'value': txnid,
            }).appendTo(form);

        }
    }




    function set_transaction_pay_id(t_id) {
        var form = $("#transaction_history_data_formpay");
        $('<input>', {
            'type': 'hidden',
            'id': 'paytxnID',
            'name': 'paytxnID'

        }).remove();
        $('<input>', {
            'type': 'hidden',
            'id': 'paytxnID',
            'name': 'paytxnID',
            'value': t_id,
        }).appendTo(form);
    }
</script>