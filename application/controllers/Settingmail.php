<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Settingmail extends CI_Controller
{

    protected $loginDetails;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('general');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('general_model');
        $this->load->model('customer_model');
        $this->load->model('company_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2') {
        } else if ($this->session->userdata('user_logged_in') != "") {
        } else {
            redirect('login', 'refresh');
        }
        $this->loginDetails = get_names();
    }

    public function index()
    {
        redirect('Settingmail/email_temlate', 'refresh');
    }

    public function create_template_data()
    {

        $login_info = $this->session->userdata('logged_in');
        $fromEmail  = $login_info['companyEmail'];
        $merchantID = $login_info['merchID'];

        $templatedatas = $this->general_model->get_table_data('tbl_email_template_data', '');

        foreach ($templatedatas as $templatedata) {

            $insert_data = array(
                'templateName' => $templatedata['templateName'],
                'templateType' => $templatedata['templateType'],
                'merchantID'   => '6',
                'fromEmail'    => 'falkner076@gmail.com',
                'message'      => $templatedata['message'],
                'emailSubject' => $templatedata['emailSubject'],
                'createdAt'    => date('Y-m-d H:i:s'),
            );
            $this->general_model->insert_row('tbl_email_template', $insert_data);
        }
    }

    public function reset_email_template(){
        if ($this->session->userdata('logged_in')) {
            $merchant_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchant_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $templatedatas = $this->general_model->get_table_data('tbl_email_template_data', '');

        foreach ($templatedatas as $templatedata) {

            $insert_data = array(
                'templateName' => $templatedata['templateName'],
                'templateType' => $templatedata['templateType'],
                'merchantID'   => $merchant_id,
                'fromEmail'    => DEFAULT_FROM_EMAIL,
                'message'      => $templatedata['message'],
                'emailSubject' => $templatedata['emailSubject'],
                'createdAt'    => date('Y-m-d H:i:s'),
            );

            $emailCondition = [
                'templateType' => $templatedata['templateType'],
                'merchantID'   => $merchant_id,
            ];

            $emailData = $this->general_model->get_row_data('tbl_email_template', $emailCondition);
            if($emailData && !empty($emailData)){
                $this->general_model->update_row_data('tbl_email_template', $emailCondition, $insert_data);
            } else {
                $this->general_model->insert_row('tbl_email_template', $insert_data);
            }
        }

        $this->session->set_flashdata('success', 'Successfully Reset Templates');
        redirect('Settingmail/email_temlate');
    }

    public function check_custom_template_name($name)
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $rowID = $this->czsecurity->xssCleanPostInput('tempID');

        if ($rowID != '') {
            $result = $this->general_model->get_num_rows('tbl_email_template', array('templateID' => $rowID, 'templateName' => $name, 'merchantID' => $user_id));
            if ($result == 1) {
                $result = 0;
            }

        } else {
            $result = $this->general_model->get_num_rows('tbl_email_template', array('templateName' => $name, 'merchantID' => $user_id));
        }
        if ($result == 0) {
            $response = true;
        } else {
            $this->form_validation->set_message('check_custom_template_name', 'Template name already exist');
            $response = false;
        }
        return $response;
    }

    public function email_temlate()
    {
        if ($this->session->userdata('logged_in')['merchID'] != "" || $this->session->userdata('user_logged_in')['merchantID'] != "") {
            $data['primary_nav'] = primary_nav();
            $data['template']    = template_variable();
            $data['login_info']  = $this->session->userdata('logged_in');
            if ($this->session->userdata('logged_in')) {
                $data['login_info'] = $this->session->userdata('logged_in');
                $user_id            = $data['login_info']['merchID'];
            } else if ($this->session->userdata('user_logged_in')) {
                $data['login_info'] = $this->session->userdata('user_logged_in');
                $user_id            = $data['login_info']['merchantID'];
            }
            $codition                 = array('merchantID' => $user_id);
            $templates                = $this->company_model->template_data_list($codition);
            $codition1                = array('merchantID' => $user_id, 'systemMail' => 0);
            $data['custom_templates'] = $this->company_model->custom_template_data_list($codition1);

            $data['templates'] = prepareMailList($templates, $user_id);

            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);
            $this->load->view('pages/page_email', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);
        } else {
            redirect('index', 'refresh');
        }
    }

    public function create_template()
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $this->session->userdata('user_logged_in')['merchantID'];
        }

        if (!empty($this->input->post(null, true))) {
            $tempID = '';
            if ($this->czsecurity->xssCleanPostInput('tempID') == "") {
                $this->form_validation->set_rules('templateName', 'Template Name', 'trim|required|callback_check_custom_template_name|xss-clean');
            }
            $this->form_validation->set_rules('emailSubject', 'Email Subject', 'required|xss_clean');
            $this->form_validation->set_rules('fromEmail', 'From Email', 'required|xss_clean');
            $tempID = $this->czsecurity->xssCleanPostInput('tempID');
            if ($this->form_validation->run() == true) {

                $templateName = $this->czsecurity->xssCleanPostInput('templateName');

                $fromEmail       = $this->czsecurity->xssCleanPostInput('fromEmail');
                $toEmail         = $this->czsecurity->xssCleanPostInput('toEmail');
                $addCC           = $this->czsecurity->xssCleanPostInput('ccEmail');
                $addBCC          = $this->czsecurity->xssCleanPostInput('bccEmail');
                $replyTo         = $this->czsecurity->xssCleanPostInput('replyEmail');
                $message         = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
                $subject         = $this->czsecurity->xssCleanPostInput('emailSubject');
                $mailDisplayName = $this->czsecurity->xssCleanPostInput('mailDisplayName');
                $createdAt       = date('Y-m-d H:i:s');

                if ($this->czsecurity->xssCleanPostInput('attachedTo')) {
                    $add_attachment = '1';
                } else {
                    $add_attachment = '0';
                }
                $message  = htmlspecialchars_decode($message);
                $insert_data = array(
                    'templateName'    => $templateName,

                    'merchantID'      => $user_id,
                    'fromEmail'       => $fromEmail,
                    'toEmail'         => $toEmail,
                    'addCC'           => $addCC,
                    'addBCC'          => $addBCC,
                    'replyTo'         => $replyTo,
                    'message'         => $message,
                    'emailSubject'    => $subject,
                    'attachedTo'      => $add_attachment,
                    'mailDisplayName' => $mailDisplayName,
                );
                $update_data = array(
                    'templateName'    => $templateName,
                    'merchantID'      => $user_id,
                    'fromEmail'       => $fromEmail,
                    'toEmail'         => $toEmail,
                    'addCC'           => $addCC,
                    'addBCC'          => $addBCC,
                    'replyTo'         => $replyTo,
                    'message'         => $message,
                    'emailSubject'    => $subject,
                    'attachedTo'      => $add_attachment,
                    'mailDisplayName' => $mailDisplayName,
                );

                if ($this->czsecurity->xssCleanPostInput('tempID') != "") {

                    $update_data['updatedAt'] = date('Y-m-d H:i:s');
                    $condition                = array('templateID' => $this->czsecurity->xssCleanPostInput('tempID'));
                    $data['templatedata']     = $this->general_model->update_row_data('tbl_email_template', $condition, $update_data, ['message']);
                } else {

                    $insert_data['systemMail'] = 0;
                    $insert_data['createdAt']  = date('Y-m-d H:i:s');
                    $id                        = $this->general_model->insert_row('tbl_email_template', $insert_data, ['message']);
                }
                redirect('Settingmail/email_temlate/custom_templates', 'refresh');
            } else {
                if (form_error('templateName') != "") {
                    $error['templateName'] = form_error('templateName');
                }
                if (form_error('emailSubject') != "") {
                    $error['emailSubject'] = form_error('emailSubject');
                }
                if (form_error('fromEmail') != "") {
                    $error['fromEmail'] = form_error('fromEmail');
                }

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> ' . implode(',', $error) . ' </div>');
            }

            redirect('Settingmail/create_template/' . $tempID, 'refresh');
        }
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['from_mail']       = DEFAULT_FROM_EMAIL;
        $data['mailDisplayName'] = $this->loginDetails['companyName'];

        if ($this->uri->segment('3')) {
            $temID                = $this->uri->segment('3');
            $condition            = array('templateID' => $temID);
            $templatedata         = $this->general_model->get_row_data('tbl_email_template', $condition);
            $data['templatedata'] = $templatedata;
            if ($templatedata && !empty($templatedata['fromEmail'])) {
                $data['from_mail'] = $templatedata['fromEmail'];
            }

            if ($templatedata && !empty($templatedata['mailDisplayName'])) {
                $data['mailDisplayName'] = $templatedata['mailDisplayName'];
            }
        }

        $data['types'] = $this->general_model->get_table_data('tbl_teplate_type', '');

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('pages/page_email_template', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function copy_template()
    {

        $temID        = $this->uri->segment('3');
        $condition    = array('templateID' => $temID);
        $templatedata = $this->general_model->get_row_data('tbl_email_template', $condition);

        $insert_data = array(
            'templateName'    => $templatedata['templateName'],
            'templateType'    => $templatedata['templateType'],
            'merchantID'      => $templatedata['merchantID'],
            'fromEmail'       => $templatedata['fromEmail'],
            'toEmail'         => $templatedata['toEmail'],
            'addCC'           => $templatedata['addCC'],
            'addBCC'          => $templatedata['addBCC'],
            'replyTo'         => $templatedata['replyTo'],
            'message'         => $templatedata['message'],
            'emailSubject'    => $templatedata['emailSubject'],
            'attachedTo'      => $templatedata['attachedTo'],
            'createdAt'       => date('Y-m-d H:i:s'),
            'mailDisplayName' => $templatedata['mailDisplayName'],
        );

     

        if ($this->general_model->insert_row('tbl_email_template', $insert_data)) {
            $this->session->set_flashdata('success', 'Successfully Copied Template');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }

        redirect('Settingmail/email_temlate', 'refresh');
    }

    public function delete_template()
    {

        $temID = $this->czsecurity->xssCleanPostInput('tempateDelID');
        $del   = $this->general_model->delete_row_data('tbl_email_template', array('templateID' => $temID, 'systemMail' => 0));
        if ($del) {
            $this->session->set_flashdata('success', ' Successfully Deleted Template');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }
        redirect('Settingmail/email_temlate/custom_templates', 'refresh');
    }

    public function view_template()
    {

        $temID     = $this->czsecurity->xssCleanPostInput('tempateViewID');
        $condition = array('templateID' => $temID);

        $view_data = $this->company_model->template_data($condition);

        ?>
		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-left"><strong> Template Name</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['templateName']; ?></a></td>
				</tr>
				<tr>
					<th class="text-left"><strong> From Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['fromEmail']; ?></a></td>
				</tr>
				<tr>
					<th class="text-left"><strong>Cc Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['addCC']; ?></a></td>
				</tr>
				<tr>
					<th class="text-left"><strong>Bcc Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['addBCC']; ?></a></td>
				</tr>

				<tr>
					<th class="text-left"><strong> Set to Reply</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['replyTo']; ?></a></td>
				</tr>
				<tr>
					<th class="text-left"><strong> Email Subject</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['emailSubject']; ?></a></td>
				</tr>
				<tr>
					<th class="text-left"><strong> Template Type</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['typeText']; ?></td>
				</tr>
				<tr>
					<th colspan="2" class="text-left"><strong> Message</strong></th>

				</tr>
				<tr>
					<td colspan="2" class="text-left"><?php echo $view_data['message']; ?></td>

				</tr>

			</tbody>
		</table>

		<?php
die;
    }

    public function set_template()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $invoiceID  = $this->czsecurity->xssCleanPostInput('invoiceID');
        $typeID     = $this->czsecurity->xssCleanPostInput('typeID');
        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        $condition  = array('templateType' => $typeID, 'merchantID' => $user_id);
        $view_data  = $this->company_model->template_data($condition);

        $merchant_data = $this->general_model->get_merchant_data(array('merchID' => $user_id));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $user_id));

        $currency            = "$";
        $customer            = '';
        $config_email        = $merchant_data['merchantEmail'];
        $merchant_name       = ($merchant_data['companyName'] != null)?$merchant_data['companyName']:$this->loginDetails['companyName'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $mphone              = $merchant_data['merchantContact'];
        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $tr_date             = '';
        $update_link         = $config_data['customerPortalURL'];
        if (!empty($config_data['ProfileImage'])) {
            $logo_url = base_url() . LOGOURL . $config_data['ProfileImage'];
        } else {
            $logo_url = CZLOGO;
        }

        $code     = '';
        $str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
        $shuffled = str_shuffle($str);
        $shuffled = substr($shuffled, 1, 12) . '=' . $user_id;
        $code     = $this->safe_encode($shuffled);

        if ($this->czsecurity->xssCleanPostInput('invoiceID') != "") {

            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
            $ttt       = explode(PLINK, $update_link);
            $purl = $ttt[0] . PLINK . '/customer/';
            $invcode = $this->safe_encode($invoiceID);
            $invURL = $purl . 'update_payment/' . $code . '/' . $invcode;
            $in_link = '<a  target="_blank" href="'.$invURL.'" class="btn btn-primary">Click Here</a>';
        }

        $company = '';

        $data['login_info'] = $merchant_data;
        $company            = $data['login_info']['companyName'];
        $cust_company       = '';
        $condition1         = " and Customer_ListID='" . $customerID . "' and  qbmerchantID='" . $user_id . "' ";
        if ($this->czsecurity->xssCleanPostInput('invoiceID') != "") {

            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
            $condition1 .= " and TxnID='" . $invoiceID . "' ";

            if ($typeID == '1') {

                $condition1 .= " and   `IsPaid` = 'false' and userStatus!='cancel'  ";
            }
            if ($typeID == '2') {


                $condition1 .= " and   `IsPaid` = 'false' and userStatus!='cancel'  ";
            }
            if ($typeID == '3') {
             
                $condition1 .= " and   `IsPaid` = 'false' and userStatus!='cancel'  ";
            }

            if ($typeID == '5') {

                $condition1 .= " and   `IsPaid` = 'true' and userStatus=''  ";
            }
            if ($typeID == '4') {
                $condition1 .= " and   `IsPaid` = 'false' and userStatus!='cancel'  ";
            }
        } else {

            if ($typeID == '12' || $typeID == '11') {

                $card_data = $this->card_model->get_expiry_card_data($customerID, '1');

                if (!empty($card_data)) {
                    $cardno        = $card_data['CustomerCard'];
                    $friendly_name = $card_data['customerCardfriendlyName'];
                }
            }
        }

        $data = $this->customer_model->get_invoice_data_template($condition1);
        $toEmail = '';

        if (!empty($data)) {
            $customer     = $data['FullName'];
            $amount       = (-$data['AppliedAmount']);
            $balance      = $data['BalanceRemaining'];
            $paymethods   = $data['paymentType'];
            $toEmail      = $data['email'];
            $duedate      = date('F d, Y', strtotime($data['DueDate']));
            $ref_number   = $data['RefNumber'];
            $tr_date      = date('F d, Y', strtotime($data['TimeCreated']));
            $cust_company = $data['companyName'];
        }

        if(!$view_data['replyTo'] || empty($view_data['replyTo'])){
            $view_data['replyTo'] = $config_email;
        }

        $subject = $view_data['emailSubject'];
        $subject = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $subject));
        $subject = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $subject));
        $subject = stripslashes(str_ireplace('{{company_name}}', $cust_company, $subject));

       
        if ($typeID == '11') {

            $subject = stripslashes(str_ireplace('{{creditcard.mask_number}}', $cardno, $view_data['emailSubject']));
        }

        $message = $view_data['message'];
        $message = stripslashes(str_ireplace('{{merchant_name}}', $merchant_name, $message));
        $message = stripslashes(str_ireplace('{{creditcard.mask_number}}', $cardno, $message));
        $message = stripslashes(str_ireplace('{{creditcard.type_name}}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{creditcard.url_updatelink}}', $update_link, $message));

        $message = stripslashes(str_ireplace('{{customer.company}}', $cust_company, $message));
        $message = stripslashes(str_ireplace('{{customer.name}}', $customer, $message));
        $message = stripslashes(str_ireplace('{{transaction.currency_symbol}}', $currency, $message));
        $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_ireplace('{{transaction.amount}}', ($balance) ? ('$' . number_format($balance, 2)) : '0.00', $message));
        $message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message));
        $message = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $message));
        $message = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $message));
        $message = stripslashes(str_ireplace('{{invoice.balance}}', '$' . number_format($balance, 2), $message));
        $message = stripslashes(str_ireplace('{{invoice.due_date|date("F j, Y")}}', $duedate, $message));
        $message = stripslashes(str_ireplace('{{invoice.days_overdue}}', $overday, $message));
        $message = stripslashes(str_ireplace('{{invoice.url_permalink}}', $in_link, $message));
        $message = stripslashes(str_ireplace('{{invoice_payment_pagelink}}', $in_link, $message));

        $message = stripslashes(str_ireplace('{{merchant_email}}', $config_email, $message));
        $message = stripslashes(str_ireplace('{{merchant_phone}}', $mphone, $message));
        $message = stripslashes(str_ireplace('{{current.date}}', $cur_date, $message));
        $message = stripslashes(str_ireplace('{{logo}}', "<img src='" . $logo_url . "'>", $message));

        $new_data_array = array();

        $new_data_array['message']      = $message;
        $new_data_array['emailSubject'] = $subject;
        $new_data_array['message']      = $message;
        $new_data_array['addCC']        = $view_data['addCC'];
        $new_data_array['addBCC']       = $view_data['addBCC'];
        $new_data_array['toEmail']      = $toEmail;
        $new_data_array['replyTo']      = $view_data['replyTo'];

        $new_data_array['fromEmail']      = ($view_data['fromEmail'] != null)?$view_data['fromEmail']:DEFAULT_FROM_EMAIL;
        $new_data_array['mailDisplayName']      = ($view_data['mailDisplayName'] != null)?$view_data['mailDisplayName']:$this->loginDetails['companyName'];
        $new_data_array['templateName']      = $view_data['templateName'];
        $new_data_array['invCode']      = $code;

        echo json_encode($new_data_array);
        die;
    }

    public function set_template_cus_details()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $new_data_array = array();
        if (!empty($this->czsecurity->xssCleanPostInput('templateID')) && !empty($this->czsecurity->xssCleanPostInput('customerID'))) {
            $templateID = $this->czsecurity->xssCleanPostInput('templateID');
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $condition  = array('templateID' => $templateID, 'merchantID' => $user_id);
            $view_data  = $this->company_model->template_data($condition);

            $merchant_data = $this->general_model->get_merchant_data(array('merchID' => $user_id));
            $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $user_id));

            $currency      = "$";
            $customer      = '';
            $config_email  = $merchant_data['merchantEmail'];
            $merchant_name = $merchant_data['companyName'];
            $logo_url      = $merchant_data['merchantProfileURL'];
            $mphone        = $merchant_data['merchantContact'];

            if (!empty($config_data['ProfileImage'])) {
                $logo_url = base_url() . LOGOURL . $config_data['ProfileImage'];
            } else {
                $logo_url = CZLOGO;
            }

            $cur_date            = date('Y-m-d');
            $amount              = '';
            $paymethods          = '';
            $transaction_details = '';
            $tr_data             = '';
            $ref_number          = '';
            $overday             = '';
            $balance             = '0.00';
            $in_link             = '';
            $duedate             = '';
            $company             = '';
            $cardno              = '';
            $expired             = '';
            $expiring            = '';
            $friendly_name       = '';
            $tr_date             = '';
            $update_link         = $config_data['customerPortalURL'];
            $code                = '';
            $company             = '';

            $c_data = $this->general_model->get_select_data('qb_test_customer', array('companyName', 'FullName', 'Contact'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));

            $cust_company = $c_data['companyName'];
            $customer     = $c_data['companyName'];
            $toEmail      = $c_data['Contact'];
            $subject      = $view_data['emailSubject'];
            $subject      = stripslashes(str_ireplace('{{company_name}}', $cust_company, $subject));
            $message      = $view_data['message'];
            $message      = stripslashes(str_ireplace('{{merchant_name}}', $merchant_name, $message));
            $message      = stripslashes(str_ireplace('{{customer.company}}', $cust_company, $message));
            $message      = stripslashes(str_ireplace('{{merchant_email}}', $config_email, $message));
            $message      = stripslashes(str_ireplace('{{merchant_phone}}', $mphone, $message));
            $message      = stripslashes(str_ireplace('{{current.date}}', $cur_date, $message));
            $message      = stripslashes(str_ireplace('{{logo}}', "<img src='" . $logo_url . "'>", $message));

            $new_data_array['message']      = $message;
            $new_data_array['emailSubject'] = $subject;
            $new_data_array['message']      = $message;
            $new_data_array['addCC']        = $view_data['addCC'];
            $new_data_array['addBCC']       = $view_data['addBCC'];
            $new_data_array['toEmail']      = $toEmail;
            $new_data_array['replyTo']      = $view_data['replyTo'];

            $new_data_array['invCode'] = $view_data['systemMail'];
        }

        echo json_encode($new_data_array);
        die;
    }

    public function set_due_template()
    {

        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        $companyID  = $this->czsecurity->xssCleanPostInput('companyID');
        $typeID     = $this->czsecurity->xssCleanPostInput('typeID');
        $customer   = $this->czsecurity->xssCleanPostInput('customer');

        $comp_data = $this->general_model->get_row_data('tbl_company', array('id' => $companyID));

        $condition     = array('templateType' => $typeID, 'merchantID' => $comp_data['merchantID']);
        $view_data     = $this->company_model->template_data($condition);
        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $comp_data['merchantID']));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchID' => $comp_data['merchantID']));

        $currency = "$";

        $config_email  = $merchant_data['merchantEmail'];
        $merchant_name = $merchant_data['companyName'];
        $logo_url      = $merchant_data['merchantProfileURL'];
        $mphone        = $merchant_data['merchantContact'];
        $cur_date      = date('Y-m-d');

        $currency = "$";

        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $update_link         = $config_data['customerPortalURL'];

        $data['login_info'] = $merchant_data;
        $company            = $merchant_data['companyName'];

        $condtion = " and Customer_ListID='" . $customerID . "' and  companyID='" . $companyID . "' ";
        if ($typeID == '1' || $typeID == '3') {
            $condtion .= " and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '" . date('Y-m-d') . "' AND `IsPaid` = 'false' and userStatus=''  ";
        }
        if ($typeID == '2') {
            $condtion .= " and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '" . date('Y-m-d') . "' AND `IsPaid` = 'false' and userStatus=''  ";
        }
        if ($typeID == '5') {
            $condtion .= " and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '" . date('Y-m-d') . "' AND `IsPaid` = 'false' and userStatus='' and  (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300' ";
        }
        if ($typeID == '12') {

            $card_data = $this->card_model->get_card_expiry_data($customerID);
            if (!empty($card_data)) {
                $cardno        = $card_data['CustomerCard'];
                $friendly_name = $card_data['customerCardfriendlyName'];
            }
        }
        $data = $this->customer_model->get_invoice_data_template($condtion);

        if (!empty($data)) {
            $customer   = $data['FullName'];
            $amount     = $data['AppliedAmount'];
            $balance    = $data['BalanceRemaining'];
            $paymethods = $data['paymentType'];

            $duedate    = date('F d, Y', strtotime($data['DueDate']));
            $ref_number = $data['RefNumber'];
        }

        $subject = stripslashes(str_ireplace('{{ invoice.refnumber }}', $ref_number, $view_data['emailSubject']));
        if ($view_data['emailSubject'] == "Welcome to { company_name }") {
            $subject = 'Welcome to ' . $company;
        }

        $message = $view_data['message'];

        $message = stripslashes(str_ireplace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.mask_number }}', $cardno, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.url_updatelink }}', $update_link, $message));
        $message = stripslashes(str_ireplace('{{ customer.company }}', $customer, $message));
        $message = stripslashes(str_ireplace('{{ transaction.currency_symbol }}', $currency, $message));
        $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_ireplace('{{ transaction.amount }}', ($amount) ? ($amount) : '0.00', $message));

        $message = stripslashes(str_ireplace(' {{ transaction.transaction_method }}', $transaction_details, $message));
        $message = stripslashes(str_ireplace('{{ transaction.transaction_date}}', $tr_data, $message));
        $message = stripslashes(str_ireplace('{{ transaction.transaction_detail }}', $transaction_details, $message));
        $message = stripslashes(str_ireplace('{{ invoice.days_overdue }}', $overday, $message));
        $message = stripslashes(str_ireplace('{{ invoice.refnumber }}', $ref_number, $message));
        $message = stripslashes(str_ireplace('{{invoice.balance}}', $balance, $message));
        $message = stripslashes(str_ireplace('{{ invoice.due_date|date("F j, Y") }}', $duedate, $message));

        $message = stripslashes(str_ireplace('{{ invoice.url_permalink }}', $update_link, $message));
        $message = stripslashes(str_ireplace('{{ merchant_email }}', $config_email, $message));
        $message = stripslashes(str_ireplace('{{ merchant_phone }}', $mphone, $message));
        $message = stripslashes(str_ireplace('{{ current.date }}', $cur_date, $message));

        $new_data_array                 = array();
        $new_data_array['message']      = $message;
        $new_data_array['emailSubject'] = $subject;
        $new_data_array['message']      = $message;
        $new_data_array['addCC']        = $view_data['addCC'];
        $new_data_array['addBCC']       = $view_data['addBCC'];

        $new_data_array['replyTo'] = $view_data['replyTo'];

        echo json_encode($new_data_array);
        die;
    }

    //-------------------   START -------------------------------//

    //---------- View email history ----------------------//

    public function get_history_id()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
        }

        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
        }

        $historyID    = $this->czsecurity->xssCleanPostInput('customertempID');
        $condition    = array('mailID' => $historyID);
        $historydatas = $this->general_model->get_row_data('tbl_template_data', $condition);

        if (!empty($historydatas)) {
            if(isset($data['login_info']['merchant_default_timezone']) && !empty($data['login_info']['merchant_default_timezone'])){
                $timezone = ['time' => $historydatas['emailsendAt'], 'current_format' => 'UTC', 'new_format' => $data['login_info']['merchant_default_timezone']];
                $historydatas['emailsendAt'] = getTimeBySelectedTimezone($timezone);
            }
            ?>


			<table class="table table-bordered table-striped table-vcenter">

				<tbody>


					<tr>
						<th class="text-right col-md-1 control-label"><strong> To </strong> </th>
						<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailto']; ?></a> </td>
					</tr>
					<tr>
						<th class="text-right col-md-1 control-label"><strong>Cc </strong></th>
						<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailcc']; ?> </a> </td>
					</tr>
					<tr>
						<th class="text-right col-md-1 control-label"><strong>Bcc </strong></th>
						<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailbcc']; ?></td>
					</tr>

					<tr>
						<th class="text-right col-md-1 control-label"><strong> Date</strong></th>
						<td class="text-left visible-lg col-md-11"> <?php echo date('M d, Y - h:i A', strtotime($historydatas['emailsendAt'])); ?> </a></td>
					</tr>

					<tr>
						<th class="text-right col-md-1 control-label"><strong>Subject</strong></th>
						<td class="text-left visible-lg col-md-11"> <?php echo $historydatas['emailSubject']; ?> </a></td>
					</tr>




					<tr>
						<td colspan="2" class="text-left"><?php echo $historydatas['emailMessage']; ?></td>

					</tr>

				</tbody>
			</table>

<?php }
        die;
    }

    public function send_mail()
    {
        if ($this->session->userdata('logged_in')) {

            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user_id));
        $logo_url      = '';
        $config_data   = $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID' => $user_id));
        if (!empty($config_data)) {
            $logo_url = base_url() . LOGOURL . $config_data['ProfileImage'];
        } else {
            $logo_url = CZLOGO;
        }

        $merchant_name = $merchant_data['companyName'];

        $this->load->library('email');

        $type      = $this->czsecurity->xssCleanPostInput('type');
        $invoiceID = '';
        $toEmail   = $this->czsecurity->xssCleanPostInput('toEmail');
        $addCC     = $this->czsecurity->xssCleanPostInput('ccEmail');
        $addBCC    = $this->czsecurity->xssCleanPostInput('bccEmail');
        $replyTo   = $this->czsecurity->xssCleanPostInput('replyEmail');
        $message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
        $subject   = $this->czsecurity->xssCleanPostInput('emailSubject');
        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceCode');

        $message = stripslashes(str_ireplace('{{logo}}', "<img src='$logo_url'>", $message));
        $message = stripslashes(str_ireplace('<div id="lg_div">&nbsp;</div>', "<img src='$logo_url'>", $message));

        $fromEmail       = $this->czsecurity->xssCleanPostInput('fromEmail');
        $mailDisplayName = $this->czsecurity->xssCleanPostInput('mailDisplayName');
        $fromEmail       = ($fromEmail == '') ? 'donotreply@payportal.com' : $fromEmail;
        $mailDisplayName = ($mailDisplayName == '') ? $merchant_name : $mailDisplayName;
        $replyTo       = ($replyTo == '') ? $merchant_data['merchantEmail'] : $replyTo;
        $date          = date('Y-m-d H:i:s');
        $customerID    = $this->czsecurity->xssCleanPostInput('customertempID');
        $invoicetempID = $this->czsecurity->xssCleanPostInput('invoicetempID');
        $email_data    = array(
            'customerID'   => $customerID,
            'merchantID'   => $user_id,
            'emailSubject' => $subject,
            'emailfrom'    => $fromEmail,
            'emailto'      => $toEmail,
            'emailcc'      => $addCC,
            'emailbcc'     => $addBCC,
            'emailreplyto' => $replyTo,
            'emailMessage' => $message,
            'emailsendAt'  => $date,

        );
        if ($invoiceID != '') {
            $email_data['emailCode'] = $invoiceID;
            $email_data['invoiceID'] = $invoicetempID;
        }
        $toArr = (explode(";",$toEmail));
        $toArrEmail = [];

        foreach ($toArr as $value) {
            # code...
            $toArrEmail[]['email'] = trim($value);
        }

        $toaddCCArrEmail = [];
        if($addCC != null){
            $toaddCCArr = (explode(";",$addCC));
            
            foreach ($toaddCCArr as $value) {
                # code...
                $toaddCCArrEmail[]['email'] = trim($value);
            }
        }
        
        $toaddBCCArrEmail = [];
        if($addBCC != null){
            $toaddBCCArr = (explode(";",$addBCC));
            
            foreach ($toaddBCCArr as $value) {
                # code...
                $toaddBCCArrEmail[]['email'] = trim($value);
            }
        }
        $file_name = '';
       
        if ($this->czsecurity->xssCleanPostInput('add_attachment') == 'on') {
            
            if ($invoicetempID != "") {
                $invoice = $this->company_model->get_invoice_details_data($invoicetempID);

                $inv_no = $invoice['invoice_data']['invoiceNumber'];

                if (!empty($invoice)) {
                    $this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'F');
                }

            }

            if ($inv_no != '') {
                $file_name = $invoicetempID . '-' . "$inv_no.pdf";
            } else {
                $file_name = "$invoicetempID.pdf";
            }

        }
        $request_array = [
            "personalizations" => [
                [
                    "to" => $toArrEmail,
                    "subject" => $subject,
                    "cc"=> $toaddCCArrEmail,
                    "bcc"=>$toaddBCCArrEmail
                ]
            ],
            "from" => [
                "email" => $fromEmail,
                "name" => $mailDisplayName
            ],
            "reply_to" => [
                "email" => $replyTo
            ],
            "content" => [
                [
                "type" => "text/html",
                "value" => $message
                ]
                ],
            
                    
        ];
        if(count($toaddCCArrEmail) == 0){
            unset($request_array['personalizations'][0]['cc']);
        }
        if(count($toaddBCCArrEmail) == 0){
            unset($request_array['personalizations'][0]['bcc']);
        }
        if($file_name!="")	
        {
            $attachments[] = [
                'filename' => $file_name,
                'type'=> 'text/plain',
                'content' => base64_encode(file_get_contents(FCPATH . '/uploads/invoice_pdf/' . $file_name))
            ];
            $request_array['attachments'] = $attachments;
        }
        // get api key for send grid
        $api_key = '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';
        $url = 'https://api.sendgrid.com/v3/mail/send';

        // set authorization header
        $headerArray = [
            'Authorization: Bearer '.$api_key,
            'Content-Type: application/json',
            'Accept: application/json'
        ];

        $ch = curl_init($url);
        curl_setopt ($ch, CURLOPT_POST, true);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        // add authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // parse header data from response
        $header_text = substr($response, 0, $header_size);
        // parse response body from curl response
        $body = substr($response, $header_size);

        $headers = createSendGridHeaders($header_text);
        

        // if mail sent success
        if($httpcode == '202' || $httpcode == '200'){
            $email_data['send_grid_email_id'] = isset($headers['X-Message-Id']) ? $headers['X-Message-Id'] : '';
            $email_data['send_grid_email_status'] = 'Sent';
            $this->general_model->insert_row('tbl_template_data', $email_data);

            $this->session->set_flashdata('success', 'Email Sent Successfully');
        } else {
            $email_data['send_grid_email_status'] = 'Failed';
            $email_data['mailStatus'] = 0;
            $this->general_model->insert_row('tbl_template_data', $email_data);
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>.</div>');
        }

        if (!empty($this->czsecurity->xssCleanPostInput('sendmailbyinvdtl'))) {
            redirect('home/invoice_details/' . $invoicetempID, 'refresh');
        } else {
            redirect('home/view_customer/' . $customerID, 'refresh');
        }
    }

    //-------------------   END -------------------------------//

    public function get_expiry_card_data($customerID, $type)
    {

        $card = array();
        $this->load->library('encrypt');

        if ($type == '1') {
            /**************Expired Card***********/

            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day )  order by expired_date asc  ";
        }

        if ($type == '0') {

            /**************Expiring SOON Card***********/
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  order by CardID desc  ";
        }
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();

        if (!empty($card_data)) {

            $card['CustomerCard']             = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
            $card['cardMonth']                = $card_data['cardMonth'];
            $card['cardYear']                 = $card_data['cardYear'];
            $card['expiry']                   = $card_data['expired_date'];
            $card['CardID']                   = $card_data['CardID'];
            $card['CardCVV']                  = $this->encrypt->decode($card_data['CardCVV']);
            $card['customerListID']           = $card_data['customerListID'];
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
        }

        return $card;
    }

    public function safe_encode($string)
    {
        return strtr(base64_encode($string), '+/=', '-_-');
    }
}
