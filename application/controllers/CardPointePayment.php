<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */


class CardPointePayment extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    private $gatewayEnvironment;
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/Cardpointe.class.php';

        $this->load->config('quickbooks');
        $this->load->model('quickbooks');
        $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
        $this->load->model('general_model');
        $this->load->model('company_model');
        $this->db1 = $this->load->database('otherdb', true);
        $this->load->model('customer_model');
        $this->load->model('card_model');
        $this->load->library('form_validation');
        $this->load->config('fluidpay');
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID = $merchID;
        $this->gatewayEnvironment = $this->config->item('environment');
    }
     /**
     * Redirect to transaction page 
     */
    public function index(){
        redirect('home/', 'refresh');
    }

    /**
     * pay invoice by invoice listing page
     * @return Receipt Page
    */
    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $da      = $this->session->userdata('logged_in');
            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da      = $this->session->userdata('user_logged_in');
            $user_id = $da['merchantID'];
        }

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
       
        $cusproID = '';
        $error    = '';
        $cusproID = $this->czsecurity->xssCleanPostInput('customerProcessID');
		$checkPlan = check_free_plan_transactions();

        if ($this->form_validation->run() == true) {
            $custom_data_fields = [];
            $cusproID  = '';
            $error     = '';
            $cusproID  = $this->czsecurity->xssCleanPostInput('customerProcessID');
            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
            $cardID    = $this->czsecurity->xssCleanPostInput('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
            }

            $gateway = $gatlistval;

            $amount     = $this->czsecurity->xssCleanPostInput('inv_amount');
            $in_data    = $this->quickbooks->get_invoice_data_pay($invoiceID, $user_id);
            $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($checkPlan && !empty($in_data)) {

                $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $cardpointeuser   = $gt_result['gatewayUsername'];
        	    $cardpointepass   = $gt_result['gatewayPassword'];
        	    $cardpointeMerchID = $gt_result['gatewayMerchantID'];
                $cardpointeSiteName = $gt_result['gatewaySignature'];
                $customerID = $in_data['Customer_ListID'];
                $c_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName','FirstName', 'LastName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                $companyID  = $c_data['companyID'];

                $Customer_ListID = $in_data['Customer_ListID'];

                $cardID_upd = '';

                if (!empty($cardID)) {

                    $cr_amount = 0;
                    $amount    = $this->czsecurity->xssCleanPostInput('inv_amount');

                    $name = $in_data['Customer_FullName'];
                    $client = new CardPointe();
                    if ($sch_method == "1") {

                        if ($cardID != "new1") {

                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
                            $cvv       = $card_data['CardCVV'];
                            $cardType  = $card_data['CardType'];
                            $address1  = $card_data['Billing_Addr1'];
                            $address2  = $card_data['Billing_Addr2'];
                            $city      = $card_data['Billing_City'];
                            $zipcode   = $card_data['Billing_Zipcode'];
                            $state     = $card_data['Billing_State'];
                            $country   = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        } else {

                            $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cardType = $this->general_model->getType($card_no);
                            $cvv      = '';
                            if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                            }

                            $address1 = $this->czsecurity->xssCleanPostInput('address1');
                            $address2 = $this->czsecurity->xssCleanPostInput('address2');
                            $city     = $this->czsecurity->xssCleanPostInput('city');
                            $country  = $this->czsecurity->xssCleanPostInput('country');
                            $phone    = $this->czsecurity->xssCleanPostInput('contact');
                            $state    = $this->czsecurity->xssCleanPostInput('state');
                            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        $exyear1  = substr($exyear, 2);
                        if (strlen($expmonth) == 1) {
                            $expmonth = '0' . $expmonth;
                        }
                        $expry = $expmonth . $exyear1;

                        $result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $address1, $city, $state, $zipcode);
                    } else if ($sch_method == "2") {
                        if ($cardID == 'new1') {
                            $accountDetails = [
                                'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                'customerListID'     => $customerID,
                                'companyID'          => $companyID,
                                'merchantID'         => $user_id,
                                'createdAt'          => date("Y-m-d H:i:s"),
                                'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                            ];
                        } else {
                            $accountDetails = $this->card_model->get_single_card_data($cardID);
                        }
                        $accountNumber = $accountDetails['accountNumber'];
                        $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        $result = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountDetails['accountNumber'], $accountDetails['routeNumber'], $amount, $accountDetails['accountType'], $name, $accountDetails['Billing_Addr1'], $accountDetails['Billing_City'], $accountDetails['Billing_State'], $accountDetails['Billing_Zipcode']);
                    }
                    $responseId = isset($result['retref'])?$result['retref']:'TXNFAILED'.time();
                    if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Success' || $result['resptext'] == "Approved") {
                        $responseId = $result['retref'];
                        $txnID                      = $in_data['TxnID'];
                        $ispaid                     = 'true';

                        $bamount = $in_data['BalanceRemaining'] - $amount;
                        if ($bamount > 0) {
                            $ispaid = 'false';
                        }

                        $app_amount = $in_data['AppliedAmount'] + (-$amount);
                        $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);

                        $condition = array('TxnID' => $in_data['TxnID']);
                        $this->general_model->update_row_data('qb_test_invoice', $condition, $data);

                        $user = $in_data['qbwc_username'];
                        if ($cardID == "new1") {
                            if ($sch_method == "1") {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $cardType = $this->general_model->getType($card_no);

                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                $card_data = array(
                                    'cardMonth'       => $expmonth,
                                    'cardYear'        => $exyear,
                                    'CardType'        => $cardType,
                                    'CustomerCard'    => $card_no,
                                    'CardCVV'         => $cvv,
                                    'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'  => $customerID,

                                    'companyID'       => $companyID,
                                    'merchantID'      => $this->merchantID,
                                    'createdAt'       => date("Y-m-d H:i:s"),
                                );
                                $id1 = $this->card_model->process_card($card_data);
                            } else if ($sch_method == "2") {
                                $id1 = $this->card_model->process_ack_account($accountDetails);
                            }
                        }

                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                        $ref_number     = $in_data['RefNumber'];
                        $tr_date        = date('Y-m-d H:i:s');
                        $toEmail        = $c_data['Contact'];
                        $company        = $c_data['companyName'];
                        $customer       = $c_data['FullName'];
                        if ($chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                        }
                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                    } else {
                        $err_msg  =  $result['resptext'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
                    }

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID = '', $this->resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                    if ($result['resptext'] == 'Approval' && !is_numeric($invoiceID)) {
                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
            }

        } else {
            $error = 'Validation Error. Please fill the requred fields';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
        }

        if ($cusproID == "2") {
            redirect('home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3" && $in_data['TxnID'] != '') {
            redirect('home/invoice_details/' . $in_data['TxnID'], 'refresh');
        }

        $invoice_IDs = array();

        $receipt_data = array(
            'proccess_url'      => 'home/invoices',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }
        redirect('home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
    }
    /**
     * Customer sale page transaction and return to reciept page
     * @return Receipt page
    */
    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $cardpointeuser   = $gt_result['gatewayUsername'];
            $cardpointepass   = $gt_result['gatewayPassword'];
            $cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $cardpointeSiteName  = $gt_result['gatewaySignature'];
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $custom_data_fields = [];
            $applySurcharge = false;
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            $inputData = $this->input->post();

            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                if ($this->session->userdata('logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                if (!$customerID || empty($customerID)) {
                    $customerID = create_card_customer($this->input->post(), '1');
                }

                $comp_data = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $qbd_comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
                $user      = $qbd_comp_data['qbwc_username'];

                $user_id     = $merchantID;
                $cardID      = $this->czsecurity->xssCleanPostInput('card_list');
                $contact     = $this->czsecurity->xssCleanPostInput('phone');
                $cvv         = '';
                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                        $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                    }

                    $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                    $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
                    $city     = $this->czsecurity->xssCleanPostInput('bcity');
                    $country  = $this->czsecurity->xssCleanPostInput('bcountry');
                    $phone    = $this->czsecurity->xssCleanPostInput('bphone');
                    $state    = $this->czsecurity->xssCleanPostInput('bstate');
                    $zipcode  = $this->czsecurity->xssCleanPostInput('bzipcode');

                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    if ($card_data['CardCVV']) {
                        $cvv = $card_data['CardCVV'];
                    }

                    $cardType = $card_data['CardType'];
                    $address1 = $card_data['Billing_Addr1'];
                    $address2 = $card_data['Billing_Addr2'];
                    $city     = $card_data['Billing_City'];
                    $zipcode  = $card_data['Billing_Zipcode'];
                    $state    = $card_data['Billing_State'];
                    $country  = $card_data['Billing_Country'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . $exyear1;

                $name    = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address = $this->czsecurity->xssCleanPostInput('address');
                $country = $this->czsecurity->xssCleanPostInput('country');
                $city    = $this->czsecurity->xssCleanPostInput('city');
                $state   = $this->czsecurity->xssCleanPostInput('state');
                $amount  = $this->czsecurity->xssCleanPostInput('totalamount');
                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;
                $zipcode = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '55555';
               
                $crtxnID = '';
                $invID   = '';
                $responseId = 'TXNFAILED'.time();
				$client = new CardPointe();

                $result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $inputData['baddress1'], $inputData['bcity'], $inputData['bstate'], $zipcode);
                
				if ($result['respcode'] == '00') {
                    $responseId = $result['retref'];

                    $invoiceIDs                 = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }
                    $refnum = array();
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

                            if (!empty($theInvoice))
                            {
                                
                                $amount_data = $theInvoice['BalanceRemaining'];
                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                    $actualInvoicePayAmount += $surchargeAmount;
                                    $updatedInvoiceData = [
                                        'inID' => $inID,
                                        'merchantID' => $user_id,
                                        'amount' => $surchargeAmount,
                                    ];
                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,2);
                                    $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
                                    $amount_data = $theInvoice['BalanceRemaining'];

                                }
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                if($saleAmountRemaining > 0){
                                    $refnum[] = $theInvoice['RefNumber'];
                                    if($amount_data == $actualInvoicePayAmount){
                                        $actualInvoicePayAmount = $amount_data;
                                        $isPaid      = 'true';

                                    }else{

                                        $actualInvoicePayAmount = $actualInvoicePayAmount;
                                        $isPaid      = 'false';
                                        $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                        
                                        
                                    }
                                    $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                    $tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                    $comp_data1 = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$user_id));
                                    $user      = $comp_data1['qbwc_username'] ;
                                    if(!is_numeric($inID))
                                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
                                }
                                
                            }
                            $payIndex++;
                            
                        }

                    } else {

                        $transactiondata = array();
                        $inID            = '';
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $contact,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                    $ref_number     = implode(',', $refnum);
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['Contact'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['FullName'];
                    if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $err_msg = $result['resptext'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>');
            }

        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => $_SERVER['REMOTE_ADDR'],
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('home/transation_sale_receipt', 'refresh');
    }
    /**
     * eCheck Sale A Transaction 
     * @return Receipt Page
    */
    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        
        if (!empty($this->input->post())) {

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $responseId = 'TXNFAILED'.time();
            $checkPlan = check_free_plan_transactions();

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $cardpointeuser   = $gt_result['gatewayUsername'];
			$cardpointepass   = $gt_result['gatewayPassword'];
			$cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $cardpointeSiteName = $gt_result['gatewaySignature'];
            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
            $name = $this->czsecurity->xssCleanPostInput('firstName'). " " . $this->czsecurity->xssCleanPostInput('lastName');
            $customerID   = $this->czsecurity->xssCleanPostInput('customerID');

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $comp_data = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                $invoiceIDs = [];
                
                $client = new CardPointe();
                $result = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountDetails['accountNumber'], $accountDetails['routeNumber'], $amount, $accountDetails['accountType'], $name, $accountDetails['Billing_Addr1'], $accountDetails['Billing_City'], $accountDetails['Billing_State'], $accountDetails['Billing_Zipcode']);
                if ($result['resptext'] == 'Approved' || $result['resptext'] == 'Approval') {
                    $responseId = $result['retref'];

					$invoiceIDs = [];
                    $invoicePayAmounts = [];
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }
					$refnum = array();
					$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));

					if (!empty($invoiceIDs)) {
                        $payIndex = 0;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();

							$theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

							if (!empty($theInvoice)) {
								$amount_data = $theInvoice['BalanceRemaining'];

                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                $refnum[] = $theInvoice['RefNumber'];

                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 'true';

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 'false';
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $txnAmount = $actualInvoicePayAmount;

                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                
                                $tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID, $crtxnID = '', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);

								$user      = $comp_data['qbwc_username'];
                                if(!is_numeric($inID))
								    $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
							}
                            $payIndex++;
						}
					} else {
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
					}

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number     = '';
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['companyEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['firstName'] . ' ' . $comp_data['lastName'];

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg = $result['resptext'];
                    $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                }
            
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            if(!$checkPlan){
                $responseId  = '';
            }
            
            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('Payments/create_customer_esale', 'refresh');
    }
     /**
     * Authorize Transaction and return to reciept page
     * @return Receipt page
    */
    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $inputData = $this->input->post();

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $cardpointeuser   = $gt_result['gatewayUsername'];
			$cardpointepass   = $gt_result['gatewayPassword'];
			$cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $cardpointeSiteName = $gt_result['gatewaySignature'];

            $checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }  
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                if (!$customerID || empty($customerID)) {
                    $customerID = create_card_customer($this->input->post(), '1');
                }

                $comp_data = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
                $companyID = $comp_data['companyID'];

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                } else {

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];

                    $exyear = $card_data['cardYear'];
                    $cvv = $card_data['CardCVV'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . $exyear1;

                $name    = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country = $this->czsecurity->xssCleanPostInput('bcountry');
                $city    = $this->czsecurity->xssCleanPostInput('bcity');
                $state   = $this->czsecurity->xssCleanPostInput('bstate');
                $amount  = $this->czsecurity->xssCleanPostInput('totalamount');
                $zipcode = ($this->czsecurity->xssCleanPostInput('bzipcode')) ? $this->czsecurity->xssCleanPostInput('bzipcode') : '55555';

                $name = $inputData['firstName'] . ' ' . $inputData['lastName'];
                $crtxnID = '';
                $invID = $inID = $responseId  = '';

                $client = new CardPointe();
                $result = $client->authorize($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $inputData['baddress1'], $inputData['bcity'], $inputData['bstate'], $zipcode);
                if ($result['respcode'] == '00') {
                    $responseId = $result['retref'];
                    /* This block is created for saving Card info in encrypted form  */

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                        $cardType = $this->general_model->getType($card_no);

                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $cardType,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('baddress1'),
                            'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('baddress2'),
                            'Billing_City'    => $this->czsecurity->xssCleanPostInput('bcity'),
                            'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                            'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                            'Billing_State'   => $this->czsecurity->xssCleanPostInput('bstate'),
                            'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                            'customerListID'  => $this->czsecurity->xssCleanPostInput('customerID'),
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                        );

                        $id1 = $this->card_model->process_card($card_data);
                    }
                    $this->session->set_flashdata('success', 'Successfully Authorized Credit Card Payment');
                } else {
                    $err_msg = $result['resptext'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed: '. $err_msg .'</strong></div>');
                }
                
                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>');
            }

            if(!$checkPlan){
                $responseId  = '';
            }

            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'Payments/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('home/transation_sale_receipt', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $compdata          = $this->customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/payment_auth', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    /**
     * Capture Transaction Amount and return to transaction page
     * @return Transaction page
    */
    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $cardpointeuser  = $gt_result['gatewayUsername'];
			$cardpointepass  =  $gt_result['gatewayPassword'];
			$cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $cardpointeSiteName  = $gt_result['gatewaySignature'];
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($tID != '' && !empty($gt_result)) {
                
                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);

                $customerID = $paydata['customerListID'];
                $amount     = $paydata['transactionAmount'];
                $customerID = $paydata['customerListID'];
                $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                $client = new CardPointe();
                $transactionData = [
                    "amount" => ((float) $amount),
                ];

                $result = $client->capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $tID, $transactionData);
                if ($result['respcode'] == '00') {
                    $res_code = 100;
                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "4");

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['Contact'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['FullName'];
                    if ($chh_mail == '1') {

                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                    }
                    $this->session->set_flashdata('success', ' Successfully Captured Authorization');
                } else {
                    $res_code = 300;
                    $err_msg = $result['resptext'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed:</strong>'. ' ' . $err_msg .'</div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');

            }
            $invoice_IDs = array();
            $receipt_data = array(
                'proccess_url'      => 'Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'transaction';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            
            if (!isset($result['data']) || $result['data']['id'] == '') {
                $result['transactionid'] = 'null';
            } else {
                $result['transactionid'] = $result['data']['id'];
            }

            redirect('home/transation_credit_receipt/' . $paydata['invoiceTxnID'] . '/' . $paydata['customerListID'] . '/' . $result['retref'], 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    /**
     * Void Transaction Amount and return to reciept page
     * @return Receipt page
    */
    public function create_customer_void()
    {
        $custom_data_fields = [];
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $con     = array(
                'transactionID' => $tID,
            );
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $cardpointeuser  = $gt_result['gatewayUsername'];
			$cardpointepass  =  $gt_result['gatewayPassword'];
			$cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $cardpointeSiteName  = $gt_result['gatewaySignature'];
            $client =  new CardPointe();

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($tID != '' && !empty($gt_result)) {
                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);

                $customerID = $paydata['customerListID'];
                $amount     = $paydata['transactionAmount'];
                $customerID = $paydata['customerListID'];
                $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                $result = $client->void($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $amount, $tID);
                if ($result['resptext'] == 'Approved' || $result['resptext'] == 'Approved') {
                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "3");

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                    if ($chh_mail == '1') {
                        $condition  = array('transactionID' => $tID);
                        $customerID = $paydata['customerListID'];

                        $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['Contact'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['FullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                    }

                    $this->session->set_flashdata('success', 'The transaction has been voided.');
                } else {
                    $err_msg = $result['resptext'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
            }
        }
        redirect('Payments/payment_capture', 'refresh');
    }

    /**
     * Display error 
     * @return String
     * @param Array()
    **/
    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }
    /**
     * Pay multiple invoice 
     * @return Redirect to page with message
    */
    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $user_id = $da['merchantID'];
        }

        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        $invoices   = $this->czsecurity->xssCleanPostInput('multi_inv');
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');
        $customerID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID, 'qbmerchantID' => $user_id));
        $companyID  = $comp_data['companyID'];

        $cusproID = '';
        $error    = '';
        $custom_data_fields = [];
        $cusproID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

        $resellerID = $this->resellerID;
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && !empty($invoices)) {
            foreach ($invoices as $invoiceID) {
                $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                $in_data     = $this->quickbooks->get_invoice_data_pay($invoiceID);

                $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $cardpointeuser   = $gt_result['gatewayUsername'];
                $cardpointepass   = $gt_result['gatewayPassword'];
                $cardpointeMerchID = $gt_result['gatewayMerchantID'];
                $cardpointeSiteName  = $gt_result['gatewaySignature'];
                $comp_data   = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'FirstName', 'LastName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                

                if ($cardID != "" || $gateway != "") {

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['Customer_ListID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
    
                            $address1 = $this->czsecurity->xssCleanPostInput('address1');
                            $address2 = $this->czsecurity->xssCleanPostInput('address2');
                            $city     = $this->czsecurity->xssCleanPostInput('city');
                            $country  = $this->czsecurity->xssCleanPostInput('country');
                            $phone    = $this->czsecurity->xssCleanPostInput('contact');
                            $state    = $this->czsecurity->xssCleanPostInput('state');
                            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        if (!empty($cardID)) {

                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount = $pay_amounts;
                            $amount = $amount - $cr_amount;

                            $name    = $in_data['Customer_FullName'];
                            $address = $in_data['ShipAddress_Addr1'];

                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;
                           
                            $client = new CardPointe();
                            
                            $responseId = $crtxnID = '';
                            
                            $result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $address1, $city, $state, $zipcode);
                            if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') {
                                $responseId = $result['retref'];
                                $txnID   = $in_data['TxnID'];
                                $ispaid  = 'true';
                                $bamount = $in_data['BalanceRemaining'] - $amount;
                                if ($bamount > 0) {
                                    $ispaid = 'false';
                                }

                                $app_amount = $in_data['AppliedAmount'] + (-$amount);
                                $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);
                                $condition  = array('TxnID' => $in_data['TxnID']);
                                $this->general_model->update_row_data('qb_test_invoice', $condition, $data);

                                $user = $in_data['qbwc_username'];

                                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $in_data['Customer_ListID'],

                                        'companyID'       => $comp_data['companyID'],
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                }

                                if ($chh_mail == '1') {
                                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                                    $ref_number     = $in_data['RefNumber'];
                                    $tr_date        = date('Y-m-d H:i:s');
                                    $toEmail        = $comp_data['Contact'];
                                    $company        = $comp_data['companyName'];
                                    $customer       = $comp_data['FullName'];

                                    $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                                }
                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            } else {
                                $err_msg = $result['resptext']; 
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $user_id, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                            if ( ($result['resptext'] == 'Approval'  || $result['resptext'] == 'Approved' ) && !is_numeric($invoiceID)) {
                                $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                            }

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>');
                }

            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Gateway and Card are required</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
        
        if ($cusproID != "") {
            redirect('home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('home/invoices', 'refresh');
        }

    }

}
