<?php

/**
 * Authorize.net Payment Gateway Operations
 * Sale using create_customer_sale
 * Authorize  using create_customer_auth
 * Capture using create_customer_capture 
 * Void using create_customer_void
 * Refund create_customer_refund
 * Single Invoice Payment using pay_invoice
 * Multiple Invoice Payment using multi_pay_invoice
 */

include_once APPPATH .'libraries/Manage_payments.php';

class AuthPayment extends CI_Controller
{
	private $resellerID;
	private $merchantID;
	private $transactionByUser;

	public function __construct()
	{
		parent::__construct();

		include APPPATH . 'third_party/Freshbooks.php';
		include APPPATH . 'third_party/FreshbooksNew.php';
		include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

		$this->load->config('auth_pay');
		$this->load->model('general_model');
		$this->load->model('card_model');
		$this->load->library('form_validation');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
		$this->db1 = $this->load->database('otherdb', true);
		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '3') {

			$logged_in_data = $this->session->userdata('logged_in');
            $this->merchantID = $logged_in_data['merchID'];
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$logged_in_data = $this->session->userdata('user_logged_in');
                  
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
			$this->merchantID = $merchID;
		} else {
			redirect('login', 'refresh');
		}

		$val = array(
			'merchantID' => $this->merchantID,
		);
		$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		if (empty($Freshbooks_data)) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');

			redirect('FreshBoolks_controllers/home/index', 'refresh');
		}

		$this->subdomain     = $Freshbooks_data['sub_domain'];
		$key                = $Freshbooks_data['secretKey'];
		$this->accessToken   = $Freshbooks_data['accessToken'];
		$this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
		$this->fb_account    = $Freshbooks_data['accountType'];
		$condition1 = array('resellerID' => $this->resellerID);
		$sub1 = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
		$domain1 = $sub1['merchantPortalURL'];
		$URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

		define('OAUTH_CONSUMER_KEY', $this->subdomain);
		define('OAUTH_CONSUMER_SECRET', $key);
		define('OAUTH_CALLBACK', $URL1);
	}


	public function index()
	{

		redirect('FreshBooks_controllers/home', 'refresh');
	}




	public function pay_invoice()
	{

		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");

		$cusproID = array();
		$error = array();
		$cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');


		$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
		$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
		$this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');

		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');

		if ($this->czsecurity->xssCleanPostInput('CardID') == "new1") {

			
			$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
		
			$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
			$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
		}

        $checkPlan = check_free_plan_transactions();

		if ($checkPlan && $this->form_validation->run() == true) {

			$merchID    = $this->merchantID;
			$user_id    = $merchID;
			$resellerID =	$this->resellerID;
			$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			
			$cardID = $this->czsecurity->xssCleanPostInput('CardID');
			if (!$cardID || empty($cardID)) {
			$cardID = $this->czsecurity->xssCleanPostInput('schCardID');
			}
	
			$gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
			if (!$gatlistval || empty($gatlistval)) {
			$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
			}
			$gateway = $gatlistval;
			$chh_mail  = 0;
			$in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

			if (!empty($in_data)) {

				$customerID = $in_data['Customer_ListID'];

				$companyID = $in_data['companyID'];
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));


				$apiloginID   = $gt_result['gatewayUsername'];
				$transactionKey   = $gt_result['gatewayPassword'];
				
				if ($cardID != "new1") {
					$card_data =   $this->card_model->get_single_card_data($cardID);

					$card_no  = $card_data['CardNo'];
					$expmonth =  $card_data['cardMonth'];
					$exyear   = $card_data['cardYear'];
					$cvv      = $card_data['CardCVV'];
					$cardType = $card_data['CardType'];
					$address1 = $card_data['Billing_Addr1'];
					$city     =  $card_data['Billing_City'];
					$zipcode  = $card_data['Billing_Zipcode'];
					$state    = $card_data['Billing_State'];
					$country  = $card_data['Billing_Country'];
				} else {
					$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$cardType = $this->general_model->getType($card_no);
					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('cvv') != "")
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
				}

				if ($in_data['BalanceRemaining'] > 0) {
					$cr_amount      = 0;


					$amount         =	 $this->czsecurity->xssCleanPostInput('inv_amount');
					$transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
					$transaction->setSandbox($this->config->item('auth_test_mode'));

					$transaction->__set('company', $in_data['companyName']);
					$transaction->__set('first_name', $in_data['firstName']);
					$transaction->__set('last_name', $in_data['lastName']);
					$transaction->__set('address', $address1);
					$transaction->__set('country', $country);
					$transaction->__set('city', $city);
					$transaction->__set('state', $state);
					$transaction->__set('phone', $in_data['phoneNumber']);

					$transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
					$exyear   = substr($exyear, 2);
					if (strlen($expmonth) == 1) {
						$expmonth = '0' . $expmonth;
					}
					$expry    = $expmonth . $exyear;
					$result = $transaction->authorizeAndCapture($amount, $card_no, $expry);


					$pinv_id = '';
					if ($result->response_code == "1"  && $result->transaction_id != 0 && $result->transaction_id != '') {


						$val = array(
							'merchantID' => $merchID,
						);

						$ispaid = 1;
						$st = 'paid';
						$bamount    = $in_data['BalanceRemaining'] - $amount;
						if ($bamount > 0) {
							$ispaid 	  = '0';
							$st         = 'unpaid';
						}
						$app_amount = $in_data['AppliedAmount'] + $amount;
						$up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
						$condition = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);

						$this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);


						if (isset($this->accessToken) && isset($this->access_token_secret)) {
							if ($this->fb_account == 1) {
								$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

								$payment = array('invoiceid' => $invoiceID, 'amount' => array('amount' => $amount), 'type' => 'Check', 'date' => date('Y-m-d'));
								$invoices   = $c->add_payment($payment);
							} else {
								$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

								$payment = '<?xml version="1.0" encoding="utf-8"?>  
                            						<request method="payment.create">  
                            						  <payment>         
                            						    <invoice_id>' . $invoiceID . '</invoice_id>               
                            						    <amount>' . $amount . '</amount>             
                            						    <currency_code>USD</currency_code> 
                            						    <type>Check</type>                   
                            						  </payment>  
                            						</request>';
								$invoices = $c->add_payment($payment);
							}




							if ($invoices['status'] != 'ok') {

								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');


							} else {
								$pinv_id =  $invoices['payment_id'];
								$this->session->set_flashdata('success', 'Successfully Processed Invoice');
							}
						}
						$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
							$ref_number =  $in_data['refNumber'];
							$tr_date   = date('Y-m-d H:i:s');
							$toEmail = $in_data['userEmail'];
							$company = $in_data['companyName'];
							$customer = $in_data['fullName'];
						if ($chh_mail == '1') {
							
							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
						}
						
						if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
							$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
							$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
							$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
							$card_type      = $this->general_model->getType($card_no);

							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	 => $exyear,
								'CardType'     => $card_type,
								'CustomerCard' => $card_no,
								'CardCVV'      => $cvv,
								'customerListID' => $customerID,
								'companyID'     => $companyID,
								'merchantID'   => $user_id,

								'createdAt' 	=> date("Y-m-d H:i:s"),
								'Billing_Addr1'	 => $address1,
								'Billing_Addr2'	 => $address2,
								'Billing_City'	 => $city,
								'Billing_State'	 => $state,
								'Billing_Country'	 => $country,
								'Billing_Contact'	 => $phone,
								'Billing_Zipcode'	 => $zipcode,
							);

							$id1 =    $this->card_model->process_card($card_data);
						}
					} else {

						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error ' . $result->response_reason_text . '</strong>.</div>');
					}


					$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $pinv_id, $this->resellerID, $in_data['invoiceID'], false, $this->transactionByUser);
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error ' . $result->response_reason_text . '</strong>.</div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Validation Error</strong>.</div>');
		}
		$trans_id = ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time();
		$invoice_IDs = array();
		$receipt_data = array(
			'proccess_url' => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
			'proccess_btn_text' => 'Process New Invoice',
			'sub_header' => 'Sale',
			'checkPlan'	=> $checkPlan
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		$this->session->set_userdata("invoice_IDs",$invoice_IDs);
		$this->session->set_userdata("in_data",$in_data);
		if ($cusproID != "") {
			redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		}

		 else {
			redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
		}
	}



	public function pay_invoicefff()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$user_id = $merchID;
		$invoicep_id = '';
		$rs_daata = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
		$resellerID =	$rs_daata['resellerID'];
		$condition1 = array('resellerID' => $resellerID);

		$sub1 = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
		$domain1 = $sub1['merchantPortalURL'];
		$URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

		$val = array(
			'merchantID' => $merchID,
		);
		$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

		$subdomain     = $Freshbooks_data['sub_domain'];
		$key           = $Freshbooks_data['secretKey'];
		$accessToken   = $Freshbooks_data['accessToken'];
		$access_token_secret = $Freshbooks_data['oauth_token_secret'];

		define('OAUTH_CONSUMER_KEY', $subdomain);
		define('OAUTH_CONSUMER_SECRET', $key);
		define('OAUTH_CALLBACK', $URL1);

		$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		$cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		$gateway			   = $this->czsecurity->xssCleanPostInput('gateway');
		$cusproID = array();
		$error = array();
		$cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');


		if (!empty($cardID) && !empty($gateway)) {

			$in_data =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));


			$apiloginID       = $gt_result['gatewayUsername'];
			$transactionKey   = $gt_result['gatewayPassword'];

			if (!empty($in_data)) {

				$Customer_ListID = $in_data['CustomerListID'];
				if ($cardID == 'new1') {
					$cardID_upd  = $cardID;
					$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
					$companyID = $c_data['companyID'];


					$this->load->library('encrypt');
					$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
					$merchantID = $merchID;


					$this->db1->where(array('customerListID' => $in_data['Customer_ListID'], 'merchantID' => $merchantID, 'customerCardfriendlyName' => $friendlyname));
					$this->db1->select('*')->from('customer_card_data');
					$qq =  $this->db1->get();

					if ($qq->num_rows() > 0) {

						$cardID = $qq->row_array()['CardID'];
						$card_data = array(
							'cardMonth'   => $expmonth,
							'cardYear'	 => $exyear,
							'CustomerCard' => $this->card_model->encrypt($card_no),
							'CardCVV'      => '', 
							'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
							'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
							'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
							'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
							'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
							'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
							'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
							'customerListID' => $in_data['Customer_ListID'],
							'companyID'     => $companyID,
							'merchantID'   => $merchantID,
							'customerCardfriendlyName' => $friendlyname,
							'updatedAt' 	=> date("Y-m-d H:i:s")
						);
						$this->db1->where(array('CardID' => $cardID));
						$this->db1->update('customer_card_data', $card_data);
					} else {
						$is_default = 0;
				     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
			        	if($checkCustomerCard == 0){
			        		$is_default = 1;
			        	}

						$card_data = array(
							'cardMonth'   => $expmonth,
							'cardYear'	 => $exyear,
							'CustomerCard' => $this->card_model->encrypt($card_no),
							'CardCVV'      => '', 
							'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
							'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
							'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
							'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
							'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
							'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
							'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
							'customerListID' => $in_data['Customer_ListID'],
							'companyID'     => $companyID,
							'merchantID'   => $merchantID,
							'is_default'			   => $is_default,
							'customerCardfriendlyName' => $friendlyname,
							'createdAt' 	=> date("Y-m-d H:i:s")
						);



						$this->db1->insert('customer_card_data', $card_data);
						$cardID = $this->db1->insert_id();
					}
				}

				$card_data      =   $this->card_model->get_single_card_data($cardID);

				if (!empty($card_data)) {

					if ($in_data['BalanceRemaining'] > 0) {
						$cr_amount = 0;
						$amount  =	 $in_data['BalanceRemaining'];

						$amount = $this->czsecurity->xssCleanPostInput('inv_amount');

						$amount           = $amount - $cr_amount;
						$transaction1 = new AuthorizeNetAIM($apiloginID, $transactionKey);
						$transaction1->setSandbox($this->config->item('auth_test_mode'));

						$card_no  = $card_data['CardNo'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						$exyear   = substr($exyear, 2);
						if (strlen($expmonth) == 1) {
							$expmonth = '0' . $expmonth;
						}
						$expry    = $expmonth . $exyear;


						$result = $transaction1->authorizeAndCapture($amount, $card_no, $expry);

						if ($result->response_code == "1"  && $result->transaction_id != 0 && $result->transaction_id != '') {


							if ($this->session->userdata('logged_in')) {
								$user_id = $this->session->userdata('logged_in')['merchID'];
								$merchID = $user_id;
							} else if ($this->session->userdata('user_logged_in')) {
								$user_id = $this->session->userdata('user_logged_in');
								$merchID = $user_id['merchantID'];
							}


							if (isset($accessToken) && isset($access_token_secret)) {
								$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);

								$payment = '<?xml version="1.0" encoding="utf-8"?>  
                						<request method="payment.create">  
                						  <payment>         
                						    <invoice_id>' . $invoiceID . '</invoice_id>               
                						    <amount>' . $amount . '</amount>             
                						    <currency_code>USD</currency_code> 
                						    <type>Check</type>                   
                						  </payment>  
                						</request>';

								$invoices = $c->add_payment($payment);

								if ($invoices['status'] != 'ok') {
									$err = "The Status code is: " . $error->getHttpStatusCode() . "\n";
									
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error ' . $err . '</strong></div>');

									redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
								} else {
									$invoicep_id = $invoices['payment_id'];
									
									$this->session->set_flashdata('success', 'Success!');
									
								}
							}
						} else {

							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result->response_reason_text . '</strong>.</div>');
						}

						$transactiondata = array();
						if ($result->transaction_id != 0 && $result->transaction_id != '') 
						{
							$transactiondata['transactionID']       = $result->transaction_id;
						}else{
							$transactiondata['transactionID']       = 'TXNFAILED'.time();
						}
						
						$transactiondata['transactionStatus']   = $result->response_reason_text;
						$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
						$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
						$transactiondata['transactionCode']     = $result->response_code;
						$transactiondata['transactionCard']     = substr($result->account_number, 4);

						$transactiondata['transactionType']    = $result->transaction_type;
						$transactiondata['gatewayID']           = $gateway;
						$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
						$transactiondata['customerListID']      = $in_data['CustomerListID'];
						$transactiondata['transactionAmount']   = $result->amount;
						$transactiondata['merchantID']   = $merchID;
						$transactiondata['invoiceID']   = $invoiceID;
						$transactiondata['qbListTxnID'] =   $invoicep_id;
						$transactiondata['resellerID']   = $resellerID;
						$transactiondata['gateway']   = "Auth";
						$CallCampaign = $this->general_model->triggerCampaign($merchID,$transactiondata['transactionCode']); 

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 
						
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>');
		}

		if ($cusproID != "") {
			redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $cusproID, 'refresh');
		} else {
			redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
		}
	}




	public function create_customer_sale_old()
	{

		if (!empty($this->input->post(null, true))) {
			$inv_array  = array();
			$inv_invoice = array();
			$qblist = array();
			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$resellerID =	$this->resellerID;

			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			if ($gatlistval != "" && !empty($gt_result)) {
				$apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];

				$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
				$cardID = $this->czsecurity->xssCleanPostInput('card_list');
				$invoiceIDs = '';
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				}
				$transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
				$transaction->setSandbox($this->config->item('auth_test_mode'));



				if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

					$card_no = $this->czsecurity->xssCleanPostInput('card_number');
					
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$exyear1   = substr($exyear, 2);
					$expry    = $expmonth . $exyear;

				} else {


					$card_data = $this->card_model->get_single_card_data($cardID);
					$card_no = $card_data['CardNo'];
					$expmonth =  $card_data['cardMonth'];

					$exyear   = $card_data['cardYear'];
					$exyear1   = substr($exyear, 2);
					if (strlen($expmonth) == 1) {
						$expmonth = '0' . $expmonth;
					}
					$expry    = $expmonth . $exyear;

				}


				$transaction->__set('company', $this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->__set('first_name', $this->czsecurity->xssCleanPostInput('fistName'));
				$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
				$transaction->__set('country', $this->czsecurity->xssCleanPostInput('country'));
				$transaction->__set('city', $this->czsecurity->xssCleanPostInput('city'));
				$transaction->__set('state', $this->czsecurity->xssCleanPostInput('state'));
				$transaction->__set('phone', $this->czsecurity->xssCleanPostInput('phone'));

				$transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
				$amount = $this->czsecurity->xssCleanPostInput('totalamount');

				$result = $transaction->authorizeAndCapture($amount, $card_no, $expry);


				if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '') {

					/*********** fresh book API************/

					$condition1 = array('resellerID' => $resellerID);

					$sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
					$domain1 = $sub1['merchantPortalURL'];
					$URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

					$val = array(
						'merchantID' => $merchantID,
					);
					$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

					$subdomain     = $Freshbooks_data['sub_domain'];
					$key           = $Freshbooks_data['secretKey'];
					$accessToken   = $Freshbooks_data['accessToken'];
					$access_token_secret = $Freshbooks_data['oauth_token_secret'];

					define('OAUTH_CONSUMER_KEY', $subdomain);
					define('OAUTH_CONSUMER_SECRET', $key);
					define('OAUTH_CALLBACK', $URL1);



					if (isset($accessToken) && isset($access_token_secret)) {

						$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);


						if (!empty($invoiceIDs)) {

							foreach ($invoiceIDs as $inID) {

								$con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
								$res = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining'), $con);

								$amount_data = $res['BalanceRemaining'];
								$invID       = $inID;

								$payment = '<?xml version="1.0" encoding="utf-8"?>  
												<request method="payment.create">  
												  <payment>         
													<invoice_id>' . $invID . '</invoice_id>               
													<amount>' . $amount_data . '</amount>             
													<currency_code>USD</currency_code> 
													<type>Check</type>                   
												  </payment>  
												</request>';

								$invoices = $c->add_payment($payment);



								if ($invoices['status'] != 'ok') {

									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

									redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
								} else {

									$inv_array['inv_no'][] = $invID;
									$inv_array['inv_amount'][] = $amount_data;
									$inv_invoice[]  =   $invID;
									$pinv_id =  $invoices['payment_id'];
									$qblist[] = $pinv_id;
									
									$this->session->set_flashdata('success', 'Transaction Successful');
								}
							}
						} else {
							$this->session->set_flashdata('success', 'Transaction Successful');
						
							
						}
					}
					

					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {
						$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
						$card_no = $this->czsecurity->xssCleanPostInput('card_number');
						$card_type      = $this->general_model->getType($card_no);
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');

						$cvv     = $this->czsecurity->xssCleanPostInput('cvv');

						$card_data = array(
							'cardMonth'   => $expmonth,
							'cardYear'	     => $exyear,
							'CardType'     => $card_type,
							'companyID'    => $companyID,
							'merchantID'   => $merchantID,
							'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
							'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
							'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
							'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
							'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
							'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
							'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
							'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
							'CustomerCard' => $card_no,
							'CardCVV'      => $cvv,
							'updatedAt'    => date("Y-m-d H:i:s")
						);



						$this->card_model->process_card($card_data);
					}

					if ($chh_mail == '1') {

						$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
						if (!empty($refNumber))
							$ref_number = implode(',', $refNumber);
						else
							$ref_number = '';
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
					}
					
					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result->response_reason_text . '</strong>.</div>');
				}

				$transactiondata = array();
				if (isset($result->transaction_id)) {
					$transactiondata['transactionID']       = $result->transaction_id;
					$transactiondata['transactionCode']     = $result->response_code;
				} else {
					$transactiondata['transactionID']  = '';
					$transactiondata['transactionCode']     = 400;
				}

				$transactiondata['transactionStatus']    = $result->response_reason_text;
				$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
				$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
				
				$transactiondata['transactionCard']     = substr($result->account_number, 4);
				$transactiondata['gatewayID']            = $gatlistval;
				$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
				$transactiondata['transactionType']    = $result->transaction_type;
				$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
				$transactiondata['transactionAmount']   = $result->amount;
				$transactiondata['merchantID']   = $merchantID;
				$transactiondata['resellerID']   = $this->resellerID;
				$transactiondata['gateway']   = "Auth";

				if (!empty($invoiceIDs) && !empty($inv_array)) {
					$transactiondata['invoiceID']            = implode(',', $inv_invoice);
					$transactiondata['invoiceRefID']         = json_encode($inv_array);
					$transactiondata['qbListTxnID']          = implode(',', $qblist);
				}

				$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				
				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>');
			}
		}
		redirect('FreshBooks_controllers/Transactions/create_customer_sale', 'refresh');
	}

	public function create_customer_sale()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$user_id = $this->merchantID;
        $checkPlan = check_free_plan_transactions();
		
		if ($checkPlan && !empty($this->input->post(null, true))) {
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {

				$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
			}



			if ($this->form_validation->run() == true) {

				$custom_data_fields = [];
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }

				$gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				if (!empty($gt_result)) {
					$apiloginID   = $gt_result['gatewayUsername'];
					$transactionKey   = $gt_result['gatewayPassword'];
					$invoiceIDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}

					$customerID = $this->czsecurity->xssCleanPostInput('customerID');

					$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
					$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
					$companyID  = $comp_data['companyID'];
					$cardID = $this->czsecurity->xssCleanPostInput('card_list');

					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						if ($this->czsecurity->xssCleanPostInput('cvv') != "")
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					} else {
						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no  = $card_data['CardNo'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						if ($card_data['CardCVV'])
							$cvv      = $card_data['CardCVV'];
						$cardType = $card_data['CardType'];
						$address1 = $card_data['Billing_Addr1'];
						$city     =  $card_data['Billing_City'];
						$zipcode  = $card_data['Billing_Zipcode'];
						$state    = $card_data['Billing_State'];
						$country  = $card_data['Billing_Country'];
					}
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');


					$transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
					$transaction->setSandbox($this->config->item('auth_test_mode'));

					$transaction->__set('company', $this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->__set('first_name', $this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->__set('address', $address1);
					$transaction->__set('country', $country);
					$transaction->__set('city', $city);
					$transaction->__set('state', $state);
					$transaction->__set('phone', $phone);

					$transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
					$exyear   = substr($exyear, 2);
					if (strlen($expmonth) == 1) {
						$expmonth = '0' . $expmonth;
					}
					$expry    = $expmonth . $exyear;

					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
	                    $transaction->__set('invoice_num', $new_invoice_number);
	                }

	                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                    $transaction->__set('po_num', $this->czsecurity->xssCleanPostInput('po_number'));
	                }

					$amount = $this->czsecurity->xssCleanPostInput('totalamount');
					$result = $transaction->authorizeAndCapture($amount, $card_no, $expry);


					$pinv_id = '';
					if ($result->response_code == "1" && $result->transaction_id != 0 && $result->transaction_id != '') {

						
						include APPPATH . 'libraries/Freshbooks_data.php';
						$fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
						$invoiceIDs = array();
						if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
							$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						}

						$refNumber = array();

						if (!empty($invoiceIDs)) {

							foreach ($invoiceIDs as $inID) {

								$in_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invoiceID' => $inID));
								$refNumber[] = 	$in_data['refNumber'];
								$amount_data = $amount;
								$pinv_id = '';
								$invoices =  $fb_data->create_invoice_payment($inID, $amount_data);
								if ($invoices['status'] != 'ok') {
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
								} else {
									$pinv_id =  $invoices['payment_id'];

									$this->session->set_flashdata('success', 'Successfully Processed Invoice');
								}

								$id = $this->general_model->insert_gateway_transaction_data($result, 'auth_capture', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
							}
						} else {
							$crtxnID = '';
							$inID    = '';
							$id      = $this->general_model->insert_gateway_transaction_data($result, 'auth_capture', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
						}
						$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
						if (!empty($refNumber))
							$ref_number = implode(',', $refNumber);
						else
							$ref_number = '';
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						if ($chh_mail == '1') {

							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
						}
						

						if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
							$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
							$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
							$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
							$card_type = $this->general_model->getType($card_no);

							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	 => $exyear,
								'CardType'     => $card_type,
								'CustomerCard' => $card_no,
								'CardCVV'      => $cvv,
								'customerListID' => $customerID,
								'companyID'     => $companyID,
								'merchantID'   => $this->merchantID,

								'createdAt' 	=> date("Y-m-d H:i:s"),
								'Billing_Addr1'	 => $address1,
								'Billing_Addr2'	 => $address2,
								'Billing_City'	 => $city,
								'Billing_State'	 => $state,
								'Billing_Country'	 => $country,
								'Billing_Contact'	 => $phone,
								'Billing_Zipcode'	 => $zipcode,
							);

							$id1 =    $this->card_model->process_card($card_data);
						}

						$this->session->set_flashdata('success', 'Transaction Successful');
					} else {
						$crtxnID = '';
						$msg = $result->response_reason_text;

						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
						$id = $this->general_model->insert_gateway_transaction_data($result, 'auth_capture', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
				}
			} else {


				$error = 'Validation Error. Please fill the requred fields';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
			}
		}
		$invoice_IDs = array();
		if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
		}
	
		$receipt_data = array(
			'transaction_id' => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
			'IP_address' => getClientIpAddr(),
			'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
			'proccess_btn_text' => 'Process New Sale',
			'sub_header' => 'Sale',
			'checkPlan'	=> $checkPlan
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		$this->session->set_userdata("invoice_IDs",$invoice_IDs);
		
		
		redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
		
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];

		$compdata				= $this->fb_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/payment_sale', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function create_customer_auth()
	{

		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
        $checkPlan = check_free_plan_transactions();
		
		if ($checkPlan && !empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			if ($gatlistval != "" && !empty($gt_result)) {
				$apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];

				$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];

				$transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
				$transaction->setSandbox($this->config->item('auth_test_mode'));
				$cardID = $this->czsecurity->xssCleanPostInput('card_list');
				if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

					$card_no = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$exyear1   = substr($exyear, 2);
					$expry    = $expmonth . $exyear;

				} else {


					$card_data = $this->card_model->get_single_card_data($cardID);
					$card_no = $card_data['CardNo'];
					$expmonth =  $card_data['cardMonth'];

					$exyear   = $card_data['cardYear'];
					$exyear1   = substr($exyear, 2);
					if (strlen($expmonth) == 1) {
						$expmonth = '0' . $expmonth;
					}
					$expry    = $expmonth . $exyear;


				}


				$transaction->__set('company', $this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->__set('first_name', $this->czsecurity->xssCleanPostInput('fistName'));
				$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
				$transaction->__set('country', $this->czsecurity->xssCleanPostInput('country'));
				$transaction->__set('city', $this->czsecurity->xssCleanPostInput('city'));
				$transaction->__set('state', $this->czsecurity->xssCleanPostInput('state'));
				
				$transaction->__set('phone', $this->czsecurity->xssCleanPostInput('phone'));

				$transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
				$amount = $this->czsecurity->xssCleanPostInput('totalamount');

				
				$result = $transaction->authorizeOnly($amount, $card_no, $expry);


				if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {
					$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
					$card_no = $this->czsecurity->xssCleanPostInput('card_number');
					$card_type      = $this->general_model->getType($card_no);
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');

					$cvv     = $this->czsecurity->xssCleanPostInput('cvv');

					$card_data = array(
						'cardMonth'   => $expmonth,
						'cardYear'	     => $exyear,
						'CardType'     => $card_type,
						'companyID'    => $companyID,
						'merchantID'   => $merchantID,
						'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'CustomerCard' => $card_no,
						'CardCVV'      => $cvv,
						'updatedAt'    => date("Y-m-d H:i:s")
					);



					$this->card_model->process_card($card_data);
				}

				

				if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '')
				{
					if ($chh_mail == '1') {

						$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
						if (!empty($refNumber))
							$ref_number = implode(',', $refNumber);
						else
							$ref_number = '';
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
					}

					$this->session->set_flashdata('success', ' Transaction Successful');

					$transactiondata = array();
					$transactiondata['transactionID']       = $result->transaction_id;
					$transactiondata['transactionStatus']    = $result->response_reason_text;
					$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
					$transactiondata['transactionCode']     = $result->response_code;
					$transactiondata['transactionCard']     = substr($result->account_number, 4);
					$transactiondata['gatewayID']            = $gatlistval;
					$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
					$transactiondata['transactionType']    = $result->transaction_type;
					$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					$transactiondata['transactionAmount']   = $result->amount;
					$transactiondata['merchantID']   = $merchantID;
					$transactiondata['resellerID']   = $this->resellerID;
					$transactiondata['gateway']   = "Auth";
					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

					if(!empty($this->transactionByUser)){
					    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
					$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result->response_reason_text . '</strong>.</div>');
				}

			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>');
			}
		}
		$invoice_IDs = array();
	
		$receipt_data = array(
			'transaction_id' => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
			'IP_address' => getClientIpAddr(),
			'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_auth',
			'proccess_btn_text' => 'Process New Transaction',
			'sub_header' => 'Authorize',
			'checkPlan'	=> $checkPlan
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		$this->session->set_userdata("invoice_IDs",$invoice_IDs);
		
		
		redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');	
		
	}


	public function create_customer_void_old()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if (!empty($this->input->post(null, true))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID1');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];
			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			} else if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$apiloginID  = $gt_result['gatewayUsername'];
			$transactionKey  =  $gt_result['gatewayPassword'];

			$transaction =  new AuthorizeNetAIM($apiloginID, $transactionKey);
			$transaction->setSandbox($this->config->item('auth_test_mode'));


			$customerID = $paydata['customerListID'];
			$amount  =  $paydata['transactionAmount'];

			$result     = $transaction->void($tID);

			if ($result->response_code == '1') {

				
				$condition = array('transactionID' => $tID);

				$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

				$this->general_model->update_row_data('customer_transaction', $condition, $update_data);


				$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result->response_reason_text . '</strong>.</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']       = $result->transaction_id;
			$transactiondata['transactionStatus']    = $result->response_reason_text;
			$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionCode']     = $result->response_code;
			$transactiondata['gatewayID']            = $gatlistval;
			$transactiondata['transactionGateway']    = $gt_result['gatewayType'];

			$transactiondata['transactionType']    = $result->transaction_type;
			$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
			$transactiondata['transactionAmount']   = $result->amount;
			$transactiondata['merchantID']  = $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = "Auth";
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);


			redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
		}
	}
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID1');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			if ($tID != '' && !empty($gt_result)) {
				$apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  =  $gt_result['gatewayPassword'];

				$transaction =  new AuthorizeNetAIM($apiloginID, $transactionKey);
				$transaction->setSandbox($this->config->item('auth_test_mode'));


				$customerID = $paydata['customerListID'];
				$amount  =  $paydata['transactionAmount'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
				$result     = $transaction->void($tID);

				if ($result->response_code == '1') {

					
					$condition = array('transactionID' => $tID);

					$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

					$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

					if ($chh_mail == '1') {
						$condition = array('transactionID' => $tID);
						$customerID = $paydata['customerListID'];
						$tr_date   = date('Y-m-d H:i:s');
						$ref_number =  $tID;
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
					}
					$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');

				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  "' . $result->response_reason_text . '"</strong></div>');
				}
				$transactiondata = array();
				$transactiondata['transactionID']       = $result->transaction_id;
				$transactiondata['transactionStatus']    = $result->response_reason_text;
				$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
				$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
				$transactiondata['transactionCode']     = $result->response_code;
				$transactiondata['gatewayID']            = $gatlistval;
				$transactiondata['transactionGateway']    = $gt_result['gatewayType'];

				$transactiondata['transactionType']    = $result->transaction_type;
				$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
				$transactiondata['transactionAmount']   = $result->amount;
				$transactiondata['merchantID']   = $merchantID;
				$transactiondata['gateway']   = "Auth";
				$transactiondata['resellerID']   =  $this->resellerID;
				$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}
			redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
		}
	}

	/*****************Capture Transaction***************/
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
			$tID     = $this->czsecurity->xssCleanPostInput('txnID1');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$merchantID = $merchantID;
			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;

			$apiloginID  = $gt_result['gatewayUsername'];
			$transactionKey  =  $gt_result['gatewayPassword'];


			$transaction =  new AuthorizeNetAIM($apiloginID, $transactionKey);
			$transaction->setSandbox($this->config->item('auth_test_mode'));



			$customerID = $paydata['customerListID'];
			$amount  =  $paydata['transactionAmount'];
			
			$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
			$result     = $transaction->priorAuthCapture($tID, $amount);

			if ($result->response_code == '1') {


				$condition = array('transactionID' => $tID);

				$update_data =   array('transaction_user_status' => "4");

				$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

				$condition = array('transactionID' => $tID);
				$customerID = $paydata['customerListID'];
				$tr_date   = date('Y-m-d H:i:s');
				$ref_number =  $tID;
				$toEmail = $comp_data['userEmail'];
				$company = $comp_data['companyName'];
				$customer = $comp_data['fullName'];
				if ($chh_mail == '1') {
					
					$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
				}
				
				$this->session->set_flashdata('success', 'Successfully Captured Authorization');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
			}

			$transactiondata = array();
			$transactiondata['transactionID']       = $result->transaction_id;
			$transactiondata['transactionStatus']    = $result->response_reason_text;
			$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionCode']     = $result->response_code;
			$transactiondata['transactionCard']     = $result->account_number;
			$transactiondata['gatewayID']            = $gatlistval;
			$transactiondata['transactionGateway']    = $gt_result['gatewayType'];

			$transactiondata['transactionType']    = $result->transaction_type;
			$transactiondata['customerListID']      = $customerID;
			$transactiondata['transactionAmount']   = $result->amount;
			$transactiondata['merchantID']         =  $merchantID;
			$transactiondata['resellerID']         = $this->resellerID;
			$transactiondata['gateway']             = "Auth";
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			$invoice_IDs = array();
			
		
			$receipt_data = array(
				'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Capture',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result->transaction_id == ''){
				$result->transaction_id ='null';
			}
			redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->transaction_id,  'refresh');
			
		}


		return false;
	}
	public function create_customer_capture_old()
	{

		//Show a form here which collects someone's name and e-mail address

		if (!empty($this->input->post(null, true))) {
			$tID     = $this->czsecurity->xssCleanPostInput('txnID1');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$merchantID = $this->session->userdata('logged_in')['merchID'];
			if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			} else if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}
			
			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			$apiloginID  = $gt_result['gatewayUsername'];
			$transactionKey  =  $gt_result['gatewayPassword'];

			$transaction =  new AuthorizeNetAIM($apiloginID, $transactionKey);
			$transaction->setSandbox($this->config->item('auth_test_mode'));

			$customerID = $paydata['customerListID'];
			$amount  =  $paydata['transactionAmount'];

			$result     = $transaction->priorAuthCapture($tID, $amount);

			if ($result->response_code == '1') {


				$condition = array('transactionID' => $tID);

				$update_data =   array('transaction_user_status' => "4");

				$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

				$this->session->set_flashdata('success', 'Successfully Captured Authorization');

			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result->response_reason_text . '</strong>.</div>');
			}

			$transactiondata = array();
			$transactiondata['transactionID']       = $result->transaction_id;
			$transactiondata['transactionStatus']    = $result->response_reason_text;
			$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionCode']     = $result->response_code;
			$transactiondata['transactionCard']     = $result->account_number;
			$transactiondata['gatewayID']            = $gatlistval;
			$transactiondata['transactionGateway']    = $gt_result['gatewayType'];

			$transactiondata['transactionType']    = $result->transaction_type;
			$transactiondata['customerListID']      = $customerID;
			$transactiondata['transactionAmount']   = $result->amount;
			$transactiondata['merchantID']  =  $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = "Auth";

			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

			redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');

		}


		return false;
	}



	public function create_customer_refund()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		//Show a form here which collects someone's name and e-mail address

		if (!empty($this->input->post(null, true))) {



			$tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			$apiloginID  = $gt_result['gatewayUsername'];
			$transactionKey  =  $gt_result['gatewayPassword'];

			$transaction =  new AuthorizeNetAIM($apiloginID, $transactionKey);
			$transaction->setSandbox($this->config->item('auth_test_mode'));

			$merchantID = $paydata['merchantID'];

			$card    = $paydata['transactionCard'];
			$customerID = $paydata['customerListID'];
			$amount     =  $paydata['transactionAmount'];
			$amount     = $this->czsecurity->xssCleanPostInput('ref_amount');
			$result     = $transaction->credit($tID, $amount, $card);



			if ($result->response_code == '1') {

				$val = array(
					'merchantID' => $paydata['merchantID'],
				);

				$merchID = $paydata['merchantID'];


				$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
				$redID = $this->resellerID;
				$condition1 = array('resellerID' => $redID);
				$sub1 = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
				$domain1 = $sub1['merchantPortalURL'];
				$URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';  

				$subdomain     = $Freshbooks_data['sub_domain'];
				$key           = $Freshbooks_data['secretKey'];
				$accessToken   = $Freshbooks_data['accessToken'];
				$access_token_secret = $Freshbooks_data['oauth_token_secret'];

				define('OAUTH_CONSUMER_KEY', $subdomain);
				define('OAUTH_CONSUMER_SECRET', $key);
				define('OAUTH_CALLBACK', $URL1);

				if (!empty($paydata['invoiceID'])) {
					$refund = $amount;
					$ref_inv = explode(',', $paydata['invoiceID']);
					if (count($ref_inv) > 1) {
						$ref_inv_amount = json_decode($paydata['invoiceRefID']);
						$imn_amount =  $ref_inv_amount->inv_amount;
						$p_ids     =   explode(',', $paydata['qbListTxnID']);

						$inv_array = array();
						foreach ($ref_inv as $k => $r_inv) {

							$inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

							if (!empty($inv_data)) {
								$due_date =   date('Y-m-d', strtotime($inv_data['DueDate']));
							} else {
								$due_date =   date('Y-m-d');
							}


							$re_amount = $imn_amount[$k];
							if ($refund > 0 && $imn_amount[$k] > 0) {


								$p_refund1 = 0;

								$p_id  =  $p_ids[$k];

								$ittem =  $this->fb_customer_model->get_invoice_item_data($r_inv);

								if ($refund <= $imn_amount[$k]) {
									$p_refund1 =  $refund;
									$p_refund =   $imn_amount[$k] - $refund;
									$re_amount = $imn_amount[$k] - $refund;
									$refund = 0;
								} else {
									$p_refund = 0;
									$p_refund1 =  $imn_amount[$k];
									$refund = $refund - $imn_amount[$k];

									$re_amount = 0;
								}


								if (isset($accessToken) && isset($access_token_secret)) {
									$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

									$payment = '<?xml version="1.0" encoding="utf-8"?>  
                    						<request method="payment.update">  
                    						  <payment>         
                    						    
                                                    <payment_id>' . $p_id . '</payment_id>
                                                    <amount>' . $p_refund . '</amount>
                                                    <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>              
                    						  
                    						</request>';

									$invoices = $c->edit_payment($payment);



									if ($invoices['status'] != 'ok') {

										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

										

									} else {
										$ins_id = $p_id;
										$refnd_trr = array(
											'merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
											'creditInvoiceID' => $r_inv, 'creditTransactionID' => $tID,
											'creditTxnID' => $ins_id, 'refundCustomerID' => $paydata['customerListID'],
											'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'),
										);
										$ppid =  $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
										$lineArray = '';

										foreach ($ittem as $item_val) {

											$lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
											$lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
											$lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
											$lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
											$lineArray .= '<type>Item</type></line>';
										}
										$lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
										$lineArray .= '<description>This is used to refund Incoive payment</description>';
										$lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
										$lineArray .= '<quantity>1</quantity>';
										$lineArray .= '<type>Item</type></line>';



										$invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update"> 
                                     		 <invoice> 
                                     		  <invoice_id>' . $r_inv . '</invoice_id> 
                                     		  
                                     		   <date>' . $due_date . '</date>
                                     		  
                        				          <lines>' . $lineArray . '</lines>  
                        						</invoice>  
                        						</request> ';

										$invoices1 = $c->add_invoices($invoice);
										$this->session->set_flashdata('success', 'Successfully Refunded Payment');

										
									}
								}


								$inv_array['inv_no'][] = $r_inv;
								$inv_array['inv_amount'][] = $re_amount;
							} else {

								$inv_array['inv_no'][] = $r_inv;
								$inv_array['inv_amount'][] = $re_amount;
							}
						}
						$this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
					} else {

						$ittem =  $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

						$p_id     = $paydata['qbListTxnID'];
						$p_amount = $paydata['transactionAmount'] - $amount;

						if (isset($accessToken) && isset($access_token_secret)) {
							$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

							$payment = '<?xml version="1.0" encoding="utf-8"?>  
            						<request method="payment.update">  
            						  <payment>         
            						    
                                            <payment_id>' . $p_id . '</payment_id>
                                            <amount>' . $p_amount . '</amount>
                                            <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>              
            						  
            						</request>';

							$invoices = $c->edit_payment($payment);



							if ($invoices['status'] != 'ok') {

								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

							} else {
								$ins_id = $p_id;
								$refnd_trr = array(
									'merchantID' => $paydata['merchantID'], 'refundAmount' => $amount,
									'creditInvoiceID' => $paydata['invoiceID'], 'creditTransactionID' => $tID,
									'creditTxnID' => $ins_id, 'refundCustomerID' => $paydata['customerListID'],
									'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'),
								);
								$ppid =  $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
								$lineArray = '';

								foreach ($ittem as $item_val) {

									$lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
									$lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
									$lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
									$lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
									$lineArray .= '<type>Item</type></line>';
								}
								$lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
								$lineArray .= '<description>This is used to refund Incoive payment</description>';
								$lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
								$lineArray .= '<quantity>1</quantity>';
								$lineArray .= '<type>Item</type></line>';



								$invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update"> 
                 		 <invoice> 
                 		  <invoice_id>' . $paydata['invoiceID'] . '</invoice_id> 
                 		  
    				          <lines>' . $lineArray . '</lines>  
    						</invoice>  
    						</request> ';

								$invoices1 = $c->add_invoices($invoice);
								$this->session->set_flashdata('success', 'Successfully Refunded Payment');

							}
						}
					}
				}

				$this->customer_model->update_refund_payment($tID, 'AUTH');
				$this->session->set_flashdata('success', 'Successfully Refunded Payment');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result->response_reason_text . '</strong>.</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']       = $result->transaction_id;
			$transactiondata['transactionStatus']    = $result->response_reason_text;
			$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionCode']     = $result->response_code;
			$transactiondata['gatewayID']            = $gatlistval;
			$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
			if (!empty($paydata['invoiceID'])) {
				$transactiondata['invoiceID']  = $paydata['invoiceID'];
			}
			$transactiondata['transactionType']    = $result->transaction_type;
			$transactiondata['customerListID']      = $customerID;
			$transactiondata['transactionAmount']   = $result->amount;


			$transactiondata['merchantID']   = $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = "Auth";
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

			
			$invoice_IDs = array();
			
		
			$receipt_data = array(
				'proccess_url' => 'FreshBooks_controllers/Transactions/payment_refund',
				'proccess_btn_text' => 'Process New Refund',
				'sub_header' => 'Refund',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result->transaction_id == ''){
				$result->transaction_id ='null';
			}
			redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->transaction_id,  'refresh');
		}
	}


	public function get_single_card_data($cardID)
	{

		$card = array();
		$this->load->library('encrypt');


		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    ";
		$query1 = $this->db1->query($sql);
		$card_data =   $query1->row_array();
		if (!empty($card_data)) {

			$card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']);
			$card['cardMonth']  = $card_data['cardMonth'];
			$card['cardYear']  = $card_data['cardYear'];
			$card['CardID']    = $card_data['CardID'];
			$card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
			$card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
		}

		return  $card;
	}




	public function Auth_Process()
	{

		$tran = new AuthorizeNetAIM('6L7z3mEakZjV', '367sbrCsKp878N4j');
		$data = $tran->authorizeOnly('20', '4111111111111111', '1217');

		echo "<pre>";
		print_r($data);
		die;
	}


	public function Void_Process()
	{

		$tran = new AuthorizeNetAIM('6L7z3mEakZjV', '367sbrCsKp878N4j');
		$data = $tran->void($tran_id);
		echo "<pre>";
		print_r($data);
		die;
	}





	public function create_customer_esale()
	{
		if ($this->session->userdata('logged_in')) {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {

			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			$checkPlan = check_free_plan_transactions();

			if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
				$apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];

				$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
				$companyID  = $comp_data['companyID'];

				$transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
				$transaction->setSandbox($this->config->item('auth_test_mode'));

		    	$customerID	= $this->czsecurity->xssCleanPostInput('customerID');
				$payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
				$sec_code =     'WEB';
				
				if($payableAccount == '' || $payableAccount == 'new1') {
					$accountDetails = [
						'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
						'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
						'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'companyID'     => $companyID,
						'merchantID'   => $merchantID,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $sec_code
					];
				} else {
					$accountDetails = $this->card_model->get_single_card_data($payableAccount);
				}

				$transaction->setECheck($accountDetails['routeNumber'], $accountDetails['accountNumber'], $accountDetails['accountType'], $bank_name='Wells Fargo Bank NA', $accountDetails['accountName'], $accountDetails['secCodeEntryMethod']);

				$transaction->__set('company', $this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->__set('first_name', $this->czsecurity->xssCleanPostInput('fistName'));
				$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->__set('address', $this->czsecurity->xssCleanPostInput('baddress'));
				$transaction->__set('country', $this->czsecurity->xssCleanPostInput('bcountry'));
				$transaction->__set('city', $this->czsecurity->xssCleanPostInput('bcity'));
				$transaction->__set('state', $this->czsecurity->xssCleanPostInput('bstate'));
				$transaction->__set('zip', $this->czsecurity->xssCleanPostInput('bzipcode'));

				$transaction->__set('ship_to_address', $this->czsecurity->xssCleanPostInput('address'));
				$transaction->__set('ship_to_country', $this->czsecurity->xssCleanPostInput('country'));
				$transaction->__set('ship_to_city', $this->czsecurity->xssCleanPostInput('city'));
				$transaction->__set('ship_to_state', $this->czsecurity->xssCleanPostInput('state'));
				$transaction->__set('ship_to_zip', $this->czsecurity->xssCleanPostInput('zipcode'));


				$transaction->__set('phone', $this->czsecurity->xssCleanPostInput('phone'));

				$transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
				$amount = $this->czsecurity->xssCleanPostInput('totalamount');

				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 3);
                    $transaction->__set('invoice_num', $new_invoice_number);
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $transaction->__set('po_num', $this->czsecurity->xssCleanPostInput('po_number'));
                }
                
				$result = $transaction->authorizeAndCapture($amount);


				if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '') {

					/* This block is created for saving Card info in encrypted form  */
					
					if($payableAccount == '' || $payableAccount == 'new1') {
						$id1 = $this->card_model->process_ack_account($accountDetails);
					}

					$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
					$customerID = '';
					$ref_number = '';
					$tr_date   = date('Y-m-d H:i:s');
					$toEmail = $comp_data['userEmail'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['fullName'];
					

					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
				}

				$transactiondata = array();
				if ($result->transaction_id != 0 && $result->transaction_id != '') 
				{
					$transactiondata['transactionID']       = $result->transaction_id;
					$transactiondata['transactionCode']     = $result->response_code;
				}else{
					$transactiondata['transactionID']       = 'TXNFAILED'.time();
					$transactiondata['transactionCode']     = 400;
				}
				$transactiondata['transactionStatus']    = $result->response_reason_text;
				$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
				$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
				$transactiondata['transactionCard']     = substr($result->account_number, 4);
				$transactiondata['gatewayID']            = $gatlistval;
				$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
				$transactiondata['transactionType']    = $result->transaction_type;
				$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
				$transactiondata['transactionAmount']   = $result->amount;
				$transactiondata['merchantID']   = $merchantID;
				$transactiondata['resellerID']   = $this->resellerID;
				$transactiondata['gateway']   = "AUTH ECheck";
				$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
			}

			$receipt_data = array(
				'transaction_id' => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_esale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
				'checkPlan'	=> $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh'); 

		} else {
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please fill all details.</div>'); 		
		}
		redirect('FreshBooks_controllers/Transactions/create_customer_esale','refresh');
	}




	public function payment_erefund()
	{
		//Show a form here which collects someone's name and e-mail address

		if (!empty($this->input->post(null, true))) {



			$tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			$apiloginID  = $gt_result['gatewayUsername'];
			$transactionKey  =  $gt_result['gatewayPassword'];


			$transaction =  new AuthorizeNetAIM($apiloginID, $transactionKey);
			$transaction->setSandbox($this->config->item('auth_test_mode'));


			$transaction->__set('method', 'echeck');

			$customerID = $paydata['customerListID'];
			$amount     =  $paydata['transactionAmount'];

			$result     = $transaction->credit($tID, $amount);

			if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				
			} else if ($this->session->userdata('user_logged_in')) {
				$user_id = $this->session->userdata('user_logged_in');
				$merchantID = $user_id['merchantID'];
			}
			if ($result->response_code == '1') {




				$c_data               =  $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));	
				
				$condition         = array('templateType' => '7');
				$view_data         = $this->company_model->template_data($condition);
				$merchant_data    = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID));
				$config_data       = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchantID));
				$currency          = "$";
				$subject           =  $view_data['emailSubject'];
				$config_email      = $merchant_data['merchantEmail'];
				$merchant_name     = $merchant_data['companyName'];
				$logo_url          = $merchant_data['merchantProfileURL'];
				$mphone            =  $merchant_data['merchantContact'];
				$cur_date          = date('Y-m-d');
				$currency          = "$";
				$customer          = $c_data['fullName'];
				$cardno              = '';
				$cardno              = $paydata['transactionCard'];
				$message = $view_data['message'];
				$message = stripslashes(str_replace('{{ merchant_name }}', $merchant_name, $message));
				$message = stripslashes(str_replace('{{ logo }}', "<img src='$logo_url'>", $message));
				$message = stripslashes(str_replace('{{ transaction.amount }}', ($amount) ? ($amount) : '0.00', $message));
				$message = stripslashes(str_replace('{{ transaction.currency_symbol }}', $currency, $message));
				$message = stripslashes(str_replace('{{ customer.company }}', $customer, $message));
				$message = stripslashes(str_replace('{{ merchant_email }}', $config_email, $message));
				$message = stripslashes(str_replace('{{ transaction.transaction_date}}', $cur_date, $message));
				$message = stripslashes(str_replace('{{ creditcard.mask_number }}', $cardno, $message));
				$message = stripslashes(str_replace('{{invoice.currency_symbol}}', $currency, $message));

				$fromEmail    = $merchant_data['merchantEmail'];
				$toEmail     = $c_data['userEmail'];
				$addCC      = $view_data['addCC'];
				$addBCC		= $view_data['addBCC'];
				$replyTo    = $view_data['replyTo'];
				$this->load->library('email');
				$email_data    = array(
					'customerID' => $customerID,
					'merchantID' => $merchant_data['merchID'],
					'emailSubject' => $subject,
					'emailfrom' => $fromEmail,
					'emailto' => $toEmail,
					'emailcc' => $addCC,
					'emailbcc' => $addBCC,
					'emailreplyto' => $replyTo,
					'emailMessage' => $message,
					'emailsendAt' => date("Y-m-d H:i:s"),

				);


				$this->email->clear();
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from($fromEmail, $merchant_name);
				$this->email->to($toEmail);
				$this->email->subject($subject);
				$this->email->message($message);
				if ($this->email->send()) {
					$this->general_model->insert_row('tbl_template_data', $email_data);
				}



				$this->customer_model->update_refund_payment($tID, 'AUTH');
				$this->session->set_flashdata('success', 'Successfully Refunded Payment');
				
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']       = $result->transaction_id;
			$transactiondata['transactionStatus']    = $result->response_reason_text;
			$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
			$transactiondata['transactionCode']     = $result->response_code;
			$transactiondata['gatewayID']            = $gatlistval;
			$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result->transaction_type;
			$transactiondata['customerListID']      = $customerID;
			$transactiondata['transactionAmount']   = $result->amount;
			$transactiondata['merchantID']   = $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = "AUTH Echeck";
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

			redirect('FreshBooks_controllers/Transactions/echeck_transaction', 'refresh');
		}
	}


	public function payment_evoid()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if (!empty($this->input->post(null, true))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID1');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			if ($tID != '' && !empty($gt_result)) {
				$apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  =  $gt_result['gatewayPassword'];

				$transaction =  new AuthorizeNetAIM($apiloginID, $transactionKey);
				$transaction->setSandbox($this->config->item('auth_test_mode'));

				$transaction->__set('method', 'echeck');
				$customerID = $paydata['customerListID'];
				$amount  =  $paydata['transactionAmount'];

				$result     = $transaction->void($tID);
				if ($result->response_code == '1') {

					
					$condition = array('transactionID' => $tID);

					$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

					$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

					$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  "' . $result->response_reason_text . '"</strong></div>');
				}
				if($this->session->userdata('logged_in')){
					$user_id = $this->session->userdata('logged_in')['merchID'];
					$merchID = $user_id;
				} else if ($this->session->userdata('user_logged_in')) {
					$user_id = $this->session->userdata('user_logged_in');
					$merchID = $user_id['merchantID'];
				}
				$transactiondata = array();
				$transactiondata['transactionID']       = $result->transaction_id;
				$transactiondata['transactionStatus']    = $result->response_reason_text;
				$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
				$transactiondata['transactionCode']     = $result->response_code;
				$transactiondata['gatewayID']            = $gatlistval;
				$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
				$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
				$transactiondata['transactionType']    = $result->transaction_type;
				$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
				$transactiondata['transactionAmount']   = $result->amount;
				$transactiondata['merchantID']   = $merchID;
				$transactiondata['gateway']   = "AUTH Echeck";
				$transactiondata['resellerID']   = $this->resellerID;
				$CallCampaign = $this->general_model->triggerCampaign($merchID,$transactiondata['transactionCode']);

				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}

			redirect('FreshBooks_controllers/Transactions/evoid_transaction', 'refresh');
		}
	}


	/*****************Delete Payment Transaction****************/



	public function delete_freshbooks_transaction()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}


		$rs_daata = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
		$resellerID =	$rs_daata['resellerID'];
		$condition1 = array('resellerID' => $resellerID);

		$setMailVoid     =  ($this->czsecurity->xssCleanPostInput('setMailVoid')) ? 1 : 0; 
		$payment_capture_page     =  ($this->czsecurity->xssCleanPostInput('payment_capture_page')) ? $this->czsecurity->xssCleanPostInput('payment_capture_page') : 0; 

		$sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
		$domain1 = $sub1['merchantPortalURL'];
		$URL1    = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';
		$val = array(
			'merchantID' => $merchID,
		);
		$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

		$subdomain     = $Freshbooks_data['sub_domain'];
		$key           = $Freshbooks_data['secretKey'];
		$accessToken   = $Freshbooks_data['accessToken'];
		$access_token_secret = $Freshbooks_data['oauth_token_secret'];

		define('OAUTH_CONSUMER_KEY', $subdomain);
		define('OAUTH_CONSUMER_SECRET', $key);
		define('OAUTH_CALLBACK', $URL1);


		if (!empty($this->czsecurity->xssCleanPostInput('paytxnID'))) {
			$ptxnID = $this->czsecurity->xssCleanPostInput('paytxnID');
			if(isset($_POST['txnvoidID'])){
                $paydata = $this->general_model->get_row_data('customer_transaction', array('transactionID' => $_POST['txnvoidID'], 'merchantID' => $merchID));
            }else{
                $paydata = $this->general_model->get_row_data('customer_transaction', array('id' => $ptxnID, 'merchantID' => $merchID));
            }
            $ptxnID = $paydata['id'];
            $transaction_id = $paydata['transactionID'];
			if (!empty($paydata)) {

				$transactionGateway = $paydata['transactionGateway'];
                $gatlistval = $paydata['gatewayID'];
                $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			    $paymentType = (strrpos($paydata['gateway'], 'ECheck'))? 2 : 1;
                
                 $voidObj = new Manage_payments($merchID);
				 $voidedTransaction = $voidObj->voidTransaction([
				 	'trID' => $paydata['transactionID'],
				 	'gatewayID' => $gatlistval,
				 	'paymentType' => $paymentType,
				 ]);
				 if($voidedTransaction){
					if ((!empty($paydata['qbListTxnID']))) {
						$qb_txn_id =   $paydata['qbListTxnID'];
	
	
	
	
						if (isset($accessToken) && isset($access_token_secret)) {
							$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);
	
	
							$payment = '<?xml version="1.0" encoding="utf-8"?>  
													   <request method="payment.delete">
														   <payment_id>' . $qb_txn_id . '</payment_id>
														</request>';
	
							$invoices = $c->add_invoices($payment);
	
							if ($invoices != null) {
								$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - ' . $invoices . '</strong></div>');
							} else {
								$st = $this->general_model->delete_row_data('customer_transaction', array('id' => $ptxnID, 'merchantID' => $merchID));
								$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
							}
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>');
						}
					} else {
	
						if ($paydata['invoiceID'] == '' || $paydata['invoiceID'] == NULL) {
	
							$st = $this->general_model->delete_row_data('customer_transaction', array('id' => $ptxnID, 'merchantID' => $merchID));
							if ($st) {
								$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
							} else {
								$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed</strong></div>');
							}
						}
					}
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>');
			}
			$receipt_data = array(
                'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header' => 'Void',
            );
            
            $this->session->set_userdata("receipt_data",$receipt_data);            
            
            redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$transaction_id,  'refresh');
                
			if($payment_capture_page)
				redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
			else
				redirect('FreshBooks_controllers/Transactions/payment_transaction','refresh');

		} else {

			redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
		}
	}



	public function pay_multi_invoice()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$user_id = $merchID;
		$invoicep_id = '';
		$rs_daata = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
		$resellerID =	$rs_daata['resellerID'];
		$condition1 = array('resellerID' => $resellerID);

		$sub1 = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
		$domain1 = $sub1['merchantPortalURL'];
		$URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

		$cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		$gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');

        $checkPlan = check_free_plan_transactions();

		if ($checkPlan && $cardID != "" || $gateway != "") {
			$invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));


			$apiloginID       = $gt_result['gatewayUsername'];
			$transactionKey   = $gt_result['gatewayPassword'];
			$val = array(
				'merchantID' => $merchID,
			);

			$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

			$subdomain     = $Freshbooks_data['sub_domain'];
			$key           = $Freshbooks_data['secretKey'];
			$accessToken   = $Freshbooks_data['accessToken'];
			$access_token_secret = $Freshbooks_data['oauth_token_secret'];

			define('OAUTH_CONSUMER_KEY', $subdomain);
			define('OAUTH_CONSUMER_SECRET', $key);
			define('OAUTH_CALLBACK', $URL1);

			if (!empty($invoices)) {
				foreach ($invoices as $key => $invoiceID) {

					$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);

					$in_data =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

					if (!empty($in_data)) {

						$Customer_ListID = $in_data['CustomerListID'];
						if ($cardID == 'new1') {
							$cardID_upd  = $cardID;
							$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
							$companyID = $c_data['companyID'];


							$this->load->library('encrypt');
							$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
							$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
							$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
							$cardType       = $this->general_model->getType($card_no);
							$friendlyname   =  $cardType.' - '.substr($card_no,-4);
							$merchantID = $merchID;


							$this->db1->where(array('customerListID' => $in_data['Customer_ListID'], 'merchantID' => $merchantID, 'customerCardfriendlyName' => $friendlyname));
							$this->db1->select('*')->from('customer_card_data');
							$qq =  $this->db1->get();

							if ($qq->num_rows() > 0) {

								$cardID = $qq->row_array()['CardID'];
								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '',
									'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
									'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
									'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
									'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
									'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
									'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
									'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
									'customerListID' => $in_data['Customer_ListID'],
									'companyID'     => $companyID,
									'merchantID'   => $merchantID,
									'customerCardfriendlyName' => $friendlyname,
									'updatedAt' 	=> date("Y-m-d H:i:s")
								);
								$this->db1->where(array('CardID' => $cardID));
								$this->db1->update('customer_card_data', $card_data);
								
							} else {
								$is_default = 0;
						     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
					        	if($checkCustomerCard == 0){
					        		$is_default = 1;
					        	}
								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '', 
									'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
									'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
									'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
									'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
									'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
									'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
									'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
									'customerListID' => $in_data['Customer_ListID'],
									'companyID'     => $companyID,
									'merchantID'   => $merchantID,
									'is_default'			   => $is_default,
									'customerCardfriendlyName' => $friendlyname,
									'createdAt' 	=> date("Y-m-d H:i:s")
								);



								$this->db1->insert('customer_card_data', $card_data);
								$cardID = $this->db1->insert_id();
							}
						}

						$card_data      =   $this->card_model->get_single_card_data($cardID);

						if (!empty($card_data)) {

							if ($in_data['BalanceRemaining'] > 0) {
								$cr_amount = 0;
								$amount  =	 $in_data['BalanceRemaining'];
								$amount  = $pay_amounts;

								$amount =	$pay_amounts;
								$amount           = $amount - $cr_amount;
								$transaction1 = new AuthorizeNetAIM($apiloginID, $transactionKey);
								$transaction1->setSandbox($this->config->item('auth_test_mode'));

								$card_no  = $card_data['CardNo'];
								$expmonth =  $card_data['cardMonth'];
								$exyear   = $card_data['cardYear'];
								$exyear   = substr($exyear, 2);
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
								}
								$expry    = $expmonth . $exyear;


								$result = $transaction1->authorizeAndCapture($amount, $card_no, $expry);


								if ($result->response_code == "1" && $result->transaction_id != 0 && $result->transaction_id != '') {



									if (isset($accessToken) && isset($access_token_secret)) {
										$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);

										$payment = '<?xml version="1.0" encoding="utf-8"?>  
						<request method="payment.create">  
						  <payment>         
						    <invoice_id>' . $invoiceID . '</invoice_id>               
						    <amount>' . $amount . '</amount>             
						    <currency_code>USD</currency_code> 
						    <type>Check</type>                   
						  </payment>  
						</request>';

										$invoices = $c->add_payment($payment);

										if ($invoices['status'] != 'ok') {
											$err = "The Status code is: " . $error->getHttpStatusCode() . "\n";
											
											$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error ' . $err . '</strong></div>');

											redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
										} else {
											
											$this->session->set_flashdata('success', 'Success');
							
										}
									}
								} else {

									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result->response_reason_text . '</strong>.</div>');
								}

								$transactiondata = array();
								if ($result->transaction_id != 0 && $result->transaction_id != '') 
								{
									$transactiondata['transactionID']       = $result->transaction_id;
									$transactiondata['transactionCode']     = $result->response_code;
								}else{
									$transactiondata['transactionID']       = 'TXNFAILED'.time();
									$transactiondata['transactionCode']     = 400;
								}
								$transactiondata['transactionStatus']   = $result->response_reason_text;
								$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
								$transactiondata['transactionCard']     = substr($result->account_number, 4);
								$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
								$transactiondata['transactionType']    = $result->transaction_type;
								$transactiondata['gatewayID']           = $gateway;
								$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
								$transactiondata['customerListID']      = $in_data['CustomerListID'];
								$transactiondata['transactionAmount']   = $result->amount;
								$transactiondata['merchantID']          = $merchID;
								$transactiondata['invoiceID']   = $invoiceID;
								$transactiondata['qbListTxnID'] =   $invoices['payment_id'];
								$transactiondata['resellerID']   = $resellerID;
								$transactiondata['gateway']   = "Auth";

								
								$CallCampaign = $this->general_model->triggerCampaign($merchID,$transactiondata['transactionCode']);

								if(!empty($this->transactionByUser)){
								    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
								    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
							} else {
								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>');
							}
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
						}
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
					}
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select invoices</strong>.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>');
		}

		if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

		redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
	}
}
