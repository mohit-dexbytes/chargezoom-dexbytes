<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
include APPPATH . 'libraries/Freshbooks_data.php';
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('general_model');
        $this->load->model('card_model');

        $this->load->model('Freshbook_models/fb_company_model');
        $this->load->model('Freshbook_models/fb_invoice_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {

        } else if ($this->session->userdata('user_logged_in') != "") {

        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        if (isset($user_id)) {
            $where = ['merchID' => $user_id, 'merchantPasswordNew' => null];

            $newPasswordNotFound = $this->general_model->get_row_data('tbl_merchant_data', $where);

            if ($newPasswordNotFound) {
                $dateDiff = time() - strtotime(getenv('PASSWORD_EXP_DATE'));

                $data['passwordExpDays'] = abs(round($dateDiff / (60 * 60 * 24)));
            }
        }

        $today      = date('Y-m-d');
        $condition2 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "inv.merchantID" => $user_id);
        $condition  = array("inv.merchantID" => $user_id, 'cust.merchantID' => $user_id, 'trans.merchantID' => $user_id);

        /* New updated dashboard date 27-10-2020 */
        $month = date("M-Y");
        $data['recent_pay']      = $this->fb_company_model->get_recent_volume_dashboard($user_id,$month);
        $data['card_payment'] = $this->fb_company_model->get_creditcard_payment($user_id,$month);
        $data['eCheck_payment'] = $this->fb_company_model->get_echeck_payment($user_id,$month);
       
        $data['outstanding_total'] = $this->fb_company_model->get_outstanding_payment($user_id);

        $data['recent_paid']    = $this->fb_company_model->get_paid_recent($user_id);


        $data['oldest_invs']   = $this->fb_company_model->get_oldest_due($user_id);
        $plantype              = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']      = $plantype;
        $data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
		$data['plantype_vs'] = $this->general_model->chk_merch_plantype_vt($user_id);
        $data['plantype_vt'] = 0;
        $merchData = $this->general_model->get_select_data('tbl_merchant_data',array('rhgraphOption','rhgraphFromDate','rhgraphToDate'), array('merchID' => $user_id));
        
        $data['rhgraphOption'] = isset($merchData['rhgraphOption'])?$merchData['rhgraphOption']:0;
        $data['rhgraphFromDate'] = isset($merchData['rhgraphFromDate'])?$merchData['rhgraphFromDate']:'';
        $data['rhgraphToDate'] = isset($merchData['rhgraphToDate'])?$merchData['rhgraphToDate']:'';
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/index_new', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function invoice_schedule()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $val = array(
            'merchantID' => $merchID,
        );
        $duedate     = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date')));
        $sch_gateway = $this->czsecurity->xssCleanPostInput('sch_method');

        $Inv_id     = $this->czsecurity->xssCleanPostInput('scheduleID');
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');
        $sch_card   = $this->czsecurity->xssCleanPostInput('schCardID');
        $sch_amount = $this->czsecurity->xssCleanPostInput('schAmount');
        $inv_data   = $this->general_model->get_row_data('Freshbooks_test_invoice', array('invoiceID' => $Inv_id, 'merchantID' => $merchID, 'isPaid' => '0'));

        $update_data = array('scheduleDate' => $duedate, 'invoiceID' => $Inv_id, 'scheduleAmount' => $sch_amount, 'cardID' => $sch_card, 'paymentMethod' => $sch_method, 'gatewayID' => $sch_gateway, 'updatedAt' => date('Y-m-d H:i:s'));
        if (!empty($inv_data)) {
            $sch_data = $this->general_model->get_row_data('tbl_scheduled_invoice_payment', array('invoiceID' => $Inv_id, 'merchantID' => $merchID));
            if (!empty($sch_data)) {
                $this->general_model->update_row_data('tbl_scheduled_invoice_payment', array('scheduleID' => $sch_data['scheduleID']), $update_data);
            } else {

                if (empty($sch_card)) {
                    $cr_q = $this->db1->query("Select CardID from customer_card_data where  customerListID='" . $inv_data['CustomerListID'] . "'  and merchantID= '" . $merchID . "' limit 1 ");

                    if ($cr_q->num_rows() > 0) {
                        $update_data['cardID'] = $cr_q->row_array()['CardID'];
                    } else {
                        $update_data['cardID'] = 0;
                    }
                }
                $update_data['merchantID'] = $merchID;
                $update_data['customerID'] = $inv_data['CustomerListID'];
                $update_data['updatedAt']  = date('Y-m-d H:i:s');
                $update_data['createdAt']  = date('Y-m-d H:i:s');
                $update_data['autoPay']    = 1;
                $update_data['autoPay']    = 1;
                $this->general_model->insert_row('tbl_scheduled_invoice_payment', $update_data);
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Scheduled Payment</strong></div>');

            array('status' => "success");
            echo json_encode(array('status' => "success"));
            die;
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

        }

        return false;
    }

    public function get_gateway_data()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('gatewayID');
        $condition = array('gatewayID' => $gatewayID);

        $res = $this->general_model->get_row_data('tbl_merchant_gateway', $condition);

        if (!empty($res)) {

            $res['status'] = 'true';
            echo json_encode($res);
        }

        die;

    }

    //------------- Merchant gateway START ------------//

    public function gateway()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $condition = array('merchantID' => $user_id);

        $data['all_gateway'] = $this->general_model->get_table_data('tbl_master_gateway', '');
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/page_merchant', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    
    public function create_gateway()
    {
        $signature  = '';
        $extra1 = '';
        $cr_status  = 1;
        $ach_status = 0;
        $isSurcharge = $surchargePercentage = 0;

        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {

                $user_id = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $user_id = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $condition = array('merchantID' => $user_id);
            $gatetype  = $this->czsecurity->xssCleanPostInput('gateway_opt');
            $gmID   = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
            if ($gatetype == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                if ($this->czsecurity->xssCleanPostInput('mni_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '2') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '3') {
				$this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');
                $signature  = PAYTRACE_INTEGRATOR_ID; 

                if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
            } else if ($gatetype == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');

            } else if ($gatetype == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');

            } else if ($gatetype == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');

            } else if ($gatetype == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey');

            } else if ($gatetype == '9') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '10') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                
                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
                    $isSurcharge = 1;
                }
                else{
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
			} else if ($gatetype  == '11') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == '13') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == '12') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword');
                $gmID     = $this->czsecurity->xssCleanPostInput('tsysMerchID');

                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } else if ($gatetype  == '15') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser');
				$nmipassword     = '';
				$cr_status       =  1;
				$ach_status       =  0;
			} else if ($gatetype  == '17') {

				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId');

				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
			}  else if ($gatetype == '16') {
                $nmiuser         = $this->input->post('EPXCustNBR');
                $nmipassword     = $this->input->post('EPXMerchNBR');
                $signature     = $this->input->post('EPXDBANBR');
                $extra1   = $this->input->post('EPXterminal');
                if ($this->input->post('EPX_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
            }

            $gnum = 0;
            $gnum = $this->general_model->get_num_rows('tbl_merchant_gateway', array('merchantID' => $user_id));

            if ($this->czsecurity->xssCleanPostInput('setdefaultgateway') || $gnum == 0) {
                $chk_gateway = 1;
            } else {
                $chk_gateway = 0;
            }

            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');

            $frname = $this->czsecurity->xssCleanPostInput('frname');
            

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewaySignature'                     => $signature,
                'extra_field_1'                        => $extra1,
                'gatewayType'                          => $gatetype,
                'merchantID'                           => $user_id,
                'gatewayFriendlyName'                  => $frname,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
                'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
            );

            if ($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {
                
                $val1 = array(
                    'merchantID' => $user_id,
                );
                
                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
                
                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Successfully Inserted</strong></div>');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

            }

            redirect(base_url('FreshBooks_controllers/home/gateway'));

        }

    }

    public function update_gateway()
    {
        if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {
            $ach_status = 0;
            $signature  = '';
            $extra1 = '';
            $cr_status  = 1;
            $isSurcharge = $surchargePercentage = 0;

            $id            = $this->czsecurity->xssCleanPostInput('gatewayEditID');
            $chk_condition = array('gatewayID' => $id);
            $gatetype      = $this->czsecurity->xssCleanPostInput('gateway');
            $gmID   = $this->czsecurity->xssCleanPostInput('mid');
            if ($gatetype == 'NMI') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword1');
                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if (strtolower($gatetype) == 'authorize.net') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey1');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Paytrace') {
				$this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword1');
                $signature  = PAYTRACE_INTEGRATOR_ID; 

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

            } else if ($gatetype == 'Paypal') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword1');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature1');

            } else if ($gatetype == 'Stripe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword1');

            } else if ($gatetype == 'USAePay') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin1');

            } else if ($gatetype == 'Heartland') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey1');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Cybersource') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey1');

            } else if ($gatetype == 'Chargezoom') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword1');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == iTransactGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY1');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                
                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box1')){
                    $isSurcharge = 1;
                }
                else{
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage1');
			} else if ($gatetype  == FluidGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == TSYSGatewayName) {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword1');
                $gmID                  = $this->czsecurity->xssCleanPostInput('tsysMerchID1');
                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } else if ($gatetype  == BASYSGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if($gatetype == PayArcGatewayName){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser1');
                $nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
            } else if ($gatetype  == MaverickGatewayName) {

				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId1');

				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
            } else if ($gatetype == EPXGatewayName) {
                $nmiuser         = $this->input->post('EPXCustNBR1');
                $nmipassword     = $this->input->post('EPXMerchNBR1');
                $signature     = $this->input->post('EPXDBANBR1');
                $extra1   = $this->input->post('EPXterminal1');
                if ($this->input->post('EPX_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
            } 

            $frname = $this->czsecurity->xssCleanPostInput('fname');
            

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewayFriendlyName'                  => $frname,
                'gatewaySignature'                     => $signature,
                'extra_field_1'                        => $extra1,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
                'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
            );

            if ($this->general_model->update_row_data('tbl_merchant_gateway', $chk_condition, $insert_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Successfully Updated</strong></div>');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }

            redirect(base_url('FreshBooks_controllers/home/gateway'));

        }

    }

    public function get_gatewayedit_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
        $val = array(
            'gatewayID' => $id,
        );

        $data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
		$data['gateway'] = getGatewayNames($data['gatewayType']);
        
        echo json_encode($data);die;
    }

    public function set_gateway_default()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {

            if ($this->session->userdata('logged_in')) {
                $da['login_info'] = $this->session->userdata('logged_in');

                $merchID = $da['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da['login_info'] = $this->session->userdata('user_logged_in');

                $merchID = $da['login_info']['merchantID'];
            }

            $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
            $val = array(
                'gatewayID' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );
            $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
            $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
        }
        redirect(base_url('FreshBooks_controllers/home/gateway'));
    }

    /**************Delete credit********************/

    public function delete_gateway()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
        $condition = array('gatewayID' => $gatewayID);

        if ($this->session->userdata('logged_in')) {
            $da['login_info'] = $this->session->userdata('logged_in');

            $merchID = $da['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $da['login_info'] = $this->session->userdata('user_logged_in');

            $merchID = $da['login_info']['merchantID'];
        }

        $num = $this->general_model->get_num_rows('tbl_chargezoom_subscriptions', array('paymentGateway' => $gatewayID, 'merchantDataID' => $merchID));
        if ($num == 0) {
            $del = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition);
            if ($del) {
                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Successfully Deleted</strong></div>');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Error: Gateway can not be deleted, Please change subscription gateway</strong></div>');

        }
        redirect(base_url('FreshBooks_controllers/home/gateway'));

    }

    public function add_note()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('customerID'))) {

            if ($this->session->userdata('logged_in')) {
                $da['login_info'] = $this->session->userdata('logged_in');

                $merchID = $da['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da['login_info'] = $this->session->userdata('user_logged_in');

                $merchID = $da['login_info']['merchantID'];
            }

            $cusID = $this->czsecurity->xssCleanPostInput('customerID');
            if ($this->czsecurity->xssCleanPostInput('private_note') != "") {

                $private_note = $this->czsecurity->xssCleanPostInput('private_note');
                $data_ar      = array('privateNote' => $private_note, 'privateNoteDate' => date('Y-m-d H:i:s'), 'customerID' => $cusID, 'merchantID' => $merchID);
                $id = $this->general_model->insert_row('tbl_private_note', $data_ar);
                if ($id > 0) {
                    array('status' => "success");
                    echo json_encode(array('status' => "success"));
                    die;

                } else {
                    array('status' => "error");
                    echo json_encode(array('status' => "success"));
                    die;
                }
            }

        }

    }
    public function delele_note()
    {

       
        if ($this->czsecurity->xssCleanPostInput('noteID') != "") {

            $noteID = $this->czsecurity->xssCleanPostInput('noteID');

            if ($this->db->query("Delete from tbl_private_note where noteID =  '" . $noteID . "' ")) {

                array('status' => "success");
                echo json_encode(array('status' => "success"));
                die;
            }
            return false;
        }

    }

    public function general_volume()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];

        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $get_result = $this->fb_company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
            
            $result_set    = array();
            $result_value1 = array();
            $result_value2 = array();
            $result_online_value = array();
            $result_online_month = array();
            
            $result_eCheck_value = array();
            $result_eCheck_month = array();
            foreach ($get_result['data'] as $count_merch) {
                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, $count_merch['revenu_volume']);
                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);
                
                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
            }
            $opt_array['totalRevenue'] =   $get_result['totalRevenue'];
            $opt_array['revenu_month'] =   $result_value1;
			$opt_array['revenu_volume'] =   $result_value2;
			$opt_array['online_month'] =   $result_online_month;
			$opt_array['online_volume'] =   $result_online_value;
		
            $opt_array['eCheck_month'] =   $result_eCheck_month;
            $opt_array['eCheck_volume'] =   $result_eCheck_value;

            echo json_encode($opt_array);

        }

    }

    public function get_invoice_due_company()
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $condition     = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id);
        $result_value1 = array();
        $result_value2 = array();
        $get_result    = array();
        $get_result1   = $this->fb_company_model->get_invoice_due_by_company($condition, 1);
        if ($get_result1) {

            foreach ($get_result1 as $k => $count_merch) {
                $res[$k][] = $count_merch['label'];
                $res[$k][] = (float) $count_merch['balance'];
               

            }
            $get_result = $res;

            echo json_encode($get_result);
            die;
        } else {

            echo json_encode($get_result);
            die;
        }

    }
    public function get_invoice_Past_due_company()
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $condition   = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id);
        $get_result  = array();
        $get_result1 = $this->fb_company_model->get_invoice_due_by_company($condition, 0);
        if ($get_result1) {

            foreach ($get_result1 as $k => $count_merch) {
                $res[$k][] = $count_merch['label'];
                $res[$k][] = (float) $count_merch['balance'];

            }
            $get_result = $res;

            echo json_encode($get_result);

        } else {
            echo json_encode($get_result);

        }
        die;

    }

    public function get_invoice_transaction_data()
    {

    }

    public function get_product_data()
    {
        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $data = $this->fb_customer_model->get_fb_item_data($merchantID);

        $data      = json_encode($data);
        echo $data = str_replace("'", "", $data);die;

    }
    public function get_subs_item_count_data()
    {
        $sbID           = $this->czsecurity->xssCleanPostInput('subID');
        $data1['items'] = $this->general_model->get_table_data('tbl_subscription_invoice_item_fb', array('subscriptionID' => $sbID));

        $data1['rows'] = $this->general_model->get_num_rows('tbl_subscription_invoice_item_fb', array('subscriptionID' => $sbID));
        echo json_encode($data1);
        die;
    }

    public function company()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
            $resellerID = $data['login_info']['resellerID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
            $rs_Data          = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $user_id));
            $resellerID = $rs_Data['resellerID'];
        }

        $condition         = array('merchantID' => $user_id);
        $data['companies'] = $this->general_model->get_table_data('tbl_freshbooks', $condition);

        $fbData = new Freshbooks_data($user_id, $resellerID);
        $data['isConnected'] = $fbData->isConnected();
        
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/page_company', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_company_details()
    {

        $CompanyID = $this->czsecurity->xssCleanPostInput('id');
        
        $condition   = array('id' => $CompanyID);
        $companydata = $this->general_model->get_row_data('tbl_company', $condition);

        if (!empty($companydata)) {

            ?>

		 <table class="table table-bordered table-striped table-vcenter"   >
            <thead>
                <tr>
                    <th class="text-left">Attribute</th>
                    <th class="visible-lg text-left">Details</th>
                </tr>
            </thead>
            <tbody>


				<tr>
					<th class="text-left"><strong>App Name</strong></th>
					<td class="text-left visible-lg"><?php echo $companydata['companyName']; ?></td>
			  </tr>
			<tr>
					<th class="text-left"><strong>QBWC Username</strong></th>
					<td class="text-left visible-lg"><?php echo $companydata['qbwc_username']; ?></td>
			</tr>
			<tr>
					<th class="text-left"><strong>QBWC Password</strong></th>
					<td class="text-left visible-lg" > <?php echo $companydata['qbwc_password']; ?> </td>

			</tr>
			<tr>
					<th class="text-left"><strong>App Tag Line</strong></th>
					<td class="text-left visible-lg"><?php echo ($companydata['companyTagline']) ? $companydata['companyTagline'] : '----'; ?></td>
			</tr>
            <tr>
					<th class="text-left"><strong>Email</strong></th>
					<td class="text-left visible-lg"><?php echo $companydata['companyEmail']; ?></td>
			</tr>
			<tr>
					<th class="text-left"><strong>Contact</strong></th>
					<td class="text-left visible-lg"><?php echo $companydata['companyContact']; ?></td>
			</tr>
             <tr>
					<th class="text-left"><strong>Address</strong></th>
					<td class="text-left visible-lg"><?php echo $companydata['companyAddress1']; ?></td>
			</tr>



			</tbody>
        </table>

	<?php }

        die;

    }

    public function get_invoice_item_count_data()
    {

        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $sbID           = $this->czsecurity->xssCleanPostInput('invID');
        $con            = array('invoiceID' => $sbID);
        $data1['items'] = $this->general_model->get_table_data('tbl_freshbook_invoice_item', $con);
        $data1['rows']  = $this->general_model->get_num_rows('tbl_freshbook_invoice_item', $con);
        echo json_encode($data1);die;

    }
    public function transation_receipt($txt_id = '', $invoiceId = '', $trans_id = '')
	{

		$page_data = $this->session->userdata('receipt_data');
        $page_data['transaction_id'] = $trans_id;
        $this->session->set_userdata("receipt_data", $page_data);
        foreach($page_data as $key => $rcData){
			$page_data[$key] = strip_tags($rcData);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_data'] = $page_data;
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
            $data['transaction_id'] = $trans_id;
            $transaction_id = $trans_id;
			$con = array('transactionID' => $transaction_id);
            $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);
            $data['transactionDetail'] = $pay_amount;
            $data['transactionAmount'] = ($pay_amount) ? $pay_amount['transactionAmount'] : '0.00';

            $surCharge = 0;
            $totalAmount = 0;
            $isSurcharge = 0;
            
            $transactionType = $pay_amount['transactionGateway'];
            if(isset($pay_amount['transactionGateway']) && $pay_amount['transactionID'] != '' ){
                if($pay_amount['transactionGateway'] == 10){

                    $resultAmount = getiTransactTransactionDetails($user_id,$transaction_id);
                    $totalAmount = $resultAmount['totalAmount'];
                    $surCharge = $resultAmount['surCharge'];
                    $isSurcharge = $resultAmount['isSurcharge'];
                    if($resultAmount['payAmount'] != 0){
                        $data['transactionAmount'] = $resultAmount['payAmount'];
                    }   
                }
            }
            $data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;
            $data['transactionCode'] = ($pay_amount) ? $pay_amount['transactionCode'] : '0';
			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
			$in_data  =  $this->session->userdata('in_data');
			$data['invoice'] = $invoiceId;
			$data['invoice_number'] = $in_data['refNumber'];
			$condition2 			= array('invoiceID' => $invoiceId, 'merchantID' => $user_id);
            $invoice_data = [];
            $invoice_data[]          = $this->general_model->get_row_data('freshbooks_test_invoice', $condition2);
            $condition3 			= array('Customer_ListID' => $in_data['Customer_ListID'], 'merchantID' => $user_id);
			$customer_data			= $this->general_model->get_row_data('Freshbooks_custom_customer', $condition3);
            $customer_data = convertCustomerDataFieldName($customer_data,3);
			$data['customer_data'] = $customer_data;
			$data['invoice_data'] = $invoice_data;
		


			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
            $this->load->view('comman-pages/transaction_proccess_receipt', $data);
			
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}
	public function transation_credit_receipt($invoiceId = "null", $customer_id = "null", $trans_id = "null")
	{

		$page_data = $this->session->userdata('receipt_data');
        $page_data['transaction_id'] = $trans_id;
        $this->session->set_userdata("receipt_data", $page_data);

        foreach($page_data as $key => $rcData){
			$page_data[$key] = strip_tags($rcData);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_data'] = $page_data;
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
			$data['transaction_id'] = $trans_id;
			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
			$data['invoice'] = '';
			$data['invoice_number'] = '';
			
			$condition3 			= array('Customer_ListID' => $customer_id);
			$customer_data			= $this->general_model->get_row_data('Freshbooks_custom_customer', $condition3);
            $customer_data = convertCustomerDataFieldName($customer_data,3);
			$data['customer_data'] = $customer_data;
			$data['invoice_data'] = $customer_data;
            if(isset($invoiceId) && !empty($invoiceId) && $invoiceId != 'transaction'){
                $in_data   =    $this->fb_company_model->get_invoice_data_pay($invoiceId, $user_id);
                $data['invoice'] = $invoiceId;
                $data['invoice_number'] = $in_data['refNumber'];
            }

		    $surCharge = 0;
            $isSurcharge = 0;
            $totalAmount = 0;
            $transactionType = 0;

            $invoice_IDs = [];
            $invoice_data = [];
            if($trans_id != null){
                $condition4             = array('transactionID' => $trans_id, 'merchantID' => $user_id,'customerListID' => $customer_id);
                $transactionData          = $this->general_model->get_row_data('customer_transaction', $condition4);
                $data['transactionAmount'] = $transactionData['transactionAmount'];
                $data['transactionCode'] = $transactionData['transactionCode'];
                $transactionType = $transactionData['transactionGateway'];
                $data['transactionDetail'] = $transactionData;
                /*Invoice Set*/
                $invoiceArray = json_decode($transactionData['custom_data_fields']);
                $invoiceStr = '';
                
                if(!empty($invoiceArray) && isset($invoiceArray->invoice_number)){

                    $invoiceStr = $invoiceArray->invoice_number;
                    $invoice_IDs = explode(',', $invoiceStr);
                }
                
                
                if (!empty($invoice_IDs)) {

                    foreach ($invoice_IDs as $inID) {
                        $condition2 = array('invoiceID' => $inID);
                        $invoice_data[]  = $this->general_model->get_row_data('Freshbooks_test_invoice', $condition2);
                    }
                }
                $data['invoice_IDs'] = $invoice_IDs;
                
                $data['invoice_data'] = $invoice_data;
            }else{
                $data['transactionAmount'] = 0;
                $data['transactionCode'] = 0;
                $data['invoice_IDs'] = $invoice_IDs;
                $data['invoice_data'] = $invoice_data;
            }

            if(isset($transactionType)){
                if($transactionType == 10){

                    $resultAmount = getiTransactTransactionDetails($user_id,$trans_id);
                    $totalAmount = $resultAmount['totalAmount'];
                    $surCharge = $resultAmount['surCharge'];
                    $isSurcharge = $resultAmount['isSurcharge'];
                    if($resultAmount['payAmount'] != 0){
                        $data['transactionAmount'] = $resultAmount['payAmount'];
                    }   
                }
            }
            $data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;
            
			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
            $this->load->view('comman-pages/transaction_proccess_receipt', $data);
		
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}
	
	public function transation_sale_receipt()
	{

		$invoice_IDs = $this->session->userdata('invoice_IDs');
		$receipt_data = $this->session->userdata('receipt_data');
        if(!empty($receipt_data)){
            foreach($receipt_data as $key => $rcData){
                $receipt_data[$key] = strip_tags($rcData);
            }
        }

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$transactionCode = 0;
        $surCharge = 0;
        $isSurcharge = 0;
        $totalAmount = 0;
        $transactionType = 0;
        $pay_amount = [];
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
            $data['transactionAmount'] = '';
            $data['transactionCode'] = 0;
			if(isset($receipt_data['transaction_id']) && !empty($receipt_data['transaction_id'])){
                $transaction_id = $receipt_data['transaction_id'];
                $transactionRowID = isset($receipt_data['transactionRowID'])?$receipt_data['transactionRowID']:'';
                $con = array('transactionID' => $transaction_id);
                $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount');
                $this->db->from('customer_transaction tr');
                $this->db->where($con);
                if(isset($transactionRowID) && !empty($transactionRowID)){
                    $this->db->where("tr.id",$transactionRowID);
                }
                $this->db->group_by("tr.transactionID");
                $pay_amount = $this->db->get()->row_array();

                $payAmountSet = isset($pay_amount['transactionAmount'])?$pay_amount['transactionAmount']:0;
                
                $transactionType = $pay_amount['transactionGateway'];

                if($pay_amount['transactionGateway'] == 10){

                    $resultAmount = getiTransactTransactionDetails($user_id,$transaction_id);
                    $totalAmount = $resultAmount['totalAmount'];
                    $surCharge = $resultAmount['surCharge'];
                    $isSurcharge = $resultAmount['isSurcharge'];
                    if($resultAmount['payAmount'] != 0){
                        $payAmountSet = $resultAmount['payAmount'];
                    }   
                }

                
                $data['transactionCode'] = $pay_amount['transactionCode'];
                $data['transactionAmount'] = $payAmountSet;
                $data['transactionDetail'] = $pay_amount;
            }

            $data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;

            if(isset($receipt_data['refundAmount']) && !empty($receipt_data['refundAmount'])){
                $data['transactionAmount'] = $receipt_data['refundAmount'];
            }
		
			$data['customer_data'] = $receipt_data;
			$invoice_data = [];
			if (!empty($invoice_IDs)) {

				foreach ($invoice_IDs as $inID) {
					$condition2 = array('invoiceID' => $inID);
					$invoiceData  = $this->general_model->get_row_data('Freshbooks_test_invoice', $condition2);
                    if(!empty($invoiceData)){
                        $invoice_data[]  = $invoiceData;
                    }
				}
			}
			$data['invoice_IDs'] = $invoice_IDs;
			 $data['invoice_data'] = $invoice_data;
            $data['transactionDetail'] = $pay_amount; 
			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
            $this->load->view('comman-pages/transaction_receipt', $data);
			
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}

    public function my_account()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');

            $user_id                = $data['login_info']['merchID'];
            $data['loginType'] = 1;
        }else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];

            $data['loginType'] = 2;
        }   

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();
        
        $condition  = array('merchantID' => $user_id);
        $data['invoices'] = $this->general_model->get_table_data('tbl_merchant_billing_invoice', $condition);

        $plandata = $this->general_model->chk_merch_plantype_data($user_id);
        $resellerID = $data['login_info']['resellerID'];
        $planID = $plandata->plan_id;
        $planname = $this->general_model->chk_merch_planFriendlyName($resellerID,$planID);

        if(isset($planname) && !empty($planname)){
            $data['planname'] = $planname;
        }else{
            $data['planname'] = $plandata->plan_name;
        }


        if($plandata->cardID > 0 && $plandata->payOption > 0){
            $carddata = $this->card_model->get_merch_card_data($plandata->cardID);
        }else{
            $carddata = [];
        }
        
        $data['plan'] = $plandata;

        $data['carddata'] = $carddata;

        $conditionMerch = array('merchID'=>$user_id);
        $data['merchantData'] = $this->general_model->get_row_data('tbl_merchant_data', $conditionMerch);

        $data['interface'] = 3;
        $data['merchantID'] = $user_id;
        
        $data['carddata'] = $carddata;
        
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_my_account', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }


    public function update_card_data()
    {
        
        $success_msg = null;
            if ($this->session->userdata('logged_in')) {
                $data['login_info']     = $this->session->userdata('logged_in');

                $user_id                = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info']     = $this->session->userdata('user_logged_in');

                $user_id                = $data['login_info']['merchantID'];
            }   
            
            
            $resellerID  = $this->czsecurity->xssCleanPostInput('resellerID');
                
            $merchantcardID = $this->czsecurity->xssCleanPostInput('cardID'); 

            $merchantID = $user_id;

            $condition = array('merchantListID'=>$merchantID);

            $conditionMerch = array('merchID'=>$merchantID);
            
            $billing_first_name = ($this->czsecurity->xssCleanPostInput('billing_first_name') != null)?$this->czsecurity->xssCleanPostInput('billing_first_name'):null;

            $billing_last_name = ($this->czsecurity->xssCleanPostInput('billing_last_name')!= null)?$this->czsecurity->xssCleanPostInput('billing_last_name'):null;

            $billing_phone_number = ($this->czsecurity->xssCleanPostInput('billing_phone_number')!= null)?$this->czsecurity->xssCleanPostInput('billing_phone_number'):null;

            $billing_email = ($this->czsecurity->xssCleanPostInput('billing_email')!= null)?$this->czsecurity->xssCleanPostInput('billing_email'):null;

            $billing_address = ($this->czsecurity->xssCleanPostInput('billing_address')!= null)?$this->czsecurity->xssCleanPostInput('billing_address'):null;

            $billing_state = ($this->czsecurity->xssCleanPostInput('billing_state')!= null)?$this->czsecurity->xssCleanPostInput('billing_state'):null;

            $billing_city = ($this->czsecurity->xssCleanPostInput('billing_city')!= null)?$this->czsecurity->xssCleanPostInput('billing_city'):null;

            $billing_zipcode = ($this->czsecurity->xssCleanPostInput('billing_zipcode')!= null)?$this->czsecurity->xssCleanPostInput('billing_zipcode'):null;
            $statusInsert = 0;
            /* check is_address_update condition 1 than only address update and 2 for all */
            if($this->czsecurity->xssCleanPostInput('is_address_update') == 1){
                $insert_array =  array( 
                                    "billing_first_name" => $billing_first_name,
                                    "billing_last_name" => $billing_last_name,
                                    "billing_phone_number" => $billing_phone_number,
                                    "billing_email" => $billing_email,
                                    "Billing_Addr1" => $billing_address,
                                    "Billing_Country" =>null,
                                    "Billing_State" => $billing_state,
                                    "Billing_City" =>$billing_city,
                                    "Billing_Zipcode" => $billing_zipcode
                                             );
                if($merchantcardID!="")
                {
                    
                    $id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
                    
                    
                    $success_msg = 'Address Updated Successfully';  

                }
            }else{
                /* Save credit card data */
                if($this->czsecurity->xssCleanPostInput('payOption') == 1){

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
                    $card_type = $this->general_model->getcardType($card_no);
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                    $type = 'Credit';
                    $friendlyname = $card_type . ' - ' . substr($card_no, -4);
                    $insert_array =  array( 'CardMonth'  =>$expmonth,
                                    'CardYear'   =>$exyear, 
                                    'resellerID'  =>$resellerID,
                                    'merchantListID'=>$merchantID,
                                    'accountNumber'   => null,
                                    'routeNumber'     => null,
                                    'accountName'   => null,
                                    'accountType'   => null,
                                    'accountHolderType'   => null,
                                    'secCodeEntryMethod'   => null,
                                    'merchantFriendlyName' => $friendlyname,
                                    "billing_first_name" => $billing_first_name,
                                    "billing_last_name" => $billing_last_name,
                                    "billing_phone_number" => $billing_phone_number,
                                    "billing_email" => $billing_email,
                                    "Billing_Addr1" => $billing_address,
                                    "Billing_Country" =>null,
                                    "Billing_State" => $billing_state,
                                    "Billing_City" =>$billing_city,
                                    "Billing_Zipcode" => $billing_zipcode
                                             );
                    if($merchantcardID!="")
                    {
                        
                           
                        $insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
                        $insert_array['CardCVV']        = ''; 
                        $insert_array['CardType']        = $card_type;
                        $insert_array['createdAt']    = date('Y-m-d H:i:s');
                        $id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
                        
                        
                        $success_msg = 'Credit Card Updated Successfully';  

                    }else{
                        
                        $insert_array['CardType']    = $card_type;
                        $insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
                        $insert_array['CardCVV']        = '';
                        $insert_array['createdAt']    = date('Y-m-d H:i:s');
                              
                            
                        $id = $this->card_model->insert_merchant_card_data($insert_array);
                        $statusInsert = 1;
                        $merchantcardID = $id;
                        $success_msg = 'Credit Card Inserted Successfully';  
                        
                    }

                }else if($this->czsecurity->xssCleanPostInput('payOption') == 2){
                    /* Save checking card data */
                    $acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
                    $route_number = $this->czsecurity->xssCleanPostInput('route_number');
                    $acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCode      = $this->czsecurity->xssCleanPostInput('secCode');
                    $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
                    $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                    $card_type = 'Checking';
                    $type = 'Checking';
                    $friendlyname = $type . ' - ' . substr($acc_number, -4);
                    $card_data = array(
                        'CardType'     => $card_type,
                        'CardMonth'  => null,
                        'CardYear'   => null, 
                        'MerchantCard' => null, 
                        'CardCVV' => null,
                        'accountNumber'   => $acc_number,
                        'routeNumber'     => $route_number,
                        'accountName'   => $acc_name,
                        'accountType'   => $acct_type,
                        'accountHolderType'   => $acct_holder_type,
                        'secCodeEntryMethod'   => $secCode,
                        'merchantListID'=>$merchantID,
                        'resellerID'  =>$resellerID,
                        'merchantFriendlyName' => $friendlyname,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        "billing_first_name" => $billing_first_name,
                        "billing_last_name" => $billing_last_name,
                        "billing_phone_number" => $billing_phone_number,
                        "billing_email" => $billing_email,
                        "Billing_Addr1" => $billing_address,
                        "Billing_Country" =>null,
                        "Billing_State" => $billing_state,
                        "Billing_City" =>$billing_city,
                        "Billing_Zipcode" => $billing_zipcode
                                 
                    );
                    if($merchantcardID!="")
                    {
                        
                        $id = $this->card_model->update_merchant_card_data($condition, $card_data);  
                        
                        

                        $success_msg = 'Checking Card Updated Successfully';  
                    }else{
                        
                        $id = $this->card_model->insert_merchant_card_data($card_data);
                        $merchantcardID = $id;
                        $statusInsert = 1;
                        $success_msg = 'Checking Card Inserted Successfully';  
                    }


                }else{
                    /* do nothing*/
                    $id = false;
                }
            }
            
            if($statusInsert == 1){
                $merchant_condition = ['merchID' => $merchantID];
                $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$merchant_condition);
                if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                {
                    /* Start campaign in hatchbuck CRM*/  
                    $this->load->library('hatchBuckAPI');
                    
                    $merchantData['merchant_type'] = 'Freshbooks';        
                    
                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                    if($status['statusCode'] == 400){
                        $resource = $this->hatchbuckapi->createContact($merchantData);
                        if($resource['contactID'] != '0'){
                            $contact_id = $resource['contactID'];
                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                        }
                    }
                    /* End campaign in hatchbuck CRM*/ 
                }
                
            }   

            if( $id ){
                /* Update Pay option type in merchant table */
                $update_array =  array( 'payOption'  => $this->czsecurity->xssCleanPostInput('payOption'), 'cardID' => $merchantcardID );

                $update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array); 

                $this->session->set_flashdata('success', $success_msg);
            }else{
             
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
            }

            redirect('FreshBooks_controllers/home/my_account/');
    }
    public function marchant_invoice()
    {


        if ($this->uri->segment(4) != "") {
            $data['primary_nav']    = primary_nav();
            $data['template']       = template_variable();
            if ($this->session->userdata('logged_in')) {
                $data['login_info']     = $this->session->userdata('logged_in');

                $user_id                = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info']     = $this->session->userdata('user_logged_in');

                $user_id                = $data['login_info']['merchantID'];
            }
            $invoiceID             =  $this->uri->segment(4);
            
            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);
            $this->load->view('FreshBooks_views/marchant_invoice', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);
        } else {
            redirect(base_url('FreshBooks_controllers/home/invoices'));
        }
    }

    public function level_three()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }

        $data['level_three_master_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master']);
        $data['level_three_visa_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa']);
        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/level_three', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    // add or update visa details
    public function level_three_visa()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');
            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['customer_tax_id'] = $this->czsecurity->xssCleanPostInput('customer_tax_id');
            $insert_data['commodity_code'] = $this->czsecurity->xssCleanPostInput('commodity_code');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['addtnl_tax_freight'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_freight');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['line_item_commodity_code'] = $this->czsecurity->xssCleanPostInput('line_item_commodity_code');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['card_type'] = 'visa';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'visa']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa'], $insert_data);
                
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('FreshBooks_controllers/home/level_three');
    }

    // add or update master details
    public function level_three_master_card()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');

            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['addtnl_tax_indicator'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_indicator');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['debit_credit_indicator'] = $this->czsecurity->xssCleanPostInput('debit_credit_indicator');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['addtnl_tax_type'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_type');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['net_gross_indicator'] = $this->czsecurity->xssCleanPostInput('net_gross_indicator');
            
            $insert_data['card_type'] = 'master';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'master']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master'], $insert_data);
                
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('FreshBooks_controllers/home/level_three');
    }
    public function dashboardReport()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];

        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        
        $filterType  = $this->czsecurity->xssCleanPostInput('revenue_filter');
        /*Update filter*/
        $endDate  = $startDate = date('Y-m-d');
        if($this->czsecurity->xssCleanPostInput('endDate',true) != null){
            $endDate  = $this->czsecurity->xssCleanPostInput('endDate',true);
        }
        if($this->czsecurity->xssCleanPostInput('startDate',true) != null){
            $startDate  = $this->czsecurity->xssCleanPostInput('startDate',true);
        }
        $update_array =  array( 'rhgraphOption'  => $filterType, 'rhgraphToDate' => $endDate, 'rhgraphFromDate' => $startDate );
        $conditionMerch = array('merchID'=>$user_id);

        $update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array);

        if($filterType == 0){
            $opt_array = $this->getAnnualRevenue($user_id);
            echo json_encode($opt_array);
        }else if($filterType == 1){
            $startDate = date('Y-m-d', strtotime('today - 30 days'));
            $endDate = date('Y-m-d');
            $opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,1);
            echo json_encode($opt_array);
        }else if($filterType == 2){
            $startDate = date('Y-m-01');
            $endDate = date('Y-m-d');
            $opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,2);
            echo json_encode($opt_array);
        }else if($filterType == 3){
            $opt_array = $this->general_model->getHourlyRevenue($user_id);
            echo json_encode($opt_array);
        }else if($filterType == 4){
            $opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,2);
            echo json_encode($opt_array);
        }
    }
    public function batchReciept()
    {
        

        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');
            //print_r($data['login_info']); die;
            $user_id                = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];
        }

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();

        $page_data = $this->session->userdata('batch_process_receipt_data'); 
        
        $invoices = $page_data['data'];
        $invoiceObj = [];
        if(count($invoices) > 0){
            foreach ($invoices as $value) {
                
                $invoiceObj[] = $this->general_model->getInvoiceBatchTransactionList($value['invoice_id'],$value['customerID'],$value['transactionRowID'],3);

                # code...
            }
        }
        
        $data['allTransaction'] = $invoiceObj;
        $data['statusCode'] = array('200','100','111','1','102','120');
        $data['integrationType'] = 3;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/batch_invoice_reciept', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function getAnnualRevenue($user_id){
        $get_result = $this->fb_company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
            $result_set = array();
            $result_value1 = array();
            $result_value2 = array();
            $result_online_value = array();
            $result_online_month = array();
            $result_eCheck_value = array();
            $result_eCheck_month = array();
            foreach ($get_result['data'] as $count_merch) {

                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, (float) $count_merch['revenu_volume']);
                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);
                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
            }
            $ob[] = [];
            $obRevenu = [];
            $obOnline = [];
            $obeCheck = [];
            $in = 0;

            foreach ($result_value1 as $value) {
                $custmonths = date("M", strtotime($value));
                
                $ob1 = [];
                $obR = [];
                $obON = [];
                $obEC = [];
                $inc = strtotime($value);
                $ob1[0] = $inc;
                $obR[] = $inc;
                $obR[] = $custmonths;
                $ob1[1] = $result_value2[$in];
                $ob[] = $ob1;
                $obON[0] = $inc;
                $obON[1] = $result_online_value[$in];
                $obOnline[] = $obON;
                $obEC[0] = $inc;
                $obEC[1] = $result_eCheck_value[$in];
                $obeCheck[] = $obEC;
                $obRevenu[] = $obR; 
                $in++;
            }
            $opt_array['revenu_month'] =   $obRevenu;
            $opt_array['revenu_volume'] =   $ob;
            $opt_array['online_month'] =   $result_online_month;
            $opt_array['online_volume'] =   $obOnline; 
            $opt_array['eCheck_month'] =   $result_eCheck_month;
            $opt_array['eCheck_volume'] =   $obeCheck;
            $opt_array['totalRevenue'] =   $get_result['totalRevenue'];
            $opt_array['totalCCA'] =   $get_result['totalCCA'];
            $opt_array['totalECLA'] =   $get_result['totalECLA'];
            return ($opt_array);
        }
        return [];
    }
}