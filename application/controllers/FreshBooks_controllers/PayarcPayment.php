<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */

include APPPATH . 'third_party/Fluidpay.class.php';

class PayarcPayment extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';

        $this->load->config('fluidpay');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->library('form_validation');
        $this->load->model('Freshbook_models/freshbooks_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $merchID;
        } else {
            redirect('login', 'refresh');
        }

        $val = array(
            'merchantID' => $this->merchantID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
        if (empty($Freshbooks_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - FreshBooks Account Integration Error</strong></div>');
            redirect('FreshBoolks_controllers/home/index', 'refresh');
        }

        $this->subdomain           = $Freshbooks_data['sub_domain'];
        $key                       = $Freshbooks_data['secretKey'];
        $this->accessToken         = $Freshbooks_data['accessToken'];
        $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
        $this->fb_account          = $Freshbooks_data['accountType'];
        $condition1                = array('resellerID' => $this->resellerID);
        $sub1                      = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1                   = $sub1['merchantPortalURL'];
        $URL1                      = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        define('OAUTH_CONSUMER_KEY', $this->subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        redirect('FreshBooks_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");

        $cusproID = array();
        $error    = array();
        $cusproID = $this->czsecurity->xssCleanPostInput('customerProcessID');

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $this->form_validation->run() == true) {

            $merchID    = $this->merchantID;
            $user_id    = $merchID;
            $resellerID = $this->resellerID;
            $invoiceID  = $this->czsecurity->xssCleanPostInput('invoiceProcessID');

            $cardID = $this->czsecurity->xssCleanPostInput('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
            }

            $gateway    = $gatlistval;
            $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

            $amount   = $this->czsecurity->xssCleanPostInput('inv_amount');
            $chh_mail = 0;
            $in_data  = $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

            if (!empty($in_data)) {

                $customerID = $in_data['Customer_ListID'];

                $companyID = $in_data['companyID'];
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey      = $gt_result['gatewayPassword'];

                if (!empty($cardID)) {
                    if ($in_data['BalanceRemaining'] > 0) {

                        $cusproID = 1;
                        $pinv_id  = '';

                        $name = $in_data['Customer_FullName'];

                        $isCheck = true;
                        if ($sch_method == "1") {
                            $isCheck = false;

                            if ($cardID != "new1") {

                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];

                            } else {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                    $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                                }

                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('phone');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            }

                            $exyear1  = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear1;

                            $transaction = array(
                                "type"                => "sale",
                                "amount"              => $amount * 100,
                                "currency"            => "USD",
                                "ip_address"          => getClientIpAddr(),
                                "payment_method"      => array(
                                    "card" => array(
                                        "entry_type"      => "keyed",
                                        "number"          => $card_no,
                                        "expiration_date" => $expry,
                                        "cvc"             => $cvv,
                                    ),
                                ),
                                "billing_address"     => array(
                                    "first_name"     => $in_data['firstName'],
                                    "last_name"      => $in_data['lastName'],
                                    "company"        => $in_data['companyName'],
                                    "address_line_1" => $address1,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "country"        => $country,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                    "email"          => $in_data['phoneNumber'],
                                ),
                                "shipping_address"    => array(
                                    "first_name"     => $in_data['firstName'],
                                    "last_name"      => $in_data['lastName'],
                                    "company"        => $in_data['companyName'],
                                    "address_line_1" => $address1,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "country"        => $country,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                    "email"          => $in_data['phoneNumber'],
                                ),
                            );

                            
                        } else if ($sch_method == "2") {
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                    'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                    'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                    'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                    'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                    'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'merchantID'         => $user_id,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }

                            $transaction = array(
                                "type"                => "sale",
                                "amount"              => $amount * 100,
                                "currency"            => "USD",
                                "ip_address"          => getClientIpAddr(),
                                "payment_method"      => array(
                                    "ach" => array(
                                        "routing_number" => $accountDetails['routeNumber'],
                                        "account_number" => $accountDetails['accountNumber'],
                                        "sec_code"       => $accountDetails['secCodeEntryMethod'],
                                        "account_type"   => $accountDetails['accountType'],
                                    ),
                                ),
                                "billing_address"     => array(
                                    "first_name"     => $in_data['firstName'],
                                    "last_name"      => $in_data['lastName'],
                                    "company"        => $in_data['companyName'],
                                    "address_line_1" => $accountDetails['Billing_Addr1'],
                                    "address_line_2" => $accountDetails['Billing_Addr2'],
                                    "city"           => $accountDetails['Billing_City'],
                                    "state"          => $accountDetails['Billing_State'],
                                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                                    "phone"          => $accountDetails['Billing_Contact'],
                                    "fax"            => $accountDetails['Billing_Contact'],
                                    "email"          => $in_data['phoneNumber'],
                                ),
                                "shipping_address"    => array(
                                    "first_name"     => $in_data['firstName'],
                                    "last_name"      => $in_data['lastName'],
                                    "company"        => $in_data['companyName'],
                                    "address_line_1" => $accountDetails['Billing_Addr1'],
                                    "address_line_2" => $accountDetails['Billing_Addr2'],
                                    "city"           => $accountDetails['Billing_City'],
                                    "state"          => $accountDetails['Billing_State'],
                                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                                    "phone"          => $accountDetails['Billing_Contact'],
                                    "fax"            => $accountDetails['Billing_Contact'],
                                    "email"          => $in_data['phoneNumber'],
                                ),
                            );
                        }

                        $responseId = '';

                        $gatewayTransaction              = new Fluidpay();
                        $gatewayTransaction->environment = $this->gatewayEnvironment;
                        $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];
                        $result = $gatewayTransaction->processTransaction($transaction);

                        if ($result['status'] == 'success') {
                            $responseId = $result['data']['id'];

                            $val = array(
                                'merchantID' => $merchID,
                            );

                            $ispaid  = 1;
                            $st      = 'paid';
                            $bamount = $in_data['BalanceRemaining'] - $amount;
                            if ($bamount > 0) {
                                $ispaid = '0';
                                $st     = 'unpaid';
                            }
                            $app_amount = $in_data['AppliedAmount'] + $amount;
                            $up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
                            $condition  = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);

                            $this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);

                            if (isset($this->accessToken) && isset($this->access_token_secret)) {
                                if ($this->fb_account == 1) {
                                    $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                                    $payment  = array('invoiceid' => $invoiceID, 'amount' => array('amount' => $amount), 'type' => 'Check', 'date' => date('Y-m-d'));
                                    $invoices = $c->add_payment($payment);
                                } else {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                                <request method="payment.create">
                                                <payment>
                                                    <invoice_id>' . $invoiceID . '</invoice_id>
                                                    <amount>' . $amount . '</amount>
                                                    <currency_code>USD</currency_code>
                                                    <type>Check</type>
                                                </payment>
                                                </request>';
                                    $invoices = $c->add_payment($payment);

                                }

                                if ($invoices['status'] != 'ok') {

                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed </strong></div>');


                                } else {
                                    $pinv_id = $invoices['payment_id'];

                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                }
                            }
                            $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                            $ref_number     = $in_data['refNumber'];
                            $tr_date        = date('Y-m-d H:i:s');
                            $toEmail        = $in_data['userEmail'];
                            $company        = $in_data['companyName'];
                            $customer       = $in_data['fullName'];
                            if ($chh_mail == '1') {

                                $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                            }
                            

                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }

                        } else {
                            $err_msg      = $result['msg'];
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                        }

                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $pinv_id, $this->resellerID, $in_data['invoiceID'], $isCheck, $this->transactionByUser);

                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Customer has no card</strong>.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Not valid</strong>.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Validation Error</strong>.</div>');
        }

        if ($cusproID != "") {
            $trans_id     = ($sch_method == "2") ? $result['check_transaction_id'] : $result['id'];
            $invoice_IDs  = array();
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
                'proccess_btn_text' => 'Process New Invoice',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            $this->session->set_userdata("in_data", $in_data);
            if ($cusproID == "1") {
                redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');

            }

            redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $cusproID, 'refresh');
        } else {
            if(!$checkPlan){
                $responseId  = '';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
            }
            redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
        }

    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $user_id = $this->merchantID;
        $checkPlan = check_free_plan_transactions();
        if ($checkPlan && !empty($this->input->post(null, true))) {
            $this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
            $this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
            if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {
                $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
            }

            if ($this->form_validation->run() == true) {
                $custom_data_fields = [];
                // get custom field data
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }

                $inputData = $this->input->post(null, true);
                $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                $invoiceIDs = array();
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                }

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
                $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
                $companyID = $comp_data['companyID'];
                $cardID    = $this->czsecurity->xssCleanPostInput('card_list');

                $cvv = '';
                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                        $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                    }

                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    if ($card_data['CardCVV']) {
                        $cvv = $card_data['CardCVV'];
                    }

                    $cardType = $card_data['CardType'];
                    $address1 = $card_data['Billing_Addr1'];
                    $city     = $card_data['Billing_City'];
                    $zipcode  = $card_data['Billing_Zipcode'];
                    $state    = $card_data['Billing_State'];
                    $country  = $card_data['Billing_Country'];
                }

                $exyear1 = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . $exyear1;

                $city     = $this->czsecurity->xssCleanPostInput('city');
                $country  = $this->czsecurity->xssCleanPostInput('country');
                $phone    = $this->czsecurity->xssCleanPostInput('phone');
                $state    = $this->czsecurity->xssCleanPostInput('state');
                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                $name     = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address  = $this->czsecurity->xssCleanPostInput('address1') . ' ' . $this->czsecurity->xssCleanPostInput('address2');
                $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $address2 = $this->czsecurity->xssCleanPostInput('baddress2');

                $amount = $this->czsecurity->xssCleanPostInput('totalamount');

                include APPPATH . 'libraries/Freshbooks_data.php';
                $fb_data = new Freshbooks_data($this->resellerID, $this->merchantID);
                $pinv_id = '';

                $transaction = array(
                    "type"             => "sale",
                    "amount"           => ($amount * 100),
                    "currency"         => "USD",
                    "ip_address"       => getClientIpAddr(),
                    "payment_method"   => array(
                        "card" => array(
                            "entry_type"      => "keyed",
                            "number"          => $card_no,
                            "expiration_date" => $expry,
                            "cvc"             => $cvv,
                        ),
                    ),
                    "billing_address"  => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['baddress1'],
                        "address_line_2" => $inputData['baddress2'],
                        "city"           => $inputData['bcity'],
                        "state"          => $inputData['bstate'],
                        "postal_code"    => $inputData['bzipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                    "shipping_address" => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['address1'],
                        "address_line_2" => $inputData['address2'],
                        "city"           => $inputData['city'],
                        "state"          => $inputData['state'],
                        "postal_code"    => $inputData['zipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                );

                if(!empty($this->czsecurity->xssCleanPostInput('po_number'))){
                    $transaction['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }

                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
                    $transaction['order_id'] = $new_invoice_number;
                }

                $crtxnID                         = '';
                $invID                           = '';
                $responseId                      = '';
                $gatewayTransaction              = new Fluidpay();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];
                $result                          = $gatewayTransaction->processTransaction($transaction);

                if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];
                    
                    $invoiceIDs = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                    }

                    $refNumber = array();

                    if (!empty($invoiceIDs)) {

                        foreach ($invoiceIDs as $inID) {

                            $in_data     = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invocieID' => $inID));
                            $refNumber[] = $in_data['refNumber'];
                            $amount_data = $in_data['BalanceRemaining'];
                            $pinv_id     = '';
                            $invoices    = $fb_data->create_invoice_payment($inID, $amount_data);
                            if ($invoices['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed </strong></div>');
                            } else {
                                $pinv_id = $invoices['payment_id'];

                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                        }
                    } else {
                        $crtxnID = '';
                        $inID    = '';

                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                    if (!empty($refNumber)) {
                        $ref_number = implode(',', $refNumber);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['userEmail'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['fullName'];
                    if ($chh_mail == '1') {

                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                    }
                    

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear    = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_type = $this->general_model->getType($card_no);

                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'CardType'                     => $card_type,
                            'CustomerCard'                 => $card_no,
                            'CardCVV'                      => $cvv,
                            'customerListID'               => $customerID,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $this->merchantID,

                            'createdAt'                    => date("Y-m-d H:i:s"),
                            'Billing_Addr1'                => $address1,
                            'Billing_Addr2'                => $address2,
                            'Billing_City'                 => $city,
                            'Billing_State'                => $state,
                            'Billing_Country'              => $country,
                            'Billing_Contact'              => $phone,
                            'Billing_Zipcode'              => $zipcode,
                        );

                        $id1 = $this->card_model->process_card($card_data);
                    }

                    $this->session->set_flashdata('success', 'Successfully Authorized Credit Card Payment');

                } else {
                    $err_msg = $result['msg'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }

            } else {

                $error = 'Validation Error. Please fill the requred fields';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - ' . $error . '</strong></div>');
            }

        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_auth()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");

        if (!empty($this->input->post(null, true))) {

            $inputData  = $this->input->post(null, true);
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
                $companyID  = $comp_data['companyID'];

                $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                } else {
                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $cvv       = $card_data['CardCVV'];
                    $exyear    = $card_data['cardYear'];
                }

                $exyear1 = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . $exyear1;

                $name     = $this->czsecurity->xssCleanPostInput('fistName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address  = $this->czsecurity->xssCleanPostInput('address');
                $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country  = $this->czsecurity->xssCleanPostInput('country');
                $city     = $this->czsecurity->xssCleanPostInput('city');
                $state    = $this->czsecurity->xssCleanPostInput('state');
                $amount   = $this->czsecurity->xssCleanPostInput('totalamount');
                $zipcode  = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '74035';

                $transaction = array(
                    "type"             => "authorize",
                    "amount"           => ($amount * 100),
                    "currency"         => "USD",
                    "ip_address"       => getClientIpAddr(),
                    "payment_method"   => array(
                        "card" => array(
                            "entry_type"      => "keyed",
                            "number"          => $card_no,
                            "expiration_date" => $expry,
                            "cvc"             => $cvv,
                        ),
                    ),
                    "billing_address"  => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['baddress1'],
                        "address_line_2" => $inputData['baddress2'],
                        "city"           => $inputData['bcity'],
                        "state"          => $inputData['bstate'],
                        "postal_code"    => $inputData['bzipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                    "shipping_address" => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['address1'],
                        "address_line_2" => $inputData['address2'],
                        "city"           => $inputData['city'],
                        "state"          => $inputData['state'],
                        "postal_code"    => $inputData['zipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                );

                $crtxnID = '';
                $invID   = $inID   = $responseId   = '';

                $gatewayTransaction              = new Fluidpay();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                $result = $gatewayTransaction->processTransaction($transaction);

                if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];
                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {$cid = $this->czsecurity->xssCleanPostInput('customerID');
                        $card_no                        = $this->czsecurity->xssCleanPostInput('card_number');
                        $card_type                      = $this->general_model->getType($card_no);
                        $expmonth                       = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv = $this->czsecurity->xssCleanPostInput('cvv');

                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'CardType'                     => $card_type,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $merchantID,
                            'customerListID'               => $this->czsecurity->xssCleanPostInput('customerID'),
                            'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('baddress1'),
                            'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('baddress2'),
                            'Billing_City'                 => $this->czsecurity->xssCleanPostInput('bcity'),
                            'Billing_Country'              => $this->czsecurity->xssCleanPostInput('bcountry'),
                            'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('bphone'),
                            'Billing_State'                => $this->czsecurity->xssCleanPostInput('bstate'),
                            'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('bzipcode'),
                            'CustomerCard'                 => $card_no,
                            'CardCVV'                      => $cvv,
                            'updatedAt'                    => date("Y-m-d H:i:s"),
                        );

                        $card = $this->card_model->process_card($card_data);

                    }

                    if ($chh_mail == '1') {

                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                        if (!empty($refNumber)) {
                            $ref_number = implode(',', $refNumber);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $comp_data['userEmail'];
                        $company  = $comp_data['companyName'];
                        $customer = $comp_data['fullName'];
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                    }

                    $this->session->set_flashdata('success', 'Successfully Authorized Credit Card Payment');
                } else {
                    $err_msg = $result['msg'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser);

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Please select gateway</strong>.</div>');
            }

            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');

        }

    }

    public function create_customer_esale()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();

            $amount = $this->czsecurity->xssCleanPostInput('totalamount');

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }

                $transaction = array(
                    "type"             => "sale",
                    "amount"           => $amount * 100,
                    "currency"         => "USD",
                    "ip_address"       => getClientIpAddr(),
                    "payment_method"   => array(
                        "ach" => array(
                            "routing_number" => $accountDetails['routeNumber'],
                            "account_number" => $accountDetails['accountNumber'],
                            "sec_code"       => $accountDetails['secCodeEntryMethod'],
                            "account_type"   => $accountDetails['accountType'],
                        ),
                    ),
                    "billing_address"  => array(
                        "first_name"     => $this->czsecurity->xssCleanPostInput('firstName'),
                        "last_name"      => $this->czsecurity->xssCleanPostInput('lastName'),
                        "company"        => $this->czsecurity->xssCleanPostInput('companyName'),
                        "address_line_1" => $this->czsecurity->xssCleanPostInput('baddress1'),
                        "address_line_2" => $this->czsecurity->xssCleanPostInput('baddress2'),
                        "city"           => $this->czsecurity->xssCleanPostInput('bcity'),
                        "state"          => $this->czsecurity->xssCleanPostInput('bstate'),
                        "postal_code"    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        "phone"          => $this->czsecurity->xssCleanPostInput('phone'),
                        "fax"            => $this->czsecurity->xssCleanPostInput('phone'),
                        "email"          => $this->czsecurity->xssCleanPostInput('email'),
                    ),
                    "shipping_address" => array(
                        "first_name"     => $this->czsecurity->xssCleanPostInput('firstName'),
                        "last_name"      => $this->czsecurity->xssCleanPostInput('lastName'),
                        "company"        => $this->czsecurity->xssCleanPostInput('companyName'),
                        "address_line_1" => $this->czsecurity->xssCleanPostInput('address1'),
                        "address_line_2" => $this->czsecurity->xssCleanPostInput('address2'),
                        "city"           => $this->czsecurity->xssCleanPostInput('city'),
                        "state"          => $this->czsecurity->xssCleanPostInput('state'),
                        "postal_code"    => $this->czsecurity->xssCleanPostInput('zipcode'),
                        "phone"          => $this->czsecurity->xssCleanPostInput('phone'),
                        "fax"            => $this->czsecurity->xssCleanPostInput('phone'),
                        "email"          => $this->czsecurity->xssCleanPostInput('email'),
                    ),
                );

                if(!empty($this->czsecurity->xssCleanPostInput('po_number'))){
                    $transaction['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }

                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 3);
                    $transaction['order_id'] = $new_invoice_number;
                }
                
                $invoiceIDs = [];

                $responseId                      = '';
                $gatewayTransaction              = new Fluidpay();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                $result = $gatewayTransaction->processTransaction($transaction);

                if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];

                    $invoiceIDs = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                    }

                    $refNumber = array();

                    if (!empty($invoiceIDs)) {

                        foreach ($invoiceIDs as $inID) {

                            $in_data     = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invocieID' => $inID));
                            $refNumber[] = $in_data['refNumber'];
                            $amount_data = $in_data['BalanceRemaining'];
                            $pinv_id     = '';
                            $invoices    = $fb_data->create_invoice_payment($inID, $amount_data);
                            if ($invoices['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed</strong></div>');
                            } else {
                                $pinv_id = $invoices['payment_id'];

                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser);
                        }
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);
                    }

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $this->session->set_flashdata('success', 'Successfully Processed Electronic Check');
                } else {
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['msg'] . '</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => isset($responseId) ? $responseId : '',
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('FreshBooks_controllers/Transactions/create_customer_esale', 'refresh');
    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnID');

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $payusername = $gt_result['gatewayUsername'];
            $paypassword = $gt_result['gatewayPassword'];
            $grant_type  = "password";

            $integratorId = $gt_result['gatewaySignature'];

            $payAPI = new PayTraceAPINEW();

            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

            //call a function of Utilities.php to verify if there is any error with OAuth token.
            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
            $amount            = $paydata['transactionAmount'];

            $customerID = $paydata['customerListID'];

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $payload = [
                'amount' => ($amount * 100)
            ];
            $result = $gatewayTransaction->refundTransaction($tID, $payload);

            if ($result['status'] == 'success') {
                $this->customer_model->update_refund_payment($tID, iTransactGatewayName);

                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );

                $merchID = $paydata['merchantID'];

                $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
                $rs_data         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
                $redID           = $rs_data['resellerID'];
                $condition1      = array('resellerID' => $redID);
                $sub1            = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                $domain1         = $sub1['merchantPortalURL'];
                $URL1            = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth'; //print_r($gt_result);die;

                $subdomain           = $Freshbooks_data['sub_domain'];
                $key                 = $Freshbooks_data['secretKey'];
                $accessToken         = $Freshbooks_data['accessToken'];
                $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                define('OAUTH_CONSUMER_KEY', $subdomain);
                define('OAUTH_CONSUMER_SECRET', $key);
                define('OAUTH_CALLBACK', $URL1);

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                        $imn_amount     = $ref_inv_amount->inv_amount;
                        $p_ids          = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();
                        foreach ($ref_inv as $k => $r_inv) {
                            $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                            if (!empty($inv_data)) {
                                $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                            } else {
                                $due_date = date('Y-m-d');
                            }

                            $re_amount = $imn_amount[$k];
                            if ($refund > 0 && $imn_amount[$k] > 0) {

                                $p_refund1 = 0;

                                $p_id = $p_ids[$k];

                                $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                if ($refund <= $imn_amount[$k]) {
                                    $p_refund1 = $refund;

                                    $p_refund = $imn_amount[$k] - $refund;

                                    $re_amount = $imn_amount[$k] - $refund;
                                    $refund    = 0;

                                } else {
                                    $p_refund  = 0;
                                    $p_refund1 = $imn_amount[$k];
                                    $refund    = $refund - $imn_amount[$k];

                                    $re_amount = 0;
                                }

                                if (isset($accessToken) && isset($access_token_secret)) {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="payment.update">
                                            <payment>

                                                <payment_id>' . $p_id . '</payment_id>
                                                <amount>' . $p_refund . '</amount>
                                                <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                <currency_code>USD</currency_code>
                                                </payment>

                                        </request>';

                                    $invoices = $c->edit_payment($payment);

                                    if ($invoices['status'] != 'ok') {

                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed </strong></div>');

                                       

                                    } else {
                                        $ins_id    = $p_id;
                                        $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                            'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                            'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                        );
                                        $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                        $lineArray = '';

                                        foreach ($ittem as $item_val) {

                                            $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                            $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                            $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                            $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                            $lineArray .= '<type>Item</type></line>';

                                        }
                                        $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                        $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                        $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                        $lineArray .= '<quantity>1</quantity>';
                                        $lineArray .= '<type>Item</type></line>';

                                        $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="invoice.update">
                                            <invoice>
                                            <invoice_id>' . $r_inv . '</invoice_id>
                                                <date>' . $due_date . '</date>
                                                <lines>' . $lineArray . '</lines>
                                            </invoice>
                                            </request> ';

                                        $invoices1 = $c->add_invoices($invoice);

                                        $this->session->set_flashdata('success', 'Transaction Successfully Refunded');

                                       
                                    }

                                }

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;

                            } else {

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;
                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
                                <request method="payment.update">
                                    <payment>

                                        <payment_id>' . $p_id . '</payment_id>
                                        <amount>' . $p_amount . '</amount>
                                        <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                        <currency_code>USD</currency_code>
                                        </payment>

                                </request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed</strong></div>');

                               

                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                    <request method="invoice.update">
                        <invoice>
                        <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

                            <lines>' . $lineArray . '</lines>
                        </invoice>
                        </request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Transaction Successfully Refunded');

                            }
                        }
                    }

                }

                $this->session->set_flashdata('success', 'Transaction Successfully Refunded');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - ' . $result['msg'] . '</strong>.</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $paydata['customerListID'], $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser);

            redirect('FreshBooks_controllers/Transactions/echeck_transaction', 'refresh');
        }
    }

    public function payment_evoid()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $paydata['transactionAmount'];

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $result = $gatewayTransaction->voidTransaction($tID);
                
            if ($result['status'] == 'success') {

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                }

                $this->session->set_flashdata('success', $result['status']);
            } else {
                $err_msg      = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser);
        }
        redirect('FreshBooks_controllers/Transactions/evoid_transaction', 'refresh');
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $transactionData = [
                "amount" => ((float) $amount) * 100,
            ];

            $result = $gatewayTransaction->captureTransaction($tID, $transactionData);
            
            if ($result['status'] == 'success') {

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {

                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                }
                $this->session->set_flashdata('success', 'Successfully Captured Authorization');
            } else {
                $err_msg      = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser);

            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Transactions/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            
            if (!isset($result['data']) || $result['data']['id'] == '') {
                $result['transactionid'] = 'null';
            } else {
                $result['transactionid'] = $result['data']['id'];
            }

            redirect('FreshBooks_controllers/home/transation_credit_receipt/' . $paydata['invoiceTxnID'] . '/' . $paydata['customerListID'] . '/' . $result['transactionid'], 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_void()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $result = $gatewayTransaction->voidTransaction($tID);
                
            if ($result['status'] == 'success') {

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                }

                $this->session->set_flashdata('success', $result['status']);
            } else {
                $err_msg      = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser);
        }
        redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');

    }

    public function create_customer_refund()
    {

        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            $tID        = $this->czsecurity->xssCleanPostInput('paytxnID');
            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey      = $gt_result['gatewayPassword'];

            $customerID = $paydata['customerListID'];
            $amount1    = $paydata['transactionAmount'];
            $total      = $this->czsecurity->xssCleanPostInput('ref_amount');
            $amount     = $total;

            if ($paydata['transactionCode'] == '100') {
                $gatewayTransaction              = new Fluidpay();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                $payload = [
                    'amount' => ($amount * 100)
                ];
                $result = $gatewayTransaction->refundTransaction($tID, $payload);
            }

            $resposneId= '';
            if ($result['status'] == 'success') {
                $resposneId = $result['data']['id'];
                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );

                $merchID = $paydata['merchantID'];

                $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
                $rs_data         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
                $redID           = $rs_data['resellerID'];
                $condition1      = array('resellerID' => $redID);
                $sub1            = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                $domain1         = $sub1['merchantPortalURL'];
                $URL1            = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

                $subdomain           = $Freshbooks_data['sub_domain'];
                $key                 = $Freshbooks_data['secretKey'];
                $accessToken         = $Freshbooks_data['accessToken'];
                $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                define('OAUTH_CONSUMER_KEY', $subdomain);
                define('OAUTH_CONSUMER_SECRET', $key);
                define('OAUTH_CALLBACK', $URL1);

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                        $imn_amount     = $ref_inv_amount->inv_amount;
                        $p_ids          = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();
                        foreach ($ref_inv as $k => $r_inv) {
                            $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                            if (!empty($inv_data)) {
                                $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                            } else {
                                $due_date = date('Y-m-d');
                            }

                            $re_amount = $imn_amount[$k];
                            if ($refund > 0 && $imn_amount[$k] > 0) {

                                $p_refund1 = 0;

                                $p_id = $p_ids[$k];

                                $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                if ($refund <= $imn_amount[$k]) {
                                    $p_refund1 = $refund;

                                    $p_refund = $imn_amount[$k] - $refund;

                                    $re_amount = $imn_amount[$k] - $refund;
                                    $refund    = 0;

                                } else {
                                    $p_refund  = 0;
                                    $p_refund1 = $imn_amount[$k];
                                    $refund    = $refund - $imn_amount[$k];

                                    $re_amount = 0;
                                }

                                if (isset($accessToken) && isset($access_token_secret)) {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="payment.update">
                                            <payment>

                                                <payment_id>' . $p_id . '</payment_id>
                                                <amount>' . $p_refund . '</amount>
                                                <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                <currency_code>USD</currency_code>
                                                </payment>

                                        </request>';

                                    $invoices = $c->edit_payment($payment);

                                    if ($invoices['status'] != 'ok') {

                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed</strong></div>');


                                    } else {
                                        $ins_id    = $p_id;
                                        $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                            'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                            'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                        );
                                        $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                        $lineArray = '';

                                        foreach ($ittem as $item_val) {

                                            $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                            $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                            $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                            $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                            $lineArray .= '<type>Item</type></line>';

                                        }
                                        $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                        $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                        $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                        $lineArray .= '<quantity>1</quantity>';
                                        $lineArray .= '<type>Item</type></line>';

                                        $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="invoice.update">
                                            <invoice>
                                            <invoice_id>' . $r_inv . '</invoice_id>
                                                <date>' . $due_date . '</date>
                                                <lines>' . $lineArray . '</lines>
                                            </invoice>
                                            </request> ';

                                        $invoices1 = $c->add_invoices($invoice);

                                        $this->session->set_flashdata('success', 'Transaction Successfully Refunded');

                                    }

                                }

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;

                            } else {

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;
                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
                                <request method="payment.update">
                                    <payment>

                                        <payment_id>' . $p_id . '</payment_id>
                                        <amount>' . $p_amount . '</amount>
                                        <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                        <currency_code>USD</currency_code>
                                        </payment>

                                </request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed</strong></div>');


                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                    <request method="invoice.update">
                        <invoice>
                        <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

                            <lines>' . $lineArray . '</lines>
                        </invoice>
                        </request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Transaction Successfully Refunded');

                            }
                        }
                    }

                }

                
                $this->fb_customer_model->update_refund_payment($tID, iTransactGatewayName);

                $this->session->set_flashdata('success', 'Success ' . $result['status']);
            } else {
                $err_msg      = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $paydata['customerListID'], $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser);

            $invoice_IDs = array();
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Transactions/payment_refund',
                'proccess_btn_text' => 'Process New Refund',
                'sub_header'        => 'Refund',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            
            redirect('FreshBooks_controllers/home/transation_credit_receipt/' . $paydata['invoiceTxnID'] . '/' . $paydata['customerListID'] . '/' . $resposneId, 'refresh');
        }

    }

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $invoicp_id = '';
        $merchantID = $merchID;
        $user_id = $merchID;

        $rs_daata   = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
        $resellerID = $rs_daata['resellerID'];
        $condition1 = array('resellerID' => $resellerID);

        $sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1 = $sub1['merchantPortalURL'];
        $URL1    = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        $val = array(
            'merchantID' => $merchID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);

        $subdomain           = $Freshbooks_data['sub_domain'];
        $key                 = $Freshbooks_data['secretKey'];
        $accessToken         = $Freshbooks_data['accessToken'];
        $access_token_secret = $Freshbooks_data['oauth_token_secret'];

        define('OAUTH_CONSUMER_KEY', $subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
        $cardID  = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway = $this->czsecurity->xssCleanPostInput('gateway1');

        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

        $apiUsername = $gt_result['gatewayUsername'];
        $apiKey      = $gt_result['gatewayPassword'];
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $cardID != "" || $gateway != "") {

            $invices = $this->czsecurity->xssCleanPostInput('multi_inv');
            if (!empty($invices)) {

                foreach ($invices as $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
    
                            $address1 = $this->czsecurity->xssCleanPostInput('address1');
                            $address2 = $this->czsecurity->xssCleanPostInput('address2');
                            $city     = $this->czsecurity->xssCleanPostInput('city');
                            $country  = $this->czsecurity->xssCleanPostInput('country');
                            $phone    = $this->czsecurity->xssCleanPostInput('contact');
                            $state    = $this->czsecurity->xssCleanPostInput('state');
                            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }

                        if (!empty($card_data)) {

                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount   = $pay_amounts;
                            $amount   = $amount - $cr_amount;
                            $card_no  = $card_data['CardNo'];
                            $expmonth = $card_data['cardMonth'];

                            $exyear = $card_data['cardYear'];
                            $cvv = $card_data['CardCVV'];

                            $name    = $in_data['fullName'];
                            $address = $in_data['ShipAddress_Addr1'];
                            
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;
                           
                            
                            $gatewayTransaction              = new Fluidpay();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                            $transaction = array(
                                "type"                => "sale",
                                "amount"              => ($amount * 100),
                                "currency"            => "USD",
                                "ip_address"          => getClientIpAddr(),
                                "payment_method"      => array(
                                    "card" => array(
                                        "entry_type"      => "keyed",
                                        "number"          => $card_no,
                                        "expiration_date" => $expry,
                                        "cvc"             => $cvv,
                                    ),
                                ),
                                "billing_address"     => array(
                                    "first_name"     => $in_data['firstName'],
                                    "last_name"      => $in_data['lastName'],
                                    "company"        => $in_data['companyName'],
                                    "address_line_1" => $address,
                                    "address_line_2" => $address2,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                ),
                                "shipping_address"    => array(
                                    "first_name"     => $in_data['firstName'],
                                    "last_name"      => $in_data['lastName'],
                                    "company"        => $in_data['companyName'],
                                    "address_line_1" => $address,
                                    "address_line_2" => $address2,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                ),
                            );

                            $responseId = $crtxnID = '';
                            
            				$result = $gatewayTransaction->processTransaction($transaction);
                            if ($result['status'] == 'success') {
                                $responseId = $result['data']['id'];

                                if (isset($accessToken) && isset($access_token_secret)) {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $subdomain, $accessToken, $access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="payment.create">
                                        <payment>
                                            <invoice_id>' . $invoiceID . '</invoice_id>
                                            <amount>' . $amount . '</amount>
                                            <currency_code>USD</currency_code>
                                            <type>Check</type>
                                        </payment>
                                        </request>';

                                    $invoices = $c->add_payment($payment);
                                    if ($invoices['status'] != 'ok') {

                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed </strong></div>');

                                        redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');

                                    } else {
                                        $invoicp_id = $invoices['payment_id'];
                                        $this->session->set_flashdata('success', 'Success');

                                    }
                                }

                            } else {
                                $err_msg      = $result['msg'];
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $user_id, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser);

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice</strong>.</div>');
                    }
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Please select invoices</strong>.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Select Gateway and Card</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');

    }

}
