<?php
/*
 * NMI, Authorize.net, Paytrace, Paypal, Stripe Payment Gateway Operations

 * Refund create_customer_refund
 * Single Invoice Payment transaction refund
 * merchantID ans resellerID are Private Member
 */
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

include APPPATH . 'third_party/Fluidpay.class.php';
include APPPATH . 'third_party/TSYS.class.php';
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;

class RefundInvoice extends CI_Controller
{
    private $gatewayEnvironment;
    private $merchantID;
    private $resellerID;
    private $transactionByUser;

    public function __construct()
    {

        parent::__construct();
        include APPPATH . 'third_party/Freshbooks.php';

        $this->load->config('auth_pay');
        $this->load->config('paytrace');
        $this->load->config('paypal');
        $this->load->config('fluidpay');
        $this->load->config('TSYS');
        $this->load->config('payarc');
        $this->load->library('PayarcGateway');

        $this->load->config('maverick');
        $this->load->library('MaverickGateway');

                
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        if ($this->session->userdata('logged_in')) {
            
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $logged_in_data['merchID'];
        }
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        redirect('FreshBooks_controllers/Transactions/payment_transaction', 'refresh');
    }

    public function create_customer_refund()
    {

        $trID       = $this->czsecurity->xssCleanPostInput('trID');
        $refAmt     = $this->czsecurity->xssCleanPostInput('refAmt');
        $ref_invID  = $this->czsecurity->xssCleanPostInput('ref_invID');
        $paydata    = $this->general_model->get_select_data('customer_transaction', array('transactionID', 'transactionCard', 'transactionAmount', 'customerListID', 'merchantID', 'transactionGateway', 'gatewayID' ,'gateway', 'invoiceID', 'invoiceRefID', 'qbListTxnID'), array('id' => $trID));
        $con        = array('id' => $trID);
        $gatlistval = $paydata['gatewayID'];
		$tID        = $paydata['transactionID'];
		
		$codests = '';
        if ($paydata['transactionGateway'] == '1' || $paydata['transactionGateway'] == '9') {
            include APPPATH . 'third_party/nmiDirectPost.class.php';

			$gatewayName = getGatewayName($paydata['transactionGateway']);

            $gatlistval = $paydata['gatewayID'];
            $tID        = $paydata['transactionID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $nmiuser  = $gt_result['gatewayUsername'];
            $nmipass  = $gt_result['gatewayPassword'];
            $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

            $transaction = new nmiDirectPost($nmi_data);
            $customerID  = $paydata['customerListID'];


            $amount = $paydata['transactionAmount'];
            $amount = $refAmt;

            $transaction->setTransactionId($tID);

            $transaction->refund($tID, $amount);

            $result = $transaction->execute();

            if ($result['response_code'] == '100') {

                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );

                $merchID = $paydata['merchantID'];

                $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
                $rs_data         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
                $redID           = $rs_data['resellerID'];
                $condition1      = array('resellerID' => $redID);
                $sub1            = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                $domain1         = $sub1['merchantPortalURL'];
                $URL1            = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth'; //print_r($gt_result);die;

                $subdomain           = $Freshbooks_data['sub_domain'];
                $key                 = $Freshbooks_data['secretKey'];
                $accessToken         = $Freshbooks_data['accessToken'];
                $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                define('OAUTH_CONSUMER_KEY', $subdomain);
                define('OAUTH_CONSUMER_SECRET', $key);
                define('OAUTH_CALLBACK', $URL1);

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);

                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);

                        $imn_amount = $ref_inv_amount->inv_amount;
                        $p_ids      = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();

                        foreach ($ref_inv as $k => $r_inv) {

                            if ($r_inv == $ref_invID) {

                                $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                                if (!empty($inv_data)) {
                                    $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                                } else {
                                    $due_date = date('Y-m-d');
                                }

                                $re_amount = $imn_amount[$k];

                                if ($refund > 0 && $imn_amount[$k] > 0) {

                                    $p_refund1 = 0;

                                    $p_id = $p_ids[$k];

                                    $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                    if ($refund <= $imn_amount[$k]) {
                                        $p_refund1 = $refund;

                                        $p_refund = $imn_amount[$k] - $refund;

                                        $re_amount = $imn_amount[$k] - $refund;
                                        $refund    = 0;

                                    } else {
                                        $p_refund  = 0;
                                        $p_refund1 = $imn_amount[$k];
                                        $refund    = $refund - $imn_amount[$k];

                                        $re_amount = 0;
                                    }

                                    if (isset($accessToken) && isset($access_token_secret)) {
                                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                        $payment = '<?xml version="1.0" encoding="utf-8"?>
                    						<request method="payment.update">
                    						  <payment>

                                                    <payment_id>' . $p_id . '</payment_id>
                                                    <amount>' . $p_refund . '</amount>
                                                    <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>

                    						</request>';

                                        $invoices = $c->edit_payment($payment);

                                        if ($invoices['status'] != 'ok') {

                                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                          

                                        } else {
                                            $ins_id    = $p_id;
                                            $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                                'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                                'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                                'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                            );
                                            $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                            $lineArray = '';

                                            foreach ($ittem as $item_val) {

                                                $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                                $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                                $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                                $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                                $lineArray .= '<type>Item</type></line>';

                                            }
                                            $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                            $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                            $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                            $lineArray .= '<quantity>1</quantity>';
                                            $lineArray .= '<type>Item</type></line>';

                                            $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update">
                                     		 <invoice>
                                     		  <invoice_id>' . $r_inv . '</invoice_id>
                                     		    <date>' . $due_date . '</date>
                        				          <lines>' . $lineArray . '</lines>
                        						</invoice>
                        						</request> ';

                                            $invoices1 = $c->add_invoices($invoice);

                                            $this->session->set_flashdata('success', 'Success');

                                            
                                        }

                                    }

                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;

                                } else {

                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;
                                }

                            } else {

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $imn_amount[$k];

                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
            						<request method="payment.update">
            						  <payment>

                                            <payment_id>' . $p_id . '</payment_id>
                                            <amount>' . $p_amount . '</amount>
                                            <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>

            						</request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                

                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update">
                 		 <invoice>
                 		  <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

    				          <lines>' . $lineArray . '</lines>
    						</invoice>
    						</request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Success');

                               
                            }
                        }
                    }

                }

                $this->fb_customer_model->update_refund_payment($tID, $gatewayName);

                $this->session->set_flashdata('success', 'Success');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $result['responsetext'] . '</strong>.</div>');
            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result['transactionid'];
            $transactiondata['transactionStatus']   = $result['responsetext'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = $result['type'];
            $transactiondata['transactionCode']     = $result['response_code'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $paydata['merchantID'];

            if (!empty($paydata['invoiceID'])) {
                $transactiondata['invoiceID'] = $paydata['invoiceID'];
            }
            $transactiondata['resellerID'] = $this->resellerID;
            $transactiondata['gateway']    = $gatewayName;

            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

        } else if ($paydata['transactionGateway'] == '2') {
            include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

            $gatlistval = $paydata['gatewayID'];
            $tID        = $paydata['transactionID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiloginID     = $gt_result['gatewayUsername'];
            $transactionKey = $gt_result['gatewayPassword'];

            $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
            $transaction->setSandbox($this->config->item('Sandbox'));

            $merchantID = $paydata['merchantID'];

            $card       = $paydata['transactionCard'];
            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $amount     = $refAmt;


            $result = $transaction->credit($tID, $amount, $card);

            if ($result->response_code == '1') {

                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );

                $merchID = $paydata['merchantID'];

                $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);

                $redID      = $this->resellerID;
                $condition1 = array('resellerID' => $redID);
                $sub1       = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                $domain1    = $sub1['merchantPortalURL'];
                $URL1       = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth'; //print_r($gt_result);die;

                $subdomain           = $Freshbooks_data['sub_domain'];
                $key                 = $Freshbooks_data['secretKey'];
                $accessToken         = $Freshbooks_data['accessToken'];
                $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                define('OAUTH_CONSUMER_KEY', $subdomain);
                define('OAUTH_CONSUMER_SECRET', $key);
                define('OAUTH_CALLBACK', $URL1);

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                        $imn_amount     = $ref_inv_amount->inv_amount;
                        $p_ids          = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();
                        foreach ($ref_inv as $k => $r_inv) {

                            if ($r_inv == $ref_invID) {

                                $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                                if (!empty($inv_data)) {
                                    $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                                } else {
                                    $due_date = date('Y-m-d');
                                }

                                $re_amount = $imn_amount[$k];
                                if ($refund > 0 && $imn_amount[$k] > 0) {

                                    $p_refund1 = 0;

                                    $p_id = $p_ids[$k];

                                    $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                    if ($refund <= $imn_amount[$k]) {
                                        $p_refund1 = $refund;
                                        $p_refund  = $imn_amount[$k] - $refund;
                                        $re_amount = $imn_amount[$k] - $refund;
                                        $refund    = 0;

                                    } else {
                                        $p_refund  = 0;
                                        $p_refund1 = $imn_amount[$k];
                                        $refund    = $refund - $imn_amount[$k];

                                        $re_amount = 0;
                                    }

                                    if (isset($accessToken) && isset($access_token_secret)) {
                                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                        $payment = '<?xml version="1.0" encoding="utf-8"?>
                    						<request method="payment.update">
                    						  <payment>

                                                    <payment_id>' . $p_id . '</payment_id>
                                                    <amount>' . $p_refund . '</amount>
                                                    <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>

                    						</request>';

                                        $invoices = $c->edit_payment($payment);

                                        if ($invoices['status'] != 'ok') {

                                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                          

                                        } else {
                                            $ins_id    = $p_id;
                                            $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                                'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                                'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                                'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                            );
                                            $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                            $lineArray = '';

                                            foreach ($ittem as $item_val) {

                                                $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                                $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                                $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                                $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                                $lineArray .= '<type>Item</type></line>';

                                            }
                                            $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                            $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                            $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                            $lineArray .= '<quantity>1</quantity>';
                                            $lineArray .= '<type>Item</type></line>';

                                            $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update">
                                     		 <invoice>
                                     		  <invoice_id>' . $r_inv . '</invoice_id>

                                     		   <date>' . $due_date . '</date>

                        				          <lines>' . $lineArray . '</lines>
                        						</invoice>
                        						</request> ';

                                            $invoices1 = $c->add_invoices($invoice);

                                            $this->session->set_flashdata('success', 'Success');

                                           
                                        }

                                    }

                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;

                                } else {

                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;
                                }

                            } else {
                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $imn_amount[$k];

                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
            						<request method="payment.update">
            						  <payment>

                                            <payment_id>' . $p_id . '</payment_id>
                                            <amount>' . $p_amount . '</amount>
                                            <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>

            						</request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                               

                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update">
                 		 <invoice>
                 		  <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

    				          <lines>' . $lineArray . '</lines>
    						</invoice>
    						</request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Success');

                             
                            }
                        }
                    }

                }

                $this->fb_customer_model->update_refund_payment($tID, 'AUTH');

                $this->session->set_flashdata('success', 'Success');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $result->response_reason_text . '</strong>.</div>');
            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result->transaction_id;
            $transactiondata['transactionStatus']   = $result->response_reason_text;
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result->response_code;
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            if (!empty($paydata['invoiceID'])) {
                $transactiondata['invoiceID'] = $paydata['invoiceID'];
            }
            $transactiondata['transactionType']   = $result->transaction_type;
            $transactiondata['customerListID']    = $customerID;
            $transactiondata['transactionAmount'] = $result->amount;

            $transactiondata['merchantID'] = $this->merchantID;
            $transactiondata['resellerID'] = $this->resellerID;
            $transactiondata['gateway']    = "Auth";
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

        } else if ($paydata['transactionGateway'] == '3') {

            include APPPATH . 'third_party/PayTraceAPINEW.php';

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $payusername = $gt_result['gatewayUsername'];
            $paypassword = $gt_result['gatewayPassword'];
            $grant_type  = "password";
            $payAPI      = new PayTraceAPINEW();
            $merchantID  = $paydata['merchantID'];

            $card         = $paydata['transactionCard'];
            $customerID   = $paydata['customerListID'];
            $amount       = $paydata['transactionAmount'];
            $amount       = $refAmt;
            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
            //call a function of Utilities.php to verify if there is any error with OAuth token.
            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
            if (!$oauth_moveforward) {
                //Decode the Raw Json response.
                $json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

                //set Authentication value based on the successful oAuth response.
                //Add a space between 'Bearer' and access _token
                $oauth_token = sprintf("Bearer %s", $json['access_token']);

                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);

                $customerID = $paydata['customerListID'];
               
                $amount1 = $paydata['transactionAmount'];
                $total   = $refAmt;
                $amount  = $total;
                if ($paydata['transactionCode'] == '200') {
                    $request_data = array("transaction_id" => $tID, 'amount' => $total);
                    // encode Json data by calling a function from json.php
                    $request_data = json_encode($request_data);
                    $result       = $payAPI->processTransaction($oauth_token, $request_data, URL_TRID_REFUND);
                    $response     = $payAPI->jsonDecode($result['temp_json_response']);

                }

                if ($result['http_status_code'] == '200') {

                    $val = array(
                        'merchantID' => $paydata['merchantID'],
                    );

                    $merchID = $paydata['merchantID'];

                    $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);

                    $redID      = $this->resellerID;
                    $condition1 = array('resellerID' => $redID);
                    $sub1       = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                    $domain1    = $sub1['merchantPortalURL'];
                    $URL1       = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

                    $subdomain           = $Freshbooks_data['sub_domain'];
                    $key                 = $Freshbooks_data['secretKey'];
                    $accessToken         = $Freshbooks_data['accessToken'];
                    $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                    define('OAUTH_CONSUMER_KEY', $subdomain);
                    define('OAUTH_CONSUMER_SECRET', $key);
                    define('OAUTH_CALLBACK', $URL1);

                    if (!empty($paydata['invoiceID'])) {
                        $refund  = $amount;
                        $ref_inv = explode(',', $paydata['invoiceID']);
                        if (count($ref_inv) > 1) {
                            $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                            $imn_amount     = $ref_inv_amount->inv_amount;
                            $p_ids          = explode(',', $paydata['qbListTxnID']);

                            $inv_array = array();
                            foreach ($ref_inv as $k => $r_inv) {

                                if ($_inv == $ref_invID) {
                                    $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                                    if (!empty($inv_data)) {
                                        $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                                    } else {
                                        $due_date = date('Y-m-d');
                                    }

                                    $re_amount = $imn_amount[$k];
                                    if ($refund > 0 && $imn_amount[$k] > 0) {

                                        $p_refund1 = 0;

                                        $p_id = $p_ids[$k];

                                        $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                        if ($refund <= $imn_amount[$k]) {
                                            $p_refund1 = $refund;

                                            $p_refund = $imn_amount[$k] - $refund;

                                            $re_amount = $imn_amount[$k] - $refund;
                                            $refund    = 0;

                                        } else {
                                            $p_refund  = 0;
                                            $p_refund1 = $imn_amount[$k];
                                            $refund    = $refund - $imn_amount[$k];

                                            $re_amount = 0;
                                        }

                                        if (isset($accessToken) && isset($access_token_secret)) {
                                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                            $payment = '<?xml version="1.0" encoding="utf-8"?>
                    						<request method="payment.update">
                    						  <payment>

                                                    <payment_id>' . $p_id . '</payment_id>
                                                    <amount>' . $p_refund . '</amount>
                                                    <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>

                    						</request>';

                                            $invoices = $c->edit_payment($payment);

                                            if ($invoices['status'] != 'ok') {

                                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                              

                                            } else {
                                                $ins_id    = $p_id;
                                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                                    'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                                );
                                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                                $lineArray = '';

                                                foreach ($ittem as $item_val) {

                                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                                    $lineArray .= '<type>Item</type></line>';

                                                }
                                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                                $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                                $lineArray .= '<quantity>1</quantity>';
                                                $lineArray .= '<type>Item</type></line>';

                                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update">
                                     		 <invoice>
                                     		  <invoice_id>' . $r_inv . '</invoice_id>
                                     		      <date>' . $due_date . '</date>
                        				          <lines>' . $lineArray . '</lines>
                        						</invoice>
                        						</request> ';

                                                $invoices1 = $c->add_invoices($invoice);

                                                $this->session->set_flashdata('success', ' Success');

                                               
                                            }

                                        }

                                        $inv_array['inv_no'][]     = $r_inv;
                                        $inv_array['inv_amount'][] = $re_amount;

                                    } else {

                                        $inv_array['inv_no'][]     = $r_inv;
                                        $inv_array['inv_amount'][] = $re_amount;
                                    }
                                } else {
                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $imn_amount[$k];

                                }

                            }
                            $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                        } else {

                            $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                            $p_id     = $paydata['qbListTxnID'];
                            $p_amount = $paydata['transactionAmount'] - $amount;

                            if (isset($accessToken) && isset($access_token_secret)) {
                                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                $payment = '<?xml version="1.0" encoding="utf-8"?>
            						<request method="payment.update">
            						  <payment>

                                            <payment_id>' . $p_id . '</payment_id>
                                            <amount>' . $p_amount . '</amount>
                                            <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>

            						</request>';

                                $invoices = $c->edit_payment($payment);

                                if ($invoices['status'] != 'ok') {

                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                  

                                } else {
                                    $ins_id    = $p_id;
                                    $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                        'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                        'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                    );
                                    $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                    $lineArray = '';

                                    foreach ($ittem as $item_val) {

                                        $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                        $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                        $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                        $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                        $lineArray .= '<type>Item</type></line>';

                                    }
                                    $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                    $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                    $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                    $lineArray .= '<quantity>1</quantity>';
                                    $lineArray .= '<type>Item</type></line>';

                                    $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update">
                 		 <invoice>
                 		  <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

    				          <lines>' . $lineArray . '</lines>
    						</invoice>
    						</request> ';

                                    $invoices1 = $c->add_invoices($invoice);

                                    $this->session->set_flashdata('success', 'Success');

                                   
                                }
                            }
                        }

                    }

                    $this->fb_customer_model->update_refund_payment($tID, 'PAYTRACE');

                    $this->session->set_flashdata('success', ' Success ' . $response['status_message']);
                } else {

                    if (!empty($response['errors'])) {$err_msg = $this->getError($response['errors']);} else { $err_msg = $approval_message;}

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $transactiondata = array();
                if (isset($response['transaction_id'])) {
                    $transactiondata['transactionID'] = $response['transaction_id'];
                } else {
                    $transactiondata['transactionID'] = '';
                }
                $transactiondata['transactionStatus']   = $response['status_message'];
                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                $transactiondata['transactionCode']     = $result['http_status_code'];
                $transactiondata['transactionCard']     = $paydata['transactionCard'];
                $transactiondata['gatewayID']           = $gatlistval;
                $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
                $transactiondata['transactionType']     = 'pay_refund';
                $transactiondata['customerListID']      = $paydata['customerListID'];
                $transactiondata['transactionAmount']   = $amount;

                if (!empty($paydata['invoiceID'])) {
                    $transactiondata['invoiceID'] = $paydata['invoiceID'];
                }
                $transactiondata['merchantID'] = $paydata['merchantID'];
                $transactiondata['resellerID'] = $this->resellerID;
                $transactiondata['gateway']    = "Paytrace";
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Authentication failed.</strong></div>');
            }

        } else if ($paydata['transactionGateway'] == '4') {
            include APPPATH . 'third_party/PayPalAPINEW.php';
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $username  = $gt_result['gatewayUsername'];
            $password  = $gt_result['gatewayPassword'];
            $signature = $gt_result['gatewaySignature'];

            $config = array(
                'Sandbox'      => $this->config->item('Sandbox'), // Sandbox / testing mode option.
                'APIUsername'  => $username, // PayPal API username of the API caller
                'APIPassword'  => $password, // PayPal API password of the API caller
                'APISignature' => $signature, // PayPal API signature of the API caller
                'APISubject'   => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                'APIVersion'   => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
            );

            if ($config['Sandbox']) {
                error_reporting(E_ALL);
                ini_set('display_errors', '1');
            }
            $this->load->library('paypal/Paypal_pro', $config);

            $customerID = $paydata['customerListID'];

           
            $amount     = $paydata['transactionAmount'];
            $merchantID = $paydata['merchantID'];

            $total = $refAmt;

            if ($amount == $total) {
                $restype = "Full";
            } else {
                $restype = "Partial";
            }
            $amount = $total;
            
            $RTFields = array(
                'transactionid'       => $tID, // Required.  PayPal transaction ID for the order you're refunding.
                'payerid'             => '', // Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
                'invoiceid'           => '', // Your own invoice tracking number.
                'refundtype'          => $restype, // Required.  Type of refund.  Must be Full, Partial, or Other.
                'amt'                 => $amount, // Refund Amt.  Required if refund type is Partial.
                'currencycode'        => '', // Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
                'note'                => '', // Custom memo about the refund.  255 char max.
                'retryuntil'          => '', // Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
                'refundsource'        => '', // Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
                'merchantstoredetail' => '', // Information about the merchant store.
                'refundadvice'        => '', // Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
                'refunditemdetails'   => '', // Details about the individual items to be returned.
                'msgsubid'            => '', // A message ID used for idempotence to uniquely identify a message.
                'storeid'             => '', // ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
                'terminalid'          => '', // ID of the terminal.  50 char max.
            );

            $PayPalRequestData = array('RTFields' => $RTFields);

            $PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);

            if ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {

                $code = '111';

                $c_data = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $customerID));


                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );

                $merchID = $paydata['merchantID'];

                $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
                $rs_data         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
                $redID           = $rs_data['resellerID'];
                $condition1      = array('resellerID' => $redID);
                $sub1            = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                $domain1         = $sub1['merchantPortalURL'];
                $URL1            = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

                $subdomain           = $Freshbooks_data['sub_domain'];
                $key                 = $Freshbooks_data['secretKey'];
                $accessToken         = $Freshbooks_data['accessToken'];
                $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                define('OAUTH_CONSUMER_KEY', $subdomain);
                define('OAUTH_CONSUMER_SECRET', $key);
                define('OAUTH_CALLBACK', $URL1);

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                        $imn_amount     = $ref_inv_amount->inv_amount;
                        $p_ids          = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();
                        foreach ($ref_inv as $k => $r_inv) {

                            if ($r_inv == $ref_invID) {

                                $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                                if (!empty($inv_data)) {
                                    $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                                } else {
                                    $due_date = date('Y-m-d');
                                }

                                $re_amount = $imn_amount[$k];
                                if ($refund > 0 && $imn_amount[$k] > 0) {

                                    $p_refund1 = 0;

                                    $p_id = $p_ids[$k];

                                    $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                    if ($refund <= $imn_amount[$k]) {
                                        $p_refund1 = $refund;

                                        $p_refund = $imn_amount[$k] - $refund;

                                        $re_amount = $imn_amount[$k] - $refund;
                                        $refund    = 0;

                                    } else {
                                        $p_refund  = 0;
                                        $p_refund1 = $imn_amount[$k];
                                        $refund    = $refund - $imn_amount[$k];

                                        $re_amount = 0;
                                    }

                                    if (isset($accessToken) && isset($access_token_secret)) {
                                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                        $payment = '<?xml version="1.0" encoding="utf-8"?>
                    						<request method="payment.update">
                    						  <payment>

                                                    <payment_id>' . $p_id . '</payment_id>
                                                    <amount>' . $p_refund . '</amount>
                                                    <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>

                    						</request>';

                                        $invoices = $c->edit_payment($payment);

                                        if ($invoices['status'] != 'ok') {

                                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                           
                                        } else {
                                            $ins_id    = $p_id;
                                            $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                                'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                                'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                                'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                            );
                                            $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                            $lineArray = '';

                                            foreach ($ittem as $item_val) {

                                                $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                                $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                                $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                                $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                                $lineArray .= '<type>Item</type></line>';

                                            }
                                            $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                            $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                            $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                            $lineArray .= '<quantity>1</quantity>';
                                            $lineArray .= '<type>Item</type></line>';

                                            $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update">
                                     		 <invoice>
                                     		  <invoice_id>' . $r_inv . '</invoice_id>
                                     		  <date>' . $due_date . '</date>
                        				          <lines>' . $lineArray . '</lines>
                        						</invoice>
                        						</request> ';

                                            $invoices1 = $c->add_invoices($invoice);

                                            $this->session->set_flashdata('success', ' Success');

                                           
                                        }

                                    }
                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;

                                } else {

                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;
                                }

                            } else {
                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $imn_amount[$k];
                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
            						<request method="payment.update">
            						  <payment>

                                            <payment_id>' . $p_id . '</payment_id>
                                            <amount>' . $p_amount . '</amount>
                                            <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>

            						</request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                

                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update">
                 		 <invoice>
                 		  <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

    				          <lines>' . $lineArray . '</lines>
    						</invoice>
    						</request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Success');

                            }
                        }
                    }

                }

                $this->fb_customer_model->update_refund_payment($tID, 'PAYPAL');

                $this->session->set_flashdata('success', 'Success');
            } else {
                $responsetext = $PayPalResult['L_LONGMESSAGE0'];
                $code         = '401';
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $responsetext . '</strong>.</div>');
            }
            $transactiondata = array();
            $tranID          = '';
            $amt             = '0.00';

            if (isset($PayPalResult['REFUNDTRANSACTIONID'])) {$tranID = $PayPalResult['REFUNDTRANSACTIONID'];
                $amt                               = $PayPalResult['GROSSREFUNDAMT'];}
            $transactiondata['transactionID']       = $tranID;
            $transactiondata['transactionStatus']   = $PayPalResult['ACK'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = 'Paypal_refund';
            $transactiondata['transactionCode']     = $code;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amt;
            if (!empty($paydata['invoiceID'])) {
                $transactiondata['invoiceID'] = $paydata['invoiceID'];
            }
            $transactiondata['merchantID'] = $this->merchantID;
            $transactiondata['resellerID'] = $this->resellerID;
            $transactiondata['gateway']    = "Paypal";
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

        } else if ($paydata['transactionGateway'] == '5') {
            include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount  = $refAmt;
            $nmiuser = $gt_result['gatewayUsername'];
            $nmipass = $gt_result['gatewayPassword'];

            $plugin = new ChargezoomStripe();
            $plugin->setApiKey($gt_result['gatewayPassword']);

            $charge = \Stripe\Refund::create(array(
                "charge" => $tID,
                "amount" => ($amount * 100),
            ));

            $customerID = $paydata['customerListID'];

            $charge = json_encode($charge);

            $result = json_decode($charge);

            $trID = '';
            if (strtoupper($result->status) == strtoupper('succeeded')) {

                $amount = ($result->amount / 100);
                $code   = '200';
                $trID   = $result->id;

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                        $imn_amount     = $ref_inv_amount->inv_amount;
                        $p_ids          = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();
                        foreach ($ref_inv as $k => $r_inv) {

                            if ($r_inv == $ref_invID) {
                                $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                                if (!empty($inv_data)) {
                                    $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                                } else {
                                    $due_date = date('Y-m-d');
                                }

                                $re_amount = $imn_amount[$k];
                                if ($refund > 0 && $imn_amount[$k] > 0) {

                                    $p_refund1 = 0;

                                    $p_id = $p_ids[$k];

                                    $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                    if ($refund <= $imn_amount[$k]) {
                                        $p_refund1 = $refund;

                                        $p_refund = $imn_amount[$k] - $refund;

                                        $re_amount = $imn_amount[$k] - $refund;
                                        $refund    = 0;

                                    } else {
                                        $p_refund  = 0;
                                        $p_refund1 = $imn_amount[$k];
                                        $refund    = $refund - $imn_amount[$k];

                                        $re_amount = 0;
                                    }

                                    if (isset($accessToken) && isset($access_token_secret)) {
                                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                        $payment = '<?xml version="1.0" encoding="utf-8"?>
                    						<request method="payment.update">
                    						  <payment>

                                                    <payment_id>' . $p_id . '</payment_id>
                                                    <amount>' . $p_refund . '</amount>
                                                    <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>

                    						</request>';

                                        $invoices = $c->edit_payment($payment);

                                        if ($invoices['status'] != 'ok') {

                                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');


                                        } else {
                                            $ins_id    = $p_id;
                                            $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                                'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                                'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                                'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                            );
                                            $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                            $lineArray = '';

                                            foreach ($ittem as $item_val) {

                                                $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                                $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                                $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                                $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                                $lineArray .= '<type>Item</type></line>';

                                            }
                                            $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                            $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                            $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                            $lineArray .= '<quantity>1</quantity>';
                                            $lineArray .= '<type>Item</type></line>';

                                            $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update">
                                     		 <invoice>
                                     		  <invoice_id>' . $r_inv . '</invoice_id>
                                     		   <date>' . $due_date . '</date>
                        				          <lines>' . $lineArray . '</lines>
                        						</invoice>
                        						</request> ';

                                            $invoices1 = $c->add_invoices($invoice);

                                            $this->session->set_flashdata('success', 'Success');

                                        }

                                    }

                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;

                                } else {

                                    $inv_array['inv_no'][]     = $r_inv;
                                    $inv_array['inv_amount'][] = $re_amount;
                                }
                            } else {

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $imn_amount[$k];
                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
            						<request method="payment.update">
            						  <payment>

                                            <payment_id>' . $p_id . '</payment_id>
                                            <amount>' . $p_amount . '</amount>
                                            <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>

            						</request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');


                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update">
                 		 <invoice>
                 		  <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

    				          <lines>' . $lineArray . '</lines>
    						</invoice>
    						</request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Success');

                            }
                        }
                    }

                }

                $this->fb_customer_model->update_refund_payment($tID, 'STRIPE');

                $this->session->set_flashdata('success', 'Success');
            } else {
                $code = $result->failure_code;
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
            }

            $ttype = 'stripe_refund';

            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result->id;
            $transactiondata['transactionStatus']   = $result->status;
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = $ttype;
            $transactiondata['transactionCode']     = $code;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $this->merchantID;
            if (!empty($paydata['invoiceID'])) {
                $transactiondata['invoiceID'] = $paydata['invoiceID'];
            }
            $transactiondata['resellerID'] = $this->resellerID;
            $transactiondata['gateway']    = "Stripe";
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

		} else if ($paydata['transactionGateway'] == '6') {
			require_once APPPATH . "third_party/usaepay/usaepay.php";
			$gt_result                        = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			$amount                           = $refAmt;
			$payusername                      = $gt_result['gatewayUsername'];
			$paypassword                      = $gt_result['gatewayPassword'];
			$inID                             = '';
			$customerID                       = $paydata['customerListID'];
			$inID                             = $paydata['invoiceTxnID'];
			$crtxnID                          = '';
			$transaction                      = new umTransaction;
			$transaction->ignoresslcerterrors = ($this->config->item('ignoresslcerterrors') !== null) ? $this->config->item('ignoresslcerterrors') : true;

			$transaction->key        = $payusername; // Your Source Key
			$transaction->pin        = $paypassword; // Source Key Pin
			$transaction->usesandbox = $this->config->item('Sandbox'); // Sandbox true/false
			$transaction->testmode   = $this->config->item('TESTMODE'); // Change this to 0 for the transaction to process
			$transaction->command    = "refund"; // refund command to refund transaction.
			$transaction->refnum     = $tID; // Specify refnum of the transaction that you would like to capture.
			$transaction->amount     = $amount;
			$customerID              = $paydata['customerListID'];
			// $amount  =  $paydata['transactionAmount'];

			$transaction->Process();

			$trID1 = '';
			if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') {

				$msg     = $transaction->result;
				$trID1   = $transaction->refnum;
				$codests = 'SUCCESS';
				$res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID1);
				$this->customer_model->update_refund($trID, 'USAEPAY');

				$ins_id    = '';
				$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
					'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
					'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
					'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),

				);
				$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
				$this->session->set_flashdata('success', 'Transaction Successfully Refunded');

			} else {
				$trID1 = '';
				$msg   = $transaction->result;
				$trID1 = $transaction->refnum;

				$res = array('transactionCode' => '300', 'status' => $msg, 'transactionId' => $trID1);
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>');

			}


			$id = $this->general_model->insert_gateway_transaction_data($res, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $refAmt, $this->merchantID, $crtxnID = '', $this->resellerID, $inID, false, $this->transactionByUser);
		} else if ($paydata['transactionGateway'] == '7') {

			require_once dirname(__FILE__) . '/../../vendor/autoload.php';

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$amount = $refAmt;
			$secretApiKey = $gt_result['gatewayPassword'];
			$inID         = '';
			$customerID   = $paydata['customerListID'];
			$inID         = $paydata['invoiceTxnID'];
			$crtxnID      = '';

			$config = new PorticoConfig();

			$config->secretApiKey = $secretApiKey;
			$config->serviceUrl   = $this->config->item('GLOBAL_URL');

			ServicesContainer::configureService($config);

			try
			{

				$response = Transaction::fromId($tID)

					->refund($amount)
					->withCurrency("USD")
					->execute();
				$tr1ID = '';
				if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
					$msg     = $response->responseMessage;
					$tr1ID   = $response->transactionId;
					$codests = 'SUCCESS';
					$res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $tr1ID);
					$this->customer_model->update_refund($trID, 'GLOBAL');

					$ins_id    = '';
					$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
						'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
						'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
						'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),
					);
					$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
					$this->session->set_flashdata('success', 'Transaction Successfully Refunded');


				} else {
					$msg   = $response->responseMessage;
					$tr1ID = $response->transactionId;
					$res   = array('trnsactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $tr1ID);
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>');

				}


				$id = $this->general_model->insert_gateway_transaction_data($res, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $refAmt, $this->merchantID, $crtxnID = '', $this->resellerID, $inID, false, $this->transactionByUser);

			} catch (BuilderException $e) {
				$error = 'Build Exception Failure: ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (ConfigurationException $e) {
				$error = 'ConfigurationException Failure: ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (GatewayException $e) {
				$error = 'GatewayException Failure: ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (UnsupportedTransactionException $e) {
				$error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (ApiException $e) {
				$error = ' ApiException Failure: ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			}

		} else if ($paydata['transactionGateway'] == '8') {

			$this->load->config('cyber_pay');

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$amount = $refAmt;

			$option['merchantID'] = trim($gt_result['gatewayUsername']);
			$option['apiKey']     = trim($gt_result['gatewayPassword']);
			$option['secretKey']  = trim($gt_result['gatewaySignature']);

			if ($this->config->item('Sandbox')) {
				$env = $this->config->item('SandboxENV');
			} else {
				$env = $this->config->item('ProductionENV');
			}

			$option['runENV'] = $env;

			$commonElement = new CyberSource\ExternalConfiguration($option);

			$config = $commonElement->ConnectionHost();

			$merchantConfig = $commonElement->merchantConfigObject();
			$apiclient      = new CyberSource\ApiClient($config, $merchantConfig);
			$api_instance   = new CyberSource\Api\RefundApi($apiclient);

			$cliRefInfoArr = [
				"code" => "Refund Payment",
			];
			$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
			$amountDetailsArr             = [
				"totalAmount" => $amount,
				"currency"    => CURRENCY,
			];
			$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

			$orderInfoArry = [
				"amountDetails" => $amountDetInfo,
			];

			$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
			$paymentRequestArr = [
				"clientReferenceInformation" => $client_reference_information,
				"orderInformation"           => $order_information,
			];

			$paymentRequest = new CyberSource\Model\RefundPaymentRequest($paymentRequestArr);

			$inID       = '';
			$customerID = $paydata['customerListID'];
			$inID       = $paydata['invoiceTxnID'];
			$crtxnID    = '';

			$trID1        = '';
			$api_response = list($response, $statusCode, $httpHeader) = null;
			try
			{
				$api_response = $api_instance->refundPayment($paymentRequest, $tID);
				if ($api_response[0]['status'] != "DECLINED" && $api_response[1] == '201') {
					$codests = 'SUCCESS';
					$trID1   = $api_response[0]['id'];
					$msg     = $api_response[0]['status'];

					$code = '200';
					$res  = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID1);
					$this->customer_model->update_refund($trID, 'CYBER');

					$ins_id    = '';
					$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
						'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
						'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
						'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),
					);
					$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
					$this->session->set_flashdata('success', 'Transaction Successfully Refunded');
				} else {
					$trID1 = $api_response[0]['id'];
					$msg   = $api_response[0]['status'];
					$code  = $api_response[1];
					$res   = array('transactionCode' => $code, 'status' => $msg, 'transactionId' => $trID1);

					$error = $api_response[0]['status'];
					$this->session->set_flashdata('success', 'Payment ' . $error);

				}

				$id = $this->general_model->insert_gateway_transaction_data($res, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $refAmt, $this->merchantID, $crtxnID = '', $this->resellerID, $inID, false, $this->transactionByUser);

			} catch (Cybersource\ApiException $e) {

				$error = $e->getMessage();
			}

		} else if ($paydata['transactionGateway'] == '10') {
			$gatlistval = $paydata['gatewayID'];
			$tID        = $paydata['transactionID'];
			$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$apiUsername  = $gt_result['gatewayUsername'];
			$apiKey  = $gt_result['gatewayPassword'];

			$merchantID = $paydata['merchantID'];

			$card       = $paydata['transactionCard'];
			$customerID = $paydata['customerListID'];
			$amount     = $paydata['transactionAmount'];
			$amount     = $refAmt;

			$tr_type     = 'refund';
			
			$payload = [
				'amount' => ($amount * 100)
			];
			$sdk = new iTTransaction();
			
			$result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, $tID);

			$res = $result;

			if ($result['status_code'] == '200' || $result['status_code'] == '201') {
				$result['status_code'] = '200';

				$codests = 'SUCCESS';

				$this->customer_model->update_refund($trID, 'AUTH');
				$ins_id    = '';
				$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
					'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
					'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
					'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),

				);
				$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
				$this->session->set_flashdata('success', 'Success');
			} else {
				$err_msg = $result['status'] = $result['error']['message'];
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $err_msg . '</strong>.</div>');
			}

			$transactiondata                       = array();
			$transactiondata['transactionID']       = (isset($result['id'])) ? $result['id'] : '';
			$transactiondata['transactionStatus']   = $result['status'];
			$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
			$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
			$transactiondata['transactionType']     = $tr_type;
			$transactiondata['transactionCode']     = $result['status_code'];
			$transactiondata['gatewayID']          = $gatlistval;
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['customerListID']    = $customerID;
			$transactiondata['transactionAmount'] = $amount;
			$transactiondata['merchantID']        = $this->merchantID;
			$transactiondata['resellerID']        = $this->resellerID;
			$transactiondata['gateway']           = iTransactGatewayName;
			if (!empty($paydata['invoiceTxnID'])) {
				$transactiondata['invoiceTxnID'] = $paydata['invoiceTxnID'];
			}
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
			$id = $this->general_model->insert_row('customer_transaction', $transactiondata);

		} else if ($paydata['transactionGateway'] == '11' || $paydata['transactionGateway'] == '13') {
			$gatlistval = $paydata['gatewayID'];
			$tID        = $paydata['transactionID'];
			$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$merchantID = $paydata['merchantID'];

			$card       = $paydata['transactionCard'];
			$customerID = $paydata['customerListID'];
			$amount     = $paydata['transactionAmount'];
			$amount     = $refAmt;

			$tr_type     = 'refund';
			
			$gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $refundAmount = $amount * 100;
            $payload = [
                'amount' => round($refundAmount,2)
            ];
            
            $result = $gatewayTransaction->refundTransaction($tID, $payload);
            $res = $result;


            if ($result['status'] == 'success') {
                $pay_status = "SUCCESS";

				$codests = 'SUCCESS';

                $isEcheck = false;
                if (strpos($paydata['gateway'], 'ECheck') !== false) {
                    $isEcheck = true;
                }

				$this->customer_model->update_refund($trID, 'AUTH');
				$ins_id    = '';
				$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
					'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
					'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
					'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),

				);
				$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                $this->session->set_flashdata('success', 'Success');
                
                $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], $isEcheck, $this->transactionByUser);
			} else {
				$err_msg = $result['status'] = $result['error']['message'];
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $err_msg . '</strong>.</div>');
			}
		} else if ($paydata['transactionGateway'] == '12') {
            $gatlistval = $paydata['gatewayID'];
            $tID        = $paydata['transactionID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $merchantID = $paydata['merchantID'];

            $card       = $paydata['transactionCard'];
            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $amount     = $refAmt;

            $tr_type     = 'refund';
            
            $deviceID = $gt_result['gatewayMerchantID'].'01';    

            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:'.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;

            $payload = [
                'amount' => ($amount * 100)
            ];
            $result = $gatewayTransaction->refundTransaction($tID, $payload);
            $res = $result;
            $responseType = 'ReturnResponse';
            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
            {
                $pay_status = "SUCCESS";

                $codests = 'SUCCESS';

                $isEcheck = false;
                

                $this->customer_model->update_refund($trID, 'AUTH');
                $ins_id    = '';
                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
                    'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),

                );
                $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                $this->session->set_flashdata('success', 'Success');
                $result['responseType'] = $responseType;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], $isEcheck, $this->transactionByUser);
            } else {
                $err_msg = $result[$responseType]['responseMessage'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $err_msg . '</strong>.</div>');
            }

        } else if($paydata['transactionGateway'] == '15')
        {

            // PayArc
            $gatlistval = $paydata['gatewayID'];
			$tID        = $paydata['transactionID'];
			$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$merchantID = $paydata['merchantID'];

			$card       = $paydata['transactionCard'];
			$customerID = $paydata['customerListID'];
			$amount     = $paydata['transactionAmount'];
			$amount     = $refAmt;

			$tr_type     = 'refund';

            $this->payarcgateway->setApiMode($this->config->item('environment'));
            $this->payarcgateway->setSecretKey($gt_result['gatewayUsername']);

            $charge_response = $this->payarcgateway->refundCharge($tID, ($amount * 100));

            $result = json_decode($charge_response['response_body'], 1);
            
            $res = $result;


            if (isset($result['data']) && $result['data']['status'] == 'refunded') {

                $pay_status = "SUCCESS";

				$codests = 'SUCCESS';

                $isEcheck = false;
                if (strpos($paydata['gateway'], 'ECheck') !== false) {
                    $isEcheck = true;
                }

				$this->customer_model->update_refund($trID, 'AUTH');
				$ins_id    = '';
				$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
					'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
					'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
					'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),

				);
				$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                $this->session->set_flashdata('success', 'Success');
                
                $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], $isEcheck, $this->transactionByUser);
			} else {
				$err_msg = $result['message'];
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $err_msg . '</strong>.</div>');
			}
        }
        else if($paydata['transactionGateway'] == '17')
        {
            // Maverick Payment Begins
            $gatlistval = $paydata['gatewayID'];
			$tID        = $paydata['transactionID'];
			$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$merchantID = $paydata['merchantID'];

			$card       = $paydata['transactionCard'];
			$customerID = $paydata['customerListID'];
			$amount     = $paydata['transactionAmount'];
            $amount     = $refAmt;
            
            // Maverick Payment Gateway
            $this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
            $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
            $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

            $isEcheck = false;
            if($isEcheck) {
                $r = $this->maverickgateway->refundAchSale($tID, $amount);
            } else {
                $r = $this->maverickgateway->refundSale($tID, $amount);
            }

            $rbody = json_decode($r['response_body'], 1);
            
            $result = [];
            $result['data'] = $rbody;
            
            $result['response_code'] = $r['response_code'];

            if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                if($rbody['status']['status'] == 'Approved'){
                    $result['status'] = 'success';
                    $result['msg'] = $result['message'] = 'Successfully Refunded Payment.';
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = 'Payment refund failed.';    
                }
            } else {
                $result['status'] = 'failed';
                $result['msg'] = $result['message'] = $rbody['message'];
            }

            if ($result['status'] == 'success') {
                $pay_status = "SUCCESS";

				$codests = 'SUCCESS';

                $isEcheck = false;
                if (strpos($paydata['gateway'], 'ECheck') !== false) {
                    $isEcheck = true;
                }

				$this->customer_model->update_refund($trID, 'AUTH');
				$ins_id    = '';
				$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $refAmt,
					'creditInvoiceID'               => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
					'creditTxnID'                   => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
					'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),

				);
				$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                $this->session->set_flashdata('success', 'Success');
                
                $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], $isEcheck, $this->transactionByUser);
			} else {
				$err_msg = $result['status'] = $result['error']['message'];
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $err_msg . '</strong>.</div>');
			}
            // Maverick Payment Ends
        }

		if (!empty($paydata['invoiceTxnID']) && $codests == 'SUCCESS') {
			$refund  = $amount;
			$ref_inv = explode(',', $paydata['invoiceID']);

			if (count($ref_inv) > 1) {
				$ref_inv_amount = json_decode($paydata['invoiceRefID']);

				$imn_amount = $ref_inv_amount->inv_amount;
				$p_ids      = explode(',', $paydata['qbListTxnID']);

				$inv_array = array();

				foreach ($ref_inv as $k => $r_inv) {

					if ($r_inv == $ref_invID) {

						$inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

						if (!empty($inv_data)) {
							$due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
						} else {
							$due_date = date('Y-m-d');
						}

						$re_amount = $imn_amount[$k];

						if ($refund > 0 && $imn_amount[$k] > 0) {

							$p_refund1 = 0;

							$p_id = $p_ids[$k];

							$ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

							if ($refund <= $imn_amount[$k]) {
								$p_refund1 = $refund;

								$p_refund = $imn_amount[$k] - $refund;

								$re_amount = $imn_amount[$k] - $refund;
								$refund    = 0;

							} else {
								$p_refund  = 0;
								$p_refund1 = $imn_amount[$k];
								$refund    = $refund - $imn_amount[$k];

								$re_amount = 0;
							}

							if (isset($accessToken) && isset($access_token_secret)) {
								$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

								$payment = '<?xml version="1.0" encoding="utf-8"?>
									<request method="payment.update">
										<payment>

											<payment_id>' . $p_id . '</payment_id>
											<amount>' . $p_refund . '</amount>
											<notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
											<currency_code>USD</currency_code>
											</payment>

									</request>';

								$invoices = $c->edit_payment($payment);

								if ($invoices['status'] != 'ok') {

									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

								

								} else {
									$ins_id    = $p_id;
									$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
										'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
										'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
										'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
									);
									$ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
									$lineArray = '';

									foreach ($ittem as $item_val) {

										$lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
										$lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
										$lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
										$lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
										$lineArray .= '<type>Item</type></line>';

									}
									$lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
									$lineArray .= '<description>This is used to refund Incoive payment</description>';
									$lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
									$lineArray .= '<quantity>1</quantity>';
									$lineArray .= '<type>Item</type></line>';

									$invoice = '<?xml version="1.0" encoding="utf-8"?>
									<request method="invoice.update">
										<invoice>
										<invoice_id>' . $r_inv . '</invoice_id>
										<date>' . $due_date . '</date>
											<lines>' . $lineArray . '</lines>
										</invoice>
										</request> ';

									$invoices1 = $c->add_invoices($invoice);

									$this->session->set_flashdata('success', 'Success');

								
								}

							}

							$inv_array['inv_no'][]     = $r_inv;
							$inv_array['inv_amount'][] = $re_amount;

						} else {

							$inv_array['inv_no'][]     = $r_inv;
							$inv_array['inv_amount'][] = $re_amount;
						}

					} else {

						$inv_array['inv_no'][]     = $r_inv;
						$inv_array['inv_amount'][] = $imn_amount[$k];

					}

				}
				$this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
			} else {

				$ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

				$p_id     = $paydata['qbListTxnID'];
				$p_amount = $paydata['transactionAmount'] - $amount;

				if (isset($accessToken) && isset($access_token_secret)) {
					$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

					$payment = '<?xml version="1.0" encoding="utf-8"?>
							<request method="payment.update">
								<payment>

									<payment_id>' . $p_id . '</payment_id>
									<amount>' . $p_amount . '</amount>
									<notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
									<currency_code>USD</currency_code>
									</payment>

							</request>';

					$invoices = $c->edit_payment($payment);

					if ($invoices['status'] != 'ok') {

						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

					

					} else {
						$ins_id    = $p_id;
						$refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
							'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
							'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
							'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
						);
						$ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
						$lineArray = '';

						foreach ($ittem as $item_val) {

							$lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
							$lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
							$lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
							$lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
							$lineArray .= '<type>Item</type></line>';

						}
						$lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
						$lineArray .= '<description>This is used to refund Incoive payment</description>';
						$lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
						$lineArray .= '<quantity>1</quantity>';
						$lineArray .= '<type>Item</type></line>';

						$invoice = '<?xml version="1.0" encoding="utf-8"?>
				<request method="invoice.update">
					<invoice>
					<invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

						<lines>' . $lineArray . '</lines>
					</invoice>
					</request> ';

						$invoices1 = $c->add_invoices($invoice);

						$this->session->set_flashdata('success', 'Success');

						
					}

                    $receipt_data = array(
                        'proccess_url' => 'FreshBooks_controllers/Transactions/payment_refund',
                        'proccess_btn_text' => 'Process New Refund',
                        'sub_header' => 'Refund',
                    );
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);
                    $this->session->set_userdata("invoice_IDs",$paydata['invoiceTxnID']);
                    if($paydata['invoiceTxnID'] != ''){
                        redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$customerID.'/'.$tID,  'refresh');
                    }else{
                        redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$customerID.'/'.$tID,  'refresh');
                    }
                    
                    
				}
			}

		}

        redirect('FreshBooks_controllers/Transactions/refund_transaction');

    }

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

}
