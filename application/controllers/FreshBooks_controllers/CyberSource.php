<?php

/**
 * This Controller has Stripe Payment Gateway Process
 * 
 * Create_customer_sale for Sale process : here we use the payment token
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */
error_reporting(0);
error_reporting(E_ERROR | E_PARSE);

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;
class CyberSource extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
   	public function __construct()
	{
		parent::__construct();
		
		
        $this->load->config('cyber_pay');
    	$this->load->model('quickbooks');
         $this->load->model('customer_model');
		$this->load->model('card_model');
	
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
       $this->db1 = $this->load->database('otherdb', true); 
  if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='3' )
		  {

            $logged_in_data = $this->session->userdata('logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		    $this->merchantID = $logged_in_data['merchID'];
            $this->resellerID = $logged_in_data['resellerID'];
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    
            $logged_in_data = $this->session->userdata('user_logged_in');     
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];  
		    $this->merchantID = $merchID;
		  }
		  else
		  {
			redirect('login','refresh');
		  }
		  
		  
		   $val = array(
             'merchantID' => $this->merchantID,
             );
		  	$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		  	if(empty($Freshbooks_data))
		  	{
		  	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>'); 
                
		  	  redirect('FreshBoolks_controllers/home/index','refresh');
		  	  
		  	}
	}
	
	
	
	public function index()
	{
	    
    	   	redirect('FreshBooks_controllers/home','refresh'); 

	}
	
	
   	public function pay_invoice()
	{

		 $this->session->unset_userdata("receipt_data");
		 $this->session->unset_userdata("invoice_IDs");
		 $this->session->unset_userdata("in_data");
	      if($this->session->userdata('logged_in'))
	      {
			    $user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
		
			    $user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('sch_gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
               
                $this->form_validation->set_rules('contact', 'Phone Number', 'trim|required|xss-clean');
            }
            
                 $cusproID=''; $error='';
		    	 $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	
			$checkPlan = check_free_plan_transactions();
              
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		       
		          $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			    
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			     $in_data              =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			    if(!empty( $in_data)) 
			    {
			          
    			        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			        $customerID =  $in_data['Customer_ListID'];
    				  	$c_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID', 'firstName', 'lastName',  'companyName','userEmail', 'phoneNumber','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
    			     	$companyID = $c_data['companyID'];
    			        $gmerchantID    = $gt_result['gatewayUsername'];
    			        $secretApiKey   = $gt_result['gatewayPassword'];
						$secretKey      = $gt_result['gatewaySignature'];
    			     
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			       
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					$phone    = ($card_data['Billing_Contact'])?$card_data['Billing_Contact']:$c_data['phoneNumber'];
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }                              
         
             	
    				if( $in_data['BalanceRemaining'] > 0)
    				{
        				    
                    	$crtxnID='';  
                        $flag="true";
                        $error=$user='';
                        $user = $in_data['qbwc_username'];
        				$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "test_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $c_data['firstName'],
                    		"lastName" => $c_data['lastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => $phone,
                    		"company" => $c_data['companyName'],
                    		"email" => $c_data['userEmail']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                    	
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    	
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                    	
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  
        					  
        					  	$st='0'; $action='Pay Invoice'; $msg ="Payment Success "; 
						 $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
						 
						 $this->session->set_flashdata('success', 'Successfully Processed Invoice'); 
                         
                             $txnID      = $in_data['invoiceID'];  
							 $ispaid 	 = 1;
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 0;
							 $app_amount = $in_data['Total_payment']+$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('invoiceID'=>$in_data['invoiceID'],'merchantID'=>$user_id  );
						
							 $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
                          
                       $val = array(
							'merchantID' => $user_id,
						);
						$data = $this->general_model->get_row_data('QBO_token',$val);

						$accessToken  = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];
						$dataService  = DataService::Configure(array(
						 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
						));
		
        			    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        	            
        	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
        				$refNum=array();
        				
        				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        				{
        					$theInvoice = current($targetInvoiceArray);
        			
                				$updatedInvoice = Invoice::update($theInvoice, [
                				    "sparse" => 'true'
                				]);
                				
                				$updatedResult = $dataService->Update($updatedInvoice);
                				
                				$newPaymentObj = Payment::create([
                				    "TotalAmt" => $amount,
                				    "SyncToken" => $updatedResult->SyncToken,
                				    "CustomerRef" => $customerID,
                				    "Line" => [
                				     "LinkedTxn" =>[
                				            "TxnId" => $invoiceID,
                				            "TxnType" => "Invoice",
                				        ],    
                				       "Amount" => $amount
                			        ]
                				    ]);
                             
                				$savedPayment = $dataService->Add($newPaymentObj);
                				
                		    	$crtxnID=	$savedPayment->Id;
                				
                				$error = $dataService->getLastError();
                              
                			    if ($error != null) 
                			    {
                	
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                				$st='';
                
                				$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); 
                				
                				$qbID=$trID;
                				
                			   }
                    		   else 
                    		   {  $refNum[] =  $in_data['refNumber']; 
                    		       
                    		       	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
                    		           
									   $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                    			}
                                      
                          
							}    
							$ref_number=implode(',',$refNum); 
							$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							
							$tr_date   =date('Y-m-d H:i:s');
								$toEmail = $c_data['userEmail']; $company=$c_data['companyName']; $customer = $c_data['fullName'];  
								
                             	if($chh_mail =='1')
        							 {
        							 
        							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
        							 } 
									
							       $qbID=$invoiceID; 
        						
                    			         if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                    				 		
                    				        
											$cardType       = $this->general_model->getType($card_no);
											$friendlyname   =  $cardType.' - '.substr($card_no,-4);
                    						$card_condition = array(
                    										 'customerListID' =>$customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    					
                    						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
                    					     
                    					   
                    						if(!empty($crdata))
                    						{
                    							
                    						   $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	     =>$exyear,
                    										  'companyID'    =>$companyID,
                    										  'merchantID'   => $user_id,
                    										  'CardType'     =>$this->general_model->getType($card_no),
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										  'updatedAt'    => date("Y-m-d H:i:s"),
                    										   'Billing_Addr1'	 =>$address1,
                        										 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										  );
                    										  
                    							     		  
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					     	$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'     =>$this->general_model->getType($card_no),
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        										 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->insert_card_data($card_data);	
                    				            
                    						
                    						}
                    				
                    				 }
                    				 
                    				 
									 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
        					    
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					       
        					      $error = $api_response[0]['status'];
									$st='0'; $action='Pay Invoice'; $msg ="Payment Failed".$error; $qbID=$invoiceID;
									$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$error.'</strong></div>');
        					      
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$invoiceID, false, $this->transactionByUser);  
                                if($error!='')
                                {
                                       $st='0'; $action='Pay Invoice'; $msg ="Payment Failed"; $qbID=$invoiceID;
                                }
                               $qbo_log =array('qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
                  
                                $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    	    
        			}  
        			
                      
        				   
        				           
        		    }
        		    else        
                    {
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                        
                    }
                 
        		   
		    } 
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
			}
			
			 $trans_id = $api_response[0]['id'];
			 $invoice_IDs = array();
			 $receipt_data = array(
				 'proccess_url' => 'FreshBooks_controllers/Create_invoice/Invoice_details',
				 'proccess_btn_text' => 'Process New Invoice',
				 'sub_header' => 'Sale',
				 'checkPlan'	=> $checkPlan
			 );
			 
			 $this->session->set_userdata("receipt_data",$receipt_data);
			 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
			 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		   	
	      
	}
	
	
	 
   
	public function create_customer_sale_old()
	{ 
	      
	    
	        if($this->session->userdata('logged_in'))
		    {
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$merchantID				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	    		  if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			
        			if(!empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				
                        $comp_data     =$this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                           
        				$cvv='';	
        		        if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    
                   
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1   = substr($exyear,2);
                    $expry    = $expmonth.$exyear;  
                    $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
                  
				 }else {
					  
						   
                    $card_data= $this->card_model->get_single_card_data($cardID);
                    
                  
                    $expmonth =  $card_data['cardMonth'];
                    
                    $exyear   = $card_data['cardYear'];
                    $exyear1   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                    $expry    = $expmonth.$exyear;  
                    	$cvv     = $card_data['CardCVV'];
               
					}	
							
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                      $contact=  $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');
                                 	$creditCardType 	= ($card_type)?$card_type:'Visa';
				
					$creditCardNumber 	= $card_no;
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                    
					$expDateYear=  	$exyear;
					$cvv2Number =   $cvv;
						
                            $crtxnID='';  
                            $flag="true";
                            $error=$user='';
                      
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        						
        						
        						
        						
        						
        						
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" =>  $this->session->userdata('logged_in')['companyName']
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        			
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        					
        					
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
                				 /* This block is created for saving Card info in encrypted form  */
            				       
            	 $invoiceIDs=array();      
        				         
            				 $invoiceIDs=array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				     
				         
				        
				             $condition1 = array('resellerID'=>$resellerID);
                
                 	         $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
                 	 
        			        $domain1 = $sub1['merchantPortalURL'];
        			 
        			 $URL1 = $domain1.'FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY',$subdomain);
                                define('OAUTH_CONSUMER_SECRET',$key);
                                define('OAUTH_CALLBACK',$URL1);
				
						
			            $refNumber =array();
                 
				           if(!empty($invoiceIDs))
				           {
				               
				              foreach($invoiceIDs as $inID)
				              {
        				                $theInvoice = array();
        							  
        						       
        							   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        							 
        								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '".$inID."'");
        							
        						
        								    	$con = array('invoiceID'=>$inID,'merchantID'=>$user_id);
            									$res1 = $this->general_model->get_select_data('QBO_test_invoice',array('BalanceRemaining','refNumber', 'Total_payment'), $con);
            									$amount_data = $res1['BalanceRemaining'];
            									$refNumber[]   =  $res1['refNumber'];
            									 $txnID      = $inID;  
            							        $ispaid 	 = 1;
            						
            						     	     $app_amount = $res1['Total_payment']+$amount;
            							        $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>'0.00' );
            						
            							      $this->general_model->update_row_data('QBO_test_invoice',$con, $data);
        							 		
        								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        								{
        									$theInvoice = current($targetInvoiceArray);
        								
            								$updatedInvoice = Invoice::update($theInvoice, [
            								   
            									"sparse" => 'true'
            								]);
            							
            								$updatedResult = $dataService->Update($updatedInvoice);
            								
            								$newPaymentObj = Payment::create([
            									"TotalAmt" => $app_amount,
            									"SyncToken" => $updatedResult->SyncToken,
            									"CustomerRef" => $customerID,
            									"Line" => [
            									 "LinkedTxn" =>[
            											"TxnId" => $inID,
            											"TxnType" => "Invoice",
            										],    
            									   "Amount" => $amount_data
            									]
            									]);
            							 
            								$savedPayment = $dataService->Add($newPaymentObj);
        								
        								
        								
            								$error = $dataService->getLastError();
            								if ($error != null)
            							{
            							    $err='';
            					        	$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
            									$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
            								$err.="The Response message is: " . $error->getResponseBody() . "\n";
            								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>'.	$err.'</div>');
            
            							$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody();
            							
            							$qbID=$trID;	
            								$pinv_id='';
            							}
            							else 
            							{ 
            							      $pinv_id='';
            							       $pinv_id        =  $savedPayment->Id;
            							    	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
											$this->session->set_flashdata('success', 'Transaction Successful');
        					      
            							}
            					          $qbo_log =array('qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				                           $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
            							   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount_data,$merchantID,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser);     		
            							
        						}
				              }
						 
				            }
				          else
				          {
				              
				                
				                    $crtxnID='';
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
				          }
				          
				          
				          
				          	
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
				               
            				       
				     
            				       
            	 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    {	           
                   $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $card_type      =$this->general_model->getType($card_no);
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                    
                    $card_data = array('cardMonth'   =>$expmonth,
                    'cardYear'	     =>$exyear,
                    'CardType'     =>$card_type,
                    'companyID'    =>$companyID,
                    'merchantID'   => $merchantID,
                    'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
                    'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
                    'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
                    'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
                    'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
                    'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
                    'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
                    'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
                    'CustomerCard' =>$card_no,
                    'CardCVV'      =>$cvv, 
                    'updatedAt'    => date("Y-m-d H:i:s") 
                    );
                    
                    
                    
                    $card=	$this->card_model->process_card($card_data) ;  
                    
                    
                    }  
            				   
						
            			$this->session->set_flashdata('success', 'Transaction Successful');
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
                                     
                              }
                         
                         
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
					}		
        				        
                          
        		 
			

                     redirect('FreshBooks_controllers/Transactions/create_customer_sale','refresh'); 
	    
	    
    	}
	
	public function create_customer_sale()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	    
	        if($this->session->userdata('logged_in'))
		    {
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$merchantID				= $this->session->userdata('user_logged_in')['merchantID'];
			}

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

	    		  if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
					
					   $checkPlan = check_free_plan_transactions();
        			
        			if($checkPlan && !empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				
                        $comp_data     =$this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                           
        				$cvv='';	
        		        if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    
                   
                
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1   = substr($exyear,2);
                    $expry    = $expmonth.$exyear;  
                    $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
                 
				 }else {
					  
						   
                    $card_data= $this->card_model->get_single_card_data($cardID);
                    
                  
                    $expmonth =  $card_data['cardMonth'];
                    
                    $exyear   = $card_data['cardYear'];
                    $exyear1   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                    $expry    = $expmonth.$exyear;  
                    	$cvv     = $card_data['CardCVV'];
                 
					}	
							
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                      $contact=  $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');
                                 	$creditCardType 	= ($card_type)?$card_type:'Visa';
				
					$creditCardNumber 	= $card_no;
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                    
					$expDateYear=  	$exyear;
					$cvv2Number =   $cvv;
						
                            $crtxnID='';  
                            $flag="true";
                            $error=$user='';
                        
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        						
        						
        						
        						
        						
        						
        						
        						
        						
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" =>  $this->session->userdata('logged_in')['companyName']
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        			
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];

                        $invoiceDetail = [];

                        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id')) || !empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                            $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 3);
                            $invoiceDetailArr = [
                                'invoiceNumber' => $new_invoice_number,
                                'purchaseOrderNumber' => $this->czsecurity->xssCleanPostInput('po_number')
                            ];
                            $invoiceDetail = new CyberSource\Model\Ptsv2paymentsOrderInformationInvoiceDetails($invoiceDetailArr);
                        }

                        if($invoiceDetail){
                            $orderInfoArr['invoiceDetails'] = $invoiceDetail; 
                        }

        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        					
        					
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];


                             
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				
                				 /* This block is created for saving Card info in encrypted form  */
            				       
            	 $invoiceIDs=array();      
        				         
            				 $invoiceIDs=array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				     
				         
				        
				             $condition1 = array('resellerID'=>$resellerID);
                
                 	         $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
                 	 
        			        $domain1 = $sub1['merchantPortalURL'];
        			 
        			 $URL1 = $domain1.'FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY',$subdomain);
                                define('OAUTH_CONSUMER_SECRET',$key);
                                define('OAUTH_CALLBACK',$URL1);
				
						
			            $refNumber =array();
                 
				           if(!empty($invoiceIDs))
				           {
				               
								include APPPATH . 'libraries/Freshbooks_data.php';
								$fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
								
								foreach ($invoiceIDs as $inID) {

									$in_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invoiceID' => $inID));
									$refNumber[] = 	$in_data['refNumber'];
									$amount_data  = $amount;
									$pinv_id = '';
									$invoices =  $fb_data->create_invoice_payment($inID, $amount_data);
									if ($invoices['status'] != 'ok') {
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
									} else {
										$pinv_id =  $invoices['payment_id'];

										$this->session->set_flashdata('success', 'Transaction Successful');
									}

									$id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount_data,$merchantID,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);
								}
						 
				            }
				          else
				          {
				              
				                
				                    $crtxnID='';
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
				          }
				          
				          
				          
						  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
						  if(!empty($refNumber))
						  $ref_number = implode(',',$refNumber); 
						  else
						  $ref_number = '';
						  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 
				          if($chh_mail =='1')
							 {
							   
							   
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
							
            				       
				     
            				       
            	 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    {	           
                   $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $card_type      =$this->general_model->getType($card_no);
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                    
                    $card_data = array('cardMonth'   =>$expmonth,
                    'cardYear'	     =>$exyear,
                    'CardType'     =>$card_type,
                    'companyID'    =>$companyID,
                    'merchantID'   => $merchantID,
                    'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
                    'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
                    'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
                    'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
                    'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
                    'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
                    'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
                    'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
                    'CustomerCard' =>$card_no,
                    'CardCVV'      =>$cvv, 
                    'updatedAt'    => date("Y-m-d H:i:s") 
                    );
                    
                    
                    
                    $card=	$this->card_model->process_card($card_data) ;  
                   
                    }  
            				    
							   $this->session->set_flashdata('success', 'Transaction Successful');
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                                     
                              }
                         
                         
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
					}		
        			$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => $api_response[0]['id'],
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'	=> $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');	        
                          
        	
	    
	    
    	}
	
	
	public function create_customer_auth()
	{ 
	   
	      
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	        if($this->session->userdata('logged_in'))
		    {
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	    			
		
           
               
	    	
	   
                       if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			$checkPlan = check_free_plan_transactions();
        			   
        			  
        			if($checkPlan && !empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
                       $comp_data     =$this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
        				$companyID  = $comp_data['companyID'];
        		           $cardID = $this->czsecurity->xssCleanPostInput('card_list');
        				   $cvv='';	
						   if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    
                   
              
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1   = substr($exyear,2);
                    $expry    = $expmonth.$exyear;  
                 
				 }else {
					  
						   
                    $card_data= $this->card_model->get_single_card_data($cardID);
                    
                  
                    $expmonth =  $card_data['cardMonth'];
                    
                    $exyear   = $card_data['cardYear'];
                    $exyear1   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                    $expry    = $expmonth.$exyear;  
                  
					}	
							
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								$address1 =  $this->czsecurity->xssCleanPostInput('address');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                        $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');

						
                        $crtxnID='';  
                        $flag='false';
                        $error=$user='';
                       
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" => "test_payment"
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Auth'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        				
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					    $trID =   $api_response[0]['id'];
        					    $msg  =   $api_response[0]['status'];
        					  
        					  
        					  $condition1 = array('resellerID'=>$resellerID);
                
                 	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
                 	 
        			 $domain1 = $sub1['merchantPortalURL'];
        			 
        			 $URL1 = $domain1.'FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY',$subdomain);
                                define('OAUTH_CONSUMER_SECRET',$key);
                                define('OAUTH_CALLBACK',$URL1);
				
        					  
        					  
        					  
        					  
        					  
        					    $code =   '200';
        					    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
								$transactiondata= array();
								$inID='';
							 
							 
            				       
            	if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    {	           
                   $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $card_type      =$this->general_model->getType($card_no);
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                    
                    $card_data = array('cardMonth'   =>$expmonth,
                    'cardYear'	     =>$exyear,
                    'CardType'     =>$card_type,
                    'companyID'    =>$companyID,
                    'merchantID'   => $merchantID,
                    'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
                    'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
                    'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
                    'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
                    'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
                    'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
                    'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
                    'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
                    'CustomerCard' =>$card_no,
                    'CardCVV'      =>$cvv, 
                    'updatedAt'    => date("Y-m-d H:i:s") 
                    );
                    
                    
                    
                    $card=	$this->card_model->process_card($card_data) ;  
                    
                    
                    }  
            				 
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 }   
							  
            			 
							   $this->session->set_flashdata('success', 'Transaction Successful');
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                       
                              }
                        
                             $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser); 
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        				
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
					}		
        			$invoice_IDs = array();
					
				
					$receipt_data = array(
						'transaction_id' => $api_response[0]['id'],
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_auth',
						'proccess_btn_text' => 'Process New Transaction',
						'sub_header' => 'Authorize',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');	        
                          
        		 
	       
	    
    	}
	
	
		
    		
	public function create_customer_capture_old()
	{
		if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('strtxnID', 'Transaction ID', 'required|xss_clean');
			
             
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
                	$comp_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
             	
				 
    			            	$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                            $config    = $commonElement->ConnectionHost();
                        	$merchantConfig = $commonElement->merchantConfigObject();
                        	$apiclient    = new CyberSource\ApiClient($config, $merchantConfig);
                        	$api_instance = new CyberSource\Api\CaptureApi($apiclient);
                                
                                 $cliRefInfoArr = [
                    	    	"code" => $this->mName
                    	        ];
				 
                    		   $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                  $amountDetailsArr = [
                                      "totalAmount" => $amount,
                                      "currency" => CURRENCY
                                  ];
                                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                                
                                  $orderInfoArry = [
                                    "amountDetails" => $amountDetInfo
                                  ];
                                
                                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                                  $requestArr = [
                                    "clientReferenceInformation" => $client_reference_information,
                                    "orderInformation" => $order_information
                                  ];
                                  //Creating model
                                  $request = new CyberSource\Model\CapturePaymentRequest($requestArr);
                                  $api_response = list($response,$statusCode,$httpHeader)=null;
                 
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->capturePayment($request, $tID);  
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  	$condition = array('transactionID'=>$tID);
            					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
            					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
        					  	if($chh_mail =='1')
                                {
                                    $condition = array('transactionID'=>$tID);
                                 
                                 
                                    $tr_date   =date('Y-m-d H:i:s');
                                    $ref_number =  $tID;
                                    $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                    $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                                
                                }
                    			
							   $this->session->set_flashdata('success', 'Successfully Captured Transaction');
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
								 
								  
								  $this->session->set_flashdata('success', 'Payment '.$error);
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                         
                         
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        				
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
                  
				 
					redirect('Transactions/payment_capture','refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('QBO_controllers/Transactions/payment_capture','refresh');
			
	}
		public function create_customer_capture()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
				
		
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $gateway = $paydata['gatewayID'];
				
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
                	$comp_data     = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
             	
				 
    			            	$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                            $config    = $commonElement->ConnectionHost();
                        	$merchantConfig = $commonElement->merchantConfigObject();
                        	$apiclient    = new CyberSource\ApiClient($config, $merchantConfig);
                        	$api_instance = new CyberSource\Api\CaptureApi($apiclient);
                                
                                 $cliRefInfoArr = [
                    	    	"code" => $this->mName
                    	        ];
				 
                    		   $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                  $amountDetailsArr = [
                                      "totalAmount" => $amount,
                                      "currency" => CURRENCY
                                  ];
                                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                                
                                  $orderInfoArry = [
                                    "amountDetails" => $amountDetInfo
                                  ];
                                
                                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                                  $requestArr = [
                                    "clientReferenceInformation" => $client_reference_information,
                                    "orderInformation" => $order_information
                                  ];
                                  //Creating model
                                  $request = new CyberSource\Model\CapturePaymentRequest($requestArr);
                                  $api_response = list($response,$statusCode,$httpHeader)=null;
                 
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->capturePayment($request, $tID);  
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  	$condition = array('transactionID'=>$tID);
            					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
								$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
								$condition = array('transactionID'=>$tID);
                                
								  $tr_date   =date('Y-m-d H:i:s');
								  $ref_number =  $tID;
								  $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 
        					  	if($chh_mail =='1')
                                {
                                    
                                    $this->general_model->send_mail_voidcapture_data($user_id,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                                
                                }
								
									 $this->session->set_flashdata('success', 'Successfully Captured Transaction');
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
								  $error = $api_response[0]['status'];
								  
								  $this->session->set_flashdata('success', 'Payment '.$error);
        					      
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                         
                         
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
                  
				  
				  $invoice_IDs = array();
				 
			  
				  $receipt_data = array(
					  'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
					  'proccess_btn_text' => 'Process New Transaction',
					  'sub_header' => 'Capture',
				  );
				  
				  $this->session->set_userdata("receipt_data",$receipt_data);
				  $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				  
				  if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($api_response[0]['id'] == ''){
						$api_response[0]['id'] ='null';
					}
				  redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$api_response[0]['id'],  'refresh');
					
		   
			
		        
		   
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
			
	}
	
	
	
		public function create_customer_void()
	{
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
		
					
    			   $tID     = $this->czsecurity->xssCleanPostInput('cybervoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $gateway = $paydata['gatewayID'];
				
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
                	$comp_data     = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
             	
    			            	$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                            $config    = $commonElement->ConnectionHost();
                        	$merchantConfig = $commonElement->merchantConfigObject();
                        	$apiclient    = new CyberSource\ApiClient($config, $merchantConfig);
                        	$api_instance = new CyberSource\Api\CaptureApi($apiclient);
                                
                                 $cliRefInfoArr = [
                    	    	"code" => $this->mName
                    	        ];
				 
                    		   $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                  $amountDetailsArr = [
                                      "totalAmount" => $amount,
                                      "currency" => CURRENCY
                                  ];
                                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                                
                                  $orderInfoArry = [
                                    "amountDetails" => $amountDetInfo
                                  ];
                                
                                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                                  $requestArr = [
                                    "clientReferenceInformation" => $client_reference_information,
                                    "orderInformation" => $order_information
                                  ];
                                  //Creating model
                                  $request = new CyberSource\Model\CapturePaymentRequest($requestArr);
                                  $api_response = list($response,$statusCode,$httpHeader)=null;
                 
                              try {
                                        $api_response = $api_instance->voidPayment($paymentRequest, $tID);
                                                                	
                            	
                            	
                                                
                                    if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
                					{
                					  $trID =   $api_response[0]['id'];
                					  $msg  =   $api_response[0]['status'];
                					  
                					  $code =   '200';
                					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                					  	$condition = array('transactionID'=>$tID);
                    					$update_data =   array('transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s'));
        					            $this->general_model->update_row_data('customer_transaction',$condition, $update_data);
                					   if($chh_mail =='1')
                                {
                                    $condition = array('transactionID'=>$tID);
                                 
                                  
                                    $tr_date   =date('Y-m-d H:i:s');
                                    $ref_number =  $tID;
                                    $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                    $this->general_model->send_mail_voidcapture_data($user_id,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                                
                                }
										
											 
									 $this->session->set_flashdata('success', 'Successfully void Transaction');
                					    
                					}
                					else
                					{
                					     $trID  =   $api_response[0]['id'];
                					      $msg  =   $api_response[0]['status'];
                					      $code =   $api_response[1];
                					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
                					      
                					      $error = $api_response[0]['status'];
										  
										  $this->session->set_flashdata('success', 'Payment '.$error);
                					}
                					
            			        	$id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                         
                         
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        				print_r($e->getMessage());
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
                  
			
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
		   
				 
				
	}
	

	
		
	public function pay_multi_invoice()
	{
	
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
		
				
			$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
		
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    	
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
        
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                            
             $cusproID=''; $error='';
		     $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		
		 	$checkPlan = check_free_plan_transactions();
		  
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		        
		    	 $cardID_upd ='';
			     
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
			     $amount               = $this->czsecurity->xssCleanPostInput('totalPay');
			     $customerID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			     $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
			       $invoiceIDs =implode(',', $invoices);
            
			   $inv_data   =    $this->qbo_customer_model->get_due_invoice_data($invoices,$user_id);
		  
			    if(!empty( $invoices) && !empty($inv_data)) 
			    {
                   $cusproID=  $customerID  = $inv_data['CustomerListID']; 
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  	$comp_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','firstName', 'companyName','lastName','userEmail','phoneNumber'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
            			$companyID = $comp_data['companyID'];
    			     $secretApiKey   = $gt_result['gatewayPassword'];
    			    
    			     if($cardID!="new1")
    			     {
    			         
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['cardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$phone    = $card_data['Billing_Contact'];
    					$country  = $card_data['Billing_Country'];
    					 
    					
    			     }
    			     else
    			     { 
    			         
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }   
    			     
    			      
        		$crtxnID=''; $flag='true';	     
        	
                  $option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "test_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $comp_data['firstName'],
                    		"lastName" => $comp_data['lastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => ($phone)?$phone:$comp_data['phoneNumber'],
                    		"company" => $comp_data['companyName'],
                    		"email" => $comp_data['userEmail']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                    	
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                    	
                    
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    	
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                             
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
                          $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					   
                        $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
						
						$this->session->set_flashdata('success', 'Successfully Processed Invoice');
                       
            			
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			            
                 
				           if(!empty($invoices))
				           {
				               
				              foreach($invoices as $invoiceID)
				              {
        				                $theInvoice = array();
        							  
        						       
        							   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        							
        								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '".$invoiceID."'");
        							    $condition  = array('invoiceID'=>$invoiceID,'merchantID'=>$user_id );
        						            $in_data =$this->general_model->get_select_data('QBO_test_invoice',array('BalanceRemaining','Total_payment'),$condition);
        								     $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                             $txnID       = $invoiceID;  
            						            $ispaid   = 1;
            							
            						    	$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 0;
            							 $app_amount = $in_data['Total_payment']+$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $to_amount = $in_data['BalanceRemaining']+$in_data['Total_payment'];
            							 
        							    $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
        							 		
        								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        								{
        									$theInvoice = current($targetInvoiceArray);
        								
            								$updatedInvoice = Invoice::update($theInvoice, [
            								   
            									"sparse" => 'true'
            								]);
            							
            								$updatedResult = $dataService->Update($updatedInvoice);
            								
            								$newPaymentObj = Payment::create([
            									"TotalAmt" => $to_amount,
            									"SyncToken" => $updatedResult->SyncToken,
            									"CustomerRef" => $updatedResult->CustomerRef,
            									"Line" => [
            									 "LinkedTxn" =>[
            											"TxnId" => $updatedResult->Id,
            											"TxnType" => "Invoice",
            										],    
            									   "Amount" => $pay_amounts
            									]
            									]);
            							 
            								$savedPayment = $dataService->Add($newPaymentObj);
        								
        								
        								
            								$error = $dataService->getLastError();
            								if ($error != null)
            						   	{
            							    $err='';
            					        	$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
            									$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
            								$err.="The Response message is: " . $error->getResponseBody() . "\n";
            								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>'.	$err.'</div>');
            
            							$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody();
            							
            							$qbID=$trID;	
            								$pinv_id='';
            							}
            							else 
            							{ 
            							      $pinv_id='';
            							       $pinv_id        =  $savedPayment->Id;
            							    	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
            								
											$this->session->set_flashdata('success', 'Transaction Successful');
            							}
            					          $qbo_log =array('qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				                           $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
            							   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$pay_amounts,$user_id,$pinv_id, $this->resellerID,$inID='', false, $this->transactionByUser);     		
            							
        						}
				              }
						 
				            }
                           
                            
                             if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				 {
            				 		
            				        $friendlyname   =  $this->czsecurity->xssCleanPostInput('friendlyname');
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            							
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
							 
							 
                     }
                     else
                     {
                         $crtxnID='';
                         
                               $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
                         
                     }
                     
                     
                     
                   
                 } 
                	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
		    }
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
			}
			
			if(!$checkPlan){
				$responseId  = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
			
		
		    	 if($cusproID!="")
				 {
					 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
				 } 
	      
	}
	
	
	
	
	}  