<?php

class Update_payment extends CI_Controller
{
    private $resellerID;
    
	public function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Freshbooks.php';
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->load->model('general_model');
		
	}
	
	
	public function index(){
	    
	    
	    
	    $marchant_id = $this->czsecurity->xssCleanPostInput('mid');
        $invoice_no = $this->czsecurity->xssCleanPostInput('invid');
        $token = $this->czsecurity->xssCleanPostInput('token');
        $rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$marchant_id));
		 $resellerID =	$rs_daata['resellerID'];
		 $condition1 = array('resellerID'=>$resellerID);
    	
     	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
		 $domain1 = $sub1['merchantPortalURL'];
		 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
		 
		 $val = array(
			'merchantID' => $marchant_id,
		);
        $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

        $subdomain     = $Freshbooks_data['sub_domain'];
        $key           = $Freshbooks_data['secretKey'];
        $accessToken   = $Freshbooks_data['accessToken'];
        $access_token_secret = $Freshbooks_data['oauth_token_secret'];
 
        define('OAUTH_CONSUMER_KEY', $subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
                                
        $con = array('emailCode'=>$token);
        $checkCode = $this->general_model->get_num_rows('tbl_template_data', $con);
                
	    if($checkCode > 0){
    	    $con = array('invoiceID'=>$invoice_no,'merchantID'=>$marchant_id);
    	    $result = $this->general_model->get_row_data('QBO_test_invoice', $con);
    	    
    	    $data['get_invoice'] = $result;
    	    $payamount = $result['BalanceRemaining'];
    	    $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoice_no, $marchant_id);
    	    
    	    $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID'=>$marchant_id));
    		$resellerID = $rs_Data['resellerID'];
    	    //Invoice detail
    	  
    	      $con = array('merchantID'=>$marchant_id,'set_as_default'=>1);
    	      $get_gateway = $this->general_model->get_row_data('tbl_merchant_gateway', $con);
    	     
    	      if($get_gateway['gatewayType']==5 )
    	      $data['userName']   =$get_gateway['gatewayUsername'];
    	      else
    	        $data['userName']   ='';
    	      
    	      
    	    
    	    if($this->czsecurity->xssCleanPostInput('pay')){
    	        $gateway = $get_gateway['gatewayType'];
    	        $bill_email ='';
        	    switch ($gateway) {
                    case "1":
                        
                        include APPPATH . 'third_party/nmiDirectPost.class.php';
	                    include APPPATH . 'third_party/nmiCustomerVault.class.php';
                        //start NMI
                        $nmiuser   = $get_gateway['gatewayUsername'];
        		        $nmipass   = $get_gateway['gatewayPassword'];
        		        $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
        		        
        		        //Billing Data
        		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
        		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
        		     //   $bill_email = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('email'));
        		        $cardtype = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
        		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
        		        $address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
        		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
        		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
        		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
        		        $zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
        		        $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
        		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
        		        $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
        		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
        		        
                        if($payamount > 0){
                        	$transaction1 = new nmiDirectPost($nmi_data); 
    						$transaction1->setCcNumber($cnumber);
    					    $expmonth =  $expmonth;
    						$exyear   = $expyear;
    						$exyear   = substr($exyear,2);
    						if(strlen($expmonth)==1){
    							$expmonth = '0'.$expmonth;
    						}
    					    $expry    = $expmonth.$exyear;  
    						$transaction1->setCcExp($expry);
    						$transaction1->setCvv($cvv);
    						$transaction1->setAmount($payamount);
    			            $transaction1->sale();
    					    $getwayResponse = $transaction1->execute(); 
    					    
    					    if( $getwayResponse['response_code']=="100"){
    					        
    					        if(isset($accessToken) && isset($access_token_secret)){
                                
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                               
                                  $payment = '<?xml version="1.0" encoding="utf-8"?>  
                        						<request method="payment.create">  
                        						  <payment>         
                        						    <invoice_id>'.$invoice_no.'</invoice_id>               
                        						    <amount>'.$payamount.'</amount>             
                        						    <currency_code>USD</currency_code> 
                        						    <type>Check</type>                   
                        						  </payment>  
                        						</request>';
                                    //  print_r($payment);
                                      $invoices = $c->add_payment($payment);
                                     
                                
                        
                        			if ($invoices['status'] != 'ok') {
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        			
                        			}
                        		
        			
    					         $txnID      = $result['invoiceID'];  
        						 $ispaid 	 = 'true';
        						 $pay        = $payamount;
        						 $remainbal  = $result['BalanceRemaining']-$pay;
        						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );
        						 $condition  = array('invoiceID'=>$result['invoiceID'] );
        						 $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
        				     
        					     
        					    $transaction['transactionID']      = $getwayResponse['transactionid'];
        					    $transaction['transactionStatus']  = $getwayResponse['responsetext'];
        					    $transaction['transactionCode']    = $getwayResponse['response_code'];
        					    $transaction['transactionType']    = ($getwayResponse['type'])?$getwayResponse['type']:'auto-nmi';
        					    $transaction['transactionDate']    = date('Y-m-d H:i:s');  
        					     $transaction['transactionModified']     = date('Y-m-d H:i:s');  
        					    $transaction['invoiceID']        = $invoiceIDs;  
        					    $transaction['gatewayID']          = 1;
                                $transaction['transactionGateway'] = 1;	
        					    $transaction['customerListID']     = $in_data['CustomerListID'];
        					    $transaction['qbListTxnID']        = $crtxnID;
        					    $transaction['transactionAmount']  = $payamount;
        					    $transaction['merchantID']         = $marchant_id;
        					    $transaction['gateway']            = "NMI";
        					    $transaction['resellerID']         = $resellerID;
        					  
        				        $id = $this->general_model->insert_row('customer_transaction',   $transaction);
        				        if(!empty($id)){
        				             $this->session->set_flashdata('success','Successfully Paid ');  
        				               redirect('update_payment/thankyou/');
        				        }
    					    }
                        }
					    else{
					        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Payment processing got error.</div>');  
					        	header('Location: '.$_SERVER['REQUEST_URI']);
					    }
				    }
        	       
        	        break;
                case "2":
                    
                    include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
                    $this->load->config('auth_pay');
                    //Login in Auth
                    $apiloginID       = $get_gateway['gatewayUsername'];
	                $transactionKey   = $get_gateway['gatewayPassword'];
                    
                    //Billing Data
    		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
    		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
    		       // $bill_email = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('email'));
    		        $cardtype = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
    		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
    		        $address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
    		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
    		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
    		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
    		        $zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
    		        $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
    		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
    		        $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
    		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
                    if($payamount > 0){
                        $transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
					  	$transaction1->setSandbox($this->config->item('Sandbox'));
					    $card_no  = $cnumber;
					    $expmonth = $expmonth;
						$exyear   = $expyear;
						$exyear   = substr($exyear,2);
						if(strlen($expmonth)==1){
							$expmonth = '0'.$expmonth;
						}
			 	            $expry = $expmonth.$exyear;  
						$getwayResponse = $transaction1->authorizeAndCapture($payamount,$card_no,$expry);
				       if( $getwayResponse->response_code=="1"){
				           
				            if(isset($accessToken) && isset($access_token_secret)){
                                
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                               
                                  $payment = '<?xml version="1.0" encoding="utf-8"?>  
                        						<request method="payment.create">  
                        						  <payment>         
                        						    <invoice_id>'.$invoice_no.'</invoice_id>               
                        						    <amount>'.$payamount.'</amount>             
                        						    <currency_code>USD</currency_code> 
                        						    <type>Check</type>                   
                        						  </payment>  
                        						</request>';
                                    //  print_r($payment);
                                      $invoices = $c->add_payment($payment);
                                     
                                
                        
                        			if ($invoices['status'] != 'ok') {
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        			
                        			}
				            $txnID      = $result['invoiceID'];  
    						$ispaid 	 = 'true';
    						$pay        = $payamount;
    						$remainbal  = $result['BalanceRemaining']-$pay;
    						$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );
    						$condition  = array('invoiceID'=>$result['invoiceID'] );
    						$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
    				        
    				        $transactiondata= array();
				            $transactiondata['transactionID']       = $getwayResponse->transaction_id;
					        $transactiondata['transactionStatus']   = $getwayResponse->response_reason_text;
					        $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					         $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					        $transactiondata['transactionCode']     = $getwayResponse->response_code;  
					        $transactiondata['transactionCard']     = substr($getwayResponse->account_number,4);  
					        $transactiondata['transactionType']     = $getwayResponse->transaction_type;	   
					        $transactiondata['gatewayID']           = 2;
                            $transactiondata['transactionGateway']  = $get_gateway['gatewayType'];
					        $transactiondata['customerListID']      = $in_data['CustomerListID'];
					        $transactiondata['transactionAmount']   = $payamount;
					        $transactiondata['invoiceID']        = $invoiceIDs;  
                            $transactiondata['qbListTxnID']        = $crtxnID;
					        $transactiondata['merchantID']          = $marchant_id;
					        $transactiondata['gateway']             = "Auth";
					        $transactiondata['resellerID']          = $resellerID;
					  
    					  
    				        $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
    				        if(!empty($id)){
        				             $this->session->set_flashdata('success','Successfully Paid ');  
        				               redirect('update_payment/thankyou/');
        				    }
					     }
				       }
					   else{
					        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Any Date not Selected2</div>');  
					        	header('Location: '.$_SERVER['REQUEST_URI']);
					   }
                    }
                    
                    break;
                case "3":
                    
		            include APPPATH . 'third_party/PayTraceAPINEW.php';
		            $this->load->config('paytrace');
                    $payusername   = $get_gateway['gatewayUsername'];
        		    $paypassword   = $get_gateway['gatewayPassword'];
        		    $grant_type    = "password";
		            
		            //Billing Data
    		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
    		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
    		       // $bill_email = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('email'));
    		        $cardtype = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
    		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
    		        $address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
    		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
    		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
    		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
    		        $zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
    		        $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
    		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
    		        $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
    		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
    		        $name = $fname." ".$lname;
                    if($payamount > 0){
		                
                    	$expmonth = $expmonth;
					    if(strlen($expmonth)==1){
							$expmonth = '0'.$expmonth;
						}
					  
					    $payAPI  = new PayTraceAPINEW();	
					    $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                    	//call a function of Utilities.php to verify if there is any error with OAuth token. 
						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
						
    				    if(!$oauth_moveforward){ 
    		                $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
    			            //set Authentication value based on the successful oAuth response.
            				//Add a space between 'Bearer' and access _token 
            				$oauth_token = sprintf("Bearer %s",$json['access_token']);
    				        $request_data = array(
                                "amount"            => $payamount,
                                "credit_card"       => array (
                                    "number"            => $cnumber,
                                    "expiration_month"  =>$expmonth,
                                    "expiration_year"   =>$expyear
                                ),
                                
                                "csc"               => $cvv,
                                "invoice_id"        =>$invoice_no,
                                
                                "billing_address"=> array(
                                    "name"          =>$name,
                                    "street_address"=> $address,
                                    "city"          => $city,
                                    "state"         => $state,
                                    "zip"           => $zip
        						)
        					);  
                     
    				       $request_data = json_encode($request_data); 
    			           $gatewayres    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
    				       $response  = $payAPI->jsonDecode($gatewayres['temp_json_response']); 
    			           if ( $gatewayres['http_status_code']=='200' ){
        				       
        				      if(isset($accessToken) && isset($access_token_secret)){
                                
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                               
                                  $payment = '<?xml version="1.0" encoding="utf-8"?>  
                        						<request method="payment.create">  
                        						  <payment>         
                        						    <invoice_id>'.$invoice_no.'</invoice_id>               
                        						    <amount>'.$payamount.'</amount>             
                        						    <currency_code>USD</currency_code> 
                        						    <type>Check</type>                   
                        						  </payment>  
                        						</request>';
                                    //  print_r($payment);
                                      $invoices = $c->add_payment($payment);
                                     
                                
                        
                        			if ($invoices['status'] != 'ok') {
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        			
                        			}
        				       
        				        $txnID      = $result['invoiceID'];  
        						$ispaid 	 = 'true';
        						$pay        = $payamount;
        						$remainbal  = $result['BalanceRemaining']-$pay;
        						$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );
        						$condition  = array('invoiceID'=>$result['invoiceID'] );
        						$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
        				     
        					    $transactiondata= array();
        					    if(isset($response['transaction_id'])){
        				            $transactiondata['transactionID']   = $response['transaction_id'];
        					    }
        					    else{
        						    $transactiondata['transactionID']   = '';
        					    }
        					    $transactiondata['transactionStatus']   = $response['status_message'];
        					    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
        					    $transactiondata['transactionCode']     = $gatewayres['http_status_code'];
        					    $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
        					    $transactiondata['transactionType']     = 'pay_sale';
        						$transactiondata['gatewayID']           = 3;
                                $transactiondata['transactionGateway']  = $get_gateway['gatewayType'] ;							
        					    $transactiondata['customerListID']      = $in_data['CustomerListID'];
        					    $transactiondata['invoiceID']        = $invoiceIDs;  
                                $transactiondata['qbListTxnID']        = $crtxnID;
        					    $transactiondata['transactionAmount']   = $payamount;
        					    $transactiondata['merchantID']          = $marchant_id;
        					    $transactiondata['gateway']             = "Paytrace";
    					        $transactiondata['resellerID']          = $resellerID;
        					   
        				        $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
        				        if(!empty($id)){
        				             $this->session->set_flashdata('success','Successfully Paid ');  
        				               redirect('update_payment/thankyou/');
        				        }
    					     }
    			           }
    					   else{
    					        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Any Date not Selected3</div>'); 
    					        	header('Location: '.$_SERVER['REQUEST_URI']);
    					    }
    				    }
		
                    }
                    
                    break;
                case "4":
                    include APPPATH . 'third_party/PayPalAPINEW.php';
			        $this->load->config('paypal');
                  
                    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $get_gateway['gatewayUsername'], 	// PayPal API username of the API caller
						'APIPassword' => $get_gateway['gatewayPassword'],	// PayPal API password of the API caller
						'APISignature' => $get_gateway['gatewaySignature'], 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
					  $this->load->library('paypal/Paypal_pro', $config);	  
					  if($config['Sandbox'])
    					{
    						error_reporting(E_ALL);
    						ini_set('display_errors', '1');
    					}
					
					//Billing Data
    		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
    		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
    		      //  $bill_email = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('email'));
    		        $cardtype = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
    		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
    		        $address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
    		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
    		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
    		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
    		        $zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
    		        $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
    		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
    		        $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
    		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
    		        $name = $fname." ".$lname;
					if($payamount > 0){
                   
                        $creditCardType   = 'Visa';
						$creditCardNumber = $cnumber;
						$expDateMonth     = $expmonth;
					    $expDateYear      = $expyear;
						$creditCardType   = ($cardtype)?$cardtype:$creditCardType;
						$padDateMonth 	  = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
						$cvv2Number       =   $cvv;
						$currencyID       = "USD";
						
						$firstName = $fname;
                        $lastName =  $lname; 
						$address1 = $address; 
                        $address2 = $address2; 
						$country  = $country; 
						$city     = $city;
						$state    = $state;		
						$zip  = $zip;  
						$email = $bill_email; 
										
                		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                		);
                						
                		$CCDetails = array(
							'creditcardtype' => $cardtype, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct'           => $cnumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate'        => $expmonth.$expyear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2'           => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							'startdate'      => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber'    => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
                						
                		$PayerInfo = array(
							'email'          => $bill_email, 								// Email address of payer.
							'payerid'        => '', 							// Unique PayPal customer ID for payer.
							'payerstatus'    => 'verified', 						// Status of payer.  Values are verified or unverified
							'business'       => '' 							// Payer's business name.
						);  
                						
                		$PayerName = array(
							'salutation'     => '', 						// Payer's salutation.  20 char max.
							'firstname'      => $fname, 							// Payer's first name.  25 char max.
							'middlename'     => '', 						// Payer's middle name.  25 char max.
							'lastname'       => $lname, 							// Payer's last name.  25 char max.
							'suffix'         => ''								// Payer's suffix.  12 char max.
						);
                					
                		$BillingAddress = array(
							'street'         => $address1, 						// Required.  First street address.
							'street2'        => $address2, 						// Second street address.
							'city'           => $city, 							// Required.  Name of City.
							'state'          => $state, 							// Required. Name of State or Province.
							'countrycode'    => $country, 					// Required.  Country code.
							'zip'            => $zip 						// Phone Number of payer.  20 char max.
						);
                	
                							
	                   $PaymentDetails = array(
							'amt'            => $payamount,					// Required.  Three-letter currency code.  Default is USD.
							'itemamt'        => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
							'shippingamt'    => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
							'insuranceamt'   => '', 					// Total shipping insurance costs for this order.  
							'shipdiscamt'    => '', 					// Shipping discount for the order, specified as a negative number.
							'handlingamt'    => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
							'taxamt'         => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
							'desc'           => '', 							// Description of the order the customer is purchasing.  127 char max.
							'custom'         => '', 						// Free-form field for your own use.  256 char max.
							'invnum'         => '', 						// Your own invoice or tracking number
							'buttonsource'   => '', 					// An ID code for use by 3rd party apps to identify transactions.
							'notifyurl'      => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
							'recurring'      => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
						);					

						$PayPalRequestData = array(
							'DPFields'       => $DPFields, 
							'CCDetails'      => $CCDetails, 
							'PayerInfo'      => $PayerInfo, 
							'PayerName'      => $PayerName, 
							'BillingAddress' => $BillingAddress, 
							'PaymentDetails' => $PaymentDetails, 
							
						);
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
					    if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])){
					         if(isset($accessToken) && isset($access_token_secret)){
                                
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                               
                                  $payment = '<?xml version="1.0" encoding="utf-8"?>  
                        						<request method="payment.create">  
                        						  <payment>         
                        						    <invoice_id>'.$invoice_no.'</invoice_id>               
                        						    <amount>'.$payamount.'</amount>             
                        						    <currency_code>USD</currency_code> 
                        						    <type>Check</type>                   
                        						  </payment>  
                        						</request>';
                                    //  print_r($payment);
                                      $invoices = $c->add_payment($payment);
                                     
                                
                        
                        			if ($invoices['status'] != 'ok') {
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        			
                        			}
					        $txnID      = $result['invoiceID'];  
    						$ispaid 	 = 'true';
    						$pay        = $payamount;
    						$remainbal  = $result['BalanceRemaining']- $pay;
    						$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );
    						$condition  = array('invoiceID'=>$result['invoiceID'] );
    						$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
    				     
    					    $transaction= array();
                            $tranID ='' ;
                            $amt='0.00';
					        if(isset($PayPalResult['TRANSACTIONID'])) { 
					            $tranID = $PayPalResult['TRANSACTIONID'];   
					            $amt=$PayPalResult["AMT"];  
					        }
                            
				            $transaction['transactionID']       = $tranID;
					        $transaction['transactionStatus']   = $PayPalResult["ACK"];
					        $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					        $transaction['transactionModified']     = date('Y-m-d H:i:s');  
					        $transaction['transactionCode']     = $code;  
					        $transaction['transactionType']     = "Paypal_sale";	
						    $transaction['gatewayID']           = $gateway;
                            $transaction['transactionGateway']  = $get_gateway['gatewayType'];					
					        $transaction['customerListID']      = $in_data['CustomerListID'];
					        $transaction['transactionAmount']   = $payamount;
					        $transaction['invoiceID']           = $invoiceIDs;  
                            $transaction['qbListTxnID']         = $crtxnID;
					        $transaction['merchantID']          = $marchant_id;
					        $transaction['gateway']             = "Paypal";
					        $transaction['resellerID']          = $resellerID;
					
				            $this->general_model->insert_row('customer_transaction',   $transaction);
    				        if(!empty($id)){
        				             $this->session->set_flashdata('success','Successfully Paid ');  
        				               redirect('update_payment/thankyou/');
        				        }
					         }
					    }
					    else{
    					        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Any Date not Selected3</div>'); 
    					        	header('Location: '.$_SERVER['REQUEST_URI']);
    					    }
					}
					
                    break;
                case "5":
                    include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
		
                    $token = $this->czsecurity->xssCleanPostInput('stripeToken'); 
                 
                    if($payamount > 0 && $token !='')
                    {
                        
                        $paidamount =  (int)($payamount*100);
                        
                        $plugin = new ChargezoomStripe();
                        $plugin->setApiKey($gt_result['gatewayPassword']);
                    	//\Stripe\Stripe::setApiKey($get_gateway['gatewayPassword']);
						$charge =	\Stripe\Charge::create(array(
							  "amount" => $paidamount,
							  "currency" => "usd",
							  "source" => $token, // obtained with Stripe.js
							  "description" => "Charge Using Stripe Gateway",
							 
							));	
                        $charge= json_encode($charge);
                        
                        $resultstripe = json_decode($charge);
                        
                       
                        $trID='';
                        if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
				         {
				             
				             if(isset($accessToken) && isset($access_token_secret)){
                                
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                               
                                  $payment = '<?xml version="1.0" encoding="utf-8"?>  
                        						<request method="payment.create">  
                        						  <payment>         
                        						    <invoice_id>'.$invoice_no.'</invoice_id>               
                        						    <amount>'.$payamount.'</amount>             
                        						    <currency_code>USD</currency_code> 
                        						    <type>Check</type>                   
                        						  </payment>  
                        						</request>';
                                    //  print_r($payment);
                                      $invoices = $c->add_payment($payment);
                                     
                                
                        
                        			if ($invoices['status'] != 'ok') {
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        			
                        			}
				            $trID       = $resultstripe->id;
				            $code		=  '200';
						    $txnID      = $result['invoiceID'];  
    						$ispaid 	= 'false';
    						$pay        = $paidamount;
    						$remainbal  = $result['BalanceRemaining']-$pay;
    						$data   	= array('IsPaid'=>$ispaid, 'AppliedAmount'=>$payamount , 'BalanceRemaining'=>$remainbal );
    						$condition  = array('invoiceID'=>$result['invoiceID'] );
    						$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
				            
				            $transaction['transactionID']       = $trID;
					        $transaction['transactionStatus']   = $resultstripe->status;
					        $transaction['transactionDate']     = date('Y-m-d H:i:s');  
					         $transaction['transactionModified']     = date('Y-m-d H:i:s');  
					        $transaction['transactionCode']     = $code;  
					        $transaction['invoiceID']        = $invoiceIDs;  
                            $transaction['qbListTxnID']        = $crtxnID;
						    $transaction['transactionType']     = 'stripe_sale';	
						    $transaction['gatewayID']           = $gateway;
                            $transaction['transactionGateway']  = $get_gateway['gatewayType'] ;					
					        $transaction['customerListID']      = $in_data['CustomerListID'];
					        $transaction['transactionAmount']   = $payamount;
					        $transaction['merchantID']          = $marchant_id;
					        $transaction['gateway']             = "Stripe";
					        $transaction['resellerID']          = $resellerID;
					        
					        $id = $this->general_model->insert_row('customer_transaction',   $transaction); 
					        
					         if(!empty($id)){
        				             $this->session->set_flashdata('success','Successfully Paid ');  
        				               redirect('update_payment/thankyou/');
        				        }
				            }
				         }
				         else{
    					        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Any Date not Selected3</div>'); 
    					        	header('Location: '.$_SERVER['REQUEST_URI']);
    					    }
                    }
                    
                    break;
                default:
                    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>This Link is not valid.</div>'); 
	    		        header('Location: '.$_SERVER['REQUEST_URI']);
            }
	    }
	
        $this->load->view('pages/public_invoice_page', $data);
		$this->load->view('template/page_footer',$data);
	
	
	    
	 }
	 else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>This Link is not valid.</div>'); 
	    		header('Location: '.$_SERVER['REQUEST_URI']);
	    }
	}
	
}