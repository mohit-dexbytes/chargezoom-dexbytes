<?php
include APPPATH . 'libraries/Freshbooks_data.php';

class Freshbooks_Customer extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    protected $loginDetails;
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        $this->load->model('Freshbook_models/fb_company_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
            $this->merchantID = $this->session->userdata('logged_in')['merchID'];
            $this->resellerID = $this->session->userdata('logged_in')['resellerID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $merchID          = $this->session->userdata('user_logged_in')['merchantID'];
            $this->merchantID = $merchID;
            $rs_Data          = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }

        $val = array(
            'merchantID' => $this->merchantID,
        );

        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
        if (empty($Freshbooks_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
            redirect('FreshBooks_controllers/home/index', 'refresh');
        }
        $this->subdomain           = $Freshbooks_data['sub_domain'];
        $key                       = $Freshbooks_data['secretKey'];
        $this->accessToken         = $Freshbooks_data['accessToken'];
        $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
        $this->fb_account          = $Freshbooks_data['accountType'];
        $condition1                = array('resellerID' => $this->resellerID);
        $sub1                      = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1                   = $sub1['merchantPortalURL'];
        $domain1                   = 'http://localhost/payportal';
        $URL1                      = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        define('OAUTH_CONSUMER_KEY', $this->subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
        $this->loginDetails = get_names();
    }

    public function index()
    {
      redirect('FreshBooks_controllers/home', 'refresh');
    }

    public function get_customer_data()
    {
        $merchID   = $this->merchantID;
        $condition = array('merchantID' => $merchID);

        $val = array(
            'merchantID' => $merchID,
        );
        $resellerID = $this->resellerID;
        
        $fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
        $fb_data->get_freshbooks_customers();
        
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $merchID             = $this->merchantID;
        if ($this->uri->segment('4') == "InActive") {
            $data['activeval'] = "Active";
        } else {
            $data['activeval'] = "InActive";
        }

        $condition1          = array('merchantID' => $merchID, 'systemMail' => 0);
		$data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);
		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/page_customers', $data);
        $this->load->view('FreshBooks_views/page_fb_model', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_customer()
    {

        $val = array(
            'merchantID' => $this->merchantID,
        );

        if (isset($this->accessToken) && isset($this->access_token_secret)) {
            if ($this->fb_account == 1) {
                $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
            } else {
                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
            }

            if (!empty($this->input->post(null, true))) {

                $this->load->library('form_validation');
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
                $this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');

                if ($this->form_validation->run() == true) {
                    $innput['fname']    = $firstName    = $this->czsecurity->xssCleanPostInput('firstName');
                    $innput['lname']    = $lastName    = $this->czsecurity->xssCleanPostInput('lastName');
                    $innput['username'] = $fullName = $this->czsecurity->xssCleanPostInput('fullName');
                    $innput['email']    = $userEmail    = $this->czsecurity->xssCleanPostInput('userEmail');

                    $innput['p_province'] = $state = $this->czsecurity->xssCleanPostInput('companyState');
                    $innput['p_city']     = $city     = $this->czsecurity->xssCleanPostInput('companyCity');

                    $innput['organization'] = $companyName = $this->czsecurity->xssCleanPostInput('companyName');
                    $innput['home_phone']   = $phoneNumber   = $this->czsecurity->xssCleanPostInput('phone');
                    $innput['p_street']     = $address1     = $this->czsecurity->xssCleanPostInput('address1');
                    $innput['p_street2']    = $address2    = $this->czsecurity->xssCleanPostInput('address2');
                    $innput['p_code']       = $zipCode       = $this->czsecurity->xssCleanPostInput('zipCode');
                    $innput['p_country']    = $country    = $this->czsecurity->xssCleanPostInput('companyCountry');

                    $innput['s_country']  = $ship_country  = $this->czsecurity->xssCleanPostInput('sCountry');
                    $innput['s_province'] = $ship_state = $this->czsecurity->xssCleanPostInput('sState');
                    $innput['s_city']     = $ship_city     = $this->czsecurity->xssCleanPostInput('sCity');
                    $innput['s_street']   = $ship_address1   = $this->czsecurity->xssCleanPostInput('saddress1');
                    $innput['s_street2']  = $ship_address2  = $this->czsecurity->xssCleanPostInput('saddress2');
                    $innput['s_code']     = $ship_zipcode     = $this->czsecurity->xssCleanPostInput('szipCode');
                    $createdat            = date('Y-m-d H:i:s');
                    $merchantID           = $this->merchantID;
                    if ($this->czsecurity->xssCleanPostInput('customerListID') != "") {

                        if ($this->fb_account == 1) {
                            $client_id        = $this->czsecurity->xssCleanPostInput('customerListID');
                            $innput['userid'] = $client_id;
                            $clients          = $c->update_customer($innput);
                        } else {

                            $customer = '<request method="client.update">
                          <client>
                             <client_id>' . $this->czsecurity->xssCleanPostInput('customerListID') . '</client_id>
                           <first_name>' . $firstName . '</first_name>
                            <last_name>' . $lastName . '</last_name>
                            <organization>' . $companyName . '</organization>
                            <email>' . $userEmail . '</email>
                            <username>' . $fullName . '</username>
                            <home_phone>' . $phoneNumber . '</home_phone>
                             <p_street1>' . $address1 . '</p_street1>
                             <p_street2>' . $address2 . '</p_street2>
                             <p_code>' . $zipCode . '</p_code>
                              <p_city>' . $city . '</p_city>
                            <p_state>' . $state . '</p_state>
                            <p_country>' . $country . '</p_country>


                            <s_street1>' . $ship_address1 . '</s_street1>
                            <s_street2>' . $ship_address2 . '</s_street2>
                            <s_city>' . $ship_city . '</s_city>
                            <s_state>' . $ship_state . '</s_state>
                            <s_country>' . $ship_country . '</s_country>
                            <s_code>' . $ship_zipcode . '</s_code>

                          </client>
                        </request> ';

                            $clients = $c->add_customers($customer);

                        }
                        $this->session->set_flashdata('success', 'Success');

                    } else {

                        if ($this->fb_account == 1) {
                            $client_id        = '';
                            $innput['userid'] = $client_id;

                            $clients = $c->add_customer($innput);
                        } else {
                            $customer = '<?xml version="1.0" encoding="utf-8"?>
                         <request method="client.create">
                          <client>
                            <first_name>' . $firstName . '</first_name>
                            <last_name>' . $lastName . '</last_name>
                            <organization>' . $companyName . '</organization>
                            <email>' . $userEmail . '</email>
                            <username>' . $fullName . '</username>
                            <home_phone>' . $phoneNumber . '</home_phone>
                             <p_street1>' . $address1 . '</p_street1>
                               <p_street2>' . $address2 . '</p_street2>
                             <p_code>' . $zipCode . '</p_code>
                             <p_city>' . $city . '</p_city>
                            <p_state>' . $state . '</p_state>
                            <p_country>' . $country . '</p_country>


                            <s_street1>' . $ship_address1 . '</s_street1>
                            <s_street2>' . $ship_address2 . '</s_street2>
                            <s_city>' . $ship_city . '</s_city>
                            <s_state>' . $ship_state . '</s_state>
                            <s_country>' . $ship_country . '</s_country>
                            <s_code>' . $ship_zipcode . '</s_code>
                          </client>
                        </request> ';

                            $clients = $c->add_customer_data($customer);
                        }
                        if (is_numeric($clients)) {

                            $login['isEnable']          = 1;
                            $login['customerEmail']     = $userEmail;
                            $login['merchantID']        = $merchID;
                            $login['customerID']        = $clients;
                            $login['customerPassword']  = md5($userEmail);
                            $login['customerUsername	'] = $userEmail;
                            $login['createdAt'] = date('Y-m-d H:i:s');
                            $this->general_model->insert_row('tbl_customer_login', $login);
                        }

                        $this->session->set_flashdata('success', 'Success');

                    }

                    redirect(base_url('FreshBooks_controllers/Freshbooks_Customer/get_customer_data'));
                }
            }
        }
        if ($this->uri->segment('4')) {
            $customerListID   = $this->uri->segment('4');
            $con              = array('Customer_ListID' => $customerListID, 'merchantID' => $this->merchantID);
            $data['customer'] = $this->general_model->get_row_data('Freshbooks_custom_customer', $con);
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $country               = $this->general_model->get_table_data('country', '');
        $data['country_datas'] = $country;

        $state               = $this->general_model->get_table_data('state', '');
        $data['state_datas'] = $state;

        $city               = $this->general_model->get_table_data('city', '');
        $data['city_datas'] = $city;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/create_customer', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function delete_customer()
    {

        $cust_id = $this->czsecurity->xssCleanPostInput('fbcustID');
        $st_act  = $this->czsecurity->xssCleanPostInput('st_act');

        if ($st_act == 1) {
            $st = 0;
        } else {
            $st = 1;
        }

        if (isset($this->accessToken) && isset($this->access_token_secret)) {

            if ($cust_id != "") {

                if ($this->fb_account == 1) {
                    $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                    $innput['userid']    = $cust_id;
                    $innput['vis_state'] = $st;
                    $clients = $c->update_customer($innput);

                    if ($clients) {
                        $this->general_model->update_row_data('Freshbooks_custom_customer', array('merchantID' => $this->merchantID, 'Customer_ListID' => $cust_id), array('updatedAt' => date('Y-m-d H:i:s'), 'customerStatus' => $st_act));
                    }

                } else {
                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                    if ($st_act == 0) {
                        $customer = '<request method="client.delete">
                            <client_id>' . $cust_id . '</client_id>
                          </request>  ';
                    }
                    if ($st_act == 1) {
                        $customer = '<request method="client.update">
                            <client_id>' . $cust_id . '</client_id>
                             <folder>active</folder>
                          </request>  ';
                    }

                    $clients = $c->add_customers($customer);

                }

                if ($st == 0) {

                    $this->session->set_flashdata('success', 'Successfully Activated');
                } else {
                    $this->session->set_flashdata('success', 'Successfully Deactivated');

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
            }

            redirect(base_url('FreshBooks_controllers/Freshbooks_Customer/get_customer_data/InActive'));
        }
    }

    public function view_customer($cusID = '')
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info']      = $this->session->userdata('logged_in');
            $data['merchantEmailID'] = $this->session->userdata('logged_in')['merchantEmail'];
            $user_id                 = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info']      = $this->session->userdata('user_logged_in');
            $data['merchantEmailID'] = $this->session->userdata('user_logged_in')['merchantEmail'];
            $user_id                 = $data['login_info']['merchantID'];
        }
        $user_id = $this->merchantID;

        $data['gateway_datas']         = $this->general_model->get_gateway_data($user_id);
        $data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
        $data['isCardGatewayExists']   = $data['gateway_datas']['isCardExists'];

        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway                = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway']        = $defaultGateway[0];
            $data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
            $data['isCardGatewayExists']   = $data['defaultGateway']['creditCard'];
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['page_num']    = 'customer_freshbook';
        
        $data['customer'] = $this->fb_customer_model->customer_details($cusID, $this->merchantID);

        // Convert added date in timezone
        if (isset($data['customer']->TimeCreated) && isset($data['login_info']['merchant_default_timezone']) && !empty($data['login_info']['merchant_default_timezone'])) {
            $timezone                      = ['time' => $data['customer']->TimeCreated, 'current_format' => 'UTC', 'new_format' => $data['login_info']['merchant_default_timezone']];
            $data['customer']->TimeCreated = getTimeBySelectedTimezone($timezone);
        }

        $data_invoice           = $this->fb_customer_model->get_customer_invoice_data_sum($cusID, $user_id);
        $data['invoices_count'] = ($data_invoice->incount) ? $data_invoice->incount : '0';
        $data['invoices']       = $this->fb_customer_model->get_invoice_upcomming_data($cusID, $user_id);
        $data['latest_invoice'] = $this->fb_customer_model->get_invoice_latest_data($cusID, $user_id);
        $paydata               = $this->fb_customer_model->get_customer_invoice_data_payment($cusID, $user_id);
        $data['notes']         = $this->fb_customer_model->get_customer_note_data($cusID, $user_id);
        $data['pay_invoice']   = ($paydata->applied_amount1) ? $paydata->applied_amount1 : '0.00';
        $data['pay_upcoming']  = ($paydata->upcoming_balance) ? $paydata->upcoming_balance : '0.00';
        $data['pay_remaining'] = ($paydata->remaining_amount) ? $paydata->remaining_amount : '0.00';

        $data['pay_due_amount'] = ($paydata->applied_due) ? $paydata->applied_due : '0.00';
        $mail_con               = array('merchantID' => $user_id, 'customerID' => $cusID);
        $data['sum_invoice']    = ($paydata->applied_amount) ? $paydata->applied_amount : '0.00';

        $condition1 = array('merchantID' => $user_id, 'systemMail' => 0);

        $data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);

        $data['editdatas']       = $this->fb_customer_model->get_email_history($mail_con);
        $data['card_data_array'] = $this->card_model->get_customer_card_data($cusID);
        $condition = array('merchantID' => $user_id);
        $sub                      = array('sbs.customerID' => $cusID, 'cust.merchantID' => $user_id);
        $data['getsubscriptions'] = $this->fb_customer_model->get_cust_subscriptions_data($sub);

        $data['from_mail']       = DEFAULT_FROM_EMAIL;
        $data['mailDisplayName'] = $this->loginDetails['companyName'];

        $this->load->model('Freshbook_models/fb_invoice_model');
        $this->load->model('Freshbook_models/freshbooks_model');

        $filter         = $this->czsecurity->xssCleanPostInput('status_filter');
        $data['filter'] = $filter;

        if (!empty($this->input->post(null, true))) {
            $search = $this->input->post(null, true);
            if (isset($search['search_data'])) {
                $data['serachString'] = $search['search_data'];
            }
        }
        if ($filter != '') {
            if ($filter == 'All') {
                $data['all_invoices'] = $this->fb_invoice_model->get_invoice_list_data($user_id, $cusID);
            } else {
                if ($filter == 'open') {
                    $data['all_invoices'] = $this->fb_invoice_model->get_open_invoice_data($user_id, $cusID);
                } else {
                    $data['all_invoices'] = $this->fb_invoice_model->get_invoice_data_by_status($user_id, $filter, $cusID);
                }
            }
        } else {
            $data['all_invoices'] = $this->fb_invoice_model->get_open_invoice_data($user_id, $cusID);
        }

        $plantype            = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
        $data['plantype_vs'] = $this->general_model->chk_merch_plantype_vt($user_id);
        $data['plantype']    = $plantype;

        $data['transaction_history_data'] = $this->freshbooks_model->get_transaction_history_data($user_id, $cusID);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/page_customer_details', $data);
        $this->load->view('FreshBooks_views/page_fb_model', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_fb_sale_invoices()
    {
        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $invoices = $this->fb_customer_model->get_invoice_upcomming_data($customerID, $merchantID);
            $new_inv = '<div class="row form-group" >
			 <div class="col-md-1  text-right"><b></b></div>
			<div class="col-md-2  text-right"><b>Number</b></div>
			<div class="col-md-3 text-right"><b>Due Date</b></div>
			<div class="col-md-2  text-right"><b>Amount</b></div>
			<div class="col-md-3 text-left"><b>Payment</b></div>
			</div>';
            $i        = 1;
            $inv_data = [];
            foreach ($invoices as $inv) {

                $new_inv .= '<div class="row form-group" >

			<div class="col-md-1 text-right"><input type="checkbox" class="chk_pay check_' . $inv['refNumber'] . '"  id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
			<div class="col-md-2 text-right">' . $inv['refNumber'] . '</div>
			<div class="col-md-3 text-right">' . date("M d,Y", strtotime($inv['DueDate'])) . '</div>
			<div class="col-md-2 text-right">$' . number_format($inv['BalanceRemaining'], 2) . '</div>
      <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['invoiceID'] . '  geter" data-id="multiinv' . $inv['invoiceID'] . '" data-inv="' . $inv['invoiceID'] . '" data-ref="' . $inv['refNumber'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
      </div>';

                $inv_data[] = [
                    'refNumber'        => $inv['refNumber'],
                    'invoiceID'        => $inv['invoiceID'],
                    'BalanceRemaining' => $inv['BalanceRemaining'],
                ];

            }

            $card         = '';
            $card_name    = '';
            $customerdata = array();

            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('Freshbooks_custom_customer', $condition);
            if (!empty($customerdata)) {
                $customerdata['status'] = 'success';

                $card_data                = $this->card_model->get_card_expiry_data($customerID);
                $customerdata['card']     = $card_data;
                $customerdata['invoices'] = $new_inv;
                $customerdata['inv_data'] = $inv_data;

                echo json_encode($customerdata);
                die;
            }

        }

    }
    public function get_fb_customer_invoices()
    {
        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $invoices = $this->fb_company_model->get_open_invoices($customerID, $merchantID);
            $new_inv = '<div class="form-group" >
		         <div class="col-md-1  text-right"><b>Select</b></div>
		        <div class="col-md-2  text-right"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2  text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
		        </div>';
            $i = 1;
            foreach ($invoices as $inv) {

                $new_inv .= '<div class="form-group" >

		        <div class="col-md-1 text-right"><input type="checkbox" class="chk_pay"  id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
		        <div class="col-md-2 text-right">' . $inv['refNumber'] . '</div>
		        <div class="col-md-3 text-right">' . date("M d,Y", strtotime($inv['DueDate'])) . '</div>
		        <div class="col-md-2 text-right">$' . number_format($inv['BalanceRemaining'], 2) . '</div>
		       <div class="col-md-3 text-left"><input type="text" id="invc' . $i . '"  name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position(this);"  class="form-control   multiinv' . $inv['invoiceID'] . ' geter" data-id="multiinv' . $inv['invoiceID'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
		       </div>';

            }

            $card         = '';
            $card_name    = '';
            $customerdata = array();

            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('Freshbooks_custom_customer', $condition);
            if (!empty($customerdata)) {
                $customerdata['status'] = 'success';

                $card_data                = $this->card_model->getCustomerCardDataByID($customerID);
                $customerdata['card']     = $card_data;
                $customerdata['invoices'] = $new_inv;

                echo json_encode($customerdata);
                die;
            }

        }

    }

    public function get_customer_data_ajax()
    {
        $showEmail = false;
        if ($this->session->userdata('logged_in')) {
            $user_id         = $this->session->userdata('logged_in')['merchID'];
            $merchantEmailID = $this->session->userdata('logged_in')['merchantEmail'];

            $merchID   = $user_id;
            $showEmail = true;
        } else if ($this->session->userdata('user_logged_in')) {
            $user_id         = $this->session->userdata('user_logged_in')['merchantID'];
            $merchantEmailID = $this->session->userdata('user_logged_in')['merchant_data']['merchantEmail'];

            $merchID = $user_id;
            if (in_array('Send Email', $this->session->userdata('user_logged_in')['authName'])) {
                $showEmail = true;
            }
        }
        $type = $this->czsecurity->xssCleanPostInput('type');
        if ($type == 0) {
          $customer_data = $this->fb_customer_model->get_all_customer_details($merchID, '0');
        } else {
          $customer_data = $this->fb_customer_model->get_all_customer_details($merchID, '1');
        }

        $plantype         = $this->general_model->chk_merch_plantype_status($merchID);
        $data['plantype'] = $plantype;
        $data             = array();
        $no               = $_POST['start'];

        $customers = $customer_data['result'];
        if (isset($customers) && $customers) {
            foreach ($customers as $customer) {
                $customerEmailId        = $customer['Customer_ListID'];
                $customerEmailName      = $customer['firstName'] . ' ' . $customer['lastName'];
                $customerEmail          = $customer['userEmail'];
                $customerEmailCompanyID = $customer['companyID'];

                $emailAnchor = "<div class='text-left'>$customerEmail";
                if ($showEmail) {
                    $emailAnchor = "<div class='text-left cust_view'><a href='#set_tempemail_freshbook' onclick=\"set_template_data_fb('$customerEmailId','$customerEmailName', '$customerEmailCompanyID','$customerEmail','$merchantEmailID')\" title='' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-original-title=''>$customerEmail</a>";
                }
                $base_url1 = base_url() . "FreshBooks_controllers/Freshbooks_Customer/view_customer/" . $customer['Customer_ListID'];
                $no++;
                $row   = array();
                $row[] = "<div class='text-left cust_view'><a href='" . $base_url1 . "' >" . $customer['fullName'] . "</a>";
                $row[] = "<div class='text-left'>" . $customer['firstName'] . ' ' . $customer['lastName'] . "";
                $row[] = $emailAnchor;
                $balance = ($customer['Balance']) ? number_format($customer['Balance'], 2) : '0.00';
                $row[]   = "<div class='text-right'>" . $balance . "</div>";
                $data[]  = $row;
            }
        }
        if ($type == 0) {
            $count = 0;
        } else {
            $count = $this->fb_customer_model->get_all_customer_count($merchID, '1');

        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $count,
            "recordsFiltered" => $count,
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
        die;
    }
}
