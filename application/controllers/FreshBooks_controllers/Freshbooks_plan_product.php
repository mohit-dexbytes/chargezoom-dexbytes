<?php

class Freshbooks_plan_product extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();
        include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';

        $this->load->model('general_model');
        $this->load->model('Freshbook_models/freshbooks_model');

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
            $this->merchantID = $this->session->userdata('logged_in')['merchID'];
            $this->resellerID = $this->session->userdata('logged_in')['resellerID'];

        } else if ($this->session->userdata('user_logged_in') != "") {

            $merchID          = $this->session->userdata('user_logged_in')['merchantID'];
            $this->merchantID = $merchID;
            $rs_Data          = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];

        } else {
            redirect('login', 'refresh');
        }

        $val = array(
            'merchantID' => $this->merchantID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
        if (empty($Freshbooks_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
            redirect('FreshBooks_controllers/home/index', 'refresh');

        }
        $this->subdomain           = $Freshbooks_data['sub_domain'];
        $key                       = $Freshbooks_data['secretKey'];
        $this->accessToken         = $Freshbooks_data['accessToken'];
        $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
        $this->fb_account          = $Freshbooks_data['accountType'];
        $condition1                = array('resellerID' => $this->resellerID);
        $sub1                      = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1                   = $sub1['merchantPortalURL'];
        $URL1                      = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        define('OAUTH_CONSUMER_KEY', $this->subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);

    }

    public function index()
    {

        redirect('FreshBooks_controllers/home', 'refresh');
    }

    public function create_product_services()
    {

        $merchID = $this->merchantID;

        $val = array(
            'merchantID' => $merchID,
        );

        if (isset($this->accessToken) && isset($this->access_token_secret)) {

            if ($this->fb_account == 1) {
                $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $this->subdomain, $this->accessToken, $this->access_token_secret);
            }
            if ($this->fb_account == 2) {
                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $this->subdomain, $this->accessToken, $this->access_token_secret);
            }

            if (!empty($this->input->post(null, true))) {

                $productName = $this->czsecurity->xssCleanPostInput('productName');
                $productDesc = $this->czsecurity->xssCleanPostInput('productDesc');
                $salePrice   = $this->czsecurity->xssCleanPostInput('salePrice');
                $quantity    = $this->czsecurity->xssCleanPostInput('quantity');
                $type        = $this->czsecurity->xssCleanPostInput('ser_type');
                $createdat   = date('Y-m-d H:i:s');

                $isService        = $this->czsecurity->xssCleanPostInput('isService');

                if ($type == 'Inventory') {
                    if ($this->czsecurity->xssCleanPostInput('stock') != "") {
                        $stock = $this->czsecurity->xssCleanPostInput('stock');
                    } else {
                        $stock = 0;
                    }

                    if ($this->czsecurity->xssCleanPostInput('productID') != "") {

                        $itemid = $this->czsecurity->xssCleanPostInput('productID');
                        if ($this->fb_account == 1) {

                            if($isService == 1){
                                $items = [
                                    "service_id" => $itemid,
                                    "name" => "$productName",
                                    "description" => "$productDesc",
                                ];
                                $items = $c->update_services($item);

                                $item_rate = [
                                    "service_id" => "$itemid",
                                    "rate" => $salePrice,
                                ];
                                $items = $c->update_service_rate($item_rate);
                            } else {
                                $item   = array('itemid' => $itemid, 'name' => $productName, 'description' => $productDesc, 'unit_cost' => array('amount' => $salePrice, 'code' => 'USD'), 'qty' => $quantity, 'inventory' => $stock);
                                $items = $c->update_items($item);
                            }
                        }
                        if ($this->fb_account == 2) {

                            $item = [
                                "itemid" =>  $itemid,
                                "name" => $productName,
                                "description" => $productDesc,
                                "qty" => $quantity,
                                "inventory" => $stock,
                                "unit_cost" => [
                                    "amount" => $salePrice,
                                    "code" => "USD"
                                ]
                            ];

                            $items = $c->add_items($item);
                        }

                        
                        $this->session->set_flashdata('success', 'Success');
	
                    } else {

                        if ($this->fb_account == 1) {
                            $item = array('itemid' => '', 'name' => $productName, 'description' => $productDesc, 'unit_cost' => array('amount' => $salePrice, 'code' => 'USD'), 'qty' => $quantity, 'inventory' => $stock);

                            $items = $c->add_items($item);
                        }
                        if ($this->fb_account == 2) {
                            $item = [
                                "name" => $productName,
                                "description" => $productDesc,
                                "qty" => $quantity,
                                "unit_cost" => [
                                    "amount" => $salePrice,
                                    "code" => "USD"
                                ]
                            ];

                            $items = $c->add_items($item);
                        }

                        
                        $this->session->set_flashdata('success', 'Success');
                    }

                } else {
                    if ($this->czsecurity->xssCleanPostInput('productID') != "") {
                        $itemid = $this->czsecurity->xssCleanPostInput('productID');
                        if($isService == 1){
                            $item = [
                                "service_id" => $itemid,
                                "name" => "$productName",
                                "description" => "$productDesc",
                            ];
                            $items = $c->update_services($item);

                            $item_rate = [
                                "service_id" => "$itemid",
                                "rate" => $salePrice,
                            ];
                            $items = $c->update_service_rate($item_rate);
                        } else {
                            $item = [
                                "itemid" =>  $itemid,
                                "name" => $productName,
                                "description" => $productDesc,
                                "qty" => $quantity,
                                "unit_cost" => [
                                    "amount" => $salePrice,
                                    "code" => "USD"
                                ]
                            ];
    
                            $items = $c->update_items($item);
                        }
                        $this->session->set_flashdata('success', 'Success');

                    } else {
                        if($isService == 1){
                            $item = [
                                "name" => "$productName",
                                "description" => "productDesc",
                            ];
                            $items = $c->add_services($item);

                            $item_rate = [
                                "service_id" => "$items",
                                "rate" => $salePrice,
                            ];
                            $items = $c->add_service_rate($item_rate);
                        } else {
                            $item = [
                                "name" => $productName,
                                "description" => $productDesc,
                                "qty" => $quantity,
                                "unit_cost" => [
                                    "amount" => $salePrice,
                                    "code" => "USD"
                                ]
                            ];
    
                            $items = $c->add_items($item);
                        }

                       
                        $this->session->set_flashdata('success', 'Success');
                    }
                }

                redirect(base_url('FreshBooks_controllers/Freshbooks_plan_product/Item_detailes'));
            }
        }

        if ($this->uri->segment('4')) {
            $productID        = $this->uri->segment('4');
            $con              = array('productID' => $productID);
            $data['item_pro'] = $this->general_model->get_row_data('Freshbooks_test_item', $con);
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/create_product', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function Item_detailes()
    {

        $merchID   = $this->merchantID;
        $condition = array('merchantID' => $merchID);

        $val = array(
            'merchantID' => $merchID,
        );

        $resellerID = $this->resellerID;

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $fb_data   = $this->general_model->get_select_data('tbl_fb_config', array('lastUpdated'), array('merchantID' => $merchID, 'fb_action' => 'ItemQuery'));
        $last_date = $current_date = '';
        if (!empty($fb_data)) {
            $last_date    = $fb_data['lastUpdated'];
            $current_date = date('Y-m-d H:i:s');
            $my_timezone  = date_default_timezone_get();
            $from         = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
            $to           = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
            $last_date    = $this->general_model->datetimeconv($last_date, $from, $to);
            $current_date = $this->general_model->datetimeconv($current_date, $from, $to);

            $last_date    = date('Y-m-d H:i:s', strtotime('-6 hour', strtotime($last_date)));
            $current_date = date('Y-m-d H:i:s', strtotime('-3 hour', strtotime($current_date)));

        }

        if (!empty($fb_data)) {
            $updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
            $updatedata['updatedAt']   = date('Y-m-d H:i:s');

            $this->general_model->update_row_data('tbl_fb_config', array('merchantID' => $merchID, 'fb_action' => 'ItemQuery'), $updatedata);
        } else {
            $updateda = date('Y-m-d') . 'T' . date('H:i:s');
            $upteda   = array('merchantID' => $merchID, 'fb_action' => 'ItemQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
            $this->general_model->insert_row('tbl_fb_config', $upteda);
        }

        if (isset($this->accessToken) && isset($this->access_token_secret)) {
            if ($this->fb_account == 1) {
                $c     = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $this->subdomain, $this->accessToken, $this->access_token_secret);
                $items = $c->get_items_data();
                foreach ($items as $oneItem) {

                    $FB_item_details = array(
                        "productID"        => $oneItem->itemid,
                        "Name"             => $this->db->escape_str($oneItem->name),
                        "SalesDescription" => $this->db->escape_str($oneItem->description),
                        "saleCost"         => $this->db->escape_str($oneItem->unit_cost->amount),
                        "QuantityOnHand"   => $this->db->escape_str($oneItem->qty),
                        "TimeModified"     => $this->db->escape_str($oneItem->updated),
                        "Inventory"        => $this->db->escape_str(($oneItem->inventory)),
                        "Tax1"             => $this->db->escape_str(($oneItem->tax1)),
                        "Tax2"             => $this->db->escape_str(($oneItem->tax2)),
                        "IsActive"         => $this->db->escape_str(($oneItem->vis_state == 0) ? 1 : '0'),
                        "merchantID"       => $this->merchantID,
                        "fbCompanyID"      => $oneItem->accounting_systemid,
                    );

                    if ($this->general_model->get_num_rows('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->itemid)) > 0) {
                        $this->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->itemid), $FB_item_details);
                    } else {
                        $this->general_model->insert_row('Freshbooks_test_item', $FB_item_details);
                    }

                }

                $services = $c->get_service_data();
                foreach ($services as $services) {
                    $FB_services_details = array(
                        "productID"   => $services->id,
                        "Name"        => $this->db->escape_str($services->name),
                        "saleCost"    => $this->db->escape_str(($services->priceDetails == 0) ? '0' : $services->priceDetails),
                        "IsActive"    => $this->db->escape_str(($services->vis_state == 0) ? 1 : '0'),
                        "merchantID"  => $this->merchantID,
                        "fbCompanyID" => $services->business_id,
                        "isService"   => 1,
                    );

                    if ($this->general_model->get_num_rows('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $services->id)) > 0) {
                        $this->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $services->id), $FB_services_details);
                    } else {
                        $this->general_model->insert_row('Freshbooks_test_item', $FB_services_details);
                    }

                }
            }
            if ($this->fb_account == 2) {
                $c     = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $this->subdomain, $this->accessToken, $this->access_token_secret);
                $items = $c->get_items_data();
                foreach ($items as $oneItem) {

                    $FB_item_details = array(
                        "productID"        => $oneItem->item_id,
                        "Name"             => $this->db->escape_str($oneItem->name),
                        "SalesDescription" => $this->db->escape_str($oneItem->description),
                        "saleCost"         => $this->db->escape_str($oneItem->unit_cost),
                        "QuantityOnHand"   => $this->db->escape_str($oneItem->quantity),
                        "TimeModified"     => $this->db->escape_str($oneItem->updated),
                        "Inventory"        => $this->db->escape_str(($oneItem->inventory) ? $oneItem->inventory : ''),
                        "Tax1"             => $this->db->escape_str(($oneItem->tax1_id) ? $oneItem->tax1_id : ''),
                        "Tax2"             => $this->db->escape_str(($oneItem->tax2_id) ? $oneItem->tax2_id : ''),
                        "IsActive"         => $this->db->escape_str(($oneItem->folder == 'active') ? '1' : '0'),
                        "merchantID"       => $merchID,
                    );
                    if (!empty($this->general_model->get_row_data('Freshbooks_test_item', array('merchantID' => $merchID, 'productID' => $oneItem->item_id)))) {
                        $this->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->item_id), $FB_item_details);
                    } else {
                        $this->general_model->insert_row('Freshbooks_test_item', $FB_item_details);
                    }

                }
            }

        }

        $data['plans'] = $this->general_model->get_table_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'IsActive' => 1));

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/plan_products', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function delete_item()
    {

        if ($this->czsecurity->xssCleanPostInput('productID') != "") {
            if (isset($this->accessToken) && isset($this->access_token_secret)) {
                if ($this->fb_account == 1) {
                    $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $this->subdomain, $this->accessToken, $this->access_token_secret);

                    $itemid = $this->czsecurity->xssCleanPostInput('productID');
                    $item   = array('itemid' => $itemid, 'vis_state' => 1);
                    $items  = $c->update_items($item);
                    if ($items) {
                        $this->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $itemid), array('IsActive' => '0', 'TimeModified' => date('Y-m-d H:i:s')));

                    }

                } else {
                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $this->subdomain, $this->accessToken, $this->access_token_secret);

                    $item_id = $this->czsecurity->xssCleanPostInput('productID');

                    $item = '<request method="item.delete">
                            <item_id>' . $item_id . '</item_id>
						  </request> ';

                    $items = $c->add_items($item);
                }
                
                $this->session->set_flashdata('success', 'Successfully Deleted');
                
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }
        redirect(base_url('FreshBooks_controllers/Freshbooks_plan_product/Item_detailes'));

    }


    public function plan_product_details()
    {

        if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $productID = $this->czsecurity->xssCleanPostInput('productID');
        $condition = array('productID' => $productID, 'merchantID' => $user_id);
        $plandata  = $this->general_model->get_row_data('Freshbooks_test_item', $condition);
        if (!empty($plandata)) {
            ?>
		 <table class="table table-bordered table-striped table-vcenter"   >
            <thead>
                <tr>
                    <th class="text-left">Attribute</th>
                    <th class="visible-lg text-left">Details</th>
                </tr>
            </thead>
            <tbody>

            <tr>
                <th class="text-left"><strong>Item ID</strong></th>
                <td class="text-left visible-lg"><?php echo $plandata['productID']; ?></a></td>
            </tr>
			<tr>
                <th class="text-left"><strong>Name</strong></th>
                <td class="text-left visible-lg"><?php echo $plandata['Name']; ?></a></td>
			</tr>
			<tr>
                <th class="text-left"><strong>Description</strong></th>
                <td class="text-left visible-lg"><?php echo ($plandata['SalesDescription']) ? $plandata['SalesDescription'] : '---'; ?></a></td>
			</tr>
           <tr>
                <th class="text-left"><strong>Inventory</strong></th>
                <td class="text-left visible-lg"><?php echo $plandata['Inventory']; ?></a></td>
			</tr>
            <tr>
                <th class="text-left"><strong>  Quantity On Hand</strong></th>
                <td class="text-left visible-lg"><?php echo ($plandata['QuantityOnHand']) ? $plandata['QuantityOnHand'] : '--'; ?></a></td>
			</tr>
            	<tr>
					<th class="text-left"><strong>  Sales Price</strong></th>
					<td class="text-left visible-lg">$<?php echo number_format($plandata['saleCost'], 2); ?></a></td>
			</tr>

			<tr>
                <th class="text-left"><strong>  Modified Time</strong></th>
                <td class="text-left visible-lg"><?php if (!empty($plandata['TimeModified'])) {
                    echo date('M d, Y', strtotime($plandata['TimeModified']));
                } else {
                    echo '';
                }
                ?></a>
                </td>
			</tr>


			</tbody>
        </table>

	<?php }

        die;

    }

    public function product_ajax(){
        if($this->session->userdata('logged_in')){
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in');
			$merchID = $user_id['merchantID'];
		}
        $cond                  = array("merchantID" => $merchID);
        $plans = $this->freshbooks_model->get_product_table_data('Freshbooks_test_item', $cond);
        $count = $this->freshbooks_model->get_product_table_count('Freshbooks_test_item', $cond);
        $data = array();
		$no = $_POST['start'];
        if(isset($plans) && $plans)
        {
            foreach($plans as $plan)
            {
                $type = ($plan['isService'] == 0) ? "Item" : "Service";
                $no++;
                $row = array();
                $url = base_url('FreshBooks_controllers/Freshbooks_plan_product/create_product_services/'.$plan['productID']);
                $row[] = "<div class='text-left cust_view'><a href='" . $url . "' data-toggle='tooltip' title='Edit' >".$plan['Name']."</a>";
                $row[] = "<div class='text-left hidden-xs'>".$plan['SalesDescription']."";
                $row[] = "<div class='hidden-xs text-right'>$type";
                $row[] = "<div class='hidden-xs text-right'>".number_format($plan['QuantityOnHand'],2)."";
                $row[] = "<div class='hidden-xs text-right'>$".number_format($plan['saleCost'],2)."";
                
                $row[] = "<div class='hidden-xs text-right'><div class='btn-group btn-group-xs'><a href='#plan_product' class='btn btn-sm btn-default' onclick='view_plan_details(".$plan['productID'].");'  data-backdrop='static' data-keyboard='false'  title='View' data-toggle='modal'><i class='fa fa-eye'></i></a>
                    
                <a href='#del_plan_product' onclick='set_del_plan_product(".$plan['productID'].",'0');' data-backdrop='static' data-keyboard='false' data-toggle='modal'  title='Deactivate Product' class='btn btn-danger'> <i class='fa fa-ban'> </i> </a>";
                
                // if($plan['IsActive']=='true'){
                    
                // }
                // if($plan['IsActive']=='false'){
                //     $row[] = "<a href='#del_plan_product' onclick='set_del_plan_product(".$plan['productID'].",'1');' data-backdrop='static' data-keyboard='false' data-toggle='modal'  title='Activate Product'  class='btn btn-info'><i class='fa fa-check'></i></a>";
                // }
                $data[] = $row;
            }
        }
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" =>  $count,
			"recordsFiltered" => $count,
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
		die;
    }
}