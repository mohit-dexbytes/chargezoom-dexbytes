<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;


class CheckPlan extends CI_Controller {
    
	function __construct()
	{
		parent::__construct();
	    include APPPATH . 'third_party/nmiDirectPost.class.php';
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('QBO_models/qbo_company_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->library('encrypt');
	}
	
	public function index(){
	    
	    
	}
	
	
	
	public function thankyou(){
	   
	   
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$this->load->view('template/template_start', $data);
		$this->load->view('QBO_views/thankyou', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data); 
	    
	}
	
	public function check_out(){
	    
	    $marchent_id = $this->uri->segment(4);
	    $plan = $this->uri->segment(5);
	    $marcid = base64_decode($marchent_id);
	    $con = array('merchantID'=>$marcid);
	    $con1 = array('merchantID'=>$marcid,'set_as_default'=>'1');
	    
	   	$result = $this->general_model->get_row_data('app_integration_setting',$con);
	    
	    switch ($result['appIntegration']) {
            case "1":
                
                $data = $this->general_model->get_row_data('QBO_token',$con);
                
        		$accessToken = $data['accessToken'];
        		$refreshToken = $data['refreshToken'];
        	    $realmID      = $data['realmID']; 
        		$dataService = DataService::Configure(array(
        						 'auth_mode' => 'oauth2',
						 'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
						 'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
						));
                $con = "merchantDataID = '".$marcid."'  AND  postPlanURL = '".$plan."'";
                $splan = $this->general_model->get_rows('tbl_subscriptions_plan_qbo',$con);
                $data['selected_plan'] = $splan;
                
                $con = "planID = '". $splan->planID ."'";
            	$item = $this->general_model->get_rows('tbl_subscription_plan_item_qbo',$con);
            						     
                if(!empty($this->czsecurity->xssCleanPostInput('pay'))){
                     $this->load->library('form_validation');
                    $this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|max_length[20]|is_unique[tbl_customer_login.customerUsername]',
                        array(
                                'required'      => 'You have not provided %s.',
                                'is_unique'     => 'This %s already exists.'
                        )
                    );
                    $this->form_validation->set_rules('email', 'Email', 'required');
                    $this->form_validation->set_rules('mobile', 'Mobile', 'required');
                    $this->form_validation->set_rules('address', 'Address', 'required');
                    $this->form_validation->set_rules('zip', 'Zip Code', 'required');
                    $this->form_validation->set_rules('city', 'City', 'required');
                    $this->form_validation->set_rules('state', 'State', 'required');
                    $this->form_validation->set_rules('first_name', 'First Name', 'required');
                    $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                    
                    $this->form_validation->set_rules('cardNumber', 'Card Number', 'required');
                    $this->form_validation->set_rules('cardfriendlyname', 'Card Friendly Name', 'required');
                    $this->form_validation->set_rules('cardExpiry', 'Card Expiry', 'required');
                    $this->form_validation->set_rules('cardCVC', 'Card CVC', 'required');
                    $this->form_validation->set_rules('cardExpiry2', 'Card Expiry Year', 'required');
                    if ($this->form_validation->run() == TRUE)
                        {
                           session_start(); 
                           
                            $sessiondata = array(
                               'customer'  => $this->czsecurity->xssCleanPostInput('cname'),
                               'email'  =>  $this->czsecurity->xssCleanPostInput('email'),
                               'username' =>  $this->czsecurity->xssCleanPostInput('username'),
                               'password' =>  $this->czsecurity->xssCleanPostInput('password'),
                               'fname'  =>  $this->czsecurity->xssCleanPostInput('first_name'),
                               'lname'  => $this->czsecurity->xssCleanPostInput('last_name'),
                               'mobile' =>  $this->czsecurity->xssCleanPostInput('mobile'),
                               'country' =>  $this->czsecurity->xssCleanPostInput('country'),
                               'zip'  =>  $this->czsecurity->xssCleanPostInput('zip'),
                               'add1'     =>  $this->czsecurity->xssCleanPostInput('address'),
                               'add2' =>  $this->czsecurity->xssCleanPostInput('address2'),
                               'city'  => $this->czsecurity->xssCleanPostInput('city'),
                               'state' =>  $this->czsecurity->xssCleanPostInput('state'),
                               
                               'scountry' =>$this->czsecurity->xssCleanPostInput('scountry'),
                               'szip' =>  $this->czsecurity->xssCleanPostInput('szip'),
                               'sadd1' =>  $this->czsecurity->xssCleanPostInput('saddress'),
                               'sadd2'  =>   $this->czsecurity->xssCleanPostInput('saddress2'),
                               'scity'     =>  $this->czsecurity->xssCleanPostInput('scity'),
                               'sstate' =>  $this->czsecurity->xssCleanPostInput('sstate'),
                               'key' => $this->czsecurity->xssCleanPostInput('key'),
                               'token'=> $this->czsecurity->xssCleanPostInput('stripeToken'),
                                'card'     =>  $this->czsecurity->xssCleanPostInput('cardNumber'),
                               'cardfriendlyname'=> $this->czsecurity->xssCleanPostInput('cardfriendlyname'),
                               'ccv' =>  $this->czsecurity->xssCleanPostInput('cardCVC'),
                               'exp1' =>  $this->czsecurity->xssCleanPostInput('cardExpiry'),
                               'exp2'  =>  $this->czsecurity->xssCleanPostInput('cardExpiry2'),
                               'marchantid'     => $marcid
                               
                             );
                            $this->session->set_userdata('records', $sessiondata);
							$cust_sess= $this->session->userdata('records');
						   // print_r($cust_sess);
							$customerObj = Customer::create([
        							     
        								 "GivenName"=>  $cust_sess['fname'],
        								 "MiddleName"=>  $cust_sess['lname'],
        								 "FullyQualifiedName"=>  $cust_sess['fname'].' '.$cust_sess['lname'],
        								 "CompanyName"=>  'AfBC',
        								 "PrimaryPhone"=>  [
        									 "FreeFormNumber"=>  $cust_sess['mobile'],
        									 "CountryCode"=>'US'
        								 ],
        								 "PrimaryEmailAddr"=>  [
        									 "Address" => $cust_sess['email']
        								 ],
        								 "BillAddr"=>  [
        									 "Line1" => $cust_sess['add1'],
        									 "Line2" => $cust_sess['add2'],
        									 "PostalCode" => $cust_sess['zip'],
        									 "Country" => $cust_sess['country'],
        									 "CountrySubDivisionCode" =>$cust_sess['state'],
        									 "City" => $cust_sess['city']
        								 ],
        						    	"ShipAddr"=>  [
        									 "Line1" => $cust_sess['sadd1'],
        									 "Line2" =>$cust_sess['sadd2'],
        									 "PostalCode" => $cust_sess['szip'],
        									 "Country" => $cust_sess['scountry'],
        									 "CountrySubDivisionCode" =>$cust_sess['sstate'],
        									 "City" => $cust_sess['scity']
        								 ]
        								]);
        							
        				    $resultingCustomerObj = $dataService->Add($customerObj);
        				   	$error = $dataService->getLastError(); 
        				   
        				   if(!empty($resultingCustomerObj->Id)){
								$amount = $this->czsecurity->xssCleanPostInput('amount');	
								$con = "merchantID = '".$marcid."'  AND  set_as_default = '1'";
								$chk_gateway = $this->general_model->get_rows('tbl_merchant_gateway',$con);
                          
								if($chk_gateway->gatewayType ==1 || $chk_gateway->gatewayType == 9)
								{
									
									$gatewayuser = $chk_gateway->gatewayUsername;
									$gatewaypass = $chk_gateway->gatewayPassword;
									$marchantID =  $chk_gateway->gatewayMerchantID;
									$nmi_data  = array('nmi_user'=>$gatewayuser, 'nmi_password'=>$gatewaypass);
									$transaction = new nmiDirectPost($nmi_data);
									
									if( !empty($this->czsecurity->xssCleanPostInput('cardNumber')) ){	
										$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('cardNumber'));
										$expmonth =  $this->czsecurity->xssCleanPostInput('cardExpiry');
										if(strlen($expmonth)==1){
												$expmonth = '0'.$expmonth;
											}
										$exyear   = $this->czsecurity->xssCleanPostInput('cardExpiry2');
										$exyear   = substr($exyear,2);
										$expry    = $expmonth.$exyear; 
										$transaction->setCcExp($expry);
										$transaction->setCvv($this->czsecurity->xssCleanPostInput('cardCVC'));
										
										// add level III data
										$level_request_data = [
											'transaction' => $transaction,
											'card_no' => $this->czsecurity->xssCleanPostInput('cardNumber'),
											'merchID' => $marcid,
											'amount' => $amount,
											'invoice_id' => $splan->generatedInvoice,
											'gateway' => 1
										];
										$transaction = addlevelThreeDataInTransaction($level_request_data);
								
								  }	
										$transaction->setCompany('ABC');
										$transaction->setFirstName($this->czsecurity->xssCleanPostInput('fistName'));
										$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
										$transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
										$transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
										$transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
										$transaction->setState($this->czsecurity->xssCleanPostInput('state'));
										$transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
										$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));
									
										$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
										$transaction->setAmount($amount);
										$transaction->setTax('tax');
										$transaction->sale();
										$result = $transaction->execute();
										
									
										
										
										if($result['response_code'] == '100') {
										 
										 
											$custID =  $resultingCustomerObj->Id;
											$customer_login = array(
												'customerEmail'=>$cust_sess['email'],
												'customerPassword'=>$cust_sess['password'],
												'createdAt'=>date('Y-m-d H:i:s'),
												'is_logged_in'=>$cust_sess['email'],
												'loginCode'=>'xyz',
												'isEnable'=>'0',
												'customerUsername'=>$cust_sess['username'],
												'merchantID'=>$cust_sess['marchantid'],
												'customerID'=>$custID
											 );
										   //inser here customer login table
										  
										   //customer card
										   $card_type = $this->general_model->getType($cust_sess['card']);

										    $is_default = 0;
									     	$checkCustomerCard = checkCustomerCard($custID,$cust_sess['marchantid']);
								        	if($checkCustomerCard == 0){
								        		$is_default = 1;
								        	}	
										 
										   $card_data = array(
											  'cardMonth'   =>$cust_sess['exp1'],
											  'cardYear' =>$cust_sess['exp2'],
											  'CardType'=>$card_type,
											  'CustomerCard' =>$this->card_model->encrypt($cust_sess['card']),
											  'CardCVV'      =>$this->card_model->encrypt($cust_sess['ccv']), 
											  'customerListID' =>$custID,                           
											  'Billing_Addr1'=> $cust_sess['add1'],
											  'Billing_Addr2'=> $cust_sess['add2'],
											  'Billing_City'=> $cust_sess['city'],
											  'Billing_Country'=>$cust_sess['country'],
											  'Billing_Contact'=> $cust_sess['mobile'],
											  'Billing_State'=> $cust_sess['state'],
											  'Billing_Zipcode'=> $cust_sess['zip'],
											  'merchantID'   => $cust_sess['marchantid'],
											  'is_default'			   => $is_default,
											  'customerCardfriendlyName'=>$cust_sess['cardfriendlyname'],
											  'createdAt' 	=> date("Y-m-d H:i:s") 
											);
											
											   
										   
											 //qbo custom customer
											 
											 $qbo_customer = array(
											   'Customer_ListID'=>  $custID,
											   'firstName'=>  $cust_sess['fname'],
											   'lastName'=>  $cust_sess['lname'],
											   'fullName'=> $cust_sess['fname'].' '.$cust_sess['lname'],
											   'userEmail'=>  $cust_sess['email'],
											   'phoneNumber'=>  $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=>  $cust_sess['add2'],
											   'Country'=>  $cust_sess['country'],
											   'State'=>  $cust_sess['state'],
											   'City'=>  $cust_sess['city'],
											   'zipCode'=>  $cust_sess['zip'],
											   'ship_address1'=> $cust_sess['sadd1'],
											   'ship_address2'=>  $cust_sess['sadd2'],
											   'ship_country'=>  $cust_sess['scountry'],
											   'ship_state'=>  $cust_sess['sstate'],
											   'ship_city'=>  $cust_sess['scity'],
											   'ship_zipcode'=>  $cust_sess['szip'],
											   'companyName'=>  $resultingCustomerObj->CompanyName,
											   'companyID'=>  $custID,
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'listID'=>  '1',
											   'merchantID'=>  $marcid
											 );
											 
											 
											 
											 $tbl_subscriptions_qbo = array(
											   'customerID'=>  $custID,
											   'merchantDataID'=>  $marcid,
											   'subscriptionName'=> $splan->planName,
											   'subscriptionPlan'=>$splan->subscriptionPlan,
											   'freeTrial'=>$splan->freeTrial,
											   'invoiceFrequency'=>$splan->invoiceFrequency,
											   'subscriptionAmount'=>$splan->subscriptionAmount,
											   'totalInvoice'=>$splan->totalInvoice,
											   'generatedInvoice'=>$splan->generatedInvoice,
											   
											   'totalAmount'=> $splan->totalAmount,
											   'paidAmount'=>$splan->paidAmount,
											   'generatingDate'=>date("Y-m-d H:i:s"),
											   'firstDate'=>date("Y-m-d H:i:s"),
											   'nextGeneratingDate'=>date("Y-m-d H:i:s"),
											   'startDate'=>date("Y-m-d H:i:s"),
											   'endDate'=>date("Y-m-d H:i:s"),
											   
											   'paymentGateway'=> $splan->paymentGateway,
											   'automaticPayment'=>$splan->automaticPayment,
											   'emailRecurring'=>$splan->emailRecurring,
											   'usingExistingAddress'=>$splan->usingExistingAddress,
											   'cardID'=>$splan->subscriptionPlan,
											   
											   'contactNumber'=> $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=> $cust_sess['add2'],
											   'country'=>  $cust_sess['country'],
											   'state'=>$cust_sess['state'],
											   'city'=>  $cust_sess['city'],
											   'zipcode'=>$cust_sess['zip'],
											   
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'taxID'=>  '1',
											   'planID'=>  $splan->planID
											 );
											 
										   
											 //qbo item
											 
											$this->db->trans_begin();
												$this->general_model->insert_row('tbl_customer_login', $customer_login);
												$this->general_model->insert_row('QBO_custom_customer', $qbo_customer);
												$this->db1->insert('customer_card_data', $card_data);
											   
											
											if ($this->db->trans_status() === FALSE)
											{
												 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong></div>'); 
													$this->db->trans_rollback();
													
											}
											else
											{
												
												$insertId =  $this->general_model->insert_row('tbl_subscriptions_qbo', $tbl_subscriptions_qbo);
												$qbo_item = array(
												   'itemListID'=>  $item->itemListID,
												   'itemFullName'=>  $item->itemFullName,
												   'itemDescription'=> $item->itemDescription,
												   'itemQuantity'=>$item->itemQuantity,
												   'itemRate'=> $item->itemRate,
												   'itemTax'=>  $item->itemTax,
												   'oneTimeCharge'=> $item->oneTimeCharge,
												   'subscriptionID'=> $insertId
												 );
												$insertId =  $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $qbo_item);
												if($insertId){
											         $this->db->trans_commit();
												}
											   }
											   
											$this->session->set_flashdata('success', 'Success');
										    	
										    	redirect('/QBO_controllers/CheckPlan/thankyou/');
										}else{
										  
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['responsetext'].'</div>'); 
											
										}
								}
								//second gateway
								elseif($chk_gateway->gatewayType ==2){
									include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
									$apiloginID  = $chk_gateway->gatewayUsername;
						        	$transactionKey  = $chk_gateway->gatewayPassword;
									$transaction = new AuthorizeNetAIM($apiloginID,$transactionKey); 
									if( !empty($cust_sess['card'])){	
									   
										$card_no  = $cust_sess['card'];
										$expmonth =  $cust_sess['exp1'];
										if(strlen($expmonth)==1){
												$expmonth = '0'.$expmonth;
											}
										$exyear   = substr($cust_sess['exp2'],2);
										$expry    = $expmonth.$exyear;
									}
									$transaction->__set('company','ABC');
									$transaction->__set('first_name', $cust_sess['fname']);
									$transaction->__set('last_name',  $cust_sess['lname']);
									$transaction->__set('address',  $cust_sess['add1']);
									$transaction->__set('country', $cust_sess['country']);
									$transaction->__set('city', $cust_sess['city']);
									$transaction->__set('state', $cust_sess['state']);
									$transaction->__set('phone', $cust_sess['mobile']);
									$transaction->__set('email', $cust_sess['email']);
									$amount = $amount;
									$result = $transaction->authorizeAndCapture($amount,$card_no,$expry);
									
									if($result->response_code == '1'){
										
										$custID =  $resultingCustomerObj->Id;
											$customer_login = array(
												'customerEmail'=>$cust_sess['email'],
												'customerPassword'=>$cust_sess['password'],
												'createdAt'=>date('Y-m-d H:i:s'),
												'is_logged_in'=>$cust_sess['email'],
												'loginCode'=>'xyz',
												'isEnable'=>'0',
												'customerUsername'=>$cust_sess['username'],
												'merchantID'=>$cust_sess['marchantid'],
												'customerID'=>$custID
											 );
										   //inser here customer login table
										  
										   //customer card
										   $card_type = $this->general_model->getType($cust_sess['card']);	

										   $is_default = 0;
									     	$checkCustomerCard = checkCustomerCard($custID,$cust_sess['marchantid']);
								        	if($checkCustomerCard == 0){
								        		$is_default = 1;
								        	}
										 
										   $card_data = array(
											  'cardMonth'   =>$cust_sess['exp1'],
											  'cardYear' =>$cust_sess['exp2'],
											  'CardType'=>$card_type,
											  'CustomerCard' =>$this->card_model->encrypt($cust_sess['card']),
											  'CardCVV'      =>$this->card_model->encrypt($cust_sess['ccv']), 
											  'customerListID' =>$custID,                           
											  'Billing_Addr1'=> $cust_sess['add1'],
											  'Billing_Addr2'=> $cust_sess['add2'],
											  'Billing_City'=> $cust_sess['city'],
											  'Billing_Country'=>$cust_sess['country'],
											  'Billing_Contact'=> $cust_sess['mobile'],
											  'Billing_State'=> $cust_sess['state'],
											  'Billing_Zipcode'=> $cust_sess['zip'],
											  'merchantID'   => $cust_sess['marchantid'],
											  'is_default'			   => $is_default,
											  'customerCardfriendlyName'=>$cust_sess['cardfriendlyname'],
											  'createdAt' 	=> date("Y-m-d H:i:s") 
											);
											
											   
										   
											 //qbo custom customer
											 
											$qbo_customer = array(
											   'Customer_ListID'=>  $custID,
											   'firstName'=>  $cust_sess['fname'],
											   'lastName'=>  $cust_sess['lname'],
											   'fullName'=> $cust_sess['fname'].' '.$cust_sess['lname'],
											   'userEmail'=>  $cust_sess['email'],
											   'phoneNumber'=>  $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=>  $cust_sess['add2'],
											   'Country'=>  $cust_sess['country'],
											   'State'=>  $cust_sess['state'],
											   'City'=>  $cust_sess['city'],
											   'zipCode'=>  $cust_sess['zip'],
											   'ship_address1'=> $cust_sess['sadd1'],
											   'ship_address2'=>  $cust_sess['sadd2'],
											   'ship_country'=>  $cust_sess['scountry'],
											   'ship_state'=>  $cust_sess['sstate'],
											   'ship_city'=>  $cust_sess['scity'],
											   'ship_zipcode'=>  $cust_sess['szip'],
											   'companyName'=>  $resultingCustomerObj->CompanyName,
											   'companyID'=>  $custID,
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'listID'=>  '1',
											   'merchantID'=>  $marcid
											 );
											 
											 
											 
											 $tbl_subscriptions_qbo = array(
											   'customerID'=>  $custID,
											   'merchantDataID'=>  $marcid,
											   'subscriptionName'=> $splan->planName,
											   'subscriptionPlan'=>$splan->subscriptionPlan,
											   'freeTrial'=>$splan->freeTrial,
											   'invoiceFrequency'=>$splan->invoiceFrequency,
											   'subscriptionAmount'=>$splan->subscriptionAmount,
											   'totalInvoice'=>$splan->totalInvoice,
											   'generatedInvoice'=>$splan->generatedInvoice,
											   
											   'totalAmount'=> $splan->totalAmount,
											   'paidAmount'=>$splan->paidAmount,
											   'generatingDate'=>date("Y-m-d H:i:s"),
											   'firstDate'=>date("Y-m-d H:i:s"),
											   'nextGeneratingDate'=>date("Y-m-d H:i:s"),
											   'startDate'=>date("Y-m-d H:i:s"),
											   'endDate'=>date("Y-m-d H:i:s"),
											   
											   'paymentGateway'=> $splan->paymentGateway,
											   'automaticPayment'=>$splan->automaticPayment,
											   'emailRecurring'=>$splan->emailRecurring,
											   'usingExistingAddress'=>$splan->usingExistingAddress,
											   'cardID'=>$splan->subscriptionPlan,
											   
											   'contactNumber'=> $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=> $cust_sess['add2'],
											   'country'=>  $cust_sess['country'],
											   'state'=>$cust_sess['state'],
											   'city'=>  $cust_sess['city'],
											   'zipcode'=>$cust_sess['zip'],
											   
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'taxID'=>  '1',
											   'planID'=>  $splan->planID
											 );
											 
										   
											 //qbo item
											 
											$this->db->trans_begin();
												$this->general_model->insert_row('tbl_customer_login', $customer_login);
												$this->general_model->insert_row('QBO_custom_customer', $qbo_customer);
												$this->db1->insert('customer_card_data', $card_data);
											   
											
											if ($this->db->trans_status() === FALSE)
											{
												 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong></div>'); 
													$this->db->trans_rollback();
													
											}
											else
											{
												
												$insertId =  $this->general_model->insert_row('tbl_subscriptions_qbo', $tbl_subscriptions_qbo);
												$qbo_item = array(
												   'itemListID'=>  $item->itemListID,
												   'itemFullName'=>  $item->itemFullName,
												   'itemDescription'=> $item->itemDescription,
												   'itemQuantity'=>$item->itemQuantity,
												   'itemRate'=> $item->itemRate,
												   'itemTax'=>  $item->itemTax,
												   'oneTimeCharge'=> $item->oneTimeCharge,
												   'subscriptionID'=> $insertId
												 );
												$insertId =  $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $qbo_item);
												if($insertId){
												
												$this->db->trans_commit();
												}
											}
									   
											$this->session->set_flashdata('success', 'Success');
									    redirect('/QBO_controllers/CheckPlan/thankyou/');
									}else{
										  
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['responsetext'].'</div>'); 
											
										}
								}
								//second gateway end
								elseif($chk_gateway->gatewayType ==3){    //third gateway stat paytrace
								
								    include APPPATH . 'third_party/PayTraceAPINEW.php';
									$payusername   = $chk_gateway->gatewayUsername;
									$paypassword   = $chk_gateway->gatewayPassword;
									$integratorId   = $chk_gateway->gatewaySignature;
									$grant_type    = "password";
									$payAPI = new PayTraceAPINEW();
								    //set the properties for this request in the class
									$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
									
									//call a function of Utilities.php to verify if there is any error with OAuth token. 
									$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
									if(!$oauth_moveforward){
										//Decode the Raw Json response.
										$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
										$oauth_token = sprintf("Bearer %s",$json['access_token']);
										
											if( !empty($cust_sess['card'])){	
									   
        										$card_no  = $cust_sess['card'];
        										$expmonth =  $cust_sess['exp1'];
        										if(strlen($expmonth)==1){
        												$expmonth = '0'.$expmonth;
        											}
        										$exyear   = substr($cust_sess['exp2'],2);
        									}
									}
									
										$cvv     =  $cust_sess['ccv'];
										$name    = $cust_sess['fname'].' '.$cust_sess['lname'];
										$address =	$cust_sess['add1'];
										$country = $cust_sess['country'];
										$city    = $cust_sess['city'];
										$state   = $cust_sess['state'];
										$amount = $amount;		
										$zipcode  = ($cust_sess['zip'])?$cust_sess['zip']:'74035';	
										
										$request_data = array(
										"amount" => $amount,
										"credit_card"=> array (
											 "number"=> $card_no,
											 "expiration_month"=>$expmonth ,
											 "expiration_year"=>$exyear ),
										"csc"=> $cvv,
										"billing_address"=> array(
											"name"=>$name,
											"street_address"=> $address,
											"city"=> $city,
											"state"=> $state,
											"zip"=> $zipcode
											)
										);
										$request_data = json_encode($request_data);
										$result     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
											
										$response = $payAPI->jsonDecode($result['temp_json_response']); 
								
						
									if ( $result['http_status_code']=='200' ){
									    
									    // add level three data in transaction
					                    if($response['success']){
					                        $level_three_data = [
					                            'card_no' => $card_no,
					                            'merchID' => $marchent_id,
					                            'amount' => $amount,
					                            'token' => $oauth_token,
					                            'integrator_id' => $integratorId,
					                            'transaction_id' => $response['transaction_id'],
					                            'invoice_id' => '',
					                            'gateway' => 3,
					                        ];
					                        addlevelThreeDataInTransaction($level_three_data);
					                    }

									    $custID =  $resultingCustomerObj->Id;
											$customer_login = array(
												'customerEmail'=>$cust_sess['email'],
												'customerPassword'=>$cust_sess['password'],
												'createdAt'=>date('Y-m-d H:i:s'),
												'is_logged_in'=>$cust_sess['email'],
												'loginCode'=>'xyz',
												'isEnable'=>'0',
												'customerUsername'=>$cust_sess['username'],
										    	'merchantID'=>$cust_sess['marchantid'],
												'customerID'=>$custID
											 );
										   //inser here customer login table
										  
										   //customer card
										   $card_type = $this->general_model->getType($cust_sess['card']);	

										    $is_default = 0;
									     	$checkCustomerCard = checkCustomerCard($custID,$cust_sess['marchantid']);
								        	if($checkCustomerCard == 0){
								        		$is_default = 1;
								        	}
										 
										   $card_data = array(
											  'cardMonth'   =>$cust_sess['exp1'],
											  'cardYear' =>$cust_sess['exp2'],
											  'CardType'=>$card_type,
											  'CustomerCard' =>$this->card_model->encrypt($cust_sess['card']),
											  'CardCVV'      =>$this->card_model->encrypt($cust_sess['ccv']), 
											  'customerListID' =>$custID,                           
											  'Billing_Addr1'=> $cust_sess['add1'],
											  'Billing_Addr2'=> $cust_sess['add2'],
											  'Billing_City'=> $cust_sess['city'],
											  'Billing_Country'=>$cust_sess['country'],
											  'Billing_Contact'=> $cust_sess['mobile'],
											  'Billing_State'=> $cust_sess['state'],
											  'Billing_Zipcode'=> $cust_sess['zip'],
											  'merchantID'   => $cust_sess['marchantid'],
											  'is_default'			   => $is_default,
											  'customerCardfriendlyName'=>$cust_sess['cardfriendlyname'],
											  'createdAt' 	=> date("Y-m-d H:i:s") 
											);
											
											   
										   
											 //qbo custom customer
											 
											 $qbo_customer = array(
											   'Customer_ListID'=>  $custID,
											   'firstName'=>  $cust_sess['fname'],
											   'lastName'=>  $cust_sess['lname'],
											   'fullName'=> $cust_sess['fname'].' '.$cust_sess['lname'],
											   'userEmail'=>  $cust_sess['email'],
											   'phoneNumber'=>  $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=>  $cust_sess['add2'],
											   'Country'=>  $cust_sess['country'],
											   'State'=>  $cust_sess['state'],
											   'City'=>  $cust_sess['city'],
											   'zipCode'=>  $cust_sess['zip'],
											   'ship_address1'=> $cust_sess['sadd1'],
											   'ship_address2'=>  $cust_sess['sadd2'],
											   'ship_country'=>  $cust_sess['scountry'],
											   'ship_state'=>  $cust_sess['sstate'],
											   'ship_city'=>  $cust_sess['scity'],
											   'ship_zipcode'=>  $cust_sess['szip'],
											   'companyName'=>  $resultingCustomerObj->CompanyName,
											   'companyID'=>  $custID,
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'listID'=>  '1',
											   'merchantID'=>  $marcid
											 );
											 
											 
											 $tbl_subscriptions_qbo = array(
											   'customerID'=>  $custID,
											   'merchantDataID'=>  $marcid,
											   'subscriptionName'=> $splan->planName,
											   'subscriptionPlan'=>$splan->subscriptionPlan,
											   'freeTrial'=>$splan->freeTrial,
											   'invoiceFrequency'=>$splan->invoiceFrequency,
											   'subscriptionAmount'=>$splan->subscriptionAmount,
											   'totalInvoice'=>$splan->totalInvoice,
											   'generatedInvoice'=>$splan->generatedInvoice,
											   
											   'totalAmount'=> $splan->totalAmount,
											   'paidAmount'=>$splan->paidAmount,
											   'generatingDate'=>date("Y-m-d H:i:s"),
											   'firstDate'=>date("Y-m-d H:i:s"),
											   'nextGeneratingDate'=>date("Y-m-d H:i:s"),
											   'startDate'=>date("Y-m-d H:i:s"),
											   'endDate'=>date("Y-m-d H:i:s"),
											   
											   'paymentGateway'=> $splan->paymentGateway,
											   'automaticPayment'=>$splan->automaticPayment,
											   'emailRecurring'=>$splan->emailRecurring,
											   'usingExistingAddress'=>$splan->usingExistingAddress,
											   'cardID'=>$splan->subscriptionPlan,
											   
											   'contactNumber'=> $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=> $cust_sess['add2'],
											   'country'=>  $cust_sess['country'],
											   'state'=>$cust_sess['state'],
											   'city'=>  $cust_sess['city'],
											   'zipcode'=>$cust_sess['zip'],
											   
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'taxID'=>  '1',
											   'planID'=>  $splan->planID
											 );
											 
										   
											 //qbo item
											 
											$this->db->trans_begin();
												$this->general_model->insert_row('tbl_customer_login', $customer_login);
												$this->general_model->insert_row('QBO_custom_customer', $qbo_customer);
												$this->db1->insert('customer_card_data', $card_data);
											   
											
											if ($this->db->trans_status() === FALSE)
											{
												 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong></div>'); 
													$this->db->trans_rollback();
													
											}
											else
											{
												
												$insertId =  $this->general_model->insert_row('tbl_subscriptions_qbo', $tbl_subscriptions_qbo);
												$qbo_item = array(
												   'itemListID'=>  $item->itemListID,
												   'itemFullName'=>  $item->itemFullName,
												   'itemDescription'=> $item->itemDescription,
												   'itemQuantity'=>$item->itemQuantity,
												   'itemRate'=> $item->itemRate,
												   'itemTax'=>  $item->itemTax,
												   'oneTimeCharge'=> $item->oneTimeCharge,
												   'subscriptionID'=> $insertId
												 );
												$insertId =  $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $qbo_item);
												if($insertId){
											    	$this->db->trans_commit();
												}
										}
										
										
										$this->session->set_flashdata('success', 'Success'); 
										redirect('/QBO_controllers/CheckPlan/thankyou/');
										
									}else{
										  
										 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['responsetext'].'</div>'); 
											
										}
								} // end here
								
								//forth gateway stripe
								elseif($chk_gateway->gatewayType == 5){  
								 
								  include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
		                            
								    
								   
								    $payusername   = $chk_gateway->gatewayUsername;
									$paypassword   =  $chk_gateway->gatewayPassword;
								    if(!empty($cust_sess['card'])){	
                				     	$card     = $cust_sess['card'];
                						$expmonth =  $cust_sess['exp1'];
                						$exyear   =$cust_sess['exp2'];
                						 if(strlen($expmonth)==1){
                								$expmonth = '0'.$expmonth;
                							}
                						
                						$cvv    = $cust_sess['ccv'];
                					 }
                				    
                				   $amount =  (int)$amount*100;
                				  
                				  
                				      
                				   		$plugin = new ChargezoomStripe();
				        				$plugin->setApiKey($paypassword);
                        					$token  =  $cust_sess['token'];
                        					$charge = \Stripe\Charge::create([
                                                  "amount" => $amount,
                                                  "currency" => "usd",
                                                  "source" => $token, // obtained with Stripe.js
                                                  "description" => "Charge for test Account"
                                                ]);
                        						
                                   
                    			       $charge= json_encode($charge);
                    				   $result = json_decode($charge);
                    				  
                    				 if($result->paid=='1' && $result->failure_code==""){
                    				     
                    				        $custID =  $resultingCustomerObj->Id;
											$customer_login = array(
												'customerEmail'=>$cust_sess['email'],
												'customerPassword'=>$cust_sess['password'],
												'createdAt'=>date('Y-m-d H:i:s'),
												'is_logged_in'=>$cust_sess['email'],
												'loginCode'=>'xyz',
												'isEnable'=>'0',
												'customerUsername'=>$cust_sess['username'],
												'merchantID'=>$cust_sess['marchantid'],
												'customerID'=>$custID
											 );
										   //inser here customer login table
										  
										   //customer card
										   $card_type = $this->general_model->getType($cust_sess['card']);

										    $is_default = 0;
									     	$checkCustomerCard = checkCustomerCard($custID,$cust_sess['marchantid']);
								        	if($checkCustomerCard == 0){
								        		$is_default = 1;
								        	}	
										 	
										   $card_data = array(
											  'cardMonth'   =>$cust_sess['exp1'],
											  'cardYear' =>$cust_sess['exp2'],
											  'CardType'=>$card_type,
											  'CustomerCard' =>$this->card_model->encrypt($cust_sess['card']),
											  'CardCVV'      =>$this->card_model->encrypt($cust_sess['ccv']), 
											  'customerListID' =>$custID,                           
											  'Billing_Addr1'=> $cust_sess['add1'],
											  'Billing_Addr2'=> $cust_sess['add2'],
											  'Billing_City'=> $cust_sess['city'],
											  'Billing_Country'=>$cust_sess['country'],
											  'Billing_Contact'=> $cust_sess['mobile'],
											  'Billing_State'=> $cust_sess['state'],
											  'Billing_Zipcode'=> $cust_sess['zip'],
											  'merchantID'   => $cust_sess['marchantid'],
											  'is_default'			   => $is_default,
											  'customerCardfriendlyName'=>$cust_sess['cardfriendlyname'],
											  'createdAt' 	=> date("Y-m-d H:i:s") 
											);
											
											   
										   
											 //qbo custom customer
											 
											 $qbo_customer = array(
											   'Customer_ListID'=>  $custID,
											   'firstName'=>  $cust_sess['fname'],
											   'lastName'=>  $cust_sess['lname'],
											   'fullName'=> $cust_sess['fname'].' '.$cust_sess['lname'],
											   'userEmail'=>  $cust_sess['email'],
											   'phoneNumber'=>  $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=>  $cust_sess['add2'],
											   'Country'=>  $cust_sess['country'],
											   'State'=>  $cust_sess['state'],
											   'City'=>  $cust_sess['city'],
											   'zipCode'=>  $cust_sess['zip'],
											   'ship_address1'=> $cust_sess['sadd1'],
											   'ship_address2'=>  $cust_sess['sadd2'],
											   'ship_country'=>  $cust_sess['scountry'],
											   'ship_state'=>  $cust_sess['sstate'],
											   'ship_city'=>  $cust_sess['scity'],
											   'ship_zipcode'=>  $cust_sess['szip'],
											   'companyName'=>  $resultingCustomerObj->CompanyName,
											   'companyID'=>  $custID,
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'listID'=>  '1',
											   'merchantID'=>  $marcid
											 );
											 
											 
											 $tbl_subscriptions_qbo = array(
											   'customerID'=>  $custID,
											   'merchantDataID'=>  $marcid,
											   'subscriptionName'=> $splan->planName,
											   'subscriptionPlan'=>$splan->subscriptionPlan,
											   'freeTrial'=>$splan->freeTrial,
											   'invoiceFrequency'=>$splan->invoiceFrequency,
											   'subscriptionAmount'=>$splan->subscriptionAmount,
											   'totalInvoice'=>$splan->totalInvoice,
											   'generatedInvoice'=>$splan->generatedInvoice,
											   
											   'totalAmount'=> $splan->totalAmount,
											   'paidAmount'=>$splan->paidAmount,
											   'generatingDate'=>date("Y-m-d H:i:s"),
											   'firstDate'=>date("Y-m-d H:i:s"),
											   'nextGeneratingDate'=>date("Y-m-d H:i:s"),
											   'startDate'=>date("Y-m-d H:i:s"),
											   'endDate'=>date("Y-m-d H:i:s"),
											   
											   'paymentGateway'=> $splan->paymentGateway,
											   'automaticPayment'=>$splan->automaticPayment,
											   'emailRecurring'=>$splan->emailRecurring,
											   'usingExistingAddress'=>$splan->usingExistingAddress,
											   'cardID'=>$splan->subscriptionPlan,
											   
											   'contactNumber'=> $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=> $cust_sess['add2'],
											   'country'=>  $cust_sess['country'],
											   'state'=>$cust_sess['state'],
											   'city'=>  $cust_sess['city'],
											   'zipcode'=>$cust_sess['zip'],
											   
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'taxID'=>  '1',
											   'planID'=>  $splan->planID
											 );
											 
										   
											 //qbo item
											 
											$this->db->trans_begin();
												$this->general_model->insert_row('tbl_customer_login', $customer_login);
												$this->general_model->insert_row('QBO_custom_customer', $qbo_customer);
												$this->db1->insert('customer_card_data', $card_data);
											   
											
											if ($this->db->trans_status() === FALSE)
											{
												 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong></div>'); 
													$this->db->trans_rollback();
													
											}
											else
											{
												
												$insertId =  $this->general_model->insert_row('tbl_subscriptions_qbo', $tbl_subscriptions_qbo);
												$qbo_item = array(
												   'itemListID'=>  $item->itemListID,
												   'itemFullName'=>  $item->itemFullName,
												   'itemDescription'=> $item->itemDescription,
												   'itemQuantity'=>$item->itemQuantity,
												   'itemRate'=> $item->itemRate,
												   'itemTax'=>  $item->itemTax,
												   'oneTimeCharge'=> $item->oneTimeCharge,
												   'subscriptionID'=> $insertId
												 );
												$insertId =  $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $qbo_item);
												if($insertId){
											    	$this->db->trans_commit();
												}
											}
										
											$this->session->set_flashdata('success', 'Success'); 
										redirect('/QBO_controllers/CheckPlan/thankyou/');
                    				 }else{
                    				     
                    				    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Not Working</strong></div>'); 
                    				 }
								    
								}//forth gateway stripe end here
								
								elseif($chk_gateway->gatewayType == 4){
								    
								    $username   = $chk_gateway->gatewayUsername;
									$password   =  $chk_gateway->gatewayPassword;
									$signature = $chk_gateway->gatewaySignature;
									include APPPATH . 'third_party/PayPalAPINEW.php';
			                        $this->load->config('paypal');
			                        
			                        
			                        $config = array(
                						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
                						'APIUsername' => $username, 	// PayPal API username of the API caller
                						'APIPassword' => $password,	// PayPal API password of the API caller
                						'APISignature' => $signature, 	// PayPal API signature of the API caller
                						'APISubject' => '', 		// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                						'APIVersion' => $this->config->item('APIVersion') // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                					  );
                		
                					// Show Errors
                					if($config['Sandbox'])
                					{
                						error_reporting(E_ALL);
                						ini_set('display_errors', '1');
                					}
                					$this->load->library('paypal/Paypal_pro', $config);		
					
					
					
								    if(!empty($cust_sess['card'])){	
                				     	$card     = $cust_sess['card'];
                						$expmonth =  $cust_sess['exp1'];
                						$exyear   =$cust_sess['exp2'];
                						 if(strlen($expmonth)==1){
                								$expmonth = '0'.$expmonth;
                							}
                						
                						$cvv    = $cust_sess['ccv'];
                					 }
                					 
                					 
                					$paymentType 		=	'Sale';
                					$companyName        ='ABCS';
                					$firstName 			= $cust_sess['fname'];
                					$lastName 			= $cust_sess['lname'];
                					$creditCardType 	= 'Visa';
                					$creditCardNumber 	= $cust_sess['card'];
                					$expDateMonth 		= $cust_sess['exp1'];
                					// Month must be padded with leading zero
                					$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                                    
                					$expDateYear=  	$exyear;
                					$cvv2Number =   $cvv;
                					$address1	=   $cust_sess['add1'];
                					$address2 	= $cust_sess['add2'];
                					$city 		=   $cust_sess['city'];
                					$state 		= $cust_sess['state'];
                					$zip 		= $cust_sess['zip'];
                					$email      = $cust_sess['email'];
                					$phone      = $cust_sess['mobile'];
                					
                					$country='';
                					$currencyID ='';
                					if($cust_sess['country']=="United States" || $cust_sess['country']=="United State"){
                					  $country 	= 'US';	// US or other valid country code
                					  $currencyID = 'USD';
                					 
                					}
                					
                					if($cust_sess['country']=="Canada" || $cust_sess['country']=="canada"){
                					  $country 	  = 'CAD';	// US or other valid country code
                					  $currencyID = 'CAD';
                					}
                					
                					$DPFields = array(
            							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
            																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
            							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
            							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
            						);
						
                            		$CCDetails = array(
            							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
            							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
            							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
            							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
            							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
            							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
            						);
		
                            		$PayerInfo = array(
            							'email' => $email, 								// Email address of payer.
            							'payerid' => '', 							// Unique PayPal customer ID for payer.
            							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
            							'business' => '' 							// Payer's business name.
            						);  
						
                            		$PayerName = array(
            							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
            							'firstname' => $firstName, 							// Payer's first name.  25 char max.
            							'middlename' => '', 						// Payer's middle name.  25 char max.
            							'lastname' => $lastName, 							// Payer's last name.  25 char max.
            							'suffix' => ''								// Payer's suffix.  12 char max.
            						);
                            					
                            		$BillingAddress = array(
        								'street' => $address1, 						// Required.  First street address.
        								'street2' => $address2, 						// Second street address.
        								'city' => $city, 							// Required.  Name of City.
        								'state' => $state, 							// Required. Name of State or Province.
        								'countrycode' => $country, 					// Required.  Country code.
        								'zip' => $zip, 							// Required.  Postal code of payer.
        								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
        							);
	
							
        	                       $PaymentDetails = array(
            							'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
            							'currencycode' => $currencyID, 					// Required.  Three-letter currency code.  Default is USD.
            							'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
            							'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
            							'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
            							'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
            							'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
            							'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
            							'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
            							'custom' => '', 						// Free-form field for your own use.  256 char max.
            							'invnum' => '', 						// Your own invoice or tracking number
            							'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
            							'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
            							'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
            						);					

            						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
								
				                    $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				                    
				                    
								    if(!empty($PayPalResult["ACK"]) && "SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
								        
								        $custID =  $resultingCustomerObj->Id;
											$customer_login = array(
												'customerEmail'=>$cust_sess['email'],
												'customerPassword'=>$cust_sess['password'],
												'createdAt'=>date('Y-m-d H:i:s'),
												'is_logged_in'=>$cust_sess['email'],
												'loginCode'=>'xyz',
												'isEnable'=>'0',
												'customerUsername'=>$cust_sess['username'],
												'merchantID'=>$cust_sess['marchantid'],
												'customerID'=>$custID
											 );
										   //inser here customer login table
										  
										   //customer card
										   $card_type = $this->general_model->getType($cust_sess['card']);

										   $is_default = 0;
									     	$checkCustomerCard = checkCustomerCard($custID,$cust_sess['marchantid']);
								        	if($checkCustomerCard == 0){
								        		$is_default = 1;
								        	}	
										 
										   $card_data = array(
											  'cardMonth'   =>$cust_sess['exp1'],
											  'cardYear' =>$cust_sess['exp2'],
											  'CardType'=>$card_type,
											  'CustomerCard' =>$this->card_model->encrypt($cust_sess['card']),
											  'CardCVV'      =>$this->card_model->encrypt($cust_sess['ccv']), 
											  'customerListID' =>$custID,                           
											  'Billing_Addr1'=> $cust_sess['add1'],
											  'Billing_Addr2'=> $cust_sess['add2'],
											  'Billing_City'=> $cust_sess['city'],
											  'Billing_Country'=>$cust_sess['country'],
											  'Billing_Contact'=> $cust_sess['mobile'],
											  'Billing_State'=> $cust_sess['state'],
											  'Billing_Zipcode'=> $cust_sess['zip'],
											  'merchantID'   => $cust_sess['marchantid'],
											  'is_default'			   => $is_default,
											  'customerCardfriendlyName'=>$cust_sess['cardfriendlyname'],
											  'createdAt' 	=> date("Y-m-d H:i:s") 
											);
											
											   
										   
											 //qbo custom customer
											 
											 $qbo_customer = array(
											   'Customer_ListID'=>  $custID,
											   'firstName'=>  $cust_sess['fname'],
											   'lastName'=>  $cust_sess['lname'],
											   'fullName'=> $cust_sess['fname'].' '.$cust_sess['lname'],
											   'userEmail'=>  $cust_sess['email'],
											   'phoneNumber'=>  $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=>  $cust_sess['add2'],
											   'Country'=>  $cust_sess['country'],
											   'State'=>  $cust_sess['state'],
											   'City'=>  $cust_sess['city'],
											   'zipCode'=>  $cust_sess['zip'],
											   'ship_address1'=> $cust_sess['sadd1'],
											   'ship_address2'=>  $cust_sess['sadd2'],
											   'ship_country'=>  $cust_sess['scountry'],
											   'ship_state'=>  $cust_sess['sstate'],
											   'ship_city'=>  $cust_sess['scity'],
											   'ship_zipcode'=>  $cust_sess['szip'],
											   'companyName'=>  $resultingCustomerObj->CompanyName,
											   'companyID'=>  $custID,
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'listID'=>  '1',
											   'merchantID'=>  $marcid
											 );
											 
											 
											 $tbl_subscriptions_qbo = array(
											   'customerID'=>  $custID,
											   'merchantDataID'=>  $marcid,
											   'subscriptionName'=> $splan->planName,
											   'subscriptionPlan'=>$splan->subscriptionPlan,
											   'freeTrial'=>$splan->freeTrial,
											   'invoiceFrequency'=>$splan->invoiceFrequency,
											   'subscriptionAmount'=>$splan->subscriptionAmount,
											   'totalInvoice'=>$splan->totalInvoice,
											   'generatedInvoice'=>$splan->generatedInvoice,
											   
											   'totalAmount'=> $splan->totalAmount,
											   'paidAmount'=>$splan->paidAmount,
											   'generatingDate'=>date("Y-m-d H:i:s"),
											   'firstDate'=>date("Y-m-d H:i:s"),
											   'nextGeneratingDate'=>date("Y-m-d H:i:s"),
											   'startDate'=>date("Y-m-d H:i:s"),
											   'endDate'=>date("Y-m-d H:i:s"),
											   
											   'paymentGateway'=> $splan->paymentGateway,
											   'automaticPayment'=>$splan->automaticPayment,
											   'emailRecurring'=>$splan->emailRecurring,
											   'usingExistingAddress'=>$splan->usingExistingAddress,
											   'cardID'=>$splan->subscriptionPlan,
											   
											   'contactNumber'=> $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=> $cust_sess['add2'],
											   'country'=>  $cust_sess['country'],
											   'state'=>$cust_sess['state'],
											   'city'=>  $cust_sess['city'],
											   'zipcode'=>$cust_sess['zip'],
											   
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'taxID'=>  '1',
											   'planID'=>  $splan->planID
											 );
											 
										   
											 //qbo item
											 
											$this->db->trans_begin();
												$this->general_model->insert_row('tbl_customer_login', $customer_login);
												$this->general_model->insert_row('QBO_custom_customer', $qbo_customer);
												$this->db1->insert('customer_card_data', $card_data);
											   
											
											if ($this->db->trans_status() === FALSE)
											{
												 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong></div>'); 
													$this->db->trans_rollback();
													
											}
											else
											{
												
												$insertId =  $this->general_model->insert_row('tbl_subscriptions_qbo', $tbl_subscriptions_qbo);
												$qbo_item = array(
												   'itemListID'=>  $item->itemListID,
												   'itemFullName'=>  $item->itemFullName,
												   'itemDescription'=> $item->itemDescription,
												   'itemQuantity'=>$item->itemQuantity,
												   'itemRate'=> $item->itemRate,
												   'itemTax'=>  $item->itemTax,
												   'oneTimeCharge'=> $item->oneTimeCharge,
												   'subscriptionID'=> $insertId
												 );
												$insertId =  $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $qbo_item);
												if($insertId){
											    	$this->db->trans_commit();
												}
											}
										
											$this->session->set_flashdata('success', 'Success');
										redirect('/QBO_controllers/CheckPlan/thankyou/');
								    }
								}//end 5th gateway
								
								
																
						}else{
							$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:QBO</strong></div>');
						}
                    }
               }
                 
                break;
        }
	   
	     $data['gateway'] = $this->general_model->get_row_data('tbl_merchant_gateway',$con1);
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$this->load->view('template/template_start', $data);
		$this->load->view('QBO_views/checkout', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	    
	}
	
	
}