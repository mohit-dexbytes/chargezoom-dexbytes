<?php

/**
 * Paytrace Payment Gateway Operations
 * Sale using create_customer_sale
 * Authorize  using create_customer_auth
 * Capture using create_customer_capture 
 * Void using create_customer_void
 * Refund create_customer_refund
 * Single Invoice Payment using pay_invoice
 * Multiple Invoice Payment using multi_pay_invoice
 */


class PaytracePayment extends CI_Controller
{
    private $resellerID;
    private $merchantID;
	private $transactionByUser;
    
	public function __construct()
	{
		parent::__construct();
		
	    include APPPATH . 'third_party/Freshbooks.php';
	    include APPPATH . 'third_party/FreshbooksNew.php';
	
		include APPPATH . 'third_party/PayTraceAPINEW.php';

     
		$this->load->config('paytrace');
     	$this->load->model('general_model');

			$this->load->model('card_model');
				$this->load->library('form_validation');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
        $this->db1 = $this->load->database('otherdb', true); 
		  if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='3')
		  {
		  	$logged_in_data = $this->session->userdata('logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		    $this->merchantID = $logged_in_data['merchID'];
			$this->resellerID = $logged_in_data['resellerID'];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 	$logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		    $this->merchantID = $merchID;
		  }else{
			redirect('login','refresh');
		  }
		  
		  
		   $val = array(
             'merchantID' => $this->merchantID,
             );
		  	$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		  	if(empty($Freshbooks_data))
		  	{
		  	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>'); 
                
		  	  redirect('FreshBoolks_controllers/home/index','refresh');
		  	  
		  	}
            
            $this->subdomain     = $Freshbooks_data['sub_domain'];
             $key                = $Freshbooks_data['secretKey'];
            $this->accessToken   = $Freshbooks_data['accessToken'];
            $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
            $this->fb_account    = $Freshbooks_data['accountType'];
            $condition1 = array('resellerID'=>$this->resellerID);
           	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			$domain1 = $sub1['merchantPortalURL'];
			$URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';  
            
            define('OAUTH_CONSUMER_KEY', $this->subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
	}
	
	
	public function index(){
	    
	
	   	redirect('FreshBooks_controllers/home','refresh'); 
	}

	 
public function pay_invoice111()
{
    
    if($this->session->userdata('logged_in') )
    {
       $merchID = $this->session->userdata('logged_in')['merchID'];
    	}
    	if($this->session->userdata('user_logged_in') )
    	{
          $merchID = $this->session->userdata('user_logged_in')['merchantID'];
    	} 
	     
	       $merchantID  = $merchID;
			        	
			$rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$merchID));
			 $resellerID =	$rs_daata['resellerID'];
			 $condition1 = array('resellerID'=>$resellerID);
        	
         	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			 $domain1 = $sub1['merchantPortalURL'];
			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
			 
			
			 
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID'); 
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
		 $cusproID=array(); $error=array();
         $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		 
         	$in_data =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
		 
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   
		 
             	$payusername   = $gt_result['gatewayUsername'];
    		    $paypassword   = $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
    		    
    		 
			
	 if($cardID!="" || $gateway!="")
	 {  
     
		 
		 if(!empty($in_data)){ 
			
				$Customer_ListID = $in_data['CustomerListID'];
         
           if($cardID=='new1')
           {
			        	$cardID_upd  =$cardID;
			        		$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				        $merchantID = $merchID;
          
           
                         $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID' => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                           $this->db1->select('*')->from('customer_card_data');
                           $qq =  $this->db1->get();
          
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                         }else{
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
				 $card_data      =   $this->card_model->get_single_card_data($cardID);
				 
			 if(!empty($card_data))
			 {
						
						   $cr_amount = 0;
							 $amount  =	 $this->czsecurity->xssCleanPostInput('inv_amount');
					
					       $amount    = $amount-$cr_amount;
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$cvv      = $card_data['CardCVV'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						  
						$payAPI  = new PayTraceAPINEW();	
					 
				    	$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                    
						//call a function of Utilities.php to verify if there is any error with OAuth token. 
						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                    

		          if(!$oauth_moveforward){ 
		
		
		            $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token 
				$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
				$name = $in_data['fullName'];
				$address = $in_data['ShipAddress_Addr1'];
				$city = $in_data['ShipAddress_City'];
				$state = $in_data['ShipAddress_State'];
				$zipcode = ($in_data['ShipAddress_PostalCode'])?$in_data['ShipAddress_PostalCode']:'85284';
				
			
			
				$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                     "integrator_id"=>mt_rand(100000,200000),
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $card_data['Billing_Addr1'],
                        "city"=>$card_data['Billing_City'],
                        "state"=>$card_data['Billing_State'],
                        "zip"=>$card_data['Billing_Zipcode'],
						)
					);  
					
               
				      $request_data = json_encode($request_data); 
				      
			           $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
				       $response  = $payAPI->jsonDecode($result['temp_json_response']); 
				       
				     
			  
	             $invoicp_id = '';  
				   if ( $result['http_status_code']=='200' )
				   {
				       
				       
				       	 $condition1 = array('resellerID'=>$resellerID);
                	
                 	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY', $subdomain);
                                define('OAUTH_CONSUMER_SECRET', $key);
                                define('OAUTH_CALLBACK', $URL1);
				       
				       
				       
						
			
        
    
        
        if(isset($accessToken) && isset($access_token_secret))
        {
                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
        
                    $payment = '<?xml version="1.0" encoding="utf-8"?>  
						<request method="payment.create">  
						  <payment>         
						    <invoice_id>'.$invoiceID.'</invoice_id>               
						    <amount>'.$amount.'</amount>             
						    <currency_code>USD</currency_code> 
						    <type>Check</type>                   
						  </payment>  
						</request>';
             
                      $invoices = $c->add_payment($payment);

        			if ($invoices['status'] !='ok') 
        			{
        			
        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>');
                
        				
        			}
        			else 
        			{
        			    $invoicp_id =$invoices['payment_id'];
        				$this->session->set_flashdata('success','Success'); 
        				
        			} 
				}	  
				
				       
				   }
				   
				   else{
					   
							if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
							 redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
					
				  }	   
					   $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					   
						$transactiondata['transactionType']    = 'pay_sale';
						$transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					   $transactiondata['customerListID']      =$in_data['CustomerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['invoiceID']   = $invoiceID;
					      $transactiondata['qbListTxnID'] =   $invoicp_id;
					    $transactiondata['resellerID']   = $this->resellerID;
			            $transactiondata['gateway']   = "Paytrace";
    				    
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
    				   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
    				    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}         
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication not valid</strong>.</div>'); 
				}	   
			
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
	        }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
		  }		 
			 
		  		 if($cusproID!=""){
			 	 redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
		   	 }

    
}     


        	 	 
	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");

		$cusproID = array();
		$error    = array();
		$cusproID = $this->czsecurity->xssCleanPostInput('customerProcessID');

		$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
		$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
        $checkPlan = check_free_plan_transactions();

		if ($checkPlan && $this->form_validation->run() == true) {

			$merchID    = $this->merchantID;
			$user_id    = $merchID;
			$resellerID = $this->resellerID;
			$invoiceID  = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			
			$cardID    = $this->czsecurity->xssCleanPostInput('CardID');
			if (!$cardID || empty($cardID)) {
				$cardID = $this->czsecurity->xssCleanPostInput('schCardID');
			}

			$gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
			if (!$gatlistval || empty($gatlistval)) {
				$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
			}

			$gateway = $gatlistval;
			$sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

			$amount     = $this->czsecurity->xssCleanPostInput('inv_amount');
			$chh_mail   = 0;
			$in_data    = $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

			if (!empty($in_data)) {

				$customerID = $in_data['Customer_ListID'];

				$companyID = $in_data['companyID'];
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

				$payusername = $gt_result['gatewayUsername'];
				$paypassword = $gt_result['gatewayPassword'];
				$integratorId = $gt_result['gatewaySignature'];
				$grant_type  = "password";
				
				if (!empty($cardID)) {
					$payAPI = new PayTraceAPINEW();
		
					$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
		
					//call a function of Utilities.php to verify if there is any error with OAuth token.
					$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
		
					if (!$oauth_moveforward) {
		
						$json = $payAPI->jsonDecode($oauth_result['temp_json_response']);
		
						//set Authentication value based on the successful oAuth response.
						//Add a space between 'Bearer' and access _token
						$oauth_token = sprintf("Bearer %s", $json['access_token']);
		
						if ($in_data['BalanceRemaining'] > 0) {
							$add_level_data = false;
							if ($sch_method == "1") {

								if ($cardID != "new1") {
		
									$card_data = $this->card_model->get_single_card_data($cardID);
									$card_no   = $card_data['CardNo'];
									$expmonth  = $card_data['cardMonth'];
									$exyear    = $card_data['cardYear'];
									$cvv       = $card_data['CardCVV'];
									$cardType  = $card_data['CardType'];
									$address1  = $card_data['Billing_Addr1'];
									$city      = $card_data['Billing_City'];
									$zipcode   = $card_data['Billing_Zipcode'];
									$state     = $card_data['Billing_State'];
									$country   = $card_data['Billing_Country'];
					
								} else {
					
									$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
									$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
									$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
									$cardType = $this->general_model->getType($card_no);
									$cvv      = '';
									if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
										$cvv = $this->czsecurity->xssCleanPostInput('cvv');
									}
					
									$address1 = $this->czsecurity->xssCleanPostInput('address1');
									$address2 = $this->czsecurity->xssCleanPostInput('address2');
									$city     = $this->czsecurity->xssCleanPostInput('city');
									$country  = $this->czsecurity->xssCleanPostInput('country');
									$phone    = $this->czsecurity->xssCleanPostInput('phone');
									$state    = $this->czsecurity->xssCleanPostInput('state');
									$zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
					
								}
		
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
								}
			
								$request_data = array(
									"amount"          => $amount,
									"credit_card"     => array(
										"number"           => $card_no,
										"expiration_month" => $expmonth,
										"expiration_year"  => $exyear),
									"csc"             => $cvv,
									"integrator_id"   => mt_rand(100000, 200000),
									"billing_address" => array(
										"name"           => $in_data['fullName'],
										"street_address" => $address1,
										"city"           => $city,
										"state"          => $state,
										"zip"            => $zipcode,
									),
								);
								$add_level_data = true;
								$reqURL = URL_KEYED_SALE;
							} else if ($sch_method == "2") {
								if($cardID == 'new1') {
									$accountDetails = [
										'accountName' => $this->czsecurity->xssCleanPostInput('acc_name'),
										'accountNumber' => $this->czsecurity->xssCleanPostInput('acc_number'),
										'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
										'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
										'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
										'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
										'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
										'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
										'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
										'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
										'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
										'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
										'customerListID' => $customerID,
										'companyID'     => $companyID,
										'merchantID'   => $merchantID,
										'createdAt' 	=> date("Y-m-d H:i:s"),
										'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode')
									];
								} else {
									$accountDetails = $this->card_model->get_single_card_data($cardID);
								}
		
								$request_data = array(
									"amount"          => $amount,
									"check"     => array(
										"account_number"=> $accountDetails['accountNumber'],
										"routing_number"=> $accountDetails['routeNumber'],
									),
									"integrator_id" => $integratorId,
									"billing_address" => array(
										"name" => $accountDetails['accountName'],
										"street_address" => $accountDetails['Billing_Addr1']. ', '.$accountDetails['Billing_Addr2'],
										"city" => $accountDetails['Billing_City'],
										"state" => $accountDetails['Billing_State'],
										"zip" => $accountDetails['Billing_Zipcode'],
									),
								);
		
								$reqURL = URL_ACH_SALE;
							}
		

							$cusproID = 1;
		
							$request_data = json_encode($request_data);
		
							$result1  = $payAPI->processTransaction($oauth_token, $request_data, $reqURL);
							$response = $payAPI->jsonDecode($result1['temp_json_response']);
		
							$response['http_status_code'] = $result1['http_status_code'];
							$pinv_id                      = '';
							$result                       = $response;

							
							if ($result1['http_status_code'] == '200') {
								// add level three data in transaction
			                    if($response['success'] && $add_level_data){
			                        $level_three_data = [
			                            'card_no' => $card_no,
			                            'merchID' => $merchID,
			                            'amount' => $amount,
			                            'token' => $oauth_token,
			                            'integrator_id' => $integratorId,
			                            'transaction_id' => $response['transaction_id'],
			                            'invoice_id' => $invoiceID,
			                            'gateway' => 3,
			                        ];
			                        addlevelThreeDataInTransaction($level_three_data);
			                    }
								
								$val = array(
									'merchantID' => $merchID,
								);
		
								$ispaid  = 1;
								$st      = 'paid';
								$bamount = $in_data['BalanceRemaining'] - $amount;
								if ($bamount > 0) {
									$ispaid = '0';
									$st     = 'unpaid';
								}
								$app_amount = $in_data['AppliedAmount'] + $amount;
								$up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
								$condition  = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);
		
								$this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);
		
								if (isset($this->accessToken) && isset($this->access_token_secret)) {
									if ($this->fb_account == 1) {
										$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
		
										$payment  = array('invoiceid' => $invoiceID, 'amount' => array('amount' => $amount), 'type' => 'Check', 'date' => date('Y-m-d'));
										$invoices = $c->add_payment($payment);
									} else {
										$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
		
										$payment = '<?xml version="1.0" encoding="utf-8"?>
													<request method="payment.create">
													<payment>
														<invoice_id>' . $invoiceID . '</invoice_id>
														<amount>' . $amount . '</amount>
														<currency_code>USD</currency_code>
														<type>Check</type>
													</payment>
													</request>';
										$invoices = $c->add_payment($payment);
		
									}
		
									if ($invoices['status'] != 'ok') {
		
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
		
										
		
									} else {
										$pinv_id = $invoices['payment_id'];
		
										$this->session->set_flashdata('success', 'Successfully Processed Invoice');
		
									}
								}
								$condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
								$ref_number     = $in_data['refNumber'];
								$tr_date        = date('Y-m-d H:i:s');
								$toEmail        = $in_data['userEmail'];
								$company        = $in_data['companyName'];
								$customer       = $in_data['fullName'];
								if ($chh_mail == '1') {
		
									$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
								}
								
								if ($cardID == "new1") {
									if ($sch_method == "1") {
		
										$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
										$cardType = $this->general_model->getType($card_no);
			
										$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
										$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
										$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
			
										$card_data = array(
											'cardMonth'       => $expmonth,
											'cardYear'        => $exyear,
											'CardType'        => $cardType,
											'CustomerCard'    => $card_no,
											'CardCVV'         => $cvv,
											'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
											'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
											'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
											'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
											'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
											'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
											'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
											'customerListID'  => $customerID,
			
											'companyID'       => $companyID,
											'merchantID'      => $user_id,
											'createdAt'       => date("Y-m-d H:i:s"),
										);
			
										$id1 = $this->card_model->process_card($card_data);
									} else if ($sch_method == "2") {
										$id1 = $this->card_model->process_ack_account($accountDetails);
									}
								}
		
							} else {
								if (!empty($response['errors'])) {$err_msg = $this->getError($response['errors']);} else { $err_msg = $response['approval_message'];}
							
								$this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');		
							}
		
							$id = $this->general_model->insert_gateway_transaction_data($result, 'pay_sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $pinv_id, $this->resellerID, $in_data['invoiceID'], false, $this->transactionByUser);
		
						}
		
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error Authentication Failed</strong>.</div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Validation Error</strong>.</div>');
		}

		if ($cusproID != "") {
			$trans_id     = ($sch_method == "2") ? $result['check_transaction_id'] : $result['transaction_id'];
			$invoice_IDs  = array();
			$receipt_data = array(
				'proccess_url'      => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
				'proccess_btn_text' => 'Process New Invoice',
				'sub_header'        => 'Sale',
				'checkPlan'			=> $checkPlan
			);

			$this->session->set_userdata("receipt_data", $receipt_data);
			$this->session->set_userdata("invoice_IDs", $invoice_IDs);
			$this->session->set_userdata("in_data", $in_data);
			if ($cusproID == "1") {
				redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');

			}

			redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $cusproID, 'refresh');
		} else {
			if(!$checkPlan){
				$responseId  = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
			redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
		}

	}     




	public function create_new_sale()
	{



		$username ="demo123";
		$password ="demo123";
		$grant_type ="password";
		
		$payAPI  = new PayTraceAPINEW();
		
		$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $username, $password);

		//call a function of Utilities.php to verify if there is any error with OAuth token. 
		$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

		//If IsFoundOAuthTokenError results True, means no error 
		//next is to move forward for the actual request 

		if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			// Build the transaction 
			    $request_data = array(
                    "amount" => "2.50",
                    "credit_card"=> array (
                         "number"=> "4111111111111122",
                         "expiration_month"=> "12",
                         "expiration_year"=> "2020"),
                    "csc"=> "999",
                    "billing_address"=> array(
                        "name"=> "Mark Smith",
                        "street_address"=> "8320 E. West St.",
                        "city"=> "Spokane",
                        "state"=> "WA",
                        "zip"=> "85284")
                    );
    
            $request_data = json_encode($request_data);
   
			
			$result =$payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
			echo "<pre>";
            print_r($result);
					
			  $json = $payAPI->jsonDecode($result['temp_json_response']);  
			  			echo "<pre>";
						print_r($json); 
			  foreach($json['errors'] as $error =>$no_of_errors )
				{
					//Do you code here as an action based on the particular error number 
					//you can access the error key with $error in the loop as shown below.
					echo "<br>". $error;
					// to access the error message in array assosicated with each key.
					foreach($no_of_errors as $item)
					{
					   //Optional - error message with each individual error key.
						echo "  " . $item ; 
					} 
				}
	die;
			
			
		}

	}	


		 
	public function create_customer_sale_old()
    {
        
        
		
		if(!empty($this->input->post(null, true))){
				$inv_array  = array();
		        $inv_invoice= array();
		        	$qblist=array();
			if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
			
        			 $resellerID =	$this->resellerID;
			    
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			    $customerID=$this->czsecurity->xssCleanPostInput('customerID');
			   	if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
			  if($gatlistval !="" && !empty($gt_result) )
			{
			
    		$payusername   = $gt_result['gatewayUsername'];
   	        $paypassword   = $gt_result['gatewayPassword'];
     	    $grant_type    = "password";
    		    
    		
				
	      	$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
				  $cardID = $this->czsecurity->xssCleanPostInput('card_list');
			  
				
				
			   $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			  
			  $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

		     //call a function of Utilities.php to verify if there is any error with OAuth token. 
		     $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

	     	//If IsFoundOAuthTokenError results True, means no error 
		    //next is to move forward for the actual request 

		if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			  if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
              	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear,2);
						$expry    = $expmonth.$exyear;  
				
				 }else {
					  
						   
                   $cardID = $this->czsecurity->xssCleanPostInput('card_list');
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}

					}	
			
			
					
					$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
					$name    = $this->czsecurity->xssCleanPostInput('fistName').' '.$this->czsecurity->xssCleanPostInput('lastName');
					$address =	$this->czsecurity->xssCleanPostInput('address1').' '. $this->czsecurity->xssCleanPostInput('address2');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$state   = $this->czsecurity->xssCleanPostInput('state');
				     $amount = $this->czsecurity->xssCleanPostInput('totalamount');		
				      $zipcode  = ($this->czsecurity->xssCleanPostInput('zipcode'))?$this->czsecurity->xssCleanPostInput('zipcode'):'74035';	
				   $invoiceIDs='';
				   if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
				   {
				     $invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				            
				    }
				    $random_inv_paytrace = rand(1000,1002200);
					$request_data = array(
                         "amount" => $amount,
                         "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth ,
                         "expiration_year"=>$exyear ),
                         "csc"=> $cvv,
                         
                         "invoice_id"=>$random_inv_paytrace,
                         "billing_address"=> array(
                         "name"=>$name,
                        "street_address"=>$address,
                         "city"=> $city,
                         "zip"=> $zipcode
						)
					);
					
				
					
					    $request_data = json_encode($request_data);
						$result     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
						$response = $payAPI->jsonDecode($result['temp_json_response']); 
						
				 if ( $result['http_status_code']=='200' )
				 {
					
					
					
        			 $condition1 = array('resellerID'=>$resellerID);
                	
                 	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY', $subdomain);
                                define('OAUTH_CONSUMER_SECRET', $key);
                                define('OAUTH_CALLBACK', $URL1);
					 
					
					  
                    if(isset($accessToken) && isset($access_token_secret))
                    {
						
    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
    
                           $qblist=array();
				           if(!empty($invoiceIDs))
				           {
				              foreach($invoiceIDs as $inID)
				              {
								  
								  $con = array('invoiceID'=>$inID,'merchantID'=>$merchantID);
            									$res = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining','invoiceID'), $con);
            									$amount_data = $res['BalanceRemaining'];
								                $invID       = $res['invoiceID'];
								  
								  $payment = '<?xml version="1.0" encoding="utf-8"?>  
												<request method="payment.create">  
												  <payment>         
													<invoice_id>'.$invID.'</invoice_id>               
													<amount>'.$amount_data.'</amount>             
													<currency_code>USD</currency_code> 
													<type>Check</type>                   
												  </payment>  
												</request>';
									  $invoices = $c->add_payment($payment);
									 
						
									if ($invoices['status'] != 'ok') {
						
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
						
										
									}
									else {
									   $inv_array['inv_no'][]= $invID;
									    $inv_array['inv_amount'][]= $amount_data;
									    $inv_invoice[]  =   $invID;
										$pinv_id   =  $invoices['payment_id'];
										$qblist[]  = $pinv_id;
										$this->session->set_flashdata('success','Transaction Successful'); 
										
									}
								}			
						   }
					}						   
					   
             
             if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                            {	            $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                                        	 $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    				 		 $card_type      =$this->general_model->getType($card_no);
               								   $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    				       
                                         		$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                                            	
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										 	'CardType'     =>$card_type,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
										  'CustomerCard' =>$card_no,
										  'CardCVV'      =>$cvv, 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
    
                                         	
                          			$card=	$this->card_model->process_card($card_data) ;  
                          			
                          			
                                         }  
              
				
                 
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					
	               if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');  
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
						$transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					    $transactiondata['transactionStatus']    = $response['status_message'];
					    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionCode']     = $result['http_status_code'];
					    $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					   
						$transactiondata['transactionType']    = 'pay_sale';	
						$transactiondata['gatewayID']          = $gatlistval;
                        $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					    $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					    $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
			            $transactiondata['gateway']   = "Paytrace"; 
			              if(!empty($invoiceIDs) && !empty($inv_array))
				        {
					      $transactiondata['invoiceID']            = implode(',',$inv_invoice);
					      $transactiondata['invoiceRefID']         = json_encode($inv_array);
					      if(!empty($qblist))
					      $transactiondata['qbListTxnID']          = implode(',',$qblist); 
				        }   
    					if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
			            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			            if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		   }else{
	     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong>.</div>'); 
		 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
		}			
					   
                       
				      
        }
             redirect('FreshBooks_controllers/Transactions/create_customer_sale','refresh');

	
    }
	
	
	public function create_customer_sale()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      $user_id = $this->merchantID;
		
		$checkPlan = check_free_plan_transactions();
		if($checkPlan && !empty($this->input->post(null, true)))
		{
		
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('cardID')=="new1")
            { 
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           
               
	    	if ($this->form_validation->run() == true)
		    {
	   
        		$custom_data_fields = [];
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }

			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        		if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
        		
        			      	$payusername   = $gt_result['gatewayUsername'];
							$paypassword   = $gt_result['gatewayPassword'];
							$integratorId = $gt_result['gatewaySignature'];

							$grant_type    = "password";
            				 $invoiceIDs   = array();
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     $invoiceIDs   = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				
		             $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        			
					 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
					 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
        			 $companyID  = $comp_data['companyID'];
        			 $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                         
        			 $cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =$this->czsecurity->xssCleanPostInput('cvv'); 
        				     
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						 
        					
        					}
        					
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    	                        $name    =  $this->czsecurity->xssCleanPostInput('firstName').' '.$this->czsecurity->xssCleanPostInput('lastName');
								$address= $this->czsecurity->xssCleanPostInput('address1').' '.$this->czsecurity->xssCleanPostInput('address2');
								$payAPI = new PayTraceAPINEW();
								  //set the properties for this request in the class
								  
								  $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

								 //call a function of Utilities.php to verify if there is any error with OAuth token. 
								 $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

								//If IsFoundOAuthTokenError results True, means no error 
								//next is to move forward for the actual request 

							if(!$oauth_moveforward)
							{
								//Decode the Raw Json response.
								$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
								
								//set Authentication value based on the successful oAuth response.
								//Add a space between 'Bearer' and access _token 
								$oauth_token = sprintf("Bearer %s",$json['access_token']);
								$amount = $this->czsecurity->xssCleanPostInput('totalamount');
							     $random_inv_paytrace = rand(1000,1002200);
								$request_data = array(
									 "amount" => $amount,
									 "credit_card"=> array (
									 "number"=> $card_no,
									 "expiration_month"=>$expmonth ,
									 "expiration_year"=>$exyear ),
									 "csc"=> $cvv,
									 
									 "invoice_id"=>$random_inv_paytrace,
									 "billing_address"=> array(
									 "name"=>$name,
									"street_address"=>$address,
									 "city"=> $city,
									 "zip"=> $zipcode
									)
								);
								if($this->czsecurity->xssCleanPostInput('invoice_id')){
									$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
			                    	
									$request_data['invoice_id'] = $new_invoice_number;
								}
					              include APPPATH .'libraries/Freshbooks_data.php';
								 $fb_data = new Freshbooks_data($this->merchantID,$this->resellerID);
								
				
					    $request_data = json_encode($request_data);
						$result1      =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
						$response     = $payAPI->jsonDecode($result1['temp_json_response']); 
						$response['http_status_code'] =$result1['http_status_code'];
						$pinv_id='';
						$result = $response;
				
				 if ( $result1['http_status_code']=='200' )
				 {
						
						// add level three data in transaction
	                    if($response['success']){
	                        $level_three_data = [
	                            'card_no' => $card_no,
	                            'merchID' => $user_id,
	                            'amount' => $amount,
	                            'token' => $oauth_token,
	                            'integrator_id' => $integratorId,
	                            'transaction_id' => $response['transaction_id'],
	                            'invoice_id' => $random_inv_paytrace,
	                            'gateway' => 3,
	                        ];
	                        addlevelThreeDataInTransaction($level_three_data);
	                    }
								 $invoiceIDs=array();      
								 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
								 {
									$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
								 }
				     
								 $refNumber =array();
					 
							   if(!empty($invoiceIDs))
							   {
								   
								  foreach($invoiceIDs as $inID)
								  {
									
									$in_data = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining','refNumber','AppliedAmount'),array('merchantID'=>$this->merchantID,'invoiceID'=>$inID));
									$refNumber[] = 	$in_data['refNumber'];								
									$amount_data  = $amount; 								
									 $pinv_id =''; 
									$invoices=  $fb_data->create_invoice_payment($inID, $amount_data);
									 if ($invoices['status'] != 'ok')
									 {
											  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
									  }
									   else 
									   {
										 $pinv_id =  $invoices['payment_id'];
										
										  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
										}
									
									$id = $this->general_model->insert_gateway_transaction_data($result,'pay_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount_data,$this->merchantID,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);  
								  }
							   }  
							   else
							   {
											$crtxnID = '';
											$inID    = '';
										$id      = $this->general_model->insert_gateway_transaction_data($result,'pay_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$this->merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields); 
										   
								}		   
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								if(!empty($refNumber))
								$ref_number = implode(',',$refNumber); 
								else
								$ref_number = '';
								$tr_date   =date('Y-m-d H:i:s');
								  $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 	
								if($chh_mail =='1')
								 {
								   
								   
								   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
								 } 
								   
									 
								 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
								 {
											$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
											$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
											$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
										   $card_type = $this->general_model->getType($card_no);
									 
												$card_data = array('cardMonth'   =>$expmonth,
														   'cardYear'	 =>$exyear, 
														  'CardType'     =>$card_type,
														  'CustomerCard' =>$card_no,
														  'CardCVV'      =>$cvv, 
														 'customerListID' =>$customerID, 
														 'companyID'     =>$companyID,
														  'merchantID'   => $this->merchantID,
														
														 'createdAt' 	=> date("Y-m-d H:i:s"),
														 'Billing_Addr1'	 =>$address1,
														 'Billing_Addr2'	 =>$address2,	 
															  'Billing_City'	 =>$city,
															  'Billing_State'	 =>$state,
															  'Billing_Country'	 =>$country,
															  'Billing_Contact'	 =>$phone,
															  'Billing_Zipcode'	 =>$zipcode,
														 );
											
												
											$id1 =    $this->card_model->process_card($card_data);	
									 }
				
								
							 $this->session->set_flashdata('success','Transaction Successful'); 
            			
							 } 
							 else
							{
											  $crtxnID ='';
											  
											
											   $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Payment Failed</strong></div>'); 
												$id = $this->general_model->insert_gateway_transaction_data($result,'pay_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
											 
							}
                         
                         
                         
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Authentication Failed</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
		}
		$invoice_IDs = array();
		if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
		}
	
		$receipt_data = array(
			'transaction_id' => $result['transaction_id'],
			'IP_address' => getClientIpAddr(),
			'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
			'proccess_btn_text' => 'Process New Sale',
			'sub_header' => 'Sale',
			'checkPlan'  => $checkPlan
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		$this->session->set_userdata("invoice_IDs",$invoice_IDs);
		
		
		redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
		    
			  $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				  
				$condition				= array('merchantID'=>$user_id );
			     $gateway        		= $this->general_model->get_gateway_data($user_id);
			     $data['gateways']      = $gateway['gateway'];
			     $data['gateway_url']   = $gateway['url'];
			     $data['stp_user']      = $gateway['stripeUser'];
   
				$compdata				= $this->fb_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
				$this->load->view('template/template_start', $data);
					
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
	    
	    
    	}
	
 
	public function create_customer_auth()
	{
	    
            
			$this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");	
	   	    
		
			if(!empty($this->input->post(null, true))){
			
			    
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			     	if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
        	$checkPlan = check_free_plan_transactions();
			  if($checkPlan && $gatlistval !="" && !empty($gt_result) )
			{
			
    			$payusername   = $gt_result['gatewayUsername'];
    		    $paypassword   = $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
    		    
    		
				
				$merchantID = $this->session->userdata('logged_in')['merchID'];
					    $customerID=$this->czsecurity->xssCleanPostInput('customerID');
			$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
				
				
			   $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			  
			  $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

		//call a function of Utilities.php to verify if there is any error with OAuth token. 
		$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

		//If IsFoundOAuthTokenError results True, means no error 
		//next is to move forward for the actual request 

		if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
          $cardID = $this->czsecurity->xssCleanPostInput('card_list');
						 
        
            
        
			  if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
              	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear,2);
						$expry    = $expmonth.$exyear;  
				
				 }else {
					  
						   
                   $cardID = $this->czsecurity->xssCleanPostInput('card_list');
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							$cvv=$card_data['CardCVV'];
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}

					}	
					
        
        
        
        
					$name    = $this->czsecurity->xssCleanPostInput('fistName').' '.$this->czsecurity->xssCleanPostInput('lastName');
					$address =	$this->czsecurity->xssCleanPostInput('address');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$state   = $this->czsecurity->xssCleanPostInput('state');
				     $amount = $this->czsecurity->xssCleanPostInput('totalamount');		
				    $zipcode  = ($this->czsecurity->xssCleanPostInput('zipcode'))?$this->czsecurity->xssCleanPostInput('zipcode'):'74035';	
					
					$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth ,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode)
					);
					    $request_data = json_encode($request_data);
						$result     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_AUTHORIZATION );
						
				       $response = $payAPI->jsonDecode($result['temp_json_response']); 
				 
	
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                            {	            $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                                        	 $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    				 		 $card_type      =$this->general_model->getType($card_no);
               								   $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    				       
                                         		$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                                            	
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										 	'CardType'     =>$card_type,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
										  'CustomerCard' =>$card_no,
										  'CardCVV'      =>$cvv, 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
    
                                         	
                          			$card=	$this->card_model->process_card($card_data) ;  
                          			
                          			
                                         }  
              
				
                 
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
				 
				 
				 
				 
				 
				
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					 if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					   
						$transactiondata['transactionType']    = 'pay_auth';	  
								$transactiondata['gatewayID']  = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
			            $transactiondata['gateway']   = "Paytrace";					
			            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			            if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}    
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong>.</div>'); 
				}		
				
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
			}	
    				
			$invoice_IDs = array();
			
		
			$receipt_data = array(
				'transaction_id' =>  isset($response) ? $response['transaction_id'] : '',
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_auth',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Authorize',
				'checkPlan'  => $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			
			redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
                       
				       
        }
              
		  
	}  
	
	public function create_customer_esale()
	{
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$amount = $this->czsecurity->xssCleanPostInput('totalamount');
			$checkPlan = check_free_plan_transactions();

			if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
				$payusername   = $gt_result['gatewayUsername'];
				$paypassword   = $gt_result['gatewayPassword'];
				$grant_type    = "password";

				$integratorId = $gt_result['gatewaySignature'];
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');

				$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];

				$payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
				$sec_code =     'WEB';

				if($payableAccount == '' || $payableAccount == 'new1') {
					$accountDetails = [
						'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
						'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
						'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'companyID'     => $companyID,
						'merchantID'   => $merchantID,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $sec_code
					];
				} else {
					$accountDetails = $this->card_model->get_single_card_data($payableAccount);
				}

				$payAPI = new PayTraceAPINEW();

				$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

				//call a function of Utilities.php to verify if there is any error with OAuth token.
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

				if(!$oauth_moveforward){
					$json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

					//set Authentication value based on the successful oAuth response.
					//Add a space between 'Bearer' and access _token
					$oauth_token = sprintf("Bearer %s", $json['access_token']);

					$request_data = array(
						"amount"          => $amount,
						"check"     => array(
							"account_number"=> $accountDetails['accountNumber'],
							"routing_number"=> $accountDetails['routeNumber'],
						),
						"integrator_id" => $integratorId,
						"billing_address" => array(
							"name" => $accountDetails['accountName'],
							"street_address" => $this->czsecurity->xssCleanPostInput('baddress1'). ', '.$this->czsecurity->xssCleanPostInput('baddress2'),
							"city" => $this->czsecurity->xssCleanPostInput('bcity'),
							"state" => $this->czsecurity->xssCleanPostInput('bstate'),
							"zip" => $this->czsecurity->xssCleanPostInput('bzipcode'),
						),
					);

					if($this->czsecurity->xssCleanPostInput('invoice_id')){
						$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 3);
                    	
						$request_data['invoice_id'] = $new_invoice_number;
					}

					$request_data = json_encode($request_data);
					$result       = $payAPI->processTransaction($oauth_token, $request_data, URL_ACH_SALE);
					$response     = $payAPI->jsonDecode($result['temp_json_response']);
					
					if ( $result['http_status_code']=='200' ){
				     	$response['http_status_code']=200;

						if($payableAccount == '' || $payableAccount == 'new1') {
							$id1 = $this->card_model->process_ack_account($accountDetails);
						}

						$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
						$ref_number = '';
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						
					
						$this->session->set_flashdata('success', 'Transaction Successful');
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $response['status_message']. '</div>');
					}
	
					$transactiondata = $invoice_IDs = array();
					$transactiondata['transactionID']       = $response['check_transaction_id'];
					$transactiondata['transactionStatus']    = $response['status_message'];
					$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					$transactiondata['transactionModified']    = date('Y-m-d H:i:s');
					$transactiondata['transactionCode']     = $response['response_code'];
	
					$transactiondata['transactionType']    = 'pay_sale';
					$transactiondata['gatewayID']          = $gatlistval;
					$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
					$transactiondata['customerListID']      = $customerID;
					$transactiondata['transactionAmount']   = $amount;
					$transactiondata['merchantID']   = $merchantID;
					$transactiondata['resellerID']   = $this->resellerID;
					$transactiondata['gateway']   = "Paytrace ECheck";
					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					if(!empty($this->transactionByUser)){
					    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
					$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

				} else {
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong></div>'); 
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
			}

			$invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

			$receipt_data = array(
				'transaction_id' => isset($response) ? $response['check_transaction_id'] : '',
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_esale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
		}
		redirect('FreshBooks_controllers/Transactions/create_customer_esale', 'refresh');
	}
	
	public function payment_erefund()
	{
		//Show a form here which collects someone's name and e-mail address
		$merchantID = $this->session->userdata('logged_in')['merchID'];

		if (!empty($this->input->post(null, true))) {
			
			$tID     = $this->czsecurity->xssCleanPostInput('txnID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$payusername   = $gt_result['gatewayUsername'];
			$paypassword   = $gt_result['gatewayPassword'];
			$grant_type    = "password";

			$integratorId = $gt_result['gatewaySignature'];
			
			$payAPI = new PayTraceAPINEW();

			$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

			//call a function of Utilities.php to verify if there is any error with OAuth token.
			$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			$amount     =  $paydata['transactionAmount'];

			$customerID = $paydata['customerListID'];
			
			if(!$oauth_moveforward){
	
				$json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token
				$oauth_token = sprintf("Bearer %s", $json['access_token']);

				$request_data = array(
					"check_transaction_id" => 3018735, //$tID,
					"integrator_id" => $integratorId,
				);

				$request_data = json_encode($request_data);
				$result       = $payAPI->processTransaction($oauth_token, $request_data, URL_ACH_REFUND_TRANSACTION);
				$response     = $payAPI->jsonDecode($result['temp_json_response']);

				if ( $result['http_status_code']=='200' ){
					$this->customer_model->update_refund_payment($tID, 'PAYTRACE');

					if (!empty($paydata['invoice_id'])) {
						$paymts   = explode(',', $paydata['tr_amount']);
						$invoices = explode(',', $paydata['invoice_id']);
						$ins_id   = '';
						foreach ($invoices as $k1 => $inv) {
	
							$refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paymts[$k1],
								'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
								'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
								'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
							$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
	
						}
	
						$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $this->merchantID));
						
						$user_id = $this->merchantID;
						$user    = $cusdata['qbwc_username'];
						$comp_id = $cusdata['id'];
						$ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
						$refund  = $amount;
	
						if (empty($ittem)) {
							$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
							
							exit;
						}
						$ins_data['customerID'] = $customerID;
	
						foreach ($invoices as $k => $inv) {
							$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
							if (!empty($in_data)) {
								$inv_pre    = $in_data['prefix'];
								$inv_po     = $in_data['postfix'] + 1;
								$new_inv_no = $inv_pre . $inv_po;
							}
							$ins_data['merchantDataID']    = $this->merchantID;
							$ins_data['creditDescription'] = "Credit as Refund";
							$ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
							$ins_data['creditDate']        = date('Y-m-d H:i:s');
							$ins_data['creditAmount']      = $paymts[$k];
							$ins_data['creditNumber']      = $new_inv_no;
							$ins_data['updatedAt']         = date('Y-m-d H:i:s');
							$ins_data['Type']              = "Payment";
							$ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);
	
							$item['itemListID']      = $ittem['ListID'];
							$item['itemDescription'] = $ittem['Name'];
							$item['itemPrice']       = $paymts[$k];
							$item['itemQuantity']    = 0;
							$item['crlineID']        = $ins_id;
							$acc_name                = $ittem['DepositToAccountName'];
							$acc_ID                  = $ittem['DepositToAccountRef'];
							$method_ID               = $ittem['PaymentMethodRef'];
							$method_name             = $ittem['PaymentMethodName'];
							$ins_data['updatedAt']   = date('Y-m-d H:i:s');
							$ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
							$refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $paymts[$k],
								'creditInvoiceID'                             => $invID, 'creditTransactionID'          => $tID,
								'creditTxnID'                                 => $ins_id, 'refundCustomerID'            => $customerID,
								'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
								'paymentMethod'                               => $method_ID, 'paymentMethodName'        => $method_name,
								'AccountRef'                                  => $acc_ID, 'AccountName'                 => $acc_name,
							);
	
							
	
							if ($ins_id && $ins) {
								$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));
	
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $ins_id, '1', '', $user);
							}
	
						}
	
					} else {
						$inv       = '';
						$ins_id    = '';
						$refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
							'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
							'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
							'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
						);
						$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
					}

					$this->session->set_flashdata('success', 'Successfully Refunded Payment');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $response['status_message'] . '</strong>.</div>');
				}
			} else {
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong></div>'); 
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $response['check_transaction_id'];
			$transactiondata['transactionStatus']  = $response['status_message'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']    = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = 'refund'; // $result['type'];
			$transactiondata['transactionCode']   = $response['response_code'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']         = $gatlistval;
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']  = $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = "Paytrace ECheck";
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

			redirect('FreshBooks_controllers/Transactions/echeck_transaction', 'refresh');
		}
	}	

	public function payment_evoid()
	{
		//Show a form here which collects someone's name and e-mail address
		$merchantID = $this->session->userdata('logged_in')['merchID'];

		if (!empty($this->input->post(null, true))) {
			
			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$payusername   = $gt_result['gatewayUsername'];
			$paypassword   = $gt_result['gatewayPassword'];
			$grant_type    = "password";

			$integratorId = $gt_result['gatewaySignature'];
			
			$payAPI = new PayTraceAPINEW();

			$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

			//call a function of Utilities.php to verify if there is any error with OAuth token.
			$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			$amount     =  $paydata['transactionAmount'];

			$customerID = $paydata['customerListID'];
			
			if(!$oauth_moveforward){
	
				$json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token
				$oauth_token = sprintf("Bearer %s", $json['access_token']);

				$request_data = array(
					"check_transaction_id" => $tID,
					"integrator_id" => $integratorId,
				);

				$request_data = json_encode($request_data);
				$result       = $payAPI->processTransaction($oauth_token, $request_data, URL_ACH_VOID_TRANSACTION);
				$response     = $payAPI->jsonDecode($result['temp_json_response']);

				if ( $result['http_status_code']=='200' ){
					$condition = array('transactionID' => $tID);
					$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

					$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

					$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $response['status_message'] . '</strong>.</div>');
				}
			} else {
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong></div>'); 
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $tID;
			$transactiondata['transactionStatus']  = $response['status_message'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']    = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = 'void'; // $result['type'];
			$transactiondata['transactionCode']   = $response['response_code'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']         = $gatlistval;
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']  = $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = "Paytrace ECheck";
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

			redirect('FreshBooks_controllers/Transactions/evoid_transaction', 'refresh');
		}
	}

	public function create_customer_capture_old()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
			
			    $tID         = $this->czsecurity->xssCleanPostInput('txnID2');
			      $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			    $gatlistval  = $paydata['gatewayID'];
				$merchantID = $this->session->userdata('logged_in')['merchID']; 
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
			
			
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			    
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
			
				 $amount  =  $paydata['transactionAmount']; 
				 
				
				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_CAPTURE);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
				   
			
				   
				   
			    	
				}
				
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
				
				    $this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']   = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']           = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_capture';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']  =  $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
			            $transactiondata['gateway']   = "Paytrace";					
			            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			            if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}    
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed.</strong></div>'); 
		}		
			 		   
			   
                       
				   redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
        }
				
		       
	
	}
	
	
	
	public function create_customer_void_old()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
			     $tID       = $this->czsecurity->xssCleanPostInput('txnvoidID2');
				   $con     = array('transactionID'=>$tID);
				 $paydata   = $this->general_model->get_row_data('customer_transaction',$con);
	            $gatlistval  = $paydata['gatewayID'];
				$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type   = "password";
			   $merchantID = $this->session->userdata('logged_in')['merchID'];
			  
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			 
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
			
				 $amount  =  $paydata['transactionAmount']; 
				 
				
				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_VOID_TRANSACTION);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    	
				}
				
			   
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"3");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
				
				    $this->session->set_flashdata('success','Transaction Successfully Cancelled. '.$response['status_message']); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
                }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']           = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_void';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']  = $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
			            $transactiondata['gateway']   = "Paytrace";
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed.</strong></div>'); 
		}		
					   
			   
                       
				        redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
        }
				
	
	}
	

	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
			
			 if($this->session->userdata('logged_in') ){
			           $merchantID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
			    $tID         = $this->czsecurity->xssCleanPostInput('txnID2');
			      $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			    $gatlistval  = $paydata['gatewayID'];
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
				$integratorId = $gt_result['gatewaySignature'];

    		    $grant_type    = "password";
			
			
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			    
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      
				
				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_CAPTURE);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
				   
				}
				
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 	
				 	$card_last_number = $paydata['transactionCard'];

				 	$card_detail = $this->db1->select('CardType')->from('customer_card_data')->where('merchantID = '.$merchantID.' AND customerCardfriendlyName like "%'.$card_last_number.'%"')->get()->row_array();

				 	$cardType = isset($card_detail['CardType']) ? strtolower($card_detail['CardType']) : '';

				 	// add level three data in transaction
                    if($response['success']){
                        $level_three_data = [
                            'card_no' => '',
                            'merchID' => $merchantID,
                            'amount' => $amount,
                            'token' => $oauth_token,
                            'integrator_id' => $integratorId,
                            'transaction_id' => $response['transaction_id'],
                            'invoice_id' => '',
                            'gateway' => 3,
                            'card_type' => $cardType
                        ];
                        addlevelThreeDataInTransaction($level_three_data);
                    }

					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 
					if($chh_mail =='1')
                            {
                                 
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
							
				    $this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']   = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionmodified']     = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_capture';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']  =  $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paytrace";					
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}    
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Authentication failed.</div>'); 
		}		
		$invoice_IDs = array();
			
		
			$receipt_data = array(
				'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Capture',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
			$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($response['transaction_id']== ''){
				$response['transaction_id'] ='null';
			}
			redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$response['transaction_id'],  'refresh');	 		   
			   
                       
				   
        }
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	
	
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			   
			      if($this->session->userdata('logged_in') ){
			           $merchantID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
			
			     $tID       = $this->czsecurity->xssCleanPostInput('txnvoidID2');
				   $con     = array('transactionID'=>$tID);
				 $paydata   = $this->general_model->get_row_data('customer_transaction',$con);
	            $gatlistval  = $paydata['gatewayID'];
				$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type   = "password";
			  
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			 
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      
				 
				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_VOID_TRANSACTION);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    	
				}
				
			   
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
				
				    $this->session->set_flashdata('success',$response['status_message']); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
                }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']           = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_void';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']  = $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paytrace";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Authentication failed.</div>'); 
		}		
					   
			   
                       
				           redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
        }
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	

	public function create_customer_refund()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
			
	             $tID       = $this->czsecurity->xssCleanPostInput('paytxnID');
				  $con      = array('transactionID'=>$tID);
				 $paydata   = $this->general_model->get_row_data('customer_transaction',$con);
			     $gatlistval  = $paydata['gatewayID'];
				  
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
			  
			
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
				 $amount1  =  $paydata['transactionAmount']; 
				  $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
				  $amount  =  $total;
				if($paydata['transactionCode']=='200'){
					 $request_data = array( "transaction_id" => $tID,'amount'=>$total );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_TRID_REFUND);
				     $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    		
				}
				
			   
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
				 
				 
			   		    $val = array(
    					'merchantID' => $paydata['merchantID'],
    				);
				
    				$merchID =$paydata['merchantID'];
        		
    		
            		$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
                   $rs_data = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=>$merchID));
                	 	$redID = $rs_data['resellerID'];
                	$condition1 = array('resellerID'=>$redID);
                	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        						 $domain1 = $sub1['merchantPortalURL'];
        						 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
                    
                    $subdomain     = $Freshbooks_data['sub_domain'];
                    $key           = $Freshbooks_data['secretKey'];
                    $accessToken   = $Freshbooks_data['accessToken'];
                    $access_token_secret = $Freshbooks_data['oauth_token_secret'];
             
                    define('OAUTH_CONSUMER_KEY', $subdomain);
                    define('OAUTH_CONSUMER_SECRET', $key);
                    define('OAUTH_CALLBACK', $URL1);
			   	  	  
				 if(!empty($paydata['invoiceID']))
				{
				     $refund =$amount;
				    $ref_inv =explode(',',$paydata['invoiceID']);
				    if(count($ref_inv)>1)
				    {
				      $ref_inv_amount = json_decode($paydata['invoiceRefID']);
				      $imn_amount =  $ref_inv_amount->inv_amount; 
				      $p_ids     =   explode(',',$paydata['qbListTxnID']);
				     
				      $inv_array = array();
				      foreach($ref_inv as $k=> $r_inv)
				      {
				         $inv_data=$this->general_model->get_select_data('Freshbooks_test_invoice',array('DueDate'),array('merchantID'=>$merchID,'invoiceID'=>$r_inv));
				          
				          if(!empty($inv_data))
				          {
				           $due_date=   date('Y-m-d',strtotime($inv_data['DueDate']));
				          }else{
				               $due_date=   date('Y-m-d');  
				          }
				          
				          $re_amount = $imn_amount[$k];
				          if($refund > 0 && $imn_amount[$k] >0)
				          {
				              
				              
				            $p_refund1=0;  
				              
				            $p_id  =  $p_ids[$k];
				              
				          	$ittem =  $this->fb_customer_model->get_invoice_item_data($r_inv);
				           
				            if($refund <= $imn_amount[$k] )
				            {
				                $p_refund1 =  $refund;
				                  
				                  
				                   $p_refund =   $imn_amount[$k] -$refund;
				                 
				                  $re_amount =$imn_amount[$k] -$refund;
				                  $refund =0;
				               
				            }
				            else
				            {
				                 $p_refund =0;
				                 $p_refund1 =  $imn_amount[$k];
				                 $refund=$refund-$imn_amount[$k];
				                 
				                 $re_amount =0;
				            }
				            
        				               
                            if(isset($accessToken) && isset($access_token_secret))
                            {
                                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                           
                              $payment = '<?xml version="1.0" encoding="utf-8"?>  
                    						<request method="payment.update">  
                    						  <payment>         
                    						    
                                                    <payment_id>'.$p_id.'</payment_id>
                                                    <amount>'.$p_refund.'</amount>
                                                    <notes>Payment refund for invoice:'.$r_inv.' '.$p_refund1.' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>              
                    						  
                    						</request>';
                                  
                                  $invoices = $c->edit_payment($payment);
                                 
                             
                               
                        			if ($invoices['status'] != 'ok') 
                        			{
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        
                        			
                        				
                        			}
                        			else {
                        			        $ins_id = $p_id;
                                            $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$p_refund1,
            				  	           'creditInvoiceID'=>$r_inv,'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           );	
            				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
            				  	        $lineArray='';
            				 
                            			  foreach($ittem as $item_val)
                            			  {
                            					
                            				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
                        					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
                        					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
                        					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
                        					$lineArray .= '<type>Item</type></line>';  
                            					
                            			  }		
                            					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
                        					$lineArray .= '<description>This is used to refund Incoive payment</description>';
                        					$lineArray .= '<unit_cost>'.(-$p_refund1).'</unit_cost>';
                        					$lineArray .= '<quantity>1</quantity>';
                        					$lineArray .= '<type>Item</type></line>'; 	
            					
            					
            					
                        				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update"> 
                                     		 <invoice> 
                                     		  <invoice_id>'.$r_inv.'</invoice_id> 
                                     		      <date>'.$due_date.'</date>
                        				          <lines>'.$lineArray.'</lines>  
                        						</invoice>  
                        						</request> ';
                        		           
                                             $invoices1 = $c->add_invoices($invoice);
                        				  	          
            				  	     
                        				$this->session->set_flashdata('success','Successfully Refunded Payment'); 
                        				
                        				
                        			}
                    			
                                
                            }	
                            
                            
                            $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount;
        				           
				          }else{
				               
				                $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount; 
				           }  
				            
				          
				      }
				       $this->general_model->update_row_data('customer_transaction',$con,array('invoiceRefID'=>json_encode($inv_array)));
				     }
				     else
				     {
				  
        			$ittem =  $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);
    		
                    $p_id     = $paydata['qbListTxnID'];
                    $p_amount = $paydata['transactionAmount']-$amount; 
                    
                    if(isset($accessToken) && isset($access_token_secret))
                    {
                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                   
                      $payment = '<?xml version="1.0" encoding="utf-8"?>  
            						<request method="payment.update">  
            						  <payment>         
            						    
                                            <payment_id>'.$p_id.'</payment_id>
                                            <amount>'.$p_amount.'</amount>
                                            <notes>Payment refund for invoice:'.$paydata['invoiceID'].' '.$amount.' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>              
            						  
            						</request>';
                          
                          $invoices = $c->edit_payment($payment);
                         
                     
                       
                			if ($invoices['status'] != 'ok') 
                			{
                
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                
                			
                				
                			}
                			else {
                			        $ins_id = $p_id;
                                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$amount,
    				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
    				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
    				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
    				  	           );	
    				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
    				  	        $lineArray='';
    				 
    			  foreach($ittem as $item_val)
    			  {
    					
    				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
					$lineArray .= '<type>Item</type></line>';  
    					
    			  }		
    					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
					$lineArray .= '<description>This is used to refund Incoive payment</description>';
					$lineArray .= '<unit_cost>'.(-$amount).'</unit_cost>';
					$lineArray .= '<quantity>1</quantity>';
					$lineArray .= '<type>Item</type></line>'; 	
    					
    					
    					
    				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update"> 
                 		 <invoice> 
                 		  <invoice_id>'.$paydata['invoiceID'].'</invoice_id> 
                 		  
    				          <lines>'.$lineArray.'</lines>  
    						</invoice>  
    						</request> ';
    		           
                         $invoices1 = $c->add_invoices($invoice);
    				  	          
    				  	     
                				$this->session->set_flashdata('success','Successfully Refunded Payment'); 
                				
                			
                			}
            			}			
				     }	   
					   
    		  
             }
             
             
				 
				
			
					$this->fb_customer_model->update_refund_payment($tID,'PAYTRACE');
				
				    $this->session->set_flashdata('success','Success '.$response['status_message']); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
                }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_refund';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   
					      if(!empty($paydata['invoiceID']))
        			     	{
        				      $transactiondata['invoiceID']  = $paydata['invoiceID'];
        			     	}
					     $transactiondata['merchantID']   = $paydata['merchantID'];
					    $transactiondata['resellerID']   = $this->resellerID;
			            $transactiondata['gateway']   = "Paytrace";
			            $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
			            if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed.</strong></div>'); 
		}		
					   
		$invoice_IDs = array();
				
			 
				 $receipt_data = array(
					 'proccess_url' => 'FreshBooks_controllers/Transactions/payment_refund',
					 'proccess_btn_text' => 'Process New Refund',
					 'sub_header' => 'Refund',
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 
				 if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($response['transaction_id'] == ''){
						$response['transaction_id'] ='null';
					}
				 redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$response['transaction_id'],  'refresh');
                       
			
      }
              
				
		       
	
	}
	
	
	
	
	 public function getError($eee){ 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }
	 
	 
	 
  
       
       	 
public function pay_multi_invoice()
{
    
    if($this->session->userdata('logged_in') )
    {
       $merchID = $this->session->userdata('logged_in')['merchID'];
    	}
    	if($this->session->userdata('user_logged_in') )
    	{
          $merchID = $this->session->userdata('user_logged_in')['merchantID'];
    	} 
	      $invoicp_id='';
	       $merchantID  = $merchID;
			        	
			$rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$merchID));
			 $resellerID =	$rs_daata['resellerID'];
			 $condition1 = array('resellerID'=>$resellerID);
        	
         	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			 $domain1 = $sub1['merchantPortalURL'];
			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
	 				
				$val = array(
					'merchantID' => $merchID,
				);
				$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
            
            $subdomain     = $Freshbooks_data['sub_domain'];
            $key           = $Freshbooks_data['secretKey'];
            $accessToken   = $Freshbooks_data['accessToken'];
            $access_token_secret = $Freshbooks_data['oauth_token_secret'];
     
            define('OAUTH_CONSUMER_KEY', $subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
         
		 
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   
		 
             	$payusername   = $gt_result['gatewayUsername'];
    		    $paypassword   = $gt_result['gatewayPassword'];
				$integratorId = $gt_result['gatewaySignature'];

    		    $grant_type    = "password";
    		    
    		 
	$checkPlan = check_free_plan_transactions();
			
	 if($checkPlan && $cardID!="" || $gateway!="")
	 {  
     
		$invices =$this->czsecurity->xssCleanPostInput('multi_inv');
		if(!empty($invices))
		{ 
		    
		    
		    foreach($invices as $invoiceID)
		    {
		    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);  
	     	$in_data =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
		 
		 if(!empty($in_data)){ 
			
				$Customer_ListID = $in_data['CustomerListID'];
         
           if($cardID=='new1')
           {
                         
			        	$cardID_upd  =$cardID;
			        		$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
						
				        $merchantID = $merchID;
          
           
                         $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID' => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                           $this->db1->select('*')->from('customer_card_data');
                           $qq =  $this->db1->get();
          
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                         }else{
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
				 $card_data      =   $this->card_model->get_single_card_data($cardID);
				 
			 if(!empty($card_data))
			 {
						
						   $cr_amount = 0;
							 $amount  =	 $in_data['BalanceRemaining']; 
					
					         $amount           =	$pay_amounts;
							$amount           = $amount-$cr_amount;
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$cvv      = $card_data['CardCVV'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						  
						$payAPI  = new PayTraceAPINEW();	
					 
					$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                    
						//call a function of Utilities.php to verify if there is any error with OAuth token. 
						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                    

		          if(!$oauth_moveforward){ 
		
		
		            $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token 
				$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
				$name = $in_data['fullName'];
				$address = $in_data['ShipAddress_Addr1'];
				$city = $in_data['ShipAddress_City'];
				$state = $in_data['ShipAddress_State'];
				$zipcode = ($in_data['ShipAddress_PostalCode'])?$in_data['ShipAddress_PostalCode']:'85284';
				
			
			
				$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);  
                 
				      $request_data = json_encode($request_data); 
				      
			           $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
				       $response  = $payAPI->jsonDecode($result['temp_json_response']); 
	               
				   if ( $result['http_status_code']=='200' ){
			
						// add level three data in transaction
	                    if($response['success']){
	                        $level_three_data = [
	                            'card_no' => $card_no,
	                            'merchID' => $merchID,
	                            'amount' => $amount,
	                            'token' => $oauth_token,
	                            'integrator_id' => $integratorId,
	                            'transaction_id' => $response['transaction_id'],
	                            'invoice_id' => $invoice_number,
	                            'gateway' => 3,
	                        ];
	                        addlevelThreeDataInTransaction($level_three_data);
	                    }
    
        
        if(isset($accessToken) && isset($access_token_secret))
        {
            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);

            $payment = '<?xml version="1.0" encoding="utf-8"?>  
						<request method="payment.create">  
						  <payment>         
						    <invoice_id>'.$invoiceID.'</invoice_id>               
						    <amount>'.$amount.'</amount>             
						    <currency_code>USD</currency_code> 
						    <type>Check</type>                   
						  </payment>  
						</request>';
             
              $invoices = $c->add_payment($payment);
			if ($invoices['status'] !='ok') {
			
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>');

				 redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');

				
			}
			else {
			    $invoicp_id = $invoices['payment_id'];
				$this->session->set_flashdata('success','Success'); 
				
			} 
				}	   
				       
				       
				   } else{
					   
							if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
					
				  }	   
					   $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					   
						$transactiondata['transactionType']    = 'pay_sale';
						$transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					   $transactiondata['customerListID']      =$in_data['CustomerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['invoiceID']   = $invoiceID;
					      $transactiondata['qbListTxnID'] = $invoicp_id;
					    $transactiondata['resellerID']   = $resellerID;
			            $transactiondata['gateway']   = "Paytrace";
    				            
    				   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
    				    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}         
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				    
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication not valid</strong>.</div>'); 
				}	   
			
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
    	 }	 
    			 
    	 }
    	 else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Please select invoices</strong>.</div>'); 
			 } 
			 
			 
	        }
	        else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
		  }		
		  
		if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
			 
		   	  redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');

    
}     


	
}