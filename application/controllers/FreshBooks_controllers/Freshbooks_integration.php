<?php

class Freshbooks_integration extends CI_Controller
{
	public function __construct()
	{

	  
		parent::__construct();
		include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';
	    $this->load->model('general_model');
	    $this->load->helper('url');
	
	
	}
	
	public function index(){
    }

/**************** Function for: Add user key details ************************/

    public function user()
 	{
	 if(!empty($this->input->post(null, true))){
			
			$input_data['sub_domain']	   = $this->czsecurity->xssCleanPostInput('subdomain');
			$input_data['secretKey']	   = $this->czsecurity->xssCleanPostInput('secretKey');
				$merchID  = $this->session->userdata('logged_in')['merchID'];
				    $merchantID = $merchID;
				$input_data['merchantID']	   = $merchantID;
		 $this->general_model->insert_row('tbl_freshbooks', $input_data); 
					
		 redirect(base_url('FreshBooks_controllers/Freshbooks_integration/freshbooks_auth_process'));
		
	}
					$data['primary_nav'] 	= primary_nav();
					$data['template'] 		= template_variable();
										 
					$this->load->view('template/template_start', $data);
					$this->load->view('template/page_head', $data);
					$this->load->view('pages/app_integration');
					$this->load->view('template/page_footer',$data);
					$this->load->view('template/template_end', $data);
   }
	
/************** End Of User Function  ***************/

/******************* Freshbooks Authorization ********************/	

   public function auth()
   {
  
		$merchantID     = $this->session->userdata('logged_in')['merchID'];
		$con            = array('merchantID'=> $merchantID);  
		$resellerID    = $this->session->userdata('logged_in')['resellerID'];
		$redID = $resellerID;
		$condition = array('resellerID'=>$resellerID); 
		$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition);
		$domain1 = $sub1['portalprefix'];
		$domainURL = $sub1['merchantPortalURL'];

		$fb_data =  $this->general_model->get_row_data('tbl_freshbooks_config',array('id'=>1));
		$clientID =  $fb_data['ClientID'];
		$key =  $fb_data['ClientSecret'];
		$redirectURL =  $fb_data['FreshBooksURL'];
	
		if(isset($_GET['code']))	  
		{
			
			$code=$_GET['code'];
			
			$curl = curl_init();
	
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.freshbooks.com/auth/oauth/token",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "{\n    \"grant_type\": \"authorization_code\",\n    \"client_secret\": \"$key\",\n    \"code\": \"$code\",\n    \"client_id\": \"$clientID\",\n    \"redirect_uri\": \"$redirectURL\"\n}",
				CURLOPT_HTTPHEADER => array(
						"api-version: alpha",
						"cache-control: no-cache",
						"content-type: application/json"
					),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
				$errrr= "cURL Error #:". $err;
				$this->session->set_flashdata('success', $errrr);
                  
			} 
			else 
			{
				$response=json_decode($response);
		
				$in_data['accessToken']          = $response->access_token;
				$in_data['refreshToken']   = $response->refresh_token;           
				$in_data['oauth_token_secret']  = $code;
				$in_data['merchantID']  = $merchantID;
				
				$chk_condition = array('merchantID'=>$merchantID);
				$num = $this->general_model->get_num_rows('tbl_freshbooks',$chk_condition);
				if($num > 0)
				{
					$this->general_model->update_row_data('tbl_freshbooks',$chk_condition, $in_data);
				}
				else
				{
					$this->general_model->insert_row('tbl_freshbooks',$in_data); 
				}
						
				$data['appIntegration']    = "3";
				$data['merchantID']        = $merchantID;
			
				$app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);
				if(!empty($app_integration))
				{
				
					$this->general_model->update_row_data('app_integration_setting',$chk_condition, $data);
					$user = $this->session->userdata('logged_in');
					$user ['active_app'] = '3';
					$this->session->set_userdata('logged_in',$user);
				}
				else
				{
					$this->general_model->insert_row('app_integration_setting', $data);
					$user = $this->session->userdata('logged_in');
					$user ['active_app'] = '3';
					$this->session->set_userdata('logged_in',$user);
				}
			
            	redirect($domainURL . 'FreshBooks_controllers/Freshbooks_integration/success_auth');
			}
		}

		$gt_result 	 = $this->general_model->get_row_data('tbl_freshbooks', $con);
		if(!empty($gt_result)){
			$subdomain     = $gt_result['sub_domain'];
			$key           = $gt_result['secretKey'];
		} else {
			$token = $this->general_model->get_row_data('tbl_freshbooks_config',array('id'=>'1'));
			$key = $token['ClientSecret'];
		}
	
		$URL1 = $redirectURL;
					
		define('OAUTH_CONSUMER_KEY', $subdomain);
		define('OAUTH_CONSUMER_SECRET', $key);
		define('OAUTH_CALLBACK', $URL1);
	    
       	if(isset($_SESSION['oauth_token']) && isset($_SESSION['oauth_token_secret']))
        {
        	$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
        }
        
        else if(isset($_GET['oauth_token']) && isset($_GET['oauth_verifier']))
        {
           
        	$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain);
        	$access_token = $c->getAccessToken($_GET['oauth_token'], $_GET['oauth_verifier']);
        
        	$merchID  = $this->session->userdata('logged_in')['merchID'];
			$merchantID = $merchID;
			$in_data['accessToken']    = $access_token['oauth_token'];
			$in_data['oauth_token_secret']  = $access_token['oauth_token_secret'];
		
			$chk_condition = array('merchantID'=>$merchantID);
			$this->general_model->update_row_data('tbl_freshbooks',$chk_condition, $in_data);
						 
						 
			$data['appIntegration']    = "3";
			$data['merchantID']        = $merchantID;
			
			$app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);
			if(!empty($app_integration))
			{
				$this->general_model->update_row_data('app_integration_setting',$chk_condition, $data);
				$user = $this->session->userdata('logged_in');
				$user ['active_app'] = '3';
				$this->session->set_userdata('logged_in',$user);
			}
			else
			{
					$this->general_model->insert_row('app_integration_setting', $data);
					$user = $this->session->userdata('logged_in');
					$user ['active_app'] = '3';
					$this->session->set_userdata('logged_in',$user);
			}
			redirect(base_url('FreshBooks_controllers/Freshbooks_integration/success_auth'));
     
        }
        
      	else if(isset($subdomain))
        {
           
        	$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain);
      
          	redirect($c->getLoginUrl());
          
        }

    	else
        {
        	$_SESSION['subdomain'] = OAUTH_CONSUMER_KEY; 
        	redirect(base_url('FreshBooks_controllers/Freshbooks_integration/auth'));
        }

	}
	  
	  
/***************** End of Authorization *********************/

/*********** Funcrion for: Authentication Process ************/

public function freshbooks_auth_process()
	{
	         $data['primary_nav']  = primary_nav();
			 $data['template']   = template_variable();
			 
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('FreshBooks_views/freshbooks_auth_process');
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
	}

/************* End *************/


/************** Function for: Successful Autorization Message ***************/

	public function success_auth()
	{
	         $data['primary_nav']  = primary_nav();
			 $data['template']   = template_variable();
			 
			  $merchID  = $this->session->userdata('logged_in')['merchID'];
			  $condition = array('merchID'=> $merchID); 
		   	  $this->general_model->update_row_data('tbl_merchant_data',$condition,array('is_integrate'=>1));
		   	  $user = $this->session->userdata('logged_in');
			  $user['is_integrate'] = '1';
			   $this->session->set_userdata('logged_in',$user);

				/* Start campaign in hatchbuck CRM*/  
	            $this->load->library('hatchBuckAPI');
	            
	            $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$condition);
	            $merchantData['merchant_type'] = 'Freshbooks';        
	            
	            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	            if($status['statusCode'] == 400){
	                $resource = $this->hatchbuckapi->createContact($merchantData);
	                if($resource['contactID'] != '0'){
	                    $contact_id = $resource['contactID'];
	                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	                }
	            }
				/* End campaign in hatchbuck CRM*/
				
				/* Freshbook Data sync starts here */
				
				$Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', ['merchantID' => $merchID]);
				$syncObject = new FreshbooksNew($Freshbooks_data['sub_domain'], $Freshbooks_data['secretKey'], null, $Freshbooks_data['sub_domain'], $Freshbooks_data['accessToken'], $Freshbooks_data['oauth_token_secret']);
				
				$syncObject->syncAll();
				
				/* Freshbook Data sync starts here */

			   $gatew                  = $this->general_model->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchID));
				if (!empty($gatew)) {
					$merchID  = $this->session->userdata('logged_in')['merchID'];
					$condition = array('merchID'=> $merchID); 
					   $this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
					   $user = $this->session->userdata('logged_in');
					$user['firstLogin'] = '1';
					 $this->session->set_userdata('logged_in',$user);
					if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchID,'systemMail'=>1)) == '0')
						   {  
							   
							   $fromEmail       = $user['merchantEmail'];
							  
								 $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
							  
								foreach($templatedatas as $templatedata){
								 $insert_data = array('templateName'  =>$templatedata['templateName'],
										  'templateType'    => $templatedata['templateType'],
										   'merchantID'      => $merchID,
										   'fromEmail'      => DEFAULT_FROM_EMAIL,
										   'message'		  => $templatedata['message'],
										   'emailSubject'   => $templatedata['emailSubject'],
										   'createdAt'      => date('Y-m-d H:i:s') 	
									   );
							   $this->general_model->insert_row('tbl_email_template', $insert_data);
							  
							  }
						   }
					
					
						 
				   
						  redirect(base_url('FreshBooks_controllers/home/index'));
					
				}else{
					redirect(base_url('firstlogin/dashboard_first_login'));
				}
				
			  
 
	}
	public function final_success_auth()
	{
	         $data['primary_nav']  = primary_nav();
			 $data['template']   = template_variable();
			 
			  $merchID  = $this->session->userdata('logged_in')['merchID'];
			  $condition = array('merchID'=> $merchID); 
		   	  $this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
		   	  $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$condition);

		   	  if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
              {
		   	  	/* Start campaign in hatchbuck CRM*/  
	            $this->load->library('hatchBuckAPI');

	            $merchantData['merchant_type'] = 'Freshbooks';        
	            
	            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	            if($status['statusCode'] == 400){
	                $resource = $this->hatchbuckapi->createContact($merchantData);
	                if($resource['contactID'] != '0'){
	                    $contact_id = $resource['contactID'];
	                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	                }
	            }
	            /* End campaign in hatchbuck CRM*/ 
	          }
		   	  $user = $this->session->userdata('logged_in');
			  $user['firstLogin'] = '1';
			   $this->session->set_userdata('logged_in',$user);
			  if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchID,'systemMail'=>1)) == '0')
					 {  
					     
						 $fromEmail       = $user['merchantEmail'];
						
						   $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						
						  foreach($templatedatas as $templatedata){
						   $insert_data = array('templateName'  =>$templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									 'merchantID'      => $merchID,
									 'fromEmail'      => DEFAULT_FROM_EMAIL,
									 'message'		  => $templatedata['message'],
									 'emailSubject'   => $templatedata['emailSubject'],
									 'createdAt'      => date('Y-m-d H:i:s') 	
								 );
						 $this->general_model->insert_row('tbl_email_template', $insert_data);
						
						}
	                 }
			  
			  
				
					redirect(base_url('FreshBooks_controllers/home/index'));
		
 
	}
/****************** End ********************/	
	/**
     * Method to disconnect freshbook account
     * 
     * @return void
     */
	public function revoke(){
		if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $merchantID            = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $merchantID            = $data['login_info']['merchantID'];
        }

		$fbData  =  $this->general_model->get_row_data('tbl_freshbooks_config', array('id' => 1));
		$clientID  =  $fbData['ClientID'];
		$key       =  $fbData['ClientSecret'];

		$condition         = array('merchantID' => $merchantID);
        $fbMerchantData = $this->general_model->get_row_data('tbl_freshbooks', $condition);

		$revokeData = [
			'client_id' => $clientID,
			'client_secret' => $key,
			'token' => $fbMerchantData['refreshToken'],
		];
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => FRESHBOOK_DISCONNECT_URL,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => json_encode($revokeData),
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$response = json_decode($response, true);
		if(empty($response)){
			$this->session->set_flashdata('success', "Account Disconnected");
		} else {
			$this->session->set_flashdata('success', "SomeThing went wrong");
		}
		redirect('FreshBooks_controllers/home/company', 'refresh');
	}
}
	
