<?php
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;


class UsaePay extends CI_Controller
{
    private  $resellerID;
	private $transactionByUser;
	private $merchantID;
	public function __construct()
	{
		parent::__construct();
	    
		
	    require_once APPPATH."third_party/usaepay/usaepay.php";	
		$this->load->config('usaePay');
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
	    $this->load->model('customer_model');
		$this->load->model('card_model');
	    $this->load->library('form_validation');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
       $this->db1 = $this->load->database('otherdb', true); 
  if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='3' )
		  {
		  	$logged_in_data = $this->session->userdata('logged_in');
			$this->resellerID = $logged_in_data['resellerID'];
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		    $this->merchantID = $logged_in_data['merchID'];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		    $this->merchantID = $merchID;
		  }
		  else
		  {
			redirect('login','refresh');
		  }
		  
		  
		   $val = array(
             'merchantID' => $this->merchantID,
             );
		  	$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		  	if(empty($Freshbooks_data))
		  	{
		  	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>'); 
                
		  	  redirect('FreshBoolks_controllers/home/index','refresh');
		  	  
		  	}
            
	}
	
	
	
	public function index(){
	    
	
	   	redirect('FreshBooks_controllers/home','refresh'); 
	}
	
	
	
	
	
	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
	       
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('sch_gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
        	$checkPlan = check_free_plan_transactions();
              
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		       
		       
		     
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	 $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				 
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;	
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			     $in_data =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
			   
			      $ref_number= array();
			    if(!empty( $in_data)) 
			    {
			          
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  $username   = $gt_result['gatewayUsername'];
    			      $password   = $gt_result['gatewayPassword'];
    			      $customerID =  $in_data['Customer_ListID'];
    			     
    			     $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
        			 $c_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
        		   	 $companyID  = $c_data['companyID']; 
    			   
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$address2 = $card_data['Billing_Addr2'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }                              
         
                		if( $in_data['BalanceRemaining'] > 0)
        				{
				    
                    	$crtxnID='';  
                        $invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$username;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Invoice Payment";	// description of charge
					//	$transaction->refnum="47100443"; //refnum stored from the $tran->refnum of a previous transaction
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
						
						
					
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
						
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zipcode;
							
							
					     	$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zipcode;
						
							
							$transaction->amount = $amount;
						 
							$transaction->Process();
					
                     if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                            $txnID      = $in_data['invoiceID'];  
							 $ispaid 	 = 1;
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 0;
							 $app_amount = $in_data['Total_payment']+$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('invoiceID'=>$in_data['invoiceID'],'merchantID'=>$user_id  );
							 
							 $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
                          
                       $val = array(
							'merchantID' => $user_id,
						);
						$data = $this->general_model->get_row_data('QBO_token',$val);

						$accessToken  = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];
						$dataService  = DataService::Configure(array(
						 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
						));
		
        			    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        	            
        	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
        				
        				
        				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        				{
        					$theInvoice = current($targetInvoiceArray);
        			
                				$updatedInvoice = Invoice::update($theInvoice, [
                				    "sparse" => 'true'
                				]);
                				
                				$updatedResult = $dataService->Update($updatedInvoice);
                				
                				$newPaymentObj = Payment::create([
                				    "TotalAmt" => $amount,
                				    "SyncToken" => $updatedResult->SyncToken,
                				    "CustomerRef" => $updatedResult->CustomerRef,
                				    "Line" => [
                				     "LinkedTxn" =>[
                				            "TxnId" => $updatedResult->Id,
                				            "TxnType" => "Invoice",
                				        ],    
                				       "Amount" => $amount
                			        ]
                				    ]);
                             
                				$savedPayment = $dataService->Add($newPaymentObj);
                				
                		    	$crtxnID=	$savedPayment->Id;
                				
                				$error = $dataService->getLastError();
                              
                			    if ($error != null) 
                			    {
                	
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                
                				$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); 
                				
                				$qbID=$trID;
                				
                			   }
                    		   else 
                    		   {
									   $st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
									   $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
									   $ref_number =  $in_data['refNumber'];
									  $tr_date   =date('Y-m-d H:i:s');
										  $toEmail = $c_data['userEmail']; $company=$c_data['companyName']; $customer = $c_data['fullName'];  
                    		        	if($chh_mail =='1')
                    						 {
                    						   
                    						 
                    						   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
											 }  
                    				$this->session->set_flashdata('success','Successfully Processed Invoice'); 
                    			}
                                      
                          
        			    	}    
                          
							 
							 $qbID=$invoiceID; 
                     }
                     else
                     {
                           $msg = $transaction->result;
                           $trID = $transaction->refnum;
                           $res =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                           
                         
                     }		
						
                       $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$invoiceID, false, $this->transactionByUser);  
                  
		             }
            		    else        
                        {
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                            
                        }
			    }
			    else
			    {
			       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');  
			        
			    }
		   
		   }
		   else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			
			if ($cusproID != "") {
				$trans_id = $transaction->refnum;
				$invoice_IDs  = array();
				$receipt_data = array(
					'proccess_url'      => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
					'proccess_btn_text' => 'Process New Invoice',
					'sub_header'        => 'Sale',
					'checkPlan'         => $checkPlan
				);
	
				$this->session->set_userdata("receipt_data", $receipt_data);
				$this->session->set_userdata("invoice_IDs", $invoice_IDs);
				$this->session->set_userdata("in_data", $in_data);
				if ($cusproID == "1") {
					redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
	
				}
	
				redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $cusproID, 'refresh');
			} else {
				if(!$checkPlan){
					$responseId  = '';
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
				}
				redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
			}
	      
	}
	
	
	
	
	
		public function create_customer_sale_old()
	{
       
		if(!empty($this->input->post(null, true))){
			$inv_array=array();
			$inv_invoice=array();
        		  if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				  if(!empty($this->input->post(null, true)))
	            	{       
		     $customerID=$this->czsecurity->xssCleanPostInput('customerID');
		    $resellerID =	$this->resellerID;
          
                $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			 
			  	if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
		    	if(!empty($gt_result))
		    	{
                              
                $user      = $gt_result['gatewayUsername'];
                $password   = $gt_result['gatewayPassword'];
					
                $invoiceIDs=array();
                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
                {
                $invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                }
				
				$con_cust = array('Customer_ListID'=>$this->czsecurity->xssCleanPostInput('customerID'),'merchantID'=>$merchantID) ;
				
				$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
				
				
			  
			    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
			   
                if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    
                   
                     $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1   = substr($exyear,2);
                    $expry    = $expmonth.$exyear;  
                
				 }else {
					  
						   
                    $card_data= $this->card_model->get_single_card_data($cardID);
                   $cvv     = $card_data['CardCVV'];
                
                    $expmonth =  $card_data['cardMonth'];
                    
                    $exyear   = $card_data['cardYear'];
                    $exyear1   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                    $expry    = $expmonth.$exyear;  
                  
					}	
				
				
					$invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						$transaction->key=$user;
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Sales for Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$type=$transaction->command="sale";	
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
                        
							$transaction->billcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->billfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->billlname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->billstreet = $this->czsecurity->xssCleanPostInput('baddress1');
							$transaction->billstreet2 = $this->czsecurity->xssCleanPostInput('baddress2');
							$transaction->billcountry = $this->czsecurity->xssCleanPostInput('bcountry');
							$transaction->billcity = $this->czsecurity->xssCleanPostInput('bcity');
							$transaction->billstate = $this->czsecurity->xssCleanPostInput('bstate');
							$transaction->billzip = $this->czsecurity->xssCleanPostInput('bzipcode');
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->shipcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->shipfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->shiplname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->shipstreet = $this->czsecurity->xssCleanPostInput('address1');
							$transaction->shipstreet2 = $this->czsecurity->xssCleanPostInput('address2');
							$transaction->shipcountry = $this->czsecurity->xssCleanPostInput('country');
							$transaction->shipcity = $this->czsecurity->xssCleanPostInput('city');
							$transaction->shipstate = $this->czsecurity->xssCleanPostInput('state');
							$transaction->shipzip = $this->czsecurity->xssCleanPostInput('zipcode');
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('phone');
							$amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $transaction->email = $this->czsecurity->xssCleanPostInput('email');
							
							$transaction->amount = $amount;
							$transaction->Process();
			
	
				
			
              	if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                       
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					 
					 $condition1 = array('resellerID'=>$resellerID);
                
                 	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
                 	 
        			 $domain1 = $sub1['merchantPortalURL'];
        			 
        			 $URL1 = $domain1.'FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY',$subdomain);
                                define('OAUTH_CONSUMER_SECRET',$key);
                                define('OAUTH_CALLBACK',$URL1);
				
					  
                   
                    if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    {	           
                   $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $card_type      =$this->general_model->getType($card_no);
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                    
                    $card_data = array('cardMonth'   =>$expmonth,
                    'cardYear'	     =>$exyear,
                    'CardType'     =>$card_type,
                    'companyID'    =>$companyID,
                    'merchantID'   => $merchantID,
                    'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
                    'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
                    'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
                    'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
                    'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
                    'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
                    'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
                    'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
                    'CustomerCard' =>$card_no,
                    'CardCVV'      =>$cvv, 
                    'updatedAt'    => date("Y-m-d H:i:s") 
                    );
                    
                    
                    
                    $card=	$this->card_model->process_card($card_data) ;  
                    
                    
                    }  
              
				
                 
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
							   $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
							   $this->session->set_flashdata('success','Transaction Successful'); 
                  
					}						   
					   
					
				 
				  
					
				 /* This block is created for saving Card info in encrypted form  */
				 
				 
				    else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$result['responsetext'].'</strong>.</div>'); 
				 }
				 
				       $transactiondata= array();
				       $transactiondata['transactionID']       = $trID;
					   $transactiondata['transactionStatus']    =$msg;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['transactionCode'];  
					   $transactiondata['transactionModified']    = date('Y-m-d H:i:s');
						$transactiondata['transactionType']    = $type;	
						$transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $this->czsecurity->xssCleanPostInput('totalamount');
					   $transactiondata['merchantID']   = $merchantID;
						   if(!empty($invoiceIDs) && !empty($inv_array))
				        {
					      $transactiondata['invoiceID']            = implode(',',$inv_invoice);
					      $transactiondata['invoiceRefID']         = json_encode($inv_array);
					      if(!empty($qblist))
					      $transactiondata['qbListTxnID']          = implode(',',$qblist); 
				        }   
    				     $transactiondata['resellerID']   = $this->resellerID;
						 $transactiondata['gateway']   = "UsaePay";
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 	 
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}		
				
				        redirect('FreshBooks_controllers/Transactions/create_customer_sale','refresh');
        
		}
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }    
		}		
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				$condition				= array('merchantID'=>$user_id );
			     $gateway        		= $this->general_model->get_gateway_data($user_id);
			     $data['gateways']      = $gateway['gateway'];
			     $data['gateway_url']   = $gateway['url'];
			     $data['stp_user']      = $gateway['stripeUser'];
				$compdata				= $this->fb_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	public function create_customer_sale()
	{ 
	    
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      $user_id = $this->merchantID;
		
		$checkPlan = check_free_plan_transactions();
      	if($checkPlan && !empty($this->input->post(null, true)))
		{
			 
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
             
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
          
               
   	if ($this->form_validation->run() == true)
		    {
	   			$custom_data_fields = [];
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }
        			      
			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        		if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
        			  
        			if(!empty($gt_result) )
        			{
        			        $user   = $gt_result['gatewayUsername'];
            				$password   = $gt_result['gatewayPassword'];
                		    $invoiceIDs=array();
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				
		             $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        			
					 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
					 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
        			 $companyID  = $comp_data['companyID'];
        			 $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        			 $cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						$cardType  = $this->general_model->getType($card_no); 
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =$this->czsecurity->xssCleanPostInput('cvv'); 
        			  }
        			  else 
        			  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						 
        					
        			  }
        						$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    	                        
								
								$invNo  =mt_rand(1000000,2000000); 
								$transaction = new umTransaction;
								$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
								$transaction->key=$user;
								$transaction->pin=$password;
								$transaction->usesandbox=$this->config->item('Sandbox');
								$transaction->invoice=$invNo;   		// invoice number.  must be unique.
								$transaction->description="Sales for Invoice Payment";	// description of charge
								$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
								$type=$transaction->command="sale";	
								$transaction->card = $card_no;
								$expyear   = substr($exyear,2);
								if(strlen($expmonth)==1){
									$expmonth = '0'.$expmonth;
								}
								$expry    = $expmonth.$expyear;  
								$transaction->exp = $expry;
								if($cvv!="")
								$transaction->cvv2 = $cvv;
                        
							$transaction->billcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->billfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->billlname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->billstreet = $this->czsecurity->xssCleanPostInput('baddress1');
							$transaction->billstreet2 = $this->czsecurity->xssCleanPostInput('baddress2');
							$transaction->billcountry = $this->czsecurity->xssCleanPostInput('bcountry');
							$transaction->billcity = $this->czsecurity->xssCleanPostInput('bcity');
							$transaction->billstate = $this->czsecurity->xssCleanPostInput('bstate');
							$transaction->billzip = $this->czsecurity->xssCleanPostInput('bzipcode');
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->shipcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->shipfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->shiplname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->shipstreet = $this->czsecurity->xssCleanPostInput('address1');
							$transaction->shipstreet2 = $this->czsecurity->xssCleanPostInput('address2');
							$transaction->shipcountry = $this->czsecurity->xssCleanPostInput('country');
							$transaction->shipcity = $this->czsecurity->xssCleanPostInput('city');
							$transaction->shipstate = $this->czsecurity->xssCleanPostInput('state');
							$transaction->shipzip = $this->czsecurity->xssCleanPostInput('zipcode');
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('phone');
							$amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $transaction->email = $this->czsecurity->xssCleanPostInput('email');
							
							$transaction->amount = $amount;

							if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
		            			$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
								$transaction->orderid = $new_invoice_number;
							}

							if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
								$transaction->ponum = $this->czsecurity->xssCleanPostInput('po_number');
							}


							$transaction->Process();
			
				
						$pinv_id='';
                    	if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                        {
                       
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
							 
								 include APPPATH .'libraries/Freshbooks_data.php';
								$fb_data = new Freshbooks_data($this->merchantID,$this->resellerID);
								 $invoiceIDs=array();      
								 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
								 {
									$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
								 }
				     
								 $refNumber =array();
					 
							   if(!empty($invoiceIDs))
							   {
								   
								  foreach($invoiceIDs as $inID)
								  {
									
									$in_data = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining','refNumber','AppliedAmount'),array('merchantID'=>$this->merchantID,'invoiceID'=>$inID));
									$refNumber[] = 	$in_data['refNumber'];								
									$amount_data  = $amount;								
									 $pinv_id =''; 
									$invoices=  $fb_data->create_invoice_payment($inID, $amount_data);
									 if ($invoices['status'] != 'ok')
									 {
											  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
									  }
									   else 
									   {
										 $pinv_id =  $invoices['payment_id'];
										
										  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
										}
									
									$id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount_data,$this->merchantID,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);  
								  }
							   }  
							   else
							   {
								     
											$crtxnID = '';
											$inID    = '';
											$id      = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$this->merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields); 
										   
								}		   
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								  if(!empty($refNumber))
								  $ref_number = implode(',',$refNumber); 
								  else
								  $ref_number = '';
								  $tr_date   =date('Y-m-d H:i:s');
									$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];		
								if($chh_mail =='1')
								 {
								   
								    
								   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
								 } 
									 
								 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
								 {
											$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
											$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
											$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
										   $cardType = $this->general_model->getType($card_no);
									 
												$card_data = array('cardMonth'   =>$expmonth,
														   'cardYear'	 =>$exyear, 
														  'CardType'     =>$cardType,
														  'CustomerCard' =>$card_no,
														  'CardCVV'      =>$cvv, 
														 'customerListID' =>$customerID, 
														 'companyID'     =>$companyID,
														  'merchantID'   => $this->merchantID,
														
														 'createdAt' 	=> date("Y-m-d H:i:s"),
														 'Billing_Addr1'	 =>$address1,
														 'Billing_Addr2'	 =>$address2,	 
															  'Billing_City'	 =>$city,
															  'Billing_State'	 =>$state,
															  'Billing_Country'	 =>$country,
															  'Billing_Contact'	 =>$phone,
															  'Billing_Zipcode'	 =>$zipcode,
														 );
												
											$id1 =    $this->card_model->process_card($card_data);	
									 }
				
								
							 $this->session->set_flashdata('success','Transaction Successful'); 
            			
							 } 
							 else
							{
											  $crtxnID ='';
											  
											 $msg = $transaction->result;
											 $trID = $transaction->refnum;
											 
											 $result =array('transactionCode'=>'300', 'status'=>$msg, 'transactionId'=> $trID );
											   $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
												$id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
											 
							}
                         
                         
                         
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
		} 
		$invoice_IDs = array();
		if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
		}
	
		$receipt_data = array(
			'transaction_id' => isset($transaction) ? $transaction->refnum : '',
			'IP_address' => getClientIpAddr(),
			'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
			'proccess_btn_text' => 'Process New Sale',
			'sub_header' => 'Sale',
			'checkPlan'  => $checkPlan
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		$this->session->set_userdata("invoice_IDs",$invoice_IDs);
		
		
		redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');



                   
			  $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				  
				$condition				= array('merchantID'=>$user_id );
			     $gateway        		= $this->general_model->get_gateway_data($user_id);
			     $data['gateways']      = $gateway['gateway'];
			     $data['gateway_url']   = $gateway['url'];
			     $data['stp_user']      = $gateway['stripeUser'];
   
				$compdata				= $this->fb_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
				$this->load->view('template/template_start', $data);
					
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
	    
	 
    	}
	

	
	/******************* usaepay refund function start ********************/
	
   public function create_customer_refund()
   {
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
		        if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
				$tID       = $this->czsecurity->xssCleanPostInput('usaepayID');
				$con      = array('transactionID'=>$tID);
				$paydata   = $this->general_model->get_row_data('customer_transaction',$con);
				$gatlistval  = $paydata['gatewayID'];
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
				$paypassword  =  $gt_result['gatewayPassword'];
				$grant_type    = "password";
		
				 $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$payusername; 		// Your Source Key
                 $transaction->pin= $paypassword;		// Source Key Pin
                 $transaction->usesandbox=true;		// Sandbox true/false
                 $transaction->testmode=0;    // Change this to 0 for the transaction to process
                 
                 $transaction->command="refund";    // refund command to refund transaction.
                  
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.

			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
			 
				 if($transaction->Process()){
				    $transactionCode ='200';
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"2",'transactionModified'=>date('Y-m-d H:i:s') );
					
					  $this->qbo_customer_model->update_refund($trID, 'USAEPAY');	
					$this->session->set_flashdata('success','Success'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				      $transactiondata= array();
				      $transactiondata['transactionID']       = $transaction->refnum;
				       $transactiondata['transactionStatus']    = $transaction->authcode;
					  $transactiondata['transactionDate']    = date('Y-m-d h:i:s');  
					  $transactiondata['transactionType']    = 'usaepay_refund';
					  $transactiondata['transactionGateway']= $gt_result['gatewayType'];
					  $transactiondata['gatewayID']        = $gatlistval;
					  $transactiondata['transactionCode']   = $transactionCode;
					  $transactiondata['customerListID']     = $customerID;
					  $transactiondata['transactionAmount']  = $amount;
					  $transactiondata['merchantID']  = $merchantID;
					  $transactiondata['resellerID']   = $this->session->userdata('logged_in')['resellerID'];
					  $transactiondata['gateway']   = "USAePay";  
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}          
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
					   $invoice_IDs = array();
					  
					   $receipt_data = array(
						   'proccess_url' => 'FreshBooks_controllers/Transactions/payment_refund',
						   'proccess_btn_text' => 'Process New Refund',
						   'sub_header' => 'Refund',
					   );
					   
					   $this->session->set_userdata("receipt_data",$receipt_data);
					   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					   
					   if($paydata['invoiceTxnID'] == ''){
					   $paydata['invoiceTxnID'] ='null';
					   }
					   if($paydata['customerListID'] == ''){
						   $paydata['customerListID'] ='null';
					   }
					   if($transaction->refnum == ''){
						$transaction->refnum = 'null';
					   }
					   redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$transaction->refnum,  'refresh');
				
					
					
		}
	}
	
	
	/********************** end refunc function *******************/
	
	
	
	
	
	
/************************* auth function ***********************/	
	
	
	
	public function create_customer_auth()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	  
	     	$inv_array=array();
			$inv_invoice=array();
        		  if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}
          
           	if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
        		     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			  
        			if(!empty($gt_result) )
        			{
        			     $user      = $gt_result['gatewayUsername'];
        				 $password   = $gt_result['gatewayPassword'];
        			
        			   
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				$comp_data  = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID'), array('merchantID'=>$merchantID,'Customer_ListID' => $customerID));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        
			   
                if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    	
                   
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1   = substr($exyear,2);
                    $expry    = $expmonth.$exyear;  
                  
				 }else {
					  
						   
                    $card_data= $this->card_model->get_single_card_data($cardID);
                    
                    $expmonth =  $card_data['cardMonth'];
                    
                    $exyear   = $card_data['cardYear'];
                    $exyear1   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                    $expry    = $expmonth.$exyear;  
                  
					}	
				
						$invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$user;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Auth of Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="authonly";	
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
							$transaction->billcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->billfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->billlname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->billstreet = $this->czsecurity->xssCleanPostInput('baddress1');
							$transaction->billstreet2 = $this->czsecurity->xssCleanPostInput('baddress2');
							$transaction->billcountry = $this->czsecurity->xssCleanPostInput('bcountry');
							$transaction->billcity = $this->czsecurity->xssCleanPostInput('bcity');
							$transaction->billstate = $this->czsecurity->xssCleanPostInput('bstate');
							$transaction->billzip = $this->czsecurity->xssCleanPostInput('bzipcode');
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->shipcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->shipfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->shiplname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->shipstreet = $this->czsecurity->xssCleanPostInput('address1');
							$transaction->shipstreet2 = $this->czsecurity->xssCleanPostInput('address2');
							$transaction->shipcountry = $this->czsecurity->xssCleanPostInput('country');
							$transaction->shipcity = $this->czsecurity->xssCleanPostInput('city');
							$transaction->shipstate = $this->czsecurity->xssCleanPostInput('state');
							$transaction->shipzip = $this->czsecurity->xssCleanPostInput('zipcode');
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('phone');
							$amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $transaction->email = $this->czsecurity->xssCleanPostInput('email');
							
							$transaction->amount = $amount;
							$transaction->Process();
        				   
            	          	$crtxnID='';
            				
            			  	 	     
					    if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                        {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                        $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                        /* This block is created for saving Card info in encrypted form  */
                        
                        $condition1 = array('resellerID'=>$resellerID);
                        
                        $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
                        
                        $domain1 = $sub1['merchantPortalURL'];
                        
                        $URL1 = $domain1.'FreshBooks_controllers/Freshbooks_integration/auth';
                        
                        $val = array(
                        'merchantID' => $merchantID,
                        );
                        $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
                        
                        $subdomain     = $Freshbooks_data['sub_domain'];
                        $key           = $Freshbooks_data['secretKey'];
                        $accessToken   = $Freshbooks_data['accessToken'];
                        $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                        
                        define('OAUTH_CONSUMER_KEY',$subdomain);
                        define('OAUTH_CONSUMER_SECRET',$key);
                        define('OAUTH_CALLBACK',$URL1);
            			      
                    if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    {
                   $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $card_type      =$this->general_model->getType($card_no);
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                    
                    $card_data = array('cardMonth'   =>$expmonth,
                    'cardYear'	     =>$exyear,
                    'CardType'     =>$card_type,
                    'companyID'    =>$companyID,
                    'merchantID'   => $merchantID,
                    'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
                    'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
                    'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
                    'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
                    'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
                    'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
                    'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
                    'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
                    'CustomerCard' =>$card_no,
                    'CardCVV'      =>$cvv, 
                    'updatedAt'    => date("Y-m-d H:i:s") 
                    );
                    
                    
                    
                    $card=	$this->card_model->process_card($card_data) ; 
                     
                    
                    }  
                    
                      if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
                    
                    
                    
            				    $this->session->set_flashdata('success','Transaction Successful'); 
            			
            				 } 
            				 else
            				 {
                                    	     
				
                        
                                     $msg = $transaction->result;
                                     $trID = $transaction->refnum;
                                     
                                     $res =array('transactionCode'=>'300', 'status'=>$msg, 'transactionId'=> $trID );
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                      
                                     
                              }
                         
                         
                           $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                           
                           
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                
						$invoice_IDs = array();
						
						$receipt_data = array(
							'transaction_id' => $transaction->refnum,
							'IP_address' => getClientIpAddr(),
							'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
							'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
							'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
							'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
							'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
							'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
							'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
							'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
							'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
							'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
							'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
							'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
							'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
							'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
							'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
							'Contact' => $this->czsecurity->xssCleanPostInput('email'),
							'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_auth',
							'proccess_btn_text' => 'Process New Transaction',
							'sub_header' => 'Authorize',
						);
						
						$this->session->set_userdata("receipt_data",$receipt_data);
						$this->session->set_userdata("invoice_IDs",$invoice_IDs);
						
						
						redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
	    
	    
    	}
	
				
	
    
    /****************  usaepay auth end ******************/
	
	/******************** usaepay customer capture function start***************************/
	
	
	
	public function create_customer_capture_old()
	{
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('usatxnID', 'Transaction ID', 'required|xss_clean');
			
               
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('usatxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                 
                 $transaction->command="capture";   // void command cancels a pending transaction.
                 //$transaction->command="void:release";    // void:release speeds up the process of releasing customer's funds.   
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
						if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
					$this->session->set_flashdata('success','Successfully Captured'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				 
				   $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);
				 
				
					redirect('QBO_controllers/Transactions/payment_capture','refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
			
	}
	
		public function create_customer_capture()
	{
	    
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	    
	if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
			
			
			
			
			$this->form_validation->set_rules('usatxnID', 'Transaction ID', 'required|xss_clean');
			
              
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('usatxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                 
                 $transaction->command="capture";   // void command cancels a pending transaction.
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
					$customerID = $paydata['customerListID'];
					$tr_date   =date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 
						if($chh_mail =='1')
                            {
                                
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
							}
					$this->session->set_flashdata('success','Successfully Captured'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				 
				   $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);
				 
				   $invoice_IDs = array();
				   
			   
				   $receipt_data = array(
					   'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
					   'proccess_btn_text' => 'Process New Transaction',
					   'sub_header' => 'Capture',
				   );
				   
				   $this->session->set_userdata("receipt_data",$receipt_data);
				   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				   if($paydata['invoiceTxnID'] == ''){
					   $paydata['invoiceTxnID'] ='null';
					   }
					   if($paydata['customerListID'] == ''){
						   $paydata['customerListID'] ='null';
					   }
					   if($transaction->refnum == ''){
						$transaction->refnum = 'null';
					   }
				   
				   redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$transaction->refnum,  'refresh');
					
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
			
	}
	
	
	
	/***************************** usaepay customer capture end***********************************/
	
	
	
	/**************************** uasepay  void Payment **************************/
		public function create_customer_void()
	{
			if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('usavoidID', 'Transaction ID', 'required|xss_clean');
			
               
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('usavoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			
			   
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                 
                 $transaction->command="void";    // void command cancels a pending transaction.
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			 
		     	  $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
						if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
                            
					$this->session->set_flashdata('success','Successfully Voided'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				 
				   $id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);
				 
			
				
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Voided";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
				redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
			
	}
	
	
	
	
	
	public function create_customer_void_old()
	{
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('usavoidID', 'Transaction ID', 'required|xss_clean');
			
               
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('usavoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			
			   
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                 
                 $transaction->command="void";    // void command cancels a pending transaction.
                 //$transaction->command="void:release";    // void:release speeds up the process of releasing customer's funds.   
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			 
		     	  $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
						if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
                            
					$this->session->set_flashdata('success','Successfully Voided'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				 
				   $id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);
				 
			
				
					redirect('QBO_controllers/Transactions/payment_capture','refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Voided";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
			
	}
	
	
	/**************************** usaepay void payment function end*********************/
	
	
     
	public function pay_multi_invoice()
	{
	 if($this->session->userdata('logged_in')){
			
		
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
    
       	$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
        
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                            
             $cusproID=''; 
             $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID'); 
        	$checkPlan = check_free_plan_transactions();
               
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
    
            $cardID_upd ='';
            $invoices           = $this->czsecurity->xssCleanPostInput('multi_inv');
            $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
            $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
 
            $customerID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
          

            $error='';

               $invoiceIDs =implode(',', $invoices);
            
			   $inv_data   =    $this->qbo_customer_model->get_due_invoice_data($invoiceIDs,$user_id);
			   
			    if(!empty( $invoices) && !empty($inv_data)) 
			    {
                    $cusproID      =   $customerID  = $inv_data['CustomerListID']; 
            $comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' =>$customerID , 'merchantID'=>$user_id));
            $companyID  = $comp_data['companyID'];
     
             $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   
			if(!empty($gt_result))
        	{
        	                 $username   = $gt_result['gatewayUsername'];
                            $password   = $gt_result['gatewayPassword'];
                            $Customer_ListID = $customerID;
                          $amount  = $this->czsecurity->xssCleanPostInput('totalPay');
                          
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    						$address2 = $card_data['Billing_Addr2'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }                              
         
                	
				    
                    	$crtxnID='';  
                        $invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$username;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Multi Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
						
						
					
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
						
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zipcode;
							
							
					     	$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zipcode;
						
							
							$transaction->amount = $amount;
						 
							$result = $transaction->Process();
					
                     if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                       
                                     
                        	$c_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
            		    	$companyID = $c_data['companyID'];
            			
            			
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			            
                 
				           if(!empty($invoices))
				           {
				               
				              foreach($invoices as $invoiceID)
				              {
        				                $theInvoice = array();
        							  
        						       
        							   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        							 // $invse = $inID;
        								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '".$invoiceID."'");
        							    $condition  = array('invoiceID'=>$invoiceID,'merchantID'=>$user_id );
        						            $in_data =$this->general_model->get_select_data('QBO_test_invoice',array('BalanceRemaining','Total_payment'),$condition);
        								     $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                             $txnID       = $invoiceID;  
            						            $ispaid   = 1;
            							
            						    	$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 0;
            							 $app_amount = $in_data['Total_payment']+$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $to_amount = $in_data['BalanceRemaining']+$in_data['Total_payment'];
            							 
        							    $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
        							 		
        								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        								{
        									$theInvoice = current($targetInvoiceArray);
        								
            								$updatedInvoice = Invoice::update($theInvoice, [
            									"sparse" => 'true'
            								]);
            							
            								$updatedResult = $dataService->Update($updatedInvoice);
            								
            								$newPaymentObj = Payment::create([
            									"TotalAmt" => $to_amount,
            									"SyncToken" => $updatedResult->SyncToken,
            									"CustomerRef" => $updatedResult->CustomerRef,
            									"Line" => [
            									 "LinkedTxn" =>[
            											"TxnId" => $updatedResult->Id,
            											"TxnType" => "Invoice",
            										],    
            									   "Amount" => $pay_amounts
            									]
            									]);
            							 
            								$savedPayment = $dataService->Add($newPaymentObj);
        								
        								
        								
            								$error = $dataService->getLastError();
            								if ($error != null)
            						   	{
            							    $err='';
            					        	$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
            									$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
            								$err.="The Response message is: " . $error->getResponseBody() . "\n";
            								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>'.	$err.'</div>');
            
            							$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody();
            							
            							$qbID=$trID;	
            								$pinv_id='';
            							}
            							else 
            							{ 
            							      $pinv_id='';
            							       $pinv_id        =  $savedPayment->Id;
            							    	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
            								$this->session->set_flashdata('success','Transaction Successful'); 
            							
            							}
            					          $qbo_log =array('qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				                           $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
            							   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$pay_amounts,$user_id,$pinv_id, $this->resellerID,$txnID, false, $this->transactionByUser);     		
            							
        						}
				              }
						 
				            }
                              
                                 	 if( $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				        {
            				 
                    				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
                    				 		
                    				        $cardType       = $this->general_model->getType($card_no);
											$friendlyname   =  $cardType.' - '.substr($card_no,-4);
                    						$card_condition = array(
                    										 'customerListID' => $customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    										
                    							$cid      =    $customerID;			
                    							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
                    						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
                    							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
                    					
                    						$crdata =	$this->card_model->chk_card_firendly_name($cid,$friendlyname);			
                					   
                					
                    					 
                    						if($crdata > 0)
                    						{
                    							
                    						  $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'    =>$cardType,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                    	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                    	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                    	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                    	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                    	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                    	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                    										 'customerListID' =>$customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                    									
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					      $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'    =>$cardType,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                    	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                    	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                    	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                    	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                    	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                    	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                    										 'customerListID' =>$customerID,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										 'createdAt' 	=> date("Y-m-d H:i:s") );
                    										 
                    							 		 
                    								
                    				            $id1 = $this->card_model->insert_card_data($card_data);	
                    						
                    						}
            				
            				          }
            				    
            				    	  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
            				 	
            						
                                 }
                                 else
                                 {
                                     
                                     $msg = $transaction->result;
                                     $trID = $transaction->refnum;
                                     
                                     $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                      }		
            						
                              $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$txnID, false, $this->transactionByUser);      
                               
            			
            				           
            		    }
                		    else        
                            {
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Payment Authentication Failed! </strong>.</div>');
                                
                            }
                     
             }
             else
             {
                $error='Invalid Invoice';
                  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
              }
            }
    		else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
			}
			
			if(!$checkPlan){
				$responseId  = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
			   
		    	if($cusproID!="")
				 {
					 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 	redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
				 }
	      
	}
	
	
	
}

