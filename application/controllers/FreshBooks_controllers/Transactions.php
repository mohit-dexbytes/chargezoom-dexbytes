<?php

/**
 * Transactions Gateway Operations
 * Sale using create_customer_sale
 * Authorize  using create_customer_auth
 * Capture using create_customer_capture 
 * Void using create_customer_void
 * Refund create_customer_refund
 * Single Invoice Payment using pay_invoice
 * Multiple Invoice Payment using multi_pay_invoice
 * getting  card data
 */

include APPPATH . 'libraries/Freshbooks_data.php';

class Transactions extends CI_Controller
{
    
    private $resellerID;
    private $merchantID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
		
		include APPPATH . 'third_party/Freshbooks.php';
		include APPPATH . 'third_party/FreshbooksNew.php';
    	include APPPATH . 'third_party/nmiDirectPost.class.php';
	
		$this->load->model('customer_model');
		$this->load->model('card_model');
	    $this->load->library('form_validation');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
        $this->load->model('Freshbook_models/fb_company_model');
		$this->db1 = $this->load->database('otherdb', true); 
  if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='3' )
		  {
			$logged_in_data = $this->session->userdata('logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		    $this->merchantID =  $logged_in_data['merchID'];
			$this->resellerID = $logged_in_data['resellerID'];
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];  
		       $merchID = $this->session->userdata('user_logged_in')['merchantID'];
		    $this->merchantID = $merchID;
		  }
		  else
		  {
			redirect('login','refresh');
		  }
		  
		  
		   $val = array(
             'merchantID' => $this->merchantID,
             );
		  	$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		  	if(empty($Freshbooks_data))
		  	{
		  	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>'); 
                
		  	  redirect('FreshBoolks_controllers/home/index','refresh');
		  	  
		  	}
            
            $this->subdomain     = $Freshbooks_data['sub_domain'];
             $key                = $Freshbooks_data['secretKey'];
            $this->accessToken   = $Freshbooks_data['accessToken'];
            $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
            $this->fb_account    = $Freshbooks_data['accountType'];
            $condition1 = array('resellerID'=>$this->resellerID);
           	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			$domain1 = $sub1['merchantPortalURL'];
			$URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';  
            
            define('OAUTH_CONSUMER_KEY', $this->subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
	}
	
	
	public function index(){
	    

	   	redirect('FreshBooks_controllers/home','refresh'); 
	}
	
	
	public function get_invoice()
	{
		
		   if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
	     
		$id='';
		if(!empty($this->czsecurity->xssCleanPostInput('id'))){
			$id = $this->czsecurity->xssCleanPostInput('id');
		}
		$invoices = $this->fb_customer_model->get_invoice_upcomming_data($id, $merchID);
		if($invoices){
		echo'<table class="mytable" width="75%">';
		echo"<tr><th>Ref Number</th><th>Total Amount</th></tr>";
		
			foreach($invoices as $inv){
				echo "<tr><td><input type='checkbox' name='inv' value='".$inv['BalanceRemaining'] ."' class='test' rel='".$inv['txnID'] ."'/>  ". $inv['refNumber'] ."</td><td>".$inv['BalanceRemaining'] ."</td>";
			}
		
		echo'</table>';
		}else{
			echo'No pending Invoices Found';
		}
	}
	
	
		
	
	
	
	
        	
        	 	 
	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");	
		
				
				$cusproID=array(); 
				$error=array();
				$cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
				$merchID    = $this->merchantID;
				$user_id    = $merchID;
				$resellerID =	$this->resellerID;
				   $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
				
				   $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				   if (!$cardID || empty($cardID)) {
				   $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				   }
		   
				  
				   $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				   if (!$gatlistval || empty($gatlistval)) {
				   $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				   }
				   $gateway = $gatlistval;
				$chh_mail  =0;	 
				$in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
				$checkPlan = check_free_plan_transactions();
					
				if($checkPlan && !empty($in_data))
				{ 
					$customerID = $in_data['Customer_ListID'];
				
					$companyID = $in_data['companyID'];
					
					
					 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
					 
					
				  if($gatlistval !="" && !empty($gt_result) )
				  {
					  $nmiuser  = $gt_result['gatewayUsername'];
					  $nmipass  = $gt_result['gatewayPassword'];
					  $nmi_data  = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
					  
					  
				  
			  
					  $comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID') , 'merchantID'=>$user_id));
					  $companyID  = $comp_data['companyID'];
					  
					  
					  $transaction = new nmiDirectPost($nmi_data);
						  
					 if( $this->czsecurity->xssCleanPostInput('acc_number')!="" ){	
						   
								  $transaction->setAccountName($this->czsecurity->xssCleanPostInput('acc_name'));
								  $transaction->setAccount($this->czsecurity->xssCleanPostInput('acc_number'));
								  $transaction->setRouting($this->czsecurity->xssCleanPostInput('route_number'));
								  
								  $sec_code ='WEB';
								  
								  $transaction->setAccountType($this->czsecurity->xssCleanPostInput('acct_holder_type'));
								  $transaction->setAccountHolderType($this->czsecurity->xssCleanPostInput('acct_type'));
								  $transaction->setSecCode($sec_code);
								  
								  $transaction->setPayment('check');
	  
					  
								#Billing Details
								$transaction->setCompany($comp_data['companyName']);
								$transaction->setFirstName($comp_data['firstName']);
								$transaction->setLastName($comp_data['lastName']);
								$transaction->setAddress1($this->czsecurity->xssCleanPostInput('address1'));
								$transaction->setAddress2($this->czsecurity->xssCleanPostInput('address2'));
								$transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
								$transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
								$transaction->setState($this->czsecurity->xssCleanPostInput('state'));
								$transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
								$transaction->setPhone($this->czsecurity->xssCleanPostInput('contact'));

								#Shipping Details
								$transaction->setShippingCompany($comp_data['companyName']);
								$transaction->setShippingFirstName($comp_data['firstName']);
								$transaction->setShippingLastName($comp_data['lastName']);
								$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
								$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
								$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
								$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
								$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
								$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
							  
							  
							  $amount = $this->czsecurity->xssCleanPostInput('inv_amount');
							  $transaction->setAmount($amount);
							  $transaction->setTax('tax');
							  $transaction->sale();
							  $result = $transaction->execute();
			  
					   if($result['response_code'] == '100'){
						$val = array(
							'merchantID' => $merchID,
						);
			
						  $ispaid = 1; $st ='paid';
						   $bamount    = $in_data['BalanceRemaining']-$amount;
						  if($bamount > 0)
						  {
						  $ispaid 	  = '0';
						  $st         ='unpaid';
						  }
						  $app_amount = $in_data['AppliedAmount']+$amount;
						  $up_data    = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount,'userStatus'=>$st,'isPaid'=>$ispaid,'TimeModified'=>date('Y-m-d H:i:s') );
							$condition=array('invoiceID'=>$invoiceID,'merchantID'=>$this->merchantID);
							
							$this->general_model->update_row_data('Freshbooks_test_invoice',$condition,$up_data);
   
					
					if(isset($this->accessToken) && isset($this->access_token_secret))
					{
						if($this->fb_account==1)    
						{
							$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
					   
						  $payment = array('invoiceid'=>$invoiceID,'amount'=>array('amount'=>$amount),'type'=>'Check','date'=>date('Y-m-d'));
						  $invoices   = $c->add_payment($payment);
						}
						else
						{
							   $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
					   
						  $payment = '<?xml version="1.0" encoding="utf-8"?>  
										<request method="payment.create">  
										  <payment>         
											<invoice_id>'.$invoiceID.'</invoice_id>               
											<amount>'.$amount.'</amount>             
											<currency_code>USD</currency_code> 
											<type>Check</type>                   
										  </payment>  
										</request>';  
						  $invoices = $c->add_payment($payment);				
							
						}
						
						
					
			
						if ($invoices['status'] != 'ok') {
			
							$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
						
							
							
							
						}
						else 
						{
							$pinv_id =  $invoices['payment_id'];
							$this->session->set_flashdata('success','Success');
						
							 
							 
								  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
					 
										   }
						}	
						$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
					  $ref_number =  $in_data['refNumber']; 
					  $tr_date   =date('Y-m-d H:i:s');
						  $toEmail = $in_data['userEmail']; $company=$in_data['companyName']; $customer = $in_data['fullName'];
					 if($chh_mail =='1')
					 {
						
					   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
					 }	
					
					   }else{
						   
						  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
					   }
					   
							 $transactiondata= array();
							 $transactiondata['transactionID']       = $result['transactionid'];
							 $transactiondata['transactionStatus']    = $result['responsetext'];
							 $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
							  $transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
							 $transactiondata['transactionCode']     = $result['response_code'];  
							
							  $transactiondata['transactionType']    = $result['type'];	
							  $transactiondata['gatewayID']          = $gatlistval;
							 $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
							 $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
							 $transactiondata['transactionAmount']   = $this->czsecurity->xssCleanPostInput('totalamount');
							 $transactiondata['merchantID']   = $merchID;
							 $transactiondata['resellerID']   = $this->resellerID;
							  $transactiondata['gateway']   = "NMI ECheck";  
							}
						}  
				
				if (!empty($cardID) && !empty($gateway)) {
					$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
		  
	
					 $nmiuser   = $gt_result['gatewayUsername'];
					$nmipass   = $gt_result['gatewayPassword'];
					 $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
					 if($cardID!="new1")
					 {
						$card_data=   $this->card_model->get_single_card_data($cardID);
					   
						$card_no  = $card_data['CardNo'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						$cvv      = $card_data['CardCVV'];
						$cardType = $card_data['cardType'];
						$address1 = $card_data['Billing_Addr1'];
						$city     =  $card_data['Billing_City'];
						$zipcode  = $card_data['Billing_Zipcode'];
						$state    = $card_data['Billing_State'];
						$country  = $card_data['Billing_Country'];
					
					 }
					 else
					 {
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
						$cardType = $this->general_model->getType($card_no);
						$cvv ='';
						if($this->czsecurity->xssCleanPostInput('cvv')!="")
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
							   $address1 =  $this->czsecurity->xssCleanPostInput('address1');
							   $address2 = $this->czsecurity->xssCleanPostInput('address2');
								$city    =$this->czsecurity->xssCleanPostInput('city');
								$country =$this->czsecurity->xssCleanPostInput('country');
								$phone   =  $this->czsecurity->xssCleanPostInput('phone');
								$state   =  $this->czsecurity->xssCleanPostInput('state');
								$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
						 
					 }    
						
					if( $in_data['BalanceRemaining'] > 0)
					{
							$cr_amount      = 0;
						  
						 
							$amount         =	 $this->czsecurity->xssCleanPostInput('inv_amount'); 
							   $transaction1 = new nmiDirectPost($nmi_data); 
							$transaction1->setCcNumber($card_no);
							$expmonth =  $expmonth;
							$exyear   = $exyear;
							$exyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							$transaction1->setCcExp($expry);
							if($cvv!="")
							$transaction1->setCvv($cvv);
							
							// add level III data
							$level_request_data = [
								'transaction' => $transaction1,
								'card_no' => $card_no,
								'merchID' => $user_id,
								'amount' => $amount,
								'invoice_id' => $invoiceID,
								'gateway' => 1
							];
							$transaction1 = addlevelThreeDataInTransaction($level_request_data);

							#Billing Details
							$transaction1->setCompany($in_data['companyName']);
							$transaction1->setFirstName($in_data['firstName']);
							$transaction1->setLastName($in_data['lastName']);
							$transaction1->setAddress1($address1);
							$transaction1->setCountry($country);
							$transaction1->setCity($city);
							$transaction1->setState($state);
							$transaction1->setZip($zipcode);
							$transaction1->setPhone($in_data['phoneNumber']);

							#Shipping Details
							$transaction1->setShippingCompany($in_data['companyName']);
							$transaction1->setShippingFirstName($in_data['firstName']);
							$transaction1->setShippingLastName($in_data['lastName']);
							$transaction1->setShippingAddress1($address1);
							$transaction1->setShippingCountry($country);
							$transaction1->setShippingCity($city);
							$transaction1->setShippingState($state);
							$transaction1->setShippingZip($zipcode);

							$transaction1->setAmount($amount);
						
							$transaction1->sale();
							$result = $transaction1->execute();

						 $pinv_id='';
						   if( $result['response_code']=="100")
						   {
					 
			
								$val = array(
									'merchantID' => $merchID,
								);
					
								  $ispaid = 1; $st ='paid';
								   $bamount    = $in_data['BalanceRemaining']-$amount;
								  if($bamount > 0)
								  {
								  $ispaid 	  = '0';
								  $st         ='unpaid';
								  }
								  $app_amount = $in_data['AppliedAmount']+$amount;
								  $up_data    = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount,'userStatus'=>$st,'isPaid'=>$ispaid,'TimeModified'=>date('Y-m-d H:i:s') );
									$condition=array('invoiceID'=>$invoiceID,'merchantID'=>$this->merchantID);
									
									$this->general_model->update_row_data('Freshbooks_test_invoice',$condition,$up_data);
		   
							
							if(isset($this->accessToken) && isset($this->access_token_secret))
							{
								if($this->fb_account==1)    
								{
									$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
							   
								  $payment = array('invoiceid'=>$invoiceID,'amount'=>array('amount'=>$amount),'type'=>'Check','date'=>date('Y-m-d'));
								  $invoices   = $c->add_payment($payment);
								}
								else
								{
									   $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
							   
								  $payment = '<?xml version="1.0" encoding="utf-8"?>  
												<request method="payment.create">  
												  <payment>         
													<invoice_id>'.$invoiceID.'</invoice_id>               
													<amount>'.$amount.'</amount>             
													<currency_code>USD</currency_code> 
													<type>Check</type>                   
												  </payment>  
												</request>';  
								  $invoices = $c->add_payment($payment);				
									
								}
								
								
							
					
								if ($invoices['status'] != 'ok') {
					
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
								
									
									
									
								}
								else 
								{
									$pinv_id =  $invoices['payment_id'];
									$this->session->set_flashdata('success','Success');
								
									 
									 
										  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
							 
												   }
								}	
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number =  $in_data['refNumber']; 
							  $tr_date   =date('Y-m-d H:i:s');
								  $toEmail = $in_data['userEmail']; $company=$in_data['companyName']; $customer = $in_data['fullName'];
							 if($chh_mail =='1')
							 {
								
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
							 }	
							
								if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
							 {
									$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
									$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
									$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
										$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
								 $card_type      =$this->general_model->getType($card_no);
							 
										 $card_data = array('cardMonth'   =>$expmonth,
												   'cardYear'	 =>$exyear, 
												  'CardType'     =>$card_type,
												  'CustomerCard' =>$card_no,
												  'CardCVV'      =>$cvv, 
												 'customerListID' =>$customerID, 
												 'companyID'     =>$companyID,
												  'merchantID'   => $user_id,
												
												 'createdAt' 	=> date("Y-m-d H:i:s"),
												 'Billing_Addr1'	 =>$address1,
												 'Billing_Addr2'	 =>$address2,	 
													  'Billing_City'	 =>$city,
													  'Billing_State'	 =>$state,
													  'Billing_Country'	 =>$country,
													  'Billing_Contact'	 =>$phone,
													  'Billing_Zipcode'	 =>$zipcode,
												 );
										
									$id1 =    $this->card_model->process_card($card_data);	
							 }
		
								
				   }		
					else
				   {
				   
					   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error '.$result['responsetext'].'</strong>.</div>'); 
						 
				   }   				
									 
								 
					$id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$pinv_id, $this->resellerID,$in_data['invoiceID'], false, $this->transactionByUser);  
			   
			
				   
				   } 
				   else
				   {
				   
					   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error '.$result['responsetext'].'</strong>.</div>'); 
						 
				   }  
	 
				
			 
			}
				else
				{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
					  
				}
	  
	
	 
			}
			
			 
			 $trans_id = $result['transactionid'];
			 $invoice_IDs = array();
			 $receipt_data = array(
				 'proccess_url' => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
				 'proccess_btn_text' => 'Process New Invoice',
				 'sub_header' => 'Sale',
				 'checkPlan'  => $checkPlan
			 );
			 
			 $this->session->set_userdata("receipt_data",$receipt_data);
			 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
			 $this->session->set_userdata("in_data",$in_data);
		
			 redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		 
	
		
   

} 







   
 public function get_vault_data($customerID)
 {
	                $this->load->library('encrypt');
					
			
			  
		     	 $sql = "SELECT * from customer_card_data  where  customerListID='$customerID'  order by CardID desc limit 1  "; 
				 $query1 = $this->db1->query($sql);
				 $card_data = $query1->row_array();
			       if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
				}		
					
					return  $card;

 }
 
	public function delete_card_data()
    {
		if($this->session->userdata('logged_in') )
				    	{
			               $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') )
			        	{
			               $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
		        
		          
		if(!empty($this->czsecurity->xssCleanPostInput('delCardID')))
		{
		
		
			   $cardID = $this->czsecurity->xssCleanPostInput('delCardID'); 
			   $customer =  $this->czsecurity->xssCleanPostInput('delCustodID'); 
			   
			 
			   
			        	
		       $sts =  $this->card_model->delete_card_data(array('CardID'=>$cardID));
		       
				 if($sts){
		    $this->session->set_flashdata('success','Successfully Deleted'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>'); 
		 }
		 
	    	redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/'.$customer,'refresh');
		}
		$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Invalid Request</strong></div>'); 
			redirect('FreshBooks_controllers/home/index/','refresh');
	}	


		
	  
		 public function update_card_data()
      {
	       
	      
	                   	if($this->session->userdata('logged_in') )
				    	{
			               $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') )
			        	{
			               $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
			        	
			        	$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');	
					
					 	
						 
						 $card_data = $this->card_model->get_single_card_data($cardID);
						 $con_cust = array('Customer_ListID' => $card_data['customerListID'], 'merchantID' => $merchID);
						 $c_data = $this->general_model->get_row_data('Freshbooks_custom_customer', $con_cust);
						 $companyID  = $c_data['companyID'];
						 $customer = $card_data['customerListID'];

						if ($card_data['CardType'] != 'Echeck') 
			        	{
    						
    						$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
    						$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
    					    $decryptedcvv      = $this->czsecurity->xssCleanPostInput('edit_cvv');
							$cvv      = '';
    						$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
    						$acc_number   =$route_number = $acc_name =$secCode=$acct_type=$acct_holder_type = '';
    					
			        	}
			        	
			        	if($this->czsecurity->xssCleanPostInput('edit_acc_number')!=='')
			        	{
    						$acc_number   = $this->czsecurity->xssCleanPostInput('edit_acc_number'); 
    						$route_number = $this->czsecurity->xssCleanPostInput('edit_route_number');
    						$acc_name     = $this->czsecurity->xssCleanPostInput('edit_acc_name');
    						$secCode      = $this->czsecurity->xssCleanPostInput('edit_secCode');
    						$acct_type        = $this->czsecurity->xssCleanPostInput('edit_acct_type');
    						$acct_holder_type = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
							$expmonth = $exyear   = 	$cvv= 0;
							
							$card_no = '';
							$friendlyname = 'Checking - '. substr($acc_number, -4);
			        	}
					
			        	$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');
			        	
						$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
						
						$b_addr1 = $this->czsecurity->xssCleanPostInput('baddress1');
				        $b_addr2 = $this->czsecurity->xssCleanPostInput('baddress2');
				        $b_city = $this->czsecurity->xssCleanPostInput('bcity');
				        $b_state = $this->czsecurity->xssCleanPostInput('bstate');
				        $b_country = $this->czsecurity->xssCleanPostInput('bcountry');
				        $b_contact = $this->czsecurity->xssCleanPostInput('bcontact');
				        $b_zip = $this->czsecurity->xssCleanPostInput('bzipcode');
				        $merchantID = $merchID ;
				        $is_default = ($this->czsecurity->xssCleanPostInput('defaultMethod') != null )?$this->czsecurity->xssCleanPostInput('defaultMethod'):0;
								
			         $condition = array('CardID'=>$cardID);
		           	$insert_array =  array( 
						'cardMonth'  =>$expmonth,
						'cardYear'	 =>$exyear, 
						'CardCVV'      =>$cvv, 
										    
						'Billing_Addr1'     =>$b_addr1,
						'Billing_Addr2'     =>$b_addr2,
						'Billing_City'      =>$b_city,
						'Billing_State'     =>$b_state,
						'Billing_Zipcode'   =>$b_zip,
						'Billing_Country'   =>$b_country,
						'Billing_Contact'   =>$b_contact,
						'accountNumber'   =>$acc_number,
						'routeNumber'     =>$route_number,
						'accountName'   =>$acc_name,
						'accountType'   =>$acct_type,
						'accountHolderType'   =>$acct_holder_type,
						'secCodeEntryMethod'   =>$secCode,
						'is_default'		 => $is_default,
						'updatedAt' 	=> date("Y-m-d H:i:s")
					);
      				if($friendlyname != ''){
			        	$insert_array['customerCardfriendlyName'] = $friendlyname;
			        }
      
					$decryptedCard = '';
				      if($this->czsecurity->xssCleanPostInput('edit_card_number')!='')
				      {
						$card_no  = $this->czsecurity->xssCleanPostInput('edit_card_number'); 						 
						$decryptedCard = $card_no;
						$insert_array['CustomerCard'] =$this->card_model->encrypt($card_no);
				     	$card_type = $this->general_model->getType($card_no);
				     	
				     	 $friendlyname = $card_type.' - '.substr($card_no,-4);
				     	 $insert_array['customerCardfriendlyName']=$friendlyname;
				     	 $insert_array['CardType'] =$card_type;
				     	
				      } else {
						$decryptedCard     = $card_data['CardNo'];
					}				 
  
	         
		$isAuthorised = true;

		if ($decryptedCard != '') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['FullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}

		if($isAuthorised){
			if($is_default == 1){
				$conditionDefaultSet = array('customerListID' => $card_data['customerListID'], 'merchantID' => $merchID);
				$updateData = $this->card_model->update_customer_card_data($conditionDefaultSet,['is_default' => 0]);
			}
			$id = $this->card_model->update_card_data($condition,  $insert_array);
			if ($id) {
				$this->session->set_flashdata('success', 'Successfully Updated Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
		}
		 
	   	redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/'.$customer,'refresh');
	}


	  
	 



	  
	  public function insert_new_data()
	  {
	      
	          
	            $this->load->library('encrypt');
			  	if($this->session->userdata('logged_in') )
				    	{
			               $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') )
			        	{
			               $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
	   	    
			        	$customer = $this->czsecurity->xssCleanPostInput('customerID11');	
			        	
			         $c_data   = $this->general_model->get_row_data('Freshbooks_custom_customer',array('Customer_ListID'=>$customer));
			        	$companyID = 0;
			        	
			        	
			        	
			        	if($this->czsecurity->xssCleanPostInput('formselector')=='1')
			        	{
							$decryptedCard = $this->czsecurity->xssCleanPostInput('card_number');
							$card_no  = $this->card_model->encrypt($decryptedCard); 
    						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
    						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
							$decryptedcvv = $this->czsecurity->xssCleanPostInput('cvv');
							$cvv      = $this->card_model->encrypt($decryptedcvv);
    					   	$type = $this->general_model->getType($decryptedCard);
                             $friendlyname = $type.' - '.substr($decryptedCard, -4);
    						$acc_number   =$route_number = $acc_name =$secCode=$acct_type=$acct_holder_type = '';
    					
			        	}
			        	
			        	if($this->czsecurity->xssCleanPostInput('formselector')=='2')
			        	{
    						$acc_number   = $this->czsecurity->xssCleanPostInput('acc_number'); 
    						$route_number = $this->czsecurity->xssCleanPostInput('route_number');
    						$acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
    						$secCode      = $this->czsecurity->xssCleanPostInput('secCode');
    						$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
							$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
							$card_no  =$expmonth = $exyear   = 	$cvv = '00';
							$type ='Echeck';
							$friendlyname = 'Checking - '. substr($acc_number, -4);
			        	}
						$b_addr1 = $this->czsecurity->xssCleanPostInput('address1');
						$b_addr2 = $this->czsecurity->xssCleanPostInput('address2');
						$b_city = $this->czsecurity->xssCleanPostInput('city');
						$b_state = $this->czsecurity->xssCleanPostInput('state');
						$b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
						$b_country = $this->czsecurity->xssCleanPostInput('country');
						$b_contact = $this->czsecurity->xssCleanPostInput('contact');
					
								
					$is_default = ($this->czsecurity->xssCleanPostInput('defaultMethod') != null )?$this->czsecurity->xssCleanPostInput('defaultMethod'):0;
					
		           	$insert_array =  array( 'cardMonth'         =>$expmonth,
										    'cardYear'	        =>$exyear, 
										    'CustomerCard'      =>$card_no,
										    'CardCVV'           =>$cvv, 
										    'CardType'           =>$type,
										    'customerListID'    =>$customer, 
										    'merchantID'        =>$merchID,
										    'companyID'         => $companyID,
										    'Billing_Addr1'     =>$b_addr1,
										    'Billing_Addr2'     =>$b_addr2,
										    'Billing_City'      =>$b_city,
										    'Billing_State'     =>$b_state,
										    'Billing_Zipcode'   =>$b_zipcode,
										    'Billing_Country'   =>$b_country,
										    'Billing_Contact'   =>$b_contact,
										
										    'customerCardfriendlyName'=>$friendlyname,
										    'accountNumber'   =>$acc_number,
										    'routeNumber'     =>$route_number,
										    'accountName'   =>$acc_name,
										    'accountType'   =>$acct_type,
										    'accountHolderType'   =>$acct_holder_type,
										    'secCodeEntryMethod'   =>$secCode,
										   
										    'createdAt' 	          => date("Y-m-d H:i:s") );
										    
										    
										 
		$isAuthorised = true;
		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['fullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}												
		    	
		if($isAuthorised){
			$insert_array['CardCVV'] = '';
			if($is_default == 1){
				$condition = array('customerListID' => $customer, 'merchantID' => $merchID);
				
				$updateData = $this->card_model->update_customer_card_data($condition,['is_default' => 0]);
				
			}else{
				$checkCustomerCard = $this->card_model->get_customer_card_data($customer);
				if(count($checkCustomerCard) == 0){
					$is_default == 1;
				}
	        	
			}
			$insert_array['is_default'] = $is_default;
			$id = $this->card_model->insertBillingdata($insert_array);
			if ($id) {
				
				$this->session->set_flashdata('success', 'Successfully Inserted Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}
		}
		    
	    	redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/'.$customer,'refresh');
	  }
	  
	 
	
	

	public function create_customer_sale_old()
	{

		if(!empty($this->input->post(null, true))){
			$inv_array=array();
			$inv_invoice=array();
        		  if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				  if(!empty($this->input->post(null, true)))
	            	{       
		     $customerID=$this->czsecurity->xssCleanPostInput('customerID');
		    $resellerID =	$this->resellerID;
           
                $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
			  	if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
		    	if(!empty($gt_result))
		    	{
			    $nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  = $gt_result['gatewayPassword'];
    		    $nmi_data  = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";
				
                $invoiceIDs=array();
                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
                {
                $invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                }
				
				$con_cust = array('Customer_ListID'=>$this->czsecurity->xssCleanPostInput('customerID'),'merchantID'=>$merchantID) ;
				
				$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
				
				
			    $transaction = new nmiDirectPost($nmi_data);
			    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
			  
                if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
                    $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                    
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1   = substr($exyear,2);
                    $expry    = $expmonth.$exyear;  
                    $transaction->setCcExp($expry);
                    if($this->czsecurity->xssCleanPostInput('cvv')!="")
                    {
                     $transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));
                    }
				 }else {
					  
						   
                    $card_data= $this->card_model->get_single_card_data($cardID);
                    
                    $transaction->setCcNumber($card_data['CardNo']);
                    $expmonth =  $card_data['cardMonth'];
                    
                    $exyear   = $card_data['cardYear'];
                    $exyear1   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                    $expry    = $expmonth.$exyear;  
                    $transaction->setCcExp($expry);
                    if($card_data['CardCVV']!="")
                        $transaction->setCvv($card_data['CardCVV']);

					}	
                        $transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
						$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
						$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
						$transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
						$transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
						$transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
						$transaction->setState($this->czsecurity->xssCleanPostInput('state'));
						$transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
						$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));
					
						$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
						$amount = $this->czsecurity->xssCleanPostInput('totalamount');
						$transaction->setAmount($amount);
						$transaction->sale();
						$result = $transaction->execute();
			           
				     	 
				 if($result['response_code'] == '100'){
					  $condition1 = array('resellerID'=>$resellerID);
                	
                 	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                
					 
					
					  
                    if(isset($accessToken) && isset($access_token_secret))
                    {
						
                          $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
    
                           $qblist=array();
				           if(!empty($invoiceIDs))
				           {
				              foreach($invoiceIDs as $inID)
				              {
								  
								  $con = array('invoiceID'=>$inID,'merchantID'=>$merchantID);
            									$res = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining','invoiceID'), $con);
            									$amount_data = $res['BalanceRemaining'];
								                $invID       = $res['invoiceID'];
								  
								  $payment = '<?xml version="1.0" encoding="utf-8"?>  
												<request method="payment.create">  
												  <payment>         
													<invoice_id>'.$invID.'</invoice_id>               
													<amount>'.$amount_data.'</amount>             
													<currency_code>USD</currency_code> 
													<type>Check</type>                   
												  </payment>  
												</request>';
									  $invoices = $c->add_payment($payment);
									 
						
									if ($invoices['status'] != 'ok') {
						
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
						
									
										
									}
									else {
									   $inv_array['inv_no'][]= $invID;
									    $inv_array['inv_amount'][]= $amount_data;
									    $inv_invoice[]  =   $invID;
										$pinv_id   =  $invoices['payment_id'];
										$qblist[]  = $pinv_id;
										$this->session->set_flashdata('success','Transaction Successful'); 
										
									}
								}			
						   }
						   
						   
						     else
				          {
				             $crtxnID='';
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
				          }
				          	
				          
				        
				           if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                            {	            $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                                        	 $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    				 		 $card_type      =$this->general_model->getType($card_no);
               								   $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    				       
                                         		$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                                            	
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										 	'CardType'     =>$card_type,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
										  'CustomerCard' =>$card_no,
										  'CardCVV'      =>$cvv, 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
    
                                         	
                          			$card=	$this->card_model->process_card($card_data) ;  
                          			
                          			
                                         }  
              
				
                 
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
							 
							   $this->session->set_flashdata('success','Transaction Successful'); 
                    }
					}						   
					   
					
				 
				  
					
				 /* This block is created for saving Card info in encrypted form  */
				 
				 
				    else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$result['responsetext'].'</strong>.</div>'); 
				 }
				 
				       $transactiondata= array();
				       $transactiondata['transactionID']       = $result['transactionid'];
					   $transactiondata['transactionStatus']    = $result['responsetext'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['response_code'];  
					   $transactiondata['transactionModified']    = date('Y-m-d H:i:s');
						$transactiondata['transactionType']    = $result['type'];	
						$transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $this->czsecurity->xssCleanPostInput('totalamount');
					   $transactiondata['merchantID']   = $merchantID;
                       
						   if(!empty($invoiceIDs) && !empty($inv_array))
				        {
					      $transactiondata['invoiceID']            = implode(',',$inv_invoice);
					      $transactiondata['invoiceRefID']         = json_encode($inv_array);
					      if(!empty($qblist))
					      $transactiondata['qbListTxnID']          = implode(',',$qblist); 
				        }   
    				 $transactiondata['resellerID']   = $this->resellerID;
						 $transactiondata['gateway']   = $gatewayName;
						 
						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}		
				
				        redirect('FreshBooks_controllers/Transactions/create_customer_sale','refresh');
        
		}
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }    
		}		
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}
				
				$merchant_condition = [
					'merchID' => $user_id,
				];
		
				$data['defaultGateway'] = false;
				if(!merchant_gateway_allowed($merchant_condition)){
					$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
					$data['defaultGateway'] = $defaultGateway[0];
				}

				$condition				= array('merchantID'=>$user_id );
			     $gateway        		= $this->general_model->get_gateway_data($user_id);
			     $data['gateways']      = $gateway['gateway'];
			     $data['gateway_url']   = $gateway['url'];
			     $data['stp_user']      = $gateway['stripeUser'];
				$compdata				= $this->fb_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	public function create_customer_sale()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      $user_id = $this->merchantID;
		
		$checkPlan = check_free_plan_transactions();
		      
		$fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
        $fb_data->get_freshbooks_customers();

                   
			  $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				$plantype = $this->general_model->chk_merch_plantype_status($user_id);

				$merchant_condition = [
					'merchID' => $user_id,
				];

				$data['defaultGateway'] = false;
				if(!merchant_gateway_allowed($merchant_condition)){
					$gt_result = $this->general_model->merchant_default_gateway($merchant_condition);
					$data['defaultGateway'] = $gt_result[0];
				}
				$data['merchID'] 		= $user_id;

				$condition				= array('merchantID'=>$user_id );
				 $gatewayData        		= $this->general_model->get_gateway_data($user_id);
				 
			     $data['gateways']      = $gatewayData['gateway'];
			     $data['gateway_url']   = $gatewayData['url'];
			     $data['stp_user']      = $gatewayData['stripeUser'];
   
				$compdata				= $this->fb_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				$data['plantype'] = $plantype;
				
				$this->load->view('template/template_start', $data);
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
	    
	    
    	}
	
/*****************Authorize Transaction***************/
	
	public function create_customer_auth()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		//Show a form here which collects someone's name and e-mail address
		
		$fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
        $fb_data->get_freshbooks_customers();

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		} else if($this->session->userdata('user_logged_in')){
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}	

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);

		$condition				= array('merchantID'=>$user_id );
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];
		$compdata				= $this->fb_customer_model->get_customers_data($user_id);
		
		$data['customers']		= $compdata	;
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/payment_auth', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	/*****************Refund Transaction***************/
	
	public function create_customer_refund()
	{
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
		
		if(!empty($this->input->post(null, true)))
		{
			
			     $tID     = $this->czsecurity->xssCleanPostInput('txnID');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 
			    
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword'];
    		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";
			
				$transaction = new nmiDirectPost($nmi_data);
				 $customerID = $paydata['customerListID'];
				
				 $amount     =  $paydata['transactionAmount']; 
				 $amount     = $this->czsecurity->xssCleanPostInput('ref_amount');
				 
				 
               
			    $transaction->setTransactionId($tID);  
			
				$transaction->refund($tID,$amount);
				
				$result     = $transaction->execute();
				
				   
				 if($result['response_code'] == '100')
				 {  
		          
		          
		            $val = array(
    					'merchantID' => $paydata['merchantID'],
    				);
				
    				$merchID =$paydata['merchantID'];
        		
    		
            		$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
                   $rs_data = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=>$merchID));
                	 	$redID = $rs_data['resellerID'];
                	$condition1 = array('resellerID'=>$redID);
                	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        						 $domain1 = $sub1['merchantPortalURL'];
        						 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth'; 
                    
                    $subdomain     = $Freshbooks_data['sub_domain'];
                    $key           = $Freshbooks_data['secretKey'];
                    $accessToken   = $Freshbooks_data['accessToken'];
                    $access_token_secret = $Freshbooks_data['oauth_token_secret'];
             
                    define('OAUTH_CONSUMER_KEY', $subdomain);
                    define('OAUTH_CONSUMER_SECRET', $key);
                    define('OAUTH_CALLBACK', $URL1);
			   	  	  
				 if(!empty($paydata['invoiceID']))
				{
				     $refund =$amount;
				    $ref_inv =explode(',',$paydata['invoiceID']);
				    if(count($ref_inv)>1)
				    {
				      $ref_inv_amount = json_decode($paydata['invoiceRefID']);
				      $imn_amount =  $ref_inv_amount->inv_amount; 
				      $p_ids     =   explode(',',$paydata['qbListTxnID']);
				     
				      $inv_array = array();
				      foreach($ref_inv as $k=> $r_inv)
				      {
				          $inv_data=$this->general_model->get_select_data('Freshbooks_test_invoice',array('DueDate'),array('merchantID'=>$merchID,'invoiceID'=>$r_inv));
				          
				            if(!empty($inv_data))
				          {
				           $due_date=   date('Y-m-d',strtotime($inv_data['DueDate']));
				          }else{
				               $due_date=   date('Y-m-d');  
				          }
				          
				          $re_amount = $imn_amount[$k];
				          if($refund > 0 && $imn_amount[$k] >0)
				          {
				              
				              
				            $p_refund1=0;  
				              
				            $p_id  =  $p_ids[$k];
				              
				          	$ittem =  $this->fb_customer_model->get_invoice_item_data($r_inv);
				           
				            if($refund <= $imn_amount[$k] )
				            {
				                $p_refund1 =  $refund;
				                  
				                  
				                   $p_refund =   $imn_amount[$k] -$refund;
				                 
				                  $re_amount =$imn_amount[$k] -$refund;
				                  $refund =0;
				               
				            }
				            else
				            {
				                 $p_refund =0;
				                 $p_refund1 =  $imn_amount[$k];
				                 $refund=$refund-$imn_amount[$k];
				                 
				                 $re_amount =0;
				            }
				            
        				               
                            if(isset($accessToken) && isset($access_token_secret))
                            {
                                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                           
                              $payment = '<?xml version="1.0" encoding="utf-8"?>  
                    						<request method="payment.update">  
                    						  <payment>         
                    						    
                                                    <payment_id>'.$p_id.'</payment_id>
                                                    <amount>'.$p_refund.'</amount>
                                                    <notes>Payment refund for invoice:'.$r_inv.' '.$p_refund1.' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>              
                    						  
                    						</request>';
                                  
                                  $invoices = $c->edit_payment($payment);
                                 
                             
                               
                        			if ($invoices['status'] != 'ok') 
                        			{
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        
                        			
                        				
                        			}
                        			else {
                        			        $ins_id = $p_id;
                                            $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$p_refund1,
            				  	           'creditInvoiceID'=>$r_inv,'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           );	
            				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
            				  	        $lineArray='';
            				 
                            			  foreach($ittem as $item_val)
                            			  {
                            					
                            				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
                        					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
                        					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
                        					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
                        					$lineArray .= '<type>Item</type></line>';  
                            					
                            			  }		
                            					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
                        					$lineArray .= '<description>This is used to refund Incoive payment</description>';
                        					$lineArray .= '<unit_cost>'.(-$p_refund1).'</unit_cost>';
                        					$lineArray .= '<quantity>1</quantity>';
                        					$lineArray .= '<type>Item</type></line>'; 	
            					
            					
            					
                        				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update"> 
                                     		 <invoice> 
                                     		  <invoice_id>'.$r_inv.'</invoice_id> 
                                     		    <date>'.$due_date.'</date>
                        				          <lines>'.$lineArray.'</lines>  
                        						</invoice>  
                        						</request> ';
                        		           
                                             $invoices1 = $c->add_invoices($invoice);
                        				  	          
            				  	     
                        				$this->session->set_flashdata('success','Successfully Refunded Payment'); 
                        				
                        				
                        			}
                    			
                                
                            }	
                            
                            
                            $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount;
        				           
				          }else{
				               
				                $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount; 
				           }  
				            
				          
				      }
				       $this->general_model->update_row_data('customer_transaction',$con,array('invoiceRefID'=>json_encode($inv_array)));
				     }
				     else
				     {
				  
        			$ittem =  $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);
    		
                    $p_id     = $paydata['qbListTxnID'];
                    $p_amount = $paydata['transactionAmount']-$amount; 
                    
                    if(isset($accessToken) && isset($access_token_secret))
                    {
                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                   
                      $payment = '<?xml version="1.0" encoding="utf-8"?>  
            						<request method="payment.update">  
            						  <payment>         
            						    
                                            <payment_id>'.$p_id.'</payment_id>
                                            <amount>'.$p_amount.'</amount>
                                            <notes>Payment refund for invoice:'.$paydata['invoiceID'].' '.$amount.' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>              
            						  
            						</request>';
                          
                          $invoices = $c->edit_payment($payment);
                         
                     
                       
                			if ($invoices['status'] != 'ok') 
                			{
                
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                
                			
                				
                			}
                			else {
                			        $ins_id = $p_id;
                                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$amount,
    				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
    				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
    				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
    				  	           );	
    				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
    				  	        $lineArray='';
    				 
    			  foreach($ittem as $item_val)
    			  {
    					
    				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
					$lineArray .= '<type>Item</type></line>';  
    					
    			  }		
    					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
					$lineArray .= '<description>This is used to refund Incoive payment</description>';
					$lineArray .= '<unit_cost>'.(-$amount).'</unit_cost>';
					$lineArray .= '<quantity>1</quantity>';
					$lineArray .= '<type>Item</type></line>'; 	
    					
    					
    					
    				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update"> 
                 		 <invoice> 
                 		  <invoice_id>'.$paydata['invoiceID'].'</invoice_id> 
                 		  
    				          <lines>'.$lineArray.'</lines>  
    						</invoice>  
    						</request> ';
    		           
                         $invoices1 = $c->add_invoices($invoice);
    				  	          
    				  	     
                				$this->session->set_flashdata('success','Success'); 
                				
                				
                			}
            			}			
				     }	   
					   
    		  
             }
             
             
				 
				 
				 
				 
				 
			   
                 $this->fb_customer_model->update_refund_payment($tID, 'NMI');	
					
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$result['responsetext'].'</strong>.</div>'); 
				 }
				       $transactiondata= array();
				       $transactiondata['transactionID']      = $result['transactionid'];
					   $transactiondata['transactionStatus']  = $result['responsetext'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s'); 
					    $transactiondata['transactionModified']    = date('Y-m-d H:i:s');
					   $transactiondata['transactionType']    = $result['type'];
					    $transactiondata['transactionCode']   = $result['response_code'];
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
                              $transactiondata['merchantID']  = $merchantID;
                              
                            if(!empty($paydata['invoiceID']))
        			     	{
        				      $transactiondata['invoiceID']  = $paydata['invoiceID'];
        			     	}
                        $transactiondata['resellerID']   = $this->resellerID;
						 $transactiondata['gateway']   = $gatewayName;

						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
					   $invoice_IDs = array();
					   
				   
					   $receipt_data = array(
						   'proccess_url' => 'FreshBooks_controllers/Transactions/payment_refund',
						   'proccess_btn_text' => 'Process New Refund',
						   'sub_header' => 'Refund',
					   );
					   
					   $this->session->set_userdata("receipt_data",$receipt_data);
					   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					   
					   if($paydata['invoiceTxnID'] == ''){
						   $paydata['invoiceTxnID'] ='null';
					   }
					   if($paydata['customerListID'] == ''){
						   $paydata['customerListID'] ='null';
					   }
					   if($result['transactionid'] == ''){
						   $result['transactionid'] ='null';
					   }
					   redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result['transactionid'],  'refresh');
				
				
        }
              

	
	}
	
	/*****************Capture Transaction***************/
		public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
		    if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
			
				
				 $tID     = $this->czsecurity->xssCleanPostInput('txnID');
    			 $con     = array('transactionID'=>$tID);
    			 
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			   if( $paydata['gatewayID'] > 0){ 
			       
			     $merchantID = $this->session->userdata('logged_in')['merchID']; 
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 
    		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";
			
			   	$transaction = new nmiDirectPost($nmi_data);

			 
				 
				 $customerID = $paydata['customerListID'];
				  
				 $amount  =  $paydata['transactionAmount']; 
				  
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
				  
			    $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			
				 $transaction->setTransactionId($tID);  
			   	 $transaction->setAmount($amount);  
				if($paydata['transactionType']=='auth'){
					 
			    	$transaction->capture($tID, $amount);
			    	
				}
				$result     = $transaction->execute();
	
				 if($result['response_code'] == '100'){
			
					
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 
					if($chh_mail =='1')
                            {
                                 
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
						
			        $this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
				     
					 }
					 
					   $transactiondata= array();
				       $transactiondata['transactionID']      = $result['transactionid'];
					   $transactiondata['transactionStatus']  = $result['responsetext'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s'); 
                      $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionType']    = $result['type'];
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $result['response_code'];
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					   $transactiondata['merchantID']         =  $merchantID;
					    $transactiondata['resellerID']        = $this->resellerID;
					             $transactiondata['gateway']   = $gatewayName;  
					             
						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				     
					 }	
					 $invoice_IDs = array();
					
				 
					 $receipt_data = array(
						 'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
						 'proccess_btn_text' => 'Process New Transaction',
						 'sub_header' => 'Capture',
					 );
					 
					 $this->session->set_userdata("receipt_data",$receipt_data);
					 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					 
					 if($paydata['invoiceTxnID'] == ''){
						 $paydata['invoiceTxnID'] ='null';
					 }
					 if($paydata['customerListID'] == ''){
						 $paydata['customerListID'] ='null';
					 }
					 if($result['transactionid'] == ''){
						 $result['transactionid'] ='null';
					 }
					 redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result['transactionid'],  'refresh');
					
		
        }
              
		redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');


	}
	
	
	
	
	/*****************Void Transaction***************/
	
		public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if(!empty($this->input->post(null, true))){
		    
		      	 if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
				
						
    			  $tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				  $gatlistval = $paydata['gatewayID'];
				  $merchantID = $this->session->userdata('logged_in')['merchID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			      if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
				 $gatewayName = getGatewayName($gt_result['gatewayType']);
				 $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";
			
			   	$transaction = new nmiDirectPost($nmi_data);

			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 

                                
                 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
				$transaction->setTransactionId($tID);  
			
					$transaction->void($tID);
				
				$result     = $transaction->execute();
			 
				 if($result['response_code'] == '100'){
					
			  
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                               $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
					
			        $this->session->set_flashdata('success','Transaction Successfully Cancelled.'); 
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
				
				 }
				      $transactiondata= array();
				       $transactiondata['transactionID']      = $result['transactionid'];
					   $transactiondata['transactionStatus']  = $result['responsetext'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
                       $transactiondata['transactionModified']     = date('Y-m-d H:i:s');
					   $transactiondata['transactionType']    = $result['type'];
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $result['response_code'];
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					   $transactiondata['merchantID']         = $merchantID;
					    $transactiondata['resellerID']         = $this->resellerID;
					             $transactiondata['gateway']   = $gatewayName;  
					             
						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			
				
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
		}     
			
	}
	
  
	 public function payment_capture()
	 {

	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				
		       $data['transactions']   = $this->fb_customer_model->get_transaction_data_captue($user_id );
			     $plantype = $this->general_model->chk_merch_plantype_status($user_id);
                  $data['plantype'] = $plantype;
			
				$this->load->view('template/template_start', $data);
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_capture', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


    	}	 
						
     
	    public function refund_transaction()
	    {

	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				if ($this->session->userdata('logged_in')) {
					$data['login_info'] 	= $this->session->userdata('logged_in');
					$user_id 				= $data['login_info']['merchID'];
				}
				if ($this->session->userdata('user_logged_in')) {
					$data['login_info'] 	= $this->session->userdata('user_logged_in');
					$user_id 				= $data['login_info']['merchantID'];
				}
		       $data['transactions']   = $this->fb_customer_model->get_refund_transaction_data($user_id );
		        $plantype = $this->general_model->chk_merch_plantype_status($user_id);
                 $data['plantype'] = $plantype;   
			
		       
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/page_refund', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


    	}	 
				

						
  
	 public function payment_refund()
	 {

	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			    if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				
		      
		        $data['transactions']   = $this->fb_customer_model->get_transaction_datarefund($user_id);
		         $plantype = $this->general_model->chk_merch_plantype_status($user_id);
                  $data['plantype'] = $plantype;

				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_refund', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


    	}	 
						
		
		
	
	public function payment_transaction()
	{
	
        $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
		$data['login_info'] 	= $this->session->userdata('logged_in');
		
		$user_id 				= $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$data['login_info'] 	= $this->session->userdata('user_logged_in');
		
		$user_id 				= $data['login_info']['merchantID'];
		}	
        $data['transactions']   = $this->freshbooks_model->get_transaction_history_data($user_id );
		$this->load->view('template/template_start', $data);
		
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;   
		
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/payment_transaction', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
    }
	
	 public function payment_transaction_old()
	 {

	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
		        $data['transactions']   = $this->freshbooks_model->get_transaction_data($user_id );
				$this->load->view('template/template_start', $data);
				
				$plantype = $this->general_model->chk_merch_plantype_status($user_id);
                $data['plantype'] = $plantype;   
				
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
    }	


  
	





    public function chk_friendly_name()
    {
		$res =array();
		$frname  = $this->czsecurity->xssCleanPostInput('frname');
		$condition = array('gatewayFriendlyName'=>$frname);
		$num   = $this->general_model->get_num_rows('tbl_merchant_gateway',$condition);
		if($num){
			 $res =array('gfrname'=>'Friendly name already exist', 'status'=>'false');
			
		}else{
		$res =array('status'=>'true');
		}
		echo json_encode($res);
		die;
	}	

	
	/*****************Test NMI Validity***************/
	
	public function testNMI()
	{
			
			
			$nmiuser  = $this->czsecurity->xssCleanPostInput('nmiuser');
		    $nmipass  = $this->czsecurity->xssCleanPostInput('nmipassword');
			
			
			
		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
		
			$transaction = new nmiDirectPost($nmi_data);
			
			$transaction->setOrderDescription('Some Item');
			$transaction->setAmount('1.00');
			$transaction->setTax('1.00');
			$transaction->setShipping('1.00');

			$transaction->setCcNumber('4111111111111111');
			$transaction->setCcExp('1113');
			$transaction->setCvv('999');

			$transaction->setCompany('Some company');
			$transaction->setFirstName('John');
			$transaction->setLastName('Smith');
			$transaction->setAddress1('888');
			$transaction->setCity('Dallas');
			$transaction->setState('TX');
			$transaction->setZip('77777');
			$transaction->setPhone('5555555555');
			$transaction->setEmail('test@domain.com');

			$transaction->auth();

			$result = $transaction->execute();
		
           if($result['responsetext']=="SUCCESS"){
          
			$res =array('status'=>'true');
			  echo json_encode($res);
		   }else{
			    
			   $error =array('nmiPassword'=>'User name or password not matched','status'=>'false');
			    echo json_encode($error);
			 
		   }			   
	
	}
	
    //------------------ To view the credit -----------------------//	 
	public function credit()
	{
	    
	 $data['primary_nav']  = primary_nav();
     $data['template']   = template_variable();
     	 if($this->session->userdata('logged_in')){
		$data['login_info'] 	= $this->session->userdata('logged_in');
		
		$user_id 				= $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$data['login_info'] 	= $this->session->userdata('user_logged_in');
		
		$user_id 				= $data['login_info']['merchantID'];
		}	
	 
	 $compdata	= $this->fb_customer_model->get_customers_data($user_id);
	 $data['customers']		= $compdata	;
	 $condition  = array('cr.merchantID'=>$user_id); 
	 
	 $data['credits'] = $this->fb_customer_model->get_credit_user_data($condition);
	  $plantype = $this->general_model->chk_merch_plantype_status($user_id);
      $data['plantype'] = $plantype; 
	
	 	
     $this->load->view('template/template_start', $data);
     $this->load->view('template/page_head', $data);
     $this->load->view('FreshBooks_views/page_credit', $data);
     $this->load->view('template/page_footer',$data);
     $this->load->view('template/template_end', $data);
	
	}
	
/********** Add Credit ********/

	/********** Add Credit ********/
			
  public function create_credit()
 {
     

      if($this->session->userdata('logged_in')){
		$data['login_info'] 	= $this->session->userdata('logged_in');
		
		$user_id 				= $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$data['login_info'] 	= $this->session->userdata('user_logged_in');
		
		$user_id 				= $data['login_info']['merchantID'];
		}	
      
	 if(!empty($this->input->post(null, true))){
			
			$input_data['merchantID'] = $user_id ;
			
			$input_data['creditName']  = $this->czsecurity->xssCleanPostInput('creditname');
			
	        $input_data['creditDate']  = date("Y-m-d");
			$input_data['creditAmount']  = $this->czsecurity->xssCleanPostInput('creditamount');
			
			$input_data['creditDescription']  = $this->czsecurity->xssCleanPostInput('creditdescription');
			
			  
			
		     if( $this->general_model->insert_row('tbl_credits', $input_data)){ 
						  
			   $c_data               =  $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID'=>$this->czsecurity->xssCleanPostInput('creditname')));
              
			   
			   $condition         = array('templateType'=>'6');  
			   $view_data         = $this->company_model->template_data($condition);	
			
			    $merchant_data    = $this->general_model->get_row_data('tbl_merchant_data', array('merchID'=>$user_id)); 
			   $config_data       = $this->general_model->get_row_data('tbl_config_setting', array('merchantID'=>$user_id)); 
               $currency          = "$";   			   
			   $amount            = $this->czsecurity->xssCleanPostInput('creditamount');
			   $config_email      = $merchant_data['merchantEmail'];
			   $merchant_name     = $merchant_data['companyName'];
			   $logo_url          = $merchant_data['merchantProfileURL']; 
			   $mphone            =  $merchant_data['merchantContact'];
			   $tr_date          = date('Y-m-d'); 
			   $currency          = "$";
			   $customer          = $c_data['fullName'];
			   $message = $view_data['message'];
			   $message = stripslashes(str_replace('{{ merchant_name }}',$merchant_name ,$message));
			    $message = stripslashes(str_replace('{{ logo }}',"<img src='$logo_url'>" ,$message));
			    $message = stripslashes(str_replace('{{ transaction.amount }}',($amount)?($amount):'0.00' ,$message ));
				$message = stripslashes(str_replace('{{ transaction.currency_symbol }}',$currency ,$message ));
				$message = stripslashes(str_replace('{{ customer.company }}',$customer ,$message));
			    $message = stripslashes(str_replace('{{ merchant_email }}',$config_email ,$message )); 
			     $message = stripslashes(str_replace('{{ transaction.transaction_date}}', $tr_date, $message ));
			   $message = stripslashes(str_replace('{{invoice.currency_symbol}}',$currency ,$message )); 
				
				
				
				$customerID = $this->czsecurity->xssCleanPostInput('creditname');
		       $subject = $view_data['emailSubject']; 
	          $fromEmail    = $merchant_data['merchantEmail'];
			   $toEmail     = $c_data['userEmail'];
			    $addCC      = $view_data['addCC'];
				$addBCC		= $view_data['addBCC'];
				$replyTo    = $view_data['replyTo'];
          	    $this->load->library('email');
		          $email_data    = array(     'customerID'=>$customerID,
										'merchantID'=>$merchant_data['merchID'], 
										'emailSubject'=>$subject,
										'emailfrom'=>$fromEmail,
										'emailto'=>$toEmail,
										'emailcc'=>$addCC,
										'emailbcc'=>$addBCC,
										'emailreplyto'=>$replyTo,
										'emailMessage'=>$message,
										'emailsendAt'=>date("Y-m-d H:i:s"),
										
										);
    
    

							$this->email->clear();
							$config['charset'] = 'utf-8';
							$config['wordwrap'] = TRUE;
							$config['mailtype'] = 'html';
							$this->email->initialize($config);
							$this->email->from($fromEmail, $merchant_name);					
							$this->email->to($toEmail);
							$this->email->subject($subject);
							$this->email->message($message);
			 if ($this->email->send()){
				 $this->general_model->insert_row('tbl_template_data', $email_data);
			 }
					
			 
			  $this->session->set_flashdata('success','Success'); 
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>.</div>'); 
				
				 }
					
			    redirect(base_url('FreshBooks_controllers/Transactions/credit'));
		

		     }
	
 
 }
	
	//----------- TO update the credit  --------------//
	
	public function update_credit()
	{
	    
	     if($this->czsecurity->xssCleanPostInput('creditEditID')!="" ){
				      
				        $id = $this->czsecurity->xssCleanPostInput('creditEditID');
					    $chk_condition = array('creditID'=>$id);
						
						$input_data['creditDate']  = date("Y-m-d");
						$input_data['creditAmount']  = $this->czsecurity->xssCleanPostInput('amount');
						$input_data['creditDescription']  = $this->czsecurity->xssCleanPostInput('description');
						 
	                    if($this->general_model->update_row_data('tbl_credits',$chk_condition, $input_data) ){
							 $this->session->set_flashdata('success','Success'); 
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				
				 }
			
	 
			
	       }		
	              redirect(base_url('FreshBooks_controllers/Transactions/credit'));
	 
	 
	 
	} 
	


	
	
	public function get_creditedit_id()
     {
		
        $id = $this->czsecurity->xssCleanPostInput('credit_id');
		$val = array(
		'creditID' => $id,
		);
		
		$data = $this->general_model->get_row_data('tbl_credits',$val);
        echo json_encode($data);
     }
	
	
 
	//-------------for view credit  data--------------//
	
	
	public function get_credit_id()
	
    {
		
                 $creditID          =  $this->czsecurity->xssCleanPostInput('customerID'); 
		         $condition 		= array('creditName'=> $creditID);
				 $creditdatas		= $this->general_model->get_table_data('tbl_credits', $condition);
		
          ?>
		  
		   <table class="table table-bordered table-striped table-vcenter">
            
            <tbody>
				
				<tr>
					<th class="text-right"> <strong> Credit Date</strong></th>
					
					<th class="text-right"><strong>Amount ($)</strong></th>
					<th class="text-right"><strong> Processed On</strong></th>
					<th class="text-left"><strong> Edit/Delete</strong></th>
			  </tr>	
 	<?php	
				if(!empty($creditdatas))
				{  foreach($creditdatas as $creditdata){
			?>	
		
			<tr>  
			<td class="text-right visible-lg"><?php echo date('F d, Y', strtotime($creditdata['creditDate'])); ?> </a> </td>
			<td class="text-right visible-lg"> <?php echo number_format($creditdata['creditAmount'],2); ?> </a> </td>
			<td class="text-right visible-lg">  <?php if($creditdata['creditStatus']=="0" ){ echo"Yet to Processed";}else{echo $creditdata['creditDate']; }?> </a> 
			</td>
			
			<td class="text-left visible-lg"> <a href="javascript:void(0);" class="btn btn-default" onclick="set_edit_credit('<?php echo $creditdata['creditID'];  ?>');" title="Edit"> <i class="fa fa-edit"> </i> </a>  
              
			  <a href="#fb_del_credit" onclick="fb_del_credit_id('<?php echo $creditdata['creditID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Credit" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>



			</td>
			
			
			</tr>	
			
           
			<?php     }   }  ?>
           
			</tbody>
        </table>
				
				
		<?php  die;
		
	
    }
	
	
	
	public function get_card_data(){
		$customerdata =array();
		if($this->czsecurity->xssCleanPostInput('cardID')!=""){
			 $crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_single_card_data($crID);
			if(!empty($card_data)){
			         $customerdata['status'] =  'success';	
					$customerdata['card']     = $card_data;
					echo json_encode($customerdata)	;
					die;
			}
		}
		
		
	}
	
	
	
	/**************Delete credit********************/
	
	public function delete_credit(){
	
		 $creditID = $this->czsecurity->xssCleanPostInput('fb_invoicecreditid');
		 $condition =  array('creditID'=>$creditID); 
		 $del      = $this->general_model->delete_row_data('tbl_credits', $condition);
      if($del){
	            $this->session->set_flashdata('success', 'Successfully Deleted');
	  }else{
                $this->session->set_flashdata('message', 'Error: Somthing Is Wrong...');        
	  }	  
	  
	  	redirect(base_url('FreshBooks_controllers/Transactions/credit'));
    	 
	
	}
	
	
	
 

		
	 
	public function check_vault(){
		 
 	   if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$merchantID 				= $da['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
		$merchantID 				= $da['merchantID'];
		}	

		$card=''; $card_name=''; $customerdata=array();
		$invoiceIDs = [];
		$gatewayID 		= $this->czsecurity->xssCleanPostInput('gatewayID');  
		if($this->czsecurity->xssCleanPostInput('customerID')!=""){
			
			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID'); 

			$condition     =  array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID); 
			$customerdata = $this->general_model->get_row_data('Freshbooks_custom_customer',$condition);
			
			if(!empty($customerdata)){

				$syncObejct = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
				$customerSync = $syncObejct->syncInvoice($customerID);
								
				$invoices = $this->fb_company_model->get_open_invoices($customerID, $merchantID);
				$table='';
				$new_inv = '';
				$totalInvoiceAmount = 0.00;
				if(!empty($invoices)){
					$new_inv = '<div class="form-group alignTableInvoiceList" >
				      	<div class="col-md-1 text-center"><b></b></div>
				        	<div class="col-md-3 text-left"><b>Number</b></div>
				        	<div class="col-md-3 text-right"><b>Due Date</b></div>
				        	<div class="col-md-2 text-right"><b>Amount</b></div>
				        	<div class="col-md-3 text-left"><b>Payment</b></div>
					    </div>';
					foreach($invoices as $inv){
						if($inv['status'] != 'Cancel'){
							$new_inv .= '<div class="form-group alignTableInvoiceList" >
				       
				         	<div class="col-md-1 text-center"><input checked type="checkbox" class="chk_pay check_'.$inv['refNumber'].'" id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
				        	<div class="col-md-3 text-left">' . $inv['refNumber'] . '</div>
				        	<div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
				        	<div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
					   		<div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['invoiceID'] . '  geter" data-id="multiinv' . $inv['invoiceID'] . '" data-inv="' . $inv['invoiceID'] . '" data-ref="' . $inv['refNumber'] . '" data-value="' . $inv['BalanceRemaining'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
					   		</div>';
							$inv_data[] = [
								'RefNumber' => $inv['refNumber'],
								'TxnID' => $inv['invoiceID'],
								'BalanceRemaining' => $inv['BalanceRemaining'],
							];
							$invoiceIDs[$inv['invoiceID']] =  $inv['refNumber'];
							$totalInvoiceAmount = $totalInvoiceAmount + $inv['BalanceRemaining'];
						}
					}
				}else{
					$table.='';
				}

				$customerdata['invoices'] = $new_inv;
				$customerdata['invoiceIDs'] = $invoiceIDs;
				$customerdata['totalInvoiceAmount'] = $totalInvoiceAmount;
				$customerdata['status'] =  'success';
					
				$ach_data =   $this->card_model->get_ach_info_data($customerID);
				$ACH = [];
				if(!empty($ach_data)){
					foreach($ach_data as $card){
						$ACH[] = $card['CardID'];
					}
				}
				$recentACH = end($ACH);
				$customerdata['ach_data']  = $ach_data;
				$customerdata['recent_ach_account']  = $recentACH;
				
				$conditionGW = array('gatewayID' => $gatewayID);
				$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
				$cardTypeOption = 2;
				if(isset($gateway['gatewayID'])){
					if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 1;
					}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 2;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 3;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 4;
					}else{
						$cardTypeOption = 2;
					}
					
				}
				$customerdata['cardTypeOption']  = $cardTypeOption;
				
				$card_data =   $this->card_model->getCardData($customerID,$cardTypeOption);
				$cardArr = [];
				if(!empty($card_data)){
					foreach($card_data as $card){
						$cardArr[] = $card['CardID'];
					}
				}
				$recentCard = end($cardArr);
				$customerdata['card']  = $card_data;
				$customerdata['recent_card']  = $recentCard;

				if(empty($customerdata['companyName'])){
					$customerdata['companyName'] = $customerdata['FullName'];
				}
				$customerdata['invoiceIDs'] = $invoiceIDs;
				echo json_encode($customerdata)	;
				die;
			} 	 
			 
		}else{
			$customerdata = [];
			$conditionGW = array('gatewayID' => $gatewayID);
			$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
			$cardTypeOption = 2;
			if(isset($gateway['gatewayID'])){
				if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 1;
				}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 2;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 3;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 4;
				}else{
					$cardTypeOption = 2;
				}
				
			}
			
			$customerdata['status']  = 'success';
			$customerdata['cardTypeOption']  = $cardTypeOption;
			echo json_encode($customerdata);
				die;
		}		  
	}	
	 
	 
	 
  public function get_card_edit_data()
  {
	  
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->card_model->get_single_mask_card_data($cardID);
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
  }
  
	 
	 
	 public function view_transaction(){

				
				$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
				
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
		        $transactions           = $this->fb_customer_model->get_invoice_transaction_data($invoiceID, $user_id);
				
				if(!empty($transactions) )
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-left"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'----'; ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($transaction['transactionAmount'],2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo ucfirst($transaction['transactionType']); ?></td>
<td class="text-right visible-lg"><?php if($transaction['transactionCode']=='300'){ ?> <span class="">Failed</span> <?php }else if($transaction['transactionCode']=='100'){ ?> <span class="">Success</span><?php } ?></td>
					
				</tr>
				
		<?php     }

				}else{
					echo '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
				}
              die;				

      }	

	   
	   
	   
	   
  	/*********ECheck Transactions**********/					
  
	 public function evoid_transaction()
     {

	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			    if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				
		        $data['transactions']    = $this->fb_customer_model->get_transaction_data_erefund($user_id );
		         $plantype = $this->general_model->chk_merch_plantype_status($user_id);
              $data['plantype'] = $plantype;

				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_ecapture', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


    	}	
    	
    	
     public function echeck_transaction()
     {

	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			    if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				
		        $data['transactions']    = $this->fb_customer_model->get_transaction_data_erefund($user_id );
		         $plantype = $this->general_model->chk_merch_plantype_status($user_id);
                 $data['plantype'] = $plantype;

				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_erefund', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


    	}	
	
	public function payment_erefund()
	{
		//Show a form here which collects someone's name and e-mail address
		$merchantID = $this->session->userdata('logged_in')['merchID'];
		if(!empty($this->input->post(null, true))){
			
			     $tID     = $this->czsecurity->xssCleanPostInput('txnID');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 
			    
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword'];
    		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
			  
			
				$transaction = new nmiDirectPost($nmi_data);
				 $customerID = $paydata['customerListID'];
				 
				
				 $amount     =  $paydata['transactionAmount']; 
				
                  $transaction->setPayment('check');
			    $transaction->setTransactionId($tID);  

			
			
			   
				$transaction->refund($tID,$amount);
				
				$result     = $transaction->execute();
				
				   
			
				 if($result['response_code'] == '100'){  
				     
			
				     
				 $this->customer_model->update_refund_payment($tID, 'NMI');	 
			
					
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$result['responsetext'].'</strong>.</div>'); 
				 }
				       $transactiondata= array();
				       $transactiondata['transactionID']      = $result['transactionid'];
					   $transactiondata['transactionStatus']  = $result['responsetext'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
					   $transactiondata['transactionType']    = $result['type'];
					    $transactiondata['transactionCode']   = $result['response_code'];
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					    $transactiondata['merchantID']  = $merchantID;
					     $transactiondata['resellerID']   = $this->resellerID;
					             $transactiondata['gateway']   = "NMI ECheck";  
					             
						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				
				redirect('FreshBooks_controllers/Transactions/echeck_transaction','refresh');
				
        }
              
				


	}
	public function payment_evoid()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if(!empty($this->input->post(null, true))){
						
    			  $tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				  $gatlistval = $paydata['gatewayID'];
				  $merchantID = $this->session->userdata('logged_in')['merchID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
			  
			
			   	$transaction = new nmiDirectPost($nmi_data);

			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				  $transaction->setPayment('check');
				 $transaction->setTransactionId($tID);  
			
					$transaction->void($tID);
				
				$result     = $transaction->execute();
			 
				 if($result['response_code'] == '100'){
					
			    
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
					
			        $this->session->set_flashdata('success','Transaction Successfully Cancelled.'); 
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
				
				 }
				      $transactiondata= array();
				       $transactiondata['transactionID']      = $result['transactionid'];
					   $transactiondata['transactionStatus']  = $result['responsetext'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
					   $transactiondata['transactionType']    = $result['type'];
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $result['response_code'];
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					   $transactiondata['merchantID']         = $merchantID;
					    $transactiondata['resellerID']        = $this->resellerID;
					             $transactiondata['gateway']   = "NMI ECheck";  
					            
						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			
				
					redirect('FreshBooks_controllers/Transactions/echeck_transaction','refresh');
		}     
		
		
		
		  
			
	}
	


	 
	public function create_customer_esale()
	{
		$fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
        $fb_data->get_freshbooks_customers();

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_name'] 	= "NMI ESale";
		
		if($this->session->userdata('logged_in')){
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}	

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$condition				= array('merchantID'=>$user_id );
		
		$gateway		= $this->general_model->get_gateway_data($user_id, 'echeck');
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		
		$compdata				= $this->fb_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata	;
		
	
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/payment_esale', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
/**********END****************/
	   
		 	 
public function pay_multi_invoice()
{
    
    
	                  if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	} 
			        	$user_id = $merchID;
			        	
        		    	$rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$merchID));
        			 $resellerID =	$rs_daata['resellerID'];
        			 $condition1 = array('resellerID'=>$resellerID);
                	
                 	    $sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
        			 
                    	$val = array(
        					'merchantID' => $merchID,
        				);
	                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
		
                        $subdomain     = $Freshbooks_data['sub_domain'];
                        $key           = $Freshbooks_data['secretKey'];
                        $accessToken   = $Freshbooks_data['accessToken'];
                        $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                 
                        define('OAUTH_CONSUMER_KEY', $subdomain);
                        define('OAUTH_CONSUMER_SECRET', $key);
                        define('OAUTH_CALLBACK', $URL1);
               	        	
	 	 
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
		 

         
		$checkPlan = check_free_plan_transactions();
       
				
       if($checkPlan && !empty($cardID)&& !empty($gateway))
       {  
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	$nmiuser   = $gt_result['gatewayUsername'];
    		    $nmipass   = $gt_result['gatewayPassword'];
    		     $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
    		     $gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";
    		     
			  
			  
			  
              $invoices    = $this->czsecurity->xssCleanPostInput('multi_inv');
              
             if(!empty($invoices))
             {
                 foreach($invoices as $key=> $invoiceID)
                 {
                     $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);  
                     
                     $in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
       
                     
                	if(!empty($in_data))
	          	{ 
		  
            $Customer_ListID = $in_data['CustomerListID'];
             if($cardID=='new1')
           {
			        	$cardID_upd  =$cardID;
			        		$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				        $merchantID = $merchID;
          
           
                         $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID' => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                           $this->db1->select('*')->from('customer_card_data');
                           $qq =  $this->db1->get();
          
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                         
                         }
                         else
                         {
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
		 $card_data    =   $this->card_model->get_single_card_data($cardID);
		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0)
				{
					     $cr_amount = 0;
					     $amount    =	 $in_data['BalanceRemaining']; 
					 
					         $amount  = $pay_amounts  ;
					       $amount    = $amount-$cr_amount;
					   
					      
					   
					  		$transaction1 = new nmiDirectPost($nmi_data); 
							$transaction1->setCcNumber($card_data['CardNo']);
						    $expmonth =  $card_data['cardMonth'];
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						    $expry    = $expmonth.$exyear;  
							$transaction1->setCcExp($expry);
							$transaction1->setCvv($card_data['CardCVV']);
							$transaction1->setAmount($amount);
				        	

							$level_request_data = [
								'transaction' => $transaction1,
								'card_no' => $card_data['CardNo'],
								'merchID' => $merchantID,
								'amount' => $amount,
								'invoice_id' => $invoiceID,
								'gateway' => 1
							];
							$transaction1 = addlevelThreeDataInTransaction($level_request_data);

						    $transaction1->sale();
						     $result = $transaction1->execute();
						
					
					   if( $result['response_code']=="100"){
						 

						 if($cr_amount > 0){
							  $credit_data = array('creditStatus'=>'1');
							  $this->general_model->update_row_data('tbl_credits',array('creditName'=>$in_data['CustomerListID']), $credit_data );
						 } 
				
            				$val = array(
            					'merchantID' => $merchID,
            				);
				
			                
                    
                
                    
                    if(isset($accessToken) && isset($access_token_secret))
                    {
                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);
                   
                      $payment = '<?xml version="1.0" encoding="utf-8"?>  
            						<request method="payment.create">  
            						  <payment>         
            						    <invoice_id>'.$invoiceID.'</invoice_id>               
            						    <amount>'.$amount.'</amount>             
            						    <currency_code>USD</currency_code> 
            						    <type>Check</type>                   
            						  </payment>  
            						</request>';
                          $invoices = $c->add_payment($payment);
                         
            
            			if ($invoices['status'] != 'ok') {
            
            				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
            
            				redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
            				
            			}
            			else {
            				$this->session->set_flashdata('success','Success'); 
            				
            			}
            			}			
					   } else{
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error '.$result['responsetext'].'</strong>.</div>'); 
					   }  
				 
					   $transaction['transactionID']      = $result['transactionid'];
					   $transaction['transactionStatus']  = $result['responsetext'];
					   $transaction['transactionCode']    =  $result['response_code'];
					    $transaction['transactionType']   =  ($result['type'])?$result['type']:'auto-nmi';
					   $transaction['transactionDate']    = date('Y-m-d H:i:s');  
					    $transaction['transactionModified']    = date('Y-m-d H:i:s');  
					   $transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']  = $gt_result['gatewayType'] ;	
					   $transaction['customerListID']     = $in_data['CustomerListID'];
					   $transaction['transactionAmount']  = $pay_amounts;
					    $transaction['qbListTxnID'] =   $invoices['payment_id'];
					    $transaction['merchantID']   = $merchID;
					    $transaction['invoiceID']   = $invoiceID;
                        $transaction['resellerID']   = $resellerID;
						 $transaction['gateway']   = $gatewayName;

						$transaction = alterTransactionCode($transaction);
						$CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
			      
                 }
             }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Please Select Invoices</strong>.</div>'); 
			 }
                
	
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
		  }

		if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
			
	
		    redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
		    
    
}     



	
	public function check_transaction_payment()
	{
	      if($this->session->userdata('logged_in'))
      {
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	
		
		if(!empty($this->czsecurity->xssCleanPostInput('trID')))
		{
		    
		    
		    $trID = $this->czsecurity->xssCleanPostInput('trID');
		    $av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');
		    $p_data = $this->fb_customer_model->get_transaction_details_data(array('id'=>$trID,'tr.merchantID'=>$user_id));
		   
		   if(!empty($p_data))
		   {
		         if($p_data['transactionAmount'] >= $av_amount)
		         {
		              $resdata['status']  ='success';
		          
		         }else{
		            $resdata['status']  ='error';
		           $resdata['message']  ='Your are not allowed to refund exceeded amount more than actual amount :'.$p_data['transactionAmount'];  
		         }
		   }
		   else
		   { 
		       $resdata['status']  ='error';
		       $resdata['message']  ='Invalid transactions ';
		   }
		}else
		   { 
		       $resdata['status']  ='error';
		       $resdata['message']  ='Invalid request';
		   }
	   echo json_encode($resdata);
	   die;
	    
	    
	}
	
}