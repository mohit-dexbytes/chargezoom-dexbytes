<?php

include_once APPPATH . 'libraries/Manage_payments.php';
include APPPATH .'libraries/Freshbooks_data.php';

class NMICron extends CI_Controller
{
	private $gatewayEnvironment;
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Freshbook_models/fb_company_model');
        $this->load->model('Freshbook_models/fb_invoice_model');
        $this->load->model('Freshbook_models/fb_customer_model');

     	$this->load->model('general_model');
		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', TRUE);

		$this->db1->query("SET SESSION sql_mode = ''");
		$this->db->query("SET SESSION sql_mode = ''");
	}
	
	
	public function index(){
    	echo "Page Declined !";  die;
	}
	
	
	public function send_mail_data($indata=array(), $type)
	{
					
				
								   
			   	$customerID        = $indata['Customer_ListID']; 
		      	$companyID		  = $indata['companyID']; 
			    $typeID			  = $type; 
				$invoiceID        = $indata['TxnID'];
			  
    	          
    	       $comp_data= $this->general_model->get_row_data('tbl_company',array('id'=>$companyID));
                 
    	       $condition         = array('templateType'=>$typeID, 'merchantID'=>$comp_data['merchantID']);  
			   $view_data         = $this->company_model->template_data($condition);	
			   
			   $merchant_data     = $this->general_model->get_row_data('tbl_merchant_data', array('merchID'=>$comp_data['merchantID'])); 
			   $config_data     = $this->general_model->get_row_data('tbl_config_setting', array('merchantID'=>$comp_data['merchantID'])); 
			   
               $currency          = "$";   			   
			   
			   $config_email      = $merchant_data['merchantEmail'];
			   $merchant_name     = $merchant_data['companyName'];
			   $logo_url          = $merchant_data['merchantProfileURL']; 
			   $logo_url          = $merchant_data['merchantProfileURL']; 
			   $mphone            =  $merchant_data['merchantContact'];
			   $cur_date          = date('Y-m-d'); 
			   $amount 		='';  
			   $paymethods   ='';  
			   $transaction_details = '';
			   $tr_data      ='';  
			   $ref_number   = '';  
			    $overday      = '';
				$balance      = '0.00';
				$in_link      = '';
				$duedate      = '';
				$company      ='';
				$cardno       = '';
				$expired      = '';
				$expiring     = '';
				$friendly_name = '';
				$tr_amount    ='0.00';
				$update_link  = $config_data['customerPortalURL'];
				$gateway_msg  = '';
				$companyName = '';
				
				 $condition1 = " and Customer_ListID='".$customerID."' ";
				 $condition1.= " and TxnID='".$invoiceID."' " ;
	               
				  if($typeID == '5'){
					    $data      = $this->customer_model->get_invoice_data_template($condition1);
					}
					if($typeID     == '4'){
					$data         = $this->customer_model->get_invoice_data_template($condition1);
				     $card_data  = $this->get_card_by_ID($indata['cardID']);
					if(!empty($card_data))
					{
						  $cardno          =  $card_data['CustomerCard'];
						$friendly_name     =  $card_data['customerCardfriendlyName'];
					}

			}
			        
			
			   
			
				     if(!empty($data)){
					 
						   $customer = $data['fullName'];
						  $amount    = $data['AppliedAmount'] ; 
						  
						 $balance    = $data['BalanceRemaining'] ;  
					  $paymethods    = $data['paymentType'] ;  
							
					 $duedate        = date('F d, Y', strtotime($data['DueDate'])) ;   
					$ref_number      = $data['RefNumber'] ;  
        			      $tr_date   =  date('F d, Y',strtotime($data['tr_date']));
						  $tr_data   =  $data['tr_data']; 
						  $tr_amount =  $data['tr_amount']; 
						  $companyName = $data['comapanyName'];
					}	
					
			 
			   	if ($typeID == '5' && isset($merchant_data['merchant_default_timezone'])  && !empty($merchant_data['merchant_default_timezone'])) {
		            // Convert added date in timezone 
		            $timezone = ['time' => date('Y-m-d H:i:s'), 'current_format' => 'UTC', 'new_format' => $merchant_data['merchant_default_timezone']];
		            $tr_date = getTimeBySelectedTimezone($timezone);
		            $tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
		        }
		        
			 
	    	   $subject =	stripslashes(str_ireplace('{{invoice.refnumber}}',$ref_number, $view_data['emailSubject']));
			   if( $view_data['emailSubject'] =="Welcome to { company_name }")
			   $subject = 'Welcome to '.$company;
				/* Check merchant logo is available if yes than display otherwise display default chargezoom */
			    $logo_url = CZLOGO;
			    if($config_data['ProfileImage']!=""){
		           $logo_url = LOGOURL1.$config_data['ProfileImage']; 
		        }

                $logo_url = "<img src='" . $logo_url . "' />";
                $message  = stripslashes(str_ireplace('{{logo}}', $logo_url, $message));
			   
			   $message = $view_data['message'];
			   
			   $message = stripslashes(str_ireplace('{{creditcard.type_name}}',$friendly_name ,$message));
			   $message = stripslashes(str_ireplace('{{merchant_company_name}}',$merchant_name,$message));
			   $message = stripslashes(str_ireplace('{{creditcard.mask_number}}',$cardno ,$message ));
			   $message = stripslashes(str_ireplace('{{creditcard.type_name}}',$friendly_name ,$message ));
			   $message = stripslashes(str_ireplace('{{creditcard.url_updatelink}}',$update_link ,$message ));
			   
			   $message = stripslashes(str_ireplace('{{customer.company}}',$companyName  ,$message));
			   $message = stripslashes(str_ireplace('{{transaction.currency_symbol}}',$currency ,$message ));
			   $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}',$currency ,$message ));
			    
			   $message = stripslashes(str_ireplace('{{transaction.amount}}',($tr_amount)?($tr_amount):'0.00' ,$message )); 
			   
			   $message = stripslashes(str_ireplace('{{transaction.transaction_method}}',$paymethods ,$message));
			   $message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message ));
			   $message = stripslashes(str_ireplace('{{transaction.transaction_detail}}', $tr_data, $message ));

               $message = stripslashes(str_ireplace('{% transaction.gateway_method %}', $paymethods, $message ));				
			   $message = stripslashes(str_ireplace('{{invoice.days_overdue}}', $overday, $message ));
			   $message = stripslashes(str_ireplace('{{invoice.refnumber}}',$ref_number, $message));
			   $message = stripslashes(str_ireplace('{{invoice.balance}}',$balance, $message));
			   $message = stripslashes(str_ireplace('{{invoice.due_date|date("F j, Y")}}',$duedate, $message));
			   $message = stripslashes(str_ireplace('{{transaction.gateway_msg}}',$transaction_details ,$message));
			   
			   $message = stripslashes(str_ireplace('{{invoice_payment_pagelink}}','' ,$message ));
				 
			  $message = stripslashes(str_ireplace('{{invoice.url_permalink}}',$in_link, $message));
			  $message = stripslashes(str_ireplace('{{merchant_email}}',$config_email ,$message ));
			   $message = stripslashes(str_ireplace('{{merchant_phone}}',$mphone ,$message ));
			   $message = stripslashes(str_ireplace('{{current.date}}',$cur_date ,$message ));
		    
			 
			   $fromEmail    = isset($view_data['fromEmail'])? $view_data['fromEmail']:$merchant_data['merchantEmail'];
			   $toEmail   = isset($indata['Contact'])? $indata['Contact']:$indata['userEmail'];
			   $addCC      = $view_data['addCC'];
			   $addBCC		= $view_data['addBCC'];
			   $replyTo    = isset($view_data['replyTo'])?$view_data['replyTo']:$config_email;
          	    
            
			$isValid = is_a_valid_customer([
                'email' => $toEmail
            ]);
            if(!$isValid){
                return false;
            }

			$mailData = [
				'to' => $toEmail,
				'from' => $fromEmail,
				'fromname' => $view_data['mailDisplayName'],
				'subject' => $subject,
				'replyto' => $replyTo,
				'html' => $message,
			];
			sendEmailUsingSendGrid($mailData);
	}
	
	
	public function process_recurring_invoice()
    {

        $invoice_data = $this->fb_company_model->getAutoRecurringInvoices();

        if (!empty($invoice_data)) {
            foreach ($invoice_data as $in_data) {

                if ($in_data['BalanceRemaining'] != '0.00') {

                    if ($in_data['DueDate'] != '') {
						$card_data  = $this->card_model->get_single_card_data($in_data['cardID']);
						if(empty($card_data)){
							continue;
						}
						$user_id    = $in_data['merchantID'];

                        $invWhere  = array( 'BalanceRemaining >' => 0, 'invoiceID' => $in_data['invoiceID'], 'merchantID' => $user_id);
						$invData = $this->general_model->get_row_data('Xero_test_invoice', $invWhere);
						if(!$invData || empty($invData)){
							continue;
						}

                        $card_no    = $card_data['CardNo'];
                        $cvv        = $card_data['CardCVV'];
                        $expmonth   = $card_data['cardMonth'];
                        $exyear     = $card_data['cardYear'];
						$cardType   = $card_data['CardType'];
						
                        $address1   = $card_data['Billing_Addr1'];
                        $address2   = $card_data['Billing_Addr2'];
                        $city       = $card_data['Billing_City'];
                        $zipcode = $zip    = $card_data['Billing_Zipcode'];
                        $state      = $card_data['Billing_State'];
                        $country    = $card_data['Billing_Country'];
                        $customerID = $in_data['Customer_ListID'];

						$phone     	= $in_data['Phone'];
						$email = $in_data['userEmail'];
						$companyName =  $in_data['companyName'];
						$firstName = $in_data['firstName'];
						$lastName  = $in_data['lastName'];
						
						$name =  $in_data['firstName'] . " " .  $in_data['lastName'];
                        $invoiceID  = $in_data['invoiceID'];

						$amount = $in_data['BalanceRemaining'];

						$pay_sts = "";
                        $type = 'sale';

                        $paymentObj = new Manage_payments($user_id);
						$saleData = [
							'paymentDetails' => $card_data,
							'transactionByUser' => $this->transactionByUser,
							'ach_type' => (!empty($card_data['CardNo'])) ? 1 : 2,
							'invoiceID'	=> $invoiceID,
							'crtxnID'	=> '',
							'companyName'	=> $in_data['companyName'],
							'fullName'	=> $in_data['fullName'],
							'firstName'	=> $in_data['firstName'],
							'lastName'	=> $in_data['lastName'],
							'contact'	=> $in_data['phoneNumber'],
							'email'	=> $in_data['userEmail'],
							'amount'	=> $amount,
							'gatewayID' => $in_data['gatewayType'],
							'returnResult' => true,
							'customerListID' => $in_data['Customer_ListID'],
						];

						$saleData = array_merge($saleData, $in_data);
						$paidResult = $paymentObj->_processSaleTransaction($saleData);

                        $action = 'Pay Invoice';
                        $st = $pay_sts = 0;
						$msg = 'Payment Failed';
						$crtxnID = $qbID = '';

                        if(isset($paidResult['response']) && $paidResult['response']){
							$pay_sts = true;

                            $invoiceID = $in_data['invoiceID'];
                            $ispaid    = 1;
                            
                            $bamount = $in_data['BalanceRemaining'] - $amount;
                            $totamt  = $in_data['BalanceRemaining'] + $in_data['AppliedAmount'];
                            if ($bamount > 0) {
                                $ispaid = 0;
							}

                            $app_amount = $in_data['AppliedAmount'] + $amount;
                            $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
                            $condition  = array('invoiceID' => $in_data['invoiceID'], 'merchantID' => $user_id);
                            $this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $data);

							$fb_data = new Freshbooks_data($in_data['resellerID'],$user_id);
							$invoices=  $fb_data->create_invoice_payment($in_data['invoiceID'], $amount);
                            if ($invoices['status'] != 'ok') {
                                $crtxnID = '';
                            } else {
                                $crtxnID = $invoices['payment_id'];
                            }

                            $nf = $this->addNotificationForMerchant($amount,$in_data['fullName'],$customerID,$in_data['merchID'],$in_data['invoiceID']);
                        }else{
                        	$nf =  $this->failedNotificationForMerchant($amount,$in_data['fullName'],$customerID,$in_data['merchID'],$in_data['invoiceID']);
                        }

						$id = $this->general_model->insert_gateway_transaction_data($paidResult['result'], $type, $in_data['gatewayID'], $in_data['gatewayType'], $in_data['Customer_ListID'], $amount, $user_id, $crtxnID, $in_data['resellerID'], $invoiceID, false, ['id' => 0, 'type' => 4]);

						if ($pay_sts && $in_data['recurring_send_mail']) {
							$this->send_mail_data($in_data, '5');
						}
                    }

                }
            }

        }

    }

    public function addNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        if($invoiceNumber == null){
            $title = 'Sale Payments';
            $nf_desc = 'A payment for <b>'.$customerName.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'.';
            $type = 1;
        }else{
            $title = 'Invoice Checkout Payments';
            $in_data =    $this->general_model->get_row_data('Freshbooks_test_invoice', array('invoiceID'=>$invoiceNumber,'merchantID'=>$merchantID));
            if(isset($in_data['refNumber'])){
                $invoiceRefNumber = $in_data['refNumber'];
            }else{
                $invoiceRefNumber = $invoiceNumber;
            }
            $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'';
            $type = 2;
        }
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 4,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }   
    public function failedNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title = 'Failed Invoice Checkout payments';
        $in_data =    $this->general_model->get_row_data('Freshbooks_test_invoice', array('invoiceID'=>$invoiceNumber,'merchantID'=>$merchantID));
        if(isset($in_data['refNumber'])){
            $invoiceRefNumber = $in_data['refNumber'];
        }else{
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was attempted on '.$payDateTime.' but failed';
        $type = 2;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 4,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
	
  }

?>