<?php

class Freshbooks_invoice extends CI_Controller
{
	private $resellerID;
	private $merchantID;
    protected $loginDetails;
	public function __construct()
	{
		parent::__construct();

		include APPPATH . 'third_party/Freshbooks.php';
		include APPPATH . 'third_party/FreshbooksNew.php';

		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('Freshbook_models/fb_invoice_model');
		$this->load->model('Freshbook_models/fb_company_model');
		$this->load->model('Freshbook_models/fb_customer_model');
		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', true);

		$this->db1->query("SET SESSION sql_mode = ''");
		$this->db->query("SET SESSION sql_mode = ''");
		
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
			$this->merchantID = $this->session->userdata('logged_in')['merchID'];
			$this->resellerID = $this->session->userdata('logged_in')['resellerID'];
		} else if ($this->session->userdata('user_logged_in') != "") {

			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
			$this->merchantID = $merchID;
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		} else {
			redirect('login', 'refresh');
		}


		$val = array(
			'merchantID' => $this->merchantID,
		);
		$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		if (empty($Freshbooks_data)) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
			redirect('FreshBooks_controllers/home/index', 'refresh');
		}
		$this->subdomain     = $Freshbooks_data['sub_domain'];
		$key                = $Freshbooks_data['secretKey'];
		$this->accessToken   = $Freshbooks_data['accessToken'];
		$this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
		$this->fb_account    = $Freshbooks_data['accountType'];
		$condition1 = array('resellerID' => $this->resellerID);
		$sub1 = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
		$domain1 = $sub1['merchantPortalURL'];
		$URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

		define('OAUTH_CONSUMER_KEY', $this->subdomain);
		define('OAUTH_CONSUMER_SECRET', $key);
		define('OAUTH_CALLBACK', $URL1);
		$this->loginDetails = get_names();
	}

	public function ajax_invoices_list()
	{

		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		}else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in');
			$merchID = $user_id['merchantID'];
		}
		$condition =  array('merchantID' => $merchID);

		$condition1 =  array('qb.merchantID' => $merchID, 'cs.merchantID' => $merchID);
		$customerID = $this->czsecurity->xssCleanPostInput('customerID');
		$filter = $this->czsecurity->xssCleanPostInput('status_filter');

		$list = $this->fb_invoice_model->get_datatables_invoices($condition1, $filter);
		$list_data = $list['result'];
		$count = $this->fb_invoice_model->count_filtered_invoices($condition1,$filter);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$data['plantype_as'] = $this->general_model->chk_merch_plantype_as($merchID);
		$plantype_vs = $this->general_model->chk_merch_plantype_vt($merchID);
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list_data as $person) {
			$no++;
			$row = array();


				$balance = ($person->BalanceRemaining) ? number_format($person->BalanceRemaining, 2) : "0.00";
				$base_url1 = base_url() . "FreshBooks_controllers/Freshbooks_Customer/view_customer/" . $person->CustomerListID;
			$base_url2 = base_url() . "FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/" . $person->invoiceID;
			if(empty($customerID)){

				$row[] = "<div class='text-center visible-lg'><input class='singleCheck' type='checkbox' name='selectRow[]'' value='" . $person->invoiceID . "' ></div>";
			
				if ($plantype) {
					$row[] = "<div class='hidden-xs text-left'>$person->custname";
				} else {
					$row[] = "<div class='hidden-xs text-left cust_view'><a href='" . $base_url1 . "' >$person->custname </a>";
				}
			}
			
			if($plantype_vs){
				$row[] = "<div class='text-right cust_view'>".$person->refNumber;
			}else{
				$row[] = "<div class='text-right cust_view'><a href='" . $base_url2 . "' >$person->refNumber </a>";
			}
			$row[] = "<div class='hidden-xs text-right'> " . date('M d, Y', strtotime($person->DueDate)) . " </div>";

			$row[] = '<div class="hidden-xs text-right cust_view"> <a href="#pay_data_process" onclick="set_fb_payment_data(\'' . $person->invoiceID . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">' . '$' .  number_format(($person->BalanceRemaining), 2) . ' </a> </div>';

			$row[] =	"<div class='text-right'>$person->status</div>";

			$link = '';
			$disabled_class = "";
			if(strtolower($filter) == 'paid' AND strtolower($person->payment_gateway) == 'heartland echeck'){
				$disabled_class = "disabled";
			}
			if (($person->BalanceRemaining == 0 ||  $person->IsPaid == '1') && $person->UserStatus == '0') {


				$link .= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li>
						           <a href="javascript:void(0);" id="txnRefund'. $person->invoiceID.'"  invoice-id="'. $person->invoiceID.'" integration-type="3" data-url="'.base_url().'ajaxRequest/getTotalRefundOfInvoice" class="refunTotalInvoiceAmountCustom">Refund</a>
						           </li>
                               
						          </ul>
						          </div> </div>';
			} else  if ($person->UserStatus == '1') {
				$link .= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li><a href="javascript:void(0);"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>
                               
						          </ul>
						          </div> </div>';
			} else {
				$link .= '<div class="text-center">
             	            	
                     	            	<div class="btn-group dropbtn">
                                         <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
                                         <ul class="dropdown-menu text-left">
                                            <li> <a href="#fb_invoice_process" class=""  data-backdrop="static" data-keyboard="false"
                                     data-toggle="modal" onclick="set_fb_invoice_process_id(\'' . $person->invoiceID . '\',\'' . $person->CustomerListID . '\',\'' . $person->BalanceRemaining . '\',1);">Process</a></li>
                                       <li><a href="#fb_invoice_schedule" onclick="set_invoice_schedule_date_id(\'' . $person->invoiceID . '\',\'' . $person->CustomerListID . '\',\'' . $person->BalanceRemaining . '\',\'' . date('M d Y', strtotime($person->DueDate)) . '\');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                                            
                                        <li> <a href="#set_subs" class=""  onclick=
        							"set_sub_status_id(\'' . $person->invoiceID . '\',\'' . $person->BalanceRemaining . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>';
				if ($person->BalanceRemaining == $person->Total_payment) {

					$link .= '<li> <a href="#fb_invoice_delete" onclick="get_invoice_id(\'' . $person->invoiceID . '\');" data-keyboard="false" data-toggle="modal" class="'.$disabled_class.'">Void</a></li>';
				}

				$link .= '</ul>
                                     </div> </div>  ';
			}

			$row[] = $link;

			$data[] = $row;

		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $count, // $this->fb_invoice_model->count_all_invoices($condition1),
			"recordsFiltered" => $count, // $this->fb_invoice_model->count_filtered_invoices($condition1),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
		die;
	}


	public function index()
	{
		redirect('FreshBooks_controllers/home', 'refresh');
	}

	public function add_invoice()
	{

		if($this->session->userdata('logged_in')){

			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id = $data['login_info']['merchantID'];
		}
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$condition1 = array('merchantID' => $user_id);

		$compdata				= $this->fb_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

	
		$data['plans'] = $this->general_model->get_table_data('Freshbooks_test_item', $condition1);
		$taxes = $this->general_model->get_table_data('tbl_taxes_fb', $condition1);
		$data['taxes'] = $taxes;
		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

		if ($this->uri->segment(4) != "") {
			$customerID = $this->uri->segment(4);
			$data['Invcustomer']  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $customerID));
			$data['cards']        = $this->card_model->get_card_data($customerID);
		}

		$data['netterms']  = $this->fb_customer_model->get_terms_data($user_id);

		$country = $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;
		$data['selectedID'] = $this->general_model->getTermsSelectedID($user_id);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/create_invoice', $data);
		$this->load->view('FreshBooks_views/page_fb_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function Invoice_details()
	{
		$filter = $this->czsecurity->xssCleanPostInput('status_filter');
		$data['filter'] = $filter;
		$merchID = $this->merchantID;
		$syncObejct = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
		$customerSync = $syncObejct->syncLastRun(['fb_action' => 'CustomerQuery']);
		$invoiceSync = $syncObejct->syncLastRun(['fb_action' => 'InvoiceQuery']);

		$invNumber = 0;
		$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
		if (!empty($in_data)) {
			$invNumber    =   $in_data['postfix'];
		}

		$customer = [];
		if (isset($this->accessToken) && isset($this->access_token_secret)) {

			if ($this->fb_account == 1) {
				$c = $syncObejct;

				$invoices = $c->get_invoices_data($invoiceSync['last_date'], $invoiceSync['current_date']);
				$customer = $c->get_customers_data($customerSync['last_date'], $customerSync['current_date']);
			} else {
				$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

				$invoices = $c->get_invoices_data($invoiceSync['last_date'], $invoiceSync['current_date']);
				$customer = $c->get_customers_data($customerSync['last_date'], $customerSync['current_date']);
			}

			if(!empty($customer)) {
				foreach ($customer as $oneCustomer) 
				{
					if($this->fb_account==1)
					{
						$companyName = $oneCustomer->organization;
						$username = $oneCustomer->username;
						$fname = $oneCustomer->fname;
						$lname = $oneCustomer->lname;
						
					$FB_customer_details = array(
						"Customer_ListID" => $oneCustomer->userid,
						"firstName" => "$fname",
						"lastName" => "$lname",
						"companyName" => "$companyName",
						"fullName" => "$fname $lname",
						"userEmail" => $this->db->escape_str(($oneCustomer->email)?$oneCustomer->email:$oneCustomer->contacts->contact->email),
						"phoneNumber" => $this->db->escape_str(($oneCustomer->home_phone)?$oneCustomer->home_phone: ''),
						"address1" => $this->db->escape_str(($oneCustomer->p_street)?$oneCustomer->p_street:''),
						"address2" => $this->db->escape_str(($oneCustomer->p_street2)?$oneCustomer->p_street2:''),
						"zipCode" => $this->db->escape_str(($oneCustomer->p_code)?$oneCustomer->p_code:''),
						"City" => $this->db->escape_str(($oneCustomer->p_city)?$oneCustomer->p_city:''),
						"State" => $this->db->escape_str(($oneCustomer->p_province)?$oneCustomer->p_province:''),
						"Country" => $this->db->escape_str(($oneCustomer->p_country)?$oneCustomer->p_country:''),
						
						"ship_address1" => $this->db->escape_str(($oneCustomer->s_street)?$oneCustomer->s_street:''),
						"ship_address2" => $this->db->escape_str(($oneCustomer->s_street2)?$oneCustomer->s_street2:''),
						"ship_zipcode" => $this->db->escape_str(($oneCustomer->s_code)?$oneCustomer->s_code:''),
						"ship_city" => $this->db->escape_str(($oneCustomer->s_city)?$oneCustomer->s_city:''),
						"ship_state" => $this->db->escape_str(($oneCustomer->s_province)?$oneCustomer->s_province:''),
						"ship_country" => $this->db->escape_str(($oneCustomer->s_country)?$oneCustomer->s_country:''),
						
                        "TimeCreated" => $oneCustomer->signup_date,
						"companyID" => $oneCustomer->accounting_systemid,
						"updatedAt" => $this->db->escape_str($oneCustomer->updated),
						"merchantID" => $this->merchantID,
					);
					
					if($this->general_model->get_num_rows('Freshbooks_custom_customer',array('Customer_ListID'=>$oneCustomer->userid, 'merchantID'=>$this->merchantID)) > 0){  
	
						$this->general_model->update_row_data('Freshbooks_custom_customer',array( 'Customer_ListID'=>$oneCustomer->userid, 'merchantID'=>$this->merchantID), $FB_customer_details);
	
					}else{
						$this->general_model->insert_row('Freshbooks_custom_customer', $FB_customer_details);  
					} 
						
					} else if($this->fb_account==2)
					{
	
					$FB_customer_details = array(
						"Customer_ListID" => $oneCustomer->client_id,
						"firstName" => $this->db->escape_str($oneCustomer->first_name),
						"lastName" => $this->db->escape_str($oneCustomer->last_name),
						"fullName" => $this->db->escape_str($oneCustomer->organization),
						"userEmail" => $this->db->escape_str(($oneCustomer->email)?$oneCustomer->email:$oneCustomer->contacts->contact->email),
						"phoneNumber" => $this->db->escape_str(($oneCustomer->home_phone)?$oneCustomer->home_phone:$oneCustomer->work_phone),
						"address1" => $this->db->escape_str(($oneCustomer->p_street1)?$oneCustomer->p_street1:''),
						"address2" => $this->db->escape_str(($oneCustomer->p_street2)?$oneCustomer->p_street2:''),
						"zipCode" => $this->db->escape_str(($oneCustomer->p_code)?$oneCustomer->p_code:''),
						"City" => $this->db->escape_str(($oneCustomer->p_city)?$oneCustomer->p_city:''),
						"State" => $this->db->escape_str(($oneCustomer->p_state)?$oneCustomer->p_state:''),
						"Country" => $this->db->escape_str(($oneCustomer->p_country)?$oneCustomer->p_country:''),
						
						"ship_address1" => $this->db->escape_str(($oneCustomer->s_street1)?$oneCustomer->s_street1:''),
						"ship_address2" => $this->db->escape_str(($oneCustomer->s_street2)?$oneCustomer->s_street2:''),
						"ship_zipcode" => $this->db->escape_str(($oneCustomer->s_code)?$oneCustomer->s_code:''),
						"ship_city" => $this->db->escape_str(($oneCustomer->s_city)?$oneCustomer->s_city:''),
						"ship_state" => $this->db->escape_str(($oneCustomer->s_state)?$oneCustomer->s_state:''),
						"ship_country" => $this->db->escape_str(($oneCustomer->s_country)?$oneCustomer->s_country:''),
						
						"companyID" => 2,
						"updatedAt" => $this->db->escape_str($oneCustomer->updated),
						"merchantID" => $$this->merchantID,
						"Reminder" => ''
					);
						
					if($this->general_model->get_num_rows('Freshbooks_custom_customer',array('Customer_ListID'=>$oneCustomer->client_id, 'merchantID'=>$merchID)) > 0){  
						$this->general_model->update_row_data('Freshbooks_custom_customer',array( 'Customer_ListID'=>$oneCustomer->client_id, 'merchantID'=>$merchID), $FB_customer_details);
					}
					else{
						$this->general_model->insert_row('Freshbooks_custom_customer', $FB_customer_details);  
					}
					}
				}
			}
			
			foreach ($invoices as $oneInvoice) {

				if ($this->fb_account == 1) {
					$invRefNumber = (string) $oneInvoice->invoice_number;
					$invRefNumberEx = explode('-', $invRefNumber);
					if(isset($invRefNumberEx[1]) && $invNumber < trim($invRefNumberEx[1])){
						$invNumber = trim($invRefNumberEx[1]);
					}

					$invoice_id = $oneInvoice->invoiceid;
					$FB_invoice_details['invoiceID'] = $oneInvoice->invoiceid;
					$FB_invoice_details['refNumber'] = $invRefNumber;
					$FB_invoice_details['ShipAddress_Addr1'] = $this->db->escape_str($oneInvoice->street);
					$FB_invoice_details['ShipAddress_Addr2'] = $this->db->escape_str($oneInvoice->street2);
					$FB_invoice_details['ShipAddress_City'] = $this->db->escape_str($oneInvoice->city);
					$FB_invoice_details['ShipAddress_Country'] = $this->db->escape_str($oneInvoice->country);
					$FB_invoice_details['ShipAddress_PostalCode'] = $this->db->escape_str($oneInvoice->code);
					$FB_invoice_details['CustomerFullName'] = $this->db->escape_str(($oneInvoice->organization) ? $oneInvoice->organization : '');
					$FB_invoice_details['BalanceRemaining'] = $oneInvoice->outstanding->amount;
					$FB_invoice_details['Total_payment'] = $oneInvoice->amount->amount;
					$FB_invoice_details['AppliedAmount'] = $oneInvoice->paid->amount;
					$FB_invoice_details['UserStatus'] = $this->db->escape_str($oneInvoice->payment_status);
					$FB_invoice_details['CustomerListID'] = $oneInvoice->customerid;
					$FB_invoice_details['TimeModified'] = $this->db->escape_str(($oneInvoice->updated) ? $oneInvoice->updated : '');
					$FB_invoice_details['TimeCreated'] = $this->db->escape_str(($oneInvoice->created_at) ? $oneInvoice->created_at : '');
					$FB_invoice_details['DueDate'] = $this->db->escape_str($oneInvoice->due_date);
					$FB_invoice_details['merchantID'] = $merchID;

					if($FB_invoice_details['BalanceRemaining'] == 0 || $FB_invoice_details['Total_payment'] == $FB_invoice_details['AppliedAmount']){
						$FB_invoice_details['IsPaid'] = 1;
					} else {
						$FB_invoice_details['IsPaid'] = 0;
					}

					$to_tax = 0;
					if ($oneInvoice->lines) {
						$lines = $oneInvoice->lines;

						$this->general_model->delete_row_data('tbl_freshbook_invoice_item', array('merchantID' => $merchID, 'invoiceID' => $invoice_id));
						$this->general_model->delete_row_data('tbl_freshbook_invoice_item_tax', array('merchantID' => $merchID, 'invoiceID' => $invoice_id));
						foreach ($lines as $line) {

							$line_data['itemID'] = $line->lineid;
							$line_data['itemName'] = $line->name;
							$line_data['itemDescription'] = (string) $line->description;
							$line_data['itemPrice'] = $line->unit_cost->amount;
							$line_data['itemQty'] = $line->qty;
							$line_data['totalAmount'] = $line->amount->amount;
							
							$line_data['merchantID'] = $merchID;
							$line_data['invoiceID'] = $line->invoiceid;
							$line_data['createdAt'] = date('Y-m-d H:i:s', strtotime($line->updated));

							$itemInvID = $this->general_model->insert_row('tbl_freshbook_invoice_item', $line_data);

							if($itemInvID){
								$taxIndex = 1;
								$taxIndexFound = true;
								do {
									if (isset($line->{"taxAmount$taxIndex"}) && $line->{"taxAmount$taxIndex"} > 0) {
										$taxInvItem = [];
										$taxInvItem['itemLineID'] = $itemInvID;
										$taxInvItem['merchantID'] = $merchID;
										$taxInvItem['invoiceID'] = $invoice_id;

										$taxInvItem['taxName'] = $line->{"taxName$taxIndex"};
										$taxInvItem['taxAmount'] = $line->{"taxAmount$taxIndex"};
										$taxInvItem['taxNumber'] = $line->{"taxNumber$taxIndex"};

										$this->general_model->insert_row('tbl_freshbook_invoice_item_tax', $taxInvItem);
										++$taxIndex;
									} else {
										$taxIndexFound = false;
									}
								} while ($taxIndexFound === true);
							}
						}
					}
					$FB_invoice_details['totalTax'] = $to_tax;
				} else {
					$invoice_id = $oneInvoice->invoice_id;
					$FB_invoice_details['invoiceID'] = $oneInvoice->invoice_id;
					$FB_invoice_details['refNumber'] = (string) $oneInvoice->number;
					$FB_invoice_details['ShipAddress_Addr1'] = $this->db->escape_str(($oneInvoice->p_street1) ? $oneInvoice->p_street1 : '');
					$FB_invoice_details['ShipAddress_Addr2'] = $this->db->escape_str(($oneInvoice->p_street2) ? $oneInvoice->p_street2 : '');
					$FB_invoice_details['ShipAddress_City'] = $this->db->escape_str(($oneInvoice->p_city) ? $oneInvoice->p_city : '');
					$FB_invoice_details['ShipAddress_Country'] = $this->db->escape_str(($oneInvoice->p_country) ? $oneInvoice->p_country : '');
					$FB_invoice_details['ShipAddress_PostalCode'] = $this->db->escape_str(($oneInvoice->p_code) ? $oneInvoice->p_code : '');
					$FB_invoice_details['CustomerFullName'] = $this->db->escape_str(($oneInvoice->organization) ? $oneInvoice->organization : '');
					$FB_invoice_details['BalanceRemaining'] = $oneInvoice->amount_outstanding;
					$FB_invoice_details['Total_payment'] = 0;
					$FB_invoice_details['AppliedAmount'] = $oneInvoice->paid;
					$FB_invoice_details['UserStatus'] = $this->db->escape_str($oneInvoice->status);
					$FB_invoice_details['CustomerListID'] = $oneInvoice->client_id;
					$FB_invoice_details['TimeModified'] = $this->db->escape_str(($oneInvoice->updated) ? $oneInvoice->updated : '');
					$FB_invoice_details['TimeCreated'] = $this->db->escape_str(($oneInvoice->created_at) ? $oneInvoice->created_at : '');
					$FB_invoice_details['DueDate'] = $this->db->escape_str(($oneInvoice->date) ? $oneInvoice->date : '');
					$FB_invoice_details['merchantID'] = $merchID;


					$to_tax = 0;
					if ($oneInvoice->lines->line) {
						$lines = $oneInvoice->lines->line;
						$this->general_model->delete_row_data('tbl_freshbook_invoice_item', array('merchantID' => $merchID, 'invoiceID' => $invoice_id));
						foreach ($lines as $line) {
							$line_data['itemID'] = $line->order;
							$line_data['itemDescription'] = (string) $line->name;
							$line_data['itemPrice'] = $line->unit_cost;
							$line_data['itemQty'] = $line->quantity;
							$line_data['totalAmount'] = $line->amount;
							if ($line->tax1_name) {
								$line_data['taxName'] = (string) $line->tax1_name;
								$to_tax +=    $line_data['taxValue'] = (($line->quantity * $line->unit_cost) * $line->tax1_percent) / 100;
								$line_data['taxPercent'] = $line->tax1_percent;
							} else {
								$line_data['taxName'] = array();
								$line_data['taxValue'] = 0;
							}
							$line_data['merchantID'] = $merchID;
							$line_data['invoiceID'] = $oneInvoice->invoice_id;


							$this->general_model->insert_row('tbl_freshbook_invoice_item', $line_data);
						}
					}
					$FB_invoice_details['totalTax'] = $to_tax;
				}

				if($FB_invoice_details['UserStatus'] == 'partial'){
                    $FB_invoice_details['UserStatus'] = 'unpaid';
                }

				if ($this->general_model->get_num_rows('Freshbooks_test_invoice', array('merchantID' => $merchID, 'invoiceID' => $invoice_id)) > 0)

					$this->general_model->update_row_data('Freshbooks_test_invoice', array('merchantID' => $merchID, 'invoiceID' => $invoice_id), $FB_invoice_details);
				else
					$this->general_model->insert_row('Freshbooks_test_invoice', $FB_invoice_details);
			}

			if($invNumber != 0){
				$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $invNumber));
			}
		}

		if(!empty($this->input->post(null, true))) {
			$search = $this->input->post(null, true);
			if(isset($search['search_data'])){
				$data['serachString'] = $search['search_data'];

			}
		}



		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		if($filter != ''){
			if($filter == 'All'){
				$data['invoices'] = $this->fb_invoice_model->get_invoice_list_data($merchID);
			}else{
				if($filter == 'open'){
					$data['invoices'] = $this->fb_invoice_model->get_open_invoice_data($merchID);
				}
				else{
					$data['invoices'] = $this->fb_invoice_model->get_invoice_data_by_status($merchID,$filter);
				}
			}
		}
		else{
			$data['invoices'] = $this->fb_invoice_model->get_open_invoice_data($merchID);
		}
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($merchID);
		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];
		$data['inv_datas']   = $this->fb_invoice_model->get_invoice_details($merchID);

		$plantype = $this->general_model->chk_merch_plantype_status($merchID);
		$data['plantype_as'] = $this->general_model->chk_merch_plantype_as($merchID);
		$data['plantype_vs'] = $this->general_model->chk_merch_plantype_vt($merchID);
		$data['plantype'] = $plantype;

		$merchant_condition = [
			'merchID' => $merchID,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
			$data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}

		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];
		$data['merchantEmail'] = $this->session->userdata('logged_in')['merchantEmail'];
		
        
		$templateList = $this->general_model->getTemplateForInvoice($merchID);


		$data['templateList'] = $templateList;

		$data['page_num']      = 'customer_freshbook';
		$data['merchID']      = $merchID;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/page_invoices', $data);
		$this->load->view('FreshBooks_views/page_fb_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function invoice_create()
	{
		if (!empty($this->input->post(null, true))) {
			$total = 0;

			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$item = [];

				$prID = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemID'] = $prID;
				$prod = $this->general_model->get_select_data('Freshbooks_test_item', array('Name'), array('productID' => $prID, 'merchantID' => $this->merchantID));
				$iname = $prod['Name'];
				$insert_row['itemListName'] = $iname;
				$item['qty']                = $insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']    = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$item['name']               = $insert_row['itemFullName'] = $iname;
				$taxID = 0;
				$tname = '';
				$trate = 0;
				$taxval  = 0;
				
				$item['description'] = $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item['type'] = 0;

				$totalTaxItems = $this->czsecurity->xssCleanPostInput('totalTaxItems')[$key];
				if($totalTaxItems != ''){
					$totalTaxItems = json_decode($totalTaxItems, true);
					
					$taxIndex = 1;
					foreach($totalTaxItems as $taxItem){
						$item["taxName$taxIndex"] = $taxItem['friendlyName'];
						$item["taxNumber$taxIndex"] = $taxItem['number'];
						$item["taxAmount$taxIndex"] =  $taxItem['taxRate'];
						++$taxIndex;
					}
				}

				$item['unit_cost'] = array('amount' => $insert_row['itemRate'], 'code' => 'USD');
				$item_val[$key] = $insert_row;
				$fb_items[] = $item;
			}

			$merchID = $this->merchantID;
			$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
			$address1 = $this->czsecurity->xssCleanPostInput('address1');
			$address2 = $this->czsecurity->xssCleanPostInput('address2');
			$country = $this->czsecurity->xssCleanPostInput('country');

			$state = $this->czsecurity->xssCleanPostInput('state');
			$city = $this->czsecurity->xssCleanPostInput('city');
			$zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
			$phone = $this->czsecurity->xssCleanPostInput('phone');

			$inv_date  = date($this->czsecurity->xssCleanPostInput('invdate'));
			$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');
			if ($pterm != "" && $pterm != 0) {
				$duedate = date("Y-m-d", strtotime("$inv_date +$pterm day"));
			} else {
				$duedate = date('Y-m-d', strtotime($inv_date));
			}

			$paygateway  = 0;
			$cardID = 0;

			$customerID = $this->czsecurity->xssCleanPostInput('customerID');

			if (isset($this->accessToken) && isset($this->access_token_secret)) {
				$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
				if (!empty($in_data)) {
					$inv_pre   =   $in_data['prefix'];
					$nexnum    =   $in_data['postfix'] + 1;
					$inv_po    = $inv_pre . $nexnum;
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong> Create your invoice prefix</div>');

					redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
				}

				if ($this->fb_account == 1) {

					$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
					$invoice = array(
						'customerid' => $customerID, 'create_date' => date('Y-m-d'), 'due_offset_days' => $pterm, 'invoice_number' => $inv_po, 'country' => $country, 'province' => $state, 'street' => $address2, 'address' => $address1, 'city' => $city,
						'status' => 1, "lines" => $fb_items
					);

					$invoices = $c->add_invoices($invoice);


					if ($invoices) {
						foreach ($item_val as $itm) {
							$ins['itemID']      = $itm['itemID'];
							$ins['itemName']      = $itm['itemListName'];
							$ins['invoiceID']   = $invoices;
							$ins['merchantID']  = $this->merchantID;
							$ins['totalAmount']      = $itm['itemTotal'];
							$ins['itemQty']          = $itm['itemQuantity'];
							$ins['itemDescription']  = $itm['itemDescription'];
							$ins['itemPrice'] = $itm['itemRate'];
							// $ins['taxName']     = $itm['taxName'];
							// $ins['taxValue']    = $itm['taxValue'];
							// $ins['taxPercent']  = $itm['taxPercent'];
							$ins['createdAt']   = date('Y-m-d H:i:s');

							$this->general_model->insert_row('tbl_freshbook_invoice_item', $ins);
						}
					}

					$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $nexnum));
				} else {


					$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

					if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

						if ($this->czsecurity->xssCleanPostInput('taxes') == "") {
							$lineArray = "";

							for ($i = 0; $i < count($item_val); $i++) {

								$lineArray .= '<line><name>' . $item_val[$i]['itemListName'] . '</name>';
								$lineArray .= '<description>' . $item_val[$i]['itemDescription'] . '</description>';
								$lineArray .= '<unit_cost>' . $item_val[$i]['itemRate'] . '</unit_cost>';
								$lineArray .= '<quantity>' . $item_val[$i]['itemQuantity'] . '</quantity>';
								$lineArray .= '<type>Item</type></line>';
							}
							$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
							if (!empty($in_data)) {
								$inv_pre   = $in_data['prefix'];
								$nexnum    =   $in_data['postfix'] + 1;
								$inv_po    = $inv_pre . $nexnum;
							} else {
								$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong> Create your invoice prefix</div>');

								redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
							}
							$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $nexnum));

							$invoice = '<?xml version="1.0" encoding="utf-8"?>
             		<request method="invoice.create"> 
             		 <invoice> 
             		 <client_id>' . $customerID . '</client_id>
             		   <contacts>  
				        <contact>  
				            <contact_id>0</contact_id>   
				        </contact>
				        </contacts>
				         <number>' . $inv_po . '</number>
				        <status>draft</status>  
				        <date>' . $duedate . '</date>
				        <p_street1>' . $address1 . '</p_street1>
				         <p_street2>' . $address2 . '</p_street2>
				         <p_city>' . $city . '</p_city> 
				         <p_state>' . $state . '</p_state> 
				         <p_country>' . $country . '</p_country>
				         <p_code>' . $zipcode . '</p_code>
				          <lines>' .
								$lineArray . '
					     </lines>  
						</invoice>  
						</request> ';


							$invoices = $c->add_invoices($invoice);

							$this->session->set_flashdata('success', 'Success');
            
						} else {
							$taxval = explode(",", $this->czsecurity->xssCleanPostInput('taxes'));


							$lineArray = "";

							for ($i = 0; $i < count($item_val); $i++) {

								$lineArray .= '<line><name>' . $item_val[$i]['itemListName'] . '</name>';
								$lineArray .= '<description>' . $item_val[$i]['itemDescription'] . '</description>';
								$lineArray .= '<unit_cost>' . $item_val[$i]['itemRate'] . '</unit_cost>';
								$lineArray .= '<quantity>' . $item_val[$i]['itemQuantity'] . '</quantity> <tax1_name>' . $taxval[1] . '</tax1_name><tax1_percent>' . $taxval[2] . '</tax1_percent>';
								$lineArray .= '<type>Item</type></line>';
							}

							$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $nexnum));


							$invoice = '<?xml version="1.0" encoding="utf-8"?>
             		<request method="invoice.create"> 
             		 <invoice> 
             		 <client_id>' . $customerID . '</client_id>
             		   <contacts>  
				        <contact>  
				            <contact_id>0</contact_id>   
				        </contact>
				        </contacts>
				        <number>' . $inv_po . '</number>
				        <status>draft</status>  
				        <date>' . $duedate . '</date>
				        <p_street1>' . $address1 . '</p_street1>
				         <p_street2>' . $address2 . '</p_street2>
				         <p_city>' . $city . '</p_city> 
				         <p_state>' . $state . '</p_state> 
				         <p_country>' . $country . '</p_country>
				         <p_code>' . $zipcode . '</p_code>
				          <lines>' .
								$lineArray . '
					     </lines>  
						</invoice>  
						</request> ';


							$invoices = $c->add_invoices($invoice);
							$this->session->set_flashdata('success', 'Success');
						}
					}
				}

				if (!empty($invoices)) {


					$schedule['invoiceID'] =  $invoices;
					$schedule['gatewayID'] = $paygateway;
					$schedule['cardID'] =  $cardID;
					$schedule['customerID'] = $customerID;

					if ($this->czsecurity->xssCleanPostInput('autopay')) {
						$schedule['autoPay'] =  1;
						$schedule['paymentMethod'] =   1;
					} else {
						$schedule['autoPay'] =  1;
						$schedule['paymentMethod']  =   2;
					}
					$schedule['merchantID']     =  $merchID;
					$schedule['createdAt']    =  date('Y-m-d H:i:s');
					$schedule['updatedAt']  =  date('Y-m-d H:i:s');

					$this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);
				}


				redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
			}
		}
	}

	public function invoice_details_page()
	{

		$oneInvoice = array();
		if ($this->session->userdata('logged_in')) {
			$data['login_info']	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info']		= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$merchID = $user_id;

		$condition1 =  array('qb.merchantID' => $merchID, 'cs.merchantID' => $merchID);

		$condition =  array('merchantID' => $merchID);
		$inv_id = $this->uri->segment('4');
		if ($inv_id) {

			$syncObejct = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
			$syncedData = $syncObejct->getInvoiceByID($inv_id);

			$val = array(
				'merchantID' => $merchID,
			);
			$oneInvoice = $this->general_model->get_row_data('Freshbooks_test_invoice', array('invoiceID' => $inv_id, 'merchantID' => $merchID));
			if (!empty($oneInvoice) && !empty($syncedData)) {

				$invoiceID = $inv_id;
				$l_items = $this->fb_invoice_model->get_invoice_item_data($inv_id, $merchID);

				$invItems = [];
				if(!empty($l_items)){
					foreach($l_items as $singleItem){
						$itemLineID = $singleItem['itemLineID'];
						
						$taxItems = [];
						if(isset($invItems[$itemLineID])){
							$taxItems = $invItems[$itemLineID]['taxItems'];
						}

						$invItems[$itemLineID] = [
							'itemLineID' => $itemLineID,
							'invoiceID' => $singleItem['invoiceID'],
							'itemID' => $singleItem['itemID'],
							'itemName' => $singleItem['itemName'],
							'merchantID' => $singleItem['merchantID'],
							'releamID' => $singleItem['releamID'],
							'itemQty' => $singleItem['itemQty'],
							'itemDescription' => $singleItem['itemDescription'],
							'itemPrice' => $singleItem['itemPrice'],
							'createdAt' => $singleItem['createdAt'],
							'itemRefID' => $singleItem['itemRefID'],
						];

						$taxData = [];

						if($singleItem['item_tax_id'] !== null && $singleItem['item_tax_id']){
							$taxData = [
								'taxName' => $singleItem['taxName'],
								'taxValue' => $singleItem['taxValue'],
								'taxPercent' => $singleItem['taxPercent'],
								'item_tax_id' => $singleItem['item_tax_id'],
								'taxAmount' => $singleItem['taxAmount'],
								'taxNumber' => $singleItem['taxNumber'],
							];

							$taxItems[] = $taxData;
						}

						$invItems[$itemLineID]['taxItems'] = $taxItems; 
					}
				}

				$oneInvoice['l_items'] = $invItems;

				$oneInvoice['cust_data'] = $this->general_model->get_row_data('Freshbooks_custom_customer', array('merchantID' => $merchID, 'Customer_ListID' => $oneInvoice['CustomerListID']));

				$code = '';
				$link_data = $this->general_model->get_row_data('tbl_template_data', array('merchantID' => $merchID, 'invoiceID' => $invoiceID));
				$user_id = $merchID;
				$coditionp = array('merchantID' => $user_id, 'customerPortal' => '1');

				$ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);
				$data['ur_data'] = $ur_data;
				$position1 = $position = 0;
				$purl = '';
				if(isset($ur_data['customerPortalURL']) && !empty($ur_data['customerPortalURL'])){
					$ttt = explode(PLINK, $ur_data['customerPortalURL']);
					$purl = $ttt[0] . PLINK . '/customer/';
				}
				$str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
				$shuffled = str_shuffle($str);
				$shuffled = substr($shuffled, 1, 12) . '=' . $user_id;
				$code     =    $this->safe_encode($shuffled);
				$invcode    =  $this->safe_encode($invoiceID);
				$data['paylink']  = $purl . 'update_payment/' . $code . '/' . $invcode;
			} else {
				redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
			}
		} else {
			redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
		}

		$data['gateway_datas']		  = $this->general_model->get_gateway_data($merchID);
		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];

		$merchant_condition = [
			'merchID' => $merchID,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
			$data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}
		
		$data['primary_nav']  = primary_nav();
		$data['template']     = template_variable();
		$data['page_num']      = 'invoice_details';
		$data['invoice_data']     = $oneInvoice;
		$data['notes']   		  = $this->fb_customer_model->get_customer_note_data($oneInvoice['CustomerListID'], $merchID);
		$con = array('invoiceID' => $oneInvoice['invoiceID'], 'merchantID' => $merchID);
		$transactionData    =  $this->general_model->get_row_data('customer_transaction', $con);
		if(!empty($transactionData)){
			if (strpos($transactionData['gateway'], 'ECheck') !== false) {
	            $transactionData['paymentType'] = 2;
	        }else if (strpos($transactionData['gateway'], 'Offline') !== false) {
	            $transactionData['paymentType'] = 'Offline Payment';
	        }else{
	            $transactionData['paymentType'] = 1;
	        }
		}
    
    	$data['transaction'] = $transactionData;
		$condition1 = array('merchantID' => $user_id);
		$data['plans'] = $this->general_model->get_table_data('Freshbooks_test_item', $condition1);
		$taxes = $this->general_model->get_table_data('tbl_taxes_fb', $condition1);
		$data['taxes'] = $taxes;

		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/page_invoice_details', $data);
		$this->load->view('FreshBooks_views/page_fb_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function invoice_details_print()
	{


		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$invoiceID             =  $this->uri->segment(4);

		$data['template'] 		= template_variable();
		$merchID = $user_id;


		$condition2 			= array('invoiceID' => $invoiceID);
		$invoice_data           = $this->general_model->get_row_data('Freshbooks_test_invoice', $condition2);
		$condition3 			= array('Customer_ListID' => $invoice_data['CustomerListID']);
		$customer_data			= $this->general_model->get_row_data('Freshbooks_custom_customer', $condition3);

		$condition4 			= array('merchID' => $user_id);


		$company_data			= $this->general_model->get_select_data('tbl_merchant_data', array('firstName', 'lastName', 'companyName', 'merchantAddress1', 'merchantEmail', 'merchantContact', 'merchantAddress2', 'merchantCity', 'merchantState', 'merchantCountry', 'merchantZipCode'), $condition4);

		$config_data			= $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID' => $merchID));
		if ($config_data['ProfileImage'] != "")
			$logo = base_url() . LOGOURL . $config_data['ProfileImage'];
		else
			$logo = CZLOGO;
		$data['customer_data']  = $customer_data;
		$data['company_data']    = $company_data;

		$invoice_items  = $this->fb_company_model->get_invoice_item_data($invoiceID);
		
		$no = $invoiceID;
		$pdfFilePath = "$no.pdf";

		ini_set('memory_limit', '320M');
		$this->load->library("Pdf");
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		define(PDF_HEADER_TITLE, 'Invoice');
		define(PDF_HEADER_STRING, '');
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Chargezoom 1.0');
		$pdf->SetTitle($data['template']['title']);
		$pdf->SetPrintHeader(False);
		
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


		$pdf->SetFont('dejavusans', '', 10, '', true);
		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);
		// Add a page

		$pdf->AddPage();


		$y = 20;
		$logo_div = '<div style="text-align:left; float:left ">
		<img src="' . $logo . '"  border="0" />
		</div>';


		// set color for background
		$pdf->SetFillColor(255, 255, 255);
		// write the first column
		$pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);



		$tt = "\n" . $company_data['firstName'] . " " . $company_data['lastName'] . "<br/>" . $company_data['companyName'] . "<br/>" . ($company_data['merchantAddress1']) . "<br/> " . ($company_data['merchantAddress2']) . "<br/>" . ($company_data['merchantCity']) . ", " . ($company_data['merchantState']) . " " . ($company_data['merchantZipCode']) . '<br/>' . ($company_data['merchantCountry']);


		$y = 50;
		// set color for background
		$pdf->SetFillColor(255, 255, 255);

		// set color for text
		$pdf->SetTextColor(51, 51, 51);

		// write the first column
		$pdf->writeHTMLCell(80, 50, '', $y, "<b>From:</b><br/>" . $tt, 0, 0, 1, true, 'J', true);

		// set color for background

		// set color for text
		$pdf->SetTextColor(51, 51, 51);

		$pdf->writeHTMLCell(80, 50, '', '', '<b>Invoice: </b>' . $invoice_data['refNumber'] . "<br/><br/>" . '<b>Due Date: </b>' . date("m/d/Y", strtotime($invoice_data['DueDate'])), 0, 1, 1, true, 'J', true);

		$lll = '';

		if ($customer_data['firstName'] != '') {
			$lll .= $customer_data['firstName'] . ' ' . $customer_data['lastName'] . "<br>";
		} else {
			$lll .= $invoice_data['CustomerFullName'] . "<br>";
		}

		$lll .= $customer_data['companyName'] . '<br/>';
		if ($invoice_data['BillAddress_Addr1'] != '') {
			$lll .= $invoice_data['BillAddress_Addr1'] . '<br>' . $invoice_data['BillAddress_Addr2'];
			$lll .= '<br>';
		}


		if ($invoice_data['BillAddress_City'] || $invoice_data['BillAddress_State'] || $invoice_data['BillAddress_PostalCode']) {

			$lll .= ($invoice_data['BillAddress_City']) ? $invoice_data['BillAddress_City'] . ',' : '--';
			$lll .= ($invoice_data['BillAddress_State']) ? $invoice_data['BillAddress_State '] . ' ' : '--';
			$lll .= ($invoice_data['BillAddress_PostalCode']) ? $invoice_data['BillAddress_PostalCode'] : '--';
			$lll .= '<br>';
		}
		$lll .= ($customer_data['phoneNumber']) ? $customer_data['phoneNumber'] : '--';
		$lll .= '<br>';
		$email = ($customer_data['userEmail']) ? $customer_data['userEmail'] : '#';

		$lll .= '<a href="' . $email . '">' . $email . '</a>';

		$lll_ship = '';
		if ($customer_data['firstName'] != '') {
			$lll_ship .= $customer_data['firstName'] . ' ' . $customer_data['lastName'] . "<br>";
		} else {
			$lll_ship .= $invoice_data['CustomerFullName'] . "<br>";
		}
		$lll_ship .= $customer_data['companyName'] . '<br>';
		if ($invoice_data['ShipAddress_Addr1'] != '') {
			$lll_ship .= $invoice_data['ShipAddress_Addr1'] . '<br>' . $invoice_data['ShipAddress_Addr2'];
			$lll_ship .= '<br>';
		}

		if ($invoice_data['ShipAddress_City'] || $invoice_data['ShipAddress_State'] || $invoice_data['ShipAddress_PostalCode']) {
			$lll_ship .= ($invoice_data['ShipAddress_City']) ? $invoice_data['ShipAddress_City'] . ',' : '--' . ',';
			$lll_ship .= ($invoice_data['ShipAddress_State']) ? $invoice_data['ShipAddress_State'] . ' ' : '--';
			$lll_ship .= ($invoice_data['ShipAddress_PostalCode']) ? $invoice_data['ShipAddress_PostalCode'] : '--';
			$lll_ship .= '<br>';
		}
		$lll_ship .= ($customer_data['phoneNumber']) ? $customer_data['phoneNumber'] : '--';
		$lll_ship .= '<br>';

		$email = ($customer_data['userEmail']) ? $customer_data['userEmail'] : '#';

		$lll_ship .= '<a href="' . $email . '">' . $email . '</a>';


		$y = $pdf->getY();
		
		// write the first column
		$pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>" . $lll, 0, 0, 1, true, 'J', true);
		$pdf->SetTextColor(51, 51, 51);
		$pdf->writeHTMLCell(80, 0, '', '', '<b>Shipping Address</b><br/>' . $lll_ship . "<br/>", 0, 1, 1, true, 'J', true);
		$pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$pdf->setCellMargins(0, 1, 0, 0);
		$html = "\n\n\n" . '<h3></h3><table style="border: 1px solid black;"  cellpadding="4" >
    <tr style="border: 1px solid black;background-color:black;color:#FFFFFF;">
       
        <th style="border: 1px solid black;" colspan="2" align="center"><b>Product/Services</b></th>
        <th style="border: 1px solid black;" align="center"><b>Qty</b></th>
		  <th style="border: 1px solid black;" align="center"><b>Unit Rate</b></th>
		    <th style="border:1px solid black;" align="center"><b>Amount</b></th>
        
    </tr>';
		$html1 = '';
		$total_val = 0;
		$totaltax = 0;
		$total = 0;
		$tax = 0;
		foreach ($invoice_items as $key => $item) {

			$total +=  $item['itemQty'] * $item['itemPrice'];

			$html .= '<tr style="border: 1px solid black;" >
                       
						<td style="border: 1px solid black;" colspan="2">' . $item['Name'] . '</td>
                        <td style="border: 1px solid black;" align="center">' . $item['itemQty'] . '</td>
                        <td style="border: 1px solid black;" align="right" >' . number_format($item['itemPrice'], 2) . '</td>
						<td style="border: 1px solid black;" align="right" >' . number_format($item['itemQty'] * $item['itemPrice'], 2) . '</td>
                    </tr>';
		}
		$total_val = $total + $invoice_data['totalTax'];
		$total_tax = ($invoice_data['totalTax']) ? $invoice_data['totalTax'] : 0;
		$trate = ($invoice_data['taxRate']) ? $invoice_data['taxRate'] : '0.00';
		$html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right" >Tax ' . $trate . '%</td><td style="border: 1px solid black;" align="right">' . number_format($total_tax, 2) . '</td></tr>';
		$html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right">Subtotal</td><td style="border: 1px solid black;" align="right">' . number_format($total, 2) . '</td></tr>';
		$html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right">Total</td><td style="border: 1px solid black;" align="right">' . number_format($total_val, 2) . '</td></tr>';
		$html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right">Payment</td><td style="border: 1px solid black;" align="right">' . number_format((-$invoice_data['AppliedAmount']), 2) . '</td></tr>';
		$html .= '<tr><td colspan="4" align="right" >Balance</td><td align="right">' . number_format(($invoice_data['BalanceRemaining']), 2) . '</td></tr>';

		$email1 = ($company_data['merchantEmail']) ? $company_data['merchantEmail'] : '#';


		$html .= '</table>';
		$html .= '</table>';

		$pdf->writeHTML($html, true, false, true, false, '');

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output($pdfFilePath, 'D');
	}


	public function invoice_schedule()
	{
		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in');
			$merchID = $user_id['merchantID'];
		}
		
		$val = array(
			'merchantID' => $merchID,
		);

		$duedate = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date')));

		$Inv_id = $this->czsecurity->xssCleanPostInput('scheduleID');

		$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

		$subdomain     = $Freshbooks_data['sub_domain'];
		$key           = $Freshbooks_data['secretKey'];
		$accessToken   = $Freshbooks_data['accessToken'];
		$access_token_secret = $Freshbooks_data['oauth_token_secret'];

		define('OAUTH_CONSUMER_KEY', $subdomain);
		define('OAUTH_CONSUMER_SECRET', $key);
		define('OAUTH_CALLBACK', 'http://chargezoom.net/FreshBooks_controllers/Freshbooks_integration/auth');


		if (isset($accessToken) && isset($access_token_secret)) {
			$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);

			$invoice = '<?xml version="1.0" encoding="utf-8"?>  
								<request method="invoice.update">  
 								 <invoice>
 								 <invoice_id>' . $Inv_id . '</invoice_id>
 								 <date>' . $duedate . '</date>
 								 </invoice>  
								</request>';

			$sch_invoice = $c->add_invoices($invoice);

			if ($sch_invoice != null) {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

				redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
			} else {
				$this->session->set_flashdata('success', 'Success');

				redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
			}
		}
	}

	public function get_item_data()
	{

		$user_id = $this->merchantID;

		if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemID = $this->czsecurity->xssCleanPostInput('itemID');
			$itemdata = $this->general_model->get_row_data('Freshbooks_test_item', array('productID' => $itemID, 'merchantID' => $user_id));

			echo json_encode($itemdata);
			die;
		}
		return false;
	}

	public function delete_invoice()
	{

		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$merchID = $user_id;
		$val = array(
			'merchantID' => $merchID,
		);

		$inv_id = $this->czsecurity->xssCleanPostInput('invID');

		if (isset($this->accessToken) && isset($this->access_token_secret)) {
			if ($this->fb_account == 1) {

				$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
				//$inv_val =  array('amount'=>$amount,'code'=>'USD');
				$invoice = array(
					'invoiceid' => $inv_id,
					'vis_state' => 1,
				);

				$invoices = $c->update_invoices($invoice);
			} else {
				$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
				$invoice = '<request method="invoice.delete">  
					<invoice_id>' . $inv_id . '</invoice_id>  
				</request> ';

				$invoices = $c->add_invoices($invoice);
			}

			if($invoices){
				$invUpdate = [
					'UserStatus' => 'voided'
				];
				$this->general_model->update_row_data('Freshbooks_test_invoice',array('invoiceID'=> $inv_id, 'merchantID'=>$this->merchantID), $invUpdate);
				$this->session->set_flashdata('success', 'Success');
			}
		}
		redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
	}



	function getSequence($num)
	{
		return sprintf("%'.04d", $num);
	}


	function safe_encode($string)
	{
		return strtr(base64_encode($string), '+/=', '-_-');
	}


	public function edit_custom_invoice()
	{

		if (!empty($this->input->post(null, true))) {
			
			$total = 0;

			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$item = [];

				$prID = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemID'] = $prID;
				$prod = $this->general_model->get_select_data('Freshbooks_test_item', array('Name'), array('productID' => $prID, 'merchantID' => $this->merchantID));
				$iname = $prod['Name'];
				$insert_row['itemListName'] = $iname;
				$item['qty']                = $insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']    = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$item['name']               = $insert_row['itemFullName'] = $iname;
				$taxID = 0;
				$tname = '';
				$trate = 0;
				$taxval  = 0;
				
				$item['description'] = $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item['type'] = 0;

				$totalTaxItems = $this->czsecurity->xssCleanPostInput('totalTaxItems')[$key];
				if($totalTaxItems != ''){
					$totalTaxItems = json_decode($totalTaxItems, true);
					
					$taxIndex = 1;
					foreach($totalTaxItems as $taxItem){
						$item["taxName$taxIndex"] = $taxItem['friendlyName'];
						$item["taxNumber$taxIndex"] = $taxItem['number'];
						$item["taxAmount$taxIndex"] =  $taxItem['taxRate'];
						++$taxIndex;
					}
				}

				$item['unit_cost'] = array('amount' => $insert_row['itemRate'], 'code' => 'USD');
				$item_val[$key] = $insert_row;
				$fb_items[] = $item;
			}

			$merchID = $this->merchantID;
			$invNo = $this->czsecurity->xssCleanPostInput('invNo');
			$inv_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invdate')));

			$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');
			if (isset($this->accessToken) && isset($this->access_token_secret)) {
				if ($this->fb_account == 1) {
					$c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
					//$inv_val =  array('amount'=>$amount,'code'=>'USD');
					$invoice = array(
						'create_date' => "$inv_date", "invoiceid" => $invNo, "lines" => $fb_items
					);
					$invoices = $c->update_invoices($invoice);
					$this->session->set_flashdata('success', 'Success');
				} else {


					$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

					if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

						if ($this->czsecurity->xssCleanPostInput('taxes') == "") {
							$lineArray = "";

							for ($i = 0; $i < count($item_val); $i++) {

								$lineArray .= '<line><name>' . $item_val[$i]['itemListName'] . '</name>';
								$lineArray .= '<description>' . $item_val[$i]['itemDescription'] . '</description>';
								$lineArray .= '<unit_cost>' . $item_val[$i]['itemRate'] . '</unit_cost>';
								$lineArray .= '<quantity>' . $item_val[$i]['itemQuantity'] . '</quantity>';
								$lineArray .= '<type>Item</type></line>';
							}
							$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
							if (!empty($in_data)) {
								$inv_pre   = $in_data['prefix'];
								$nexnum    =   $in_data['postfix'] + 1;
								$inv_po    = $inv_pre . $nexnum;
							} else {
								$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong> Create your invoice prefix</div>');

								redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
							}
							$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $nexnum));

							$invoice = '<?xml version="1.0" encoding="utf-8"?>
             		<request method="invoice.create"> 
             		 <invoice> 
             		 <client_id>' . $customerID . '</client_id>
             		   <contacts>  
				        <contact>  
				            <contact_id>0</contact_id>   
				        </contact>
				        </contacts>
				         <number>' . $inv_po . '</number>
				        <status>draft</status>  
				        <date>' . $duedate . '</date>
				        <p_street1>' . $address1 . '</p_street1>
				         <p_street2>' . $address2 . '</p_street2>
				         <p_city>' . $city . '</p_city> 
				         <p_state>' . $state . '</p_state> 
				         <p_country>' . $country . '</p_country>
				         <p_code>' . $zipcode . '</p_code>
				          <lines>' .
								$lineArray . '
					     </lines>  
						</invoice>  
						</request> ';


							$invoices = $c->add_invoices($invoice);

							$this->session->set_flashdata('success', 'Success');
            
						} else {
							$taxval = explode(",", $this->czsecurity->xssCleanPostInput('taxes'));


							$lineArray = "";

							for ($i = 0; $i < count($item_val); $i++) {

								$lineArray .= '<line><name>' . $item_val[$i]['itemListName'] . '</name>';
								$lineArray .= '<description>' . $item_val[$i]['itemDescription'] . '</description>';
								$lineArray .= '<unit_cost>' . $item_val[$i]['itemRate'] . '</unit_cost>';
								$lineArray .= '<quantity>' . $item_val[$i]['itemQuantity'] . '</quantity> <tax1_name>' . $taxval[1] . '</tax1_name><tax1_percent>' . $taxval[2] . '</tax1_percent>';
								$lineArray .= '<type>Item</type></line>';
							}

							$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $nexnum));


							$invoice = '<?xml version="1.0" encoding="utf-8"?>
             		<request method="invoice.create"> 
             		 <invoice> 
             		 <client_id>' . $customerID . '</client_id>
             		   <contacts>  
				        <contact>  
				            <contact_id>0</contact_id>   
				        </contact>
				        </contacts>
				        <number>' . $inv_po . '</number>
				        <status>draft</status>  
				        <date>' . $duedate . '</date>
				        <p_street1>' . $address1 . '</p_street1>
				         <p_street2>' . $address2 . '</p_street2>
				         <p_city>' . $city . '</p_city> 
				         <p_state>' . $state . '</p_state> 
				         <p_country>' . $country . '</p_country>
				         <p_code>' . $zipcode . '</p_code>
				          <lines>' .
								$lineArray . '
					     </lines>  
						</invoice>  
						</request> ';


							$invoices = $c->add_invoices($invoice);
							// print_r($invoices);
							$this->session->set_flashdata('success', 'Success');
						}
					}
				}

				$redirect = $this->czsecurity->xssCleanPostInput('index');
				if($redirect == "self"){
					redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/'.$invNo));
				} else {
					redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
				}
			}
		}
	}
}
