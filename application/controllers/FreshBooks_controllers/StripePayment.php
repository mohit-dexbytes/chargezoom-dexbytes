<?php

/**
 * Sripe Payment Gateway Operations
 * Sale using create_customer_sale
 * Authorize  using create_customer_auth
 * Capture using create_customer_capture 
 * Void using create_customer_void
 * Refund create_customer_refund
 * Single Invoice Payment using pay_invoice
 * Multiple Invoice Payment using multi_pay_invoice
 
 */

class StripePayment extends CI_Controller
{
    
    private $resellerID;
    private $merchantID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
	    
		include APPPATH . 'third_party/Freshbooks.php';
	   	include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
	    $this->load->config('quickbooks');
		$this->load->model('quickbooks');
		 $this->load->library('form_validation');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('card_model');
		$this->load->model('company_model');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
       $this->db1=  $this->load->database('otherdb', TRUE);
	 if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='3')
		  {
			$logged_in_data = $this->session->userdata('logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		    $this->merchantID = $logged_in_data['merchID'];
			$this->resellerID = $logged_in_data['resellerID'];
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];  
		    $this->merchantID = $merchID;
		  }else{
			redirect('login','refresh');
		  }
		  
		  
		   $val = array(
             'merchantID' => $this->merchantID,
             );
		  	$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		  	if(empty($Freshbooks_data))
		  	{
		  	  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>'); 
              redirect('FreshBooks_controllers/home/index','refresh');
		  	  
		  	}
            $this->subdomain     = $Freshbooks_data['sub_domain'];
             $key                = $Freshbooks_data['secretKey'];
            $this->accessToken   = $Freshbooks_data['accessToken'];
            $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
            $this->fb_account    = $Freshbooks_data['accountType'];
            $condition1 = array('resellerID'=>$this->resellerID);
           	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			$domain1 = $sub1['merchantPortalURL'];
			$URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';  
            
            define('OAUTH_CONSUMER_KEY', $this->subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
	}
	
	
	public function index()
	{
	
	   	redirect('FreshBooks_controllers/home','refresh'); 
	}
	
	
	
	
        	 	 
        public function pay_invoice()
        {
            $this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");
			$this->session->unset_userdata("in_data");
       
        
        	 $cusproID=array(); 
        	 $error=array();
             $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
	    	
			        
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    	
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
        
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
        	$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {		
	 	
	                $merchID    = $this->merchantID;
		        	$user_id    = $merchID;
		        	$resellerID =	$this->resellerID;
		       		$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
                	
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				 
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;	
        		    $chh_mail  =0;	 
                    $in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
                        
                	if(!empty($in_data)&& $in_data['BalanceRemaining'] > 0)
            		{ 
		                 
                        $customerID = $in_data['Customer_ListID'];
            		
            			$companyID = $in_data['companyID'];
                        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
                     		
						 $stripeKey      = $gt_result['gatewayUsername'];
						 $strPass        = $gt_result['gatewayPassword'];
            		 
			        	
        			     if($cardID!="new1")
        			     {
        			        $card_data=   $this->card_model->get_single_card_data($cardID);
        			       
        			        $card_no  = $card_data['CardNo'];
    				        $expmonth =  $card_data['cardMonth'];
        					$exyear   = $card_data['cardYear'];
        					$cvv      = $card_data['CardCVV'];
        					$cardType = $card_data['CardType'];
        					$address1 = $card_data['Billing_Addr1'];
        					$city     =  $card_data['Billing_City'];
        					$zipcode  = $card_data['Billing_Zipcode'];
        					$state    = $card_data['Billing_State'];
        					$country  = $card_data['Billing_Country'];
        			     }
        			     else
        			     {
        			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
        					$cardType = $this->general_model->getType($card_no);
        					$cvv ='';
        					if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
        					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
        	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
        	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
        	                        $country =$this->czsecurity->xssCleanPostInput('country');
        	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
        	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
        	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        			         
        			     }    
    			        	
        			             $cr_amount      = 0;
    					      
    					     
    					            $amount      =	 $this->czsecurity->xssCleanPostInput('inv_amount'); 
									
									
									
        					      $real_amt  =  (int)($amount*100); 

								$plugin = new ChargezoomStripe();
				        		$plugin->setApiKey($stripeKey);
								$res =  \Stripe\Token::create([
        									'card' => [
        									'number' =>$card_no,
        									'exp_month' => $expmonth,
        									'exp_year' =>  $exyear,
        									'cvc' => $cvv,
										
        									'name' =>$comp_data['fullName']
        								   ]
        								   ]);
        						  
							$tcharge= json_encode($res);
									  
							$rest = json_decode($tcharge);
							if($rest->id)
							{	
        							
									$plugin = new ChargezoomStripe();
				        			$plugin->setApiKey($strPass);
        							$charge =	\Stripe\Charge::create(array(
        								  "amount" => $real_amt,
        								  "currency" => "usd",
        								  "source" => $rest->id, 
        								  "description" => "Charge Using Stripe Gateway",
        								 
        								));	
    					        

				         	$pinv_id='';
        					   if( $result->response_code=="1")
        					   {
						 
				
            			        	include APPPATH .'libraries/Freshbooks_data.php';
								   $fb_data = new Freshbooks_data($this->resellerID,$this->merchantID);
								  $invoices =  $fb_data->Freshbooks_data->create_invoice_payment($invoiceID,$amount);
								  if ($invoices['status'] != 'ok')
								  {
									  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
								  }
								  else 
								  {
									$pinv_id =  $invoices['payment_id'];
								
										  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
								  }

								  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
    							  $ref_number =  $in_data['refNumber']; 
    							  $tr_date   =date('Y-m-d H:i:s');
    							  	$toEmail = $in_data['userEmail']; $company=$in_data['companyName']; $customer = $in_data['fullName']; 
								if($chh_mail =='1')
    							 {
    							   
    							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
    							 }	
    							
                               	 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
                                 	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
										$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
										$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
											$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
            				 		$card_type      =$this->general_model->getType($card_no);
                                 
                                 			$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										  'CardType'     =>$card_type,
            										  'CustomerCard' =>$card_no,
            										  'CardCVV'      =>$cvv, 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                									 'Billing_Addr2'	 =>$address2,	 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->process_card($card_data);	
                                 }
    		
                        			
					   }		
                        else
					   {
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error '.$result->response_reason_text.'</strong>.</div>'); 
					   	  
					   }   				
            							 
            					     
                        $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$pinv_id, $this->resellerID,$in_data['invoiceID'], false, $this->transactionByUser);  
                   
				
					   
					   } 
					   else
					   {
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error Payment Detailes not Valid</strong>.</div>'); 
					   	  
					   }  
		 
				
				 
				}
    				else
    				{
    					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
    					  
    				}
          
		
		 
    	    	}
    	    	else
    	    	{
    	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Validation Error</strong>.</div>'); 
    			 }
			 
        
			if($cusproID!="")
			{
				$trans_id = $result->id;
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'FreshBooks_controllers/Create_invoice/Invoice_details',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
					 'checkPlan'  => $checkPlan
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 	 redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/'.$cusproID,'refresh');
			 }
		   	 else
		   	 {
		    	 redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
		   	 }
	   
    
}     


	
		 
    public function pay_invoiceOOOOO()
    {
        
    
	        $token='';
	        if($this->session->userdata('logged_in') )
	        {
	           $merchID = $this->session->userdata('logged_in')['merchID'];
	        	}
	        	if($this->session->userdata('user_logged_in') ){
	               $merchID = $this->session->userdata('user_logged_in')['merchantID'];
	        	} 
	        $merchantID  = $merchID;
			        	
			$rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$merchID));
			 $resellerID =	$rs_daata['resellerID'];
			 $condition1 = array('resellerID'=>$resellerID);
        	
         	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			 $domain1 = $sub1['merchantPortalURL'];
			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth'; 	
			 
			 	$val = array(
					'merchantID' => $merchID,
				);

				$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
            
            $subdomain     = $Freshbooks_data['sub_domain'];
            $key           = $Freshbooks_data['secretKey'];
            $accessToken   = $Freshbooks_data['accessToken'];
            $access_token_secret = $Freshbooks_data['oauth_token_secret'];
     
            define('OAUTH_CONSUMER_KEY', $subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
	        $invoicep_id='';	
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
		  $cusproID=array(); $error=array();
         $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		 
		 
		 
		 if($this->czsecurity->xssCleanPostInput('qbo_check') == 'qbo_pay'){
		 	
		 


         $token = $this->czsecurity->xssCleanPostInput('stripeToken');
       if(!empty($cardID) && !empty($gateway) && !empty($token) )
       {  
         	$in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
		 	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
       
		if(!empty($in_data)){ 
		  
            $Customer_ListID = $in_data['CustomerListID'];
              if($cardID=='new1')
           {
			        	$cardID_upd  =$cardID;
			        		$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				        $merchantID = $merchID;
          
           
                         $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID' => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                           $this->db1->select('*')->from('customer_card_data');
                           $qq =  $this->db1->get();
          
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                         
                         }else{
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
					    	if($checkCustomerCard == 0){
					    		$is_default = 1;
					    	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
        
		    $card_data    =   $this->card_model->get_single_card_data($cardID);

		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
				$cr_amount = 0;
			    	$amount    =	 $in_data['BalanceRemaining']; 
			    	
			    		$amount    =	 $this->czsecurity->xssCleanPostInput('inv_amount'); 
			

				 	 $amount    = $amount-$cr_amount;
					        
		        	 $amount1 =  (int)($amount*100);

			$plugin = new ChargezoomStripe();
			$plugin->setApiKey($gt_result['gatewayPassword']);    	 
		 	
			$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
			$charge =	\Stripe\Charge::create(array(
				  "amount" => $amount1,
				  "currency" => "usd",
				  "source" => $token, // obtained with Stripe.js
				  "description" => "Charge Using Stripe Gateway",
				 
				));	

			 $charge= json_encode($charge);
			 $result = json_decode($charge);

			  if($result->paid=='1' && $result->failure_code==""){
			  	 $code		 =  '200';
				if($this->session->userdata('logged_in')){
					$user_id = $this->session->userdata('logged_in')['merchID'];
					$merchID = $user_id;
				}
		
        
        if(isset($accessToken) && isset($access_token_secret))
        {
            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);

            $payment = '<?xml version="1.0" encoding="utf-8"?>  
						<request method="payment.create">  
						  <payment>         
						    <invoice_id>'.$invoiceID.'</invoice_id>               
						    <amount>'.$amount.'</amount>             
						    <currency_code>USD</currency_code> 
						    <type>Check</type>                   
						  </payment>  
						</request>';
             
              $invoices = $c->add_payment($payment);

			if ($invoices['status'] != 'ok') {
			
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>');

				redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
				
			}
			else {
			     $invoicep_id = $invoices['payment_id'];
				$this->session->set_flashdata('success','Success'); 
				
				
			}
		}
				}
				else{
					 $code =  $result->failure_code;
			       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error!  "'.$result->status.'"</strong>.</div>'); 
						}

				 
				 	   $transaction['transactionID']      = $result->id;
					   $transaction['transactionStatus']    = $result->status;
					   $transaction['transactionDate']     = date('Y-m-d H:i:s');  
					    $transaction['transactionModified']     = date('Y-m-d H:i:s');  
					   $transaction['transactionCode']         = $code;  
					  
						$transaction['transactionType']    = 'stripe_sale';	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					    $transaction['customerListID']     = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   = ($result->amount/100);
					  $transaction['merchantID']   = $merchID;
					  $transaction['invoiceID']   = $invoiceID;
					     $transaction['qbListTxnID'] =$invoicep_id  ;
                        $transaction['resellerID']   = $resellerID;
    				      $transaction['gateway']   = "Stripe";
					  $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 	
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);

					}
				 else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
			 
          }else{
			  $ermsg='';
       if($cardID=="" )
       $ermsg ="Card is required";
       if($gateway=="")
       $ermsg ="Gateway is required";
       if($token=="" )
        $ermsg ="Stripe token is required";
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$ermsg.'</div>'); 
		  }
			
		
		  
		  		 if($cusproID!=""){
			 	 redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
		   	 }
		  
		}
    
    }     

	
	
	

	public function create_customer_sale()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      $user_id = $this->merchantID;
		  $checkPlan = check_free_plan_transactions();
	      if($checkPlan && !empty($this->input->post(null, true)))
		{
			  
		
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			   
        
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
      
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
         
             
	   	if ($this->form_validation->run() == true)
		    {
	   			$custom_data_fields = [];
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }
        			      
					   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
					   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
					if($this->czsecurity->xssCleanPostInput('setMail'))
					 $chh_mail =1;
					 else
					  $chh_mail =0; 
        		 
				  
        			      
					 $invoiceIDs=array();
					 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
					 {
						$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					 }
				
		             $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        			
					 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
					 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
        			 $companyID  = $comp_data['companyID'];
        			 $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                           
        			 $cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =$this->czsecurity->xssCleanPostInput('cvv'); 
        				     
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						 
        					
        					}
        						$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    	                        
								
							 $stripeKey      = $gt_result['gatewayUsername'];
        				     $strPass        = $gt_result['gatewayPassword'];
        					 $amount         =	 $this->czsecurity->xssCleanPostInput('totalamount'); 
                              
        					      $real_amt  =  (int)($amount*100); 
                               	$metadata = [];
			            		if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			            			$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
									$metadata['invoice_number'] = $new_invoice_number;
								}

								if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
									$metadata['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
								}
								$plugin = new ChargezoomStripe();
				        		$plugin->setApiKey($stripeKey);
								$res =  \Stripe\Token::create([
        									'card' => [
        									'number' =>$card_no,
        									'exp_month' => $expmonth,
        									'exp_year' =>  $exyear,
        									'cvc' => $cvv,
											
        									'name' =>$comp_data['fullName']
        								   ]
        								   ]);
							$tcharge= json_encode($res);
									  
							$rest = json_decode($tcharge);
							if($rest->id)
							{	
        						 	
									$plugin = new ChargezoomStripe();
				        			$plugin->setApiKey($strPass);
        							$charge =	\Stripe\Charge::create(array(
        								  "amount" => $real_amt,
        								  "currency" => "usd",
        								  "source" => $rest->id, 
        								  "description" => "Charge Using Stripe Gateway",
        									'metadata' => $metadata
        								 
        								));	
                              
        						   $charge= json_encode($charge);
                              	  
        						   $result = json_decode($charge);
                                 		 
                                   if($result->paid=='1' && $result->failure_code=="")
                                   {
                                       
                                  
							 			
										 $invoiceIDs=array();      
										 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
										 {
											$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
										 }
							 
										 $refNumber =array();
							 
									   if(!empty($invoiceIDs))
									   {
								           include APPPATH .'libraries/Freshbooks_data.php';
										 $fb_data = new Freshbooks_data($this->merchantID,$this->resellerID);
									  foreach($invoiceIDs as $inID)
									  {
										
									$in_data = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining','refNumber','AppliedAmount'),array('merchantID'=>$this->merchantID,'invoiceID'=>$inID));
									$refNumber[] = 	$in_data['refNumber'];								
									$amount_data  = $amount; 								
									 $pinv_id =''; 
									$invoices=  $fb_data->create_invoice_payment($inID, $amount_data);
									 if ($invoices['status'] != 'ok')
									 {
											  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
									  }
									   else 
									   {
										 $pinv_id =  $invoices['payment_id'];
										
										  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
										}
									
									$id = $this->general_model->insert_gateway_transaction_data($result,'stripe_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount_data,$this->merchantID,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);  
								  }
							   }  
							   else
							   {
											$crtxnID = '';
											$inID    = '';
											$id      = $this->general_model->insert_gateway_transaction_data($result,'stripe_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$this->merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields); 
										   
								}		   
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								if(!empty($refNumber))
								$ref_number = implode(',',$refNumber); 
								else
								$ref_number = '';
								$tr_date   =date('Y-m-d H:i:s');
								  $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];		
								if($chh_mail =='1')
								 {
								   
								   
								   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
								 } 
								   
								 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
								 {
											$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
											$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
											$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
										   $card_type = $this->general_model->getType($card_no);
									 
												$card_data = array('cardMonth'   =>$expmonth,
														   'cardYear'	 =>$exyear, 
														  'CardType'     =>$card_type,
														  'CustomerCard' =>$card_no,
														  'CardCVV'      =>$cvv, 
														 'customerListID' =>$customerID, 
														 'companyID'     =>$companyID,
														  'merchantID'   => $this->merchantID,
														
														 'createdAt' 	=> date("Y-m-d H:i:s"),
														 'Billing_Addr1'	 =>$address1,
														 'Billing_Addr2'	 =>$address2,	 
															  'Billing_City'	 =>$city,
															  'Billing_State'	 =>$state,
															  'Billing_Country'	 =>$country,
															  'Billing_Contact'	 =>$phone,
															  'Billing_Zipcode'	 =>$zipcode,
														 );
												
											$id1 =    $this->card_model->process_card($card_data);	
									 }
				
								
							 $this->session->set_flashdata('success','Transaction Successful'); 
            			
							 } 
							 else
							{
											  $crtxnID ='';
											  
											
											   $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Payment Failed</strong></div>'); 
												$id = $this->general_model->insert_gateway_transaction_data($result,'stripe_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
											 
							}
                         
                         
                         
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Payment Detailes not Valid</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
		} 
		$invoice_IDs = array();
		   if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			   $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
		   }
	   
		   	$receipt_data = array(
			   'transaction_id' => isset($result) ? $result->id : '',
			   'IP_address' => getClientIpAddr(),
			   'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			   'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			   'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			   'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			   'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			   'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			   'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			   'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			   'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			   'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			   'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			   'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			   'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			   'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			   'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			   'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			   'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
			   'proccess_btn_text' => 'Process New Sale',
			   'sub_header' => 'Sale',
			   'checkPlan'  => $checkPlan
		   );
		   
		   $this->session->set_userdata("receipt_data",$receipt_data);
		   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
		   
		   
		   redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');


                   
			  $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				  
				$condition				= array('merchantID'=>$user_id );
			     $gateway        		= $this->general_model->get_gateway_data($user_id);
			     $data['gateways']      = $gateway['gateway'];
			     $data['gateway_url']   = $gateway['url'];
			     $data['stp_user']      = $gateway['stripeUser'];
   
				$compdata				= $this->fb_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
				$this->load->view('template/template_start', $data);
					
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
	    
	    
    	}
	


	
	
	public function create_customer_saleNEW()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs"); 			
		 
		 if(!empty($this->input->post(null, true)))
		 {
				$inv_array  = array();
		            	$inv_invoice= array();
		            	$qblist=array();
				if($this->session->userdata('logged_in'))
				{
					$merchantID = $this->session->userdata('logged_in')['merchID'];
			   }
			   if($this->session->userdata('user_logged_in'))
			   {
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			   }	
			 
        	   $resellerID =	$this->resellerID;
			
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  
			if($gatlistval !="" && !empty($gt_result) )
			{
			   
    		  
				
				 
		   $invoiceIDs='';
				          if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
				           {
				               $invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				            
				           }
		
			$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
					
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" )
		       {	
				     	$card     = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						 if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
					
						$cvv    = $this->czsecurity->xssCleanPostInput('cvv');
				
				  }
				  else 
				  {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];							
							$exyear   = $card_data['cardYear'];
							$exyear   = $exyear;
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							
							$cvv      = $card_data['CardCVV'];
					}	
					$amount =  (int)($this->czsecurity->xssCleanPostInput('totalamount')*100);
				
					
					$plugin = new ChargezoomStripe();
				    $plugin->setApiKey($gt_result['gatewayPassword']);
					$res = \Stripe\Token::create([
        									'card' => [
        									'number' =>$card,
        									'exp_month' => $expmonth,
        									'exp_year' =>  $exyear,
        									'cvc' => $cvv
        								   ]
        						]);
					$tcharge= json_encode($res);  
        			$rest = json_decode($tcharge);
        			
        			if($rest->id){
        				$token = $rest->id; 
        			}else{
        				$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
        			}
					$charge =	\Stripe\Charge::create(array(
						  "amount" => $amount,
						  "currency" => "usd",
						  "source" => $token, // obtained with Stripe.js
						  "description" => "Charge for test Account",
						 
						));	
               
			       $charge= json_encode($charge);
				   $result = json_decode($charge);
			  
				 $trID='';
				 if($result->paid=='1' && $result->failure_code=="")
				 {
				  $code ='200';
				  $trID = $result->id;
				 /* This block is created for saving Card info in encrypted form  */
				 
				 
				 
				 /*********** fresh book API************/
				 
				     $condition1 = array('resellerID'=>$resellerID);
                	
                 	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY', $subdomain);
                                define('OAUTH_CONSUMER_SECRET', $key);
                                define('OAUTH_CALLBACK', $URL1);
					 
					
					  
                    if(isset($accessToken) && isset($access_token_secret))
                    {
						
					$c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);
					
					$qblist='';
				           if(!empty($invoiceIDs))
				           {
				            
				              foreach($invoiceIDs as $inID)
				              {
								  
								  $con = array('invoiceID'=>$inID,'merchantID'=>$merchantID);
            									$res = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining'), $con);
            								
            									$amount_data = $res['BalanceRemaining'];
								                $invID       = $inID;
								  
								  $payment = '<?xml version="1.0" encoding="utf-8"?>  
												<request method="payment.create">  
												  <payment>         
													<invoice_id>'.$invID.'</invoice_id>               
													<amount>'.$amount_data.'</amount>             
													<currency_code>USD</currency_code> 
													<type>Check</type>                   
												  </payment>  
												</request>';
									  $invoices = $c->add_payment($payment);
									 
						
									if ($invoices['status'] != 'ok') {
						
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
						
										redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
										
									}
									else {
									       $inv_array['inv_no'][]     = $invID;
    									    $inv_array['inv_amount'][]= $amount_data;
    									     $inv_invoice[]           =   $invID;
    										$pinv_id                  =  $invoices['payment_id'];
    										$qblist[]                 = $pinv_id;
									    
										$this->session->set_flashdata('success','Transaction Successful'); 
									}
								}			
						   }
					}	
				 
		       /********************end fresh book code*************************/
			   
				 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
				 {
				     
				        $this->load->library('encrypt');
			
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
						 $cardType       = $this->general_model->getType($card_no);
						 $friendlyname   =  $cardType.' - '.substr($card_no,-4);		
						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
			
			$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					   
				  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$customerListID = $this->czsecurity->xssCleanPostInput('customerID');
							$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
					    	if($checkCustomerCard == 0){
					    		$is_default = 1;
					    	}
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'cardType'    =>$this->general_model->getType($card_no),
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',  
										 'customerListID' =>$customerListID, 
										 'companyID'     =>0,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
							
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
                    
						
						}
				
				 }
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$result['responsetext'].'</strong>.</div>'); 
				 }
				 
				      $transactiondata= array();
					   if(isset($result->id)){
						$transactiondata['transactionID']       = $result->id;
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					    $transactiondata['transactionStatus']    = $result->status;
					    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionCode']     = $code;  
					  
						$transactiondata['transactionType']    = 'stripe_sale';	
						$transactiondata['gatewayID']          = $gatlistval;
                        $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					    $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					    $transactiondata['transactionAmount']   = ($amount/100);
					    $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
    				    $transactiondata['gateway']   = "Stripe";
    				    if(!empty($invoiceIDs) && !empty($inv_array))
				        {
					      $transactiondata['invoiceID']            = implode(',',$inv_invoice);
					      $transactiondata['invoiceRefID']         = json_encode($inv_array);
					      if(!empty($qblist))
					      $transactiondata['qbListTxnID']          = implode(',',$qblist); 
				        } 
					  	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
    					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']); 	
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
            
           
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}	
			
           }
		   $invoice_IDs = array();
		   if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			   $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
		   }
	   
		   $receipt_data = array(
			   'transaction_id' => $result->id,
			   'IP_address' => getClientIpAddr(),
			   'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			   'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			   'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			   'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			   'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			   'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			   'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			   'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			   'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			   'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			   'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			   'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			   'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			   'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			   'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			   'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			   'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
			   'proccess_btn_text' => 'Process New Sale',
			   'sub_header' => 'Sale',
		   );
		   
		   $this->session->set_userdata("receipt_data",$receipt_data);
		   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
		   
		   
		   redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
				    
	   }
	 
	 
	 
	 
	
	public function create_customer_auth()
	{
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		 			   	
	   	    	  
        $checkPlan = check_free_plan_transactions();
		 
		if($checkPlan && !empty($this->input->post(null, true))){
			
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  
			if($gatlistval !="" && !empty($gt_result))
			{
				
			 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
		
			$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
						
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
				     	$card     = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						 if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
					
						$cvv    = $this->czsecurity->xssCleanPostInput('cvv');
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];							
							$exyear   = $card_data['cardYear'];
							$exyear   = $exyear;
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							
							$cvv      = $card_data['CardCVV'];
					}
            
        
					$amount =  (int)($this->czsecurity->xssCleanPostInput('totalamount')*100);
				
					
					$plugin = new ChargezoomStripe();
				    $plugin->setApiKey($gt_result['gatewayPassword']);
					$res = \Stripe\Token::create([
        									'card' => [
        									'number' =>$card,
        									'exp_month' => $expmonth,
        									'exp_year' =>  $exyear,
        									'cvc' => $cvv
        								   ]
        						]);
					$tcharge= json_encode($res);  
        			$rest = json_decode($tcharge);
        			
        			if($rest->id){
        				$token = $rest->id; 
        			}else{
        				$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
        			}
					$charge =	\Stripe\Charge::create(array(
						  "amount" => $amount,
						  "currency" => "usd",
						  "source" => $token, // obtained with Stripe.js
						  "description" => "Charge for test Account",
						  'capture'     => 'false' 
						));	
               
			       $charge= json_encode($charge);
				   $result = json_decode($charge);
				   
				 $trID='';
				 if($result->paid=='1' && $result->failure_code==""){
				  $code ='200';
				  $trID = $result->id;
				 /* This block is created for saving Card info in encrypted form  */
		       
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error '.$result['responsetext'].'</strong>.</div>'); 
				 }
				 
				       $transactiondata= array();
				       $transactiondata['transactionID']       = $trID;
					   $transactiondata['transactionStatus']    = $result->status;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $code;  
					  
						$transactiondata['transactionType']    = 'stripe_auth';	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = ($amount/100);
					   $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
    				      $transactiondata['gateway']   = "Stripe";
    				      
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}	
			
				    
           }
		   $invoice_IDs = array();
		   
	   
				$receipt_data = array(
					'transaction_id' => $result->id,
					'IP_address' => getClientIpAddr(),
					'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
					'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
					'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
					'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
					'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
					'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
					'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
					'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
					'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
					'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
					'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
					'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
					'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
					'Contact' => $this->czsecurity->xssCleanPostInput('email'),
					'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_auth',
					'proccess_btn_text' => 'Process New Transaction',
					'sub_header' => 'Authorize',
					'checkPlan'  => $checkPlan
				);
				
				$this->session->set_userdata("receipt_data",$receipt_data);
				$this->session->set_userdata("invoice_IDs",$invoice_IDs);
				
				
				redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
	    
	}
	 
	/*****************Capture Transaction***************/
	
	public function create_customer_capture()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
				 $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
    			 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			   if( $paydata['gatewayID'] > 0){ 
			       
			       $merchantID = $this->session->userdata('logged_in')['merchID']; 
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 
				

				$plugin = new ChargezoomStripe();
				$plugin->setApiKey($gt_result['gatewayPassword']);
				$ch = \Stripe\Charge::retrieve($tID);
				$charge = $ch->capture();
								 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				
			      $charge= json_encode($charge);
				  
				   $result = json_decode($charge);
				
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded')){  
				     
				$amount = ($result->amount/100) ;
				 $code  ='200';
			    $trID   = $result->id;
				$condition = array('transactionID'=>$tID);
				$update_data =   array('transaction_user_status'=>"4");
				$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
				$this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 $code =  $result->failure_code; 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$result['responsetext'].'</strong>.</div>'); 
				     
					 }
					 
					   $transactiondata= array();
				       $transactiondata['transactionID']      =  $trID;
					   $transactiondata['transactionStatus']  = $result->status;;
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionType']    = 'stripe_capture';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					    $transactiondata['merchantID']  =  $merchantID;
					     $transactiondata['resellerID']   = $this->resellerID;
    				      $transactiondata['gateway']   = "Stripe";
    				    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']); 	 
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				     
					 }	
					 $invoice_IDs = array();
					
				 
					 $receipt_data = array(
						 'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
						 'proccess_btn_text' => 'Process New Transaction',
						 'sub_header' => 'Capture',
					 );
					 
					 $this->session->set_userdata("receipt_data",$receipt_data);
					 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					 if($paydata['invoiceTxnID'] == ''){
						 $paydata['invoiceTxnID'] ='null';
						 }
						 if($paydata['customerListID'] == ''){
							 $paydata['customerListID'] ='null';
						 }
						 if($result->id == ''){
							 $result->id = 'null';
						 }
					 
					 redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->id,  'refresh');
				
        }
              

	
	}
	
	 
	public function create_customer_refund()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
			     
			     if($this->czsecurity->xssCleanPostInput('txnred')){
			     $txnred  = $this->czsecurity->xssCleanPostInput('txnred');
	         	}else{
	         	    $txnred ='';
	         	}
			     $tID     = $this->czsecurity->xssCleanPostInput('txnstrID');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			    
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   $amount     = $this->czsecurity->xssCleanPostInput('ref_amount');
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 

				$plugin = new ChargezoomStripe();
				$plugin->setApiKey($gt_result['gatewayPassword']);
				$charge = \Stripe\Refund::create(array(
				  "charge" => $tID,
				  "amount"=>($amount*100)
				));
			  
				 
				 $customerID = $paydata['customerListID'];
			     $merchantID     =$paydata['merchantID'] ;
				
			      $charge= json_encode($charge);
				  
				   $result = json_decode($charge);
			
				   
				  
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded'))
				 {  
				     
				$amount = ($result->amount/100) ;
				 $code ='200';
			    $trID = $result->id;
			     $val = array(
    					'merchantID' => $paydata['merchantID'],
    				);
				
    				$merchID =$paydata['merchantID'];
        		
    		
            		$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
               
                	 	$redID = $this->resellerID;
                	$condition1 = array('resellerID'=>$redID);
                	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        						 $domain1 = $sub1['merchantPortalURL'];
        						 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth'; 
                    
                    $subdomain     = $Freshbooks_data['sub_domain'];
                    $key           = $Freshbooks_data['secretKey'];
                    $accessToken   = $Freshbooks_data['accessToken'];
                    $access_token_secret = $Freshbooks_data['oauth_token_secret'];
             
                    define('OAUTH_CONSUMER_KEY', $subdomain);
                    define('OAUTH_CONSUMER_SECRET', $key);
                    define('OAUTH_CALLBACK', $URL1);
			   		  	  
				 if(!empty($paydata['invoiceID']))
				{
				     $refund =$amount;
				    $ref_inv =explode(',',$paydata['invoiceID']);
				    if(count($ref_inv)>1)
				    {
				      $ref_inv_amount = json_decode($paydata['invoiceRefID']);
				      $imn_amount =  $ref_inv_amount->inv_amount; 
				      $p_ids     =   explode(',',$paydata['qbListTxnID']);
				     
				      $inv_array = array();
				      foreach($ref_inv as $k=> $r_inv)
				      {
				            $inv_data=$this->general_model->get_select_data('Freshbooks_test_invoice',array('DueDate'),array('merchantID'=>$merchID,'invoiceID'=>$r_inv));
				          
				          if(!empty($inv_data))
				          {
				           $due_date=   date('Y-m-d',strtotime($inv_data['DueDate']));
				          }else{
				               $due_date=   date('Y-m-d');  
				          }
				          
				          $re_amount = $imn_amount[$k];
				          if($refund > 0 && $imn_amount[$k] >0)
				          {
				              
				              
				            $p_refund1=0;  
				              
				            $p_id  =  $p_ids[$k];
				              
				          	$ittem =  $this->fb_customer_model->get_invoice_item_data($r_inv);
				           
				            if($refund <= $imn_amount[$k] )
				            {
				                $p_refund1 =  $refund;
				                  
				                  
				                   $p_refund =   $imn_amount[$k] -$refund;
				                 
				                  $re_amount =$imn_amount[$k] -$refund;
				                  $refund =0;
				               
				            }
				            else
				            {
				                 $p_refund =0;
				                 $p_refund1 =  $imn_amount[$k];
				                 $refund=$refund-$imn_amount[$k];
				                 
				                 $re_amount =0;
				            }
				            
        				               
                            if(isset($accessToken) && isset($access_token_secret))
                            {
                                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                           
                              $payment = '<?xml version="1.0" encoding="utf-8"?>  
                    						<request method="payment.update">  
                    						  <payment>         
                    						    
                                                    <payment_id>'.$p_id.'</payment_id>
                                                    <amount>'.$p_refund.'</amount>
                                                    <notes>Payment refund for invoice:'.$r_inv.' '.$p_refund1.' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>              
                    						  
                    						</request>';
                                  
                                  $invoices = $c->edit_payment($payment);
                                 
                             
                               
                        			if ($invoices['status'] != 'ok') 
                        			{
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        
                        			
                        				
                        			}
                        			else {
                        			        $ins_id = $p_id;
                                            $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$p_refund1,
            				  	           'creditInvoiceID'=>$r_inv,'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           );	
            				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
            				  	        $lineArray='';
            				 
                            			  foreach($ittem as $item_val)
                            			  {
                            					
                            				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
                        					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
                        					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
                        					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
                        					$lineArray .= '<type>Item</type></line>';  
                            					
                            			  }		
                            					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
                        					$lineArray .= '<description>This is used to refund Incoive payment</description>';
                        					$lineArray .= '<unit_cost>'.(-$p_refund1).'</unit_cost>';
                        					$lineArray .= '<quantity>1</quantity>';
                        					$lineArray .= '<type>Item</type></line>'; 	
            					
            					
            					
                        				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update"> 
                                     		 <invoice> 
                                     		  <invoice_id>'.$r_inv.'</invoice_id> 
                                     		   <date>'.$due_date.'</date>
                        				          <lines>'.$lineArray.'</lines>  
                        						</invoice>  
                        						</request> ';
                        		           
                                             $invoices1 = $c->add_invoices($invoice);
                        				  	          
            				  	     
                        				$this->session->set_flashdata('success','Successfully Refunded Payment'); 
                        				
                        				
                        			}
                    			
                                
                            }	
                            
                            
                            $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount;
        				           
				           }else{
				               
				                $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount; 
				           }  
				            
				            
				          
				      }
				       $this->general_model->update_row_data('customer_transaction',$con,array('invoiceRefID'=>json_encode($inv_array)));
				     }
				     else
				     {
				  
        			$ittem =  $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);
    		
                    $p_id     = $paydata['qbListTxnID'];
                    $p_amount = $paydata['transactionAmount']-$amount; 
                    
                    if(isset($accessToken) && isset($access_token_secret))
                    {
                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                   
                      $payment = '<?xml version="1.0" encoding="utf-8"?>  
            						<request method="payment.update">  
            						  <payment>         
            						    
                                            <payment_id>'.$p_id.'</payment_id>
                                            <amount>'.$p_amount.'</amount>
                                            <notes>Payment refund for invoice:'.$paydata['invoiceID'].' '.$amount.' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>              
            						  
            						</request>';
                          
                          $invoices = $c->edit_payment($payment);
                         
                     
                       
                			if ($invoices['status'] != 'ok') 
                			{
                
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                
                			
                				
                			}
                			else {
                			        $ins_id = $p_id;
                                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$amount,
    				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
    				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
    				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
    				  	           );	
    				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
    				  	        $lineArray='';
    				 
    			  foreach($ittem as $item_val)
    			  {
    					
    				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
					$lineArray .= '<type>Item</type></line>';  
    					
    			  }		
    					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
					$lineArray .= '<description>This is used to refund Incoive payment</description>';
					$lineArray .= '<unit_cost>'.(-$amount).'</unit_cost>';
					$lineArray .= '<quantity>1</quantity>';
					$lineArray .= '<type>Item</type></line>'; 	
    					
    					
    					
    				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update"> 
                 		 <invoice> 
                 		  <invoice_id>'.$paydata['invoiceID'].'</invoice_id> 
                 		  
    				          <lines>'.$lineArray.'</lines>  
    						</invoice>  
    						</request> ';
    		           
                         $invoices1 = $c->add_invoices($invoice);
    				  	          
    				  	     
                				$this->session->set_flashdata('success','Successfully Refunded Payment'); 
                				
                			
                			}
            			}			
				     }	   
					   
    		  
             }
			    
				$this->fb_customer_model->update_refund_payment($tID,'STRIPE');
					
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>'); 
				 }
				 if($txnred == 'void'){
				            $ttype = 'stripe_void';
				        }
				        else{
				            $ttype = 'stripe_refund';
				        }
				        
				       $transactiondata= array();
				       $transactiondata['transactionID']      = $result->id;
					   $transactiondata['transactionStatus']  =  $result->status;
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionType']    = $ttype;
					    $transactiondata['transactionCode']   = $code;
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					   $transactiondata['merchantID']  = $merchantID;
					    if(!empty($paydata['invoiceID']))
    			     	{
    				      $transactiondata['invoiceID']  = $paydata['invoiceID'];
    			     	}
					    $transactiondata['resellerID']   = $this->resellerID;
    				      $transactiondata['gateway']   = "Stripe";
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				if($txnred == 'void'){
				    	redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
				}
				else{
					$invoice_IDs = array();
					
				
					$receipt_data = array(
						'proccess_url' => 'FreshBooks_controllers/Transactions/payment_refund',
						'proccess_btn_text' => 'Process New Refund',
						'sub_header' => 'Refund',
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($result->id == ''){
						$result->id = 'null';
					}
					redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->id,  'refresh');
				    	
				}
			
				
        }
              

	    
	}
	
	 
	 
	 
		 
    public function pay_multi_invoice()
    {
        
    
	        $token='';
	        if($this->session->userdata('logged_in') )
	        {
	           $merchID = $this->session->userdata('logged_in')['merchID'];
	        	}
	        	if($this->session->userdata('user_logged_in') ){
	               $merchID = $this->session->userdata('user_logged_in')['merchantID'];
	        	} 
	        $merchantID  = $merchID;
			        	
			$rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$merchID));
			 $resellerID =	$rs_daata['resellerID'];
			 $condition1 = array('resellerID'=>$resellerID);
        	
           	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			 $domain1 = $sub1['merchantPortalURL'];
			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth'; 	
			 
			 	$val = array(
					'merchantID' => $merchID,
				);

				$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
            
            $subdomain     = $Freshbooks_data['sub_domain'];
            $key           = $Freshbooks_data['secretKey'];
            $accessToken   = $Freshbooks_data['accessToken'];
            $access_token_secret = $Freshbooks_data['oauth_token_secret'];
     
            define('OAUTH_CONSUMER_KEY', $subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
	        $invoicep_id='';	
	 	 
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
		 
		 if($this->czsecurity->xssCleanPostInput('qbo_check') == 'qbo_pay'){
		 
		 	
			$checkPlan = check_free_plan_transactions();

          
         $token_array = $this->czsecurity->xssCleanPostInput('stripeToken');
       if($checkPlan && !empty($cardID) && !empty($gateway) )
       {  
           $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
         	$invices = $this->czsecurity->xssCleanPostInput('multi_inv');
         if(!empty($invices))
         { 	
         	foreach($invices as $k=> $invoiceID)
         	{
         	  $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
         	    
		 	  $in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
               $token    = $token_array[$k];
	    	if(!empty($in_data)){ 
		  
            $Customer_ListID = $in_data['CustomerListID'];
              if($cardID=='new1')
              {
                        	
			            	$cardID_upd  =$cardID;
			        		$c_data      = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID));
			        	$companyID       = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);

				        $merchantID = $merchID;
          
           
                         $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID' => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                           $this->db1->select('*')->from('customer_card_data');
                           $qq =  $this->db1->get();
          
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                        
                         }else{
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
					    	if($checkCustomerCard == 0){
					    		$is_default = 1;
					    	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
        
		       $card_data    =   $this->card_model->get_single_card_data($cardID);

		      if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
				$cr_amount = 0;
				$amount    =	 $in_data['BalanceRemaining'];
		    	$amount    =	$pay_amounts;
				
				     
				     
                     $amount    =	$pay_amounts;
				 	 $amount    = $amount-$cr_amount;
					        
		        	 $amount1 =  (int)($amount*100);

			$plugin = new ChargezoomStripe();
			$plugin->setApiKey($gt_result['gatewayPassword']);   	 
		 	
			$charge =	\Stripe\Charge::create(array(
				  "amount" => $amount1,
				  "currency" => "usd",
				  "source" => $token, // obtained with Stripe.js
				  "description" => "Charge Using Stripe Gateway",
				 
				));	

			 $charge= json_encode($charge);
			 $result = json_decode($charge);
         
			  if($result->paid=='1' && $result->failure_code==""){
			  	 $code		 =  '200';
	
        
        if(isset($accessToken) && isset($access_token_secret))
        {
            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);

            $payment = '<?xml version="1.0" encoding="utf-8"?>  
						<request method="payment.create">  
						  <payment>         
						    <invoice_id>'.$invoiceID.'</invoice_id>               
						    <amount>'.$amount.'</amount>             
						    <currency_code>USD</currency_code> 
						    <type>Check</type>                   
						  </payment>  
						</request>';
             
              $invoices = $c->add_payment($payment);
           
			if ($invoices['status'] != 'ok') {
			
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>');

				redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
				
			}
			else {
			     $invoicep_id = $invoices['payment_id'];
				$this->session->set_flashdata('success','Success'); 
				
				
			}
		}
				}
				else{
					 $code =  $result->failure_code;
			       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error!  "'.$result->status.'"</strong>.</div>'); 
						}

				 	   $transaction['transactionID']      = $result->id;
					   $transaction['transactionStatus']    = $result->status;
					   $transaction['transactionDate']     = date('Y-m-d H:i:s');  
					    $transaction['transactionModified']     = date('Y-m-d H:i:s');  
					   $transaction['transactionCode']         = $code;  
					  
						$transaction['transactionType']    = 'stripe_sale';	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					    $transaction['customerListID']     = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   = ($result->amount/100);
					  $transaction['merchantID']   = $merchID;
					  $transaction['invoiceID']   = $invoiceID;
					     $transaction['qbListTxnID'] =$invoicep_id  ;
                        $transaction['resellerID']   = $resellerID;
    				      $transaction['gateway']   = "Stripe";
					  $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 	
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);

					}
				 else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
				}
          
		     }
		     else
		     {
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}
	    	else
	    	{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
         	}
         }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select invoices</strong>.</div>'); 
			 }
			 
          }else{
			  $ermsg='';
       if($cardID=="" )
       $ermsg ="Card is required";
       if($gateway=="")
       $ermsg ="Gateway is required";
       if($token=="" )
        $ermsg ="Stripe token is required";
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$ermsg.'</div>'); 
		  }

		if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
			
			 
		    redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
		}
    
    }     

	
}  