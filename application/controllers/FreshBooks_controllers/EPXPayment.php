<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */

include APPPATH . 'third_party/EPX.class.php';

class EPXPayment extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';

        $this->load->config('EPX');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->library('form_validation');
        $this->load->model('Freshbook_models/freshbooks_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $merchID;
        } else {
            redirect('login', 'refresh');
        }

        $val = array(
            'merchantID' => $this->merchantID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
        if (empty($Freshbooks_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
            redirect('FreshBoolks_controllers/home/index', 'refresh');
        }

        $this->subdomain           = $Freshbooks_data['sub_domain'];
        $key                       = $Freshbooks_data['secretKey'];
        $this->accessToken         = $Freshbooks_data['accessToken'];
        $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
        $this->fb_account          = $Freshbooks_data['accountType'];
        $condition1                = array('resellerID' => $this->resellerID);
        $sub1                      = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1                   = $sub1['merchantPortalURL'];
        $URL1                      = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        define('OAUTH_CONSUMER_KEY', $this->subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        redirect('FreshBooks_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");

        $cusproID = array();
        $error    = array();
        $cusproID = $this->input->post('customerProcessID');

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $this->form_validation->run() == true) {

            $merchID    = $this->merchantID;
            $user_id    = $merchID;
            $resellerID = $this->resellerID;
            $invoiceID  = $this->input->post('invoiceProcessID');

            $cardID = $this->input->post('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->input->post('schCardID');
            }

            $gatlistval = $this->input->post('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->input->post('gateway');
            }
            $responseId = $transactionID = 'TXNFAILED'.time();
            $gateway    = $gatlistval;
            $sch_method = $this->input->post('sch_method');

            $amount   = $this->input->post('inv_amount');
            $chh_mail = 0;
            $in_data  = $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

            if (!empty($in_data)) {

                $customerID = $in_data['Customer_ListID'];

                $companyID = $in_data['companyID'];
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

               

                if (!empty($cardID)) {
                    if ($in_data['BalanceRemaining'] > 0) {

                        $cusproID = 1;
                        $pinv_id  = '';

                        $name = $in_data['Customer_FullName'];
                        $isCheck = false;

                        $CUST_NBR = $gt_result['gatewayUsername'];
                        $MERCH_NBR = $gt_result['gatewayPassword'];
                        $DBA_NBR = $gt_result['gatewaySignature'];
                        $TERMINAL_NBR = $gt_result['extra_field_1'];
                        $orderId = time();
                        
                        $amount = number_format($amount,2,'.','');
                        $transaction = array(
                                'CUST_NBR' => $CUST_NBR,
                                'MERCH_NBR' => $MERCH_NBR,
                                'DBA_NBR' => $DBA_NBR,
                                'TERMINAL_NBR' => $TERMINAL_NBR,
                                'AMOUNT' => $amount,
                                'TRAN_NBR' => rand(1,10),
                                'BATCH_ID' => time(),
                                'VERBOSE_RESPONSE' => 'Y',
                        );

                        
                        if ($sch_method == "1") {
                            $isCheck = false;

                            if ($cardID != "new1") {

                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];

                            } else {

                                $card_no  = $this->input->post('card_number');
                                $expmonth = $this->input->post('expiry');
                                $exyear   = $this->input->post('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->input->post('cvv') != "") {
                                    $cvv = $this->input->post('cvv');
                                }

                                $address1 = $this->input->post('address1');
                                $address2 = $this->input->post('address2');
                                $city     = $this->input->post('city');
                                $country  = $this->input->post('country');
                                $phone    = $this->input->post('phone');
                                $state    = $this->input->post('state');
                                $zipcode  = $this->input->post('zipcode');
                            }

                            $exyear1  = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $exyear1.$expmonth;
                            $transaction['EXP_DATE'] = $expry;
                            $transaction['ACCOUNT_NBR'] = $card_no;
                            $transaction['TRAN_TYPE'] = 'CCE1';
                            $transaction['CARD_ENT_METH'] = 'E';
                            $transaction['INDUSTRY_TYPE'] = 'E';

                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            if($zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }

                        } else if ($sch_method == "2") {
                            $isCheck = true;
                            $cardType = 'Check';
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->input->post('acc_name'),
                                    'accountNumber'      => $this->input->post('acc_number'),
                                    'routeNumber'        => $this->input->post('route_number'),
                                    'accountType'        => $this->input->post('acct_type'),
                                    'accountHolderType'  => $this->input->post('acct_holder_type'),
                                    'Billing_Addr1'      => $this->input->post('address1'),
                                    'Billing_Addr2'      => $this->input->post('address2'),
                                    'Billing_City'       => $this->input->post('city'),
                                    'Billing_Country'    => $this->input->post('country'),
                                    'Billing_Contact'    => $this->input->post('phone'),
                                    'Billing_State'      => $this->input->post('state'),
                                    'Billing_Zipcode'    => $this->input->post('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'merchantID'         => $user_id,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->input->post('secCode'),
                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }
                            $transaction['RECV_NAME'] = $accountDetails['accountName'];
                            $transaction['ACCOUNT_NBR'] = $accountDetails['accountNumber'];
                            $transaction['ROUTING_NBR'] = $accountDetails['routeNumber'];

                            if($accountDetails['accountType'] == 'savings'){
                                $transaction['TRAN_TYPE'] = 'CKS2';
                            }else{
                                $transaction['TRAN_TYPE'] = 'CKC2';
                            }
                            if($accountDetails['Billing_Addr1'] != ''){
                                $transaction['ADDRESS'] = $accountDetails['Billing_Addr1'];
                            }
                            if($accountDetails['Billing_City'] != ''){
                                $transaction['CITY'] = $accountDetails['Billing_City'];
                            }
                            if( $accountDetails['Billing_Zipcode'] != ''){
                                $transaction['ZIP_CODE'] = $accountDetails['Billing_Zipcode'];
                            }
                        }
                        $gatewayTransaction              = new EPX();
                        $result = $gatewayTransaction->processTransaction($transaction);

                        if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                        {
                            $responseId = $transactionID = $result['AUTH_GUID'];

                            $val = array(
                                'merchantID' => $merchID,
                            );

                            $ispaid  = 1;
                            $st      = 'paid';
                            $bamount = $in_data['BalanceRemaining'] - $amount;
                            if ($bamount > 0) {
                                $ispaid = '0';
                                $st     = 'unpaid';
                            }
                            $app_amount = $in_data['AppliedAmount'] + $amount;
                            $up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
                            $condition  = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);

                            $this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);

                            if (isset($this->accessToken) && isset($this->access_token_secret)) {
                                if ($this->fb_account == 1) {
                                    $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                                    $payment  = array('invoiceid' => $invoiceID, 'amount' => array('amount' => $amount), 'type' => 'Check', 'date' => date('Y-m-d'));
                                    $invoices = $c->add_payment($payment);
                                } else {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                                <request method="payment.create">
                                                <payment>
                                                    <invoice_id>' . $invoiceID . '</invoice_id>
                                                    <amount>' . $amount . '</amount>
                                                    <currency_code>USD</currency_code>
                                                    <type>Check</type>
                                                </payment>
                                                </request>';
                                    $invoices = $c->add_payment($payment);

                                }

                                if ($invoices['status'] != 'ok') {

                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                } else {
                                    $pinv_id = $invoices['payment_id'];

                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                }
                            }
                            $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                            $ref_number     = $in_data['refNumber'];
                            $tr_date        = date('Y-m-d H:i:s');
                            $toEmail        = $in_data['userEmail'];
                            $company        = $in_data['companyName'];
                            $customer       = $in_data['fullName'];
                            if ($chh_mail == '1') {

                                
                                $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                            }
                            

                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->input->post('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->input->post('expiry');
                                    $exyear   = $this->input->post('expiry_year');
                                    $cvv      = $this->input->post('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->input->post('address1'),
                                        'Billing_Addr2'   => $this->input->post('address2'),
                                        'Billing_City'    => $this->input->post('city'),
                                        'Billing_Country' => $this->input->post('country'),
                                        'Billing_Contact' => $this->input->post('phone'),
                                        'Billing_State'   => $this->input->post('state'),
                                        'Billing_Zipcode' => $this->input->post('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }

                        } else {
                            $err_msg      = $result['AUTH_RESP_TEXT'];
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                        }
                        $result['transactionid'] = $responseId;
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $pinv_id, $this->resellerID, $in_data['invoiceID'], $isCheck, $this->transactionByUser);

                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Validation Error</strong>.</div>');
        }

        if ($cusproID != "") {
            $trans_id     = $responseId;
            $invoice_IDs  = array();
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
                'proccess_btn_text' => 'Process New Invoice',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            $this->session->set_userdata("in_data", $in_data);
            if ($cusproID == "1") {
                redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');

            }

            redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $cusproID, 'refresh');
        } else {
            if(!$checkPlan){
                $responseId  = '';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
            }
            redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
        }

    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $user_id = $this->merchantID;
        $checkPlan = check_free_plan_transactions();
        if ($checkPlan && !empty($this->input->post())) {
            $this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
            $this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
            if ($this->input->post('cardID') == "new1") {
                $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
            }

            if ($this->form_validation->run() == true) {
                $custom_data_fields = [];
                // get custom field data
                if (!empty($this->input->post('invoice_id'))) {
                    $custom_data_fields['invoice_number'] = $this->input->post('invoice_id');
                }

                if (!empty($this->input->post('po_number'))) {
                    $custom_data_fields['po_number'] = $this->input->post('po_number');
                }
                $responseId = $transactionID = 'TXNFAILED'.time();
                $inputData = $this->input->post();
                $gateway   = $this->input->post('gateway_list');
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                if ($this->input->post('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                $invoiceIDs = array();
                if (!empty($this->input->post('invoice_id'))) {
                    $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                }

                $customerID = $this->input->post('customerID');

                $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
                $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
                $companyID = $comp_data['companyID'];
                $cardID    = $this->input->post('card_list');

                $cvv = '';
                if ($this->input->post('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');

                    $exyear = $this->input->post('expiry_year');
                    if ($this->input->post('cvv') != "") {
                        $cvv = $this->input->post('cvv');
                    }

                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    if ($card_data['CardCVV']) {
                        $cvv = $card_data['CardCVV'];
                    }

                    $cardType = $card_data['CardType'];
                    $address1 = $card_data['Billing_Addr1'];
                    $city     = $card_data['Billing_City'];
                    $zipcode  = $card_data['Billing_Zipcode'];
                    $state    = $card_data['Billing_State'];
                    $country  = $card_data['Billing_Country'];
                }

                $exyear1 = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $exyear1.$expmonth;

                $city     = $this->input->post('city');
                $country  = $this->input->post('country');
                $phone    = $this->input->post('phone');
                $state    = $this->input->post('state');
                $zipcode  = $this->input->post('zipcode');
                $name     = $this->input->post('firstName') . ' ' . $this->input->post('lastName');
                $address  = $this->input->post('address1') . ' ' . $this->input->post('address2');
                $address1 = $this->input->post('baddress1');
                $address2 = $this->input->post('baddress2');

                $amount = $this->input->post('totalamount');

                include APPPATH . 'libraries/Freshbooks_data.php';
                $fb_data = new Freshbooks_data($this->resellerID, $this->merchantID);
                $pinv_id = '';
                $calamount = $amount * 100;

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $orderId = time();
                $amount = number_format($amount,2,'.','');

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE1',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',
                );
                if($inputData['firstName'] != ''){
                    $transaction['FIRST_NAME'] = $inputData['firstName'];
                }
                if($inputData['lastName'] != ''){
                    $transaction['LAST_NAME'] = $inputData['lastName'];
                }
                if($cvv && !empty($cvv)){
                    $transaction['CVV2'] = $cvv;
                }
                if($inputData['baddress1'] != ''){
                    $transaction['ADDRESS'] = $inputData['baddress1'];
                }
                if($inputData['bcity'] != ''){
                    $transaction['CITY'] = $inputData['bcity'];
                }
                
                if($inputData['bzipcode'] != ''){
                    $transaction['ZIP_CODE'] = $inputData['bzipcode'];
                }
                if(!empty($this->input->post('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->input->post('invoice_id'), $merchantID, 2);
                    $transaction['ORDER_NBR'] = $new_invoice_number;
                }
                $crtxnID = '';
                $invID   = '';
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);

                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {

                    $message = $result['AUTH_RESP_TEXT'];
                    $responseId = $transactionID = $result['AUTH_GUID'];
                    
                    $invoiceIDs = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                    }

                    $refNumber = array();

                    if (!empty($invoiceIDs)) {

                        foreach ($invoiceIDs as $inID) {

                            $in_data     = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invocieID' => $inID));
                            $refNumber[] = $in_data['refNumber'];
                            $amount_data = $amount;
                            $pinv_id     = '';
                            $invoices    = $fb_data->create_invoice_payment($inID, $amount_data);
                            if ($invoices['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
                            } else {
                                $pinv_id = $invoices['payment_id'];

                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            }
                            $result['transactionid'] = $responseId;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                        }
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $result['transactionid'] = $responseId;
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                    if (!empty($refNumber)) {
                        $ref_number = implode(',', $refNumber);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['userEmail'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['fullName'];
                    if ($chh_mail == '1') {

                       
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                    }
                   

                    if ($cardID == "new1" && !($this->input->post('tc'))) {
                        $card_no   = $this->input->post('card_number');
                        $expmonth  = $this->input->post('expiry');
                        $exyear    = $this->input->post('expiry_year');
                        $cvv       = $this->input->post('cvv');
                        $card_type = $this->general_model->getType($card_no);

                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'CardType'                     => $card_type,
                            'CustomerCard'                 => $card_no,
                            'CardCVV'                      => $cvv,
                            'customerListID'               => $customerID,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $this->merchantID,

                            'createdAt'                    => date("Y-m-d H:i:s"),
                            'Billing_Addr1'                => $address1,
                            'Billing_Addr2'                => $address2,
                            'Billing_City'                 => $city,
                            'Billing_State'                => $state,
                            'Billing_Country'              => $country,
                            'Billing_Contact'              => $phone,
                            'Billing_Zipcode'              => $zipcode,
                        );

                        $id1 = $this->card_model->process_card($card_data);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg = $result['AUTH_RESP_TEXT'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                    $result['transactionid'] = $responseId;
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }

            } else {

                $error = 'Validation Error. Please fill the requred fields';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
            }

        }
        $invoice_IDs = array();
        if (!empty($this->input->post('invoice_id'))) {
            $invoice_IDs = explode(',', $this->input->post('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => $_SERVER['REMOTE_ADDR'],
            'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'billing_address1'  => $this->input->post('baddress1'),
            'billing_address2'  => $this->input->post('baddress2'),
            'billing_city'      => $this->input->post('bcity'),
            'billing_zip'       => $this->input->post('bzipcode'),
            'billing_state'     => $this->input->post('bstate'),
            'billing_country'   => $this->input->post('bcountry'),
            'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'shipping_address1' => $this->input->post('address1'),
            'shipping_address2' => $this->input->post('address2'),
            'shipping_city'     => $this->input->post('city'),
            'shipping_zip'      => $this->input->post('zipcode'),
            'shipping_state'    => $this->input->post('state'),
            'shiping_counry'    => $this->input->post('country'),
            'Phone'             => $this->input->post('phone'),
            'Contact'           => $this->input->post('email'),
            'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_auth()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");

        if (!empty($this->input->post())) {
            $responseId = 'TXNFAILED-'.time();
            $inputData  = $this->input->post();
            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $customerID = $this->input->post('customerID');
                $comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->input->post('customerID')));
                $companyID  = $comp_data['companyID'];

                $cardID = $this->input->post('card_list');

                if ($this->input->post('card_number') != "" && $cardID == 'new1') {

                    $card_no  = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');
                    $cvv      = $this->input->post('cvv');
                    $exyear   = $this->input->post('expiry_year');
                } else {
                    $cardID = $this->input->post('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $cvv       = $card_data['CardCVV'];
                    $exyear    = $card_data['cardYear'];
                }

                $exyear1 = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $exyear1.$expmonth;

                $name     = $this->input->post('fistName') . ' ' . $this->input->post('lastName');
                $address  = $this->input->post('address');
                $address1 = $this->input->post('baddress1');
                $address2 = $this->input->post('baddress2');
                $country  = $this->input->post('country');
                $city     = $this->input->post('city');
                $state    = $this->input->post('state');
                $amount   = $this->input->post('totalamount');
                $zipcode  = ($this->input->post('zipcode')) ? $this->input->post('zipcode') : '74035';
                $calamount = $amount * 100;

                $orderId = time();
                $amount = number_format($amount,2,'.','');
                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE2',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',
                );
                
                if($cvv != ''){
                    $transaction['CVV2'] = $cvv;
                }
                if($baddress1 != ''){
                    $transaction['ADDRESS'] = $baddress1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);

                $crtxnID = '';
                $invID = $inID = '';

                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    $responseId = $result['AUTH_GUID'];
                    if ($this->input->post('card_number') != "" && $cardID == "new1" && !($this->input->post('tc'))) {$cid = $this->input->post('customerID');
                        $card_no                        = $this->input->post('card_number');
                        $card_type                      = $this->general_model->getType($card_no);
                        $expmonth                       = $this->input->post('expiry');

                        $exyear = $this->input->post('expiry_year');

                        $cvv = $this->input->post('cvv');

                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'CardType'                     => $card_type,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $merchantID,
                            'customerListID'               => $this->input->post('customerID'),
                            'Billing_Addr1'                => $this->input->post('baddress1'),
                            'Billing_Addr2'                => $this->input->post('baddress2'),
                            'Billing_City'                 => $this->input->post('bcity'),
                            'Billing_Country'              => $this->input->post('bcountry'),
                            'Billing_Contact'              => $this->input->post('bphone'),
                            'Billing_State'                => $this->input->post('bstate'),
                            'Billing_Zipcode'              => $this->input->post('bzipcode'),
                            'CustomerCard'                 => $card_no,
                            'CardCVV'                      => $cvv,
                            'updatedAt'                    => date("Y-m-d H:i:s"),
                        );

                        $card = $this->card_model->process_card($card_data);

                    }

                    if ($chh_mail == '1') {

                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                        if (!empty($refNumber)) {
                            $ref_number = implode(',', $refNumber);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $comp_data['userEmail'];
                        $company  = $comp_data['companyName'];
                        $customer = $comp_data['fullName'];
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg = $result['AUTH_RESP_TEXT'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }
                $result['transactionid'] = $responseId;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser);

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>');
            }

            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'billing_address1'  => $this->input->post('baddress1'),
                'billing_address2'  => $this->input->post('baddress2'),
                'billing_city'      => $this->input->post('bcity'),
                'billing_zip'       => $this->input->post('bzipcode'),
                'billing_state'     => $this->input->post('bstate'),
                'billing_country'   => $this->input->post('bcountry'),
                'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'shipping_address1' => $this->input->post('address1'),
                'shipping_address2' => $this->input->post('address2'),
                'shipping_city'     => $this->input->post('city'),
                'shipping_zip'      => $this->input->post('zipcode'),
                'shipping_state'    => $this->input->post('state'),
                'shiping_counry'    => $this->input->post('country'),
                'Phone'             => $this->input->post('phone'),
                'Contact'           => $this->input->post('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');

           
        }

    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();

            $amount = $this->input->post('totalamount');
            $responseId = 'TXNFAILED-'.time();
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $customerID = $this->input->post('customerID');

                $comp_data = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->input->post('customerID')));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->input->post('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->input->post('account_name'),
                        'accountNumber'      => $this->input->post('account_number'),
                        'routeNumber'        => $this->input->post('route_number'),
                        'accountType'        => $this->input->post('acct_type'),
                        'accountHolderType'  => $this->input->post('acct_holder_type'),
                        'Billing_Addr1'      => $this->input->post('baddress1'),
                        'Billing_Addr2'      => $this->input->post('baddress2'),
                        'Billing_City'       => $this->input->post('bcity'),
                        'Billing_Country'    => $this->input->post('bcountry'),
                        'Billing_Contact'    => $this->input->post('phone'),
                        'Billing_State'      => $this->input->post('bstate'),
                        'Billing_Zipcode'    => $this->input->post('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $calamount = $amount * 100;

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $amount = number_format($amount,2,'.','');
                if($accountDetails['accountType'] == 'savings'){
                    $txnType = 'CKS2'; 
                }else{
                    $txnType = 'CKC2';
                }
                $orderId = time();


                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => $txnType,
                    'ACCOUNT_NBR' => $accountDetails['accountNumber'],
                    'ROUTING_NBR' => $accountDetails['routeNumber'],
                    'RECV_NAME' => $accountDetails['accountName'],
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',

                );
                if($firstName != ''){
                    $transaction['FIRST_NAME'] = $firstName;
                }
                if($lastName != ''){
                    $transaction['LAST_NAME'] = $lastName;
                }
                if(!empty($this->input->post('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->input->post('invoice_id'), $merchantID, 2);
                    $transaction['ORDER_NBR'] = $new_invoice_number;
                }
                if($address1 != ''){
                    $transaction['ADDRESS'] = $address1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
             
                $gatewayTransaction = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                $invoiceIDs = [];

                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    $responseId = $result['AUTH_GUID'];

                    $invoiceIDs = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                    }

                    $refNumber = array();

                    if (!empty($invoiceIDs)) {

                        foreach ($invoiceIDs as $inID) {

                            $in_data     = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invocieID' => $inID));
                            $refNumber[] = $in_data['refNumber'];
                            $amount_data = $amount;
                            $pinv_id     = '';
                            $invoices    = $fb_data->create_invoice_payment($inID, $amount_data);
                            if ($invoices['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
                            } else {
                                $pinv_id = $invoices['payment_id'];

                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            }
                            $result['transactionid'] = $responseId;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser);
                        }
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $result['transactionid'] = $responseId;
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);
                    }

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $result['transactionid'] = $responseId;
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['AUTH_RESP_TEXT'] . '</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->input->post('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => isset($responseId) ? $responseId : '',
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'billing_address1'  => $this->input->post('baddress1'),
                'billing_address2'  => $this->input->post('baddress2'),
                'billing_city'      => $this->input->post('bcity'),
                'billing_zip'       => $this->input->post('bzipcode'),
                'billing_state'     => $this->input->post('bstate'),
                'billing_country'   => $this->input->post('bcountry'),
                'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'shipping_address1' => $this->input->post('address1'),
                'shipping_address2' => $this->input->post('address2'),
                'shipping_city'     => $this->input->post('city'),
                'shipping_zip'      => $this->input->post('zipcode'),
                'shipping_state'    => $this->input->post('state'),
                'shiping_counry'    => $this->input->post('country'),
                'Phone'             => $this->input->post('phone'),
                'Contact'           => $this->input->post('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('FreshBooks_controllers/Transactions/create_customer_esale', 'refresh');
    }

    
    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->input->post('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }
            $transactionid = $transactionID = 'TXNFAILED'.time();
            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $CUST_NBR = $gt_result['gatewayUsername'];
            $MERCH_NBR = $gt_result['gatewayPassword'];
            $DBA_NBR = $gt_result['gatewaySignature'];
            $TERMINAL_NBR = $gt_result['extra_field_1'];
            $orderId = time();
            $amount = number_format($amount,2,'.','');

            $transaction = array(
                'CUST_NBR' => $CUST_NBR,
                'MERCH_NBR' => $MERCH_NBR,
                'DBA_NBR' => $DBA_NBR,
                'TERMINAL_NBR' => $TERMINAL_NBR,
                'ORIG_AUTH_GUID' => $tID,
                'TRAN_TYPE' => 'CCE4',
                'CARD_ENT_METH' => 'Z',
                'INDUSTRY_TYPE' => 'E',
                'AMOUNT' => $amount,
                'TRAN_NBR' => rand(1,10),
                'INVOICE_NBR' => $orderId,
                'BATCH_ID' => time(),
                'VERBOSE_RESPONSE' => 'Y',
            );
            $gatewayTransaction              = new EPX();
            $result = $gatewayTransaction->processTransaction($transaction);
            
            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
            {
                $transactionid = $transactionID = $result['AUTH_GUID'];

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {

                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                }
                
                $this->session->set_flashdata('success', 'Successfully Captured Authorization');
            } else {
                $err_msg      = $result['AUTH_RESP_TEXT'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }
            $result['transactionid'] = $transactionid;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser);

            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Transactions/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            
           

            redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $invoicp_id = '';
        $merchantID = $merchID;
        $user_id = $merchID;

        $rs_daata   = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
        $resellerID = $rs_daata['resellerID'];
        $condition1 = array('resellerID' => $resellerID);
        $responseId = $transactionid = 'TXNFAILED-'.time();

        $sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1 = $sub1['merchantPortalURL'];
        $URL1    = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        $val = array(
            'merchantID' => $merchID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);

        $subdomain           = $Freshbooks_data['sub_domain'];
        $key                 = $Freshbooks_data['secretKey'];
        $accessToken         = $Freshbooks_data['accessToken'];
        $access_token_secret = $Freshbooks_data['oauth_token_secret'];

        define('OAUTH_CONSUMER_KEY', $subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
        $cardID  = $this->input->post('CardID1');
        $gateway = $this->input->post('gateway1');

        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

        $apiUsername = $gt_result['gatewayUsername'];
        $apiKey      = $gt_result['gatewayPassword'];
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $cardID != "" || $gateway != "") {

            $invices = $this->input->post('multi_inv');
            if (!empty($invices)) {

                foreach ($invices as $invoiceID) {
                    $pay_amounts = $this->input->post('pay_amount' . $invoiceID);
                    $in_data     = $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->input->post('card_number');
                            $expmonth   = $this->input->post('expiry');
                            $exyear     = $this->input->post('expiry_year');
                            $cvv        = $this->input->post('cvv');
    
                            $address1 = $this->input->post('address1');
                            $address2 = $this->input->post('address2');
                            $city     = $this->input->post('city');
                            $country  = $this->input->post('country');
                            $phone    = $this->input->post('contact');
                            $state    = $this->input->post('state');
                            $zipcode  = $this->input->post('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }

                        if (!empty($card_data)) {

                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount   = $pay_amounts;
                            $amount   = $amount - $cr_amount;
                            $card_no  = $card_data['CardNo'];
                            $expmonth = $card_data['cardMonth'];

                            $exyear = $card_data['cardYear'];
                            $cvv = $card_data['CardCVV'];

                            $name    = $in_data['fullName'];
                            $address = $in_data['ShipAddress_Addr1'];
                           
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $exyear1 = substr($exyear, 2);
                            $expry = $exyear1.$expmonth;
                            $amount = number_format($amount,2,'.','');
                            $CUST_NBR = $gt_result['gatewayUsername'];
                            $MERCH_NBR = $gt_result['gatewayPassword'];
                            $DBA_NBR = $gt_result['gatewaySignature'];
                            $TERMINAL_NBR = $gt_result['extra_field_1'];
                            $orderId = time();
                            $transaction = array(
                                'CUST_NBR' => $CUST_NBR,
                                'MERCH_NBR' => $MERCH_NBR,
                                'DBA_NBR' => $DBA_NBR,
                                'TERMINAL_NBR' => $TERMINAL_NBR,
                                'TRAN_TYPE' => 'CCE1',
                                'ACCOUNT_NBR' => $card_no,
                                'EXP_DATE' => $expry,
                                'CARD_ENT_METH' => 'E',
                                'INDUSTRY_TYPE' => 'E',
                                'AMOUNT' => $amount,
                                'TRAN_NBR' => rand(1,10),
                                'INVOICE_NBR' => $invoiceID,
                                'ORDER_NBR' => $orderId,
                                'BATCH_ID' => time(),
                                'VERBOSE_RESPONSE' => 'Y',


                            );
                            if($c_data['firstName'] != ''){
                                $transaction['FIRST_NAME'] = $c_data['firstName'];
                            }
                            if($c_data['lastName'] != ''){
                                $transaction['LAST_NAME'] = $c_data['lastName'];
                            }
                            
                            if($cvv != ''){
                                $transaction['CVV2'] = $cvv;
                            }
                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            
                            if($zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }
                           
                            $gatewayTransaction              = new EPX();
                            $result = $gatewayTransaction->processTransaction($transaction);
                            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                            {
                                $responseId = $transactionid = $result['AUTH_GUID'];

                                if (isset($accessToken) && isset($access_token_secret)) {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $subdomain, $accessToken, $access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="payment.create">
                                        <payment>
                                            <invoice_id>' . $invoiceID . '</invoice_id>
                                            <amount>' . $amount . '</amount>
                                            <currency_code>USD</currency_code>
                                            <type>Check</type>
                                        </payment>
                                        </request>';

                                    $invoices = $c->add_payment($payment);
                                    if ($invoices['status'] != 'ok') {

                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

                                        redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');

                                    } else {
                                        $invoicp_id = $invoices['payment_id'];
                                        $this->session->set_flashdata('success', 'Success');

                                     
                                    }
                                }

                            } else {
                                $err_msg      = $result['AUTH_RESP_TEXT'];
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }
                            $result['transactionid'] = $transactionid;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $user_id, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser);

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
                    }
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Please select invoices</strong>.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');

    }

}
