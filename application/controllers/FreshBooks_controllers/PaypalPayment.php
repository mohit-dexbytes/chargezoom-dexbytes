<?php

/**
 * Paypal Payment Gateway Operations
 * Sale using create_customer_sale
 * Authorize  using create_customer_auth
 * Capture using create_customer_capture 
 * Void using create_customer_void
 * Refund create_customer_refund
 * Single Invoice Payment using pay_invoice
 * Multiple Invoice Payment using multi_pay_invoice
 * 
 */

class PaypalPayment extends CI_Controller
{
    
    private $resellerID;
    private $merchantID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
	    include APPPATH . 'third_party/Freshbooks.php';
	    include APPPATH . 'third_party/FreshbooksNew.php';
	    include APPPATH . 'third_party/PayPalAPINEW.php';
	    $this->load->config('paypal');
		$this->load->model('general_model');
		
		$this->load->model('card_model');
		$this->load->library('form_validation');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
      $this->db1 = $this->load->database('otherdb', true); 
		  if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='3')
		  {
		    $logged_in_data = $this->session->userdata('logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
			$this->merchantID = $logged_in_data['merchID'];
			$this->resellerID = $logged_in_data['resellerID'];
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 	$logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		    $this->merchantID = $merchID;
		 
		  }else{
			redirect('login','refresh');
		  }
		  
		   $val = array(
             'merchantID' => $this->merchantID,
             );
		  	$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		  	if(empty($Freshbooks_data))
		  	{
		  	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>'); 
                
		  	  redirect('FreshBoolks_controllers/home/index','refresh');
		  	  
		  	}
            
            $this->subdomain     = $Freshbooks_data['sub_domain'];
             $key                = $Freshbooks_data['secretKey'];
            $this->accessToken   = $Freshbooks_data['accessToken'];
            $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
            $this->fb_account    = $Freshbooks_data['accountType'];
            $condition1 = array('resellerID'=>$this->resellerID);
           	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			$domain1 = $sub1['merchantPortalURL'];
			$URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';  
            
            define('OAUTH_CONSUMER_KEY', $this->subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
	}
	
	
	public function index(){
	    
	
	   	redirect('FreshBooks_controllers/home','refresh'); 
	}
	
	  
	  
	   public function create_customer_sale_old()
	   {
	       	
		   if(!empty($this->input->post(null, true))){
					$inv_array  = array();
		            	$inv_invoice= array();
		            		 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                    }
                    $country  = "US";
                    $customerID=$this->czsecurity->xssCleanPostInput('customerID');
                    $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
                    $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                    if($this->czsecurity->xssCleanPostInput('setMail'))
                    $chh_mail =1;
                    else
                    $chh_mail =0; 
			  
			   if($gatlistval !="" && !empty($gt_result) )
			   {
					 $resellerID= $this->resellerID;
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);			
					
					
					
					
					
			
					$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
	    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
	    
	       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                      $card_no 	= $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth  	= $this->czsecurity->xssCleanPostInput('expiry');
						$card_type  = $this->general_model->getType($card_no); 
						$exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv        = $this->czsecurity->xssCleanPostInput('cvv');
                    
				 }else {
					  
						   
						    $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
							$card_type = $card_data['CardType'];
					    	$expmonth =  $card_data['cardMonth']; 
							$exyear   = $card_data['cardYear'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$cvv     = $card_data['CardCVV'];

					}	
	    
	    
	    				 
					$paymentType 		=	'Sale';
					$companyName        = $this->czsecurity->xssCleanPostInput('companyName');
					$firstName 			= $this->czsecurity->xssCleanPostInput('fistName');
					$lastName 			= $this->czsecurity->xssCleanPostInput('lastName');
					$creditCardType 	= ($card_type)?$card_type:'Visa';
				
					$creditCardNumber 	= $card_no;
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                    
					$expDateYear=  	$exyear;
					$cvv2Number =   $cvv;
					$address1	=   $this->czsecurity->xssCleanPostInput('address');
					$address2 	= '';
					$city 		=   $this->czsecurity->xssCleanPostInput('city');
					$state 		= $this->czsecurity->xssCleanPostInput('state');
					$zip 		= $this->czsecurity->xssCleanPostInput('zipcode');
					$email      = $this->czsecurity->xssCleanPostInput('email');
					$phone      = $this->czsecurity->xssCleanPostInput('phone');
					 $currencyID = 'USD';
					if($this->czsecurity->xssCleanPostInput('country')=="United States" ||$this->czsecurity->xssCleanPostInput('country')=="United State"){
					  $country 	= 'US';	// US or other valid country code
					  $currencyID = 'USD';
					}
					
					if($this->czsecurity->xssCleanPostInput('country')=="Canada" ||$this->czsecurity->xssCleanPostInput('country')=="canada"){
					  $country 	  = 'CAD';	// US or other valid country code
					  $currencyID = 'CAD';
					}
					
					$amount 	= $this->czsecurity->xssCleanPostInput('totalamount');	//actual amount should be substituted here
					// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
				       	  
                      $invoiceIDs='';
				          if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
				           {
				               $invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				            
				           }
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
									
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				
              
					
						if(!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))) 
				     	{
						$code = '111'; 
						  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                            {	            $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                                        	 $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    				 		 $card_type      =$this->general_model->getType($card_no);
               								   $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    				       
                                         		$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                                            	
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										 	'CardType'     =>$card_type,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
										  'CustomerCard' =>$card_no,
										  'CardCVV'      =>$cvv, 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
    
                                         	
                          			$card=	$this->card_model->process_card($card_data) ;  
                          			
                          			
                                         }  
              
				
                 
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
							 
				  
				    
        			 $condition1 = array('resellerID'=>$resellerID);
                	
                 	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
        			 
        			 $val = array(
                					'merchantID' => $merchantID,
                				);
			                    $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
				
                                $subdomain     = $Freshbooks_data['sub_domain'];
                                $key           = $Freshbooks_data['secretKey'];
                                $accessToken   = $Freshbooks_data['accessToken'];
                                $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                         
                                define('OAUTH_CONSUMER_KEY', $subdomain);
                                define('OAUTH_CONSUMER_SECRET', $key);
                                define('OAUTH_CALLBACK', $URL1);
					 
					
					  
                    if(isset($accessToken) && isset($access_token_secret))
                    {
						
    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
    
                           $qblist=array();
				           if(!empty($invoiceIDs))
				           {
				              foreach($invoiceIDs as $inID)
				              {
								  
								            $con = array('invoiceID'=>$inID,'merchantID'=>$merchantID);
            									$res = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining','invoiceID'), $con);
            									$amount_data = $res['BalanceRemaining'];
								                $invID       = $res['invoiceID'];
								  
								  $payment = '<?xml version="1.0" encoding="utf-8"?>  
												<request method="payment.create">  
												  <payment>         
													<invoice_id>'.$invID.'</invoice_id>               
													<amount>'.$amount_data.'</amount>             
													<currency_code>USD</currency_code> 
													<type>Check</type>                   
												  </payment>  
												</request>';
									  $invoices = $c->add_payment($payment);
									 
						
									if ($invoices['status'] != 'ok') {
						
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
						
										redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
										
									}
									else {
									   $inv_array['inv_no'][]= $invID;
									    $inv_array['inv_amount'][]= $amount_data;
									    $inv_invoice[]  =   $invID;
										$pinv_id   =  $invoices['payment_id'];
										$qblist[]  = $pinv_id;
										$this->session->set_flashdata('success','Success'); 
										
									}
								}			
						   }
					}						   
					   
					
				    
				    
				    
				    
				    
				    
					 $this->session->set_flashdata('success','Transaction Successful'); 	
					
				} else{
					$code='401';
					$responsetext='';
					if(!empty($PayPalResult["RAWRESPONSE"]))
						{
					
					 $responsetext= $PayPalResult['L_LONGMESSAGE0'];
						}
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$responsetext.'</strong>.</div>'); 
					
					} 
						if(!empty($PayPalResult["RAWRESPONSE"]))
						{
					 $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt =$PayPalResult["AMT"];  }
					   $transactiondata= array();
				       $transactiondata['transactionID']       = $tranID;
					   $transactiondata['transactionStatus']    = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					     $transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $code;  
					  
						$transactiondata['transactionType']    = "Paypal_sale";	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $amt;
					   $transactiondata['merchantID']   = $merchantID;
					   	   if(!empty($invoiceIDs) && !empty($inv_array))
				        {
					      $transactiondata['invoiceID']            = implode(',',$inv_invoice);
					      $transactiondata['invoiceRefID']         = json_encode($inv_array);
					      if(!empty($qblist))
					      $transactiondata['qbListTxnID']          = implode(',',$qblist); 
				        }   
					   
					   
					   
				     $transactiondata['resellerID']   = $this->resellerID;
				      $transactiondata['gateway']   = "Paypal";					 
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
						}
			   }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}		
		 
            
		   }
	         redirect('FreshBooks_controllers/Transactions/create_customer_sale','refresh');
	   }
	
	public function create_customer_sale()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		   
	      $user_id = $this->merchantID;
		
		$checkPlan = check_free_plan_transactions();
		if($checkPlan && !empty($this->input->post(null, true)))
		{
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
             
           
               
	    	if ($this->form_validation->run() == true)
		    {
	   
        		$custom_data_fields = [];
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }

			  $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        		if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
        		
        			      	    $username  = $gt_result['gatewayUsername'];
								$password  = $gt_result['gatewayPassword'];
								$signature = $gt_result['gatewaySignature'];
								
								
				 			
								$config = array(
									'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
									'APIUsername' => $username, 	// PayPal API username of the API caller
									'APIPassword' => $password,	// PayPal API password of the API caller
									'APISignature' => $signature, 	// PayPal API signature of the API caller
									'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
									'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
								  );
					
							// Show Errors
								if($config['Sandbox'])
								{
									error_reporting(E_ALL);
									ini_set('display_errors', '1');
								}
								$this->load->library('paypal/Paypal_pro', $config);		
            				 $invoiceIDs   = array();
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     $invoiceIDs   = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				  
		             $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        			
					 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
					 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
        			 $companyID  = $comp_data['companyID'];
        			 $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                         
        			 $cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						$cardType  = $this->general_model->getType($card_no); 
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =$this->czsecurity->xssCleanPostInput('cvv'); 
        				     
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						 
        					
        					}
									
							    $country 	= 'US';	// US or other valid country code
								$currencyID = 'USD';
        						$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
								$email   =  $this->czsecurity->xssCleanPostInput('email');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    	                        $name    =  $this->czsecurity->xssCleanPostInput('firstName').''.$this->czsecurity->xssCleanPostInput('lastName');
									if($this->czsecurity->xssCleanPostInput('country')=="United States" ||$this->czsecurity->xssCleanPostInput('country')=="United State"){
									  $country 	= 'US';	// US or other valid country code
									  $currencyID = 'USD';
									}
									
									if($this->czsecurity->xssCleanPostInput('country')=="Canada" ||$this->czsecurity->xssCleanPostInput('country')=="canada"){
									  $country 	  = 'CAD';	// US or other valid country code
									  $currencyID = 'CAD';
									}
					
									$amount 	= $this->czsecurity->xssCleanPostInput('totalamount');	
																// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
				       	
					
							
							 
								$paymentType 		=	'Sale';
								$companyName        = $this->czsecurity->xssCleanPostInput('companyName');
								$firstName 			= $this->czsecurity->xssCleanPostInput('fistName');
								$lastName 			= $this->czsecurity->xssCleanPostInput('lastName');
								$creditCardType 	= ($cardType)?$cardType:'Visa';
							
								$creditCardNumber 	= $card_no;
								$expDateMonth 		= $expmonth;
								// Month must be padded with leading zero
								$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
								
								$expDateYear=  	$exyear;
								$cvv2Number =   $cvv;
								$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
							$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
						$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
							$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zipcode, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);	

		                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
		                	$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
                    		
		                	$PaymentDetails['invnum'] = $new_invoice_number;
		                }

		                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {

		                	$PaymentDetails['custom'] = 'PO Number: '.$this->czsecurity->xssCleanPostInput('po_number');
		                }

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
					 	include APPPATH .'libraries/Freshbooks_data.php';
								 $fb_data = new Freshbooks_data($this->merchantID,$this->resellerID);     
						
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
						 $result = $PayPalResult;  
					    $pinv_id='';
						if(!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))) 
				     	{
	
								
								 $invoiceIDs=array();      
								 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
								 {
									$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
								 }
				     
								 $refNumber =array();
					 
							   if(!empty($invoiceIDs))
							   {
								   
								  foreach($invoiceIDs as $inID)
								  {
									
									$in_data = $this->general_model->get_select_data('Freshbooks_test_invoice',array('BalanceRemaining','refNumber','AppliedAmount'),array('merchantID'=>$this->merchantID,'invoiceID'=>$inID));
									$refNumber[] = 	$in_data['refNumber'];								
									$amount_data  = $amount;						
									 $pinv_id =''; 
									$invoices=  $fb_data->create_invoice_payment($inID, $amount_data);
									 if ($invoices['status'] != 'ok')
									 {
											  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
									  }
									   else 
									   {
										 $pinv_id =  $invoices['payment_id'];
										
										  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
										}
									
									$id = $this->general_model->insert_gateway_transaction_data($result,'pay_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount_data,$this->merchantID,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);  
								  }
							   }  
							   else
							   {
											$crtxnID = '';
											$inID    = '';
											$id      = $this->general_model->insert_gateway_transaction_data($result,'pay_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$this->merchantID,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields); 
										   
								}		   
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								if(!empty($refNumber))
								$ref_number = implode(',',$refNumber); 
								else
								$ref_number = '';
								$tr_date   =date('Y-m-d H:i:s');
								  $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 		
								if($chh_mail =='1')
								 {
								   
								  
								   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
								 } 
								   
								
								 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
								 {
											$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
											$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
											$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
										   $card_type = $this->general_model->getType($card_no);
									 
												$card_data = array('cardMonth'   =>$expmonth,
														   'cardYear'	 =>$exyear, 
														  'CardType'     =>$card_type,
														  'CustomerCard' =>$card_no,
														  'CardCVV'      =>$cvv, 
														 'customerListID' =>$customerID, 
														 'companyID'     =>$companyID,
														  'merchantID'   => $this->merchantID,
														
														 'createdAt' 	=> date("Y-m-d H:i:s"),
														 'Billing_Addr1'	 =>$address1,
														 'Billing_Addr2'	 =>$address2,	 
															  'Billing_City'	 =>$city,
															  'Billing_State'	 =>$state,
															  'Billing_Country'	 =>$country,
															  'Billing_Contact'	 =>$phone,
															  'Billing_Zipcode'	 =>$zipcode,
														 );
												
											$id1 =    $this->card_model->process_card($card_data);	
									 }
				
								
							 $this->session->set_flashdata('success','Transaction Successful'); 
            			
							 } 
							 else
							{
											  $crtxnID ='';
											  
											
											   $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Payment Failed</strong></div>'); 
												$id = $this->general_model->insert_gateway_transaction_data($result,'paypal_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
											 
							}
                         
                         
                        
                				       
        				}
        			
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
		} 
		$invoice_IDs = array();
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				}
			
				$receipt_data = array(
					'transaction_id' => isset($PayPalResult) ? $PayPalResult['TRANSACTIONID'] : '',
					'IP_address' => getClientIpAddr(),
					'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
					'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
					'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
					'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
					'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
					'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
					'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
					'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
					'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
					'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
					'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
					'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
					'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
					'Contact' => $this->czsecurity->xssCleanPostInput('email'),
					'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
					'proccess_btn_text' => 'Process New Sale',
					'sub_header' => 'Sale',
					'checkPlan'  => $checkPlan
				);
				
				$this->session->set_userdata("receipt_data",$receipt_data);
				$this->session->set_userdata("invoice_IDs",$invoice_IDs);
				
				
				redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
		 

                   
			  $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
			 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				  
				$condition				= array('merchantID'=>$user_id );
			     $gateway        		= $this->general_model->get_gateway_data($user_id);
			     $data['gateways']      = $gateway['gateway'];
			     $data['gateway_url']   = $gateway['url'];
			     $data['stp_user']      = $gateway['stripeUser'];
   
				$compdata				= $this->fb_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
				$this->load->view('template/template_start', $data);
					
				$this->load->view('template/page_head', $data);
				$this->load->view('FreshBooks_views/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
	    
	    
    	}
	
	
	  
	   public function create_customer_auth()
	   {
		  
			$this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");
			$checkPlan = check_free_plan_transactions();

		   if($checkPlan && !empty($this->input->post(null, true))){
				
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
                if($this->czsecurity->xssCleanPostInput('setMail'))
                $chh_mail =1;
                else
                $chh_mail =0; 
                
                
                $customerID=$this->czsecurity->xssCleanPostInput('customerID');
			   if($gatlistval !="" && !empty($gt_result) )
			   {
					$username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					$option = array('API_UserName' => $username,
						'API_Password'       => $password,
						'API_Signature'      => $signature,
						'API_Endpoint'       => "https://api-3t.paypal.com/nvp",
						'envoironment'       => 'sandbox',
						'version'            => '123');
					
					 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					
			
				$comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];	
                $cardID = $this->czsecurity->xssCleanPostInput('card_list');
			  
			  
			  
			  
			    
                if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
                {	
                    
                    $card_no 	= $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth  	= $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv        = $this->czsecurity->xssCleanPostInput('cvv');
				 }else {
					  
						   
                  $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
					    	$expmonth =  $card_data['cardMonth']; 
							$exyear   = $card_data['cardYear'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$cvv     = $card_data['CardCVV'];
					}	
			  
			  
			    	$paymentType 		=	'Authorization';
					$companyName        = urlencode($this->czsecurity->xssCleanPostInput('companyName'));
					$firstName 			= urlencode($this->czsecurity->xssCleanPostInput('firstName'));
					$lastName 			= urlencode($this->czsecurity->xssCleanPostInput('lastName'));
					$creditCardType 	= urlencode('Visa');
					$creditCardNumber 	= urlencode($card_no);
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));

					$expDateYear= urlencode($exyear);
					$cvv2Number = urlencode($cvv);
					$address1	= urlencode($this->czsecurity->xssCleanPostInput('address'));
					$address2 	= '';
					$city 		= urlencode($this->czsecurity->xssCleanPostInput('city'));
					$state 		= urlencode($this->czsecurity->xssCleanPostInput('state'));
					$zip 		= urlencode($this->czsecurity->xssCleanPostInput('zipcode'));
				 $country 	= 'US';	
					if(strtoupper($this->czsecurity->xssCleanPostInput('country'))=="UNITED STATES" || strtoupper($this->czsecurity->xssCleanPostInput('country'))=="UNITED STATE"){
					  $country 	= 'US';	// US or other valid country code
					}
					
					if($this->czsecurity->xssCleanPostInput('country')=="Canada" ||$this->czsecurity->xssCleanPostInput('country')=="canada"){
					  $country 	= 'CAD';	// US or other valid country code
					}
					
					$amount 	= $this->czsecurity->xssCleanPostInput('totalamount');	//actual amount should be substituted here
					$currencyID = 'USD';// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
					  
								   
					// Add request-specific fields to the request string.
					$nvpStr =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
								"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&COMPANYNAME=$companyName".
								"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";

					// Execute the API operation; see the PPHttpPost function above.
						// Set up your API credentials, PayPal end point, and API version.
						
					//	$version = urlencode('51.0');
					$paypal = new PayPalAPINEW($option);
										
					$httpParsedResponseAr = $paypal ->PPHttpPost('DoDirectPayment', $nvpStr, $option);
				
				 
				           if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                            {	            $cid      =    $this->czsecurity->xssCleanPostInput('customerID');	
                                        	 $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    				 		 $card_type      =$this->general_model->getType($card_no);
               								   $expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
											$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    				       
                                         		$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                                            	
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										 	'CardType'     =>$card_type,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'),
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('baddress1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('baddress2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('bcity'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('bcountry'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('bphone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('bstate'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('bzipcode'),
										  'CustomerCard' =>$card_no,
										  'CardCVV'      =>$cvv, 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
    
                                         	
                          			$card=	$this->card_model->process_card($card_data) ;  
                          			
                          			
                                         }  
              
				
                 
				          if($chh_mail =='1')
							 {
							   
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode(',',$refNumber); 
							  else
							  $ref_number = '';
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
							 } 
							 
				
				
				
				
					if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
						$code='111';
						
					 $this->session->set_flashdata('success','Transaction Successful'); 	
					
				} else{
					 $code='401';
					 $responsetext= urldecode($httpParsedResponseAr['L_LONGMESSAGE0']);
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$responsetext.'</strong>.</div>'); 
					
					}
					  $transactiondata= array();
					  	 $tranID ='' ;$amt='0.00';
					     if(isset($httpParsedResponseAr['TRANSACTIONID'])) { $tranID = $httpParsedResponseAr['TRANSACTIONID'];   $amt = urldecode($httpParsedResponseAr["AMT"]);  }
				       $transactiondata['transactionID']       =  $tranID;
					   $transactiondata['transactionStatus']    = $httpParsedResponseAr["ACK"];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime(urldecode($httpParsedResponseAr["TIMESTAMP"])));  
					   $transactiondata['transactionCode']     = $code; 
					    $transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
						$transactiondata['transactionType']    = "Paypal_auth";	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   =  $amt;
					   $transactiondata['merchantID']   = $merchantID;
				     $transactiondata['resellerID']   = $this->resellerID;
				      $transactiondata['gateway']   = "Paypal";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					   	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					
			   }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}		
				
             
		   }
		   $invoice_IDs = array();
			 
		  
			  $receipt_data = array(
				  'transaction_id' => isset($httpParsedResponseAr) ? $httpParsedResponseAr['TRANSACTIONID'] : '',
				  'IP_address' => getClientIpAddr(),
				  'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				  'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				  'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				  'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				  'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				  'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				  'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				  'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				  'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				  'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				  'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				  'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				  'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				  'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				  'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				  'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				  'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_auth',
				  'proccess_btn_text' => 'Process New Transaction',
				  'sub_header' => 'Authorize',
			  );
			  
			  $this->session->set_userdata("receipt_data",$receipt_data);
			  $this->session->set_userdata("invoice_IDs",$invoice_IDs);
			  
			  
			  redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');   
		   
		   
	   }
	
	
	public function create_customer_refund()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
			     
				   
				 
			     $tID     = $this->czsecurity->xssCleanPostInput('txnpaypalID');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				
			    
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			       
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);	
						
    		
				 $customerID = $paydata['customerListID'];
				 
			
				 $amount     =  $paydata['transactionAmount']; 
				 	$merchantID =$paydata['merchantID']	;  
				 
				 	 $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
				 	 
				 	 if($amount==$total)
				 	 {
				 	    $restype="Full" ;
				 	 }else{
				 	    $restype="Partial" ;  
				 	 }
                    $amount     = $total ;
				 
			  
                   	$RTFields = array(
					'transactionid' => $tID, 							// Required.  PayPal transaction ID for the order you're refunding.
					'payerid' => '', 								// Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
					'invoiceid' => '', 								// Your own invoice tracking number.
					'refundtype' => $restype, 							// Required.  Type of refund.  Must be Full, Partial, or Other.
					'amt' =>  $amount, 									// Refund Amt.  Required if refund type is Partial.  
					'currencycode' => '', 							// Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
					'note' => '',  									// Custom memo about the refund.  255 char max.
					'retryuntil' => '', 							// Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
					'refundsource' => '', 							// Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
					'merchantstoredetail' => '', 					// Information about the merchant store.
					'refundadvice' => '', 							// Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
					'refunditemdetails' => '', 						// Details about the individual items to be returned.
					'msgsubid' => '', 								// A message ID used for idempotence to uniquely identify a message.
					'storeid' => '', 								// ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
					'terminalid' => ''								// ID of the terminal.  50 char max.
				);	
					
		$PayPalRequestData = array('RTFields' => $RTFields);
		
		$PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);
		
				
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
				     
				$code = '111';
			  
			  $c_data               =  $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID'=>$customerID));
			  
			  
		            $val = array(
    					'merchantID' => $paydata['merchantID'],
    				);
				
    				$merchID =$paydata['merchantID'];
        		
    		
            		$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
                   $rs_data = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=>$merchID));
                	 	$redID = $rs_data['resellerID'];
                	$condition1 = array('resellerID'=>$redID);
                	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        						 $domain1 = $sub1['merchantPortalURL'];
        						 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';  //print_r($gt_result);die;
                    
                    $subdomain     = $Freshbooks_data['sub_domain'];
                    $key           = $Freshbooks_data['secretKey'];
                    $accessToken   = $Freshbooks_data['accessToken'];
                    $access_token_secret = $Freshbooks_data['oauth_token_secret'];
             
                    define('OAUTH_CONSUMER_KEY', $subdomain);
                    define('OAUTH_CONSUMER_SECRET', $key);
                    define('OAUTH_CALLBACK', $URL1);
			   	  	  
				 if(!empty($paydata['invoiceID']))
				{
				     $refund =$amount;
				    $ref_inv =explode(',',$paydata['invoiceID']);
				    if(count($ref_inv)>1)
				    {
				      $ref_inv_amount = json_decode($paydata['invoiceRefID']);
				      $imn_amount =  $ref_inv_amount->inv_amount; 
				      $p_ids     =   explode(',',$paydata['qbListTxnID']);
				     
				      $inv_array = array();
				      foreach($ref_inv as $k=> $r_inv)
				      {
				          
				           $inv_data=$this->general_model->get_select_data('Freshbooks_test_invoice',array('DueDate'),array('merchantID'=>$merchID,'invoiceID'=>$r_inv));
				          
				         if(!empty($inv_data))
				          {
				           $due_date=   date('Y-m-d',strtotime($inv_data['DueDate']));
				          }else{
				               $due_date=   date('Y-m-d');  
				          }
				          
				          $re_amount = $imn_amount[$k];
				          if($refund > 0 && $imn_amount[$k] >0)
				          {
				              
				              
				            $p_refund1=0;  
				              
				            $p_id  =  $p_ids[$k];
				              
				          	$ittem =  $this->fb_customer_model->get_invoice_item_data($r_inv);
				           
				            if($refund <= $imn_amount[$k] )
				            {
				                $p_refund1 =  $refund;
				                  
				                  
				                   $p_refund =   $imn_amount[$k] -$refund;
				                 
				                  $re_amount =$imn_amount[$k] -$refund;
				                  $refund =0;
				               
				            }
				            else
				            {
				                 $p_refund =0;
				                 $p_refund1 =  $imn_amount[$k];
				                 $refund=$refund-$imn_amount[$k];
				                 
				                 $re_amount =0;
				            }
				            
        				               
                            if(isset($accessToken) && isset($access_token_secret))
                            {
                                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                           
                              $payment = '<?xml version="1.0" encoding="utf-8"?>  
                    						<request method="payment.update">  
                    						  <payment>         
                    						    
                                                    <payment_id>'.$p_id.'</payment_id>
                                                    <amount>'.$p_refund.'</amount>
                                                    <notes>Payment refund for invoice:'.$r_inv.' '.$p_refund1.' </notes>
                                                    <currency_code>USD</currency_code>
                                                  </payment>              
                    						  
                    						</request>';
                                  
                                  $invoices = $c->edit_payment($payment);
                                 
                             
                               
                        			if ($invoices['status'] != 'ok') 
                        			{
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        
                        				
                        			}
                        			else {
                        			        $ins_id = $p_id;
                                            $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$p_refund1,
            				  	           'creditInvoiceID'=>$r_inv,'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           );	
            				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
            				  	        $lineArray='';
            				 
                            			  foreach($ittem as $item_val)
                            			  {
                            					
                            				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
                        					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
                        					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
                        					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
                        					$lineArray .= '<type>Item</type></line>';  
                            					
                            			  }		
                            					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
                        					$lineArray .= '<description>This is used to refund Incoive payment</description>';
                        					$lineArray .= '<unit_cost>'.(-$p_refund1).'</unit_cost>';
                        					$lineArray .= '<quantity>1</quantity>';
                        					$lineArray .= '<type>Item</type></line>'; 	
            					
            					
            					
                        				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                     		<request method="invoice.update"> 
                                     		 <invoice> 
                                     		  <invoice_id>'.$r_inv.'</invoice_id> 
                                     		  <date>'.$due_date.'</date>
                        				          <lines>'.$lineArray.'</lines>  
                        						</invoice>  
                        						</request> ';
                        		           
                                             $invoices1 = $c->add_invoices($invoice);
                        				  	          
            				  	     
                        				$this->session->set_flashdata('success','Successfully Refunded Payment'); 
                        				
                        			}
                    			
                                
                            }	
                            
                            
                            $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount;
        				           
				           }else{
				               
				                $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount; 
				           }  
				            
				            
				          
				      }
				       $this->general_model->update_row_data('customer_transaction',$con,array('invoiceRefID'=>json_encode($inv_array)));
				     }
				     else
				     {
				  
        			$ittem =  $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);
    		
                    $p_id     = $paydata['qbListTxnID'];
                    $p_amount = $paydata['transactionAmount']-$amount; 
                    
                    if(isset($accessToken) && isset($access_token_secret))
                    {
                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                   
                      $payment = '<?xml version="1.0" encoding="utf-8"?>  
            						<request method="payment.update">  
            						  <payment>         
            						    
                                            <payment_id>'.$p_id.'</payment_id>
                                            <amount>'.$p_amount.'</amount>
                                            <notes>Payment refund for invoice:'.$paydata['invoiceID'].' '.$amount.' </notes>
                                            <currency_code>USD</currency_code>
                                          </payment>              
            						  
            						</request>';
                          
                          $invoices = $c->edit_payment($payment);
                         
                     
                       
                			if ($invoices['status'] != 'ok') 
                			{
                
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                
                				
                			}
                			else {
                			        $ins_id = $p_id;
                                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$amount,
    				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
    				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
    				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
    				  	           );	
    				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
    				  	        $lineArray='';
    				 
    			  foreach($ittem as $item_val)
    			  {
    					
    				$lineArray .= '<line><name>'.$item_val['itemDescription'].'</name>';
					$lineArray .= '<description>'.$item_val['itemDescription'].'</description>';
					$lineArray .= '<unit_cost>'.$item_val['itemPrice'].'</unit_cost>';
					$lineArray .= '<quantity>'.$item_val['itemQty'].'</quantity> <tax1_name>'.$item_val['taxName'].'</tax1_name><tax1_percent>'.$item_val['taxPercent'].'</tax1_percent>';
					$lineArray .= '<type>Item</type></line>';  
    					
    			  }		
    					$lineArray .= '<line><name>'.$ittem[0]['itemDescription'].'</name>';
					$lineArray .= '<description>This is used to refund Incoive payment</description>';
					$lineArray .= '<unit_cost>'.(-$amount).'</unit_cost>';
					$lineArray .= '<quantity>1</quantity>';
					$lineArray .= '<type>Item</type></line>'; 	
    					
    					
    					
    				  $invoice = '<?xml version="1.0" encoding="utf-8"?>
                 		<request method="invoice.update"> 
                 		 <invoice> 
                 		  <invoice_id>'.$paydata['invoiceID'].'</invoice_id> 
                 		  
    				          <lines>'.$lineArray.'</lines>  
    						</invoice>  
    						</request> ';
    		           
                         $invoices1 = $c->add_invoices($invoice);
    				  	          
    				  	     
                				$this->session->set_flashdata('success','Successfully Refunded Payment'); 
                				
                			}
            			}			
				     }	   
					   
    		  
             }
             
           

             
				
				  $this->fb_customer_model->update_refund_payment($tID, 'PAYPAL');	
					
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					  $code = '401';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$responsetext.'</strong>.</div>'); 
				 }
				       $transactiondata= array();
				       	 $tranID ='' ;$amt='0.00';
				       	 
				       	 
					     if(isset($PayPalResult['REFUNDTRANSACTIONID'])) { $tranID = $PayPalResult['REFUNDTRANSACTIONID'];   $amt =$PayPalResult['GROSSREFUNDAMT'];  }
				       $transactiondata['transactionID']      = $tranID;
					   $transactiondata['transactionStatus']  = $PayPalResult['ACK'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					     $transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
					   $transactiondata['transactionType']    = 'Paypal_refund' ;
					    $transactiondata['transactionCode']   = $code;
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amt ;
					     if(!empty($paydata['invoiceID']))
        			     	{
        				      $transactiondata['invoiceID']  = $paydata['invoiceID'];
        			     	}
					   $transactiondata['merchantID']  = $merchantID ;
				     $transactiondata['resellerID']   = $this->resellerID;
				      $transactiondata['gateway']   = "Paypal";	
				      $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				        if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
					   $invoice_IDs = array();
					  
				   
					   $receipt_data = array(
						   'proccess_url' => 'FreshBooks_controllers/Transactions/payment_refund',
						   'proccess_btn_text' => 'Process New Refund',
						   'sub_header' => 'Refund',
					   );
					   
					   $this->session->set_userdata("receipt_data",$receipt_data);
					   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					   if($paydata['invoiceTxnID'] == ''){
						   $paydata['invoiceTxnID'] ='null';
						   }
						   if($paydata['customerListID'] == ''){
							   $paydata['customerListID'] ='null';
						   }
						   if($PayPalResult['REFUNDTRANSACTIONID']== ''){
							   $PayPalResult['REFUNDTRANSACTIONID'] ='null';
						   }
					   
					   redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$PayPalResult['REFUNDTRANSACTIONID'],  'refresh');
				
          }
              
	
	}
	
	
	/*****************Capture Transaction***************/
	
	
	
		
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
		    
		    
		          	 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					
		       
				 $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnID');
    			 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			   if( $paydata['gatewayID'] > 0){ 
			      
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			        if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
			  
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	

			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      
				$DCFields = array(
						'authorizationid' => $tID, 				// Required. The authorization identification number of the payment you want to capture. This is the transaction ID returned from DoExpressCheckoutPayment or DoDirectPayment.
						'amt' =>  $amount , 							// Required. Must have two decimal places.  Decimal separator must be a period (.) and optional thousands separator must be a comma (,)
						'completetype' => 'Complete', 					// Required.  The value Complete indiciates that this is the last capture you intend to make.  The value NotComplete indicates that you intend to make additional captures.
						'currencycode' => '', 					// Three-character currency code
						'invnum' => '', 						// Your invoice number
						'note' => '', 							// Informational note about this setlement that is displayed to the buyer in an email and in his transaction history.  255 character max.
						'softdescriptor' => '', 				// Per transaction description of the payment that is passed to the customer's credit card statement.
						'storeid' => '', 						// ID of the merchant store.  This field is required for point-of-sale transactions.  Max: 50 char
						'terminalid' => ''						// ID of the terminal.  50 char max.  
					);
					
	    	$PayPalRequestData = array('DCFields' => $DCFields);
	    	 $PayPalResult["ACK"]='';
	    	$PayPalResult = $this->paypal_pro->DoCapture($PayPalRequestData);
	
				 
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])){  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
					$customerID = $paydata['customerListID'];
					$tr_date   =date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
					if($chh_mail =='1')
                            {
                               
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
					
			        $this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 $code = '401';
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
				     
					 }
					 
					   $transactiondata= array();
					    $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt = $PayPalResult['AMT'];  }
					   
				       $transactiondata['transactionID']      =$tranID ;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult["TIMESTAMP"]));  
					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));
					   $transactiondata['transactionType']    = 'Paypal_capture';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  =$amt;
					    $transactiondata['merchantID']        =  $merchantID;
					    $transactiondata['resellerID']        = $this->resellerID;
					   $transactiondata['gateway']            = "Paypal";
					    $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				     
					 }	
					 $invoice_IDs = array();
					
				 
					 $receipt_data = array(
						 'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
						 'proccess_btn_text' => 'Process New Transaction',
						 'sub_header' => 'Capture',
					 );
					 
					 $this->session->set_userdata("receipt_data",$receipt_data);
					 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					 if($paydata['invoiceTxnID'] == ''){
						 $paydata['invoiceTxnID'] ='null';
						 }
						 if($paydata['customerListID'] == ''){
							 $paydata['customerListID'] ='null';
						 }
						 if($PayPalResult['TRANSACTIONID']== ''){
							 $PayPalResult['TRANSACTIONID'] ='null';
						 }
					 
					 redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$PayPalResult['TRANSACTIONID'],  'refresh');
					
        }
              


	}
	
	
	
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if(!empty($this->input->post(null, true))){
		    
		        
		        	 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					
			
    			  $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnvoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				  $gatlistval = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
			   
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
					
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      
				$DVFields = array(
						'authorizationid' => $tID, 				// Required.  The value of the original authorization ID returned by PayPal.  NOTE:  If voiding a transaction that has been reauthorized, use the ID from the original authorization, not the reauth.
						'note' => 'This test void',  							// An information note about this void that is displayed to the payer in an email and in his transaction history.  255 char max.
						'msgsubid' => ''						// A message ID used for idempotence to uniquely identify a message.
					);	
								
					$PayPalRequestData = array('DVFields' => $DVFields);
					$PayPalResult = $this->paypal_pro->DoVoid($PayPalRequestData);
		
		
			 
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
					
			        $this->session->set_flashdata('success','Transaction Successfully Cancelled.'); 
				 }else{
					  $code = '401';
				   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
				
				 }
				       $transactiondata= array();
				         $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['AUTHORIZATIONID'])) { $tranID = $PayPalResult['AUTHORIZATIONID'];    }
				       
				       $transactiondata['transactionID']      = $tranID;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					   $transactiondata['transactionModified']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					   $transactiondata['transactionType']    = 'Paypal_void';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $paydata['transactionAmount'];
					   $transactiondata['merchantID']  = $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paypal";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			
		
				
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
		}     
			
	}
	
	
	
	
	
	
	public function create_customer_capture_old()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
		    
		    
		          
		       
				 $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnID');
    			 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			   if( $paydata['gatewayID'] > 0){ 
			        $merchantID = $this->session->userdata('logged_in')['merchID']; 
			      
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			  
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	

			   
				 
				 $customerID = $paydata['customerListID'];
			// print_r($paydata['transactionAmount'] ); die;
				 $amount  =  $paydata['transactionAmount']; 
				 
				$DCFields = array(
						'authorizationid' => $tID, 				// Required. The authorization identification number of the payment you want to capture. This is the transaction ID returned from DoExpressCheckoutPayment or DoDirectPayment.
						'amt' =>  $amount , 							// Required. Must have two decimal places.  Decimal separator must be a period (.) and optional thousands separator must be a comma (,)
						'completetype' => 'Complete', 					// Required.  The value Complete indiciates that this is the last capture you intend to make.  The value NotComplete indicates that you intend to make additional captures.
						'currencycode' => '', 					// Three-character currency code
						'invnum' => '', 						// Your invoice number
						'note' => '', 							// Informational note about this setlement that is displayed to the buyer in an email and in his transaction history.  255 character max.
						'softdescriptor' => '', 				// Per transaction description of the payment that is passed to the customer's credit card statement.
						'storeid' => '', 						// ID of the merchant store.  This field is required for point-of-sale transactions.  Max: 50 char
						'terminalid' => ''						// ID of the terminal.  50 char max.  
					);
					
		$PayPalRequestData = array('DCFields' => $DCFields);
		$PayPalResult = $this->paypal_pro->DoCapture($PayPalRequestData);
		
				 
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
					
			        $this->session->set_flashdata('success',' Successfully Captured Authorization'); 
				 }else{
					 $code = '401';
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$responsetext.'</strong>.</div>'); 
				     
					 }
					 
					   $transactiondata= array();
					    $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt = $PayPalResult['AMT'];  }
					   
				       $transactiondata['transactionID']      =$tranID ;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult["TIMESTAMP"]));  
					     $transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
					   $transactiondata['transactionType']    = 'Paypal_capture';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  =$amt;
					    $transactiondata['merchantID']  =  $merchantID;
				     $transactiondata['resellerID']   = $this->resellerID;
				      $transactiondata['gateway']   = "Paypal";	
				      $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				        if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				     
					 }	
					
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
        }
              
				

	
	}
	
	
	
	public function create_customer_void_old()
	{
	    
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if(!empty($this->input->post(null, true))){
			
    			  $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnvoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				  $gatlistval = $paydata['gatewayID'];
				  $merchantID = $this->session->userdata('logged_in')['merchID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
			   
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
					
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
				$DVFields = array(
						'authorizationid' => $tID, 				// Required.  The value of the original authorization ID returned by PayPal.  NOTE:  If voiding a transaction that has been reauthorized, use the ID from the original authorization, not the reauth.
						'note' => 'This test void',  							// An information note about this void that is displayed to the payer in an email and in his transaction history.  255 char max.
						'msgsubid' => ''						// A message ID used for idempotence to uniquely identify a message.
					);	
								
					$PayPalRequestData = array('DVFields' => $DVFields);
					$PayPalResult = $this->paypal_pro->DoVoid($PayPalRequestData);
		
		
			 
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
					
			        $this->session->set_flashdata('success','Transaction Successfully Cancelled.'); 
				 }else{
					  $code = '401';
				   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  '.$result['responsetext'].'</strong>.</div>'); 
				
				 }
				       $transactiondata= array();
				         $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['AUTHORIZATIONID'])) { $tranID = $PayPalResult['AUTHORIZATIONID'];    }
				       
				       $transactiondata['transactionID']      = $tranID;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					   $transactiondata['transactionType']    = 'Paypal_void';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $paydata['transactionAmount'];
					   $transactiondata['merchantID']  = $merchantID;
				     $transactiondata['resellerID']   = $this->resellerID;
				      $transactiondata['gateway']   = "Paypal";	
				      $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				        if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			
		
				
					redirect('FreshBooks_controllers/Transactions/payment_capture','refresh');
		}     
			
	
	}
	
	
	
	
	 	 
        public function pay_invoice()
        {
			$this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");
			$this->session->unset_userdata("in_data");
       
        
        	 $cusproID=array(); 
        	 $error=array();
             $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
	    	
			        
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    	
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
        	$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {		
	 	
	                $merchID    = $this->merchantID;
		        	$user_id    = $merchID;
		        	$resellerID =	$this->resellerID;
		       		$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
                	
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;	
        		    $chh_mail  =0;	 
                    $in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
                     $address2 ='';   
                	if(!empty($in_data))
            		{ 
		                 
                        $customerID = $in_data['Customer_ListID'];
            		
            			$companyID = $in_data['companyID'];
                        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
                     	$username  = $gt_result['gatewayUsername'];
				    	$password  = $gt_result['gatewayPassword'];
					    $signature = $gt_result['gatewaySignature'];
					
					
				        $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					   );
		
    					// Show Errors
    					if($config['Sandbox'])
    					{
    						error_reporting(E_ALL);
    						ini_set('display_errors', '1');
    					}
    					
    					$this->load->library('paypal/Paypal_pro', $config);
			        	
        			     if($cardID!="new1")
        			     {
        			        $card_data=   $this->card_model->get_single_card_data($cardID);
        			       
        			        $card_no  = $card_data['CardNo'];
    				        $expmonth =  $card_data['cardMonth'];
        					$exyear   = $card_data['cardYear'];
        					$cvv      = $card_data['CardCVV'];
        					$cardType = $card_data['CardType'];
        					$address1 = $card_data['Billing_Addr1'];
        					$city     =  $card_data['Billing_City'];
        					$zipcode  = $card_data['Billing_Zipcode'];
        					$state    = $card_data['Billing_State'];
        					$country  = $card_data['Billing_Country'];
        			     }
        			     else
        			     {
        			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
        					$cardType = $this->general_model->getType($card_no);
        					$cvv ='';
        					if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
        					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
        	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
        	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
        	                        $country =$this->czsecurity->xssCleanPostInput('country');
        	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
        	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
        	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        			         
        			     }    
    			        	
        				if( $in_data['BalanceRemaining'] > 0)
        				{
    					   
			             $amount         =	 $this->czsecurity->xssCleanPostInput('inv_amount'); 
			           	 $currencyID     = "USD";
						  $padDateMonth 		= str_pad($expmonth, 2, '0', STR_PAD_LEFT);
                           
                           $firstName = $in_data['firstName'];
                            $lastName =  $in_data['lastName']; 
                            $companyName =  $in_data['companyName']; 
							 
							$phone = $in_data['phoneNumber']; 
							$email = $in_data['userEmail']; 
										
	                	$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
	                	$CCDetails = array(
							'creditcardtype' => $cardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $card_no, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$exyear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
                		$PayerInfo = array(
                							'email' => $email, 								// Email address of payer.
                							'payerid' => '', 							// Unique PayPal customer ID for payer.
                							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
                							'business' => '' 							// Payer's business name.
                						);  
                						
                		$PayerName = array(
                							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
                							'firstname' => $firstName, 							// Payer's first name.  25 char max.
                							'middlename' => '', 						// Payer's middle name.  25 char max.
                							'lastname' => $lastName, 							// Payer's last name.  25 char max.
                							'suffix' => ''								// Payer's suffix.  12 char max.
                						);
                					
                		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zipcode, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

					         	$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
						
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				        $result  = $PayPalResult ;
					 	$pinv_id='';
				    	 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
				    	 {  
            					
            			        	$val = array(
                    					'merchantID' => $this->merchantID,
                    				);
        				
        			                  $ispaid = 1; $st ='paid';
                                 	  $bamount    = $in_data['BalanceRemaining']-$amount;
        							  if($bamount > 0)
        							  {
        							  $ispaid 	  = '0';
        							  $st         ='unpaid';
        							  }
        							  $app_amount = $in_data['AppliedAmount']+$amount;
        							  $up_data    = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount,'userStatus'=>$st,'isPaid'=>$ispaid,'TimeModified'=>date('Y-m-d H:i:s') );
        						  	  $condition=array('invoiceID'=>$invoiceID,'merchantID'=>$this->merchantID);
        						  	  
        						  	  $this->general_model->update_row_data('Freshbooks_test_invoice',$condition,$up_data);
               
                                
                                if(isset($this->accessToken) && isset($this->access_token_secret))
                                {
                                    if($this->fb_account==1)    
                                    {
                                        $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
                                   
                                      $payment = array('invoiceid'=>$invoiceID,'amount'=>array('amount'=>$amount),'type'=>'Check','date'=>date('Y-m-d'));
                                      $invoices   = $c->add_payment($payment);
                                    }
                                    else
                                    {
                                           $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
                                   
                                      $payment = '<?xml version="1.0" encoding="utf-8"?>  
                            						<request method="payment.create">  
                            						  <payment>         
                            						    <invoice_id>'.$invoiceID.'</invoice_id>               
                            						    <amount>'.$amount.'</amount>             
                            						    <currency_code>USD</currency_code> 
                            						    <type>Check</type>                   
                            						  </payment>  
                            						</request>';  
                            		  $invoices = $c->add_payment($payment);				
                                        
                                    }
                                    
                                    
                                
                        
                        			if ($invoices['status'] != 'ok') {
                        
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        			
                                        
                        				
                        			}
                        			else 
                        			{
                        			    $pinv_id =  $invoices['payment_id'];
                        				
            							 
            							 
            						          $this->session->set_flashdata('success','Successfully Processed Invoice'); 
                                 
                                   					}
									}	
									$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
    							  $ref_number =  $in_data['refNumber']; 
    							  $tr_date   =date('Y-m-d H:i:s');
    							  	$toEmail = $in_data['userEmail']; $company=$in_data['companyName']; $customer = $in_data['fullName'];  
                        		 if($chh_mail =='1')
    							 {
    							  
    							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
    							 }	
                               	 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
                                 	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
										$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
										$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
											$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
            				 		$card_type      =$this->general_model->getType($card_no);
                                 
                                 			$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										  'CardType'     =>$card_type,
            										  'CustomerCard' =>$card_no,
            										  'CardCVV'      =>$cvv, 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                									 'Billing_Addr2'	 =>$address2,	 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->process_card($card_data);	
                                 }
    		
                        			
					   }		
                        else
					   {
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error '.$result->response_reason_text.'</strong>.</div>'); 
					   	  
					   }   				
            							 
            					     
                        $id = $this->general_model->insert_gateway_transaction_data($result,'Paypal_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$this->merchantID,$pinv_id, $this->resellerID,$in_data['invoiceID'], false, $this->transactionByUser);  
                   
				
					   
					   } 
					   else
					   {
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error Payment Failed.</strong>.</div>'); 
					   	  
					   }  
		 
				
				 
			     	}
    				else
    				{
    					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
    					  
    				}
          
		
		 
    	    	}
    	    	else
    	    	{
    	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Validation Error</strong>.</div>'); 
    			 }
			 
        
			if($cusproID!="")
			{
				$trans_id = $PayPalResult['transactionid'];
				$invoice_IDs = array();
					$receipt_data = array(
						'proccess_url' => 'FreshBooks_controllers/Create_invoice/Invoice_details',
						'proccess_btn_text' => 'Process New Invoice',
						'sub_header' => 'Sale',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					$this->session->set_userdata("in_data",$in_data);
				if ($cusproID == "1") {
					redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
				}
			 	redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
		   	 else
		   	 {
				if(!$checkPlan){
					$responseId  = '';
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
				}
		    	 redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
		   	 }
	   
    
}     


	
	 	 
public function pay_invoiceoooo()
{
              if($this->session->userdata('logged_in') )
              {
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	} 
			        	
		            	$user_id = $merchID;
			        	
        		    	$rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$merchID));
        			 $resellerID =	$rs_daata['resellerID'];
        			 $condition1 = array('resellerID'=>$resellerID);
                	
                 	    $sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';	        	
			        	$val = array(
					'merchantID' => $merchID,
				);
				
				$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
            
            $subdomain     = $Freshbooks_data['sub_domain'];
            $key           = $Freshbooks_data['secretKey'];
            $accessToken   = $Freshbooks_data['accessToken'];
            $access_token_secret = $Freshbooks_data['oauth_token_secret'];
     
            define('OAUTH_CONSUMER_KEY', $subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);	
			        	
	     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 $cusproID=array(); $error=array();
         $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		 
		 
         $in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
         
				   if($cardID!="" || $gateway!=""){  
					 
				   
					if(!empty($in_data)){ 
					  
						$Customer_ListID = $in_data['CustomerListID'];
                      if($cardID=='new1')
                      {
			        	$cardID_upd  =$cardID;
			        		$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				        $merchantID = $merchID;
          
           
                         $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID' => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                           $this->db1->select('*')->from('customer_card_data');
                           $qq =  $this->db1->get();
          
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                         }else{
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
                      }
						   $card_data    =   $this->card_model->get_single_card_data($cardID);
					 if(!empty($card_data)){
							
				if( $in_data['BalanceRemaining'] > 0){
				    $cr_amount=0;
					      
							   $amount         =	 $this->czsecurity->xssCleanPostInput('inv_amount'); 
								
							$amount           = $amount-$cr_amount;
						   $creditCardType    = ($card_data['CardType'])?$card_data['CardType']:'Visa';
					  		
							$creditCardNumber = $card_data['CardNo'];
						    $expDateMonth     =  $card_data['cardMonth'];
							$expDateYear      = $card_data['cardYear'];
								
								
								// Month must be padded with leading zero
						$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
						$cvv2Number =   $card_data['CardCVV'];
							$currencyID     = "USD";
						  
                           
                           $firstName = $in_data['firstName'];
                            $lastName =  $in_data['lastName']; 
                            $companyName =  $in_data['companyName']; 
							 
							$phone = $in_data['phoneNumber']; 
							$email = $in_data['userEmail']; 
										
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
							
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
					 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
					 {  
					
					           $code = '111';
					
						 $ispaid 	 = 'true';
						
						 
					

        
                    if(isset($accessToken) && isset($access_token_secret))
                    {
                        $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
            
                        $payment = '<?xml version="1.0" encoding="utf-8"?>  
            						<request method="payment.create">  
            						  <payment>         
            						    <invoice_id>'.$invoiceID.'</invoice_id>               
            						    <amount>'.$amount.'</amount>             
            						    <currency_code>USD</currency_code> 
            						    <type>Check</type>                   
            						  </payment>  
            						</request>';
                         
                          $invoices = $c->add_payment($payment);
            
                        			if ($invoices['status'] != 'ok') 
                        			{
                        			
                        				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                        
                        				redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
                        				
                        			}
                        			else 
                        			{
                        				$this->session->set_flashdata('success','Success'); 
                        				
                        			
                        			}
            				}

					   } else{
					    $code = '401';
				     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
					   }  
					   
				
						
                       $transaction= array();
                         $tranID ='' ;$amt='0.00';
					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                       
				       $transaction['transactionID']       = $tranID;
					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
					   $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					   $transaction['transactionCode']     = $code;  
					    $transaction['transactionModified']    = date('Y-m-d H:i:s');  
						$transaction['transactionType']    = "Paypal_sale";	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transaction['customerListID']      = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   =$amt ;
					   $transaction['merchantID']   = $merchID;
					    $transaction['invoiceID']   = $invoiceID;
					     $transaction['qbListTxnID'] =   $invoices['payment_id'];
                        $transaction['resellerID']   = $resellerID;
				      $transaction['gateway']   = "Paypal";	
				      $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
				        if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
		  }
						
		    if($cusproID!=""){
			 	 redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');
		   	 }

    }     



	
	 	 
public function pay_multi_invoice()
{
                      if($this->session->userdata('logged_in') )
                      {
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	} 
			        	
		            	$user_id = $merchID;
			        	
        		    	$rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$merchID));
        			 $resellerID =	$rs_daata['resellerID'];
        			 $condition1 = array('resellerID'=>$resellerID);
                	
                 	    $sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
        			 $domain1 = $sub1['merchantPortalURL'];
        			 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';	        	
			        	
			        	$val = array(
					'merchantID' => $merchID,
				);
				
				$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);
            
            $subdomain     = $Freshbooks_data['sub_domain'];
            $key           = $Freshbooks_data['secretKey'];
            $accessToken   = $Freshbooks_data['accessToken'];
            $access_token_secret = $Freshbooks_data['oauth_token_secret'];
     
            define('OAUTH_CONSUMER_KEY', $subdomain);
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
        	
	 
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');
		 $checkPlan = check_free_plan_transactions();
		 
		 if($checkPlan && !empty($cardID) &&  !empty($gateway))
		 { 
	
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
         
				  
					     $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
						if(!empty($invoices))
					{   
					    foreach($invoices as $key=> $invoiceID)
					    {
					      $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);  
					        
                          $in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID,$merchID);
         		   
					if(!empty($in_data))
					{ 
					  
						$Customer_ListID = $in_data['CustomerListID'];
                      if($cardID=='new1')
                       {
			        	$cardID_upd  =$cardID;
			        		$c_data   = $this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				        $merchantID = $merchID;
          
           
                         $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID' => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                           $this->db1->select('*')->from('customer_card_data');
                           $qq =  $this->db1->get();
          
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', // $this->card_model->encrypt($cvv),
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                         }else{
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
						   $card_data    =   $this->card_model->get_single_card_data($cardID);
					 if(!empty($card_data)){
							
				if( $in_data['BalanceRemaining'] > 0){
				         	$cr_amount =0;
					        $amount 	    =	 $in_data['BalanceRemaining'];
					     
							$amount           =	$pay_amounts;
							$amount           = $amount-$cr_amount;
						   $creditCardType    =  ($card_data['CardType'])?$card_data['CardType']:'Visa';
							$creditCardNumber = $card_data['CardNo'];
						    $expDateMonth     =  $card_data['cardMonth'];
							$expDateYear      = $card_data['cardYear'];
								
								// Month must be padded with leading zero
						$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
								$cvv2Number =   $card_data['CardCVV'];
							$currencyID     = "USD";
						  
                           
                           $firstName = $in_data['firstName'];
                            $lastName =  $in_data['lastName']; 
                            $companyName =  $in_data['companyName']; 
							$address1 = $in_data['ShipAddress_Addr1']; 
                            $address2 = $in_data['ShipAddress_Addr2']; 
							$country  = $in_data['ShipAddress_Country']; 
							$city     = $in_data['ShipAddress_City'];
							$state    = $in_data['ShipAddress_State'];		
								$zip  = $in_data['ShipAddress_PostalCode']; 
								$phone = $in_data['phoneNumber']; 
								$email = $in_data['userEmail']; 
										
	                  	$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
	 					
	                	$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
                		$PayerInfo = array(
                							'email' => $email, 								// Email address of payer.
                							'payerid' => '', 							// Unique PayPal customer ID for payer.
                							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
                							'business' => '' 							// Payer's business name.
                						);  
                						
                		$PayerName = array(
                							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
                							'firstname' => $firstName, 							// Payer's first name.  25 char max.
                							'middlename' => '', 						// Payer's middle name.  25 char max.
                							'lastname' => $lastName, 							// Payer's last name.  25 char max.
                							'suffix' => ''								// Payer's suffix.  12 char max.
                						);
                					
                		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
							
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
					 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) 
					 {  
					
					           $code = '111';
					
						
        
        if(isset($accessToken) && isset($access_token_secret))
        {
            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

            $payment = '<?xml version="1.0" encoding="utf-8"?>  
						<request method="payment.create">  
						  <payment>         
						    <invoice_id>'.$invoiceID.'</invoice_id>               
						    <amount>'.$amount.'</amount>             
						    <currency_code>USD</currency_code> 
						    <type>Check</type>                   
						  </payment>  
						</request>';
             
              $invoices = $c->add_payment($payment);

			if ($invoices['status'] != 'ok') {
			
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');

				redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'));
				
			}
			else {
				$this->session->set_flashdata('success','Success'); 
				
			}
				}

					   } else{
					    $code = '401';
				     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
					   }  
					   
					
                       $transaction= array();
                         $tranID ='' ;$amt='0.00';
					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                       
				       $transaction['transactionID']       = $tranID;
					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
					   $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					   $transaction['transactionCode']     = $code;  
					    $transaction['transactionModified']    = date('Y-m-d H:i:s');  
						$transaction['transactionType']    = "Paypal_sale";	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transaction['customerListID']      = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   =$amt ;
					   $transaction['merchantID']   = $merchID;
					    $transaction['invoiceID']   = $invoiceID;
					     $transaction['qbListTxnID'] =   $invoices['payment_id'];
                        $transaction['resellerID']   = $resellerID;
				      $transaction['gateway']   = "Paypal";	
				       $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
				        if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
				}
          
		            }else{
	                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			            }
		
		 
	    	         }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
			 
				   }
				   }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Please select invoices</strong>.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
		  }

		if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
						
		redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details','refresh');

    }     




	
	  
  public function get_card_expiry_data($customerID){  
  
                       $card = array();
               		   $this->load->library('encrypt');
					
	   	  
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $customer_card['CardNo']  = substr($this->card_model->decrypt($card_data['CustomerCard']),12) ;
						 $customer_card['CardID'] = $card_data['CardID'] ;
						 $customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						 $card[$key] = $customer_card;
					   }
				}		
					
                return  $card;

     }
 
  public function get_card_edit_data(){
	  
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->card_model->get_single_card_data($cardID);
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
  }
  
  
  
  public function get_single_card_data($cardID){  
  
                  $card = array();
               	  $this->load->library('encrypt');

	   	    
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
				}		
					
					return  $card;

       }
 

		
	 
	 public function check_vault(){
		 
 	
		  $card=''; $card_name=''; $customerdata=array();
		    if($this->session->userdata('logged_in') )
              {
			           $merchantID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			        	} 
		  
		  
		 if($this->czsecurity->xssCleanPostInput('customerID')!=""){
			 
				
			 	$customerID 	= $this->czsecurity->xssCleanPostInput('customerID'); 
			
			 	$condition     =  array('ListID'=>$customerID); 
			    $customerdata = $this->general_model->get_row_data('qb_test_customer',$condition);
				if(!empty($customerdata)){
	                 				
   				  
				   	 $customerdata['status'] =  'success';	     
					
					 $card_data =   $this->card_model->get_card_expiry_data($customerID);
					$customerdata['card']  = $card_data;
				
					echo json_encode($customerdata)	;
					die;
			    } 	 
			 
	      }		 
		 
	 }		 

	  
	 public function view_transaction(){

				
				$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
        				 if($this->session->userdata('logged_in') )
                      {
			           $user_id = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $user_id = $this->session->userdata('user_logged_in')['merchantID'];
			        	} 
			
		        $transactions           = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id);
				
				if(!empty($transactions) )
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-center"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					<td class="hidden-xs text-right"><?php echo number_format($transaction['transactionAmount'],2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
<td class="text-right visible-lg"><?php if($transaction['transactionCode']=='300'){ ?> <span class="btn btn-alt1 btn-danger">Failed</span> <?php }else if($transaction['transactionCode']=='100'){ ?> <span class="btn btn-alt1 btn-success">Success</span><?php } ?></td>
					
				</tr>
				
		<?php     }

				}else{
					echo '<tr><td colspan="5" class="text-center">No record available</td></tr>';
				}
              die;				

      }	

	   
}

