<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
class MerchantUser extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		

		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('customer_model');
		$this->load->model('company_model');
		$this->db1 = $this->load->database('otherdb', true);
		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '3') {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
	}


	public function index()
	{

		redirect('FreshBooks_controllers/home', 'refresh');
	}

	/********** Get Admin Details ********/

	public function admin_role()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] = $this->session->userdata('logged_in');
			$user_id = $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id = $data['login_info']['merchantID'];
		}
		
		$roles        = $this->general_model->get_table_data('tbl_role', array('merchantID' => $user_id));

		$rolenew = array();
		$authnew = array();
		if (!empty($roles)) {
			foreach ($roles as $key => $role) {
				$auth = '';
				if (!empty($role['authID']))
					$auth = $this->general_model->get_auth_data($role['authID']);

				$role['authName'] = $auth;
				$authnew[$key]    = $role;
			}
		}
		$data['roles_data']      = $authnew;
		$rolename = $this->general_model->get_table_data('tbl_auth', '');
		$data['auths'] = $rolename;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/page_userrole_new', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/********** Add, Update and Edit Admin Roles ********/

	public function create_role()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('roleName', 'roleName', 'required');


		if ($this->form_validation->run() == true && !empty($this->czsecurity->xssCleanPostInput('role'))) {
			$input_data['roleName']	   = $this->czsecurity->xssCleanPostInput('roleName');

			$qq = implode(',', $this->czsecurity->xssCleanPostInput('role'));
			$input_data['roleName']  = $this->czsecurity->xssCleanPostInput('roleName');
			$input_data['authID'] = $qq;
			if ($this->session->userdata('logged_in')) {
				$merchantID  = $this->session->userdata('logged_in')['merchID'];
			} else if ($this->session->userdata('user_logged_in')) {
				$merchantID  = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$input_data['merchantID'] = $merchantID;

			if ($this->czsecurity->xssCleanPostInput('roleID') != "") {

				$id = $this->czsecurity->xssCleanPostInput('roleID');

				$condition = array('roleID' => $id);
				$updt = $this->general_model->update_row_data('tbl_role', $condition, $input_data);
				if ($updt) {
					$this->session->set_flashdata('success', 'Successfully Updated Role');
				} else {


					$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
				}
			} else {
				$insert =  $this->general_model->insert_row('tbl_role', $input_data);
				if ($insert) {
					$this->session->set_flashdata('success', 'Successfully Inserted Role');
				} else {


					$this->session->set_flashdata('message', '<Strong>Error:</strong> Something Is Wrong.');
				}
			}


			redirect(base_url('FreshBooks_controllers/MerchantUser/admin_role'));
		} else {
			$this->session->set_flashdata('message', 'Error: Validation Error.');
		}
		if ($this->uri->segment('3') != "") {

			$roleId = $this->uri->segment('3');
			$con = array('roleID' => $roleId);
			$data['role'] = $this->general_model->get_row_data('tbl_role', $con);
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] = $this->session->userdata('logged_in');
			$user_id = $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id = $data['login_info']['merchantID'];
		}

		$rolename = $this->general_model->get_table_data('tbl_auth', '');

		$data['auths'] = $rolename;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/page_userrole', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/********** Get Role ID ********/

	public function get_role_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('role_id');
		$val = array(
			'roleID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_role', $val);
		echo json_encode($data);
	}


	/********** Delete Admin Role ********/



	public function delete_role()
	{

		$roleID = $this->czsecurity->xssCleanPostInput('merchantroleID');
		$condition =  array('roleID' => $roleID);

		$del      = $this->general_model->delete_row_data('tbl_role', $condition);
		if ($del) {
			$this->session->set_flashdata('success', 'Successfully Deleted Role');
		} else {


			$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
		}

		redirect(base_url('FreshBooks_controllers/MerchantUser/admin_role'));
	}

	/********** Get Admin Users Records ********/
	public function admin_user()
	{


		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] = $this->session->userdata('logged_in');
			$user_id = $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id = $data['login_info']['merchantID'];
		}

		$users = $this->general_model->get_merchant_user_data($user_id);
		$usernew = array();
		foreach ($users as $key => $user) {
			$auth = $this->general_model->get_auth_data($user['authID']);
			$user['authName'] = $auth;
			$usernew[$key] = $user;
		}

		$data['user_data']  = $usernew;
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;



		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/admin_user', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/********** Add, Update and Edit Admin Users ********/

	public function create_user()
	{
		if ($this->session->userdata('logged_in')) {
            $data['login_info']      = $this->session->userdata('logged_in');
            $merchantID = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']      = $this->session->userdata('user_logged_in');
            $merchantID = $data['login_info']['merchantID'];
        }

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('userFname', 'userFname', 'required');
			$this->form_validation->set_rules('userLname', 'userLname', 'required');
			
			if ($this->form_validation->run() == true) {

				$input_data['userFname']	   = $this->czsecurity->xssCleanPostInput('userFname');
				$input_data['userLname']	   = $this->czsecurity->xssCleanPostInput('userLname');
				$input_data['userEmail']	   = $this->czsecurity->xssCleanPostInput('userEmail');

				$is_exist_data = [
					'email' => $input_data['userEmail']
				];

				if ($this->czsecurity->xssCleanPostInput('userID') == "") {
					$input_data['userPasswordNew'] = password_hash(
                        $this->czsecurity->xssCleanPostInput('userPassword'),
                        PASSWORD_BCRYPT
                    );
				} else {
					$is_exist_data['merchantUserID'] = $this->czsecurity->xssCleanPostInput('userID');
				}

				$input_data['userAddress']	   = $this->czsecurity->xssCleanPostInput('userAddress');
				$input_data['roleId']          = $this->czsecurity->xssCleanPostInput('roleID');
				$input_data['merchantID']	   = $merchantID;

				$is_exist = is_email_exists($is_exist_data);
				if ($is_exist) {
					$this->session->set_flashdata('message', '<strong>Error:</strong> ' . $is_exist);
				} else {
					if ($this->czsecurity->xssCleanPostInput('userID') != "") {

						$id = $this->czsecurity->xssCleanPostInput('userID');
						$chk_condition = array('merchantUserID' => $id, 'merchantID' => $merchantID);
						$updt = $this->general_model->update_row_data('tbl_merchant_user', $chk_condition, $input_data);
						if ($updt) {
							$this->session->set_flashdata('success', 'Successfully Updated User');
						} else {
							$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
						}
					} else {
						$insert = $this->general_model->insert_row('tbl_merchant_user', $input_data);
						if ($insert) {
							$this->session->set_flashdata('success', 'Successfully Created New User');
						} else {
							$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
						}
					}
				}

				redirect(base_url('FreshBooks_controllers/MerchantUser/admin_user'));
			}
		}

		if ($this->uri->segment('4')) {
			$userID  			  = $this->uri->segment('4');
			$con                = array('merchantUserID' => $userID, 'merchantID' => $merchantID);
			$data['user'] 	  = $this->general_model->get_row_data('tbl_merchant_user', $con);
			if(!$data['user'] || empty($data['user'])){
				$this->session->set_flashdata('error', 'Invalid Request');
				redirect(base_url('FreshBooks_controllers/MerchantUser/admin_user'));
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] = $this->session->userdata('logged_in');
			$user_id = $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id = $data['login_info']['merchantID'];
		}

		$username = $this->general_model->get_table_data('tbl_auth', '');
		$data['auths'] = $username;

		$con1                = array('merchantID' => $user_id);
		$role = $this->general_model->get_table_data('tbl_role', $con1);

		$data['role_name'] = $role;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/adduser', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/**************Delete Admin Users********************/

	public function delete_user()
	{

		$userID = $this->czsecurity->xssCleanPostInput('merchantID1');
		$condition =  array('merchantUserID' => $userID);
		$del      = $this->general_model->delete_row_data('tbl_merchant_user', $condition);
		if ($del) {
			$this->session->set_flashdata('success', 'Successfully Deleted');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> MerchantID not found.</div>');
		}

		redirect(base_url('FreshBooks_controllers/MerchantUser/admin_user'));
	}







	public function create_customer()
	{
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('firstName', 'firstName', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'lastName', 'required|xss_clean');
			$this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');
			$this->form_validation->set_rules('companyName', 'comapanyName', 'required|xss_clean');

			$this->form_validation->set_rules('userEmail', 'userEmail', 'required|xss_clean');
			$this->form_validation->set_rules('companyCountry', 'companyCountry', 'required|xss_clean');
			$this->form_validation->set_rules('companyState', 'companyState', 'companyState|xss_clean');
			$this->form_validation->set_rules('companyCity', 'companyCity', 'companyCity|xss_clean');
			$this->form_validation->set_rules('address1', 'address1', 'required|xss_clean');
			$this->form_validation->set_rules('phone', 'Contact No.', 'required');


			if ($this->form_validation->run() == true) {

				$input_data['firstName']   = $this->czsecurity->xssCleanPostInput('firstName');
				$input_data['lastName']	   = $this->czsecurity->xssCleanPostInput('lastName');
				$input_data['fullName']	   = $this->czsecurity->xssCleanPostInput('fullName');
				$input_data['userEmail']   = $this->czsecurity->xssCleanPostInput('userEmail');
				$input_data['country']	   = $this->czsecurity->xssCleanPostInput('companyCountry');
				$input_data['state']	   = $this->czsecurity->xssCleanPostInput('companyState');
				$input_data['city']	       = $this->czsecurity->xssCleanPostInput('companyCity');
				$input_data['companyName']	       = $this->czsecurity->xssCleanPostInput('companyName');
				$input_data['isActive']   = 'true';
				
				$input_data['zipcode']	  = $this->czsecurity->xssCleanPostInput('zipCode');

				$input_data['ship_country']	   = $this->czsecurity->xssCleanPostInput('sCountry');
				$input_data['ship_state']	   = $this->czsecurity->xssCleanPostInput('sState');
				$input_data['ship_city']	       = $this->czsecurity->xssCleanPostInput('sCity');
				$input_data['ship_address1']	   = $this->czsecurity->xssCleanPostInput('saddress1');
				$input_data['ship_address2']    = $this->czsecurity->xssCleanPostInput('saddress2');
				$input_data['ship_zipcode']	  = $this->czsecurity->xssCleanPostInput('szipCode');



				$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' =>	$user_id));
				$user = 	$comp_data['qbwc_username'];
				$input_data['phoneNumber'] = $this->czsecurity->xssCleanPostInput('phone');

				$input_data['address1']	   = $this->czsecurity->xssCleanPostInput('address1');
				$input_data['address2']    = $this->czsecurity->xssCleanPostInput('address2');
				$input_data['merchantID']   = $user_id;
				$input_data['createdat']    = date('Y-m-d H:i:s');
				$c_List   				   = $this->czsecurity->xssCleanPostInput('customerListID');
				$input_data['ListID']       = $c_List;
				if ($c_List != "") {
					$exist_row = $this->general_model->get_row_data('tbl_custom_customer', array('ListID' => $c_List));
					if (!empty($exist_row)) {

						$input_data['EditSequence'] =  $this->czsecurity->xssCleanPostInput('EditSequence');
						$cusID =   $this->general_model->update_row_data('tbl_custom_customer', array('ListID' => $c_List), $input_data);
						$cusID = $exist_row['customerID'];
						$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $cusID, '', '', $user);
						$this->session->set_flashdata('success', 'Successfully Updated Customer');
					} else {
						$input_data['EditSequence'] =  $this->czsecurity->xssCleanPostInput('EditSequence');
						$cusID =  $this->general_model->insert_row('tbl_custom_customer', $input_data);
						$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $cusID, '', '', $user);
						$this->session->set_flashdata('success', 'Successfully Inserted Customer');
					}
					
				} else {

					$cusID = 	 $this->general_model->insert_row('tbl_custom_customer', $input_data);
					$this->quickbooks->enqueue(QUICKBOOKS_ADD_CUSTOMER, $cusID, '', '', $user);
					$this->session->set_flashdata('success', 'Successfully Inserted Customer');
				}
				if ($cusID) {

					
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
				}
				redirect(base_url('home/customer'));
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:  Validation errors</strong>.</div>');
				redirect(base_url('home/index'));
			}
		}

		if ($this->uri->segment('3')) {
			$userID  		  = $this->uri->segment('3');
			$con                = array('ListID' => $userID);
			$data['customer']   = $this->general_model->get_row_data('qb_test_customer', $con);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info']     = $this->session->userdata('logged_in');

		$country = $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;
		$company = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
		$data['companys'] = $company;

		$state = $this->general_model->get_table_data('state', '');
		$data['state_datas'] = $state;

		$city = $this->general_model->get_table_data('city', '');
		$data['city_datas'] = $city;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_customer', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function delete_customer()
	{


		if ($this->czsecurity->xssCleanPostInput('status') != "") {
			$customerID  = $this->czsecurity->xssCleanPostInput('custactiveID');
			$status      = 'true';
		} else {

			$customerID  = $this->czsecurity->xssCleanPostInput('custID');
			$status      = 'false';
		}

		$customer    = $this->customer_model->customer_by_id($customerID);
		$user       = $customer->qbwc_username;
		$input_data['firstName']   = $customer->FirstName;
		$input_data['lastName']	   = $customer->LastName;
		$input_data['fullName']	   = $customer->FullName;
		$input_data['userEmail']   = $customer->Contact;
		$input_data['ListID']      = $customerID;
		$input_data['isActive']    = $status;
		$input_data['companyName'] =	$customer->companyName;
		$input_data['companyID']   =   $customer->companyID;

		$input_data['phoneNumber'] = $customer->Phone;
		$input_data['EditSequence'] =  $customer->EditSequence;
		$exist_row = $this->general_model->get_row_data('tbl_custom_customer', array('ListID' => $customerID));
		if (!empty($exist_row)) {
			$cusID =   $this->general_model->update_row_data('tbl_custom_customer', array('ListID' => $customerID), $input_data);
			$cusID = $exist_row['customerID'];
			$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $cusID, '', '', $user);
		} else {
			$cusID =  $this->general_model->insert_row('tbl_custom_customer', $input_data);
			$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $cusID, '', '', $user);
		}






		$condition   = array('ListID' => $customerID);
		$update_data = array('customerStatus' => '0', 'isActive' => 'false');
		if ($this->general_model->update_row_data('qb_test_customer', $condition, $update_data)) {

			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: In delete process</strong>.</div>');
		}

		redirect('home/customer', 'refresh');
	}





	public function create_product()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('productName', 'productName', 'required|xss_clean');

			$this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');

			$this->form_validation->set_rules('type', 'type', 'required|xss_clean');
			$this->form_validation->set_rules('company', 'company', 'required|xss_clean');

			$this->form_validation->set_rules('quantityonhand', 'Quantity', 'required|xss_clean');
			$this->form_validation->set_rules('averageCost', 'averageCost', 'required|xss_clean');
			$this->form_validation->set_rules('proDescription', 'proDescription', 'required|xss_clean');


			if ($this->form_validation->run() == true) {

				$input_data['productName']         = $this->czsecurity->xssCleanPostInput('productName');
				$input_data['productFullName']	   		   = $this->czsecurity->xssCleanPostInput('fullName');
				$input_data['productType']	   	   = $this->czsecurity->xssCleanPostInput('type');
				$input_data['companyID']   		   = $this->czsecurity->xssCleanPostInput('company');
				$comp_data = $this->general_model->get_row_data('tbl_company', array('id' =>	$input_data['companyID']));
				$user = 	$comp_data['qbwc_username'];

				$input_data['parentID']	  		   = $this->czsecurity->xssCleanPostInput('productParent');
				$input_data['productQty']	       = $this->czsecurity->xssCleanPostInput('productQty');
				$input_data['productPrice']	       = $this->czsecurity->xssCleanPostInput('averageCost');

				$input_data['productDescription']         = $this->czsecurity->xssCleanPostInput('proDescription');
				$itemType = trim($this->czsecurity->xssCleanPostInput('type'));

				$input_data['merchantID']   = $user_id;
				$input_data['createdat']    = date('Y-m-d H:i:s');

				$p_List   				   = $this->czsecurity->xssCleanPostInput('productID');
				$input_data['productListID']       = $p_List;
				if ($p_List != "") {
					$exist_row = $this->general_model->get_row_data('tbl_custom_product', array('productListID' => $p_List));


					if (!empty($exist_row)) {
						$proID =  $this->general_model->update_row_data('tbl_custom_product', array('productListID' => $p_List), $input_data);
						$proID =  $exist_row['productID'];
					} else {
						$proID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
					}

					$itemType = $exist_row['productType'];
					if ($itemType == "NonInventory") {
						$this->quickbooks->enqueue(QUICKBOOKS_MOD_NONINVENTORYITEM,  $proID, '', '', $user);
					}

					if ($itemType == "Service") {
						$this->quickbooks->enqueue(QUICKBOOKS_MOD_SERVICEITEM,  $proID, '', '', $user);
					}
				} else {
					$proID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
					if ($proID) {
						if ($itemType == "NonInventory") {
							$this->quickbooks->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM,  $proID, '', '', $user);
						}

						if ($itemType == "Service") {
							$this->quickbooks->enqueue(QUICKBOOKS_ADD_SERVICEITEM,  $proID, '', '', $user);
						}
						$this->session->set_flashdata('success', 'Success');
					} else {

						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
					}
					redirect(base_url('home/index'));
				}
			}
		}

		if ($this->uri->segment('3')) {
			$userID  			  = $this->uri->segment('3');
			$con                = array('ListID' => $userID);
			$data['item_pro'] 	  = $this->general_model->get_row_data('qb_test_item', $con);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$company = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
		$data['companys'] = $company;

		$parent = $this->company_model->get_product_data(array('merchantID' => $user_id));
		$data['products'] = $parent;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_product', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}





	public function change_product_status()
	{


		if ($this->czsecurity->xssCleanPostInput('status') == "") {
			$productID    = $this->czsecurity->xssCleanPostInput('prolistID');
			$status       = 'false';
		}
		if ($this->czsecurity->xssCleanPostInput('status') == "1") {

			$productID   = $this->czsecurity->xssCleanPostInput('proactiveID');
			$status      = 'true';
		}

		$product_data    = $this->general_model->get_row_data('qb_test_item', array('ListID' => $productID));


		$input_data['productName']         = $product_data['Name'];
		$input_data['productFullName']	   = $product_data['FullName'];
		$input_data['productType']	   	   = $product_data['Type'];
		$input_data['IsActive']	   	       = $status;
		$input_data['companyID']   		   = $product_data['companyListID'];
		$comp_data    = $this->general_model->get_row_data('tbl_company', array('id' =>	$input_data['companyID']));
		$user = 	$comp_data['qbwc_username'];

		$input_data['parentID']	  		   = $product_data['Parent_ListID'];
		$itemType 						   = trim($product_data['Type']);

		$input_data['merchantID']       = $comp_data['merchantID'];
		$input_data['createdat']        = date('Y-m-d H:i:s');

		$input_data['productListID']        = $productID;
		$input_data['EditSequence']       =  $product_data['EditSequence']; 
		$exist_row = $this->general_model->get_row_data('tbl_custom_product', array('productListID' => $productID));
		if (!empty($exist_row)) {
			$prodID =   $this->general_model->update_row_data('tbl_custom_product', array('productID' => $exist_row['productID']), $input_data);
			$prodID = $exist_row['productID'];
		} else {
			$prodID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
		}

		if ($prodID) {
			$itemType = $exist_row['productType'];
			if ($itemType == "NonInventory") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_NONINVENTORYITEM,  $prodID, '', '', $user);
			}

			if ($itemType == "Service") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_SERVICEITEM,  $prodID, '', '', $user);
			}

			$condition   = array('ListID' => $productID);
			$update_data = array('IsActive' => $status);
			if ($this->general_model->update_row_data('qb_test_item', $condition, $update_data)) {

				$this->session->set_flashdata('success', 'Successfully Updated');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: In delete process.</strong></div>');
			}
		}

		redirect('home/index', 'refresh');
	}


	public function recover_pwd()
	{
		if ($this->session->userdata('logged_in')) {

			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}


		$id = $this->uri->segment('4');
		$results = $this->general_model->get_select_data('tbl_merchant_user', array('userFname', 'userLname', 'userEmail', 'merchantID'), array('merchantUserID' => $id));
		$res = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID', 'firstName', 'lastName','merchantEmail','merchantContact'), array('merchID' => $results['merchantID']));
		$res_data = $this->general_model->get_select_data('tbl_reseller', array('resellerfirstName', 'lastName', 'resellerCompanyName', 'resellerEmail'), array('resellerID' => $res['resellerID']));
		$Merchant_url = $this->general_model->get_select_data('Config_merchant_portal', array('merchantPortalURL'), array('resellerID' => $res['resellerID']));

		if (!empty($results)) {
			$name = $results['userFname'];
			$login_url = $Merchant_url['merchantPortalURL'];
			$toEmail = $results['userEmail'];
			$this->load->helper('string');
			$password = random_string('alnum', 16);
			$marchant_name = $res['firstName'] . ' ' . $res['lastName'];
			$condition = array('merchantUserID' => $id);
			$data = array(
				'userPasswordNew' => password_hash(
                    $password,
                    PASSWORD_BCRYPT
                )
			);
			$update = $this->general_model->update_row_data('tbl_merchant_user', $condition, $data);
			if ($update) {
				$temp_data = $this->general_model->get_row_data('tbl_email_template', array('merchantID' => $results['merchantID'], 'templateType' => '14'));
				$message = $temp_data['message'];
				$subject = $temp_data['emailSubject'];
				if ($temp_data['fromEmail'] != '') {
					$fromEmail = $temp_data['fromEmail'];
				} else {

					$fromEmail = $res_data['resellerEmail'];
				}

				$mailDisplayName = $marchant_name;
				if ($temp_data['mailDisplayName'] != '') {
					$mailDisplayName = $temp_data['mailDisplayName'];
				}
				$logo_url ='';
				$config_data = $this->general_model->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
				if(!empty($config_data)){
			   		$logo_url  = $config_data['ProfileImage']; 
				}
				if( !empty( $config_data['ProfileImage'])){
				   	$logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  
				}else{
				  	$logo_url = CZLOGO; 
				}
    
    			$logo_url=	"<img src='".$logo_url."' />";
				$login_url = '<a href=' . $login_url . '>' . $login_url . '<a/>';
				$message = stripslashes(str_replace('{{userFname}}', $name, $message));
				$message = stripslashes(str_replace('{{login_url}}', $login_url, $message));
				$message = stripslashes(str_replace('{{userEmail}}', $toEmail, $message));
				$message = stripslashes(str_replace('{{userPassword}}', $password, $message));
				$message = stripslashes(str_replace('{{merchant_name}}', $marchant_name, $message));

				$message = stripslashes(str_replace('{{merchant_email}}', $res['merchantEmail'], $message));
				$message = stripslashes(str_replace('{{merchant_phone}}', $res['merchantContact'], $message));
				$message = stripslashes(str_replace('{{logo}}',$logo_url ,$message ));


				if($temp_data['replyTo'] != ''){
					$replyTo = $temp_data['replyTo'];
				}else{
					$replyTo = $results['userEmail'];
				}
				$addCC      = $temp_data['addCC'];
				$addBCC		= $temp_data['addBCC'];
				$email_data          = array(
					'customerID'=>'',
					'merchantID'=>$user_id, 
					'emailSubject'=>$subject,
					'emailfrom'=>$fromEmail,
					'emailto'=>$toEmail,
					'emailcc'=>$addCC,
					'emailbcc'=>$addBCC,
					'emailreplyto'=>$replyTo,
					'emailMessage'=>$message,
					'emailsendAt'=> date('Y-m-d H:i:s'),
					
				);

				$mail_sent = $this->general_model->sendMailBySendgrid($toEmail,$subject,$fromEmail,$mailDisplayName,$message,$replyTo, $addCC, $addBCC);

				if($mail_sent){
					$email_data['send_grid_email_id'] = $mail_sent;
					$email_data['send_grid_email_status'] = 'Sent';
					$this->general_model->insert_row('tbl_template_data', $email_data);
					$this->session->set_flashdata('message', '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Password Reset request has been sent. If your email is listed with an account, an email will be sent to you.</div>');
				} else {
					$email_data['send_grid_email_status'] = 'Failed';
					$email_data['mailStatus']=0;
					$this->general_model->insert_row('tbl_template_data', $email_data);
					$this->session->set_flashdata('message', '<div class="alert alert-danger">Email was not sent, please contact your administrator.</div>');
				}
			}
			redirect(base_url() . 'FreshBooks_controllers/MerchantUser/admin_user');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Email was not sent, please contact your administrator.</div>');
			redirect(base_url() . 'FreshBooks_controllers/MerchantUser/admin_user');
		}
	}

}
