<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tax extends CI_Controller {
	
	private $resellerID;
    private $merchantID;
    public function __construct()
    {
        parent::__construct();
        
        include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        $this->db1 = $this->load->database('otherdb', true); 
     
        
          if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='3' )
		  {
		    $this->merchantID = $this->session->userdata('logged_in')['merchID'];
		    $this->resellerID = $this->session->userdata('logged_in')['resellerID'];
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		      
		       $merchID = $this->session->userdata('user_logged_in')['merchantID'];
		       $this->merchantID = $merchID;
		       $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID'=>$merchID));
		       $this->resellerID = $rs_Data['resellerID'];
		 
		  }
		  else
		  {
			redirect('login','refresh');
		  }
		  
		  
		   $val = array(
             'merchantID' => $this->merchantID,
             );
		  	$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		  	if(empty($Freshbooks_data))
		  	{
		  	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>'); 
                
		  	  redirect('FreshBooks_controllers/home/index','refresh');
		  	  
		  	}
            $this->subdomain     = $Freshbooks_data['sub_domain'];
             $key                = $Freshbooks_data['secretKey'];
            $this->accessToken   = $Freshbooks_data['accessToken'];
            $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
            $this->fb_account    = $Freshbooks_data['accountType'];
            $condition1 = array('resellerID'=>$this->resellerID);
           	$sub1 = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
			$domain1 = $sub1['merchantPortalURL'];
			$URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';  
            
            define('OAUTH_CONSUMER_KEY', $this->subdomain );
            define('OAUTH_CONSUMER_SECRET', $key);
            define('OAUTH_CALLBACK', $URL1);
	}
	
	

	public function index()
	{   
			redirect('FreshBooks_controllers/home','refresh');
	}
	
     public function tax_list()
	{   
	   
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
			$data['login_info'] = $this->session->userdata('logged_in');
			$user_id = $data['login_info']['merchID'];
		
		}	
		else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id = $data['login_info']['merchantID'];
		}
		$condition = array('merchantID'=>$user_id);
		 	 if(isset($this->accessToken) && isset($this->access_token_secret))
        {
            if($this->fb_account==1)
            {
                $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                 $taxes = $c->get_taxes_data();
                 
                 
                 
                   foreach ($taxes as $oneTax) {
           
              	$fb_tax_details['taxID'] = $oneTax->taxid;
		        $fb_tax_details['friendlyName'] = $this->db->escape_str($oneTax->name);
		        $fb_tax_details['taxRate'] = $this->db->escape_str($oneTax->amount);
		        $fb_tax_details['updatedAt'] = $this->db->escape_str($oneTax->updated);
		        $fb_tax_details['createdAt'] = date('Y-m-d H:i:s');
		        $fb_tax_details['number'] = $this->db->escape_str($oneTax->number);
		        $fb_tax_details['merchantID'] = $this->merchantID;
		        $fb_tax_details['companyID'] = $this->db->escape_str($oneTax->accounting_systemid);
		        
		        if($this->general_model->get_num_rows('tbl_taxes_fb', array('taxID'=>$oneTax->taxid,'merchantID'=>$this->merchantID))>0)
		        {
		             $this->general_model->update_row_data('tbl_taxes_fb', array('taxID'=>$oneTax->taxid,'merchantID'=>$this->merchantID), $fb_tax_details);
		        }
		        else
		        $this->general_model->insert_row('tbl_taxes_fb', $fb_tax_details);
              }
            }
            else
            {
                $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                $taxes = $c->get_taxlist(); 
                  foreach ($taxes as $oneTax) {
           
              	$fb_tax_details['taxID'] = $oneTax->tax_id;
		        $fb_tax_details['friendlyName'] = $this->db->escape_str($oneTax->name);
		        $fb_tax_details['taxRate'] = $this->db->escape_str($oneTax->rate);
		        $fb_tax_details['date'] = $this->db->escape_str($oneTax->updated);
		        $fb_tax_details['number'] = $this->db->escape_str($oneTax->number);
		        $fb_tax_details['merchantID'] = $merchID;
		        
		        $this->general_model->insert_row('tbl_taxes_fb', $fb_tax_details);
              }
            }
         

        }
			  $taxname = $this->general_model->get_table_data('tbl_taxes_fb',$condition);
			  $data['taxes'] = $taxname;
			 
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('FreshBooks_views/taxes', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	}
	

	 public function create_taxes()
 
	{
	  if(!empty($this->input->post(null, true))){
	 
	   $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('friendlyName', 'Friendly Name', 'required|xss_clean');
		$this->form_validation->set_rules('taxRate', 'Tax Rate', 'required|xss_clean');
		
		if ($this->form_validation->run() == true)
		{
			$data['login_info'] = $this->session->userdata('logged_in');
		    $user_id    = $data['login_info']['merchID'];
			$merchID = $user_id;
			$val = array(
				'merchantID' => $merchID,
			);
			$condition =  array('merchantID'=>$merchID);

			    	$input_data['friendlyName']	   = $this->czsecurity->xssCleanPostInput('friendlyName');
        			$input_data['taxRate']	   = $this->czsecurity->xssCleanPostInput('taxRate');
        			$input_data['taxNumber']	   = $this->czsecurity->xssCleanPostInput('taxNumber');
        			$input_data['createdAt'] = date('Y-m-d H:i:s');

		
			if(isset($this->accessToken) && isset($this->access_token_secret))
        	{
                   
					if($this->czsecurity->xssCleanPostInput('taxID')!="" )
					{
				      
			        $id = $this->czsecurity->xssCleanPostInput('taxID');
               
        		
	
        	    if($this->fb_account==1)
        	    { $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
             
        	         $tax = array('taxid'=>$id,'name'=>$input_data['friendlyName'],'amount'=>$input_data['taxRate'],'number'=>$input_data['taxNumber']) ; 
						 
        	    }
        	    else
        	    {
                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
        
                    $tax = '<?xml version="1.0" encoding="utf-8"?>  
        						<request method="tax.update">  
        						  <tax>  
						  	<tax_id>'.$id.'</tax_id>    
						    <name>'.$input_data['friendlyName'].'</name>                       
						    <rate>'.$input_data['taxRate'].'</rate>                       
						    <number>'.$input_data['taxNumber'].'</number>               
						  </tax>  
						</request>  ';
        	    }									
            $invoices = $c->update_taxs($tax);
            
            $this->session->set_flashdata('success', 'Successfully Updated');
            }		
			    	
				
					else
					{
					

           
        	    if($this->fb_account==1)
        	    { $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
             $id='';
        	         $tax = array('taxid'=>$id,'name'=>$input_data['friendlyName'],'amount'=>$input_data['taxRate'],'number'=>$input_data['taxNumber']) ; 
				
        	    }
        	    else
        	    {
                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
        
                    $tax = '<?xml version="1.0" encoding="utf-8"?>  
        						<request method="tax.update">  
        						  <tax>  
						  	   
						    <name>'.$input_data['friendlyName'].'</name>                       
						    <rate>'.$input_data['taxRate'].'</rate>                       
						    <number>'.$input_data['taxNumber'].'</number>               
						  </tax>  
						</request>  ';
        	    }
            $invoices = $c->add_taxs($tax);
            
            $this->session->set_flashdata('success', 'Your Information Stored successfully');	
            }					 
					 
				}
				
			
		}
			redirect(base_url('FreshBooks_controllers/Tax/tax_list'));
		
	}
			
 }
 
 
	public function get_tax_id()
    {
		
        $id = $this->czsecurity->xssCleanPostInput('taxID');
		$val = array(
		'taxID' => $id,
		);
		
		$data = $this->general_model->get_row_data('tbl_taxes_fb',$val);
        echo json_encode($data);
    }
	
	
	
   public function delete_tax(){
	
	
	 	$id = $this->czsecurity->xssCleanPostInput('tax_id');
	 
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];
		$merchID = $user_id;
		$val = array(
			'merchantID' => $merchID,
		);

		$Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
       
        
        $subdomain     = $Freshbooks_data['sub_domain'];
        $key           = $Freshbooks_data['secretKey'];
        $accessToken   = $Freshbooks_data['accessToken'];
        $access_token_secret = $Freshbooks_data['oauth_token_secret'];
    
        define('OAUTH_CONSUMER_KEY', $subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', 'http://chargezoom.net/FreshBooks_controllers/Freshbooks_integration/auth');
        		
			if(isset($accessToken) && isset($access_token_secret))
        	{
            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, NULL, $subdomain, $accessToken, $access_token_secret);

            $tax = '<?xml version="1.0" encoding="utf-8"?>  
					<request method="tax.delete">  
					  <tax_id>'.$id.'</tax_id>  
					</request>';
												
            $taxdel = $c->add_taxs($tax);

         if ($taxdel != null) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Somthing Is Wrong...</div>');  
			}
		else{
           		$this->session->set_flashdata('success', 'Successfully Deleted');             
	  		}	  
	  
      	   }
		
	  	redirect(base_url('FreshBooks_controllers/Tax/tax_list'));
    	 
	
	}
	
	
	
}