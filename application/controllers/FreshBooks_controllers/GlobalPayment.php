<?php
/**
 * This Controller has Authorize.net Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 
 */

require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\PaymentMethods\ECheck;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

class GlobalPayment extends CI_Controller
{

	private $resellerID;
	private $transactionByUser;

	public function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Freshbooks.php';
		include APPPATH . 'third_party/FreshbooksNew.php';

		$this->load->config('globalpayments');
		$this->load->model('quickbooks');
		$this->load->model('customer_model');
		$this->load->model('card_model');
		$this->load->library('form_validation');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('Freshbook_models/fb_customer_model');
		$this->db1 = $this->load->database('otherdb', true);
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
			$logged_in_data = $this->session->userdata('logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
			$this->merchantID = $logged_in_data['merchID'];
			$this->resellerID = $logged_in_data['resellerID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
			$this->merchantID = $merchID;
		} else {
			redirect('login', 'refresh');
		}


		$val = array(
			'merchantID' => $this->merchantID,
		);
		$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		if (empty($Freshbooks_data)) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');

			redirect('FreshBoolks_controllers/home/index', 'refresh');
		}
	}


	public function index()
	{
		redirect('FreshBooks_controllers/home', 'refresh');
	}




	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");

		if ($this->session->userdata('logged_in')) {


			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
		$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
		$this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');

		if($this->czsecurity->xssCleanPostInput('sch_method') != 2){
            $this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
        }

		if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {

			$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
			$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
			$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
		}

        $checkPlan = check_free_plan_transactions();
        $pay_option = $this->czsecurity->xssCleanPostInput('sch_method');
        $echeck_payment = false;
        if($pay_option == 2){
            $echeck_payment = true;
        } 
		if ($checkPlan && $this->form_validation->run() == true) {

			$cusproID = '';
			$error = '';
			$cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
			$cardID_upd = '';

			$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');

			
			$cardID = $this->czsecurity->xssCleanPostInput('CardID');
			if (!$cardID || empty($cardID)) {
			$cardID = $this->czsecurity->xssCleanPostInput('schCardID');
			}
	
			$gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
			if (!$gatlistval || empty($gatlistval)) {
			$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
			}
			$gateway = $gatlistval;

			$amount               = $this->czsecurity->xssCleanPostInput('inv_amount');

			$in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;

			$ref_number = array();
			if (!empty($in_data)) {
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
				$customerID = $in_data['CustomerListID'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
				$c_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
				$secretApiKey   = $gt_result['gatewayPassword'];

				if ($cardID != "new1") {
					$card_data =   $this->card_model->get_single_card_data($cardID);
					$card_no  = $card_data['CardNo'];
					$expmonth =  $card_data['cardMonth'];
					$exyear   = $card_data['cardYear'];
					$cvv      = $card_data['CardCVV'];
					$cardType = $card_data['CardType'];
					$address1 = $card_data['Billing_Addr1'];
					$city     =  $card_data['Billing_City'];
					$zipcode  = $card_data['Billing_Zipcode'];
					$state    = $card_data['Billing_State'];
					$country  = $card_data['Billing_Country'];
					$accountNumber  = $card_data['accountNumber'];
                    $routeNumber  = $card_data['routeNumber'];
                    $accountName  = $card_data['accountName'];
                    $secCodeEntryMethod  = $card_data['secCodeEntryMethod'];
                    $accountType  = $card_data['accountType'];
                    $accountHolderType  = $card_data['accountHolderType'];
				} else {
					$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$cardType = $this->general_model->getType($card_no);
					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('cvv') != "")
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');

					$accountNumber  = $this->czsecurity->xssCleanPostInput('acc_number');
                    $routeNumber  = $this->czsecurity->xssCleanPostInput('route_number');
                    $accountName  = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCodeEntryMethod  = $this->czsecurity->xssCleanPostInput('secCode');
                    $accountType  = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                    $accountHolderType  = $this->czsecurity->xssCleanPostInput('acct_type');
				}
				$crtxnID = '';

				$config = new PorticoConfig();

				$config->secretApiKey = $secretApiKey;
				$config->serviceUrl =  $this->config->item('GLOBAL_URL');



				ServicesContainer::configureService($config);
				$card = new CreditCardData();
				$card->number = $card_no;
				$card->expMonth = $expmonth;
				$card->expYear = $exyear;
				if ($cvv != "")
					$card->cvn = $cvv;
				$card->cardType = $cardType;

				$address = new Address();
				$address->streetAddress1 = $address1;
				$address->city = $city;
				$address->state = $state;
				$address->postalCode = $zipcode;
				$address->country = $country;

				$refNum = array();
				$ref_numbeer = '';
				$invNo  = mt_rand(1000000, 2000000);
				try {
					if($pay_option == 2){
                        $check = new ECheck();
                        $check->accountNumber = $accountNumber;
                        $check->routingNumber = $routeNumber;
                        if(strtolower($accountType) == 'checking'){
                            $check->accountType = 0;
                        }else{
                            $check->accountType = 1;
                        }

                        if(strtoupper($accountHolderType) == 'PERSONAL'){
                            $check->checkType = 0;
                        }else{
                            $check->checkType = 1;
                        }
                        $check->checkHolderName = $accountName;
                        $check->secCode = "WEB";

                        $response = $check->charge($amount)
                        ->withCurrency(CURRENCY)
                        ->withAddress($address)
                        ->withInvoiceNumber($invNo)
                        ->withAllowDuplicates(true)
                        ->execute();
                    }else{

						$response = $card->charge($amount)
							->withCurrency('USD')
							->withAddress($address)
							->withInvoiceNumber($invNo)
							->withAllowDuplicates(true)
							->execute();
                    }

                    if($response->responseCode != 0 && $response->responseCode != '00')
                    {
                        $error='Gateway Error. Invalid Account Details';
                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
                        if($cusproID=="2"){
                            redirect('FreshBooks_controllers/Freshbooks_Customer/'.$customerID);
                        }else if($cusproID=="3" ){
                            redirect('FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/'.$in_data['invoiceID']);
                        }else{
                            redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details');
                        }
                    }

					if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
						if($pay_option != 2){

							// add level three data
	                        $transaction = new Transaction();
	                        $transaction->transactionReference = new TransactionReference();
	                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
	                        $level_three_request = [
	                            'card_no' => $card_no,
	                            'amount' => $amount,
	                            'invoice_id' => $invNo,
	                            'merchID' => $user_id,
	                            'transaction_id' => $response->transactionId,
	                            'transaction' => $transaction,
	                            'levelCommercialData' => $levelCommercialData,
	                            'gateway' => 7
	                        ];
	                        addlevelThreeDataInTransaction($level_three_request);
						}

						$msg = $response->responseMessage;
						$trID = $response->transactionId;
						$st = '0';
						$action = 'Pay Invoice';
						$msg = "Payment Success ";
						$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
						
						$this->session->set_flashdata('success', ' Successfully Processed Invoice');
                

						$txnID      = $in_data['invoiceID'];
						$ispaid 	 = 1;
						$bamount    = $in_data['BalanceRemaining'] - $amount;
						if ($bamount > 0)
							$ispaid 	 = 0;
						$app_amount = $in_data['Total_payment'] + $amount;
						$data   	 = array('IsPaid' => $ispaid, 'Total_payment' => ($app_amount), 'BalanceRemaining' => $bamount);
						$condition  = array('invoiceID' => $in_data['invoiceID'], 'merchantID' => $user_id);

						$this->general_model->update_row_data('QBO_test_invoice', $condition, $data);

						$val = array(
							'merchantID' => $user_id,
						);
						$data = $this->general_model->get_row_data('QBO_token', $val);

						$accessToken  = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];
						$dataService  = DataService::Configure(array(
							'auth_mode' => $this->config->item('AuthMode'),
							'ClientID'  => $this->config->item('client_id'),
							'ClientSecret' => $this->config->item('client_secret'),
							'accessTokenKey' =>  $accessToken,
							'refreshTokenKey' => $refreshToken,
							'QBORealmID' => $realmID,
							'baseUrl' => $this->config->item('QBOURL'),
						));

						$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

						$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");


						if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
							$theInvoice = current($targetInvoiceArray);

							$updatedInvoice = Invoice::update($theInvoice, [
								"sparse" => 'true'
							]);

							$updatedResult = $dataService->Update($updatedInvoice);

							$newPaymentObj = Payment::create([
								"TotalAmt" => $amount,
								"SyncToken" => $updatedResult->SyncToken,
								"CustomerRef" => $customerID,
								"Line" => [
									"LinkedTxn" => [
										"TxnId" => $invoiceID,
										"TxnType" => "Invoice",
									],
									"Amount" => $amount
								]
							]);

							$savedPayment = $dataService->Add($newPaymentObj);

							$crtxnID =	$savedPayment->Id;

							$error = $dataService->getLastError();

							if ($error != null) {

								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
								$st = '';

								$st = '0';
								$action = 'Pay Invoice';
								$msg = "Payment Success but " . $error->getResponseBody();

								$qbID = $trID;
							} else {
								$refNum[] =  $in_data['refNumber'];

								$st = '1';
								$action = 'Pay Invoice';
								$msg = "Payment Success";
								$qbID = $trID;

								$this->session->set_flashdata('success', 'Successfully Processed Invoice');
							}
						}
						$ref_number = implode(',', $refNum);
							$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);

							$tr_date   = date('Y-m-d H:i:s');
							$toEmail = $c_data['userEmail'];
							$company = $c_data['companyName'];
							$customer = $c_data['fullName'];
						if ($chh_mail == '1') {
							
							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
						}
						
						$qbID = $invoiceID;
					} else {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;
						$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);

						$st = '0';
						$action = 'Pay Invoice';
						$qbID = $invoiceID;

						
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
					}

					$id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gateway, $gt_result['gatewayType'], $in_data['CustomerListID'], $amount, $user_id, $crtxnID, $this->resellerID, $in_data['invoiceID'], $echeck_payment, $this->transactionByUser);
				} catch (BuilderException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ConfigurationException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (GatewayException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (UnsupportedTransactionException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ApiException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				}


				if ($error != '') {
					$st = '0';
					$action = 'Pay Invoice';
					$msg = "Payment Failed";
					$qbID = $invoiceID;
				}
				$qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
				$this->general_model->insert_row('tbl_qbo_log', $qbo_log);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
			}
		} else {


			$error = 'Validation Error. Please fill the requred fields';
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
		}

		if ($cusproID != "") {
			$trans_id = $response->transactionId;
            $invoice_IDs  = array();
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
                'proccess_btn_text' => 'Process New Invoice',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            $this->session->set_userdata("in_data", $in_data);
            if ($cusproID == "1") {
                redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');

            }

            redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $cusproID, 'refresh');
        } else {
            if(!$checkPlan){
                $responseId  = '';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
            }
            redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
        }
	}



	public function create_customer_sale_old()
	{


		if ($this->session->userdata('logged_in')) {


			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
		$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;

		if (!empty($gt_result)) {
			$apiloginID      = $gt_result['gatewayUsername'];
			$secretApiKey   = $gt_result['gatewayPassword'];

			$customerID = $this->czsecurity->xssCleanPostInput('customerID');

			$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
			
			$companyID  = $comp_data['companyID'];


			$cardID = $this->czsecurity->xssCleanPostInput('card_list');

			$cvv = '';

			if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

				$card_no = $this->czsecurity->xssCleanPostInput('card_number');


			
				$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				$exyear1   = substr($exyear, 2);
				$expry    = $expmonth . $exyear;
				
			} else {


				$card_data = $this->card_model->get_single_card_data($cardID);
				$expmonth =  $card_data['cardMonth'];

				$exyear   = $card_data['cardYear'];
				$exyear1   = substr($exyear, 2);
				if (strlen($expmonth) == 1) {
					$expmonth = '0' . $expmonth;
				}
				$expry    = $expmonth . $exyear;
				
			}

			$config = new PorticoConfig();

			$config->secretApiKey = $secretApiKey;
			$config->serviceUrl =  $this->config->item('GLOBAL_URL');
			ServicesContainer::configureService($config);
			$card = new CreditCardData();
			$card->number = $card_no;
			$card->expMonth = $expmonth;
			$card->expYear = $exyear;
			if ($cvv != "")
				$card->cvn = $cvv;
			$card->cardType = $cardType;

			$address = new Address();
			$address->streetAddress1 = $address1;
			$address->city = $city;
			$address->state = $state;
			$address->postalCode = $zipcode;
			$address->country = $country;
			$amount = $this->czsecurity->xssCleanPostInput('totalamount');

			$invNo  = mt_rand(1000000, 2000000);
			try {
				$response = $card->charge($amount)
					->withCurrency('USD')
					->withAddress($address)
					->withInvoiceNumber($invNo)
					->withAllowDuplicates(true)
					->execute();

				$crtxnID = '';

				if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
					$msg = $response->responseMessage;
					$trID = $response->transactionId;

					$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);



					$invoiceIDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}



					$condition1 = array('resellerID' => $resellerID);

					$sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);

					$domain1 = $sub1['merchantPortalURL'];

					$URL1 = $domain1 . 'FreshBooks_controllers/Freshbooks_integration/auth';

					$val = array(
						'merchantID' => $merchantID,
					);
					$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

					$subdomain     = $Freshbooks_data['sub_domain'];
					$key           = $Freshbooks_data['secretKey'];
					$accessToken   = $Freshbooks_data['accessToken'];
					$access_token_secret = $Freshbooks_data['oauth_token_secret'];

					define('OAUTH_CONSUMER_KEY', $subdomain);
					define('OAUTH_CONSUMER_SECRET', $key);
					define('OAUTH_CALLBACK', $URL1);

					$refNumber = array();

					if (!empty($invoiceIDs)) {

						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();


							$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
							$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");


							$con = array('invoiceID' => $inID, 'merchantID' => $user_id);
							$res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment'), $con);
							$amount_data = $res1['BalanceRemaining'];
							$refNumber[]   =  $res1['refNumber'];
							$txnID      = $inID;
							$ispaid 	 = 1;

							$app_amount = $res1['Total_payment'] + $amount;
							$data   	 = array('IsPaid' => $ispaid, 'Total_payment' => ($app_amount), 'BalanceRemaining' => '0.00');

							$this->general_model->update_row_data('QBO_test_invoice', $con, $data);

							if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
								$theInvoice = current($targetInvoiceArray);

								$updatedInvoice = Invoice::update($theInvoice, [
									"sparse" => 'true'
								]);

								$updatedResult = $dataService->Update($updatedInvoice);

								$newPaymentObj = Payment::create([
									"TotalAmt" => $app_amount,
									"SyncToken" => $updatedResult->SyncToken,
									"CustomerRef" => $updatedResult->CustomerRef,
									"Line" => [
										"LinkedTxn" => [
											"TxnId" => $updatedResult->Id,
											"TxnType" => "Invoice",
										],
										"Amount" => $amount_data
									]
								]);

								$savedPayment = $dataService->Add($newPaymentObj);



								$error = $dataService->getLastError();
								if ($error != null) {
									$err = '';
									$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
									$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
									$err .= "The Response message is: " . $error->getResponseBody() . "\n";
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Success but " . $error->getResponseBody();

									$qbID = $trID;
									$pinv_id = '';
								} else {
									$pinv_id = '';
									$pinv_id        =  $savedPayment->Id;
									$st = '1';
									$action = 'Pay Invoice';
									$msg = "Payment Success";
									$qbID = $trID;
									$this->session->set_flashdata('success', 'Transaction Successful');
								}
								$qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

								$this->general_model->insert_row('tbl_qbo_log', $qbo_log);
								$id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $user_id, $pinv_id, $this->resellerID, $inID = '', false, $this->transactionByUser);
							}
						}
					} else {

						$crtxnID = '';
						$inID = '';
						$id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);
					}



					if ($chh_mail == '1') {

						$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
						$ref_number = implode(',', $refNumber);
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
					}


					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {
						$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
						$card_no = $this->czsecurity->xssCleanPostInput('card_number');
						$card_type      = $this->general_model->getType($card_no);
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');

						$cvv     = $this->czsecurity->xssCleanPostInput('cvv');

						$card_data = array(
							'cardMonth'   => $expmonth,
							'cardYear'	     => $exyear,
							'CardType'     => $card_type,
							'companyID'    => $companyID,
							'merchantID'   => $merchantID,
							'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
							'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
							'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
							'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
							'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
							'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
							'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
							'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
							'CustomerCard' => $card_no,
							'CardCVV'      => $cvv,
							'updatedAt'    => date("Y-m-d H:i:s")
						);



						$card =	$this->card_model->process_card($card_data);


					}

					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {
					$crtxnID = '';
					$msg = $response->responseMessage;
					$trID = $response->transactionId;
					$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
					$id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);
				}
			} catch (BuilderException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (ConfigurationException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (GatewayException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (UnsupportedTransactionException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (ApiException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
		}



		redirect('FreshBooks_controllers/Transactions/create_customer_sale', 'refresh');
	}

	public function create_customer_sale()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$user_id = $this->merchantID;
		if (!empty($this->input->post(null, true))) {
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			if ($this->czsecurity->xssCleanPostInput('card_list') == "new1") {

				
				$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
				
				$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
			}

			$checkPlan = check_free_plan_transactions();

			if ($checkPlan && $this->form_validation->run() == true) {
				$custom_data_fields = [];
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }

				$gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				if (!empty($gt_result)) {
					$apiloginID      = $gt_result['gatewayUsername'];
					$secretApiKey   = $gt_result['gatewayPassword'];

					$invoiceIDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}

					$customerID = $this->czsecurity->xssCleanPostInput('customerID');

					$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
					$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
					$companyID  = $comp_data['companyID'];
					$cardID = $this->czsecurity->xssCleanPostInput('card_list');

					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$cardType  = $this->general_model->getType($card_no);
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						if ($this->czsecurity->xssCleanPostInput('cvv') != "")
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					} else {
						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no  = $card_data['CardNo'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						if ($card_data['CardCVV'])
							$cvv      = $card_data['CardCVV'];
						$cardType = $card_data['CardType'];
						$address1 = $card_data['Billing_Addr1'];
						$city     =  $card_data['Billing_City'];
						$zipcode  = $card_data['Billing_Zipcode'];
						$state    = $card_data['Billing_State'];
						$country  = $card_data['Billing_Country'];
					}
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');

					$config = new PorticoConfig();

					$config->secretApiKey = $secretApiKey;
					$config->serviceUrl =  $this->config->item('GLOBAL_URL');

					ServicesContainer::configureService($config);
					$card = new CreditCardData();
					$card->number = $card_no;
					$card->expMonth = $expmonth;
					$card->expYear = $exyear;

					if ($cvv != "")
						$card->cvn = $cvv;
					$card->cardType = $cardType;

					$address = new Address();
					$address->streetAddress1 = $address1;
					$address->city = $city;
					$address->state = $state;
					$address->postalCode = $zipcode;
					$address->country = $country;
					$amount = $this->czsecurity->xssCleanPostInput('totalamount');
					$invNo  = mt_rand(1000000, 2000000);
                    if($this->czsecurity->xssCleanPostInput('invoice_id')){
                        $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
                        $invNo = $new_invoice_number;
                    }
					try {
						$response = $card->charge($amount)
							->withCurrency('USD')
							->withAddress($address)
							->withInvoiceNumber($invNo)
							->withAllowDuplicates(true)
							->execute();
						$crtxnID = '';

						if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
							

								// add level three data
		                        $transaction = new Transaction();
		                        $transaction->transactionReference = new TransactionReference();
		                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
		                        $level_three_request = [
		                            'card_no' => $card_no,
		                            'amount' => $amount,
		                            'invoice_id' => $invNo,
		                            'merchID' => $user_id,
		                            'transaction_id' => $response->transactionId,
		                            'transaction' => $transaction,
		                            'levelCommercialData' => $levelCommercialData,
		                            'gateway' => 7
		                        ];
		                        addlevelThreeDataInTransaction($level_three_request);

							$msg = $response->responseMessage;
							$trID = $response->transactionId;

							$result = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);

							include APPPATH . 'libraries/Freshbooks_data.php';
							$fb_data = new Freshbooks_data($this->resellerID, $this->merchantID);

							$invoiceIDs = array();
							if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
								$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
							}

							$refNumber = array();

							if (!empty($invoiceIDs)) {

								foreach ($invoiceIDs as $inID) {

									$in_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invoiceID' => $inID));
									$refNumber[] = 	$in_data['refNumber'];
									$amount_data  = $amount;
									$pinv_id = '';
									$invoices =  $fb_data->create_invoice_payment($inID, $amount_data);
									if ($invoices['status'] != 'ok') {
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
									} else {
										$pinv_id =  $invoices['payment_id'];

										$this->session->set_flashdata('success', 'Successfully Processed Invoice');
									}

									$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
								}
							} else {
								$crtxnID = '';
								$inID    = '';
								$id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
							}
							$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
							if (!empty($refNumber))
								$ref_number = implode(',', $refNumber);
							else
								$ref_number = '';
							$tr_date   = date('Y-m-d H:i:s');
							$toEmail = $comp_data['userEmail'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['fullName'];
							if ($chh_mail == '1') {

								
								$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
							}
							

							if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
								$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
								$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
								$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
								$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
								$card_type = $this->general_model->getType($card_no);

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'     => $card_type,
									'CustomerCard' => $card_no,
									'CardCVV'      => $cvv,
									'customerListID' => $customerID,
									'companyID'     => $companyID,
									'merchantID'   => $this->merchantID,

									'createdAt' 	=> date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,
									'Billing_Addr2'	 => $address2,
									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $phone,
									'Billing_Zipcode'	 => $zipcode,
								);
								
								$id1 =    $this->card_model->process_card($card_data);
							}


							$this->session->set_flashdata('success', 'Transaction Successful');
						} else {
							$crtxnID = '';
							$msg = $response->responseMessage;
							$trID = $response->transactionId;

							$result = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);

							$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
							$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
						}
					} catch (BuilderException $e) {
						$error = 'Transaction Failed - ' . $e->getMessage();
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
					} catch (ConfigurationException $e) {
						$error = 'Transaction Failed - ' . $e->getMessage();
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
					} catch (GatewayException $e) {
						$error = 'Transaction Failed - ' . $e->getMessage();
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
					} catch (UnsupportedTransactionException $e) {
						$error = 'Transaction Failed - ' . $e->getMessage();
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
					} catch (ApiException $e) {
						$error = 'Transaction Failed - ' . $e->getMessage();
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
					}

				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
				}
			} else {


				$error = 'Validation Error. Please fill the requred fields';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
			}
			$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => $response->transactionId,
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'	=> $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
		}




		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];

		$compdata				= $this->fb_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/payment_sale', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function create_customer_auth()
	{

		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if ($this->session->userdata('logged_in')) {


			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
		}


		$gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
		$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
		
		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;

		$checkPlan = check_free_plan_transactions();
		if ($checkPlan && !empty($gt_result)) {
			$apiloginID      = $gt_result['gatewayUsername'];
			$secretApiKey   = $gt_result['gatewayPassword'];

			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			
			$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

			$companyID  = $comp_data['companyID'];

			$cardID = $this->czsecurity->xssCleanPostInput('card_list');

			$cvv = '';
			if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

				$card_no = $this->czsecurity->xssCleanPostInput('card_number');


			
				$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				$exyear1   = substr($exyear, 2);
				$expry    = $expmonth . $exyear;
				
			} else {


				$card_data = $this->card_model->get_single_card_data($cardID);

				
				$expmonth =  $card_data['cardMonth'];

				$exyear   = $card_data['cardYear'];
				$exyear1   = substr($exyear, 2);
				if (strlen($expmonth) == 1) {
					$expmonth = '0' . $expmonth;
				}
				$expry    = $expmonth . $exyear;
				
			}

			$config = new PorticoConfig();

			$config->secretApiKey = $secretApiKey;
			$config->serviceUrl =  $this->config->item('GLOBAL_URL');
			ServicesContainer::configureService($config);
			$card = new CreditCardData();
			$card->number = $card_no;
			$card->expMonth = $expmonth;
			$card->expYear = $exyear;
			if ($cvv != "")
				$card->cvn = $cvv;
			$card->cardType = $cardType;

			$address = new Address();
			$address->streetAddress1 = $address1;
			$address->city = $city;
			$address->state = $state;
			$address->postalCode = $zipcode;
			$address->country = $country;
			$amount = $this->czsecurity->xssCleanPostInput('totalamount');

			$invNo  = mt_rand(1000000, 2000000);
			try {
				$response = $card->authorize($amount)
					->withCurrency('USD')
					->withAddress($address)
					
					->execute();


				
				$crtxnID = '';

				if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {



					// add level three data
                    $transaction = new Transaction();
                    $transaction->transactionReference = new TransactionReference();
                    $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                    $level_three_request = [
                        'card_no' => $card_no,
                        'amount' => $amount,
                        'invoice_id' => $invNo,
                        'merchID' => $merchantID,
                        'transaction_id' => $response->transactionId,
                        'transaction' => $transaction,
                        'levelCommercialData' => $levelCommercialData,
                        'gateway' => 7
                    ];
                    addlevelThreeDataInTransaction($level_three_request);

					$msg = $response->responseMessage;
					$trID = $response->transactionId;

					$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);

					$condition1 = array('resellerID' => $resellerID);

					$sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);

					$domain1 = $sub1['merchantPortalURL'];

					$URL1 = $domain1 . 'FreshBooks_controllers/Freshbooks_integration/auth';

					$val = array(
						'merchantID' => $merchantID,
					);
					$Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

					$subdomain     = $Freshbooks_data['sub_domain'];
					$key           = $Freshbooks_data['secretKey'];
					$accessToken   = $Freshbooks_data['accessToken'];
					$access_token_secret = $Freshbooks_data['oauth_token_secret'];

					define('OAUTH_CONSUMER_KEY', $subdomain);
					define('OAUTH_CONSUMER_SECRET', $key);
					define('OAUTH_CALLBACK', $URL1);

					/* This block is created for saving Card info in encrypted form  */


					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {
						$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
						$card_no = $this->czsecurity->xssCleanPostInput('card_number');
						$card_type      = $this->general_model->getType($card_no);
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');

						$cvv     = $this->czsecurity->xssCleanPostInput('cvv');

						$card_data = array(
							'cardMonth'   => $expmonth,
							'cardYear'	     => $exyear,
							'CardType'     => $card_type,
							'companyID'    => $companyID,
							'merchantID'   => $merchantID,
							'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
							'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
							'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
							'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
							'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
							'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
							'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
							'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
							'CustomerCard' => $card_no,
							'CardCVV'      => $cvv,
							'updatedAt'    => date("Y-m-d H:i:s")
						);



						$card =	$this->card_model->process_card($card_data);


					}

					if ($chh_mail == '1') {

						$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
						$ref_number = implode(',', $refNumber);
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
					}

					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {
					$msg = $response->responseMessage;
					$trID = $response->transactionId;
					$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
				}



				$id = $this->general_model->insert_gateway_transaction_data($res, 'auth', $gateway, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);
			} catch (BuilderException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (ConfigurationException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (GatewayException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (UnsupportedTransactionException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			} catch (ApiException $e) {
				$error = 'Transaction Failed - ' . $e->getMessage();
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
		}
		$invoice_IDs = array();
					
				
					$receipt_data = array(
						'transaction_id' => $response->transactionId,
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_auth',
						'proccess_btn_text' => 'Process New Transaction',
						'sub_header' => 'Authorize',
						'checkPlan'	=> $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');

	}



	public function create_customer_void_old()
	{


		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}

			$tID     = $this->czsecurity->xssCleanPostInput('strtxnvoidID');

			$con     = array('transactionID' => $tID, 'transactionType' => 'auth');
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gateway = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			if ($tID != '' && !empty($gt_result)) {

				
				$secretApiKey  =  $gt_result['gatewayPassword'];

				$config = new PorticoConfig();

				$config->secretApiKey = $secretApiKey;
				$config->serviceUrl =  $this->config->item('GLOBAL_URL');


				$amount =  $paydata['transactionAmount'];
				ServicesContainer::configureService($config);
				$customerID = $paydata['customerListID'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
				try {
					$response = Transaction::fromId($tID)
						->void()
						->execute();


					if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;

						$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);

						$condition = array('transactionID' => $tID);

						$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

						$this->general_model->update_row_data('customer_transaction', $condition, $update_data);


						if ($chh_mail == '1') {
							$condition = array('transactionID' => $tID);
							$customerID = $paydata['customerListID'];
							$tr_date   = date('Y-m-d H:i:s');
							$ref_number =  $tID;
							$toEmail = $comp_data['userEmail'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['fullName'];
							$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
						}
						$this->session->set_flashdata('success', 'Successfully Captured Authorize Transaction');
					} else {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;
						$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
					}



					$id = $this->general_model->insert_gateway_transaction_data($res, 'void', $gateway, $gt_result['gatewayType'], $paydata['customerListID'], $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser);
				} catch (BuilderException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ConfigurationException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (GatewayException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (UnsupportedTransactionException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ApiException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				}
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}
			redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
			
		}


		redirect('FreshBooks_controllers/Transactions/payment_transaction', 'refresh');
	}

	public function create_customer_void()
	{


		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}

			$tID     = $this->czsecurity->xssCleanPostInput('strtxnvoidID');

			$con     = array('transactionID' => $tID, 'transactionType' => 'auth');
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gateway = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			if ($tID != '' && !empty($gt_result)) {

				
				$secretApiKey  =  $gt_result['gatewayPassword'];

				$config = new PorticoConfig();

				$config->secretApiKey = $secretApiKey;
				$config->serviceUrl =  $this->config->item('GLOBAL_URL');


				$amount =  $paydata['transactionAmount'];
				ServicesContainer::configureService($config);
				$customerID = $paydata['customerListID'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
				try {
					$response = Transaction::fromId($tID)
						->void()
						->execute();


					if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;

						$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);

						$condition = array('transactionID' => $tID);

						$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

						$this->general_model->update_row_data('customer_transaction', $condition, $update_data);


						if ($chh_mail == '1') {
							$condition = array('transactionID' => $tID);
							$customerID = $paydata['customerListID'];
							$tr_date   = date('Y-m-d H:i:s');
							$ref_number =  $tID;
							$toEmail = $comp_data['userEmail'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['fullName'];
							$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
						}
						$this->session->set_flashdata('success', 'Successfully Captured Authorize Transaction');
					} else {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;
						$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
					}



					$id = $this->general_model->insert_gateway_transaction_data($res, 'void', $gateway, $gt_result['gatewayType'], $paydata['customerListID'], $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser);
				} catch (BuilderException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ConfigurationException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (GatewayException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (UnsupportedTransactionException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ApiException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				}
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}
			redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
		
		}


		redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
	}

	/*****************Capture Transaction***************/

	public function create_customer_capture_old()
	{
		//Show a form here which collects someone's name and e-mail address

		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}

			$tID     = $this->czsecurity->xssCleanPostInput('strtxnID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gateway = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			if ($tID != '' && !empty($gt_result)) {

				
				$secretApiKey  =  $gt_result['gatewayPassword'];

				$config = new PorticoConfig();

				$config->secretApiKey = $secretApiKey;
				$config->serviceUrl =  $this->config->item('GLOBAL_URL');


				$amount =  $paydata['transactionAmount'];
				ServicesContainer::configureService($config);
				$customerID = $paydata['customerListID'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

				try {
					$response = Transaction::fromId($tID)
						->capture($amount)
						->execute();


					if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;

						$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);

						$condition = array('transactionID' => $tID);

						$update_data =   array('transaction_user_status' => "4");

						$this->general_model->update_row_data('customer_transaction', $condition, $update_data);
						if ($chh_mail == '1') {
							$condition = array('transactionID' => $tID);
							$customerID = $paydata['customerListID'];

							$tr_date   = date('Y-m-d H:i:s');
							$ref_number =  $tID;
							$toEmail = $comp_data['userEmail'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['fullName'];
							$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
						}
						$this->session->set_flashdata('success', 'Successfully Captured Authorize Transaction');
					} else {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;
						$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
					}



					$id = $this->general_model->insert_gateway_transaction_data($res, 'capture', $gateway, $gt_result['gatewayType'], $paydata['customerListID'], $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser);
				} catch (BuilderException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ConfigurationException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (GatewayException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (UnsupportedTransactionException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ApiException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				}
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}
			redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
			
		}


		redirect('FreshBooks_controllers/Transactions/payment_transaction', 'refresh');
	}



	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}

			$tID     = $this->czsecurity->xssCleanPostInput('strtxnID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gateway = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			if ($tID != '' && !empty($gt_result)) {

				
				$secretApiKey  =  $gt_result['gatewayPassword'];

				$config = new PorticoConfig();

				$config->secretApiKey = $secretApiKey;
				$config->serviceUrl =  $this->config->item('GLOBAL_URL');


				$amount =  $paydata['transactionAmount'];
				ServicesContainer::configureService($config);
				$customerID = $paydata['customerListID'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

				try {
					$response = Transaction::fromId($tID)
						->capture($amount)
						->execute();


					if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;

						$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);

						$condition = array('transactionID' => $tID);

						$update_data =   array('transaction_user_status' => "4");

						$this->general_model->update_row_data('customer_transaction', $condition, $update_data);
						$condition = array('transactionID' => $tID);
							$customerID = $paydata['customerListID'];

							$tr_date   = date('Y-m-d H:i:s');
							$ref_number =  $tID;
							$toEmail = $comp_data['userEmail'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['fullName'];
							$condition_mail = '';
						if ($chh_mail == '1') {
							
							$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
						}
						
						$this->session->set_flashdata('success', 'Successfully Captured Authorize Transaction');
					} else {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;
						$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
					}



					$id = $this->general_model->insert_gateway_transaction_data($res, 'capture', $gateway, $gt_result['gatewayType'], $paydata['customerListID'], $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser);
				} catch (BuilderException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ConfigurationException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (GatewayException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (UnsupportedTransactionException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ApiException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				}
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}
			$invoice_IDs = array();
				
				 $receipt_data = array(
					 'proccess_url' => 'FreshBooks_controllers/Transactions/payment_capture',
					 'proccess_btn_text' => 'Process New Transaction',
					 'sub_header' => 'Capture',
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 if($paydata['invoiceTxnID'] == ''){
					 $paydata['invoiceTxnID'] ='null';
					 }
					 if($paydata['customerListID'] == ''){
						 $paydata['customerListID'] ='null';
					 }
					 if($response->transactionId == ''){
						 $response->transactionId ='null';
					 }
				 
				 redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$response->transactionId,  'refresh');	
			
		}


		redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
	}


	public function pay_multi_invoice()
	{


		if ($this->session->userdata('logged_in')) {


			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

				
		$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
		$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
		
		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
		if ($this->czsecurity->xssCleanPostInput('CardID1') == "new1") {


			$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
			
			$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
			$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
		}

		$cusproID = '';
		$error = '';
		$cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $checkPlan = check_free_plan_transactions();

		if ($checkPlan && $this->form_validation->run() == true) {

			$cardID_upd = '';

			$cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			$gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');
			$amount               = $this->czsecurity->xssCleanPostInput('totalPay');
			$customerID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			$invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
			$invoiceIDs = implode(',', $invoices);

			$inv_data   =    $this->qbo_customer_model->get_due_invoice_data($invoiceIDs, $user_id);

			if (!empty($invoices) && !empty($inv_data)) {
				$cusproID =  $customerID  = $inv_data['CustomerListID'];
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

				$secretApiKey   = $gt_result['gatewayPassword'];

				if ($cardID != "new1") {
					$card_data =   $this->card_model->get_single_card_data($cardID);
					$card_no  = $card_data['CardNo'];
					$expmonth =  $card_data['cardMonth'];
					$exyear   = $card_data['cardYear'];
					$cvv      = $card_data['CardCVV'];
					$cardType = $card_data['cardType'];
					$address1 = $card_data['Billing_Addr1'];
					$city     =  $card_data['Billing_City'];
					$zipcode  = $card_data['Billing_Zipcode'];
					$state    = $card_data['Billing_State'];
					$country  = $card_data['Billing_Country'];
				} else {
					$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$cardType = $this->general_model->getType($card_no);
					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('cvv') != "")
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
				}
				$crtxnID = '';

				$config = new PorticoConfig();

				$config->secretApiKey = $secretApiKey;
				$config->serviceUrl =  $this->config->item('GLOBAL_URL');



				ServicesContainer::configureService($config);
				$card = new CreditCardData();
				$card->number = $card_no;
				$card->expMonth = $expmonth;
				$card->expYear = $exyear;
				if ($cvv != "")
					$card->cvn = $cvv;
				$card->cardType = $cardType;

				$address = new Address();
				$address->streetAddress1 = $address1;
				$address->city = $city;
				$address->state = $state;
				$address->postalCode = $zipcode;
				$address->country = $country;


				$invNo  = mt_rand(1000000, 2000000);
				try {
					$response = $card->charge($amount)
						->withCurrency('USD')
						->withAddress($address)
						->withInvoiceNumber($invNo)
						->withAllowDuplicates(true)
						->execute();


					if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
						$msg = $response->responseMessage;
						$trID = $response->transactionId;



						// add level three data
                        $transaction = new Transaction();
                        $transaction->transactionReference = new TransactionReference();
                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                        $level_three_request = [
                            'card_no' => $card_no,
                            'amount' => $amount,
                            'invoice_id' => $invNo,
                            'merchID' => $user_id,
                            'transaction_id' => $response->transactionId,
                            'transaction' => $transaction,
                            'levelCommercialData' => $levelCommercialData,
                            'gateway' => 7
                        ];
                        addlevelThreeDataInTransaction($level_three_request);

						$res = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
						$this->session->set_flashdata('success', 'Successfully Processed Invoice');
						$c_data     = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('Customer_ListID' => $customerID, 'merchantID' => $user_id));
						$companyID = $c_data['companyID'];


						$val = array('merchantID' => $user_id);
						$data = $this->general_model->get_row_data('QBO_token', $val);

						$accessToken = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];

						$dataService = DataService::Configure(array(
							'auth_mode' => $this->config->item('AuthMode'),
							'ClientID'  => $this->config->item('client_id'),
							'ClientSecret' => $this->config->item('client_secret'),
							'accessTokenKey' =>  $accessToken,
							'refreshTokenKey' => $refreshToken,
							'QBORealmID' => $realmID,
							'baseUrl' => $this->config->item('QBOURL'),
						));



						if (!empty($invoices)) {

							foreach ($invoices as $invoiceID) {
								$theInvoice = array();


								$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $invoiceID . "'");
								$condition  = array('invoiceID' => $invoiceID, 'merchantID' => $user_id);
								$in_data = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'Total_payment'), $condition);
								$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
								$txnID       = $invoiceID;
								$ispaid   = 1;

								$bamount    = $in_data['BalanceRemaining'] - $pay_amounts;
								if ($bamount > 0)
									$ispaid 	 = 0;
								$app_amount = $in_data['Total_payment'] + $pay_amounts;
								$data   	 = array('IsPaid' => $ispaid, 'Total_payment' => ($app_amount), 'BalanceRemaining' => $bamount);
								$to_amount = $in_data['BalanceRemaining'] + $in_data['Total_payment'];

								$this->general_model->update_row_data('QBO_test_invoice', $condition, $data);

								if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
									$theInvoice = current($targetInvoiceArray);

									$updatedInvoice = Invoice::update($theInvoice, [
										"sparse" => 'true'
									]);

									$updatedResult = $dataService->Update($updatedInvoice);

									$newPaymentObj = Payment::create([
										"TotalAmt" => $to_amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
											"LinkedTxn" => [
												"TxnId" => $updatedResult->Id,
												"TxnType" => "Invoice",
											],
											"Amount" => $pay_amounts
										]
									]);

									$savedPayment = $dataService->Add($newPaymentObj);



									$error = $dataService->getLastError();
									if ($error != null) {
										$err = '';
										$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
										$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
										$err .= "The Response message is: " . $error->getResponseBody() . "\n";
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

										$st = '0';
										$action = 'Pay Invoice';
										$msg = "Payment Success but " . $error->getResponseBody();

										$qbID = $trID;
										$pinv_id = '';
									} else {
										$pinv_id = '';
										$pinv_id        =  $savedPayment->Id;
										$st = '1';
										$action = 'Pay Invoice';
										$msg = "Payment Success";
										$qbID = $trID;
										$this->session->set_flashdata('success', 'Transaction Successful');
									}
									$qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

									$this->general_model->insert_row('tbl_qbo_log', $qbo_log);
									$id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $pay_amounts, $user_id, $pinv_id, $this->resellerID, $inID = '', false, $this->transactionByUser);
								}
							}
						}


						if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

							$cardType       = $this->general_model->getType($card_no);
							$friendlyname   =  $cardType.' - '.substr($card_no,-4);
							$card_condition = array(
								'customerListID' => $customerID,
								'customerCardfriendlyName' => $friendlyname,
							);

							$crdata =	$this->card_model->check_friendly_name($customerID, $friendlyname);


							if (!empty($crdata)) {

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	     => $exyear,
									'companyID'    => $companyID,
									'merchantID'   => $user_id,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '', 
									'updatedAt'    => date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,
									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $contact,
									'Billing_Zipcode'	 => $zipcode,
								);



								$this->card_model->update_card_data($card_condition, $card_data);
							} else {
								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '',
									'customerListID' => $customerID,
									'companyID'     => $companyID,
									'merchantID'   => $user_id,
									'customerCardfriendlyName' => $friendlyname,
									'createdAt' 	=> date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,
									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $contact,
									'Billing_Zipcode'	 => $zipcode,
								);
								

								$id1 =    $this->card_model->insert_card_data($card_data);
							}
						}
					} else {
						$crtxnID = '';
						$msg = $response->responseMessage;
						$trID = $response->transactionId;
						$res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
						$id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);
					}
				} catch (BuilderException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ConfigurationException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (GatewayException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (UnsupportedTransactionException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				} catch (ApiException $e) {
					$error = 'Transaction Failed - ' . $e->getMessage();
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
			}
		} else {


			$error = 'Validation Error. Please fill the requred fields';
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
		}

		if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

		if ($cusproID != "") {
			redirect('FreshBooks_controllers/home/view_customer/' . $cusproID, 'refresh');
		} else {
			redirect('FreshBooks_controllers/Create_invoice/Invoice_details', 'refresh');
		}
	}

	public function test_index()
	{
		$config = new PorticoConfig();

		$config->secretApiKey = 'skapi_cert_MYl2AQAowiQAbLp5JesGKh7QFkcizOP2jcX9BrEMqQ';
		$config->serviceUrl = 'https://cert.api2.heartlandportico.com';



		ServicesContainer::configureService($config);


		$card = new CreditCardData();
		$card->number = "4111111111111111";
		$card->expMonth = "12";
		$card->expYear = "2022";
		$card->cvn = "123";
		$card->cardType = 'VISA';

		$address = new Address();
		$address->streetAddress1 = "1231 E Dyer Rd Ste 290";
		$address->city = "Santa Ana";
		$address->state = "CA";
		$address->postalCode = "92705";
		$address->country = "United States";
		$invNo  = mt_rand(1000000, 2000000);
		try {


			$response = Transaction::fromId('1176936353')
				->capture(4)
				->execute();


			$response = $card->authorize(15)
				->withCurrency('USD')
				->withAddress($address)
				->execute();



			$response = $card->charge(15)
				->withCurrency('USD')
				->withAddress($address)
				->withInvoiceNumber($invNo)
				->withAllowDuplicates(true)
				->execute();
			echo "<pre>";
			print_r($response);
			$body = '<h1>Success!</h1>';
			$body .= '<p>Thank you, Test User for your order of $15.00</p>';
			echo "APPROVAL" . $response->responseMessage;
			echo "Transaction Id: " . $response->transactionId;
			echo "<br />Invoice Number:" . $invNo;

			// i'm running windows, so i had to update this:
			//ini_set("SMTP", "my-mail-server");

			
		} catch (BuilderException $e) {
			echo 'Transaction Failed - ' . $e->getMessage();
			exit;
		} catch (ConfigurationException $e) {
			echo 'Transaction Failed - ' . $e->getMessage();
			exit;
		} catch (GatewayException $e) {
			echo 'Transaction Failed - ' . $e->getMessage();
			exit;
		} catch (UnsupportedTransactionException $e) {
			echo 'Transaction Failed - ' . $e->getMessage();
			exit;
		} catch (ApiException $e) {
			echo 'Transaction Failed - ' . $e->getMessage();
			exit;
		}
	}

	public function create_customer_esale()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
           
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $secretApiKey  = $gt_result['gatewayPassword'];
                
                $comp_data     =$this->general_model->get_select_data('Freshbooks_custom_customer',array('companyID','companyName', 'Contact','FullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
                $companyID  = $comp_data['companyID'];

                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code =     'WEB';

                if($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID' => $customerID,
                        'companyID'     => $companyID,
                        'merchantID'   => $merchantID,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }

                
                try
                {
                    $config = new PorticoConfig();
               
                    $config->secretApiKey = $secretApiKey;
                    if($this->config->item('Sandbox')){
                        $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                    }
                    else{
                        $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                        $config->developerId =  $this->config->item('DeveloperId');
                        $config->versionNumber =  $this->config->item('VersionNumber');
                    }
       
                    ServicesContainer::configureService($config);
                    $check = new ECheck();
                    $check->accountNumber = $accountDetails['accountNumber'];
                    $check->routingNumber = $accountDetails['routeNumber'];
                    if(strtolower($accountDetails['accountType']) == 'checking'){
                        $check->accountType = 0;
                    }else{
                        $check->accountType = 1;
                    }

                    if(strtoupper($accountDetails['accountHolderType']) == 'PERSONAL'){
                        $check->checkType = 0;
                    }else{
                        $check->checkType = 1;
                    }
                    $check->checkHolderName = $accountDetails['accountName'];
                    $check->secCode = "WEB";
               
                    $address = new Address();
                    $address->streetAddress1 = $accountDetails['Billing_Addr1'];
                    $address->city = $accountDetails['Billing_City'];
                    $address->state = $accountDetails['Billing_State'];
                    $address->postalCode = $accountDetails['Billing_Zipcode'];
                    $address->country = $accountDetails['Billing_Country'];

                    $invNo = '';
                    if($this->czsecurity->xssCleanPostInput('invoice_id')){
                        $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 3);
                        $invNo = $new_invoice_number;
                    }

                    $response = $check->charge($amount)
                                    ->withCurrency(CURRENCY)
                                    ->withAddress($address)
                                    ->withInvoiceNumber($invNo)
                                    ->withAllowDuplicates(true)
                                    ->execute();
                   
                    $msg = $response->responseMessage;
                    $trID = $response->transactionId;
                    $res =array('transactionCode' => 200, 'status'=>$msg, 'transactionId'=> $trID );

                    if($response->responseCode != 0 && $response->responseCode != '00')
                    {
                        $error='Gateway Error. Invalid Account Details';
                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
                        redirect('FreshBooks_controllers/Transactions/create_customer_esale');
                    }

                    if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                    {
                        if ($this->czsecurity->xssCleanPostInput('tr_checked')){
                            $chh_mail = 1;
                        }else{
                            $chh_mail = 0;
                        }
                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                        
                        $ref_number = '';
                        $tr_date   = date('Y-m-d H:i:s');
                        $toEmail = $comp_data['Contact'];
                        $company = $comp_data['companyName'];
                        $customer = $comp_data['FullName'];

                        if($payableAccount == '' || $payableAccount == 'new1') {
                            $id1 = $this->card_model->process_ack_account($accountDetails);
                        }
                        if($chh_mail =='1')
                        {
                           
                          $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                        }
                        
                        $this->session->set_flashdata('success', ' Transaction Successful');
                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gatlistval, $gt_result['gatewayType'],$customerID,$amount,$merchantID,$trID, $this->resellerID,$inID='', true, $this->transactionByUser);
                    }else{
                        $res['transactionCode'] = $response->responseCode;
                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gatlistval, $gt_result['gatewayType'],$customerID,$amount,$merchantID,$trID, $this->resellerID,$inID='', true, $this->transactionByUser);
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $msg . '</div>');
                    }
                } catch (Exception $e)
                {
                    $error='Transaction Failed - ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    redirect('FreshBooks_controllers/Transactions/create_customer_esale');

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
                redirect('FreshBooks_controllers/Transactions/create_customer_esale');

            }

            $receipt_data = array(
                'transaction_id' => $response->transactionId,
                'IP_address' => getClientIpAddr(),
                'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
                'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact' => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url' => 'FreshBooks_controllers/Transactions/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header' => 'Sale',
                'checkPlan' => $checkPlan
            );
            $this->session->set_userdata("receipt_data",$receipt_data);
            redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
        }
        redirect('FreshBooks_controllers/Transactions/create_customer_esale');
    }
}
