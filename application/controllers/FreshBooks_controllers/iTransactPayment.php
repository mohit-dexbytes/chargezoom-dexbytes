<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */

include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

class iTransactPayment extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';

        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->library('form_validation');
        $this->load->model('Freshbook_models/freshbooks_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
            $this->resellerID = $logged_in_data['resellerID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $merchID;
        } else {
            redirect('login', 'refresh');
        }

        $val = array(
            'merchantID' => $this->merchantID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
        if (empty($Freshbooks_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
            redirect('FreshBoolks_controllers/home/index', 'refresh');
        }

        $this->subdomain           = $Freshbooks_data['sub_domain'];
        $key                       = $Freshbooks_data['secretKey'];
        $this->accessToken         = $Freshbooks_data['accessToken'];
        $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
        $this->fb_account          = $Freshbooks_data['accountType'];
        $condition1                = array('resellerID' => $this->resellerID);
        $sub1                      = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1                   = $sub1['merchantPortalURL'];
        $URL1                      = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        define('OAUTH_CONSUMER_KEY', $this->subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
    }

    public function index()
    {

      
        redirect('FreshBooks_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");

        $cusproID = array();
        $error    = array();
        $cusproID = $this->czsecurity->xssCleanPostInput('customerProcessID');

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $this->form_validation->run() == true) {

            $merchID    = $this->merchantID;
            $user_id    = $merchID;
            $resellerID = $this->resellerID;
            $invoiceID  = $this->czsecurity->xssCleanPostInput('invoiceProcessID');

            $cardID = $this->czsecurity->xssCleanPostInput('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
            }

         
            $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
            }

            $gateway    = $gatlistval;
            $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

            $amount   = $this->czsecurity->xssCleanPostInput('inv_amount');
            $chh_mail = 0;
            $in_data  = $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

            if (!empty($in_data)) {

                $customerID = $in_data['Customer_ListID'];

                $companyID = $in_data['companyID'];
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

                $apiUsername  = $gt_result['gatewayUsername'];
                $apiKey  = $gt_result['gatewayPassword'];
                $isSurcharge = $gt_result['isSurcharge'];
                if (!empty($cardID)) {
                    if ($in_data['BalanceRemaining'] > 0) {

                        $cusproID = 1;
                        $pinv_id                      = '';

                        $name = $in_data['Customer_FullName'];

                        if ($sch_method == "1") {

                            if ($cardID != "new1") {

                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];

                            } else {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                    $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                                }

                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('phone');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            }

                            if (strlen($expmonth) > 1 && $expmonth <= 9) {
                                $expmonth = substr($expmonth, 1);
                            }

                            $payload = array(
                                "amount"          => ($amount * 100),
                                "card"     => array(
                                    "name" => $name,
                                    "number" => $card_no,
                                    "exp_month" => $expmonth,
                                    "exp_year"  => $exyear,
                                    "cvv"             => $cvv,
                                ),
                                "billing_address" => array(
                                    "line1"           => $address1,
                                    "line2" => $address2,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"            => $zipcode,
                                ),
                            );
                            if($cvv == ''){
                                unset($payload['card']['cvv']);
                            }
                           
                        } else if ($sch_method == "2") {
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                    'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                    'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                    'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                    'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                    'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'merchantID'         => $user_id,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }

                            $payload = [
                                "amount" => ($amount * 100),
                                "ach" => [
                                    "name" => $accountDetails['accountName'], 
                                    "account_number" => $accountDetails['accountNumber'], 
                                    "routing_number" => $accountDetails['routeNumber'], 
                                    "phone_number" => $accountDetails['Billing_Contact'], 
                                    "sec_code" => $accountDetails['secCodeEntryMethod'], 
                                    "savings_account" => ($accountDetails['accountType'] == 'savings') ? true : false, 
                                    
                                 
                                ],
                                "address" => [
                                    "line1" => $accountDetails['Billing_Addr1'],
                                    "line2" => $accountDetails['Billing_Addr2'],
                                    "city" => $accountDetails['Billing_City'],
                                    "state" => $accountDetails['Billing_State'],
                                    "postal_code" => $accountDetails['Billing_Zipcode'],
                                    "country" => $accountDetails['Billing_Country']
                                ],
                               
                            ];
                        }
                        
                        $sdk = new iTTransaction();
                        $result = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);

                        if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                            $result['status_code'] = 200;

                            $val = array(
                                'merchantID' => $merchID,
                            );

                            $ispaid  = 1;
                            $st      = 'paid';
                            $bamount = $in_data['BalanceRemaining'] - $amount;
                            if ($bamount > 0) {
                                $ispaid = '0';
                                $st     = 'unpaid';
                            }
                            $app_amount = $in_data['AppliedAmount'] + $amount;
                            $up_data    = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount, 'userStatus' => $st, 'isPaid' => $ispaid, 'TimeModified' => date('Y-m-d H:i:s'));
                            $condition  = array('invoiceID' => $invoiceID, 'merchantID' => $this->merchantID);

                            $this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $up_data);

                            if (isset($this->accessToken) && isset($this->access_token_secret)) {
                                if ($this->fb_account == 1) {
                                    $c = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                                    $payment  = array('invoiceid' => $invoiceID, 'amount' => array('amount' => $amount), 'type' => 'Check', 'date' => date('Y-m-d'));
                                    $invoices = $c->add_payment($payment);
                                } else {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                                <request method="payment.create">
                                                <payment>
                                                    <invoice_id>' . $invoiceID . '</invoice_id>
                                                    <amount>' . $amount . '</amount>
                                                    <currency_code>USD</currency_code>
                                                    <type>Check</type>
                                                </payment>
                                                </request>';
                                    $invoices = $c->add_payment($payment);

                                }

                                if ($invoices['status'] != 'ok') {

                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                   

                                } else {
                                    $pinv_id = $invoices['payment_id'];

                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                }
                            }
                            
                            $ref_number     = $in_data['refNumber'];
                            $tr_date        = date('Y-m-d H:i:s');
                            $toEmail        = $in_data['userEmail'];
                            $company        = $in_data['companyName'];
                            $customer       = $in_data['fullName'];
                            if ($chh_mail == '1') {

                                if($isSurcharge){
                                    $condition_mail = array('templateType' => '16', 'merchantID' => $user_id);
                                    
                                    $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date,$result['id']);
                                }else{
                                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                                    $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                                }
                                
                            }
                            $condition_mail = array('templateType' => '15', 'merchantID' => $user_id);
                            
                            
                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }

                        } else {
                            $err_msg = $result['status'] = $result['error']['message'];
                            $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                        }

                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $pinv_id, $this->resellerID, $in_data['invoiceID'], false, $this->transactionByUser);

                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Validation Error</strong>.</div>');
        }

        if ($cusproID != "") {
            $trans_id     = ($sch_method == "2") ? $result['check_transaction_id'] : $result['id'];
            $invoice_IDs  = array();
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
                'proccess_btn_text' => 'Process New Invoice',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            $this->session->set_userdata("in_data", $in_data);
            if ($cusproID == "1") {
                redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
            }

            redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $cusproID, 'refresh');
        } else {
            if(!$checkPlan){
                $responseId  = '';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
            }
            redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
        }

    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $user_id = $this->merchantID;
        
        $checkPlan = check_free_plan_transactions();
        if ($checkPlan && !empty($this->input->post(null, true))) {
            $this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
            $this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
            if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {
                $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
            }

            if ($this->form_validation->run() == true) {

                $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }
                $custom_data_fields = [];
                // get custom field data
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }

                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];
                $isSurcharge = $gt_result['isSurcharge'];
                $invoiceIDs  = array();
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                }

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
                $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
                $companyID = $comp_data['companyID'];
                $cardID    = $this->czsecurity->xssCleanPostInput('card_list');

                $cvv = '';
                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                        $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                    }

                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    if ($card_data['CardCVV']) {
                        $cvv = $card_data['CardCVV'];
                    }

                    $cardType = $card_data['CardType'];
                    $address1 = $card_data['Billing_Addr1'];
                    $city     = $card_data['Billing_City'];
                    $zipcode  = $card_data['Billing_Zipcode'];
                    $state    = $card_data['Billing_State'];
                    $country  = $card_data['Billing_Country'];
                }

                if (strlen($expmonth) > 1 && $expmonth <= 9) {
                    $expmonth = substr($expmonth, 1);
                }
                
                $city    = $this->czsecurity->xssCleanPostInput('city');
                $country = $this->czsecurity->xssCleanPostInput('country');
                $phone   = $this->czsecurity->xssCleanPostInput('phone');
                $state   = $this->czsecurity->xssCleanPostInput('state');
                $zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
                $name    = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address = $this->czsecurity->xssCleanPostInput('address1') . ' ' . $this->czsecurity->xssCleanPostInput('address2');
                $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
                
                $amount              = $this->czsecurity->xssCleanPostInput('totalamount');

                include APPPATH . 'libraries/Freshbooks_data.php';
                $fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
                $pinv_id                      = '';

                $request_data = array(
                    "amount"          => ($amount * 100),
                    "card"     => array(
                        "name" => $name,
                        "number" => $card_no,
                        "exp_month" => $expmonth,
                        "exp_year"  => $exyear,
                        "cvv"             => $cvv,
                    ),
                    "billing_address" => array(
                        "line1"           => $address1,
                        "line2" => $address2,
                        "city"           => $city,
                        "state"          => $state,
                        "postal_code"            => $zipcode,
                    ),
                );
                if($cvv == ''){
                    unset($request_data['card']['cvv']);
                }

                $meta_data = [];
                if($this->czsecurity->xssCleanPostInput('invoice_id')){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 3);
                    $meta_data['invoice_number'] = $new_invoice_number;
                }

                if($this->czsecurity->xssCleanPostInput('po_number')){
                    $meta_data['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
                if($meta_data){
                    $request_data['metadata'] = $meta_data;
                }

                $sdk = new iTTransaction();
                $result = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);

                $crtxnID = '';

                if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                    $result['status_code'] = 200;
                    
                    $invoiceIDs = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                    }

                    $refNumber = array();

                    if (!empty($invoiceIDs)) {

                        foreach ($invoiceIDs as $inID) {

                            $in_data     = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invoiceID' => $inID));
                            $refNumber[] = $in_data['refNumber'];
                            $amount_data = $amount;
                            $pinv_id     = '';
                            $invoices    = $fb_data->create_invoice_payment($inID, $amount_data);
                            if ($invoices['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
                            } else {
                                $pinv_id = $invoices['payment_id'];

                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                        }
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

                    }
                    
                    if (!empty($refNumber)) {
                        $ref_number = implode(',', $refNumber);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['userEmail'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['fullName'];
                    if ($chh_mail == '1') {
                        if($isSurcharge){
                            $condition_mail = array('templateType' => '16', 'merchantID' => $merchantID);
                            $ref_number     = implode(',', $refNumber);
                            $tr_date        = date('Y-m-d H:i:s');
                            $toEmail        = $comp_data['userEmail'];
                            $company        = $comp_data['companyName'];
                            $customer       = $comp_data['fullName'];
                            $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date,$result['id']);
                        }else{
                            $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                        }
                       
                        
                    }
                    
                    $condition_mail = array('templateType' => '15', 'merchantID' => $user_id);
                   
                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear    = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_type = $this->general_model->getType($card_no);

                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'CardType'                     => $card_type,
                            'CustomerCard'                 => $card_no,
                            'CardCVV'                      => $cvv,
                            'customerListID'               => $customerID,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $this->merchantID,

                            'createdAt'                    => date("Y-m-d H:i:s"),
                            'Billing_Addr1'                => $address1,
                            'Billing_Addr2'                => $address2,
                            'Billing_City'                 => $city,
                            'Billing_State'                => $state,
                            'Billing_Country'              => $country,
                            'Billing_Contact'              => $phone,
                            'Billing_Zipcode'              => $zipcode,
                        );
                        

                        $id1 = $this->card_model->process_card($card_data);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg = $result['status'] = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }

            } else {

                $error = 'Validation Error. Please fill the requred fields';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
            }

        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $result['id'],
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
       
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $condition           = array('merchantID' => $user_id);
        $gateway             = $this->general_model->get_gateway_data($user_id);
        $data['gateways']    = $gateway['gateway'];
        $data['gateway_url'] = $gateway['url'];
        $data['stp_user']    = $gateway['stripeUser'];

        $compdata          = $this->fb_customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/payment_sale', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_auth()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");

        if (!empty($this->input->post(null, true))) {

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }
            
            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];

                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data  = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
                $companyID  = $comp_data['companyID'];

                $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                } else {
                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $cvv       = $card_data['CardCVV'];
                    $exyear    = $card_data['cardYear'];
                }

                if (strlen($expmonth) > 1 && $expmonth <= 9) {
                    $expmonth = substr($expmonth, 1);
                }

                $name    = $this->czsecurity->xssCleanPostInput('fistName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address = $this->czsecurity->xssCleanPostInput('address');
                $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country = $this->czsecurity->xssCleanPostInput('country');
                $city    = $this->czsecurity->xssCleanPostInput('city');
                $state   = $this->czsecurity->xssCleanPostInput('state');
                $amount  = $this->czsecurity->xssCleanPostInput('totalamount');
                $zipcode = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '74035';

                $request_data = array(
                    "amount"          => ($amount * 100),
                    "card"     => array(
                        "name" => $name,
                        "number" => $card_no,
                        "exp_month" => $expmonth,
                        "exp_year"  => $exyear,
                        "cvv"             => $cvv,
                    ),
                    "billing_address" => array(
                        "line1"           => $address1,
                        "line2" => $address2,
                        "city"           => $city,
                        "state"          => $state,
                        "postal_code"            => $zipcode,
                    ),
                    'capture' => false
                );
                if($cvv == ''){
                    unset($request_data['card']['cvv']);
                }
                $sdk = new iTTransaction();
                $result = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);

                $crtxnID = '';

                if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                    $result['status_code'] = 200;
                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {$cid = $this->czsecurity->xssCleanPostInput('customerID');
                        $card_no                        = $this->czsecurity->xssCleanPostInput('card_number');
                        $card_type                      = $this->general_model->getType($card_no);
                        $expmonth                       = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv = $this->czsecurity->xssCleanPostInput('cvv');

                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'CardType'                     => $card_type,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $merchantID,
                            'customerListID'               => $this->czsecurity->xssCleanPostInput('customerID'),
                            'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('baddress1'),
                            'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('baddress2'),
                            'Billing_City'                 => $this->czsecurity->xssCleanPostInput('bcity'),
                            'Billing_Country'              => $this->czsecurity->xssCleanPostInput('bcountry'),
                            'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('bphone'),
                            'Billing_State'                => $this->czsecurity->xssCleanPostInput('bstate'),
                            'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('bzipcode'),
                            'CustomerCard'                 => $card_no,
                            'CardCVV'                      => $cvv,
                            'updatedAt'                    => date("Y-m-d H:i:s"),
                        );

                        $card = $this->card_model->process_card($card_data);

                    }

                    if ($chh_mail == '1') {

                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                        if (!empty($refNumber)) {
                            $ref_number = implode(',', $refNumber);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $comp_data['userEmail'];
                        $company  = $comp_data['companyName'];
                        $customer = $comp_data['fullName'];
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg = $result['status'] = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] :'TXNFAILED'.time();
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $transactiondata = array();
                if (isset($result['id'])) {
                    $transactiondata['transactionID'] = $result['id'];
                } else {
                    $transactiondata['transactionID'] = '';
                }
                $transactiondata['transactionStatus']   = $result['status'];
                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                $transactiondata['transactionCode']     = $result['status_code'];

                $transactiondata['transactionType']    = 'auth';
                $transactiondata['gatewayID']          = $gatlistval;
                $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                $transactiondata['customerListID']     = $this->czsecurity->xssCleanPostInput('customerID');
                $transactiondata['transactionAmount']  = $amount;
                $transactiondata['merchantID']         = $merchantID;
                $transactiondata['resellerID']         = $this->resellerID;
                $transactiondata['gateway']            = iTransactGatewayName;
                $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                $id                                    = $this->general_model->insert_row('customer_transaction', $transactiondata);

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>');
            }

            $invoice_IDs = array();
           

            $receipt_data = array(
                'transaction_id'    => $result['id'],
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         =>  $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');

           
        }

    }

    public function create_customer_esale()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];

                $customerID   = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }

                $payload = [
                    "amount" => ($amount * 100),
                    "ach" => [
                        "name" => $comp_data['firstName']. ' '.$comp_data['lastName'],
                        "account_number" => $accountDetails['accountNumber'],
                        "routing_number" => $accountDetails['routeNumber'], 
                        "phone_number" => $accountDetails['Billing_Contact'], 
                        "sec_code" => $accountDetails['secCodeEntryMethod'],
                        "savings_account" => (strtolower($accountDetails['accountType']) == 'savings') ? true : false, 
                    ],
                    "address" => [
                        "line1" => $accountDetails['Billing_Addr1'],
                        "line2" => $accountDetails['Billing_Addr2'],
                        "city" => $accountDetails['Billing_City'],
                        "state" => $accountDetails['Billing_State'],
                        "postal_code" => $accountDetails['Billing_Zipcode'],
                        "country" => $accountDetails['Billing_Country']
                    ],
                ];
                
                $meta_data = [];
                if($this->czsecurity->xssCleanPostInput('invoice_id')){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 3);
                    $meta_data['invoice_number'] = $new_invoice_number;
                }

                if($this->czsecurity->xssCleanPostInput('po_number')){
                    $meta_data['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
                if($meta_data){
                    $payload['metadata'] = $meta_data;
                }
                $sdk = new iTTransaction();
                $result = $sdk->postACHTransaction($apiUsername, $apiKey, $payload);

                if ($result['status_code'] == '200'  || $result['status_code'] == '201') {
                    $result['status_code'] = 200;

                    $invoiceIDs = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                    }

                    $refNumber = array();

                    if (!empty($invoiceIDs)) {

                        foreach ($invoiceIDs as $inID) {

                            $in_data     = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'AppliedAmount'), array('merchantID' => $this->merchantID, 'invocieID' => $inID));
                            $refNumber[] = $in_data['refNumber'];
                            $amount_data = $amount;
                            $pinv_id     = '';
                            $invoices    = $fb_data->create_invoice_payment($inID, $amount_data);
                            if ($invoices['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
                            } else {
                                $pinv_id = $invoices['payment_id'];

                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount_data, $this->merchantID, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser);
                        }
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser);
                    }

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['msg'] . '</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }
            
            $receipt_data = array(
                'transaction_id'    => isset($result) ? $result['check_transaction_id'] : '',
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('FreshBooks_controllers/Transactions/create_customer_esale', 'refresh');
    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnID');

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $payusername = $gt_result['gatewayUsername'];
            $paypassword = $gt_result['gatewayPassword'];
            $grant_type  = "password";

            $integratorId = $gt_result['gatewaySignature'];

            $payAPI = new PayTraceAPINEW();

            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

            //call a function of Utilities.php to verify if there is any error with OAuth token.
            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
            $amount            = $paydata['transactionAmount'];

            $customerID = $paydata['customerListID'];

            $payload = [
                'amount' => ($amount * 100)
            ];
            $sdk = new iTTransaction();
            
            $result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, $tID);

            if ($result['status_code'] == '200'  || $result['status_code'] == '201') {
                $this->customer_model->update_refund_payment($tID, iTransactGatewayName);

                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );

                $merchID = $paydata['merchantID'];

                $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
                $rs_data         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
                $redID           = $rs_data['resellerID'];
                $condition1      = array('resellerID' => $redID);
                $sub1            = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                $domain1         = $sub1['merchantPortalURL'];
                $URL1            = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

                $subdomain           = $Freshbooks_data['sub_domain'];
                $key                 = $Freshbooks_data['secretKey'];
                $accessToken         = $Freshbooks_data['accessToken'];
                $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                define('OAUTH_CONSUMER_KEY', $subdomain);
                define('OAUTH_CONSUMER_SECRET', $key);
                define('OAUTH_CALLBACK', $URL1);

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                        $imn_amount     = $ref_inv_amount->inv_amount;
                        $p_ids          = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();
                        foreach ($ref_inv as $k => $r_inv) {
                            $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                            if (!empty($inv_data)) {
                                $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                            } else {
                                $due_date = date('Y-m-d');
                            }

                            $re_amount = $imn_amount[$k];
                            if ($refund > 0 && $imn_amount[$k] > 0) {

                                $p_refund1 = 0;

                                $p_id = $p_ids[$k];

                                $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                if ($refund <= $imn_amount[$k]) {
                                    $p_refund1 = $refund;

                                    $p_refund = $imn_amount[$k] - $refund;

                                    $re_amount = $imn_amount[$k] - $refund;
                                    $refund    = 0;

                                } else {
                                    $p_refund  = 0;
                                    $p_refund1 = $imn_amount[$k];
                                    $refund    = $refund - $imn_amount[$k];

                                    $re_amount = 0;
                                }

                                if (isset($accessToken) && isset($access_token_secret)) {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="payment.update">
                                            <payment>

                                                <payment_id>' . $p_id . '</payment_id>
                                                <amount>' . $p_refund . '</amount>
                                                <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                <currency_code>USD</currency_code>
                                                </payment>

                                        </request>';

                                    $invoices = $c->edit_payment($payment);

                                    if ($invoices['status'] != 'ok') {

                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                       

                                    } else {
                                        $ins_id    = $p_id;
                                        $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                            'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                            'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                        );
                                        $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                        $lineArray = '';

                                        foreach ($ittem as $item_val) {

                                            $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                            $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                            $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                            $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                            $lineArray .= '<type>Item</type></line>';

                                        }
                                        $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                        $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                        $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                        $lineArray .= '<quantity>1</quantity>';
                                        $lineArray .= '<type>Item</type></line>';

                                        $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="invoice.update">
                                            <invoice>
                                            <invoice_id>' . $r_inv . '</invoice_id>
                                                <date>' . $due_date . '</date>
                                                <lines>' . $lineArray . '</lines>
                                            </invoice>
                                            </request> ';

                                        $invoices1 = $c->add_invoices($invoice);

                                        $this->session->set_flashdata('success', 'Successfully Refunded Payment');

                                       
                                    }

                                }

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;

                            } else {

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;
                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
                                <request method="payment.update">
                                    <payment>

                                        <payment_id>' . $p_id . '</payment_id>
                                        <amount>' . $p_amount . '</amount>
                                        <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                        <currency_code>USD</currency_code>
                                        </payment>

                                </request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                               
                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                    <request method="invoice.update">
                        <invoice>
                        <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

                            <lines>' . $lineArray . '</lines>
                        </invoice>
                        </request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Successfully Refunded Payment');

                            }
                        }
                    }

                }

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['msg'] . '</strong>.</div>');
            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result['check_transaction_id'];
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = 'refund'; 
            $transactiondata['transactionCode']     = $response['response_code'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName." ECheck";
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('FreshBooks_controllers/Transactions/echeck_transaction', 'refresh');
        }
    }

    public function payment_evoid()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $chh_mail = 0;

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey = $gt_result['gatewayPassword'];

            $amount            = $paydata['transactionAmount'];

            $payload = [];
            $sdk = new iTTransaction();
            
            $result = $sdk->voidCardTransaction($apiUsername, $apiKey, $payload, $tID);

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                $result['status_code'] = '200' ;

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                }

                $this->session->set_flashdata('success', $result['status']);
            } else {
                $err_msg = $result['status'] = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] :'TXNFAILED'.time();

                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }
            
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $tID;
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = 'void'; // $result['type'];
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $paydata['customerListID'];
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName." ECheck";
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('FreshBooks_controllers/Transactions/evoid_transaction', 'refresh');
        }
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];
            
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey = $gt_result['gatewayPassword'];
            $isSurcharge = $gt_result['isSurcharge'];
            $customerID = $paydata['customerListID'];
            $amount    = $paydata['transactionAmount'];
            $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $payload = [
                'amount' => ($paydata['transactionAmount'] * 100)
            ];
            $sdk = new iTTransaction();
            
            $result = $sdk->captureAuthTransaction($apiUsername, $apiKey, $payload, $tID);

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                /* This block is created for saving Card info in encrypted form  */
                $result['status_code'] = '200' ;

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {
                    if($isSurcharge){
                        $condition_mail = array('templateType' => '16', 'merchantID' => $merchantID);
                        
                        $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date,$result['id']);
                    }else{
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
                    }
                    

                }
                
                $this->session->set_flashdata('success', 'Successfully Captured Authorization');
            } else {
                $err_msg = $result['status'] = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $transactiondata = array();
            if (isset($result['id'])) {
                $transactiondata['transactionID'] = $result['id'];
            } else {
                $transactiondata['transactionID'] = '';
            }
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionmodified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionCard']     = $paydata['transactionCard'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['transactionType']     = 'capture';
            $transactiondata['customerListID']      = $paydata['customerListID'];
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName;
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id                                     = $this->general_model->insert_row('customer_transaction', $transactiondata);
            $invoice_IDs = array();
            
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Transactions/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if ($result['id'] == '') {
                $result['id'] = 'null';
            }
            
            redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result['id'], 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_void()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey = $gt_result['gatewayPassword'];

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $customerID = $paydata['customerListID'];
            $amount    = $paydata['transactionAmount'];
            $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $payload = [];
            $sdk = new iTTransaction();
            
            $result = $sdk->voidCardTransaction($apiUsername, $apiKey, $payload, $tID);

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                $result['status_code'] = '200' ;

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                }

                $this->session->set_flashdata('success', $result['status']);
            } else {
                $err_msg = $result['status'] = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();

                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $transactiondata = array();
            if (isset($result['id'])) {
                $transactiondata['transactionID'] = $result['id'];
            } else {
                $transactiondata['transactionID'] = '';
            }
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionCard']     = $paydata['transactionCard'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['transactionType']     = 'void';
            $transactiondata['customerListID']      = $paydata['customerListID'];
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName;
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_refund()
    {

        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            $tID        = $this->czsecurity->xssCleanPostInput('paytxnID');
            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];

            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            
            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey = $gt_result['gatewayPassword'];

            $customerID = $paydata['customerListID'];
            $amount1 = $paydata['transactionAmount'];
            $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
            $amount  = $total;
            
            if ($paydata['transactionCode'] == '200') {
                $payload = [
                    'amount' => ($total * 100)
                ];
                $sdk = new iTTransaction();
                
                $result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, $tID);
            }

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                $result['status_code'] = '200';

                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );

                $merchID = $paydata['merchantID'];

                $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
                $rs_data         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
                $redID           = $rs_data['resellerID'];
                $condition1      = array('resellerID' => $redID);
                $sub1            = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                $domain1         = $sub1['merchantPortalURL'];
                $URL1            = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth'; //print_r($gt_result);die;

                $subdomain           = $Freshbooks_data['sub_domain'];
                $key                 = $Freshbooks_data['secretKey'];
                $accessToken         = $Freshbooks_data['accessToken'];
                $access_token_secret = $Freshbooks_data['oauth_token_secret'];

                define('OAUTH_CONSUMER_KEY', $subdomain);
                define('OAUTH_CONSUMER_SECRET', $key);
                define('OAUTH_CALLBACK', $URL1);

                if (!empty($paydata['invoiceID'])) {
                    $refund  = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (count($ref_inv) > 1) {
                        $ref_inv_amount = json_decode($paydata['invoiceRefID']);
                        $imn_amount     = $ref_inv_amount->inv_amount;
                        $p_ids          = explode(',', $paydata['qbListTxnID']);

                        $inv_array = array();
                        foreach ($ref_inv as $k => $r_inv) {
                            $inv_data = $this->general_model->get_select_data('Freshbooks_test_invoice', array('DueDate'), array('merchantID' => $merchID, 'invoiceID' => $r_inv));

                            if (!empty($inv_data)) {
                                $due_date = date('Y-m-d', strtotime($inv_data['DueDate']));
                            } else {
                                $due_date = date('Y-m-d');
                            }

                            $re_amount = $imn_amount[$k];
                            if ($refund > 0 && $imn_amount[$k] > 0) {

                                $p_refund1 = 0;

                                $p_id = $p_ids[$k];

                                $ittem = $this->fb_customer_model->get_invoice_item_data($r_inv);

                                if ($refund <= $imn_amount[$k]) {
                                    $p_refund1 = $refund;

                                    $p_refund = $imn_amount[$k] - $refund;

                                    $re_amount = $imn_amount[$k] - $refund;
                                    $refund    = 0;

                                } else {
                                    $p_refund  = 0;
                                    $p_refund1 = $imn_amount[$k];
                                    $refund    = $refund - $imn_amount[$k];

                                    $re_amount = 0;
                                }

                                if (isset($accessToken) && isset($access_token_secret)) {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="payment.update">
                                            <payment>

                                                <payment_id>' . $p_id . '</payment_id>
                                                <amount>' . $p_refund . '</amount>
                                                <notes>Payment refund for invoice:' . $r_inv . ' ' . $p_refund1 . ' </notes>
                                                <currency_code>USD</currency_code>
                                                </payment>

                                        </request>';

                                    $invoices = $c->edit_payment($payment);

                                    if ($invoices['status'] != 'ok') {

                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                       

                                    } else {
                                        $ins_id    = $p_id;
                                        $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $p_refund1,
                                            'creditInvoiceID'               => $r_inv, 'creditTransactionID'          => $tID,
                                            'creditTxnID'                   => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                                        );
                                        $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                        $lineArray = '';

                                        foreach ($ittem as $item_val) {

                                            $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                            $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                            $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                            $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                            $lineArray .= '<type>Item</type></line>';

                                        }
                                        $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                        $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                        $lineArray .= '<unit_cost>' . (-$p_refund1) . '</unit_cost>';
                                        $lineArray .= '<quantity>1</quantity>';
                                        $lineArray .= '<type>Item</type></line>';

                                        $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="invoice.update">
                                            <invoice>
                                            <invoice_id>' . $r_inv . '</invoice_id>
                                                <date>' . $due_date . '</date>
                                                <lines>' . $lineArray . '</lines>
                                            </invoice>
                                            </request> ';

                                        $invoices1 = $c->add_invoices($invoice);

                                        $this->session->set_flashdata('success', 'Successfully Refunded Payment');

                                      
                                    }

                                }

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;

                            } else {

                                $inv_array['inv_no'][]     = $r_inv;
                                $inv_array['inv_amount'][] = $re_amount;
                            }

                        }
                        $this->general_model->update_row_data('customer_transaction', $con, array('invoiceRefID' => json_encode($inv_array)));
                    } else {

                        $ittem = $this->fb_customer_model->get_invoice_item_data($paydata['invoiceID']);

                        $p_id     = $paydata['qbListTxnID'];
                        $p_amount = $paydata['transactionAmount'] - $amount;

                        if (isset($accessToken) && isset($access_token_secret)) {
                            $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);

                            $payment = '<?xml version="1.0" encoding="utf-8"?>
                                <request method="payment.update">
                                    <payment>

                                        <payment_id>' . $p_id . '</payment_id>
                                        <amount>' . $p_amount . '</amount>
                                        <notes>Payment refund for invoice:' . $paydata['invoiceID'] . ' ' . $amount . ' </notes>
                                        <currency_code>USD</currency_code>
                                        </payment>

                                </request>';

                            $invoices = $c->edit_payment($payment);

                            if ($invoices['status'] != 'ok') {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                

                            } else {
                                $ins_id    = $p_id;
                                $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $amount,
                                    'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                    'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),
                                );
                                $ppid      = $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                                $lineArray = '';

                                foreach ($ittem as $item_val) {

                                    $lineArray .= '<line><name>' . $item_val['itemDescription'] . '</name>';
                                    $lineArray .= '<description>' . $item_val['itemDescription'] . '</description>';
                                    $lineArray .= '<unit_cost>' . $item_val['itemPrice'] . '</unit_cost>';
                                    $lineArray .= '<quantity>' . $item_val['itemQty'] . '</quantity> <tax1_name>' . $item_val['taxName'] . '</tax1_name><tax1_percent>' . $item_val['taxPercent'] . '</tax1_percent>';
                                    $lineArray .= '<type>Item</type></line>';

                                }
                                $lineArray .= '<line><name>' . $ittem[0]['itemDescription'] . '</name>';
                                $lineArray .= '<description>This is used to refund Incoive payment</description>';
                                $lineArray .= '<unit_cost>' . (-$amount) . '</unit_cost>';
                                $lineArray .= '<quantity>1</quantity>';
                                $lineArray .= '<type>Item</type></line>';

                                $invoice = '<?xml version="1.0" encoding="utf-8"?>
                    <request method="invoice.update">
                        <invoice>
                        <invoice_id>' . $paydata['invoiceID'] . '</invoice_id>

                            <lines>' . $lineArray . '</lines>
                        </invoice>
                        </request> ';

                                $invoices1 = $c->add_invoices($invoice);

                                $this->session->set_flashdata('success', 'Successfully Refunded Payment');

                            }
                        }
                    }

                }

                $this->fb_customer_model->update_refund_payment($tID, iTransactGatewayName);

                $this->session->set_flashdata('success', 'Success ' . $result['status']);
            } else {
                $err_msg = $result['status'] = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();

                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $transactiondata = array();
            if (isset($result['id'])) {
                $transactiondata['transactionID'] = $result['id'];
            } else {
                $transactiondata['transactionID'] = '';
            }
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionCard']     = $paydata['transactionCard'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['transactionType']     = 'refund';
            $transactiondata['customerListID']      = $paydata['customerListID'];
            $transactiondata['transactionAmount']   = $amount;

            if (!empty($paydata['invoiceID'])) {
                $transactiondata['invoiceID'] = $paydata['invoiceID'];
            }
            $transactiondata['merchantID'] = $paydata['merchantID'];
            $transactiondata['resellerID'] = $this->resellerID;
            $transactiondata['gateway']    = iTransactGatewayName;
            $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);

            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            $invoice_IDs = array();
           

            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Transactions/payment_refund',
                'proccess_btn_text' => 'Process New Refund',
                'sub_header'        => 'Refund',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if ($result['id'] == '') {
                $result['id'] = 'null';
            }
            redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result['id'], 'refresh');

           
        }

    }

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $invoicp_id = '';
        $merchantID = $merchID;

        $rs_daata   = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
        $resellerID = $rs_daata['resellerID'];
        $condition1 = array('resellerID' => $resellerID);

        $sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1 = $sub1['merchantPortalURL'];
        $URL1    = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        $val = array(
            'merchantID' => $merchID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);

        $subdomain           = $Freshbooks_data['sub_domain'];
        $key                 = $Freshbooks_data['secretKey'];
        $accessToken         = $Freshbooks_data['accessToken'];
        $access_token_secret = $Freshbooks_data['oauth_token_secret'];

        define('OAUTH_CONSUMER_KEY', $subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
        $cardID  = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway = $this->czsecurity->xssCleanPostInput('gateway1');

        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

        $apiUsername  = $gt_result['gatewayUsername'];
        $apiKey  = $gt_result['gatewayPassword'];
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $cardID != "" || $gateway != "") {

            $invices = $this->czsecurity->xssCleanPostInput('multi_inv');
            if (!empty($invices)) {

                foreach ($invices as $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];

                        if ($cardID == 'new1') {
                            
                            $cardID_upd = $cardID;
                            $c_data     = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID));
                            $companyID  = $c_data['companyID'];

                            $this->load->library('encrypt');
                            $card_no      = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth     = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear       = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv          = $this->czsecurity->xssCleanPostInput('cvv');
                            $cardType     = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            
                            $merchantID = $merchID;

                            $this->db1->where(array('customerListID' => $in_data['Customer_ListID'], 'merchantID' => $merchantID, 'customerCardfriendlyName' => $friendlyname));
                            $this->db1->select('*')->from('customer_card_data');
                            $qq = $this->db1->get();

                            if ($qq->num_rows() > 0) {

                                $cardID    = $qq->row_array()['CardID'];
                                $card_data = array('cardMonth' => $expmonth,
                                    'cardYear'                     => $exyear,
                                    'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                    'CardCVV'                      => '',
                                    'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'               => $in_data['Customer_ListID'],
                                    'companyID'                    => $companyID,
                                    'merchantID'                   => $merchantID,
                                    'customerCardfriendlyName'     => $friendlyname,
                                    'updatedAt'                    => date("Y-m-d H:i:s"));
                                $this->db1->where(array('CardID' => $cardID));
                                $this->db1->update('customer_card_data', $card_data);
                                
                            } else {
                                $is_default = 0;
                                $checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
                                if($checkCustomerCard == 0){
                                    $is_default = 1;
                                }
                                $card_data = array('cardMonth' => $expmonth,
                                    'cardYear'                     => $exyear,
                                    'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                    'CardCVV'                      => '', 
                                    'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'               => $in_data['Customer_ListID'],
                                    'companyID'                    => $companyID,
                                    'merchantID'                   => $merchantID,
                                    'is_default'                   => $is_default,
                                    'customerCardfriendlyName'     => $friendlyname,
                                    'is_default'                   => $is_default,
                                    'createdAt'                    => date("Y-m-d H:i:s"));

                                $this->db1->insert('customer_card_data', $card_data);
                                $cardID = $this->db1->insert_id();
                            }

                        }
                        $card_data = $this->card_model->get_single_card_data($cardID);

                        
                        if (!empty($card_data)) {

                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount   = $pay_amounts;
                            $amount   = $amount - $cr_amount;
                            $card_no  = $card_data['CardNo'];
                            $expmonth = $card_data['cardMonth'];

                            $exyear = $card_data['cardYear'];
                            $cvv = $card_data['CardCVV'];
                            
                            $name    = $in_data['fullName'];
                            $address = $in_data['ShipAddress_Addr1'];
                            $city    = $in_data['ShipAddress_City'];
                            $state   = $in_data['ShipAddress_State'];
                            $zipcode = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                            if (strlen($expmonth) > 1 && $expmonth <= 9) {
                                $expmonth = substr($expmonth, 1);
                            }

                            $request_data = array(
                                "amount"          => $amount,
                                "credit_card"     => array(
                                    "number"           => $card_no,
                                    "expiration_month" => $expmonth,
                                    "expiration_year"  => $exyear),
                                "csc"             => $cvv,
                                "billing_address" => array(
                                    "name"           => $name,
                                    "street_address" => $address,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "zip"            => $zipcode,
                                ),
                            );

                            $request_data = json_encode($request_data);

                            $result   = $payAPI->processTransaction($oauth_token, $request_data, URL_KEYED_SALE);
                            $response = $payAPI->jsonDecode($result['temp_json_response']);

                            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                                $result['status_code'] = '200';

                                if (isset($accessToken) && isset($access_token_secret)) {
                                    $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, null, $subdomain, $accessToken, $access_token_secret);

                                    $payment = '<?xml version="1.0" encoding="utf-8"?>
                                        <request method="payment.create">
                                        <payment>
                                            <invoice_id>' . $invoiceID . '</invoice_id>
                                            <amount>' . $amount . '</amount>
                                            <currency_code>USD</currency_code>
                                            <type>Check</type>
                                        </payment>
                                        </request>';

                                    $invoices = $c->add_payment($payment);
                                    if ($invoices['status'] != 'ok') {

                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

                                        redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');

                                    } else {
                                        $invoicp_id = $invoices['payment_id'];
                                        $this->session->set_flashdata('success', 'Success');

                                    }
                                }

                            } else {
                                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
                                $err_msg = $result['status'] = $result['error']['message'];

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }
                            $transactiondata = array();
                            if (isset($result['id'])) {
                                $transactiondata['transactionID'] = $result['id'];
                            } else {
                                $transactiondata['transactionID'] = '';
                            }
                            $transactiondata['transactionStatus']   = $result['status'];
                            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                            $transactiondata['transactionCode']     = $result['status_code'];
                          
                            $transactiondata['transactionType']    = 'sale';
                            $transactiondata['gatewayID']          = $gateway;
                            $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                            $transactiondata['customerListID']     = $in_data['CustomerListID'];
                            $transactiondata['transactionAmount']  = $amount;
                            $transactiondata['merchantID']         = $merchantID;
                            $transactiondata['invoiceID']          = $invoiceID;
                            $transactiondata['qbListTxnID']        = $invoicp_id;
                            $transactiondata['resellerID']         = $resellerID;
                            $transactiondata['gateway']            = iTransactGatewayName;
                            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

                            if(!empty($this->transactionByUser)){
                                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                            }
                            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
                    }
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Please select invoices</strong>.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');

    }

}
