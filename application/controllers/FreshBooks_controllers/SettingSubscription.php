<?php
include_once APPPATH .'libraries/Manage_payments.php';
include_once APPPATH . 'libraries/Freshbooks_data.php';

class SettingSubscription extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('card_model');
		$this->load->model('customer_model');
		$this->load->model('Freshbook_models/fb_customer_model');
		$this->load->model('Freshbook_models/fb_invoice_model');
		$this->load->model('Freshbook_models/fb_subscription_model');
		$this->load->model('Freshbook_models/freshbooks_model');
		$this->load->model('general_model');
		$this->db1 = $this->load->database('otherdb', true);

		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '3') {
            $this->merchantID = $this->session->userdata('logged_in')['merchID'];
			$this->resellerID = $this->session->userdata('logged_in')['resellerID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
            $this->merchantID = $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		} else {
			redirect('login', 'refresh');
		}

		$val = array(
			'merchantID' => $this->merchantID,
		);
		$Freshbooks_data     = $this->general_model->get_row_data('tbl_freshbooks', $val);
		if (empty($Freshbooks_data)) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
			redirect('FreshBooks_controllers/home/index', 'refresh');
		}
		$this->subdomain     = $Freshbooks_data['sub_domain'];
		$key                = $Freshbooks_data['secretKey'];
		$this->accessToken   = $Freshbooks_data['accessToken'];
		$this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
		$this->fb_account    = $Freshbooks_data['accountType'];
		$condition1 = array('resellerID' => $this->resellerID);
		$sub1 = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
		$domain1 = $sub1['merchantPortalURL'];
		$URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

		define('OAUTH_CONSUMER_KEY', $this->subdomain);
		define('OAUTH_CONSUMER_SECRET', $key);
		define('OAUTH_CALLBACK', $URL1);
		$this->loginDetails = get_names();
	}


	public function index()
	{
		redirect('FreshBooks_controllers/home', 'refresh');
	}

	public function status()
	{
		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		}
		$invoiceID = $this->czsecurity->xssCleanPostInput('subscID');
		$in_data   =    $this->freshbooks_model->get_fb_invoice_data_pay($invoiceID, $merchID);

		if ($in_data) {
			$amount    =	 $in_data['BalanceRemaining'];
			$Customer_ListID = $in_data['Customer_ListID'];
			$amount   = $this->czsecurity->xssCleanPostInput('inv_amount');
			$val = array(
				'merchantID' => $merchID,
			);
			$transaction = array();
			$check      = $this->czsecurity->xssCleanPostInput('check_number');

			include APPPATH . 'libraries/Freshbooks_data.php';
			$fb_data = new Freshbooks_data($this->merchantID, $this->resellerID);
			$invoices    = $fb_data->create_invoice_payment($invoiceID, $amount);
			if ($invoices['status'] != 'ok') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			} else {
				$pinv_id = $invoices['payment_id'];

				$this->session->set_flashdata('success', 'Successfully Processed Invoice');
			}

			$transaction = array();
			$transaction['transactionID']       = $check;
			$transaction['transactionStatus']    = 'Offline Payment';
			$transaction['transactionDate']     =  date('Y-m-d H:i:s');
			$transaction['transactionModified']      = date('Y-m-d H:i:s');
			$transaction['transactionCode']     = '100';
			$transaction['transactionType']    = "Offline Payment";
			$transaction['customerListID']       = $in_data['Customer_ListID'];
			$transaction['transactionAmount']   = $amount;
			$transaction['merchantID']          = $merchID;
			$transaction['resellerID']      = $this->resellerID;
			$transaction['qbListTxnID']       = $pinv_id;

			$ispaid 	 = 'true';
			$status = $this->czsecurity->xssCleanPostInput('status');

			$CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);   
			$id = $this->general_model->insert_row('customer_transaction',   $transaction);
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong>Invalid Invoice </div>');
		}
		redirect(base_url('FreshBooks_controllers/Freshbooks_invoice/Invoice_details'), 'refresh');
	}

	public function subscriptions()
	{

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$data['merchantID']     =  $user_id;

		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

		$data['subscriptions']   = $this->fb_subscription_model->get_subscriptions_plan_data($user_id);
		
		$data['subs_data']        = $this->fb_customer_model->get_total_subscription($user_id);
		$this->load->view('template/template_start', $data);

		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/page_subscriptions', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function create_subscription()
	{

		if ($this->session->userdata('logged_in')) {
			$da     = $this->session->userdata('logged_in');
			$user_id                = $da['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da     = $this->session->userdata('user_logged_in');
			$user_id                = $da['merchantID'];
		}
		
		if (!empty($this->input->post(null, true))) {
            $subscriptionID = false;

			$in_prefix = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
			if (empty($in_prefix)) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');
				redirect('FreshBooks_controllers/SettingSubscription/create_subscription', 'refresh');
			}

            $total = 0;
			$current_date = date('Y-m-d');
            
            $taxList = $this->czsecurity->xssCleanPostInput('is_tax_check');
            foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
                $insert_row['itemListID']   = $this->czsecurity->xssCleanPostInput('productID')[$key];
                $insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
                $insert_row['itemRate']     = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                if(isset($taxList[$key])){
                    $insert_row['itemTax'] = $taxList[$key];
                }else{
                    $insert_row['itemTax'] = '';
                }
                
                if(empty($insert_row['itemTax'])){
					$insert_row['itemTax'] = 0;
				} else {
					$insert_row['itemTax'] = 1;
				}

                $insert_row['itemFullName']    = $this->czsecurity->xssCleanPostInput('description')[$key];
                $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
                if (($this->czsecurity->xssCleanPostInput('onetime_charge')[$key]) && $this->czsecurity->xssCleanPostInput('onetime_charge')[$key] != 'Recurring') {
                    $insert_row['oneTimeCharge'] = '1';
                } else {
                    $insert_row['oneTimeCharge'] = '0';
                }

				$insert_row['totalTaxItems'] = $this->czsecurity->xssCleanPostInput('totalTaxItems')[$key];

                $total          = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
                $item_val[$key] = $insert_row;
            }

            $sname      = $this->czsecurity->xssCleanPostInput('sub_name');
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $plan       = $this->czsecurity->xssCleanPostInput('duration_list');
            if ($plan > 0) {
                $subsamount = $total / $plan;
            } else {
                $subsamount = $total;
            }

            $st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
            $invoice_date = $first_date = $st_date;
            
            $freetrial = $this->czsecurity->xssCleanPostInput('freetrial');

            $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
            $address1 = $this->czsecurity->xssCleanPostInput('address1');
            $address2 = $this->czsecurity->xssCleanPostInput('address2');
            $country  = $this->czsecurity->xssCleanPostInput('country');
            $state    = $this->czsecurity->xssCleanPostInput('state');
            $city     = $this->czsecurity->xssCleanPostInput('city');
            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
            $phone    = $this->czsecurity->xssCleanPostInput('phone');
            $paycycle = $this->czsecurity->xssCleanPostInput('paycycle');
            $planid   = $this->czsecurity->xssCleanPostInput('sub_plan');

            $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
            $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
            $bcity     = $this->czsecurity->xssCleanPostInput('bcity');
            $bstate    = $this->czsecurity->xssCleanPostInput('bstate');
            $bcountry  = $this->czsecurity->xssCleanPostInput('bcountry');
            $bphone    = $this->czsecurity->xssCleanPostInput('bphone');
            $bzipcode  = $this->czsecurity->xssCleanPostInput('bzipcode');

            if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
                $paygateway = $this->czsecurity->xssCleanPostInput('gateway_list');
            } else {
                $paygateway = 0;
            }

            if ($this->czsecurity->xssCleanPostInput('taxes') != '') {
                $taxval = $this->czsecurity->xssCleanPostInput('taxes');
            } else {
                $taxval = 0;
            }
            
            $end_date = date('Y-m-d', strtotime($st_date . "+$plan months"));

            $totalInv = ($plan > 0) ? $plan + $freetrial - 1 : 1;
            $nextGeneratingDate = $st_date;
            $fromdateCus =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($st_date)) . " -1 day"));
            $conditionGt                = array('planID' => $planid);

            $subplansData       = $this->general_model->get_row_data('tbl_subscriptions_plan_fb', $conditionGt);
            if($subplansData['proRate']){
                $proRate = $subplansData['proRate']; 
            }else{
                $proRate = 0;
            }
            if($subplansData['proRateBillingDay']){
                $proRateday = $subplansData['proRateBillingDay'];  
            }
            else{
                $proRateday = 1;             
            }
            
			if ($paycycle == 'dly') {
                $totalInv   = ($totalInv) ? $totalInv : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv day"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 day"));
            } else if ($paycycle == '1wk') {
                $totalInv   = ($totalInv) ? $totalInv : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 week"));
            } else if ($paycycle == '2wk') {
                $totalInv   = ($totalInv) ? $totalInv + 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 week"));
            } else if ($paycycle == 'mon') {
                
                $totalInv   = ($totalInv) ? $totalInv + 1 : '1';
                if($proRate==1){
                    $end_date =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));  
                    $nextGeneratingDate =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
                } else {
                    $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));  
                    $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
                }

            } else if ($paycycle == '2mn') {
                $totalInv   = ($totalInv) ? $totalInv + 2 * 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 month"));

            } else if ($paycycle == 'qtr') {
                $totalInv   = ($totalInv) ? $totalInv + 3 * 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +3 month"));

            } else if ($paycycle == 'six') {
                $totalInv   = ($totalInv) ? $totalInv + 6 * 1 : '1';
                $end_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +6 month"));
            } else if ($paycycle == 'yrl') {
                $totalInv   = ($totalInv) ? $totalInv + 12 * 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +12 month"));
            } else if ($paycycle == '2yr') {
                $totalInv   = ($totalInv) ? $totalInv + 2 * 12 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +24 month"));
            } else if ($paycycle == '3yr') {
                $totalInv   = ($totalInv) ? $totalInv + 3 * 12 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +36 month"));
            }
            
            if ($this->czsecurity->xssCleanPostInput('autopay')) {

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card         = array();
                    $card_type    = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                    $friendlyName = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);

                    $card['card_number']     = $this->czsecurity->xssCleanPostInput('card_number');
                    $card['expiry']          = $this->czsecurity->xssCleanPostInput('expiry');
                    $card['expiry_year']     = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $card['cvv']             = '';
                    $card['friendlyname']    = $friendlyName;
                    $card['customerID']      = $customerID;
                    $card['Billing_Addr1']   = $this->czsecurity->xssCleanPostInput('baddress1');
                    $card['Billing_Addr2']   = $this->czsecurity->xssCleanPostInput('baddress2');
                    $card['Billing_City']    = $this->czsecurity->xssCleanPostInput('bcity');
                    $card['Billing_State']   = $this->czsecurity->xssCleanPostInput('bstate');
                    $card['Billing_Contact'] = $this->czsecurity->xssCleanPostInput('bphone');
                    $card['Billing_Country'] = $this->czsecurity->xssCleanPostInput('bcountry');
                    $card['Billing_Zipcode'] = $this->czsecurity->xssCleanPostInput('bzipcode');
                    
                    $card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
                    if (!empty($card_data)) {
                        $this->card_model->update_card($card, $customerID, $friendlyName);
                        $cardID = $card_data['CardID'];
                    } else {

                        $cardID = $this->card_model->insert_new_card($card);
                    }
                }else if ($this->czsecurity->xssCleanPostInput('acc_number') != "") {
                    $card = array();
                    $acc_number         = $this->czsecurity->xssCleanPostInput('acc_number');
                    $route_number       = $this->czsecurity->xssCleanPostInput('route_number');
                    $acc_name           = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCode            = 'WEB';
                    $acct_type          = $this->czsecurity->xssCleanPostInput('acct_type');
                    $acct_holder_type   = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                    
                    $friendlyName = 'Checking - ' . substr($acc_number, -4);

                    $insert_array =  array(
                        'accountNumber'  => $acc_number,
                        'routeNumber'    => $route_number,
                        'accountName' => $acc_name,
                        'secCodeEntryMethod'      =>$secCode,
                        'accountType'      => $acct_type,
                        'accountHolderType'      => $acct_holder_type,
                        'CardType' => 'Echeck',
                        'Billing_Addr1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'   => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_State'  => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('bphone'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID' => $customerID,
                        'merchantID'     => $user_id,
                        'customerCardfriendlyName' => $friendlyName,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        
                    );
                    $con  = array(
                        'customerListID' => $customerID,
                        'merchantID'     => $user_id,
                        'customerCardfriendlyName' => $friendlyName
                    );

                    $card_data = $this->card_model->chk_card_firendly_name($customerID, $friendlyName);


                    if ($card_data > 0) {
                        $cardID = $this->card_model->update_card_data($con, $insert_array);
                        $cardID  = $card_data['CardID'];
                    } else {

                        $cardID = $this->card_model->insert_card_data($insert_array);
                    }
                }
            }

			$subdata = array(
                'baddress1'          => $this->czsecurity->xssCleanPostInput('baddress1'),
                'baddress2'          => $this->czsecurity->xssCleanPostInput('baddress2'),
                'bcity'              => $this->czsecurity->xssCleanPostInput('bcity'),
                'bstate'             => $this->czsecurity->xssCleanPostInput('bstate'),
                'bcountry'           => $this->czsecurity->xssCleanPostInput('bcountry'),
                'bphone'             => $this->czsecurity->xssCleanPostInput('bphone'),
                'bzipcode'           => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'subscriptionName'   => $sname,
                'customerID'         => $customerID,
                'subscriptionPlan'   => $plan,
                'subscriptionAmount' => $total,
                'nextGeneratingDate'    =>$nextGeneratingDate,
                'firstDate'          => $first_date,
                'startDate'          => $st_date,
                'endDate'            => $end_date,
                'paymentGateway'     => $paygateway,
                'address1'           => $address1,
                'address2'           => $address2,
                'country'            => $country,
                'state'              => $state,
                'city'               => $city,
                'zipcode'            => $zipcode,
                'contactNumber'      => $phone,
                'totalInvoice'       => $plan,
                'invoicefrequency'   => $paycycle,
                'freeTrial'          => $freetrial,
                'taxID'              => $taxval,
                'planID'             => $planid,
            );

            if ($this->czsecurity->xssCleanPostInput('autopay')) {
                $subdata['cardID']           = $cardID;
                $subdata['automaticPayment'] = '1';
            } else {
                $subdata['cardID']           = 0;
                $subdata['automaticPayment'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('email_recurring')) {

                $subdata['emailRecurring'] = '1';
            } else {
                $subdata['emailRecurring'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('billinfo')) {

                $subdata['usingExistingAddress'] = '1';
            } else {
                $subdata['usingExistingAddress'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('subID') != "") {
                $rowID = $subscriptionID = $this->czsecurity->xssCleanPostInput('subID');
                $subdata['updatedAt'] = date('Y-m-d H:i:s');

                $subs = $this->general_model->get_row_data('tbl_subscriptions_fb', array('subscriptionID' => $rowID));
                $date = $first_date;

                $in_num = $subs['generatedInvoice'];
                if(strtotime($current_date) <= strtotime($st_date) && $subs['generatedInvoice'] == 0){
					$subdata['nextGeneratingDate'] = $st_date;
				}

                $subdata['generatedInvoice'] = $in_num;
                $subdata['merchantDataID'] = $user_id;

                $ins_data = $this->general_model->update_row_data('tbl_subscriptions_fb', array('subscriptionID' => $rowID), $subdata);

                $this->general_model->delete_row_data('tbl_subscription_invoice_item_fb', array('subscriptionID' => $rowID));
                $this->general_model->delete_row_data('tbl_freshbook_subscription_item_tax', array('subscriptionID' => $rowID));

                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $rowID;

					$totalTaxItems = $item['totalTaxItems'];
					unset($item['totalTaxItems']);

                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item_fb', $item);
					if($totalTaxItems != ''){
						$totalTaxItems = json_decode($totalTaxItems, true);
						foreach($totalTaxItems as $taxItem){
							$taxData = [
								'itemID' => $ins,
								'subscriptionID' => $rowID,
								'taxID' => $taxItem['id'],
								'merchantID' => $user_id,
							];
							$this->general_model->insert_row('tbl_freshbook_subscription_item_tax', $taxData);
						}
					}
                }
            } else {
                $subdata['merchantDataID']     = $user_id;
                $subdata['createdAt']          = date('Y-m-d H:i:s');
                $subdata['generatedInvoice'] = 0;
                if(strtotime($current_date) <= strtotime($st_date)){
					$subdata['nextGeneratingDate'] = $st_date;
				}
                
                $ins_data  = $subscriptionID = $this->general_model->insert_row('tbl_subscriptions_fb', $subdata);

                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $ins_data;

					$totalTaxItems = $item['totalTaxItems'];
					unset($item['totalTaxItems']);

                    $ins = $this->general_model->insert_row('tbl_subscription_invoice_item_fb', $item);
					if($totalTaxItems != ''){
						$totalTaxItems = json_decode($totalTaxItems, true);
						foreach($totalTaxItems as $taxItem){
							$taxData = [
								'itemID' => $ins,
								'subscriptionID' => $rowID,
								'taxID' => $taxItem['id'],
								'merchantID' => $user_id,
							];
							$this->general_model->insert_row('tbl_freshbook_subscription_item_tax', $taxData);
						}
					}
                }
            }
            if ($ins_data && $ins) {
				if($subdata['generatedInvoice'] == 0){
					$subdata['subscriptionID'] = $subscriptionID;
					$subdata['subplansData'] = $subplansData;
					$subdata['subscriptionItems'] = $item_val;
					$this->_createSubscriptionFirstInvoice($subdata);
				}
                $this->session->set_flashdata('success', 'Successfully Created Subscription');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
            }

            redirect('FreshBooks_controllers/SettingSubscription/subscriptions', 'refresh');
        }

		if ($this->uri->segment('4') != "") {

			$sbID = $this->uri->segment('4');
			$data['subs']		= $this->general_model->get_row_data('tbl_subscriptions_fb', array('subscriptionID' => $sbID));
			$c_cards    = $this->get_card_expiry_data($data['subs']['customerID']);
			$data['c_cards']  = $c_cards;
			$data['items']      = $this->fb_invoice_model->get_subscription_item_data($sbID, $user_id);
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);
		$condition11				= array('merchantDataID' => $user_id);

		$merchant_condition = [
			'merchID' => $user_id,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$data['subplans']		= $this->general_model->get_table_data('tbl_subscriptions_plan_fb', $condition11);

		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
		$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan', '');
		$data['plans'] = $this->general_model->get_table_data('Freshbooks_test_item', $condition);

		$compdata				= $this->fb_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$taxes = $this->general_model->get_table_data('tbl_taxes_fb', $condition);
		$data['taxes'] = $taxes;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('FreshBooks_views/create_subscription', $data);
		$this->load->view('FreshBooks_views/page_fb_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function get_tax_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('tax_id');
		$val = array(
			'taxID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_taxes_fb', $val);
		echo json_encode($data);
	}


	public function update_gateway()
	{

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}

			if (!empty($this->czsecurity->xssCleanPostInput('gate')) &&  !empty($this->czsecurity->xssCleanPostInput('gateway_old')) && !empty($this->czsecurity->xssCleanPostInput('gateway_new'))) {
				$sub_ID = implode(',', $this->czsecurity->xssCleanPostInput('gate'));


				$old_gateway =  $this->czsecurity->xssCleanPostInput('gateway_old');
				$new_gateway =  $this->czsecurity->xssCleanPostInput('gateway_new');

				$condition	  = array('merchantDataID' => $user_id, 'paymentGateway' => $old_gateway);
				$num_rows    = $this->general_model->get_num_rows('tbl_subscriptions_fb', $condition);

				if ($num_rows > 0) {
					$update_data = array('paymentGateway' => $new_gateway);

					$sss = $this->db->query("update tbl_subscriptions_fb set paymentGateway='" . $new_gateway . "' where subscriptionID IN ($sub_ID) ");

					if ($sss) {

						$this->session->set_flashdata('success', 'Successfully Updated');
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In update process. </div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> No data selected. </div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Validation Error. </div>');
			}
		}
		redirect('FreshBooks_controllers/SettingSubscription/subscriptions', 'refresh');
	}


	//------------- To edit gateway ------------------//

	public function get_gateway_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gateway_id');
		$merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
		$val = array(
			'paymentGateway' => $id,
			'merchantDataID' => $merchantID
		);

		$datas = $this->fb_subscription_model->get_subscriptions_gatway_data($val);

		?>


		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-left col-md-6"> <strong> Customer Name</strong></th>

					<th class="text-center col-md-6"><strong>Select List </strong></th>

				</tr>
				<?php
				if (!empty($datas)) {
					foreach ($datas as $c_data) {
				?>

						<tr>
							<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['fullName']; ?> </td>

							<td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['subscriptionID']; ?>" /> </td>


						</tr>


					<?php     }
				} else { ?>

					<tr>
						<td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Record Found..!"; ?> </td>

						<td class="text-right visible-lg col-md-6 control-label"> </td>

					<?php } ?>

			</tbody>
		</table>


		<?php die;
	}

	public function delete_subscription()
	{

		$subscID = $this->czsecurity->xssCleanPostInput('fbsubscID');
		$condition =  array('subscriptionID' => $subscID);
		$this->general_model->delete_row_data('tbl_subscription_invoice_item_fb', $condition);

		$del  = $this->general_model->delete_row_data('tbl_subscriptions_fb', $condition);
		if ($del) {

			$this->session->set_flashdata('success', 'Successfully Deleted');
			array('status' => "success");
			echo json_encode(array('status' => "success"));
			die;
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
		}

		return false;
	}

	public function get_item_data()
	{

		if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemID = $this->czsecurity->xssCleanPostInput('itemID');
			$itemdata = $this->general_model->get_row_data('Freshbooks_test_item', array('ListID' => $itemID));
			echo json_encode($itemdata);
			die;
		}
		return false;
	}

	public function get_subplan()
	{

		$planid =   $this->czsecurity->xssCleanPostInput('planID');

		$val = $this->fb_subscription_model->get_subplan_data($planid);

		echo json_encode($val);
	}

	public function check_friendly_name($cid, $cfrname)
	{
		$card = array();
		$this->load->library('encrypt');
		$query1 = $this->db1->query("Select * from customer_card_data where customerListID = '" . $cid . "' and customerCardfriendlyName ='" . $cfrname . "' ");
		$card = $query1->row_array();

		return $card;
	}

	public function get_card_expiry_data($customerID)
	{

		$card = array();
		$this->load->library('encrypt');

		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'    ";
		$query1 = $this->db1->query($sql);
		$card_datas =   $query1->result_array();
		if (!empty($card_datas)) {
			foreach ($card_datas as $key => $card_data) {

				$customer_card['CardNo']  = substr($this->card_model->decrypt($card_data['CustomerCard']), 12);
				$customer_card['CardID'] = $card_data['CardID'];
				$customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
				$card[$key] = $customer_card;
			}
		}

		return  $card;
	}

	protected function _createSubscriptionFirstInvoice($subs){
        $merchantID = $merchID = $subs['merchantDataID'];
        $customerID = $subs['customerID'];

        $autopay    = 0;
        $cardID     = $subs['cardID'];
        $paygateway = $subs['paymentGateway'];
        $autopay    = $subs['automaticPayment'];

        if ($subs['generatedInvoice'] < $subs['freeTrial']) {
            $free_trial = '1';
        } else {
            $free_trial = '0';
        }

        $total = $recurring = $onetime = 0;

        $cs_data   = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

        $isInvoiceExist = $this->general_model->get_row_data('tbl_subscription_auto_invoices', array('app_type' => 3, 'subscriptionID' => $subs['subscriptionID']));

        $item_val = [];
        $products = $subs['subscriptionItems'];
        foreach ($products as $key => $prod) {
			$item = [];

            if($isInvoiceExist && $prod['oneTimeCharge']){
                continue;
            }
            
            $insert_row['itemID'] = $prod['itemListID'];
			$prods = $this->general_model->get_select_data('Freshbooks_test_item', array('Name'), array('productID' => $prod['itemListID'], 'merchantID' => $this->merchantID));
			$iname = $prods['Name'];
			$insert_row['itemListName'] = $iname;
			$item['name']               = $insert_row['itemFullName'] = $iname;
			$item['qty']                = $insert_row['itemQuantity'] = $prod['itemQuantity'];
			$insert_row['itemRate'] = $prod['itemRate'];
			$item['description'] = $insert_row['itemDescription'] = $prod['itemDescription'];
			$insert_row['itemTotal'] = $itemTotal = $prod['itemQuantity'] * $prod['itemRate'];
			
			$taxID = 0;
			$tname = '';
			$trate = 0;
			$taxval  = 0;

			$item['type'] = 0;
			
            if($prod['oneTimeCharge']){
                $onetime += $itemTotal;
            } else {
                $recurring += $itemTotal;
            }
            $total = $total + $itemTotal;

			$totalTaxItems = $prod['totalTaxItems'];
			if($totalTaxItems != ''){
				$totalTaxItems = json_decode($totalTaxItems, true);
				
				$taxIndex = 1;
				foreach($totalTaxItems as $taxItem){
					$item["taxName$taxIndex"] = $taxItem['friendlyName'];
					$item["taxNumber$taxIndex"] = $taxItem['number'];
					$item["taxAmount$taxIndex"] =  $taxItem['taxRate'];
					++$taxIndex;
				}
			}

			$item['unit_cost'] = array('amount' => $insert_row['itemRate'], 'code' => 'USD');
			$item_val[] = $insert_row;
			$fb_items[] = $item;
        }

        $val = array(
            'merchantID' => $subs['merchantDataID'],
        );

        $chh_mail = $subs['emailRecurring'];

        $inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
        if (!empty($inv_data)) {
            $inv_pre    = $inv_data['prefix'];
            $inv_po     = $inv_data['postfix'] + 1;
            $new_inv_no = $inv_pre . $inv_po;
            $Number     = $new_inv_no;
        }

        $customer_ref = $subs['customerID'];
        $address1     = $subs['address1'];
        $address2     = $subs['address2'];
        $country      = $subs['country'];
        $state        = $subs['state'];
        $city         = $subs['city'];
        $zipcode      = $subs['zipcode'];
        $phone        = $subs['bphone'];
        $baddress1    = $subs['baddress1'];
        $baddress2    = $subs['baddress2'];
        $bcountry     = $subs['bcountry'];
        $bstate       = $subs['bstate'];
        $bcity        = $subs['bcity'];
        $bzipcode     = $subs['bzipcode'];
        $bphone       = $subs['bphone'];

        $inv_date = $duedate = date($subs['nextGeneratingDate']);

        $fullname  = $cs_data['fullName'];
        $companyID = $cs_data['companyID'];

        $date      = $subs['firstDate'];

        if ($subs['subplansData']['proRate']) {
            $proRate = $subs['subplansData']['proRate'];
        } else {
            $proRate = 0;
        }

        if ($subs['subplansData']['proRateBillingDay']) {
            $proRateday = $subs['subplansData']['proRateBillingDay'];
            $nextInvoiceDate = $subs['subplansData']['nextMonthInvoiceDate'];
        } else {
            $proRateday = 1;
			$nextInvoiceDate = date('d', strtotime($date));
        }

        $in_num    = $subs['generatedInvoice'];
        $paycycle  = $subs['invoicefrequency'];

        $prorataCalculation = prorataCalculation([
            'paycycle' => $subs['invoicefrequency'],
            'proRateday' => $proRateday,
            'nextInvoiceDate' => $nextInvoiceDate,
            'total_amount' => $total,
            'onetime' => $onetime,
            'recurring' => $recurring,
            'date' => $date,
            'in_num' => $in_num,
            'proRate' => $proRate,
        ]);

        $next_date = $prorataCalculation['next_date'];
        $total = $prorataCalculation['total_amount'];

		foreach ($fb_items as $key => $singleItem) {
			$prorataCalculatedRate = 1;
			if($products[$key]['oneTimeCharge'] == 0){
				$prorataCalculatedRate = $prorataCalculation['prorataCalculatedRate'];
			}
			
			$itemCost = $singleItem['unit_cost']['amount'] * $prorataCalculatedRate;
			$fb_items[$key]['unit_cost']['amount'] = $itemCost;
		}

		$freshbookSync  = new Freshbooks_data($this->merchantID, $this->resellerID);
		$syncObejct = new FreshbooksNew(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);
        $invoice = array(
			'customerid' => $customer_ref, 'create_date' => date('Y-m-d', strtotime($date)), 'due_offset_days' => 0, 'invoice_number' => $Number, 'country' => $bcountry, 'province' => $bstate, 'street' => $baddress2, 'address' => $baddress1, 'city' => $bcity,
			'status' => 1, "lines" => $fb_items
		);

		$newInvoice = $syncObejct->add_invoices($invoice, false);

        if ($newInvoice) {
			$syncObejct->storeInvoiceDB([$newInvoice]);
			$invID = $newInvoice->invoiceid;

            $schedule['invoiceID']  = $invID;
            $schedule['gatewayID']  = $paygateway;
            $schedule['cardID']     = $cardID;
            $schedule['customerID'] = $customerID;

            if ($autopay) {
                $schedule['autoInvoicePay']       = 1;
                $schedule['paymentMethod'] = 1;
            } else {
                $schedule['autoInvoicePay']       = 0;
                $schedule['paymentMethod'] = 0;
            }
            $schedule['merchantID'] = $merchID;
            $schedule['createdAt']  = date('Y-m-d H:i:s');
            $schedule['updatedAt']  = date('Y-m-d H:i:s');
            $this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);

            $subscription_auto_invoices_data = [
                'subscriptionID' => $subs['subscriptionID'],
                'invoiceID'      => $invID,
                'app_type'       => 3, // Freshbook
            ];

            $this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);

            $first_date = strtotime($date);
            $current_time = time();
            if($subs['automaticPayment'] == 1 && $first_date <= $current_time){
                $paymentObj = new Manage_payments($subs['merchantDataID']);
                $paymentDetails =   $this->card_model->get_single_card_data($subs['cardID']);
                $saleData = [
                    'paymentDetails' => $paymentDetails,
                    'transactionByUser' => $this->transactionByUser,
                    'ach_type' => (!empty($paymentDetails['CardNo'])) ? 1 : 2,
                    'invoiceID'	=> $invID,
                    'crtxnID'	=> '',
                    'companyName'	=> $cs_data['companyName'],
                    'fullName'	=> $cs_data['fullName'],
                    'firstName'	=> $cs_data['firstName'],
                    'lastName'	=> $cs_data['lastName'],
                    'contact'	=> $cs_data['phoneNumber'],
                    'email'	=> $cs_data['userEmail'],
                    'amount'	=> $total,
                    'gatewayID' => $subs['paymentGateway'],
                    'storeResult' => true,
                    'customerListID' => $cs_data['Customer_ListID'],
                ];
    
                $saleData = array_merge($saleData, $subs);
                $paidResult = $paymentObj->_processSaleTransaction($saleData);
                if(isset($paidResult['response']) && $paidResult['response']){
					$invoicePayment = $freshbookSync->create_invoice_payment($invID, $total);
					if ($invoicePayment['status'] != 'ok') {
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
					} else {
						$crtxnID = $invoicePayment['payment_id'];
					}

                    $this->general_model->update_row_data('customer_transaction', ['id' => $paidResult['id']], ['qbListTxnID' => $crtxnID]);
                }
            }

			$gen_inv = $subs['generatedInvoice'] + 1;
			$this->general_model->update_row_data('tbl_subscriptions_fb',
				array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));
        }
    }

	public function subscription_invoices()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$sbID = $this->uri->segment(4);
        $data['subs'] = $this->general_model->get_row_data('tbl_subscriptions_fb', array('subscriptionID' => $sbID));

        $conditionGt                = array('planID' => $data['subs']['planID']);
        $conditionCust              = array('Customer_ListID' => $data['subs']['customerID'],'merchantID' =>$user_id);

        $subplansData       = $this->general_model->get_row_data('tbl_subscriptions_plan_fb', $conditionGt);
        $subplansCustomer       = $this->general_model->get_row_data('Freshbooks_custom_customer', $conditionCust);

		$data['page_num']      = 'customer_qbo';
		$condition				  = array('merchantID' => $user_id);
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);

		$invoices    			 = $this->fb_invoice_model->get_subscription_invoice_data($user_id, $sbID);
		$data['totalInvoice'] = count($invoices);

        $data['revenue']             = $this->fb_invoice_model->get_subscription_revenue($user_id, $sbID);

        $data['customerName'] = $subplansCustomer['fullName'];
        $data['planName'] = $subplansData['planName'];

		$condition  = array('comp.merchantID' => $user_id);
        
		$data['credits'] = $this->general_model->get_credit_user_data($condition);
        
		$data['invoices']    = $invoices;
        
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;
        
		$this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('FreshBooks_views/page_subscription_details', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
	}
}
