<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

include_once APPPATH . 'libraries/Manage_payments.php';
include_once APPPATH . 'libraries/Freshbooks_data.php';

class Payments extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->config('fluidpay');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->library('form_validation');
        $this->load->model('Freshbook_models/freshbooks_model');
        $this->load->model('Freshbook_models/fb_customer_model');
        $this->load->model('Freshbook_models/fb_company_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;

        $val = array(
            'merchantID' => $this->merchantID,
        );
        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
        if (empty($Freshbooks_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
            redirect('FreshBoolks_controllers/home/index', 'refresh');
        }

        $this->subdomain           = $Freshbooks_data['sub_domain'];
        $key                       = $Freshbooks_data['secretKey'];
        $this->accessToken         = $Freshbooks_data['accessToken'];
        $this->access_token_secret = $Freshbooks_data['oauth_token_secret'];
        $this->fb_account          = $Freshbooks_data['accountType'];
        $condition1                = array('resellerID' => $this->resellerID);
        $sub1                      = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
        $domain1                   = $sub1['merchantPortalURL'];
        $URL1                      = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';

        define('OAUTH_CONSUMER_KEY', $this->subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');

        $user_id   = $this->merchantID;
        $cusproID  = '';
        $error     = '';
        $cusproID  = $this->czsecurity->xssCleanPostInput('customerProcessID');
        $checkPlan = check_free_plan_transactions();

        if ($this->form_validation->run() == true) {

            $cusproID  = '';
            $error     = '';
            $cusproID  = $this->czsecurity->xssCleanPostInput('customerProcessID');
            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
            $cardID    = $this->czsecurity->xssCleanPostInput('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
            }

            // $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
            }

            $gateway = $gatlistval;

            $amount = $this->czsecurity->xssCleanPostInput('inv_amount');

            $condition2 = array('merchantID' => $this->merchantID, 'invoiceID' => $invoiceID);
            $in_data    = $this->general_model->get_row_data('Freshbooks_test_invoice', $condition2);

            $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $responseId = '';
            if ($checkPlan && !empty($in_data)) {

                $in_data['Customer_ListID'] = $in_data['CustomerListID'];
                $gt_result                  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $customerID                 = $in_data['CustomerListID'];
                $customerData               = $this->general_model->get_select_data('Freshbooks_custom_customer', array('*'), array('Customer_ListID' => $customerID, 'merchantID' => $this->merchantID));

                $CustomerListID = $in_data['CustomerListID'];

                $cardID_upd = '';

                if (!empty($cardID)) {

                    $paymentObj = new Manage_payments($this->merchantID);
                    if ($cardID != 'new1') {
                        $accountDetails = $this->card_model->get_single_card_data($cardID);
                    } else {
                        if ($sch_method == "1") {
                            $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                            $cardType = $this->general_model->getType($card_no);

                            $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                            $accountDetails = [
                                'cardMonth'       => $expmonth,
                                'cardYear'        => $exyear,
                                'CardType'        => $cardType,
                                'CustomerCard'    => $card_no,
                                'CardCVV'         => $cvv,
                                'CardNo'          => $card_no,

                                'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                                'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                'customerListID'  => $CustomerListID,
                                'merchantID'      => $this->merchantID,
                                'createdAt'       => date("Y-m-d H:i:s"),
                            ];
                        } else {
                            $accountDetails = [
                                'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                'customerListID'     => $CustomerListID,
                                'merchantID'         => $this->merchantID,
                                'createdAt'          => date("Y-m-d H:i:s"),
                                'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                            ];
                        }
                    }

                    $saleData = [
                        'paymentDetails'    => $accountDetails,
                        'transactionByUser' => $this->transactionByUser,
                        'ach_type'          => $sch_method,
                        'invoiceID'         => $in_data['invoiceID'],
                        'crtxnID'           => '',
                        'companyName'       => $customerData['companyName'],
                        'fullName'          => $customerData['fullName'],
                        'firstName'         => $customerData['firstName'],
                        'lastName'          => $customerData['lastName'],
                        'contact'           => $customerData['phoneNumber'],
                        'email'             => $customerData['userEmail'],
                        'amount'            => $amount,
                        'gatewayID'         => $gateway,
                        'returnResult'      => true,
                        'customerListID'    => $customerData['Customer_ListID'],
                    ];

                    $isACH = ($sch_method == 1) ? false : true;

                    $saleData   = array_merge($saleData, $in_data);
                    $paidResult = $paymentObj->_processSaleTransaction($saleData);

                    $crtxnID = '';
                    if (isset($paidResult['response']) && $paidResult['response']) {
                        $tr_date           = date('Y-m-d H:i:s');
                        $updateInvoiceData = [
                            'invoiceID'       => $in_data['invoiceID'],
                            'accountCode'     => $accountData['accountCode'],
                            'trnasactionDate' => $tr_date,
                            'amount'          => $amount,
                        ];

                        $freshbookSync  = new Freshbooks_data($this->merchantID, $this->resellerID);
                        $invoicePayment = $freshbookSync->create_invoice_payment($in_data['invoiceID'], $amount);
                        if ($invoicePayment['status'] != 'ok') {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
                        } else {
                            $crtxnID = $invoicePayment['payment_id'];
                        }
                        if ($cardID == "new1") {
                            if ($sch_method == "1") {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $cardType = $this->general_model->getType($card_no);

                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                $card_data = array(
                                    'cardMonth'       => $expmonth,
                                    'cardYear'        => $exyear,
                                    'CardType'        => $cardType,
                                    'CustomerCard'    => $card_no,
                                    'CardCVV'         => $cvv,
                                    'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'  => $customerID,

                                    'companyID'       => $companyID,
                                    'merchantID'      => $this->merchantID,
                                    'createdAt'       => date("Y-m-d H:i:s"),
                                );

                                $id1 = $this->card_model->process_card($card_data);
                            } else if ($sch_method == "2") {
                                $id1 = $this->card_model->process_ack_account($accountDetails);
                            }
                        }

                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                        $ref_number     = $in_data['refNumber'];
                        $toEmail        = $customerData['userEmail'];
                        $company        = $customerData['fullName'];
                        $customer       = $customerData['fullName'];
                        if ($chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                        }
                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                    }

                    $responseId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $in_data['invoiceID'], $isACH, $this->transactionByUser);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                }
            } else {
                $errorMessage = "Transaction Failed -  This is not valid invoice";
                $this->session->set_flashdata('message', "<div class='alert alert-danger'><strong>$errorMessage</strong>.</div>");
            }
        } else {
            $error = 'Validation Error. Please fill the requred fields';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
        }

        $transactionID = '';
        if (isset($responseId) && !empty($responseId)) {
            $transactionData = $this->general_model->get_row_data('customer_transaction', ['id' => $responseId]);
            $transactionID   = $transactionData['transactionID'];

            $tcode =array('100','200', '120','111','1');
            if(in_array($transactionData['transactionCode'], $tcode)){
                $this->session->set_flashdata('success', 'Transaction Successfull');
                $this->session->set_flashdata('message', '');
            } else {
                $errorMsg = $transactionData['transactionStatus'];
                if($errorMsg == ''){
                    $errorMsg = "Transaction Failed - Declined";
                }

                $this->session->set_flashdata('success', '');
                $this->session->set_flashdata('message', $errorMsg);
            }
        }

        if ($cusproID == "3" && $in_data['invoiceID'] != '') {
            redirect('FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/' . $in_data['invoiceID'], 'refresh');
        }

        $invoice_IDs = array();

        $receipt_data = array(
            'proccess_url'      => 'FreshBooks_controllers/Freshbooks_invoice/Invoice_details',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan,
            'invoice_page'      => true,
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $transactionID, 'refresh');
        }
        redirect('FreshBooks_controllers/home/transation_receipt/' . $in_data['txnID'] . '/' . $invoiceID . '/' . $transactionID, 'refresh');
    }

    public function pay_multi_invoice()
    {

        $merchantID = $this->merchantID;
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');

        $checkPlan       = check_free_plan_transactions();
        $Customer_ListID = '';
        $Customer_ListID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $error           = '';
        $resellerID      = $this->resellerID;

        $cardSaved = false;

        if ($checkPlan && $cardID != "" && $gateway != "") {
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
            if (!empty($invoices)) {
                $customerData = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'firstName', 'lastName', 'companyName'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchantID));

                $paymentObj = new Manage_payments($this->merchantID);

                if ($cardID != 'new1') {
                    $accountDetails = $this->card_model->get_single_card_data($cardID);
                } else {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                    $cardType = $this->general_model->getType($card_no);

                    $accountDetails = [
                        'cardMonth'       => $expmonth,
                        'cardYear'        => $exyear,
                        'CardType'        => $cardType,
                        'CustomerCard'    => $card_no,
                        'CardCVV'         => $cvv,
                        'CardNo'          => $card_no,

                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                        'customerListID'  => $Customer_ListID,
                        'merchantID'      => $this->merchantID,
                        'createdAt'       => date("Y-m-d H:i:s"),
                    ];
                }

                $freshbookSync = new Freshbooks_data($this->merchantID, $this->resellerID);
                foreach ($invoices as $key => $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->fb_company_model->get_invoice_data_pay($invoiceID, $merchantID);
                    if (!empty($in_data)) {
                        $cr_amount = 0;
                        $amount    = $pay_amounts;
                        $amount    = $amount - $cr_amount;

                        $saleData = [
                            'paymentDetails'    => $accountDetails,
                            'transactionByUser' => $this->transactionByUser,
                            'ach_type'          => 1,
                            'invoiceID'         => $in_data['invoiceID'],
                            'crtxnID'           => '',
                            'companyName'       => $customerData['companyName'],
                            'fullName'          => $customerData['fullName'],
                            'firstName'         => $customerData['firstName'],
                            'lastName'          => $customerData['lastName'],
                            'contact'           => $customerData['phoneNumber'],
                            'email'             => $customerData['userEmail'],
                            'amount'            => $amount,
                            'gatewayID'         => $gateway,
                            'returnResult'      => true,
                            'customerListID'    => $customerData['Customer_ListID'],
                        ];

                        $saleData   = array_merge($saleData, $in_data);
                        $paidResult = $paymentObj->_processSaleTransaction($saleData);

                        $crtxnID = '';
                        if (isset($paidResult['response']) && $paidResult['response']) {
                            $invoicePayment = $freshbookSync->create_invoice_payment($in_data['invoiceID'], $amount);
                            if ($invoicePayment['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
                            } else {
                                $crtxnID = $invoicePayment['payment_id'];
                            }

                            if ($cardSaved && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                                $id1 = $this->card_model->process_card($accountDetails);
                            }
                            $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                        }
                        $id = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $in_data['invoiceID'], false, $this->transactionByUser);
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if (!$checkPlan) {
            $responseId = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "' . base_url() . 'FreshBooks_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($Customer_ListID != "") {
            redirect('FreshBooks_controllers/Freshbooks_Customer/view_customer/' . $Customer_ListID, 'refresh');
        } else {
            redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
        }

    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $user_id = $this->merchantID;

        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
            $this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
            if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {
                $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
            }

            $amount            = $this->czsecurity->xssCleanPostInput('totalamount');
            $invoiceIDs        = array();
            $invoicePayAmounts = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }
            $cardID = $this->czsecurity->xssCleanPostInput('card_list');

            $freshbookSync = new Freshbooks_data($this->merchantID, $this->resellerID);
            $customerID    = $this->czsecurity->xssCleanPostInput('customerID');

            $transactionid = '';
            $payIndex      = 0;
            $checkPlan     = check_free_plan_transactions();
            if ($checkPlan && $this->form_validation->run() == true) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
                $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                if (!empty($gt_result)) {
                    $con_cust     = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
                    $customerData = $this->general_model->get_select_data('Xero_custom_customer', array('*'), $con_cust);

                    $paymentObj = new Manage_payments($this->merchantID);
                    if ($cardID != 'new1') {
                        $accountDetails = $this->card_model->get_single_card_data($cardID);
                    } else {
                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                        $cardType = $this->general_model->getType($card_no);
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                        $accountDetails = [
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $cardType,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'CardNo'          => $card_no,

                            'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                            'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                            'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                            'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                            'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                            'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                            'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                            'customerListID'  => $customerID,
                            'merchantID'      => $this->merchantID,
                            'createdAt'       => date("Y-m-d H:i:s"),
                        ];
                    }

                    $amount = $this->czsecurity->xssCleanPostInput('totalamount');

                    if (!empty($this->input->post('invoice_id'))) {
                        $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                    }

                    if (!empty($this->input->post('po_number'))) {
                        $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                    }

                    $saleData = [
                        'paymentDetails'    => $accountDetails,
                        'transactionByUser' => $this->transactionByUser,
                        'ach_type'          => 1,
                        'invoiceID'         => $this->czsecurity->xssCleanPostInput('invoice_id'),
                        'po_number'         => $this->czsecurity->xssCleanPostInput('po_number'),
                        'crtxnID'           => '',
                        'companyName'       => $customerData['companyName'],
                        'fullName'          => $customerData['fullName'],
                        'firstName'         => $customerData['firstName'],
                        'lastName'          => $customerData['lastName'],
                        'contact'           => $customerData['phoneNumber'],
                        'email'             => $customerData['userEmail'],
                        'amount'            => $amount,
                        'gatewayID'         => $gatlistval,
                        'returnResult'      => true,
                        'customerListID'    => $customerData['Customer_ListID'],
                    ];

                    // $saleData   = array_merge($saleData, $in_data);
                    $paidResult = $paymentObj->_processSaleTransaction($saleData);

                    $crtxnID = '';
                    if (isset($paidResult['response']) && $paidResult['response']) {
                        $invoiceIDs        = array();
                        $invoicePayAmounts = array();
                        if (!empty($this->input->post('invoice_id'))) {
                            $invoiceIDs        = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                            $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                        }

                        if (!empty($invoiceIDs)) {
                            $payIndex            = 0;
                            $saleAmountRemaining = $amount;
                            foreach ($invoiceIDs as $inID) {
                                $con  = array('invoiceID' => $inID, 'merchantID' => $user_id);
                                $res1 = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment', 'AppliedAmount'), $con);

                                $refNumber[] = $res1['refNumber'];

                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];

                                $invoicePayment = $freshbookSync->create_invoice_payment($inID, $actualInvoicePayAmount);
                                if ($invoicePayment['status'] != 'ok') {
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
                                } else {
                                    $crtxnID = $invoicePayment['payment_id'];
                                }

                                $custom_data_fields['invoice_number'] = '';

                                $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                $payIndex++;
                            }

                        } else {
                            $crtxnID             = '';
                            $inID                = '';
                            $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                        }

                        if ($chh_mail == '1') {
                            $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                            if (!empty($refNumber)) {
                                $ref_number = implode(',', $refNumber);
                            } else {
                                $ref_number = '';
                            }

                            $tr_date  = date('Y-m-d H:i:s');
                            $toEmail  = $customerData['userEmail'];
                            $company  = $customerData['fullName'];
                            $customer = $customerData['fullName'];
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                        }

                        if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                            $id1 = $this->card_model->process_card($accountDetails);
                        }

                        $this->session->set_flashdata('success', 'Transaction Successful');
                    } else {
                        $crtxnID             = '';
                        $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }

                    if (isset($storedTransactionId) && $storedTransactionId) {
                        $condition2         = array('id' => $storedTransactionId);
                        $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                        if ($storedTransactData) {
                            $transactionid = $storedTransactData['transactionID'];

                            $tcode =array('100','200', '120','111','1');
                            if(in_array($storedTransactData['transactionCode'], $tcode)){
                                $this->session->set_flashdata('success', 'Transaction Successfull');
                                $this->session->set_flashdata('message', '');
                            } else {
                                $errorMsg = $storedTransactData['transactionStatus'];
                                if($errorMsg == ''){
                                    $errorMsg = "Transaction Failed - Declined";
                                }
                
                                $this->session->set_flashdata('success', '');
                                $this->session->set_flashdata('message', $errorMsg);
                            }
                        }
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
                }
            } else {
                $error = 'Validation Error. Please fill the requred fields';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
            }

            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $transactionid,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_sale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan,
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
        }
        redirect('FreshBooks_controllers/Transactions/create_customer_sale', 'refresh');
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            $merchantID = $this->merchantID;

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan  = check_free_plan_transactions();
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $cardID     = $this->czsecurity->xssCleanPostInput('card_list');

            $transactionid = '';
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $comp_data = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

                $paymentObj = new Manage_payments($this->merchantID);
                if ($cardID != 'new1') {
                    $accountDetails                 = $this->card_model->get_single_card_data($cardID);
                    $accountDetails['CustomerCard'] = $accountDetails['CardNo'];
                } else {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $cardType = $this->general_model->getType($card_no);
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                    $accountDetails = [
                        'cardMonth'       => $expmonth,
                        'cardYear'        => $exyear,
                        'CardType'        => $cardType,
                        'CustomerCard'    => $card_no,
                        'CardCVV'         => $cvv,
                        'CardNo'          => $card_no,

                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                        'customerListID'  => $customerID,
                        'merchantID'      => $this->merchantID,
                        'createdAt'       => date("Y-m-d H:i:s"),
                    ];
                }

                $amount = $this->czsecurity->xssCleanPostInput('totalamount');

                $authData                = $accountDetails;
                $authData['name']        = $comp_data['fullName'];
                $authData['amount']      = $amount;
                $authData['authType']    = 2; /* 1 for used auto authrize amount 2 for given manually amount */
                $authData['gatewayID']   = $gatlistval;
                $authData['storeResult'] = true;

                $paidResult = $paymentObj->authoriseTransaction($authData);
                if (isset($paidResult['response']) && $paidResult['response']) {
                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $id1 = $this->card_model->process_card($accountDetails);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                }

                if (isset($paidResult['id']) && $paidResult['id']) {
                    $condition2         = array('id' => $paidResult['id']);
                    $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                    if ($storedTransactData) {
                        $transactionid = $storedTransactData['transactionID'];
                        $tcode =array('100','200', '120','111','1');
                        if(in_array($storedTransactData['transactionCode'], $tcode)){
                            $this->session->set_flashdata('success', 'Transaction Successfull');
                            $this->session->set_flashdata('message', '');
                        } else {
                            $errorMsg = $storedTransactData['transactionStatus'];
                            if($errorMsg == ''){
                                $errorMsg = "Transaction Failed - Declined";
                            }

                            $this->session->set_flashdata('success', '');
                            $this->session->set_flashdata('message', $errorMsg);
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $transactionid,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         => $checkPlan,
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
        }
        redirect('FreshBooks_controllers/Transactions/create_customer_auth', 'refresh');
    }

    public function payment_capture()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['transactions'] = $this->customer_model->get_transaction_data_captue($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/payment_capture', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /*****************Capture Transaction***************/
    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $transactionID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);
            if ($paydata['gatewayID'] > 0) {

                $gatlistval = $paydata['gatewayID'];

                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                $customerID = $paydata['customerListID'];
                $amount     = $paydata['transactionAmount'];
                $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
                $comp_data  = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

                $captureData = [
                    'transactionID'     => $transactionID,
                    'amount'            => $amount,
                    'gatewayID'         => $paydata['gatewayID'],
                    'customerListID'    => $customerID,
                    'storeResult'       => true,
                    'returnResult'      => true,
                    'transactionByUser' => $this->transactionByUser,
                ];
                $paymentObj = new Manage_payments($this->merchantID);
                $paidResult = $paymentObj->_captureTransaction($captureData);

                if (isset($paidResult['response']) && $paidResult['response']) {
                    $this->session->set_flashdata('success', 'Successfully Captured Authorization');

                    if ($chh_mail == '1') {
                        $customerID = $paydata['customerListID'];
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['userEmail'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['fullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
                    }
                }

                if (isset($paidResult['id']) && $paidResult['id']) {
                    $condition2         = array('id' => $paidResult['id']);
                    $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                    if ($storedTransactData) {
                        $transactionID = $storedTransactData['transactionID'];
                        $tcode =array('100','200', '120','111','1');
                        if(in_array($storedTransactData['transactionCode'], $tcode)){
                            $this->session->set_flashdata('success', 'Transaction Successfull');
                            $this->session->set_flashdata('message', '');
                        } else {
                            $errorMsg = $storedTransactData['transactionStatus'];
                            if($errorMsg == ''){
                                $errorMsg = "Transaction Failed - Declined";
                            }

                            $this->session->set_flashdata('success', '');
                            $this->session->set_flashdata('message', $errorMsg);
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Invalid Request Details</strong></div>');
            }
            $invoice_IDs  = array();
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Transactions/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if ($transactionID == '') {
                $transactionID = 'null';
            }
            redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionID, 'refresh');
        }
        redirect('FreshBooks_controllers/Transactions/payment_capture', 'refresh');
    }

    /*****************Esale Transaction***************/
    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        if (!empty($this->input->post())) {
            $merchantID = $this->merchantID;

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->input->post('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                $invoiceIDs                           = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount        = $this->czsecurity->xssCleanPostInput('totalamount');
            $name          = $this->czsecurity->xssCleanPostInput('firstName') . " " . $this->czsecurity->xssCleanPostInput('lastName');
            $responseId    = '';
            $checkPlan     = check_free_plan_transactions();
            $freshbookSync = new Freshbooks_data($this->merchantID, $this->resellerID);

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $customerData = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }

                $saleData = [
                    'paymentDetails'    => $accountDetails,
                    'transactionByUser' => $this->transactionByUser,
                    'ach_type'          => 2,
                    'invoiceID'         => $this->czsecurity->xssCleanPostInput('invoice_id'),
                    'po_number'         => $this->czsecurity->xssCleanPostInput('po_number'),
                    'crtxnID'           => '',
                    'companyName'       => $customerData['companyName'],
                    'fullName'          => $customerData['fullName'],
                    'firstName'         => $customerData['firstName'],
                    'lastName'          => $customerData['lastName'],
                    'contact'           => $customerData['phoneNumber'],
                    'email'             => $customerData['userEmail'],
                    'amount'            => $amount,
                    'gatewayID'         => $gatlistval,
                    'returnResult'      => true,
                    'customerListID'    => $customerData['Customer_ListID'],
                ];

                $paymentObj = new Manage_payments($this->merchantID);
                $paidResult = $paymentObj->_processSaleTransaction($saleData);

                $crtxnID = '';
                if (isset($paidResult['response']) && $paidResult['response']) {
                    $invoiceIDs        = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs        = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    if (!empty($invoiceIDs)) {
                        $payIndex            = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $con  = array('invoiceID' => $inID, 'merchantID' => $user_id);
                            $res1 = $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment', 'AppliedAmount'), $con);

                            $refNumber[] = $res1['refNumber'];

                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];

                            $invoicePayment = $freshbookSync->create_invoice_payment($inID, $actualInvoicePayAmount);
                            if ($invoicePayment['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
                            } else {
                                $crtxnID = $invoicePayment['payment_id'];
                            }

                            $custom_data_fields['invoice_number'] = '';

                            $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $crtxnID, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                            $payIndex++;
                        }

                    } else {
                        $crtxnID             = '';
                        $inID                = '';
                        $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($chh_mail == '1') {
                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                        if (!empty($refNumber)) {
                            $ref_number = implode(',', $refNumber);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $customerData['userEmail'];
                        $company  = $customerData['companyName'];
                        $customer = $customerData['fullName'];
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                    }

                    if ($payableAccount == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $crtxnID             = '';
                    $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                }

                if (isset($storedTransactionId) && $storedTransactionId) {
                    $condition2         = array('id' => $storedTransactionId);
                    $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                    if ($storedTransactData) {
                        $responseId = $storedTransactData['transactionID'];
                        $tcode =array('100','200', '120','111','1');
                        if(in_array($storedTransactData['transactionCode'], $tcode)){
                            $this->session->set_flashdata('success', 'Transaction Successfull');
                            $this->session->set_flashdata('message', '');
                        } else {
                            $errorMsg = $storedTransactData['transactionStatus'];
                            if($errorMsg == ''){
                                $errorMsg = "Transaction Failed - Declined";
                            }

                            $this->session->set_flashdata('success', '');
                            $this->session->set_flashdata('message', $errorMsg);
                        }
                    }
                }

            } else {
                $errorMessage = " Please select gateway.";
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - ' . $errorMessage . '</strong></div>');
            }

            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'FreshBooks_controllers/Transactions/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan,
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('FreshBooks_controllers/home/transation_sale_receipt', 'refresh');
        }
        redirect('FreshBooks_controllers/Transactions/create_customer_esale', 'refresh');
    }

    public function void_transaction()
    {
        $merchID = $this->merchantID;

        $setMailVoid          = ($this->czsecurity->xssCleanPostInput('setMailVoid')) ? 1 : 0;
        $payment_capture_page = ($this->czsecurity->xssCleanPostInput('payment_capture_page')) ? $this->czsecurity->xssCleanPostInput('payment_capture_page') : 0;

        if (!empty($this->czsecurity->xssCleanPostInput('paytxnID')) || !empty($this->czsecurity->xssCleanPostInput('txnvoidID'))) {
            $ptxnID = $this->czsecurity->xssCleanPostInput('paytxnID');
            if (isset($_POST['txnvoidID'])) {
                $paydata = $this->general_model->get_row_data('customer_transaction', array('transactionID' => $_POST['txnvoidID'], 'merchantID' => $merchID));
            } else {
                $paydata = $this->general_model->get_row_data('customer_transaction', array('id' => $ptxnID, 'merchantID' => $merchID));
            }
            $ptxnID         = $paydata['id'];
            $transaction_id = $paydata['transactionID'];

            // echo '<pre>';
            // print_r($paydata);die;
            if (!empty($paydata)) {

                $transactionGateway = $paydata['transactionGateway'];
                $gatlistval         = $paydata['gatewayID'];
                $gt_result          = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
                $paymentType        = (strrpos($paydata['gateway'], 'ECheck')) ? 2 : 1;

                $voidObj           = new Manage_payments($merchID);
                $voidedTransaction = $voidObj->voidTransaction([
                    'trID'        => $paydata['transactionID'],
                    'gatewayID'   => $gatlistval,
                    'paymentType' => $paymentType,
                ]);
                // $voidedTransaction = true;

                if ($voidedTransaction) {
                    if ((!empty($paydata['qbListTxnID']))) {
                        $qb_txn_id      = $paydata['qbListTxnID'];
                        $freshbookSync  = new Freshbooks_data($this->merchantID, $this->resellerID);
                        $invoicePayment = $freshbookSync->void_invoice_payment($paydata['invoiceID'], $paydata['transactionAmount'], $qb_txn_id);
                        if ($invoicePayment['status'] != 'ok') {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
                        } else {
                            $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                        }

                        $st = $this->general_model->update_row_data('customer_transaction', array('transactionID' => $paydata['transactionID'], 'merchantID' => $merchID), array('transaction_user_status' => '3', 'transactionModified' => date('Y-m-d H:i:s')));
                    } else {

                        if ($paydata['invoiceID'] == '' || $paydata['invoiceID'] == null) {

                            $st = $this->general_model->update_row_data('customer_transaction', array('transactionID' => $paydata['transactionID'], 'merchantID' => $merchID), array('transaction_user_status' => '3', 'transactionModified' => date('Y-m-d H:i:s')));
                            if ($st) {
                                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                            } else {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed</strong></div>');
                            }
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>');
            }
            $receipt_data = array(
                'proccess_url'      => 'FreshBooks_controllers/Transactions/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Void',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);

            redirect('FreshBooks_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transaction_id, 'refresh');
        } else {
            redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details', 'refresh');
        }
    }
}
