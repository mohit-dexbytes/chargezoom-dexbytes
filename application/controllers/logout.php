<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Logout extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
		$this->load->model('login_model'); 
	}


	public function index()

	{  
		$data['title'] = 'Logout';

		$status = $this->login_model->user_logout();

		if(isset($_GET['lr'])){
			redirect(FIRST_LOGIN_SUPPORT_URL, 'refresh'); 
		}
		redirect('/', 'refresh'); 
	}
	
		public function index2()
	{ 
		$data['title'] = 'Logout';
		$status = $this->login_model->merchant_user_logout();
		 redirect('/', 'refresh'); 
		
	}
	

}



