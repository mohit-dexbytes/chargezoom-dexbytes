<?php
/**
 * Page 404
 */
class Page_404 extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Error 404 Page
	 *
	 * @return void
	 */
	public function index()
	{	 
	    $this->output->set_status_header('404'); 
	    $this->load->view('pages/page_ready_404');
	}

	/**
	 * Paid Message Error Page
	 *
	 * @return void
	 */
    public function msg_paid()
	{	 
	    $this->output->set_status_header('404'); 
	    $data['template'] 		= template_variable();
	    $this->load->view('template/template_start', $data);
	    $this->load->view('pages/paidmessage');
	    $this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
}

?>