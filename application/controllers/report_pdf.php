<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_pdf extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('customer_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
			 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='2' )
          {
           
          }else if($this->session->userdata('user_logged_in')!="")
          {
           
          }else{
            redirect('login','refresh');
          }
	}
	

	public function report_details_pdf()
	{
       error_reporting(1); 
   
	    $data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
		
			$da    = $this->session->userdata('logged_in');
			$user_id			    = $da['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$da   = $this->session->userdata('user_logged_in');
		$user_id			    = $da['merchantID'];
		}
	    $type = $this->uri->segment(3);  
		 
	 
			$today 			    = date('Y-m-d');
			if($type=='1' ){
			
			$condition 			 	= array("comp.merchantID"=>$user_id);
            $this->db->select("`inv`.Customer_ListID,`inv`.Customer_FullName, cust.FirstName, cust.LastName, `cust`.Contact,`cust`.FullName, (case when cust.ListID !='' then 'Active' else 'InActive' end) as status , `inv`.ShipAddress_Addr1, `cust`.ListID, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->group_by('inv.Customer_ListID');
		
		$this->db->order_by('balance','desc');
	   
	    $this->db->where($condition);
		$this->db->where('cust.customerStatus','1');
		$query = $this->db->get();
      
		$invoices = $query->result_array();
            
	
			$data['report1']		= 	$invoices;
		
		}		
			
		else if($type=='2'){
			$condition1 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,"IsPaid"=>"false",  "comp.merchantID"=>$user_id);   
		
			$invoices    			 = $this->company_model->get_invoice_data_by_due($condition1);  		
			$data['report2']	   	 = 	$invoices;
			 
		}		
		else if($type=='3'){
		
		$invoices    			 = $this->company_model->get_invoice_data_by_past_due($user_id); 
		$data['report3']		 = 	$invoices;
	 
	}
	
	
		
		else if($type=='4'){
		$invoices    			 = $this->company_model->get_invoice_data_by_past_time_due($user_id); 
		 $data['report4']		 = 	$invoices;
	
	}
	
	else if($type=='5'){
		
		$invoices    			 = $this->company_model->get_transaction_failure_report_data($user_id); 
		$data['report5']		 = 	$invoices;

	}
	
	else if($type=='6'){
		   $this->load->library('encrypt');
		       $report=array();
		   $card_data =  $this->card_model->get_credit_card_info($user_id);
		   foreach($card_data as $key=> $card){
		       
		       
            		   
		         $condition      =  array('ListID'=>$card['customerListID']);
			     $customer_data  = $this->general_model->get_row_data('qb_test_customer',$condition);
						     $customer_card['ListID']  = $customer_data['ListID'] ;
                 $customer_card['CardNo']  = substr($this->card_model->decrypt($card['CustomerCard']),12) ;
				 $customer_card['expired_date'] = $card['expired_date'] ;
                 $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['Contact']  = $customer_data['Contact'] ;
				 $customer_card['FullName']  = $customer_data['FullName'] ;
				 $customer_card['companyName']  = $customer_data['companyName'] ;
			     $report[$key] = $customer_card;
            	 
	
            	
		   }
		   
		 
			$data['report6']		 = 	$report;
		    
	}
	
	else if($type=='8' ||  $type=='9' || $type=='10'  ){
	
    	
		
	   if($type=='10'){
	   $invoices   				 = 	 $this->company_model->get_invoice_upcomming_data($user_id);
	   $data['report10']		 = 	$invoices;
	   }else{
		$invoices    			 = $this->company_model->get_invoice_data_open_due($user_id, $type); 
		  $data['report89']		 = 	$invoices;
		}
		
	}
   else	if($type=='7'){
	     $startdate = $this->uri->segment(4);  
	     $enddate   = $this->uri->segment(5); 
	
	  $invoices   				 = 	 $this->company_model->get_transaction_report_data($user_id,$startdate,  $enddate);
	  $data['report7']				 = 	$invoices;
	
	}else{
		$condition 			 	= array("companyID"=>$user_id);
		$invoices    			= $this->company_model->get_invoice_data_by_due($condition);   	
		
		$data['report1']		= 	$invoices;
	
	
	}
	    
	     	$no = 'Report'.$type;
			$pdfFilePath = "$no.pdf"; 
			
			 ini_set('memory_limit','256M'); 
			ob_start();
     	$html= $this->load->view('pages/page_report_pdf', $data,true);
  
 		  $this->load->library('pdf');
			 $pdf = $this->pdf->load();
             ob_end_clean();
			 $pdf->WriteHTML($html); // write the HTML into the PDF
           ob_end_flush();
			 $pdf->Output($pdfFilePath, 'D'); // save to file because we can
 
	
   
	}
	
	
	
	public function get_credit_card_info($compID)
	{
		$card_data=array();
	  
		 $db_user = "devcz_quicktest";
		$db_pass = "ecrubit@123";
		$db_name = "devcz_chargezoom_card_db"; 
	   
			$dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name; 
	  $this->db1 = $this->load->database($dsn, true);        
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
		 $query1   = $this->db1->query($sql);
		$card_data =   $query1->result_array();
		return $card_data;
	}
	
	
	
	public function get_credit_card_info_data($compID){
		 $db_user = "devcz_quicktest";
		$db_pass = "ecrubit@123";
		$db_name = "devcz_chargezoom_card_db"; 
	   
	
			$dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name; 
	  $this->db1 = $this->load->database($dsn, true);        
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
    	 $query1   = $this->db1->query($sql);
	    return $query1->result_array();
	}
		
	public function billing_report(){
	    $data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}
	    $invoiceID = $this->uri->segment(3);  
		
		$condition 			 	= array("invoice"=>$invoiceID);
		$invoices    			= $this->company_model->get_billing_invoice_data($invoiceID);   	
		
		$data['invoice1']		= 	$invoices; 
	
		$today 			    = date('Y-m-d');
		
		$no = 'Billing'.$invoiceID;
		$pdfFilePath = "$no.pdf"; 
		
		 ini_set('memory_limit','32M'); 
		
		$html= $this->load->view('pages/page_billing_pdf', $data,true);
	
    
    
    
    
	}	
	
}



