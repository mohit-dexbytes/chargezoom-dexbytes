<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_controller extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('general_model');
	}

	function get_states(){
		$country_id = $this->czsecurity->xssCleanPostInput('id');
		$data = array("country_id" => $country_id);
		$state = $this->general_model->get_table_data('state',$data);
		if($state != ""){
			echo json_encode($state);
		}
	}

	function get_city(){
		$state_id = $this->czsecurity->xssCleanPostInput('id');
		$data = array("state_id" => $state_id);
		$city = $this->general_model->get_table_data('city',$data);
		if($city != ""){
			echo json_encode($city);
		}
	}

	function get_process_trans(){
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
			$today = date('Y-m-d');
			$today_trans =$this->general_model->get_process_trans_data($user_id);
			if($today_trans != false){
				echo json_encode($today_trans);
			}
			else{
				echo 2;
			}
	}

	function get_scheduled_incoice(){

			$today = date('Y-m-d');
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
			$condition2 			= array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= "=>$today,"IsPaid"=>"false", 'userStatus'=>'', "merchantID"=>$user_id);

			$sch_data =$this->general_model->get_modal_invoice_data($condition2);
			if($sch_data != false){
				echo json_encode($sch_data);
			}else{
				echo 2;
			}

	}
	function get_failed_incoice(){

			$today = date('Y-m-d');
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
			$condition3 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,"IsPaid"=>"false", 'userStatus'=>'', "cmp.merchantID"=>$user_id);
			$sch_data =$this->general_model->get_modal_invoice_data($condition3);
			if($sch_data != false){
				echo json_encode($sch_data);
			}else{
				echo 2;
			}

	}
	
	function set_offline_amount(){
	    $tnID = $this->czsecurity->xssCleanPostInput('tid');
	    	$data = array("TxnID " => $tnID);
	    $bal_amnt = $this->general_model->get_invoice_bal_data('qb_test_invoice',$data);
	    if($bal_amnt != ""){
			echo json_encode($bal_amnt);
		}
	}


}