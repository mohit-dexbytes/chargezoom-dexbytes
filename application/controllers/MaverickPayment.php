<?php
/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
require_once dirname(__FILE__)."/QbdPaymentGateway.php";

class MaverickPayment extends QbdPaymentGateway
{
    public function __construct()
    {
        parent::__construct();

        $this->load->config('maverick');
        $this->load->library('MaverickGateway');

        $this->gatewayEnvironment = $this->config->item('maverick_payment_env');
    }

    
    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $da      = $this->session->userdata('logged_in');
            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da      = $this->session->userdata('user_logged_in');
            $user_id = $da['merchantID'];
        }

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
       
        $cusproID = '';
        $error    = '';
        $cusproID = $this->czsecurity->xssCleanPostInput('customerProcessID');
		$checkPlan = check_free_plan_transactions();

        if ($this->form_validation->run() == true) {
            $custom_data_fields = [];
            $cusproID  = '';
            $error     = '';
            $cusproID  = $this->czsecurity->xssCleanPostInput('customerProcessID');
            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
            $cardID    = $this->czsecurity->xssCleanPostInput('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
            }

            $gateway = $gatlistval;

            $amount     = $this->czsecurity->xssCleanPostInput('inv_amount');
            $in_data    = $this->quickbooks->get_invoice_data_pay($invoiceID, $user_id);
            $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($checkPlan && !empty($in_data)) {

                $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $customerID = $in_data['Customer_ListID'];
                $c_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName','FirstName', 'LastName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                $companyID  = $c_data['companyID'];

                $Customer_ListID = $in_data['Customer_ListID'];

                $cardID_upd = '';

                // Maverick Payment Gateway
                $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                if (!empty($cardID)) {

                    $cr_amount = 0;
                    $amount    = $this->czsecurity->xssCleanPostInput('inv_amount');

                    $name = $in_data['Customer_FullName'];

                    if ($sch_method == "1") {

                        if ($cardID != "new1") {

                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
                            $cvv       = $card_data['CardCVV'];
                            $cardType  = $card_data['CardType'];
                            $address1  = $card_data['Billing_Addr1'];
                            $address2  = $card_data['Billing_Addr2'];
                            $city      = $card_data['Billing_City'];
                            $zipcode   = $card_data['Billing_Zipcode'];
                            $state     = $card_data['Billing_State'];
                            $country   = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        } else {

                            $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cardType = $this->general_model->getType($card_no);
                            $cvv      = '';
                            if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                            }

                            $address1 = $this->czsecurity->xssCleanPostInput('address1');
                            $address2 = $this->czsecurity->xssCleanPostInput('address2');
                            $city     = $this->czsecurity->xssCleanPostInput('city');
                            $country  = $this->czsecurity->xssCleanPostInput('country');
                            $phone    = $this->czsecurity->xssCleanPostInput('contact');
                            $state    = $this->czsecurity->xssCleanPostInput('state');
                            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        $exyear1  = substr($exyear, 2);
                        if (strlen($expmonth) == 1) {
                            $expmonth = '0' . $expmonth;
                        }
                        $expry = $expmonth . '/' . $exyear1;
                        
                        // Sale Payload
                        $request_payload = [
                            'level' => 1,
                            'threeds' => [
                                'id' => null,
                            ],
                            'amount' => $amount,
                            'card' => [
                                'number' => $card_no,
                                'cvv'    => $cvv,
                                'exp'    => $expry,
                                'save'   => 'No',
                                'address' => [
                                    'street' => $address1,
                                    'city' => $city,
                                    'state' => $state,
                                    'country' => $country,
                                    'zip' => $zipcode,
                                ]
                            ],
                            'contact' => [
                                'name'   => $c_data['FirstName'].' '.$c_data['LastName'],
                                'email'  => $c_data['Contact'],
                                'phone' => $phone,
                            ]
                        ];

                        // Process Sale
                        $r = $this->maverickgateway->processSale($request_payload);
                        
                    } else if ($sch_method == "2") {
                        if ($cardID == 'new1') {
                            $accountDetails = [
                                'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                'customerListID'     => $customerID,
                                'companyID'          => $companyID,
                                'merchantID'         => $user_id,
                                'createdAt'          => date("Y-m-d H:i:s"),
                                'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                            ];
                        } else {
                            $accountDetails = $this->card_model->get_single_card_data($cardID);
                        }
                        $accountNumber = $accountDetails['accountNumber'];
                        $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;
                        
                        // Maverick ACH

                        // get DBA id
                        $dbaId = $this->maverickgateway->getDba(true);

                        // electronic Sale
                        $request_payload = [
                                'amount'          => $amount,
                                'routingNumber'   => $accountDetails['routeNumber'],
                                'accountName'     => $accountDetails['accountName'],
                                'accountNumber'   => $accountDetails['accountNumber'],
                                'accountType'     => ucwords($accountDetails['accountType']), // Checking or Savings
                                'transactionType' => 'Debit', // Debit or Credit
                                'customer' => [
                                    'email'          => $c_data['Contact'],
                                    'firstName'      => $c_data['FirstName'],
                                    'lastName'       => $c_data['LastName'],
                                    "address1"       => $accountDetails['Billing_Addr1'],
                                    "address_2"      => $accountDetails['Billing_Addr2'],
                                    "city"           => $accountDetails['Billing_City'],
                                    "state"          => $accountDetails['Billing_State'],
                                    "zipCode"        => $accountDetails['Billing_Zipcode'],
                                    "country"        => $accountDetails['Billing_Country'],
                                    "phone"          => $accountDetails['Billing_Contact'],
                                ],
                                'dba' => [
                                    'id' => $dbaId,
                                ],
                            ];
                        // Process ACH
                        $r = $this->maverickgateway->processAch($request_payload);
                    }
                    
                    $responseId = '';

                    $result = [];

                    $rbody = json_decode($r['response_body'], true);

                    $result['response_code'] = $r['response_code'];

                    $result['data'] = $rbody;
                    
                    // Response                
                    if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                        if(isset($rbody['id']) && $rbody['id']){
                            $result['status'] = 'success';
                            $result['msg'] = $result['message'] = 'Payment success.';
                        } else {
                            $result['status'] = 'failed';
                            $result['msg'] = $result['message'] = 'Payment failed.';    
                        }

                    } else {
                        $result['status'] = 'failed';
                        $result['msg'] = $result['message'] = $rbody['message'];
                    }

                    if ($result['status'] == 'success') {
                        $responseId = $result['data']['id'];
                        $txnID                      = $in_data['TxnID'];
                        $ispaid                     = 'true';

                        $bamount = $in_data['BalanceRemaining'] - $amount;
                        if ($bamount > 0) {
                            $ispaid = 'false';
                        }

                        $app_amount = $in_data['AppliedAmount'] + (-$amount);
                        $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);

                        $condition = array('TxnID' => $in_data['TxnID']);
                        $this->general_model->update_row_data('qb_test_invoice', $condition, $data);

                        $user = $in_data['qbwc_username'];
                        
                        if ($cardID == "new1") {
                            if ($sch_method == "1") {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $cardType = $this->general_model->getType($card_no);

                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                $card_data = array(
                                    'cardMonth'       => $expmonth,
                                    'cardYear'        => $exyear,
                                    'CardType'        => $cardType,
                                    'CustomerCard'    => $card_no,
                                    'CardCVV'         => $cvv,
                                    'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'  => $customerID,

                                    'companyID'       => $companyID,
                                    'merchantID'      => $this->merchantID,
                                    'createdAt'       => date("Y-m-d H:i:s"),
                                );

                                $id1 = $this->card_model->process_card($card_data);
                            } else if ($sch_method == "2") {
                                $id1 = $this->card_model->process_ack_account($accountDetails);
                            }
                        }

                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                        $ref_number     = $in_data['RefNumber'];
                        $tr_date        = date('Y-m-d H:i:s');
                        $toEmail        = $c_data['Contact'];
                        $company        = $c_data['companyName'];
                        $customer       = $c_data['FullName'];
                        if ($chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                        }
                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                    } else {
                        $err_msg = $result['msg'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                    }

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID = '', $this->resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                    if ($result['status'] == 'success' && !is_numeric($invoiceID)) {
                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
            }

        } else {
            $error = 'Validation Error. Please fill the requred fields';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
        }

        if ($cusproID == "2") {
            redirect('home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3" && $in_data['TxnID'] != '') {
            redirect('home/invoice_details/' . $in_data['TxnID'], 'refresh');
        }

        $invoice_IDs = array();

        $receipt_data = array(
            'proccess_url'      => 'home/invoices',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }
        redirect('home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $custom_data_fields = [];
            $applySurcharge = false;
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            $inputData = $this->input->post(null, true);

            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                if ($this->session->userdata('logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                if (!$customerID || empty($customerID)) {
                    $customerID = create_card_customer($this->input->post(null, true), '1');
                }

                $comp_data = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $qbd_comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
                $user      = $qbd_comp_data['qbwc_username'];

                $user_id     = $merchantID;
                $cardID      = $this->czsecurity->xssCleanPostInput('card_list');
                $contact     = $this->czsecurity->xssCleanPostInput('phone');
                $cvv         = '';
                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                        $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                    }

                    $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                    $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
                    $city     = $this->czsecurity->xssCleanPostInput('bcity');
                    $country  = $this->czsecurity->xssCleanPostInput('bcountry');
                    $phone    = $this->czsecurity->xssCleanPostInput('bphone');
                    $state    = $this->czsecurity->xssCleanPostInput('bstate');
                    $zipcode  = $this->czsecurity->xssCleanPostInput('bzipcode');

                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    if ($card_data['CardCVV']) {
                        $cvv = $card_data['CardCVV'];
                    }

                    $cardType = $card_data['CardType'];
                    $address1 = $card_data['Billing_Addr1'];
                    $address2 = $card_data['Billing_Addr2'];
                    $city     = $card_data['Billing_City'];
                    $zipcode  = $card_data['Billing_Zipcode'];
                    $state    = $card_data['Billing_State'];
                    $country  = $card_data['Billing_Country'];
                }
                /*Added card type in transaction table*/
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . '/' . $exyear1;

                $name    = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address = $this->czsecurity->xssCleanPostInput('address');
                $country = $this->czsecurity->xssCleanPostInput('country');
                $city    = $this->czsecurity->xssCleanPostInput('city');
                $state   = $this->czsecurity->xssCleanPostInput('state');
                $amount  = $this->czsecurity->xssCleanPostInput('totalamount');
                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;
                $zipcode = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '74035';
                
                if(!empty($this->czsecurity->xssCleanPostInput('po_number'))){
                    
                }

                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 2);
                    
                }

                $crtxnID = '';
                $invID   = '';
                $responseId = '';
                
                // Maverick Payment Gateway
                $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                // Sale Payload
                $request_payload = [
                    'level' => 1,
                    'threeds' => [
                        'id' => null,
                    ],
                    'amount' => $amount,
                    'card' => [
                        'number' => $card_no,
                        'cvv'    => $cvv,
                        'exp'    => $expry,
                        'save'   => 'No',
                        'address' => [
                            'street' => $inputData['baddress1'],
                            'city' => $inputData['bcity'],
                            'state' => $inputData['bstate'],
                            'country' => $inputData['bcountry'],
                            'zip' => $inputData['bzipcode'],
                        ]
                    ],
                    'contact' => [
                        'name'   => $inputData['firstName'].' '.$inputData['lastName'],
                        'email'  => $inputData['email'],
                        'phone' => $inputData['phone'],
                    ]
                ];


                $result = [];

                // Process Sale
                $r = $this->maverickgateway->processSale($request_payload);

                $rbody = json_decode($r['response_body'], 1);
                
                $result['data'] = $rbody;
                
                $result['response_code'] = $r['response_code'];

                if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                    if($rbody['status']['status'] == 'Approved'){
                        $result['status'] = 'success';
                        $result['msg'] = $result['message'] = 'Payment success.';
                    } else {
                        $result['status'] = 'failed';
                        $result['msg'] = $result['message'] = 'Payment failed.';    
                    }
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = $rbody['message'];
                }

				if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];

                    $invoiceIDs                 = array();
					$invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }
                    $refnum = array();
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

                            if (!empty($theInvoice))
                            {
                                
                                $amount_data = $theInvoice['BalanceRemaining'];
                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                    $actualInvoicePayAmount += $surchargeAmount;
                                    $updatedInvoiceData = [
                                        'inID' => $inID,
                                        'merchantID' => $user_id,
                                        'amount' => $surchargeAmount,
                                    ];
                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,2);
                                    $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
                                    $amount_data = $theInvoice['BalanceRemaining'];

                                }
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                if($saleAmountRemaining > 0){
                                    $refnum[] = $theInvoice['RefNumber'];
                                    if($amount_data == $actualInvoicePayAmount){
                                        $actualInvoicePayAmount = $amount_data;
                                        $isPaid      = 'true';

                                    }else{

                                        $actualInvoicePayAmount = $actualInvoicePayAmount;
                                        $isPaid      = 'false';
                                        $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                        
                                        
                                    }
                                    $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                    $tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                    $comp_data1 = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$user_id));
                                    $user      = $comp_data1['qbwc_username'] ;
                                    if(!is_numeric($inID))
                                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
                                }
                                
                            }
                            $payIndex++;
                            
                        }
                    } else {

                        $transactiondata = array();
                        $inID            = '';
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $contact,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                    $ref_number     = implode(',', $refnum);
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['Contact'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['FullName'];
                    if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $err_msg = $result['msg'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>');
            }

        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        
        if (!empty($this->input->post(null, true))) {

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $checkPlan = check_free_plan_transactions();

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
            $name = $this->czsecurity->xssCleanPostInput('firstName'). " " . $this->czsecurity->xssCleanPostInput('lastName');
            $customerID   = $this->czsecurity->xssCleanPostInput('customerID');

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $comp_data = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $invoiceIDs = [];
                
                $responseId = '';
                $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);
                
                // get DBA id
                $dbaId = $this->maverickgateway->getDba(true);

                // electronic Sale
                $request_payload = [
                    'amount'          => $amount,
                    'routingNumber'   => $accountDetails['routeNumber'],
                    'accountName'     => $accountDetails['accountName'],
                    'accountNumber'   => $accountDetails['accountNumber'],
                    'accountType'     => ucwords($accountDetails['accountType']), // Checking or Savings
                    'transactionType' => 'Debit', // Debit or Credit
                    'customer' => [
                        'email' => $this->czsecurity->xssCleanPostInput('email'),
                        'firstName'     => $this->czsecurity->xssCleanPostInput('firstName'),
                        'lastName' => $this->czsecurity->xssCleanPostInput('lastName'),
                        "address1" => $this->czsecurity->xssCleanPostInput('baddress1'),
                        "address_2" => $this->czsecurity->xssCleanPostInput('baddress2'),
                        "city"           => $this->czsecurity->xssCleanPostInput('bcity'),
                        "state"          => $this->czsecurity->xssCleanPostInput('bstate'),
                        "zipCode"    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        "country"        => $this->czsecurity->xssCleanPostInput('country'),
                        "phone"          => $this->czsecurity->xssCleanPostInput('phone'),
                    ],
                    'dba' => [
                        'id' => $dbaId,
                    ],
                        
                ];


                if(!empty($this->czsecurity->xssCleanPostInput('po_number'))){
                }

                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 2);
                }

                $result = [];

                $r = $this->maverickgateway->processAch($request_payload);
                
                $rbody = json_decode($r['response_body'], true);

                $result['response_code'] = $r['response_code'];
                
                // Response                
                if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                    
                    if(isset($rbody['id']) && $rbody['id']){

                        $result['data']['id'] =  $rbody['id'];
                        $result['status'] = 'success';
                        $result['msg'] = $result['message'] = 'Payment success.';
                    } else {
                        $result['status'] = 'failed';
                        $result['msg'] = $result['message'] = 'Payment failed.';    
                    }

                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = $rbody['message'];

                 
                }

                if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];

					$invoiceIDs = [];
                    $invoicePayAmounts = [];
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }
					$refnum = array();
					$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));

					if (!empty($invoiceIDs)) {
                        $payIndex = 0;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();

							$theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

							if (!empty($theInvoice)) {
                                $amount_data = $theInvoice['BalanceRemaining'];

                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                $refnum[] = $theInvoice['RefNumber'];

                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 'true';

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 'false';
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $txnAmount = $actualInvoicePayAmount;

                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                
                                $tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID, $crtxnID = '', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);

								$user      = $comp_data['qbwc_username'];
                                if(!is_numeric($inID))
								    $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
							}
                            $payIndex++;
						}
					} else {
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
					}

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number     = '';
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['companyEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['firstName'] . ' ' . $comp_data['lastName'];

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg = $result['msg'];
                    $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                }
            
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            if(!$checkPlan){
                $responseId  = '';
            }
            
            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('Payments/create_customer_esale', 'refresh');
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $inputData = $this->input->post(null, true);

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                if (!$customerID || empty($customerID)) {
                    $customerID = create_card_customer($this->input->post(null, true), '1');
                }

                $comp_data = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
                $companyID = $comp_data['companyID'];

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                } else {

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];

                    $exyear = $card_data['cardYear'];
                    $cvv = $card_data['CardCVV'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . '/' . $exyear1;

                $name    = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country = $this->czsecurity->xssCleanPostInput('bcountry');
                $city    = $this->czsecurity->xssCleanPostInput('bcity');
                $state   = $this->czsecurity->xssCleanPostInput('bstate');
                $amount  = $this->czsecurity->xssCleanPostInput('totalamount');
                $zipcode = ($this->czsecurity->xssCleanPostInput('bzipcode')) ? $this->czsecurity->xssCleanPostInput('bzipcode') : '74035';
                // Maverick Payment Gateway
                $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                $crtxnID = '';
                // Sale Payload
                $request_payload = [
                    'level' => 1,
                    'threeds' => [
                        'id' => null,
                    ],
                    'amount' => $amount,
                    'card' => [
                        'number' => $card_no,
                        'cvv'    => $cvv,
                        'exp'    => $expry,
                        'save'   => 'No',
                        'address' => [
                            'street' => $inputData['baddress1'],
                            'city' => $inputData['bcity'],
                            'state' => $inputData['bstate'],
                            'country' => $inputData['bcountry'],
                            'zip' => $inputData['bzipcode'],
                        ]
                    ],
                    'contact' => [
                        'name'   => $inputData['firstName'].' '.$inputData['lastName'],
                        'email'  => $inputData['email'],
                        'phone' => $inputData['phone'],
                    ]
                ];
                if(!empty($po_number)){
                    $request_payload['po_number'] = $po_number;
                }

                if(!empty($this->czsecurity->xssCleanPostInput('invoice_number'))){
                    $request_payload['order_id'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                // Process Sale
                $r = $this->maverickgateway->processAuthorize($request_payload);


                $rbody = json_decode($r['response_body'], 1);

                $result = [];
                $result['response_code'] = $r['response_code'];

                if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                    if($rbody['status']['status'] == 'Approved'){
                        $result['data'] = $rbody;
                        $result['status'] = 'success';
                        $result['msg'] = $result['message'] = 'Payment Authorize success.';
                    } else {
                        $result['status'] = 'failed';
                        $result['msg'] = $result['message'] = 'Payment Authorize failed.';    
                    }
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = $rbody['message'];
                }
                
                if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];
                    /* This block is created for saving Card info in encrypted form  */

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                        $cardType = $this->general_model->getType($card_no);

                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $cardType,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('baddress1'),
                            'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('baddress2'),
                            'Billing_City'    => $this->czsecurity->xssCleanPostInput('bcity'),
                            'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                            'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                            'Billing_State'   => $this->czsecurity->xssCleanPostInput('bstate'),
                            'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                            'customerListID'  => $this->czsecurity->xssCleanPostInput('customerID'),
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                        );

                        $id1 = $this->card_model->process_card($card_data);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg = $result['msg'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  '. $err_msg .'</strong></div>');
                }
                
                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, '', false, $this->transactionByUser, $custom_data_fields);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>');
            }

            if(!$checkPlan){
                $responseId  = '';
            }

            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'Payments/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('home/transation_sale_receipt', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $compdata          = $this->customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/payment_auth', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($tID != '' && !empty($gt_result)) {
                
                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);

                $customerID = $paydata['customerListID'];
                $amount     = $paydata['transactionAmount'];
                $customerID = $paydata['customerListID'];
                $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                $result = [];

                $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                // Capture
                $r = $this->maverickgateway->captureSale($tID, $amount);

                $result['response_code'] = $r['response_code'];
                

                $rbody = json_decode($r['response_body'], 1);
                
                $result['data'] = $rbody;

                if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                    if($rbody['status']['status'] == 'Approved'){
                        $result['status'] = 'success';
                        $result['msg'] = $result['message'] = 'Amount captured successfully.';
                    }
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = $rbody['message'];
                }
                
                if ($result['status'] == 'success') {
                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "4");

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];

                    $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['Contact'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['FullName'];
                    if ($chh_mail == '1') {

                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                    }
                    $this->session->set_flashdata('success', ' Successfully Captured Authorization');
                } else {
                    $err_msg = $result['status'] = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : '';

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');

            }
            $invoice_IDs = array();
           

            $receipt_data = array(
                'proccess_url'      => 'Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            
            if (!isset($result['data']['id'])) {
                $result['transactionid'] = 'null';
            } else {
                $result['transactionid'] = $result['data']['id'];
            }
            redirect('home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result['transactionid'], 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_void()
    {
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $con     = array(
                'transactionID' => $tID,
            );
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($tID != '' && !empty($gt_result)) {
                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);

                $customerID = $paydata['customerListID'];
                $amount     = $paydata['transactionAmount'];
                $customerID = $paydata['customerListID'];
                $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                $r = $this->maverickgateway->voidSale($tID);

                $result['response_code'] = $r['response_code'];
                
                $rbody = json_decode($r['response_body'], 1);
                
                $result['data'] = $rbody;
                
                $this->maverickgateway->d($rbody);
                
                if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                    if($rbody['status']['status'] == 'Approved'){
                        $result['status'] = 'success';
                        $result['msg'] = $result['message'] = 'Amount captured successfully.';
                    }
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = $rbody['message'];
                }
                    
                if ($result['status'] == 'success') {
                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "3");

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                    if ($chh_mail == '1') {
                        $condition  = array('transactionID' => $tID);
                        $customerID = $paydata['customerListID'];

                        $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['Contact'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['FullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                    }

                    $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                } else {
                    $err_msg = $result['msg'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
            }
        }
        redirect('Payments/payment_capture', 'refresh');
    }

    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $user_id = $da['merchantID'];
        }
        if (!empty($this->input->post(null, true))) {
            $inputData = $this->input->post(null, true);
            $tID        = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array(
                'transactionID' => $tID
            );
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($tID != '' && !empty($gt_result)) {
                $amount = $total = $paydata['transactionAmount'];
                
                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);
                if (!empty($paydata)) {
                    $customerID = $paydata['customerListID'];
                    if(isset($inputData['ref_amount'])){
                        $total  = $this->czsecurity->xssCleanPostInput('ref_amount');
                        $amount = $total;
                    }
                    
                    if ($paydata['transactionCode'] != '300') {
                        $request_data = array("transaction_id" => $tID);
                        /******************This is for Invoice Refund Process***********/

                        if (!empty($paydata['invoiceTxnID'])) {
                            $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $paydata['merchantID']));

                            $user_id = $paydata['merchantID'];
                            $user    = $cusdata['qbwc_username'];
                            $comp_id = $cusdata['id'];

                            $ittem                  = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                            $ins_data['customerID'] = $paydata['customerListID'];
                            if (empty($ittem)) {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');

                            }

                            $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                            if (!empty($in_data)) {
                                $inv_pre    = $in_data['prefix'];
                                $inv_po     = $in_data['postfix'] + 1;
                                $new_inv_no = $inv_pre . $inv_po;

                            }
                            $ins_data['merchantDataID']    = $paydata['merchantID'];
                            $ins_data['creditDescription'] = "Credit as Refund";
                            $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                            $ins_data['creditDate']        = date('Y-m-d H:i:s');
                            $ins_data['creditAmount']      = $total;
                            $ins_data['creditNumber']      = $new_inv_no;
                            $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                            $ins_data['Type']              = "Payment";
                            $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                            $item['itemListID']      = $ittem['ListID'];
                            $item['itemDescription'] = $ittem['Name'];
                            $item['itemPrice']       = $total;
                            $item['itemQuantity']    = 0;
                            $item['crlineID']        = $ins_id;
                            $acc_name                = $ittem['DepositToAccountName'];
                            $acc_ID                  = $ittem['DepositToAccountRef'];
                            $method_ID               = $ittem['PaymentMethodRef'];
                            $method_name             = $ittem['PaymentMethodName'];
                            $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                            $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                            $refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $total,
                                'creditInvoiceID'                             => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
                                'creditTxnID'                                 => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
                                'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),
                                'paymentMethod'                               => $method_ID, 'paymentMethodName'                 => $method_name,
                                'AccountRef'                                  => $acc_ID, 'AccountName'                          => $acc_name,
                            );

                            if ($ins_id && $ins) {
                                $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));
                            } else {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>');
                            }

                        }

                        // Maverick Payment Gateway
                        $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                        $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                        $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                        $r = $this->maverickgateway->refundSale($tID, $amount);

                        $result = [];

                        $rbody = json_decode($r['response_body'], 1);

                        $result['data'] = $rbody;

                        $result['response_code'] = $r['response_code'];

                        if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                            if($rbody['status']['status'] == 'Approved'){
                                $result['status'] = 'success';
                                $result['msg'] = $result['message'] = 'Successfully Refunded Payment.';
                            } else {
                                $result['status'] = 'failed';
                                $result['msg'] = $result['message'] = 'Payment refund failed.';    
                            }
                        } else {
                            $result['status'] = 'failed';
                            $result['msg'] = $result['message'] = $rbody['message'];
                        }
                    }

                    $resposneId= '';
                    if ($result['status'] == 'success') {
                        $resposneId = $result['data']['id'];
                        $this->customer_model->update_refund_payment($tID, '');

                        if (!empty($paydata['invoiceTxnID'])) {
                            $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $ins_id, '1', '', $user);
                        } else {
                            $inv       = '';
                            $ins_id    = '';
                            $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
                                'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                                'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                                'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
                            );
                        }
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                        $this->session->set_flashdata('success', 'Successfully Refunded Payment');
                    } else {
                        $err_msg = $result['msg'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                    }

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $paydata['customerListID'], $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, $custom_data_fields);

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Invalid Transactions</strong>.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
            }
        }

        if (!empty($this->czsecurity->xssCleanPostInput('payrefund'))) {
            $invoice_IDs = array();
           
            $receipt_data = array(
                'proccess_url'      => 'Payments/payment_refund',
                'proccess_btn_text' => 'Process New Refund',
                'sub_header'        => 'Refund',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            redirect('home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $resposneId, 'refresh');

        } else {
            redirect('Payments/payment_transaction', 'refresh');
        }
    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnID');

            $con     = array(
                'transactionID' => $tID
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $customerID = $paydata['customerListID'];
            $amount = $paydata['transactionAmount'];

            // Maverick Payment Gateway
            $this->maverickgateway->setApiMode($this->gatewayEnvironment);
            $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
            $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

            $r = $this->maverickgateway->refundAchSale($tID, $amount);

            $rbody = json_decode($r['response_body'], 1);
            
            $result['data'] = $rbody;
            
            $result['response_code'] = $r['response_code'];

            if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                if($rbody['status']['status'] == 'Approved'){
                    $result['status'] = 'success';
                    $result['msg'] = $result['message'] = 'Payment Refunded successfully.';
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = 'Payment refund failed.';    
                }
            } else {
                $result['status'] = 'failed';
                $result['msg'] = $result['message'] = $rbody['message'];
            }

            if ($result['status'] == 'success') {
                $this->customer_model->update_refund_payment($tID, '');

                if (!empty($paydata['invoice_id'])) {
                    $paymts   = explode(',', $paydata['tr_amount']);
                    $invoices = explode(',', $paydata['invoice_id']);
                    $ins_id   = '';
                    foreach ($invoices as $k1 => $inv) {

                        $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paymts[$k1],
                            'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                    }

                    $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $this->merchantID));
                    $user_id = $this->merchantID;
                    $user    = $cusdata['qbwc_username'];
                    $comp_id = $cusdata['id'];
                    $ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                    $refund  = $amount;

                    if (empty($ittem)) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
                        exit;
                    }
                    $ins_data['customerID'] = $customerID;

                    foreach ($invoices as $k => $inv) {
                        $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                        if (!empty($in_data)) {
                            $inv_pre    = $in_data['prefix'];
                            $inv_po     = $in_data['postfix'] + 1;
                            $new_inv_no = $inv_pre . $inv_po;
                        }
                        $ins_data['merchantDataID']    = $this->merchantID;
                        $ins_data['creditDescription'] = "Credit as Refund";
                        $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                        $ins_data['creditDate']        = date('Y-m-d H:i:s');
                        $ins_data['creditAmount']      = $paymts[$k];
                        $ins_data['creditNumber']      = $new_inv_no;
                        $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                        $ins_data['Type']              = "Payment";
                        $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                        $item['itemListID']      = $ittem['ListID'];
                        $item['itemDescription'] = $ittem['Name'];
                        $item['itemPrice']       = $paymts[$k];
                        $item['itemQuantity']    = 0;
                        $item['crlineID']        = $ins_id;
                        $acc_name                = $ittem['DepositToAccountName'];
                        $acc_ID                  = $ittem['DepositToAccountRef'];
                        $method_ID               = $ittem['PaymentMethodRef'];
                        $method_name             = $ittem['PaymentMethodName'];
                        $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                        $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                        $refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $paymts[$k],
                            'creditInvoiceID'                             => $invID, 'creditTransactionID'          => $tID,
                            'creditTxnID'                                 => $ins_id, 'refundCustomerID'            => $customerID,
                            'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                            'paymentMethod'                               => $method_ID, 'paymentMethodName'        => $method_name,
                            'AccountRef'                                  => $acc_ID, 'AccountName'                 => $acc_name,
                        );


                        if ($ins_id && $ins) {
                            $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

                            $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $ins_id, '1', '', $user);
                        }

                    }

                } else {
                    $inv       = '';
                    $ins_id    = '';
                    $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
                        'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                        'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
                    );
                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                }

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {
                $err_msg = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['status'] . '</strong>.</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], true, $this->transactionByUser, $custom_data_fields);
        }
        redirect('Payments/echeck_transaction', 'refresh');
    }

    public function payment_evoid()
    {
        $custom_data_fields = [];
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con     = array(
                'transactionID' => $tID
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $customerID = $paydata['customerListID'];
            $amount            = $paydata['transactionAmount'];

            // Maverick Payment Gateway
            $this->maverickgateway->setApiMode($this->gatewayEnvironment);
            $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
            $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

            $r = $this->maverickgateway->voidAchSale($tID);

            $rbody = json_decode($r['response_body'], 1);
            
            $result['data'] = $rbody;
            
            $result['response_code'] = $r['response_code'];

            if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                if($rbody['status']['status'] == 'Approved'){
                    $result['status'] = 'success';
                    $result['msg'] = $result['message'] = 'Payment Refunded successfully.';
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = 'Payment refund failed.';    
                }
            } else {
                $result['status'] = 'failed';
                $result['msg'] = $result['message'] = $rbody['message'];
            }
                
            if ($result['status'] == 'success') {

                $condition   = array('transactionID' => $tID);
                $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
            } else {
                $err_msg = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['status'] . '</strong>.</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            redirect('Payments/evoid_transaction', 'refresh');
        }
    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $user_id = $da['merchantID'];
        }

        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        $invoices   = $this->czsecurity->xssCleanPostInput('multi_inv');
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');
        $customerID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID, 'qbmerchantID' => $user_id));
        $companyID  = $comp_data['companyID'];

        $cusproID = '';
        $error    = '';
        $custom_data_fields = [];
        $cusproID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

        $resellerID = $this->resellerID;
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && !empty($invoices)) {
            foreach ($invoices as $invoiceID) {
                $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                $in_data     = $this->quickbooks->get_invoice_data_pay($invoiceID);

                $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $comp_data   = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'FirstName', 'LastName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                
                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];

                if ($cardID != "" || $gateway != "") {

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['Customer_ListID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
    
                            $address1 = $this->czsecurity->xssCleanPostInput('address1');
                            $address2 = $this->czsecurity->xssCleanPostInput('address2');
                            $city     = $this->czsecurity->xssCleanPostInput('city');
                            $country  = $this->czsecurity->xssCleanPostInput('country');
                            $phone    = $this->czsecurity->xssCleanPostInput('contact');
                            $state    = $this->czsecurity->xssCleanPostInput('state');
                            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;


                        if (!empty($cardID)) {

                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount = $pay_amounts;
                            $amount = $amount - $cr_amount;

                            $name    = $in_data['Customer_FullName'];
                            $address = $in_data['ShipAddress_Addr1'];
                           
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth .'/'. $exyear;
                           
                            
                            // Maverick Payment Gateway
                            $this->maverickgateway->setApiMode($this->gatewayEnvironment);
                            $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                            $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                            // Sale Payload
                            $request_payload = [
                                'level' => 1,
                                'threeds' => [
                                    'id' => null,
                                ],
                                'amount' => $amount,
                                'card' => [
                                    'number' => $card_no,
                                    'cvv'    => $cvv,
                                    'exp'    => $expry,
                                    'save'   => 'No',
                                    'address' => [
                                        'street' => $address,
                                        'city' => $city,
                                        'state' => $state,
                                        'country' => $country,
                                        'zip' => $zipcode,
                                    ]
                                ],
                                'contact' => [
                                    'name'   => $comp_data['FirstName'].' '.$comp_data['LastName'],
                                    'email'  => '',
                                    'phone' => $phone,
                                ]
                            ];
            
                            // Process Sale
                            $r = $this->maverickgateway->processSale($request_payload);
            
                            $rbody = json_decode($r['response_body'], 1);
                            
                            $result['data'] = $rbody;
                            
                            $result['response_code'] = $r['response_code'];
            
                            if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                                if($rbody['status']['status'] == 'Approved'){
                                    $result['status'] = 'success';
                                    $result['msg'] = $result['message'] = 'Payment success.';
                                } else {
                                    $result['status'] = 'failed';
                                    $result['msg'] = $result['message'] = 'Payment failed.';    
                                }
                            } else {
                                $result['status'] = 'failed';
                                $result['msg'] = $result['message'] = $rbody['message'];
                            }


                            $responseId = $crtxnID = '';
                            
                            if ($result['status'] == 'success') {
                                $responseId = $result['data']['id'];
                                $txnID   = $in_data['TxnID'];
                                $ispaid  = 'true';
                                $bamount = $in_data['BalanceRemaining'] - $amount;
                                if ($bamount > 0) {
                                    $ispaid = 'false';
                                }

                                $app_amount = $in_data['AppliedAmount'] + (-$amount);
                                $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);
                                $condition  = array('TxnID' => $in_data['TxnID']);
                                $this->general_model->update_row_data('qb_test_invoice', $condition, $data);

                                $user = $in_data['qbwc_username'];

                                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $in_data['Customer_ListID'],

                                        'companyID'       => $comp_data['companyID'],
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                }

                                if ($chh_mail == '1') {
                                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                                    $ref_number     = $in_data['RefNumber'];
                                    $tr_date        = date('Y-m-d H:i:s');
                                    $toEmail        = $comp_data['Contact'];
                                    $company        = $comp_data['companyName'];
                                    $customer       = $comp_data['FullName'];

                                    $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                                }
                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            } else {
        
                                $err_msg = $result['status'] = $result['msg'];
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $user_id, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                            if ($result['status'] == 'success' && !is_numeric($invoiceID)) {
                                $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                            }

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>');
                }

            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Gateway and Card are required</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
        
        if ($cusproID != "") {
            redirect('home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('home/invoices', 'refresh');
        }

    }
}
