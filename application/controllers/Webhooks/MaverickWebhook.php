<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MaverickWebhook extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();

    }

    public function getSignature(){
        switch(getenv('ENV')){
            case 'development':
                $signature = 'Pcda7zR-s08KrbGOFaDQc6WILziyrsqg';
            break;
            case 'staging':
                $signature = 'Pcda7zR-s08KrbGOFaDQc6WILziyrsqg';
            break;
            default:
                $signature = 'Pcda7zR-s08KrbGOFaDQc6WILziyrsqg';
            break;
        }

        return $signature;
    }

    /**
     * 
     * <domain_url>/Webhooks/MaverickWebhook/ach_trxn
     */
    public function ach_trxn(){
        
        $content = file_get_contents('php://input');

        if(getenv('ENV') == 'development') {
            
            $fp = fopen('./uploads/maverick_webhook_log.txt', 'a+');
            fwrite($fp, "\r\n".'===== ACH '.date('c').'====='."\r\n");
            fwrite($fp, $content);
            fclose($fp);
        }

        if($content != ''){
            $response = json_decode($content, 1);

            if($response['module'] == 'ach'){
                $action = $response['action'];

                $data = $response['data'];

                $id = $data['id'];

                $where = ['transactionID' => $id, 'gatewayID' => 17];

                $transaction_data = $this->general_model->get_row_data('customer_transaction',  $where);
                
            }
        }
        echo 'ACH';

        exit;
    }

    public function chargeback_trxn(){
        $content = file_get_contents('php://input');

        if(getenv('ENV') == 'development') {
            $fp = fopen('./uploads/maverick_webhook_log.txt', 'a+');
            fwrite($fp, "\r\n".'===== ChargeBack '.date('c').'====='."\r\n");
            fwrite($fp, $content);
            fclose($fp);
        }

        echo 'Chargeback';

        exit;
    }
}