<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require APPPATH . 'libraries/integration/XeroSync.php';

class Invoices extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');

        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function invoice_list_ajax()
    {
        $in_data['merchantID'] = $this->merchantID;
        $active_app            = $this->logged_in_data['active_app'];
        $merchantEmailID       = $this->logged_in_data['merchantEmail'];

        $count = $no = 0;

        $filter = $this->czsecurity->xssCleanPostInput('status_filter');

        $customerID = $this->czsecurity->xssCleanPostInput('customerID');

        $invoices = $this->data_model->get_invoice_data($this->merchantID, $filter, $customerID);
        $count    = $this->data_model->get_invoice_list_count($this->merchantID, $filter, $customerID);

        $plantype    = $this->general_model->chk_merch_plantype_status($this->merchantID);
        $plantype_vt = $this->general_model->chk_merch_plantype_vt($this->merchantID);

        $data = array();
        $no   = $_POST['start'];
        foreach ($invoices as $invoice) {
            $no++;
            $row       = array();
            $base_url1 = base_url('Integration/Customers/customer_detail/' . $invoice['ListID']);
            $base_url2 = base_url('Integration/Invoices/invoice_details/' . $invoice['invoiceID']);
            if (empty($customerID)) {

                $row[] = "<div class='text-center visible-lg'><input class='singleCheck' type='checkbox' name='selectRow[]'' value='" . $invoice['invoiceID'] . "' ></div>";

                if ($plantype) {
                    $row[] = "<div class='text-left visible-lg'>" . $invoice['FullName'] . "";
                } else {
                    $row[] = "<div class='text-left visible-lg cust_view'><a href='" . $base_url1 . "' >" . $invoice['FullName'] . " </a>";
                }
            }
            if ($plantype_vt) {
                $row[] = "<div class='text-right cust_view'>" . $invoice['refNumber'] . "";
            } else {
                $row[] = "<div class='text-right cust_view'><a href='" . $base_url2 . "' >" . $invoice['refNumber'] . " </a>";
            }
            $row[] = "<div class='hidden-xs text-right'> " . date('M d, Y', strtotime($invoice['DueDate'])) . " </div>";
            $row[] = '<div class="hidden-xs text-right cust_view"> <a href="#pay_data_process" onclick="set_common_payment_data(\'' . $invoice['invoiceID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">' . '$' . number_format(($invoice['BalanceRemaining']), 2) . ' </a> </div>';

            $row[]          = "<div class='text-right'>" . ucfirst($invoice['status']) . "</div>";
            $link           = '';
            $disabled_class = '';
            if (strtolower($filter) == 'paid') {
                $transaction_history = $this->general_model->get_select_data('customer_transaction', array('gateway'), array('customerListID' => $invoice['ListID'], 'invoiceID' => $invoice['invoiceID'], 'merchantID' => $this->merchantID, 'transactionType' => 'sale', 'transactionCode' => 200));

                if ($transaction_history && strtolower($transaction_history['gateway']) == 'heartland echeck') {
                    $disabled_class = "disabled";
                }
            }
            if ($invoice['status'] == "Paid") {
                $link .= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle ' . $disabled_class . '">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li><a href="javascript:void(0);" id="txnRefund' . $invoice['invoiceID'] . '"  invoice-id="' . $invoice['invoiceID'] . '" integration-type="2" data-url="' . base_url() . 'ajaxRequest/getTotalRefundOfInvoice" class="refunTotalInvoiceAmountCustom">Refund</a></li>

						          </ul>
						          </div> </div>';
            } else if (($invoice['status'] = 'Scheduled' or $invoice['status'] == 'Past Due') && strtoupper($invoice['userStatus']) != strtoupper('Cancel')) {
                $link .= '<div class="text-center">

					<div class="btn-group dropbtn">
					<a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle ' . $disabled_class . '">Select <span class="caret"></span></a>
					<ul class="dropdown-menu text-left">
					<li> <a href="#invoice_process" class=""  data-backdrop="static" data-keyboard="false"
				data-toggle="modal" onclick="set_invoice_process_id(\'' . $invoice['invoiceID'] . '\',\'' . $invoice['CustomerListID'] . '\',\'' . $invoice['BalanceRemaining'] . '\',1);">Process</a></li>
			  <li><a href="#invoice_schedule" onclick="set_invoice_schedule_id(\'' . $invoice['invoiceID'] . '\',\'' . $invoice['CustomerListID'] . '\',\'' . $invoice['BalanceRemaining'] . '\',\'' . date('M d Y', strtotime($invoice['DueDate'])) . '\');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>

			   <li> <a href="#set_subs" class=""  onclick=
		   "set_sub_status_id(\'' . $invoice['invoiceID'] . '\',\'' . $invoice['CustomerListID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>';
                if ($invoice['AppliedAmount'] == 0 || $invoice['AppliedAmount'] == '0.00') {
                    $link .= '<li> <a href="#invoice_cancel" onclick="set_common_invoice_id(\'' . $invoice['invoiceID'] . '\');" data-keyboard="false" data-toggle="modal" class="">Void</a></li>';
                }
                $link .= '</ul>
                                     </div> </div>  ';
            } else {
                $link .= '<div class="btn-group dropbtn">
			 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle ' . $disabled_class . '">Select <span class="caret"></span></a>
			 <ul class="dropdown-menu text-left">

				 <li> <a href="javascript:void(0);" disabled class="">Voided</a></li></ul>
				 </div>';
            }
            $row[] = $link;

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $count,
            "recordsFiltered" => $count,
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
        die;
    }

    public function invoice_list()
    {
        $filter              = $this->czsecurity->xssCleanPostInput('status_filter');
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['filter']      = $filter;

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        if (!empty($this->input->post())) {
            $search = $this->czsecurity->xssCleanPostInput();
            if (isset($search['search_data'])) {
                $data['serachString'] = $search['search_data'];
            }
        }

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->sycnCustomer();
        $xeroSync->syncInvoices();
        $xeroSync->syncAccounts(['EnablePaymentsToAccount' => 1]);

        $data['page_num']      = 'invoices_xero';
        $condition             = array('merchantID' => $user_id);
        $data['gateway_datas'] = $this->general_model->get_gateway_data($user_id);

        $data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
        $data['isCardGatewayExists']   = $data['gateway_datas']['isCardExists'];

        $merchant_condition = [
            'merchID' => $user_id,
        ];
        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway                = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway']        = $defaultGateway[0];
            $data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
            $data['isCardGatewayExists']   = $data['defaultGateway']['creditCard'];
        }

        //    $data['gateway_datas']          = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $today = date('Y-m-d');

        $condition1 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "IsPaid" => "true", "cmp.merchantID" => $user_id);
        $condition2 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "IsPaid" => "false", "inv.userStatus !=" => 'cancel', "cmp.merchantID" => $user_id);
        $condition3 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "IsPaid" => "false", "inv.userStatus !=" => 'cancel', "cmp.merchantID" => $user_id);
        $condition4 = array("IsPaid" => "false", 'userStatus' => 'cancel', "companyID" => $user_id);

        $data['success_invoice'] = 0; // $this->company_model->get_invoice_paid_data_count($user_id);
        $data['upcomming_inv']   = 0; // $this->customer_model->get_invoice_schedule_data_count($user_id);
        $data['failed_inv']      = 0; // $this->company_model->get_invoice_data_count_failed($user_id);
        $data['cancel_inv']      = 0; // $this->company_model->get_invoice_past_data_count($user_id);

        $condition = array('comp.merchantID' => $user_id);

        $data['credits'] = $this->general_model->get_credit_user_data($condition);

        $data['invoices'] = array();

        $plantype            = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
        $data['plantype_vt'] = $this->general_model->chk_merch_plantype_vt($user_id);
        $data['plantype']    = $plantype;

        $data['from_mail']       = DEFAULT_FROM_EMAIL;
        $data['mailDisplayName'] = $this->logged_in_data['companyName'];
        $data['merchantEmail']   = $this->session->userdata('logged_in')['merchantEmail'];

        $templateList = $this->general_model->getTemplateForInvoice($user_id);

        $data['templateList'] = $templateList;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_invoice_list', $data);
        $this->load->view('comman-pages/popup_box', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_invoice()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        if (!empty($this->input->post())) {
            $input_data = $this->czsecurity->xssCleanPostInput();

            $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
            if (!empty($in_data)) {
                $inv_pre    = $in_data['prefix'];
                $inv_po     = $in_data['postfix'] + 1;
                $new_inv_no = $inv_pre . $inv_po;
                $randomNum  = rand(100000, 900000);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

                redirect('Integration/Invoices/invoice_list', 'refresh');
            }

            $invoiceID = $this->czsecurity->xssCleanPostInput('invNo');

            if ($invoiceID && !empty($invoiceID)) {
                $dueDate  = date("Y-m-d", strtotime($input_data['dueDate']));
                $inv_date = date("Y-m-d", strtotime($input_data['invDate']));

                $saveMessage = "Updated";
            } else {
                $inv_date = date("Y-m-d", strtotime($input_data['invdate']));
                $pterm    = $input_data['pay_terms'];
                if ($pterm != "" && $pterm != 0) {
                    $dueDate = date("Y-m-d", strtotime("$inv_date +$pterm day"));
                } else {
                    $dueDate = date('Y-m-d', strtotime($inv_date));
                }

                $input_data['refNumber'] = $new_inv_no;
                $saveMessage = "Created";
            }

            $input_data['invoiceDate']    = $inv_date;
            $input_data['invoiceDueDate'] = $dueDate;

            $xeroSync   = new XeroSync($this->merchantID);
            $retrunData = $xeroSync->createUpdateInvoice($input_data, $invoiceID);

            $redirectURI = 'Integration/Invoices/invoice_list';
            if ($retrunData) {
                if (isset($input_data['inv_save'])) {
                    $redirectURI = "Integration/Invoices/invoice_details/$invoiceID";
                }
                $this->session->set_flashdata('success', "Invoice $saveMessage Successfully");

                if (empty($invoiceID)) {
                    $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));
                }
            }

            redirect(base_url($redirectURI));
        }

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->syncTaxRates();
        $xeroSync->sycnCustomer();
        $xeroSync->syncItems();

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $condition           = array('merchantID' => $user_id);
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['frequencies'] = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
        $data['durations']   = $this->general_model->get_table_data('tbl_subscription_plan', '');

        $whereMerch = [
            'merchantID' => $user_id,
        ];
        $data['plans']     = $this->data_model->get_product_data($user_id);
        $compdata          = $this->customer_model->get_customer_data($whereMerch);
        $data['customers'] = $compdata;

        if ($this->uri->segment(4) != "") {
            $customerID          = $this->uri->segment(4);
            $data['Invcustomer'] = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $customerID));
        }

        $data['netterms'] = $this->general_model->get_table_data('tbl_payment_terms', array('merchantID' => $user_id), array('name' => 'pt_netTerm', 'type' => 'asc'));

        $taxWhere              = $whereMerch;
        $taxWhere['taxRate >'] = 0;
        $taxname               = $this->data_model->get_tax_data($taxWhere);
        $data['taxes']         = $taxname;

        $country               = $this->general_model->get_table_data('country', '');
        $data['country_datas'] = $country;

        $data['selectedID'] = $this->general_model->getTermsSelectedID($user_id);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/create_new_invoice', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function invoice_details()
    {
        $invoiceID           = $this->uri->segment(4);
        $condition1          = array('item_ListID' => $invoiceID);
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->syncInvoices(['InvoiceID' => $invoiceID]);
        $xeroSync->syncAccounts(['EnablePaymentsToAccount' => 1]);

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['gateway_datas']         = $this->general_model->get_gateway_data($user_id);
        $data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
        $data['isCardGatewayExists']   = $data['gateway_datas']['isCardExists'];
        $merchant_condition            = [
            'merchID' => $user_id,
        ];
        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway                = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway']        = $defaultGateway[0];
            $data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
            $data['isCardGatewayExists']   = $data['defaultGateway']['creditCard'];
        }
        $data['page_num'] = 'invoice_details';
        $conditiong       = array('merchantID' => $user_id);

        $condition2    = array('merchantID' => $user_id, 'invoiceID' => $invoiceID);
        $invoice_data  = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
        $data['plans'] = $this->data_model->get_product_data($user_id);
        $code          = '';
        $link_data     = $this->general_model->get_row_data('tbl_template_data', $condition2);

        $data['from_mail']       = DEFAULT_FROM_EMAIL;
        $data['mailDisplayName'] = $this->logged_in_data['companyName'];

        $coditionp = array('merchantID' => $user_id, 'customerPortal' => '1');

        $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);

        $data['ur_data'] = $ur_data;
        $position1       = $position       = 0;
        $purl            = '';
        if(isset($ur_data['customerPortalURL']) && !empty($ur_data['customerPortalURL'])){
            $ttt = explode(PLINK, $ur_data['customerPortalURL']);
            $purl = $ttt[0] . PLINK . '/customer/';
        }
        if (!isset($invoice_data['TaxListID'])) {
            $invoice_data['TaxListID'] = 0;
        }

        $invoiceID       = $invoice_data['invoiceID'];
        $str             = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
        $shuffled        = str_shuffle($str);
        $shuffled        = substr($shuffled, 1, 12) . '=' . $user_id;
        $code            = $this->general_model->safe_encode($shuffled);
        $invcode         = $this->general_model->safe_encode($invoiceID);
        $data['paylink'] = $purl . 'update_payment/' . $code . '/' . $invcode;

        $condition3    = array('Customer_ListID' => $invoice_data['CustomerListID'], 'merchantID' => $user_id);
        $customer_data = $this->general_model->get_row_data('Xero_custom_customer', $condition3);

        $data['notes']         = $this->customer_model->get_customer_note_data($invoice_data['CustomerListID'], $user_id);
        $data['customer_data'] = $customer_data;
        $data['invoice_data']  = $invoice_data;

        $taxname       = $this->data_model->get_tax_data($conditiong);
        if($taxname){
            foreach($taxname as $key => $tax){
                $taxname[$key]['friendlyName'] = $tax['friendlyName'].' ('.$tax['displayTaxRate'].'%)';
            }
        }
        $data['taxes'] = $taxname;
        $company_ID    = $this->db->select('id')->from('tbl_company')->where('merchantID', $user_id)->get();
        $cID           = '';
        if ($company_ID->num_rows() > 0) {
            $company_IDs = $company_ID->row_array();
            $cID         = $company_IDs['id'];
        }

        $data['invoice_items'] = $this->data_model->get_invoice_details_item_data($invoiceID, $user_id);

        $con                 = array('invoiceID' => $invoiceID);
        $data['transaction'] = $this->general_model->get_row_data('customer_transaction', $con);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);

        $this->load->view('comman-pages/page_invoice_details', $data);
        $this->load->view('comman-pages/popup_box', $data);
        $this->load->view('pages/page_popup_modals', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function invoice_details_print()
	{
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $invoiceID             =  $this->uri->segment(4);
        $condition2    = array('merchantID' => $user_id, 'invoiceID' => $invoiceID);
		$invoice_data  = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
        $item_data = $this->data_model->get_invoice_details_item_data($invoiceID, $user_id);

        $condition3    = array('Customer_ListID' => $invoice_data['CustomerListID'], 'merchantID' => $user_id);
        $customer_data = $this->general_model->get_row_data('Xero_custom_customer', $condition3);
		
		$this->general_model->generate_invoice_pdf_xero($user_id, $customer_data, $invoice_data, $item_data, 'D');
	}
    
    public function voidInvoice(){
        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
        $xeroSync   = new XeroSync($this->merchantID);
        $retrunData = $xeroSync->voidInvoice($invoiceID);
		if ($retrunData) {
			$this->session->set_flashdata('success', 'Invoice cancelled successfully.');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Error in process.</div>');
        }
		redirect('Integration/Invoices/invoice_list', 'refresh');
	}
}
