<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require APPPATH . 'libraries/integration/XeroSync.php';

class Items extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');

        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function item_list()
    {

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->syncItems();

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }

        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_item_list', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function plan_product_ajax()
    {
        $merchID = $this->merchantID;
        $cond    = array("merchantID" => $merchID);
        $resultItems   = $this->data_model->get_product_list($merchID);
        $data    = array();
        $no      = $_POST['start'];

        $count = $resultItems['count'];
        $plans = $resultItems['products'];
        if (isset($plans) && $plans) {
            foreach ($plans as $plan) {
                $no++;
                $row   = array();
                $url   = base_url('Integration/Items/create_product/' . $plan['productID']);
                $row[] = "<div class='text-left cust_view'><a href='" . $url . "' data-toggle='tooltip' title='Edit' >" . $plan['Name'] . "</a>";
                $row[] = "<div class='text-left hidden-xs'>" . $plan['SalesDescription'] . "";
                $row[] = "<div class='text-left'>" . ($plan['QuantityOnHand']) ? $plan['QuantityOnHand'] : '0' . "";
                $row[] = "<div class='hidden-xs text-right'>$" . number_format($plan['saleCost'], 2) . "";

                if ($plan['IsActive'] == 'true') {
                    $row[] = "<div class='hidden-xs text-center'><div class='btn-group btn-group-xs'><a href='#deactive_product' class='btn btn-sm btn-danger' onclick='del_product_id(" . $plan['productID'] . ");'  data-backdrop='static' data-keyboard='false'  title='delete' data-toggle='modal'><i class='fa fa-ban'></i></a>";
                } else if ($plan['IsActive'] == 'false') {
                    $row[] = "<div class='hidden-xs text-center'><div class='btn-group btn-group-xs'><a href='#active_product' onclick='set_productList_active_id(" . $plan['productID'] . ",'1');' data-backdrop='static' data-keyboard='false' data-toggle='modal'  title='Activate Product'  class='btn btn-sm btn-info'><i class='fa fa-check'></i></a>";
                } else {
                    $row[] = '';
                }
                $data[] = $row;
            }
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $count,
            "recordsFiltered" => $count,
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
        die;
    }

    public function create_product()
    {

        $user_id = $this->merchantID;

        if (!empty($this->input->post())) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
            $this->form_validation->set_rules('productName', 'Product Name', 'required|xss_clean');
            $this->form_validation->set_rules('productCode', 'Product Code', 'required|xss_clean');

            if ($this->form_validation->run() == true) {

                $input_data = $this->czsecurity->xssCleanPostInput();
                $xeroSync   = new XeroSync($this->merchantID);
                $retrunData = $xeroSync->createUpdateItem($input_data, $input_data['productID']);

                if ($retrunData) {
                    $message = 'Updated';
                    if ($input_data['productID'] == '') {
                        $message = 'Created';
                    }
                    $this->session->set_flashdata('success', "Item Successfully $message");
                }
                redirect(base_url('Integration/Items/item_list'));
            }
        }

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->syncAccounts();

        if ($this->uri->segment('4')) {
            $productID           = $this->uri->segment('4');
            $con              = array('productID' => $productID, 'merchantID' => $user_id);
            $data['item_pro'] = $this->general_model->get_row_data('Xero_test_item', $con);
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $company          = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
        $data['companys'] = $company;

        $parent           = []; // $this->company_model->get_product_data(array('merchantID' => $user_id));
        $data['products'] = $parent;

        $account = $this->general_model->get_table_data('tbl_xero_accounts', ['merchantID' => $user_id]);
        // print_r($account);die;
        $data['accounts'] = $account;
        $data['items']    = []; // $this->company_model->get_plan_data_item($user_id);

        // echo '<pre>';
        // print_r([$data['item_pro'], $data['accounts']]);die;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/create_product', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function is_product_exists()
    {
        $merchID = $this->merchantID;
        $productCode = $this->czsecurity->xssCleanPostInput('productCode');
        $productID = $this->czsecurity->xssCleanPostInput('productID');
        
        $cond    = array("merchantID" => $merchID, 'Code' => $productCode);
        if($productID != ''){
            $cond['productID !='] = $productID;
        }

        $resultItems   = $this->data_model->get_product_data($merchID, $cond);

        $status = true;
        if($resultItems){
            $status = false;
        }
        
        echo json_encode(['status' => $status]);die;
    }

    public function get_product_data(){

        $taxWhere['merchantID'] = $this->merchantID;
        $taxList               = $this->data_model->get_tax_data($taxWhere);
        if($taxList){
            foreach($taxList as $key => $tax){
                $taxList[$key]['friendlyName'] = $tax['friendlyName'].' ('.$tax['displayTaxRate'].'%)';
            }
        }

        $data['tax_list'] = $taxList;
        $data['product_list'] = $this->data_model->get_plan_data_invoice($this->merchantID);
		$data = json_encode($data);
		echo   $data = str_replace("'", "", $data);

		die;
    }

    public function get_item_data()
	{

		$data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

		if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemID = $this->czsecurity->xssCleanPostInput('itemID');
			$itemdata = $this->general_model->get_row_data('Xero_test_item', array('productID' => $itemID));
			$itemdata['SalesPrice'] =  number_format($itemdata['saleCost'], 2, '.', '');
			echo json_encode($itemdata);
			die;
		}
		return false;
	}
}
