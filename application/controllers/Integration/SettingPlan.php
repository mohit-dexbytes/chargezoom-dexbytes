<?php

require APPPATH . 'libraries/integration/XeroSync.php';

class SettingPlan extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');

        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function index()
    {
        redirect('login', 'refresh');
    }

    public function plans()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->logged_in_data;
        $user_id             = $this->merchantID;

        $data['merchantID'] = $user_id;

        $condition  = array('merchantID' => $user_id);
        $condition1 = array('merchantDataID' => $user_id);

        $data['plandata'] = $this->general_model->get_table_data('tbl_subscriptions_plan_xero', $condition1);
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_plans', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_plan()
    {
        //Show a form here which collects someone's name and e-mail address
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $base_id   = base64_encode($user_id);
        $coditionp = array('merchantID' => $user_id, 'customerPortal' => '1');

        $ur_data = $this->general_model->get_row_data('tbl_config_setting', $coditionp);

        if (empty($ur_data)) {
            $this->session->set_flashdata('error', '<strong>Errr: Please enable your customer portal</strong>');
            redirect('Integration/Settings/setting_customer_portal', 'refresh');
        }

        $ur_data         = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);
        $data['ur_data'] = $ur_data;
        $position1       = $position       = 0;
        $purl            = '';

        $ttt = explode(PLINK, $ur_data['customerPortalURL']);

        $purl = $ttt[0] . PLINK . '/customer/' . COMMON_PLAN_CHECKOUT_METHOD . '/' . $base_id . '/';

        $data['dpurl'] = $purl;

        $inv_pre_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
        if (empty($inv_pre_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Errr: Please set your invoice prefix to create invoice</strong></div>');
            redirect('Integration/Settings/profile_setting', 'refresh');
        }

        if (!empty($this->input->post())) {

            $total = 0;
            foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
                //print_r($prod);
                $insert_row['itemListID']      = $this->czsecurity->xssCleanPostInput('productID')[$key];
                $insert_row['itemQuantity']    = $this->czsecurity->xssCleanPostInput('quantity')[$key];
                $insert_row['itemRate']        = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                $insert_row['itemRate']        = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                $insert_row['itemTax']         = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
                $insert_row['itemFullName']    = $this->czsecurity->xssCleanPostInput('description')[$key];
                $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
                if ($this->czsecurity->xssCleanPostInput('onetime_charge')[$key] == 'One Time' || $this->czsecurity->xssCleanPostInput('onetime_charge')[$key] == '1') {
                    $insert_row['oneTimeCharge'] = '1';
                } else {
                    $insert_row['oneTimeCharge'] = '0';
                    $total                       = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
                }

                if (empty($insert_row['itemTax'])) {
                    $insert_row['itemTax'] = 0;
                } else {
                    $insert_row['itemTax'] = 1;
                }

                $itemdata = $this->general_model->get_row_data('Xero_test_item', array('productID' => $insert_row['itemListID'], 'merchantID' => $user_id));
                if ($itemdata) {
                    $insert_row['itemFullName'] = $itemdata['Name'];
                }

                $item_val[$key] = $insert_row;
            }

            $pname = $this->czsecurity->xssCleanPostInput('plan_name');
            $plan  = $this->czsecurity->xssCleanPostInput('duration_list');
            if ($plan > 0) {
                $subsamount = $total / $plan;
            } else {
                $subsamount = $total;
            }
            $first_date   = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
            $invoice_date = date('d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
            $st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
            //$tot_invoice  = $this->czsecurity->xssCleanPostInput('total_invoice');
            $freetrial = $this->czsecurity->xssCleanPostInput('freetrial');
            $address1  = $this->czsecurity->xssCleanPostInput('address1');

            $paygateway = $this->czsecurity->xssCleanPostInput('gateway_list');
            $address2   = $this->czsecurity->xssCleanPostInput('address2');
            $country    = $this->czsecurity->xssCleanPostInput('country');
            $state      = $this->czsecurity->xssCleanPostInput('state');
            $city       = $this->czsecurity->xssCleanPostInput('city');
            $zipcode    = $this->czsecurity->xssCleanPostInput('zipcode');
            $phone      = $this->czsecurity->xssCleanPostInput('phone');
            $paycycle   = $this->czsecurity->xssCleanPostInput('paycycle');

            $shoturl          = $this->czsecurity->xssCleanPostInput('short_url');
            $confirm_page_url = $this->czsecurity->xssCleanPostInput('confirm_page_url');
            $subdata          = array(
                'planName'           => $pname,
                'subscriptionPlan'   => $plan,
                'subscriptionAmount' => $total,
                'totalInvoice'       => $plan,
                'invoicefrequency'   => $paycycle,
                'freeTrial'          => $freetrial,
                'confirm_page_url'   => $confirm_page_url,
                'postPlanURL'        => $shoturl,
                'merchantPlanURL'    => $purl,
            );

            if ($paycycle == 'mon') {
                if ($this->czsecurity->xssCleanPostInput('pro_rate')) {
                    $subdata['proRate'] = '1';

                    $subdata['proRateBillingDay']    = $this->czsecurity->xssCleanPostInput('pro_billing_date');
                    $subdata['nextMonthInvoiceDate'] = $this->czsecurity->xssCleanPostInput('pro_next_billing_date');
                } else {
                    $subdata['proRate'] = '0';

                    $subdata['proRateBillingDay']    = 0;
                    $subdata['nextMonthInvoiceDate'] = 0;
                }
            }

            if ($this->czsecurity->xssCleanPostInput('autopay')) {
                $subdata['paymentGateway']   = (empty($paygateway)) ? 0 : $paygateway;
                $subdata['automaticPayment'] = '1';
            } else {
                $subdata['automaticPayment'] = '0';
                $subdata['paymentGateway']   = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('email_recurring')) {

                $subdata['emailRecurring'] = '1';
            } else {
                $subdata['emailRecurring'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('billinfo')) {

                $subdata['usingExistingAddress'] = '1';
            } else {
                $subdata['usingExistingAddress'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('require_shipping')) {
                $subdata['isShipping'] = '1';
            } else {
                $subdata['isShipping'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('require_billing')) {
                $subdata['isBilling'] = '1';
            } else {
                $subdata['isBilling'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('require_service')) {
                $subdata['isService'] = '1';
            } else {
                $subdata['isService'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('plan_url') != '') {
                $plan_url  = $this->czsecurity->xssCleanPostInput('plan_url');
                $coditionp = array('merchantID' => $user_id, 'customerPortal' => '1');
                $base_id   = base64_encode($user_id);

                $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);
                if (!empty($ur_data)) {
                    $subdata['merchantPlanURL'] = $purl . $plan_url;
                    $subdata['postPlanURL']     = $plan_url;
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong> Please create merchant portal URL</div>');
                    redirect('Integration/Integration/SettingPlan/plans', 'refresh');
                }
            }

            $isCC                  = ($this->czsecurity->xssCleanPostInput('cc') != null) ? $this->czsecurity->xssCleanPostInput('cc') : 0;
            $isEC                  = ($this->czsecurity->xssCleanPostInput('ec') != null) ? $this->czsecurity->xssCleanPostInput('ec') : 0;
            $subdata['creditCard'] = $isCC;
            $subdata['eCheck']     = $isEC;

            if ($this->czsecurity->xssCleanPostInput('planID') != "") {
                $rowID                = $this->czsecurity->xssCleanPostInput('planID');
                $subdata['updatedAt'] = date('Y-m-d H:i:s');

                $subs = $this->general_model->get_row_data('tbl_subscriptions_plan_xero', array('planID' => $rowID));

                $ins_data = $this->general_model->update_row_data('tbl_subscriptions_plan_xero', array('planID' => $rowID), $subdata);
                $this->general_model->delete_row_data('tbl_subscription_plan_item_xero', array('planID' => $rowID));

                foreach ($item_val as $k => $item) {
                    $item['planID'] = $rowID;
                    $ins            = $this->general_model->insert_row('tbl_subscription_plan_item_xero', $item);
                }
            } else {
                $subdata['merchantDataID'] = $user_id;
                if ($this->czsecurity->xssCleanPostInput('short_url') != '') {
                    $plan_url  = $this->czsecurity->xssCleanPostInput('short_url');
                    $coditionp = array('merchantID' => $user_id, 'customerPortal' => '1');
                    $base_id   = base64_encode($user_id);

                    $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);
                    if (!empty($ur_data)) {
                        $subdata['merchantPlanURL'] = $purl . $plan_url;
                        $subdata['postPlanURL']     = $plan_url;
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong> Please create merchant portak URL</div>');
                        redirect('Integration/Integration/SettingPlan/plans', 'refresh');
                    }
                }

                $subdata['createdAt'] = date('Y-m-d H:i:s');
                $rowID                = $ins_data                = $this->general_model->insert_row('tbl_subscriptions_plan_xero', $subdata);
                foreach ($item_val as $k => $item) {
                    $item['planID'] = $ins_data;
                    $ins            = $this->general_model->insert_row('tbl_subscription_plan_item_xero', $item);
                }
            }
            if ($ins_data && $ins) {
                $this->session->set_flashdata('success', 'Successfully Created Plan');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Something went wrong.</strong></div>');
            }

            redirect('Integration/SettingPlan/create_plan/' . $rowID, 'refresh');
        }

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->syncItems();

        if ($this->uri->segment('4') != "") {
            $sbID          = $this->uri->segment('4');
            $data['subs']  = $this->general_model->get_row_data('tbl_subscriptions_plan_xero', array('planID' => $sbID));
            $data['items'] = $this->general_model->get_table_data('tbl_subscription_plan_item_xero', array('planID' => $sbID));
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['base_id']    = base64_encode($user_id);
        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['isCreditCard']   = 0;
        $data['isEcheck']       = 0;
        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        } elseif (isset($data['subs']['paymentGateway'])) {
            $conditionGW    = array('gatewayID' => $data['subs']['paymentGateway']);
            $gatewayGetByID = $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
            if ($gatewayGetByID) {
                $data['isCreditCard'] = $gatewayGetByID['creditCard'];
                $data['isEcheck']     = $gatewayGetByID['echeckStatus'];
            }
        }

        $condition           = array('merchantID' => $user_id);
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['frequencies'] = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
        $data['durations']   = $this->general_model->get_table_data('tbl_subscription_plan', '');
        $plans               = $this->data_model->get_product_data($user_id);

        if (!empty($plans)) {
            foreach ($plans as $key => $plan) {
                if (empty($item['FullName'])) {
                    $plan['FullName'] = $plan['Name'];
                    $plans[$key]      = $plan;
                }
            }
        }

        $data['plans']     = $plans;
        $compdata          = $this->customer_model->get_customer_data(['merchantID' => $user_id]);
        $data['customers'] = $compdata;

        $taxname       = $this->data_model->get_tax_data(['merchantID' => $user_id]);
        $data['taxes'] = $taxname;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/create_plan', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function delete_plan()
    {

        $plancID   = $this->czsecurity->xssCleanPostInput('plancID');
        $condition = array('planID' => $plancID);
        $this->general_model->delete_row_data('tbl_subscription_plan_item_xero', $condition);

        $del = $this->general_model->delete_row_data('tbl_subscriptions_plan_xero', $condition);
        if ($del) {

            $this->session->set_flashdata('success', 'Successfully Deleted Plan');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }

        redirect('Integration/SettingPlan/plans', 'refresh');
    }

    public function get_subs_item_count_data()
    {
        $sbID           = $this->czsecurity->xssCleanPostInput('subID');
        $data1['items'] = $this->general_model->get_table_data('tbl_subscription_plan_item_xero', array('planID' => $sbID));
        $data1['rows']  = $this->general_model->get_num_rows('tbl_subscription_plan_item_xero', array('planID' => $sbID));
        echo json_encode($data1);
        die;
    }

    public function recurring_payment()
    {

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        if (!empty($this->input->post())) {
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $editrecID  = '';
            $recpayTerm = '';
            $editrecID  = $this->czsecurity->xssCleanPostInput('editrecID');

            $customerID = $this->czsecurity->xssCleanPostInput('recCustomer');
            $cardID     = $this->czsecurity->xssCleanPostInput('rec_cardID');
            $payterm    = '';
            $gateway    = '0';
            $gt_data    = $this->general_model->get_select_data('tbl_merchant_gateway', array('gatewayID'), array('merchantID' => $user_id, 'set_as_Default' => 1));
            if (!empty($gt_data)) {
                $gateway = $gt_data['gatewayID'];
            }

            $recurAuto = $this->czsecurity->xssCleanPostInput('recurAuto');
            if (!empty($this->input->post('rec_payTem'))) {
                $payterm = implode(',', $this->czsecurity->xssCleanPostInput('rec_payTem'));
            }

            $recurAuto = $this->czsecurity->xssCleanPostInput('recurAuto');

            $amount = sprintf('%0.2f', $this->czsecurity->xssCleanPostInput('recAmount'));
            $card   = array('cardID' => $cardID, 'amount' => $amount, 'gateway' => $gateway, 'customerID' => $customerID, 'merchantID' => $user_id, 'optionData' => $recurAuto, 'paymentTerm' => $payterm);

            $card['recurring_send_mail'] = $chh_mail;
            if ($recurAuto == 4) {
                $card['month_day'] = $this->czsecurity->xssCleanPostInput('calendar_date_day');
            }

            $conCustomer = array('merchantID' => $this->merchantID, 'Customer_ListID' => $customerID);
            $comp_data   = $this->general_model->get_row_data('Xero_custom_customer', $conCustomer);

            if ($chh_mail == '1') {
                $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                $ref_number     = '';
                $tr_date        = date('Y-m-d H:i:s');
                $toEmail        = $comp_data['userEmail'];
                $company        = $comp_data['fullName'];
                $customer       = $comp_data['fullName'];
                $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
            }

            if ($editrecID != "") {
                $card['updatedAt'] = date('Y-m-d H:i:s');
                $con               = array('recurrID' => $editrecID);
                $this->general_model->update_row_data('tbl_recurring_payment', $con, $card);
            } else {
                $card['createdAt'] = date('Y-m-d H:i:s');
                $card['updatedAt'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('tbl_recurring_payment', $card);
            }

            redirect(base_url() . 'Integration/Customers/customer_detail/' . $customerID, 'refresh');
        }
    }

    public function delete_recurring()
    {
        $merchID = $this->merchantID;
        if ($this->uri->segment('4') != "") {

            $recurringID = $this->uri->segment('4');
            $qq          = $this->general_model->get_select_data('tbl_recurring_payment', array('customerID'), array('recurrID' => $recurringID, 'merchantID' => $merchID));
            if (!empty($qq)) {
                $customer = $qq['customerID'];
            }

            $sts = $this->general_model->delete_row_data('tbl_recurring_payment', array('recurrID' => $recurringID, 'merchantID' => $merchID));
            if ($sts) {
                $this->session->set_flashdata('success', ' Successfully Deleted');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }

            redirect('Integration/Customers/customer_detail/' . $customer, 'refresh');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }
        redirect('Integration/home/index', 'refresh');
    }    
}
