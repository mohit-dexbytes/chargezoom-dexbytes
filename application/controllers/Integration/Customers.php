<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require APPPATH . 'libraries/integration/XeroSync.php';

class Customers extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function customer_list_all()
    {
        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->sycnCustomer();

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_customer_list_all', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function customer_list_ajax()
    {
        $in_data['merchantID'] = $this->merchantID;
        $active_app            = $this->logged_in_data['active_app'];
        $merchantEmailID       = $this->logged_in_data['merchantEmail'];

        $count     = $no     = 0;
        $customers = $data = [];
        $condition = array('merchantID' => $this->merchantID);
        if ($active_app == 4) {
            $customers = $this->customer_model->get_all_customer_details($this->merchantID);
            $count = $this->customer_model->get_all_customer_count($this->merchantID);
        }

        $showEmail = true;
        if (!isset($this->logged_in_data['authName']) || !in_array('Send Email', $this->logged_in_data['authName'])) {
            $showEmail = false;
        }

        if (!empty($customers['result'])) {
            foreach ($customers['result'] as $customer) {
                $customerEmailId        = $customer['userEmail'];
                $customerEmailName      = $customer['fullName'];
                $customerEmail          = $customer['userEmail'];
                $customerEmailCompanyID = $customer['companyID'];

                $emailAnchor = "<div class='text-left'>$customerEmail";
                if ($showEmail) {
                    $emailAnchor = "<div class='text-left cust_view'><a href='#set_tempemail' onclick=\"set_template_data_qbd('$customerEmailId','$customerEmailName', '$customerEmailCompanyID','$customerEmail','$merchantEmailID')\" title='' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-original-title=''>$customerEmail</a>";
                }
                $no++;
                $row = array();
                $url = base_url('Integration/Customers/customer_detail/' . $customer['Customer_ListID']);

                $name  = '<a href=' . $url . '>' . $customer['fullName'] . '</a>';
                $row[] = "<div class='text-left cust_view'>$name";
                $row[] = "<div class='text-left'>" . $customer['firstName'] . ' ' . $customer['lastName'] . "";
                $row[] = $emailAnchor;
                $balance = (isset($customer['Balance']) && $customer['Balance']) ? number_format($customer['Balance'], 2) : '0.00';
                $row[]   = "<div class='text-right'>" . $balance . "</div>";
                $data[]  = $row;
            }
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $count,
            "recordsFiltered" => $count,
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
        die;
    }

    /********************** Function for: Create Customer ********************/

    public function create_customer()
    {
        $user_id = $this->merchantID;

        if (!empty($this->input->post())) {

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
            $this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');

            if ($this->form_validation->run() == true) {

                $Customer_ListID = $this->czsecurity->xssCleanPostInput('Customer_ListID');

                $insert['firstName']     = $firstName     = $this->czsecurity->xssCleanPostInput('firstName');
                $insert['lastName']      = $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
                $insert['fullName']      = $fullName      = $this->czsecurity->xssCleanPostInput('fullName');
                $insert['userEmail']     = $userEmail     = $this->czsecurity->xssCleanPostInput('userEmail');
                $insert['Country']       = $country       = $this->czsecurity->xssCleanPostInput('Country');
                $insert['State']         = $state         = $this->czsecurity->xssCleanPostInput('State');
                $insert['City']          = $city          = $this->czsecurity->xssCleanPostInput('companyCity');
                $insert['phoneNumber']   = $phoneNumber   = $this->czsecurity->xssCleanPostInput('phoneNumber');
                $insert['address1']      = $address1      = $this->czsecurity->xssCleanPostInput('address1');
                $insert['address2']      = $address2      = $this->czsecurity->xssCleanPostInput('address2');
                $insert['zipcode']       = $zipCode       = $this->czsecurity->xssCleanPostInput('zipcode');
                $insert['ship_address1'] = $ship_address1 = $this->czsecurity->xssCleanPostInput('ship_address1');
                $insert['ship_address2'] = $ship_address2 = $this->czsecurity->xssCleanPostInput('ship_address2');
                $insert['ship_city']     = $ship_city     = $this->czsecurity->xssCleanPostInput('ship_city');
                $insert['ship_country']  = $ship_country  = $this->czsecurity->xssCleanPostInput('ship_country');
                $insert['ship_state']    = $ship_state    = $this->czsecurity->xssCleanPostInput('ship_state');
                $insert['ship_zipcode']  = $ship_zipcode  = $this->czsecurity->xssCleanPostInput('ship_zipcode');
                $insert['merchantID']    = $merchantID    = $user_id;
                $insert['createdat']     = $createdat     = date('Y-m-d H:i:s');

                $xeroSync   = new XeroSync($this->merchantID);
                $retrunData = $xeroSync->createUpdateContact($insert, $Customer_ListID);

                if ($retrunData) {
                    $message = 'Updated';
                    if ($Customer_ListID == '') {
                        $message = 'Created';
                    }
					$this->session->set_flashdata('success', "Successfully Contact $message");
                }
                redirect(base_url('Integration/Customers/customer_list_all'));
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Validation Error </strong></div>');
                redirect(base_url('Integration/Customers/customer_list_all'));
            }
        }

        if ($this->uri->segment('4')) {
            $customerListID = $this->uri->segment('4');

            $xeroSync = new XeroSync($this->merchantID);
            $xeroSync->sycnCustomerById($customerListID);

            $con              = array('merchantID' => $this->merchantID, 'Customer_ListID' => $customerListID);
            $data['customer'] = $this->general_model->get_row_data('Xero_custom_customer', $con);
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/create_customer', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /*********************** End **************************************/

    public function customer_detail($cusID)
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info']      = $this->logged_in_data;
        $merchantEmailID         = $this->logged_in_data['merchantEmail'];
        $data['merchantEmailID'] = $merchantEmailID;
        $user_id                 = $this->merchantID;

        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['gateway_datas']         = $this->general_model->get_gateway_data($user_id);
        $data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
        $data['isCardGatewayExists']   = $data['gateway_datas']['isCardExists'];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway                = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway']        = $defaultGateway[0];
            $data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
            $data['isCardGatewayExists']   = $data['defaultGateway']['creditCard'];
        }

        $data['merchID']  = $user_id;
        $data['customer'] = $this->customer_model->xero_customer_id($cusID, $user_id);
        if ($data['customer'] != '') {

            // Convert added date in timezone
            if (isset($data['customer']->createdAt) && isset($data['login_info']['merchant_default_timezone']) && !empty($data['login_info']['merchant_default_timezone'])) {
                $timezone                      = ['time' => $data['customer']->createdAt, 'current_format' => 'UTC', 'new_format' => $data['login_info']['merchant_default_timezone']];
                $data['customer']->createdAt = getTimeBySelectedTimezone($timezone);
            }

            $data['page_num'] = 'customer_common';
            $data['types']    = $this->general_model->get_table_data('tbl_teplate_type', '');
            $condition        = array('merchantID' => $user_id);

            $data['notes']    = $this->customer_model->get_customer_note_data($cusID, $user_id);

            $condition1 = array('merchantID' => $user_id, 'systemMail' => 0);

            $data['from_mail']       = DEFAULT_FROM_EMAIL;
            $data['mailDisplayName'] = $this->logged_in_data['companyName'];

            $data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);
            $data['card_data_array'] = $this->card_model->get_customer_card_data($cusID);

            $mail_con = array('merchantID' => $user_id, 'customerID' => $cusID);

            $data['editdatas'] = $this->general_model->get_email_history($mail_con);

            //--------------- To get  Subsriptions -----------------//
            $sub                              = array('customerID' => $cusID);
            $data['getsubscriptions']         = $this->general_model->get_subscriptions_data($sub);
            $plantype                         = $this->general_model->chk_merch_plantype_status($user_id);
            $data['plantype']                 = $plantype;
            $data['transaction_history_data'] = $this->customer_model->get_transaction_history_data($user_id, $cusID);

            $data['invoices']    = $this->data_model->get_invoice_list_count($this->merchantID, 'Open', $cusID);

            //    print_r($data);die;
            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);
            $this->load->view('comman-pages/page_customer_details', $data);
            $this->load->view('comman-pages/popup_box', $data);
            $this->load->view('template/page_footer', $data);

            $this->load->view('template/template_end', $data);
        } else {
            redirect('Integration/home/index', 'refresh');
        }
    }
}
