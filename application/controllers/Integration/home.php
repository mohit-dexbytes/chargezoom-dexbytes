<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require APPPATH . 'libraries/integration/XeroSync.php';

class Home extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Xero_models/xero_company_model');
        $this->load->model('Xero_models/xero_customer_model');

        $this->load->model('Common/company_model');
        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data          = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                 = $logged_in_data['merchantID'];
            $rs_Data                 = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID        = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function index()
    {
        if ($this->session->userdata('logged_in')) {
            $login_info = $this->session->userdata('logged_in');
            $user_id    = $login_info['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $login_info = $this->session->userdata('user_logged_in');
            $user_id    = $login_info['merchantID'];
        }

        $data = $this->xeroDashboardData($user_id);

        $plantype            = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']    = $plantype;
        $data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
        $data['plantype_vs'] = $this->general_model->chk_merch_plantype_vt($user_id);
        $data['plantype_vt'] = 0;

        $merchData               = $this->general_model->get_select_data('tbl_merchant_data', array('rhgraphOption', 'rhgraphFromDate', 'rhgraphToDate'), array('merchID' => $user_id));
        $data['rhgraphOption']   = isset($merchData['rhgraphOption']) ? $merchData['rhgraphOption'] : 0;
        $data['rhgraphFromDate'] = isset($merchData['rhgraphFromDate']) ? $merchData['rhgraphFromDate'] : '';
        $data['rhgraphToDate']   = isset($merchData['rhgraphToDate']) ? $merchData['rhgraphToDate'] : '';

        $data['login_info']  = $login_info;
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/index', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    private function xeroDashboardData($user_id)
    {
        $today                = date('Y-m-d');
        $condition2           = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "inv.merchantID" => $user_id);
        $condition            = array("inv.merchantID" => $user_id, 'cust.merchantID' => $user_id, 'trans.merchantID' => $user_id);
        $month                = date("M-Y");
        $data['recent_pay']   = $this->company_model->get_recent_volume_dashboard($user_id, $month);
        $data['card_payment'] = $this->company_model->get_creditcard_payment($user_id, $month);
        //$data['offline_payment'] = $this->company_model->get_offline_payment($user_id,$month);
        $data['eCheck_payment']    = $this->company_model->get_eCheck_payment($user_id, $month);
        $data['outstanding_total'] = $this->company_model->get_outstanding_payment($user_id);
        $data['recent_paid']       = $this->company_model->get_recent_transaction_data($user_id);
        $data['oldest_invs']       = $this->company_model->get_oldest_due($user_id);

        return $data;
    }

    public function dashboardReport()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $filterType = $this->czsecurity->xssCleanPostInput('revenue_filter');
        /*Update filter*/
        $endDate = $startDate = date('Y-m-d');
        if ($this->czsecurity->xssCleanPostInput('endDate', true) != null) {
            $endDate = $this->czsecurity->xssCleanPostInput('endDate', true);
        }
        if ($this->czsecurity->xssCleanPostInput('startDate', true) != null) {
            $startDate = $this->czsecurity->xssCleanPostInput('startDate', true);
        }
        $update_array   = array('rhgraphOption' => $filterType, 'rhgraphToDate' => $endDate, 'rhgraphFromDate' => $startDate);
        $conditionMerch = array('merchID' => $user_id);

        $update = $this->general_model->update_row_data('tbl_merchant_data', $conditionMerch, $update_array);

        if ($filterType == 0) {
            $opt_array = $this->getAnnualRevenue($user_id);
            echo json_encode($opt_array);
        } else if ($filterType == 1) {
            $startDate = date('Y-m-d', strtotime('today - 30 days'));
            $endDate   = date('Y-m-d');
            $opt_array = $this->general_model->getDayRevenue($user_id, $startDate, $endDate, 1);
            echo json_encode($opt_array);
        } else if ($filterType == 2) {
            $startDate = date('Y-m-01');
            $endDate   = date('Y-m-d');
            $opt_array = $this->general_model->getDayRevenue($user_id, $startDate, $endDate, 2);
            echo json_encode($opt_array);
        } else if ($filterType == 3) {
            $opt_array = $this->general_model->getHourlyRevenue($user_id);
            echo json_encode($opt_array);
        } else if ($filterType == 4) {
            $opt_array = $this->general_model->getDayRevenue($user_id, $startDate, $endDate, 2);
            echo json_encode($opt_array);
        }

    }

    //------------- Merchant gateway START ------------//

    public function gateway()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $condition = array('merchantID' => $user_id);

        $data['all_gateway'] = $this->general_model->get_table_data('tbl_master_gateway', '');
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_merchant', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    //---------------- To add gateway  ---------------//

    public function create_gateway()
    {
        $this->load->library('Gateway');
        $cr_status  = 1;
        $ach_status = 0;

        $signature = '';

        if (!empty($this->input->post())) {

            $user_id = $this->merchantID;

            $condition = array('merchantID' => $user_id);

            $gt_status = $isSurcharge = $surchargePercentage = 0;
            $gt_obj    = new Gateway();
            $gatetype  = $this->czsecurity->xssCleanPostInput('gateway_opt');
            $gmID      = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
            if ($gatetype == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
                $gt_status   = $gt_obj->chk_nmi_gateway_auth($nmi_data);
                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '2') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);
                $gt_status   = $gt_obj->chk_auth_gateway_auth($auth_data);
                if ($this->czsecurity->xssCleanPostInput('auth_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '3') {
                $this->load->config('paytrace');

                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);
                $signature   = PAYTRACE_INTEGRATOR_ID; // $this->czsecurity->xssCleanPostInput('paytraceIntegratorId');

                $gt_status = $gt_obj->chk_paytrace_gateway_auth($auth_data);
                if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword, 'signature' => $signature);

                $gt_status = $gt_obj->chk_paypal_gateway_auth($auth_data);
            } else if ($gatetype == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_stripe_gateway_auth($auth_data);
            } else if ($gatetype == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                // $gt_status          = $gt_obj->chk_usaePay_gateway_auth($auth_data);

            } else if ($gatetype == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
                $auth_data = array('user' => $nmiuser, 'password' => $nmipassword);
                //     $gt_status          = $gt_obj->chk_heartland_gateway_auth($auth_data);

            } else if ($gatetype == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey');
            } else if ($gatetype == '9') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword');
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
                $gt_status   = $gt_obj->chk_nmi_gateway_auth($nmi_data);
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }
                if ($this->czsecurity->xssCleanPostInput('cz_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '10') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('iTransactUsername');
                $nmipassword = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
                if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')) {
                    $isSurcharge = 1;
                } else {
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
            } else if ($gatetype == '11') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('fluidUser');
                $nmipassword = '';
                if ($this->czsecurity->xssCleanPostInput('fluid_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('fluid_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '13') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('basysUser');
                $nmipassword = '';
                if ($this->czsecurity->xssCleanPostInput('basys_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('basys_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '12') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('tsysUserID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('tsysPassword');
                $gmID        = $this->czsecurity->xssCleanPostInput('tsysMerchID');

                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '15') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('payarcUser');
                $nmipassword = '';
                $cr_status   = 1;
                $ach_status  = 0;
            } else if ($gatetype == '14') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '16') {
                $nmiuser     = $this->input->post('EPXCustNBR');
                $nmipassword = $this->input->post('EPXMerchNBR');
                $signature   = $this->input->post('EPXDBANBR');
                $extra1      = $this->input->post('EPXterminal');
                if ($this->input->post('EPX_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->input->post('EPX_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            }

            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');
            $frname   = $this->czsecurity->xssCleanPostInput('frname');

            $insert_data = array(
                'gatewayUsername'     => $nmiuser,
                'gatewayPassword'     => $nmipassword,
                'gatewayMerchantID'   => $gmID,
                'gatewaySignature'    => $signature,
                'gatewayType'         => $gatetype,
                'merchantID'          => $user_id,
                'gatewayFriendlyName' => $frname,
                'creditCard'          => $cr_status,
                'echeckStatus'        => $ach_status,
                'isSurcharge'         => $isSurcharge,
                'surchargePercentage' => $surchargePercentage,
            );

            if ($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {

                $val1 = array(
                    'merchantID' => $user_id,
                );

                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);

                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

                $this->session->set_flashdata('success', 'Successfully Inserted');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }

            redirect(base_url('Integration/home/gateway'));
        }
    }

    //----------- TO update the gateway  --------------//

    public function update_gateway()
    {
        if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {
            $ach_status  = 0;
            $signature   = '';
            $cr_status   = 1;
            $isSurcharge = $surchargePercentage = 0;

            $id            = $this->czsecurity->xssCleanPostInput('gatewayEditID');
            $chk_condition = array('gatewayID' => $id);
            $gatetype      = $this->czsecurity->xssCleanPostInput('gateway');
            $gmID          = $this->czsecurity->xssCleanPostInput('mid');
            if ($gatetype == 'NMI') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword1');
                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if (strtolower($gatetype) == 'authorize.net') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey1');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status1')) {
                    $cr_status = 1; // $this->czsecurity->xssCleanPostInput('auth_cr_status1');
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Paytrace') {
                $this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword1');
                $signature   = PAYTRACE_INTEGRATOR_ID; // $this->czsecurity->xssCleanPostInput('paytraceIntegratorId1');

                if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Paypal') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword1');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature1');

            } else if ($gatetype == 'Stripe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword1');

            } else if ($gatetype == 'USAePay') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin1');

            } else if ($gatetype == 'Heartland') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecret1');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Cybersource') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey1');

            } else if ($gatetype == 'Chargezoom') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword1');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == iTransactGatewayName) {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('iTransactUsername1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY1');
                if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box1')) {
                    $isSurcharge = 1;
                } else {
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage1');
            } else if ($gatetype == FluidGatewayName) {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('fluidUser1');
                $nmipassword = '';
                if ($this->czsecurity->xssCleanPostInput('fluid_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('fluid_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == TSYSGatewayName) {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('tsysUserID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('tsysPassword1');
                $gmID        = $this->czsecurity->xssCleanPostInput('tsysMerchID1');
                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == BASYSGatewayName) {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('basysUser1');
                $nmipassword = '';
                if ($this->czsecurity->xssCleanPostInput('basys_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('basys_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'CardPointe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword1');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID1');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status1', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status1', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == PayArcGatewayName) {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('payarcUser1');
                $nmipassword = '';
                $cr_status   = 1;
                $ach_status  = 0;
            } else if ($gatetype == EPXGatewayName) {
                $nmiuser     = $this->input->post('EPXCustNBR1');
                $nmipassword = $this->input->post('EPXMerchNBR1');
                $signature   = $this->input->post('EPXDBANBR1');
                $extra1      = $this->input->post('EPXterminal1');
                if ($this->input->post('EPX_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->input->post('EPX_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            }

            $frname = $this->czsecurity->xssCleanPostInput('fname');

            $insert_data = array(
                'gatewayUsername'     => $nmiuser,
                'gatewayPassword'     => $nmipassword,
                'gatewayMerchantID'   => $gmID,
                'gatewayFriendlyName' => $frname,
                'gatewaySignature'    => $signature,
                'creditCard'          => $cr_status,
                'echeckStatus'        => $ach_status,
                'isSurcharge'         => $isSurcharge,
                'surchargePercentage' => $surchargePercentage,
            );

            if ($this->general_model->update_row_data('tbl_merchant_gateway', $chk_condition, $insert_data)) {
                $this->session->set_flashdata('success', 'Successfully Updated');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }

            redirect(base_url('Integration/home/gateway'));
        }
    }

    public function get_gatewayedit_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
        $val = array(
            'gatewayID' => $id,
        );

        $data            = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
        $data['gateway'] = getGatewayNames($data['gatewayType']);

        echo json_encode($data);
    }

    public function set_gateway_default()
    {

        if (!empty($this->input->post('gatewayid'))) {

            $merchID = $this->merchantID;

            $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
            $val = array(
                'gatewayID' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );
            $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
            $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
        }
        redirect(base_url('Integration/home/gateway'));
    }

    /**************Delete credit********************/

    public function delete_gateway()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
        $condition = array('gatewayID' => $gatewayID);
        $del       = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition);
        if ($del) {
            $this->session->set_flashdata('success', 'Successfully Deleted');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }

        redirect(base_url('Integration/home/gateway'));

    }

    public function general_volume()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $get_result = $this->xero_company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
            /*print_r($get_result);
            exit;*/
            $result_set    = array();
            $result_value1 = array();
            $result_value2 = array();

            $result_online_value = array();
            $result_online_month = array();

            /*$result_offline_value = array();
            $result_offline_month = array();*/

            $result_eCheck_value = array();
            $result_eCheck_month = array();

            foreach ($get_result['data'] as $count_merch) {

                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, (float) $count_merch['revenu_volume']);

                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);

                //array_push($result_offline_month, $count_merch['revenu_Month']);
                //array_push($result_offline_value, (float) $count_merch['offline_volume']);

                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);

            }
            // $opt_array['month'] =array_chunk($result_value, 2);
            //
            $opt_array['revenu_month']  = $result_value1;
            $opt_array['revenu_volume'] = $result_value2;

            // $opt_array['month'] =array_chunk($result_value, 2);
            //
            $opt_array['online_month']  = $result_online_month;
            $opt_array['online_volume'] = $result_online_value;

            $opt_array['totalRevenue'] = $get_result['totalRevenue'];

            /*$opt_array['offline_month'] =   $result_offline_month;
            $opt_array['offline_volume'] =   $result_offline_value;*/

            $opt_array['eCheck_month']  = $result_eCheck_month;
            $opt_array['eCheck_volume'] = $result_eCheck_value;
            echo json_encode($opt_array);

        }

    }

    public function get_invoice_due_company()
    {

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $condition     = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id);
        $result_value1 = array();
        $result_value2 = array();
        $get_result    = array();
        $get_result1   = $this->company_model->get_invoice_due_by_company($condition, 0);
        if ($get_result1) {
            foreach ($get_result1 as $k => $count_merch) {
                $res[$k][] = $count_merch['label'];
                $res[$k][] = (float) $count_merch['balance'];
            }
            $get_result = $res;

            echo json_encode($get_result);
            die;
        } else {

            echo json_encode($get_result);
            die;
        }

    }

    public function get_invoice_Past_due_company()
    {

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $condition   = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id);
        $get_result  = array();
        $get_result1 = $this->company_model->get_invoice_due_by_company($condition, 1);
        if ($get_result1) {

            foreach ($get_result1 as $k => $count_merch) {
                $res[$k][] = $count_merch['label'];
                $res[$k][] = (float) $count_merch['balance'];

            }
            $get_result = $res;

            echo json_encode($get_result);

        } else {
            echo json_encode($get_result);

        }
        die;
    }

    public function company()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $merchantID         = $this->merchantID;

        $val = array(
            'merchantID' => $merchantID,
        );

        $data1 = $this->general_model->get_row_data('QBO_token', $val);

        $xeroSync            = new XeroSync($this->merchantID);
        $data['isConnected'] = $xeroSync->isConnected();

        $condition         = array('merchantID' => $merchantID);
        $data['companies'] = $this->general_model->get_table_data('tbl_xero_token', $condition);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);

        $this->load->view('comman-pages/page_company', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function xero_log()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['alls'] = $this->general_model->get_table_data('tbl_xero_log', array('merchantID' => $user_id));

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_log', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_gateway_data()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('gatewayID');
        $condition = array('gatewayID' => $gatewayID);

        $res = $this->general_model->get_row_data('tbl_merchant_gateway', $condition);

        if (!empty($res)) {

            $res['status'] = 'true';
            echo json_encode($res);
        }

        die;

    }

    public function get_subs_item_count_data()
    {
        $sbID           = $this->czsecurity->xssCleanPostInput('subID');
        $data1['items'] = $this->general_model->get_table_data('tbl_subscription_invoice_item_xero', array('subscriptionID' => $sbID));
        $data1['rows']  = $this->general_model->get_num_rows('tbl_subscription_invoice_item_xero', array('subscriptionID' => $sbID));
        echo json_encode($data1);
        die;
    }

    public function get_product_data()
    {
        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $data      = $this->xero_customer_model->get_xero_item_data($merchantID);
        $data      = json_encode($data);
        echo $data = str_replace("'", "", $data);

        die;

    }

    public function get_item_test_data()
    {

        if ($this->session->userdata('logged_in') != "") {
            $user_id = $this->session->userdata('logged_in')['merchID'];

        } else if ($this->session->userdata('user_logged_in') != "") {
            $user_id = $this->session->userdata('logged_in')['merchantID'];

        }

        if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

            $itemID   = $this->czsecurity->xssCleanPostInput('itemID');
            $itemdata = $this->general_model->get_row_data('Xero_test_item', array('productID' => $itemID, 'merchantID' => $user_id));

            $data      = json_encode($itemdata);
            echo $data = str_replace("'", "", $data);

            die;
        }
        return false;
    }

    public function my_account()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id           = $data['login_info']['merchID'];
            $data['loginType'] = 1;
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];

            // redirect('Integration/home/index');
            $data['loginType'] = 2;
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $condition        = array('merchantID' => $user_id);
        $data['invoices'] = $this->general_model->get_table_data('tbl_merchant_billing_invoice', $condition);

        $plandata = $this->general_model->chk_merch_plantype_data($user_id);
        //print_r($data['login_info']);die();
        $resellerID = $data['login_info']['resellerID'];
        $planID     = $plandata->plan_id;
        $planname   = $this->general_model->chk_merch_planFriendlyName($resellerID, $planID);

        if (isset($planname) && !empty($planname)) {
            $data['planname'] = $planname;
        } else {
            $data['planname'] = $plandata->plan_name;
        }

        if ($plandata->cardID > 0 && $plandata->payOption > 0) {
            $carddata = $this->card_model->get_merch_card_data($plandata->cardID);
        } else {
            $carddata = [];
        }

        $data['plan'] = $plandata;

        $data['carddata'] = $carddata;

        $conditionMerch       = array('merchID' => $user_id);
        $data['merchantData'] = $this->general_model->get_row_data('tbl_merchant_data', $conditionMerch);

        $data['interface']  = 4;
        $data['merchantID'] = $user_id;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_my_account', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function update_card_data()
    {

        $success_msg = null;
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $resellerID = $this->czsecurity->xssCleanPostInput('resellerID');

        $merchantcardID = $this->czsecurity->xssCleanPostInput('cardID');

        $merchantID = $user_id;

        $condition = array('merchantListID' => $merchantID);

        $conditionMerch = array('merchID' => $merchantID);

        $billing_first_name = ($this->czsecurity->xssCleanPostInput('billing_first_name') != null) ? $this->czsecurity->xssCleanPostInput('billing_first_name') : null;

        $billing_last_name = ($this->czsecurity->xssCleanPostInput('billing_last_name') != null) ? $this->czsecurity->xssCleanPostInput('billing_last_name') : null;

        $billing_phone_number = ($this->czsecurity->xssCleanPostInput('billing_phone_number') != null) ? $this->czsecurity->xssCleanPostInput('billing_phone_number') : null;

        $billing_email = ($this->czsecurity->xssCleanPostInput('billing_email') != null) ? $this->czsecurity->xssCleanPostInput('billing_email') : null;

        $billing_address = ($this->czsecurity->xssCleanPostInput('billing_address') != null) ? $this->czsecurity->xssCleanPostInput('billing_address') : null;

        $billing_state = ($this->czsecurity->xssCleanPostInput('billing_state') != null) ? $this->czsecurity->xssCleanPostInput('billing_state') : null;

        $billing_city = ($this->czsecurity->xssCleanPostInput('billing_city') != null) ? $this->czsecurity->xssCleanPostInput('billing_city') : null;

        $billing_zipcode = ($this->czsecurity->xssCleanPostInput('billing_zipcode') != null) ? $this->czsecurity->xssCleanPostInput('billing_zipcode') : null;
        $statusInsert    = 0;
        /* check is_address_update condition 1 than only address update and 2 for all */
        if ($this->czsecurity->xssCleanPostInput('is_address_update') == 1) {
            $insert_array = array(
                "billing_first_name"   => $billing_first_name,
                "billing_last_name"    => $billing_last_name,
                "billing_phone_number" => $billing_phone_number,
                "billing_email"        => $billing_email,
                "Billing_Addr1"        => $billing_address,
                "Billing_Country"      => null,
                "Billing_State"        => $billing_state,
                "Billing_City"         => $billing_city,
                "Billing_Zipcode"      => $billing_zipcode,
            );
            if ($merchantcardID != "") {

                $id = $this->card_model->update_merchant_card_data($condition, $insert_array);

                $success_msg = 'Address Updated Successfully';

            }
        } else {
            /* Save credit card data */
            if ($this->czsecurity->xssCleanPostInput('payOption') == 1) {

                $card_no      = $this->czsecurity->xssCleanPostInput('card_number');
                $card_type    = $this->general_model->getcardType($card_no);
                $expmonth     = $this->czsecurity->xssCleanPostInput('expiry');
                $exyear       = $this->czsecurity->xssCleanPostInput('expiry_year');
                $cvv          = $this->czsecurity->xssCleanPostInput('cvv');
                $type         = 'Credit';
                $friendlyname = $card_type . ' - ' . substr($card_no, -4);
                $insert_array = array('CardMonth' => $expmonth,
                    'CardYear'                        => $exyear,
                    'resellerID'                      => $resellerID,
                    'merchantListID'                  => $merchantID,
                    'accountNumber'                   => null,
                    'routeNumber'                     => null,
                    'accountName'                     => null,
                    'accountType'                     => null,
                    'accountHolderType'               => null,
                    'secCodeEntryMethod'              => null,
                    'merchantFriendlyName'            => $friendlyname,
                    "billing_first_name"              => $billing_first_name,
                    "billing_last_name"               => $billing_last_name,
                    "billing_phone_number"            => $billing_phone_number,
                    "billing_email"                   => $billing_email,
                    "Billing_Addr1"                   => $billing_address,
                    "Billing_Country"                 => null,
                    "Billing_State"                   => $billing_state,
                    "Billing_City"                    => $billing_city,
                    "Billing_Zipcode"                 => $billing_zipcode,
                );
                if ($merchantcardID != "") {

                    $insert_array['MerchantCard'] = $this->card_model->encrypt($card_no);
                    $insert_array['CardCVV']      = ''; // $this->card_model->encrypt($cvv);
                    $insert_array['CardType']     = $card_type;
                    $insert_array['createdAt']    = date('Y-m-d H:i:s');
                    $id                           = $this->card_model->update_merchant_card_data($condition, $insert_array);

                    $success_msg = 'Credit Card Updated Successfully';

                } else {
                    //$card_type = $this->general_model->getcardType($card_no);
                    $insert_array['CardType']     = $card_type;
                    $insert_array['MerchantCard'] = $this->card_model->encrypt($card_no);
                    $insert_array['CardCVV']      = ''; // $this->card_model->encrypt($cvv);
                    $insert_array['createdAt']    = date('Y-m-d H:i:s');

                    $id             = $this->card_model->insert_merchant_card_data($insert_array);
                    $statusInsert   = 1;
                    $merchantcardID = $id;
                    $success_msg    = 'Credit Card Inserted Successfully';

                }

            } else if ($this->czsecurity->xssCleanPostInput('payOption') == 2) {
                /* Save checking card data */
                $acc_number       = $this->czsecurity->xssCleanPostInput('acc_number');
                $route_number     = $this->czsecurity->xssCleanPostInput('route_number');
                $acc_name         = $this->czsecurity->xssCleanPostInput('acc_name');
                $secCode          = $this->czsecurity->xssCleanPostInput('secCode');
                $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
                $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                $card_type        = 'Checking';
                $type             = 'Checking';
                $friendlyname     = $type . ' - ' . substr($acc_number, -4);
                $card_data        = array(
                    'CardType'             => $card_type,
                    'CardMonth'            => null,
                    'CardYear'             => null,
                    'MerchantCard'         => null,
                    'CardCVV'              => null,
                    'accountNumber'        => $acc_number,
                    'routeNumber'          => $route_number,
                    'accountName'          => $acc_name,
                    'accountType'          => $acct_type,
                    'accountHolderType'    => $acct_holder_type,
                    'secCodeEntryMethod'   => $secCode,
                    'merchantListID'       => $merchantID,
                    'resellerID'           => $resellerID,
                    'merchantFriendlyName' => $friendlyname,
                    'createdAt'            => date("Y-m-d H:i:s"),
                    "billing_first_name"   => $billing_first_name,
                    "billing_last_name"    => $billing_last_name,
                    "billing_phone_number" => $billing_phone_number,
                    "billing_email"        => $billing_email,
                    "Billing_Addr1"        => $billing_address,
                    "Billing_Country"      => null,
                    "Billing_State"        => $billing_state,
                    "Billing_City"         => $billing_city,
                    "Billing_Zipcode"      => $billing_zipcode,

                );
                if ($merchantcardID != "") {

                    $id = $this->card_model->update_merchant_card_data($condition, $card_data);

                    $success_msg = 'Checking Card Updated Successfully';
                } else {

                    $id             = $this->card_model->insert_merchant_card_data($card_data);
                    $merchantcardID = $id;
                    $statusInsert   = 1;
                    $success_msg    = 'Checking Card Inserted Successfully';
                }

            } else {
                /* do nothing*/
                $id = false;
            }
        }
        if ($statusInsert == 1) {
            $merchant_condition = ['merchID' => $merchantID];
            $merchantData       = $this->general_model->get_row_data('tbl_merchant_data', $merchant_condition);
            if (ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23) {
                /* Start campaign in hatchbuck CRM*/
                $this->load->library('hatchBuckAPI');

                $merchantData['merchant_type'] = 'Xero';

                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                if ($status['statusCode'] == 400) {
                    $resource = $this->hatchbuckapi->createContact($merchantData);
                    if ($resource['contactID'] != '0') {
                        $contact_id = $resource['contactID'];
                        $status     = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                    }
                }
                /* End campaign in hatchbuck CRM*/
            }
        }

        if ($id) {
            /* Update Pay option type in merchant table */
            $update_array = array('payOption' => $this->czsecurity->xssCleanPostInput('payOption'), 'cardID' => $merchantcardID);

            $update = $this->general_model->update_row_data('tbl_merchant_data', $conditionMerch, $update_array);

            $this->session->set_flashdata('success', $success_msg);
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
        }

        redirect('Integration/home/my_account/');
    }
    public function marchant_invoice()
    {

        if ($this->uri->segment(4) != "") {
            $data['primary_nav'] = primary_nav();
            $data['template']    = template_variable();
            //  $data['pagename']      = 'invoice_details';
            if ($this->session->userdata('logged_in')) {
                $data['login_info'] = $this->session->userdata('logged_in');

                $user_id = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info'] = $this->session->userdata('user_logged_in');

                $user_id = $data['login_info']['merchantID'];
            }
            $invoiceID = $this->uri->segment(4);

            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);

            //  echo $invoice_data['BalanceRemaining']; die;

            $this->load->view('Xero_views/marchant_invoice', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);
        } else {
            redirect(base_url('Integration/home/invoices'));
        }
    }

    public function level_three()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        } else {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        $data['level_three_master_data'] = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master']);
        $data['level_three_visa_data']   = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa']);
        $data['primary_nav']             = primary_nav();
        $data['template']                = template_variable();
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/level_three', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    // add or update visa details
    public function level_three_visa()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        } else {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        if ($this->czsecurity->xssCleanPostInput()) {
            $insert_data                              = [];
            $insert_data['customer_reference_id']     = $this->czsecurity->xssCleanPostInput('customer_reference_id');
            $insert_data['local_tax']                 = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax']              = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['merchant_tax_id']           = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['customer_tax_id']           = $this->czsecurity->xssCleanPostInput('customer_tax_id');
            $insert_data['commodity_code']            = $this->czsecurity->xssCleanPostInput('commodity_code');
            $insert_data['discount_rate']             = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['freight_amount']            = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['duty_amount']               = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['destination_zip']           = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['source_zip']                = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['destination_country']       = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['addtnl_tax_freight']        = $this->czsecurity->xssCleanPostInput('addtnl_tax_freight');
            $insert_data['addtnl_tax_rate']           = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['line_item_commodity_code']  = $this->czsecurity->xssCleanPostInput('line_item_commodity_code');
            $insert_data['description']               = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['product_code']              = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['unit_measure_code']         = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_amount']         = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['discount']                  = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['card_type']                 = 'visa';
            $insert_data['updated_date']              = date('Y-m-d H:i:s');
            $insert_data['merchant_id']               = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'visa']);
            if ($check_exist) {
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa'], $insert_data);

            } else {
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('Integration/home/level_three');
    }

    // add or update master details
    public function level_three_master_card()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        } else {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        if ($this->czsecurity->xssCleanPostInput()) {
            $insert_data                          = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');

            $insert_data['local_tax']                 = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax']              = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['freight_amount']            = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['destination_country']       = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['duty_amount']               = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['addtnl_tax_amount']         = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['destination_zip']           = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['addtnl_tax_indicator']      = $this->czsecurity->xssCleanPostInput('addtnl_tax_indicator');
            $insert_data['source_zip']                = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['description']               = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['debit_credit_indicator']    = $this->czsecurity->xssCleanPostInput('debit_credit_indicator');
            $insert_data['product_code']              = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['addtnl_tax_type']           = $this->czsecurity->xssCleanPostInput('addtnl_tax_type');
            $insert_data['discount']                  = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['unit_measure_code']         = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_rate']           = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['discount_rate']             = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['merchant_tax_id']           = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['net_gross_indicator']       = $this->czsecurity->xssCleanPostInput('net_gross_indicator');

            $insert_data['card_type']    = 'master';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id']  = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'master']);
            if ($check_exist) {
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master'], $insert_data);

            } else {
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('Integration/home/level_three');
    }

    public function check_vault()
    {

        $card         = '';
        $card_name    = '';
        $customerdata = array();
        $merchantID   = $this->merchantID;

        $gatewayID = $this->czsecurity->xssCleanPostInput('gatewayID');

        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {
            $invoiceIDs = [];
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('Xero_custom_customer', $condition);
            if (!empty($customerdata)) {
                $syncInvoice = $this->czsecurity->xssCleanPostInput('syncInvoice');
                if ($syncInvoice) {
                    $xeroSync = new XeroSync($this->merchantID);
                    $xeroSync->syncInvoices(['ContactID' => $customerID]);
                }
                //    $vaultID    = ($valul_data['vaultID'])?$valul_data['vaultID']:'';
                $customerdata['status'] = 'success';

                $ach_data = $this->card_model->get_ach_info_data($customerID);
                $ACH      = [];
                if (!empty($ach_data)) {
                    foreach ($ach_data as $card) {
                        $ACH[] = $card['CardID'];
                    }
                }
                $recentACH                          = end($ACH);
                $customerdata['ach_data']           = $ach_data;
                $customerdata['recent_ach_account'] = $recentACH;

                $conditionGW    = array('gatewayID' => $gatewayID);
                $gateway        = $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
                $cardTypeOption = 2;
                if (isset($gateway['gatewayID'])) {
                    if ($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1) {
                        $cardTypeOption = 1;
                    } else if ($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0) {
                        $cardTypeOption = 2;
                    } else if ($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1) {
                        $cardTypeOption = 3;
                    } else if ($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0) {
                        $cardTypeOption = 4;
                    } else {
                        $cardTypeOption = 2;
                    }

                }
                $customerdata['cardTypeOption'] = $cardTypeOption;
                $card_data                      = $this->card_model->getCardData($customerID, $cardTypeOption);
                //$card_data =   $this->card_model->get_card_expiry_data($customerID);
                $cardArr = [];
                if (!empty($card_data)) {
                    foreach ($card_data as $card) {
                        $cardArr[] = $card['CardID'];
                    }
                }
                $recentCard                  = end($cardArr);
                $customerdata['card']        = $card_data;
                $customerdata['recent_card'] = $recentCard;

                $invoices = $this->data_model->get_invoice_data($this->merchantID, 'Open', $customerID);

                // print_r(    $invoices['status']);die;
                $table              = '';
                $new_inv            = '';
                $totalInvoiceAmount = 0.00;

                if (!empty($invoices)) {
                    $table .= '<table class="col-md-offset-3 mytable" width="50%">';
                    $new_inv = '<div class="form-group alignTableInvoiceList" >
		      <div class="col-md-1 text-center"><b></b></div>
		        <div class="col-md-3 text-left"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2 text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
			   </div>';

                    $inv_data = [];
                    foreach ($invoices as $inv) {
                        if (strtoupper($inv['status']) != 'CANCEL') {
                            $new_inv .= '<div class="form-group alignTableInvoiceList" >

				         <div class="col-md-1 text-center"><input  checked type="checkbox" class="chk_pay check_' . $inv['refNumber'] . '" id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
				        <div class="col-md-3 text-left">' . $inv['refNumber'] . '</div>
				        <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
				        <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
					   <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['invoiceID'] . '  geter" data-id="multiinv' . $inv['invoiceID'] . '" data-inv="' . $inv['invoiceID'] . '" data-ref="' . $inv['refNumber'] . '" data-value="' . $inv['BalanceRemaining'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
					   </div>';
                            $inv_data[] = [
                                'refNumber'        => $inv['refNumber'],
                                'invoiceID'        => $inv['invoiceID'],
                                'BalanceRemaining' => $inv['BalanceRemaining'],
                            ];
                            $invoiceIDs[$inv['invoiceID']] = $inv['refNumber'];
                            $totalInvoiceAmount            = $totalInvoiceAmount + $inv['BalanceRemaining'];
                        }
                    }
                    $table .= "</table>";
                } else {
                    $table .= '';
                }
                if (empty($customerdata['companyName'])) {
                    $customerdata['companyName'] = $customerdata['fullName'];
                }
                $customerdata['invoices']           = $new_inv;
                $customerdata['invoiceIDs']         = $invoiceIDs;
                $customerdata['totalInvoiceAmount'] = $totalInvoiceAmount;
                echo json_encode($customerdata);
                die;
            }
        } else {
            $customerdata   = [];
            $conditionGW    = array('gatewayID' => $gatewayID);
            $gateway        = $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
            $cardTypeOption = 2;
            if (isset($gateway['gatewayID'])) {
                if ($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1) {
                    $cardTypeOption = 1;
                } else if ($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0) {
                    $cardTypeOption = 2;
                } else if ($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1) {
                    $cardTypeOption = 3;
                } else if ($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0) {
                    $cardTypeOption = 4;
                } else {
                    $cardTypeOption = 2;
                }

            }

            $customerdata['status']         = 'success';
            $customerdata['cardTypeOption'] = $cardTypeOption;
            echo json_encode($customerdata);
            die;
        }
    }

    public function get_tax_id()
    {
        $merchantID = $this->merchantID;

        $id  = $this->czsecurity->xssCleanPostInput('tax_id');
        $val = array(
            'taxID'      => $id,
            'merchantId' => $merchantID,
        );

        $data = $this->general_model->get_row_data('tbl_taxes_xero', $val);
        echo json_encode($data);
    }

    public function add_note()
    {
        $user_id = $this->merchantID;

        if (!empty($this->input->post('customerID'))) {
            $cusID = $this->czsecurity->xssCleanPostInput('customerID');
            if ($this->czsecurity->xssCleanPostInput('private_note') != "") {

                $private_note = $this->czsecurity->xssCleanPostInput('private_note');
                $data_ar      = array('privateNote' => $private_note, 'privateNoteDate' => date('Y-m-d H:i:s'), 'customerID' => $cusID, 'merchantID' => $user_id);
                $id           = $this->general_model->insert_row('tbl_private_note', $data_ar);
                if ($id > 0) {
                    array('status' => "success");
                    echo json_encode(array('status' => "success"));
                    die;
                } else {
                    array('status' => "error");
                    echo json_encode(array('status' => "success"));
                    die;
                }
            }
        }
    }

    public function delele_note()
    {
        if ($this->czsecurity->xssCleanPostInput('noteID') != "") {
            $noteID = $this->czsecurity->xssCleanPostInput('noteID');
            if ($this->db->query("Delete from tbl_private_note where noteID =  '" . $noteID . "' ")) {
                array('status' => "success");
                echo json_encode(array('status' => "success"));
                die;
            }
            return false;
        }
    }

    public function batchReciept()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $page_data = $this->session->userdata('batch_process_receipt_data');

        $invoices   = $page_data['data'];
        $invoiceObj = [];
        if (count($invoices) > 0) {
            foreach ($invoices as $value) {
                $invoiceObj[] = $this->general_model->getInvoiceBatchTransactionList($value['invoice_id'], $value['customerID'], $value['transactionRowID'], $this->logged_in_data['active_app']);
            }
        }

        $data['allTransaction']  = $invoiceObj;
        $data['statusCode']      = array('200', '100', '111', '1', '102', '120');
        $data['integrationType'] = 4;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/batch_invoice_reciept', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_customer_all_invoices()
    {
        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $invoices = $this->data_model->get_invoice_data($this->merchantID, 'Open', $customerID);

            //  print_r($invoices);
            $new_inv = '<div class="form-group" >
		      <div class="col-md-2 text-center"><b>Select</b></div>
		        <div class="col-md-2 text-left"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2 text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
		       </div>';
            foreach ($invoices as $inv) {
                if (strtoupper($inv['status']) != 'CANCEL') {
                    $new_inv .= '<div class="form-group" >

		         <div class="col-md-2 text-center"><input type="checkbox" class="chk_pay"  id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
		        <div class="col-md-2 text-left">' . $inv['refNumber'] . '</div>
		        <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
		        <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
		       <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '"    onblur="chk_pay_position(this);"  class="form-control   geter multiinv' . $inv['invoiceID'] . '" data-id="multiinv' . $inv['invoiceID'] . '"  class="form-control" value="' . $inv['BalanceRemaining'] . '" /></div>
		       </div>';
                }
            }

            $card         = '';
            $card_name    = '';
            $customerdata = array();

            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $this->merchantID);
            $customerdata = $this->general_model->get_row_data('Xero_custom_customer', $condition);
            if (!empty($customerdata)) {

                //    $vaultID    = ($valul_data['vaultID'])?$valul_data['vaultID']:'';
                $customerdata['status'] = 'success';

                $card_data                = $this->card_model->getCustomerCardDataByID($customerID);
                $customerdata['card']     = $card_data;
                $customerdata['invoices'] = $new_inv;

                echo json_encode($customerdata);
                die;
            }
        }
    }

    public function getAnnualRevenue($user_id)
    {
        $get_result = $this->company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
            $result_set          = array();
            $result_value1       = array();
            $result_value2       = array();
            $result_online_value = array();
            $result_online_month = array();
            $result_eCheck_value = array();
            $result_eCheck_month = array();
            foreach ($get_result['data'] as $count_merch) {

                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, (float) $count_merch['revenu_volume']);
                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);
                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
            }
            $ob[]     = [];
            $obRevenu = [];
            $obOnline = [];
            $obeCheck = [];
            $in       = 0;

            foreach ($result_value1 as $value) {
                $custmonths = date("M", strtotime($value));

                $ob1        = [];
                $obR        = [];
                $obON       = [];
                $obEC       = [];
                $inc        = strtotime($value);
                $ob1[0]     = $inc;
                $obR[]      = $inc;
                $obR[]      = $custmonths;
                $ob1[1]     = $result_value2[$in];
                $ob[]       = $ob1;
                $obON[0]    = $inc;
                $obON[1]    = $result_online_value[$in];
                $obOnline[] = $obON;
                $obEC[0]    = $inc;
                $obEC[1]    = $result_eCheck_value[$in];
                $obeCheck[] = $obEC;
                $obRevenu[] = $obR;
                $in++;
            }
            $opt_array['revenu_month']  = $obRevenu;
            $opt_array['revenu_volume'] = $ob;
            $opt_array['online_month']  = $result_online_month;
            $opt_array['online_volume'] = $obOnline;
            $opt_array['eCheck_month']  = $result_eCheck_month;
            $opt_array['eCheck_volume'] = $obeCheck;
            $opt_array['totalRevenue']  = $get_result['totalRevenue'];
            $opt_array['totalCCA']      = $get_result['totalCCA'];
            $opt_array['totalECLA']     = $get_result['totalECLA'];
            return ($opt_array);
        }
        return [];
    }
}
