<?php
ini_set('display_errors', 'On');
require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

include_once APPPATH .'libraries/Manage_payments.php';
require_once APPPATH . 'libraries/integration/XeroSync.php';
class CronProcess extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');
        $this->load->model('Common/company_model');
        $this->load->model('card_model');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);
    }

    public function index()
    {
        exit('Invalid Request');
    }

    public function refresh_xero_authentication(){
        $this->load->config('xero');

        $confiData  = [
            'urlAuthorize'            => $this->config->item('urlAuthorize'),
            'urlAccessToken'          => $this->config->item('urlAccessToken'),
            'urlResourceOwnerDetails' => $this->config->item('urlResourceOwnerDetails'),
            'clientId'                => $this->config->item('clientId'),
            'clientSecret'            => $this->config->item('clientSecret'),
            'redirectUri'             => $this->config->item('redirectUri'),
        ];

        $tok_data      = $this->general_model->get_table_data('tbl_xero_token', '');
        if (!empty($tok_data)) {
            $provider = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                => $confiData['clientId'],
                'clientSecret'            => $confiData['clientSecret'],
                'redirectUri'             => $confiData['redirectUri'],
                'urlAuthorize'            => $confiData['urlAuthorize'],
                'urlAccessToken'          => $confiData['urlAccessToken'],
                'urlResourceOwnerDetails' => $confiData['urlResourceOwnerDetails'],
            ]);
        
            foreach ($tok_data as $key => $tokenData) {
                try {
                    $newAccessToken = $provider->getAccessToken('refresh_token', [
                        'refresh_token' => $tokenData['refresh_token']
                    ]);
                    // Save my tokens, expiration tenant_id
                    $access_token_data = [
                        'access_token' => $newAccessToken->getToken(),
                        'refresh_token' => $newAccessToken->getRefreshToken(),
                        'token_validity' => $newAccessToken->getExpires(),
                        'access_id_token' => $newAccessToken->getValues()["id_token"],
                    ];

                    $this->general_model->update_row_data('tbl_xero_token', ['xero_id' => $tokenData['xero_id']], $access_token_data);
                    echo "Token refreshed for: ".$tokenData['merchantID'].'<br>';

                } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                    echo "Token failed for: ".$tokenData['merchantID'].'<br>';
                }
            }
        }
    }

    public function create_invoice()
	{
		$subsdata = $this->customer_model->get_subcription_cron_data();
		if (!empty($subsdata)) {
			foreach ($subsdata as $subs) {
	
				$merchantID = $merchID = $subs['merchantDataID'];
				$customerID = $subs['customerID'];
	
				if ($subs['generatedInvoice'] < $subs['totalInvoice'] || $subs['totalInvoice'] == 0) {
	
					$autopay    = 0;
					$cardID     = $subs['cardID'];
					$paygateway = $subs['paymentGateway'];
					$autopay    = $subs['automaticPayment'];
	
					if ($subs['generatedInvoice'] < $subs['freeTrial']) {
						$free_trial = '1';
					} else {
						$free_trial = '0';
					}
	
					$total = 0;
	
					$tax = $taxval = $subs['taxID'];
					if ($tax) {
						$tax_data = $this->general_model->get_row_data('tbl_taxe_code_qbo', array('taxID' => $tax, 'merchantID' => $merchantID));
						$taxRate  = $tax_data['total_tax_rate'];
						$taxlsID  = $tax_data['taxID'];
					} else {
						$taxRate = 0;
						$taxlsID = 0;
					}
	
					$isInvoiceExist = $this->general_model->get_row_data('tbl_subscription_auto_invoices', array('app_type' => 4, 'subscriptionID' => $subs['subscriptionID']));

					$item_val = [];
					$products = $this->customer_model->get_cron_subcription_items_data($subs['subscriptionID']);
					foreach ($products as $key => $prod) {
						if($isInvoiceExist && $prod['oneTimeCharge']){
							continue;
						}
						
						$insert_row['itemListID']      = $prod['itemListID'];
						$insert_row['itemQuantity']    = $prod['itemQuantity'];
						$insert_row['itemRate']        = $prod['itemRate'];
						$insert_row['itemFullName']    = $prod['itemFullName'];
						$insert_row['itemTax']         = $prod['itemTax'];
						$insert_row['itemDescription'] = $prod['itemDescription'];
						$insert_row['itemTotal'] = $itemTotal = $prod['itemQuantity'] * $prod['itemRate'];

						if($insert_row['itemTax'] == 1) {
							$itemTotal += ($insert_row['itemTotal'] * $taxRate / 100);
						}

						if($prod['oneTimeCharge']){
							$onetime += $itemTotal;
						} else {
							$recurring += $itemTotal;
						}

						$total = $total + $itemTotal;

						$item_val[]                = $insert_row;
					}
	
					$val = array(
						'merchantID' => $subs['merchantDataID'],
					);
	
					$chh_mail = $subs['emailRecurring'];
	
					$inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
					if (!empty($inv_data)) {
						$inv_pre    = $inv_data['prefix'];
						$inv_po     = $inv_data['postfix'] + 1;
						$new_inv_no = $inv_pre . $inv_po;
						$Number     = $new_inv_no;
					} else {
						continue;
					}
	
					$inv_date = $duedate = date($subs['nextGeneratingDate']);
	
					$cs_data   = $this->general_model->get_select_data('Xero_custom_customer', array('fullName', 'companyID', 'companyName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
					$fullname  = $cs_data['fullName'];
					$companyID = $cs_data['companyID'];

					$date      = $subs['firstDate'];

					if ($subs['proRate']) {
						$proRate = $subs['proRate'];
					} else {
						$proRate = 0;
					}

					if ($subs['proRateBillingDay']) {
						$proRateday = $subs['proRateBillingDay'];
						$nextInvoiceDate = $subs['nextMonthInvoiceDate'];
					} else {
						$proRateday = 1;
						$nextInvoiceDate = date('d', strtotime($date));
					}

					$in_num    = $subs['generatedInvoice'];
					$paycycle  = $subs['invoiceFrequency'];

					$prorataCalculation = prorataCalculation([
						'paycycle' => $subs['invoiceFrequency'],
						'proRateday' => $proRateday,
						'nextInvoiceDate' => $nextInvoiceDate,
						'total_amount' => $total,
						'onetime' => $onetime,
						'recurring' => $recurring,
						'date' => $date,
						'in_num' => $in_num,
						'proRate' => $proRate,
					]);

					$next_date = $prorataCalculation['next_date'];
					$total = $prorataCalculation['total_amount'];
	
					for ($i = 0; $i < count($products); $i++) {
						$prorataCalculatedRate = 1;
						if($products[$i]['oneTimeCharge'] == 0){
							$prorataCalculatedRate = $prorataCalculation['prorataCalculatedRate'];
						}
			
						$inputData['productID'][] = $products[$i]['Code'];
						$inputData['quantity'][] = $products[$i]['itemQuantity'];
						$inputData['unit_rate'][] = $products[$i]['itemRate'] * $prorataCalculatedRate;
						$inputData['description'][] = $products[$i]['itemDescription'];
						$inputData['AccountCode'][] = $products[$i]['AccountCode'];
						$inputData['ptaxID'][]      = $products[$i]['itemTax'];
					}
					$inputData['refNumber'] = $new_inv_no;
					$inputData['customerID'] = $customerID;
					$inputData['invoiceDate'] = $inv_date;
					$inputData['invoiceDueDate'] = $duedate;

					if($subs['taxes'] == "NoTax"){
						$subs['taxes'] = '';
					} else if($subs['taxes'] == 'Exclusive'){
						$subs['taxes'] = 1;
					}else if($subs['taxes'] == 'Inclusive'){
						$subs['taxes'] = 2;
					}
					$inputData['taxes']          = $subs['taxes'];

					
					$xeroSync   = new XeroSync($merchID);
					$retrunData = $xeroSync->createUpdateInvoice($inputData, false);
					if ($retrunData) {
						$invID = $retrunData[0];
	
						$schedule['invoiceID']  = $invID;
						$schedule['gatewayID']  = $paygateway;
						$schedule['cardID']     = $cardID;
						$schedule['customerID'] = $customerID;
	
						if ($autopay) {
							$schedule['autoInvoicePay']       = 1;
							$schedule['paymentMethod'] = 1;
						} else {
							$schedule['autoInvoicePay']       = 0;
							$schedule['paymentMethod'] = 0;
						}
						$schedule['merchantID'] = $merchID;
						$schedule['createdAt']  = date('Y-m-d H:i:s');
						$schedule['updatedAt']  = date('Y-m-d H:i:s');
						$this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);
	
						$subscription_auto_invoices_data = [
							'subscriptionID' => $subs['subscriptionID'],
							'invoiceID'      => $invID,
							'app_type'       => 4,
						];
	
						$this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);
	
						if ($chh_mail == '1') {
							$inv_no    = $new_inv_no;
							$invoiceID = $invID;
							
							$condition2    = array('merchantID' => $user_id, 'invoiceID' => $invoiceID);
							$invoice_data  = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
							$item_data = $this->data_model->get_invoice_details_item_data($invoiceID, $user_id);

							$condition3    = array('Customer_ListID' => $invoice_data['CustomerListID'], 'merchantID' => $user_id);
							$customer_data = $this->general_model->get_row_data('Xero_custom_customer', $condition3);
							
							$this->general_model->generate_invoice_pdf_xero($user_id, $customer_data, $invoice_data, $item_data, 'F');

							$condition_mail = array('templateType' => '3', 'merchantID' => $merchID);

							$toEmail          = $cs_data['userEmail'];
							$company          = $cs_data['companyName'];
							$customer         = $cs_data['fullName'];
							$invoice_due_date = $duedate;
							$invoicebalance   = 0; // $QBO_invoice_details['BalanceRemaining'];

							// $invoiceID= $QBO_invoice_details['invoiceID'];
							$tr_date = date('Y-m-d H:i:s');
							//  print_r($invoicebalance);die;
							$this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $customerID, $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $inv_no);
						}
					}
	
					$gen_inv = $subs['generatedInvoice'] + 1;
					$this->general_model->update_row_data('tbl_subscriptions_qbo',
						array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));
	
					$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']), array('postfix' => $inv_po));
				}
			}
		}
	}

	public function pay_invoice()
    {
		$processedIds = [];
        $invoice_data = $this->customer_model->get_invoice_data_auto_pay();
        if (!empty($invoice_data)) {
			
            foreach ($invoice_data as $in_data) {

				#Block of code to fetch updated details of invoice if a payment has been already processed in this execution 
				/* Updateded Details Code Starts */
				$checkId = $in_data['invoiceID'].'-'.$in_data['merchantID'];
				if(in_array($checkId, $processedIds)){
					$newData = $this->customer_model->get_invoice_data_auto_pay([
						'scheduleID' => $in_data['scheduleID']
					]);

					if(!$newData){
						continue;
					}
					$in_data = $newData[0];
				}
				/* Updateded Details Code Starts */

                if ($in_data['BalanceRemaining'] != '0.00') {

                    if ($in_data['DueDate'] != '') {
						$card_data  = $this->card_model->get_single_card_data($in_data['cardID']);
						if(empty($card_data)){
							continue;
						}

						$payOption = $in_data['paymentMethod'];
						
						$card_no    = $card_data['CardNo'];
                        $cvv        = $card_data['CardCVV'];
                        $expmonth   = $card_data['cardMonth'];
                        $exyear     = $card_data['cardYear'];
						$cardType   = $card_data['CardType'];
						
                        $address1   = $card_data['Billing_Addr1'];
                        $address2   = $card_data['Billing_Addr2'];
                        $city       = $card_data['Billing_City'];
                        $zipcode = $zip    = $card_data['Billing_Zipcode'];
                        $state      = $card_data['Billing_State'];
                        $country    = $card_data['Billing_Country'];
                        $customerID = $in_data['Customer_ListID'];
						$user_id    = $in_data['merchantID'];
						$transactionByUser = ['id' => null, 'type' => 4];
						
						$name =  $in_data['firstName'] . " " .  $in_data['lastName'];
						$cs_data     = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $user_id));
						
						$invoiceID  = $in_data['invoiceID'];
						
						if($in_data['autoInvoicePay'] == 1){
							$amount = $in_data['BalanceRemaining'];
							$isScheduleProcessed = 0; 
						} else if($in_data['autoPay'] == 1 && $in_data['schedule'] != "" && $in_data['schedule'] == date('Y-m-d') ){
							if ($in_data['scheduleAmount'] <= $in_data['BalanceRemaining']) {
								$amount = $in_data['scheduleAmount'];
								$isScheduleProcessed = 1; 
							} else {
								$amount = $in_data['BalanceRemaining'];
								$isScheduleProcessed = 0; 
							}
						} else {
							continue;
						}

						$pay_sts = "";
                        $type = 'sale';

                        $paymentObj = new Manage_payments($user_id);
						$saleData = [
							'paymentDetails' => $card_data,
							'transactionByUser' => ['id' => 0, 'type' => 4],
							'ach_type' => (!empty($card_data['CardNo'])) ? 1 : 2,
							'invoiceID'	=> $invoiceID,
							'crtxnID'	=> '',
							'companyName'	=> $cs_data['companyName'],
							'fullName'	=> $cs_data['fullName'],
							'firstName'	=> $cs_data['firstName'],
							'lastName'	=> $cs_data['lastName'],
							'contact'	=> $cs_data['phoneNumber'],
							'email'	=> $cs_data['userEmail'],
							'amount'	=> $amount,
							'gatewayID' => $in_data['gatewayType'],
							'storeResult' => true,
							'customerListID' => $cs_data['Customer_ListID'],
						];

						$saleData = array_merge($saleData, $in_data);
						$paidResult = $paymentObj->_processSaleTransaction($saleData);

                        $action = 'Pay Invoice';
                        $st = 0;
						$msg = 'Payment Failed';
						$crtxnID = $qbID = '';
						
                        if(isset($paidResult['response']) && $paidResult['response']){
							$accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $user_id, 'isPaymentAccepted' => 1]);
							$tr_date           = date('Y-m-d H:i:s');
							$updateInvoiceData = [
								'invoiceID'       => $invoiceID,
								'accountCode'     => $accountData['accountCode'],
								'trnasactionDate' => $tr_date,
								'amount'          => $amount,
							];
							
							$xeroSync   = new XeroSync($user_id);
							$crtxnID  = $xeroSync->createPayment($updateInvoiceData);
							
							$this->general_model->update_row_data('customer_transaction', ['id' => $paidResult['id']], ['qbListTxnID' => $crtxnID]);

							
							$updateScheduleData = [
								'merchantID' => $user_id,
							];

							if($isScheduleProcessed) {
								$updateScheduleData['ScheduleDate'] = null;
								$updateScheduleData['scheduleAmount'] = 0;
							}

							if($in_data['autoInvoicePay'] == 1 && $ispaid == 1) {
								$isProcess = 1;
							} else if($in_data['autoInvoicePay'] == 0) {
								$isProcess = 1;
							} else {
								$isProcess = 0;
							}
							$updateScheduleData['isProcessed'] = $isProcess;

							$this->general_model->update_row_data('tbl_scheduled_invoice_payment', [
								'scheduleID' => $in_data['scheduleID']
							], $updateScheduleData);
							
                            

                        }
                        
						$id = $this->general_model->insert_gateway_transaction_data($res, $type, $in_data['gatewayID'], $in_data['gatewayType'], $in_data['Customer_ListID'], $amount, $user_id, $crtxnID, $in_data['resellerID'], $invoiceID, false, $transactionByUser);

                    }

                }
            }

        }

    }

	public function process_recurring_invoice()
    {

        $invoice_data = $this->customer_model->getAutoRecurringInvoices();
        
        if (!empty($invoice_data)) {
            foreach ($invoice_data as $in_data) {

                if ($in_data['BalanceRemaining'] != '0.00') {

                    if ($in_data['DueDate'] != '') {
						$card_data  = $this->card_model->get_single_card_data($in_data['cardID']);
						if(empty($card_data)){
							continue;
						}

						$user_id    = $in_data['merchantID'];

						$invWhere  = array( 'BalanceRemaining >' => 0, 'invoiceID' => $in_data['invoiceID'], 'merchantID' => $user_id);
						$invData = $this->general_model->get_row_data('Xero_test_invoice', $invWhere);
						if(!$invData || empty($invData)){
							continue;
						}

                        $card_no    = $card_data['CardNo'];
                        $cvv        = $card_data['CardCVV'];
                        $expmonth   = $card_data['cardMonth'];
                        $exyear     = $card_data['cardYear'];
						$cardType   = $card_data['CardType'];
						
                        $address1   = $card_data['Billing_Addr1'];
                        $address2   = $card_data['Billing_Addr2'];
                        $city       = $card_data['Billing_City'];
                        $zipcode = $zip    = $card_data['Billing_Zipcode'];
                        $state      = $card_data['Billing_State'];
                        $country    = $card_data['Billing_Country'];
                        $customerID = $in_data['Customer_ListID'];

						$phone     	= $in_data['Phone'];
						$email = $in_data['userEmail'];
						$companyName =  $in_data['companyName'];
						$firstName = $in_data['firstName'];
						$lastName  = $in_data['lastName'];
						
						$name =  $in_data['firstName'] . " " .  $in_data['lastName'];
                        $invoiceID  = $in_data['invoiceID'];

						$amount = $in_data['BalanceRemaining'];

						$pay_sts = "";
                        $type = 'sale';

                        $paymentObj = new Manage_payments($user_id);
						$saleData = [
							'paymentDetails' => $card_data,
							'transactionByUser' => $this->transactionByUser,
							'ach_type' => (!empty($card_data['CardNo'])) ? 1 : 2,
							'invoiceID'	=> $invoiceID,
							'crtxnID'	=> '',
							'companyName'	=> $in_data['companyName'],
							'fullName'	=> $in_data['fullName'],
							'firstName'	=> $in_data['firstName'],
							'lastName'	=> $in_data['lastName'],
							'contact'	=> $in_data['phoneNumber'],
							'email'	=> $in_data['userEmail'],
							'amount'	=> $amount,
							'gatewayID' => $in_data['gatewayType'],
							'returnResult' => true,
							//'storeResult' => true,
							'customerListID' => $in_data['Customer_ListID'],
						];

						$saleData = array_merge($saleData, $in_data);

						$paidResult = $paymentObj->_processSaleTransaction($saleData);
						
                        $action = 'Pay Invoice';
                        $st = 0;
						$msg = 'Payment Failed';
						$crtxnID = $qbID = '';
						
						if(isset($paidResult['response']) && $paidResult['response']){
							$pay_sts = "SUCCESS";
							$accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $user_id, 'isPaymentAccepted' => 1]);
							$tr_date           = date('Y-m-d H:i:s');
							$updateInvoiceData = [
								'invoiceID'       => $invoiceID,
								'accountCode'     => $accountData['accountCode'],
								'trnasactionDate' => $tr_date,
								'amount'          => $amount,
							];
							
							$xeroSync   = new XeroSync($user_id);
							
							$crtxnID  = $xeroSync->createPayment($updateInvoiceData);
							
							$this->general_model->update_row_data('customer_transaction', ['id' => $paidResult['id']], ['qbListTxnID' => $crtxnID]);
							
                            $nf =  $this->general_model->addNotificationForMerchant($amount,$in_data['fullName'],$customerID,$in_data['merchID'],$in_data['invoiceID']);
                            
                        }else{
                        	$nf =  $this->general_model->failedNotificationForMerchant($amount,$in_data['fullName'],$customerID,$in_data['merchID'],$in_data['invoiceID']);
                        }

						$id = $this->general_model->insert_gateway_transaction_data($paidResult['result'], $type, $in_data['gatewayID'], $in_data['gatewayType'], $in_data['Customer_ListID'], $amount, $user_id, $crtxnID, $in_data['resellerID'], $invoiceID, false, ['id' => 0, 'type' => 4]);
						
						if ($pay_sts == "SUCCESS" && $in_data['recurring_send_mail']) {
							$this->send_mail_data($in_data, '5');
						}
                    }

                }
            }

        }

    }
    /**
     * Description : Function used for sending mail
     * 
     * @param array | $indata
     * @param int | $type
     * @return void
     * */
    public function send_mail_data(array $indata = array(), int $type)
	{
		
		$customerID        = $indata['Customer_ListID'];
		$companyID		  = $indata['companyID'];
		$typeID			  = $type;
		$invoiceID        = $indata['invoiceID'];
		
		$merchantID = $indata['merchantID'];

		$condition         = array('templateType' => $typeID, 'merchantID' => $merchantID);
		$view_data  = $this->company_model->template_data($condition);
		$merchant_data     = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID));
		$config_data     = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchantID));

		if (empty($view_data['mailDisplayName'])) {
			$view_data['mailDisplayName'] = $merchant_data['companyName'];
		}

		$currency          = "$";

		$config_email      = $merchant_data['merchantEmail'];
		$merchant_name     = $merchant_data['companyName'];
		$logo_url          = $merchant_data['merchantProfileURL'];
		$logo_url          = $merchant_data['merchantProfileURL'];
		$mphone            =  $merchant_data['merchantContact'];
		$cur_date          = date('Y-m-d');
		$amount 		= '';
		$paymethods   = '';
		$transaction_details = '';
		$tr_data      = '';
		$ref_number   = '';
		$overday      = '';
		$balance      = '0.00';
		$in_link      = '';
		$duedate      = '';
		$company      = '';
		$cardno       = '';
		$expired      = '';
		$expiring     = '';
		$friendly_name = '';
		$tr_amount    = '0.00';
		$update_link  = $config_data['customerPortalURL'];
		$gateway_msg  = '';
		$companyName = '';

		$condition1 = " and `inv`.CustomerListID = '" . $customerID . "' ";
		$condition1 .= " and `inv`.invoiceID = '" . $invoiceID . "' ";
		$data = [];
		if ($typeID == '5') {
			$data      = $this->data_model->get_invoice_data_template($condition1, $merchantID);
		}
		if ($typeID == '4') {
			$data = $this->data_model->get_invoice_data_template($condition1, $merchantID);
			$card_data  = $this->card_model->get_single_card_data($indata['cardID']);
			if (!empty($card_data)) {
				$cardno          =  $card_data['CustomerCard'];
				$friendly_name     =  $card_data['customerCardfriendlyName'];
			}
			        
		}
		if(!empty($data)){
			$companyName = $data['comapanyName'];		 
			$customer = $data['fullName'];
			$amount    = $data['AppliedAmount'] ; 

			$balance    = $data['BalanceRemaining'] ;  
			$paymethods    = $data['paymentType'] ;  

			$duedate        = date('F d, Y', strtotime($data['DueDate'])) ;   
			$ref_number      = $data['RefNumber'] ;  
			$tr_date   =  date('F d, Y',strtotime($data['tr_date']));
			$tr_data   =  $data['tr_data']; 
			$tr_amount =  $data['tr_amount']; 
		}
	    if ($typeID == '5' && isset($merchant_data['merchant_default_timezone'])  && !empty($merchant_data['merchant_default_timezone'])) {
            // Convert added date in timezone 
            $timezone = ['time' => date('Y-m-d H:i:s'), 'current_format' => 'UTC', 'new_format' => $merchant_data['merchant_default_timezone']];
            $tr_date = getTimeBySelectedTimezone($timezone);
            $tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
        }
	   	$subject =	stripslashes(str_ireplace('{{invoice.refnumber}}',$ref_number, $view_data['emailSubject']));
		if( $view_data['emailSubject'] =="Welcome to { company_name }"){
			$subject = 'Welcome to '.$company;
		}
		$message = $view_data['message'];
		/* Check merchant logo is available if yes than display otherwise display default chargezoom */
		$logo_url = CZLOGO;
		if($config_data['ProfileImage']!=""){
			$logo_url = LOGOURL1.$config_data['ProfileImage']; 
		}

		$logo_url = "<img src='" . $logo_url . "' />";
		$message  = stripslashes(str_ireplace('{{logo}}', $logo_url, $message));

		$message = stripslashes(str_ireplace('{{creditcard.type_name}}',$friendly_name ,$message));
		$message = stripslashes(str_ireplace('{{merchant_company_name}}',$merchant_name ,$message));
		$message = stripslashes(str_ireplace('{{creditcard.mask_number}}',$cardno ,$message ));
		$message = stripslashes(str_ireplace('{{creditcard.type_name}}',$friendly_name ,$message ));
		$message = stripslashes(str_ireplace('{{creditcard.url_updatelink}}',$update_link ,$message ));

		$message = stripslashes(str_ireplace('{{customer.company}}',$companyName,$message));
		$message = stripslashes(str_ireplace('{{transaction.currency_symbol}}',$currency ,$message ));
		$message = stripslashes(str_ireplace('{{invoice.currency_symbol}}',$currency ,$message ));

		$message = stripslashes(str_ireplace('{{transaction.amount}}',($tr_amount)?($tr_amount):'0.00' ,$message )); 

		$message = stripslashes(str_ireplace('{{transaction.transaction_method}}',$paymethods ,$message));
		$message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message ));
		$message = stripslashes(str_ireplace('{{transaction.transaction_detail}}', $tr_data, $message ));

		$message = stripslashes(str_ireplace('{% transaction.gateway_method %}', $paymethods, $message ));				
		$message = stripslashes(str_ireplace('{{invoice.days_overdue}}', $overday, $message ));
		$message =	stripslashes(str_ireplace('{{invoice.refnumber}}',$ref_number, $message));
		$message =	stripslashes(str_ireplace('{{invoice.balance}}',$balance, $message));
		$message =	stripslashes(str_ireplace('{{invoice.due_date|date("F j, Y")}}',$duedate, $message));
		$message = stripslashes(str_ireplace('{{ transaction.gateway_msg}}',$transaction_details ,$message));

		$message =	stripslashes(str_ireplace('{{invoice.url_permalink}}',$in_link, $message));
		$message = stripslashes(str_ireplace('{{merchant_email}}',$config_email ,$message ));
		$message = stripslashes(str_ireplace('{{merchant_phone}}',$mphone ,$message ));
		$message = stripslashes(str_ireplace('{{current.date}}',$cur_date ,$message ));
		$message = stripslashes(str_ireplace('{{invoice_payment_pagelink}}','' ,$message ));

		$fromEmail    = isset($view_data['fromEmail'])? $view_data['fromEmail']:$merchant_data['merchantEmail'];
		$toEmail   = isset($indata['Contact'])? $indata['Contact']:$indata['userEmail'];
		$addCC      = $view_data['addCC'];
		$addBCC		= $view_data['addBCC'];
		$replyTo    = isset($view_data['replyTo'])?$view_data['replyTo']:$config_email;

		$isValid = is_a_valid_customer([
			'email' => $toEmail
		]);
		if(!$isValid){
			return false;
		}
		
		$mailData = [
			'to' => $toEmail,
			'from' => $fromEmail,
			'fromname' => $view_data['mailDisplayName'],
			'subject' => $subject,
			'replyto' => $replyTo,
			'html' => $message,
		];
		sendEmailUsingSendGrid($mailData);
	}
}
