<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require APPPATH . 'libraries/integration/XeroSync.php';
include_once APPPATH . 'libraries/Manage_payments.php';

class Payments extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');
        $this->load->library('form_validation');

        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');

        $user_id   = $this->merchantID;
        $cusproID  = '';
        $error     = '';
        $cusproID  = $this->czsecurity->xssCleanPostInput('customerProcessID');
        $checkPlan = check_free_plan_transactions();

        if ($this->form_validation->run() == true) {

            $cusproID  = '';
            $error     = '';
            $cusproID  = $this->czsecurity->xssCleanPostInput('customerProcessID');
            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
            $cardID    = $this->czsecurity->xssCleanPostInput('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
            }

            // $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
            }

            $gateway = $gatlistval;

            $amount = $this->czsecurity->xssCleanPostInput('inv_amount');

            $condition2 = array('merchantID' => $this->merchantID, 'invoiceID' => $invoiceID);
            $in_data    = $this->general_model->get_row_data('Xero_test_invoice', $condition2);

            $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);

            $responseId = '';
            if ($checkPlan && !empty($in_data) && !empty($accountData)) {

                $gt_result    = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $customerID   = $in_data['CustomerListID'];
                $customerData = $this->general_model->get_select_data('Xero_custom_customer', array('*'), array('Customer_ListID' => $customerID, 'merchantID' => $this->merchantID));

                $CustomerListID = $in_data['CustomerListID'];

                $cardID_upd = '';

                if (!empty($cardID)) {

                    $paymentObj = new Manage_payments($this->merchantID);
                    if ($cardID != 'new1') {
                        $accountDetails = $this->card_model->get_single_card_data($cardID);
                    } else {
                        if ($sch_method == "1") {
                            $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                            $cardType = $this->general_model->getType($card_no);

                            $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                            $accountDetails = [
                                'cardMonth'       => $expmonth,
                                'cardYear'        => $exyear,
                                'CardType'        => $cardType,
                                'CustomerCard'    => $card_no,
                                'CardCVV'         => $cvv,
                                'CardNo'          => $card_no,

                                'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                                'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                'customerListID'  => $CustomerListID,
                                'merchantID'      => $this->merchantID,
                                'createdAt'       => date("Y-m-d H:i:s"),
                            ];
                        } else {
                            $accountDetails = [
                                'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                'customerListID'     => $CustomerListID,
                                'merchantID'         => $this->merchantID,
                                'createdAt'          => date("Y-m-d H:i:s"),
                                'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                            ];
                        }
                    }

                    $saleData = [
                        'paymentDetails'    => $accountDetails,
                        'transactionByUser' => $this->transactionByUser,
                        'ach_type'          => $sch_method,
                        'invoiceID'         => $in_data['invoiceID'],
                        'crtxnID'           => '',
                        'companyName'       => $customerData['companyName'],
                        'fullName'          => $customerData['fullName'],
                        'firstName'         => $customerData['firstName'],
                        'lastName'          => $customerData['lastName'],
                        'contact'           => $customerData['phoneNumber'],
                        'email'             => $customerData['userEmail'],
                        'amount'            => $amount,
                        'gatewayID'         => $gateway,
                        'returnResult'      => true,
                        'customerListID'    => $customerData['Customer_ListID'],
                    ];

                    $isACH = ($sch_method == 1) ? false : true;

                    $saleData   = array_merge($saleData, $in_data);
                    $paidResult = $paymentObj->_processSaleTransaction($saleData);

                    $crtxnID = '';
                    if (isset($paidResult['response']) && $paidResult['response']) {
                        $tr_date           = date('Y-m-d H:i:s');
                        $updateInvoiceData = [
                            'invoiceID'       => $in_data['invoiceID'],
                            'accountCode'     => $accountData['accountCode'],
                            'trnasactionDate' => $tr_date,
                            'amount'          => $amount,
                        ];

                        $xeroSync = new XeroSync($this->merchantID);
                        $crtxnID  = $xeroSync->createPayment($updateInvoiceData);

                        if ($cardID == "new1") {
                            if ($sch_method == "1") {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $cardType = $this->general_model->getType($card_no);

                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                $card_data = array(
                                    'cardMonth'       => $expmonth,
                                    'cardYear'        => $exyear,
                                    'CardType'        => $cardType,
                                    'CustomerCard'    => $card_no,
                                    'CardCVV'         => $cvv,
                                    'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                    'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'  => $customerID,

                                    'companyID'       => $companyID,
                                    'merchantID'      => $this->merchantID,
                                    'createdAt'       => date("Y-m-d H:i:s"),
                                );

                                $id1 = $this->card_model->process_card($card_data);
                            } else if ($sch_method == "2") {
                                $id1 = $this->card_model->process_ack_account($accountDetails);
                            }
                        }

                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                        $ref_number     = $in_data['refNumber'];
                        $toEmail        = $customerData['userEmail'];
                        $company        = $customerData['fullName'];
                        $customer       = $customerData['fullName'];
                        if ($chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                        }
                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                    }

                    $responseId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $in_data['invoiceID'], $isACH, $this->transactionByUser);
                    if($responseId){
                        $con1 = array('id' => $responseId);
                        $pay_amount = $this->general_model->get_row_data('customer_transaction', $con1);
                        $responseId = $pay_amount['transactionID'];
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                }
            } else {
                $errorMessage = "Transaction Failed -  This is not valid invoice";
                if (empty($accountData)) {
                    $errorMessage = "Please select a default payment account";
                }
                $this->session->set_flashdata('message', "<div class='alert alert-danger'><strong>$errorMessage</strong>.</div>");
            }
        } else {
            $error = 'Validation Error. Please fill the requred fields';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
        }

        if ($cusproID == "2") {
            redirect('home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3" && $in_data['invoiceID'] != '') {
            redirect('Integration/Invoices/invoice_details/' . $in_data['invoiceID'], 'refresh');
        }

        $invoice_IDs = array();

        $receipt_data = array(
            'proccess_url'      => 'Integration/Invoices/invoice_list',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan,
            'invoice_page'      => true,
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('Integration/Payments/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }
        redirect('Integration/Payments/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
    }

    public function offline_payments()
    {
        if ($this->session->userdata('logged_in')) {
            $data = $this->session->userdata('logged_in');

            $user_id = $data['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data = $this->session->userdata('user_logged_in');

        }
        $user_id   = $data['merchantID'];
        $amount    = $this->czsecurity->xssCleanPostInput('inv_amount');
        $invoiceID = $this->czsecurity->xssCleanPostInput('subscID');

        if ($this->czsecurity->xssCleanPostInput('pay_type') == '0') {
            $condition2  = array('merchantID' => $this->merchantID, 'invoiceID' => $invoiceID);
            $in_data     = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
            $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);

            if (!empty($in_data) && !empty($accountData)) {
                if (!empty($this->input->post('payment_date')) && !empty($this->input->post('check_number'))) {
                    $amount          = $this->czsecurity->xssCleanPostInput('inv_amount');
                    $Customer_ListID = $in_data['CustomerListID'];

                    $transaction = array();

                    $appliedAmount = $in_data['AppliedAmount'] - $amount;

                    $txnID = $in_data['invoiceID'];

                    $tr_date           = date('Y-m-d H:i:s');
                    $updateInvoiceData = [
                        'invoiceID'       => $in_data['invoiceID'],
                        'accountCode'     => $accountData['accountCode'],
                        'trnasactionDate' => $tr_date,
                        'amount'          => $amount,
                    ];

                    $xeroSync = new XeroSync($this->merchantID);
                    $crtxnID  = $xeroSync->createPayment($updateInvoiceData);

                    $status                           = $this->czsecurity->xssCleanPostInput('status');
                    $date                             = $this->czsecurity->xssCleanPostInput('payment_date');
                    $Check_Date                       = date('Y-m-d', strtotime($date));
                    $check                            = $this->czsecurity->xssCleanPostInput('check_number');
                    $transaction['transactionID']     = $check;
                    $transaction['transactionStatus'] = 'Offline Payment';
                    $transaction['transactionDate']   = date('Y-m-d H:i:s');
                    $transaction['transactionCode']   = '100';
                    $transaction['transactionType']   = "Offline Payment";
                    $transaction['customerListID']    = $in_data['CustomerListID'];
                    $transaction['transactionAmount'] = $amount;
                    $transaction['invoiceID']         = $in_data['invoiceID'];
                    $transaction['qbListTxnID']       = $crtxnID;
                    $transaction['merchantID']        = $this->merchantID;
                    $transaction['resellerID']        = $this->resellerID;
                    if (!empty($this->transactionByUser)) {
                        $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
                        $transaction['transaction_by_user_id']   = $this->transactionByUser['id'];
                    }

                    $CallCampaign = $this->general_model->triggerCampaign($user_id, $transaction['transactionCode']);
                    $id           = $this->general_model->insert_row('customer_transaction', $transaction);

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Fill the all required fields.</div>');
                }
            } else {
                $errorMessage = "Transaction Failed -  This is not valid invoice";
                if (empty($accountData)) {
                    $errorMessage = "Please select a default payment account";
                }
                $this->session->set_flashdata('message', "<div class='alert alert-danger'><strong>$errorMessage</strong>.</div>");
                redirect('Integration/Invoices/invoice_list', 'refresh');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Invalid Request</div>');
            redirect('Integration/Invoices/invoice_list', 'refresh');
        }

        redirect('Integration/Invoices/invoice_list', 'refresh');
    }

    public function pay_multi_invoice()
    {

        $merchantID = $this->merchantID;
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');

        $checkPlan       = check_free_plan_transactions();
        $Customer_ListID = '';
        $Customer_ListID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $error           = '';
        $resellerID      = $this->resellerID;

        $cardSaved   = false;
        $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);

        if ($checkPlan && $cardID != "" && $gateway != "" && !empty($accountData)) {
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
            if (!empty($invoices)) {
                $customerData = $this->general_model->get_select_data('Xero_custom_customer', array('companyID', 'firstName', 'lastName', 'companyName'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchantID));

                $paymentObj = new Manage_payments($this->merchantID);

                if ($cardID != 'new1') {
                    $accountDetails = $this->card_model->get_single_card_data($cardID);
                } else {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                    $cardType = $this->general_model->getType($card_no);

                    $accountDetails = [
                        'cardMonth'       => $expmonth,
                        'cardYear'        => $exyear,
                        'CardType'        => $cardType,
                        'CustomerCard'    => $card_no,
                        'CardCVV'         => $cvv,
                        'CardNo'          => $card_no,

                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                        'customerListID'  => $Customer_ListID,
                        'merchantID'      => $this->merchantID,
                        'createdAt'       => date("Y-m-d H:i:s"),
                    ];
                }

                foreach ($invoices as $key => $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->data_model->get_invoice_data_pay($invoiceID, $merchantID);
                    if (!empty($in_data)) {
                        $cr_amount = 0;
                        $amount    = $pay_amounts;
                        $amount    = $amount - $cr_amount;

                        $saleData = [
                            'paymentDetails'    => $accountDetails,
                            'transactionByUser' => $this->transactionByUser,
                            'ach_type'          => 1,
                            'invoiceID'         => $in_data['invoiceID'],
                            'crtxnID'           => '',
                            'companyName'       => $customerData['companyName'],
                            'fullName'          => $customerData['fullName'],
                            'firstName'         => $customerData['firstName'],
                            'lastName'          => $customerData['lastName'],
                            'contact'           => $customerData['phoneNumber'],
                            'email'             => $customerData['userEmail'],
                            'amount'            => $amount,
                            'gatewayID'         => $gateway,
                            'returnResult'      => true,
                            'customerListID'    => $customerData['Customer_ListID'],
                        ];

                        $saleData   = array_merge($saleData, $in_data);
                        $paidResult = $paymentObj->_processSaleTransaction($saleData);

                        $crtxnID = '';
                        if (isset($paidResult['response']) && $paidResult['response']) {
                            $tr_date           = date('Y-m-d H:i:s');
                            $updateInvoiceData = [
                                'invoiceID'       => $in_data['invoiceID'],
                                'accountCode'     => $accountData['accountCode'],
                                'trnasactionDate' => $tr_date,
                                'amount'          => $amount,
                            ];

                            $xeroSync = new XeroSync($this->merchantID);
                            $crtxnID  = $xeroSync->createPayment($updateInvoiceData);

                            if ($cardSaved && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                                $id1 = $this->card_model->process_card($accountDetails);
                            }
                            $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                        }
                        $id = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $this->merchantID, $crtxnID, $this->resellerID, $in_data['invoiceID'], false, $this->transactionByUser);
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if (!$checkPlan) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "' . base_url() . 'Integration/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if (empty($accountData)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if ($Customer_ListID != "") {
            redirect('Integration/Customers/customer_detail/' . $Customer_ListID, 'refresh');
        } else {
            redirect('Integration/Invoices/invoice_list', 'refresh');
        }

    }

    public function transation_receipt($txt_id = '', $invoiceId = '', $trans_id = '')
    {

        $page_data = $this->session->userdata('receipt_data');
        $page_data['transaction_id'] = $trans_id;
        $invoiceIdObj[] = $invoiceId;
        $this->session->set_userdata("receipt_data", $page_data);
        $this->session->set_userdata('invoice_IDs', $invoiceIdObj);
        foreach ($page_data as $key => $rcData) {
            $page_data[$key] = strip_tags($rcData);
        }
        $pay_amount = [];
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['page_data']   = $page_data;

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $transaction_id = $trans_id;

        $con = array('transactionID' => $transaction_id);
        
        $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);

        $transaction_id         = ($pay_amount) ? $pay_amount['transactionID'] : $transaction_id;
        $data['transaction_id'] = $transaction_id;


        $data['transactionAmount'] = ($pay_amount) ? $pay_amount['transactionAmount'] : '0.00';
        $surCharge                 = 0;
        $totalAmount               = 0;
        $isSurcharge               = 0;

        $transactionType = $pay_amount['transactionGateway'];
        if (isset($pay_amount['transactionGateway'])) {
            if ($pay_amount['transactionGateway'] == 10) {

                $resultAmount = getiTransactTransactionDetails($user_id, $trans_id);
                $totalAmount  = $resultAmount['totalAmount'];
                $surCharge    = $resultAmount['surCharge'];
                $isSurcharge  = $resultAmount['isSurcharge'];
                if ($resultAmount['payAmount'] != 0) {
                    $data['transactionAmount'] = $resultAmount['payAmount'];
                }
            }
        }
        $data['transactionDetail'] = $pay_amount;
        $data['surchargeAmount'] = $surCharge;
        $data['totalAmount']     = $totalAmount;
        $data['transactionType'] = $transactionType;
        $data['isSurcharge']     = $isSurcharge;
        $data['transactionCode'] = ($pay_amount) ? $pay_amount['transactionCode'] : '0';
        $data['Ip']              = getClientIpAddr();
        $data['email']           = $this->session->userdata('logged_in')['merchantEmail'];
        $data['name']            = $this->session->userdata('logged_in')['firstName'] . ' ' . $this->session->userdata('logged_in')['lastName'];
        $in_data                 = $this->session->userdata('in_data');
        $data['invoice']         = $invoiceID = $invoiceId;
        $data['invoice_number']  = $in_data['refNumber'];

        $condition2 = array('merchantID' => $this->merchantID, 'invoiceID' => $invoiceID);
       
        $in_data = [];
        $in_data[]    = $this->general_model->get_row_data('Xero_test_invoice', $condition2);

        $condition3            = array('Customer_ListID' => $in_data[0]['CustomerListID'], 'merchantID' => $this->merchantID);
        $customer_data         = $this->general_model->get_select_data('Xero_custom_customer', array('*'), $condition3);
        
        $customer_data = convertCustomerDataFieldName($customer_data,4);
        $data['customer_data'] = $customer_data;
        $data['invoice_data']  = $in_data;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/transaction_proccess_receipt', $data);
        $this->load->view('template/page_footer', $data);

        $this->load->view('template/template_end', $data);

    }

    public function delete_pay_transaction()
    {

        $merchID = $this->merchantID;

        if (!empty($this->input->post('paytxnID'))) {
            $ptxnID = $this->czsecurity->xssCleanPostInput('paytxnID');
            if (isset($_POST['txnvoidID'])) {
                $paydata = $this->general_model->get_row_data('customer_transaction', array('transactionID' => $_POST['txnvoidID'], 'merchantID' => $merchID));
            } else {
                $paydata = $this->general_model->get_row_data('customer_transaction', array('id' => $ptxnID, 'merchantID' => $merchID));
            }

            $ptxnID         = $paydata['id'];
            $transaction_id = $paydata['transactionID'];
            if (!empty($paydata)) {

                $transactionGateway = $paydata['transactionGateway'];
                $gatlistval         = $paydata['gatewayID'];
                $gt_result          = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

                $payment_capture_page = ($this->czsecurity->xssCleanPostInput('payment_capture_page')) ? $this->czsecurity->xssCleanPostInput('payment_capture_page') : 0;
                $setMailVoid          = ($this->czsecurity->xssCleanPostInput('setMailVoid')) ? 1 : 0;
                $paymentType          = (strrpos($paydata['gateway'], 'ECheck')) ? 2 : 1;

                $voidObj           = new Manage_payments($merchID);
                $voidedTransaction = $voidObj->voidTransaction([
                    'trID'        => $paydata['transactionID'],
                    'gatewayID'   => $gatlistval,
                    'paymentType' => $paymentType,
                ]);

                if ($voidedTransaction) {
                    if ((!empty($paydata['qbListTxnID']))) {
                        $qb_txn_id = $paydata['qbListTxnID'];

                        $updateInvoiceData = [
                            'paymentId' => $qb_txn_id,
                        ];

                        $xeroSync = new XeroSync($this->merchantID);
                        $crtxnID  = $xeroSync->deletePayment($updateInvoiceData);

                        $st = $this->general_model->update_row_data('customer_transaction',
                            array('transactionID' => $paydata['transactionID'], 'merchantID' => $merchID), array('transaction_user_status' => '3', 'transactionModified' => date('Y-m-d H:i:s')));
                        $this->session->set_flashdata('success', 'Transaction Voided');
                    } else {
                        if ($paydata['invoiceID'] == '' || $paydata['invoiceID'] == null) {
                            $st = $this->general_model->update_row_data('customer_transaction',
                                array('transactionID' => $paydata['transactionID'], 'merchantID' => $merchID), array('transaction_user_status' => '3', 'transactionModified' => date('Y-m-d H:i:s')));
                            if (!$st) {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed</strong></div>');
                            }
                        }
                    }
                    $this->session->set_flashdata('success', 'Transaction Voided');
                }

                if ($setMailVoid == '1') {
                    $customerID = $paydata['customerListID'];
                    $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchID);
                    $comp_data  = $this->general_model->get_select_data('Xero_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
                }
                $receipt_data = array(
                    'proccess_url'      => 'Integration/Payments/payment_capture',
                    'proccess_btn_text' => 'Process New Transaction',
                    'sub_header'        => 'Void',
                );

                $this->session->set_userdata("receipt_data", $receipt_data);

                redirect('Integration/Payments/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transaction_id, 'refresh');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>');
                redirect('Integration/Payments/payment_transaction', 'refresh');
            }

        }
        redirect('Integration/Payments/payment_transaction', 'refresh');
    }

    public function add_card_data()
    {
        $merchID  = $this->merchantID;
        $customer = $this->czsecurity->xssCleanPostInput('customerID11');

        $c_data      = $this->general_model->get_select_data('Xero_custom_customer', array('*'), array('Customer_ListID' => $customer, 'merchantID' => $this->merchantID));
        $isSurcharge = 0;

        if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
            $card_no       = $this->czsecurity->xssCleanPostInput('card_number');
            $decryptedCard = $card_no;
            $isSurcharge   = $this->card_model->chkCardSurcharge(['cardNumber' => $card_no]);
            $card_type     = $this->general_model->getType($card_no);
            $card_no       = $this->card_model->encrypt($card_no);
            $expmonth      = $this->czsecurity->xssCleanPostInput('expiry');
            $exyear        = $this->czsecurity->xssCleanPostInput('expiry_year');
            $decryptedcvv  = $this->czsecurity->xssCleanPostInput('cvv');
            $cvv           = ''; // $this->card_model->encrypt($cvv);

            $friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
            $acc_number   = $route_number   = $acc_name   = $secCode   = $acct_type   = $acct_holder_type   = '';
        }
        if ($this->czsecurity->xssCleanPostInput('formselector') == '2') {
            $acc_number       = $this->czsecurity->xssCleanPostInput('acc_number');
            $route_number     = $this->czsecurity->xssCleanPostInput('route_number');
            $acc_name         = $this->czsecurity->xssCleanPostInput('acc_name');
            $secCode          = $this->czsecurity->xssCleanPostInput('secCode');
            $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
            $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
            $expmonth         = $exyear         = $cvv         = 0;

            $card_no      = '';
            $friendlyname = 'Checking - ' . substr($acc_number, -4);
            $card_type    = 'Echeck';
        }

        $b_addr1    = $this->czsecurity->xssCleanPostInput('address1');
        $b_addr2    = $this->czsecurity->xssCleanPostInput('address2');
        $b_city     = $this->czsecurity->xssCleanPostInput('city');
        $b_state    = $this->czsecurity->xssCleanPostInput('state');
        $b_zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
        $b_country  = $this->czsecurity->xssCleanPostInput('country');
        $b_contact  = $this->czsecurity->xssCleanPostInput('contact');
        $is_default = ($this->czsecurity->xssCleanPostInput('defaultMethod') != null) ? $this->czsecurity->xssCleanPostInput('defaultMethod') : 0;

        $insert_array = array(
            'cardMonth'                => $expmonth,
            'cardYear'                 => $exyear,
            'CustomerCard'             => $card_no,
            'CardCVV'                  => $cvv,
            'CardType'                 => $card_type,
            'customerListID'           => $customer,
            'merchantID'               => $merchID,
            'companyID'                => 0,
            'Billing_Addr1'            => $b_addr1,
            'Billing_Addr2'            => $b_addr2,
            'Billing_City'             => $b_city,
            'Billing_State'            => $b_state,
            'Billing_Zipcode'          => $b_zipcode,
            'Billing_Country'          => $b_country,
            'Billing_Contact'          => $b_contact,
            'customerCardfriendlyName' => $friendlyname,
            'accountNumber'            => $acc_number,
            'routeNumber'              => $route_number,
            'accountName'              => $acc_name,
            'accountType'              => $acct_type,
            'accountHolderType'        => $acct_holder_type,
            'secCodeEntryMethod'       => $secCode,
            'createdAt'                => date("Y-m-d H:i:s"),
            'isSurcharge'              => $isSurcharge,
        );

        $isAuthorised = true;
        if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
            $params                 = $insert_array;
            $params['CustomerCard'] = $decryptedCard;
            $params['CardCVV']      = $decryptedcvv;
            $params['name']         = $c_data['fullName'];
            $params['amount']       = DEFAULT_AUTH_AMOUNT;
            $params['authType']     = 1; /* 1 for used auto authrize amount 2 for given manually amount */
            $paymentObject          = new Manage_payments($merchID);
            $isAuthorised           = $paymentObject->authoriseTransaction($params);
        }

        if ($isAuthorised) {
            $insert_array['CardCVV'] = '';
            if ($is_default == 1) {
                $condition = array('customerListID' => $customer, 'merchantID' => $merchID);

                $updateData = $this->card_model->update_customer_card_data($condition, ['is_default' => 0]);

            } else {
                $checkCustomerCard = $this->card_model->get_customer_card_data($customer);
                if (count($checkCustomerCard) == 0) {
                    $is_default == 1;
                }

            }
            $insert_array['is_default'] = $is_default;
            $id                         = $this->card_model->insertBillingdata($insert_array);
            if ($id) {
                $this->session->set_flashdata('success', 'Successfully Inserted Card');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }
        }

        redirect('Integration/Customers/customer_detail/' . $customer, 'refresh');
    }

    public function update_card_data()
	{
        $merchID = $this->merchantID;
		$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');

		$card_data = $this->card_model->get_single_card_data($cardID);
		$con_cust = array('Customer_ListID' => $card_data['customerListID'], 'merchantID' => $merchID);
		$c_data = $this->general_model->get_select_data('Xero_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
		$companyID  = $c_data['companyID'];
		$customer = $card_data['customerListID'];

		if ($card_data['CardType'] != 'Echeck') {

			$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
			$decryptedcvv  = $this->czsecurity->xssCleanPostInput('edit_cvv');
			$cvv      = '';
			$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
		}

		if ($this->czsecurity->xssCleanPostInput('edit_acc_number') !== '') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('edit_acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('edit_route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('edit_acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('edit_secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('edit_acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
			$expmonth = $exyear   = 	$cvv = 0;
			$card_no  = '';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
		}

		$cardID = $this->czsecurity->xsscleanpostinput('edit_cardID');

		$b_addr1 = $this->czsecurity->xsscleanpostinput('baddress1');
		$b_addr2 = $this->czsecurity->xsscleanpostinput('baddress2');
		$b_city = $this->czsecurity->xsscleanpostinput('bcity');
		$b_state = $this->czsecurity->xsscleanpostinput('bstate');
		$b_country = $this->czsecurity->xsscleanpostinput('bcountry');
		$b_contact = $this->czsecurity->xsscleanpostinput('bcontact');
		$b_zip = $this->czsecurity->xsscleanpostinput('bzipcode');
		$is_default = ($this->czsecurity->xsscleanpostinput('defaultMethod') != null )?$this->czsecurity->xsscleanpostinput('defaultMethod'):0;
		$merchantID = $merchID;

		$condition = array('CardID' => $cardID);
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CardCVV'      => $cvv,
			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zip,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'is_default'		 => $is_default,
			'updatedAt' 	=> date("Y-m-d H:i:s")
		);


		$decryptedCard = '';
		if($friendlyname != ''){
        	$insert_array['customerCardfriendlyName'] = $friendlyname;
        }

		if ($this->czsecurity->xsscleanpostinput('edit_card_number') != '') {
			$card_no  = $this->czsecurity->xsscleanpostinput('edit_card_number');
			$decryptedCard = $card_no;
			$insert_array['CustomerCard'] = $this->card_model->encrypt($card_no);
			$card_type = $this->general_model->getType($card_no);

			$friendlyname = $card_type . ' - ' . substr($card_no, -4);
			$insert_array['customerCardfriendlyName'] = $friendlyname;
			$insert_array['CardType'] = $card_type;
		} else {
			$decryptedCard     = $card_data['CardNo'];
		}

		$isAuthorised = true;

		if ($decryptedCard != '') {
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['fullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}
		
		if($isAuthorised){
			$insert_array['CardCVV'] = '';

			if($is_default == 1){
				$conditionDefaultSet = array('customerListID' => $customer, 'merchantID' => $merchID);
				$updateData = $this->card_model->update_customer_card_data($conditionDefaultSet,['is_default' => 0]);
			}
			$id = $this->card_model->update_card_data($condition,  $insert_array);
			//echo $this->db1->last_query(); die;
			if ($id) {
				$this->session->set_flashdata('success', 'Successfully Updated Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
		}

        redirect('Integration/Customers/customer_detail/' . $customer, 'refresh');
	}

    public function get_card_edit_data()
    {

        if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
            $cardID = $this->czsecurity->xssCleanPostInput('cardID');
            $data   = $this->card_model->get_single_mask_card_data($cardID);
            echo json_encode(array('status' => 'success', 'card' => $data));
            die;
        }
        echo json_encode(array('status' => 'success'));
        die;
    }

    public function get_card_data()
    {
        $customerdata = array();
        if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
            $crID      = $this->czsecurity->xssCleanPostInput('cardID');
            $card_data = $this->card_model->get_single_card_data($crID);
            if (!empty($card_data)) {
                $customerdata['status'] = 'success';
                $customerdata['card']   = $card_data;
                echo json_encode($customerdata);
                die;
            }
        }
    }

    public function delete_card_data()
    {
        $merchID = $this->merchantID;
        if (!empty($this->input->post('delCardID'))) {
            $cardID   = $this->czsecurity->xssCleanPostInput('delCardID');
            $customer = $this->czsecurity->xssCleanPostInput('delCustodID');
            $num      = $this->general_model->get_num_rows('tbl_subscriptions', array('CardID' => $cardID, 'merchantDataID' => $merchID));
            if ($num == 0) {
                $sts = $this->card_model->delete_card_data(array('CardID' => $cardID));
                if ($sts) {
                    $this->session->set_flashdata('success', 'Successfully Deleted');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Error: This card cannot be deleted. Please change Customer card on Subscription</strong></div>');
            }
            redirect('Integration/Customers/customer_detail/' . $customer, 'refresh');
        }
    }

    public function payment_transaction()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->sycnCustomer();
        $xeroSync->syncInvoices();
        $xeroSync->syncAccounts(['EnablePaymentsToAccount' => 1]);

        $data['transactions'] = $this->customer_model->get_transaction_history_data($user_id);

        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $user_id = $this->merchantID;

        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
            $this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
            if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {
                $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
            }

            $amount            = $this->czsecurity->xssCleanPostInput('totalamount');
            $invoiceIDs        = array();
            $invoicePayAmounts = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }
            $cardID = $this->czsecurity->xssCleanPostInput('card_list');

            $accountCode = '';
            if (!empty($invoiceIDs)) {
                $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);
                if (empty($accountData)) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>To accept invoice please select a default payment account in XERO</div>');
                    redirect('Integration/Payments/create_customer_sale', 'refresh');
                }
                $accountCode = $accountData['accountCode'];
            }

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            $transactionid = '';
            $payIndex      = 0;
            $checkPlan     = check_free_plan_transactions();
            if ($checkPlan && $this->form_validation->run() == true) {
                $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
                $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                if (!empty($gt_result)) {
                    $con_cust     = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
                    $customerData = $this->general_model->get_select_data('Xero_custom_customer', array('*'), $con_cust);

                    $paymentObj = new Manage_payments($this->merchantID);
                    if ($cardID != 'new1') {
                        $accountDetails = $this->card_model->get_single_card_data($cardID);
                    } else {
                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                        $cardType = $this->general_model->getType($card_no);
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                        $accountDetails = [
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $cardType,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'CardNo'          => $card_no,

                            'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                            'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                            'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                            'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                            'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                            'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                            'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                            'customerListID'  => $customerID,
                            'merchantID'      => $this->merchantID,
                            'createdAt'       => date("Y-m-d H:i:s"),
                        ];
                    }

                    $amount = $this->czsecurity->xssCleanPostInput('totalamount');

                    if (!empty($this->input->post('invoice_id'))) {
                        $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                        $invoiceIDs                           = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                    }

                    if (!empty($this->input->post('po_number'))) {
                        $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                    }

                    $saleData = [
                        'paymentDetails'    => $accountDetails,
                        'transactionByUser' => $this->transactionByUser,
                        'ach_type'          => 1,
                        'invoiceID'         => $this->czsecurity->xssCleanPostInput('invoice_id'),
                        'po_number'         => $this->czsecurity->xssCleanPostInput('po_number'),
                        'crtxnID'           => '',
                        'companyName'       => $customerData['companyName'],
                        'fullName'          => $customerData['fullName'],
                        'firstName'         => $customerData['firstName'],
                        'lastName'          => $customerData['lastName'],
                        'contact'           => $customerData['phoneNumber'],
                        'email'             => $customerData['userEmail'],
                        'amount'            => $amount,
                        'gatewayID'         => $gatlistval,
                        'returnResult'      => true,
                        'customerListID'    => $customerData['Customer_ListID'],
                    ];

                    // $saleData   = array_merge($saleData, $in_data);
                    $paidResult = $paymentObj->_processSaleTransaction($saleData);

                    $crtxnID = '';
                    if (isset($paidResult['response']) && $paidResult['response']) {
                        $invoiceIDs        = array();
                        $invoicePayAmounts = array();
                        if (!empty($this->input->post('invoice_id'))) {
                            $invoiceIDs        = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                            $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                        }

                        if (!empty($invoiceIDs)) {
                            $payIndex            = 0;
                            $saleAmountRemaining = $amount;
                            foreach ($invoiceIDs as $inID) {
                                $con  = array('invoiceID' => $inID, 'merchantID' => $user_id);
                                $res1 = $this->general_model->get_select_data('Xero_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment', 'AppliedAmount'), $con);

                                $refNumber[] = $res1['refNumber'];

                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];

                                $tr_date           = date('Y-m-d H:i:s');
                                $updateInvoiceData = [
                                    'invoiceID'       => $inID,
                                    'accountCode'     => $accountCode,
                                    'trnasactionDate' => $tr_date,
                                    'amount'          => $actualInvoicePayAmount,
                                ];

                                $xeroSync = new XeroSync($this->merchantID);
                                $crtxnID  = $xeroSync->createPayment($updateInvoiceData);

                                $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                $payIndex++;
                            }

                        } else {
                            $crtxnID             = '';
                            $inID                = '';
                            $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                        }

                        if ($chh_mail == '1') {
                            $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                            if (!empty($refNumber)) {
                                $ref_number = implode(',', $refNumber);
                            } else {
                                $ref_number = '';
                            }

                            $tr_date  = date('Y-m-d H:i:s');
                            $toEmail  = $customerData['userEmail'];
                            $company  = $customerData['fullName'];
                            $customer = $customerData['fullName'];
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                        }

                        if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                            $id1 = $this->card_model->process_card($accountDetails);
                        }

                        $this->session->set_flashdata('success', 'Transaction Successful');
                    } else {
                        $crtxnID             = '';
                        $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }

                    if (isset($storedTransactionId) && $storedTransactionId) {
                        $condition2         = array('id' => $storedTransactionId);
                        $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                        if ($storedTransactData) {
                            $transactionid = $storedTransactData['transactionID'];
                        }
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
                }
            } else {
                $error = 'Validation Error. Please fill the requred fields';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
            }

            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $transactionid,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'Integration/Payments/create_customer_sale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan,
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('Integration/Payments/transation_sale_receipt', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['merchID'] = $user_id;

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->sycnCustomer();

        $plantype           = $this->general_model->chk_merch_plantype_status($user_id);
        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }

        $condition           = array('merchantID' => $user_id);
        $gateway             = $this->general_model->get_gateway_data($user_id);
        $data['gateways']    = $gateway['gateway'];
        $data['gateway_url'] = $gateway['url'];
        $data['stp_user']    = $gateway['stripeUser'];

        $compdata          = $this->customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;
        $data['plantype']  = $plantype;
        //  print_r($data['customers']); die;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/payment_sale', $data);
        $this->load->view('comman-pages/popup_box', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            $merchantID = $this->merchantID;

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan  = check_free_plan_transactions();
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $cardID     = $this->czsecurity->xssCleanPostInput('card_list');

            $transactionid = '';
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $comp_data = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

                $paymentObj = new Manage_payments($this->merchantID);
                if ($cardID != 'new1') {
                    $accountDetails                 = $this->card_model->get_single_card_data($cardID);
                    $accountDetails['CustomerCard'] = $accountDetails['CardNo'];
                } else {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $cardType = $this->general_model->getType($card_no);
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                    $accountDetails = [
                        'cardMonth'       => $expmonth,
                        'cardYear'        => $exyear,
                        'CardType'        => $cardType,
                        'CustomerCard'    => $card_no,
                        'CardCVV'         => $cvv,
                        'CardNo'          => $card_no,

                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                        'customerListID'  => $customerID,
                        'merchantID'      => $this->merchantID,
                        'createdAt'       => date("Y-m-d H:i:s"),
                    ];
                }

                $amount = $this->czsecurity->xssCleanPostInput('totalamount');

                $authData                = $accountDetails;
                $authData['name']        = $comp_data['fullName'];
                $authData['amount']      = $amount;
                $authData['authType']    = 2; /* 1 for used auto authrize amount 2 for given manually amount */
                $authData['gatewayID']   = $gatlistval;
                $authData['storeResult'] = true;

                $paidResult = $paymentObj->authoriseTransaction($authData);
                if (isset($paidResult['response']) && $paidResult['response']) {
                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $id1 = $this->card_model->process_card($accountDetails);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                }

                if (isset($paidResult['id']) && $paidResult['id']) {
                    $condition2         = array('id' => $paidResult['id']);
                    $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                    if ($storedTransactData) {
                        $transactionid = $storedTransactData['transactionID'];
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $transactionid,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'Integration/Payments/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         => $checkPlan,
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('Integration/Payments/transation_sale_receipt', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['merchID'] = $user_id;

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->sycnCustomer();

        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }
        $plantype = $this->general_model->chk_merch_plantype_status($user_id);

        $condition           = array('merchantID' => $user_id);
        $gateway             = $this->general_model->get_gateway_data($user_id);
        $data['gateways']    = $gateway['gateway'];
        $data['gateway_url'] = $gateway['url'];
        $data['stp_user']    = $gateway['stripeUser'];
        $compdata            = $this->customer_model->get_customers_data($user_id);
        $data['customers']   = $compdata;
        $data['plantype']    = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/payment_auth', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function transation_sale_receipt()
    {

        $invoice_IDs  = $this->session->userdata('invoice_IDs');
        $receipt_data = $this->session->userdata('receipt_data');
        foreach ($receipt_data as $key => $rcData) {
            $receipt_data[$key] = strip_tags($rcData);
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $transactionCode     = 0;
        $surCharge           = 0;
        $isSurcharge         = 0;
        $totalAmount         = 0;
        $transactionType     = 0;

        $user_id            = $this->merchantID;
        $data['login_info'] = $this->logged_in_data;

        $data['transactionAmount'] = '';
        $data['transactionCode']   = 0;
        $pay_amount = [];
        if (isset($receipt_data['transaction_id']) && !empty($receipt_data['transaction_id'])) {
            $transaction_id = $receipt_data['transaction_id'];
            $transactionRowID = isset($receipt_data['transactionRowID'])?$receipt_data['transactionRowID']:'';
            $con            = array('transactionID' => $transaction_id);
            $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount');
            $this->db->from('customer_transaction tr');
            $this->db->where($con);
            if(isset($transactionRowID) && !empty($transactionRowID)){
                $this->db->where("tr.id",$transactionRowID);
            }
            $this->db->group_by("tr.transactionID");
            $pay_amount = $this->db->get()->row_array();
            if ($pay_amount) {
                $data['transactionAmount'] = $pay_amount['transactionAmount'];

                $transactionType = $pay_amount['transactionGateway'];

                if ($pay_amount['transactionGateway'] == 10) {

                    $resultAmount = getiTransactTransactionDetails($user_id, $transaction_id);
                    $totalAmount  = $resultAmount['totalAmount'];
                    $surCharge    = $resultAmount['surCharge'];
                    $isSurcharge  = $resultAmount['isSurcharge'];
                    if ($resultAmount['payAmount'] != 0) {
                        $data['transactionAmount'] = $resultAmount['payAmount'];
                    }
                }

                $data['transactionCode'] = $pay_amount['transactionCode'];
            }

        }

        $data['Ip']    = getClientIpAddr();
        $data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
        $data['name']  = $this->session->userdata('logged_in')['firstName'] . ' ' . $this->session->userdata('logged_in')['lastName'];

        $data['customer_data'] = $receipt_data;
        $invoice_data          = [];
        if (!empty($invoice_IDs)) {
            foreach ($invoice_IDs as $inID) {
                $condition2     = array('invoiceID' => $inID, 'merchantID' => $user_id);
                $invoice_data[] = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
            }
        }

        if (isset($receipt_data['refundAmount']) && !empty($receipt_data['refundAmount'])) {
            $data['transactionAmount'] = $receipt_data['refundAmount'];
        }
        $data['surchargeAmount'] = $surCharge;
        $data['totalAmount']     = $totalAmount;
        $data['transactionType'] = $transactionType;
        $data['isSurcharge']     = $isSurcharge;
        $data['invoice_IDs']     = $invoice_IDs;
        $data['invoice_data']    = $invoice_data;
        $data['transactionDetail'] = $pay_amount;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/transaction_receipt', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function transation_credit_receipt($invoiceId = "null", $customer_id = "null", $trans_id = "null")
    {

        $page_data = $this->session->userdata('receipt_data');
        $page_data['transaction_id'] = $trans_id;
        $this->session->set_userdata("receipt_data", $page_data);
        foreach ($page_data as $key => $rcData) {
            $page_data[$key] = strip_tags($rcData);
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['page_data']   = $page_data;

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['transaction_id'] = $trans_id;
        $data['Ip']             = getClientIpAddr();
        $data['email']          = $this->session->userdata('logged_in')['merchantEmail'];
        $data['name']           = $this->session->userdata('logged_in')['firstName'] . ' ' . $this->session->userdata('logged_in')['lastName'];

        $data['invoice']        = '';
        $data['invoice_number'] = '';

        $condition3            = array('Customer_ListID' => $customer_id, 'merchantID' => $user_id);
        $customer_data         = $this->general_model->get_row_data('Xero_custom_customer', $condition3);
        $customer_data = convertCustomerDataFieldName($customer_data,4);
        $data['customer_data'] = $customer_data;
        $data['invoice_data']  = $customer_data;
        if (isset($invoiceId) && !empty($invoiceId) && $invoiceId != 'transaction') {
            $in_data                = $this->data_model->get_invoice_data_pay($invoiceId, $user_id);
            $data['invoice']        = $invoiceId;
            $data['invoice_number'] = $in_data['refNumber'];
        }

        $surCharge       = 0;
        $isSurcharge     = 0;
        $totalAmount     = 0;
        $transactionType = 0;

        $invoice_IDs  = [];
        $invoice_data = [];
        if ($trans_id != null) {
            $condition4                = array('transactionID' => $trans_id, 'merchantID' => $user_id, 'customerListID' => $customer_id);
            if($page_data['sub_header'] == 'Capture'){
                $condition4['transaction_user_status'] = 1;
            }
            $transactionData           = $this->general_model->get_row_data('customer_transaction', $condition4);


            $data['transactionDetail'] = $transactionData;
            $data['transactionAmount'] = $transactionData['transactionAmount'];
            $data['transactionCode']   = $transactionData['transactionCode'];
            $transactionType           = $transactionData['transactionGateway'];

            /*Invoice Set*/
            $invoiceArray = json_decode($transactionData['custom_data_fields']);
            $invoiceStr   = '';

            if (!empty($invoiceArray)) {

                $invoiceStr  = $invoiceArray->invoice_number;
                $invoice_IDs = explode(',', $invoiceStr);
            }

            if (!empty($invoice_IDs)) {
                foreach ($invoice_IDs as $inID) {
                    $condition2     = array('invoiceID' => $inID);
                    $invoice_data[] = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
                }
            }
            $data['invoice_IDs']  = $invoice_IDs;
            $data['invoice_data'] = $invoice_data;
        } else {
            $data['transactionAmount'] = 0;
            $data['transactionCode']   = 0;
            $data['invoice_IDs']       = $invoice_IDs;
            $data['invoice_data']      = $invoice_data;
        }

        if (isset($transactionType)) {
            if ($transactionType == 10) {

                $resultAmount = getiTransactTransactionDetails($user_id, $trans_id);
                $totalAmount  = $resultAmount['totalAmount'];
                $surCharge    = $resultAmount['surCharge'];
                $isSurcharge  = $resultAmount['isSurcharge'];
                if ($resultAmount['payAmount'] != 0) {
                    $data['transactionAmount'] = $resultAmount['payAmount'];
                }
            }
        }
        $data['surchargeAmount'] = $surCharge;
        $data['totalAmount']     = $totalAmount;
        $data['transactionType'] = $transactionType;
        $data['isSurcharge']     = $isSurcharge;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/transaction_proccess_receipt', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function payment_capture()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['transactions'] = $this->customer_model->get_transaction_data_captue($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/payment_capture', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /*****************Capture Transaction***************/
    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $transactionID     = $this->czsecurity->xssCleanPostInput('txnID1');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);
            if ($paydata['gatewayID'] > 0) {

                $gatlistval = $paydata['gatewayID'];

                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                $customerID = $paydata['customerListID'];
                $amount     = $paydata['transactionAmount'];
                $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
                $comp_data  = $this->general_model->get_select_data('Xero_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

                $captureData = [
                    'transactionID'     => $transactionID,
                    'amount'            => $amount,
                    'gatewayID'         => $paydata['gatewayID'],
                    'customerListID'    => $customerID,
                    'storeResult'       => true,
                    'returnResult'      => true,
                    'transactionByUser' => $this->transactionByUser,
                ];
                $paymentObj = new Manage_payments($this->merchantID);
                $paidResult = $paymentObj->_captureTransaction($captureData);

                if (isset($paidResult['response']) && $paidResult['response']) {
                    $this->session->set_flashdata('success', 'Successfully Captured Authorization');

                    if ($chh_mail == '1') {
                        $customerID = $paydata['customerListID'];
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['userEmail'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['fullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
                    }
                }

                if (isset($paidResult['id']) && $paidResult['id']) {
                    $condition2         = array('id' => $paidResult['id']);
                    $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                    if ($storedTransactData) {
                        $transactionID = $storedTransactData['transactionID'];
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Invalid Request Details</strong></div>');
            }
            $invoice_IDs  = array();
            $receipt_data = array(
                'proccess_url'      => 'Integration/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if ($transactionID == '') {
                $transactionID = 'null';
            }
            redirect('Integration/Payments/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionID, 'refresh');
        }
        redirect('Integration/Payments/payment_capture', 'refresh');
    }

    /*****************Refund Transaction***************/
    public function payment_refund()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['transactions'] = $this->customer_model->get_transaction_datarefund($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/payment_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /*****************Esale Transaction***************/
    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        if (!empty($this->input->post())) {
            $merchantID = $this->merchantID;

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->input->post('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                $invoiceIDs                           = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
            $name   = $this->czsecurity->xssCleanPostInput('firstName') . " " . $this->czsecurity->xssCleanPostInput('lastName');
            $responseId = '';
            $checkPlan = check_free_plan_transactions();

            $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);

            if ($checkPlan && $gatlistval != "" && !empty($gt_result) && !empty($accountData)) {
                $customerID   = $this->czsecurity->xssCleanPostInput('customerID');

                $customerData = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                
                $saleData = [
                    'paymentDetails'    => $accountDetails,
                    'transactionByUser' => $this->transactionByUser,
                    'ach_type'          => 2,
                    'invoiceID'         => $this->czsecurity->xssCleanPostInput('invoice_id'),
                    'po_number'         => $this->czsecurity->xssCleanPostInput('po_number'),
                    'crtxnID'           => '',
                    'companyName'       => $customerData['companyName'],
                    'fullName'          => $customerData['fullName'],
                    'firstName'         => $customerData['firstName'],
                    'lastName'          => $customerData['lastName'],
                    'contact'           => $customerData['phoneNumber'],
                    'email'             => $customerData['userEmail'],
                    'amount'            => $amount,
                    'gatewayID'         => $gatlistval,
                    'returnResult'      => true,
                    'customerListID'    => $customerData['Customer_ListID'],
                ];

                $paymentObj = new Manage_payments($this->merchantID);
                $paidResult = $paymentObj->_processSaleTransaction($saleData);
                // echo '<pre>';
                // print_r($paidResult);die;
                $crtxnID = '';
                if (isset($paidResult['response']) && $paidResult['response']) {
                    $invoiceIDs        = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs        = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    if (!empty($invoiceIDs)) {
                        $payIndex            = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $con  = array('invoiceID' => $inID, 'merchantID' => $user_id);
                            $res1 = $this->general_model->get_select_data('Xero_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment', 'AppliedAmount'), $con);

                            $refNumber[] = $res1['refNumber'];

                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];

                            $tr_date           = date('Y-m-d H:i:s');
                            $updateInvoiceData = [
                                'invoiceID'       => $inID,
                                'accountCode'     => $accountData['accountCode'],
                                'trnasactionDate' => $tr_date,
                                'amount'          => $actualInvoicePayAmount,
                            ];

                            $xeroSync = new XeroSync($this->merchantID);
                            $crtxnID  = $xeroSync->createPayment($updateInvoiceData);

                            $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                            $payIndex++;
                        }

                    } else {
                        $crtxnID             = '';
                        $inID                = '';
                        $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($chh_mail == '1') {
                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                        if (!empty($refNumber)) {
                            $ref_number = implode(',', $refNumber);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $customerData['userEmail'];
                        $company  = $customerData['companyName'];
                        $customer = $customerData['fullName'];
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                    }

                    if ($payableAccount == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $crtxnID             = '';
                    $storedTransactionId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                }

                if (isset($storedTransactionId) && $storedTransactionId) {
                    $condition2         = array('id' => $storedTransactionId);
                    $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                    if ($storedTransactData) {
                        $responseId = $storedTransactData['transactionID'];
                    }
                }

            } else {
                $errorMessage = " Please select gateway.";
                if (empty($accountData)) {
                    $errorMessage = "Please select a default payment account";
                }
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$errorMessage.'</strong></div>');
            }
            
            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'Integration/Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('Integration/Payments/transation_sale_receipt', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['page_name']   = "NMI ESale";

        $data['merchID'] = $user_id;

        $xeroSync = new XeroSync($this->merchantID);
        $xeroSync->sycnCustomer();

        $condition = array('merchantID' => $user_id);
        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }

        $gateway             = $this->general_model->get_gateway_data($user_id, 'echeck');
        $data['gateways']    = $gateway['gateway'];
        $data['gateway_url'] = $gateway['url'];

        $compdata          = $this->customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/payment_esale', $data);
        $this->load->view('comman-pages/popup_box', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /*********ECheck Transactions**********/
	public function evoid_transaction()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

		$data['transactions']    = $this->customer_model->get_transaction_data_erefund($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('comman-pages/payment_ecapture', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

    public function echeck_transaction()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

		$data['transactions']    = $this->customer_model->get_transaction_data_erefund($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('comman-pages/payment_erefund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
}
