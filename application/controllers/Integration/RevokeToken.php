<?php
/**
 * RevokeToken Class
 */
class RevokeToken extends CI_Controller
{
    /**
     * Reseller value
     */
    private $resellerID;

    /**
     * Merchant value
     */
    private $merchantID;

    /**
     * Logged in data
     */
    private $loggedInData;
    
    /**
     * Constructor of the class
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('general');

        $this->load->model('Common/customer_model');
        $this->load->model('Common/company_model');
        $this->load->model('Common/data_model');

        $this->load->model('general_model');
        $this->load->model('common_model');

        if ($this->session->userdata('logged_in') != "") {
            $loggedInData          = $this->session->userdata('logged_in');
            $this->resellerID        = $loggedInData['resellerID'];
            $merchID                 = $loggedInData['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $loggedInData                  = $this->session->userdata('user_logged_in');
            $merchID                         = $loggedInData['merchantID'];
            $loggedInData['merchantEmail'] = $loggedInData['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $loggedInData;
    }

    /**
     * Function to destroy the XERO Aauth Token
     * 
     * @return void
     */
    public function index()
    {
        $tokenData = $this->general_model->get_row_data('tbl_xero_token', array('merchantID' => $this->merchantID));
        if(empty($tokenData)){
            $this->session->set_flashdata('success', "Token not found");
            redirect('Integration/home/company', 'refresh');
        }

        $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken((string) $tokenData['access_token']);       
        $identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
            new GuzzleHttp\Client(),
            $config
        );

        try {
            $result = $identityApi->deleteConnectionWithHttpInfo($tokenData['xero_connection_id']);
            if($result){
                $this->session->set_flashdata('success', "Account Disconnected");
            } else {
                $this->session->set_flashdata('success', "SomeThing went wrong");
            }
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            $this->session->set_flashdata('success', "SomeThing went wrong");
        }
        redirect('Integration/home/company', 'refresh');
    }
}
