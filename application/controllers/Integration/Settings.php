<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Settings extends CI_Controller
{

    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('general');
        $this->load->helper('form');

        $this->load->model('Common/customer_model');
        $this->load->model('Common/company_model');
        $this->load->model('Common/data_model');

        $this->load->model('general_model');
        $this->load->model('common_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function index()
    {
        redirect(base_url('login'), 'refresh');
    }

    //-------------- To load the API Key page ---------------//
    public function apikey()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['api_data'] = $this->general_model->get_table_data('tbl_merchant_api', array('MerchantID' => $user_id));
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/APIKey', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_api_key()
    {

        if (!empty($this->input->post('api_name')) && !empty($this->input->post('domain_name'))) {
            $data['login_info'] = $this->logged_in_data;
            $user_id            = $this->merchantID;

            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchID,companyName, merchantEmail'), array('merchID' => $user_id));

            $name   = $this->czsecurity->xssCleanPostInput('api_name');
            $domain = $this->czsecurity->xssCleanPostInput('domain_name');

            $dev_api_key = $this->general_model->get_random_number($m_data, 'DEV');
            $pro_api_key = $this->general_model->get_random_number($m_data, 'PRO');

            $api_data['MerchantID'] = $user_id;
            $api_data['APIKeyDev']  = $dev_api_key;
            $api_data['APIKeyPro']  = $pro_api_key;
            $api_data['AppName']    = $name;
            $api_data['Domain']     = $domain;
            $api_data['Status']     = 1;
            $api_data['APIPackage'] = 2;
            $indid                  = $this->general_model->insert_row('tbl_merchant_api', $api_data);
            if ($indid) {
                $this->session->set_flashdata('success', 'Successfully Created');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');
        }
        redirect('Integration/Settings/apikey', 'refresh');
    }

    public function delete_api_key()
    {
        if (!empty($this->input->post('apiID'))) {
            $data['login_info'] = $this->logged_in_data;
            $user_id            = $this->merchantID;

            $apiID  = $this->czsecurity->xssCleanPostInput('apiID');
            $m_data = $this->general_model->get_row_data('tbl_merchant_api', array('MerchantID' => $user_id, 'apiID' => $apiID));

            if (!empty($m_data)) {

                if ($this->general_model->delete_row_data('tbl_merchant_api', array('MerchantID' => $user_id, 'apiID' => $apiID))) {
                    $indid = $this->card_model->delete_dev_api_key(array('MerchantID' => $user_id, 'APIKeyDev' => $m_data['APIKeyDev']));
                    $this->session->set_flashdata('success', 'Success');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error</strong></div>');
                }

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error, Invalid API Key</strong></div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');
        }

        redirect('Integration/Settings/apikey', 'refresh');

    }

    public function terms()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->logged_in_data;
        $merchant_id         = $this->merchantID;

        $condition = array('merchantID' => $merchant_id);

        $data['netterms'] = $this->common_model->get_terms_data($merchant_id);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_terms', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_terms()
    {

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $termID = $this->czsecurity->xssCleanPostInput('termID');
        $dfid   = $this->czsecurity->xssCleanPostInput('dfID');

        $name   = $this->czsecurity->xssCleanPostInput('termname');
        $neterm = $this->czsecurity->xssCleanPostInput('netterm');
        $today  = date('Y-m-d H:i:s');

        $c_cond = array("pt_netTerm" => $neterm, "pt_name" => $name, "merchantID" => $user_id, "enable" => 0);

        $check = $this->common_model->check_payment_term($name, $neterm, $user_id);
        if (empty($check)) {
            if ($dfid == "") {

                $data = array(
                    "termID"     => 0,
                    "pt_name"    => $name,
                    "pt_netTerm" => $neterm,
                    "date"       => $today,
                    "merchantID" => $user_id,
                    "enable"     => 0,
                );

            } else {
                $data = array(
                    "termID"     => $dfid,
                    "pt_name"    => $name,
                    "pt_netTerm" => $neterm,
                    "date"       => $today,
                    "merchantID" => $user_id,
                    "enable"     => 0,
                );
            }

            if ($termID == "") {

                $table = $this->general_model->insert_row('tbl_payment_terms', $data);
                $this->session->set_flashdata('success', 'Successfully Inserted');
            } else {
                $condition = array("id" => $termID);
                $table     = $this->general_model->update_row_data('tbl_payment_terms', $condition, $data);
                $this->session->set_flashdata('success', 'Successfully Updated');
            }

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Payment term already exists.</div>');

        }

        redirect('Integration/Settings/terms', 'refresh');
    }

    public function get_termedit_id()
    {

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;
        $termID             = $this->czsecurity->xssCleanPostInput('termID');
        $cond               = array("merchantID" => $user_id, "termID" => $termID);
        $term               = $this->general_model->get_row_data('tbl_payment_terms', $cond);
        echo json_encode($term);
    }

    public function delete_term()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $termID = $this->czsecurity->xssCleanPostInput('dtermID');
        $dfID   = $this->czsecurity->xssCleanPostInput('del_dfID');

        if ($dfID == "") {
            $cond = array("merchantID" => $user_id, "id" => $termID);
            $term = $this->general_model->delete_row_data('tbl_payment_terms', $cond);

            $this->session->set_flashdata('success', ' Successfully Deleted');
        } else if ($dfID != "" && $termID != "") {

            $condition = array("id" => $termID);
            $data      = array("enable" => 1);
            $cond      = array("merchantID" => $user_id, "id" => $termID);
            $table     = $this->general_model->update_row_data('tbl_payment_terms', $condition, $data);

            $this->session->set_flashdata('success', ' Successfully Updated');
        } else {
            $data  = array("termID" => $dfID, "merchantID" => $user_id, "enable" => 1);
            $table = $this->general_model->insert_row('tbl_payment_terms', $data);
            $this->session->set_flashdata('success', 'Successfully Inserted');
        }
        redirect('Integration/Settings/terms', 'refresh');
    }

    public function set_default_terms()
    {

        if (!empty($this->input->post('termid'))) {

            $data['login_info'] = $this->logged_in_data;
            $merchID            = $this->merchantID;

            $id  = $this->czsecurity->xssCleanPostInput('termid');
            $val = array(
                'id' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );

            $update_data1 = array('set_as_default' => '0');
            $this->general_model->update_row_data('tbl_payment_terms', $val1, $update_data1);
            $update_data = array('set_as_default' => '1', 'date' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_payment_terms', $val, $update_data);
            $this->session->set_flashdata('success', 'Successfully Updated');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
        }
        redirect(base_url('Integration/Settings/terms'));

    }

    public function profile_setting()
    {
        $data['login_info'] = $da['login_info'] = $this->logged_in_data;
        $user_ID            = $user_id            = $this->merchantID;

        if (!empty($this->input->post())) {

            $input_data['merchantFullAddress'] = $this->czsecurity->xssCleanPostInput('merchantFullAddress');
            $input_data['weburl']              = $this->czsecurity->xssCleanPostInput('weburl');

            $input_data['firstName']     = $this->czsecurity->xssCleanPostInput('firstName');
            $input_data['lastName']      = $this->czsecurity->xssCleanPostInput('lastName');
            $input_data['merchantEmail'] = $this->czsecurity->xssCleanPostInput('merchantEmail');

            $input_data['merchantAddress1']          = $this->czsecurity->xssCleanPostInput('merchantAddress1');
            $input_data['companyName']               = $this->czsecurity->xssCleanPostInput('companyName');
            $input_data['merchantContact']           = $this->czsecurity->xssCleanPostInput('merchantContact');
            $input_data['merchantAddress2']          = $this->czsecurity->xssCleanPostInput('merchantAddress2');
            $input_data['merchantCountry']           = $this->czsecurity->xssCleanPostInput('country');
            $input_data['merchantState']             = $this->czsecurity->xssCleanPostInput('state');
            $input_data['merchantCity']              = $this->czsecurity->xssCleanPostInput('city');
            $input_data['merchantZipCode']           = $this->czsecurity->xssCleanPostInput('merchantZipCode');
            $input_data['merchantAlternateContact']  = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
            $input_data['updatedAt']                 = date('Y-m-d H:i:s');
            $input_data['merchant_default_timezone'] = $this->czsecurity->xssCleanPostInput('merchant_default_timezone');

            if ($input_data['merchant_default_timezone']) {
                $session_data = $this->session->userdata('logged_in');
                if ($session_data) {
                    $session_data['merchant_default_timezone'] = $input_data['merchant_default_timezone'];
                    $this->session->set_userdata('logged_in', $session_data);
                } else {
                    $session_data = $this->session->userdata('user_logged_in');
                    if ($session_data) {
                        $session_data['merchant_default_timezone'] = $input_data['merchant_default_timezone'];
                        $this->session->set_userdata('user_logged_in', $session_data);
                    }
                }
            }

            $pre   = $this->czsecurity->xssCleanPostInput('prefix');
            $post  = $this->czsecurity->xssCleanPostInput('postfix');
            $array = array($pre, $post);
            $qq    = implode('-', $array);
            $data  = array(

                'prefix'     => $pre,
                'postfix'    => $post,
                'invoiceNo'  => $qq,
                'merchantID' => $user_ID,

            );

            if (!empty($this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_ID)))) {
                $condition = array('merchantID' => $user_ID);
                $this->general_model->update_row_data('tbl_merchant_invoices', $condition, $data);
            } else {
                $condition = array('merchantID' => $user_ID);
                $this->general_model->insert_row('tbl_merchant_invoices', $data);
            }

            $id        = $this->czsecurity->xssCleanPostInput('merchID');
            $condition = array('merchID' => $user_ID);

            $status = $this->general_model->update_row_data('tbl_merchant_data', $condition, $input_data);
            if ($status) {
                $input_data['merchID'] = $user_ID;

                $merchantData = $this->general_model->get_row_data('tbl_merchant_data', $condition);
                if (ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23) {
                    /* Start campaign in hatchbuck CRM*/
                    $this->load->library('hatchBuckAPI');
                    //$merchant_condition = ['merchID' => $merchantID];
                    $input_data['merchant_type'] = 'Merchant Update';

                    $resource = $this->hatchbuckapi->createContact($input_data);
                    if ($status['statusCode'] == 400) {

                        $resource = $this->hatchbuckapi->updateContact($input_data);
                    } else {
                        //$input_data[] = $contact_id;
                        $resource = $this->hatchbuckapi->updateContact($input_data);
                    }
                    /* End campaign in hatchbuck CRM*/
                }
                $this->session->set_flashdata('success', 'Successully Updated');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong></div>');

            }
            redirect('Integration/Settings/profile_setting', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $con = array('merchID' => $user_id);
        $fix = $this->general_model->get_select_data('tbl_merchant_data', array('merchID', 'merchantAlternateContact', 'firstName', 'lastName', 'merchantEmail', 'merchantAddress1', 'merchantAddress2', 'merchantZipCode', 'weburl', 'companyName', 'merchantContact', 'merchantState', 'merchantCity', 'merchantCountry', 'merchant_default_timezone'), $con);

        $invoice['invoice'] = $fix;
        $invoice['prefix']  = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));

        $country               = $this->general_model->get_table_data('country', '');
        $data['country_datas'] = $country;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_general_setting', $invoice);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function email_temlate()
    {
        if ($this->session->userdata('logged_in')['merchID'] != "" || $this->session->userdata('user_logged_in')['merchantID'] != "") {
            $data['primary_nav'] = primary_nav();
            $data['template']    = template_variable();

            $data['login_info'] = $da['login_info'] = $this->logged_in_data;
            $user_id            = $this->merchantID;

            $codition                 = array('merchantID' => $user_id);
            $templates                = $this->data_model->template_data_list($codition);
            $codition1                = array('merchantID' => $user_id, 'systemMail' => 0);
            $data['custom_templates'] = $this->data_model->custom_template_data_list($codition1);

            $data['templates'] = prepareMailList($templates, $user_id);

            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);
            $this->load->view('common-integration/page_emails', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);
        } else {
            redirect('Integration/Settings/email_temlate', 'refresh');
        }
    }

    public function create_template()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        if (!empty($this->input->post())) {
            $tempID = '';
            if ($this->czsecurity->xssCleanPostInput('tempID') == "") {
                $this->form_validation->set_rules('templateName', 'Template Name', 'trim|required|callback_check_custom_template_name|xss-clean');
            }
            $this->form_validation->set_rules('emailSubject', 'Email Subject', 'required|xss_clean');
            $this->form_validation->set_rules('fromEmail', 'From Email', 'required|xss_clean');
            $tempID = $this->czsecurity->xssCleanPostInput('tempID');
            if ($this->form_validation->run() == true) {

                $templateName = $this->czsecurity->xssCleanPostInput('templateName');

                $fromEmail       = $this->czsecurity->xssCleanPostInput('fromEmail');
                $toEmail         = $this->czsecurity->xssCleanPostInput('toEmail');
                $addCC           = $this->czsecurity->xssCleanPostInput('ccEmail');
                $addBCC          = $this->czsecurity->xssCleanPostInput('bccEmail');
                $replyTo         = $this->czsecurity->xssCleanPostInput('replyEmail');
                $message         = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', true);
                $subject         = $this->czsecurity->xssCleanPostInput('emailSubject');
                $mailDisplayName = $this->czsecurity->xssCleanPostInput('mailDisplayName');
                $createdAt       = date('Y-m-d H:i:s');

                if ($this->czsecurity->xssCleanPostInput('add_attachment')) {
                    $add_attachment = '1';
                } else {
                    $add_attachment = '0';
                }

                $insert_data = array(
                    'templateName'    => $templateName,

                    'merchantID'      => $user_id,
                    'fromEmail'       => $fromEmail,
                    'toEmail'         => $toEmail,
                    'addCC'           => $addCC,
                    'addBCC'          => $addBCC,
                    'replyTo'         => $replyTo,
                    'message'         => $message,
                    'emailSubject'    => $subject,
                    'attachedTo'      => $add_attachment,
                    'mailDisplayName' => $mailDisplayName,
                );
                $update_data = array(
                    'templateName'    => $templateName,
                    'merchantID'      => $user_id,
                    'fromEmail'       => $fromEmail,
                    'toEmail'         => $toEmail,
                    'addCC'           => $addCC,
                    'addBCC'          => $addBCC,
                    'replyTo'         => $replyTo,
                    'message'         => $message,
                    'emailSubject'    => $subject,
                    'attachedTo'      => $add_attachment,
                    'mailDisplayName' => $mailDisplayName,
                );

                if ($this->czsecurity->xssCleanPostInput('tempID') != "") {

                    $update_data['updatedAt'] = date('Y-m-d H:i:s');
                    $condition                = array('templateID' => $this->czsecurity->xssCleanPostInput('tempID'));
                    $data['templatedata']     = $this->general_model->update_row_data('tbl_email_template', $condition, $update_data, ['message']);
                } else {

                    $insert_data['systemMail'] = 0;
                    $insert_data['createdAt']  = date('Y-m-d H:i:s');
                    $id                        = $this->general_model->insert_row('tbl_email_template', $insert_data, ['message']);
                }
                redirect('Integration/Settings/Create_template/' . $tempID, 'refresh');
            } else {
                if (form_error('templateName') != "") {
                    $error['templateName'] = form_error('templateName');
                }
                if (form_error('emailSubject') != "") {
                    $error['emailSubject'] = form_error('emailSubject');
                }
                if (form_error('fromEmail') != "") {
                    $error['fromEmail'] = form_error('fromEmail');
                }

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> ' . implode(',', $error) . ' </div>');
            }
            redirect('Integration/Settings/Create_template/' . $tempID, 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['from_mail']       = DEFAULT_FROM_EMAIL;
        $data['mailDisplayName'] = $this->logged_in_data['companyName'];

        if ($this->uri->segment('4')) {
            $temID                = $this->uri->segment('4');
            $condition            = array('templateID' => $temID);
            $templatedata         = $this->general_model->get_row_data('tbl_email_template', $condition);
            $data['templatedata'] = $templatedata;
            if ($templatedata && !empty($templatedata['fromEmail'])) {
                $data['from_mail'] = $templatedata['fromEmail'];
            }

            if ($templatedata && !empty($templatedata['mailDisplayName'])) {
                $data['mailDisplayName'] = $templatedata['mailDisplayName'];
            }
        }

        $data['types'] = $this->general_model->get_table_data('tbl_teplate_type', '');

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_email_template', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function reset_email_template()
    {
        $merchant_id   = $this->merchantID;
        $templatedatas = $this->general_model->get_table_data('tbl_email_template_data', '');

        foreach ($templatedatas as $templatedata) {

            $insert_data = array(
                'templateName' => $templatedata['templateName'],
                'templateType' => $templatedata['templateType'],
                'merchantID'   => $merchant_id,
                'fromEmail'    => DEFAULT_FROM_EMAIL,
                'message'      => $templatedata['message'],
                'emailSubject' => $templatedata['emailSubject'],
                'createdAt'    => date('Y-m-d H:i:s'),
            );

            $emailCondition = [
                'templateType' => $templatedata['templateType'],
                'merchantID'   => $merchant_id,
            ];

            $emailData = $this->general_model->get_row_data('tbl_email_template', $emailCondition);
            if ($emailData && !empty($emailData)) {
                $this->general_model->update_row_data('tbl_email_template', $emailCondition, $insert_data);
            } else {
                $this->general_model->insert_row('tbl_email_template', $insert_data);
            }
        }

        $this->session->set_flashdata('success', 'Successfully Reset Templates');
        redirect('Integration/Settings/email_temlate');
    }

    public function tax_list()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $condition     = ['merchantID' => $user_id];
        $taxname       = $this->data_model->get_tax_data($condition);
        $data['taxes'] = $taxname;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_taxes', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function setting_customer_portal()
    {

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $codition   = array('merchantID' => $user_id);
        $rootDomain = "https://demo.payportal.com/";

        if ($this->czsecurity->xssCleanPostInput()) {
            $enable      = $this->czsecurity->xssCleanPostInput('enable');
            $portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');
            $service_url = $this->czsecurity->xssCleanPostInput('service_url');
            //print_r($portal_url);die;
            $hepltext = $this->czsecurity->xssCleanPostInput('customer_help_text');
            $myInfo   = $this->czsecurity->xssCleanPostInput('myInfo');
            $myPass   = $this->czsecurity->xssCleanPostInput('myPass');
            $mySubsc  = $this->czsecurity->xssCleanPostInput('mySubscriptions');
            $image    = '';

            if (!empty($_FILES['picture']['name'])) {
                $config['image_library']  = 'uploads';
                $config['upload_path']    = 'uploads/merchant_logo/';
                $config['allowed_types']  = 'jpg|jpeg|png|gif';
                $config['file_name']      = time() . $_FILES['picture']['name'];
                $config['create_thumb']   = false;
                $config['maintain_ratio'] = false;
                $config['quality']        = '60%';
                $config['widht']          = LOGOWIDTH;
                $config['height']         = LOGOHEIGHT;
                $config['new_image']      = 'uploads/merchant_logo/';

                //Load upload library and initialize configuration
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('picture')) {

                    $uploadData = $this->upload->data();
                    $picture    = $picture    = $uploadData['file_name'];
                } else {
                    $picture = '';
                }
                $image = $picture;
            }
            $url = "https://" . $portal_url . CUS_PORTAL . "/customer/";

            if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
                if (strpos($portal_url, ' ') > 0) {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error: Invalid portal url space not allowed.</strong></div>');
                    redirect('Integration/home/index', 'refresh');
                }

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error: Invalid portal url special character not allowed.</strong></div>');
                redirect('Integration/home/index', 'refresh');
            }

            $insert_data = array('customerPortal' => $enable, 'customerPortalURL' => $url, 'customerHelpText' => $hepltext, 'serviceurl' => $service_url, 'showInfoTab' => $myInfo, 'showPassword' => $myPass, 'showSubscription' => $mySubsc, 'merchantID' => $user_id, 'portalprefix' => $portal_url);

            $update_data = array('customerPortal' => $enable, 'customerPortalURL' => $url, 'customerHelpText' => $hepltext, 'serviceurl' => $service_url, 'showInfoTab' => $myInfo, 'showPassword' => $myPass, 'showSubscription' => $mySubsc, 'portalprefix' => $portal_url);
            // print_r($update_data); die;
            if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $user_id)) > 0) {

                $Edit = $this->general_model->check_existing_edit_portalprefix('tbl_config_setting', $user_id, $portal_url);

                if ($Edit) {
                    $this->session->set_flashdata('message', 'Error: Customer Portal URL already exitsts. Please try again.');

                } else {

                    if ($image != "") {
                        $update_data['ProfileImage'] = $image;
                    }

                    $this->general_model->update_row_data('tbl_config_setting', $codition, $update_data);
                    $this->session->set_flashdata('success', 'Successfully Updated');
                }

                redirect(base_url('Integration/Settings/setting_customer_portal'), 'refresh');

            } else {
                $check = $this->general_model->check_existing_portalprefix($portal_url);

                if ($check) {
                    $this->session->set_flashdata('message', 'Error: Customer Portal URL already exitsts. Please try again.');

                } else {
                    if ($image != "") {
                        $insert_data['ProfileImage'] = $image;
                    }

                    $this->general_model->insert_row('tbl_config_setting', $insert_data);

                    $this->session->set_flashdata('success', 'successfully Inserted');
                }

                redirect(base_url('SettingConfig/setting_customer_portal'), 'refresh');

            }
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['setting'] = $this->general_model->get_row_data('tbl_config_setting', $codition);

        if ($data['setting']['serviceurl'] == '') {
            $merchant                      = $this->general_model->get_select_data('tbl_merchant_data', array('weburl'), array('merchID' => $user_id));
            $data['setting']['serviceurl'] = $merchant['weburl'];
        }

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_customer_portal', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /********** Add, Update, Edit and List Admin Roles ********/
    public function admin_role()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];
        //$conditin    =array('merchantID'=>$user_id);

        $roles = $this->general_model->get_table_data('tbl_role', array('merchantID' => $user_id));

        $rolenew = array();
        $authnew = array();
        if (!empty($roles)) {
            foreach ($roles as $key => $role) {
                $auth = '';
                if (!empty($role['authID'])) {
                    $auth = $this->general_model->get_auth_data($role['authID']);
                }

                $role['authName'] = $auth;
                $authnew[$key]    = $role;
            }
        }
        $data['roles_data'] = $authnew;
        $rolename           = $this->general_model->get_table_data('tbl_auth', '');
        $data['auths']      = $rolename;

        $data['deleteRoleURL'] = "Integration/Settings/delete_role";
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_user_role', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_role()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
        $this->form_validation->set_rules('roleName', 'roleName', 'required');

        if ($this->form_validation->run() == true && !empty($this->input->post('role'))) {
            $input_data['roleName'] = $this->czsecurity->xssCleanPostInput('roleName');

            $qq                       = implode(',', $this->czsecurity->xssCleanPostInput('role'));
            $input_data['roleName']   = $this->czsecurity->xssCleanPostInput('roleName');
            $input_data['authID']     = $qq;
            $merchantID               = $this->session->userdata('logged_in')['merchID'];
            $input_data['merchantID'] = $merchantID;

            if ($this->czsecurity->xssCleanPostInput('roleID') != "") {

                $id = $this->czsecurity->xssCleanPostInput('roleID');

                $condition = array('roleID' => $id);
                $updt      = $this->general_model->update_row_data('tbl_role', $condition, $input_data);
                if ($updt) {
                    $this->session->set_flashdata('success', 'Successfully Updated Role');
                } else {

                    $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
                }
            } else {
                $insert = $this->general_model->insert_row('tbl_role', $input_data);
                if ($insert) {
                    $this->session->set_flashdata('success', 'Successfully Inserted Role');
                } else {

                    $this->session->set_flashdata('message', '<Strong>Error:</strong> Something Is Wrong.');
                }
            }

            redirect(base_url('Integration/Settings/admin_role'));
        } else {
            $this->session->set_flashdata('message', 'Error: Validation Error.');
        }
        if ($this->uri->segment('3') != "") {

            $roleId       = $this->uri->segment('3');
            $con          = array('roleID' => $roleId);
            $data['role'] = $this->general_model->get_row_data('tbl_role', $con);
        }
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $rolename = $this->general_model->get_table_data('tbl_auth', '');

        // print_r($rolename); die;
        $data['auths'] = $rolename;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('pages/page_userrole', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /********** Get Role ID ********/

    public function get_role_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('role_id');
        $val = array(
            'roleID' => $id,
        );

        $data = $this->general_model->get_row_data('tbl_role', $val);
        echo json_encode($data);
    }

    /********** Delete Admin Role ********/

    public function delete_role()
    {

        $roleID = $this->czsecurity->xssCleanPostInput('merchantroleID');
        /* print_r($roleID));die; */
        $condition = array('roleID' => $roleID);

        $del = $this->general_model->delete_row_data('tbl_role', $condition);
        if ($del) {
            $this->session->set_flashdata('success', 'Successfully Deleted Role');
        } else {

            $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
        }

        redirect(base_url('Integration/Settings/admin_role'));
    }

    /********** Get Admin Users Records ********/

    public function admin_user()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $users   = $this->general_model->get_merchant_user_data($user_id);
        $usernew = array();
        foreach ($users as $key => $user) {
            $auth             = $this->general_model->get_auth_data($user['authID']);
            $user['authName'] = $auth;
            $usernew[$key]    = $user;
        }

        $data['user_data'] = $usernew;
        $plantype          = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']  = $plantype;

        //    $this->helper->write_file($path, $xml, $mode = FOPEN_WRITE_CREATE_DESTRUCTIVE);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_admin_user', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /********** Add, Update and Edit Admin Users ********/

    public function create_user()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info']      = $this->session->userdata('logged_in');
            $merchantID = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']      = $this->session->userdata('user_logged_in');
            $merchantID = $data['login_info']['merchantID'];
        }

        if (!empty($this->input->post())) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
            $this->form_validation->set_rules('userFname', 'userFname', 'required');
            $this->form_validation->set_rules('userLname', 'userLname', 'required');
            //    $this->form_validation->set_rules('userAddress', 'userAddress', 'required');

            if ($this->form_validation->run() == true) {

                $input_data['userFname'] = $this->czsecurity->xssCleanPostInput('userFname');
                $input_data['userLname'] = $this->czsecurity->xssCleanPostInput('userLname');
                $input_data['userEmail'] = $this->czsecurity->xssCleanPostInput('userEmail');

                $is_exist_data = [
                    'email' => $input_data['userEmail'],
                ];

                if ($this->czsecurity->xssCleanPostInput('userID') == "") {
                    $input_data['userPasswordNew'] = password_hash(
                        $this->czsecurity->xssCleanPostInput('userPassword'),
                        PASSWORD_BCRYPT
                    );
                } else {
                    $is_exist_data['merchantUserID'] = $this->czsecurity->xssCleanPostInput('userID');
                }

                $input_data['userAddress'] = $this->czsecurity->xssCleanPostInput('userAddress');
                $input_data['roleId']      = $this->czsecurity->xssCleanPostInput('roleID');
                $input_data['merchantID']  = $merchantID;

                $is_exist = is_email_exists($is_exist_data);
                if ($is_exist) {
                    $this->session->set_flashdata('message', '<strong>Error:</strong> ' . $is_exist);
                } else {
                    if ($this->czsecurity->xssCleanPostInput('userID') != "") {

                        $id            = $this->czsecurity->xssCleanPostInput('userID');
                        $chk_condition = array('merchantUserID' => $id, 'merchantID' => $merchantID);
                        $updt          = $this->general_model->update_row_data('tbl_merchant_user', $chk_condition, $input_data);
                        if ($updt) {
                            $this->session->set_flashdata('success', 'Successfully Updated User');
                        } else {
                            $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
                        }
                    } else {
                        $insert = $this->general_model->insert_row('tbl_merchant_user', $input_data);
                        if ($insert) {
                            $this->session->set_flashdata('success', 'Successfully Created New User');
                        } else {
                            $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
                        }
                    }
                }
                redirect(base_url('Integration/Settings/admin_user'));
            }
        }

        if ($this->uri->segment('4')) {
            $userID       = $this->uri->segment('4');
            $con                = array('merchantUserID' => $userID, 'merchantID' => $merchantID);
			$data['user'] 	  = $this->general_model->get_row_data('tbl_merchant_user', $con);
			if(!$data['user'] || empty($data['user'])){
				$this->session->set_flashdata('error', 'Invalid Request');
				redirect(base_url('Integration/Settings/admin_user'));
			}
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $username      = $this->general_model->get_table_data('tbl_auth', '');
        $data['auths'] = $username;

        $con1 = array('merchantID' => $user_id);
        $role = $this->general_model->get_table_data('tbl_role', $con1);

        $data['role_name'] = $role;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/create_admin_user', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /**************Delete Admin Users********************/

    public function delete_user()
    {

        $userID    = $this->czsecurity->xssCleanPostInput('merchantID');
        $condition = array('merchantUserID' => $userID);
        $del       = $this->general_model->delete_row_data('tbl_merchant_user', $condition);
        if ($del) {
            $this->session->set_flashdata('success', 'Successfully Deleted User');
        } else {
            $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
        }

        redirect(base_url('Integration/Settings/admin_user'));
    }

    public function recover_pwd()
    {
        if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $id      = $this->uri->segment('3');
        $results = $this->general_model->get_select_data('tbl_merchant_user', array('userFname', 'userLname', 'userEmail', 'merchantID'), array('merchantUserID' => $id));

        $merchant_data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID', 'firstName', 'lastName'), array('merchID' => $results['merchantID']));

        $res_data = $this->general_model->get_select_data('tbl_reseller', array('resellerfirstName', 'lastName', 'resellerCompanyName', 'resellerEmail'), array('resellerID' => $merchant_data['resellerID']));

        $Merchant_url = $this->general_model->get_select_data('Config_merchant_portal', array('merchantPortalURL'), array('resellerID' => $merchant_data['resellerID']));

        if (!empty($results)) {
            $name      = $results['userFname'];
            $login_url = $Merchant_url['merchantPortalURL'];
            $email     = $results['userEmail'];
            $this->load->helper('string');
            $password      = random_string('alnum', 16);
            $marchant_name = $merchant_data['firstName' . ' ' . 'lastName'];
            $condition     = array('merchantUserID' => $id);
            $data          = array(
                'userPasswordNew' => password_hash(
                    $password,
                    PASSWORD_BCRYPT
                )
            );
            $update = $this->general_model->update_row_data('tbl_merchant_user', $condition, $data);
            if ($update) {
                $temp_data = $this->general_model->get_row_data('tbl_email_template', array('merchantID' => $results['merchantID'], 'templateType' => '14'));
                $message   = $temp_data['message'];
                $subject   = $temp_data['emailSubject'];
                if ($temp_data['fromEmail'] != '') {
                    $fromemail = $temp_data['fromEmail'];
                } else {

                    $fromemail = $res_data['resellerEmail'];
                }

                $mailDisplayName = $marchant_name;
                if ($temp_data['mailDisplayName'] != '') {
                    $mailDisplayName = $temp_data['mailDisplayName'];
                }

                $login_url = '<a href=' . $login_url . '>' . $login_url . '<a/>';
                $message   = stripslashes(str_replace('{{userFname}}', $name, $message));
                $message   = stripslashes(str_replace('{{login_url}}', $login_url, $message));
                $message   = stripslashes(str_replace('{{userEmail}}', $email, $message));
                $message   = stripslashes(str_replace('{{userPassword}}', $password, $message));
                $message   = stripslashes(str_replace('{{merchant_name}}', $marchant_name, $message));

                $this->load->library('email');
                $this->email->from($fromemail, $mailDisplayName);
                $this->email->to($email);
                $this->email->subject($subject);
                $this->email->set_mailtype("html");
                $this->email->message($message);

                if ($this->email->send()) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success">Password Reset request has been sent. If your email is listed with an account, an email will be sent to you.</div>');
                } else {
                    $this->session->set_flashdata('message', 'Email was not sent, please contact your administrator.' . $code);
                }
            }
            redirect(base_url() . 'Integration/Settings/admin_user');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success">Password Reset request has been sent. If your email is listed with an account, an email will be sent to you.</div>');
            redirect(base_url() . 'Integration/Settings/admin_user');
        }
    }

    public function set_template_cus_details()
    {
        $user_id        = $this->merchantID;
        $new_data_array = array();
        if (!empty($this->input->post('templateID')) && !empty($this->input->post('customerID'))) {
            $templateID = $this->czsecurity->xssCleanPostInput('templateID');
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $condition  = array('templateID' => $templateID, 'merchantID' => $user_id);
            $view_data  = $this->data_model->template_data($condition);

            $merchant_data = $this->general_model->get_merchant_data(array('merchID' => $user_id));
            $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $user_id));

            $currency      = "$";
            $customer      = '';
            $config_email  = $merchant_data['merchantEmail'];
            $merchant_name = $merchant_data['companyName'];
            $logo_url      = $merchant_data['merchantProfileURL'];
            $mphone        = $merchant_data['merchantContact'];

            if (!empty($config_data['ProfileImage'])) {
                $logo_url = base_url() . LOGOURL . $config_data['ProfileImage'];
            } else {
                $logo_url = CZLOGO;
            }

            $cur_date            = date('Y-m-d');
            $amount              = '';
            $paymethods          = '';
            $transaction_details = '';
            $tr_data             = '';
            $ref_number          = '';
            $overday             = '';
            $balance             = '0.00';
            $in_link             = '';
            $duedate             = '';
            $company             = '';
            $cardno              = '';
            $expired             = '';
            $expiring            = '';
            $friendly_name       = '';
            $tr_date             = '';
            $update_link         = $config_data['customerPortalURL'];
            $code                = '';
            $company             = '';

            $con    = array('merchantID' => $this->merchantID, 'Customer_ListID' => $customerID);
            $c_data = $this->general_model->get_row_data('Xero_custom_customer', $con);

            $cust_company = $c_data['companyName'];
            $customer     = $c_data['companyName'];
            $toEmail      = $c_data['userEmail'];
            $subject      = $view_data['emailSubject'];
            $subject      = stripslashes(str_replace('{{company_name}}', $cust_company, $subject));
            $message      = $view_data['message'];
            $message      = stripslashes(str_replace('{{merchant_name}}', $merchant_name, $message));
            $message      = stripslashes(str_replace('{{customer.company}}',$cust_company, $message));
            $message      = stripslashes(str_replace('{{merchant_email}}', $config_email, $message));
            $message      = stripslashes(str_replace('{{merchant_phone}}', $mphone, $message));
            $message      = stripslashes(str_replace('{{current.date}}', $cur_date, $message));
            $message      = stripslashes(str_replace('{{logo}}', "<img src='" . $logo_url . "'>", $message));

            $new_data_array['message']      = $message;
            $new_data_array['emailSubject'] = $subject;
            $new_data_array['message']      = $message;
            $new_data_array['addCC']        = $view_data['addCC'];
            $new_data_array['addBCC']       = $view_data['addBCC'];
            $new_data_array['toEmail']      = $toEmail;
            $new_data_array['replyTo']      = $view_data['replyTo'];

            $new_data_array['invCode'] = $view_data['systemMail'];
        }

        echo json_encode($new_data_array);
        die;
    }

    public function send_mail()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user_id));
        $logo_url      = '';
        $config_data   = $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID' => $user_id));
        if (!empty($config_data)) {
            $logo_url = base_url() . LOGOURL . $config_data['ProfileImage'];
        } else {
            $logo_url = CZLOGO;
        }

        $merchant_name = $merchant_data['companyName'];

        $this->load->library('email');

        $type      = $this->czsecurity->xssCleanPostInput('type');
        $invoiceID = '';
        $toEmail   = $this->czsecurity->xssCleanPostInput('toEmail');
        $addCC     = $this->czsecurity->xssCleanPostInput('ccEmail');
        $addBCC    = $this->czsecurity->xssCleanPostInput('bccEmail');
        $replyTo   = $this->czsecurity->xssCleanPostInput('replyEmail');
        $message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', true);
        $subject   = $this->czsecurity->xssCleanPostInput('emailSubject');
        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceCode');

        $message = stripslashes(str_replace('{{logo}}', "<img src='$logo_url'>", $message));
        $message = stripslashes(str_replace('<div id="lg_div">&nbsp;</div>', "<img src='$logo_url'>", $message));

        $fromEmail       = $this->czsecurity->xssCleanPostInput('fromEmail');
        $mailDisplayName = $this->czsecurity->xssCleanPostInput('mailDisplayName');
        $fromEmail       = ($fromEmail == '') ? 'donotreply@payportal.com' : $fromEmail;
        $mailDisplayName = ($mailDisplayName == '') ? $merchant_name : $mailDisplayName;
        $replyTo         = ($replyTo == '') ? $merchant_data['merchantEmail'] : $replyTo;
        $date            = date('Y-m-d H:i:s');
        $customerID      = $this->czsecurity->xssCleanPostInput('customertempID');
        $invoicetempID   = $this->czsecurity->xssCleanPostInput('invoicetempID');
        $email_data      = array(
            'customerID'   => $customerID,
            'merchantID'   => $user_id,
            'emailSubject' => $subject,
            'emailfrom'    => $fromEmail,
            'emailto'      => $toEmail,
            'emailcc'      => $addCC,
            'emailbcc'     => $addBCC,
            'emailreplyto' => $replyTo,
            'emailMessage' => $message,
            'emailsendAt'  => $date,

        );
        if ($invoiceID != '') {
            $email_data['emailCode'] = $invoiceID;
            $email_data['invoiceID'] = $invoicetempID;
        }
        $toArr      = (explode(";", $toEmail));
        $toArrEmail = [];

        foreach ($toArr as $value) {
            # code...
            $toArrEmail[]['email'] = trim($value);
        }

        $toaddCCArrEmail = [];
        if ($addCC != null) {
            $toaddCCArr = (explode(";", $addCC));

            foreach ($toaddCCArr as $value) {
                # code...
                $toaddCCArrEmail[]['email'] = trim($value);
            }
        }

        $toaddBCCArrEmail = [];
        if ($addBCC != null) {
            $toaddBCCArr = (explode(";", $addBCC));

            foreach ($toaddBCCArr as $value) {
                # code...
                $toaddBCCArrEmail[]['email'] = trim($value);
            }
        }
        $file_name = '';

        if ($this->czsecurity->xssCleanPostInput('add_attachment') == 'on') {

            if ($invoicetempID != "") {
                $condition2   = array('merchantID' => $user_id, 'invoiceID' => $invoiceID);
                $invoice_data = $this->general_model->get_row_data('Xero_test_invoice', $condition2);

                if (!empty($invoice_data)) {
                    $inv_no    = $invoice_data['refNumber'];
                    $item_data = $this->data_model->get_invoice_details_item_data($invoiceID, $user_id);

                    $condition3    = array('Customer_ListID' => $invoice_data['CustomerListID'], 'merchantID' => $user_id);
                    $customer_data = $this->general_model->get_row_data('Xero_custom_customer', $condition3);
                    $this->general_model->generate_invoice_pdf_xero($user_id, $customer_data, $invoice_data, $item_data, 'F');
                    $file_name = $invoicetempID . '-' . "$inv_no.pdf";
                }
            }

        }
        $request_array = [
            "personalizations" => [
                [
                    "to"      => $toArrEmail,
                    "subject" => $subject,
                    "cc"      => $toaddCCArrEmail,
                    "bcc"     => $toaddBCCArrEmail,
                ],
            ],
            "from"             => [
                "email" => $fromEmail,
                "name"  => $mailDisplayName,
            ],
            "reply_to"         => [
                "email" => $replyTo,
            ],
            "content"          => [
                [
                    "type"  => "text/html",
                    "value" => $message,
                ],
            ],

        ];
        if (count($toaddCCArrEmail) == 0) {
            unset($request_array['personalizations'][0]['cc']);
        }
        if (count($toaddBCCArrEmail) == 0) {
            unset($request_array['personalizations'][0]['bcc']);
        }
        if ($file_name != "") {
            $attachments[] = [
                'filename' => $file_name,
                'type'     => 'text/plain',
                'content'  => base64_encode(file_get_contents(FCPATH . '/uploads/invoice_pdf/' . $file_name)),
            ];
            $request_array['attachments'] = $attachments;
        }
        // get api key for send grid
        $api_key = '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';
        $url     = 'https://api.sendgrid.com/v3/mail/send';

        // set authorization header
        $headerArray = [
            'Authorization: Bearer ' . $api_key,
            'Content-Type: application/json',
            'Accept: application/json',
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        // add authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

        $response    = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $httpcode    = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // parse header data from response
        $header_text = substr($response, 0, $header_size);
        // parse response body from curl response
        $body = substr($response, $header_size);

        $header = array();

        // if mail sent success
        if ($httpcode == '202' || $httpcode == '200') {
            $this->general_model->insert_row('tbl_template_data', $email_data);
            $this->session->set_flashdata('success', 'Email Sent Successfully');
        } else {
            $this->general_model->insert_row('tbl_template_data', $email_data);
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>.</div>');
        }

        if (!empty($this->input->post('sendmailbyinvdtl'))) {
            redirect('Integration/Invoices/invoice_details/' . $invoicetempID, 'refresh');
        } else {
            redirect('Integration/Customers/customer_detail/' . $customerID, 'refresh');
        }
    }

    public function set_template()
    {

        $user_id = $this->merchantID;

        $invoiceID  = $this->czsecurity->xssCleanPostInput('invoiceID');
        $typeID     = $this->czsecurity->xssCleanPostInput('typeID');
        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        $condition  = array('templateType' => $typeID, 'merchantID' => $user_id);
        $view_data  = $this->data_model->template_data($condition);

        $merchant_data = $this->general_model->get_merchant_data(array('merchID' => $user_id));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $user_id));

        $currency            = "$";
        $customer            = '';
        $config_email        = $merchant_data['merchantEmail'];
        $merchant_name       = ($merchant_data['companyName'] != null) ? $merchant_data['companyName'] : $this->loginDetails['companyName'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $mphone              = $merchant_data['merchantContact'];
        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $tr_date             = '';
        $update_link         = $config_data['customerPortalURL'];
        if (!empty($config_data['ProfileImage'])) {
            $logo_url = base_url() . LOGOURL . $config_data['ProfileImage'];
        } else {
            $logo_url = CZLOGO;
        }

        $code     = '';
        $str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
        $shuffled = str_shuffle($str);
        $shuffled = substr($shuffled, 1, 12) . '=' . $user_id;
        $code     = $this->general_model->safe_encode($shuffled);
        if ($this->czsecurity->xssCleanPostInput('invoiceID') != "") {

            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

            $link_data = $this->general_model->get_row_data('tbl_template_data', array('merchantID' => $user_id, 'invoiceID' => $invoiceID));
            $ttt       = explode(PLINK, $update_link);

            $purl = $ttt[0] . PLINK . '/customer/';
            if (!empty($link_data)) {
                if ($link_data['emailCode'] != "") {
                    $code = $link_data['emailCode'];
                } else {
                    $code = $this->general_model->safe_encode($user_id);
                }

                $invcode = $this->general_model->safe_encode($invoiceID);

                $in_link = '<a  target="_blank" href="' . $purl . 'update_payment/' . $code . '/' . $invcode . '" class="btn btn-primary">Click Here</a>';
            } else {
                $invcode = $this->general_model->safe_encode($invoiceID);
                $in_link = '<a  target="_blank" href="' . $purl . 'update_payment/' . $code . '/' . $invcode . '" class="btn btn-primary">Click Here</a>';
            }
        }

        $company = '';

        $data['login_info'] = $merchant_data;
        $company            = $data['login_info']['companyName'];
        $cust_company       = '';
        $condition1         = " and cust.Customer_ListID='" . $customerID . "' and  inv.merchantID='" . $user_id . "' ";
        if ($this->czsecurity->xssCleanPostInput('invoiceID') != "") {

            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
            $condition1 .= " and inv.invoiceID='" . $invoiceID . "' ";
        } else {
            if ($typeID == '12' || $typeID == '11') {
                $card_data = $this->card_model->get_expiry_card_data($customerID, '1');
                if (!empty($card_data)) {
                    $cardno        = $card_data['CustomerCard'];
                    $friendly_name = $card_data['customerCardfriendlyName'];
                }
            }
        }

        $data    = $this->data_model->get_invoice_data_template($condition1);
        $toEmail = '';

        if (!empty($data)) {
            $customer     = $data['fullName'];
            $amount       = (-$data['AppliedAmount']);
            $balance      = $data['BalanceRemaining'];
            $paymethods   = $data['paymentType'];
            $toEmail      = $data['userEmail'];
            $duedate      = date('F d, Y', strtotime($data['DueDate']));
            $ref_number   = $data['RefNumber'];
            $tr_date      = date('F d, Y', strtotime($data['TimeCreated']));
            $cust_company = $data['companyName'];
        }

        if (!$view_data['replyTo'] || empty($view_data['replyTo'])) {
            $view_data['replyTo'] = $config_email;
        }

        $subject = $view_data['emailSubject'];
        $subject = stripslashes(str_replace('{{invoice.refnumber}}', $ref_number, $subject));
        $subject = stripslashes(str_replace('{{invoice_due_date}}', $duedate, $subject));
        $subject = stripslashes(str_replace('{{company_name}}', $cust_company, $subject));

        if ($typeID == '11') {
            $subject = stripslashes(str_replace('{{creditcard.mask_number}}', $cardno, $view_data['emailSubject']));
        }

        $message = $view_data['message'];
        $message = stripslashes(str_replace('{{merchant_name}}', $merchant_name, $message));
        $message = stripslashes(str_replace('{{creditcard.mask_number}}', $cardno, $message));
        $message = stripslashes(str_replace('{{creditcard.type_name}}', $friendly_name, $message));
        $message = stripslashes(str_replace('{{creditcard.url_updatelink}}', $update_link, $message));

        $message = stripslashes(str_replace('{{customer.company}}', $cust_company, $message));
        $message = stripslashes(str_replace('{{customer.name}}', $customer, $message));
        $message = stripslashes(str_replace('{{transaction.currency_symbol}}', $currency, $message));
        $message = stripslashes(str_replace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_replace('{{transaction.amount}}', ($balance) ? ('$' . number_format($balance, 2)) : '0.00', $message));
        $message = stripslashes(str_replace('{{transaction.transaction_date}}', $tr_date, $message));
        $message = stripslashes(str_replace('{{invoice_due_date}}', $duedate, $message));
        $message = stripslashes(str_replace('{{invoice.refnumber}}', $ref_number, $message));
        $message = stripslashes(str_replace('{{invoice.balance}}', '$' . number_format($balance, 2), $message));
        $message = stripslashes(str_replace('{{invoice.due_date|date("F j, Y")}}', $duedate, $message));
        $message = stripslashes(str_replace('{{invoice.days_overdue}}', $overday, $message));
        $message = stripslashes(str_replace('{{invoice.url_permalink}}', $in_link, $message));
        $message = stripslashes(str_replace('{{invoice_payment_pagelink}}', $in_link, $message));

        $message = stripslashes(str_replace('{{merchant_email}}', $config_email, $message));
        $message = stripslashes(str_replace('{{merchant_phone}}', $mphone, $message));
        $message = stripslashes(str_replace('{{current.date}}', $cur_date, $message));
        $message = stripslashes(str_replace('{{logo}}', "<img src='" . $logo_url . "'>", $message));

        $new_data_array = array();

        $new_data_array['message']      = $message;
        $new_data_array['emailSubject'] = $subject;
        $new_data_array['message']      = $message;
        $new_data_array['addCC']        = $view_data['addCC'];
        $new_data_array['addBCC']       = $view_data['addBCC'];
        $new_data_array['toEmail']      = $toEmail;
        $new_data_array['replyTo']      = $view_data['replyTo'];

        $new_data_array['fromEmail']       = ($view_data['fromEmail'] != null) ? $view_data['fromEmail'] : DEFAULT_FROM_EMAIL;
        $new_data_array['mailDisplayName'] = ($view_data['mailDisplayName'] != null) ? $view_data['mailDisplayName'] : $this->logged_in_data['companyName'];
        $new_data_array['templateName']    = $view_data['templateName'];
        $new_data_array['invCode']         = $code;

        echo json_encode($new_data_array);
        die;
    }

    public function reports()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['startdate']   = '';
        $data['report_type'] = '1';

        $condition       = array("inv.merchantID" => $user_id);
        $invoices        = $this->company_model->get_invoice_data_by_due($condition, 2);
        $data['report1'] = $invoices;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_reports', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function transaction_reports()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $today               = date('Y-m-d');
        $data['report_type'] = $this->czsecurity->xssCleanPostInput('report_type');
        $data['startdate']   = '';

        if ($this->czsecurity->xssCleanPostInput('report_type') == '1') {

            $condition = array("inv.merchantID" => $user_id);
            $invoices  = $this->company_model->get_invoice_data_by_due($condition);

            $data['report1'] = $invoices;

        } else if ($this->czsecurity->xssCleanPostInput('report_type') == '2') {
            $condition1      = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "inv.merchantID" => $user_id);
            $invoices        = $this->company_model->get_invoice_data_by_due($condition1);
            $data['report2'] = $invoices;

        } else if ($this->czsecurity->xssCleanPostInput('report_type') == '3') {

            $invoices        = $this->company_model->get_invoice_data_by_past_due($user_id);
            $data['report3'] = $invoices;

        } else if ($this->czsecurity->xssCleanPostInput('report_type') == '4') {
            $invoices        = $this->company_model->get_invoice_data_by_past_time_due($user_id);
            $data['report4'] = $invoices;

        } else if ($this->czsecurity->xssCleanPostInput('report_type') == '5') {

            $invoices        = $this->company_model->get_transaction_failure_report_data($user_id);
            $data['report5'] = $invoices;
        } else if ($this->czsecurity->xssCleanPostInput('report_type') == '6') {
            $this->load->library('encrypt');
            $report    = array();
            $card_data = $this->card_model->get_credit_card_info($user_id);

            foreach ($card_data as $key => $card) {

                $condition                                 = array('Customer_ListID' => $card['customerListID']);
                $customer_data                             = $this->general_model->get_row_data('Xero_custom_customer', $condition);
                $customer_card['Customer_ListID']          = $customer_data['Customer_ListID'];
                $customer_card['CardNo']                   = substr($this->card_model->decrypt($card['CustomerCard']), 12);
                $customer_card['expired_date']             = $card['expired_date'];
                $customer_card['customerCardfriendlyName'] = $card['customerCardfriendlyName'];
                $customer_card['Contact']                  = $customer_data['userEmail'];
                $customer_card['fullName']                 = $customer_data['fullName'];
                $customer_card['companyName']              = $customer_data['companyName'];
                $report[$key]                              = $customer_card;

            }
            $data['report6'] = $report;
        } else if (in_array($data['report_type'], [8, 9, 10])) {

            $type = $this->czsecurity->xssCleanPostInput('report_type');

            if ($type == '10') {
                $invoices         = $this->company_model->get_schedule_payment_invoice($user_id);
                $data['report10'] = $invoices;
            } else {
                $invoices         = $this->company_model->get_invoice_data_open_due($user_id, $type);
                $data['report89'] = $invoices;
            }

        } else if ($this->czsecurity->xssCleanPostInput('report_type') == '7') {

            $startdate         = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('startDate')));
            $enddate           = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('endDate')));
            $data['startdate'] = date('m/d/Y', strtotime($startdate));
            $data['enddate']   = date('m/d/Y', strtotime($enddate));
            $invoices          = $this->company_model->get_transaction_report_data($user_id, $startdate, $enddate);
            $data['report7']   = $invoices;
        } else {
            $condition       = array("inv.merchantID" => $user_id);
            $invoices        = $this->company_model->get_invoice_data_by_due($condition);
            $data['report1'] = $invoices;
        }
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_reports', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function export_csv()
    {
        $delimiter = ",";
        $newline   = "\r\n";
        $enclosure = '"';

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $today     = date('Y-m-d');
        $condition = array("inv.merchantID" => $user_id);
        $this->load->dbutil(); // call db utility library
        $this->load->helper('download'); // call download helper

        $report_type = $this->uri->segment(4);
        $final_array = [];

        if ($report_type == "") {
            $report_type = 1;
        }

        if ($report_type == '1' || $report_type == '2') {

            if ($report_type == '1') {
                $condition = array("inv.merchantID" => $user_id);
            }

            if ($report_type == '2') {
                $condition = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "inv.merchantID" => $user_id);
            }

            $result_set = $this->company_model->get_invoice_data_by_due($condition);
            $final_keys = array("Company", "Customer Name", "Full Name", "Amount", "Email", "Status");

            foreach ($result_set as $value1) {
                $final_array[] = array(
                    "Company"       => $value1['companyName'],
                    "Customer Name" => $value1['fullName'],
                    "Full Name"     => $value1['firstName'] . ' ' . $value1['lastName'],
                    "Amount"        => $value1['balance'],
                    "Email"         => $value1['userEmail'],
                    "Status"        => $value1['status'],
                );
            }

        } else if ($report_type == '3' || $report_type == '4') {
            if ($report_type == '3') {
                $result_set = $this->company_model->get_invoice_data_by_past_due($user_id);
            }

            if ($report_type == '4') {
                $result_set = $this->company_model->get_invoice_data_by_past_time_due($user_id);
            }

            $final_keys = array("Invoice", "Customer Name", "Email", "Days Delinquent", "Amount", "Status");
            foreach ($result_set as $value1) {
                $final_array[] = array(
                    "Invoice"         => $value1['RefNumber'],
                    "Customer Name"   => $value1['fullName'],
                    "Email"           => $value1['userEmail'],
                    "Days Delinquent" => $value1['tr_Day'],
                    "Amount"          => $value1['BalanceRemaining'],
                    "Status"          => $value1['status'],
                );
            }

        } else if ($report_type == '5') {

            $result_set = $this->company_model->get_transaction_failure_report_data($user_id);
            $final_keys = array("Transaction ID", "Customer Name", "Invoice", "Amount", "Type", "Date", "Reference");
            foreach ($result_set as $value1) {
                $final_array[] = array(
                    "Transaction ID" => $value1['transactionID'],
                    "Customer Name"  => $value1['fullName'],
                    "Invoice"        => $value1['RefNumber'],
                    "Amount"         => $value1['transactionAmount'],
                    "Type"           => $value1['transactionType'],
                    "Date"           => date('M d, Y', strtotime($value1['transactionDate'])),
                    "Reference"      => $value1['transactionStatus'],
                );
            }

        } else if ($report_type == '6') {

            $final_keys = array("Customer Name", "Email", "Card Number", "Expiration Date", "Status");

            $this->load->library('encrypt');
            $report    = array();
            $card_data = $this->card_model->get_credit_card_info($user_id);

            foreach ($card_data as $key => $card) {

                $condition     = array('Customer_ListID' => $card['customerListID']);
                $customer_data = $this->general_model->get_row_data('Xero_custom_customer', $condition);

                $customer_card['Customer_ListID']          = $customer_data['Customer_ListID'];
                $customer_card['CardNo']                   = substr($this->card_model->decrypt($card['CustomerCard']), 12);
                $customer_card['expired_date']             = $card['expired_date'];
                $customer_card['customerCardfriendlyName'] = $card['customerCardfriendlyName'];
                $customer_card['Contact']                  = $customer_data['userEmail'];
                $customer_card['fullName']                 = $customer_data['fullName'];
                $customer_card['companyName']              = $customer_data['companyName'];

                $final_array[] = array(
                    "Customer Name"   => $customer_data['fullName'],
                    "Email"           => $customer_data['userEmail'],
                    "Card Number"     => $customer_data['CardNo'],
                    "Expiration Date" => date('m/Y', strtotime($customer_card['expired_date'])),
                    "Status"          => $customer_data['Expired'],
                );
            }
        } else if (in_array($report_type, [8, 9, 10])) {
            if ($report_type == '10') {
                $result_set = $this->company_model->get_schedule_payment_invoice($user_id);
            } else {
                $result_set = $this->company_model->get_invoice_data_open_due($user_id, $report_type);
            }

            $final_keys = array("Invoice", "Customer Name", "Added On", "Days Deliquent", "Paid", "Balance", "Status");
            foreach ($result_set as $value1) {
                $final_array[] = array(
                    "Invoice"        => ($value1['RefNumber']) ? $value1['RefNumber'] : '----',
                    "Customer Name"  => $value1['fullName'],
                    "Added On"       => date('M d, Y', strtotime($value1['addOnDate'])),
                    "Days Deliquent" => $value1['tr_Day'],
                    "Paid"           => number_format($value1['AppliedAmount'], 2),
                    "Balance"        => number_format($value1['BalanceRemaining'], 2),
                    "Status"         => $value1['status'],
                );
            }

        } else if ($report_type == '7') {
            $startdate  = date('Y-m-d', strtotime($this->uri->segment(5)));
            $enddate    = date('Y-m-d', strtotime($this->uri->segment(6)));
            $result_set = $this->company_model->get_transaction_report_data($user_id, $startdate, $enddate);
            $final_keys = array("Transaction ID", "Customer Name", "Invoice", "Amount", "Date", "Type", "Remark", "Status");
            foreach ($result_set as $value1) {
                if ($value1['transactionCode'] == 200 || $value1['transactionCode'] == 100 || $value1['transactionCode'] == 1 || $value1['transactionCode'] == 111) {
                    $tr_status = "Success";
                } else {
                    $tr_status = "Failed";
                }
                $final_array[] = array(
                    "Transaction ID" => $value1['transactionID'],
                    "Customer Name"  => $value1['fullName'],
                    "Invoice"        => $value1['RefNumber'],
                    "Amount"         => $value1['transactionAmount'],
                    "Date"           => date('M d, Y', strtotime($value1['transactionDate'])),
                    "Type"           => $value1['transactionType'],
                    "Remark"         => $value1['transactionStatus'],
                    "Status"         => $tr_status,
                );
            }
        }

        $fileName = 'reportexport-' . $user_id . '-' . time() . '.csv';
        header('Content-Type: text/csv; charset=utf-8');
        header("Content-Disposition: attachment; filename=$fileName");
        $output = fopen('php://output', 'w');

        fputcsv($output, $final_keys);

        if (!empty($final_array)) {
            foreach ($final_array as $reportList) {
                fputcsv($output, $reportList);
            }
        }
    }

    public function report_details_pdf()
    {
        $data['template'] = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $type = $this->uri->segment(4);

        $today = date('Y-m-d');

        if ($type == '1') {
            $condition = array("inv.merchantID" => $user_id);
            $invoices  = $this->company_model->get_invoice_data_by_due($condition);
            $data['report1'] = $invoices;
            $data['type']    = $type;
        } else if ($type == '2') {
            $condition1 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "inv.merchantID" => $user_id);
            $invoices        = $this->company_model->get_invoice_data_by_due($condition1);
            $data['report2'] = $invoices;
            $data['type']    = $type;
        } else if ($type == '3') {
            $invoices        = $this->company_model->get_invoice_data_by_past_due($user_id);
            $data['report3'] = $invoices;
            $data['type']    = $type;
        } else if ($type == '4') {
            $invoices        = $this->company_model->get_invoice_data_by_past_time_due($user_id);
            $data['report4'] = $invoices;
            $data['type']    = $type;
        } else if ($type == '5') {
            $invoices        = $this->company_model->get_transaction_failure_report_data($user_id);
            $data['report5'] = $invoices;
            $data['type']    = $type;
        } else if ($type == '6') {
            $this->load->library('encrypt');
            $report    = array();
            $card_data = $this->card_model->get_credit_card_info($user_id);
            foreach ($card_data as $key => $card) {
                $condition                                 = array('Customer_ListID' => $card['customerListID']);
                $customer_data                             = $this->general_model->get_row_data('Xero_custom_customer', $condition);
                $customer_card['ListID']                   = $customer_data['Customer_ListID'];
                $customer_card['CardNo']                   = substr($this->card_model->decrypt($card['CustomerCard']), 12);
                $customer_card['expired_date']             = $card['expired_date'];
                $customer_card['customerCardfriendlyName'] = $card['customerCardfriendlyName'];
                $customer_card['Contact']                  = $customer_data['userEmail'];
                $customer_card['fullName']                 = $customer_data['fullName'];
                $customer_card['companyName']              = $customer_data['companyName'];
                $report[$key]                              = $customer_card;

            }

            $data['report6'] = $report;
            $data['type']    = $type;
        } else if (in_array($type, [8, 9, 10])) {
            if ($type == '10') {
                $invoices         = $this->company_model->get_schedule_payment_invoice($user_id);
                $data['report10'] = $invoices;
                $data['type']     = $type;
            } else {
                $invoices         = $this->company_model->get_invoice_data_open_due($user_id, $type);
                $data['report89'] = $invoices;
                $data['type']     = $type;
            }
        } else if ($type == '7') {
            $startdate = $this->uri->segment(5);
            $enddate   = $this->uri->segment(6);
            $invoices        = $this->company_model->get_transaction_report_data($user_id, $startdate, $enddate);
            $data['report7'] = $invoices;
            $data['type']    = $type;
        } else {
            $condition = array("inv.merchantID" => $user_id);
            $invoices  = $this->company_model->get_invoice_data_by_due($condition);
            $data['report1'] = $invoices;
            $data['type']    = $type;
        }

        $no          = "Report-$type-$user_id-".time();
        $pdfFilePath = "$no.pdf";
        ini_set('memory_limit', '320M');

        $html = $this->load->view('comman-pages/page_report_pdf', $data, true);

        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'D'); // save to file because we can
    }
}
