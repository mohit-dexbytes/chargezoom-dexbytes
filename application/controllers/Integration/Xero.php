<?php
require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
require APPPATH . 'libraries/integration/XeroSync.php';

class Xero extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    private $confiData;
    private $loginInfo;
    public function __construct()
    {
        parent::__construct();
        $this->load->config('xero');
        $this->load->model('general_model');
        $this->load->library('form_validation');

        $logged_in_data          = $this->session->userdata('logged_in');
        $this->resellerID        = $logged_in_data['resellerID'];
        $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        $merchID                 = $logged_in_data['merchID'];

        $this->loginInfo  = $logged_in_data;
        $this->merchantID = $merchID;
        $this->confiData  = [
            'urlAuthorize'            => $this->config->item('urlAuthorize'),
            'urlAccessToken'          => $this->config->item('urlAccessToken'),
            'urlResourceOwnerDetails' => $this->config->item('urlResourceOwnerDetails'),
            'clientId'                => $this->config->item('clientId'),
            'clientSecret'            => $this->config->item('clientSecret'),
            'redirectUri'             => $this->config->item('redirectUri'),
        ];
    }

    public function index()
    {
        redirect('login', 'refresh');
    }

    public function authenticate()
    {
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $this->confiData['clientId'],
            'clientSecret'            => $this->confiData['clientSecret'],
            'redirectUri'             => $this->confiData['redirectUri'],
            'urlAuthorize'            => $this->confiData['urlAuthorize'],
            'urlAccessToken'          => $this->confiData['urlAccessToken'],
            'urlResourceOwnerDetails' => $this->confiData['urlResourceOwnerDetails'],
        ]);

        // Scope defines the data your app has permission to access.
        // Learn more about scopes at https://developer.xero.com/documentation/oauth2/scopes
        $options = [
            'scope' => ['openid email profile offline_access accounting.settings accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments'],
        ];

        // This returns the authorizeUrl with necessary parameters applied (e.g. state).
        $authorizationUrl = $provider->getAuthorizationUrl($options);

        // Save the state generated for you and store it to the session.
        // For security, on callback we compare the saved state with the one returned to ensure they match.
        // $_SESSION['oauth2state'] = $provider->getState();

        // Redirect the user to the authorization URL.
        header('Location: ' . $authorizationUrl);
    }

    public function authentication_successfull()
    {
        $providerSetting = [
            'clientId'                => $this->confiData['clientId'],
            'clientSecret'            => $this->confiData['clientSecret'],
            'redirectUri'             => $this->confiData['redirectUri'],
            'urlAuthorize'            => $this->confiData['urlAuthorize'],
            'urlAccessToken'          => $this->confiData['urlAccessToken'],
            'urlResourceOwnerDetails' => $this->confiData['urlResourceOwnerDetails'],
        ];
        $provider = new \League\OAuth2\Client\Provider\GenericProvider($providerSetting);

        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {
            echo "Something went wrong, no authorization code found";
            exit("Something went wrong, no authorization code found");

            // Check given state against previously stored one to mitigate CSRF attack
        } 
        // elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
        //     echo "Invalid State";
        //     exit('Invalid state');
        // } 
        else {

            try {
                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code'],
                ]);

                $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken((string) $accessToken->getToken());
                $identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
                    new GuzzleHttp\Client(),
                    $config
                );

                $result = $identityApi->getConnections();

                // Save my tokens, expiration tenant_id
                $access_token_data = [
                    'merchantID' => $this->merchantID,
                    'access_token' => $accessToken->getToken(),
                    'refresh_token' => $accessToken->getRefreshToken(),
                    'token_validity' => $accessToken->getExpires(),
                    'tenant_id' => $result[0]->getTenantId(),
                    'auth_event_id' => $result[0]->getAuthEventId(),
                    'xero_connection_id' => $result[0]->getId(),
                    'access_id_token' => $accessToken->getValues()["id_token"],
                ];

                $chk_condition = array('merchantID' => $this->merchantID);
                $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
                if (!empty($tok_data)) {
                    $this->general_model->update_row_data('tbl_xero_token', $chk_condition, $access_token_data);
                } else {
                    $this->general_model->insert_row('tbl_xero_token', $access_token_data);
                }

                $user                 = $this->loginInfo;

                $this->clearOldData();
                
                $initSync = false;
                if($user['firstLogin'] != 1){
                    $initSync = true;
                }

                $xeroSync = new XeroSync($this->merchantID);
                $xeroSync->syncAllData($initSync);

                // Update session for XERO integration
                $user['active_app']   = 4;
                $user['is_integrate'] = 1;
                $user['firstLogin'] = 1;
                $this->session->set_userdata('logged_in', $user);

                $appdata['appIntegration'] = "4";
                $appdata['merchantID']     = $this->merchantID;

                $chk_condition = array('merchantID' => $this->merchantID);
                $app_integration = $this->general_model->get_row_data('app_integration_setting', $chk_condition);
                if (!empty($app_integration)) {
                    $this->general_model->update_row_data('app_integration_setting', $chk_condition, $appdata);
                } else {
                    $this->general_model->insert_row('app_integration_setting', $appdata);
                }

                // Update first login successfull
                $this->general_model->update_row_data('tbl_merchant_data', ['merchID' => $this->merchantID], array('firstLogin' => 1));

                $merchantData = $this->general_model->get_row_data('tbl_merchant_data', ['merchID' => $this->merchantID]);
                if (ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23) {
                    /* Start campaign in hatchbuck CRM*/
                    $this->load->library('hatchBuckAPI');
                    $merchantData['merchant_type'] = 'XERO';

                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
                    if ($status['statusCode'] == 400) {
                        $resource = $this->hatchbuckapi->createContact($merchantData);
                        if ($resource['contactID'] != '0') {
                            $contact_id = $resource['contactID'];
                            $status     = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
                        }
                    }
                    /* End campaign in hatchbuck CRM*/
                }

                if ($this->general_model->get_num_rows('tbl_email_template', array('merchantID' => $this->merchantID)) == '0') {

                    $fromEmail = $user['merchantEmail'];
                    $templatedatas = $this->general_model->get_table_data('tbl_email_template_data', '');
                    foreach ($templatedatas as $templatedata) {
                        $insert_data = array('templateName' => $templatedata['templateName'],
                            'templateType'                      => $templatedata['templateType'],
                            'merchantID'                        => $this->merchantID,
                            'fromEmail'                         => DEFAULT_FROM_EMAIL, //$fromEmail,
                            'message'                           => $templatedata['message'],
                            'emailSubject'                      => $templatedata['emailSubject'],
                            'createdAt'                         => date('Y-m-d H:i:s'),
                        );
                        $this->general_model->insert_row('tbl_email_template', $insert_data);
                    }
                }

                $data['primary_nav']  = primary_nav();
                $data['template']   = template_variable();
    		   	$data['gt_result'] = $access_token_data;

                $this->load->view('template/template_start', $data);
				$this->load->view('template/page_head', $data);
				$this->load->view('Xero_views/xero_Auth_success',$data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);

            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                echo "Callback failed";
                exit();
            }
        }
    }

    private function clearOldData(){
        $whereInvoiceItem = array(
            'merchantID' => $this->merchantID,
        );

        $this->general_model->delete_row_data('last_sync_history', $whereInvoiceItem);
        $this->general_model->delete_row_data('Xero_custom_customer', $whereInvoiceItem);
        $this->general_model->delete_row_data('Xero_test_item', $whereInvoiceItem);
        $this->general_model->delete_row_data('tbl_xero_accounts', $whereInvoiceItem);
        $this->general_model->delete_row_data('tbl_taxes_xero', $whereInvoiceItem);
        $this->general_model->delete_row_data('Xero_test_invoice', $whereInvoiceItem);
        $this->general_model->delete_row_data('tbl_xero_invoice_item', $whereInvoiceItem);
    }
}
