<?php
include_once APPPATH . 'libraries/Manage_payments.php';
require APPPATH . 'libraries/integration/XeroSync.php';

class SettingSubscription extends CI_Controller
{

    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    private $logged_in_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');
        $this->load->model('Common/company_model');

        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "") {
            $logged_in_data          = $this->session->userdata('logged_in');
            $this->resellerID        = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID                 = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data                  = $this->session->userdata('user_logged_in');
            $this->transactionByUser         = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID                         = $logged_in_data['merchantID'];
            $logged_in_data['merchantEmail'] = $logged_in_data['merchant_data']['merchantEmail'];
            $rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID                = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID     = $merchID;
        $this->logged_in_data = $logged_in_data;
    }

    public function index()
    {

        redirect('Integration/home', 'refresh');
    }

    public function create_subscription()
    {
        $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);
        if (empty($accountData)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> Please select a default payment account.</div>');
            redirect('Integration/SettingSubscription/subscriptions', 'refresh');
        }

        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {
                $da = $this->session->userdata('logged_in');

                $user_id = $da['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da = $this->session->userdata('user_logged_in');

                $user_id = $da['merchantID'];
            }

            $subscriptionID = false;

            $in_prefix = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
            if (empty($in_prefix)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

                redirect('Integration/SettingSubscription/create_subscription', 'refresh');
            }

            $total        = 0;
            $current_date = date('Y-m-d');

            $taxList = $this->czsecurity->xssCleanPostInput('is_tax_check');
            foreach ($this->czsecurity->xssCleanPostInput('sbsProduct') as $key => $prod) {
                //print_r($prod);
                $insert_row['itemCode']     = $this->czsecurity->xssCleanPostInput('productID')[$key];
                $insert_row['itemListID']   = $this->czsecurity->xssCleanPostInput('sbsProduct')[$key];
                $insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
                $insert_row['itemRate']     = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                $insert_row['AccountCode']  = $this->czsecurity->xssCleanPostInput('AccountCode')[$key];
                $insert_row['itemTax']      = $this->czsecurity->xssCleanPostInput('ptaxID')[$key];

                $insert_row['itemFullName']    = $this->czsecurity->xssCleanPostInput('description')[$key];
                $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
                if (($this->czsecurity->xssCleanPostInput('onetime_charge')[$key]) && $this->czsecurity->xssCleanPostInput('onetime_charge')[$key] != 'Recurring') {
                    $insert_row['oneTimeCharge'] = '1';
                } else {
                    $insert_row['oneTimeCharge'] = '0';
                }

                $total          = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
                $item_val[$key] = $insert_row;
            }

            $sname      = $this->czsecurity->xssCleanPostInput('sub_name');
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $plan       = $this->czsecurity->xssCleanPostInput('duration_list');
            if ($plan > 0) {
                $subsamount = $total / $plan;
            } else {
                $subsamount = $total;
            }

            $st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
            $invoice_date = $first_date = $st_date;

            $freetrial = $this->czsecurity->xssCleanPostInput('freetrial');

            $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
            $address1 = $this->czsecurity->xssCleanPostInput('address1');
            $address2 = $this->czsecurity->xssCleanPostInput('address2');
            $country  = $this->czsecurity->xssCleanPostInput('country');
            $state    = $this->czsecurity->xssCleanPostInput('state');
            $city     = $this->czsecurity->xssCleanPostInput('city');
            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
            $phone    = $this->czsecurity->xssCleanPostInput('phone');
            $paycycle = $this->czsecurity->xssCleanPostInput('paycycle');
            $planid   = $this->czsecurity->xssCleanPostInput('sub_plan');

            $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
            $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
            $bcity     = $this->czsecurity->xssCleanPostInput('bcity');
            $bstate    = $this->czsecurity->xssCleanPostInput('bstate');
            $bcountry  = $this->czsecurity->xssCleanPostInput('bcountry');
            $bphone    = $this->czsecurity->xssCleanPostInput('bphone');
            $bzipcode  = $this->czsecurity->xssCleanPostInput('bzipcode');

            if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
                $paygateway = $this->czsecurity->xssCleanPostInput('gateway_list');
            } else {
                $paygateway = 0;
            }

            $taxes = $this->czsecurity->xssCleanPostInput('taxes');
            if ($taxes != '') {
                $taxval = ($taxval == 1) ? "Exclusive" : "Inclusive";
            } else {
                $taxval = 'NoTax';
            }

            $end_date = date('Y-m-d', strtotime($st_date . "+$plan months"));

            $totalInv           = ($plan > 0) ? $plan + $freetrial - 1 : 1;
            $nextGeneratingDate = $st_date;
            $fromdateCus        = date('Y-m-d', strtotime(date("Y-m-d", strtotime($st_date)) . " -1 day"));
            $conditionGt        = array('planID' => $planid);

            $subplansData = $this->general_model->get_row_data('tbl_subscriptions_plan_xero', $conditionGt);
            if ($subplansData['proRate']) {
                $proRate = $subplansData['proRate'];
            } else {
                $proRate = 0;
            }
            if ($subplansData['proRateBillingDay']) {
                $proRateday = $subplansData['proRateBillingDay'];
            } else {
                $proRateday = 1;
            }

            if ($paycycle == 'dly') {
                $totalInv = ($totalInv) ? $totalInv : '1';
                $end_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv day"));

                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 day"));

            } else if ($paycycle == '1wk') {
                $totalInv           = ($totalInv) ? $totalInv : '1';
                $end_date           = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 week"));
            } else if ($paycycle == '2wk') {
                $totalInv           = ($totalInv) ? $totalInv + 1 : '1';
                $end_date           = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 week"));
            } else if ($paycycle == 'mon') {

                $totalInv = ($totalInv) ? $totalInv + 1 : '1';
                if ($proRate == 1) {
                    $end_date = date('Y-m-' . $proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

                    $nextGeneratingDate = date('Y-m-' . $proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
                } else {

                    $end_date           = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                    $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
                }

            } else if ($paycycle == '2mn') {
                $totalInv           = ($totalInv) ? $totalInv + 2 * 1 : '1';
                $end_date           = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 month"));

            } else if ($paycycle == 'qtr') {
                $totalInv           = ($totalInv) ? $totalInv + 3 * 1 : '1';
                $end_date           = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +3 month"));

            } else if ($paycycle == 'six') {
                $totalInv           = ($totalInv) ? $totalInv + 6 * 1 : '1';
                $end_date           = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +6 month"));
            } else if ($paycycle == 'yrl') {
                $totalInv = ($totalInv) ? $totalInv + 12 * 1 : '1';
                $end_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +12 month"));
            } else if ($paycycle == '2yr') {
                $totalInv = ($totalInv) ? $totalInv + 2 * 12 : '1';
                $end_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +24 month"));
            } else if ($paycycle == '3yr') {
                $totalInv           = ($totalInv) ? $totalInv + 3 * 12 : '1';
                $end_date           = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +36 month"));
            }

            if ($this->czsecurity->xssCleanPostInput('autopay')) {

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card         = array();
                    $card_type    = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                    $friendlyName = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);

                    $card['card_number']     = $this->czsecurity->xssCleanPostInput('card_number');
                    $card['expiry']          = $this->czsecurity->xssCleanPostInput('expiry');
                    $card['expiry_year']     = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $card['cvv']             = ''; // $this->czsecurity->xssCleanPostInput('cvv');
                    $card['friendlyname']    = $friendlyName;
                    $card['customerID']      = $customerID;
                    $card['Billing_Addr1']   = $this->czsecurity->xssCleanPostInput('baddress1');
                    $card['Billing_Addr2']   = $this->czsecurity->xssCleanPostInput('baddress2');
                    $card['Billing_City']    = $this->czsecurity->xssCleanPostInput('bcity');
                    $card['Billing_State']   = $this->czsecurity->xssCleanPostInput('bstate');
                    $card['Billing_Contact'] = $this->czsecurity->xssCleanPostInput('bphone');
                    $card['Billing_Country'] = $this->czsecurity->xssCleanPostInput('bcountry');
                    $card['Billing_Zipcode'] = $this->czsecurity->xssCleanPostInput('bzipcode');

                    //     print_r($card);die;
                    $card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
                    if (!empty($card_data)) {
                        $this->card_model->update_card($card, $customerID, $friendlyName);
                        $cardID = $card_data['CardID'];
                    } else {

                        $cardID = $this->card_model->insert_new_card($card);
                    }
                } else if ($this->czsecurity->xssCleanPostInput('acc_number') != "") {
                    $card             = array();
                    $acc_number       = $this->czsecurity->xssCleanPostInput('acc_number');
                    $route_number     = $this->czsecurity->xssCleanPostInput('route_number');
                    $acc_name         = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCode          = 'WEB';
                    $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
                    $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');

                    $friendlyName = 'Checking - ' . substr($acc_number, -4);

                    $insert_array = array(
                        'accountNumber'            => $acc_number,
                        'routeNumber'              => $route_number,
                        'accountName'              => $acc_name,
                        'secCodeEntryMethod'       => $secCode,
                        'accountType'              => $acct_type,
                        'accountHolderType'        => $acct_holder_type,
                        'CardType'                 => 'Echeck',
                        'Billing_Addr1'            => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'            => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'             => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_State'            => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Country'          => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'          => $this->czsecurity->xssCleanPostInput('bphone'),
                        'Billing_Zipcode'          => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'           => $customerID,
                        'merchantID'               => $user_id,
                        'customerCardfriendlyName' => $friendlyName,
                        'createdAt'                => date("Y-m-d H:i:s"),

                    );
                    $con = array(
                        'customerListID'           => $customerID,
                        'merchantID'               => $user_id,
                        'customerCardfriendlyName' => $friendlyName,
                    );

                    $card_data = $this->card_model->chk_card_firendly_name($customerID, $friendlyName);

                    if ($card_data > 0) {
                        //$this->update_card($card, $customerID,$friendlyName );
                        $cardID = $this->card_model->update_card_data($con, $insert_array);
                        $cardID = $card_data['CardID'];
                    } else {

                        $cardID = $this->card_model->insert_card_data($insert_array);
                    }
                }
            }
            //print_r($subdata);die;
            $subdata = array(

                'baddress1'          => $this->czsecurity->xssCleanPostInput('baddress1'),
                'baddress2'          => $this->czsecurity->xssCleanPostInput('baddress2'),
                'bcity'              => $this->czsecurity->xssCleanPostInput('bcity'),
                'bstate'             => $this->czsecurity->xssCleanPostInput('bstate'),
                'bcountry'           => $this->czsecurity->xssCleanPostInput('bcountry'),
                'bphone'             => $this->czsecurity->xssCleanPostInput('bphone'),
                'bzipcode'           => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'subscriptionName'   => $sname,
                'customerID'         => $customerID,
                'subscriptionPlan'   => $plan,
                'subscriptionAmount' => $total,
                'nextGeneratingDate' => $nextGeneratingDate,
                'firstDate'          => $first_date,
                'startDate'          => $st_date,
                'endDate'            => $end_date,
                'paymentGateway'     => $paygateway,
                'address1'           => $address1,
                'address2'           => $address2,
                'country'            => $country,
                'state'              => $state,
                'city'               => $city,
                'zipcode'            => $zipcode,
                'contactNumber'      => $phone,
                'totalInvoice'       => $plan,
                'invoicefrequency'   => $paycycle,
                'freeTrial'          => $freetrial,
                'taxID'              => $taxval,
                'planID'             => $planid,

            );

            if ($this->czsecurity->xssCleanPostInput('autopay')) {
                $subdata['cardID']           = $cardID;
                $subdata['automaticPayment'] = '1';
            } else {
                $subdata['cardID']           = 0;
                $subdata['automaticPayment'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('email_recurring')) {

                $subdata['emailRecurring'] = '1';
            } else {
                $subdata['emailRecurring'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('billinfo')) {

                $subdata['usingExistingAddress'] = '1';
            } else {
                $subdata['usingExistingAddress'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('subID') != "") {
                $rowID                = $subscriptionID                = $this->czsecurity->xssCleanPostInput('subID');
                $subdata['updatedAt'] = date('Y-m-d H:i:s');

                //$date = date("Y-m-d", strtotime($subs['firstDate']));

                $subs = $this->general_model->get_row_data('tbl_subscriptions_xero', array('subscriptionID' => $rowID));
                $date = $first_date;

                $in_num = $subs['generatedInvoice'];
                if (strtotime($current_date) <= strtotime($st_date) && $subs['generatedInvoice'] == 0) {
                    $subdata['nextGeneratingDate'] = $st_date;
                }

                $subdata['generatedInvoice'] = $in_num;
                $subdata['merchantDataID']   = $user_id;

                $ins_data = $this->general_model->update_row_data('tbl_subscriptions_xero', array('subscriptionID' => $rowID), $subdata);

                $this->general_model->delete_row_data('tbl_subscription_invoice_item_xero', array('subscriptionID' => $rowID));

                foreach ($item_val as $k => $item) {
                    unset($item['AccountCode']);
                    unset($item['itemCode']);
                    $item['subscriptionID'] = $rowID;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item_xero', $item);
                }
            } else {
                $subdata['merchantDataID']   = $user_id;
                $subdata['createdAt']        = date('Y-m-d H:i:s');
                $subdata['generatedInvoice'] = 0;
                if (strtotime($current_date) <= strtotime($st_date)) {
                    $subdata['nextGeneratingDate'] = $st_date;
                }

                $ins_data = $subscriptionID = $this->general_model->insert_row('tbl_subscriptions_xero', $subdata);

                foreach ($item_val as $k => $item) {
                    unset($item['AccountCode']);
                    unset($item['itemCode']);
                    $item['subscriptionID'] = $ins_data;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item_xero', $item);
                }
            }
            if ($ins_data && $ins) {
                if ($subdata['generatedInvoice'] == 0) {
                    $subdata['subscriptionID']    = $subscriptionID;
                    $subdata['subplansData']      = $subplansData;
                    $subdata['subscriptionItems'] = $item_val;
                    $subdata['taxes']             = $taxes;

                    $this->_createSubscriptionFirstInvoice($subdata);
                }
                $this->session->set_flashdata('success', 'Successfully Created Subscription');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
            }

            redirect('Integration/SettingSubscription/subscriptions', 'refresh');
        }

        if ($this->uri->segment('4') != "") {
            $sbID            = $this->uri->segment('4');
            $data['subs']    = $this->general_model->get_row_data('tbl_subscriptions_xero', array('subscriptionID' => $sbID));
            $data['c_cards'] = $this->card_model->get_card_expiry_data($data['subs']['customerID']);
            $data['items']   = $this->general_model->get_table_data('tbl_subscription_invoice_item_xero', array('subscriptionID' => $sbID));
        }
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $condition     = array('merchantID' => $user_id);
        $conditionPlan = array('merchantID' => $user_id, 'IsActive' => 'true');
        $condition11   = array('merchantDataID' => $user_id);

        $merchant_condition = [
            'merchID' => $user_id,
        ];
        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }

        $data['subplans']    = $this->general_model->get_table_data('tbl_subscriptions_plan_xero', $condition11);
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['frequencies'] = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
        $data['durations']   = $this->general_model->get_table_data('tbl_subscription_plan', '');
        $data['plans']       = $this->general_model->get_table_data('Xero_test_item', $conditionPlan);
        $compdata            = $this->customer_model->get_customers_data($user_id);
        $data['customers']   = $compdata;

        $taxWhere = [
            'merchantID' => $this->merchantID,
        ];
        $taxname       = $this->data_model->get_tax_data($taxWhere);
        if($taxname){
            foreach($taxname as $key => $tax){
                $taxname[$key]['friendlyName'] = $tax['friendlyName'].' ('.$tax['displayTaxRate'].'%)';
            }
        }
        $data['taxes'] = $taxname;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/create_subscription', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function subscriptions()
    {
        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['merchantID'] = $user_id;

        $condition             = array('merchantID' => $user_id);
        $data['gateways']      = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['subscriptions'] = $this->customer_model->get_subscriptions_plan_data($user_id);
        $data['subs_data']     = $this->customer_model->get_total_subscription($user_id);
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_subscriptions', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function delete_subscription()
    {
        if ($this->czsecurity->xssCleanPostInput('qbosubscID') != "") {
            $subscID   = $this->czsecurity->xssCleanPostInput('qbosubscID');
            $condition = array('subscriptionID' => $subscID);

            $this->general_model->delete_row_data('tbl_subscription_invoice_item_xero', $condition);

            $del = $this->general_model->delete_row_data('tbl_subscriptions_xero', $condition);
            if ($del) {

                $this->session->set_flashdata('success', 'Successfully Deleted ');
                $res = array('status' => "success");
                echo json_encode($res);
                die;
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error </strong></div>');
            }
        }
        redirect('Integration/SettingSubscription/subscriptions', 'refresh');
        return false;
    }

    public function subscription_invoices()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['login_info'] = $this->logged_in_data;
        $user_id            = $this->merchantID;

        $sbID         = $this->uri->segment(4);
        $data['subs'] = $this->general_model->get_row_data('tbl_subscriptions_xero', array('subscriptionID' => $sbID));

        $conditionGt   = array('planID' => $data['subs']['planID']);
        $conditionCust = array('Customer_ListID' => $data['subs']['customerID'], 'merchantID' => $user_id);

        $subplansData     = $this->general_model->get_row_data('tbl_subscriptions_plan_xero', $conditionGt);
        $subplansCustomer = $this->general_model->get_row_data('Xero_custom_customer', $conditionCust);

        $data['page_num']      = 'customer_qbo';
        $condition             = array('merchantID' => $user_id);
        $data['gateway_datas'] = $this->general_model->get_gateway_data($user_id);
        $invoices              = $this->company_model->get_subscription_invoice_data($user_id, $sbID);
        // echo $this->db->last_query(); die;
        $data['totalInvoice'] = count($invoices);

        $data['revenue'] = $this->company_model->get_subscription_revenue($user_id, $sbID);

        $data['customerName'] = $subplansCustomer['fullName'];
        $data['planName']     = $subplansData['planName'];
        $data['invoices']     = $invoices;

        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('common-integration/page_subscription_details', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function update_subscription_gateway()
    {

        if (!empty($this->input->post())) {

            $data['login_info'] = $this->logged_in_data;
            $user_id            = $this->merchantID;

            if (!empty($this->input->post('gate')) && !empty($this->input->post('gateway_old')) && !empty($this->input->post('gateway_new'))) {
                $sub_ID = implode(',', $this->czsecurity->xssCleanPostInput('gate'));

                $old_gateway = $this->czsecurity->xssCleanPostInput('gateway_old');
                $new_gateway = $this->czsecurity->xssCleanPostInput('gateway_new');

                $condition = array('merchantDataID' => $user_id, 'paymentGateway' => $old_gateway);
                $num_rows  = $this->general_model->get_num_rows('tbl_subscriptions_xero', $condition);

                if ($num_rows > 0) {
                    $update_data = array('paymentGateway' => $new_gateway);

                    $sss = $this->db->query("update tbl_subscriptions_xero set paymentGateway='" . $new_gateway . "' where subscriptionID IN ($sub_ID) ");

                    if ($sss) {

                        $this->session->set_flashdata('success', 'Successfully Updated');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In update process. </div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> No data selected. </div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Validation Error. </div>');
            }
        }
        redirect('Integration/SettingSubscription/subscriptions', 'refresh');
    }

    public function get_subscription_gateway()
    {

        $id         = $this->czsecurity->xssCleanPostInput('gateway_id');
        $merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
        $val        = array(
            'paymentGateway' => $id,
            'merchantDataID' => $merchantID,
            'merchantID'     => $merchantID,
        );

        $datas = $this->qbo_subscription_model->get_subscriptions_gatway_data($val);
        ?>
            <table class="table table-bordered table-striped table-vcenter">
                <tbody>
                    <tr>
                        <th class="text-left col-md-6"> <strong> Customer Name</strong></th>
                        <?php /* <th class="text-left col-md-6"> <strong>Plan</strong></th> */?>
                        <th class="text-center col-md-6"><strong>Select List </strong></th>

                    </tr>
                    <?php
        if (!empty($datas)) {
            foreach ($datas as $c_data) {
                ?>
                                <tr>
                                    <td class="text-left visible-lg  col-md-6"> <?php echo $c_data['fullName']; ?> </td>
                                    <td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['subscriptionID']; ?>" /> </td>
                                </tr>
                            <?php }
        } else {?>
                            <tr>
                                <td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Record Found"; ?> </td>

                                <td class="text-right visible-lg col-md-6 control-label"> </td>
                            </tr>
                        <?php }
        ?>

                </tbody>
            </table>
        <?php die;
    }

    public function get_subplan()
    {
        $planid = $this->czsecurity->xssCleanPostInput('planID');
        $val    = $this->company_model->get_subplan_data($planid, $this->merchantID);
        echo json_encode($val);
    }

    protected function _createSubscriptionFirstInvoice($subs)
    {
        $inputData = [];

        $merchantID = $merchID = $subs['merchantDataID'];
        $customerID = $subs['customerID'];

        $autopay    = 0;
        $cardID     = $subs['cardID'];
        $paygateway = $subs['paymentGateway'];
        $autopay    = $subs['automaticPayment'];

        if ($subs['generatedInvoice'] < $subs['freeTrial']) {
            $free_trial = '1';
        } else {
            $free_trial = '0';
        }

        $total = $recurring = $onetime = 0;

        $tax = $taxval = $subs['taxID'];
        if ($tax) {
            $tax_data = $this->general_model->get_row_data('tbl_taxe_code_qbo', array('taxID' => $tax, 'merchantID' => $merchantID));
            $taxRate  = $tax_data['total_tax_rate'];
            $taxlsID  = $tax_data['taxID'];
        } else {
            $taxRate = 0;
            $taxlsID = 0;
        }

        $cs_data = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

        $isInvoiceExist = $this->general_model->get_row_data('tbl_subscription_auto_invoices', array('app_type' => 4, 'subscriptionID' => $subs['subscriptionID']));

        $item_val = [];
        $products = $subs['subscriptionItems'];

        foreach ($products as $key => $prod) {
            if ($isInvoiceExist && $prod['oneTimeCharge']) {
                continue;
            }

            $insert_row['itemListID']      = $prod['itemListID'];
            $insert_row['itemQuantity']    = $prod['itemQuantity'];
            $insert_row['itemRate']        = $prod['itemRate'];
            $insert_row['itemFullName']    = $prod['itemFullName'];
            $insert_row['itemTax']         = $prod['itemTax'];
            $insert_row['itemDescription'] = $prod['itemDescription'];
            $insert_row['itemTotal']       = $itemTotal       = $prod['itemQuantity'] * $prod['itemRate'];

            if ($insert_row['itemTax'] == 1) {
                $itemTotal += ($insert_row['itemTotal'] * $taxRate / 100);
            }

            if ($prod['oneTimeCharge']) {
                $onetime += $itemTotal;
            } else {
                $recurring += $itemTotal;
            }

            $total = $total + $itemTotal;

            // $item_val[] = $insert_row;
        }

        $chh_mail = $subs['emailRecurring'];

        $inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
        if (!empty($inv_data)) {
            $inv_pre    = $inv_data['prefix'];
            $inv_po     = $inv_data['postfix'] + 1;
            $new_inv_no = $inv_pre . $inv_po;
        }

        $inv_date = $duedate = date($subs['nextGeneratingDate']);

        $fullname = $cs_data['fullName'];

        $date = $subs['firstDate'];
        if ($subs['subplansData']['proRate']) {
            $proRate = $subs['subplansData']['proRate'];
        } else {
            $proRate = 0;
        }

        if ($subs['subplansData']['proRateBillingDay']) {
            $proRateday      = $subs['subplansData']['proRateBillingDay'];
            $nextInvoiceDate = $subs['subplansData']['nextMonthInvoiceDate'];
        } else {
            $proRateday      = 1;
            $nextInvoiceDate = date('d', strtotime($date));
        }

        $in_num   = $subs['generatedInvoice'];
        $paycycle = $subs['invoicefrequency'];

        $prorataCalculation = prorataCalculation([
            'paycycle'        => $subs['invoicefrequency'],
            'proRateday'      => $proRateday,
            'nextInvoiceDate' => $nextInvoiceDate,
            'total_amount'    => $total,
            'onetime'         => $onetime,
            'recurring'       => $recurring,
            'date'            => $date,
            'in_num'          => $in_num,
            'proRate'         => $proRate,
        ]);

        $next_date = $prorataCalculation['next_date'];
        $total     = $prorataCalculation['total_amount'];

        for ($i = 0; $i < count($products); $i++) {
            $prorataCalculatedRate = 1;
            if ($products[$i]['oneTimeCharge'] == 0) {
                $prorataCalculatedRate = $prorataCalculation['prorataCalculatedRate'];
            }
            $inputData['productID'][]   = $products[$i]['itemCode'];
            $inputData['quantity'][]    = $products[$i]['itemQuantity'];
            $inputData['unit_rate'][]   = $products[$i]['itemRate'] * $prorataCalculatedRate;
            $inputData['description'][] = $products[$i]['itemDescription'];
            $inputData['AccountCode'][] = $products[$i]['AccountCode'];
            $inputData['ptaxID'][]      = $products[$i]['itemTax'];
        }
        $inputData['refNumber']  = $new_inv_no;
        $inputData['customerID'] = $customerID;

        $inputData['invoiceDate']    = $inv_date;
        $inputData['invoiceDueDate'] = $duedate;
        $inputData['taxes']          = $subs['taxes'];

        $xeroSync   = new XeroSync($this->merchantID);
        $retrunData = $xeroSync->createUpdateInvoice($inputData, false);
        if ($retrunData) {
            $invID = $retrunData[0];

            $schedule['invoiceID']  = $invID;
            $schedule['gatewayID']  = $paygateway;
            $schedule['cardID']     = $cardID;
            $schedule['customerID'] = $customerID;

            if ($autopay) {
                $schedule['autoInvoicePay'] = 1;
                $schedule['paymentMethod']  = 1;
            } else {
                $schedule['autoInvoicePay'] = 0;
                $schedule['paymentMethod']  = 0;
            }
            $schedule['merchantID'] = $merchID;
            $schedule['createdAt']  = date('Y-m-d H:i:s');
            $schedule['updatedAt']  = date('Y-m-d H:i:s');
            $this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);

            $subscription_auto_invoices_data = [
                'subscriptionID' => $subs['subscriptionID'],
                'invoiceID'      => $invID,
                'app_type'       => $this->logged_in_data['active_app'],
            ];

            $this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);

            if ($chh_mail == '1') {
                $inv_no    = $new_inv_no;
                $invoiceID = $invID;

                $condition2   = array('merchantID' => $user_id, 'invoiceID' => $invoiceID);
                $invoice_data = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
                $item_data    = $this->data_model->get_invoice_details_item_data($invoiceID, $user_id);

                $condition3    = array('Customer_ListID' => $invoice_data['CustomerListID'], 'merchantID' => $user_id);
                $customer_data = $this->general_model->get_row_data('Xero_custom_customer', $condition3);

                $this->general_model->generate_invoice_pdf_xero($user_id, $customer_data, $invoice_data, $item_data, 'F');

                $condition_mail = array('templateType' => '3', 'merchantID' => $merchID);

                $toEmail          = $cs_data['userEmail'];
                $company          = $cs_data['companyName'];
                $customer         = $cs_data['fullName'];
                $invoice_due_date = $duedate;
                $invoicebalance   = 0; // $QBO_invoice_details['BalanceRemaining'];

                // $invoiceID= $QBO_invoice_details['invoiceID'];
                $tr_date = date('Y-m-d H:i:s');
                //  print_r($invoicebalance);die;
                $this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $customerID, $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $inv_no);
            }

            $first_date   = strtotime($date);
            $current_time = time();
            if ($subs['automaticPayment'] == 1 && $first_date <= $current_time) {
                $paymentObj     = new Manage_payments($subs['merchantDataID']);
                $paymentDetails = $this->card_model->get_single_card_data($subs['cardID']);
                $saleData       = [
                    'paymentDetails'    => $paymentDetails,
                    'transactionByUser' => [],
                    'ach_type'          => (!empty($paymentDetails['CardNo'])) ? 1 : 2,
                    'invoiceID'         => $invID,
                    'crtxnID'           => '',
                    'companyName'       => $cs_data['companyName'],
                    'fullName'          => $cs_data['fullName'],
                    'firstName'         => $cs_data['firstName'],
                    'lastName'          => $cs_data['lastName'],
                    'contact'           => $cs_data['phoneNumber'],
                    'email'             => $cs_data['userEmail'],
                    'amount'            => $total,
                    'gatewayID'         => $subs['paymentGateway'],
                    'storeResult'       => true,
                    'customerListID'    => $cs_data['Customer_ListID'],
                ];

                $saleData   = array_merge($saleData, $subs);
                $paidResult = $paymentObj->_processSaleTransaction($saleData);
                if (isset($paidResult['response']) && $paidResult['response']) {
                    $accountData       = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);
                    $tr_date           = date('Y-m-d H:i:s');
                    $updateInvoiceData = [
                        'invoiceID'       => $invID,
                        'accountCode'     => $accountData['accountCode'],
                        'trnasactionDate' => $tr_date,
                        'amount'          => $total,
                    ];
                    $crtxnID = $xeroSync->createPayment($updateInvoiceData);
                    $this->general_model->update_row_data('customer_transaction', ['id' => $paidResult['id']], ['qbListTxnID' => $crtxnID]);
                }
            }
        }

        $gen_inv = $subs['generatedInvoice'] + 1;
        $this->general_model->update_row_data('tbl_subscriptions_xero',
            array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));

        $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']), array('postfix' => $inv_po));
    }
}
