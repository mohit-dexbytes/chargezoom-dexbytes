<?php

/**

 */
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
 
class Setup extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		
    
		$this->load->model('general_model');
		
		
	if($this->session->userdata('logged_in')!="" )
		  {
		   
		  }
		  else
		  {
			redirect('login','refresh');
		  }  

	}
	
	
	public function index()
	{
	    		
		         $data1['login_info'] 	= $this->session->userdata('logged_in');
				 $merchantID 				= $data1['login_info']['merchID'];
				 
				    $data['appIntegration']    = "5";
					$data['merchantID']        = $merchantID;
				    $chk_condition = array('merchantID'=>$merchantID);
				    
					
					 $app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);
					 $udata['is_integrate'] = 1;

					 if(!empty($app_integration))
					 {
					
					
						$this->general_model->update_row_data('app_integration_setting',$chk_condition, $data);
						$chk_condition1 = array('merchID'=>$merchantID);
						$this->general_model->update_row_data('tbl_merchant_data',$chk_condition1, $udata);
						
						$user = $this->session->userdata('logged_in');
						$user['active_app'] = '5';
						$user['is_integrate'] = 1;
						$this->session->set_userdata('logged_in',$user);
                 
					}
					else
					{
						
						$company['companyName'] = "Default Chargezoom";
						
						$company['merchantID'] = $merchantID;
							$company['date_added'] =date('Y-m-d H:i:s');
						
						$chk_condition1 = array('merchID'=>$merchantID);
					    $this->general_model->update_row_data('tbl_merchant_data',$chk_condition1, $udata);
						$user = $this->session->userdata('logged_in');
						$user['active_app'] = '5';
						$user['is_integrate'] = 1;
						$this->session->set_userdata('logged_in',$user);
				   
					}



					$gatew                  = $this->general_model->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchantID));
					
							$data['appIntegration']    = "5";
					$data['merchantID']        = $merchantID;
				    $chk_condition = array('merchantID'=>$merchantID);
					 $app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);
					 $udata['firstLogin'] = 1;
					 if(!empty($app_integration))
					 {
					
					
						$this->general_model->update_row_data('app_integration_setting',$chk_condition, $data);
						$chk_condition1 = array('merchID'=>$merchantID);
						$this->general_model->update_row_data('tbl_merchant_data',$chk_condition1, $udata);
						
						$user = $this->session->userdata('logged_in');
						$user['active_app'] = '5';
						$user['firstLogin'] = 1;
						$this->session->set_userdata('logged_in',$user);
                 
					}
					else
					{
						
						$company['companyName'] = "Default Chargezoom";
						
						$company['merchantID'] = $merchantID;
							$company['date_added'] =date('Y-m-d H:i:s');
						$this->general_model->insert_row('tbl_company', $company);
						$this->general_model->insert_row('app_integration_setting', $data);
						$chk_condition1 = array('merchID'=>$merchantID);
					    $this->general_model->update_row_data('tbl_merchant_data',$chk_condition1, $udata);
						$user = $this->session->userdata('logged_in');
						$user['active_app'] = '5';
						$user['firstLogin'] = 1;
						$this->session->set_userdata('logged_in',$user);
				   
					}


					
					 if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchantID)) == '0')
    			   {  
					     
						 $fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];
						
						   $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						
						  foreach($templatedatas as $templatedata)
						  {
						   $insert_data = array('templateName'  =>$templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									 'merchantID'      => $merchantID,
									 'fromEmail'      => $fromEmail,
									 'message'		  => $templatedata['message'],
									 'emailSubject'   => $templatedata['emailSubject'],
									 'createdAt'      => date('Y-m-d H:i:s') 	
								 );
					    	 $this->general_model->insert_row('tbl_email_template', $insert_data);
						
						}
	                }
    			   
        
					 redirect(base_url().'company/home/index');
						
	    
	}
	
	public function final_auth_index()
	{
	    
		         $data1['login_info'] 	= $this->session->userdata('logged_in');
				 $merchantID 				= $data1['login_info']['merchID'];
				 
				    $data['appIntegration']    = "5";
					$data['merchantID']        = $merchantID;
				    $chk_condition = array('merchantID'=>$merchantID);
					 $app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);
					 $udata['firstLogin'] = 1;
					 if(!empty($app_integration))
					 {
					
					
						$this->general_model->update_row_data('app_integration_setting',$chk_condition, $data);
						$chk_condition1 = array('merchID'=>$merchantID);
						$this->general_model->update_row_data('tbl_merchant_data',$chk_condition1, $udata);
						
						$user = $this->session->userdata('logged_in');
						$user['active_app'] = '5';
						$user['firstLogin'] = 1;
						$this->session->set_userdata('logged_in',$user);
                 
					}
					else
					{
						
						$company['companyName'] = "Default Chargezoom";
						
						$company['merchantID'] = $merchantID;
							$company['date_added'] =date('Y-m-d H:i:s');
						$this->general_model->insert_row('tbl_company', $company);
						$this->general_model->insert_row('app_integration_setting', $data);
						$chk_condition1 = array('merchID'=>$merchantID);
					    $this->general_model->update_row_data('tbl_merchant_data',$chk_condition1, $udata);
						$user = $this->session->userdata('logged_in');
						$user['active_app'] = '5';
						$user['firstLogin'] = 1;
						$this->session->set_userdata('logged_in',$user);
				   
					}
					$chk_condition1 = array('merchID'=>$merchantID);
				    $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$chk_condition1);

				    if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                	{
					    /* Start campaign in hatchbuck CRM*/  
					    $this->load->library('hatchBuckAPI');
				    	
				        $merchantData['merchant_type'] = 'No Accounting Package';        
				        
				        $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
				        if($status['statusCode'] == 400){
				        	$resource = $this->hatchbuckapi->createContact($merchantData);
				            if($resource['contactID'] != '0'){
				                $contact_id = $resource['contactID'];
				                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
				            }
				        }
				        /* End campaign in hatchbuck CRM*/ 
				    }
					
					if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchantID)) == '0')
    			    {  
					     
						 $fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];
						
						   $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						
						  foreach($templatedatas as $templatedata)
						  {
						   $insert_data = array('templateName'  =>$templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									 'merchantID'      => $merchantID,
									 'fromEmail'      => $fromEmail,
									 'message'		  => $templatedata['message'],
									 'emailSubject'   => $templatedata['emailSubject'],
									 'createdAt'      => date('Y-m-d H:i:s') 	
								 );
					    	 $this->general_model->insert_row('tbl_email_template', $insert_data);
						
						}
	                }
    			   
        
					 redirect(base_url().'company/home/index');
	    
	}
	
	
}

