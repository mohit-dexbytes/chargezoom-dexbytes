<?php
/**
 * This Controller has EPX Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 
 * Also Applied ACH Process for TSYS Payment Gateway is given following oprations
 * create_customer_esale for Sale
 * Perform Customer Card oprtations using this controller
 * Create, Delete, Modify
 */

include APPPATH . 'third_party/EPX.class.php';

class EPXPayment extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $gatewayEnvironment;
    private $transactionByUser;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('company/customer_model', 'customer_model');
        $this->load->model('general_model');
        $this->load->model('company/company_model', 'company_model');
        $this->load->model('card_model');
        $this->load->config('EPX');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '5') {

            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
                  
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }

        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {

        redirect('company/Payments/payment_transaction', 'refresh');
    }

    public function update_invoice_date()
    {
        redirect('company/home/invoices', 'refresh');
    }
    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $cardID_upd = '';
        $custom_data_fields = [];
        $invoiceID  = $this->input->post('invoiceProcessID');
        $cardID = $this->input->post('CardID');
        if (!$cardID || empty($cardID)) {
        $cardID = $this->input->post('schCardID');
        }
        $echeckType = false;
        
        $gatlistval = $this->input->post('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
            $gatlistval = $this->input->post('gateway');
        }
        $gateway = $gatlistval;
        $sch_method = $this->input->post('sch_method');

        $cusproID = '';
        $error    = '';
        $cusproID = $this->input->post('customerProcessID');
        if ($this->input->post('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
            $CUST_NBR = $gt_result['gatewayUsername'];
            $MERCH_NBR = $gt_result['gatewayPassword'];
            $DBA_NBR = $gt_result['gatewaySignature'];
            $TERMINAL_NBR = $gt_result['extra_field_1'];
            $orderId = time();

            $in_data         = $this->company_model->get_invoice_data_pay($invoiceID);
           
            $comp_data       = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $this->input->post('customerID')));
            $companyID       = $comp_data['companyID'];
            $Customer_ListID = $in_data['Customer_ListID'];
            $customerID      = $Customer_ListID;
            $c_data          = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
            $amount = $this->input->post('inv_amount');
            $amount = number_format($amount,2,'.','');
            $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',
            );
            if (!empty($in_data)) {

                if ($sch_method == "2") {
                    $echeckType = true;
                    $getewayName = 'EPX ECheck';
                    $accountNumber = $this->input->post('acc_number');
                    if($accountNumber != '' || $cardID !=''){
                        
                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $accountName = $in_data['FullName'];
                            $accountNumber = $this->input->post('acc_number');
                            $routingNumber = $this->input->post('route_number');
                            $accountType = $accountDetails['accountType'];

                            $address1 =  $this->input->post('address1');
                            $address2 = $this->input->post('address2');
                            $city    = $this->input->post('city');
                            $country = $this->input->post('country');
                            $phone   =  $this->input->post('phone');
                            $state   =  $this->input->post('state');
                            $zipcode =  $this->input->post('zipcode');

                            $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $accountName = $card_data['accountName'];
                            $accountNumber = $card_data['accountNumber'];
                            $routingNumber = $card_data['routeNumber'];
                            $accountType = $card_data['accountType'];
                            
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     =  $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone = $card_data['Billing_Contact'];
                            $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                        }

                        $transaction['RECV_NAME'] = $accountName;
                        $transaction['ACCOUNT_NBR'] = $accountNumber;
                        $transaction['ROUTING_NBR'] = $routingNumber;

                        if($accountType == 'savings'){
                            $transaction['TRAN_TYPE'] = 'CKS2';
                        }else{
                            $transaction['TRAN_TYPE'] = 'CKC2';
                        }
                        if($address1 != ''){
                            $transaction['ADDRESS'] = $address1;
                        }
                        if($city != ''){
                            $transaction['CITY'] = $city;
                        }
                        if($zipcode != ''){
                            $transaction['ZIP_CODE'] = $zipcode;
                        }
                        if($comp_data['FirstName'] != ''){
                            $transaction['FIRST_NAME'] = $comp_data['FirstName'];
                        }
                        if($comp_data['LastName'] != ''){
                            $transaction['LAST_NAME'] = $comp_data['LastName'];
                        }
                        
                        
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Invalid request</strong>.</div>');
                    }
                } else if ($sch_method == "1") {
                    $getewayName = 'EPX';
                    $echeckType = false;
                    
                    $Customer_ListID = $in_data['Customer_ListID'];
                    $customerID      = $Customer_ListID;

                    $c_data = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));

                    $companyID = $c_data['companyID'];
                    if ($cardID == 'new1') {
                        $cardID_upd = $cardID;
                        $card_no    = $this->input->post('card_number');
                        $expmonth   = $this->input->post('expiry');
                        $exyear     = $this->input->post('expiry_year');
                        $cvv        = $this->input->post('cvv');

                        $address1 = $this->input->post('address1');
                        $address2 = $this->input->post('address2');
                        $city     = $this->input->post('city');
                        $country  = $this->input->post('country');
                        $phone    = $this->input->post('contact');
                        $state    = $this->input->post('state');
                        $zipcode  = $this->input->post('zipcode');
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                        $custom_data_fields['payment_type'] = $friendlyname;
                    } else {
                        $card_data = $this->card_model->get_single_card_data($cardID);
                        $card_no   = $card_data['CardNo'];
                        $cvv       = $card_data['CardCVV'];
                        $expmonth  = $card_data['cardMonth'];
                        $exyear    = $card_data['cardYear'];

                        $address1 = $card_data['Billing_Addr1'];
                        $address2 = $card_data['Billing_Addr2'];
                        $city     = $card_data['Billing_City'];
                        $zipcode  = $card_data['Billing_Zipcode'];
                        $state    = $card_data['Billing_State'];
                        $country  = $card_data['Billing_Country'];
                        $phone    = $card_data['Billing_Contact'];
                        $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                    }
                    $exyear1  = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $transaction['EXP_DATE'] = $exyear1.$expmonth;
                    $transaction['ACCOUNT_NBR'] = $card_no;
                    $transaction['TRAN_TYPE'] = 'CCE1';
                    $transaction['CARD_ENT_METH'] = 'E';
                    $transaction['INDUSTRY_TYPE'] = 'E';
                    if($address1 != ''){
                        $transaction['ADDRESS'] = $address1;
                    }
                    if($city != ''){
                        $transaction['CITY'] = $city;
                    }
                    if($zipcode != ''){
                        $transaction['ZIP_CODE'] = $zipcode;
                    }
                    if($comp_data['FirstName'] != ''){
                        $transaction['FIRST_NAME'] = $comp_data['FirstName'];
                    }
                    if($comp_data['LastName'] != ''){
                        $transaction['LAST_NAME'] = $comp_data['LastName'];
                    }
                    
     
                   
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Select Payment Type</strong>.</div>');
                }
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                    $transactionID = $result['AUTH_GUID'];
                    $txnID  = $in_data['TxnID'];
                    $message = 'SUCCESS';
                    $code = 100;
                    $ispaid = 'true';
                    $bamount = $in_data['BalanceRemaining'] - $amount;
                    if ($bamount > 0) {
                        $ispaid = 'false';
                    }

                    $app_amount = $in_data['AppliedAmount'] - $amount;
                    $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
                        $condition  = array('TxnID' => $in_data['TxnID']);

                    $this->general_model->update_row_data('chargezoom_test_invoice', $condition, $data);
                    $this->session->set_flashdata('success', 'Successfully Processed');
                    
                    $condition_mail = array('templateType' => '15', 'merchantID' => $user_id);
                    $ref_number     = $in_data['RefNumber'];
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $c_data['Contact'];
                    $company        = $c_data['companyName'];
                    $customer       = $c_data['FullName'];
                    

                } else {
                    $transactionID = 'TXNFAILED'.time();
                    $message = $result['AUTH_RESP_TEXT'];
                    $code = 300;
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['AUTH_RESP_TEXT'] . '</div>');
                }
                $result['transactionid'] = $transactionID;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID ='', $this->resellerID, $in_data['TxnID'], $echeckType, $this->transactionByUser, $custom_data_fields);

                if (($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' && $chh_mail == '1') {
                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                    $ref_number     = $in_data['RefNumber'];
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $c_data['Contact'];
                    $company        = $c_data['companyName'];
                    $customer       = $c_data['FullName'];
                    $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $transactionID);
                }
                    
                if ($cusproID == "2") {
                    redirect('company/home/view_customer/' . $customerID, 'refresh');
                }
                if ($cusproID == "3" && $in_data['TxnID'] != '') {
                    redirect('company/home/invoice_details/' . $in_data['TxnID'], 'refresh');
                }
                $trans_id     = $transactionID;
                $invoice_IDs  = array();
                $receipt_data = array(
                    'proccess_url'      => 'company/home/invoices',
                    'proccess_btn_text' => 'Process New Invoice',
                    'sub_header'        => 'Sale',
                );

                $this->session->set_userdata("receipt_data", $receipt_data);
                $this->session->set_userdata("invoice_IDs", $invoice_IDs);
                if ($cusproID == "1") {
                    redirect('company/home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
                }
                redirect('company/home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
            }    
            
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Select Gateway</strong>.</div>');
        }
        
        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
        
        if ($cusproID == "2") {
            redirect('company/home/view_customer/' . $customerID, 'refresh');
        }
        if ($cusproID == "3" && $in_data['TxnID'] != '') {
            redirect('company/home/invoice_details/' . $in_data['TxnID'], 'refresh');
        }
        $trans_id     = $result['transactionid'];
        $invoice_IDs  = array();
        $receipt_data = array(
            'proccess_url'      => 'company/home/invoices',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         =>  $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        if ($cusproID == "1") {
            redirect('company/home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
        }
        redirect('company/home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
    }

    public function insert_new_data()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $customer  = $this->input->post('customerID11');
        $c_data    = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customer, 'qbmerchantID' => $merchID));
        $companyID = $c_data['companyID'];

        if ($this->input->post('formselector') == '1') {
            $card_no  = $this->card_model->encrypt($this->input->post('card_number'));
            $expmonth = $this->input->post('expiry');
            $exyear   = $this->input->post('expiry_year');
            $cvv      = $this->card_model->encrypt($this->input->post('cvv'));
            $acc_number   = $route_number   = $acc_name   = $secCode   = $acct_type   = $acct_holder_type   = '';
            $type         = $this->general_model->getType($this->input->post('card_number'));
            $friendlyname = $type . ' - ' . substr($this->input->post('card_number'), -4);
        }

        if ($this->input->post('formselector') == '2') {
            $acc_number       = $this->input->post('acc_number');
            $route_number     = $this->input->post('route_number');
            $acc_name         = $this->input->post('acc_name');
            $secCode          = $this->input->post('secCode');
            $acct_type        = $this->input->post('acct_type');
            $acct_holder_type = $this->input->post('acct_holder_type');
            $card_no          = $expmonth          = $exyear          = $cvv          = '00';
            $type             = 'Echeck';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
        }
        $b_addr1   = $this->input->post('address1');
        $b_addr2   = $this->input->post('address2');
        $b_city    = $this->input->post('city');
        $b_state   = $this->input->post('state');
        $b_zipcode = $this->input->post('zipcode');
        $b_country = $this->input->post('country');
        $b_contact = $this->input->post('contact');

        $insert_array = array(
            'cardMonth'                => $expmonth,
            'cardYear'                 => $exyear,
            'CustomerCard'             => $card_no,
            'CardCVV'                  => $cvv,
            'CardType'                 => $type,
            'customerListID'           => $customer,
            'merchantID'               => $merchID,
            'companyID'                => $companyID,
            'Billing_Addr1'            => $b_addr1,
            'Billing_Addr2'            => $b_addr2,
            'Billing_City'             => $b_city,
            'Billing_State'            => $b_state,
            'Billing_Zipcode'          => $b_zipcode,
            'Billing_Country'          => $b_country,
            'Billing_Contact'          => $b_contact,

            'customerCardfriendlyName' => $friendlyname,
            'accountNumber'            => $acc_number,
            'routeNumber'              => $route_number,
            'accountName'              => $acc_name,
            'accountType'              => $acct_type,
            'accountHolderType'        => $acct_holder_type,
            'secCodeEntryMethod'       => $secCode,
            'createdAt'                => date("Y-m-d H:i:s"),
        );

        $id = $this->card_model->insert_card_data($insert_array);
        if ($id) {
            $this->session->set_flashdata('success', 'Successfully Inserted');

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }

        redirect('company/home/view_customer/' . $customer, 'refresh');
    }

    public function update_card_data()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $cardID = $this->input->post('edit_cardID');

        $qq = $this->db1->select('customerListID')->from('customer_card_data')->where(array('CardID' => $cardID, 'merchantID' => $merchID))->get();
        if ($qq->num_rows > 0) {
            $customer = $qq->row_array()['customerListID'];
        }
        if ($this->input->post('edit_expiry') != '') {

            $expmonth     = $this->input->post('edit_expiry');
            $exyear       = $this->input->post('edit_expiry_year');
            $cvv          = $this->card_model->encrypt($this->input->post('edit_cvv'));
            $friendlyname = '';
            $acc_number   = $route_number   = $acc_name   = $secCode   = $acct_type   = $acct_holder_type   = '';
        }

        if ($this->input->post('edit_acc_number') !== '') {
            $acc_number       = $this->input->post('edit_acc_number');
            $route_number     = $this->input->post('edit_route_number');
            $acc_name         = $this->input->post('edit_acc_name');
            $secCode          = $this->input->post('edit_secCode');
            $acct_type        = $this->input->post('edit_acct_type');
            $acct_holder_type = $this->input->post('edit_acct_holder_type');
            $card_no          = $expmonth          = $exyear          = $cvv          = '00';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
        }

        $b_addr1   = $this->input->post('baddress1');
        $b_addr2   = $this->input->post('baddress2');
        $b_city    = $this->input->post('bcity');
        $b_state   = $this->input->post('bstate');
        $b_country = $this->input->post('bcountry');
        $b_contact = $this->input->post('bcontact');
        $b_zip     = $this->input->post('bzipcode');

        $condition    = array('CardID' => $cardID);
        $insert_array = array(
            'cardMonth'                => $expmonth,
            'cardYear'                 => $exyear,
            'CardCVV'                  => $cvv,

            'customerCardfriendlyName' => $friendlyname,

            'Billing_Addr1'            => $b_addr1,
            'Billing_Addr2'            => $b_addr2,
            'Billing_City'             => $b_city,
            'Billing_State'            => $b_state,
            'Billing_Zipcode'          => $b_zip,
            'Billing_Country'          => $b_country,
            'Billing_Contact'          => $b_contact,
            'accountNumber'            => $acc_number,
            'routeNumber'              => $route_number,
            'accountName'              => $acc_name,
            'accountType'              => $acct_type,
            'accountHolderType'        => $acct_holder_type,
            'secCodeEntryMethod'       => $secCode,

            'updatedAt'                => date("Y-m-d H:i:s"),
        );

        if ($this->input->post('edit_card_number') != '') {
            $card_no                                  = $this->input->post('edit_card_number');
            $type                                     = $this->general_model->getType($card_no);
            $friendlyname                             = $type . ' - ' . substr($card_no, -4);
            $insert_array['customerCardfriendlyName'] = $friendlyname;

            $insert_array['CustomerCard'] = $this->card_model->encrypt($card_no);
            $insert_array['CardType']     = $type;
        }

        $id = $this->card_model->update_card_data($condition, $insert_array);
        if ($id) {
            $this->session->set_flashdata('success', 'Successfully Updated');

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }

        redirect('company/home/view_customer/' . $customer, 'refresh');
    }

    public function delete_card_data()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        if (!empty($this->input->post('delCardID'))) {

            $cardID   = $this->input->post('delCardID');
            $customer = $this->input->post('delCustodID');

            $num = $this->general_model->get_num_rows('tbl_chargezoom_subscriptions', array('CardID' => $cardID, 'merchantDataID' => $merchID));
            if ($num == 0) {
                $sts = $this->card_model->delete_card_data(array('CardID' => $cardID));
                if ($sts) {
                    $this->session->set_flashdata('success', 'Successfully Deleted');

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Transaction Failed - This card cannot be deleted. Please change Customer card on Subscription</strong></div>');
            }
            redirect('company/home/view_customer/' . $customer, 'refresh');
        }
    }

    public function create_customer_sale()
    {

        
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
        
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
           
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $plantype = $this->general_model->chk_merch_plantype_status($user_id);
        $transactionID = 'TXNFAILED'.time();

        if (!empty($this->input->post())) {

            $custom_data_fields = [];
            $applySurcharge = false;
            if($this->czsecurity->xssCleanPostInput('invoice_id')){
                $applySurcharge = true;
            }
            if (!empty($this->input->post('invoice_number'))) {
                $custom_data_fields['invoice_number'] = $this->input->post('invoice_number');
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->input->post('po_number');
            }

            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $merchant_condition = [
                'merchID' => $user_id,
            ];

            $inputData = $this->input->post();

            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$totalamount  = $this->input->post('totalamount');
			$amount = $this->input->post('totalamount');
            // update amount with surcharge 
            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                $amount += round($surchargeAmount, 2);
                $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                
            }
            $totalamount  = $amount;
            $checkPlan = check_free_plan_transactions();

            $inv_array   = array();
            $inv_invoice = array();
            $invoiceIDs  = array();
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                if ($this->session->userdata('logged_in')) {
                    $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }
                $customerID = $this->input->post('customerID');

                $comp_data = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                $companyID = $comp_data['companyID'];

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $cardID = $this->input->post('card_list');
                if ($this->input->post('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');

                    $exyear  = $this->input->post('expiry_year');
                    $exyear1 = substr($exyear, 2);
                    $expry   = $exyear1.$expmonth;
                    $cvv     = $this->input->post('cvv');
                    $cardType = $this->general_model->getType($card_no);
                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                    $custom_data_fields['payment_type'] = $friendlyname;
                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);

                    $card_no  = $card_data['CardNo'];
                    $expmonth = $card_data['cardMonth'];
                    $exyear   = $card_data['cardYear'];
                    $exyear1  = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $exyear1.$expmonth;
                    $cvv   = $card_data['CardCVV'];
                }
                /*Added card type in transaction table*/
                $card_type = $this->general_model->getType($card_no);
                $friendlyname = $card_type . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                $orderId = time();
                if($this->input->post('invoice_number')){
                    $orderId = $this->input->post('invoice_number');
                }
                $address1 = ($this->input->post('baddress1') != '')?$this->input->post('baddress1'):'None';
                $address2 = ($this->input->post('baddress1') != '')?$this->input->post('baddress1'):'None';
                $zipcode = ($this->input->post('bzipcode') != '')?$this->input->post('bzipcode'):'None';
                $city = ($this->input->post('bcity') != '')?$this->input->post('bcity'):'None';
                $state = ($this->input->post('bstate') != '')?$this->input->post('bstate'):'AZ';
                $country = ($this->input->post('bcountry') != '')?$this->input->post('bcountry'):'USA';
                $phone = ($this->input->post('phone') != '')?$this->input->post('phone'):'None';
                $firstName = ($this->input->post('firstName') != '')?$this->input->post('firstName'):'None';
                $lastName = ($this->input->post('lastName') != '')?$this->input->post('lastName'):'None';
                $companyName = ($this->input->post('companyName') != '')?$this->input->post('companyName'):'None';

                $email = ($this->input->post('email') != '')?$this->input->post('email'):'chargezoom@chargezoom.com';

                $amount = number_format($amount,2,'.','');

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE1',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',


                );
                if($firstName != ''){
                    $transaction['FIRST_NAME'] = $firstName;
                }
                if($lastName != ''){
                    $transaction['LAST_NAME'] = $lastName;
                }
                if($cvv != ''){
                    $transaction['CVV2'] = $cvv;
                }
                if($address1 != ''){
                    $transaction['ADDRESS'] = $address1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                
               
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                    $message = $result['AUTH_RESP_TEXT'];
                    $transactionID = $result['AUTH_GUID'];

                    $invoicePayAmounts = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->input->post('invoice_pay_amount'));
                    }
                    $refNum = array();
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

                            if (!empty($theInvoice)) {
                                $amount_data = $theInvoice['BalanceRemaining'];
                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                    $actualInvoicePayAmount += $surchargeAmount;
                                    $amount_data += $surchargeAmount;

                                    $updatedInvoiceData = [
                                        'inID' => $inID,
                                        'merchantID' => $user_id,
                                        'amount' => $surchargeAmount,
                                    ];
                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,5);
                                }
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                $refnum[] = $theInvoice['RefNumber'];
                                
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 'true';

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 'false';
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $txnAmount = $actualInvoicePayAmount;
                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                
                                $tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                $transactiondata = array();
                                $result['transactionid'] = $transactionID;
                                $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID, $crtxnID = '', $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                
                            }
                            ++$payIndex;
                        }
                    } else {

                        $transactiondata = array();
                        $inID            = '';
                        $result['transactionid'] = $transactionID;
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $totalamount, $merchantID, $crtxnID='', $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                    }
                    if ($cardID == "new1" && !($this->input->post('tc'))) {
                        $card_no = $this->input->post('card_number');

                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->input->post('expiry');

                        $exyear = $this->input->post('expiry_year');

                        $cvv       = $this->input->post('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address1,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $phone,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }

                    $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user_id));
                    $fromEmail     = $merchant_data['merchantEmail'];
                    
                    if ($chh_mail == '1') {
                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);

                        if (!empty($refNum)) {
                            $ref_number = implode(',', $refNum);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $comp_data['Contact'];
                        $company  = $comp_data['companyName'];
                        $customer = $comp_data['FullName'];
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $transactionID);
                    }
                    $condition_mail = array('templateType' => '15', 'merchantID' => $user_id);

                    if (!empty($refNum)) {
                        $ref_number = implode(',', $refNum);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['Contact'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['FullName'];
                   
                    $this->session->set_flashdata('success', 'Transaction Successful');


                } else {
                    
                    $transactiondata = array();
                    $inID            = '';
                    $result['transactionid'] = $transactionID;
                    $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $totalamount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                    $message = $result['AUTH_RESP_TEXT'];

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - ' . $result['AUTH_RESP_TEXT'] . '</strong></div>');
                }

               
                
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>');
            }
            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->input->post('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $transactionID,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'billing_address1'  => $this->input->post('baddress1'),
                'billing_address2'  => $this->input->post('baddress2'),
                'billing_city'      => $this->input->post('bcity'),
                'billing_zip'       => $this->input->post('bzipcode'),
                'billing_state'     => $this->input->post('bstate'),
                'billing_country'   => $this->input->post('bcountry'),
                'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'shipping_address1' => $this->input->post('address1'),
                'shipping_address2' => $this->input->post('address2'),
                'shipping_city'     => $this->input->post('city'),
                'shipping_zip'      => $this->input->post('zipcode'),
                'shipping_state'    => $this->input->post('state'),
                'shiping_counry'    => $this->input->post('country'),
                'Phone'             => $this->input->post('phone'),
                'Contact'           => $this->input->post('email'),
                'proccess_url'      => 'company/Payments/create_customer_sale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         =>  $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('company/home/transation_sale_receipt', 'refresh');

        }
        redirect('company/Payments/create_customer_sale', 'refresh');
    }

    /*****************Authorize Transaction***************/

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $responseId = 'TXNFAILED-'.time();

        $merchantID = $user_id;
        $amount = $this->input->post('totalamount');

        $inputData = $this->input->post();
        $plantype = $this->general_model->chk_merch_plantype_status($user_id);
        if (!empty($this->input->post())) {

            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $customerID = $this->input->post('customerID');
                $comp_data  = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customerID));
                $companyID  = $comp_data['companyID'];

                
                $cardID      = $this->input->post('card_list');
                if ($this->input->post('card_number') != "" && $cardID == 'new1') {

                    $card_no = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');

                    $exyear  = $this->input->post('expiry_year');
                    $exyear1 = substr($exyear, 2);
                    $expry   = $exyear1.$expmonth;
                    $cvv     = $this->input->post('cvv');
                    $cardType = $this->general_model->getType($card_no);
                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                    $custom_data_fields['payment_type'] = $friendlyname;
                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $expmonth = $card_data['cardMonth'];
                    $card_no  = $card_data['CardNo'];

                    $exyear  = $card_data['cardYear'];
                    $exyear1 = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $exyear1.$expmonth;
                    $cvv   = $card_data['CardCVV'];
                    $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                }
                $orderId = time();
                $amount = number_format($amount,2,'.','');
                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE2',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',


                );

                
                if (!empty($po_number)) {
                    $transaction['ORDER_NBR'] = $po_number;
                }
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $transaction['INVOICE_NBR'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }
                if($cvv != ''){
                    $transaction['CVV2'] = $cvv;
                }
                if($inputData['address1'] != ''){
                    $transaction['ADDRESS'] = $inputData['address1'];
                }
                if($inputData['bcity'] != ''){
                    $transaction['CITY'] = $inputData['bcity'];
                }
                
                if($inputData['bzipcode'] != ''){
                    $transaction['ZIP_CODE'] = $inputData['bzipcode'];
                }
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                
                
                $crtxnID = '';
                $invID =$inID = '';
                $responseId  = 'TXNFail-'.time();
                
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                    $responseId = $result['AUTH_GUID'];
                    /* This block is created for saving Card info in encrypted form  */

                    if ($cardID == "new1" && !($this->input->post('tc'))) {
                        $card_no   = $this->input->post('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->input->post('expiry');

                        $exyear = $this->input->post('expiry_year');

                        $cvv       = $this->input->post('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $inputData['address1'],
                            'Billing_Addr2'   => $inputData['address2'],
                            'Billing_City'    => $inputData['bcity'],
                            'Billing_State'   => $inputData['bstate'],
                            'Billing_Country' => $inputData['bcountry'],
                            'Billing_Contact' => $inputData['phone'],
                            'Billing_Zipcode' => $inputData['bzipcode'],
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $transactiondata = array();
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - ' . $result['AUTH_RESP_TEXT'] . '</strong></div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>');
            }
            $result['transactionid'] = $responseId;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);

            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'billing_address1'  => $this->input->post('baddress1'),
                'billing_address2'  => $this->input->post('baddress2'),
                'billing_city'      => $this->input->post('bcity'),
                'billing_zip'       => $this->input->post('bzipcode'),
                'billing_state'     => $this->input->post('bstate'),
                'billing_country'   => $this->input->post('bcountry'),
                'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'shipping_address1' => $this->input->post('address1'),
                'shipping_address2' => $this->input->post('address2'),
                'shipping_city'     => $this->input->post('city'),
                'shipping_zip'      => $this->input->post('zipcode'),
                'shipping_state'    => $this->input->post('state'),
                'shiping_counry'    => $this->input->post('country'),
                'Phone'             => $this->input->post('phone'),
                'Contact'           => $this->input->post('email'),
                'proccess_url'      => 'company/Payments/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         =>  $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('company/home/transation_sale_receipt', 'refresh');
        }
        redirect('company/Payments/create_customer_auth', 'refresh');
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $transactionid = $transactionID = 'TXNFAILED'.time();
            $tID     = $this->input->post('txnID');
            $con     = array('transactionID' => $tID, 'merchantID' => $merchantID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            if ($paydata['gatewayID'] > 0) {

                $gatlistval = $paydata['gatewayID'];

                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
                if ($this->input->post('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                if ($tID != '' && !empty($gt_result)) {

                    $customerID = $paydata['customerListID'];
                    $amount = $paydata['transactionAmount'];

                    $customerID = $paydata['customerListID'];
                    $comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));


                    $CUST_NBR = $gt_result['gatewayUsername'];
                    $MERCH_NBR = $gt_result['gatewayPassword'];
                    $DBA_NBR = $gt_result['gatewaySignature'];
                    $TERMINAL_NBR = $gt_result['extra_field_1'];
                    $orderId = time();
                    $amount = number_format($amount,2,'.','');

                    $transaction = array(
                        'CUST_NBR' => $CUST_NBR,
                        'MERCH_NBR' => $MERCH_NBR,
                        'DBA_NBR' => $DBA_NBR,
                        'TERMINAL_NBR' => $TERMINAL_NBR,
                        'ORIG_AUTH_GUID' => $tID,
                        'TRAN_TYPE' => 'CCE4',
                        'CARD_ENT_METH' => 'Z',
                        'INDUSTRY_TYPE' => 'E',
                        'AMOUNT' => $amount,
                        'TRAN_NBR' => rand(1,10),
                        'INVOICE_NBR' => $orderId,
                        'BATCH_ID' => time(),
                        'VERBOSE_RESPONSE' => 'Y',
                    );
                   
                    
                    $gatewayTransaction              = new EPX();
                    $result = $gatewayTransaction->processTransaction($transaction);
                        
                    if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                        $transactionid = $transactionID = $result['AUTH_GUID'];

                        $condition = array('transactionID' => $tID, 'merchantID' => $merchantID);

                        $update_data = array('transaction_user_status' => "4");

                        $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                        if ($chh_mail == '1') {
                            $condition  = array('transactionID' => $tID);
                            $customerID = $paydata['customerListID'];

                            $comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                            $tr_date    = date('Y-m-d H:i:s');
                            $ref_number = $tID;
                            $toEmail    = $comp_data['Contact'];
                            $company    = $comp_data['companyName'];
                            $customer   = $comp_data['FullName'];
                            $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
                        }
                        $condition_mail = array('templateType' => '15', 'merchantID' => $merchantID);
                        $comp_data      = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                        $customerID     = $paydata['customerListID'];
                        $ref_number     = '';
                        $tr_date        = date('Y-m-d H:i:s');
                        $toEmail        = $comp_data['Contact'];
                        $company        = $comp_data['companyName'];
                        $customer       = $comp_data['FullName'];

                        $this->session->set_flashdata('success', 'Successfully Captured Authorization');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - ' . $result['AUTH_RESP_TEXT'] . '</strong>.</div>');
                    }
                    $result['transactionid'] = $transactionid;
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>');
            }
            $invoice_IDs = array();

            $receipt_data = array(
                'proccess_url'      => 'company/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if (!isset($transactionID) || $transactionID == '') {
                $transactionid = 'TXNFAILED'.time();
            } else {
                $transactionid = $transactionID;
            }
            redirect('company/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');
        }
    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->input->post('invoice_number'))) {
                $custom_data_fields['invoice_number'] = $this->input->post('invoice_number');
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->input->post('po_number');
            }

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $responseId = 'TXNFAILED-'.time();
            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $customerID = $this->input->post('customerID');
            $comp_data  = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customerID));
            $companyID  = $comp_data['companyID'];
            $payableAccount = $this->input->post('payable_ach_account');
            if ($gatlistval != "" && !empty($gt_result)) {
                
                if($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName' => $this->input->post('account_name'),
                        'accountNumber' => $this->input->post('account_number'),
                        'routeNumber' => $this->input->post('route_number'),
                        'accountType' => $this->input->post('acct_type'),
                        'accountHolderType' => $this->input->post('acct_holder_type'),
                        'Billing_Addr1' => $this->input->post('baddress1'),
                        'Billing_Addr2' => $this->input->post('baddress2'),
                        'Billing_City' => $this->input->post('bcity'),
                        'Billing_Country' => $this->input->post('bcountry'),
                        'Billing_Contact' => $this->input->post('phone'),
                        'Billing_State' => $this->input->post('bstate'),
                        'Billing_Zipcode' => $this->input->post('bzipcode'),
                        'customerListID' => $customerID,
                        'companyID'     => $companyID,
                        'merchantID'   => $merchantID,
                        'createdAt'     => date("Y-m-d H:i:s"),
                       
                    ];
                    $accountNumber = $this->czsecurity->xssCleanPostInput('account_number');
                    $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                    $custom_data_fields['payment_type'] = $friendlyname;
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                    $custom_data_fields['payment_type'] = $accountDetails['customerCardfriendlyName'];
                }
                
                $address1 = ($this->input->post('baddress1') != '')?$this->input->post('baddress1'):'None';
                $address2 = ($this->input->post('baddress1') != '')?$this->input->post('baddress1'):'None';
                $zipcode = ($this->input->post('bzipcode') != '')?$this->input->post('bzipcode'):'None';
                $city = ($this->input->post('bcity') != '')?$this->input->post('bcity'):'None';
                $state = ($this->input->post('bstate') != '')?$this->input->post('bstate'):'AZ';
                $country = ($this->input->post('bcountry') != '')?$this->input->post('bcountry'):'USA';
                $phone = ($this->input->post('phone') != '')?$this->input->post('phone'):'None';
                $firstName = ($this->input->post('firstName') != '')?$this->input->post('firstName'):'None';
                $lastName = ($this->input->post('lastName') != '')?$this->input->post('lastName'):'None';
                $companyName = ($this->input->post('companyName') != '')?$this->input->post('companyName'):'None';

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $amount         = $this->input->post('totalamount');
                
                $amount = number_format($amount,2,'.','');
                if($accountDetails['accountType'] == 'savings'){
                    $txnType = 'CKS2'; 
                }else{
                    $txnType = 'CKC2';
                }
                $orderId = time();

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => $txnType,
                    'ACCOUNT_NBR' => $accountDetails['accountNumber'],
                    'ROUTING_NBR' => $accountDetails['routeNumber'],
                    'RECV_NAME' => $accountDetails['accountName'],
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',


                );
                if($firstName != ''){
                    $transaction['FIRST_NAME'] = $firstName;
                }
                if($lastName != ''){
                    $transaction['LAST_NAME'] = $lastName;
                }
                if($address1 != ''){
                    $transaction['ADDRESS'] = $address1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
                

                $gatewayTransaction = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    
                    $message = $result['AUTH_RESP_TEXT'];
                    $responseId = $result['AUTH_GUID'];

                    $invoicePayAmounts = [];
                    $invoiceIDs = [];
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->input->post('invoice_pay_amount'));
                    }
                    $refNum = array();
                    
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
                            if (!empty($theInvoice)) {
                                $amount_data = $theInvoice['BalanceRemaining'];
                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                $refnum[] = $theInvoice['RefNumber'];
                                
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 'true';
                                }else{
                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 'false';
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount; 
                                }
                                $txnAmount = $actualInvoicePayAmount;
                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                
                                $tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                $transactiondata = array();
                                $result['transactionid'] = $responseId;
                                $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID,'', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                            }
                            $payIndex++;
                        }
                    } else {

                        $transactiondata = array();
                        $inID = '';
                        $result['transactionid'] = $responseId;
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, '', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($this->input->post('tr_checked')) {
                        $chh_mail = 1;
                    } else {
                        $chh_mail = 0;
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number = '';
                    $tr_date    = date('Y-m-d H:i:s');
                    $toEmail    = $comp_data['Contact'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['FullName'];

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }
                    if ($chh_mail == '1') {

                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                    }
                  
                    $this->session->set_flashdata('success', ' Transaction Successful');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['AUTH_RESP_TEXT'] . '</div>');
                }
                

                $receipt_data = array(
                    'transaction_id'    => $responseId,
                    'IP_address'        => $_SERVER['REMOTE_ADDR'],
                    'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                    'billing_address1'  => $this->input->post('baddress1'),
                    'billing_address2'  => $this->input->post('baddress2'),
                    'billing_city'      => $this->input->post('bcity'),
                    'billing_zip'       => $this->input->post('bzipcode'),
                    'billing_state'     => $this->input->post('bstate'),
                    'billing_country'   => $this->input->post('bcountry'),
                    'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                    'shipping_address1' => $this->input->post('address1'),
                    'shipping_address2' => $this->input->post('address2'),
                    'shipping_city'     => $this->input->post('city'),
                    'shipping_zip'      => $this->input->post('zipcode'),
                    'shipping_state'    => $this->input->post('state'),
                    'shiping_counry'    => $this->input->post('country'),
                    'Phone'             => $this->input->post('phone'),
                    'Contact'           => $this->input->post('email'),
                    'proccess_url'      => 'company/Payments/create_customer_esale',
                    'proccess_btn_text' => 'Process New Sale',
                    'sub_header'        => 'Sale'
                );

                $this->session->set_userdata("receipt_data", $receipt_data);
                redirect('company/home/transation_sale_receipt', 'refresh');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            redirect('company/Payments/create_customer_esale', 'refresh');
        }
        redirect('company/Payments/create_customer_esale', 'refresh');
    }

    /**********END****************/

    public function chk_friendly_name()
    {
        $res       = array();
        $frname    = $this->input->post('frname');
        $condition = array('gatewayFriendlyName' => $frname);
        $num       = $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
        if ($num) {
            $res = array('gfrname' => 'Friendly name already exist', 'status' => 'false');
        } else {
            $res = array('status' => 'true');
        }
        echo json_encode($res);
        die;
    }

    /*****************Test TSYS Validity***************/

    public function get_card_edit_data()
    {

        if ($this->input->post('cardID') != "") {
            $cardID = $this->input->post('cardID');
            $data   = $this->card_model->get_single_mask_card_data($cardID);
            echo json_encode(array('status' => 'success', 'card' => $data));
            die;
        }
        echo json_encode(array('status' => 'success'));
        die;
    }

    public function check_vault()
    {

        $card         = '';
        $card_name    = '';
        $customerdata = array();
        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $merchantID = $da['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $merchantID = $da['merchantID'];
        }

        if ($this->input->post('customerID') != "") {

            $customerID = $this->input->post('customerID');

            $condition    = array('ListID' => $customerID, 'qbmerchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('chargezoom_test_customer', $condition);

            if (!empty($customerdata)) {

                $customerdata['status'] = 'success';

                $ach_data = $this->card_model->get_ach_info_data($customerID);
                $ACH      = [];
                if (!empty($ach_data)) {
                    foreach ($ach_data as $card) {
                        $ACH[] = $card['CardID'];
                    }
                }
                $recentACH                          = end($ACH);
                $customerdata['ach_data']           = $ach_data;
                $customerdata['recent_ach_account'] = $recentACH;
                $card_data = $this->card_model->get_card_expiry_data($customerID);
                $cardArr   = [];
                if (!empty($card_data)) {
                    foreach ($card_data as $card) {
                        $cardArr[] = $card['CardID'];
                    }
                }
                $recentCard                  = end($cardArr);
                $customerdata['card']        = $card_data;
                $customerdata['recent_card'] = $recentCard;

                $invoices = $this->customer_model->get_invoice_upcomming_data($customerID, $merchantID);

                $table = '';

                if (!empty($invoices)) {
                    $table .= '<table class="col-md-offset-3 mytable" width="50%">';
                    $table .= "<tr>
	    <th class='text-left'>Invoice</th>
		<th class='text-right'>Amount</th>
		</tr>";

                    foreach ($invoices as $inv) {
                        if ($inv['status'] != 'Cancel') {
                            $table .= "<tr>
				<td class='text-left'><input type='checkbox' name='inv' value='" . $inv['BalanceRemaining'] . "' class='test'  data-checkclass='" . $inv['RefNumber'] . "' rel='" . $inv['TxnID'] . "'/> " . $inv['RefNumber'] . "</td>
				<td class='text-right'>" . '$' . number_format((float) $inv['BalanceRemaining'], 2, '.', ',') . "</td>
				</tr>";
                        }
                    }
                    $table .= "</table>";
                } else {
                    $table .= '';
                }

                if (empty($customerdata['companyName'])) {
                    $customerdata['companyName'] = $customerdata['FullName'];
                }

                $customerdata['invoices'] = $table;
                echo json_encode($customerdata);
                die;
            }
        }
    }

    public function view_transaction()
    {

        $invoiceID = $this->input->post('invoiceID');

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];
        $transactions       = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id);

        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                ?>
				<tr>
					<td class="text-left"><?php echo ($transaction['transactionID']) ? $transaction['transactionID'] : ''; ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($transaction['transactionAmount'], 2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo ucfirst($transaction['transactionType']); ?></td>

					<td class="text-right visible-lg"><?php if ($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') {?> <span class="">Success</span><?php } else {?> <span class="">Failed</span> <?php }?></td>

				</tr>

<?php }
        } else {
            echo '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
        }
        die;
    }

    public function check_transaction_payment()
    {
        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $user_id = $da['merchantID'];
        }

        if (!empty($this->input->post('trID'))) {

            $trID      = $this->input->post('trID');
            $av_amount = $this->input->post('ref_amount');
            $p_data = $this->customer_model->chk_transaction_details(array('id' => $trID, 'tr.merchantID' => $user_id));

            if (!empty($p_data)) {
                if ($p_data['transactionAmount'] >= $av_amount) {
                    $resdata['status'] = 'success';
                } else {
                    $resdata['status']  = 'error';
                    $resdata['message'] = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
                }
            } else {
                $resdata['status']  = 'error';
                $resdata['message'] = 'Invalid transactions ';
            }
        } else {
            $resdata['status']  = 'error';
            $resdata['message'] = 'Invalid request';
        }
        echo json_encode($resdata);
        die;
    }

    public function pay_multi_invoice()
    {
        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $custom_data_fields = [];
        $customerID = $this->input->post('customerID');
        $c_data     = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'FirstName', 'LastName', 'companyName','Contact'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));

        $companyID  = $c_data['companyID'];
        $cardID_upd = '';
        $invoices   = $this->input->post('multi_inv');
        $cardID  = $this->input->post('CardID1');
        $gateway = $this->input->post('gateway1');

        $cusproID = '';
        $cusproID = $this->input->post('customermultiProcessID');
        $checkPlan = check_free_plan_transactions();
        $transactionid = 'TXNFAILED-'.time();

        if ($checkPlan && !empty($invoices) && $cardID != "" && $gateway != "") {
            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            foreach ($invoices as $invoiceID) {

                $pay_amounts = $this->input->post('pay_amount' . $invoiceID);
                $in_data     = $this->company_model->get_invoice_data_pay($invoiceID);
                $resellerID  = $this->resellerID;
                if (!empty($in_data)) {

                    $customerID = $Customer_ListID = $in_data['Customer_ListID'];

                    if ($cardID == 'new1') {
                        $cardID_upd = $cardID;
                        $card_no    = $this->input->post('card_number');
                        $expmonth   = $this->input->post('expiry');
                        $exyear     = $this->input->post('expiry_year');
                        $cvv        = $this->input->post('cvv');

                        $address1 = $this->input->post('address1');
                        $address2 = $this->input->post('address2');
                        $city     = $this->input->post('city');
                        $country  = $this->input->post('country');
                        $phone    = $this->input->post('contact');
                        $state    = $this->input->post('state');
                        $zipcode  = $this->input->post('zipcode');
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                        $custom_data_fields['payment_type'] = $friendlyname;
                    } else {
                        $card_data = $this->card_model->get_single_card_data($cardID);
                        $card_no   = $card_data['CardNo'];
                        $cvv       = $card_data['CardCVV'];
                        $expmonth  = $card_data['cardMonth'];
                        $exyear    = $card_data['cardYear'];

                        $address1 = $card_data['Billing_Addr1'];
                        $address2 = $card_data['Billing_Addr2'];
                        $city     = $card_data['Billing_City'];
                        $zipcode  = $card_data['Billing_Zipcode'];
                        $state    = $card_data['Billing_State'];
                        $country  = $card_data['Billing_Country'];
                        $phone    = $card_data['Billing_Contact'];
                        $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                    }

                    if (!empty($cardID)) {

                        if ($in_data['BalanceRemaining'] > 0) {
                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];
                            $amount    = $pay_amounts;

                            $amount = $amount - $cr_amount;

                            $exyear1 = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $exyear1.$expmonth;
                            $responseType = 'SaleResponse';
                            $amount = number_format($amount,2,'.','');
                            $CUST_NBR = $gt_result['gatewayUsername'];
                            $MERCH_NBR = $gt_result['gatewayPassword'];
                            $DBA_NBR = $gt_result['gatewaySignature'];
                            $TERMINAL_NBR = $gt_result['extra_field_1'];
                            $orderId = time();
                            $transaction = array(
                                'CUST_NBR' => $CUST_NBR,
                                'MERCH_NBR' => $MERCH_NBR,
                                'DBA_NBR' => $DBA_NBR,
                                'TERMINAL_NBR' => $TERMINAL_NBR,
                                'TRAN_TYPE' => 'CCE1',
                                'ACCOUNT_NBR' => $card_no,
                                'EXP_DATE' => $expry,
                                'CARD_ENT_METH' => 'E',
                                'INDUSTRY_TYPE' => 'E',
                                'AMOUNT' => $amount,
                                'TRAN_NBR' => rand(1,10),
                                'INVOICE_NBR' => $invoiceID,
                                'ORDER_NBR' => $orderId,
                                'BATCH_ID' => time(),
                                'VERBOSE_RESPONSE' => 'Y',


                            );
                            if($c_data['FirstName'] != ''){
                                $transaction['FIRST_NAME'] = $c_data['FirstName'];
                            }
                            if($c_data['LastName'] != ''){
                                $transaction['LAST_NAME'] = $c_data['LastName'];
                            }
                            if($cvv != ''){
                                $transaction['CVV2'] = $cvv;
                            }
                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            
                            if($zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }
                           
                            $gatewayTransaction              = new EPX();
                            $result = $gatewayTransaction->processTransaction($transaction);
                          
                            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                                $transactionid = $result['AUTH_GUID'];
                                $txnID  = $in_data['TxnID'];
                                $ispaid = 'true';
                                $am     = (-$amount);

                                $bamount = $in_data['BalanceRemaining'] - $amount;
                                if ($bamount > 0) {
                                    $ispaid = 'false';
                                }

                                $app_amount = $in_data['AppliedAmount'] + (-$amount);
                                $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);

                                $condition = array('TxnID' => $in_data['TxnID']);

                                $this->general_model->update_row_data('chargezoom_test_invoice', $condition, $data);
                                $user = $in_data['qbwc_username'];

                                if ($cardID == "new1" && !($this->input->post('tc'))) {
                                    $card_no   = $this->input->post('card_number');
                                    $expmonth  = $this->input->post('expiry');
                                    $exyear    = $this->input->post('expiry_year');
                                    $cvv       = $this->input->post('cvv');
                                    $card_type = $this->general_model->getType($card_no);

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $card_type,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'customerListID'  => $customerID,
                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,

                                        'createdAt'       => date("Y-m-d H:i:s"),
                                        'Billing_Addr1'   => $address1,
                                        'Billing_Addr2'   => $address2,
                                        'Billing_City'    => $city,
                                        'Billing_State'   => $state,
                                        'Billing_Country' => $country,
                                        'Billing_Contact' => $phone,
                                        'Billing_Zipcode' => $zipcode,
                                    );
                                    
                                    $id1 = $this->card_model->process_card($card_data);
                                }

                                
                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            } else {

                                if ($cardID_upd == 'new1') {
                                    $this->db1->where(array('CardID' => $cardID));
                                    $this->db1->delete('customer_card_data');
                                }

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - ' . $result['AUTH_RESP_TEXT'] . '</strong></div>');
                            }
                            $result['transactionid'] = $transactionid;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $in_data['Customer_ListID'], $pay_amounts, $user_id, $crtxnID = '', $this->resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Not valid </strong>.</div>');
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Customer has no card</strong>.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
                }
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Select Gateway and Card!</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('company/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('company/home/invoices', 'refresh');
        }
    }
}