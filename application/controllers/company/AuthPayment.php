<?php

/**
 * This Controller has Authorize.net Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */

include_once APPPATH .'libraries/Manage_payments.php';
class AuthPayment extends CI_Controller
{
	private $resellerID;
	private $transactionByUser;
	
	public function __construct()
	{
		parent::__construct();
		
	
		include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        $this->load->config('auth_pay');
     	$this->load->model('general_model');
		$this->load->model('company/company_model','company_model');
		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->model('customer_model');
			$this->load->library('form_validation');
		    if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='5' )
			  {
			  	$logged_in_data = $this->session->userdata('logged_in');
			    $this->resellerID = $logged_in_data['resellerID'];
			    $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
			  }else if($this->session->userdata('user_logged_in')!="")
			  {
			  	  $logged_in_data = $this->session->userdata('user_logged_in');
			      
			      $merchID = $logged_in_data['merchantID'];
			      $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
		          $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID'=>$merchID));
		          $this->resellerID = $rs_Data['resellerID'];
			 
			  }else{
				redirect('login','refresh');
			  }
			  
					
	
	}
	
	
	public function index(){
		redirect('company/home/index','refresh');
	    
	}
	

	
	 
public function pay_invoice()
{

	$this->session->unset_userdata("receipt_data");
	$this->session->unset_userdata("invoice_IDs");
      if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	
    
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$c_data   = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
		
			$companyID = $c_data['companyID'];
			$sch_method = $this->czsecurity->xssCleanPostInput('sch_method');
		
		
			
			 $cardID_upd ='';
			 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			 $cardID = $this->czsecurity->xssCleanPostInput('CardID');
			if (!$cardID || empty($cardID)) {
			$cardID = $this->czsecurity->xssCleanPostInput('schCardID');
			}

			$gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
			if (!$gatlistval || empty($gatlistval)) {
			$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
			}
			$gateway = $gatlistval;
			 $cusproID=''; $error='';
			 $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
			 if($this->czsecurity->xssCleanPostInput('setMail'))
                    $chh_mail =1;
                    else
					$chh_mail =0;
					
			$checkPlan = check_free_plan_transactions();
			
	 if($checkPlan && $cardID!="" && $gateway!="")
	 {  
	     
	       $in_data   =    $this->company_model->get_invoice_data_pay($invoiceID);
		  $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
		 

		 		
	    $resellerID = 	$this->resellerID;  
		 $cardID_upd='';
     	$apiloginID       = $gt_result['gatewayUsername'];
	    $transactionKey   = $gt_result['gatewayPassword'];
	    $custom_data_fields = [];
		if(!empty($in_data))
		{ 
		
			$Customer_ListID = $in_data['Customer_ListID'];
            $customerID = $in_data['Customer_ListID'];
            $c_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
            		
            $companyID = $c_data['companyID'];
          	   
           if($cardID=='new1')
           {
                        $cardID_upd  =$cardID;
			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						
           			  $address1 =  $this->czsecurity->xssCleanPostInput('address1');
	                  $address2 =  $this->czsecurity->xssCleanPostInput('address2');
	                    $city   =  $this->czsecurity->xssCleanPostInput('city');
	                    $country     =  $this->czsecurity->xssCleanPostInput('country');
	                    $phone       =  $this->czsecurity->xssCleanPostInput('contact');
	                    $state       = $this->czsecurity->xssCleanPostInput('state');
	                     $zipcode    =  $this->czsecurity->xssCleanPostInput('zipcode');

						 $accountDetails = [
							'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
							'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
							'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
							'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
							'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
							'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
							'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
							'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
							'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
							'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
							'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
							'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
							'customerListID'     => $customerID,
							'companyID'          => $companyID,
							'merchantID'         => $user_id,
							'createdAt'          => date("Y-m-d H:i:s"),
							'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
						];
						if($sch_method == 2){
							$accountNumber = $this->czsecurity->xssCleanPostInput('acc_number');
						    $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
						    $custom_data_fields['payment_type'] = $friendlyname;
						}else{
							$cardType = $this->general_model->getType($card_no);
							$friendlyname = $cardType . ' - ' . substr($card_no, -4);

							$custom_data_fields['payment_type'] = $friendlyname;
						} 
           
           }
        else{
          			  $card_data = $accountDetails   =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
        
        				$address1 =     $card_data['Billing_Addr1'];
        				$address2 =     $card_data['Billing_Addr2'];
	                    $city     =      $card_data['Billing_City'];
        			  $zipcode       =      $card_data['Billing_Zipcode'];
        				$state       =     $card_data['Billing_State'];
	                    $country     =      $card_data['Billing_Country'];
        	            $phone       =     $card_data['Billing_Contact'];
        	            $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
	                                     
        }

        
			 if(!empty($cardID))
			 {
				
				if( $in_data['BalanceRemaining'] > 0){
					        $cr_amount = 0;
					        $amount  =	 $in_data['BalanceRemaining']; 
					   
					          
							$amount  = $this->czsecurity->xssCleanPostInput('inv_amount');
							$amount           = $amount-$cr_amount;
					  		$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
					  		$transaction1->setSandbox($this->config->item('auth_test_mode'));
					  
						if($sch_method == 1) {
					  	
							$exyear1   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
				 	    	$expry    = $expmonth.$exyear1;  
							$echeckString = false;
							
							$result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);
						} else {
							$transaction1->setECheck($accountDetails['routeNumber'], $accountDetails['accountNumber'], $accountDetails['accountType'], $bank_name='Wells Fargo Bank NA', $accountDetails['accountName'], $accountDetails['secCodeEntryMethod']);
							$result = $transaction1->authorizeAndCapture($amount);
							$echeckString = true;
						} 
						$crtxnID='';
					   if( $result->response_code=="1" && $result->transaction_id != 0 && $result->transaction_id != '')
					   {
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $bamount    = $in_data['BalanceRemaining']-$amount;
						 
						 if($bamount > 0)
						  $ispaid 	 = 'false';
						  $app_amount = $in_data['AppliedAmount']+(-$amount);
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount,'TimeModified'=>date('Y-m-d H:i:s') );
						 
					
						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
						 $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
						 
						 $user = $in_data['qbwc_username'];
							   if($sch_method == 1 && $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc')))
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         } else if($sch_method == 2 && !($this->czsecurity->xssCleanPostInput('tc')) && $cardID == "new1" ){
											$id1 = $this->card_model->process_ack_account($accountDetails);
										}
                       
						
					        
						$condition_mail         = array('templateType'=>'15', 'merchantID'=>$user_id); 
						$ref_number =  $in_data['RefNumber']; 
						$tr_date   =date('Y-m-d h:i A');
						$toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
						
					
						$this->session->set_flashdata('success', 'Successfully Processed Invoice');
						
					   } else{
                       if($cardID_upd =='new1')
                       {
                         
                           $this->card_model->delete_card_data(array('CardID'=>$cardID));
                       }
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - "'.$result->response_reason_text.'"</strong>.</div>'); 
					   }  
				  
					  
					   
				     $id = $this->general_model->insert_gateway_transaction_data($result,'auth_capture',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], $echeckString, $this->transactionByUser, $custom_data_fields);  
					   
					if($result->response_code=="1" && $chh_mail =='1'  && $result->transaction_id != 0 && $result->transaction_id != '')
					{
						$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
						$ref_number =  $in_data['RefNumber']; 
						$tr_date   =date('Y-m-d h:i A');
						$toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
						$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $result->transaction_id);
					}
					   
				
				  
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Not valid</strong>.</div>'); 
				}
          
		     }
		     else
		     {
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Card data not available for customer</strong>.</div>'); 
			 }
		
		 
	    	}
	    	else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice</strong>.</div>'); 
			 }
	             
	     
	 }
	 else
	 {
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Select Gateway and Card</strong>.</div>'); 
		  }		 
		
             if($cusproID=="2"){
			 	 redirect('company/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('company/home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
			 $trans_id = ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time();
			$invoice_IDs = array();
			$receipt_data = array(
				'proccess_url' => 'company/home/invoices',
				'proccess_btn_text' => 'Process New Invoice',
				'sub_header' => 'Sale',
				'checkPlan'  =>  $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			if ($cusproID == "1") {
				redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			}
			redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		   

    }      

		 
	public function create_customer_sale()
	{

           
			$this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");      
              
			$invoiceIDs=array();   
		
		if(!empty($this->input->post(null, true)))
		{
			$custom_data_fields = [];
			$applySurcharge = false;
			if($this->czsecurity->xssCleanPostInput('invoice_id')){
                $applySurcharge = true;
            }
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
            	
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }
			     
			   $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   if($this->czsecurity->xssCleanPostInput('setMail'))
                $chh_mail =1;
                else
                $chh_mail =0;
                
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			  
        	$checkPlan = check_free_plan_transactions();
			   
			  $contact='';
			if($checkPlan && $gatlistval !="" && !empty($gt_result) )
			{
			    $apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];
				
    		  
				 if($this->session->userdata('logged_in')){
				$user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
				$merchantEmail 				= $this->session->userdata('logged_in')['merchantEmail'];
				}
				if($this->session->userdata('user_logged_in')){
				$user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				$merchantEmail 				= $this->session->userdata('user_logged_in')['merchantEmail'];
				}
				
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
				$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                
				$companyID  = $comp_data['companyID'];
			    $transaction = new AuthorizeNetAIM($apiloginID,$transactionKey);
			    $transaction->setSandbox($this->config->item('auth_test_mode'));
		
		
                            	$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');  
					            $contact = $phone; 
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" )
		       {	
				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear,2);
						$expry    = $expmonth.$exyear; 
						$cardType = $this->general_model->getType($card_no);
						$friendlyname = $cardType . ' - ' . substr($card_no, -4);

						$custom_data_fields['payment_type'] = $friendlyname;
					
				
				  }
				  else 
				  {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
					
					}
					/*Added card type in transaction table*/
	                $card_type = $this->general_model->getType($card_no);
	                $friendlyname = $card_type . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;
					   
						$transaction->__set('company',$this->czsecurity->xssCleanPostInput('companyName'));
						$transaction->__set('first_name',$this->czsecurity->xssCleanPostInput('fistName'));
						$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
						$transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
						$transaction->__set('country',$this->czsecurity->xssCleanPostInput('country'));
						$transaction->__set('city',$this->czsecurity->xssCleanPostInput('city'));
						$transaction->__set('state',$this->czsecurity->xssCleanPostInput('state'));
					
						$transaction->__set('phone',$this->czsecurity->xssCleanPostInput('phone'));
					
						$transaction->__set('email',$this->czsecurity->xssCleanPostInput('email'));
						$amount = $this->czsecurity->xssCleanPostInput('totalamount');

						// update amount with surcharge 
                        if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                            $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                            $amount += round($surchargeAmount, 2);
                            $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                            $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                            $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                            
                        }
						$totalamount  = $amount;
						if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
						    $transaction->__set('invoice_num', $this->czsecurity->xssCleanPostInput('invoice_number'));
						}

						if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
						    $transaction->__set('po_num', $this->czsecurity->xssCleanPostInput('po_number'));
						}

				        $result = $transaction->authorizeAndCapture($amount,$card_no,$expry);
				
			        $crtnxID = '';

				 if($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '')
				 {
				 	
				 
				 /* This block is created for saving Card info in encrypted form  */
				 
				                 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				     {
            				 		
            				        
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $merchantID,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      => '', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $merchantID,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }	

				 	$invoicePayAmounts = array();
				   if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
				     {
				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
				     }

				      $refNumber =array();
				         if(!empty($invoiceIDs))
				           {
							  	$payIndex = 0;
				              	foreach($invoiceIDs as $inID)
				              	{
        				            $theInvoice = array();
        							 
        							$theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
    								if(!empty($theInvoice) )
    								{

    									
    									$amount_data = $theInvoice['BalanceRemaining'];
										$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
										if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                            $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                            $actualInvoicePayAmount += $surchargeAmount;
                                            $amount_data += $surchargeAmount;

                                            $updatedInvoiceData = [
                                                'inID' => $inID,
                                                'merchantID' => $user_id,
                                                'amount' => $surchargeAmount,
                                            ];
                                            $this->general_model->updateSurchargeInvoice($updatedInvoiceData,5);
                                        }
										$isPaid 	 = 'false';
										$BalanceRemaining = 0.00;
										$refnum[] = $theInvoice['RefNumber'];
										
										if($amount_data == $actualInvoicePayAmount){
											$actualInvoicePayAmount = $amount_data;
											$isPaid 	 = 'true';

										}else{

											$actualInvoicePayAmount = $actualInvoicePayAmount;
											$isPaid 	 = 'false';
											$BalanceRemaining = $amount_data - $actualInvoicePayAmount;
											
										}
										$txnAmount = $actualInvoicePayAmount;
										$AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;

        						    	$tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

        							
        						    	$transactiondata= array();
                				      
                				        $refNumber[] = $theInvoice['RefNumber'];
                				       

                				        $id = $this->general_model->insert_gateway_transaction_data($result,'auth_capture',$gatlistval,$gt_result['gatewayType'],$customerID,$txnAmount,$merchantID,$crtnxID, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);  

        							}
        						
        						
								++$payIndex;
        						
				              	}
				              
				              
						 
				          }else{
				              
				                          $transactiondata= array();
                    				     
                    				       
                    			    $id = $this->general_model->insert_gateway_transaction_data($result,'auth_capture',$gatlistval,$gt_result['gatewayType'],$customerID,$result->amount,$merchantID,$crtnxID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                    				       	       
                    				       
                    			
				              
				          }
				          
				     
				      if($chh_mail =='1')
							 {
							  	$condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  	if (!empty($refNumber)) {
		                            $ref_number = implode(',', $refNumber);
		                        } else {
		                            $ref_number = '';
		                        }
							  
							  $tr_date   =date('Y-m-d h:i A');
							  	$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $result->transaction_id);
							 }
							 $condition_mail         = array('templateType'=>'15', 'merchantID'=>$merchantID); 
							  if(!empty($refNumber))
							  $ref_number = implode('',$refNumber); 
							    $ref_number = '';
							  $tr_date   =date('Y-m-d h:i A');
							  	$toEmail = $merchantEmail; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
							  
							 $this->session->set_flashdata('success', 'Transaction Successful');
				  
				 }else{
					    $transactiondata= array();
				     
				              
                   $id = $this->general_model->insert_gateway_transaction_data($result,'auth_capture',$gatlistval,$gt_result['gatewayType'],$customerID,$result->amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                    				
				       
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - "'.$result->response_reason_text.'"</strong>.</div>'); 
				 }
				 
				   
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select gateway</strong>.</div>'); 		
			}		
			$invoice_IDs = array();
			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
				$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
			}
		
			$receipt_data = array(
				'transaction_id' => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'company/Payments/create_customer_sale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
				'checkPlan'  =>  $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			
			redirect('company/home/transation_sale_receipt',  'refresh');         
        }
         
          redirect('company/Payments/create_customer_sale','refresh');

	}
	
	
	
		 
	public function create_customer_auth()
	{
              
			$this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");      
              
			   
		
		if(!empty($this->input->post(null, true)))
		{
			     
			   $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));

			$custom_data_fields = [];   
			$checkPlan = check_free_plan_transactions();
			if($checkPlan && $gatlistval !="" && !empty($gt_result) )
			{
			    $apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];
				
				 if($this->session->userdata('logged_in'))
				 {
			    	$merchantID = $this->session->userdata('logged_in')['merchID'];
			     }
				 if($this->session->userdata('user_logged_in'))
				 {
				    $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				 }	
				$po_number = $this->czsecurity->xssCleanPostInput('po_number');
				if (!empty($po_number)) {
	                $custom_data_fields['po_number'] = $po_number;
	            }
				
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
				$comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID'), array('ListID' => $customerID));
				$companyID  = $comp_data['companyID'];
			    $transaction = new AuthorizeNetAIM($apiloginID,$transactionKey); 
			    $transaction->setSandbox($this->config->item('auth_test_mode'));
		
		       $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                  
					
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
		       {	
				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear,2);
						$expry    = $expmonth.$exyear;  
						$cardType = $this->general_model->getType($card_no);
						$friendlyname = $cardType . ' - ' . substr($card_no, -4);

						$custom_data_fields['payment_type'] = $friendlyname;
				
				  }
				  else 
				  {
					  
						 
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							$custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
					}
					   
						$transaction->__set('company',$this->czsecurity->xssCleanPostInput('companyName'));
						$transaction->__set('first_name',$this->czsecurity->xssCleanPostInput('fistName'));
						$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
						$transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
						$transaction->__set('country',$this->czsecurity->xssCleanPostInput('country'));
						$transaction->__set('city',$this->czsecurity->xssCleanPostInput('city'));
						$transaction->__set('state',$this->czsecurity->xssCleanPostInput('state'));
						$transaction->__set('phone',$this->czsecurity->xssCleanPostInput('phone'));
					
						$transaction->__set('email',$this->czsecurity->xssCleanPostInput('email'));
						$amount = $this->czsecurity->xssCleanPostInput('totalamount');
						if (!empty($po_number)) {
                            $transaction->__set('po_num', $po_number);
                        }
				      $result = $transaction->authorizeOnly($amount,$card_no,$expry);
				$crtxnID='';
				 if($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != ''){
				 
				 
				 /* This block is created for saving Card info in encrypted form  */
				 
				 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1'){
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);		
						$card_condition = array(
										 'customerListID' =>$customerID, 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =   $customerID;			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$crdata =	$this->card_model->check_friendly_name($cid,$friendlyname)	;			
					     
					   
						if(!empty($crdata))
						{
							
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CardType'     =>$this->general_model->getType($card_no),
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->card_model->update_card_data($card_condition, $card_data);				 
						}
						else
						{
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'     =>$this->general_model->getType($card_no),
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
								
				            $id1 =    $this->card_model->insert_card_data($card_data);	
						
						}
				
				 }
				 $this->session->set_flashdata('success', 'Transaction Successful');
				
			
				 }
				 
				 else
				 {
				     
				 
				  
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$result->response_reason_text.'</strong></div>'); 		
		      	}
				$id = $this->general_model->insert_gateway_transaction_data($result,'auth_only',$gatlistval,$gt_result['gatewayType'],$customerID,$result->amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
	
				       
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>'); 
				}		
				$invoice_IDs = array();
			$receipt_data = array(
				'transaction_id' => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'company/Payments/create_customer_auth',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Authorize',
				'checkPlan'  => $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			
			redirect('company/home/transation_sale_receipt',  'refresh');        
        }
              
				
		  redirect('company/Payments/create_customer_auth','refresh');     


	}
	
	
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if(!empty($this->input->post(null, true))){
		    if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
			
			     $tID     = $this->czsecurity->xssCleanPostInput('txnvoidID1');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $gatlistval = $paydata['gatewayID'];
				   if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
		if($tID!='' && !empty($gt_result))
		{
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
				 
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));

			    
				 $crtxnID=''; $inID='';
				
				 $amount  =  $paydata['transactionAmount']; 
				  $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
				$result     = $transaction->void($tID);
			 
				 if($result->response_code == '1'){
			
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					 if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
					
							$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
				
			        
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - "'.$result->response_reason_text.'"</strong></div>'); 
				
				 }
				      $transactiondata= array();
				     
				        $id = $this->general_model->insert_gateway_transaction_data($result,'void',$gatlistval,$gt_result['gatewayType'],$customerID,$result->amount,$paydata['merchantID'],$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
				       
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>'); 
				
				 }
				
					redirect('Payments/payment_capture','refresh');
		}     
			
	}
	
	
		/*****************Capture Transaction***************/
	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true)))
		{
		    
		      	 if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				$merchantEmail 				= $this->session->userdata('logged_in')['merchantEmail'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				$merchantEmail 				= $this->session->userdata('user_logged_in')['merchantEmail'];
				}
				
			     $tID     = $this->czsecurity->xssCleanPostInput('txnID1');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			  
    			 $gatlistval = $paydata['gatewayID'];
				   if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
		if($tID!='' && !empty($gt_result))
		{
			  
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
    		
    		  
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey);
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));

				$amount  =  $paydata['transactionAmount']; 
				$customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
				$inID='';$crtxnID='';
				$result     = $transaction->priorAuthCapture($tID, $amount);
			   
				 if($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != ''){
					
					
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
                            if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d h:i A');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
							}
							$condition_mail         = array('templateType'=>'15', 'merchantID'=>$merchantID); 
							
							$customerID = $paydata['customerListID'];
							
							$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
							$tr_date   =date('Y-m-d h:i A');
							$ref_number =  $tID;
							$toEmail = $merchantEmail; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];
							$this->session->set_flashdata('success', 'Successfully Captured Authorization');
			       
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - "'.$result->response_reason_text.'"</strong></div>'); 
				     
					 }
					 
						$transactiondata= array();
				   
				       
				             
					  $id = $this->general_model->insert_gateway_transaction_data($result,'prior_auth_capture',$gatlistval,$gt_result['gatewayType'],$customerID,$result->amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
				       
				       
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>'); 
				
				 }
				 $invoice_IDs = array();
		
			$receipt_data = array(
				'proccess_url' => 'company/Payments/payment_capture',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Capture',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result->transaction_id == ''){
				$result->transaction_id ='null';
			}
			redirect('company/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->transaction_id,  'refresh');	 
					
        }
              
				
		 return false;   

	}
	
	
	
	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
			
    		
    		
    		     $tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			    
    			 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			 	if($tID!='' && !empty($gt_result))
		{  
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
			  
			
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));


				 $card       = $paydata['transactionCard'];
				 $customerID = $paydata['customerListID'];
				  $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
			      $amount  = $total;
			      $card    = $this->card_model->last_four_digit_card($customerID, $paydata['merchantID']);
            				if(!empty($paydata['invoiceTxnID']))
            				{
            			    $cusdata = $this->general_model->get_select_data('tbl_company',array('qbwc_username','id'), array('merchantID'=>$paydata['merchantID']) );
            				$user_id  = $paydata['merchantID'];
            				 $user    =  $cusdata['qbwc_username'];
            		         $comp_id  =  $cusdata['id']; 
            		        
            		        $ittem = $this->general_model->get_row_data('qb_test_item',array('companyListID'=>$comp_id, 'Type'=>'Payment'));
            				$ins_data['customerID']     = $paydata['customerListID'];
            			 if(empty($ittem))
            		        {
            		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>'); 
                                redirect('company/Payments/payment_transaction','refresh'); 
            		            
            		        }
            			    
            			    
            			      $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
                    			   if(!empty($in_data))
                    			   {
                        			$inv_pre   = $in_data['prefix'];
                        			$inv_po    = $in_data['postfix']+1;
                        			$new_inv_no = $inv_pre.$inv_po;
                                    
                                   
                                   }
            			$ins_data['merchantDataID'] = $paydata['merchantID'];	
            			  $ins_data['creditDescription']     ="Credit as Refund" ;
                           $ins_data['creditMemo']    = "This credit is given to refund for a invoice ";
            				$ins_data['creditDate']   = date('Y-m-d H:i:s');
                    	  $ins_data['creditAmount']   = $total;
                          $ins_data['creditNumber']   = $new_inv_no;
                           $ins_data['updatedAt']     = date('Y-m-d H:i:s');
                            $ins_data['Type']         = "Payment";
                    	   $ins_id = $this->general_model->insert_row('tbl_custom_credit',$ins_data);	
            					   
            					   $item['itemListID']      =    $ittem['ListID']; 
            				       $item['itemDescription'] =    $ittem['Name']; 
            				       $item['itemPrice'] =$total; 
            				       $item['itemQuantity'] =0; 
            				      	$item['crlineID'] = $ins_id;
            						$acc_name  = $ittem['DepositToAccountName']; 
            				      	$acc_ID    = $ittem['DepositToAccountRef']; 
            				      	$method_ID = $ittem['PaymentMethodRef']; 
            				      	$method_name  = $ittem['PaymentMethodName']; 
            						 $ins_data['updatedAt'] = date('Y-m-d H:i:s');
            						$ins = $this->general_model->insert_row('tbl_credit_item',$item);	
            				  	    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           'paymentMethod'=>$method_ID,'paymentMethodName'=>$method_name,
            				  	           'AccountRef'=>$acc_ID,'AccountName'=>$acc_name
            				  	           );	
            					
            				
            					
            				 if($ins_id && $ins)
            				 {
            					 $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id), array('postfix'=>$inv_po));
            					 
                                
                              }else{
                                  	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>'); 
                                  		redirect('company/Payments/payment_transaction','refresh');
                              }
            					
                         }
			  
			  
			 
				
				$result     = $transaction->credit($tID, $amount, $card);
				
				   
				
				 if($result->response_code == '1'){  

                    $this->customer_model->update_refund_payment($tID, 'AUTH');
                    
                    	if(!empty($paydata['invoiceTxnID']))
            			{	
            			    
            			 $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);
            			 $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1','', $user);
            			}
						$this->session->set_flashdata('success', 'Successfully Refunded Payment');
			         
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - "'.$result->response_reason_text.'"</strong></div>'); 
				 }
				     	$transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']   = $paydata['merchantID'];
					   $transactiondata['gateway']   = "Auth";
					  $transactiondata['resellerID']   = $this->resellerID;
					    if(!empty($this->transactionByUser)){
							$transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
							$transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  

		        }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>'); 
				
				 }		
				 $invoice_IDs = array();
				
			 
				 $receipt_data = array(
					 'proccess_url' => 'company/Payments/payment_refund',
					 'proccess_btn_text' => 'Process New Refund',
					 'sub_header' => 'Refund',
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 
				 if($paydata['invoiceTxnID'] == ''){
					 $paydata['invoiceTxnID'] ='null';
				 }
				 if($paydata['customerListID'] == ''){
					 $paydata['customerListID'] ='null';
				 }
				 if($result->transaction_id == ''){
					$result->transaction_id ='null';
				 }
				 redirect('company/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->transaction_id,  'refresh');
				
        }
              
				
		     

	}
	

	
	
	public function Auth_Process(){
		
		$tran = new AuthorizeNetAIM('6L7z3mEakZjV','367sbrCsKp878N4j'); 
		$data = $tran->authorizeOnly('20','4111111111111111','1217');
		
		echo "<pre>";
		print_r($data);
		die;
		
	}		
	
	
	public function Void_Process(){
		
		$tran = new AuthorizeNetAIM('6L7z3mEakZjV','367sbrCsKp878N4j'); 
		$data = $tran->void($tran_id);
		echo "<pre>";
		print_r($data);
		die;
		
	}	
	
	
	
	
		 
	public function create_customer_esale()
	{
		if($this->session->userdata('logged_in')){
			$merchantID = $this->session->userdata('logged_in')['merchID'];
			$merchantEmail 				= $this->session->userdata('logged_in')['merchantEmail'];
		}
		if($this->session->userdata('user_logged_in')){
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			$merchantEmail 				= $this->session->userdata('user_logged_in')['merchantEmail'];
		}	
		
		if(!empty($this->input->post(null, true))){
			
			$custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			$checkPlan = check_free_plan_transactions();
			   
			if($gatlistval !="" && !empty($gt_result) )
			{
			    $apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];
				
		    	$customerID	= $this->czsecurity->xssCleanPostInput('customerID');
				
				$comp_data  = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' =>$customerID , 'qbmerchantID'=>$merchantID));
				$companyID  = $comp_data['companyID'];
				
			    $transaction = new AuthorizeNetAIM($apiloginID,$transactionKey);
			    $transaction->setSandbox($this->config->item('auth_test_mode'));
				
				$payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
				$sec_code =     'WEB';
		
				if($payableAccount == '' || $payableAccount == 'new1') {
					$accountDetails = [
						'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
						'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
						'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'companyID'     => $companyID,
						'merchantID'   => $merchantID,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $sec_code
					];

				} else {
					$accountDetails = $this->card_model->get_single_card_data($payableAccount);
					$custom_data_fields['payment_type'] = $accountDetails['customerCardfriendlyName'];
				}
				      
				$transaction->setECheck($accountDetails['routeNumber'], $accountDetails['accountNumber'], $accountDetails['accountType'], $bank_name='Wells Fargo Bank NA', $accountDetails['accountName'], $accountDetails['secCodeEntryMethod']);
				
				$transaction->__set('company',$this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->__set('first_name',$this->czsecurity->xssCleanPostInput('fistName'));
				$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->__set('address', $this->czsecurity->xssCleanPostInput('baddress1'));
				$transaction->__set('country',$this->czsecurity->xssCleanPostInput('bcountry'));
				$transaction->__set('city',$this->czsecurity->xssCleanPostInput('bcity'));
				$transaction->__set('state',$this->czsecurity->xssCleanPostInput('bstate'));
				$transaction->__set('zip',$this->czsecurity->xssCleanPostInput('bzipcode'));
				
				$transaction->__set('ship_to_address', $this->czsecurity->xssCleanPostInput('address'));
				$transaction->__set('ship_to_country',$this->czsecurity->xssCleanPostInput('country'));
				$transaction->__set('ship_to_city',$this->czsecurity->xssCleanPostInput('city'));
				$transaction->__set('ship_to_state',$this->czsecurity->xssCleanPostInput('state'));
				$transaction->__set('ship_to_zip',$this->czsecurity->xssCleanPostInput('zipcode'));
				
				
				$transaction->__set('phone',$this->czsecurity->xssCleanPostInput('phone'));
			
				$transaction->__set('email',$this->czsecurity->xssCleanPostInput('email'));
				$amount = $this->czsecurity->xssCleanPostInput('totalamount');
				
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
				    $transaction->__set('invoice_num', $this->czsecurity->xssCleanPostInput('invoice_number'));
				}

				if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
				    $transaction->__set('po_num', $this->czsecurity->xssCleanPostInput('po_number'));
				}
				
				$result = $transaction->authorizeAndCapture($amount);
				if($result->response_code == '1'  && $result->transaction_id != 0 && $result->transaction_id != ''){

					$invoicePayAmounts = [];
					$invoiceIDs = [];
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
					}
					$refNum = array();
					if (!empty($invoiceIDs)) {
						$payIndex = 0;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();

							$theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
							if (!empty($theInvoice)) {
								$amount_data = $theInvoice['BalanceRemaining'];
								$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
								$isPaid 	 = 'false';
								$BalanceRemaining = 0.00;
								$refnum[] = $theInvoice['RefNumber'];
								
								if($amount_data == $actualInvoicePayAmount){
									$actualInvoicePayAmount = $amount_data;
									$isPaid 	 = 'true';
								}else{
									$actualInvoicePayAmount = $actualInvoicePayAmount;
									$isPaid 	 = 'false';
									$BalanceRemaining = $amount_data - $actualInvoicePayAmount;	
								}
								$txnAmount = $actualInvoicePayAmount;
								$AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
								
								$tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

								$transactiondata = array();
								$id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID,'', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
							}
							$payIndex++;
						}
					} else {

						$transactiondata = array();
						$inID = '';
						$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
					}
 
					if($payableAccount == '' || $payableAccount == 'new1') {
						$id1 = $this->card_model->process_ack_account($accountDetails);
					}
					if ($this->czsecurity->xssCleanPostInput('tr_checked'))
						$chh_mail = 1;
					else
						$chh_mail = 0;
				 	/* This block is created for saving Card info in encrypted form  */
				 	$condition_mail         = array('templateType'=>'15', 'merchantID'=>$merchantID); 
					$ref_number = '';
					$tr_date   =date('Y-m-d H:i:s');
					$toEmail = $merchantEmail; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
					if($chh_mail =='1')
					{
					   
					  $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result->transaction_id);
					}
					
							
				 	$this->session->set_flashdata('success', 'Transaction Successful');
				 
				}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->response_reason_text.'</div>'); 
				}

				    
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 		
			}
			
			$receipt_data = array(
				'transaction_id' => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'company/Payments/create_customer_esale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
				'checkPlan'  =>  $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			redirect('company/home/transation_sale_receipt',  'refresh');

        } else {
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please fill all details.</div>'); 		
		}
		redirect('company/Payments/create_customer_esale','refresh');
	}
	
	
	
	public function payment_erefund()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
    		 
			 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
    		
    		     $tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			    
    			 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			 	if($tID!='' && !empty($gt_result))
		{  
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
			  
			
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));


				
				
				
				 $customerID = $paydata['customerListID'];
				 $amount     =  $paydata['transactionAmount']; 
			  
			  	$transaction->__set('method','echeck'); 	
				
				
				$result     = $transaction->credit($tID, $amount);
				
				   
				
				 if($result->response_code == '1'){  
				     
                    $this->customer_model->update_refund_payment($tID, 'AUTH');
					$this->session->set_flashdata('success', 'Successfully Refunded Payment');
			         
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - "'.$result->response_reason_text.'"</strong></div>'); 
				 }
				     	$transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "AUTH Echeck"; 
					  $transactiondata['resellerID']   = $this->resellerID;
					  	if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
					    if(!empty($this->transactionByUser)){
							$transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
							$transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  

		        }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>'); 
				
				 }		
              
				redirect('company/Payments/echeck_transaction','refresh');
				
        }
              
				
		     

	}
	
	

	 	
	public function payment_evoid()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if(!empty($this->input->post(null, true))){
			
			   if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}
			     $tID     = $this->czsecurity->xssCleanPostInput('txnvoidID1');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
		if($tID!='' && !empty($gt_result))
		{
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
				 
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));

			    
				 	$transaction->__set('method','echeck'); 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
				$result     = $transaction->void($tID);
			 
				 if($result->response_code == '1'){
					
			 
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
			        
					
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - "'.$result->response_reason_text.'"</strong></div>'); 
				
				 }
				      $transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "AUTH Echeck";
					  $transactiondata['resellerID']   = $this->resellerID;
					    if(!empty($this->transactionByUser)){
							$transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
							$transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				       
				       
				       
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>'); 
				
				 }
				
					redirect('company/Payments/evoid_transaction','refresh');
		}     
			
	}
	
	
	     
	
   public function delete_pay_transaction()
   {
  	if(!empty($this->czsecurity->xssCleanPostInput('paytxnID')))	
    {  
        if($this->session->userdata('logged_in'))
	    {
			$data 	= $this->session->userdata('logged_in');
				
			$user_id 				= $data['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
			$data	= $this->session->userdata('user_logged_in');
				
			$user_id 				= $data['merchantID'];
		}
		$today 				    = date('Y-m-d');

		$setMailVoid     =  ($this->czsecurity->xssCleanPostInput('setMailVoid')) ? 1 : 0; 
		$payment_capture_page     =  ($this->czsecurity->xssCleanPostInput('payment_capture_page')) ? $this->czsecurity->xssCleanPostInput('payment_capture_page') : 0; 

		$txnID           =  $this->czsecurity->xssCleanPostInput('paytxnID'); 
		$txnID           =  $this->czsecurity->xssCleanPostInput('paytxnID');
		if(isset($_POST['txnvoidID'])){
			$condition		 = array('transactionID' => $_POST['txnvoidID']);

		}else{
			$condition		 = array('id'=>$txnID);
		} 
	    $invData         = $this->general_model->get_row_data('customer_transaction',$condition);
	    $transaction_id = $invData['transactionID'];
	    $transactionGateway = $invData['transactionGateway'];
	    $txnID = $invData['id'];
		$gatlistval = $invData['gatewayID'];
		$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
		$paymentType = (strrpos($invData['gateway'], 'ECheck'))? 2 : 1;
		
		 $voidObj = new Manage_payments($user_id);

		 $voidedTransaction = $voidObj->voidTransaction([
		 	'trID' => $invData['transactionID'],
		 	'gatewayID' => $gatlistval,
		 	'paymentType' => $paymentType
		 ]);
		 
		 if($voidedTransaction){	
			if(!empty($invData))
			{    
				$input_array =array();
				$input_array['qb_status']  =0;
				$input_array['qb_action']  ="Delete Payment";
				$input_array['createdAt']  =date('Y-m-d H:i:s');
				$input_array['updatedAt']  =date('Y-m-d H:i:s');;
				$input_array['merchantID'] =$user_id;
				$input_array['invoiceID']  =$invData['qbListTxnID'];

				$input_array1 =array();
				$input_array1['qb_status']  =0;
				$input_array1['txnType']  ="ReceivePayment";
				$input_array1['createdAt']  =date('Y-m-d H:i:s');
				$input_array1['TimeModified']  =date('Y-m-d H:i:s');;
				$input_array1['merchantID'] =$user_id;
				$input_array1['delTxnID']  =$invData['qbListTxnID'];
			    $updateData  =array();
			     
			    if($invData)
			     {
			       	$this->general_model->update_row_data('customer_transaction',$condition,array('transaction_user_status'=>'3'));
			      	$in_data = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$user_id));
			        
			         
					$insID =  $this->general_model->insert_row('tbl_del_transactions',$input_array1);
				
					
					if($invData['invoiceTxnID']!="")
					{
						$in_data= $this->general_model->get_row_data('chargezoom_test_invoice',array('TxnID'=>$invData['invoiceTxnID']));
						
						if($in_data['AppliedAmount'] > 0){
							$aap_amount = $in_data['AppliedAmount'] - $invData['transactionAmount'];
						} else {
							$aap_amount = $in_data['AppliedAmount'] + $invData['transactionAmount'];
						}
						$balance    = $in_data['BalanceRemaining'] + $invData['transactionAmount'];
						
						$status     = 'false';
						
						$this->general_model->update_row_data('chargezoom_test_invoice',array('TxnID'=>$invData['invoiceTxnID']),
						array('AppliedAmount'=>$aap_amount,'BalanceRemaining'=>$balance,'IsPaid'=>$status));
					}
				     	
				    

					if ($setMailVoid == '1') {
                        $customerID = $invData['customerListID'];

                        $comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['Contact'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['FullName'];
                        $this->general_model->send_mail_voidcapture_data($user_id, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                    }
					  $this->session->set_flashdata('success', 'Transaction Voided');
				}else{
					$this->general_model->update_row_data('customer_transaction',$condition,array('transaction_user_status'=>'3'));
					$this->session->set_flashdata('success', 'Transaction Voided'); 	
				}
				
			    
			}
			else
			{
				$this->session->set_flashdata('success', 'Transaction Voided');
			}
		}
		$receipt_data = array(
			'proccess_url' => 'company/Payments/payment_capture',
			'proccess_btn_text' => 'Process New Transaction',
			'sub_header' => 'Void',
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		
		redirect('company/home/transation_credit_receipt/transaction/'.$invData['customerListID'].'/'.$transaction_id,  'refresh');

		if($payment_capture_page)
			redirect('company/Payments/payment_capture','refresh');
		else
			redirect('company/Payments/payment_transaction','refresh');

       } else
               {
                   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Invalid request</strong></div>'); 
                    	redirect('company/Payments/evoid_transaction','refresh');
               }
       
       
   } 
       
       
       
           
          	 
    public function pay_multi_invoice()
    {
    
       
          if($this->session->userdata('logged_in')){
    		$da	= $this->session->userdata('logged_in');
    		
    		$user_id 				= $da['merchID'];
    		}
    		else if($this->session->userdata('user_logged_in')){
    		$da 	= $this->session->userdata('user_logged_in');
    		
    	    $user_id 				= $da['merchantID'];
    		}	
            $cusproID='';
            $custom_data_fields = [];
            $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
    
    	    $customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$c_data   = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
		
			$companyID = $c_data['companyID'];
    	 	 $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
    		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
    		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');
		
		$checkPlan = check_free_plan_transactions();
    	if($checkPlan && !empty($invoices))
    	{
    	    foreach($invoices as $invoiceID)
    	    {
    	      
    	   
        		
    	 if($cardID!="" || $gateway!="")
    	 {  
    	       $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
             $in_data =    $this->company_model->get_invoice_data_pay($invoiceID);
    		  $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    		
    		 $resellerID = 	$this->resellerID;  
    		 
         	$apiloginID       = $gt_result['gatewayUsername'];
    	    $transactionKey   = $gt_result['gatewayPassword'];
    	     
    		if(!empty($in_data)){ 
    		
    			$Customer_ListID = $in_data['Customer_ListID'];
            
                 if($cardID=='new1')
                           {
                            	$cardID_upd  =$cardID;
        			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
        						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
								$cardType       = $this->general_model->getType($card_no);
								$friendlyname   =  $cardType.' - '.substr($card_no,-4);
        						
                           		$custom_data_fields['payment_type'] = $friendlyname;
                            }else{
                                 $card_data    =   $this->card_model->get_single_card_data($cardID); 
                                $card_no  = $card_data['CardNo'];
                               	$cvv      =  $card_data['CardCVV'];
        						$expmonth =  $card_data['cardMonth'];
        						$exyear   = $card_data['cardYear'];	
                             	$custom_data_fields['payment_type'] = $accountDetails['customerCardfriendlyName'];
                             }
            
    	
    			 
    		 if(!empty($cardID))
             {
    				
    				if( $in_data['BalanceRemaining'] > 0){
    					    $cr_amount = 0;
    					     $amount  =	 $in_data['BalanceRemaining']; 
    					    
    							$amount  =$pay_amounts;
    							$amount           = $amount-$cr_amount;
    					  		$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey);
    					  		$transaction1->setSandbox($this->config->item('auth_test_mode'));
    					  
    					  	
    					
    							$exyear   = substr($exyear,2);
    							if(strlen($expmonth)==1){
    								$expmonth = '0'.$expmonth;
    							}
    				 	    $expry    = $expmonth.$exyear;  
    							
    
    						     $result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);
    
    					
    					   if( $result->response_code=="1"  && $result->transaction_id != 0 && $result->transaction_id != ''){
    						 $txnID      = $in_data['TxnID'];  
    						 $ispaid 	 = 'true';
    						 $bamount =  $in_data['BalanceRemaining']-$result->amount;
    						 if($bamount >0)
    						 	 $ispaid 	 = 'false';
    					      $app_amount = $in_data['AppliedAmount']+$result->amount;
    						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(-$app_amount) , 'BalanceRemaining'=>$bamount, 'TimeModified'=>date('Y-m-d H:i:s') );
    						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
    						 $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
    						 
    						 $user = $in_data['qbwc_username'];
    					
    					        	 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                        				 {
                        				 
                        				        $this->load->library('encrypt');
                        				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
                        				 		
                        				       
												$cardType       = $this->general_model->getType($card_no);
												$friendlyname   =  $cardType.' - '.substr($card_no,-4);
                        						$card_condition = array(
                        										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
                        										 'customerCardfriendlyName'=>$friendlyname,
                        										);
                        							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
                        							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
                        						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
                        							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
                        					
                        						 $crdata =  $this->card_model->chk_card_firendly_name($customerID,$friendlyname);
                        					
                        						if($crdata > 0)
                        						{
                        							
                        						   $card_data = array(
                        						                 'cardYear'	 =>$exyear, 
                        										   'CardType'    =>$cardType,
                        										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                        										  'CardCVV'      =>'', 
                        										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                        	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                        	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                        	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                        	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                        	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                        	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                        										 'customerListID' =>$in_data['Customer_ListID'], 
                        										 'customerCardfriendlyName'=>$friendlyname,
                        										 'companyID'     =>$companyID,
                        										  'merchantID'   => $user_id,
                        										 'updatedAt' 	=> date("Y-m-d H:i:s") 
                        										  );
                        										  
                        									
                        					
                        						   $this->card_model->update_card_data($card_condition, $card_data);				 
                        						}
                        						else
                        						{
                        					     	$card_data = array(
                        					     	    'cardMonth'   =>$expmonth,
                        										   'cardYear'	 =>$exyear, 
                        										   'CardType'    =>$cardType,
                        										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                        										  'CardCVV'      =>'', 
                        										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                        	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                        	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                        	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                        	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                        	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                        	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                        										 'customerListID' =>$in_data['Customer_ListID'],
                        										 'customerCardfriendlyName'=>$friendlyname,
                        										 'companyID'     =>$companyID,
                        										  'merchantID'   => $user_id,
                        										 'createdAt' 	=> date("Y-m-d H:i:s")
                        										  );
                        				            $id1 = $this->card_model->insert_card_data($card_data);	
                        						
                        						}
                        				
                        				 }			 
							 
					
    					
    					
    					
    						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
    					   } else{
                           if($cardID_upd =='new1')
                           {
                              $this->db1->where(array('CardID'=>$cardID));
                               $this->db1->delete('customer_card_data');
                           }
    					   
    					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - "'.$result->response_reason_text.'"</strong>.</div>'); 
    					   }  
    			
    					   $transactiondata= array();
    				   
					  $id = $this->general_model->insert_gateway_transaction_data($result,'auth_capture',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$pay_amounts,$user_id,$crtxnID='', $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser, $custom_data_fields);
    					
    				  
    				}else{
    					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Not valid</strong>.</div>'); 
    				}
              
    		     }else{
    	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Customer has no card</strong>.</div>'); 
    			 }
    		
    		 
    	    	}else{
    	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice</strong>.</div>'); 
    			 }
    	             }else{
    			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Select Gateway and Card</strong>.</div>'); 
    		  }	
    		  
    	    }	  
    		  
    		  
    }else{
    
    			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Please select invoices</strong>.</div>'); 
    		
	}
	
	if(!$checkPlan){
		$responseId  = '';
		$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
	}
    		
    		    if($cusproID!=""){
			 	 redirect('company/home/view_customer/'.$cusproID,'refresh');
    			 }
    		   	 else{
    		   	 redirect('company/home/invoices','refresh');
    		   	 }
    
        }     



	
	
	
	
	
}