<?php

/**
 * This Controller has Stripe Payment Gateway Process
 * 
 * Create_customer_sale for Sale process : here we use the payment token
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */

class CyberSource extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
   	public function __construct()
	{
		parent::__construct();
		
		
        $this->load->config('cyber_pay');
	
      	$this->load->model('general_model');
		$this->load->model('company/company_model','company_model');
		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->model('company/customer_model','customer_model');
			$this->load->library('form_validation');
	 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='5' )
			  {
			    $logged_in_data = $this->session->userdata('logged_in');
                $this->resellerID = $logged_in_data['resellerID'];
                $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];			   
			  }else if($this->session->userdata('user_logged_in')!="")
			  {
			      
                $logged_in_data = $this->session->userdata('user_logged_in');
                $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
                $merchID = $logged_in_data['merchantID'];
                $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
                $this->resellerID = $rs_Data['resellerID'];
			  }else{
				redirect('login','refresh');
			  }
		
					
	
	}
	
	
	
	
	public function index()
	{
	    
 	redirect('company/home/index','refresh');

	
	
	    
	}
	
	
   	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	    if($this->session->userdata('logged_in'))
	    {
			    $user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
		
			    $user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('sch_gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
               
                $this->form_validation->set_rules('contact', 'Phone Number', 'trim|required|xss-clean');
            }
            
                 $cusproID=''; $error='';
		    	 $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	
        	$checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];  
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		       
		          $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
			     
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			     $in_data              =    $this->company_model->get_invoice_data_pay($invoiceID, $user_id);
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
				  $chh_mail =0;
				  
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;
			     
			    if(!empty( $in_data)) 
			    {
			          
    			        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			        $customerID =  $in_data['Customer_ListID'];
    				  	$c_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID', 'FirstName', 'LastName',  'companyName','Phone', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
    			     	$companyID = $c_data['companyID'];
    			        $gmerchantID    = $gt_result['gatewayUsername'];
    			        $secretApiKey   = $gt_result['gatewayPassword'];
						$secretKey      = $gt_result['gatewaySignature'];
    			     
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			       
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					$phone    = ($card_data['Billing_Contact'])?$card_data['Billing_Contact']:$c_data['Phone'];
    					$custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
				        $address1 =  $this->czsecurity->xssCleanPostInput('address1');
                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
                    	$city    =$this->czsecurity->xssCleanPostInput('city');
                        $country =$this->czsecurity->xssCleanPostInput('country');
                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
                        $state   =  $this->czsecurity->xssCleanPostInput('state');
                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			        $cardType = $this->general_model->getType($card_no);
						$friendlyname = $cardType . ' - ' . substr($card_no, -4);

						$custom_data_fields['payment_type'] = $friendlyname; 
    			     }                              
         
             	
    				if( $in_data['BalanceRemaining'] > 0)
    				{
        				    
                    	$crtxnID='';  
                        $flag="true";
                        $error=$user='';
                        $user = $in_data['qbwc_username'];
        				$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "test_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $c_data['FirstName'],
                    		"lastName" => $c_data['LastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => $phone,
                    		"company" => $c_data['companyName'],
                    		"email" => $c_data['Contact']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];

                        $invoiceDetail = [];

                        if (!empty($this->czsecurity->xssCleanPostInput('invoice_number')) || !empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                            $new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_number');
                            $invoiceDetailArr = [
                                'invoiceNumber' => $new_invoice_number,
                                'purchaseOrderNumber' => $this->czsecurity->xssCleanPostInput('po_number')
                            ];
                            $invoiceDetail = new CyberSource\Model\Ptsv2paymentsOrderInformationInvoiceDetails($invoiceDetailArr);
                        }

                        if($invoiceDetail){
                            $orderInfoArr['invoiceDetails'] = $invoiceDetail; 
                        }
                        
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                    	
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    	
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                    	
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  
        					  
        					  
                                     $txnID      = $in_data['TxnID'];  
        							 $ispaid 	 = 'true';
        							 $bamount    = $in_data['BalanceRemaining']-$amount;
        							  if($bamount > 0)
        							  $ispaid 	 = 'false';
        							 $app_amount = $in_data['AppliedAmount']-$amount;
        							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
        							 $condition  = array('TxnID'=>$in_data['TxnID'] );
        							 
									 $condition_mail         = array('templateType'=>'15', 'merchantID'=>$user_id); 
        							  $ref_number =  $in_data['RefNumber']; 
        							  $tr_date   =date('Y-m-d h:i A');
        							  $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
        							 
        							 $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
        					
                    			       if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
    					
									  $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                    				
        					    
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
								  $error = $api_response[0]['status'];
								  $this->session->set_flashdata('success', 'Payment '.$error);
        					     
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser, $custom_data_fields);  
							if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201' && $chh_mail =='1')
							{
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								$ref_number =  $in_data['RefNumber']; 
								$tr_date   =date('Y-m-d H:i:s');
								$toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
							
								$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $trID);
							}
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    	    
        			}  
        			
                      
        				   
        				           
        		    }
        		    else        
                    {
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
                        
                    }
                 
        		   
		    } 
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			  
		    
                if($cusproID=="2"){
			 	 redirect('company/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('company/home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
			 $trans_id = isset($api_response) ? $api_response[0]['id'] : '';
			$invoice_IDs = array();
			$receipt_data = array(
				'proccess_url' => 'company/home/invoices',
				'proccess_btn_text' => 'Process New Invoice',
				'sub_header' => 'Sale',
				'checkPlan'  =>  $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			if ($cusproID == "1") {
				redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			}
			redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		   
	      
	}
	
	public function create_customer_sale()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	        if($this->session->userdata('logged_in'))
		    {
				$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	    		
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
            
           
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
                	
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
          
            }
            
           
        	$checkPlan = check_free_plan_transactions();
               
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
	            $custom_data_fields = [];
	            $applySurcharge = false;
	            if($this->czsecurity->xssCleanPostInput('invoice_id')){
	                $applySurcharge = true;
	            }
                // get custom field data
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
                       if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			    
        			  
        			if(!empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				
                        $comp_data     =$this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        			 
        		          
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                         
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                       
                                
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				}
        				else 
        				{     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];

        				}
        				/*Added card type in transaction table*/
		                $card_type = $this->general_model->getType($card_no);
		                $friendlyname = $card_type . ' - ' . substr($card_no, -4);
		                $custom_data_fields['payment_type'] = $friendlyname;
							
					$address2='';
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								// update amount with surcharge 
				                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
				                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
				                    $amount += round($surchargeAmount, 2);
				                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
				                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
				                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
				                    
				                }
				                $totalamount  = $amount;
								$address1 =  $this->czsecurity->xssCleanPostInput('address1');
                    	$address2 =  $this->czsecurity->xssCleanPostInput('address1');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                      $contact=  $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');
                                
						
                            $crtxnID='';  
                            $flag="true";
                            $error=$user='';
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];

        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" =>  $comp_data['companyName']
        				];
                        
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				 
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
                       
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				
        				
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
                    
        				$type =   'Sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        					
        					
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
                                

        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				  
                				 /* This block is created for saving Card info in encrypted form  */
      			 			$invoiceIDs=array();      
							$invoicePayAmounts = array();
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
								$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
        				     }
								$refnum=array();
                               
							   if(!empty($invoiceIDs))
							   {
								   
							  		$payIndex = 0;
								    foreach($invoiceIDs as $inID)
								    {
										$theInvoice = array();
										 
										$theInvoice = $this->general_model->get_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
										
								        
											
											if(!empty($theInvoice) )
											{

												
                                                $amount_data = $theInvoice['BalanceRemaining'];
                                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

	                                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
	                                                $actualInvoicePayAmount += $surchargeAmount;
	                                                $amount_data += $surchargeAmount;

	                                                $updatedInvoiceData = [
	                                                    'inID' => $inID,
	                                                    'merchantID' => $user_id,
	                                                    'amount' => $surchargeAmount,
	                                                ];
	                                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,5);
	                                            }
                                                $isPaid      = 'false';
                                                $BalanceRemaining = 0.00;
                                                $refnum[] = $theInvoice['RefNumber'];
                                                
                                                if($amount_data == $actualInvoicePayAmount){
                                                    $actualInvoicePayAmount = $amount_data;
                                                    $isPaid      = 'true';

                                                }else{

                                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                                    $isPaid      = 'false';
                                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                                    
                                                }
                                                $txnAmount = $actualInvoicePayAmount;
                                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                                
                                                $tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));
										
												 $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$txnAmount,$user_id,$crtxnID, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);  
											   $refnum[]= $theInvoice['RefNumber'];
											   
										
												
											}

											++$payIndex;
								  }
								  
								 
							  }
							   else
							   {
									  
											$transactiondata= array();
											$inID='';
										   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
							   }
							   
							   	if($chh_mail =='1')
								{
								  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								   
								  if (!empty($refnum)) {
		                             $ref_number = implode(',', $refnum);
		                          } else {
		                             $ref_number = '';
		                          }
								  $tr_date   =date('Y-m-d H:i:s');
								  $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
								  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $trID);
								} 
							  
                            if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
    					
										 $this->session->set_flashdata('success', 'Transaction Successful');
							    
            			
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                                     
                              }
                         
                         
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>'); 
					}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => isset($api_response) ? $api_response[0]['id'] : '',
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'company/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'  =>  $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('company/home/transation_sale_receipt',  'refresh');
	    
	   
	    
    	} 
	
	
	public function create_customer_auth()
	{ 
	      
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	        if($this->session->userdata('logged_in'))
		    {
				$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	    			
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
          
            }
            
           	$custom_data_fields = [];
        	$checkPlan = check_free_plan_transactions();
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
               
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
	   
                       if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			  
        			if(!empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
                        $comp_data     =$this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        		           $cardID = $this->czsecurity->xssCleanPostInput('card_list');
        				   $cvv='';	
						   if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
						   {	
									$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
									$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
									
									$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
									if($this->czsecurity->xssCleanPostInput('cvv')!="")
									$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
									$cardType = $this->general_model->getType($card_no);
									$friendlyname = $cardType . ' - ' . substr($card_no, -4);

									$custom_data_fields['payment_type'] = $friendlyname;
							}
							else 
							{     
										$card_data= $this->card_model->get_single_card_data($cardID);
										$card_no  = $card_data['CardNo'];
										$expmonth =  $card_data['cardMonth'];
										$exyear   = $card_data['cardYear'];
										if($card_data['CardCVV'])
										$cvv      = $card_data['CardCVV'];
										$cardType = $card_data['CardType'];
										$custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
							}
							
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								$address1 =  $this->czsecurity->xssCleanPostInput('address');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                        $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');

						
                        $crtxnID='';  
                        $flag='false';
                        $error=$user='';
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" => "test_payment"
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];

        				$invoiceDetail = [];

                        if (!empty($this->czsecurity->xssCleanPostInput('invoice_number')) || !empty($po_number)) {
                            $new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_number');
                            $invoiceDetailArr = [
                                'invoiceNumber' => $new_invoice_number,
                                'purchaseOrderNumber' => $po_number
                            ];
                            $invoiceDetail = new CyberSource\Model\Ptsv2paymentsOrderInformationInvoiceDetails($invoiceDetailArr);
                        }

                        if($invoiceDetail){
                            $orderInfoArr['invoiceDetails'] = $invoiceDetail; 
                        }

        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Auth'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        				
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					    $trID =   $api_response[0]['id'];
        					    $msg  =   $api_response[0]['status'];
        					  
        					    $code =   '200';
        					    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
								$transactiondata= array();
								$inID='';
							 
							     if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
            				       
            			     
									$this->session->set_flashdata('success', 'Transaction Successful');
							    
            			
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                       
                              }
                        
                             $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields); 
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>'); 
					}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					
				
					$receipt_data = array(
						'transaction_id' => isset($api_response) ? $api_response[0]['id'] : '',
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'company/Payments/create_customer_auth',
						'proccess_btn_text' => 'Process New Transaction',
						'sub_header' => 'Authorize',
						'checkPlan'  =>  $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('company/home/transation_sale_receipt',  'refresh');
	    
	    
    	}
    	
   

     	public function create_customer_capture()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('strtxnID', 'Transaction ID', 'required|xss_clean');
			
             
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
                	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				 
    			            	$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                            $config    = $commonElement->ConnectionHost();
                        	$merchantConfig = $commonElement->merchantConfigObject();
                        	$apiclient    = new CyberSource\ApiClient($config, $merchantConfig);
                        	$api_instance = new CyberSource\Api\CaptureApi($apiclient);
                                
                                 $cliRefInfoArr = [
                    	    	"code" => $this->mName
                    	        ];
				 
                    		   $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                  $amountDetailsArr = [
                                      "totalAmount" => $amount,
                                      "currency" => CURRENCY
                                  ];
                                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                                
                                  $orderInfoArry = [
                                    "amountDetails" => $amountDetInfo
                                  ];
                                
                                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                                  $requestArr = [
                                    "clientReferenceInformation" => $client_reference_information,
                                    "orderInformation" => $order_information
                                  ];
                                  //Creating model
                                  $request = new CyberSource\Model\CapturePaymentRequest($requestArr);
                                  $api_response = list($response,$statusCode,$httpHeader)=null;
                 
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->capturePayment($request, $tID);  
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  	$condition = array('transactionID'=>$tID);
            					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
            					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
        					  	if($chh_mail =='1')
                                {
                                    $condition = array('transactionID'=>$tID);
                                 
                                 
                                    $tr_date   =date('Y-m-d H:i:s');
                                    $ref_number =  $tID;
                                    $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                    $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                                
								}
								$condition_mail         = array('templateType'=>'15', 'merchantID'=>$merchantID); 
								$ref_number =  ''; 
								$tr_date   =date('Y-m-d h:i A');
								$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
						
								
								$this->session->set_flashdata('success', 'Successfully Cpatured Transaction');
                    			
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
								  $error = $api_response[0]['status'];
								  $this->session->set_flashdata('success', 'Payment '.$error);
        					      
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                         
                         
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
                  
				  
				  $invoice_IDs = array();
				 
			  
				  $receipt_data = array(
					  'proccess_url' => 'company/Payments/payment_capture',
					  'proccess_btn_text' => 'Process New Transaction',
					  'sub_header' => 'Capture',
				  );
				  
				  $this->session->set_userdata("receipt_data",$receipt_data);
				  $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				  
				  if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($api_response[0]['id'] == ''){
						$api_response[0]['id'] ='null';
					}
				  redirect('company/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$api_response[0]['id'],  'refresh');
					
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('company/Payments/payment_capture','refresh');
			
	}	
	



	
	public function pay_multi_invoice()
	{
	  
	      
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
				
			$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
            	$custom_data_fields = [];
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                 $cusproID='';
				 $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
				 
        	$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		    	 $cardID_upd ='';
			     $flag='true';
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
			     $amount               = $this->czsecurity->xssCleanPostInput('totalPay');
			     $customerID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			      $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
                $invoiceIDs =  implode(',',$invoices); 
			     $inv_data   =    $this->customer_model->get_due_invoice_data($invoices,$user_id);
			    if(!empty($invoices) && !empty( $inv_data)) 
			    {
			       
                         $cusproID= $customerID  = $inv_data['Customer_ListID'];
    			        $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			     	$comp_data     = $this->general_model->get_row_data('chargezoom_test_customer',array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
    				  
    			
    			     if($cardID!="new1")
    			     {
    			         
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$phone    =  $card_data['Billing_Contact'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					$custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
    			     }
    			     else
    			     {
    			        
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
				       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
                    	$city    =$this->czsecurity->xssCleanPostInput('city');
                        $country =$this->czsecurity->xssCleanPostInput('country');
                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
                        $state   =  $this->czsecurity->xssCleanPostInput('state');
                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
                        $cardType = $this->general_model->getType($card_no);
						$friendlyname = $cardType . ' - ' . substr($card_no, -4);

						$custom_data_fields['payment_type'] = $friendlyname;
    			         
    			     }      
    			     
        		$crtxnID='';	     
        	
                           $amount  = $this->czsecurity->xssCleanPostInput('totalPay');
                             
                                $option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						
        						
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "test_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $comp_data['FirstName'],
                    		"lastName" => $comp_data['LastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => ($phone)?$phone:$comp_data['Phone'],
                    		"company" => $comp_data['companyName'],
                    		"email" => $comp_data['Contact']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);

                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
               
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                             
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );  
							 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
        					 
            		
            			$companyID = $comp_data['companyID'];
                            if(!empty($invoices))
                            {
        	                    foreach($invoices as $invoiceID)
        	                    {
        	                        $crtxnID='';
                                    $in_data =    $this->company_model->get_invoice_data_pay($invoiceID);
                                    if(!empty($in_data))
                                    {
                                        $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                        $txnID       = $invoiceID;  
            							$ispaid 	 = 'true';
            							
            							$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 'false';
            							 $app_amount = $in_data['AppliedAmount']-$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $condition  = array('TxnID'=>$in_data['TxnID'] );
            							 
        							    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
        							   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$pay_amounts,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser, $custom_data_fields);  
                                    } 
        	                    }
                            } 
                            
                            
                                if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
                            
                       
                     }
                     else
                     {
                               $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                         
                     }
                     
                     
                     
                   
                 } 
                	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
            				   
                    
		    }
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }

			if(!$checkPlan){
                $responseId  = '';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
            }
			
		    	 if($cusproID!="")
				 {
					 redirect('company/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 redirect('company/home/invoices','refresh');
				 }
	      
	}
	
	
	
	
	}  