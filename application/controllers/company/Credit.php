<?php

class Credit extends CI_Controller
{
    	function __construct()
	{
		parent::__construct();

		$this->load->model('general_model');
    	$this->load->model('company/customer_model','customer_model');
		$this->load->model('company/company_model','company_model');
		if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='5')
		{
			
		}else if($this->session->userdata('user_logged_in')!="")
		{
			
		}else
		{
			redirect('login','refresh');
		}
		
		
	}
	public function index(){
		
	}	
	
	public function credits(){
		$data['primary_nav'] = primary_nav();
		$data['template'] = template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
    	 $compdata	= $this->customer_model->get_customers_data($user_id);
    	 $data['customers']		= $compdata	;
    	 $condition  = array('comp.merchantID'=>$user_id); 
    	 
    	 $data['credits'] = $this->customer_model->get_credit_user_data($condition);
    	 $plantype = $this->general_model->chk_merch_plantype_status($user_id);
         $data['plantype'] = $plantype;
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('company/page_credit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
		
	}
	
	   public function create_credit()
	{
		 if($this->session->userdata('logged_in'))
        {
        
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
        
		$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		
		if(!empty($this->input->post(null, true))){
			 
				$total=0;	
			    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod){
                       $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
					   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
					   $insert_row['itemPrice']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					 
					   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					  $insert_row['updatedAt'] = date('Y-m-d H:i:s');
					   
					   
					   $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];	
					   $item_val[$key] =$insert_row;
                }	
        
                $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
			   if(!empty($in_data)){
    			$inv_pre   = $in_data['prefix'];
    			$inv_po    = $in_data['postfix']+1;
    			$new_inv_no = $inv_pre.$inv_po;
                
               
               }else{
               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>'); 
				
				  	redirect('company/Credit/credit','refresh');
               }
				   
				$ins_data['customerID'] = $this->czsecurity->xssCleanPostInput('customerID');
				$ins_data['creditDescription']= $this->czsecurity->xssCleanPostInput('cr_description');
               $ins_data['creditMemo']  = $this->czsecurity->xssCleanPostInput('cr_note');
				$ins_data['creditDate'] = date('Y-m-d H:i:s');
        	  $ins_data['creditAmount'] = $total;
              $ins_data['creditNumber'] = $new_inv_no;
             $ins_data['updatedAt']     = date('Y-m-d H:i:s');
             $crlsID =mt_rand('4464646464','55545454544');
                $ins_data['creditListID'] = $crlsID;
             $cname='';
			   $exist_row =   $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','FullName'), array('ListID'=>$ins_data['customerID']));
                $cname    = $exist_row['FullName'];
                 $cusdata = $this->general_model->get_select_data('tbl_company',array('qbwc_username'), array('id'=>$exist_row['companyID']) );
                 $user    =  $cusdata['qbwc_username'];
		
				 $ins_data['merchantDataID'] = $user_id;
                 $ins_id = $this->general_model->insert_row('tbl_custom_credit',$ins_data);	
					
					foreach($item_val as $k=>$item)
					{
						$item['crlineID'] = $ins_id;
						$ins = $this->general_model->insert_row('tbl_credit_item',$item);
					   $linitem = mt_rand('300000000',800000000);	
						$cr_itm = array('itemListID'=>$item['itemListID'],'CreditTxnLineID'=>$linitem,
						'ItemDesc'=>$item['itemDescription'],'itemQuantity'=>$item['itemQuantity'],
						'ItemRate'=>$item['itemPrice'],'Amount'=>($item['itemPrice']*$item['itemQuantity']),
						'CreditTxnID'=>$crlsID);
						
						$this->general_model->insert_row('chargezoom_customer_credit_item',$cr_itm);
					
						
				    } 		
			
					
				 if($ins_id ){
					 $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id), array('postfix'=>$inv_po));
                  
                  $credit_ins =array('CreditTxnID'=>$crlsID,
                  'companyListID' =>$exist_row['companyID'],
                  'DueDate'=>date('Y-m-d H:i:s'),
                  'TotalAmount'=>$total,
                  'SubTotal'=>$total,
                  'CreditRemaining'=>$total,
                  'IsPending'=>'false','Memo'=>$ins_data['creditMemo'],
                  'TimeCreated'=>date('Y-m-d H:i:s'),'TimeModified'=>date('Y-m-d H:i:s'),'CustomerListID'=>$ins_data['customerID'],'CustomerFullName'=>$cname);
                  
                  
                  	$ins = $this->general_model->insert_row('chargezoom_customer_credit',$credit_ins);
					  $this->session->set_flashdata('success', 'Successfully Added');
                  
			   
				 }else{
					 
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }
				
					redirect('company/Credit/credit','refresh');
				
        }
              
		$data['primary_nav'] = primary_nav();
		$data['template'] = template_variable();
		      
				  if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				$condition				= array('merchantID'=>$user_id );
			 
				$data['plans']          = $this->company_model->get_plan_data($user_id);
				$compdata				= $this->customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('company/create_credit', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}   
	
		//-------------for view credit  data--------------//
	
	
	public function get_credit_id()
	
    {
		
                 $creditID          =  $this->czsecurity->xssCleanPostInput('customerID'); 
		         $condition 		= array('creditName'=> $creditID);
				 $creditdatas		= $this->general_model->get_table_data('tbl_credits', $condition);
		
          ?>
		  
		   <table class="table table-bordered table-striped table-vcenter">
            
            <tbody>
				
				<tr>
					<th class="text-right"> <strong> Credit Date</strong></th>
					
					<th class="text-right"><strong>Amount ($)</strong></th>
					<th class="text-right"><strong> Processed On</strong></th>
					<th class="text-left"><strong> Edit/Delete</strong></th>
			  </tr>	
 	<?php	
				if(!empty($creditdatas))
				{  foreach($creditdatas as $creditdata){
			?>	
		
			<tr>  
			<td class="text-right visible-lg"><?php echo date('F d, Y', strtotime($creditdata['creditDate'])); ?> </a> </td>
			<td class="text-right visible-lg"> <?php echo number_format($creditdata['creditAmount'],2); ?> </a> </td>
			<td class="text-right visible-lg">  <?php if($creditdata['creditStatus']=="0" ){ echo"Yet to Processed";}else{echo $creditdata['creditDate']; }?> </a> 
			</td>
			
			<td class="text-left visible-lg"> <a href="javascript:void(0);" class="btn btn-default" onclick="set_edit_credit('<?php echo $creditdata['creditID'];  ?>');" title="Edit"> <i class="fa fa-edit"> </i> </a>  
              
			  <a href="#del_credit" onclick="del_credit_id('<?php echo $creditdata['creditID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Credit" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>



			</td>
			
			
			</tr>	
			
           
			<?php     }   }  ?>
           
			</tbody>
        </table>
				
				
		<?php  die;
		
	
    }
	
	
	
	
	public function get_creditedit_id()
     {
		
        $id = $this->czsecurity->xssCleanPostInput('credit_id');
		$val = array(
		'creditID' => $id,
		);
		
		$data = $this->general_model->get_row_data('tbl_credits',$val);
        echo json_encode($data);
     }
	//----------- TO update the credit  --------------//
	
	public function update_credit() {
	     if($this->czsecurity->xssCleanPostInput('creditEditID')!="" ){
				      
				        $id = $this->czsecurity->xssCleanPostInput('creditEditID');
					    $chk_condition = array('creditID'=>$id);
						
						$input_data['creditDate']  = date("Y-m-d");
						$input_data['creditAmount']  = $this->czsecurity->xssCleanPostInput('amount');
						$input_data['creditDescription']  = $this->czsecurity->xssCleanPostInput('description');
						 
	                    if($this->general_model->update_row_data('tbl_credits',$chk_condition, $input_data) ){
							$this->session->set_flashdata('success', 'Successfully Updated');
						
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
				
				 }
			
	 
			
	       }		
	              redirect(base_url('company/Credit/credit'));
	 
	 
	 } 
	


	
	
	
	
}