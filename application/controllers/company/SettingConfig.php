<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class SettingConfig extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');

        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('general_model');

        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '5') {

        } else if ($this->session->userdata('user_logged_in') != "") {

        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {
        redirect(base_url('SettingConfig/setting_customer_portal'), 'refresh');
    }
    /*
    * Description: Load and view customer portal page
    */
    public function setting_customer_portal()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];

        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $codition   = array('merchantID' => $user_id);
        $rootDomain = "https://demo.payportal.com/";

        if ($this->input->post(null, true)) {
            $enable      = $this->czsecurity->xssCleanPostInput('enable');
            $portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');
            $service_url = $this->czsecurity->xssCleanPostInput('service_url');
            $hepltext = $this->czsecurity->xssCleanPostInput('customer_help_text');
            $myInfo   = $this->czsecurity->xssCleanPostInput('myInfo');
            $myPass   = $this->czsecurity->xssCleanPostInput('myPass');
            $mySubsc  = $this->czsecurity->xssCleanPostInput('mySubscriptions');
            $image    = '';

            if (!empty($_FILES['picture']['name'])) {
                $config['image_library']  = 'uploads';
                $config['upload_path']    = 'uploads/merchant_logo/';
                $config['allowed_types']  = 'jpg|jpeg|png|gif';
                $config['file_name']      = time() . $_FILES['picture']['name'];
                $config['create_thumb']   = false;
                $config['maintain_ratio'] = false;
                $config['quality']        = '60%';
                $config['widht']          = LOGOWIDTH;
                $config['height']         = LOGOHEIGHT;
                $config['new_image']      = 'uploads/merchant_logo/';
                //Load upload library and initialize configuration
                $this->load->library('upload', $config);

                $this->upload->initialize($config);

                if ($this->upload->do_upload('picture')) {

                    $uploadData = $this->upload->data();
                    $picture    = $picture    = $uploadData['file_name'];
                } else {
                    $picture = '';
                }
                $image = $picture;
            }
            $url = "https://" . $portal_url . CUS_PORTAL . "/customer/";

            if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
                if (strpos($portal_url, ' ') > 0) {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error: Invalid portal url space not allowed.</strong></div>');
                    redirect('company/home/index', 'refresh');
                }

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error: Invalid portal url special character not allowed.</strong></div>');
                redirect('company/home/index', 'refresh');
            }

            $insert_data = array('customerPortal' => $enable, 'customerPortalURL' => $url, 'customerHelpText' => $hepltext, 'serviceurl' => $service_url, 'showInfoTab' => $myInfo, 'showPassword' => $myPass, 'showSubscription' => $mySubsc, 'merchantID' => $user_id, 'portalprefix' => $portal_url);

            $update_data = array('customerPortal' => $enable, 'customerPortalURL' => $url, 'customerHelpText' => $hepltext, 'serviceurl' => $service_url, 'showInfoTab' => $myInfo, 'showPassword' => $myPass, 'showSubscription' => $mySubsc, 'portalprefix' => $portal_url);
            if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $user_id)) > 0) {

                $Edit = $this->general_model->check_existing_edit_portalprefix('tbl_config_setting', $user_id, $portal_url);

                if ($Edit) {
                    $this->session->set_flashdata('message', 'Error: Customer Portal URL already exitsts. Please try again.');

                } else {

                    if ($image != "") {
                        $update_data['ProfileImage'] = $image;
                    }

                    $this->general_model->update_row_data('tbl_config_setting', $codition, $update_data);
                    $this->session->set_flashdata('success', 'Successfully Updated');
                }

                redirect(base_url('company/SettingConfig/setting_customer_portal'), 'refresh');

            } else {
                $check = $this->general_model->check_existing_portalprefix($portal_url);

                if ($check) {
                    $this->session->set_flashdata('message', 'Error: Customer Portal URL already exitsts. Please try again.');

                } else {
                    if ($image != "") {
                        $insert_data['ProfileImage'] = $image;
                    }

                    $this->general_model->insert_row('tbl_config_setting', $insert_data);

                    $this->session->set_flashdata('success', 'successfully Inserted');
                }

                redirect(base_url('company/SettingConfig/setting_customer_portal'), 'refresh');

            }
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['setting'] = $this->general_model->get_row_data('tbl_config_setting', $codition);
        if ($data['setting']['serviceurl'] == '') {
            $merchant                      = $this->general_model->get_select_data('tbl_merchant_data', array('weburl'), array('merchID' => $user_id));
            $data['setting']['serviceurl'] = $merchant['weburl'];
        }
        /* Check merchant plan type is VT */
        $planData = $this->general_model->chk_merch_plantype_data($user_id);
        $isPlanVT = 0;
		if(!empty($planData)){
			if($planData->merchant_plan_type == 'VT'){
				$isPlanVT = 1;
			}
		}
        $data['isPlanVT'] = $isPlanVT;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/page_customer_portal', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function check_url()
    {
        $res        = array();
        $portal_url = $this->czsecurity->xssCleanPostInput('portal_url');

        if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
            if (strpos($portal_url, ' ') > 0) {

                $res = array('portal_url' => 'Space is not allowed in url', 'status' => 'false');
                echo json_encode($res);
                die;
            }

            $res = array('portal_url' => 'Special character not allowed in url', 'status' => 'false');
            echo json_encode($res);
            die;
        } else {
            $res = array('status' => 'true');
            echo json_encode($res);
            die;

        }

    }

    public function create_template()
    {

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['id'];

        if ($this->czsecurity->xssCleanPostInput('templateName') != "") {

            $templateName = $this->czsecurity->xssCleanPostInput('templateName');
            $type         = $this->czsecurity->xssCleanPostInput('type');
            $fromEmail    = $this->czsecurity->xssCleanPostInput('fromEmail');
            $toEmail      = $this->czsecurity->xssCleanPostInput('toEmail');
            $addCC        = $this->czsecurity->xssCleanPostInput('ccEmail');
            $addBCC       = $this->czsecurity->xssCleanPostInput('bccEmail');
            $replyTo      = $this->czsecurity->xssCleanPostInput('replyEmail');
            $message      = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
            $subject      = $this->czsecurity->xssCleanPostInput('emailSubject');
            $createdAt    = date('Y-m-d H:i:s');

            if ($this->czsecurity->xssCleanPostInput('add_attachment')) {
                $add_attachment = '1';
            } else {
                $add_attachment = '0';
            }

            $insert_data = array('templateName' => $templateName,
                'templateType'                      => $type,
                'companyID'                         => $user_id,
                'fromEmail'                         => $fromEmail,
                'toEmail'                           => $toEmail,
                'addCC'                             => $addCC,
                'addBCC'                            => $addBCC,
                'replyTo'                           => $replyTo,
                'message'                           => $message,
                'emailSubject'                      => $subject,
                'attachedTo'                        => $add_attachment,

            );
            if ($this->czsecurity->xssCleanPostInput('tempID') != "") {
                $insert_data['updatedAt'] = date('Y-m-d H:i:s');
                $condition                = array('templateID' => $this->czsecurity->xssCleanPostInput('tempID'));
                $data['templatedata']     = $this->general_model->update_row_data('tbl_email_template', $condition, $insert_data);
            } else {
                $insert_data['createdAt'] = date('Y-m-d H:i:s');
                $id                       = $this->general_model->insert_row('tbl_email_template', $insert_data);
            }
            redirect('company/Settingmail/email_temlate', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->uri->segment('4')) {
            $temID                = $this->uri->segment('4');
            $condition            = array('templateID' => $temID);
            $data['templatedata'] = $this->general_model->get_row_data('tbl_email_template', $condition);

        }
        $data['types'] = $this->general_model->get_table_data('tbl_teplate_type', '');

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/page_email_template', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function profile_setting()
    {
        if ($this->session->userdata('logged_in')) {
            $user_ID = $this->session->userdata('logged_in')['merchID'];

        } else if ($this->session->userdata('user_logged_in')) {
            $user_ID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        if (!empty($this->input->post(null, true))) {

            if (!empty($_FILES['picture']['name'])) {
                $config['upload_path']   = 'uploads/merchant_logo/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name']     = time() . $_FILES['picture']['name'];

                //Load upload library and initialize configuration
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('picture')) {
                    $uploadData = $this->upload->data();
                    $picture    = $picture    = base_url() . 'uploads/merchant_logo/' . $uploadData['file_name'];
                } else {
                    $picture = '';
                }
                $input_data['merchantProfileURL'] = $picture;
            }

            $input_data['merchantFullAddress'] = $this->czsecurity->xssCleanPostInput('merchantFullAddress');
           
            $input_data['weburl'] = $this->czsecurity->xssCleanPostInput('weburl');

            $input_data['firstName']     = $this->czsecurity->xssCleanPostInput('firstName');
            $input_data['lastName']      = $this->czsecurity->xssCleanPostInput('lastName');
            $input_data['merchantEmail'] = $this->czsecurity->xssCleanPostInput('merchantEmail');

            $input_data['merchantAddress1']         = $this->czsecurity->xssCleanPostInput('merchantAddress1');
            $input_data['companyName']              = $this->czsecurity->xssCleanPostInput('companyName');
            $input_data['merchantContact']          = $this->czsecurity->xssCleanPostInput('merchantContact');
            $input_data['merchantAddress2']         = $this->czsecurity->xssCleanPostInput('merchantAddress2');
            $input_data['merchantCountry']          = $this->czsecurity->xssCleanPostInput('country');
            $input_data['merchantState']            = $this->czsecurity->xssCleanPostInput('state');
            $input_data['merchantCity']             = $this->czsecurity->xssCleanPostInput('city');
            $input_data['merchantZipCode']          = $this->czsecurity->xssCleanPostInput('merchantZipCode');
            $input_data['merchantAlternateContact'] = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
            $input_data['merchant_default_timezone'] = $this->czsecurity->xssCleanPostInput('merchant_default_timezone');

            if ($input_data['merchant_default_timezone']) {
                $session_data = $this->session->userdata('logged_in');
                if($session_data){
                    $session_data['merchant_default_timezone'] = $input_data['merchant_default_timezone'];
                    $this->session->set_userdata('logged_in', $session_data);  
                }else{
                    $session_data = $this->session->userdata('user_logged_in');
                    if($session_data){
                        $session_data['merchant_default_timezone'] = $input_data['merchant_default_timezone'];
                        $this->session->set_userdata('user_logged_in', $session_data);  
                    }
                }
            }
            $input_data['updatedAt']                = date('Y-m-d H:i:s');

            $pre   = $this->czsecurity->xssCleanPostInput('prefix');
            $post  = $this->czsecurity->xssCleanPostInput('postfix');
            $array = array($pre, $post);
            $qq    = implode('-', $array);
            $data  = array(

                'prefix'     => $pre,
                'postfix'    => $post,
                'invoiceNo'  => $qq,
                'merchantID' => $user_ID,

            );

            if (!empty($this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_ID)))) {

                $condition = array('merchantID' => $user_ID);

                $this->general_model->update_row_data('tbl_merchant_invoices', $condition, $data);

            } else {

                $condition = array('merchantID' => $user_ID);

                $this->general_model->insert_row('tbl_merchant_invoices', $data);
            }

            $id        = $this->czsecurity->xssCleanPostInput('merchID');
            $condition = array('merchID' => $user_ID);
            
            $status    = $this->general_model->update_row_data('tbl_merchant_data', $condition, $input_data);
            if ($status) {
                $input_data['merchID'] = $user_ID;  

                $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$condition);
                if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                {
                    /* Start campaign in hatchbuck CRM*/  
                    $this->load->library('hatchBuckAPI');
                    $input_data['merchant_type'] = 'Merchant Update';        
                    
                    $resource = $this->hatchbuckapi->createContact($input_data);
                    if($status['statusCode'] == 400){
                        
                        $resource = $this->hatchbuckapi->updateContact($input_data);
                    }else{
                        $resource = $this->hatchbuckapi->updateContact($input_data);
                    }
                    /* End campaign in hatchbuck CRM*/
                }

                $this->session->set_flashdata('success', 'Successully Updated');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong></div>');

            }
            redirect('company/SettingConfig/profile_setting');

        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];

        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        $con = array('merchID' => $user_id);
        $fix = $this->general_model->get_row_data('tbl_merchant_data', $con);

        $invoice['invoice'] = $fix;
        $invoice['prefix']  = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
        

        $country               = $this->general_model->get_table_data('country', '');
        $data['country_datas'] = $country;

       
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/page_createprefix', $invoice);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    //-------------- To load the coming soon page ---------------//
    public function comingsoon()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/page_comingsoon', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    //-------------- To load the API Key page ---------------//
    public function apikey()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $data['api_data'] = $this->general_model->get_table_data('tbl_merchant_api', array('MerchantID' => $user_id));
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/APIKey', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_api_key()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('api_name')) && !empty($this->czsecurity->xssCleanPostInput('domain_name'))) {
            $user_id = $this->session->userdata('logged_in')['merchID'];

            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchID,companyName, merchantEmail'), array('merchID' => $user_id));

            $name   = $this->czsecurity->xssCleanPostInput('api_name');
            $domain = $this->czsecurity->xssCleanPostInput('domain_name');

            $dev_api_key = $this->general_model->get_random_number($m_data, 'DEV');
            $pro_api_key = $this->general_model->get_random_number($m_data, 'PRO');

            $api_data['MerchantID'] = $user_id;
            $api_data['APIKeyDev']  = $dev_api_key;
            $api_data['APIKeyPro']  = $pro_api_key;
            $api_data['AppName']    = $name;
            $api_data['Domain']     = $domain;
            $api_data['Status']     = 1;
            $api_data['APIPackage'] = 5;
            $indid                  = $this->general_model->insert_row('tbl_merchant_api', $api_data);
            if ($indid) {
                $this->session->set_flashdata('success', 'Successfully Created');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');
        }
        redirect('company/SettingConfig/apikey', 'refresh');
    }

    public function delete_api_key()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('apiID'))) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
            $apiID   = $this->czsecurity->xssCleanPostInput('apiID');
            $m_data  = $this->general_model->get_row_data('tbl_merchant_api', array('MerchantID' => $user_id, 'apiID' => $apiID));

            if (!empty($m_data)) {

                if ($this->general_model->delete_row_data('tbl_merchant_api', array('MerchantID' => $user_id, 'apiID' => $apiID))) {
                    $indid = $this->card_model->delete_dev_api_key(array('MerchantID' => $user_id, 'APIKeyDev' => $m_data['APIKeyDev']));
                    $this->session->set_flashdata('success', 'Success');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error</strong></div>');
                }

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error, Invalid API Key</strong></div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');
        }

        redirect('company/SettingConfig/apikey', 'refresh');

    }

    //------------- Merchant gateway START ------------//

    public function gateway()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        $condition = array('merchantID' => $user_id);

        $data['all_gateway'] = $this->general_model->get_table_data('tbl_master_gateway', '');
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/page_merchant', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

   
    public function create_gateway()
    {

        $this->load->library('Gateway');
        $cr_status  = 1;
        $ach_status = 0;
        $isSurcharge = $surchargePercentage = 0;
        $extra1 = '';
        $signature = '';

        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {

                $user_id = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $user_id = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $condition = array('merchantID' => $user_id);
            $gmID     = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
            $gt_status = 0;
            $gt_obj    = new Gateway();
            $gatetype = $this->czsecurity->xssCleanPostInput('gateway_opt');

            if ($gatetype == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
                $gt_status   = $gt_obj->chk_nmi_gateway_auth($nmi_data);
                if ($this->czsecurity->xssCleanPostInput('mni_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '2') {

                $this->load->config('auth_pay');

                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');

               
                $gt_status = 1;

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

                $auth_data = array('user' => $nmiuser, 'password' => $nmipassword);

            } else if ($gatetype == '3') {
				$this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);
                $gt_status   = $gt_obj->chk_paytrace_gateway_auth($auth_data);
				$signature  = PAYTRACE_INTEGRATOR_ID; 

                if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

            } else if ($gatetype == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword, 'signature' => $signature);

                $gt_status = $gt_obj->chk_paypal_gateway_auth($auth_data);

            } else if ($gatetype == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_stripe_gateway_auth($auth_data);

            } else if ($gatetype == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_usaePay_gateway_auth($auth_data);

            } else if ($gatetype == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

            } else if ($gatetype == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey');

            } else if ($gatetype == '9') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword');
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
                $gt_status   = $gt_obj->chk_nmi_gateway_auth($nmi_data);
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            }  else if ($gatetype == '10') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                    
                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
                    $isSurcharge = 1;
                }
                else{
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
			} else if ($gatetype == '11') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}  else if ($gatetype  == '12') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword');
                $gmID                  = $this->czsecurity->xssCleanPostInput('tsysMerchID');
                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } else if ($gatetype == '13') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '14') {
                    $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
                    $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
                    $gmID = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
                    $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
                    if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
                        $cr_status = 1;
                    }
    
                    if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
                        $ach_status = 1;
                    } else {
                        $ach_status = 0;
                    }
    
			} else if ($gatetype  == '15') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser');
				$nmipassword     = '';
				$cr_status       =  1;
				$ach_status       =  0;
			} else if ($gatetype == '17') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId');
				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
			} else if ($gatetype  == '16') {
                $nmiuser         = $this->input->post('EPXCustNBR');
                $nmipassword     = $this->input->post('EPXMerchNBR');
                $signature     = $this->input->post('EPXDBANBR');
                $extra1   = $this->input->post('EPXterminal');
                if ($this->input->post('EPX_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }

            
            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');
            $frname   = $this->czsecurity->xssCleanPostInput('frname');
            

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewaySignature'                     => $signature,
                'extra_field_1'                        => $extra1,
                'gatewayType'                          => $gatetype,
                'merchantID'                           => $user_id,
                'gatewayFriendlyName'                  => $frname,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
                'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
            );
            
            if ($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {
                
                $val1 = array(
                    'merchantID' => $user_id,
                );
                
                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
                
                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);
                
                $this->session->set_flashdata('success', '<strong>Successfully Inserted</strong>');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

            }

            redirect(base_url('company/SettingConfig/gateway'));

        }

    }

    //----------- TO update the gateway  --------------//

    public function update_gateway()
    {
        if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {
            $ach_status = 0;
            $signature  = '';
            $extra1 = '';
            $cr_status  = 1;
            $isSurcharge = $surchargePercentage = 0;


            $id            = $this->czsecurity->xssCleanPostInput('gatewayEditID');
            $chk_condition = array('gatewayID' => $id);
            $gatetype = $this->czsecurity->xssCleanPostInput('gateway');
            $gmID     = $this->czsecurity->xssCleanPostInput('mid');
            if ($gatetype == 'NMI') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword1');
                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if (strtolower($gatetype) == 'authorize.net') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey1');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Paytrace') {
				$this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword1');
                $signature  = PAYTRACE_INTEGRATOR_ID; 

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
					$cr_status       =  1; 
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

            } else if ($gatetype == 'Paypal') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword1');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature1');
            } else if ($gatetype == 'Stripe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword1');

            } else if ($gatetype == 'USAePay') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin1');
            } else if($gatetype == 'Heartland') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey1');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
            } else if ($gatetype == 'Cybersource') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey1');
            } else if ($gatetype == 'Chargezoom') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword1');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == iTransactGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY1');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                    
                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box1')){
                    $isSurcharge = 1;
                }
                else{
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage1');
			} else if ($gatetype == 'FluidPay') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}  else if ($gatetype  == TSYSGatewayName) {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword1');
                $gmID                  = $this->czsecurity->xssCleanPostInput('tsysMerchID1');
                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } else if ($gatetype == BASYSGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == 'CardPointe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword1');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID1');
                $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName1');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status1', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status1', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

			} else if($gatetype == PayArcGatewayName){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser1');
                $nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
            } else if ($gatetype == MaverickGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId1');
				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
            } else if($gatetype == EPXGatewayName){
                $nmiuser         = $this->input->post('EPXCustNBR1');
                $nmipassword     = $this->input->post('EPXMerchNBR1');
                $signature     = $this->input->post('EPXDBANBR1');
                $extra1   = $this->input->post('EPXterminal1');
                if ($this->input->post('EPX_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }

            $frname   = $this->czsecurity->xssCleanPostInput('fname');
            

            $insert_data    = array(
				'gatewayUsername' => $nmiuser,
				'gatewayPassword' => $nmipassword,
				'gatewayMerchantID' => $gmID,
				'gatewayFriendlyName' => $frname,
				'gatewaySignature' => $signature,
                'extra_field_1' =>$extra1,
				'creditCard'       => $cr_status,
                'echeckStatus'      => $ach_status,
                'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
			);

            if ($this->general_model->update_row_data('tbl_merchant_gateway', $chk_condition, $insert_data)) {
                $this->session->set_flashdata('success', 'Successfully Updated');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

            }

            redirect(base_url('company/SettingConfig/gateway'));

        }

    }

    public function get_gatewayedit_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
        $val = array(
            'gatewayID' => $id,
        );

        $data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
		$data['gateway'] = getGatewayNames($data['gatewayType']);
        
        echo json_encode($data);die;
    }

    public function set_gateway_default()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {

            if ($this->session->userdata('logged_in')) {
                $da['login_info'] = $this->session->userdata('logged_in');

                $merchID = $da['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da['login_info'] = $this->session->userdata('user_logged_in');

                $merchID = $da['login_info']['merchantID'];
            }

            $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
            $val = array(
                'gatewayID' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );
            $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
            $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
        }
        redirect(base_url('company/SettingConfig/gateway'));
    }

    /**************Delete credit********************/

    public function delete_gateway()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
        $condition = array('gatewayID' => $gatewayID);

        if ($this->session->userdata('logged_in')) {
            $da['login_info'] = $this->session->userdata('logged_in');

            $merchID = $da['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $da['login_info'] = $this->session->userdata('user_logged_in');

            $merchID = $da['login_info']['merchantID'];
        }

        $num = $this->general_model->get_num_rows('tbl_chargezoom_subscriptions', array('paymentGateway' => $gatewayID, 'merchantDataID' => $merchID));
        if ($num == 0) {
            $del = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition);
            if ($del) {
                $this->session->set_flashdata('success', 'Successfully Deleted');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Error: Gateway can not be deleted, Please change subscription gateway</strong></div>');

        }
        redirect(base_url('company/SettingConfig/gateway'));

    }
}
