<?php 
/* This controller has following opration for gateways
 * NMI, Authorize.net, Paytrace, Paypal, Stripe Payment Gateway Operations
 
 * Refund create_customer_refund
 * Single Invoice Payment transaction refund
 * merchantID ans resellerID are Private Member
 */


use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException; 
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Services\ReportingService;

include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

include APPPATH . 'third_party/Fluidpay.class.php';
include APPPATH . 'third_party/TSYS.class.php';
include APPPATH . 'third_party/Cardpointe.class.php';
class RefundInvoice extends CI_Controller
{
    
     private $merchantID;
     private $resellerID;
     private $gatewayEnvironment;
    private $transactionByUser;

    public function __construct()
	{
		parent::__construct();
		
		$this->load->config('auth_pay');
     	$this->load->config('paytrace');
     	$this->load->config('paypal');  
        $this->load->config('globalpayments');
        $this->load->config('usaePay');
		$this->load->config('TSYS');
		$this->load->config('payarc');
		$this->load->library('PayarcGateway');
		
		$this->load->config('maverick');
		$this->load->library('MaverickGateway');
		
		$this->load->model('general_model');
		$this->load->model('card_model');
	    $this->load->model('company/customer_model','customer_model');
	
		if($this->session->userdata('logged_in')){
    		$da['login_info']	= $this->session->userdata('logged_in');
    		$this->merchantID = $da['login_info']['merchID'];
            $this->resellerID = $da['login_info']['resellerID'];
            $this->transactionByUser = ['id' => $da['login_info']['merchID'], 'type' => 1];
		}
		else if($this->session->userdata('user_logged_in')){
	
		    $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->merchantID = $merchID;
            $this->resellerID = $rs_Data['resellerID'];

		}else{
			redirect('login','refresh');
		}	
        $this->gatewayEnvironment = $this->config->item('environment');
	}
	
	function index()
	{
	     redirect('company/Payments/payment_transaction','refresh');
	}
	
	function create_customer_refund()
	{
	    
	   $trID='';
	  
	    
	     
	       if(!empty($this->czsecurity->xssCleanPostInput('pay_amount')))
	       {
    	          $trID    = $this->czsecurity->xssCleanPostInput('multi_inv');
    	         $index    = $this->czsecurity->xssCleanPostInput('index')-1;
    	       $pay_amout  = $this->czsecurity->xssCleanPostInput('pay_amount'); 
    	       $refAmt    = $pay_amout[$index];
    	       $ref_invID = $this->czsecurity->xssCleanPostInput('ref_invID');
    	      
	       } 
    	       if(!empty($this->czsecurity->xssCleanPostInput('ref_amount')))
    	       {
    	           $refAmt = $this->czsecurity->xssCleanPostInput('ref_amount');
    	             $trID   = $this->czsecurity->xssCleanPostInput('trID');
    	       }
	     
	     
	       if(!empty($this->czsecurity->xssCleanPostInput('ref_amount')))
	       {
	           $refAmt = $this->czsecurity->xssCleanPostInput('ref_amount');
	             $trID   = $this->czsecurity->xssCleanPostInput('trID');
	       }
	       
	      
	      if(!empty($trID))
	      {
			$refundStatus = false;
    	      $paydata = $this->general_model->get_select_data('customer_transaction',
    	      array('transactionID','transactionCard','transactionAmount','customerListID','merchantID','transactionGateway','gatewayID','invoiceTxnID','invoiceRefID','qbListTxnID'),array('id'=>$trID));
    	      $con=array('id'=>$trID);
    	   
     
            	if(!empty($paydata))	
            	{
            	      	 $gatlistval = $paydata['gatewayID'];
        		         $tID       = $paydata['transactionID']; 
        		       $rd = $this->general_model->check_refund_transaction_amount($tID); 
        		       
        		   
    		     
    	        	 if($rd)
    	        	 {
    		     
            		         
                    	      if($paydata['transactionGateway']=='1' || $paydata['transactionGateway']=='9')
                    	      {
                	             include APPPATH . 'third_party/nmiDirectPost.class.php';
                	         
                	          
                				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                			
                			   
                    			$nmiuser  = $gt_result['gatewayUsername'];
                    		    $nmipass  =  $gt_result['gatewayPassword'];
                    		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
                				$transaction = new nmiDirectPost($nmi_data);
                				$customerID = $paydata['customerListID'];
                				
                				 $amount     = $refAmt;
                				 
                			    $transaction->setTransactionId($tID);  
                
                			  
                				$transaction->refund($tID,$amount);
                				
                				$result     = $transaction->execute();
                				
                				   
                				 if($result['response_code'] == '100')
                				 {  
									$refundStatus = true;
                		          
                			      $val = array(
                    					'merchantID' => $paydata['merchantID'],
                    				);
                				
                    			               	$merchID =$this->merchantID;
                        		  
                			   
                            			
                            		
                            				    $this->customer_model->update_refund($trID, 'NMI');	
                            			
                			                     $ins_id = '';
                                                $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	          
                            				  	           );	
                            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                             
									$this->session->set_flashdata('success', 'Success');
                					
                			     
                				 }else{
                					 
                					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['responsetext'].'</strong>.</div>'); 
                				 }
                				       $transactiondata= array();
                				       $transactiondata['transactionID']      = $result['transactionid'];
                					   $transactiondata['transactionStatus']  = $result['responsetext'];
                					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
                					   $transactiondata['transactionType']    = $result['type'];
                					    $transactiondata['transactionCode']   = $result['response_code'];
                						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
                						$transactiondata['gatewayID']         = $gatlistval;
                					   $transactiondata['customerListID']     = $customerID;
                					   $transactiondata['transactionAmount']  = $amount;
                                              $transactiondata['merchantID']  = $this->merchantID;
                                            if(!empty($paydata['invoiceTxnID']))
                        			     	{
                        				       $transactiondata['invoiceTxnID']  = $paydata['invoiceTxnID'];
                        			     	}
                                        $transactiondata['resellerID']   = $this->resellerID;
                						 $transactiondata['gateway']   = "NMI";
                                    $CallCampaign = $this->general_model->triggerCampaign($this->merchantID,$transactiondata['transactionCode']);
                                    if(!empty($this->transactionByUser)){
                                        $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                        $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                    }     
                				    $id = $this->general_model->insert_row('customer_transaction',   $transactiondata); 
                	         
                	          
                	      }
                	       if($paydata['transactionGateway']=='2')
                	      {
                	           include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
                	           
                	             
                    			 $gatlistval = $paydata['gatewayID'];
                				  $tID       = $paydata['transactionID']; 
                				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                			
                			   
                    			$apiloginID  = $gt_result['gatewayUsername'];
                    		    $transactionKey  =  $gt_result['gatewayPassword'];
                			  
                			
                			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
                                $transaction->setSandbox($this->config->item('Sandbox'));
                
                				
                				$merchantID = $paydata['merchantID'];
                				
                				 $card    = $paydata['transactionCard'];
                				 $customerID = $paydata['customerListID'];
                				 $amount     =  $paydata['transactionAmount']; 
                			     $amount     =  $refAmt;
                			                  				
                				$result     = $transaction->credit($tID, $amount, $card);
                				
                				   
                				
                					
                				 if($result->response_code == '1')
                				 {  
									$refundStatus = true;
                				    
                				   
                		
                				 $this->customer_model->update_refund($trID, 'AUTH');	
                				     $ins_id = '';
                                                $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	          
                            				  	           );	
															  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
									$this->session->set_flashdata('success', 'Success');
                				 }else{
                					 
                					 
                					
                					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->response_reason_text.'</div>'); 
                				 }
                				     	$transactiondata= array();
                				        $transactiondata['transactionID']       = ($result->transaction_id)?$result->transaction_id:'';
                					   $transactiondata['transactionStatus']    = $result->response_reason_text;
                					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
                					   $transactiondata['transactionCode']     = $result->response_code;  
                					     $transactiondata['gatewayID']            = $gatlistval;
                                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
                					   
                						$transactiondata['transactionType']    = $result->transaction_type;	   
                					   $transactiondata['customerListID']      = $customerID;
                					   $transactiondata['transactionAmount']   = $result->amount;
                					   $transactiondata['merchantID']   = $this->merchantID;
                					   $transactiondata['resellerID']   =  $this->resellerID;
                					   $transactiondata['gateway']   = "Auth"; 
                					     if(!empty($paydata['invoiceTxnID']))
                        			     	{
                        				      $transactiondata['invoiceTxnID']  = $paydata['invoiceTxnID'];
                        			     	}
                                        $CallCampaign = $this->general_model->triggerCampaign($this->merchantID,$transactiondata['transactionCode']);
                                        if(!empty($this->transactionByUser)){
                                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                        } 
                				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
                 
                	          
                	      }
                	       if($paydata['transactionGateway']=='3')
                	      {
                	          
                	           include APPPATH . 'third_party/PayTraceAPINEW.php';
                	           
                	            
                				
                			   	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                				$payusername  = $gt_result['gatewayUsername'];
                    		    $paypassword  =  $gt_result['gatewayPassword'];
                    		    $grant_type    = "password";
                			    $payAPI = new PayTraceAPINEW();
                				$merchantID = $paydata['merchantID'];
                				
                				 $card    = $paydata['transactionCard'];
                				 $customerID = $paydata['customerListID'];
                				 $amount     =  $paydata['transactionAmount']; 
                			     $amount     =  $refAmt;
                			   $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                				//call a function of Utilities.php to verify if there is any error with OAuth token. 
                				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                			if(!$oauth_moveforward){
                			//Decode the Raw Json response.
                			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
                			
                			//set Authentication value based on the successful oAuth response.
                			//Add a space between 'Bearer' and access _token 
                			$oauth_token = sprintf("Bearer %s",$json['access_token']);
                			
                				 $con     = array('transactionID'=>$tID);
                				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
                				 
                				 $customerID = $paydata['customerListID'];
                				 $amount1  =  $paydata['transactionAmount']; 
                				  $total   = $refAmt;
                				  $amount  =  $total;
                				
                				if($paydata['transactionCode']=='200')
                				{
                					 $request_data = array( "transaction_id" => $tID,'amount'=>$total );
                					// encode Json data by calling a function from json.php
                					$request_data = json_encode($request_data);
                					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_TRID_REFUND);
                				     $response = $payAPI->jsonDecode($result['temp_json_response']);  
                			    		
                				}
                				
                			   
                				   if ( $result['http_status_code']=='200' )
                				 {
                				
									$refundStatus = true;
                				 
                				  $this->customer_model->update_refund($trID, 'PAYTRACE');
                				 
                				    $ins_id = '';
                                                $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	          
                            				  	           );	
                            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                					
                					
									$this->session->set_flashdata('success', $response['status_message']);
                				 }else{
                					 
                					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
                				   
                					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - </strong> '.$err_msg.'</div>'); 
                                }
                				 
                				       $transactiondata= array();
                					   if(isset($response['transaction_id'])){
                				       $transactiondata['transactionID']       = $response['transaction_id'];
                					   }else{
                						    $transactiondata['transactionID']  = '';
                					   }
                					   $transactiondata['transactionStatus']   = $response['status_message'];
                					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
                					   $transactiondata['transactionCode']     = $result['http_status_code'];
                					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
                					   $transactiondata['gatewayID']           = $gatlistval;
                                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
                						$transactiondata['transactionType']    = 'pay_refund';	   
                					   $transactiondata['customerListID']      = $paydata['customerListID'];
                					   $transactiondata['transactionAmount']   = $amount;
                					   $transactiondata['merchantID']          = $this->merchantID;
                					      if(!empty($paydata['invoiceTxnID']))
                        			     	{
                        				      $transactiondata['invoiceTxnID']  = $paydata['invoiceTxnID'];
                        			     	}
                					   $transactiondata['resellerID']         = $this->resellerID;
                					   $transactiondata['gateway']            = "Paytrace";
                					   $CallCampaign = $this->general_model->triggerCampaign($this->merchantID,$transactiondata['transactionCode']);
                                       if(!empty($this->transactionByUser)){
                                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                        } 
                				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
                		}else{
                	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Authentication failed.</strong></div>'); 
                		}		
                				
                			     
                			     
                	          
                	          
                	      } 
                	      
                	      if($paydata['transactionGateway']=='4')
                	      {
                	         include APPPATH . 'third_party/PayPalAPINEW.php';
                	             	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                    			    $username  = $gt_result['gatewayUsername'];
                					$password  = $gt_result['gatewayPassword'];
                					$signature = $gt_result['gatewaySignature'];
                					
                				    $config = array(
                						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
                						'APIUsername' => $username, 	// PayPal API username of the API caller
                						'APIPassword' => $password,	// PayPal API password of the API caller
                						'APISignature' => $signature, 	// PayPal API signature of the API caller
                						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                					  );
                					  
                					 if($config['Sandbox'])
                					{
                						error_reporting(E_ALL);
                						ini_set('display_errors', '1');
                					}
                					$this->load->library('paypal/Paypal_pro', $config);	
                						
                    		
                				 $customerID = $paydata['customerListID'];
                				 
                			
                				 $amount     =  $paydata['transactionAmount']; 
                				 	$merchantID =$paydata['merchantID']	;  
                				 
                				 	 $total   = $refAmt;
                				 	 
                				 	 if($amount==$total)
                				 	 {
                				 	    $restype="Full" ;
                				 	 }else{
                				 	    $restype="Partial" ;  
                				 	 }
                                    $amount     = $total ; 
                					   
                                   	$RTFields = array(
                					'transactionid' => $tID, 							// Required.  PayPal transaction ID for the order you're refunding.
                					'payerid' => '', 								// Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
                					'invoiceid' => '', 								// Your own invoice tracking number.
                					'refundtype' => $restype, 							// Required.  Type of refund.  Must be Full, Partial, or Other.
                					'amt' =>  $amount, 									// Refund Amt.  Required if refund type is Partial.  
                					'currencycode' => '', 							// Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
                					'note' => '',  									// Custom memo about the refund.  255 char max.
                					'retryuntil' => '', 							// Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
                					'refundsource' => '', 							// Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
                					'merchantstoredetail' => '', 					// Information about the merchant store.
                					'refundadvice' => '', 							// Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
                					'refunditemdetails' => '', 						// Details about the individual items to be returned.
                					'msgsubid' => '', 								// A message ID used for idempotence to uniquely identify a message.
                					'storeid' => '', 								// ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
                					'terminalid' => ''								// ID of the terminal.  50 char max.
                				);	
                					
                		$PayPalRequestData = array('RTFields' => $RTFields);
                		
                		$PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);	  
                			
                				
                				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
                				{  
                				     
									$refundStatus = true;
                			    	$code = '111';
                			  
                			
                
                			     $merchantID = $this->merchantID;
                			   
                			   
                			  
                				  $this->customer_model->update_refund($trID, 'PAYPAL');    
                		                        $ins_id = '';
                                                $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	          
                            				  	           );	
                            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
									$this->session->set_flashdata('success', 'Success');
                			        
                				 }else{
                					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
                					  $code = '401';
                					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
                				 }
                				       $transactiondata= array();
                				       	 $tranID ='' ;$amt='0.00';
                					      if(isset($PayPalResult['REFUNDTRANSACTIONID'])) { $tranID = $PayPalResult['REFUNDTRANSACTIONID'];   $amt =$PayPalResult['GROSSREFUNDAMT'];  }
                				       $transactiondata['transactionID']      = $tranID;
                					   $transactiondata['transactionStatus']  = $PayPalResult['ACK'];
                					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
                					   $transactiondata['transactionType']    = 'Paypal_refund' ;
                					    $transactiondata['transactionCode']   = $code;
                						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
                						$transactiondata['gatewayID']         = $gatlistval;
                					   $transactiondata['customerListID']     = $customerID;
                					   $transactiondata['transactionAmount']  = $amt ;
                					   $transactiondata['merchantID']   = $this->merchantID ;
                					   $transactiondata['resellerID']   = $this->resellerID;
                					   
                					     if(!empty($paydata['invoiceTxnID']))
                        			     	{
                        				      $transactiondata['invoiceTxnID']  = $paydata['invoiceTxnID'];
                        			     	}
                					   $transactiondata['gateway']   = "Paypal";
                					   $CallCampaign = $this->general_model->triggerCampaign($this->merchantID,$transactiondata['transactionCode']);
                                       if(!empty($this->transactionByUser)){
                                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                        } 
                				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
                	          
                	      }
                	       if($paydata['transactionGateway']=='5')
                	      {
                	            include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
                	    	
                	    	    $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                			
                			    $amount     = $refAmt;
                    			$nmiuser  = $gt_result['gatewayUsername'];
                    		    $nmipass  =  $gt_result['gatewayPassword']; 
                    		    
                                $plugin = new ChargezoomStripe();
                                $plugin->setApiKey($nmipass);
                				$charge = \Stripe\Refund::create(array(
                				  "charge" => $tID,
                				  "amount"=>($amount*100)
                				));
                			  
                				 
                				 $customerID = $paydata['customerListID'];
                	
                				
                			      $charge= json_encode($charge);
                				  
                				   $result = json_decode($charge);
                			
                				  
                			
                				 if(strtoupper($result->status) == strtoupper('succeeded'))
                				 {  
									$refundStatus = true;
                				     
                				$amount = ($result->amount/100) ;
                				 $code ='200';
                		
                			  	
                			    $merchantID = $this->merchantID;
                			    
                			    
                			    
                				  $this->customer_model->update_refund($trID, 'STRIPE');    
                					 
                		                      $ins_id = '';
                                                $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	          
                            				  	           );	
                            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
									$this->session->set_flashdata('success', 'Successfully Refunded');
                			        
                				 }else{
                					 $code =  $result->failure_code;
                					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
                				 }      
                				    
                				 
                				            $ttype = 'stripe_refund';
                				     
                				       $transactiondata= array();
                				       $transactiondata['transactionID']      = $result->id;
                					   $transactiondata['transactionStatus']  =  $result->status;
                					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
                					   $transactiondata['transactionType']    = $ttype;
                					    $transactiondata['transactionCode']   = $code;
                						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
                						$transactiondata['gatewayID']         = $gatlistval;
                					   $transactiondata['customerListID']     = $customerID;
                					   $transactiondata['transactionAmount']  = $amount;
                					   $transactiondata['merchantID']  = $this->merchantID;
                					   $transactiondata['resellerID']   = $this->resellerID ;
                					     if(!empty($paydata['invoiceTxnID']))
                        			     	{
                        				      $transactiondata['invoiceTxnID']  = $paydata['invoiceTxnID'];
                        			     	}
                				       $transactiondata['gateway']   = "Stripe";
                					   $CallCampaign = $this->general_model->triggerCampaign($this->merchantID,$transactiondata['transactionCode']); 	
                                       if(!empty($this->transactionByUser)){
                                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                        }      
                				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
                	    	
                	    	
                	          
                	      }
                	      if($paydata['transactionGateway']=='6')
                	      {
                	        
                	         require_once APPPATH."third_party/usaepay/usaepay.php";	
                	    	
                	    	 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                			 
                			       $amount        = $refAmt;
                    			$payusername      = $gt_result['gatewayUsername'];
                    		    $paypassword      =  $gt_result['gatewayPassword']; 
                    		    $inID ='';
                				 $customerID = $paydata['customerListID'];
                				 $inID       = $paydata['invoiceTxnID'];
                	            	$crtxnID='';	     
                        	
                                     $transaction=new umTransaction;
									$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
                	 
                					 $transaction->key=$payusername; 		// Your Source Key
                					 $transaction->pin= $paypassword;		// Source Key Pin
                					 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                					 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                					 $transaction->command="refund";    // refund command to refund transaction.
                					 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
                					 $transaction->amount = $amount ;
                					 $customerID = $paydata['customerListID'];
                					
                				 
                					$transaction->Process();
                					 
                					    $trID1 ='';
                					 if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                                     {
										$refundStatus = true;
                                        
                                         $msg = $transaction->result;
                                         $trID1 = $transaction->refnum;
                                         
                                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID1 );
                                          $this->customer_model->update_refund($trID, 'USAEPAY');    
                					 
                		                  $ins_id = '';
                                          $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	          
                            				  	           );	
                            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                                        
                                         $this->session->set_flashdata('success', 'Successfully Refunded');
                			        
                							 
                							 
                                     }
                                     else
                                     {
                                       $trID1='';
                                         $msg = $transaction->result;
                                         $trID1 = $transaction->refnum;
                                         
                                         $res =array('transactionCode'=>'300', 'status'=>$msg, 'transactionId'=> $trID1 );
                                        
                                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>'); 
                                         
                                     }
                                     
                                 
                             
                                       $id = $this->general_model->insert_gateway_transaction_data($res,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$refAmt,$this->merchantID,$crtxnID='', $this->resellerID,$inID, false, $this->transactionByUser);  
                                 }    
                	            
                	       if($paydata['transactionGateway']=='7')
                	      {
                	        
                	          require_once dirname(__FILE__) . '/../../../vendor/autoload.php'; 
                	    	
                	    	 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                		
                			       $amount     = $refAmt;
                    		     $secretApiKey  =  $gt_result['gatewayPassword']; 
                    		     $inID ='';
                				 $customerID = $paydata['customerListID'];
                				 $inID       = $paydata['invoiceTxnID'];
                	             $crtxnID    ='';	     
                        	
                                 $config     = new PorticoConfig();
                               
                                  $config->secretApiKey = $secretApiKey;
                                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                              	
                                  ServicesContainer::configureService($config);
                               
                			   try
                			   {
                			   
                            
                			       
                			       
                			     $response= Transaction::fromId($tID)
                			    
                                 ->refund($amount)
                                 ->withCurrency("USD")
                                 ->execute();
                                
                	            $tr1ID='';
                                 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                                     {
                                         $msg = $response->responseMessage;
                                         $tr1ID = $response->transactionId;
										$refundStatus = true;
                                         
                                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $tr1ID );
                                           $this->customer_model->update_refund($trID, 'GLOBAL');      
                					 
                		                  $ins_id = '';
                                          $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	          
                            				  	           );	
                            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                                        
                                        
                                         $this->session->set_flashdata('success', 'Successfully Refunded'); 
                							 
                							 
                                     }
                                     else
                                     {
                                          $msg = $response->responseMessage;
                                          $tr1ID = $response->transactionId;
                                           $res =array('trnsactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $tr1ID );
                                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>'); 
                                         
                                     }
                                     
                                     
                               
                             
                                       $id = $this->general_model->insert_gateway_transaction_data($res,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$refAmt,$this->merchantID,$crtxnID='', $this->resellerID,$inID, false, $this->transactionByUser);  
                                  
                			   }
                			    catch (BuilderException $e)
                                    {
                                        $error= 'Build Exception Failure: ' . $e->getMessage();
                                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                    }
                                    catch (ConfigurationException $e)
                                    {
                                        $error='ConfigurationException Failure: ' . $e->getMessage();
                                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                    }
                                    catch (GatewayException $e)
                                    {
                                        $error= 'GatewayException Failure: ' . $e->getMessage();
                                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                    }
                                    catch (UnsupportedTransactionException $e)
                                    {
                                        $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                    }
                                    catch (ApiException $e)
                                    {
                                        $error=' ApiException Failure: ' . $e->getMessage();
                                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                    }
                                    
                                    
                	             }
                	             
                	                
                	       if($paydata['transactionGateway']=='8')
                	      {
                	        
                	             $this->load->config('cyber_pay'); 
                	    	
                	    	    $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                			 
                			     $amount     = $refAmt;
                			     
                			     
                    		                    $option['merchantID']     = trim($gt_result['gatewayUsername']);
                            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
                        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
                        						
                        						if($this->config->item('Sandbox'))
                        						$env   = $this->config->item('SandboxENV');
                        						else
                        						$env   = $this->config->item('ProductionENV');
                        						$option['runENV']      = $env;
                        			
                        			    $commonElement = new CyberSource\ExternalConfiguration($option);
                                    
                                        $config = $commonElement->ConnectionHost();
                                       
                                    	$merchantConfig = $commonElement->merchantConfigObject();
                                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                    		            $api_instance = new CyberSource\Api\RefundApi($apiclient);
                    		            
                    		             $cliRefInfoArr = [
                                    "code" => "Refund Payment"
                                  ];
                                  $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                  $amountDetailsArr = [
                                      "totalAmount" => $amount,
                                      "currency" => CURRENCY
                                  ];
                                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                                  
                                  $orderInfoArry = [
                                    "amountDetails" => $amountDetInfo
                                  ];
                                
                                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                                  $paymentRequestArr = [
                                    "clientReferenceInformation" => $client_reference_information,
                                    "orderInformation" => $order_information
                                  ];
                                
                                  $paymentRequest = new CyberSource\Model\RefundPaymentRequest($paymentRequestArr);
                    		     
                    		     $inID ='';
                				 $customerID = $paydata['customerListID'];
                				 $inID       = $paydata['invoiceTxnID'];
                	             $crtxnID='';	     
                	             
                                 $trID1 ='';	             
                                  $api_response = list($response,$statusCode,$httpHeader)=null;
                                  try 
                                  {
                                    $api_response = $api_instance->refundPayment($paymentRequest, $tID);
                                     if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
                				    	{
                				    	    $codests='SUCCESS';
											$refundStatus = true;

                    					  $trID1 =   $api_response[0]['id'];
                    					  $msg  =   $api_response[0]['status'];
                    					  
                    					  $code =   '200';
                    					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID1 );
                    					    $this->customer_model->update_refund($trID, 'CYBER');      
                					 
                		                  $ins_id = '';
                                          $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                            				  	           );	
                            			 $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                                         
                                         $this->session->set_flashdata('success', 'Successfully Refunded');
                				    	}
                				    	else
                				    	{
                				    	        $trID1  =   $api_response[0]['id'];
                        					      $msg  =   $api_response[0]['status'];
                        					      $code =   $api_response[1];
                        					      $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID1 ); 
                        					      
                        					      $error = $api_response[0]['status'];
                        					      $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Payment '.$error.'</strong></div>');
                				    	    
                				    	}
                				    	
                				   
                        			   $id = $this->general_model->insert_gateway_transaction_data($res,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$refAmt,$this->merchantID,$crtxnID='', $this->resellerID,$inID, false, $this->transactionByUser);  		  
                                   
                                    
                                      } 
                                  catch (Cybersource\ApiException $e)
                                  {
                                      
                                        $error =$e->getMessage();
                                          $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Payment '.$error.'</strong></div>');
                				    	    
                                }
                            	
                               
                                    
                                    
						}
						
						if($paydata['transactionGateway']=='10')
						{
							$gatlistval = $paydata['gatewayID'];
							$tID       = $paydata['transactionID']; 
							$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
							
							$merchantID = $paydata['merchantID'];
							
							$card    = $paydata['transactionCard'];
							$customerID = $paydata['customerListID'];
							$amount     =  $paydata['transactionAmount']; 
							$amount     =  $refAmt;
							
							$apiUsername  = $gt_result['gatewayUsername'];
							$apiKey  = $gt_result['gatewayPassword'];
							$tr_type     = 'refund';
							
							$payload = [
								'amount' => ($amount * 100)
							];
							$sdk = new iTTransaction();
							
							$result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, $tID);

							$res = $result;

							if ($result['status_code'] == '200' || $result['status_code'] == '201') {
								$result['status_code'] = '200';  
								$refundStatus = true;
					
								$this->customer_model->update_refund($trID, 'AUTH');	
								$ins_id = '';
								$refnd_trr =array(
									'merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
									'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
									'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
									'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
								);	
								$this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
								$this->session->set_flashdata('success', 'Success');
							}else{
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->response_reason_text.'</div>'); 
							}
							$transactiondata                       = array();
							$transactiondata['transactionID']       = (isset($result['id'])) ? $result['id'] : '';
							$transactiondata['transactionStatus']   = $result['status'];
							$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
							$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
							$transactiondata['transactionType']     = $tr_type;
							$transactiondata['transactionCode']     = $result['status_code'];
							$transactiondata['gatewayID']          = $gatlistval;
							$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
							$transactiondata['customerListID']    = $customerID;
							$transactiondata['transactionAmount'] = $amount;
							$transactiondata['merchantID']        = $this->merchantID;
							$transactiondata['resellerID']        = $this->resellerID;
							$transactiondata['gateway']           = iTransactGatewayName;
							if(!empty($paydata['invoiceTxnID']))
							{
								$transactiondata['invoiceTxnID']  = $paydata['invoiceTxnID'];
							}
                            $CallCampaign = $this->general_model->triggerCampaign($this->merchantID,$transactiondata['transactionCode']);
                            if(!empty($this->transactionByUser)){
                                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                            } 
							$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
							
						}

						if($paydata['transactionGateway']=='11' || $paydata['transactionGateway']=='13')
						{
							$gatlistval = $paydata['gatewayID'];
							$tID       = $paydata['transactionID']; 
							$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
							
							$merchantID = $paydata['merchantID'];
							
							$card    = $paydata['transactionCard'];
							$customerID = $paydata['customerListID'];
							$amount     =  $paydata['transactionAmount']; 
							$amount     =  $refAmt;

							$tr_type     = 'refund';
							
							$gatewayTransaction              = new Fluidpay();
							$gatewayTransaction->environment = $this->gatewayEnvironment;
							$gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

							$refundAmount = $amount * 100;
							$payload = [
								'amount' => round($refundAmount,2)
							];

							$result = $gatewayTransaction->refundTransaction($tID, $payload);
							

							$res = $result;

							if ($result['status'] == 'success') {
								$refundStatus = true;

								$this->customer_model->update_refund($trID, '');	
								$ins_id = '';
								$refnd_trr =array(
									'merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
									'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
									'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
									'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
								);	
								$this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
								$this->session->set_flashdata('success', 'Success');
							}else{
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['msg'].'</div>'); 
							}

							$id = $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser);
							
						}
                	    if($paydata['transactionGateway']=='12')
                        {
                            $gatlistval = $paydata['gatewayID'];
                            $tID       = $paydata['transactionID']; 
                            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                            
                            $merchantID = $paydata['merchantID'];
                            
                            $card    = $paydata['transactionCard'];
                            $customerID = $paydata['customerListID'];
                            $amount     =  $paydata['transactionAmount']; 
                            $amount     =  $refAmt;

                            $tr_type     = 'refund';
                            
                            
                            $deviceID = $gt_result['gatewayMerchantID'].'01';
                            $gatewayTransaction              = new TSYS();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->deviceID = $deviceID;
                            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                            $generateToken = '';
                            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                                
                            }
                            $gatewayTransaction->transactionKey = $generateToken;

                            $payload = [
                                'amount' => ($amount * 100)
                            ];
                            $result = $gatewayTransaction->refundTransaction($tID, $payload);
                            

                            $res = $result;
                            $responseType = 'ReturnResponse';
                            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS')
                            {
								$refundStatus = true;

                                $this->customer_model->update_refund($trID, '');    
                                $ins_id = '';
                                $refnd_trr =array(
                                    'merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
                                    'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
                                    'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
                                    'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
                                );  
                                $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
                                $this->session->set_flashdata('success', 'Success');
                            }else{
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result[$responseType]['responseMessage'].'</div>'); 
                            }
                            $result['responseType'] = $responseType;
                            $id = $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser);
                            
                        }   else if ($paydata['transactionGateway'] == '14') {	
							$gatlistval = $paydata['gatewayID'];
							$tID       = $paydata['transactionID']; 
							$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
							$cardpointuser  = $gt_result['gatewayUsername'];
							$cardpointepass   = $gt_result['gatewayPassword'];
							$cardpointeMerchID = $gt_result['gatewayMerchantID'];
							$cardpointeSiteName  = $gt_result['gatewaySignature'];
							$merchantID = $paydata['merchantID'];
							
							$card    = $paydata['transactionCard'];
							$customerID = $paydata['customerListID'];
							$amount     =  $paydata['transactionAmount']; 
							$amount     =  $refAmt;

							$client = new CardPointe();
							

							$payload = [
								'amount' => ($amount)
							];
							$result = $client->refund($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $tID, $payload);
							


							if ($result['resptext'] == 'Approval') {
								$this->customer_model->update_refund($trID, '');	
								$ins_id = '';
								$refnd_trr =array(
									'merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
									'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
									'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
									'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
								);	
								$this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
								$this->session->set_flashdata('success', 'Success');
							}else{
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['resptext'].'</div>'); 
							}

							$id = $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser);
							
						}    
                	            
						
						if($refundStatus && isset($paydata['invoiceTxnID']) && !empty($paydata['invoiceTxnID'])){
							$condition  = array('TxnID'=>$paydata['invoiceTxnID'] );  
							$invoiceData = $this->general_model->get_row_data('chargezoom_test_invoice',$condition);
							$dataUpdate = [];
							$dataUpdate['AppliedAmount'] = $invoiceData['AppliedAmount'] + $refAmt;
							$dataUpdate['BalanceRemaining'] = $invoiceData['BalanceRemaining'] + $refAmt;
							$dataUpdate['IsPaid'] = 'false';

							$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $dataUpdate);
						}
                	                
                	             
                	             
                	 
                	        if(!empty($this->czsecurity->xssCleanPostInput('ref_invID')))
                	       {
                	         	redirect('company/home/invoices');  
                	       }
                	}
                   
                    else
                	{
                	    
                	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Already Refunded</strong></div>');  
                	}   
            	}
            	else
            	{
            	    
            	     $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Invalid Transaction</strong></div>');  
            	}    
	      }	
            else
            {
               $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Validation Error! Transaction is required</strong></div>');  
            }
        	    	redirect('company/Payments/payment_transaction');
    	    
    	}
	
		function create_payment_refund()
	{
	    
	    
	    
	      $trID  = $this->czsecurity->xssCleanPostInput('txnID');
	      $paydata = $this->customer_model->get_transaction_details_by_id($this->merchantID,$trID);
      
	   if(!empty($paydata))
	   {
	     
	       $pay_status='';
		   $gateway_id=  $gatlistval   = $paydata['gatewayID'];  
		       $tID          = $paydata['transactionID']; 
			 $customerID   = $paydata['customerListID'];
			 $amount       = $paydata['transactionAmount']-$paydata['partial'];
			 $tr_type      ='refund';
			 $gt_type      =  $paydata['transactionGateway'];
			 $isEcheckPaymentType = (strrpos($paydata['gateway'], 'ECheck'))? true : false;
			 
			 
	      if($paydata['transactionGateway']=='1' || $paydata['transactionGateway']=='9')
	      {
	             include APPPATH . 'third_party/nmiDirectPost.class.php';
	          
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword'];
    		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
			
				$tr_type ='refund';
				$transaction = new nmiDirectPost($nmi_data);
				
			
			    $transaction->setTransactionId($tID);  
			    $transaction->setAmount($amount);  
	    		$transaction->refund($tID,$amount);

				if($isEcheckPaymentType){
                    $transaction->setPayment('check');
                }
				
				$result     = $transaction->execute();
				
				 $res = $result;
				
				 if($result['response_code'] == '100')
				 {  
		         	
                   $pay_status = "SUCCESS";
                   
					$this->customer_model->update_refund_payment($tID, 'NMI');
                    $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
					$this->session->set_flashdata('success', 'Success');
				 }
				 else
				 {
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['responsetext'].'</strong>.</div>'); 
				 }
				     
	         
	          
	      }
	       if($paydata['transactionGateway']=='2')
	      {
	           include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
	           
	             
    			 $gatlistval = $paydata['gatewayID'];
				  $tID       = $paydata['transactionID']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
                $transaction->setSandbox($this->config->item('Sandbox'));
				$merchantID = $paydata['merchantID'];
				
				 $card       = $paydata['transactionCard'];
				 $customerID = $paydata['customerListID'];
				 if($isEcheckPaymentType){
                    $transaction->__set('method', 'echeck');
                }
				
				$result     = $transaction->credit($tID, $amount, $card);
				
				   
				
				$res = $result	;
				$tr_type ='refund';
				 if($result->response_code == '1')
				 {  
				     
				     	$pay_status="SUCCESS";
				    $this->customer_model->update_refund_payment($tID, 'AUTH');
                    $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
				    $this->session->set_flashdata('success', 'Success');
				 }else{
					 
					 
					
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->response_reason_text.'</div>'); 
				 }
				    
 
	          
	      }
	       if($paydata['transactionGateway']=='3')
	      {
	          
	           include APPPATH . 'third_party/PayTraceAPINEW.php';
	           
	            
				
			   	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
			    $payAPI = new PayTraceAPINEW();
				$merchantID = $paydata['merchantID'];
				
				 $card       = $paydata['transactionCard'];
				
				
			   $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward)
			{
    			//Decode the Raw Json response.
    			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
    			
    			//set Authentication value based on the successful oAuth response.
    			//Add a space between 'Bearer' and access _token 
    			$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
			     $total =$amount;
				 $request_data = array("transaction_id" => $tID, 'amount' => $total);
                    $url = URL_TRID_REFUND;

                    if($isEcheckPaymentType){
                        $request_data = array(
                            "check_transaction_id" => $tID,
                            "integrator_id" => $gt_result['gatewaySignature'],
                            'amount' => $total,
                        );
                        $url = URL_ACH_REFUND_TRANSACTION;
                    }
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, $url);
				     $response = $payAPI->jsonDecode($result['temp_json_response']);  
			      $response['http_status_code'] =$result['http_status_code'];
			      $tr_type = 'pay_refund';
				   if ( $result['http_status_code']=='200' )
				 {
			
				 	$pay_status="SUCCESS";
				     $this->customer_model->update_refund_payment($tID, 'PAYTRACE');
                    $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
				    
					 $this->session->set_flashdata('success', 'Success '.$response['status_message']);
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - </strong> '.$err_msg.'</div>'); 
                }
				 
	
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Authentication failed.</strong></div>'); 
		}		
				
			     
			     
	          
	          
	      } 
	      
	      if($paydata['transactionGateway']=='4')
	      {
	                include APPPATH . 'third_party/PayPalAPINEW.php';
	             	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
					  
					 if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);	
				 	 if($paydata['partial']==0)
				 	 {
				 	    $restype="Full" ;
				 	 }else{
				 	    $restype="Partial" ;  
				 	 }
                  
				
                   	$RTFields = array(
					'transactionid' => $tID, 							// Required.  PayPal transaction ID for the order you're refunding.
					'payerid' => '', 								// Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
					'invoiceid' => '', 								// Your own invoice tracking number.
					'refundtype' => $restype, 							// Required.  Type of refund.  Must be Full, Partial, or Other.
					'amt' =>  $amount, 									// Refund Amt.  Required if refund type is Partial.  
					'currencycode' => '', 							// Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
					'note' => '',  									// Custom memo about the refund.  255 char max.
					'retryuntil' => '', 							// Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
					'refundsource' => '', 							// Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
					'merchantstoredetail' => '', 					// Information about the merchant store.
					'refundadvice' => '', 							// Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
					'refunditemdetails' => '', 						// Details about the individual items to be returned.
					'msgsubid' => '', 								// A message ID used for idempotence to uniquely identify a message.
					'storeid' => '', 								// ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
					'terminalid' => ''								// ID of the terminal.  50 char max.
				);	
					
		     $PayPalRequestData = array('RTFields' => $RTFields);
		
	    	$PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);
		   
		      $res = $PayPalResult;
		      $tr_type ='Paypal_refund';
			
				
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
				{  
				     
			    
			    	$pay_status="SUCCESS";
			  
				     
				    $this->customer_model->update_refund_payment($tID, 'PAYPAL');    
                    $this->general_model->insert_gateway_transaction_data($PayPalResult, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
		
				  $this->session->set_flashdata('success', 'Success');
			      
				 }
				 else
				 {
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					  $code = '401';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
				 }
				
	          
	      }
	       if($paydata['transactionGateway']=='5')
	      {
	            include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php'; 
	    	
	    	    $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			  
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 
    		    $plugin = new ChargezoomStripe();
                $plugin->setApiKey($nmipass);
				$charge = \Stripe\Refund::create(array(
				  "charge" => $tID,
				 
				));
			  
				
	              $charge= json_encode($charge);
				  $result = json_decode($charge);
			
				  
				$res = 	$result ;
				$tr_type = 'stripe_refund';
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded'))
				 {  
			        $pay_status ="SUCCESS";
					
				    $this->customer_model->update_refund_payment($tID, 'STRIPE');
                    $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);

					$this->session->set_flashdata('success', 'Successfully Refunded');
				 }else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }      
				    
			   }
	      
	           
        	  if($paydata['transactionGateway']=='6')
    	      {
    	        
    	         require_once APPPATH."third_party/usaepay/usaepay.php";	
    	    	
    	    	 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
    			
        			$payusername      = $gt_result['gatewayUsername'];
        		    $paypassword      =  $gt_result['gatewayPassword']; 
        		 
    	            	$crtxnID='';	     
            	
                         $transaction=new umTransaction;
						 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
    	 
    					 $transaction->key=$payusername; 		// Your Source Key
    					 $transaction->pin= $paypassword;		// Source Key Pin
    					 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
    					 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
    					 $transaction->command="refund";    // refund command to refund transaction.
    					 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
    					 $transaction->amount = $amount ;
    					
    			    	 $merchantID =$this->merchantID;
    					$transaction->Process();
    					    
    					    $trID1 ='';
    					 if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                         {
                            $pay_status="SUCCESS";
                             $msg = $transaction->result;
                             $trID1 = $transaction->refnum;
                             
                             $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID1 );
                            $this->general_model->insert_gateway_transaction_data($res, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);

                             
                               $this->customer_model->update_refund($trID, 'USAEPAY'); 
								 
								 $this->session->set_flashdata('success', 'Transaction Successfully Refunded');
                             
    										 
                         }
                         else
                         {
                           $trID1='';
                             $msg = $transaction->result;
                             $trID1 = $transaction->refnum;
                             
                             $res =array('transactionCode'=>'300', 'status'=>$msg, 'transactionId'=> $trID1);
                           
                               $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>'); 
                             
                         }
                      
                       
                     }
    	             
    	            
    	       if($paydata['transactionGateway']=='7')
    	      {
    	        
    	          require_once dirname(__FILE__) . '/../../../vendor/autoload.php'; 
    	    	
    	    	 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
    			
        		    $secretApiKey  =  $gt_result['gatewayPassword']; 
        		   
    	             $crtxnID='';	     
                     $merchantID = $this->merchantID;
                     $config     = new PorticoConfig();
                   
                      $config->secretApiKey = $secretApiKey;
                      $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                  	
                      ServicesContainer::configureService($config);
                  
    			   try
    			   {
    			       
                     $response = Transaction::fromId($tID)
                        ->refund($amount)
                        ->withCurrency(CURRENCY)
                        ->execute();

    	                 $tr1ID='';
                        if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                         {
                             $msg = $response->responseMessage;
                             $tr1ID = $response->transactionId;
                             $pay_status='SUCCESS';
                             $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $tr1ID );
                              $this->customer_model->update_refund($trID, 'GLOBAL'); 
                            $this->general_model->insert_gateway_transaction_data($res, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
                             
								 $this->session->set_flashdata('success', 'Transaction Successfully Refunded');
                           
                             
    							 
    							 
                         }
                         else
                         {
                              $msg = $response->responseMessage;
                              $tr1ID = $response->transactionId;
                               $res =array('trnsactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $tr1ID );
                               $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>'); 
                             
                         }
                         
    			   }
    			    catch (BuilderException $e)
                        {
                            $error= 'Build Exception Failure: ' . $e->getMessage();
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (ConfigurationException $e)
                        {
                            $error='ConfigurationException Failure: ' . $e->getMessage();
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (GatewayException $e)
                        {
                            $error= 'GatewayException Failure: ' . $e->getMessage();
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (UnsupportedTransactionException $e)
                        {
                            $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                           $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (ApiException $e)
                        {
                            $error=' ApiException Failure: ' . $e->getMessage();
                         $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        
                        
    	             }
    	             
    	       
    	             
    	       if($paydata['transactionGateway']=='8')
    	      {
    	         $this->load->config('cyber_pay'); 
	    	
	    	    $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			 
			    
			     
			       $option=array();
    		                    $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			    $commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
    		            $api_instance = new CyberSource\Api\RefundApi($apiclient);
    		            
    		             $cliRefInfoArr = [
                    "code" => "Refund Payment"
                  ];
                  $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                  $amountDetailsArr = [
                      "totalAmount" => $amount,
                      "currency" => CURRENCY
                  ];
                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                  
                  $orderInfoArry = [
                    "amountDetails" => $amountDetInfo
                  ];
                
                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                  $paymentRequestArr = [
                    "clientReferenceInformation" => $client_reference_information,
                    "orderInformation" => $order_information
                  ];
                
                  $paymentRequest = new CyberSource\Model\RefundPaymentRequest($paymentRequestArr);
    		     	$merchantID  = $this->merchantID;
    		     $inID ='';
				
	             $crtxnID='';	     
	             $ins_id = '';    $tr1ID='';
                $tr_type = 'refund';	             
                  $api_response = list($response,$statusCode,$httpHeader)=null;
                  try 
                  {
                     
                  
                    $api_response = $api_instance->refundPayment($paymentRequest, $tID);
                     if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
				    	{
				    	    $pay_status='SUCCESS';
    					    $trID1 =   $api_response[0]['id'];
    					    $msg  =    $api_response[0]['status'];
    					  
    					    $code =   '200';
    					    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID1 );
    					      
							$this->customer_model->update_refund($trID, 'CYBER'); 
                            $this->general_model->insert_gateway_transaction_data($res, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
							
							$this->session->set_flashdata('success', 'Success');
    					  
    		                  }
                  }
    			    catch (Cybersource\ApiException $e)
                  {
                      
                          $error =$e->getMessage();
                          $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                   }
    			   
		   } 
		   
		   	if ($paydata['transactionGateway'] == '10') {
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

				$apiUsername  = $gt_result['gatewayUsername'];
				$apiKey  = $gt_result['gatewayPassword'];
				$tr_type     = 'refund';
				
				$payload = [
					'amount' => ($amount * 100)
				];
				$sdk = new iTTransaction();
				
				$result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, $tID);

				$res = $result;

				if ($result['status_code'] == '200' || $result['status_code'] == '201') {
					$result['status_code'] = '200';
					$pay_status = "SUCCESS";

					$this->customer_model->update_refund_payment($tID, iTransactGatewayName);
					$this->session->set_flashdata('success', ' Success');

					$transactiondata                        = array();
					$transactiondata['transactionID']       = (isset($result['id'])) ? $result['id'] : '';
					$transactiondata['transactionStatus']   = $result['status'];
					$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					$transactiondata['transactionType']     = $tr_type;
					$transactiondata['transactionCode']     = $result['status_code'];
					$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
					$transactiondata['gatewayID']           = $gatlistval;

					if ($paydata['invoiceTxnID'] != "") {
						$transactiondata['invoiceTxnID'] = $paydata['invoiceTxnID'];
					}

					$transactiondata['customerListID']    = $customerID;
					$transactiondata['transactionAmount'] = $amount;
					$transactiondata['merchantID']        = $paydata['merchantID'];
					$transactiondata['gateway']           = $gatewayName;
					$transactiondata['resellerID']        = $this->resellerID;
                    $transactiondata['parent_id']    = $paydata['id'];
                    
                    $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']); 
                    if(!empty($this->transactionByUser)){
                        $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                        $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                    }
					$id = $this->general_model->insert_row('customer_transaction', $transactiondata);
				} else {
					$err_msg = $result['status'] = $result['error']['message'];
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - ' . $err_msg . '</strong>.</div>');
				}
			}  
			
			if ($paydata['transactionGateway'] == '11' || $paydata['transactionGateway'] == '13') {
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

				$tr_type     = 'refund';
							
				$gatewayTransaction              = new Fluidpay();
				$gatewayTransaction->environment = $this->gatewayEnvironment;
				$gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

				$refundAmount = $amount * 100;
                $payload = [
                    'amount' => round($refundAmount,2)
                ];
				
				$result = $gatewayTransaction->refundTransaction($tID, $payload);
				

				$res = $result;

				if ($result['status'] == 'success') {
					$this->customer_model->update_refund($trID, '');	
					$pay_status = "SUCCESS";

					$gateway_name = (($paydata['transactionGateway'] == '11') ? FluidGatewayName: BASYSGatewayName);
					$this->customer_model->update_refund_payment($tID, $gateway_name);
					$this->session->set_flashdata('success', ' Success');
				}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['msg'].'</div>'); 
				}

				$id = $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
				
			}  

    	    if ($paydata['transactionGateway'] == '12') {
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

                $tr_type     = 'refund';
                $deviceID = $gt_result['gatewayMerchantID'].'01';    

                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;

                $payload = [
                    'amount' => $amount
                ];
                $result = $gatewayTransaction->refundTransaction($tID, $payload);

                $res = $result;
                $responseType = 'ReturnResponse';
                if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                    $this->customer_model->update_refund($trID, '');    
                    $pay_status = "SUCCESS";

                    $this->customer_model->update_refund_payment($tID, TSYSGatewayName);
                    $this->session->set_flashdata('success', ' Success');
                }else{
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result[$responseType]['responseMessage'].'</div>'); 
                }
                $result['responseType'] = $responseType;
                $id = $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, [], $paydata['id']);
                
			}

			if ($paydata['transactionGateway'] == '14' ) {

                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
                $cardpointeuser  = $gt_result['gatewayUsername'];
                $cardpointepass   = $gt_result['gatewayPassword'];
                $cardpointeMerchID = $gt_result['gatewayMerchantID'];
                $cardpointeSiteName  = $gt_result['gatewaySignature'];
                
				$client = new CardPointe();

				$payload = [
					'amount' => ($amount * 100)
				];
				$result = $client->refund($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $tID, $payload);
			
				if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Success') {
					$this->customer_model->update_refund($trID, '');	
					$pay_status = "SUCCESS";

					
					$this->customer_model->update_refund_payment($tID, iTransactGatewayName);
					$this->session->set_flashdata('success', ' Success');
				}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['resptext'].'</div>'); 
				}

				$id = $this->general_model->insert_gateway_transaction_data($result, $tr_type, $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser);
				
            }

			if($paydata['transactionGateway'] == '15')
			{

				// PayArc

				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

				$tr_type     = 'refund';
				
				$this->payarcgateway->setApiMode($this->config->item('environment'));
				$this->payarcgateway->setSecretKey($gt_result['gatewayUsername']);

				$charge_response = $this->payarcgateway->refundCharge($tID, ($amount * 100));

				$result = json_decode($charge_response['response_body'], 1);
				
				$res = $result;


				if (isset($result['data']) && $result['data']['status'] == 'refunded') {
					$this->customer_model->update_refund($trID, '');	
					$pay_status = "SUCCESS";

					
					$this->customer_model->update_refund_payment($tID, PayArcGatewayName);
					$this->session->set_flashdata('success', ' Success');

				} else {
					$err_msg = $result['message'];
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $err_msg . '</strong>.</div>');
				}
			}
			
			if($paydata['transactionGateway'] == '17')
			{
				// Maverick Payment

				$isEcheck = false;
                if (strpos($paydata['gateway'], 'ECheck') !== false) {
                    $isEcheck = true;
                }

                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

                // Maverick Payment Gateway
                $this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
                $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                if($isEcheck) {
                    $r = $this->maverickgateway->refundAchSale($tID, $amount);
                } else {
                    $r = $this->maverickgateway->refundSale($tID, $amount);
                }

                $rbody = json_decode($r['response_body'], 1);
                
                $result = [];
                $result['data'] = $rbody;
                
                $result['response_code'] = $r['response_code'];

                if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                    if($rbody['status']['status'] == 'Approved'){
                        $result['status'] = 'success';
                        $result['msg'] = $result['message'] = 'Successfully Refunded Payment.';
                    } else {
                        $result['status'] = 'failed';
                        $result['msg'] = $result['message'] = 'Payment refund failed.';    
                    }
                } else {
                    $result['status'] = 'failed';
                    $result['msg'] = $result['message'] = $rbody['message'];
				}
				
				
				if ($result['status'] == 'success') {
					
					$this->customer_model->update_refund($trID, '');	
					$pay_status = "SUCCESS";

					
					$this->customer_model->update_refund_payment($tID, PayArcGatewayName);
					$this->session->set_flashdata('success', ' Success');

				} else {
					$pay_status = "FAILED";
					$err_msg = $result['message'];
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $err_msg . '</strong>.</div>');
				}
			}

	      	if($pay_status=='SUCCESS')
	      	{
	          	if(!empty(!empty($paydata['invoice_id'])))
				{
				       $paymts   =   explode(',',$paydata['tr_amount']);
				       $invoices =   explode(',',$paydata['invoice_id']);
				       $ins_id = '';
				    	foreach($invoices as $k1=> $inv)
            			{
            			     
                                $refnd_trr =array('merchantID'=>$this->merchantID, 'refundAmount'=>$paymts[$k1],
            				  	           'creditInvoiceID'=>$inv,'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$customerID,
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));	
            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
				    
            			}
				    
				    
        			  	$cusdata = $this->general_model->get_select_data('tbl_company',array('qbwc_username','id'), array('merchantID'=>$this->merchantID) );
        			
        				$user_id  = $this->merchantID;
        				$user    =  $cusdata['qbwc_username'];
        		        $comp_id  =  $cusdata['id']; 
        		        $ittem = $this->general_model->get_row_data('chargezoom_test_item',array('companyListID'=>$comp_id, 'Type'=>'Payment'));
        		        $refund =$amount;
			            
        		        if(empty($ittem))
        		        {
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>'); 
                           
                           exit;
        		        }
        				$ins_data['customerID']     = $customerID;
        				
        				
            			foreach($invoices as $k=> $inv)
            			{
            			      $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
                			   if(!empty($in_data))
                			   {
                    			$inv_pre   = $in_data['prefix'];
                    			$inv_po    = $in_data['postfix']+1;
                    			$new_inv_no = $inv_pre.$inv_po;
                               }
                			  $ins_data['merchantDataID']        = $this->merchantID;	
                			  $ins_data['creditDescription']     ="Credit as Refund" ;
                              $ins_data['creditMemo']    = "This credit is given to refund for a invoice ";
                			  $ins_data['creditDate']   = date('Y-m-d H:i:s');
                        	  $ins_data['creditAmount']   = $paymts[$k];
                              $ins_data['creditNumber']   = $new_inv_no;
                              $ins_data['updatedAt']     = date('Y-m-d H:i:s');
                              $ins_data['Type']         = "Payment";
                        	  $ins_id = $this->general_model->insert_row('tbl_custom_credit',$ins_data);
        					   
        					   $item['itemListID']      =    $ittem['ListID']; 
        				       $item['itemDescription'] =    $ittem['Name']; 
        				       $item['itemPrice'] =$paymts[$k]; 
        				       $item['itemQuantity'] =0; 
        				      	$item['crlineID'] = $ins_id;
        				      	$acc_name  = $ittem['DepositToAccountName']; 
        				      	$acc_ID    = $ittem['DepositToAccountRef']; 
        				      	$method_ID = $ittem['PaymentMethodRef']; 
        				      	$method_name  = $ittem['PaymentMethodName']; 
        						 $ins_data['updatedAt'] = date('Y-m-d H:i:s');
        						$ins = $this->general_model->insert_row('tbl_credit_item',$item);	
        				  	    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$paymts[$k],
        				  	           'creditInvoiceID'=>$invID,'creditTransactionID'=>$tID,
        				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$customerID,
        				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
        				  	           'paymentMethod'=>$method_ID,'paymentMethodName'=>$method_name,
        				  	           'AccountRef'=>$acc_ID,'AccountName'=>$acc_name
        				  	           );	
        					
        				 if($ins_id && $ins)
        				 {
        					 $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id), array('postfix'=>$inv_po));
        					 
                             $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1','', $user);
                          }
                          
            			}  
            			
				}
				else
				{
				     $inv='';$ins_id='';
				    $refnd_trr =array('merchantID'=>$this->merchantID, 'refundAmount'=>$paydata['transactionAmount'],
            				  	           'creditInvoiceID'=>$inv,'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$customerID,
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));	
            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);    
				}

                $receipt_data = array(
                    'proccess_url' => 'company/Payments/payment_refund',
                    'proccess_btn_text' => 'Process New Refund',
                    'sub_header' => 'Refund',
                );
                
                $this->session->set_userdata("receipt_data",$receipt_data);
                $this->session->set_userdata("invoice_IDs",$paydata['invoiceTxnID']);
                
                if($paydata['invoiceTxnID'] != ''){
                    redirect('company/home/transation_credit_receipt/transaction/'.$customerID.'/'.$tID,  'refresh');
                }else{
                    redirect('company/home/transation_credit_receipt/transaction/'.$customerID.'/'.$tID,  'refresh');
                    
                }
                
        					
             }
			
	      
	}  

	      
	   	redirect('company/Payments/payment_transaction','refresh');
	      
	    
	    
	    
	    
	}
	
	
	 public function getError($eee)
	 { 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }
	 
}	