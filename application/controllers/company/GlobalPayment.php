<?php
/**
 * This Controller has Authorize.net Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * create_customer_esale
 */
 
require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;
use GlobalPayments\Api\Entities\Enums\AccountType;
use GlobalPayments\Api\Entities\Enums\CheckType;
use GlobalPayments\Api\PaymentMethods\ECheck;

class GlobalPayment extends CI_Controller
{
    
    private $resellerID;
    private $transactionByUser;

	public function __construct()
	{
		parent::__construct();
		
		
        $this->load->config('globalpayments');
		
        $this->load->model('company/customer_model','customer_model');
		$this->load->model('general_model');
		$this->load->model('company/company_model','company_model');
		$this->load->model('card_model');
		$this->load->library('form_validation');
		 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='5' )
		  {
		      
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
                  
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index()
	{
	    	  
	  
	  redirect('company/home/index','refresh');
	    
	}
	
	public function pay_invoice()
	{
	  
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
            if($this->czsecurity->xssCleanPostInput('sch_method') != 2){
    		    $this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
			}
            $custom_data_fields = [];
    	
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
        
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
            }
            
           
        	$checkPlan = check_free_plan_transactions();
            $pay_option = $this->czsecurity->xssCleanPostInput('sch_method');
            $echeck_payment = false;
            if($pay_option == 2){
                $echeck_payment = true;
            }

	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	 $cardID_upd ='';
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			     $in_data   =    $this->company_model->get_invoice_data_pay($invoiceID);
			      if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			     
			  
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;
			    if(!empty( $in_data)) 
			    {
			            $customerID = $in_data['Customer_ListID'];
            			$c_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName', 'FirstName', 'LastName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
            		
            			$companyID = $c_data['companyID'];
			        
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  
    			     $secretApiKey   = $gt_result['gatewayPassword'];
    			     
    			     if($cardID!="new1")
    			     {
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			       
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
                        $state    = $card_data['Billing_State'];
    					$country    = $card_data['Billing_Country'];
                        $accountNumber  = $card_data['accountNumber'];
                        $routeNumber  = $card_data['routeNumber'];
                        $accountName  = $card_data['accountName'];
                        $secCodeEntryMethod  = $card_data['secCodeEntryMethod'];
                        $accountType  = $card_data['accountType'];
                        $accountHolderType  = $card_data['accountHolderType'];
                        $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
    			     }
    			     else
    			     {
                        $accountNumber  = $this->czsecurity->xssCleanPostInput('acc_number');
                        $routeNumber  = $this->czsecurity->xssCleanPostInput('route_number');
                        $accountName  = $this->czsecurity->xssCleanPostInput('acc_name');
                        $secCodeEntryMethod  = $this->czsecurity->xssCleanPostInput('secCode');
                        $accountType  = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                        $accountHolderType  = $this->czsecurity->xssCleanPostInput('acct_type');

    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
                        if($pay_option == 2){
                           $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                           $custom_data_fields['payment_type'] = $friendlyname;
                        }else{

                           $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                           $custom_data_fields['payment_type'] = $friendlyname;
                        }
    			         
    			     }                              
        		$crtxnID='';	     
        	
                 $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  if($this->config->item('Sandbox'))
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                else
                $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                
                 $config->developerId =  $this->config->item('DeveloperId');
                 $config->versionNumber =  $this->config->item('VersionNumber');
            
                ServicesContainer::configureService($config);
                $card = new CreditCardData();
                 
                $card->number = $card_no;
                $card->expMonth = $expmonth;
                $card->expYear = $exyear;
                if($cvv!="")
                $card->cvn = $cvv;
                 $card->cardType=$cardType;
                // add customer name
                $firstName = isset($c_data['FirstName']) ? $c_data['FirstName'] : '';
                $lastName = isset($c_data['LastName']) ? $c_data['LastName'] : '';
                $cardHolderName = ($lastName) ? $firstName.' '.$lastName : $firstName;
                $card->cardHolderName = $cardHolderName;

                $address = new Address();
                $address->streetAddress1 = $address1;
                $address->city = $city;
                $address->state = $state;
                $address->postalCode = $zipcode;
                $address->country = $country;
               
                $invNo  =mt_rand(1000000,2000000);
             	try
                {   
                    if($pay_option == 2){
                        $check = new ECheck();
                        $check->accountNumber = $accountNumber;
                        $check->routingNumber = $routeNumber;
                        if(strtolower($accountType) == 'checking'){
                            $check->accountType = 0;
                        }else{
                            $check->accountType = 1;
                        }

                        if(strtoupper($accountHolderType) == 'PERSONAL'){
                            $check->checkType = 0;
                        }else{
                            $check->checkType = 1;
                        }
                        $check->checkHolderName = $accountName;
                        $check->secCode = "WEB";

                        $response = $check->charge($amount)
                        ->withCurrency(CURRENCY)
                        ->withAddress($address)
                        ->withInvoiceNumber($invNo)
                        ->withAllowDuplicates(true)
                        ->execute();
                    }else{
                        $response = $card->charge($amount)
                        ->withCurrency(CURRENCY)
                        ->withAddress($address)
                        ->withInvoiceNumber($invNo)
                        ->withAllowDuplicates(true)
                        ->execute();
                    }
                    
                    if($response->responseCode != 0 && $response->responseCode != '00')
                    {
                        $error='Gateway Error. Invalid Account Details';
                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
                        if($cusproID=="2"){
                            redirect('company/home/view_customer/'.$customerID);
                        }else if($cusproID=="3" ){
                            redirect('company/home/invoice_details/'.$in_data['invoiceID']);
                        }else{
                            redirect('company/home/invoices');
                        }
                    }
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                        if($pay_option != 2){

                            // add level three data
                            $transaction = new Transaction();
                            $transaction->transactionReference = new TransactionReference();
                            $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                            $level_three_request = [
                                'card_no' => $card_no,
                                'amount' => $amount,
                                'invoice_id' => $invNo,
                                'merchID' => $user_id,
                                'transaction_id' => $response->transactionId,
                                'transaction' => $transaction,
                                'levelCommercialData' => $levelCommercialData,
                                'gateway' => 7
                            ];
                            addlevelThreeDataInTransaction($level_three_request);
                        }
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                            $txnID      = $in_data['TxnID'];  
							 $ispaid 	 = 'true';
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 'false';
							 $app_amount = $in_data['AppliedAmount']-$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('TxnID'=>$in_data['TxnID'] );
							 
							 $condition_mail         = array('templateType'=>'15', 'merchantID'=>$user_id); 
							  $ref_number =  $in_data['RefNumber']; 
							  $tr_date   =date('Y-m-d h:i A');
							  	$toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
							  	
							 $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
							 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
						  
                       					 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
    					
    			
                     }
                     else
                     {
                         
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('trnsactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     
                 } 
                 
                  catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                 
                 
                $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], $echeck_payment, $this->transactionByUser, $custom_data_fields);  
                    	      
                if($res['trnsactionCode'] == 200 && $chh_mail =='1')
                {
                    $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
                    $ref_number =  $in_data['RefNumber']; 
                    $tr_date   =date('Y-m-d h:i A');
                    $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
                    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $res['transactionId']);
                }
		    }
		    
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }  

			if(!$checkPlan){
				$responseId  = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
		   
              if($cusproID=="2"){
			 	 redirect('company/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('company/home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
			 $trans_id = $response->transactionId;
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'company/home/invoices',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
					 'checkPlan'  => $checkPlan
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
	      
	}
	
	
	
	public function create_customer_sale()
	{ 
	      
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
		
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
         
	        $checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
	            $applySurcharge = false;
                if($this->czsecurity->xssCleanPostInput('invoice_id')){
                    $applySurcharge = true;
                }
        	    $custom_data_fields = [];
                // get custom field data
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                        else
                        $chh_mail =0;
        			  
        			if(!empty($gt_result) )
        			{
        			     $apiloginID      = $gt_result['gatewayUsername'];
        				 $secretApiKey   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName', 'FirstName', 'LastName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        			  
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						  
        					
        					}
        					$cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                            $custom_data_fields['payment_type'] = $friendlyname;

                             $config = new PorticoConfig();
                           
                              $config->secretApiKey = $secretApiKey;
                             if($this->config->item('Sandbox'))
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                else
                $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                 $config->developerId =  $this->config->item('DeveloperId');
                 $config->versionNumber =  $this->config->item('VersionNumber');
                      
                    
                          ServicesContainer::configureService($config);
                 
                    
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;

                            // add customer name
                            $firstName = isset($comp_data['FirstName']) ? $comp_data['FirstName'] : '';
                            $lastName = isset($comp_data['LastName']) ? $comp_data['LastName'] : '';
                            $cardHolderName = ($lastName) ? $firstName.' '.$lastName : $firstName;
                            $card->cardHolderName = $cardHolderName;

                            if($cvv!="")
                            $card->cvn = $cvv;
                             $card->cardType=$cardType;
                       
                            $address = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city = $city;
                            $address->state = $state;
                            $address->postalCode = $zipcode;
                            $address->country = $country;
                            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                                $amount += round($surchargeAmount, 2);
                                $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                                $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                                $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                            }
                            $totalamount  = $amount;
                            
                            $invNo  = mt_rand(1000000,2000000);

                            if($this->czsecurity->xssCleanPostInput('invoice_number')){
                                $invNo = $this->czsecurity->xssCleanPostInput('invoice_number');
                            }
                         	try
                            {
                             $response = $card->charge($amount)
                            ->withCurrency(CURRENCY)
                            ->withAddress($address)
                            ->withInvoiceNumber($invNo)
                            ->withAllowDuplicates(true)
                            ->execute();
        				  
            	          	$crtxnID='';
                           
            			  	 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                             {

                               
                                 $msg = $response->responseMessage;
                                 $trID = $response->transactionId;
                                 
                                // add level three data
                                $transaction = new Transaction();
                                $transaction->transactionReference = new TransactionReference();
                                $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                                $level_three_request = [
                                    'card_no' => $card_no,
                                    'amount' => $amount,
                                    'invoice_id' => $invNo,
                                    'merchID' => $user_id,
                                    'transaction_id' => $response->transactionId,
                                    'transaction' => $transaction,
                                    'levelCommercialData' => $levelCommercialData,
                                    'gateway' => 7
                                ];
                                addlevelThreeDataInTransaction($level_three_request);

                                 $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
                				 /* This block is created for saving Card info in encrypted form  */
            				  $refNumber=array();       
            				 $invoiceIDs = $invoicePayAmounts =array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                                $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));

        				     }
				     
				           if(!empty($invoiceIDs))
				           {
                                $payIndex = 0;
				              foreach($invoiceIDs as $inID)
				              {
        				            $theInvoice = array();
        							 
        							$theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
        					
        								
        								if(!empty($theInvoice) )
        								{
                                        
                                           $refNumber[]=$theInvoice['RefNumber'];

                                            $amount_data = $theInvoice['BalanceRemaining'];
                                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                                $actualInvoicePayAmount += $surchargeAmount;
                                                $amount_data += $surchargeAmount;

                                                $updatedInvoiceData = [
                                                    'inID' => $inID,
                                                    'merchantID' => $user_id,
                                                    'amount' => $surchargeAmount,
                                                ];
                                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,5);
                                            }
                                            $isPaid      = 'false';
                                            $BalanceRemaining = 0.00;
                                            $refnum[] = $theInvoice['RefNumber'];
                                            
                                            if($amount_data == $actualInvoicePayAmount){
                                                $actualInvoicePayAmount = $amount_data;
                                                $isPaid      = 'true';

                                            }else{

                                                $actualInvoicePayAmount = $actualInvoicePayAmount;
                                                $isPaid      = 'false';
                                                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                                
                                            }
                                            $txnAmount = $actualInvoicePayAmount;
                                            $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                            
                                            $tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));
            						    	
            							
            						         $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$txnAmount,$user_id,$crtxnID, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);  

        					        	}
								++$payIndex;

				              }
						 
				          }
				          else
				          {
				              
				                    $transactiondata= array();
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
				          }
				     
				            if($chh_mail =='1')
                             {
                              $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
                            
                              if(!empty($refNumber))
                              $ref_number = implode('',$refNumber); 
                              else
                              $ref_number ='';
                              $tr_date   =date('Y-m-d H:i:s');
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                              
                               $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $res['transactionId']);
                             }
                               
                                                           
                                if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
    					
    			
            				    
									 $this->session->set_flashdata('success', 'Transaction Successful');
            				   
            			
            				 } 
            				 else
            				 {
                                      $msg = $response->responseMessage;
                                      $trID = $response->transactionId;
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                                     
                              }
                         
                         
                         
                             }
                            catch (BuilderException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ConfigurationException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (GatewayException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (UnsupportedTransactionException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ApiException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
					$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => isset($response) ? $response->transactionId : '',
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'company/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('company/home/transation_sale_receipt',  'refresh');
	    
	    
    	}
	
	
	public function create_customer_auth()
	{ 
	      
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
			$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
            			     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   $contact ='';
        			  
        			if(!empty($gt_result) )
        			{
        			     $apiloginID      = $gt_result['gatewayUsername'];
        				 $secretApiKey   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				$comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'FirstName','LastName'), array('ListID' => $customerID));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1'   )
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						      
        					
        					}
        					$cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                            $custom_data_fields['payment_type'] = $friendlyname;
                             $config = new PorticoConfig();
                           
                              $config->secretApiKey = $secretApiKey;
                              if($this->config->item('Sandbox'))
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                else
                $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                 $config->developerId =  $this->config->item('DeveloperId');
                 $config->versionNumber =  $this->config->item('VersionNumber');
                    
                            ServicesContainer::configureService($config);
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;
                            if($cvv!="")
                            $card->cvn = $cvv;
                             $card->cardType=$cardType;
                            
                            // add customer name
                            $firstName = isset($comp_data['FirstName']) ? $comp_data['FirstName'] : '';
                            $lastName = isset($comp_data['LastName']) ? $comp_data['LastName'] : '';
                            $cardHolderName = ($lastName) ? $firstName.' '.$lastName : $firstName;
                            $card->cardHolderName = $cardHolderName;

                            $address = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city = $city;
                            $address->state = $state;
                            $address->postalCode = $zipcode;
                            $address->country = $country;
                              $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $invNo  =mt_rand(1000000,2000000);
                         	try
                            {
                                $level2CommercialData = new CommercialData(TaxType::NOT_USED, 'Level_II');
                                $level2CommercialData->taxType = 'SALESTAX';
	                            $level2CommercialData->taxAmount = '0.00';
                                $level2CommercialData->poNumber = $this->input->post('po_number', true);

                                 $response = $card->authorize($amount)
                                    ->withCurrency(CURRENCY)
                                    ->withAddress($address)
                                    ->withCommercialRequest(true)
                                    ->withCommercialData($level2CommercialData)
                                    ->withInvoiceNumber('TBD')
                                    ->withAllowDuplicates(true)
                                    ->execute();
        				  
        						
            	          	$crtxnID='';
            				
            			  	 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                             {
                                 $msg = $response->responseMessage;
                                 $trID = $response->transactionId;
                                 
                                  // add level three data
                                  $transaction = new Transaction();
                                  $transaction->transactionReference = new TransactionReference();
                                  $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                                  $level_three_request = [
                                      'card_no' => $card_no,
                                      'amount' => $amount,
                                      'invoice_id' => $invNo,
                                      'merchID' => $user_id,
                                      'transaction_id' => $response->transactionId,
                                      'transaction' => $transaction,
                                      'levelCommercialData' => $levelCommercialData,
                                      'gateway' => 7,
                                      'po_number' => $this->input->post('po_number', true)
                                  ];
                                  addlevelThreeDataInTransaction($level_three_request);
                                 $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
                				 /* This block is created for saving Card info in encrypted form  */
            				 
            			        	   if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
    					
    			
									$this->session->set_flashdata('success', 'Transaction Successful');
            				    
            				 } 
            				 else
            				 {
                                      $msg = $response->responseMessage;
                                      $trID = $response->transactionId;
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                     
                              }
                         
                         
                         
                            $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
          
                             }
                            catch (BuilderException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ConfigurationException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (GatewayException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (UnsupportedTransactionException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ApiException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					
					$receipt_data = array(
						'transaction_id' => isset($response) ? $response->transactionId : '',
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'company/Payments/create_customer_auth',
						'proccess_btn_text' => 'Process New Transaction',
						'sub_header' => 'Authorize',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('company/home/transation_sale_receipt',  'refresh');
                   
	    
	    
    	}
    	
   
    	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
        $custom_data_fields = [];
		if(!empty($this->input->post(null, true)))
		{
		    
		      	 if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
				
			     $tID     = $this->czsecurity->xssCleanPostInput('hearttxnvoidID');
			
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			 
    			 $gateway = $paydata['gatewayID'];
    			  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
				 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				 
        		if($tID!='' && !empty($gt_result))
        		{
        			  
        		    $secretApiKey  =  $gt_result['gatewayPassword'];
        		
    		      $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  if($this->config->item('Sandbox'))
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                else
                $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                 $config->developerId =  $this->config->item('DeveloperId');
                 $config->versionNumber =  $this->config->item('VersionNumber');
   
                $amount =  $paydata['transactionAmount'];
                ServicesContainer::configureService($config);
                
                $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
             	try
                {
                    $response= Transaction::fromId($tID)
                  ->capture($amount)
                   ->execute();
                    
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                         
                         
                         
						
                         
                         	$condition = array('transactionID'=>$tID);
                            $customerID = $paydata['customerListID'];
                            
            		    
				         	$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
				        	$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
                            if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
							}
							$this->session->set_flashdata('success', 'Successfully Void Authorize Transaction');
							 
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('trnsactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     
                     
                     $id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$paydata['customerListID'],$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                 } 
                    catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
			   
				       
		}
		else
		{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>'); 
				
				 }	
				redirect('company/Payments/payment_capture','refresh');
        }
              
		else
		{
		  redirect('company/Payments/payment_capture','refresh');  
		}
		  

	}
	
		/*****************Capture Transaction***************/
	


     	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true)))
		{
		      	 if($this->session->userdata('logged_in'))
			    {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
			
			
			     $tID     = $this->czsecurity->xssCleanPostInput('hearttxnID');
			   
			     
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
    			 $gateway = $paydata['gatewayID'];
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
				 
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				 
				 
        		if($tID!='' && !empty($gt_result))
        		{
        			  
        		    $secretApiKey  =  $gt_result['gatewayPassword'];
        		
    		      $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                   if($this->config->item('Sandbox'))
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                else
                $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                   $config->developerId =  $this->config->item('DeveloperId');
                 $config->versionNumber =  $this->config->item('VersionNumber');
   
                $amount =  $paydata['transactionAmount'];
                ServicesContainer::configureService($config);
                
                $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                
                
             	try
                {
                    $response= Transaction::fromId($tID)
                  ->capture($amount)
                   ->execute();
                    
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                         
                         	$condition = array('transactionID'=>$tID);
					
				         	$update_data =   array('transaction_user_status'=>"4");
					
				        	$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
				        	 if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
							}
							$condition_mail         = array('templateType'=>'15', 'merchantID'=>$merchantID); 
							$customerID = $paydata['customerListID'];
							$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
								$ref_number =  ''; 
								$tr_date   =date('Y-m-d h:i A');
								$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
						
							$this->session->set_flashdata('success', 'Successfully Captured Authorize Transaction');
                        	 
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     
                     
                     $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$paydata['customerListID'],$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                 } 
                    catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
			   
				       
		}
        else
        {
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>'); 
				
				 }	
				 $invoice_IDs = array();
		
			$receipt_data = array(
				'proccess_url' => 'company/Payments/payment_capture',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Capture',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
				}
				if($paydata['customerListID'] == ''){
					$paydata['customerListID'] ='null';
				}
				if($response->transactionId == ''){
					$response->transactionId ='null';
				}
			
			redirect('company/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$response->transactionId,  'refresh');		 
					
        }
      	else
		{
		  redirect('company/Payments/payment_capture','refresh');  
		} 

	}
	
	public function pay_multi_invoice()
	{
	  
	      
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      		
			$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
        
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                 $cusproID='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			$custom_data_fields = [];
			$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		    	 $cardID_upd ='';
			     
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
			     $amount               = $this->czsecurity->xssCleanPostInput('totalPay');
			     $customerID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			      $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
                 $invoiceIDs =  implode(',',$invoices); 
			     $inv_data   =    $this->customer_model->get_due_invoice_data($invoiceIDs,$user_id);
			     
			    if(!empty($invoices) && !empty( $inv_data)) 
			    {
                   $cusproID= $customerID  = $inv_data['Customer_ListID'];
    			     $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  
    			     $secretApiKey   = $gt_result['gatewayPassword'];
    			     
    			     if($cardID!="new1")
    			     {
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				         	$expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    			     }
    			     else
    			     {
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }   
                    $cardType = $this->general_model->getType($card_no);
                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                    $custom_data_fields['payment_type'] = $friendlyname;                           
        		$crtxnID='';	     
        	
                 $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                   if($this->config->item('Sandbox'))
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                else
                $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                 $config->developerId =  $this->config->item('DeveloperId');
                 $config->versionNumber =  $this->config->item('VersionNumber');
   
            
                ServicesContainer::configureService($config);
                $card = new CreditCardData();
                $card->number = $card_no;
                $card->expMonth = $expmonth;
                $card->expYear = $exyear;
                if($cvv!="")
                $card->cvn = $cvv;
                 $card->cardType=$cardType;
           
                $address = new Address();
                $address->streetAddress1 = $address1;
                $address->city = $city;
                $address->state = $state;
                $address->postalCode = $zipcode;
                $address->country = $country;
                
                
                $invNo  =mt_rand(1000000,2000000);
             	try
                {
                     $response = $card->charge($amount)
                    ->withCurrency(CURRENCY)
                    ->withAddress($address)
                    ->withInvoiceNumber($invNo)
                    ->withAllowDuplicates(true)
                    ->execute();
                    
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                        $msg = $response->responseMessage;
                        $trID = $response->transactionId;
                        
                        // add level three data
                        $transaction = new Transaction();
                        $transaction->transactionReference = new TransactionReference();
                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                        $level_three_request = [
                            'card_no' => $card_no,
                            'amount' => $amount,
                            'invoice_id' => $invNo,
                            'merchID' => $user_id,
                            'transaction_id' => $response->transactionId,
                            'transaction' => $transaction,
                            'levelCommercialData' => $levelCommercialData,
                            'gateway' => 7
                        ];
                        addlevelThreeDataInTransaction($level_three_request);

						$res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
						$this->session->set_flashdata('success', 'Successfully Processed Invoice');
                      
            			$c_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
            		
            			$companyID = $c_data['companyID'];
                            if(!empty($invoices))
                            {
        	                    foreach($invoices as $invoiceID)
        	                    {
        	                        $crtxnID='';
                                    $in_data =    $this->company_model->get_invoice_data_pay($invoiceID);
                                    if(!empty($in_data))
                                    {
                                        $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                        $txnID       = $invoiceID;  
            							$ispaid 	 = 'true';
            							
            							$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 'false';
            							 $app_amount = $in_data['AppliedAmount']-$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $condition  = array('TxnID'=>$in_data['TxnID'] );
            							 
        							    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
        							   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$pay_amounts,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser, $custom_data_fields);  
                                    } 
        	                    }
                            } 
                            
                            if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
    					
    							
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                         
                     }
                     
                     
                     
                   
                 } 
                catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    
		    }
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
			}
			
			if(!$checkPlan){
				$responseId  = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
			
		    	 if($cusproID!="")
				 {
					 redirect('company/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 redirect('company/home/invoices','refresh');
				 }
	      
	}

    public function create_customer_esale()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $secretApiKey  = $gt_result['gatewayPassword'];
                
                $comp_data     =$this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                $companyID  = $comp_data['companyID'];

                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code =     'WEB';

                if($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID' => $customerID,
                        'companyID'     => $companyID,
                        'merchantID'   => $merchantID,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code
                    ];

                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                    
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                try
                {
                    $config = new PorticoConfig();
               
                    $config->secretApiKey = $secretApiKey;
                    if($this->config->item('Sandbox')){
                        $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                    }
                    else{
                        $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                        $config->developerId =  $this->config->item('DeveloperId');
                        $config->versionNumber =  $this->config->item('VersionNumber');
                    }
       
                    ServicesContainer::configureService($config);
                    $check = new ECheck();
                    $check->accountNumber = $accountDetails['accountNumber'];
                    $check->routingNumber = $accountDetails['routeNumber'];
                    if(strtolower($accountDetails['accountType']) == 'checking'){
                        $check->accountType = 0;
                    }else{
                        $check->accountType = 1;
                    }

                    if(strtoupper($accountDetails['accountHolderType']) == 'PERSONAL'){
                        $check->checkType = 0;
                    }else{
                        $check->checkType = 1;
                    }
                    $check->checkHolderName = $accountDetails['accountName'];
                    $check->secCode = "WEB";
               
                    $address = new Address();
                    $address->streetAddress1 = $accountDetails['Billing_Addr1'];
                    $address->city = $accountDetails['Billing_City'];
                    $address->state = $accountDetails['Billing_State'];
                    $address->postalCode = $accountDetails['Billing_Zipcode'];
                    $address->country = $accountDetails['Billing_Country'];

                    $invNo = '';
                    if($this->czsecurity->xssCleanPostInput('invoice_number')){
                        $invNo = $this->czsecurity->xssCleanPostInput('invoice_number');
                    }
                    $response = $check->charge($amount)
                                    ->withCurrency(CURRENCY)
                                    ->withAddress($address)
                                    ->withInvoiceNumber($invNo)
                                    ->withAllowDuplicates(true)
                                    ->execute();

                    if($response->responseCode != 0 && $response->responseCode != '00')
                    {
                        $error='Gateway Error. Invalid Account Details';
                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
                        redirect('company/Payments/create_customer_esale');
                    }
                    $msg = $response->responseMessage;
                    $trID = $response->transactionId;
                    $res =array('transactionCode' => 200, 'status'=>$msg, 'transactionId'=> $trID );
                    if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                    {

                        $invoicePayAmounts = [];
                        $invoiceIDs = [];
                        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                            $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                            $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                        }
                        $refNum = array();
                        if (!empty($invoiceIDs)) {
                            $payIndex = 0;
                            foreach ($invoiceIDs as $inID) {
                                $theInvoice = array();

                                $theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
                                if (!empty($theInvoice)) {
                                    $amount_data = $theInvoice['BalanceRemaining'];
                                    $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                    $isPaid      = 'false';
                                    $BalanceRemaining = 0.00;
                                    $refnum[] = $theInvoice['RefNumber'];
                                    
                                    if($amount_data == $actualInvoicePayAmount){
                                        $actualInvoicePayAmount = $amount_data;
                                        $isPaid      = 'true';
                                    }else{
                                        $actualInvoicePayAmount = $actualInvoicePayAmount;
                                        $isPaid      = 'false';
                                        $BalanceRemaining = $amount_data - $actualInvoicePayAmount; 
                                    }
                                    $txnAmount = $actualInvoicePayAmount;
                                    $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                    
                                    $tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                    $transactiondata = array();
                                    $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID,'', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                                }
                                $payIndex++;
                            }
                        } else {

                            $transactiondata = array();
                            $inID = '';
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                        }

                        if ($this->czsecurity->xssCleanPostInput('tr_checked')){
                            $chh_mail = 1;
                        }else{
                            $chh_mail = 0;
                        }
                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                        
                        $ref_number = '';
                        $tr_date   = date('Y-m-d H:i:s');
                        $toEmail = $comp_data['Contact'];
                        $company = $comp_data['companyName'];
                        $customer = $comp_data['FullName'];

                        if($payableAccount == '' || $payableAccount == 'new1') {
                            $id1 = $this->card_model->process_ack_account($accountDetails);
                        }

                        $this->session->set_flashdata('success', ' Transaction Successful');
                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gatlistval, $gt_result['gatewayType'],$customerID,$amount,$merchantID,$trID, $this->resellerID,$inID='', true, $this->transactionByUser, $custom_data_fields);

                        if($chh_mail =='1')
                        {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $res['transactionId']);
                        }
                    }else{
                        $res['transactionCode'] = $response->responseCode;
                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gatlistval, $gt_result['gatewayType'],$customerID,$amount,$merchantID,$trID, $this->resellerID,$inID='', true, $this->transactionByUser, $custom_data_fields);
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $msg . '</div>');
                    }
                } catch (Exception $e)
                {
                    $error='Transaction Failed - ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    redirect('company/Payments/create_customer_esale');

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
                redirect('company/Payments/create_customer_esale');

            }

            $receipt_data = array(
                'transaction_id' => $response->transactionId,
                'IP_address' => getClientIpAddr(),
                'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
                'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact' => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url' => 'company/Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header' => 'Sale',
                'checkPlan' => $checkPlan
            );
            $this->session->set_userdata("receipt_data",$receipt_data);
            redirect('company/home/transation_sale_receipt',  'refresh');
        }
        redirect('company/Payments/create_customer_esale');
    }

	public function create_customer_refund()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');
            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');
            $user_id = $da['merchantID'];
        }
        if (!empty($this->input->post(null, true))) {
            $inputData = $this->input->post(null, true);
            $tID        = $this->czsecurity->xssCleanPostInput('txnID');
            if(empty($tID)){
                $tID  = $this->czsecurity->xssCleanPostInput('txnstrID');
            }

            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];

            $amount = $total = $paydata['transactionAmount'];
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($tID != '' && !empty($gt_result)) {
                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];
                
                if (!empty($paydata)) {
                    $customerID = $paydata['customerListID'];
                    
                    if(isset($inputData['ref_amount'])){
                        $total  = $this->czsecurity->xssCleanPostInput('ref_amount');
                        $amount = $total;
                    }
                    
                    if ($paydata['transactionCode'] == '200') {
                        $request_data = array("transaction_id" => $tID);
                        /******************This is for Invoice Refund Process***********/

                        if (!empty($paydata['invoiceTxnID'])) {
                            $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $paydata['merchantID']));
                            $user_id = $paydata['merchantID'];
                            $user    = $cusdata['qbwc_username'];
                            $comp_id = $cusdata['id'];

                            $ittem                  = $this->general_model->get_row_data('chargezoom_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                            $ins_data['customerID'] = $paydata['customerListID'];
                            if (empty($ittem)) {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
                                redirect('company/Payments/payment_transaction', 'refresh');

                            }

                            $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                            if (!empty($in_data)) {
                                $inv_pre    = $in_data['prefix'];
                                $inv_po     = $in_data['postfix'] + 1;
                                $new_inv_no = $inv_pre . $inv_po;

                            }
                            $ins_data['merchantDataID']    = $paydata['merchantID'];
                            $ins_data['creditDescription'] = "Credit as Refund";
                            $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                            $ins_data['creditDate']        = date('Y-m-d H:i:s');
                            $ins_data['creditAmount']      = $total;
                            $ins_data['creditNumber']      = $new_inv_no;
                            $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                            $ins_data['Type']              = "Payment";
                            $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                            $item['itemListID']      = $ittem['ListID'];
                            $item['itemDescription'] = $ittem['Name'];
                            $item['itemPrice']       = $total;
                            $item['itemQuantity']    = 0;
                            $item['crlineID']        = $ins_id;
                            $acc_name                = $ittem['DepositToAccountName'];
                            $acc_ID                  = $ittem['DepositToAccountRef'];
                            $method_ID               = $ittem['PaymentMethodRef'];
                            $method_name             = $ittem['PaymentMethodName'];
                            $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                            $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                            $refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount'          => $total,
                                'creditInvoiceID'                             => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
                                'creditTxnID'                                 => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
                                'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),
                                'paymentMethod'                               => $method_ID, 'paymentMethodName'                 => $method_name,
                                'AccountRef'                                  => $acc_ID, 'AccountName'                          => $acc_name,
                            );


                            if ($ins_id && $ins) {
                                $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

                            } else {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>');
                                redirect('company/Payments/payment_transaction', 'refresh');
                            }

                        }

                        /************End***************/
                        $merchantID = $user_id;
                        $config     = new PorticoConfig();

                        $config->secretApiKey = $apiKey;
                        $config->serviceUrl   = $this->config->item('GLOBAL_URL');

                        ServicesContainer::configureService($config);
                        $error = '';
                        try{

                            $response = Transaction::fromId($tID)
                                ->refund($amount)
                                ->withCurrency("USD")
                                ->execute();

                            if($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
                                $msg        = $response->responseMessage;
                                $tr1ID      = $response->transactionId;
                                $pay_status = 'SUCCESS';
                                
                                $res        = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $tr1ID);
                                
                                $this->customer_model->update_refund_payment($tID, 'Heartland');
        
                                if (!empty($paydata['invoiceTxnID'])) {
                                } else {
                                    $inv       = '';
                                    $ins_id    = '';
                                    $refnd_trr = array('merchantID' => $user_id, 'refundAmount' => $paydata['transactionAmount'],
                                        'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                                        'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
                                    );
                                }
                                $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
        
                                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
                            } else {
                                $msg   = $response->responseMessage;
                                $tr1ID = $response->transactionId;
                                $res   = array('trnsactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $tr1ID);
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>');
                            }
        
                            $id = $this->general_model->insert_gateway_transaction_data($res,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$paydata['transactionAmount'],$user_id,$crtxnID='', $this->resellerID,$paydata['invoiceTxnID']);
                        } catch (BuilderException $e) {
                            $error = 'Transaction Failed - ' . $e->getMessage();
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                        } catch (ConfigurationException $e) {
                            $error = 'Transaction Failed - ' . $e->getMessage();
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                        } catch (GatewayException $e) {
                            $error = 'Transaction Failed - ' . $e->getMessage();
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                        } catch (UnsupportedTransactionException $e) {
                            $error = 'Transaction Failed - ' . $e->getMessage();
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                        } catch (ApiException $e) {
                            $error = 'Transaction Failed - ' . $e->getMessage();
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                        }

                        if($error){
                            redirect('company/Payments/payment_refund');
                        }
                    }

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Invalid Transactions</strong>.</div>');
                }
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>');

            }
            $invoice_IDs = array();
            
            $receipt_data = array(
                'proccess_url'      => 'company/Payments/payment_refund',
                'proccess_btn_text' => 'Process New Refund',
                'sub_header'        => 'Refund',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if ($res['transactionId'] == '') {
                $res['transactionId'] = 'null';
            }
            redirect('company/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $res['transactionId'], 'refresh');
        }
        redirect('company/Payments/payment_capture', 'refresh');
    }
}