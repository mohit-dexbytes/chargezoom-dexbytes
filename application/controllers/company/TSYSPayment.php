<?php
/**
 * This Controller has TSYS Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for TSYS Payment Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * Perform Customer Card oprtations using this controller
 * Create, Delete, Modify
 */

class TSYSPayment extends CI_Controller
{
    private $resellerID;
    private $gatewayEnvironment;
    private $transactionByUser;

    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/TSYS.class.php';

        $this->load->model('company/customer_model', 'customer_model');
        $this->load->model('general_model');
        $this->load->model('company/company_model', 'company_model');
        $this->load->model('card_model');
        $this->load->config('TSYS');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '5') {

            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
                  
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }

        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {

        redirect('company/Payments/payment_transaction', 'refresh');
    }

    public function update_invoice_date()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('schedule_date'))) {
            $invoiceID = $this->czsecurity->xssCleanPostInput('scheduleID');
            $due_date  = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date')));

            $condition = array('TxnID' => $invoiceID);
            $indata    = $this->customer_model->get_invoice_data_byID($invoiceID);

            if (!empty($indata)) {
                $update_data = array('DueDate' => $due_date);
                $this->general_model->update_row_data('qb_test_invoice', $condition, $update_data);
                $ransn      = rand(200000, 700000);
                $insert_arr = array();

                $insert_arr['TxnID']                  = $indata['TxnID'];
                $insert_arr['EditSequence']           = $indata['EditSequence'];
                $insert_arr['RefNumber']              = $indata['RefNumber'];
                $insert_arr['TimeCreated']            = date('Y-m-d H:i:s', strtotime($indata['TimeCreated']));
                $insert_arr['TimeModified']           = date('Y-m-d H:i:s');
                $insert_arr['Customer_ListID']        = $indata['Customer_ListID'];
                $insert_arr['Customer_FullName']      = $indata['Customer_FullName'];
                $insert_arr['ShipAddress_Addr1']      = $indata['ShipAddress_Addr1'];
                $insert_arr['ShipAddress_Addr2']      = $indata['ShipAddress_Addr2'];
                $insert_arr['ShipAddress_City']       = $indata['ShipAddress_City'];
                $insert_arr['ShipAddress_State']      = $indata['ShipAddress_State'];
                $insert_arr['ShipAddress_Country']    = $indata['ShipAddress_Country'];
                $insert_arr['ShipAddress_PostalCode'] = $indata['ShipAddress_PostalCode'];
                $insert_arr['AppliedAmount']          = $indata['AppliedAmount'];
                $insert_arr['BalanceRemaining']       = $indata['BalanceRemaining'];

                $insert_arr['IsPaid'] = $indata['IsPaid'];

                $insert_arr['invoicelsID']      = $indata['TxnID'];
                $insert_arr['DueDate']          = $due_date;
                $insert_arr['emailRecurring']   = 0;
                $insert_arr['invoiceRefNumber'] = $indata['RefNumber'];
                $insert_arr['insertInvID']      = $ransn;
                $insert_arr['freeTrial']        = date('Y-m-d H:i:s');
                $insert_arr['gatewayID']        = 0;
                $insert_arr['autoPayment']      = 0;
                $insert_arr['cardID']           = 0;
                $insert_arr['qb_status']        = '1';
                $insert_arr['invoicelsID']      = $indata['TxnID'];

                $quer = $this->db->query("Select * from tbl_custom_invoice where  (invoicelsID='" . $indata['TxnID'] . "' or insertInvID='" . $indata['TxnID'] . "')   ");
                if ($quer->num_rows() > 0) {

                    $res = $quer->row_array();
                    if ($res['invoicelsID'] != '') {
                        $this->db->where(array('invoicelsID' => $indata['TxnID']));
                    } else {
                        $this->db->where(array('insertInvID' => $indata['TxnID']));
                    }

                    $this->db->update('tbl_custom_invoice', $insert_arr);

                } else {
                    $this->db->insert('tbl_custom_invoice', $insert_arr);
                }

                $user = $indata['company_qb_username'];
                $this->quickbooks->enqueue(QUICKBOOKS_MOD_INVOICE, $ransn, '1', '', $user);
                $this->session->set_flashdata('success', 'Successfully Updated');

                redirect('home/invoices', 'refresh');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid Invoice.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Any Date not Selected</div>');
        }
        redirect('home/invoices', 'refresh');
    }

    public function pay_invoice()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $cardID_upd = '';
        $invoiceID  = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID');
        if (!$cardID || empty($cardID)) {
            $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
        }

        $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
        }
        $gateway    = $gatlistval;
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

        $cusproID = '';
        $error    = '';
        $custom_data_fields = [];
        $cusproID = $this->czsecurity->xssCleanPostInput('customerProcessID');
        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }
        $checkPlan = check_free_plan_transactions();

        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

        if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

            
            $in_data         = $this->company_model->get_invoice_data_pay($invoiceID);
            $comp_data       = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
            $companyID       = $comp_data['companyID'];
            $Customer_ListID = $in_data['Customer_ListID'];
            $customerID      = $Customer_ListID;
            $c_data          = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
            $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $resultPay = $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;
            

            $in_data         = $this->company_model->get_invoice_data_pay($invoiceID);
            $Customer_ListID = $in_data['Customer_ListID'];
            $customerID      = $Customer_ListID;

            $c_data    = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
            $companyID = $c_data['companyID'];
            $txn_id    = $in_data['TxnID'];
            $responseType = 'SaleResponse';
            if (!empty($in_data)) {
                if ($in_data['BalanceRemaining'] > 0) {
                    if (!empty($cardID)) {
                        if ($sch_method == "1") {
                            $responseType = 'SaleResponse';
                            if ($cardID == 'new1') {
                                $cardID_upd = $cardID;
                                $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                                $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cvv        = $this->czsecurity->xssCleanPostInput('cvv');

                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('contact');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            } else {
                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $cvv       = $card_data['CardCVV'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];

                                $address1 = $card_data['Billing_Addr1'];
                                $address2 = $card_data['Billing_Addr2'];
                                $city     = $card_data['Billing_City'];
                                $zipcode  = $card_data['Billing_Zipcode'];
                                $state    = $card_data['Billing_State'];
                                $country  = $card_data['Billing_Country'];
                                $phone    = $card_data['Billing_Contact'];
                                
                            }
                            $cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount = $this->czsecurity->xssCleanPostInput('inv_amount');
                            $amount = $amount - $cr_amount;

                            $exyear1 = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . '/' . $exyear1;

                            $amount = round($amount,2);

                            $transaction['Sale'] = array(
                                "deviceID"                          => $deviceID,
                                "transactionKey"                    => $generateToken,
                                "cardDataSource"                    => "MANUAL",  
                                "transactionAmount"                 => (int)($amount * 100),
                                "currencyCode"                      => "USD",
                                "cardNumber"                        => $card_no,
                                "expirationDate"                    => $expry,
                                "cvv2"                              => $cvv,
                                "addressLine1"                      => ($address1 != '')?$address1:'None',
                                "zip"                               => ($zipcode != '')?$zipcode:'None',
                                "orderNumber"                       => $in_data['RefNumber'],
                                "notifyEmailID"                     => (($comp_data['Contact'] != ''))?$comp_data['Contact']:'chargezoom@chargezoom.com',
                                "firstName"                         => (($comp_data['FirstName'] != ''))?$comp_data['FirstName']:'None',
                                "lastName"                          => (isset($comp_data['LastName']))?$comp_data['LastName']:'None',
                               
                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                "terminalOutputCapability"          => "DISPLAY_ONLY",
                                "maxPinLength"                      => "UNKNOWN",
                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                "cardPresentDetail"                 => "CARD_PRESENT",
                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                "cardholderAuthenticationEntity"    => "OTHER",
                                "cardDataOutputCapability"          => "NONE",

                                "customerDetails"   => array( 
                                            "contactDetails" => array(
                                               
                                                "addressLine1"=> ($address1 != '')?$address1:'None',
                                                 "addressLine2"  => ($address2 != '')?$address2:'None',
                                                "city"=>($city != '')?$city:'None',
                                             
                                                "zip"=>($zipcode != '')?$zipcode:'None',
                                               
                                            ),
                                            "shippingDetails" => array( 
                                                "firstName"=>(isset($comp_data['FirstName']))?$comp_data['FirstName']:'None',
                                                "lastName"=>(isset($comp_data['LastName']))?$comp_data['LastName']:'None',
                                                "addressLine1"=>($address1 != '')?$address1:'None',
                                                 "addressLine2" => ($address2 != '')?$address2:'None',
                                                "city"=>($city != '')?$city:'None',
                                               
                                                "zip"=>($zipcode != '')?$zipcode:'None',
                                              
                                                "emailID"=>(isset($comp_data['Contact']))?$comp_data['Contact']:'chargezoom@chargezoom.com'
                                             )
                                        )
                            );
                            if($cvv == ''){
                                unset($transaction['Sale']['cvv2']);
                            }
                            $crtxnID = '';
                            $txn_run = 1;
                        } else if ($sch_method == "2") {
                            $responseType = 'AchResponse';
                            if ($cardID == 'new1') {
                                $cardID_upd          = $cardID;
                                $account_name        = $this->czsecurity->xssCleanPostInput('acc_name');
                                $account_number      = $this->czsecurity->xssCleanPostInput('acc_number');
                                $route_number        = $this->czsecurity->xssCleanPostInput('route_number');
                                $account_type        = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                                $account_holder_type = $this->czsecurity->xssCleanPostInput('acct_type');

                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('contact');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            } else {
                                $card_data           = $this->card_model->get_single_card_data($cardID);
                                $account_name        = $card_data['accountName'];
                                $account_number      = $card_data['accountNumber'];
                                $route_number        = $card_data['routeNumber'];
                                $account_type        = $card_data['accountHolderType'];
                                $account_holder_type = $card_data['accountType'];

                                $address1 = $card_data['Billing_Addr1'];
                                $address2 = $card_data['Billing_Addr2'];
                                $city     = $card_data['Billing_City'];
                                $zipcode  = $card_data['Billing_Zipcode'];
                                $state    = $card_data['Billing_State'];
                                $country  = $card_data['Billing_Country'];
                                $phone    = $card_data['Billing_Contact'];
                                $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                            }
                            $accountNumber = $account_number;
                            $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount = $this->czsecurity->xssCleanPostInput('inv_amount');
                            $amount = $amount - $cr_amount;

                            $transaction['Ach'] = array(
                                "deviceID"              => $deviceID,
                                "transactionKey"        => $generateToken,
                                "transactionAmount"     => $amount * 100,
                                "accountDetails"        => array(
                                        "routingNumber" => $route_number,
                                        "accountNumber" => $account_number,
                                        "accountType"   => strtoupper($account_holder_type),
                                        "accountNotes"  => "count",
                                        "addressLine1"  => ($address1 != '')?$address1:'None',
                                        "addressLine2"  => ($address2 != '')?$address2:'None',
                                        "zip"           => ($zipcode != '')?$zipcode:'None',
                                        "city"          => ($city != '')?$city:'None',
                                        "state"         => ($state != '')?$state:'AZ',
                                        "country"       => ($country != '')?$country:'USA'
                                ),
                                "achSecCode"                => "WEB",
                                "originateDate"             => date('Y-m-d'),
                                "addenda"                   => "addenda",
                                "firstName"                 => $comp_data['FirstName'],
                                "lastName"                  => $comp_data['LastName'],
                                "customerPhone"             => ($phone != '')?$phone:'None',
                                "addressLine1"              => ($address1 != '')?$address1:'None',
                                "addressLine2"              => ($address2 != '')?$address2:'None',
                                 "zip"                      => ($zipcode != '')?$zipcode:'None',
                                "city"                      => ($city != '')?$city:'None',
                                "state"                     => ($state != '')?$state:'AZ',
                                "country"                   => ($country != '')?$country:'USA',
                                "dob"                       => "1967-08-13",
                                "ssn"                       => "1967-08-13",
                                "driverLicenseNumber"       => "101",
                                "driverLicenseIssuedState"  => ($state != '')?$state :'AZ',
                                "developerID"               => "1234"    
                            );

                            $crtxnID = '';
                            $txn_run = 1;
                        } else {
                            $txn_run = 0;
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Select Payment Type</strong>.</div>');
                        }
                        if ($txn_run == 1) {
                            
                            if($generateToken != ''){
                                $resultPay = $gatewayTransaction->processTransaction($transaction);
                            }else{
                                $responseType = 'GenerateKeyResponse';
                            }
                            
                            if (isset($resultPay[$responseType]['status']) && $resultPay[$responseType]['status'] == 'PASS') {
                                $txnID  = $in_data['TxnID'];
                                $ispaid = 'true';

                                $bamount = $in_data['BalanceRemaining'] - $amount;
                                if ($bamount > 0) {
                                    $ispaid = 'false';
                                }

                                $app_amount = $in_data['AppliedAmount'] - $amount;
                                $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
                                $condition  = array('TxnID' => $in_data['TxnID']);

                                $this->general_model->update_row_data('chargezoom_test_invoice', $condition, $data);

                                if ($cardID == "new1") {
                                    if ($sch_method == "1") {

                                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                        $cardType = $this->general_model->getType($card_no);

                                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                        $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                        $card_data = array(
                                            'cardMonth'       => $expmonth,
                                            'cardYear'        => $exyear,
                                            'CardType'        => $cardType,
                                            'CustomerCard'    => $card_no,
                                            'CardCVV'         => $cvv,
                                            'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                            'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                            'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                            'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                            'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                                            'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                            'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                            'customerListID'  => $customerID,

                                            'companyID'       => $companyID,
                                            'merchantID'      => $user_id,
                                            'createdAt'       => date("Y-m-d H:i:s"),
                                        );

                                        $id1 = $this->card_model->process_card($card_data);
                                    } else if ($sch_method == "2") {
                                        $accountDetails = [
                                            'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                            'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                            'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                            'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                            'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                            'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                            'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                            'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                            'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                            'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                                            'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                            'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                            'customerListID'     => $customerID,
                                            'companyID'          => $companyID,
                                            'merchantID'         => $user_id,
                                            'createdAt'          => date("Y-m-d H:i:s"),
                                            'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                                        ];
                                        $id1 = $this->card_model->process_ack_account($accountDetails);
                                    }
                                }

                                
                                $condition_mail = array('templateType' => '15', 'merchantID' => $user_id);
                                $ref_number     = $in_data['RefNumber'];
                                $tr_date        = date('Y-m-d H:i:s');
                                $toEmail        = $c_data['Contact'];
                                $company        = $c_data['companyName'];
                                $customer       = $c_data['FullName'];
                                $this->session->set_flashdata('success', ' Successfully Processed Invoice');

                                $txn_id        = $resultPay[$responseType]['transactionID'];
                                $response_code = 100;
                            } else {

                                $response_code = 0;

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - ' . $resultPay[$responseType]['responseMessage'] . '</strong></div>');
                            }
                            $resultPay['responseType'] = $responseType;
                            $id = $this->general_model->insert_gateway_transaction_data($resultPay, 'sale', $gateway, $gt_result['gatewayType'], $in_data['Customer_ListID'], $amount, $user_id, $crtxnID, $this->resellerID, $in_data['TxnID'], false, $this->transactionByUser, $custom_data_fields);

                            if ((isset($resultPay[$responseType]['status']) && $resultPay[$responseType]['status'] == 'PASS') && $chh_mail == '1') {
                                $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                                $ref_number     = $in_data['RefNumber'];
                                $tr_date        = date('Y-m-d H:i:s');
                                $toEmail        = $c_data['Contact'];
                                $company        = $c_data['companyName'];
                                $customer       = $c_data['FullName'];
                                $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $txn_id);
                            }
                            
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Customer has no card</strong>.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Not valid </strong>.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Select Gateway</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($cusproID == "2") {
            redirect('company/home/view_customer/' . $customerID, 'refresh');
        }
        if ($cusproID == "3" && $in_data['TxnID'] != '') {
            redirect('company/home/invoice_details/' . $in_data['TxnID'], 'refresh');
        }
        $trans_id     = $txn_id; 
        $invoice_IDs  = array();
        $receipt_data = array(
            'proccess_url'      => 'company/home/invoices',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         =>  $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        if ($cusproID == "1") {
            redirect('company/home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
        }
        redirect('company/home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
    }

    public function insert_new_data()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $customer  = $this->czsecurity->xssCleanPostInput('customerID11');
        $c_data    = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customer, 'qbmerchantID' => $merchID));
        $companyID = $c_data['companyID'];

        if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
            $card_no  = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('card_number'));
            $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
            $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
            $cvv      = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cvv'));
            $acc_number   = $route_number   = $acc_name   = $secCode   = $acct_type   = $acct_holder_type   = '';
            $type         = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
            $friendlyname = $type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
        }

        if ($this->czsecurity->xssCleanPostInput('formselector') == '2') {
            $acc_number       = $this->czsecurity->xssCleanPostInput('acc_number');
            $route_number     = $this->czsecurity->xssCleanPostInput('route_number');
            $acc_name         = $this->czsecurity->xssCleanPostInput('acc_name');
            $secCode          = $this->czsecurity->xssCleanPostInput('secCode');
            $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
            $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
            $card_no          = $expmonth          = $exyear          = $cvv          = '00';
            $type             = 'Echeck';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
        }
        $b_addr1   = $this->czsecurity->xssCleanPostInput('address1');
        $b_addr2   = $this->czsecurity->xssCleanPostInput('address2');
        $b_city    = $this->czsecurity->xssCleanPostInput('city');
        $b_state   = $this->czsecurity->xssCleanPostInput('state');
        $b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
        $b_country = $this->czsecurity->xssCleanPostInput('country');
        $b_contact = $this->czsecurity->xssCleanPostInput('contact');

        $insert_array = array(
            'cardMonth'                => $expmonth,
            'cardYear'                 => $exyear,
            'CustomerCard'             => $card_no,
            'CardCVV'                  => $cvv,
            'CardType'                 => $type,
            'customerListID'           => $customer,
            'merchantID'               => $merchID,
            'companyID'                => $companyID,
            'Billing_Addr1'            => $b_addr1,
            'Billing_Addr2'            => $b_addr2,
            'Billing_City'             => $b_city,
            'Billing_State'            => $b_state,
            'Billing_Zipcode'          => $b_zipcode,
            'Billing_Country'          => $b_country,
            'Billing_Contact'          => $b_contact,

            'customerCardfriendlyName' => $friendlyname,
            'accountNumber'            => $acc_number,
            'routeNumber'              => $route_number,
            'accountName'              => $acc_name,
            'accountType'              => $acct_type,
            'accountHolderType'        => $acct_holder_type,
            'secCodeEntryMethod'       => $secCode,
            'createdAt'                => date("Y-m-d H:i:s"),
        );

        $id = $this->card_model->insert_card_data($insert_array);
        if ($id) {
            $this->session->set_flashdata('success', 'Successfully Inserted');

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }

        redirect('company/home/view_customer/' . $customer, 'refresh');
    }

    public function update_card_data()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');

        $qq = $this->db1->select('customerListID')->from('customer_card_data')->where(array('CardID' => $cardID, 'merchantID' => $merchID))->get();
        if ($qq->num_rows > 0) {
            $customer = $qq->row_array()['customerListID'];
        }
        if ($this->czsecurity->xssCleanPostInput('edit_expiry') != '') {

            $expmonth     = $this->czsecurity->xssCleanPostInput('edit_expiry');
            $exyear       = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
            $cvv          = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('edit_cvv'));
            $friendlyname = '';
            $acc_number   = $route_number   = $acc_name   = $secCode   = $acct_type   = $acct_holder_type   = '';
        }

        if ($this->czsecurity->xssCleanPostInput('edit_acc_number') !== '') {
            $acc_number       = $this->czsecurity->xssCleanPostInput('edit_acc_number');
            $route_number     = $this->czsecurity->xssCleanPostInput('edit_route_number');
            $acc_name         = $this->czsecurity->xssCleanPostInput('edit_acc_name');
            $secCode          = $this->czsecurity->xssCleanPostInput('edit_secCode');
            $acct_type        = $this->czsecurity->xssCleanPostInput('edit_acct_type');
            $acct_holder_type = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
            $card_no          = $expmonth          = $exyear          = $cvv          = '00';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
        }

        $b_addr1   = $this->czsecurity->xssCleanPostInput('baddress1');
        $b_addr2   = $this->czsecurity->xssCleanPostInput('baddress2');
        $b_city    = $this->czsecurity->xssCleanPostInput('bcity');
        $b_state   = $this->czsecurity->xssCleanPostInput('bstate');
        $b_country = $this->czsecurity->xssCleanPostInput('bcountry');
        $b_contact = $this->czsecurity->xssCleanPostInput('bcontact');
        $b_zip     = $this->czsecurity->xssCleanPostInput('bzipcode');

        $condition    = array('CardID' => $cardID);
        $insert_array = array(
            'cardMonth'                => $expmonth,
            'cardYear'                 => $exyear,
            'CardCVV'                  => $cvv,

            'customerCardfriendlyName' => $friendlyname,

            'Billing_Addr1'            => $b_addr1,
            'Billing_Addr2'            => $b_addr2,
            'Billing_City'             => $b_city,
            'Billing_State'            => $b_state,
            'Billing_Zipcode'          => $b_zip,
            'Billing_Country'          => $b_country,
            'Billing_Contact'          => $b_contact,
            'accountNumber'            => $acc_number,
            'routeNumber'              => $route_number,
            'accountName'              => $acc_name,
            'accountType'              => $acct_type,
            'accountHolderType'        => $acct_holder_type,
            'secCodeEntryMethod'       => $secCode,

            'updatedAt'                => date("Y-m-d H:i:s"),
        );

        if ($this->czsecurity->xssCleanPostInput('edit_card_number') != '') {
            $card_no                                  = $this->czsecurity->xssCleanPostInput('edit_card_number');
            $type                                     = $this->general_model->getType($card_no);
            $friendlyname                             = $type . ' - ' . substr($card_no, -4);
            $insert_array['customerCardfriendlyName'] = $friendlyname;

            $insert_array['CustomerCard'] = $this->card_model->encrypt($card_no);
            $insert_array['CardType']     = $type;
        }

        $id = $this->card_model->update_card_data($condition, $insert_array);
        if ($id) {
            $this->session->set_flashdata('success', 'Successfully Updated');

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }

        redirect('company/home/view_customer/' . $customer, 'refresh');
    }

    public function delete_card_data()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        if (!empty($this->czsecurity->xssCleanPostInput('delCardID'))) {

            $cardID   = $this->czsecurity->xssCleanPostInput('delCardID');
            $customer = $this->czsecurity->xssCleanPostInput('delCustodID');

            $num = $this->general_model->get_num_rows('tbl_chargezoom_subscriptions', array('CardID' => $cardID, 'merchantDataID' => $merchID));
            if ($num == 0) {
                $sts = $this->card_model->delete_card_data(array('CardID' => $cardID));
                if ($sts) {
                    $this->session->set_flashdata('success', 'Successfully Deleted');

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Transaction Failed - This card cannot be deleted. Please change Customer card on Subscription</strong></div>');
            }
            redirect('company/home/view_customer/' . $customer, 'refresh');
        }
    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {


            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $plantype = $this->general_model->chk_merch_plantype_status($user_id);

        if (!empty($this->input->post(null, true))) {

            $custom_data_fields = [];
            $applySurcharge = false;
            if($this->czsecurity->xssCleanPostInput('invoice_id')){
                $applySurcharge = true;
            }
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $merchant_condition = [
                'merchID' => $user_id,
            ];

            $inputData = $this->input->post(null, true);

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$totalamount  = $this->czsecurity->xssCleanPostInput('totalamount');
			$amount = $this->czsecurity->xssCleanPostInput('totalamount');
            $checkPlan = check_free_plan_transactions();

            $inv_array   = array();
            $inv_invoice = array();
            $invoiceIDs  = array();
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                if ($this->session->userdata('logged_in')) {
                    $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                $companyID = $comp_data['companyID'];

                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            
                $generateToken = '';
                $responseErrorMsg = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;

                $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear  = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1 = substr($exyear, 2);
                    $expry   = $expmonth .'/'. $exyear1;
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);

                    $card_no  = $card_data['CardNo'];
                    $expmonth = $card_data['cardMonth'];
                    $exyear   = $card_data['cardYear'];
                    $exyear1  = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth .'/'. $exyear1;
                    $cvv   = $card_data['CardCVV'];
                }
                /*Added card type in transaction table*/
                $card_type = $this->general_model->getType($card_no);
                $friendlyname = $card_type . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                $orderId = time();
                if($this->czsecurity->xssCleanPostInput('invoice_number')){
                    $orderId = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                $address1 = ($this->czsecurity->xssCleanPostInput('baddress1') != '')?$this->czsecurity->xssCleanPostInput('baddress1'):'None';
                $address2 = ($this->czsecurity->xssCleanPostInput('baddress1') != '')?$this->czsecurity->xssCleanPostInput('baddress1'):'None';
                $zipcode = ($this->czsecurity->xssCleanPostInput('bzipcode') != '')?$this->czsecurity->xssCleanPostInput('bzipcode'):'None';
                $city = ($this->czsecurity->xssCleanPostInput('bcity') != '')?$this->czsecurity->xssCleanPostInput('bcity'):'None';
                $state = ($this->czsecurity->xssCleanPostInput('bstate') != '')?$this->czsecurity->xssCleanPostInput('bstate'):'AZ';
                $country = ($this->czsecurity->xssCleanPostInput('bcountry') != '')?$this->czsecurity->xssCleanPostInput('bcountry'):'USA';
                $phone = ($this->czsecurity->xssCleanPostInput('phone') != '')?$this->czsecurity->xssCleanPostInput('phone'):'None';
                $firstName = ($this->czsecurity->xssCleanPostInput('firstName') != '')?$this->czsecurity->xssCleanPostInput('firstName'):'None';
                $lastName = ($this->czsecurity->xssCleanPostInput('lastName') != '')?$this->czsecurity->xssCleanPostInput('lastName'):'None';
                $companyName = ($this->czsecurity->xssCleanPostInput('companyName') != '')?$this->czsecurity->xssCleanPostInput('companyName'):'None';

                $email = ($this->czsecurity->xssCleanPostInput('email') != '')?$this->czsecurity->xssCleanPostInput('email'):'chargezoom@chargezoom.com';

                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;

                $amount = round($amount,2);

                $transaction['Sale'] = array(
                    "deviceID"                          => $deviceID,
                    "transactionKey"                    => $generateToken,
                    "cardDataSource"                    => "MANUAL",  
                    "transactionAmount"                 => (int)($amount * 100),
                    "currencyCode"                      => "USD",
                    "cardNumber"                        => $card_no,
                    "expirationDate"                    => $expry,
                    "cvv2"                              => $cvv,
                    "addressLine1"                      => $address1,
                    "zip"                               => $zipcode,
                    "orderNumber"                       => "$orderId",
                    "notifyEmailID"                     => $email ,
                    "firstName"                         => $firstName,
                    "lastName"                          => $lastName,
                    "purchaseOrder" => $this->czsecurity->xssCleanPostInput('po_number'),
                    "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                    "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                    "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                    "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                    "terminalOutputCapability"          => "DISPLAY_ONLY",
                    "maxPinLength"                      => "UNKNOWN",
                    "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                    "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                    "cardPresentDetail"                 => "CARD_PRESENT",
                    "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                    "cardholderAuthenticationEntity"    => "OTHER",
                    "cardDataOutputCapability"          => "NONE",

                    "customerDetails"   => array( 
                            "contactDetails" => array(
                                "addressLine1"=> $address1,
                                "addressLine2" => $address2,
                                "city"=>$city,
                                "zip"=>$zipcode,
                            ),
                            "shippingDetails" => array( 
                                "firstName"=> $firstName,
                                "lastName"=> $lastName,
                                "addressLine1"=> ($inputData['address1'] != '')?$inputData['address1']:'None',
                                "addressLine2"                      => ($inputData['address2'] != '')?$inputData['address2']:'None',
                                "city"=>(($inputData['city']) != '')?$inputData['city']:'None',
                                "zip"=>($inputData['zipcode'] != '')?$inputData['zipcode']:'None',
                                "emailID"=> $email
                             )
                        )
                );
                if($cvv == ''){
                    unset($transaction['Sale']['cvv2']);
                }
                
                $responseType = 'SaleResponse';
                $crtxnID = '';
                $invID   = '';
                if($generateToken != ''){
                    $result = $gatewayTransaction->processTransaction($transaction);
                }else{
                    $responseType = 'GenerateKeyResponse';
                }
				
				$result['responseType'] = $responseType;
				if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
				 	$invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }
                    $refNum = array();
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

                            if (!empty($theInvoice)) {
                                $amount_data = $theInvoice['BalanceRemaining'];
                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                    $actualInvoicePayAmount += $surchargeAmount;
                                    $amount_data += $surchargeAmount;

                                    $updatedInvoiceData = [
                                        'inID' => $inID,
                                        'merchantID' => $user_id,
                                        'amount' => $surchargeAmount,
                                    ];
                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,5);
                                }
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                $refnum[] = $theInvoice['RefNumber'];
                                
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 'true';

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 'false';
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $txnAmount = $actualInvoicePayAmount;
                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                
                                $tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                $transactiondata = array();
                                
                                $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                
                            }
                            ++$payIndex;
                        }
                    } else {

                        $transactiondata = array();
                        $inID            = '';
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $totalamount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                    }

                    $txn_id        = $result[$responseType]['transactionID'];

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no = $this->czsecurity->xssCleanPostInput('card_number');

                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address1,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $phone,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }

                    $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user_id));
                    $fromEmail     = $merchant_data['merchantEmail'];
                    

                    if ($chh_mail == '1') {
                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);

                        if (!empty($refNum)) {
                            $ref_number = implode(',', $refNum);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $comp_data['Contact'];
                        $company  = $comp_data['companyName'];
                        $customer = $comp_data['FullName'];
                        
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $txn_id);
                    }
                    $condition_mail = array('templateType' => '15', 'merchantID' => $user_id);

                    if (!empty($refNum)) {
                        $ref_number = implode(',', $refNum);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['Contact'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['FullName'];
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
					
                    $transactiondata = array();
                    $inID            = '';
                    $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $totalamount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);

                    $err_msg = $result[$responseType]['responseMessage'];
                    if($responseErrorMsg != ''){
                        $err_msg = $responseErrorMsg;
                    }

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>');
            }
            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => (isset($result[$responseType]['transactionID'])) ? $result[$responseType]['transactionID'] : 'TXNFail-'.time(),
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'company/Payments/create_customer_sale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         =>  $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('company/home/transation_sale_receipt', 'refresh');

        }
        redirect('company/Payments/create_customer_sale', 'refresh');
    }

    /*****************Authorize Transaction***************/

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $responseId = '';

        $merchantID = $user_id;
        $amount = $this->czsecurity->xssCleanPostInput('totalamount');

        $inputData = $this->input->post(null, true);
        $plantype = $this->general_model->chk_merch_plantype_status($user_id);
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data  = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customerID));
                $companyID  = $comp_data['companyID'];

                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                $responseErrorMsg = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;

                $cardID      = $this->czsecurity->xssCleanPostInput('card_list');
                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear  = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1 = substr($exyear, 2);
                    $expry   = $expmonth.'/'.$exyear1;
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $expmonth = $card_data['cardMonth'];
                    $card_no  = $card_data['CardNo'];

                    $exyear  = $card_data['cardYear'];
                    $exyear1 = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth.'/'.$exyear1;
                    $cvv   = $card_data['CardCVV'];
                    
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                $custom_data_fields['payment_type'] = $friendlyname;
                $orderId = time();

                $transaction['Auth'] = array(
                    "deviceID"                          => $deviceID,
                    "transactionKey"                    => $generateToken,
                    "cardDataSource"                    => "MANUAL",  
                    "transactionAmount"                 => $amount * 100,
                    "currencyCode"                      => "USD",
                    "cardNumber"                        => $card_no,
                    "expirationDate"                    => $expry,
                    "cvv2"                              => $cvv,
                    "addressLine1"                      => ($inputData['baddress1'] != '')?$inputData['baddress1']:'None',
                    "zip"                               => ($inputData['bzipcode'] != '')?$inputData['bzipcode']:'None',
                    "orderNumber"                       => "$orderId",
                    "notifyEmailID"                     => (($inputData['email'] != ''))?$inputData['email']:'chargezoom@chargezoom.com',
                    "firstName"                         => (($inputData['firstName'] != ''))?$inputData['firstName']:'None',
                    "lastName"                          => (($inputData['lastName'] != ''))?$inputData['lastName']:'None',
                    "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                    "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                    "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                    "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                    "terminalOutputCapability"          => "DISPLAY_ONLY",
                    "maxPinLength"                      => "UNKNOWN",
                    "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                    "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                    "cardPresentDetail"                 => "CARD_PRESENT",
                    "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                    "cardholderAuthenticationEntity"    => "OTHER",
                    "cardDataOutputCapability"          => "NONE",

                    "customerDetails"   => array( 
                            "contactDetails" => array(
                                "addressLine1"=> ($inputData['baddress1'] != '')?$inputData['baddress1']:'None',
                                 "addressLine2"                      => ($inputData['baddress2'] != '')?$inputData['baddress2']:'None',
                                "city"=>(($inputData['bcity'] != ''))?$inputData['bcity']:'None',
                                "zip"=>($inputData['bzipcode'] != '')?$inputData['bzipcode']:'None',
                            ),
                            "shippingDetails" => array( 
                                "firstName"=> (($inputData['firstName'] != ''))?$inputData['firstName']:'None',
                                "lastName"=> (($inputData['lastName'] != ''))?$inputData['lastName']:'None',
                                "addressLine1"=>($inputData['address1'] != '')?$inputData['address1']:'None',
                                "addressLine2"                      => ($inputData['address2'] != '')?$inputData['address2']:'None',
                                "city"=>(($inputData['city'] != ''))?$inputData['city']:'None',
                                "zip"=>($inputData['zipcode'] != '')?$inputData['zipcode']:'None',
                                "emailID"=>(($inputData['email'] != ''))?$inputData['email']:'chargezoom@chargezoom.com'
                             )
                        )
                );
                $responseType = 'AuthResponse';

                if($cvv == ''){
                    unset($transaction['Auth']['cvv2']);
                }
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $transaction['Auth']['orderNumber'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                if (!empty($po_number)) {
                    $transaction['Auth']['purchaseOrder'] = $po_number;
                }
                $crtxnID = '';
                $invID =$inID = $responseId  = 'TXNFail-'.time();
                if($generateToken != ''){
                    $result = $gatewayTransaction->processTransaction($transaction);
                }else{
                    $responseType = 'GenerateKeyResponse';
                }
                
                
                if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                    $responseId = $result[$responseType]['transactionID'];
                    /* This block is created for saving Card info in encrypted form  */

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $inputData['address1'],
                            'Billing_Addr2'   => $inputData['address2'],
                            'Billing_City'    => $inputData['bcity'],
                            'Billing_State'   => $inputData['bstate'],
                            'Billing_Country' => $inputData['bcountry'],
                            'Billing_Contact' => $inputData['phone'],
                            'Billing_Zipcode' => $inputData['bzipcode'],
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $transactiondata = array();
                    $err_msg = $result[$responseType]['responseMessage'];
                    if($responseErrorMsg != ''){
                        $err_msg = $responseErrorMsg;
                    }

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>');
            }
            $result['responseType'] = $responseType;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);

            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'company/Payments/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         =>  $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('company/home/transation_sale_receipt', 'refresh');
        }
        redirect('company/Payments/create_customer_auth', 'refresh');
    }

    /*****************Refund Transaction***************/

    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->czsecurity->xssCleanPostInput('txnID'))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $total = $this->czsecurity->xssCleanPostInput('ref_amount');

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($tID != '' && !empty($gt_result)) {

                if (!empty($paydata['invoiceTxnID'])) {
                    $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $paydata['merchantID']));
                    $user_id = $paydata['merchantID'];
                    $user    = $cusdata['qbwc_username'];
                    $comp_id = $cusdata['id'];
                    $ittem   = $this->general_model->get_row_data('chargezoom_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));

                    if (empty($ittem)) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
                        redirect('Payments/payment_transaction', 'refresh');
                    }
                    $ins_data['customerID'] = $paydata['customerListID'];

                    $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                    if (!empty($in_data)) {
                        $inv_pre    = $in_data['prefix'];
                        $inv_po     = $in_data['postfix'] + 1;
                        $new_inv_no = $inv_pre . $inv_po;
                    }
                    $ins_data['merchantDataID']    = $paydata['merchantID'];
                    $ins_data['creditDescription'] = "Credit as Refund";
                    $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                    $ins_data['creditDate']        = date('Y-m-d H:i:s');
                    $ins_data['creditAmount']      = $total;
                    $ins_data['creditNumber']      = $new_inv_no;
                    $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                    $ins_data['Type']              = "Payment";
                    $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                    $item['itemListID']      = $ittem['ListID'];
                    $item['itemDescription'] = $ittem['Name'];
                    $item['itemPrice']       = $total;
                    $item['itemQuantity']    = 0;
                    $item['crlineID']        = $ins_id;
                    $acc_name                = $ittem['DepositToAccountName'];
                    $acc_ID                  = $ittem['DepositToAccountRef'];
                    $method_ID               = $ittem['PaymentMethodRef'];
                    $method_name             = $ittem['PaymentMethodName'];
                    $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                    $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                    $refnd_trr               = array(
                        'merchantID'      => $paydata['merchantID'], 'refundAmount'          => $total,
                        'creditInvoiceID' => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
                        'creditTxnID'     => $ins_id, 'refundCustomerID'                     => $paydata['customerListID'],
                        'createdAt'       => date('Y-m-d H:i:s'), 'updatedAt'                => date('Y-m-d H:i:s'),
                        'paymentMethod'   => $method_ID, 'paymentMethodName'                 => $method_name,
                        'AccountRef'      => $acc_ID, 'AccountName'                          => $acc_name,
                    );


                    if ($ins_id && $ins) {
                        $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>');
                        redirect('company/Payments/payment_transaction', 'refresh');
                    }
                }

                $customerID  = $paydata['customerListID'];
                $amount      = $total;

                
                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                $responseErrorMsg = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;

				$payload = [
					'amount' => ($amount * 100)
				];
                $responseType = 'ReturnResponse';
                if($generateToken != ''){
                    $result = $gatewayTransaction->refundTransaction($tID, $payload);
                }else{
                    $responseType = 'GenerateKeyResponse';
                }
                
                
                if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                    $this->customer_model->update_refund_payment($tID, 'TSYS');
                    if (!empty($paydata['invoiceTxnID'])) {

                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $ins_id, '1', '', $user);
                    }
                    $this->session->set_flashdata('success', 'Successfully Refunded Payment');

                } else {

                    $err_msg = $result[$responseType]['responseMessage'];
                    if($responseErrorMsg != ''){
                        $err_msg = $responseErrorMsg;
                    }

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
                }
                $result['responseType'] = $responseType;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $paydata['merchantID'], $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, $custom_data_fields);
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>');
                redirect('company/Payments/payment_transaction', 'refresh');
            }
            $invoice_IDs = array();
           

            $receipt_data = array(
                'proccess_url'      => 'company/Payments/payment_refund',
                'proccess_btn_text' => 'Process New Refund',
                'sub_header'        => 'Refund',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if (!isset($result[$responseType]['transactionID']) || $result[$responseType]['transactionID'] == '') {
                $transactionid = 'null';
            } else {
                $transactionid = $result[$responseType]['transactionID'];
            }
            redirect('company/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction not availabe</strong></div>');
            redirect('company/Payments/payment_transaction', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/payment_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID, 'merchantID' => $merchantID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            if ($paydata['gatewayID'] > 0) {

                $gatlistval = $paydata['gatewayID'];

                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                if ($tID != '' && !empty($gt_result)) {

                    $customerID = $paydata['customerListID'];
                    $amount = $paydata['transactionAmount'];

                    $customerID = $paydata['customerListID'];
                    $comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                    
                    $deviceID = $gt_result['gatewayMerchantID'].'01';
                    $gatewayTransaction              = new TSYS();
                    $gatewayTransaction->environment = $this->gatewayEnvironment;
                    $gatewayTransaction->deviceID = $deviceID;
                    $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                    $generateToken = '';
                    $responseErrorMsg = '';
                    if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                        $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                    }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                        $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                        
                    }
                    $gatewayTransaction->transactionKey = $generateToken;
                    $responseType = 'CaptureResponse';
                    if($generateToken != ''){
                        $result = $gatewayTransaction->captureTransaction($tID);
                    }else{
                        $responseType = 'GenerateKeyResponse';
                    }
                    if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {

                        $condition = array('transactionID' => $tID, 'merchantID' => $merchantID);

                        $update_data = array('transaction_user_status' => "4");

                        $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                        if ($chh_mail == '1') {
                            $condition  = array('transactionID' => $tID);
                            $customerID = $paydata['customerListID'];

                            $comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                            $tr_date    = date('Y-m-d H:i:s');
                            $ref_number = $tID;
                            $toEmail    = $comp_data['Contact'];
                            $company    = $comp_data['companyName'];
                            $customer   = $comp_data['FullName'];
                            $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
                        }
                        $condition_mail = array('templateType' => '15', 'merchantID' => $merchantID);
                        $comp_data      = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                        $customerID     = $paydata['customerListID'];
                        $ref_number     = '';
                        $tr_date        = date('Y-m-d H:i:s');
                        $toEmail        = $comp_data['Contact'];
                        $company        = $comp_data['companyName'];
                        $customer       = $comp_data['FullName'];

                        $this->session->set_flashdata('success', 'Successfully Captured Authorization');
                    } else {
                        $err_msg = $result[$responseType]['responseMessage'];
                        if($responseErrorMsg != ''){
                            $err_msg = $responseErrorMsg;
                        }

                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
                    }
                    $result['responseType'] = $responseType;
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Gateway not availabe</strong></div>');
            }
            $invoice_IDs = array();

            $receipt_data = array(
                'proccess_url'      => 'company/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if (!isset($result[$responseType]['transactionID']) || $result[$responseType]['transactionID'] == '') {
                $transactionid = 'null';
            } else {
                $transactionid = $result[$responseType]['transactionID'];
            }
            redirect('company/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');
        }
    }

    /*****************Void Transaction***************/

    public function create_customer_void()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        //Show a form here which collects someone's name and e-mail address
        $result = array();
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con        = array('transactionID' => $tID, 'merchantID' => $merchantID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if (!empty($gt_result)) {
                
                $amount = $paydata['transactionAmount'];

                $customerID = $paydata['customerListID'];
                $comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                
                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                $responseErrorMsg = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;
                $responseType = 'VoidResponse';
                if($generateToken != ''){
                    $result = $gatewayTransaction->voidTransaction($tID);
                }else{
                    $responseType = 'GenerateKeyResponse';
                }

                if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                    $condition = array('transactionID' => $tID, 'merchantID' => $merchantID);

                    $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                    if ($chh_mail == '1') {
                        $condition  = array('transactionID' => $tID);
                        $customerID = $paydata['customerListID'];

                        $comp_data  = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['Contact'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['FullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
                    }

                    $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');

                } else {
                    $err_msg = $result[$responseType]['responseMessage'];
                    if($responseErrorMsg != ''){
                        $err_msg = $responseErrorMsg;
                    }

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
                }
                $transactiondata = array();
                $result['responseType'] = $responseType;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Gateway has removed.</strong></div>');
            }
            $invoice_IDs = array();
           
            $receipt_data = array(
                'proccess_url'      => 'company/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Void',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            
            if (!isset($result[$responseType]['transactionID']) || $result[$responseType]['transactionID'] == '') {
                $transactionid = 'null';
            } else {
                $transactionid = $result[$responseType]['transactionID'];
            }

            redirect('company/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');

        }
    }

    public function refund_transaction()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $data['transactions'] = $this->customer_model->get_refund_transaction_data($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/page_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function payment_refund()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['transactions'] = $this->customer_model->get_transaction_data_refund($user_id);
        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('company/payment_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function payment_transaction()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['transactions'] = $this->customer_model->get_transaction_data($user_id);
        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('company/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /*********ECheck Transactions**********/

    public function evoid_transaction()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['transactions'] = $this->customer_model->get_transaction_data_erefund($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('company/payment_ecapture', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function echeck_transaction()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['transactions'] = $this->customer_model->get_transaction_data_erefund($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('company/payment_erefund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];
        if (!empty($this->input->post(null, true))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $paydata['transactionAmount'];
            $customerID  = $paydata['customerListID'];

            $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;

            $payload = [
                'amount' => ($amount * 100)
            ];
            $responseType = 'ReturnResponse';
            if($generateToken != ''){
                $result = $gatewayTransaction->refundTransaction($tID, $payload);
            }else{
                $responseType = 'GenerateKeyResponse';
            }
            
            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                $this->customer_model->update_refund_payment($tID, 'TSYS');
                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {
                $err_msg = $result[$responseType]['responseMessage'];
                if($responseErrorMsg != ''){
                    $err_msg = $responseErrorMsg;
                }

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
            }
            $result['responseType'] = $responseType;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], true, $this->transactionByUser, $custom_data_fields);

            redirect('company/Payments/echeck_transaction', 'refresh');
        }
    }
    public function payment_evoid()
    {
        //Show a form here which collects someone's name and e-mail address
        $result = array();
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $merchantID = $this->session->userdata('logged_in')['merchID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $customerID = $paydata['customerListID'];
            $amount = $paydata['transactionAmount'];
            
            $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;
            $responseType = 'VoidResponse';    
            if($generateToken != ''){
                $result = $gatewayTransaction->voidTransaction($tID);
            }else{
                $responseType = 'GenerateKeyResponse';
            }
            
            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {

              
                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');

            } else {

                $err_msg = $result[$responseType]['responseMessage'];
                if($responseErrorMsg != ''){
                    $err_msg = $responseErrorMsg;
                }

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
            }
            $result['responseType'] = $responseType;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
        }
        redirect('company/Payments/evoid_transaction', 'refresh');
    }

    public function create_customer_esale()
    {

        if (!empty($this->input->post(null, true))) {

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            if ($gatlistval != "" && !empty($gt_result)) {
                
                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                $responseErrorMsg = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;

                $comp_data = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customerID));
                $companyID = $comp_data['companyID'];

                $amount         = $this->czsecurity->xssCleanPostInput('totalamount');
                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                $address1 = ($this->czsecurity->xssCleanPostInput('baddress1') != '')?$this->czsecurity->xssCleanPostInput('baddress1'):'None';
                $address2 = ($this->czsecurity->xssCleanPostInput('baddress1') != '')?$this->czsecurity->xssCleanPostInput('baddress1'):'None';
                $zipcode = ($this->czsecurity->xssCleanPostInput('bzipcode') != '')?$this->czsecurity->xssCleanPostInput('bzipcode'):'None';
                $city = ($this->czsecurity->xssCleanPostInput('bcity') != '')?$this->czsecurity->xssCleanPostInput('bcity'):'None';
                $state = ($this->czsecurity->xssCleanPostInput('bstate') != '')?$this->czsecurity->xssCleanPostInput('bstate'):'AZ';
                $country = ($this->czsecurity->xssCleanPostInput('bcountry') != '')?$this->czsecurity->xssCleanPostInput('bcountry'):'USA';
                $phone = ($this->czsecurity->xssCleanPostInput('phone') != '')?$this->czsecurity->xssCleanPostInput('phone'):'None';
                $firstName = ($this->czsecurity->xssCleanPostInput('firstName') != '')?$this->czsecurity->xssCleanPostInput('firstName'):'None';
                $lastName = ($this->czsecurity->xssCleanPostInput('lastName') != '')?$this->czsecurity->xssCleanPostInput('lastName'):'None';
                $companyName = ($this->czsecurity->xssCleanPostInput('companyName') != '')?$this->czsecurity->xssCleanPostInput('companyName'):'None';

                $transaction['Ach'] = array(
                    "deviceID"              => $deviceID,
                    "transactionKey"        => $generateToken,
                    "transactionAmount"     => $amount * 100,
                    "accountDetails"        => array(
                            "routingNumber" => $accountDetails['routeNumber'],
                            "accountNumber" => $accountDetails['accountNumber'],
                            "accountType"   => strtoupper($accountDetails['accountType']),
                            "accountNotes"  => "count",
                            "addressLine1"  => $address1,
                            "addressLine2"  => $address2,
                            "zip"           => $zipcode,
                            "city"          => $city,
                            "state"         => $state,
                            "country"       => $country
                    ),
                    "achSecCode"                => "WEB",
                    "originateDate"             => date('Y-m-d'),
                    "addenda"                   => "addenda",
                    "firstName"                 => $firstName,
                    "lastName"                  => $lastName,
                    "customerPhone"             => $phone,
                    "addressLine1"              => $address1,
                    "addressLine2"              => $address2,
                     "zip"                      => $zipcode,
                    "city"                      => $city,
                    "state"                     => $state,
                    "country"                   => $country,
                    "dob"                       => "1967-08-13",
                    "ssn"                       => "1967-08-13",
                    "driverLicenseNumber"       => "101",
                    "driverLicenseIssuedState"  => $state,
                    "developerID"               => "1234"    
                );

                $responseId = '';
                $responseType = 'AchResponse';
                if($generateToken != ''){
                    $result = $gatewayTransaction->processTransaction($transaction);
                }else{
                    $responseType = 'GenerateKeyResponse';
                }
                
                if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                    $responseId = $result[$responseType]['transactionID'];

                    $invoicePayAmounts = [];
                    $invoiceIDs = [];
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }
                    $refNum = array();
                    $result['responseType'] = $responseType;
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
                            if (!empty($theInvoice)) {
                                $amount_data = $theInvoice['BalanceRemaining'];
                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                $refnum[] = $theInvoice['RefNumber'];
                                
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 'true';
                                }else{
                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 'false';
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount; 
                                }
                                $txnAmount = $actualInvoicePayAmount;
                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                
                                $tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

                                $transactiondata = array();
                                $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID,'', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                            }
                            $payIndex++;
                        }
                    } else {

                        $transactiondata = array();
                        $inID = '';
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($this->czsecurity->xssCleanPostInput('tr_checked')) {
                        $chh_mail = 1;
                    } else {
                        $chh_mail = 0;
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number = '';
                    $tr_date    = date('Y-m-d H:i:s');
                    $toEmail    = $comp_data['Contact'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['FullName'];

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }
                    if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                    }
                   
                    $this->session->set_flashdata('success', ' Transaction Successful');
                } else {
                    $err_msg = $result[$responseType]['responseMessage'];
                    if($responseErrorMsg != ''){
                        $err_msg = $responseErrorMsg;
                    }

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
                }
                

                $receipt_data = array(
                    'transaction_id'    => $responseId,
                    'IP_address'        => getClientIpAddr(),
                    'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                    'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                    'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                    'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                    'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                    'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                    'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                    'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                    'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                    'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                    'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                    'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                    'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                    'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                    'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                    'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                    'proccess_url'      => 'company/Payments/create_customer_esale',
                    'proccess_btn_text' => 'Process New Sale',
                    'sub_header'        => 'Sale'
                );

                $this->session->set_userdata("receipt_data", $receipt_data);
                redirect('company/home/transation_sale_receipt', 'refresh');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            redirect('company/Payments/create_customer_esale', 'refresh');
        }
        redirect('company/Payments/create_customer_esale', 'refresh');
    }

    /**********END****************/

    public function chk_friendly_name()
    {
        $res       = array();
        $frname    = $this->czsecurity->xssCleanPostInput('frname');
        $condition = array('gatewayFriendlyName' => $frname);
        $num       = $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
        if ($num) {
            $res = array('gfrname' => 'Friendly name already exist', 'status' => 'false');
        } else {
            $res = array('status' => 'true');
        }
        echo json_encode($res);
        die;
    }

    /*****************Test TSYS Validity***************/

    public function get_card_edit_data()
    {

        if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
            $cardID = $this->czsecurity->xssCleanPostInput('cardID');
            $data   = $this->card_model->get_single_mask_card_data($cardID);
            echo json_encode(array('status' => 'success', 'card' => $data));
            die;
        }
        echo json_encode(array('status' => 'success'));
        die;
    }

    public function check_vault()
    {

        $card         = '';
        $card_name    = '';
        $customerdata = array();
        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $merchantID = $da['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $merchantID = $da['merchantID'];
        }

        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
           
            $condition    = array('ListID' => $customerID, 'qbmerchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('chargezoom_test_customer', $condition);

            if (!empty($customerdata)) {

                
                $customerdata['status'] = 'success';

                $ach_data = $this->card_model->get_ach_info_data($customerID);
                $ACH      = [];
                if (!empty($ach_data)) {
                    foreach ($ach_data as $card) {
                        $ACH[] = $card['CardID'];
                    }
                }
                $recentACH                          = end($ACH);
                $customerdata['ach_data']           = $ach_data;
                $customerdata['recent_ach_account'] = $recentACH;

                
                $card_data = $this->card_model->get_card_expiry_data($customerID);
                $cardArr   = [];
                if (!empty($card_data)) {
                    foreach ($card_data as $card) {
                        $cardArr[] = $card['CardID'];
                    }
                }
                $recentCard                  = end($cardArr);
                $customerdata['card']        = $card_data;
                $customerdata['recent_card'] = $recentCard;

                $invoices = $this->customer_model->get_invoice_upcomming_data($customerID, $merchantID);

                $table = '';

                if (!empty($invoices)) {
                    $table .= '<table class="col-md-offset-3 mytable" width="50%">';
                    $table .= "<tr>
	    <th class='text-left'>Invoice</th>
		<th class='text-right'>Amount</th>
		</tr>";

                    foreach ($invoices as $inv) {
                        if ($inv['status'] != 'Cancel') {
                            $table .= "<tr>
				<td class='text-left'><input type='checkbox' name='inv' value='" . $inv['BalanceRemaining'] . "' class='test'  data-checkclass='" . $inv['RefNumber'] . "' rel='" . $inv['TxnID'] . "'/> " . $inv['RefNumber'] . "</td>
				<td class='text-right'>" . '$' . number_format((float) $inv['BalanceRemaining'], 2, '.', ',') . "</td>
				</tr>";
                        }
                    }
                    $table .= "</table>";
                } else {
                    $table .= '';
                }

                if (empty($customerdata['companyName'])) {
                    $customerdata['companyName'] = $customerdata['FullName'];
                }

                $customerdata['invoices'] = $table;
                echo json_encode($customerdata);
                die;
            }
        }
    }

    public function view_transaction()
    {

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];
        $transactions       = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id);

        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                ?>
				<tr>
					<td class="text-left"><?php echo ($transaction['transactionID']) ? $transaction['transactionID'] : ''; ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($transaction['transactionAmount'], 2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo ucfirst($transaction['transactionType']); ?></td>
					

					<td class="text-right visible-lg"><?php if ($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') {?> <span class="">Success</span><?php } else {?> <span class="">Failed</span> <?php }?></td>

				</tr>

<?php }
        } else {
            echo '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
        }
        die;
    }

    public function check_transaction_payment()
    {
        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $user_id = $da['merchantID'];
        }

        if (!empty($this->czsecurity->xssCleanPostInput('trID'))) {

            $trID      = $this->czsecurity->xssCleanPostInput('trID');
            $av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');
            $p_data = $this->customer_model->chk_transaction_details(array('id' => $trID, 'tr.merchantID' => $user_id));

            if (!empty($p_data)) {
                if ($p_data['transactionAmount'] >= $av_amount) {
                    $resdata['status'] = 'success';
                } else {
                    $resdata['status']  = 'error';
                    $resdata['message'] = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
                }
            } else {
                $resdata['status']  = 'error';
                $resdata['message'] = 'Invalid transactions ';
            }
        } else {
            $resdata['status']  = 'error';
            $resdata['message'] = 'Invalid request';
        }
        echo json_encode($resdata);
        die;
    }

    public function pay_multi_invoice()
    {
        if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        $c_data     = $this->general_model->get_select_data('chargezoom_test_customer', array('companyID', 'FirstName', 'LastName', 'companyName','Contact'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));

        $companyID  = $c_data['companyID'];
        $cardID_upd = '';
        $custom_data_fields = [];
        $invoices   = $this->czsecurity->xssCleanPostInput('multi_inv');
        $cardID  = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway = $this->czsecurity->xssCleanPostInput('gateway1');

        $cusproID = '';
        $cusproID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && !empty($invoices) && $cardID != "" && $gateway != "") {
            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;


            foreach ($invoices as $invoiceID) {

                $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                $in_data     = $this->company_model->get_invoice_data_pay($invoiceID);
                $resellerID  = $this->resellerID;
                if (!empty($in_data)) {

                    $customerID = $Customer_ListID = $in_data['Customer_ListID'];

                    if ($cardID == 'new1') {
                        $cardID_upd = $cardID;
                        $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                        $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cvv        = $this->czsecurity->xssCleanPostInput('cvv');

                        $address1 = $this->czsecurity->xssCleanPostInput('address1');
                        $address2 = $this->czsecurity->xssCleanPostInput('address2');
                        $city     = $this->czsecurity->xssCleanPostInput('city');
                        $country  = $this->czsecurity->xssCleanPostInput('country');
                        $phone    = $this->czsecurity->xssCleanPostInput('contact');
                        $state    = $this->czsecurity->xssCleanPostInput('state');
                        $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                    } else {
                        $card_data = $this->card_model->get_single_card_data($cardID);
                        $card_no   = $card_data['CardNo'];
                        $cvv       = $card_data['CardCVV'];
                        $expmonth  = $card_data['cardMonth'];
                        $exyear    = $card_data['cardYear'];

                        $address1 = $card_data['Billing_Addr1'];
                        $address2 = $card_data['Billing_Addr2'];
                        $city     = $card_data['Billing_City'];
                        $zipcode  = $card_data['Billing_Zipcode'];
                        $state    = $card_data['Billing_State'];
                        $country  = $card_data['Billing_Country'];
                        $phone    = $card_data['Billing_Contact'];
                        
                    }
                    $cardType = $this->general_model->getType($card_no);
                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                    $custom_data_fields['payment_type'] = $friendlyname;

                    if (!empty($cardID)) {

                        if ($in_data['BalanceRemaining'] > 0) {
                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];
                            $amount    = $pay_amounts;

                            $amount = $amount - $cr_amount;

                            $exyear1 = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . '/' . $exyear1;
                            $responseType = 'SaleResponse';
                            $amount = round($amount,2);
                            $transaction['Sale'] = array(
                                "deviceID"                          => $deviceID,
                                "transactionKey"                    => $generateToken,
                                "cardDataSource"                    => "MANUAL",  
                                "transactionAmount"                 => (int)($amount * 100),
                                "currencyCode"                      => "USD",
                                "cardNumber"                        => $card_no,
                                "expirationDate"                    => $expry,
                                "cvv2"                              => $cvv,
                                "addressLine1"                      => ($address1 != '')?$address1:'None',
                                "zip"                               => ($zipcode != '')?$zipcode:'None',
                                "orderNumber"                       => $in_data['RefNumber'],
                                "notifyEmailID"                     => (($c_data['Contact']  != ''))?$c_data['Contact']:'chargezoom@chargezoom.com',
                                "firstName"                         => (($c_data['FirstName'] != ''))?$c_data['FirstName']:'None',
                                "lastName"                          => (($c_data['LastName'] != ''))?$c_data['LastName']:'None',
                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                "terminalOutputCapability"          => "DISPLAY_ONLY",
                                "maxPinLength"                      => "UNKNOWN",
                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                "cardPresentDetail"                 => "CARD_PRESENT",
                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                "cardholderAuthenticationEntity"    => "OTHER",
                                "cardDataOutputCapability"          => "NONE",

                                "customerDetails"   => array( 
                                        "contactDetails" => array(
                                            "addressLine1"=> ($address1 != '')?$address1:'None',
                                             "addressLine2"                      => ($address2 != '')?$address2:'None',
                                            "city"=>($city != '')?$city:'None',
                                            "zip"=>($zipcode != '')?$zipcode:'None'
                                        ),
                                        "shippingDetails" => array( 
                                            "firstName"=> (($c_data['FirstName'] != ''))?$c_data['FirstName']:'None',
                                            "lastName"=> (($c_data['LastName'] != ''))?$c_data['LastName']:'None',
                                            "addressLine1"=>($address1 != '')?$address1:'None',
                                            "addressLine2"                      => ($address2 != '')?$address2:'None',
                                            "city"=>($city != '')?$city:'None',
                                            "zip"=>($zipcode != '')?$zipcode:'None',
                                            "emailID"=>(($c_data['Contact'] != ''))?$c_data['Contact']:'chargezoom@chargezoom.com'
                                         )
                                    )
                            );
                            if($cvv == ''){
                                unset($transaction['Sale']['cvv2']);
                            }
                            if($generateToken != ''){
                                $result = $gatewayTransaction->processTransaction($transaction);
                            }else{
                                $responseType = 'GenerateKeyResponse';
                            }
            				
                            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                                $txnID  = $in_data['TxnID'];
                                $ispaid = 'true';
                                $am     = (-$amount);

                                $bamount = $in_data['BalanceRemaining'] - $amount;
                                if ($bamount > 0) {
                                    $ispaid = 'false';
                                }

                                $app_amount = $in_data['AppliedAmount'] + (-$amount);
                                $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);

                                $condition = array('TxnID' => $in_data['TxnID']);

                                $this->general_model->update_row_data('chargezoom_test_invoice', $condition, $data);
                                $user = $in_data['qbwc_username'];

                                if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                                    $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                                    $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear    = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                                    $card_type = $this->general_model->getType($card_no);

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $card_type,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'customerListID'  => $customerID,
                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,

                                        'createdAt'       => date("Y-m-d H:i:s"),
                                        'Billing_Addr1'   => $address1,
                                        'Billing_Addr2'   => $address2,
                                        'Billing_City'    => $city,
                                        'Billing_State'   => $state,
                                        'Billing_Country' => $country,
                                        'Billing_Contact' => $phone,
                                        'Billing_Zipcode' => $zipcode,
                                    );
                                    
                                    $id1 = $this->card_model->process_card($card_data);
                                }

                                
                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            } else {

                                if ($cardID_upd == 'new1') {
                                    $this->db1->where(array('CardID' => $cardID));
                                    $this->db1->delete('customer_card_data');
                                }

                                $err_msg = $result[$responseType]['responseMessage'];
                                if($responseErrorMsg != ''){
                                    $err_msg = $responseErrorMsg;
                                }

                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
                            }
                            $result['responseType'] = $responseType;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $in_data['Customer_ListID'], $pay_amounts, $user_id, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Not valid </strong>.</div>');
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Customer has no card</strong>.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
                }
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Select Gateway and Card!</strong>.</div>');
        }

        if(!$checkPlan){
            $responseId  = '';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('company/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('company/home/invoices', 'refresh');
        }
    }
}