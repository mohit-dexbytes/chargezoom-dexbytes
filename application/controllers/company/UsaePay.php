<?php

	class UsaePay extends CI_Controller
   {
   
     	private $resellerID;
		private $transactionByUser;

    	public function __construct()
    	{
    		parent::__construct();
    		
    		
       require_once APPPATH."third_party/usaepay/usaepay.php";	
	
		$this->load->model('company/customer_model','customer_model');
		$this->load->model('general_model');
		$this->load->model('company/company_model','company_model');
    	$this->load->model('card_model');
		$this->load->config('usaePay');
		
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->library('form_validation');
		 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='5' )
		  {

			$logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
			$logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	
	public function index(){
	
	   	redirect('company/home','refresh'); 
	}
	
		/************Single Payment Process Start*****************/
				 

		public function pay_invoice()
	{
	 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('sch_gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
   
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
          
        	$checkPlan = check_free_plan_transactions();
         $custom_data_fields = [];   
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		       
		       
		     
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	 $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  	$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
                  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
			  
			       $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
			   
			   
			    if(!empty( $in_data)) 
			    {
			         $in_data   =    $this->company_model->get_invoice_data_pay($invoiceID); 
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			   
    				  $username   = $gt_result['gatewayUsername'];
    			     $password   = $gt_result['gatewayPassword'];
    			     $customerID = $in_data['Customer_ListID'];
            		$c_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
            		
            			$companyID = $c_data['companyID'];
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$address2 = $card_data['Billing_Addr2'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    					
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }  
    			     $cardType = $this->general_model->getType($card_no);
                 $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                 $custom_data_fields['payment_type'] = $friendlyname;                            
          			
                		if( $in_data['BalanceRemaining'] > 0)
        				{
				    
                    	$crtxnID='';  
                        $invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$username;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
						
						
					
                            $transaction->card = $card_no;
							$expyear1   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear1;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
                           
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zipcode;
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							
					     	$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zipcode;
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('bphone');
						
						
							$transaction->amount = $amount;
						    
							$result = $transaction->Process();
						
                 
                     if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                   
                            $txnID      = $in_data['TxnID'];  
							 $ispaid 	 = 'true';
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 'false';
							 $app_amount = $in_data['AppliedAmount']-$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('TxnID'=>$in_data['TxnID'] );
							 
							 $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
					
                  
                     	 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
        				 {
        				 
        				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
        				 		$cardType       = $this->general_model->getType($card_no);
        				        $friendlyname   =  $cardType.' - '.substr($card_no,-4);
        				        
        						$card_condition = array(
        										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
        										 'customerCardfriendlyName'=>$friendlyname,
        										);
        										
        							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
        							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
        						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
        							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        					
        						$crdata =	$this->card_model->chk_card_firendly_name($cid,$friendlyname);			
        					   
        					
        					 
        						if($crdata > 0)
        						{
        							
        						  $card_data = array('cardMonth'   =>$expmonth,
        										   'cardYear'	 =>$exyear, 
        										   'CardType'    =>$cardType,
        										  'CustomerCard' =>$this->card_model->encrypt($card_no),
        										  'CardCVV'      =>'',
        										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
        	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
        	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
        	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
        	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
        	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
        	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
        										 'customerListID' =>$in_data['Customer_ListID'], 
        										 'customerCardfriendlyName'=>$friendlyname,
        										 'companyID'     =>$companyID,
        										  'merchantID'   => $user_id,
        										 'updatedAt' 	=> date("Y-m-d H:i:s") );
        									
        					
        						   $this->card_model->update_card_data($card_condition, $card_data);				 
        						}
        						else
        						{
        					      $card_data = array('cardMonth'   =>$expmonth,
        										   'cardYear'	 =>$exyear, 
        										   'CardType'    =>$cardType,
        										  'CustomerCard' =>$this->card_model->encrypt($card_no),
        										  'CardCVV'      =>'',
        										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
        	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
        	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
        	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
        	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
        	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
        	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
        										 'customerListID' =>$in_data['Customer_ListID'],
        										 'customerCardfriendlyName'=>$friendlyname,
        										 'companyID'     =>$companyID,
        										  'merchantID'   => $user_id,
        										 'createdAt' 	=> date("Y-m-d H:i:s") );
        										 
        							 			 
        								
        				            $id1 = $this->card_model->insert_card_data($card_data);	
        					
        						}
        				
							   }
						$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
						$ref_number =  $in_data['RefNumber']; 
						$tr_date   =date('Y-m-d H:i:s');
						$toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
							
						$this->session->set_flashdata('success', ' Successfully Processed Invoice'); 
						
                     }
                       
		             }
            		    else        
                        {
                          
						   
							$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$res['responsetext'].'</strong></div>'); 
						   }  
						  
						  
						     $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser, $custom_data_fields); 
							 
							 if($res['transactionCode'] == 200 && $chh_mail =='1')
							 {
							  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $res['transactionId']);
							 }
						   
                            	
                        
			    }
			    else
			    {
			       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');  
			        
			    }
		   
		   }
		   else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
			}
			
			if(!$checkPlan){
				$responseId  = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
			
			 
		   
               if($cusproID=="2"){
			 	 redirect('company/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('company/home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
			 $trans_id = $transaction->refnum;
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'company/home/invoices',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
			 if ($cusproID == "1") {
				 redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('company/home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
	      
	}
	


	
	/*****************Single Payment Process End*****************/
	
		
	
	public function create_customer_sale()
	{ 
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	    
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           
        	$checkPlan = check_free_plan_transactions();
         $custom_data_fields = [];      
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
	   			$custom_data_fields = [];
	   			$applySurcharge = false;
	   			if($this->czsecurity->xssCleanPostInput('invoice_id')){
	                $applySurcharge = true;
	            }
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) 
	            {
	               $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }
        			   
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   if($this->czsecurity->xssCleanPostInput('setMail'))
        			     $chh_mail =1;
        			     else
        			      $chh_mail =0; 
        			  
        			if(!empty($gt_result) )
        			{
        			   $nmiuser  = $gt_result['gatewayUsername'];
						$nmipass  = $gt_result['gatewayPassword'];
						
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				
                        $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    	                        $contact =  $this->czsecurity->xssCleanPostInput('phone');
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
									$contact  = $card_data['Billing_Contact'];
        					}
        					/*Added card type in transaction table*/
		                $card_type = $this->general_model->getType($card_no);
		                $friendlyname = $card_type . ' - ' . substr($card_no, -4);
		                $custom_data_fields['payment_type'] = $friendlyname;
                
        					 
                        $transaction = new umTransaction;
						$transaction->key=$nmiuser;
						$transaction->pin=$nmipass;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=mt_rand('2500000','7500000');   		// invoice number.  must be unique.
						$transaction->description="Sale for Invocie Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
					    
					   $transaction->command="sale";	
                            $transaction->card = $card_no;
							$expyear1   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear1;  
							$transaction->exp = $expry;
							
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
                           
							$transaction->billcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->billfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->billlname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->billstreet = $this->czsecurity->xssCleanPostInput('baddress1');
							$transaction->billstreet2 = $this->czsecurity->xssCleanPostInput('baddress2');
							$transaction->billcountry = $this->czsecurity->xssCleanPostInput('bcountry');
							$transaction->billcity = $this->czsecurity->xssCleanPostInput('bcity');
							$transaction->billstate = $this->czsecurity->xssCleanPostInput('bstate');
							$transaction->billzip = $this->czsecurity->xssCleanPostInput('bzipcode');
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->shipcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->shipfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->shiplname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->shipstreet = $this->czsecurity->xssCleanPostInput('address1');
							$transaction->shipstreet2 = $this->czsecurity->xssCleanPostInput('address2');
							$transaction->shipcountry = $this->czsecurity->xssCleanPostInput('country');
							$transaction->shipcity = $this->czsecurity->xssCleanPostInput('city');
							$transaction->shipstate = $this->czsecurity->xssCleanPostInput('state');
							$transaction->shipzip = $this->czsecurity->xssCleanPostInput('zipcode');
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('phone');
							$amount = $this->czsecurity->xssCleanPostInput('totalamount');
                     // update amount with surcharge 
		                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
		                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
		                    $amount += round($surchargeAmount, 2);
		                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
		                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
		                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
		                    
		                }
		                $totalamount  = $amount;      
                            $transaction->email = $this->czsecurity->xssCleanPostInput('email');
							
							$transaction->amount = $amount;
							
							if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
								$transaction->orderid = $this->czsecurity->xssCleanPostInput('invoice_number');
							}

							if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
								$transaction->ponum = $this->czsecurity->xssCleanPostInput('po_number');
							}

							
							$transaction->Process();
        				
            	          	$crtxnID='';
            					
            		if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                     	
                     	

                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
              
				     
				        
					
				 		$invoicePayAmounts = array();
						$invoiceIDs=array(); 
						if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
							$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
							$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
						}
				     
				         if(!empty($invoiceIDs))
				           {
							  $payIndex = 0;
							  $refNumber =array(); 
				              foreach($invoiceIDs as $inID)
				              {
        				            $theInvoice = array();
        							 
        							$theInvoice = $this->general_model->get_row_data('chargezoom_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
        					
        								
        								if(!empty($theInvoice) )
        								{
        								
        									$amount_data = $theInvoice['BalanceRemaining'];
											$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
											if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

	                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
	                                    $actualInvoicePayAmount += $surchargeAmount;
	                                    $amount_data += $surchargeAmount;

	                                    $updatedInvoiceData = [
	                                        'inID' => $inID,
	                                        'merchantID' => $user_id,
	                                        'amount' => $surchargeAmount,
	                                    ];
	                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,5);
	                              }
											$isPaid 	 = 'false';
											$BalanceRemaining = 0.00;
											$refnum[] = $theInvoice['RefNumber'];
											
											if($amount_data == $actualInvoicePayAmount){
												$actualInvoicePayAmount = $amount_data;
												$isPaid 	 = 'true';

											}else{

												$actualInvoicePayAmount = $actualInvoicePayAmount;
												$isPaid 	 = 'false';
												$BalanceRemaining = $amount_data - $actualInvoicePayAmount;
												
											}
											$txnAmount = $actualInvoicePayAmount;
											$AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
											
											$tes = $this->general_model->update_row_data('chargezoom_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));
            							
            						    	$transactiondata= array();
                    				       
                    				       
                    				           $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$txnAmount,$user_id,$crtxnID='', $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields); 
                    				    
		        						}
								++$payIndex;
							  }
							  	$ref_number = implode(',',$refNumber);
							  
							
				          }
						  else
						  {
				              
				                          $transactiondata= array();
                    				     
                    				 
                    			     $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields); 
                    				$ref_number = '';   
				              
				          }
				          
                  	$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 

						  	$tr_date   =date('Y-m-d H:i:s');
						  	$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName']; 
					      if($chh_mail =='1')
						 	{
						   
						  
						   	$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $trID);
						 	}
                     
				          
                    				    if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
            				 		
            				     
            				       	$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
            				         $cardType       = $this->general_model->getType($card_no);
                                    $friendlyname = $cardType.' - '.substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
                                      
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					  
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					        	       
                    				   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            							
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	    
                    			
				              
				          }
				     
					 }
					 $this->session->set_flashdata('success', 'Successfully sale Credit Card Payment');
			
				 }
				 
		
				  
				 else
				 {
				     
				             
                               $msg = $transaction->result;
                               $trID = $transaction->refnum;
                               $res =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
                               $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  								
				 }
				 
						       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => $transaction->refnum,
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'company/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'  =>  $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('company/home/transation_sale_receipt',  'refresh');
                 
	    
	    
    	}
	
	
	
	/********************** end refunc function *******************/
	
	
	
	
	
	
/*****************Authorize Transaction***************/
	
	 
	public function create_customer_auth()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
            if($this->session->userdata('logged_in')){
			
		
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
	  
			
        $checkPlan = check_free_plan_transactions();
		$custom_data_fields = [];
		if(!empty($this->input->post(null, true)))
		{
			
        			
			
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			$po_number = $this->czsecurity->xssCleanPostInput('po_number');
         if (!empty($po_number)) {
             $custom_data_fields['po_number'] = $po_number;
         } 
			if($gatlistval !="" && !empty($gt_result) )
			{
			     $user      = $gt_result['gatewayUsername'];
            	 $password   = $gt_result['gatewayPassword'];
				
				
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
	            $customerID = 	$this->czsecurity->xssCleanPostInput('customerID');
				$comp_data  = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' =>  $customerID));
				$companyID  = $comp_data['companyID'];
				
				
			   $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
				
		       if( $this->czsecurity->xssCleanPostInput('card_number')!=""  && $cardID=='new1'){	
    				     $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
            						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
            						
            						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
            						if($this->czsecurity->xssCleanPostInput('cvv')!="")
            					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
            				     	$address1 =  $this->czsecurity->xssCleanPostInput('baddress1');
        	                	    $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
        	                    	$city    =$this->czsecurity->xssCleanPostInput('bcity');
        	                        $country =$this->czsecurity->xssCleanPostInput('bcountry');
        	                        $phone   =  $this->czsecurity->xssCleanPostInput('bphone');
        	                        $state   =  $this->czsecurity->xssCleanPostInput('bstate');
        	                        $zipcode =  $this->czsecurity->xssCleanPostInput('bzipcode');
				
				  }else {
					  
						   $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
             					$address1 = $card_data['Billing_Addr1'];
             					$address2 = $card_data['Billing_Addr2'];
             					$city     =  $card_data['Billing_City'];
             					$zipcode  = $card_data['Billing_Zipcode'];
             					$state    = $card_data['Billing_State'];
             					$country  = $card_data['Billing_Country'];
                				
					}
					   $cardType = $this->general_model->getType($card_no);
	               $friendlyname = $cardType . ' - ' . substr($card_no, -4);

	               $custom_data_fields['payment_type'] = $friendlyname;	
						$invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$user;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="auth";	
                            $transaction->card = $card_no;
							$expyear1   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear1;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
							$transaction->billcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->billfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->billlname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->billstreet = $this->czsecurity->xssCleanPostInput('baddress1');
							$transaction->billstreet2 = $this->czsecurity->xssCleanPostInput('baddress2');
							$transaction->billcountry = $this->czsecurity->xssCleanPostInput('bcountry');
							$transaction->billcity = $this->czsecurity->xssCleanPostInput('bcity');
							$transaction->billstate = $this->czsecurity->xssCleanPostInput('bstate');
							$transaction->billzip = $this->czsecurity->xssCleanPostInput('bzipcode');
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->shipcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->shipfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->shiplname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->shipstreet = $this->czsecurity->xssCleanPostInput('address1');
							$transaction->shipstreet2 = $this->czsecurity->xssCleanPostInput('address2');
							$transaction->shipcountry = $this->czsecurity->xssCleanPostInput('country');
							$transaction->shipcity = $this->czsecurity->xssCleanPostInput('city');
							$transaction->shipstate = $this->czsecurity->xssCleanPostInput('state');
							$transaction->shipzip = $this->czsecurity->xssCleanPostInput('zipcode');
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('phone');
							$amount = $this->czsecurity->xssCleanPostInput('totalamount');
                           
                            $transaction->email = $this->czsecurity->xssCleanPostInput('email');
							
							$transaction->amount = $amount;
							if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
								$transaction->orderid = $this->czsecurity->xssCleanPostInput('invoice_number');
							}

							if (!empty($po_number)) {
								$transaction->ponum = $po_number;
							}
						
							$transaction->Process();
        				  
            	          	$crtxnID='';
            				
            		if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                     
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                 
				 
				 /* This block is created for saving Card info in encrypted form  */
				
				 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
				 {
				 
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
				 		$cardType         =$this->general_model->getType($card_no);
				        $friendlyname   = $cardType.' - '.substr($card_no,-4);
				        
						$card_condition = array(
										 'customerListID' => $customerID, 
										 'customerCardfriendlyName'=>$friendlyname,
										);
										
							$cid      =     $customerID;		
							
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
							
				        
				        		
						$this->card_model->chk_card_firendly_name($cid , $friendlyname);		
					     
					 if(!empty($crdata))
							{
						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
									
					
						   $this->card_model->update_card_data($card_condition, $card_data);				 
						}
						else
						{
					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',  
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
										 
								
				            $id1 = $this->card_model->insert_card_data($card_data);	
					
						}
				
				 }
				 $this->session->set_flashdata('success', 'Transaction Successful');
			
				 }
				 
		
				  
				 else
				 {
				     
				             $msg = $transaction->result;
                               $trID = $transaction->refnum;
                               $res =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
                               $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gatlistval,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                    
				 }
				 
				       
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Please select Gateway</strong></div>'); 
				}		
				
					       
					  $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gatlistval,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
			
		}
		$invoice_IDs = array();
		
	
		$receipt_data = array(
			'transaction_id' => $transaction->refnum,
			'IP_address' => getClientIpAddr(),
			'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			'proccess_url' => 'company/Payments/create_customer_auth',
			'proccess_btn_text' => 'Process New Transaction',
			'sub_header' => 'Authorize',
			'checkPlan'  => $checkPlan
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		$this->session->set_userdata("invoice_IDs",$invoice_IDs);
		
		
		redirect('company/home/transation_sale_receipt',  'refresh');
      
              
	}			
		     
    
	/***************************** usaepay customer capture end***********************************/
		public function create_customer_capture()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
			 if($this->session->userdata('logged_in'))
			    {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
					
			$this->form_validation->set_rules('useatxnID', 'Transaction ID', 'required|xss_clean');
			
          
	    	if ($this->form_validation->run() == true)
		    {
					 $tID     = $this->czsecurity->xssCleanPostInput('useatxnID');
    			 
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
				 	 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			
			   
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                 
                 $transaction->command="capture";   // void command cancels a pending transaction.
                 
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
				
				 $amount  =  $paydata['transactionAmount'];
				 
				 
				 $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                
			      $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
					$customerID = $paydata['customerListID'];
                                
					$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
					$tr_date   =date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
					 if($chh_mail =='1')
                            {
                                
                               
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
							}
							$this->session->set_flashdata('success', 'Successfully Captured');
					 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				 
				   $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
				 
				   $invoice_IDs = array();
				  
			   
				   $receipt_data = array(
					   'proccess_url' => 'company/Payments/payment_capture',
					   'proccess_btn_text' => 'Process New Transaction',
					   'sub_header' => 'Capture',
				   );
				   
				   $this->session->set_userdata("receipt_data",$receipt_data);
				   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				   
				   if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($transaction->refnum == ''){
						$transaction->refnum ='null';
					}
				   redirect('company/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$transaction->refnum,  'refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('company/Payments/payment_capture','refresh');
			
	}
	
	
     	public function create_customer_void()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$custom_data_fields = [];
		if(!empty($this->input->post(null, true))){
		    
		        if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
						
    			  $tID     = $this->czsecurity->xssCleanPostInput('useatxnvoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gatlistval = $paydata['gatewayID'];
				 $merchantID = $this->session->userdata('logged_in')['merchID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			
			   
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=true;		// Sandbox true/false
                 $transaction->testmode=0;    // Change this to 0 for the transaction to process
                 
                 $transaction->command="void";    // void command cancels a pending transaction.
                
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.

			   
				 
				
				 $amount  =  $paydata['transactionAmount']; 
				$customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                
				 
			 
				  $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
				    $transactionCode ='200';
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
					 if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
                            $this->session->set_flashdata('success', 'Success');
					
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				      $transactiondata= array();
				      $transactiondata['transactionID']       = $transaction->refnum;
				       $transactiondata['transactionStatus']    = $transaction->authcode;
					  $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
					  $transactiondata['transactionType']    = 'void';
					  $transactiondata['transactionGateway']= $gt_result['gatewayType'];
					  $transactiondata['gatewayID']        = $gatlistval;
					  $transactiondata['transactionCode']   = $transactionCode;
					  $transactiondata['customerListID']     = $customerID;
					  $transactiondata['transactionAmount']  = $amount;
					  $transactiondata['merchantID']  = $merchantID;
					  $transactiondata['resellerID']   = $this->resellerID;
					  $transactiondata['gateway']   = "USAePay";  
					  $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}     
						if($custom_data_fields){
		                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
		            }       
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
						redirect('company/Payments/payment_capture','refresh');
		}     
			
	}
	
	

	public function pay_multi_invoice()
	{
	  
	      
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
					
			$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
        
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
            
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                 $cusproID='';
            $custom_data_fields = [];
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        	$checkPlan = check_free_plan_transactions();
               
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		    	 $cardID_upd ='';
			     
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
			     $amount               = $this->czsecurity->xssCleanPostInput('totalPay');
			     $customerID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			      $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
                  $invoiceIDs =  implode(',',$invoices);
			       $inv_data   =    $this->customer_model->get_due_invoice_data($invoiceIDs, $user_id);
			     
			   
			   
			    if(!empty($invoices) && !empty( $inv_data)) 
			    {
                   $cusproID= $customerID  = $inv_data['Customer_ListID'];
    			     $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  
    			     $username   = $gt_result['gatewayUsername'];
					 $password   = $gt_result['gatewayPassword'];
    			     
    			     if($cardID!="new1")
    			     {
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				         	$expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['cardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    					
    			     }
    			     else
    			     {
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }     
    			     $cardType = $this->general_model->getType($card_no);
                 $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                 $custom_data_fields['payment_type'] = $friendlyname;                         
        		      $crtxnID='';	     
        	
                            
					    $transaction = new umTransaction;
					    $transaction->key=$username;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
						
						
					
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
                           
						
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zipcode;
							
					     	$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zipcode;
							
							$transaction->amount = $amount;
						 
							$result = $transaction->Process();
                	
                     if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
						
						$this->session->set_flashdata('success', 'Successfully Processed Invoice');
					
            			$c_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
            		
            			$companyID = $c_data['companyID'];
                            if(!empty($invoices))
                            {
        	                    foreach($invoices as $invoiceID)
        	                    {
        	                        $crtxnID='';
                                    $in_data =    $this->company_model->get_invoice_data_pay($invoiceID);
                                    if(!empty($in_data))
                                    {
                                        $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                        $txnID       = $invoiceID;  
            							$ispaid 	 = 'true';
            							
            							$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 'false';
            							 $app_amount = $in_data['AppliedAmount']-$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $condition  = array('TxnID'=>$in_data['TxnID'] );
            							 
        							    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
        							   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$pay_amounts,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser, $custom_data_fields);  
                                    } 
        	                    }
                            } 
                            
                             if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				     {
                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
            				 		$cardType = $this->general_model->getType($card_no);
            				        $friendlyname   = $cardType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$cardType,
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            							
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
							 
							 
                     }
                     else
                     {
                            $msg = $transaction->result;
                                         $trID = $transaction->refnum;
                                      $res =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
                         
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                         
                     }
                     
                     
                     
                   
             } 
		       else        
               {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
                
               }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }

			if(!$checkPlan){
				$responseId  = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Transaction limit has been reached. <a href= "'.base_url().'company/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
			
		    	 if($cusproID!="")
				 {
					 redirect('company/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 redirect('company/home/invoices','refresh');
				 }
	      
	}

}