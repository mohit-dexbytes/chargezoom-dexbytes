<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('company/customer_model','customer_model');
		$this->load->model('company/company_model','company_model');
			$this->load->model('card_model');
      
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='5' )
          {
           
          }else if($this->session->userdata('user_logged_in')!="")
          {
           
          }else{
            redirect('login','refresh');
          }
	}
	
	
	public function reports()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		if($this->session->userdata('logged_in')){
		
			$da    = $this->session->userdata('logged_in');
			$user_id			    = $da['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$da = $this->session->userdata('user_logged_in');
		$user_id			    = $da['merchantID'];
		}
		$data['startdate'] ='';
		$data['report_type'] ='1';

	
    	$condition 			 	= array("comp.merchantID"=>$user_id);
		$invoices    			= $this->company_model->get_invoice_data_by_due($condition);  
 		$data['report1'] 	    = $invoices; 					
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('company/page_reports', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	

 
	public function transaction_reports()
	{
	   
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}
			$today 			    = date('Y-m-d');
		$data['report_type']	= $this->czsecurity->xssCleanPostInput('report_type');	
	   	$data['startdate'] ='';
	   
	    if($this->czsecurity->xssCleanPostInput('report_type')=='1' ){
		
     	$condition 			 	= array("comp.merchantID"=>$user_id);
		$invoices    			= $this->company_model->get_invoice_data_by_due($condition);   	
		
		$data['report1']		= 	$invoices;
	
	  }		
			
	else if($this->czsecurity->xssCleanPostInput('report_type')=='2'){
		
     	$condition1 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,"IsPaid"=>"false",  "comp.merchantID"=>$user_id);   
	
	    $invoices    			 = $this->company_model->get_invoice_data_by_due($condition1);  		
		 $data['report2']	   	 = 	$invoices;
		 
		
   	
	}
	
	
			
		else if($this->czsecurity->xssCleanPostInput('report_type')=='3'){
		
		$invoices    			 = $this->company_model->get_invoice_data_by_past_due($user_id); 
		$data['report3']		 = 	$invoices;
	 
	
	}
	
	
		
		else if($this->czsecurity->xssCleanPostInput('report_type')=='4'){
		$invoices    			 = $this->company_model->get_invoice_data_by_past_time_due($user_id); 
		 $data['report4']		 = 	$invoices;
	
	}
	
	else if($this->czsecurity->xssCleanPostInput('report_type')=='5'){
		$invoices              = array();
		$invoices    			 = $this->company_model->get_transaction_failure_report_data($user_id); 
	
	
	
	
		$data['report5']		 = 	$invoices;

	}
	
	else if($this->czsecurity->xssCleanPostInput('report_type')=='6'){
		 $this->load->library('encrypt');
		       $report=array();
		   $card_data =  $this->get_credit_card_info($user_id);
	      
		   foreach($card_data as $key=> $card){
		       
		         $condition      =  array('ListID'=>$card['customerListID']);
			     $customer_data  = $this->general_model->get_row_data('chargezoom_test_customer',$condition);
			     $customer_card['ListID']  = $customer_data['ListID'] ;
                 $customer_card['CardNo']  = substr($this->card_model->decrypt($card['CustomerCard']),12) ;
				 $customer_card['expired_date'] = $card['expired_date'] ;
                 $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['Contact']  = $customer_data['Contact'] ;
				 $customer_card['FullName']  = $customer_data['FullName'] ;
				 $customer_card['companyName']  = $customer_data['companyName'] ;
			     $report[$key] = $customer_card;
            		   
		   }
		 
    
			$data['report6']		 = 	$report;
			
	}
	
	else if($this->czsecurity->xssCleanPostInput('report_type')=='8' ||  $this->czsecurity->xssCleanPostInput('report_type')=='9' || $this->czsecurity->xssCleanPostInput('report_type')=='10'  ){
	
    	$type					 =	$this->czsecurity->xssCleanPostInput('report_type');
		
	   if($type=='10'){
	   $invoices   				 = 	 $this->company_model->get_schedule_payment_invoice($user_id);
	   $data['report10']		 = 	$invoices;
	   }else{
		$invoices    			 = $this->company_model->get_invoice_data_open_due($user_id, $type); 
		  $data['report89']		 = 	$invoices;
		}
		
	}
	
	
else	if($this->czsecurity->xssCleanPostInput('report_type')=='7'){
	
	 $startdate = date('Y-m-d',strtotime($this->czsecurity->xssCleanPostInput('startDate')));
     $enddate   = date('Y-m-d',strtotime($this->czsecurity->xssCleanPostInput('endDate')));
	 $data['startdate']   = date('m/d/Y', strtotime($startdate));
	 $data['enddate']     =  date('m/d/Y', strtotime($enddate)); 
	    $invoices   				 = 	 $this->company_model->get_transaction_report_data($user_id,$startdate,  $enddate);
	  $data['report7']				 = 	$invoices;
	
	}else{
		$condition 			 	= array("comp.merchantID"=>$user_id);
		$invoices    			= $this->company_model->get_invoice_data_by_due($condition);   	
		
		$data['report1']		= 	$invoices;
	
	
	}
	    $this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('company/page_reports', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	
	
	
 }	
	
 
 	public function export_csv_old()
	{
	$delimiter = ","; 
	$newline = "\r\n";
	$enclosure = '"';
	
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}

		$today 			        = date('Y-m-d');
     $condition 			 	= array("comp.merchantID"=>$user_id);
	$this->load->dbutil(); // call db utility library
	$this->load->helper('download'); // call download helper
	
      $type = $this->uri->segment(4);  
	 
	
    if($type=='1' || $type=='2' ){
	
        $condition 		    =  array("comp.merchantID"=>$user_id);
		$condition1 		= array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,"IsPaid"=>"false",  "comp.merchantID"=>$user_id); 
		
		$this->db->select(" `cust`.companyName, `inv`.Customer_FullName, sum(inv.BalanceRemaining) as balance, `cust`.Contact, (case when cust.ListID !='' then 'Active' else 'InActive' end) as status ,  ");
		$this->db->from('chargezoom_test_invoice inv ');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->group_by('inv.Customer_ListID');
		$this->db->order_by('balance','desc');
	    if($type=='1'){
	    $this->db->where($condition);
		}else if($type=='2'){
		$this->db->where($condition1);
		}
		$this->db->where('cust.customerStatus','1');	  
		
		$query = $this->db->get();
		
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
        
        $keys = array();
        if(!empty($result_set))
	{
		foreach($result_set as $value1)
		
		$keys = array_keys($value1);
	}	
	
		$a2=array("Company", "Customer Name", "Amount", "Email", "Status", );
		$final_keys = array_replace($keys, $a2); 
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
				  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		}
	}	
if($type=='3' ){	
	  
		$query  =  $this->db->query("SELECT `inv`.TxnID,  cust.FullName, cust.Contact, inv.DueDate, `inv`.BalanceRemaining,  inv.TimeCreated, 
		(case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Scheduled' 
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  then 'Past Due'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
		else 'Canceled' end ) as status 
		FROM chargezoom_test_invoice inv 
		INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
		INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
		WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `IsPaid` = 'false' and userStatus ='' and BalanceRemaining !='0.00' and 
	  
		`comp`.`merchantID` = '$user_id'  and cust.customerStatus='1'     order by  BalanceRemaining   desc limit 10 ");
		
		
		$result_set = $query->result_array(); 
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
	$keys = array();
	if(!empty($result_set))
	{
		foreach($result_set as $value1)
		
		$keys = array_keys($value1);
	}
	
		$a2=array("TxnID", "Customer Name", "Email",  "Due Date", "Amount", 
		"Time Created",  "Status");
		$final_keys = array_replace($keys, $a2); 
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
				  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		}
	  
	}
	 if($type=='4' ){	
	 

	  
	  $query  =  $this->db->query("SELECT `inv`.RefNumber, `inv`.TxnID, cust.FullName, cust.Contact, inv.DueDate,  `inv`.BalanceRemaining, inv.TimeCreated, 
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from customer_transaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM chargezoom_test_invoice inv 
	  	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
		    INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  AND `IsPaid` = 'false' and userStatus ='' and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$user_id'  and cust.customerStatus='1'   order by  DueDate   asc limit 10 "); 
	  
	  $result_set = $query->result_array(); 
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
	$keys = array();
	if(!empty($result_set))
	{
		foreach($result_set as $value1)
		
		$keys = array_keys($value1);
	}
		
		$a2=array("Txn_ID", "TxnID", "Customer", "Email", "Due Date", "Amount","Time Created", "Status");
		$final_keys = array_replace($keys, $a2); 
		unset($final_keys[0]);
		
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
			unset($values[0]);	  
			 
			 
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		}
	  
	
	}
	if($type=='5'){
	  
		 $sql ="SELECT `tr`.`id`, `tr`.`transactionID`, `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`, (select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber, `cust`.`FullName` FROM (`customer_transaction` tr) INNER JOIN `chargezoom_test_customer` cust ON `tr`.`customerListID` = `cust`.`ListID` INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID` WHERE `comp`.`merchantID` = '$user_id' AND transactionCode NOT IN ('200','1','100') AND `cust`.`customerStatus` = '1'";
		$query = $this->db->query($sql); 
		
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	$keys = array();
		if(!empty($result_set))
	{
		foreach($result_set as $value1)
		
		$keys = array_keys($value1);
	}
    	$a2=array("ID","Transaction ID", "Customer", "Transaction Date", "Amount", "Invoice ID", "Type" );
		$final_keys = array_replace($keys, $a2); 
		unset($final_keys[0]);
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
			unset($values[0]);	  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
	
	}

   if($type=='6'){
   
	   	 
		 $this->load->library('encrypt');
		       $report=array();
		   $card_data =  $this->card_model->get_credit_card_info_data($user_id);
		   
		  
		    foreach($card_data as $key=> $card){
		       
		         $condition      =  array('ListID'=>$card['customerListID']);
			     $customer_data  = $this->general_model->get_row_data('chargezoom_test_customer',$condition);
			     $customer_card['FullName']  = $customer_data['FullName'] ;
			      $customer_card['companyName']  = $customer_data['companyName'] ;
			     $customer_card['Contact']  = $customer_data['Contact'] ;
                 $customer_card['CardNo']  = substr($this->card_model->decrypt($card['CustomerCard']),12) ;
                  $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['expired_date'] = $card['expired_date'] ;
          
			     $report[$key] = $customer_card;
            		   
		   }
		
		  $result_set =  $report;
		  
		  
        header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w');
		
		$keys = array();
        if(!empty($result_set)){
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
		$a2=array( "Customer", "Company", "Email", "Card ", "Card Frindly Name", "Expiry");
		$final_keys = array_replace($keys, $a2); 
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
	 
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 

		  
	
	}
	
	 if($type=='7'){
   
	   	  $minvalue = $this->uri->segment(4);  
	      $maxvalue = $this->uri->segment(5);  
	
	      $trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
		$this->db->select('tr.transactionID, (select RefNumber from chargezoom_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber, cust.FullName,  tr.transactionAmount, tr.transactionDate, tr.transactionStatus, tr.custom_data_fields
			');
		$this->db->from('customer_transaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
	    $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where('comp.merchantID ', $user_id);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue); 
		$this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
    	$keys = array();
        if(!empty($result_set))
	{
		foreach($result_set as $value1)
		
		$keys = array_keys($value1);
	unset($keys[6]);
	}	
	
		$a2=array("TxnID ", "Invoice ID ","FullName",  "Amount", "Date", "Remarks");
		
		$final_keys = array_replace($keys, $a2); 
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$invoice_id = '';
            $custom_data = $value1['custom_data_fields'];
            if($custom_data){
                $json_data = json_decode($custom_data, 1);
                if(isset($json_data['invoice_number'])){
                    $invoice_id = $json_data['invoice_number'];
                }
            }
            unset($value1['custom_data_fields']);

            if($invoice_id){
            	$value1['RefNumber'] = $invoice_id;
            }
			$values = array_values($value1);
  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
	
	
	}
	
	
	
  	
	 if($type=='10'){
	 
		
		$result_set = $this->company_model->get_schedule_payment_invoice($user_id);
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
   $keys = array();
   	if(!empty($result_set))
	{
		foreach($result_set as $value1)
		
		$keys = array_keys($value1);
	}
		
		$a2=array("Invoice","Full Name",  "Balance", "Due Date","Paid Amount","Scheduled Date");
		$final_keys = array_replace($keys, $a2); 
	
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
		
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
	   }
	   
	   if($type=='8' || $type=='9'){
	   $con = '';
		if($type=='8')
		{
			 $con.="and `inv`.BalanceRemaining !='0.00' and `inv`.`IsPaid` = 'false' ";
		}
	  $query  =  $this->db->query("SELECT  inv.RefNumber, cust.FullName, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining , inv.TimeCreated, 
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	  
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  
	  else 'Canceled' end ) as status 


	  FROM chargezoom_test_invoice inv 
	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   	  
	  `comp`.`merchantID` = '$user_id'  $con  AND customerStatus='1'  order by  DueDate   asc ");
		
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
		
	$keys = array();	
	if(!empty($result_set))
	{
		foreach($result_set as $value1)
		
		$keys = array_keys($value1);
	}
		
		$a2=array("Invoice ID", "Full Name", "Paid", "Due Date", "Balance", "Added At",  "Status");
		$final_keys = array_replace($keys, $a2); 
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
  } 
	

  
		 
	}
 
 
	public function report_details_pdf()
	{
   
	    $data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}

	    $type = $this->uri->segment(4);  

		$today 			    = date('Y-m-d');
		
	   
		if($type=='1' ){
			
			$condition 			 	= array("comp.merchantID"=>$user_id);
			$invoices    			= $this->company_model->get_invoice_data_by_due($condition);   	
		
			$data['report1']		= 	$invoices;
			$data['type']		= 	$type;
	
		
		}		
			
		else if($type=='2'){
		
			$condition1 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,"IsPaid"=>"false",  "comp.merchantID"=>$user_id);   
		
			$invoices    			 = $this->company_model->get_invoice_data_by_due($condition1);  		
			$data['report2']	   	 = 	$invoices;
				$data['type']		= 	$type;
			 
		}		
		else if($type=='3'){
		
		$invoices    			 = $this->company_model->get_invoice_data_by_past_due($user_id); 
		$data['report3']		 = 	$invoices;
	    $data['type']		= 	$type;
	
	}
	
	
		
		else if($type=='4'){
		$invoices    			 = $this->company_model->get_invoice_data_by_past_time_due($user_id); 
		 $data['report4']		 = 	$invoices;
	    $data['type']		= 	$type;
	}
	
	else if($type=='5'){
		
		$invoices    			 = $this->company_model->get_transaction_failure_report_data($user_id); 
		$data['report5']		 = 	$invoices;
        $data['type']		= 	$type;
	}
	
	else if($type=='6'){
		   $this->load->library('encrypt');
		       $report=array();
		   $card_data =  $this->card_model->get_credit_card_info($user_id);
		   foreach($card_data as $key=> $card){
		       
		       
            		   
		         $condition      =  array('ListID'=>$card['customerListID']);
			     $customer_data  = $this->general_model->get_row_data('chargezoom_test_customer',$condition);
						     $customer_card['ListID']  = $customer_data['ListID'] ;
                 $customer_card['CardNo']  = substr($this->card_model->decrypt($card['CustomerCard']),12) ;
				 $customer_card['expired_date'] = $card['expired_date'] ;
                 $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['Contact']  = $customer_data['Contact'] ;
				 $customer_card['FullName']  = $customer_data['FullName'] ;
				 $customer_card['companyName']  = $customer_data['companyName'] ;
			     $report[$key] = $customer_card;
            	 
	
            	
		   }
		   
		
			$data['report6']		 = 	$report;
		    $data['type']		= 	$type;
	}
	
	else if($type=='8' ||  $type=='9' || $type=='10'  ){
	
    	
		
	   if($type=='10'){
	   $invoices   				 = 	 $this->company_model->get_schedule_payment_invoice($user_id);
	   $data['report10']		 = 	$invoices;
	   $data['type']		= 	$type;
	   }else{
		$invoices    			 = $this->company_model->get_invoice_data_open_due($user_id, $type); 
		  $data['report89']		 = 	$invoices;
		  $data['type']		= 	$type;
		}
		
	}
   else	if($type=='7'){
	     $startdate = $this->uri->segment(5);  
	     $enddate   = $this->uri->segment(6); 
	
	  $invoices   				 = 	 $this->company_model->get_transaction_report_data($user_id,$startdate,  $enddate);
	  $data['report7']				 = 	$invoices;
	  $data['type']		= 	$type;
	
	}else{
		$condition 			 	= array("companyID"=>$user_id);
		$invoices    			= $this->company_model->get_invoice_data_by_due($condition);   	
		
		$data['report1']		= 	$invoices;
		$data['type']		= 	$type;
		
	
	
	}
		
	     	$no = 'Report'.$type;
			$pdfFilePath = "$no.pdf"; 
			
			 ini_set('memory_limit','320M'); 
	 	
	     $html= $this->load->view('company/page_report_pdf', $data,true); 
	     
    		 $this->load->library('pdf');
			 $pdf = $this->pdf->load();

			 $pdf->WriteHTML($html); // write the HTML into the PDF
  
	      
			 $pdf->Output($pdfFilePath, 'D'); // save to file because we can
  
      
	}
	
	
	
	public function get_credit_card_info($compID)
	{
		$card_data=array();
	  
	  $this->db1 = $this->load->database('otherdb', TRUE);
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
		 $query1   = $this->db1->query($sql);
		$card_data =   $query1->result_array();
		return $card_data;
	}
	
	
	
	public function get_credit_card_info_data($compID)
	{
		 $this->db1 = $this->load->database('otherdb', TRUE);
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
    	 $query1   = $this->db1->query($sql);
	    return $query1->result_array();
	}
	
	
	
		
	public function billing_report(){
	    $data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}
	    $invoiceID = $this->uri->segment(3);  
		
		$condition 			 	= array("invoice"=>$invoiceID);
		$invoices    			= $this->company_model->get_billing_invoice_data($invoiceID);   	
		
		$data['invoice1']		= 	$invoices; 

		$today 			    = date('Y-m-d');
		
		$no = 'Billing'.$invoiceID;
		$pdfFilePath = "$no.pdf"; 
		
		 ini_set('memory_limit','32M'); 
		
		$html= $this->load->view('company/page_billing_pdf', $data,true);
		
		 $this->load->library('pdf');
		 $pdf = $this->pdf->load();
		 $pdf->WriteHTML($html); // write the HTML into the PDF
		 $pdf->Output($pdfFilePath, 'D'); // save to file because we can    
	}	
	

	public function export_csv()
    {
        $delimiter = ","; 
		$newline = "\r\n";
		$enclosure = '"';

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        } elseif ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        
		$today 			        = date('Y-m-d');
		$condition 			 	= array("inv.merchantID"=>$user_id);
		$this->load->dbutil(); // call db utility library
		$this->load->helper('download'); // call download helper

        $report_type = $this->uri->segment(4);
		$final_array = [];

		if($report_type == ""){
			$report_type = 1;
		}

        if ($report_type == '1' || $report_type == '2') {

            $condition1 			 	= array("comp.merchantID"=>$user_id);

			if($report_type == '2'){
				$condition1["DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "] = $today;
				$condition1["IsPaid"] = "false";
			}
		
			$result_set    			 = $this->company_model->get_invoice_data_by_due($condition1); 
			$final_keys         = array("Company", "Customer Name", "Full Name", "Amount", "Email", "Status");

			foreach ($result_set as $value1) {
				$final_array[] = array(
					"Company" => $value1['companyName'],
					"Customer Name" => $value1['Customer_FullName'],
					"Full Name" => $value1['FirstName'].' '.$value1['LastName'],
					"Amount" => $value1['balance'],
					"Email" => $value1['Contact'],
					"Status" => $value1['status']
				);
			}

        } else if ($report_type == '3' || $report_type == '4') {
			if($report_type == '3')
				$result_set = $this->company_model->get_invoice_data_by_past_due($user_id); 
			if($report_type == '4')
				$result_set = $this->company_model->get_invoice_data_by_past_time_due($user_id);
            
			$final_keys         = array("Invoice", "Customer Name", "Email", "Days Delinquent", "Amount");

			foreach ($result_set as $value1) {
				$final_array[] = array(
					"Invoice" => $value1['RefNumber'],
					"Customer Name" => $value1['FullName'],
					"Email" => $value1['Contact'],
					"Days Delinquent" => $value1['tr_Day'],
					"Amount" => $value1['BalanceRemaining'],
				);
			}

        } else if ($report_type == '5') {

            $result_set = $this->company_model->get_transaction_failure_report_data($user_id);
			$final_keys         = array("Transaction ID", "Customer Name", "Invoice", "Amount", "Type", "Date","Reference");
			foreach ($result_set as $value1) {
				$final_array[] = array(
					"Transaction ID" => ($value1['transactionID']) ? $value1['transactionID'] : $value1['id'],
					"Customer Name" => $value1['FullName'],
					"Invoice" => $value1['RefNumber'],
					"Amount" => $value1['transactionAmount'],
					"Type" => $value1['transactionType'],
					"Date" => date('M d, Y', strtotime($value1['transactionDate'])),
					"Reference" => $value1['transactionStatus'],
				);
			}

        } else if ($report_type == '6') {

			$final_keys         = array("Customer Name", "Email", "Card Number", "Expiration Date", "Status");

            $this->load->library('encrypt');
            $report    = array();
            $card_data = $this->card_model->get_credit_card_info($user_id);

            foreach ($card_data as $key => $card) {

                $condition      =  array(
					'ListID'=>$card['customerListID'],
					"qbmerchantID"=>$user_id,
				);

				$customer_data  = $this->general_model->get_row_data('qb_test_customer',$condition);
				$customer_card['ListID']  = $customer_data['ListID'] ;
				$customer_card['CardNo']  = substr($this->card_model->decrypt($card['CustomerCard']),12) ;
				$customer_card['expired_date'] = $card['expired_date'] ;
				$customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				$customer_card['Contact']  = $customer_data['Contact'] ;
				$customer_card['FullName']  = $customer_data['FullName'] ;
				$customer_card['companyName']  = $customer_data['companyName'] ;
				$report[$key] = $customer_card;


                $final_array[] = array(
					"Customer Name" => $customer_card['FullName'],
					"Email" => $customer_card['Contact'],
					"Card Number" => $customer_card['CardNo'],
					"Expiration Date" => date('m/Y', strtotime($customer_card['expired_date'])),
					"Status" => $customer_data['Expired'],
				);
            }
        } else if ($report_type == '8' || $report_type == '9' || $report_type == '10') {
            if ($report_type == '10') {
                $result_set = $this->company_model->get_schedule_payment_invoice($user_id);
            } else {
                $result_set = $this->company_model->get_invoice_data_open_due($user_id, $report_type);
            }

			$final_keys         = array("Invoice", "Customer Name", "Added On", "Days Deliquent", "Paid", "Balance","Status");
			foreach ($result_set as $value1) {
				$final_array[] = array(
					"Invoice" => ($value1['RefNumber'])?$value1['RefNumber']:'----',
					"Customer Name" => $value1['FullName'],
					"Added On" => date('M d, Y', strtotime($value1['addOnDate'])),
					"Days Deliquent" => $value1['tr_Day'],
					"Paid" => number_format($value1['AppliedAmount'],2),
					"Balance" => number_format($value1['BalanceRemaining'],2),
					"Status" => $value1['status'],
				);
			}

        } else if ($report_type == '7') {
            $startdate         = date('Y-m-d', strtotime($this->uri->segment(5)));
            $enddate           = date('Y-m-d', strtotime($this->uri->segment(6)));
            $result_set          = $this->company_model->get_transaction_report_data($user_id, $startdate, $enddate);
			$final_keys         = array("Transaction ID", "Customer Name", "Invoice", "Amount", "Date", "Type", "Remark","Status");
			foreach ($result_set as $value1) {

				if($value1['transactionCode'] == 200 || $value1['transactionCode'] == 100 || $value1['transactionCode'] == 1 || $value1['transactionCode'] == 111){
					$tr_status = "Success";
				}else{
					$tr_status ="Failed";	   
				}
				if(isset($value1['transactionType']) && strpos(strtolower($value1['transactionType']), 'refund') !== false){
					$transactionAmountCSV = '('.priceFormat($value1['transactionAmount'], true).')';
				}else{ 
					$transactionAmountCSV =  priceFormat($value1['transactionAmount'], true);
				}
				$final_array[] = array(
					"Transaction ID" => $value1['transactionID'],
					"Customer Name" => $value1['FullName'],
					"Invoice" => $value1['RefNumber'],
					"Amount" => $transactionAmountCSV,
					"Date" => date('M d, Y', strtotime($value1['transactionDate'])),
					"Type" => $value1['transactionType'],
					"Remark" => $value1['transactionStatus'],
					"Status" => $tr_status,
				);
			}
        }

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w');

		fputcsv($output, $final_keys);

		if(!empty($final_array)){
			foreach ($final_array as $reportList) {
				fputcsv($output, $reportList);
			}
		}
    }
}