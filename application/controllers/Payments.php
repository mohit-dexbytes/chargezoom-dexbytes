<?php

/**
 * This Controller has NMI Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * Perform Customer Card oprtations using this controller
 * Create, Delete, Modify 
 */

class Payments extends CI_Controller
{
	private $resellerID;
	private $transactionByUser;

	public function __construct()
	{
		parent::__construct();


		include APPPATH . 'third_party/nmiDirectPost.class.php';

		include APPPATH . 'third_party/nmiCustomerVault.class.php';

		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('card_model');

		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->db1->query("SET SESSION sql_mode = ''");
        $this->db->query("SET SESSION sql_mode = ''");
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2') {
			$logged_in_data = $this->session->userdata('logged_in');
			$this->resellerID = $logged_in_data['resellerID'];
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		} else {
			redirect('login', 'refresh');
		}
	}


	public function index()
	{

		redirect('Payments/payment_transaction', 'refresh');
	}




	public function delete_customer()
	{

		$customerID  = $this->czsecurity->xssCleanPostInput('custID');
		$customer    = $this->customer_model->customer_by_id($customerID);
		$user       = $customer->qbwc_username;
		$condition   = array('ListID' => $customerID);
		$update_data = array('customerStatus' => '0');
		if ($this->general_model->update_row_data('qb_test_customer', $condition, $update_data)) {
			$this->session->set_flashdata('success', 'Successfully Deleted');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: In delete process.</strong></div>');
		}

		redirect('home/customer', 'refresh');
	}






	public function update_invoice_date()
	{


		if (!empty($this->czsecurity->xssCleanPostInput('schedule_date'))) {
			$invoiceID = $this->czsecurity->xssCleanPostInput('scheduleID');
			$due_date  = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date')));

			$condition =  array('TxnID' => $invoiceID);
			$indata    = $this->customer_model->get_invoice_data_byID($invoiceID);



			if (!empty($indata)) {
				$update_data = array('DueDate' => $due_date);
				$this->general_model->update_row_data('qb_test_invoice', $condition, $update_data);
				$ransn = rand(200000, 700000);
				$insert_arr = array();

				$insert_arr['TxnID']        = $indata['TxnID'];
				$insert_arr['EditSequence'] = $indata['EditSequence'];
				$insert_arr['RefNumber'] = $indata['RefNumber'];
				$insert_arr['TimeCreated'] = date('Y-m-d H:i:s', strtotime($indata['TimeCreated']));
				$insert_arr['TimeModified'] = date('Y-m-d H:i:s');
				$insert_arr['Customer_ListID']      = $indata['Customer_ListID'];
				$insert_arr['Customer_FullName']     = $indata['Customer_FullName'];
				$insert_arr['ShipAddress_Addr1']        = $indata['ShipAddress_Addr1'];
				$insert_arr['ShipAddress_Addr2']       = $indata['ShipAddress_Addr2'];
				$insert_arr['ShipAddress_City']        = $indata['ShipAddress_City'];
				$insert_arr['ShipAddress_State']      = $indata['ShipAddress_State'];
				$insert_arr['ShipAddress_Country']     = $indata['ShipAddress_Country'];
				$insert_arr['ShipAddress_PostalCode']     = $indata['ShipAddress_PostalCode'];
				$insert_arr['AppliedAmount']     = $indata['AppliedAmount'];
				$insert_arr['BalanceRemaining']     = $indata['BalanceRemaining'];

				$insert_arr['IsPaid']      = $indata['IsPaid'];

				$insert_arr['invoicelsID'] =  $indata['TxnID'];
				$insert_arr['DueDate']        = $due_date;
				$insert_arr['emailRecurring'] = 0;
				$insert_arr['invoiceRefNumber'] = $indata['RefNumber'];
				$insert_arr['insertInvID']      = $ransn;
				$insert_arr['freeTrial']        = date('Y-m-d H:i:s');
				$insert_arr['gatewayID']        = 0;
				$insert_arr['autoPayment']      = 0;
				$insert_arr['cardID']         = 0;
				$insert_arr['qb_status']        = '1';
				$insert_arr['invoicelsID'] =  $indata['TxnID'];

				$quer = $this->db->query("Select * from tbl_custom_invoice where  (invoicelsID='" . $indata['TxnID'] . "' or insertInvID='" . $indata['TxnID'] . "')   ");
				if ($quer->num_rows() > 0) {

					$res = $quer->row_array();
					if ($res['invoicelsID'] != '')
						$this->db->where(array('invoicelsID' => $indata['TxnID']));
					else
						$this->db->where(array('insertInvID' => $indata['TxnID']));

					$this->db->update('tbl_custom_invoice', $insert_arr);

				} else {
					$this->db->insert('tbl_custom_invoice', $insert_arr);
				}
			
				$user = $indata['company_qb_username'];
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_INVOICE, $ransn, '1', '', $user);

				$this->session->set_flashdata('success', ' Successfully Updated');
				redirect('home/invoices', 'refresh');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> This is not valid Invoice.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Any Date not Selected</div>');
		}
		redirect('home/invoices', 'refresh');
	}





	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
		if ($this->session->userdata('logged_in')) {


			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$checkPlan = check_free_plan_transactions();

		$cardID_upd = '';
		$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		$custom_data_fields = [];
		$cardID = $this->czsecurity->xssCleanPostInput('CardID');
		if (!$cardID || empty($cardID)) {
		$cardID = $this->czsecurity->xssCleanPostInput('schCardID');
		}

		$gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
		if (!$gatlistval || empty($gatlistval)) {
		$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
		}
		$gateway = $gatlistval;

		$sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

		$result['transactionid'] = '';
		$cusproID = '';
		$error = '';
		$cusproID = $this->czsecurity->xssCleanPostInput('customerProcessID');
		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;
			$in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			if ($checkPlan && $gatlistval != "" && !empty($gt_result) && $sch_method == 2) {
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  = $gt_result['gatewayPassword'];
				$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

				$Customer_ListID = $in_data['Customer_ListID'];

				$customerID = $Customer_ListID;
				$comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName', 'FirstName', 'LastName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
				$companyID  = $comp_data['companyID'];
				$user = $in_data['qbwc_username'];

				$transaction = new nmiDirectPost($nmi_data);

				if ($cardID != "new1") {
					$accountDetails =   $this->card_model->get_single_card_data($cardID);
					$transaction->setAccountName($in_data['FullName']);
					$transaction->setAccount($accountDetails['accountNumber']);
					$transaction->setRouting($accountDetails['routeNumber']);
					$transaction->setAccountType($accountDetails['accountType']);
					$transaction->setAccountHolderType($accountDetails['accountHolderType']);
					$transaction->setSecCode($accountDetails['secCodeEntryMethod']);

					$transaction->setPayment('check');
				} else {

					$transaction->setAccountName($this->czsecurity->xssCleanPostInput('acc_name'));
					$transaction->setAccount($this->czsecurity->xssCleanPostInput('acc_number'));
					$transaction->setRouting($this->czsecurity->xssCleanPostInput('route_number'));

					$sec_code = "WEB";

					$transaction->setAccountType($this->czsecurity->xssCleanPostInput('acct_type'));
					$transaction->setAccountHolderType($this->czsecurity->xssCleanPostInput('acct_holder_type'));
					$transaction->setSecCode($sec_code);

					$transaction->setPayment('check');

					$accountDetails = [
						'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
						'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
						'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
						'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
						'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
						'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
						'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
						'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
						'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
						'customerListID'     => $customerID,
						'companyID'          => $companyID,
						'merchantID'         => $user_id,
						'createdAt'          => date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
					];
				}
				$accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				#Billing Details
				$transaction->setCompany($comp_data['companyName']);
				$transaction->setFirstName($comp_data['FirstName']);
				$transaction->setLastName($comp_data['LastName']);
				$transaction->setAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction->setAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction->setPhone($this->czsecurity->xssCleanPostInput('contact'));

				#Shipping Details
				$transaction->setShippingCompany($comp_data['companyName']);
				$transaction->setShippingFirstName($comp_data['FirstName']);
				$transaction->setShippingLastName($comp_data['LastName']);
				$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
				
				$amount = $this->czsecurity->xssCleanPostInput('inv_amount');
				$transaction->setAmount($amount);
				$transaction->setTax('tax');
				$transaction->sale();

				$result = $transaction->execute();
				
				if ($result['response_code'] == '100') {
					$txnID        = $in_data['TxnID'];

					$ispaid 	   = 'true';
					$bamount      = $in_data['BalanceRemaining'] - $amount;
				
					if ($bamount > 0){
						$ispaid 	 = 'false';
					}
						
					$app_amount = $in_data['AppliedAmount'] - $amount;
					$data   	 = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
					$condition  = array('TxnID' => $in_data['TxnID']);

					$this->general_model->update_row_data('qb_test_invoice', $condition, $data);
					$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
					$customerID = '';
					$ref_number = '';
					$tr_date   = date('Y-m-d H:i:s');
					$toEmail = $comp_data['Contact'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['FullName'];

					if ($cardID == "new1") {
						$this->card_model->process_ack_account($accountDetails);
					}
					
					$this->session->set_flashdata('success', ' Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
				}

				
				$transactiondata = array();
				$transactiondata['transactionID']      = $result['transactionid'];
				$transactiondata['transactionStatus']  = $result['responsetext'];
				$transactiondata['transactionCode']    =  $result['response_code'];
				$transactiondata['transactionType']   =  'sale';
				$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
				$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
				$transactiondata['invoiceTxnID']       = $in_data['TxnID'];
				$transactiondata['gatewayID']          = $gateway;
				$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
				$transactiondata['customerListID']     = $in_data['Customer_ListID'];
				$transactiondata['transactionAmount']  = $amount;
				$transactiondata['merchantID']   = $user_id;
				$transactiondata['gateway']   = 'NMI ECheck';
				$transactiondata['resellerID']   = $this->resellerID;
				$transactiondata = alterTransactionCode($transactiondata);
				$CallCampaign = $this->general_model->triggerCampaign($user_id,$transactiondata['transactionCode']);

				if(!empty($this->transactionByUser)){
					$transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
					$transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				if($custom_data_fields){
	                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
	             }
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				if ($result['response_code'] == '100' && !is_numeric($in_data['TxnID'])) {
					$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
				}
		}
		if ($checkPlan && !empty($cardID) && !empty($gateway) && $sch_method == 1) {

			$in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

			$nmiuser   = $gt_result['gatewayUsername'];
			$nmipass   = $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
			$gatewayName = getGatewayName($gt_result['gatewayType']);
			$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

			if (!empty($in_data)) {

				$Customer_ListID = $in_data['Customer_ListID'];
				$customerID = $Customer_ListID;
				$comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName', 'FirstName', 'LastName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
				$companyID  = $comp_data['companyID'];
				if ($cardID == 'new1') {


					$cardID_upd  = $cardID;
					$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
				} else {


					$card_data    =   $this->card_model->get_single_card_data($cardID);
					$card_no  = $card_data['CardNo'];
					$cvv      =  $card_data['CardCVV'];
					$expmonth =  $card_data['cardMonth'];
					$exyear   = $card_data['cardYear'];
				}
				$cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				if (!empty($cardID)) {

					if ($in_data['BalanceRemaining'] > 0) {
						$cr_amount = 0;
						$amount    =	 $in_data['BalanceRemaining'];


						$amount  = $this->czsecurity->xssCleanPostInput('inv_amount');
						$amount = $amount - $cr_amount;
						$transaction1 = new nmiDirectPost($nmi_data);
						$transaction1->setCcNumber($card_no);

						$exyear   = substr($exyear, 2);
						if (strlen($expmonth) == 1) {
							$expmonth = '0' . $expmonth;
						}
						$expry    = $expmonth . $exyear;
						$transaction1->setCcExp($expry);
						if ($cvv != "")
							$transaction1->setCvv($cvv);
	
						$transaction1->setAmount($amount);
						
						// add level III data
                        $level_request_data = [
                            'transaction' => $transaction1,
                            'card_no' => $card_no,
                            'merchID' => $user_id,
                            'amount' => $amount,
                            'invoice_id' => $invoiceID,
                            'gateway' => 1
                        ];
                        $transaction1 = addlevelThreeDataInTransaction($level_request_data);

						#Billing Details
						$transaction1->setCompany($comp_data['companyName']);
						$transaction1->setFirstName($comp_data['FirstName']);
						$transaction1->setLastName($comp_data['LastName']);
						$transaction1->setAddress1($this->czsecurity->xssCleanPostInput('address1'));
						$transaction1->setAddress2($this->czsecurity->xssCleanPostInput('address2'));
						$transaction1->setCountry($this->czsecurity->xssCleanPostInput('country'));
						$transaction1->setCity($this->czsecurity->xssCleanPostInput('city'));
						$transaction1->setState($this->czsecurity->xssCleanPostInput('state'));
						$transaction1->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
						$transaction1->setPhone($this->czsecurity->xssCleanPostInput('contact'));

						#Shipping Details
						$transaction1->setShippingCompany($comp_data['companyName']);
						$transaction1->setShippingFirstName($comp_data['FirstName']);
						$transaction1->setShippingLastName($comp_data['LastName']);
						$transaction1->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
						$transaction1->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
						$transaction1->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
						$transaction1->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
						$transaction1->setShippingState($this->czsecurity->xssCleanPostInput('state'));
						$transaction1->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));

						$transaction1->sale();
						$result = $transaction1->execute();
						if ($result['response_code'] == "100") {
							$txnID        = $in_data['TxnID'];

							$ispaid 	   = 'true';
							$bamount      = $in_data['BalanceRemaining'] - $amount;
							if ($bamount > 0)
								$ispaid 	 = 'false';
							$app_amount = $in_data['AppliedAmount'] - $amount;
							$data   	 = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
							$condition  = array('TxnID' => $in_data['TxnID']);

							$this->general_model->update_row_data('qb_test_invoice', $condition, $data);
							$user = $in_data['qbwc_username'];

							
							/* Saving Card info block  */

							if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

								$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
								$cardType        = $this->general_model->getType($card_no);


								$expmonth     =    $this->czsecurity->xssCleanPostInput('expiry');
								$exyear       =    $this->czsecurity->xssCleanPostInput('expiry_year');
								$cvv          =    $this->czsecurity->xssCleanPostInput('cvv');

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'    => $cardType,
									'CustomerCard' => $card_no,
									'CardCVV'      => $cvv,
									'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
									'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
									'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
									'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
									'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
									'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
									'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
									'customerListID' => $customerID,

									'companyID'     => $companyID,
									'merchantID'   => $user_id,
									'createdAt' 	=> date("Y-m-d H:i:s")
								);



								$id1 = $this->card_model->process_card($card_data);
							}
							$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
							$ref_number =  $in_data['RefNumber'];
							$tr_date   = date('Y-m-d H:i:s');
							$toEmail = $comp_data['Contact'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['FullName'];
							
							$this->session->set_flashdata('success', 'Successfully Processed Invoice');
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong></div>');
						}
						//transaction array
						$transactionArr = array();
						$transactionArr['transactionID']      = $result['transactionid'];
						$transactionArr['transactionStatus']  = $result['responsetext'];
						$transactionArr['transactionCode']    =  $result['response_code'];
						$transactionArr['transactionType']   =  ($result['type']) ? $result['type'] : 'auto-nmi';
						$transactionArr['transactionDate']    = date('Y-m-d H:i:s');
						$transactionArr['transactionModified'] = date('Y-m-d H:i:s');
						$transactionArr['invoiceTxnID']       = $in_data['TxnID'];
						$transactionArr['gatewayID']          = $gateway;
						$transactionArr['transactionGateway']  = $gt_result['gatewayType'];
						$transactionArr['customerListID']     = $in_data['Customer_ListID'];
						$transactionArr['transactionAmount']  = $amount;
						$transactionArr['merchantID']   = $user_id;
						$transactionArr['gateway']   = $gatewayName;
						$transactionArr['resellerID']   = $this->resellerID;

						$transactionArr = alterTransactionCode($transactionArr);
						$CallCampaign = $this->general_model->triggerCampaign($user_id,$transactionArr['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactionArr['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactionArr['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transactionArr['custom_data_fields']  = json_encode($custom_data_fields);
			             }
						$id = $this->general_model->insert_row('customer_transaction',   $transactionArr);
						if ($result['response_code'] == '100' && !is_numeric($in_data['TxnID']))
							$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);

						if ($result['response_code'] == '100' && $chh_mail == '1') {
							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
						}
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid </strong>.</div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
			}
		} 
		
		if(empty($in_data)){
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card!</strong>.</div>');
		}

		if ($cusproID == "2") {
			redirect('home/view_customer/' . $customerID, 'refresh');
		}
		if ($cusproID == "3" && $in_data['TxnID'] != '') {
			redirect('home/invoice_details/' . $in_data['TxnID'], 'refresh');
		}
		$trans_id = $result['transactionid'];
		$invoice_IDs = array();
			$receipt_data = array(
				'proccess_url' => 'home/invoices',
				'proccess_btn_text' => 'Process New Invoice',
				'sub_header' => 'Sale',
				'checkPlan' => $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			$this->session->set_userdata("in_data",$in_data);
		if ($cusproID == "1") {
			redirect('home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		}
		redirect('home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
	}








	public function get_vault_data($customerID)
	{
		$this->load->library('encrypt');
		$card = array();

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}


		$sql = "SELECT * from customer_card_data  where  customerListID='" . $customerID . "' and merchantID='" . $merchID . "'    order by CardID desc limit 1  ";
		$query1 = $this->db1->query($sql);
		$card_data = $query1->row_array();
		if (!empty($card_data)) {

			$card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']);
			$card['cardMonth']  = $card_data['cardMonth'];
			$card['cardYear']  = $card_data['cardYear'];
			$card['CardID']    = $card_data['CardID'];
			$card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
			$card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
		}

		return  $card;
	}

	public function delete_card_data()
	{
		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->czsecurity->xssCleanPostInput('delCardID'))) {


			$cardID = $this->czsecurity->xssCleanPostInput('delCardID');
			$customer =  $this->czsecurity->xssCleanPostInput('delCustodID');

			$num  = $this->general_model->get_num_rows('tbl_subscriptions', array('CardID' => $cardID, 'merchantDataID' => $merchID));
			if ($num == 0) {
				$sts =  $this->card_model->delete_card_data(array('CardID' => $cardID));
				if ($sts) {
					$this->session->set_flashdata('success', 'Successfully Deleted');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Error: This card cannot be deleted. Please change Customer card on Subscription</strong></div>');
			}
			redirect('home/view_customer/' . $customer, 'refresh');
		}
	}



	public function update_card_data()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');

		$card_data = $this->card_model->get_single_card_data($cardID);
		$con_cust = array('ListID' => $card_data['customerListID'], 'qbmerchantID' => $merchID);
		$c_data = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'FullName', 'Contact'), $con_cust);
		$companyID  = $c_data['companyID'];
		$customer = $card_data['customerListID'];

		$decryptedcvv = '';
		if ($card_data['CardType'] != 'Echeck') {

			$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
			if ($this->czsecurity->xssCleanPostInput('edit_cvv') != '') {
                $cvv      = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('edit_cvv'));
				$decryptedcvv = $this->czsecurity->xssCleanPostInput('edit_cvv');
            }else{
                $cvv      = null;
            }
			
			$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
		}

		if ($this->czsecurity->xssCleanPostInput('edit_acc_number') !== '') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('edit_acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('edit_route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('edit_acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('edit_secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('edit_acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
			$expmonth = $exyear   = 	$cvv = 0;
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
			$card_no  = '';
		}

	
		$b_addr1 = $this->czsecurity->xssCleanPostInput('baddress1');
		$b_addr2 = $this->czsecurity->xssCleanPostInput('baddress2');
		$b_city = $this->czsecurity->xssCleanPostInput('bcity');
		$b_state = $this->czsecurity->xssCleanPostInput('bstate');
		$b_country = $this->czsecurity->xssCleanPostInput('bcountry');
		$b_contact = $this->czsecurity->xssCleanPostInput('bcontact');
		$b_zip = $this->czsecurity->xssCleanPostInput('bzipcode');
		$merchantID = $merchID;
		$is_default = ($this->czsecurity->xssCleanPostInput('defaultMethod') != null )?$this->czsecurity->xssCleanPostInput('defaultMethod'):0;

		$condition = array('CardID' => $cardID);
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,

			'CardCVV'      => $cvv,

			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zip,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'is_default'		 => $is_default,
			'updatedAt' 	=> date("Y-m-d H:i:s")
		);

		$decryptedCard = '';
		if($friendlyname != ''){
        	$insert_array['customerCardfriendlyName'] = $friendlyname;
        }

		if ($this->czsecurity->xssCleanPostInput('edit_card_number') != '') {
			$card_no  = $this->czsecurity->xssCleanPostInput('edit_card_number');
			$decryptedCard = $card_no;
			$insert_array['CustomerCard'] = $this->card_model->encrypt($card_no);
			
			$isSurcharge = $this->card_model->chkCardSurcharge(['cardNumber' => $card_no]);
			$card_type = $this->general_model->getType($card_no);

			$friendlyname = $card_type . ' - ' . substr($card_no, -4);
			$insert_array['customerCardfriendlyName'] = $friendlyname;
			$insert_array['CardType'] = $card_type;
			$insert_array['isSurcharge'] = $isSurcharge;
		}  else {
			$decryptedCard     = $card_data['CardNo'];
		}


		$isAuthorised = true;

		if ($decryptedCard != '') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['FullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}
		if($isAuthorised){
			if($is_default == 1){
				$conditionDefaultSet = array('customerListID' => $customer, 'merchantID' => $merchID);
				$updateData = $this->card_model->update_customer_card_data($conditionDefaultSet,['is_default' => 0]);
			}
			$id = $this->card_model->update_card_data($condition,  $insert_array);
			if ($id) {
				$this->session->set_flashdata('success', 'Successfully Updated Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
		}
		redirect('home/view_customer/' . $customer, 'refresh');
	}




	public function insert_new_data()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}
		$customer = $this->czsecurity->xssCleanPostInput('customerID11');


		$con_cust = array('ListID' => $customer, 'qbmerchantID' => $merchID);
		$c_data = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'FullName', 'Contact'), $con_cust);
		$companyID  = $c_data['companyID'];
		$isSurcharge = 0;
		
		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
			$decryptedCard = $card_no;
			$isSurcharge = $this->card_model->chkCardSurcharge(['cardNumber' => $card_no]);
			$card_type = $this->general_model->getType($card_no);
			$card_no  = $this->card_model->encrypt($card_no);
			$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
			$decryptedcvv      = $this->czsecurity->xssCleanPostInput('cvv');
			$cvv      = ''; 
			
			$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
		}
		if ($this->czsecurity->xssCleanPostInput('formselector') == '2') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
			$expmonth = $exyear   = 	$cvv = 0;

			$card_no = '';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
			$card_type = 'Echeck';
		}

		$b_addr1 = $this->czsecurity->xssCleanPostInput('address1');
		$b_addr2 = $this->czsecurity->xssCleanPostInput('address2');
		$b_city = $this->czsecurity->xssCleanPostInput('city');
		$b_state = $this->czsecurity->xssCleanPostInput('state');
		$b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
		$b_country = $this->czsecurity->xssCleanPostInput('country');
		$b_contact = $this->czsecurity->xssCleanPostInput('contact');
		$is_default = ($this->czsecurity->xssCleanPostInput('defaultMethod') != null )?$this->czsecurity->xssCleanPostInput('defaultMethod'):0;

		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $card_no,
			'CardCVV'      => $cvv,
			'CardType'      => $card_type,
			'customerListID' => $customer,
			'merchantID'     => $merchID,
			'companyID'      => $companyID,
			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zipcode,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'customerCardfriendlyName' => $friendlyname,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'createdAt' 	=> date("Y-m-d H:i:s"),
			'isSurcharge' => $isSurcharge
		);

		$isAuthorised = true;
		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['FullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}

		if($isAuthorised){
			$insert_array['CardCVV'] = '';
			if($is_default == 1){
				$condition = array('customerListID' => $customer, 'merchantID' => $merchID);
				
				$updateData = $this->card_model->update_customer_card_data($condition,['is_default' => 0]);
				
			}else{
				$checkCustomerCard = $this->card_model->get_customer_card_data($customer);
				if(count($checkCustomerCard) == 0){
					$is_default == 1;
				}
	        	
			}
			$insert_array['is_default'] = $is_default;
			$id = $this->card_model->insertBillingdata($insert_array);
			if ($id) {
				$this->session->set_flashdata('success', 'Successfully Inserted Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}
		}

		redirect('home/view_customer/' . $customer, 'refresh');
	}




	public function insert_new_datahhhh()
	{


		if ($this->session->userdata('logged_in')) {

			$merchID 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID 				= $this->session->userdata('user_logged_in')['merchantID'];
		}


		$customer = $this->czsecurity->xssCleanPostInput('customerID11');

		$c_data   = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customer));
		$companyID = $c_data['companyID'];



		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			$card_no  = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('card_number'));
			$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
			$cvv      = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cvv'));
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
			$type = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
			$friendlyname = $type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
		}

		if ($this->czsecurity->xssCleanPostInput('formselector') == '2') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
			$card_no  = $expmonth = $exyear   = 	$cvv = $friendlyname = '';
			$type = 'Echeck';
		}
		$b_addr1 = $this->czsecurity->xssCleanPostInput('address1');
		$b_addr2 = $this->czsecurity->xssCleanPostInput('address2');
		$b_city = $this->czsecurity->xssCleanPostInput('city');
		$b_state = $this->czsecurity->xssCleanPostInput('state');
		$b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
		$b_country = $this->czsecurity->xssCleanPostInput('country');
		$b_contact = $this->czsecurity->xssCleanPostInput('contact');

		$is_default = 0;
     	$checkCustomerCard = checkCustomerCard($customer,$merchID);
    	if($checkCustomerCard == 0){
    		$is_default = 1;
    	}

		$insert_array =  array(
			'cardMonth'         => $expmonth,
			'cardYear'	        => $exyear,
			'CustomerCard'      => $card_no,
			'CardCVV'           => $cvv,
			'CardType'           => $type,
			'customerListID'    => $customer,
			'merchantID'        => $merchID,
			'companyID'         => $companyID,
			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zipcode,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'is_default'			   => $is_default,

			'customerCardfriendlyName' => $friendlyname,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'createdAt' 	          => date("Y-m-d H:i:s")
		);




		$id = $this->db1->insert('customer_card_data', $insert_array);
		if ($id) {
			$this->session->set_flashdata('success', 'Successfully Inserted');
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
		}

		redirect('home/view_customer/' . $customer, 'refresh');
	}





	public function create_customer_sale()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		
		if (!empty($this->input->post(null, true))) {
			$custom_data_fields = [];
			$applySurcharge = false;
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            	$applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

			$checkPlan = check_free_plan_transactions();
			if($checkPlan){
				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

				$inv_array = array();
				$inv_invoice = array();
				$invoiceIDs = array();
				if ($gatlistval != "" && !empty($gt_result)) {

					$nmiuser  = $gt_result['gatewayUsername'];
					$nmipass  = $gt_result['gatewayPassword'];
					$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
					$gatewayName = getGatewayName($gt_result['gatewayType']);
					$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

					if ($this->session->userdata('logged_in')) {
						$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if ($this->session->userdata('user_logged_in')) {
						$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}

					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 =    $this->czsecurity->xssCleanPostInput('address2');
					$city   =  $this->czsecurity->xssCleanPostInput('city');
					$country     =  $this->czsecurity->xssCleanPostInput('country');
					$phone       =  $this->czsecurity->xssCleanPostInput('phone');
					$state       = $this->czsecurity->xssCleanPostInput('state');
					$zipcode    =  $this->czsecurity->xssCleanPostInput('zipcode');

					$companyName    =  $this->czsecurity->xssCleanPostInput('companyName');
					$firstName    =  $this->czsecurity->xssCleanPostInput('firstName');
					$lastName    =  $this->czsecurity->xssCleanPostInput('lastName');
					$address    =  $this->czsecurity->xssCleanPostInput('address');
					$email    =  $this->czsecurity->xssCleanPostInput('email');

					$transaction = new nmiDirectPost($nmi_data);
					$customerID = $this->czsecurity->xssCleanPostInput('customerID');

					if(!$customerID || empty($customerID)){
						$customerID = create_card_customer($this->input->post(null, true), '1');
					}

					$comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
					$companyID  = $comp_data['companyID'];


					$cardID = $this->czsecurity->xssCleanPostInput('card_list');
					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
						$card_no = $this->czsecurity->xssCleanPostInput('card_number');
						$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear1   = substr($exyear, 2);
						$expry    = $expmonth . $exyear1;
						$transaction->setCcExp($expry);
						if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
							$transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));
						}
					} else {



						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no = $card_data['CardNo'];
						$transaction->setCcNumber($card_data['CardNo']);
						$expmonth =  $card_data['cardMonth'];

						$exyear   = $card_data['cardYear'];
						$exyear1   = substr($exyear, 2);
						if (strlen($expmonth) == 1) {
							$expmonth = '0' . $expmonth;
						}
						$expry    = $expmonth . $exyear1;
						$transaction->setCcExp($expry);
						if ($card_data['CardCVV'] != "")
							$transaction->setCvv($card_data['CardCVV']);
					}
					/*Added card type in transaction table*/
	                $cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;

					$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));
					
					#Billing Details
					$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->setAddress1($this->czsecurity->xssCleanPostInput('baddress1'));
					$transaction->setAddress2($this->czsecurity->xssCleanPostInput('baddress2'));
					$transaction->setCountry($this->czsecurity->xssCleanPostInput('bcountry'));
					$transaction->setCity($this->czsecurity->xssCleanPostInput('bcity'));
					$transaction->setState($this->czsecurity->xssCleanPostInput('bstate'));
					$transaction->setZip($this->czsecurity->xssCleanPostInput('bzipcode'));
					$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));

					#Shipping Details
					$transaction->setShippingCompany($this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->setShippingFirstName($this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->setShippingLastName($this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
					$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
					$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
					$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
					$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
					$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
					$transaction->setShippingEmail($this->czsecurity->xssCleanPostInput('email'));


					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_id');
						$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 2);
						$transaction->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $new_invoice_number);
					}

					if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
						$transaction->setPoNumber($this->czsecurity->xssCleanPostInput('po_number'));
					}

					$totalamount  = $this->czsecurity->xssCleanPostInput('totalamount');
					$amount = $this->czsecurity->xssCleanPostInput('totalamount');

					// update amount with surcharge 
	                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
	                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
	                    $amount += round($surchargeAmount, 2);
	                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
	                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
	                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
	                    
	                }
	                $totalamount  = $amount;
					
					$transaction->setAmount($amount);
					// add level III data
	                $level_request_data = [
	                    'transaction' => $transaction,
	                    'card_no' => $card_no,
	                    'merchID' => $user_id,
	                    'amount' => $amount,
	                    'invoice_id' => '',
	                    'gateway' => 1
	                ];
	                $transaction = addlevelThreeDataInTransaction($level_request_data);
					$transaction->sale();
					$result = $transaction->execute();
					
					if ($result['response_code'] == '100') {


						$invoicePayAmounts = array();
						if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
							$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
							$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
						}
						$refnum = array();
						if (!empty($invoiceIDs)) {
							$payIndex = 0;
							$saleAmountRemaining = $amount;
							foreach ($invoiceIDs as $inID) {
								$theInvoice = array();

								$theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

								if (!empty($theInvoice))
								{
									$amount_data = $theInvoice['BalanceRemaining'];
									$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
									if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

	                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
	                                    $actualInvoicePayAmount += $surchargeAmount;
	                                    $updatedInvoiceData = [
	                                        'inID' => $inID,
	                                        'merchantID' => $user_id,
	                                        'amount' => $surchargeAmount,
	                                    ];
	                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,2);
	                                    $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
	                                    $amount_data = $theInvoice['BalanceRemaining'];

	                                }
									$isPaid 	 = 'false';
									$BalanceRemaining = 0.00;
									$refnum[] = $theInvoice['RefNumber'];
									if($saleAmountRemaining > 0){
										if($amount_data == $actualInvoicePayAmount){
											$actualInvoicePayAmount = $amount_data;
											$isPaid 	 = 'true';

										}else{

											$actualInvoicePayAmount = $actualInvoicePayAmount;
											$isPaid 	 = 'false';
											$BalanceRemaining = $amount_data - $actualInvoicePayAmount;
											
											
										}
										$AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
										$tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

										$transactiondata = array();
										$transactiondata['transactionID']       = $result['transactionid'];
										$transactiondata['transactionStatus']   = $result['responsetext'];
										$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
										$transactiondata['transactionCode']     = $result['response_code'];
										$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
										$transactiondata['transactionType']    = $result['type'];
										$transactiondata['gatewayID']          = $gatlistval;
										$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
										$transactiondata['customerListID']      = $customerID;
										$transactiondata['transactionAmount']   = $actualInvoicePayAmount;
										$transactiondata['merchantID']         = $merchantID;
										$transactiondata['invoiceTxnID']      = $inID;
										$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
										$transactiondata['gateway']   = $gatewayName;
										$transactiondata['resellerID']   = $this->resellerID;
										
										if($custom_data_fields){
			                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			                            }

										$transactiondata = alterTransactionCode($transactiondata);
										$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
										if(!empty($this->transactionByUser)){
										    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
										    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
										}
										if($custom_data_fields){
							                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
							             }
										$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

										$comp_data_user = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
										$user      = $comp_data_user['qbwc_username'];

										if(!is_numeric($inID))
											$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);

									}
									
								}
								$payIndex++;	
							}
						} else {

							$transactiondata = array();
							$transactiondata['transactionID']       = $result['transactionid'];
							$transactiondata['transactionStatus']   = $result['responsetext'];
							$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
							$transactiondata['transactionCode']     = $result['response_code'];
							$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
							$transactiondata['transactionType']    = $result['type'];
							$transactiondata['gatewayID']          = $gatlistval;
							$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
							$transactiondata['customerListID']      = $customerID;
							$transactiondata['transactionAmount']   = $totalamount;
							$transactiondata['merchantID']         = $merchantID;
							$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
							$transactiondata['gateway']             = $gatewayName;
							$transactiondata['resellerID']           = $this->resellerID;
							
							$transactiondata = alterTransactionCode($transactiondata);
							if($custom_data_fields){
                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                            }
							$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
							if(!empty($this->transactionByUser)){
							    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
							    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
							}
							if($custom_data_fields){
				                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
				             }
							$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
						}


						if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
							$card_no = $this->czsecurity->xssCleanPostInput('card_number');
							$card_type      = $this->general_model->getType($card_no);
							$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

							$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');

							$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	 => $exyear,
								'CardType'     => $card_type,
								'CustomerCard' => $card_no,
								'CardCVV'      => $cvv,
								'customerListID' => $customerID,
								'companyID'     => $companyID,
								'merchantID'   => $merchantID,

								'createdAt' 	=> date("Y-m-d H:i:s"),
								'Billing_Addr1'	 => $address1,
								'Billing_Addr2'	 => $address2,
								'Billing_City'	 => $city,
								'Billing_State'	 => $state,
								'Billing_Country'	 => $country,
								'Billing_Contact'	 => $phone,
								'Billing_Zipcode'	 => $zipcode
							);



							$this->card_model->process_card($card_data);
						}



						$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
						$ref_number =  implode(',', $refnum);
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['Contact'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['FullName'];
						if ($chh_mail == '1') {
							

							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
						}
						$this->session->set_flashdata('success', 'Transaction Successful');
					} else {

						$transactiondata = array();
						$transactiondata['transactionID']       = $result['transactionid'];
						$transactiondata['transactionStatus']   = $result['responsetext'];
						$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
						$transactiondata['transactionCode']     = $result['response_code'];
						$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
						$transactiondata['transactionType']    = $result['type'];
						$transactiondata['gatewayID']          = $gatlistval;
						$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
						$transactiondata['customerListID']      = $customerID;
						$transactiondata['transactionAmount']   = $totalamount;
						$transactiondata['merchantID']         = $merchantID;
						$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
						$transactiondata['gateway']             = $gatewayName;
						$transactiondata['resellerID']           = $this->resellerID;
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			             }
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' . $result['responsetext'] . '</strong></div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
				}
			} else {
				$result['transactionid'] = '';
			}
			$invoice_IDs = array();
			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
				$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
			}
		
			$receipt_data = array(
				'transaction_id' => $result['transactionid'],
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'Payments/create_customer_sale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
				'checkPlan' => $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			
			redirect('home/transation_sale_receipt',  'refresh');
		}


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$data['merchID'] 		= $user_id;
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$data['plantype'] = $plantype;
		$merchant_surcharge = $this->general_model->get_row_data('tbl_merchant_surcharge', array('merchantID' => $user_id));
		$data['merchant_surcharge'] = $merchant_surcharge;
		$planData = $this->general_model->chk_merch_plantype_data($user_id);
		$is_es_plan = false;
		if(!empty($planData)){
			if($planData->merchant_plan_type == 'SS' || $planData->merchant_plan_type == 'ES'){
				$is_es_plan = true;
			}
		}
		$data['is_es_plan'] = $is_es_plan;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_sale', $data);
		$this->load->view('pages/page_popup_modals', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/*****************Authorize Transaction***************/


	public function create_customer_auth()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		$custom_data_fields = [];
		if (!empty($this->input->post(null, true))) {
			$po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
			$checkPlan = check_free_plan_transactions();
			if($checkPlan){
				$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

				if ($gatlistval != "" && !empty($gt_result)) {
					$nmiuser  = $gt_result['gatewayUsername'];
					$nmipass  = $gt_result['gatewayPassword'];
					$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
					$gatewayName = getGatewayName($gt_result['gatewayType']);
					$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

					if ($this->session->userdata('logged_in')) {
						$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if ($this->session->userdata('user_logged_in')) {
						$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}

					$customerID = $this->czsecurity->xssCleanPostInput('customerID');
					if(!$customerID || empty($customerID)){
						$customerID = create_card_customer($this->input->post(null, true), '1');
					}
					$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
					$companyID  = $comp_data['companyID'];


				$transaction = new nmiDirectPost($nmi_data);
				$cardID = $this->czsecurity->xssCleanPostInput('card_list');
				if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
					$card_no = $this->czsecurity->xssCleanPostInput('card_number');
					$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear1   = substr($exyear, 2);
						$expry    = $expmonth . $exyear;
						$transaction->setCcExp($expry);
						if ($this->czsecurity->xssCleanPostInput('cvv') != "")
							$transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));
					} else {


						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no = $card_data['CardNo'];

						$transaction->setCcNumber($card_data['CardNo']);
						$expmonth =  $card_data['cardMonth'];

						$exyear   = $card_data['cardYear'];
						$exyear1   = substr($exyear, 2);
						if (strlen($expmonth) == 1) {
							$expmonth = '0' . $expmonth;
						}
						$expry    = $expmonth . $exyear1;
						$transaction->setCcExp($expry);
						if ($card_data['CardCVV'] != "")
							$transaction->setCvv($card_data['CardCVV']);
					}
					$cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;
					
					$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));
					
					#Billing Details
					$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->setAddress1($this->czsecurity->xssCleanPostInput('baddress1'));
					$transaction->setAddress2($this->czsecurity->xssCleanPostInput('baddress2'));
					$transaction->setCountry($this->czsecurity->xssCleanPostInput('bcountry'));
					$transaction->setCity($this->czsecurity->xssCleanPostInput('bcity'));
					$transaction->setState($this->czsecurity->xssCleanPostInput('bstate'));
					$transaction->setZip($this->czsecurity->xssCleanPostInput('bzipcode'));
					$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));

					#Shipping Details
					$transaction->setShippingCompany($this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->setShippingFirstName($this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->setShippingLastName($this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
					$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
					$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
					$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
					$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
					$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
					$transaction->setShippingEmail($this->czsecurity->xssCleanPostInput('email'));

					$amount = $this->czsecurity->xssCleanPostInput('totalamount');
					$transaction->setAmount($amount);

					
					$new_invoice_number = '';
	                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
	                    $new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_number');
	                    $transaction->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $new_invoice_number);
	                }

	                // add level III data
	                $level_request_data = [
	                    'transaction' => $transaction,
	                    'card_no' => $card_no,
	                    'merchID' => $user_id,
	                    'amount' => $amount,
	                    'invoice_id' => $new_invoice_number,
	                    'gateway' => 1
	                ];
	                if (!empty($po_number)) {
                    	$transaction->setPoNumber($po_number);
	                    $level_request_data['ponumber'] = $po_number;
	                }
	                $transaction = addlevelThreeDataInTransaction($level_request_data);
					$transaction->setTax('tax');
					$transaction->auth();
					$result = $transaction->execute();

					if ($result['response_code'] == '100') {


						$merchant_data    = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user_id));



						/* This block is created for saving Card info in encrypted form  */

						if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

							$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
							$cvv          =  $this->czsecurity->xssCleanPostInput('cvv');
							$cardType         = $this->general_model->getType($card_no);

							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	 => $exyear,
								'CardType'    => $cardType,
								'CustomerCard' => $card_no,
								'CardCVV'      => $cvv,
								'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
								'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
								'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
								'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
								'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
								'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
								'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
								'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
								'companyID'     => $companyID,
								'merchantID'   => $merchantID,

								'createdAt' 	=> date("Y-m-d H:i:s")
							);



							$id1 = $this->card_model->process_card($card_data);

							
						}
						$this->session->set_flashdata('success', 'Transaction Successful');
					} else {



						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' . $result['responsetext'] . '</strong></div>');
					}

					$transactiondata = array();
					$transactiondata['transactionID']       = $result['transactionid'];
					$transactiondata['transactionStatus']   = $result['responsetext'];
					$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					$transactiondata['transactionCode']     = $result['response_code'];
					$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					$transactiondata['transactionType']    = $result['type'];
					$transactiondata['gatewayID']          = $gatlistval;
					$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
					$transactiondata['customerListID']      = $customerID;
					$transactiondata['transactionAmount']   = $amount;
					$transactiondata['merchantID']         = $merchantID;
					$transactiondata['transaction_user_status'] = '5';
					$transactiondata['gateway']             = $gatewayName;
					$transactiondata['resellerID']           = $this->resellerID;
					$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
					
					$transactiondata = alterTransactionCode($transactiondata);
					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					if(!empty($this->transactionByUser)){
					    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
					if($custom_data_fields){
	                    $transactiondata['custom_data_fields'] = json_encode($custom_data_fields);
	                }
					$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
				}
			} else {
				$result['transactionid'] = 'TXNFAILED'.time();
			}
			$invoice_IDs = array();
			
			$receipt_data = array(
				'transaction_id' => $result['transactionid'],
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'Payments/create_customer_auth',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Authorize',
				'checkPlan' => $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			
			redirect('home/transation_sale_receipt',  'refresh');
		}

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$data['merchID'] 		= $user_id;
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);

		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$data['plantype'] = $plantype;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_auth', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/*****************Refund Transaction***************/

	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if (!empty($this->czsecurity->xssCleanPostInput('txnID'))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnID');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$total   = $this->czsecurity->xssCleanPostInput('ref_amount');

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			if ($tID != '' && !empty($gt_result)) {
				if (!empty($paydata['invoiceTxnID'])) {
					$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $paydata['merchantID']));
					$user_id  = $paydata['merchantID'];
					$user    =  $cusdata['qbwc_username'];
					$comp_id  =  $cusdata['id'];
					$ittem = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));

					if (empty($ittem)) {
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
						redirect('Payments/payment_transaction', 'refresh');
					}
					$ins_data['customerID']     = $paydata['customerListID'];


					$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
					if (!empty($in_data)) {
						$inv_pre   = $in_data['prefix'];
						$inv_po    = $in_data['postfix'] + 1;
						$new_inv_no = $inv_pre . $inv_po;
					}
					$ins_data['merchantDataID']        = $paydata['merchantID'];
					$ins_data['creditDescription']     = "Credit as Refund";
					$ins_data['creditMemo']    = "This credit is given to refund for a invoice ";
					$ins_data['creditDate']   = date('Y-m-d H:i:s');
					$ins_data['creditAmount']   = $total;
					$ins_data['creditNumber']   = $new_inv_no;
					$ins_data['updatedAt']     = date('Y-m-d H:i:s');
					$ins_data['Type']         = "Payment";
					$ins_id = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

					$item['itemListID']      =    $ittem['ListID'];
					$item['itemDescription'] =    $ittem['Name'];
					$item['itemPrice'] = $total;
					$item['itemQuantity'] = 0;
					$item['crlineID'] = $ins_id;
					$acc_name  = $ittem['DepositToAccountName'];
					$acc_ID    = $ittem['DepositToAccountRef'];
					$method_ID = $ittem['PaymentMethodRef'];
					$method_name  = $ittem['PaymentMethodName'];
					$ins_data['updatedAt'] = date('Y-m-d H:i:s');
					$ins = $this->general_model->insert_row('tbl_credit_item', $item);
					$refnd_trr = array(
						'merchantID' => $paydata['merchantID'], 'refundAmount' => $total,
						'creditInvoiceID' => $paydata['invoiceTxnID'], 'creditTransactionID' => $tID,
						'creditTxnID' => $ins_id, 'refundCustomerID' => $paydata['customerListID'],
						'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'),
						'paymentMethod' => $method_ID, 'paymentMethodName' => $method_name,
						'AccountRef' => $acc_ID, 'AccountName' => $acc_name
					);


					if ($ins_id && $ins) {
						$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>');
					}
				}



				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  =  $gt_result['gatewayPassword'];
				$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";


				$transaction = new nmiDirectPost($nmi_data);
				$customerID = $paydata['customerListID'];
				$amount     =  $total;
				$transaction->setTransactionId($tID);
				$transaction->refund($tID, $amount);
				$result     = $transaction->execute();
				if ($result['response_code'] == '100') {

					$this->customer_model->update_refund_payment($tID, 'NMI');
					if (!empty($paydata['invoiceTxnID'])) {
						$this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1', '', $user);
					} else {
						$inv       = '';
						$ins_id    = '';
						$refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
							'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
							'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
							'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
						);
					}
					$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

					$this->session->set_flashdata('success', 'Successfully Refunded Payment');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' . $result['responsetext'] . '</strong></div>');
				}
				$transactiondata = array();
				$transactiondata['transactionID']      = $result['transactionid'];
				$transactiondata['transactionStatus']  = $result['responsetext'];
				$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
				$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
				$transactiondata['transactionType']    = $result['type'];
				$transactiondata['transactionCode']   = $result['response_code'];
				$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
				$transactiondata['gatewayID']         = $gatlistval;

				if ($paydata['invoiceTxnID'] != "")
					$transactiondata['invoiceTxnID']      = $paydata['invoiceTxnID'];
				$transactiondata['customerListID']     = $customerID;
				$transactiondata['transactionAmount']  = $amount;
				$transactiondata['merchantID']         = $paydata['merchantID'];
				$transactiondata['gateway']            = $gatewayName;
				$transactiondata['resellerID']          = $this->resellerID;

				$transactiondata = alterTransactionCode($transactiondata);
				$CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction not availabe</strong></div>');
		}
		$invoice_IDs = array();
			
		
			$receipt_data = array(
				'proccess_url' => 'Payments/payment_refund',
				'proccess_btn_text' => 'Process New Refund',
				'sub_header' => 'Refund',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result['transactionid'] == ''){
				$result['transactionid'] ='null';
			}
			redirect('home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result['transactionid'],  'refresh');
			
		

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['merchID'];

		$compdata				= $this->customer_model->get_customers($user_id);

		$data['customers']		= $compdata;


		$this->load->view('template/template_start', $data);



		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_refund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}

			$tID     = $this->czsecurity->xssCleanPostInput('txnID');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			if ($paydata['gatewayID'] > 0) {


				$gatlistval = $paydata['gatewayID'];

				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				if ($tID != '' && !empty($gt_result)) {

					$nmiuser  = $gt_result['gatewayUsername'];
					$nmipass  =  $gt_result['gatewayPassword'];
					$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
					$gatewayName = getGatewayName($gt_result['gatewayType']);
					$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";


					$transaction = new nmiDirectPost($nmi_data);



					$customerID = $paydata['customerListID'];
					$amount  =  $paydata['transactionAmount'];

					$comp_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));


					$transaction->setTransactionId($tID);
					$transaction->setAmount($amount);
					if ($paydata['transactionType'] == 'auth') {

						$transaction->capture($tID, $amount);
					}
					$result     = $transaction->execute();

					if ($result['response_code'] == '100') {


						$condition = array('transactionID' => $tID);

						$update_data =   array('transaction_user_status' => "2");

						$this->general_model->update_row_data('customer_transaction', $condition, $update_data);
						$condition = array('transactionID' => $tID);
						$customerID = $paydata['customerListID'];

						$comp_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
						$tr_date   = date('Y-m-d H:i:s');
						$ref_number =  $tID;
						$toEmail = $comp_data['Contact'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['FullName'];
						if ($chh_mail == '1') {
							
							$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
						}
						$this->session->set_flashdata('success', ' Successfully Captured Authorization');

					} else {

						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' . $result['responsetext'] . '</strong>.</div>');
					}

					$transactiondata = array();
					$transactiondata['transactionID']      = $result['transactionid'];
					$transactiondata['transactionStatus']  = $result['responsetext'];
					$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
					$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					$transactiondata['transactionType']    = $result['type'];
					$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
					$transactiondata['gatewayID']        = $gatlistval;
					$transactiondata['transactionCode']   = $result['response_code'];
					$transactiondata['customerListID']     = $customerID;
					$transactiondata['transactionAmount']  = $amount;
					$transactiondata['merchantID']   = $paydata['merchantID'];
					$transactiondata['gateway']   = $gatewayName;
					$transactiondata['resellerID']   = $this->resellerID;

					$transactiondata = alterTransactionCode($transactiondata);
					$CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
					if(!empty($this->transactionByUser)){
					    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
					$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>.</div>');
				}
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
			}
			$invoice_IDs = array();
		
			$receipt_data = array(
				'proccess_url' => 'Payments/payment_capture',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Capture',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result['transactionid'] == ''){
				$result['transactionid'] ='null';
			}
			redirect('home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result['transactionid'],  'refresh');
			
			
		}


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['id'];

		$compdata				= $this->customer_model->get_customers($user_id);

		$data['customers']		= $compdata;


		$this->load->view('template/template_start', $data);



		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_transaction', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/*****************Void Transaction***************/


	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}

			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			if (!empty($gt_result)) {
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  =  $gt_result['gatewayPassword'];

				$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";


				$transaction = new nmiDirectPost($nmi_data);



				$customerID = $paydata['customerListID'];
				$amount  =  $paydata['transactionAmount'];
				$comp_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
				$transaction->setTransactionId($tID);

				$transaction->void($tID);

				$result     = $transaction->execute();

				if ($result['response_code'] == '100') {

					
					$condition = array('transactionID' => $tID);

					$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

					$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

					if ($chh_mail == '1') {
						$condition = array('transactionID' => $tID);
						$customerID = $paydata['customerListID'];

						$comp_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
						$tr_date   = date('Y-m-d H:i:s');
						$ref_number =  $tID;
						$toEmail = $comp_data['Contact'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['FullName'];
						$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
					}

					$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong></div>');
				}
				$transactiondata = array();
				$transactiondata['transactionID']      = $result['transactionid'];
				$transactiondata['transactionStatus']  = $result['responsetext'];
				$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
				$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
				$transactiondata['transactionType']    = $result['type'];
				$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
				$transactiondata['gatewayID']        = $gatlistval;
				$transactiondata['transactionCode']   = $result['response_code'];
				$transactiondata['customerListID']     = $customerID;
				$transactiondata['transactionAmount']  = $amount;
				$transactiondata['merchantID']   = $paydata['merchantID'];
				$transactiondata['gateway']   = $gatewayName;
				$transactiondata['resellerID']   = $this->resellerID;

				$transactiondata = alterTransactionCode($transactiondata);
				$CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Gateway has removed.</strong></div>');
			}

			redirect('Payments/payment_capture', 'refresh');
		}
	}



	public function payment_capture()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']   = $this->customer_model->get_transaction_data_captue($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;


		$this->load->view('template/template_start', $data);

		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_capture', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function refund_transaction()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['transactions']   = $this->customer_model->get_refund_transaction_data($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_refund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}




	public function payment_refund()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
	

		$data['transactions']   = $this->customer_model->get_transaction_datarefund($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_refund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}





	public function payment_transaction()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_transaction_new', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function payment_transaction_old()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}


		$data['transactions']   = $this->customer_model->get_transaction_data($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_transaction', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/** 
	 * Get transaction using ajax
	 * 
	 */
	public function ajaxPaymentTransactions()
	{
		if ($this->session->userdata('logged_in')) {
			$login_info = $data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$login_info = $data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data = array();
		$no = $_POST['start'];

		$transactions   = $this->customer_model->get_transaction_history_ajax_data($user_id);
		$count = $this->customer_model->get_transaction_history_ajax_total_data($user_id);

		if(!empty($transactions)){
			foreach($transactions as $transaction){
				$row = [];
				$inv_url1 = '';
				if (!empty($transaction['invoice_id']) && !empty($transaction['invoice_no'])) {
					$invs = explode(',', $transaction['invoice_id']);
					$invoice_no = explode(',', $transaction['invoice_no']);
					foreach ($invs as $k => $inv) {
						$inv_url = base_url().'home/invoice_details/'.trim($inv);
						if (isset($invoice_no[$k]))
							if ($plantype) {
								$inv_url1 .= $invoice_no[$k].',';
							} else {
								$inv_url1 .= ' <a href="' . $inv_url . '">' . $invoice_no[$k] . '</a>,';
							}
					}

					$inv_url1 = substr($inv_url1, 0, -1);
				} else {
					$inv_url1 .= '<a href="javascript:void(0);">---</a> ';
				}

				$gateway = ($transaction['gateway']) ? $transaction['gateway'] : $transaction['transactionType'];
				
				if ($plantype) {
					$row[] = '<div class="text-left visible-lg">'.$transaction['FullName'].'</div>';
				} else {
					$custURL = base_url('home/view_customer/' . $transaction['ListID']);
					$row[] = '<div class="text-left visible-lg cust_view"><a href="'.$custURL.'">'.$transaction['FullName'].'</a></div>';
				}

				$row[] = '<div class="text-right cust_view">'.$inv_url1.'</div>';

				if(isset($transaction['transactionType']) && strpos(strtolower($transaction['transactionType']), 'refund') !== false){
					$row[] = '<div class="hidden-xs text-right cust_view"><a href="#pay_data_process" onclick="set_payment_transaction_data(\''.$transaction['transactionID'].'\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">($'. number_format($transaction['transactionAmount'], 2).')</a></div>';
				}else{
					$row[] = '<div class="hidden-xs text-right cust_view"><a href="#pay_data_process" onclick="set_payment_transaction_data(\''.$transaction['transactionID'].'\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$'. number_format($transaction['transactionAmount'], 2).'</a></div>';
				}

				$transactionDate = $transaction['transactionDate'];
				if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
					$timezone = ['time' => $transactionDate, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
					$transactionDate = getTimeBySelectedTimezone($timezone);
				} else {
					$timezone = ['time' => $transactionDate, 'current_format' => 'UTC', 'new_format' => DEFAULT_TIMEZONE];
					$transactionDate = getTimeBySelectedTimezone($timezone);
				}
				$row[] = '<div class=" text-right">'.date('M d, Y h:i A', strtotime($transactionDate)).'</div>';

				$showRefund = 0;
				if ($transaction['partial'] == $transaction['transactionAmount']) {
					$showTypeCustom = "<label class='label label-success'>Fully Refunded: " . '$' . number_format($transaction['partial'], 2) . "</label><br/>";
				} else if ($transaction['partial'] != '0') {
					$showRefund = 1;
					$showTypeCustom = "<label class='label label-warning'>Partially Refunded: " . '$' . number_format($transaction['partial'], 2) . " </label><br/>";
				} else {
					$showRefund = 1;
					$showTypeCustom = "";
				}

				
				if(strpos(strtolower($transaction['transactionType']), 'refund') !== false){
					$showRefund=0;
					$showType = "Refund";
				}else if (strpos($transaction['transactionType'], 'sale') !== false || strtoupper($transaction['transactionType']) == 'AUTH_CAPTURE') {
					$showType = "Sale";
				} else if (strpos($transaction['transactionType'], 'Offline Payment') !== false) {
					$showType = "Offline Payment";
				} else if (strpos($transaction['transactionType'], 'capture') !== false || strtoupper($transaction['transactionType']) != 'AUTH_CAPTURE') {
					$showType = "Capture";
				}
				$row[] = '<div class="hidden-xs text-right">'.$showTypeCustom.$showType.'</div>';
				$row[] = '<div class="text-right hidden-xs hidden-sm">'.$transaction['transactionID'].'</div>';

				if($showRefund == 0) { 
					$disabled_select = 'disabled'; 
				}else{
					$disabled_select = '';
				} 

				$selectOption = '<a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm '.$disabled_select.' dropdown-toggle">Select <span class="caret"></span></a>
				<ul class="dropdown-menu text-left">';

				if ($transaction['transaction_user_status'] == '1' || $transaction['transaction_user_status'] == '2') {
					if (
						in_array($transaction['transactionCode'], array('100', '200', '111', '1')) &&
						in_array(strtoupper($transaction['transactionType']), array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'OFFLINE PAYMENT', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
					) {
						if (($transaction['tr_Day'] == 0 &&  ($transaction['transactionGateway'] == '2' || $transaction['transactionGateway'] == '3')) ||  strtoupper($transaction['transactionType']) == 'OFFLINE PAYMENT') {
							$selectOption .= '<li> <a href="javascript:void(0);" class="" data-title="Pending Transaction" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>';
						} else {
							if ($transaction['partial'] == $transaction['transactionAmount']) {
								$selectOption .= '<li> 
									<a href="javascript:void(0)" id="txnRefund'.$transaction['transactionID'].'" transaction-id="'.$transaction['transactionID'].'" transaction-gatewayType="'.$transaction['transactionGateway'].' transaction-gatewayName="'. $transaction['gateway'].'" integration-type="2" class="refunAmountCustom" data-url="'. base_url("ajaxRequest/getRefundInvoice").'"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
								</li>';
							} else {
								if(strtolower($gateway) != 'heartland echeck'){
									$selectOption .= '<li> 
										<a href="javascript:void(0)" id="txnRefund'. $transaction['transactionID'] .'" transaction-id="'.$transaction['transactionID'] .'" transaction-gatewayType="'. $transaction['transactionGateway'] .'" transaction-gatewayName="'. $transaction['gateway'].'" integration-type="2" class="refunAmountCustom" data-url="'.base_url("ajaxRequest/getRefundInvoice").'"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
									</li>';
								}
							}
						}
					}
					if($transaction['transactionGateway'] != 5 && $transaction['transaction_user_status'] != '2'){
						$selectOption .= '<li><a href="#payment_delete" class="" onclick="set_transaction_pay('.$transaction['id'].');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>';
					}
				}  else {
					$selectOption .= '<li><a href="javascript:void(0);" data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>';
				} 
				$transaction_auto_id = $transaction['transactionID'];
				$selectOption .= '<li><a href="javascript:void(0);" onclick="getPrintTransactionReceiptData(\''. $transaction_auto_id.'\', 2)">Print</a></li>';
				$selectOption .= '</ul>';
				$row[] = '<div class="text-center hidden-xs"><div class="btn-group dropbtn">'.$selectOption.'</div></div>';

				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $this->input->post('draw', true),
			"recordsTotal" => $count,
			"recordsFiltered" => $count,
			"data" => $data,
			
		);
		//output to json format
		echo json_encode($output);
		die;
	}






	/*********ECheck Transactions**********/

	public function evoid_transaction()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']    = $this->customer_model->get_transaction_data_erefund($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_ecapture', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function echeck_transaction()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']    = $this->customer_model->get_transaction_data_erefund($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_erefund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function payment_erefund()
	{
		//Show a form here which collects someone's name and e-mail address
		$merchantID = $this->session->userdata('logged_in')['merchID'];
		if (!empty($this->input->post(null, true))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnID');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);



			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			$nmiuser  = $gt_result['gatewayUsername'];
			$nmipass  =  $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);


			$transaction = new nmiDirectPost($nmi_data);
			$customerID = $paydata['customerListID'];

		
			$amount     =  $paydata['transactionAmount'];

			$transaction->setPayment('check');
			$transaction->setTransactionId($tID);




			$transaction->refund($tID, $amount);

			$result     = $transaction->execute();



			if ($result['response_code'] == '100') {


				$this->customer_model->update_refund_payment($tID, 'NMI');

				if (!empty(!empty($paydata['invoice_id']))) {
                    $paymts   = explode(',', $paydata['tr_amount']);
                    $invoices = explode(',', $paydata['invoice_id']);
                    $ins_id   = '';
                    foreach ($invoices as $k1 => $inv) {

                        $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paymts[$k1],
                            'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                    }

                    $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $this->merchantID));
                    $user_id = $this->merchantID;
                    $user    = $cusdata['qbwc_username'];
                    $comp_id = $cusdata['id'];
                    $ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                    $refund  = $amount;

                    if (empty($ittem)) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
                        exit;
                    }
                    $ins_data['customerID'] = $customerID;

                    foreach ($invoices as $k => $inv) {
                        $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                        if (!empty($in_data)) {
                            $inv_pre    = $in_data['prefix'];
                            $inv_po     = $in_data['postfix'] + 1;
                            $new_inv_no = $inv_pre . $inv_po;
                        }
                        $ins_data['merchantDataID']    = $this->merchantID;
                        $ins_data['creditDescription'] = "Credit as Refund";
                        $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                        $ins_data['creditDate']        = date('Y-m-d H:i:s');
                        $ins_data['creditAmount']      = $paymts[$k];
                        $ins_data['creditNumber']      = $new_inv_no;
                        $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                        $ins_data['Type']              = "Payment";
                        $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                        $item['itemListID']      = $ittem['ListID'];
                        $item['itemDescription'] = $ittem['Name'];
                        $item['itemPrice']       = $paymts[$k];
                        $item['itemQuantity']    = 0;
                        $item['crlineID']        = $ins_id;
                        $acc_name                = $ittem['DepositToAccountName'];
                        $acc_ID                  = $ittem['DepositToAccountRef'];
                        $method_ID               = $ittem['PaymentMethodRef'];
                        $method_name             = $ittem['PaymentMethodName'];
                        $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                        $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                        $refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $paymts[$k],
                            'creditInvoiceID'                             => $invID, 'creditTransactionID'          => $tID,
                            'creditTxnID'                                 => $ins_id, 'refundCustomerID'            => $customerID,
                            'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                            'paymentMethod'                               => $method_ID, 'paymentMethodName'        => $method_name,
                            'AccountRef'                                  => $acc_ID, 'AccountName'                 => $acc_name,
                        );

                    

                        if ($ins_id && $ins) {
                            $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

                            $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $ins_id, '1', '', $user);
                        }

                    }

                } else {
                    $inv       = '';
                    $ins_id    = '';
                    $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
                        'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                        'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
                    );
                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                }

				$this->session->set_flashdata('success', 'Successfully Refunded Payment');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong>.</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $result['transactionid'];
			$transactiondata['transactionStatus']  = $result['responsetext'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']    = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result['type'];
			$transactiondata['transactionCode']   = $result['response_code'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']         = $gatlistval;
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']  = $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = "NMI ECheck";

			$transactiondata = alterTransactionCode($transactiondata);
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

			redirect('Payments/echeck_transaction', 'refresh');
		}
	}
	public function payment_evoid()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if (!empty($this->input->post(null, true))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$gatlistval = $paydata['gatewayID'];
			$merchantID = $this->session->userdata('logged_in')['merchID'];
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			$nmiuser  = $gt_result['gatewayUsername'];
			$nmipass  =  $gt_result['gatewayPassword'];

			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);


			$transaction = new nmiDirectPost($nmi_data);



			$customerID = $paydata['customerListID'];
			$amount  =  $paydata['transactionAmount'];
			$transaction->setPayment('check');
			$transaction->setTransactionId($tID);

			$transaction->void($tID);

			$result     = $transaction->execute();

			if ($result['response_code'] == '100') {

				
				$condition = array('transactionID' => $tID);

				$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

				$this->general_model->update_row_data('customer_transaction', $condition, $update_data);



				$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $result['transactionid'];
			$transactiondata['transactionStatus']  = $result['responsetext'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']    = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result['type'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']        = $gatlistval;
			$transactiondata['transactionCode']   = $result['response_code'];
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']         = $merchantID;
			$transactiondata['resellerID']        = $this->resellerID;
			$transactiondata['gateway']   = "NMI ECheck";

			$transactiondata = alterTransactionCode($transactiondata);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);


			redirect('Payments/evoid_transaction', 'refresh');
		}
	}




	public function create_customer_esale()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');

			$checkPlan = check_free_plan_transactions();
			
			$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
			$companyID  = $comp_data['companyID'];
			$totalamount  = $this->czsecurity->xssCleanPostInput('totalamount');
			if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  = $gt_result['gatewayPassword'];

				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";
				$gatewayName = "$gatewayName ECheck";

				$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

				$payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
				$sec_code =     'WEB';

				if($payableAccount == '' || $payableAccount == 'new1') {
					$accountDetails = [
						'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
						'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
						'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'companyID'     => $companyID,
						'merchantID'   => $merchantID,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $sec_code
					];
				} else {
					$accountDetails = $this->card_model->get_single_card_data($payableAccount);
				}
				$accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$transaction = new nmiDirectPost($nmi_data);
				
				$transaction->setAccountName($accountDetails['accountName']);
				$transaction->setAccount($accountDetails['accountNumber']);
				$transaction->setRouting($accountDetails['routeNumber']);
				
				$transaction->setAccountType($accountDetails['accountType']);
				$transaction->setAccountHolderType($accountDetails['accountHolderType']);
				$transaction->setSecCode($sec_code);
				$transaction->setPayment('check');
				
				#Billing Details
				$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
				$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->setAddress1($this->czsecurity->xssCleanPostInput('baddress1'));
				$transaction->setAddress2($this->czsecurity->xssCleanPostInput('baddress2'));
				$transaction->setCountry($this->czsecurity->xssCleanPostInput('bcountry'));
				$transaction->setCity($this->czsecurity->xssCleanPostInput('bcity'));
				$transaction->setState($this->czsecurity->xssCleanPostInput('bstate'));
				$transaction->setZip($this->czsecurity->xssCleanPostInput('bzipcode'));
				$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));

				#Shipping Details
				$transaction->setShippingCompany($this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->setShippingFirstName($this->czsecurity->xssCleanPostInput('firstName'));
				$transaction->setShippingLastName($this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction->setShippingEmail($this->czsecurity->xssCleanPostInput('email'));
				
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_id');
					$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 2);
					$transaction->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $new_invoice_number);
				}

				if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
					$transaction->setPoNumber($this->czsecurity->xssCleanPostInput('po_number'));
				}
				$amount = $this->czsecurity->xssCleanPostInput('totalamount');
				$transaction->setAmount($amount);
				$transaction->setTax('tax');
				$transaction->sale();
				
				$result = $transaction->execute();

				if ($result['response_code'] == '100') {

					$invoiceIDs = [];
					$invoicePayAmounts = [];
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
					}
					$refnum = array();
					$comp_data_user = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));

					if (!empty($invoiceIDs)) {
						$payIndex = 0;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();

							$theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));



							if (!empty($theInvoice)) {
								$amount_data = $theInvoice['BalanceRemaining'];

								$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
								$isPaid 	 = 'false';
								$BalanceRemaining = 0.00;
								$refnum[] = $theInvoice['RefNumber'];

								if($amount_data == $actualInvoicePayAmount){
									$actualInvoicePayAmount = $amount_data;
									$isPaid 	 = 'true';

								}else{

									$actualInvoicePayAmount = $actualInvoicePayAmount;
									$isPaid 	 = 'false';
									$BalanceRemaining = $amount_data - $actualInvoicePayAmount;
									
								}
								$txnAmount = $actualInvoicePayAmount;

								$AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
								
								$tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));
								

								$transactiondata = array();
								$transactiondata['transactionID']       = $result['transactionid'];
								$transactiondata['transactionStatus']   = $result['responsetext'];
								$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
								$transactiondata['transactionCode']     = $result['response_code'];
								$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
								$transactiondata['transactionType']    = 'sale';
								$transactiondata['gatewayID']          = $gatlistval;
								$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
								$transactiondata['customerListID']      = $customerID;
								$transactiondata['transactionAmount']   = $txnAmount;
								$transactiondata['merchantID']         = $merchantID;
								$transactiondata['invoiceTxnID']      = $inID;
								$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
								$transactiondata['gateway']   = $gatewayName;
								$transactiondata['resellerID']   = $this->resellerID;
								if($custom_data_fields){
	                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
	                            }
								$transactiondata = alterTransactionCode($transactiondata);
								$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
								if(!empty($this->transactionByUser)){
								    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
								    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

								$user      = $comp_data_user['qbwc_username'];

								if(!is_numeric($inID))
									$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
							}
							$payIndex++;
						}
					} else {

						$transactiondata = array();
						$transactiondata['transactionID']       = $result['transactionid'];
						$transactiondata['transactionStatus']   = $result['responsetext'];
						$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
						$transactiondata['transactionCode']     = $result['response_code'];
						$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
						$transactiondata['transactionType']    = $result['type'];
						$transactiondata['gatewayID']          = $gatlistval;
						$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
						$transactiondata['customerListID']      = $customerID;
						$transactiondata['transactionAmount']   = $totalamount;
						$transactiondata['merchantID']         = $merchantID;
						$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
						$transactiondata['gateway']             = $gatewayName;
						$transactiondata['resellerID']           = $this->resellerID;
						
						$transactiondata = alterTransactionCode($transactiondata);
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					}
					$this->session->set_flashdata('success', ' Transaction Successful');

					if ($this->czsecurity->xssCleanPostInput('tr_checked'))
                        $chh_mail = 1;
                    else
						$chh_mail = 0;
						
					$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
					$ref_number = '';
					
					$tr_date   = date('Y-m-d H:i:s');
					$toEmail = $comp_data['Contact'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['FirstName'] . ' ' . $comp_data['LastName'];

					if($payableAccount == '' || $payableAccount == 'new1') {
						$id1 = $this->card_model->process_ack_account($accountDetails);
					}
					if($chh_mail =='1')
					{
					   
					  $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
					} 
					
				} else {

					$transactiondata = array();
					$transactiondata['transactionID']       = $result['transactionid'];
					$transactiondata['transactionStatus']    = $result['responsetext'];
					$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					$transactiondata['transactionModified']    = date('Y-m-d H:i:s');
					$transactiondata['transactionCode']     = $result['response_code'];
					$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
					$transactiondata['transactionType']    = $result['type'];
					$transactiondata['gatewayID']          = $gatlistval;
					$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
					$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					$transactiondata['transactionAmount']   = $this->czsecurity->xssCleanPostInput('totalamount');
					$transactiondata['merchantID']   = $merchantID;
					$transactiondata['resellerID']   = $this->resellerID;
					$transactiondata['gateway']   = "$gatewayName";

					if($custom_data_fields){
                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                    }
                    
					$transactiondata = alterTransactionCode($transactiondata);
					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					if(!empty($this->transactionByUser)){
					    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
					$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
				}

			} else {
				$result['transactionid'] = '';
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
			}

			if(!$checkPlan){
				$result['transactionid'] = '';
			}

			$invoice_IDs = array();
			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
				$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
			}

			$receipt_data = array(
				'transaction_id' => $result['transactionid'],
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'Payments/create_customer_esale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
				'checkPlan' => $checkPlan
			);

			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			$this->session->set_userdata("receipt_data",$receipt_data);
			redirect('home/transation_sale_receipt',  'refresh');
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_name'] 	= "NMI ESale";


		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$data['merchID'] 		= $user_id;
		$condition				= array('merchantID' => $user_id);
		
		$gateway		= $this->general_model->get_gateway_data($user_id, 'echeck');
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];

		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/payment_esale', $data);
		$this->load->view('pages/page_popup_modals', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/**********END****************/



	public function chk_friendly_name()
	{
		$res = array();
		$frname  = $this->czsecurity->xssCleanPostInput('frname');
		$condition = array('gatewayFriendlyName' => $frname);
		$num   = $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
		if ($num) {
			$res = array('gfrname' => 'Friendly name already exist', 'status' => 'false');
		} else {
			$res = array('status' => 'true');
		}
		echo json_encode($res);
		die;
	}


	/*****************Test NMI Validity***************/

	public function testNMI()
	{


		$nmiuser  = $this->czsecurity->xssCleanPostInput('nmiuser');
		$nmipass  = $this->czsecurity->xssCleanPostInput('nmipassword');



		$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

		$transaction = new nmiDirectPost($nmi_data);

		$transaction->setOrderDescription('Some Item');
		$transaction->setAmount('100.00');
		$transaction->setTax('9.00');
		$transaction->setShipping('12.00');

		$transaction->setCcNumber('4111111111111111');
		$transaction->setCcExp('1113');
		$transaction->setCvv('999');

		$transaction->setCompany('Some company');
		$transaction->setFirstName('John');
		$transaction->setLastName('Smith');
		$transaction->setAddress1('888');
		$transaction->setCity('Dallas');
		$transaction->setState('TX');
		$transaction->setZip('77777');
		$transaction->setPhone('5555555555');
		$transaction->setEmail('test@domain.com');

		$transaction->auth();

		$result = $transaction->execute();

		if ($result['responsetext'] == "SUCCESS") {

			$res = array('status' => 'true');
			echo json_encode($res);
		} else {

			$error = array('nmiPassword' => 'User name or password not matched', 'status' => 'false');
			echo json_encode($error);
		}
	}




	//-------------------------  START -----------------------------//

	//------------------ To view the credit -----------------------//	 
	public function credit()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$compdata	= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$condition  = array('comp.merchantID' => $user_id);

		$data['credits'] = $this->general_model->get_credit_user_data($condition);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;



		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_credit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function get_credit_data()
	{

		$tdata = '';
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}
		if (!empty($this->czsecurity->xssCleanPostInput('customerID'))) {
			$cust = $this->czsecurity->xssCleanPostInput('customerID');
			$condition  = array('comp.merchantID' => $user_id, 'CustomerListID' => $cust);
			$credits     = $this->general_model->get_credit_user_data($condition);

			if (!empty($credits)) {

				foreach ($credits as $credit) {
					if ($credit['CreditRemaining'] != '0.00') {
						$tdata .= '<div class="col-md-12">
						  <tr><td><input type="checkbox" name="pay_check_credit[]" value="' . $credit['TxnID'] . '" /></td><td>' . $credit['RefNumber'] . '</td><td>' . $credit['CreditRemaining'] . '</td></tr></div>';
					}
				}
			} else {
				$tdata .= '<div class="col-md-12"><tr><td colspan="3"> No Credit  Available!</td></tr></div>';
			}
		}
		echo  $tdata;
		die;
	}



	/********** Add Credit ********/

	/********** Add Credit ********/

	public function create_credit()
	{
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {

			$input_data['merchantID'] = $user_id;

			$input_data['creditName']  = $this->czsecurity->xssCleanPostInput('creditname');

			$input_data['creditDate']  = date("Y-m-d");
			$input_data['creditAmount']  = $this->czsecurity->xssCleanPostInput('creditamount');

			$input_data['creditDescription']  = $this->czsecurity->xssCleanPostInput('creditdescription');



			if ($this->general_model->insert_row('tbl_credits', $input_data)) {


				$c_data               =  $this->general_model->get_row_data('qb_test_customer', array('ListID' => $this->czsecurity->xssCleanPostInput('creditname')));
				$comp_data            =  $this->general_model->get_row_data('tbl_company', array('id' => $c_data['companyID']));

				$condition         = array('templateType' => '6', 'merchantID' => $comp_data['merchantID']);
				$view_data         = $this->company_model->template_data($condition);

				$merchant_data    = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $comp_data['merchantID']));
				$config_data       = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $comp_data['merchantID']));
				$currency          = "$";
				$amount            = $this->czsecurity->xssCleanPostInput('creditamount');
				$config_email      = $merchant_data['merchantEmail'];
				$merchant_name     = $merchant_data['companyName'];
				$logo_url          = $merchant_data['merchantProfileURL'];
				$mphone            =  $merchant_data['merchantContact'];
				$tr_date          = date('Y-m-d');
				$currency          = "$";
				$customer          = $c_data['FullName'];
				$message = $view_data['message'];
				$message = stripslashes(str_replace('{{ merchant_name }}', $merchant_name, $message));
				$message = stripslashes(str_replace('{{ logo }}', "<img src='$logo_url'>", $message));
				$message = stripslashes(str_replace('{{ transaction.amount }}', ($amount) ? ($amount) : '0.00', $message));
				$message = stripslashes(str_replace('{{ transaction.currency_symbol }}', $currency, $message));
				$message = stripslashes(str_replace('{{ customer.company }}', $customer, $message));
				$message = stripslashes(str_replace('{{ merchant_email }}', $config_email, $message));
				$message = stripslashes(str_replace('{{ transaction.transaction_date}}', $tr_date, $message));
				$message = stripslashes(str_replace('{{invoice.currency_symbol}}', $currency, $message));



				$customerID = $this->czsecurity->xssCleanPostInput('creditname');
				$subject = $view_data['emailSubject'];
				$fromEmail    = $merchant_data['merchantEmail'];
				$toEmail     = $c_data['Contact'];
				$addCC      = $view_data['addCC'];
				$addBCC		= $view_data['addBCC'];
				$replyTo    = $view_data['replyTo'];
				$this->load->library('email');
				$email_data    = array(
					'customerID' => $customerID,
					'merchantID' => $merchant_data['merchID'],
					'emailSubject' => $subject,
					'emailfrom' => $fromEmail,
					'emailto' => $toEmail,
					'emailcc' => $addCC,
					'emailbcc' => $addBCC,
					'emailreplyto' => $replyTo,
					'emailMessage' => $message,
					'emailsendAt' => date("Y-m-d H:i:s"),

				);



				$this->email->clear();
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from($fromEmail, $merchant_name);
				$this->email->to($toEmail);
				$this->email->subject($subject);
				$this->email->message($message);
				if ($this->email->send()) {
					$this->general_model->insert_row('tbl_template_data', $email_data);
				}


				$this->session->set_flashdata('success', 'Successfully Inserted');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
			}

			redirect(base_url('Payments/credit'));
		}
	}







	//----------- TO update the credit  --------------//

	public function update_credit()
	{
		if ($this->czsecurity->xssCleanPostInput('creditEditID') != "") {

			$id = $this->czsecurity->xssCleanPostInput('creditEditID');
			$chk_condition = array('creditID' => $id);

			$input_data['creditDate']  = date("Y-m-d");
			$input_data['creditAmount']  = $this->czsecurity->xssCleanPostInput('amount');
			$input_data['creditDescription']  = $this->czsecurity->xssCleanPostInput('description');

			if ($this->general_model->update_row_data('tbl_credits', $chk_condition, $input_data)) {
				$this->session->set_flashdata('success', 'Successfully Updated');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}
		}
		redirect(base_url('Payments/credit'));
	}





	public function get_creditedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('credit_id');
		$val = array(
			'creditID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_credits', $val);
		echo json_encode($data);
	}



	//-------------for view credit  data--------------//


	public function get_credit_id()

	{

		$creditID          =  $this->czsecurity->xssCleanPostInput('customerID');
		$condition 		= array('creditName' => $creditID);
		$creditdatas		= $this->general_model->get_table_data('tbl_credits', $condition);

?>

		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-right"> <strong> Credit Date</strong></th>

					<th class="text-right"><strong>Amount ($)</strong></th>
					<th class="text-right"><strong> Processed On</strong></th>
					<th class="text-left"><strong> Edit/Delete</strong></th>
				</tr>
				<?php
				if (!empty($creditdatas)) {
					foreach ($creditdatas as $creditdata) {
				?>

						<tr>
							<td class="text-right visible-lg"><?php echo date('F d, Y', strtotime($creditdata['creditDate'])); ?> </a> </td>
							<td class="text-right visible-lg"> <?php echo number_format($creditdata['creditAmount'], 2); ?> </a> </td>
							<td class="text-right visible-lg"> <?php if ($creditdata['creditStatus'] == "0") {
																	echo "Yet to Processed";
																} else {
																	echo $creditdata['creditDate'];
																} ?> </a>
							</td>

							<td class="text-left visible-lg"> <a href="javascript:void(0);" class="btn btn-default" onclick="set_edit_credit('<?php echo $creditdata['creditID'];  ?>');" title="Edit"> <i class="fa fa-edit"> </i> </a>

								<a href="#del_credit" onclick="del_credit_id('<?php echo $creditdata['creditID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Delete Credit" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>



							</td>


						</tr>


				<?php     }
				}  ?>

			</tbody>
		</table>


		<?php die;
	}



	public function get_card_data()
	{
		$customerdata = array();
		if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
			$crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_single_card_data($crID);
			if (!empty($card_data)) {
				$customerdata['status'] =  'success';
				$customerdata['card']     = $card_data;
				echo json_encode($customerdata);
				die;
			}
		}
	}



	/**************Delete credit********************/

	public function delete_credit()
	{

		$creditID = $this->czsecurity->xssCleanPostInput('invoicecreditid');
		$condition =  array('creditID' => $creditID);
		$del      = $this->general_model->delete_row_data('tbl_credits', $condition);
		if ($del) {
			$this->session->set_flashdata('success', 'Successfully Deleted');
		} else {
			$this->session->set_flashdata('message', 'Oopps...Somthing Is Wrong...');
		}

		redirect(base_url('Payments/credit'));
	}



	public function get_invoice_pay_data()
	{

		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}


		if (!empty($this->input->post(null, true))) {
			$txnID = $this->czsecurity->xssCleanPostInput('txnID');
			$inv_data   =  $this->company_model->get_inv_data($txnID);
		}
	}

	public function get_card_edit_data()
	{

		if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
			$cardID = $this->czsecurity->xssCleanPostInput('cardID');
			$data   = $this->card_model->get_single_mask_card_data($cardID);
			echo json_encode(array('status' => 'success', 'card' => $data));
			die;
		}
		echo json_encode(array('status' => 'success'));
		die;
	}



	public function check_vault()
	{


		$card = '';
		$card_name = '';
		$customerdata = array();
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$merchantID 				= $da['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da	= $this->session->userdata('user_logged_in');

			$merchantID 				= $da['merchantID'];
		}
		$gatewayID 		= $this->czsecurity->xssCleanPostInput('gatewayID');

		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {
			$invoiceIDs = [];
			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');
		
			$condition     =  array('ListID' => $customerID, 'qbmerchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('qb_test_customer', $condition);
			if (!empty($customerdata)) {

				 
				$customerdata['status'] =  'success';

				$ach_data =   $this->card_model->get_ach_info_data($customerID);
				$ACH = [];
				if(!empty($ach_data)){
					foreach($ach_data as $card){
						$ACH[] = $card['CardID'];
					}
				}
				$recentACH = end($ACH);
				$customerdata['ach_data']  = $ach_data;
				$customerdata['recent_ach_account']  = $recentACH;

				$conditionGW = array('gatewayID' => $gatewayID);
				$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
				$cardTypeOption = 2;
				if(isset($gateway['gatewayID'])){
					if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 1;
					}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 2;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 3;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 4;
					}else{
						$cardTypeOption = 2;
					}
					
				}
				$customerdata['cardTypeOption']  = $cardTypeOption;
				$card_data =   $this->card_model->getCardData($customerID,$cardTypeOption);
				$cardArr = [];
				if(!empty($card_data)){
					foreach($card_data as $card){
						$cardArr[] = $card['CardID'];
					}
				}
				$recentCard = end($cardArr);
				$customerdata['card']  = $card_data;
				$customerdata['recent_card']  = $recentCard;

				$invoices = $this->customer_model->get_invoice_upcomming_data($customerID, $merchantID);

				$table = '';
				$new_inv = '';
				$totalInvoiceAmount = 0.00;
				
				if (!empty($invoices)) {
					$table .= '<table class="col-md-offset-3 mytable" width="50%">';
					$new_inv = '<div class="form-group alignTableInvoiceList" >
		      <div class="col-md-1 text-center"><b></b></div>
		        <div class="col-md-3 text-left"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2 text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
			   </div>';

					$inv_data = [];
					foreach ($invoices as $inv) {
						if (strtoupper($inv['status']) != 'CANCEL') {
							$new_inv .= '<div class="form-group alignTableInvoiceList" >
				       
				         <div class="col-md-1 text-center"><input  checked type="checkbox" class="chk_pay check_'.$inv['RefNumber'].'" id="' . 'multiinv' . $inv['TxnID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['TxnID'] . '" /> </div>
				        <div class="col-md-3 text-left">' . $inv['RefNumber'] . '</div>
				        <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
				        <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
					   <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['TxnID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['TxnID'] . '  geter" data-id="multiinv' . $inv['TxnID'] . '" data-inv="' . $inv['TxnID'] . '" data-ref="' . $inv['RefNumber'] . '" data-value="' . $inv['BalanceRemaining'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
					   </div>';
							$inv_data[] = [
								'RefNumber' => $inv['RefNumber'],
								'TxnID' => $inv['TxnID'],
								'BalanceRemaining' => $inv['BalanceRemaining'],
							];
							$invoiceIDs[$inv['TxnID']] =  $inv['RefNumber'];
							$totalInvoiceAmount = $totalInvoiceAmount + $inv['BalanceRemaining'];
						}
					}
					$table .= "</table>";
				} else {
					$table .= '';
				}
				if(empty($customerdata['companyName'])){
					$customerdata['companyName'] = $customerdata['FullName'];
				}
				$customerdata['invoices'] = $new_inv;
				$customerdata['invoiceIDs'] = $invoiceIDs;
				$customerdata['totalInvoiceAmount'] = $totalInvoiceAmount;
				echo json_encode($customerdata);
				die;
			}
		}else{
			$customerdata = [];
			$conditionGW = array('gatewayID' => $gatewayID);
			$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
			$cardTypeOption = 2;
			if(isset($gateway['gatewayID'])){
				if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 1;
				}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 2;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 3;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 4;
				}else{
					$cardTypeOption = 2;
				}
				
			}
			
			$customerdata['status']  = 'success';
			$customerdata['cardTypeOption']  = $cardTypeOption;
			echo json_encode($customerdata);
				die;
		}
	}


	public function view_transaction()
	{


		$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['merchID'];
		$transactions           = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id);

		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
		?>
				<tr>
					<td class="text-left"><?php echo ($transaction['transactionID']) ? $transaction['transactionID'] : ''; ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($transaction['transactionAmount'], 2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo ucfirst($transaction['transactionType']); ?></td>
				

					<td class="text-right visible-lg"><?php if ($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') { ?> <span class="">Success</span><?php } else { ?> <span class="">Failed</span> <?php } ?></td>

				</tr>

<?php     }
		} else {
			echo '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}
		die;
	}



	public function check_transaction_payment()
	{
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$da 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}

		if (!empty($this->czsecurity->xssCleanPostInput('trID'))) {


			$trID = $this->czsecurity->xssCleanPostInput('trID');
			$av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');
			$p_data = $this->customer_model->get_transaction_details_data(array('id' => $trID, 'tr.merchantID' => $user_id));
			if (!empty($p_data)) {
				if ($p_data['transactionAmount'] >= $av_amount) {
					$resdata['status']  = 'success';
				} else {
					$resdata['status']  = 'error';
					$resdata['message']  = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
				}
			} else {
				$resdata['status']  = 'error';
				$resdata['message']  = 'Invalid transactions ';
			}
		} else {
			$resdata['status']  = 'error';
			$resdata['message']  = 'Invalid request';
		}
		echo json_encode($resdata);
		die;
	}






	public function pay_multi_invoice()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}


		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;

		$cardID_upd = '';
		$custom_data_fields = [];
		$invoices           = $this->czsecurity->xssCleanPostInput('multi_inv');
		$cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		$merchant_condition = [
			'merchID' => $user_id,
		];

		if(!merchant_gateway_allowed($merchant_condition)){
			$default_gt_result = $this->general_model->merchant_default_gateway($merchant_condition);
			$default_gt_result = $default_gt_result[0];

			$gateway = $default_gt_result['gatewayID'];
		} else {
			$gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');
		}

		$customerID = $this->czsecurity->xssCleanPostInput('customerID');
		$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID, 'qbmerchantID' => $user_id));
		$companyID  = $comp_data['companyID'];

		$cusproID = '';
		$error = '';
		$cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		$checkPlan = check_free_plan_transactions();

		if ($checkPlan && !empty($invoices) && $cardID != "" && $gateway != "") {
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));


			$nmiuser   = $gt_result['gatewayUsername'];
			$nmipass   = $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
			$gatewayName = getGatewayName($gt_result['gatewayType']);
			$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

			foreach ($invoices as $invoiceID) {

				$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
				$in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
				if (!empty($in_data)) {

					$Customer_ListID = $in_data['Customer_ListID'];

					if ($cardID == 'new1') {


						$cardID_upd  = $cardID;
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');

						$address1 =  $this->czsecurity->xssCleanPostInput('address1');
						$address2 = $this->czsecurity->xssCleanPostInput('address2');
						$city    = $this->czsecurity->xssCleanPostInput('city');
						$country = $this->czsecurity->xssCleanPostInput('country');
						$phone   =  $this->czsecurity->xssCleanPostInput('phone');
						$state   =  $this->czsecurity->xssCleanPostInput('state');
						$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
					} else {


						$card_data    =   $this->card_model->get_single_card_data($cardID);
						$card_no  = $card_data['CardNo'];
						$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];

						$address1 = $card_data['Billing_Addr1'];
						$address2 = $card_data['Billing_Addr2'];
						$city     =  $card_data['Billing_City'];
						$zipcode  = $card_data['Billing_Zipcode'];
						$state    = $card_data['Billing_State'];
						$country  = $card_data['Billing_Country'];
						$phone = $card_data['Billing_Contact'];
					}
					$cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;


					if (!empty($cardID)) {

						if ($in_data['BalanceRemaining'] > 0) {
							$cr_amount = 0;
							$amount    =	 $in_data['BalanceRemaining'];
							$amount  = $pay_amounts;

							$amount    = $amount - $cr_amount;



							$transaction1 = new nmiDirectPost($nmi_data);
							$transaction1->setCcNumber($card_no);

							$exyear   = substr($exyear, 2);
							if (strlen($expmonth) == 1) {
								$expmonth = '0' . $expmonth;
							}
							$expry    = $expmonth . $exyear;
							$transaction1->setCcExp($expry);
							if ($cvv != "")
								$transaction1->setCvv($cvv);
							
							$transaction1->setAmount($amount);

							// add level III data
			                $level_request_data = [
			                    'transaction' => $transaction1,
			                    'card_no' => $card_no,
			                    'merchID' => $user_id,
			                    'amount' => $amount,
			                    'invoice_id' => $invoiceID,
			                    'gateway' => 1
			                ];
			                $transaction1 = addlevelThreeDataInTransaction($level_request_data);

							#Billing Details
							$transaction1->setCompany($comp_data['companyName']);
							$transaction1->setFirstName($comp_data['FirstName']);
							$transaction1->setLastName($comp_data['LastName']);
							$transaction1->setAddress1($address1);
							$transaction1->setAddress2($address2);
							$transaction1->setCountry($country);
							$transaction1->setCity($city);
							$transaction1->setState($state);
							$transaction1->setZip($zipcode);
							$transaction1->setPhone($phone);

							#Shipping Details
							$transaction1->setShippingCompany($comp_data['companyName']);
							$transaction1->setShippingFirstName($comp_data['FirstName']);
							$transaction1->setShippingLastName($comp_data['LastName']);
							$transaction1->setShippingAddress1($address1);
							$transaction1->setShippingAddress2($address2);
							$transaction1->setShippingCountry($country);
							$transaction1->setShippingCity($city);
							$transaction1->setShippingState($state);
							$transaction1->setShippingZip($zipcode);

							$transaction1->sale();
							$result = $transaction1->execute();


							if ($result['response_code'] == "100") {
								$txnID      = $in_data['TxnID'];
								$ispaid 	 = 'true';
								$am         = (-$amount);

								$bamount =  $in_data['BalanceRemaining'] - $amount;
								if ($bamount > 0)
									$ispaid 	 = 'false';

								$app_amount = $in_data['AppliedAmount'] + (-$amount);
								$data   	 = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);

								$condition  = array('TxnID' => $in_data['TxnID']);

								$this->general_model->update_row_data('qb_test_invoice', $condition, $data);
								$user = $in_data['qbwc_username'];

								
								if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

									$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
									$cardType        = $this->general_model->getType($card_no);


									$expmonth     =    $this->czsecurity->xssCleanPostInput('expiry');
									$exyear       =    $this->czsecurity->xssCleanPostInput('expiry_year');
									$cvv          =    $this->czsecurity->xssCleanPostInput('cvv');

									$card_data = array(
										'cardMonth'   => $expmonth,
										'cardYear'	 => $exyear,
										'CardType'    => $cardType,
										'CustomerCard' => $card_no,
										'CardCVV'      => $cvv,
										'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
										'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
										'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
										'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
										'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
										'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
										'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
										'customerListID' => $in_data['Customer_ListID'],

										'companyID'     => $companyID,
										'merchantID'   => $user_id,
										'createdAt' 	=> date("Y-m-d H:i:s")
									);



									$id1 = $this->card_model->process_card($card_data);
								}

								$this->session->set_flashdata('success', 'Successfully Processed Invoice');
							} else {



								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong></div>');
							}

							$transaction['transactionID']      = $result['transactionid'];
							$transaction['transactionStatus']  = $result['responsetext'];
							$transaction['transactionCode']    =  $result['response_code'];
							$transaction['transactionType']   =  ($result['type']) ? $result['type'] : 'auto-nmi';
							$transaction['transactionDate']    = date('Y-m-d H:i:s');
							$transaction['transactionModified']    = date('Y-m-d H:i:s');
							$transaction['invoiceTxnID']       = $in_data['TxnID'];
							$transaction['gatewayID']          = $gateway;
							$transaction['transactionGateway']  = $gt_result['gatewayType'];
							$transaction['customerListID']     = $in_data['Customer_ListID'];
							$transaction['transactionAmount']  = $pay_amounts;
							$transaction['merchantID']   = $user_id;
							$transaction['gateway']   = $gatewayName;
							$transaction['resellerID']   = $this->resellerID;

							$transaction = alterTransactionCode($transaction);
							$CallCampaign = $this->general_model->triggerCampaign($user_id,$transaction['transactionCode']);
							if(!empty($this->transactionByUser)){
							    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
							    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
							}
							$id = $this->general_model->insert_row('customer_transaction',   $transaction);
							if ($result['response_code'] == "100" && !is_numeric($in_data['TxnID'])) {
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
							}

							if ($result['response_code'] == "100" && $chh_mail == '1') {

								$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID, 'qbmerchantID' => $user_id));
								$condition_mail  = array('templateType' => '5', 'merchantID' => $user_id);

								$ref_number = '';

								$tr_date   = date('Y-m-d H:i:s');
								$toEmail = $comp_data['Contact'];
								$company = $comp_data['companyName'];
								$customer = $comp_data['FullName'];

								$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
							}
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid </strong>.</div>');
						}
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
				}
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card!</strong>.</div>');
		}

		if(!$checkPlan){
			$result['transactionid'] = '';
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'home/my_account">Click here</a> to upgrade.</strong>.</div>');
		}

		if ($cusproID != "") {
			redirect('home/invoices/' . $cusproID, 'refresh');
		} else {
			redirect('home/invoices', 'refresh');
		}
	}
}
