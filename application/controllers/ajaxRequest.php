<?php
class AjaxRequest extends CI_Controller
{

	private $merchantID;
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
        $this->load->model('Common/data_model');
		$this->load->model('general_model');
		$this->load->model('card_model');
		if ($this->session->userdata('logged_in')) {
			$da['login_info']	= $this->session->userdata('logged_in');
			$this->merchantID			= $da['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da['login_info']		= $this->session->userdata('user_logged_in');
			$this->merchantID      = $da['login_info']['merchantID'];
		}
	}

	public function index()
	{
		redirect('login', 'refresh');
	}


	public function get_card_data()
	{
		$customerdata = array();
		if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
			$crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_single_card_data($crID);
			if (!empty($card_data)) {
				$customerdata['status'] =  'success';
				$customerdata['card']     = $card_data;
				echo json_encode($customerdata);
				die;
			}
		}
	}




	public function get_data_invoice_transactions()
	{

		if ($this->czsecurity->xssCleanPostInput('invID') != " ") {
			$rows = '';
			$tr_data = array();
			$invID   = $this->czsecurity->xssCleanPostInput('invID');


			$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $this->merchantID));
			if ($app_data['appIntegration'] == '2') {
				$condition = "  tr.invoiceTxnID='" . $invID . "' and  transactionCode IN('200','100','111','1') 
	        and UPPER(transactionType) IN('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE') ";
			} else {
				$condition = "  tr.invoiceID='" . $invID . "' and  transactionCode IN('200','100','111','1') 
	        and UPPER(transactionType) IN('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE') ";


				$condition = "  tr.invoiceID='" . $invID . "' and  transactionCode IN('200','100','111','1') 
	        and UPPER(transactionType) IN('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE') ";
			}
			$invdatas =  $this->general_model->get_invoice_transaction_details_data($this->merchantID, $invID, $app_data['appIntegration']);








			$rows .= '<div class="form-group" >
		        <div class="col-md-4 text-left"><b>Transaction</b></div>
		        
		        <div class="col-md-4 text-right"><b>Amount</b></div>
		        
		       </div>';
			if (!empty($invdatas)) {


				foreach ($invdatas as $k => $inv) {
					$j = $k + 1;

			


					if ($inv['final_amount'] > 0) {
						$j = $k + 1;
						$rows .= '<div class="form-group" >
    		       
    		         
    		       
    		       <div class="col-md-4 text-left"><input  type="radio" onclick="check_transaction();"  class="radio1"   data-id="' . $j . '"   name="multi_inv" value="' . $inv['id'] . '" /> ' . $inv['transactionID'] . '</div>
    		        <div class="col-md-4 text-right"> $' . number_format($inv['final_amount'], 2) . '</div>
    		      
    		       <div class="col-md-3 text-left"><input type="hidden" onblur="check_invoce_value();"   name="pay_amount[]"  id="' . $inv['id'] . '"  data-id="' . $inv['final_amount'] . '"    class="form-control source-val  input_txt_pay"  value="' . sprintf('%0.2f', $inv['final_amount']) . '" /></div>
    		       </div>';
					}
				}

			
				$tr_data['status']         = 'success';
				$tr_data['transactions']   = $rows;
			} else {


				$tr_data['status'] =  'error';
			}


			echo json_encode($tr_data);
			die;
		}

		return false;
	}


	public function check_transaction_qbo_payment_data()
	{
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$da 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}

		if (!empty($this->czsecurity->xssCleanPostInput('trID'))) {

			$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $user_id));
			if ($app_data['appIntegration'] == '1') {
				$this->load->model('QBO_models/qbo_customer_model', 'customer_model');
			
			}
			if ($app_data['appIntegration'] == '2') {
				$this->load->model('customer_model', 'customer_model');
			}

			if ($app_data['appIntegration'] == '5') {
				$this->load->model('company/customer_model', 'customer_model');
			}


			$trID = $this->czsecurity->xssCleanPostInput('trID');





			$av_amount = $this->czsecurity->xssCleanPostInput('pay_amount');
			if ($av_amount) {
				$av_amount = $av_amount;
			} else {
				$av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');
			}



			$p_data = $this->customer_model->chk_transaction_details(array('id' => $trID, 'tr.merchantID' => $user_id));

			if (!empty($p_data)) {
				if ($p_data['transactionAmount'] >= $av_amount) {
					$resdata['status']  = 'success';
				} else {
					$resdata['status']  = 'error';
					$resdata['message']  = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
				}
			} else {
				$resdata['status']  = 'error';
				$resdata['message']  = 'Invalid transactions ';
			}
		} else {
			$resdata['status']  = 'error';
			$resdata['message']  = 'Invalid request';
		}
		echo json_encode($resdata);
		die;
	}



	public function check_transaction_payment_data()
	{
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$da 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}

		if (!empty($this->czsecurity->xssCleanPostInput('trID'))) {
			

			$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $user_id));
			if ($app_data['appIntegration'] == '1') {
				$this->load->model('QBO_models/qbo_customer_model', 'customer_model');
			
			}
			if ($app_data['appIntegration'] == '2') {
				$this->load->model('customer_model', 'customer_model');
			}

			if ($app_data['appIntegration'] == '5') {
				$this->load->model('company/customer_model', 'customer_model');
			}


			$trID = $this->czsecurity->xssCleanPostInput('trID');





			$av_amount = $this->czsecurity->xssCleanPostInput('pay_amount');
			if ($av_amount) {
				$av_amount = $av_amount[0];
			} else {
				$av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');
			}



			$p_data = $this->customer_model->chk_transaction_details(array('id' => $trID, 'tr.merchantID' => $user_id));



			if (!empty($p_data)) {
				if ($p_data['transactionAmount'] >= $av_amount) {
					$resdata['status']  = 'success';
				} else {
					$resdata['status']  = 'error';
					$resdata['message']  = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
				}
			} else {
				$resdata['status']  = 'error';
				$resdata['message']  = 'Invalid transactions ';
			}
		} else {
			$resdata['status']  = 'error';
			$resdata['message']  = 'Invalid request';
		}
		echo json_encode($resdata);
		die;
	}




	public function check_invoice_payment_data()
	{

		if ($this->czsecurity->xssCleanPostInput('invoiceID') != "") {
			$invID      = $this->czsecurity->xssCleanPostInput('invoiceID');
			$av_amount  = $this->czsecurity->xssCleanPostInput('inv_amount');

			$order = "id 'desc'";
			$in_data = array();
			$balance = 0.00;
			$ap_data =  $this->general_model->get_row_order_data('app_integration_setting', array('merchantID' => $this->merchantID), $order);
			if (!empty($ap_data)) {
				if ($ap_data['appIntegration'] == 2) {
					$in_data = $this->general_model->get_select_data('qb_test_invoice', array('BalanceRemaining'), array('TxnID' => $invID));
					$balance = $in_data['BalanceRemaining'];
				}
				if ($ap_data['appIntegration'] == 1) {
					$in_data =    $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining'), array('merchantID' => $this->merchantID, 'invoiceID' => $invID));
					$balance = $in_data['BalanceRemaining'];
				}
				if ($ap_data['appIntegration'] == 3) {
					$in_data =   $this->general_model->get_select_data('Freshbooks_test_invoice', array('BalanceRemaining'), array('merchantID' => $this->merchantID, 'invoiceID' => $invID));
					$balance = $in_data['BalanceRemaining'];
				}
				if ($ap_data['appIntegration'] == 4) {
					$in_data =   $this->general_model->get_select_data('Xero_test_invoice', array('BalanceRemaining'), array('merchantID' => $this->merchantID, 'invoiceID' => $invID));
					$balance = $in_data['BalanceRemaining'];
				}


				if ($ap_data['appIntegration'] == 5) {
					$in_data =   $this->general_model->get_select_data('chargezoom_test_invoice', array('BalanceRemaining'), array('TxnID' => $invID));
					$balance = $in_data['BalanceRemaining'];
				}


				if ($balance >= $av_amount) {
					$resdata['status']  = 'success';
				} else {
					$resdata['status']  = 'error';
					$resdata['message']  = 'Your are not allowed to refund exceeded amount more than actual amount :' . $balance;
				}
			} else {

				$resdata['status']  = 'error';
				$resdata['message']  = 'Invalid request';
			}
		} else {

			$resdata['status']  = 'error';
			$resdata['message']  = 'Invalid request';
		}


		echo json_encode($resdata);
		die;
	}



	public function get_payment_transactions()
	{



		if ($this->czsecurity->xssCleanPostInput('trID') != " ") {
			$rows = '';
			$tr_data = array();
			$trID   = $this->czsecurity->xssCleanPostInput('trID');


			$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $this->merchantID));

			$invdatas =  $this->general_model->get_payment_transaction_details_data($this->merchantID, $trID, $app_data['appIntegration']);


			$rows .= '<div class="form-group" >
		        <div class="col-md-4 text-left"><b>Invoice</b></div>
		        
		        <div class="col-md-4 text-right"><b>Amount</b></div>
		        
		       </div>';
			if (!empty($invdatas)) {


				foreach ($invdatas as $k => $inv) {
					$j = $k + 1;
					if ($app_data['appIntegration'] == '2' || $app_data['appIntegration'] == '5')
						$invoice =  $inv['invoceNumber'];
					else
						$invoice =  $inv['invoceNumber'];
					

					if ($inv['final_amount'] > 0) {
						$rows .= '<div class="form-group" >
    		       
    		       <div class="col-md-4 text-left"><input  type="radio" class="radio1"   data-id="' . $j . '" onclick="check_transaction();"    name="multi_inv" value="' . $inv['id'] . '" /> ' . $invoice . '</div>
    		        <div class="col-md-4 text-right"> $' . number_format($inv['final_amount'], 2) . '</div>
    		      
    		       <div class="col-md-3 text-left"><input type="hidden" name="pay_amount[]"    id="' . $inv['id'] . '" data-id="' . $inv['id'] . '"    onblur="check_invoce_value();"   class="form-control source-val  input_txt_pay"  value="' . sprintf('%0.2f', $inv['final_amount']) . '" /></div>
    		       </div>';
					}
				}

				$tr_data['status']         = 'success';
				$tr_data['transactions']   = $rows;
			} else {


				$tr_data['status'] =  'error';
			}


			echo json_encode($tr_data);
			die;
		}

		return false;
	}






	public function get_invoice_transactions()
	{

		if ($this->czsecurity->xssCleanPostInput('invID') != " ") {
			$rows = '';
			$tr_data = array();
			$invID   = $this->czsecurity->xssCleanPostInput('invID');


			$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $this->merchantID));
			if ($app_data['appIntegration'] == '2' || $app_data['appIntegration'] == '5') {
				$condition = "  tr.invoiceTxnID='" . $invID . "' and  transactionCode IN('200','100','111','1') 
	        and UPPER(transactionType) IN('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE') ";
			} else {
				$condition = "  tr.invoiceID='" . $invID . "' and  transactionCode IN('200','100','111','1') 
	        and UPPER(transactionType) IN('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE') ";


				$condition = "  tr.invoiceID='" . $invID . "' and  transactionCode IN('200','100','111','1') 
	        and UPPER(transactionType) IN('SALE','SRTIPE_SALE','PAYPAL_SALE','PAY_SALE','AUTH_CAPTURE') ";
			}
			$invdatas =  $this->general_model->get_invoice_transaction_details_data($this->merchantID, $invID, $app_data['appIntegration']);


			$rows .= '<div class="form-group" >
		        <div class="col-md-4 text-left"><b>Transaction</b></div>
		        
		        <div class="col-md-4 text-right"><b>Amount</b></div>
		        
		       </div>';
			if (!empty($invdatas)) {


				foreach ($invdatas as $k => $inv) {
					if ($inv['final_amount'] > 0) {
						$j = $k + 1;
						$rows .= '<div class="form-group" >
    		       
    		         
    		       
    		       <div class="col-md-4 text-left"><input  type="radio" onclick="check_transaction();"  class="radio1"   data-id="' . $j . '"   name="multi_inv" value="' . $inv['id'] . '" /> ' . $inv['transactionID'] . '</div>
    		        <div class="col-md-4 text-right"> $' . number_format($inv['final_amount'], 2) . '</div>
    		      
    		       <div class="col-md-3 text-left"><input type="hidden" onblur="check_invoce_value();"   name="pay_amount[]"  id="' . $inv['id'] . '"  data-id="' . $inv['final_amount'] . '"    class="form-control source-val  input_txt_pay"  value="' . sprintf('%0.2f', $inv['final_amount']) . '" /></div>
    		       </div>';
					}
				}


				$tr_data['status']         = 'success';
				$tr_data['transactions']   = $rows;
			} else {


				$tr_data['status'] =  'error';
			}


			echo json_encode($tr_data);
			die;
		}

		return false;
	}



	public function check_transaction_payment()
	{

		if ($this->czsecurity->xssCleanPostInput('invID') != " ") {
			$invID   = $this->czsecurity->xssCleanPostInput('invID');
			$condition = "FIND_IN_SET(" . $invID . ", tr.invoiceID)  and transactionCode IN('200','100','111','1') 
	        and transactionType IN('sale','stripe_sale','Paypal_sale','pay_sale','auth_capture')";

			$trID = $this->czsecurity->xssCleanPostInput('trID');

			$tr_data  = $this->general_model->get_select_data('customer_transaction', array('transactionID'), array('id' => $trID));
			$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $this->merchantID));
			$txID     = $tr_data['transactionID'];
			$tr_amount = $this->czsecurity->xssCleanPostInput('tr_amount');
			$invdatas  =  $this->general_model->get_invoice_transaction_single_data($txID, $this->merchantID, $app_data['appIntegration']);

			$old_amount =  $invdatas['final_amount'];
			$res['amount'] = $old_amount;
			if ($old_amount >= $tr_amount) {
				$res['status'] = "success";
			} else {
				$res['status'] = "error";
			}




			echo json_encode($res);
			die;
		}

		return false;
	}




	public function view_company_transaction()
	{
		$res = array();

		$this->load->model('company/customer_model', 'customer_model');

		$user_id 				= $this->merchantID;
		if ($this->czsecurity->xssCleanPostInput('action') == 'transaction') {
			$invoiceID = $this->czsecurity->xssCleanPostInput('trID');
			$action = '';
			$transactions           = $this->customer_model->get_transaction_data_by_id($invoiceID, $user_id, $action);
		} else {
			$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
			$action = 'invoice';
			$transactions           = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id, $action);
		}
		$tr_data = '';
		$tr_head = '';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
				if($transaction['transaction_user_status'] != 5 && $transaction['transactionType'] == 'auth'){
					$transaction['transactionAmount'] = $transaction['transactionAmount'] / 2;
				}
				
				$user_name = $transaction['FullName'];
				if($transaction['transaction_by_user_type'] == 1){
					$userData = $this->customer_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $transaction['transaction_by_user_id']]);
					$user_name = $userData['firstName'].' '.$userData['lastName'];
				}elseif($transaction['transaction_by_user_type'] == 2){
					$userData = $this->customer_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $transaction['transaction_by_user_id'], 'merchantID' => $transaction['merchantID']]);
					$user_name = $userData['userFname'].' '.$userData['userLname']; 
				}elseif($transaction['transaction_by_user_type'] == 4){
					$user_name = 'Automatic Payment'; 
				}

				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];

				$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs">Amount</th>
								<th class="hidden-xs text-right">User</th>
								<th class="text-right hidden-xs">Gateway</th>
								<th class="text-right hidden-xs" >Transaction ID</th>
							</tr>
						</thead>';

				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3' && trim($transaction['transaction_user_status']) != '5') {
					$status = 'Success';
				} else if ($transaction['transaction_user_status'] == '3' || $transaction['transaction_user_status'] == '5') {
				
					$status = ($transaction['transaction_user_status'] == 5) ? 'Authorize' : 'Void';

				

				} else {
					$status = 'Failed';
				}
				if(isset($transaction['transactionType']) && strpos(strtolower($transaction['transactionType']), 'refund') !== false){
	            	$transactionAmount = '($'.number_format($transaction['transactionAmount'], 2).')';
	            }else{
	            	$transactionAmount = '$'.number_format($transaction['transactionAmount'], 2);
	            }

				$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">' . $transactionAmount . '</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $transaction['transactionID'] . '</td>
						</tr>';

				$getTransactionRefundData = $this->customer_model->get_select_data('customer_transaction', ['*'], 'parent_id = "'.$transaction['id'].'" AND (transactionCode = "1" OR transactionCode = "100" OR transactionCode = "200" OR transactionCode = "300")');
				if($getTransactionRefundData){
					$refund_user_name = $transaction['FullName'];
					// if type 1 then get merchant name
					if($getTransactionRefundData['transaction_by_user_type'] == 1){
						$userDataRefund = $this->customer_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $getTransactionRefundData['transaction_by_user_id']]);
						$refund_user_name = $userDataRefund['firstName'].' '.$userDataRefund['lastName'];
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 2){
						// if type 2 then get sub user name
						$userDataRefund = $this->customer_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $getTransactionRefundData['transaction_by_user_id'], 'merchantID' => $getTransactionRefundData['merchantID']]);
						$refund_user_name = $userDataRefund['userFname'].' '.$userDataRefund['userLname']; 
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 4){
						// if type 4 then automatic payment
						$refund_user_name = 'Automatic Payment'; 
					}

					$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">($' . number_format($transaction['transactionAmount'], 2) . ')</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($refund_user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $getTransactionRefundData['transactionID'] . '</td>
						</tr>';
				}
			}
		} else {
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}

		$tr_data = $tr_head . $tr_data;


		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}




	public function view_transaction()
	{
		$res = array();

		$this->load->model('customer_model');

		if ($this->czsecurity->xssCleanPostInput('action') == 'transaction') {
			$invoiceID = $this->czsecurity->xssCleanPostInput('trID');
			$action = 'transaction';
		} else {
			$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
			$action = 'invoice';
		}
		$user_id 				= $this->merchantID;
		$transactions           = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id, $action);
		$tr_data = '';
		$tr_head = '';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
				$user_name = $transaction['FullName'];
				// if type 1 then get merchant name
				if($transaction['transaction_by_user_type'] == 1){
					$userData = $this->customer_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $transaction['transaction_by_user_id']]);
					$user_name = $userData['firstName'].' '.$userData['lastName'];
				}elseif($transaction['transaction_by_user_type'] == 2){
					// if type 2 then get sub user name
					$userData = $this->customer_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $transaction['transaction_by_user_id'], 'merchantID' => $transaction['merchantID']]);
					$user_name = $userData['userFname'].' '.$userData['userLname']; 
				}elseif($transaction['transaction_by_user_type'] == 4){
					// if type 4 then automatic payment
					$user_name = 'Automatic Payment'; 
				}
				
				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];

				$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs">Amount</th>
								<th class="hidden-xs text-right">User</th>
								<th class="text-right hidden-xs">Gateway</th>
								<th class="text-right hidden-xs" >Transaction ID</th>
							</tr>
						</thead>';

				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3' && trim($transaction['transaction_user_status']) != '5') {
					$status = 'Success';
				} else if ($transaction['transaction_user_status'] == '3' || $transaction['transaction_user_status'] == '5') {
					
					$status = ($transaction['transaction_user_status'] == 5) ? 'Authorize' : 'Void';

					

				} else {
					$status = 'Failed';
				}
				if(isset($transaction['transactionType']) && strpos(strtolower($transaction['transactionType']), 'refund') !== false){
					$transactionAmount = '($'.number_format($transaction['transactionAmount'], 2).')';
				}else{
					$transactionAmount = '$'.number_format($transaction['transactionAmount'], 2);
				}
				
				$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">' . $transactionAmount . '</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $transaction['transactionID'] . '</td>
						</tr>';

				$getTransactionRefundData = $this->customer_model->get_select_data('customer_transaction', ['*'], 'parent_id = "'.$transaction['id'].'" AND (transactionCode = "1" OR transactionCode = "100" OR transactionCode = "200" OR transactionCode = "300")');

				if($getTransactionRefundData){
					// if type 1 then get merchant name
					if($getTransactionRefundData['transaction_by_user_type'] == 1){
						$userDataRefund = $this->customer_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $getTransactionRefundData['transaction_by_user_id']]);
						$refund_user_name = $userDataRefund['firstName'].' '.$userDataRefund['lastName'];
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 2){
						// if type 2 then get sub user name
						$userDataRefund = $this->customer_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $getTransactionRefundData['transaction_by_user_id'], 'merchantID' => $getTransactionRefundData['merchantID']]);
						$refund_user_name = $userDataRefund['userFname'].' '.$userDataRefund['userLname']; 
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 4){
						// if type 4 then automatic payment
						$refund_user_name = 'Automatic Payment'; 
					}
					$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">($' . number_format($transaction['transactionAmount'], 2) . ')</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($refund_user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $getTransactionRefundData['transactionID'] . '</td>
						</tr>';
				}
			}
		} else {
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}

		$tr_data = $tr_head . $tr_data;

		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}


	public function view_qbo_transaction()
	{
		$res = array();

		$this->load->model('QBO_models/qbo_customer_model');
		if ($this->czsecurity->xssCleanPostInput('action') == 'transaction') {
			$invoiceID = $this->czsecurity->xssCleanPostInput('trID');
			$action = 'transaction';
		} else {
			$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
			$action = 'invoice';
		}
		$user_id 				= $this->merchantID;

		$transactions           = $this->qbo_customer_model->get_invoice_transaction_data($invoiceID, $user_id, $action);

	
		$tr_data = '';
		$tr_head = '';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
				
				$user_name = $transaction['FullName'];
				if($transaction['transaction_by_user_type'] == 1){
					$userData = $this->general_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $transaction['transaction_by_user_id']]);
					$user_name = $userData['firstName'].' '.$userData['lastName'];
				}elseif($transaction['transaction_by_user_type'] == 2){
					$userData = $this->general_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $transaction['transaction_by_user_id'], 'merchantID' => $transaction['merchantID']]);
					$user_name = $userData['userFname'].' '.$userData['userLname']; 
				}elseif($transaction['transaction_by_user_type'] == 4){
					// if type 4 then automatic payment
					$user_name = 'Automatic Payment'; 
				}
				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];

			
				$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs">Amount</th>
								<th class="hidden-xs text-right">User</th>
								<th class="text-right hidden-xs">Gateway</th>
								<th class="text-right hidden-xs" >Transaction ID</th>
							</tr>
						</thead>';

				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3' && trim($transaction['transaction_user_status']) != '5') {
					$status = 'Success';
				} else if ($transaction['transaction_user_status'] == '3' || $transaction['transaction_user_status'] == '5') {
					
				} else {
					$status = 'Failed';
				}

				if($transaction['transaction_user_status'] != 5 && $transaction['transactionType'] == 'auth'){
					$transaction['transactionAmount'] = $transaction['transactionAmount'] / 2;
				}
				if(isset($transaction['transactionType']) && strpos(strtolower($transaction['transactionType']), 'refund') !== false){
					$transactionAmount = '($'.number_format($transaction['transactionAmount'], 2).')';
				}else{
					$transactionAmount = '$'.number_format($transaction['transactionAmount'], 2);
				}
				$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">' . $transactionAmount . '</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $transaction['transactionID'] . '</td>
						</tr>';

				$getTransactionRefundData = $this->general_model->get_select_data('customer_transaction', ['*'], 'parent_id = "'.$transaction['id'].'" AND (transactionCode = "1" OR transactionCode = "100" OR transactionCode = "200" OR transactionCode = "300")');
				if($getTransactionRefundData){
					$refund_user_name = $transaction['FullName'];
					// if type 1 then get merchant name
					if($getTransactionRefundData['transaction_by_user_type'] == 1){
						$userDataRefund = $this->general_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $getTransactionRefundData['transaction_by_user_id']]);
						$refund_user_name = $userDataRefund['firstName'].' '.$userDataRefund['lastName'];
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 2){
						// if type 2 then get sub user name
						$userDataRefund = $this->general_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $getTransactionRefundData['transaction_by_user_id'], 'merchantID' => $getTransactionRefundData['merchantID']]);
						$refund_user_name = $userDataRefund['userFname'].' '.$userDataRefund['userLname']; 
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 4){
						// if type 4 then automatic payment
						$refund_user_name = 'Automatic Payment'; 
					}

					$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">($' . number_format($transaction['transactionAmount'], 2) . ')</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($refund_user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $getTransactionRefundData['transactionID'] . '</td>
						</tr>';
				}
			}
		} else {
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}

		$tr_data = $tr_head . $tr_data;


		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}







	public function view_fb_transaction()
	{
		$this->load->model('Freshbook_models/fb_customer_model');
		$res = array();

		if ($this->czsecurity->xssCleanPostInput('action') == 'transaction') {
			$invoiceID = $this->czsecurity->xssCleanPostInput('trID');
			$action = 'transaction';
		} else {
			$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
			$action = 'invoice';
		}
		$user_id 				= $this->merchantID;

		$transactions           = $this->fb_customer_model->get_invoice_transaction_data($invoiceID, $user_id, $action);

		$tr_data = '<thead>
					<tr class="paylist_heading">
						<th class="text-right hidden-xs">Amount</th>
						<th class="hidden-xs text-right">User</th>
						<th class="text-right hidden-xs">Gateway</th>
						<th class="text-right hidden-xs" >Transaction ID</th>
						<th class="text-right hidden-xs" >Status</th>
					</tr>
				</thead>';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
				if($transaction['transaction_user_status'] != 5 && $transaction['transactionType'] == 'auth'){
					$transaction['transactionAmount'] = $transaction['transactionAmount'] / 2;
				}
				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3') {
					$status = 'Success';
				} else if ($transaction['transaction_user_status'] == '3') {
					$status = 'Voided';
				} else {
					$status = 'Failed';
				}
				$user_name = $transaction['FullName'];
				if($transaction['transaction_by_user_type'] == 1){
					$userData = $this->general_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $transaction['transaction_by_user_id']]);
					$user_name = $userData['firstName'].' '.$userData['lastName'];
				}elseif($transaction['transaction_by_user_type'] == 2){
					$userData = $this->general_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $transaction['transaction_by_user_id'], 'merchantID' => $transaction['merchantID']]);
					$user_name = $userData['userFname'].' '.$userData['userLname']; 
				}elseif($transaction['transaction_by_user_type'] == 4){
					// if type 4 then automatic payment
					$user_name = 'Automatic Payment'; 
				}
				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];
				
				$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">$' . number_format($transaction['transactionAmount'], 2) . '</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $transaction['transactionID'] . '</td>'.
					'<td class="text-right visible-lg">' . $status . '</td>
						</tr>';
			}
		} else {
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}


		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}







	public function change_profile_picture()
	{


		$user_id  = $this->merchantID;
		$app_data =  $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $user_id));

		$customerListID = $this->czsecurity->xssCleanPostInput('customerID');


		$insert = '';
		if (!empty($_FILES['profile_picture']['name'])) {
			
			$config['upload_path'] = 'uploads/customer_pic/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['overwrite'] = false;
			$config['remove_spaces'] = true;
			$config['file_name'] = time() . $_FILES['profile_picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('profile_picture')) {
				$uploadData = $this->upload->data();


				$profle_picture = $uploadData['file_name'];
				$this->load->library('image_lib');

				$configer =  array(
					'image_library'   => 'gd2',
					'source_image'    =>  $uploadData['full_path'],
					'maintain_ratio'  =>  TRUE,
					'width'           =>  250,
					'height'          =>  250,
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($configer);
				$this->image_lib->resize();


				$input_data['profile_picture']  = $profle_picture;

				if ($app_data['appIntegration'] == '1') {
					$con                = array('Customer_ListID' => $customerListID, 'merchantID' => $user_id);
					$insert = 	$this->general_model->update_row_data('QBO_custom_customer', $con, $input_data);
				}
				if ($app_data['appIntegration'] == '2') {
					$con                = array('ListID' => $customerListID, 'qbmerchantID' => $user_id);
					$insert = 	$this->general_model->update_row_data('qb_test_customer', $con, $input_data);
				}
				if ($app_data['appIntegration'] == '3') {
					$con                = array('Customer_ListID' => $customerListID, 'merchantID' => $user_id);
					$insert = 	$this->general_model->update_row_data('Freshbooks_custom_customer', $con, $input_data);
				}

				if ($app_data['appIntegration'] == '4') {
					$con                = array('Customer_ListID' => $customerListID, 'merchantID' => $user_id);
					$insert = 	$this->general_model->update_row_data('Xero_custom_customer', $con, $input_data);
				}
			} else {

				$error = $this->upload->display_errors();

				$profle_picture = '';
			}
		}
		if ($insert) {
			$data['status'] = 'success';
			$data['msg'] = 	"Profile picture uploaded successfully";
		} else {
			$data['status'] = 'failed';
			$data['msg'] = 	$error;
		}

		echo json_encode($data);
		die;
	}



	public function check_customer_data()
	{

		if ($this->czsecurity->xssCleanPostInput('customer') != "") {
			$customerID      = $this->czsecurity->xssCleanPostInput('customerID');
			$full_name  = $this->czsecurity->xssCleanPostInput('customer');
			
			$order = "id 'desc'";
			$in_data = 0;
			$ap_data =  $this->general_model->get_row_order_data('app_integration_setting', array('merchantID' => $this->merchantID), $order);
			if (!empty($ap_data)) {
				
				if ($ap_data['appIntegration'] == 5) {
					$con =   array('qbmerchantID' => $this->merchantID, 'FullName' => $full_name);
					if ($customerID != "") {
						$con['ListID !='] = "$customerID";
					}
					$count =    $this->general_model->get_num_rows('chargezoom_test_customer', $con);

					if ($count > 0){
						$status = 'error';
					}
					else{
						$status = 'success';
					}
				}else if ($ap_data['appIntegration'] == 2) {
					
					$con =   array('qbmerchantID' => $this->merchantID, 'FullName' => $full_name);
					if ($customerID != "") {
						$con['ListID !='] = "$customerID";
					}
					$count =    $this->general_model->get_num_rows('qb_test_customer', $con);

					if ($count == 0){
						$status = 'success';
					}
					else{
						$status = 'error';
					}
				}else if ($ap_data['appIntegration'] == 1) {
					
					$con =   array('merchantID' => $this->merchantID, 'fullName' => $full_name);
					if ($customerID != "") {
						$con['Customer_ListID !='] = "$customerID";
					}
					$count =    $this->general_model->get_num_rows('QBO_custom_customer', $con);

					if ($count == 0){
						$status = 'success';
					}
					else{
						$status = 'error';
					}
					
				}else if ($ap_data['appIntegration'] == 3) {
					$con =   array('merchantID' => $this->merchantID, 'fullName' => $full_name);
					if ($customerID != "") {
						$con['Customer_ListID !='] = "$customerID";
					}
					$count =    $this->general_model->get_num_rows('Freshbooks_custom_customer', $con);
					if ($count == 0){
						$status = 'success';
					}
					else{
						$status = 'error';
					}
				}

				if ($status == 'success') {
					$resdata['status']  = 'success';
				} else {
					$resdata['status']  = 'error';
					$resdata['message']  = 'This customer already exist';
				}
			} else {

				$resdata['status']  = 'error';
				$resdata['message']  = 'Invalid request';
			}
		} else {

			$resdata['status']  = 'error';
			$resdata['message']  = 'Invalid request';
		}


		echo json_encode($resdata);
		die;
	}

	public function chk_new_password()
	{
		if (!empty($this->input->post())) {
			$newPassword        = $this->czsecurity->xssCleanPostInput('user_settings_currentpassword');

			if ($this->session->userdata('logged_in')) {
				$merchantData         	= $this->general_model->get_row_data(
					'tbl_merchant_data',
					array('merchID' => $this->merchantID)
				);
	
				if (!empty($merchantData)) {
					if ($merchantData['merchantPasswordNew'] !== null) {
						if (password_verify($newPassword, $merchantData['merchantPasswordNew'])) {
							$data['status'] = 'success';
						} else {
							$data['status'] = 'error';
						}
					} else {
						if (md5($newPassword) === $merchantData['merchantPassword']) {
							$data['status'] = 'success';
						} else {
							$data['status'] = 'error';
						}
					}
				} else {
					$data['status'] = 'error';
				}
			} elseif ($this->session->userdata('user_logged_in')) {
				$dataSession['login_info'] 	= $this->session->userdata('user_logged_in');
				$userData = $this->general_model->get_row_data('tbl_merchant_user', array('merchantUserID' => $dataSession['login_info']['merchantUserID']));

				if (!empty($userData)) {
					if ($userData['userPasswordNew'] !== null) {
						if (password_verify($newPassword, $userData['userPasswordNew'])) {
							$data['status'] = 'success';
						} else {
							$data['status'] = 'error';
						}
					} else {
						if ($newPassword == $userData['userPassword']) {
							$data['status'] = 'success';
						} else {
							$data['status'] = 'error';
						}
					}
				} else {
					$data['status'] = 'error';
				}
			}      
		}
		else
		{
			$data['status'] = 'error';
		}
		echo json_encode($data);
		die;
	}



	public function chk_portal_url()
	{
		if (!empty($this->input->post(null, true))) {
			$user_id  = $this->merchantID;
			$check = $this->czsecurity->xssCleanPostInput('inp');
			if ($check == 0) {
				$data['status'] = 'success';
			} else {
				$merch_data = $this->general_model->get_select_data('tbl_config_setting', array('serviceurl'), array('merchantID' => $user_id));
				
				if (!empty($merch_data['serviceurl'])) {
					$data['status'] = 'success';
				} else {
					$data['status'] = 'error';
				}
			}
			echo json_encode($data);
			die;
		}
		echo "Invalid Request" . ' <a href="' . base_url('home/index') . '">Go Back</a>';
	}






	public function get_ach_details_data()
	{

		$merchantID = $this->merchantID;
		$invoiceID  = $this->czsecurity->xssCleanPostInput('invID');
		$type       = $this->czsecurity->xssCleanPostInput('type');

		$onGatewaychange = 'set_url(this);';
		$app_data  = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $this->merchantID));

		if ($type == 2)
			$gateway_datas = $this->general_model->get_table_select_data('tbl_merchant_gateway', array('gatewayID', 'set_as_default', 'gatewayFriendlyName','isSurcharge' ,'surchargePercentage'), array('merchantID' => $merchantID, 'echeckStatus' => 1));
		if ($type == 1)
			$gateway_datas = $this->general_model->get_table_select_data('tbl_merchant_gateway', array('gatewayID', 'set_as_default', 'gatewayFriendlyName', 'isSurcharge', 'surchargePercentage'), array('merchantID' => $merchantID, 'creditCard' => 1));
		$gate = '';

		if ($app_data['appIntegration'] == '2') {
			$res = $this->general_model->get_select_data('qb_test_invoice', array('Customer_ListID'), array('TxnID' => $invoiceID));
			$customerID = $res['Customer_ListID'];
		}

		if ($app_data['appIntegration'] == '1') {
			$res = $this->general_model->get_select_data('QBO_test_invoice', array('CustomerListID'), array('invoiceID' => $invoiceID, 'merchantID' => $merchantID));
			$customerID = $res['CustomerListID'];
			$onGatewaychange = 'set_qbo_url(this);';
		}

		if ($app_data['appIntegration'] == '5') {
			$res = $this->general_model->get_select_data('chargezoom_test_invoice', array('Customer_ListID'), array('TxnID' => $invoiceID));
			$customerID = $res['Customer_ListID'];
		}
		if ($app_data['appIntegration'] == '3') {
			$res = $this->general_model->get_select_data('Freshbooks_test_invoice', array('CustomerListID'), array('invoiceID' => $invoiceID));
			$customerID = $res['CustomerListID'];
			$onGatewaychange = 'set_fb_url(this);';
		}

		if ($app_data['appIntegration'] == '4') {
			$res = $this->general_model->get_select_data('Xero_test_invoice', array('CustomerListID'), array('invoiceID' => $invoiceID, 'merchantID' => $merchantID));
			$customerID = $res['CustomerListID'];
			$onGatewaychange = 'set_qbo_url(this);';
		}

		$option = '';
		$option_card = '';

		$defaultGateway = '';
		$defaultCard = 'new1';
		$surchargePercentage = 0;
		if ($type == 1) {
			if (isset($gateway_datas) && !empty($gateway_datas)) {
				foreach ($gateway_datas as $gateway_data) {
					if($gateway_data['set_as_default']){
						$defaultGateway = $gateway_data['gatewayID'];
						$surchargePercentage = ($gateway_data['isSurcharge'] == 1) ? $gateway_data['surchargePercentage'] : 0 ;
					}

					if ($gateway_data['set_as_default'] == 1) $sel = ' selected';
					else $sel = '';
					$option .=  '<option value="' . $gateway_data['gatewayID'] . '"  ' . $sel . '  >' . $gateway_data['gatewayFriendlyName'] . '</option>';
				}
			}

			$option_card .=  '<option value="new1"  >New Card</option>';
			$card =   $this->card_model->get_card_expiry_data($customerID);
			if (isset($card) && !empty($card)) {
				$cardCount = count($card);
				$iCard = 1;
				foreach ($card as $card_data) {
					if($cardCount == $iCard){
						$cardSel = "selected";
						$defaultCard = $iCard;
					} else {
						$cardSel = "";
					}
					++$iCard;
					if (strtoupper($card_data['CardType']) != 'ECHECK')
						$option_card .=  '<option value="' . $card_data['CardID'] . '" '.$cardSel.' >' . $card_data['customerCardfriendlyName'] . '</option>';
				}
			}


			$lable = "Card";
		}

		if ($type == 2) {
			
			if (isset($gateway_datas) && !empty($gateway_datas)) {
				foreach ($gateway_datas as $gateway_data) {
					if($gateway_data['set_as_default']){
						$defaultGateway = $gateway_data['gatewayID'];
					}
					if ($gateway_data['set_as_default'] == 1) $sel = ' selected';
					else $sel = '';
					$option .=  '<option value="' . $gateway_data['gatewayID'] . '" ' . $sel . '  >' . $gateway_data['gatewayFriendlyName'] . '</option>';
				}
			}

			$option_card .=  '<option value="new1"  >New Account</option>';
			$card =   $this->card_model->get_ach_card_data($customerID, $merchantID);
			if (isset($card) && !empty($card)) {
				$cardCount = count($card);
				$iAch = 1;
				foreach ($card as $card_data) {
					if($cardCount == $iAch){
						$achSel = "selected";
						$defaultCard = $iAch;
					} else {
						$achSel = "";
					}

					$option_card .=  '<option value="' . $card_data['CardID'] . '" '. $achSel .' >' . $card_data['customerCardfriendlyName'] . '</option>';
					++$iAch;
				}
			}

			$lable = "Account Number";
		}

		$merchant_condition = [
            'merchID' => $merchantID,
        ];

        if (!merchant_gateway_allowed($merchant_condition)) {
			$gateways = '<input type="hidden" name="sch_gateway" value="'.$defaultGateway.'">';
        } else {
			$gateways = '<div class="form-group ">' .

			'<label class="col-md-4 control-label" for="card_list">Gateway</label>' .
			'<div class="col-md-8">' .
			'<select id="sch_gateway" onchange="'.$onGatewaychange.'"  name="sch_gateway"   class="form-control">' .
			'<option value="" >Select Gateway</option>' .
			$option .

			'</select></div></div>';
		}

		$gate = $gateways.'<div class="form-group ">' .

			'<label class="col-md-4 control-label" for="card_list">' . $lable . '</label><div class="col-md-8">' .
			'<select id="schCardID" name="schCardID" onchange="create_sch_card(this,\'' . $type . '\');"   class="form-control">' .
			$option_card .

			'</select>' .

			'</div></div>';          

		$data['defaultCard'] = $defaultCard;
		$data['schd'] = $gate;
		$data['surchargePercentage'] = $surchargePercentage;
		$data['status'] = "success";

		echo json_encode($data);
		die;
	}

	public function get_recurring_data()
	{
		if (!empty($this->input->post(null, true))) {
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$merchantID = $this->merchantID;
			$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $this->merchantID));
			$table      = '';
			$rec_data   = $this->general_model->get_recurring_data(array('customerID' => $customerID, 'merchantID' => $merchantID));
			
			if (!empty($rec_data)) {
				$table .= '<table  class="table compamount table-bordered table-striped 	table-vcenter "> <thead>	
												<tr>	
											
													<th> Card</th>
													<th> Auto Payment</th>
												   <th  class="text-right" >Amount</th>
													<th class="text-center">Action</th> 
										
											</tr>
										</thead> <tbody>';
				foreach ($rec_data as $recurr) {
					$mss = "Do you really want to delete";
					if ($app_data['appIntegration'] == 1)
						$link = base_url() . 'QBO_controllers/SettingPlan/delete_recurring/' . $recurr['recurrID'];
					if ($app_data['appIntegration'] == 2)
						$link = base_url() . 'SettingPlan/delete_recurring/' . $recurr['recurrID'];
					if ($app_data['appIntegration'] == 3)
						$link = base_url() . 'FreshBooks_controllers/SettingPlan/delete_recurring/' . $recurr['recurrID'];
					if ($app_data['appIntegration'] == 4)
						$link = base_url() . 'Integration/SettingPlan/delete_recurring/' . $recurr['recurrID'];
					if ($app_data['appIntegration'] == 5)
						$link = base_url() . 'company/SettingPlan/delete_recurring/' . $recurr['recurrID'];
					$card = $this->card_model->get_select_card_data(array('CardID' => $recurr['cardID']));
					if(isset($card['customerCardfriendlyName'])){
						$customerCardfriendlyName = $card['customerCardfriendlyName'];
					}else{
						$customerCardfriendlyName = 'None';
					}
					$table .= '<tr><td>' . $customerCardfriendlyName . '</td><td>' . $recurr['plan'] . '</td><td class="text-right hidden-xs"><div class="btn-group btn-group-xs">' . number_format($recurr['amount'], 2) . '</td> <td class="text-center"><a href="#" class="btn btn-sm btn-default" onclick="edit_recurring_payment(\'' . $recurr['recurrID'] . '\');"  data-original-title="Edit Recurring Payment"><i class="fa fa-edit"></i></a>';
					$table .= "<a href='" . $link . "'   onclick='return confirm(\"" . $mss . "\");'    data-toggle='tooltip' class='btn btn-sm btn-danger'  data-original-title='Delete Recurring Payment'><i class='fa fa-times'></i></a></div></td></tr>";
				}
				$table .= '</tbody></table>';
			}

			$res['sch_data'] = $table;
			$res['rec_data'] = ($rec_data) ? $rec_data[0] : [];

			$res['card'] = $this->card_model->get_card_data($customerID, $merchantID);
			$res['pay_term'] = $this->general_model->get_table_data('tbl_payment_terms', array('merchantID' => $merchantID, 'enable' => '0'));
			echo json_encode($res);
			die;
		}
		return false;
	}

	public function edit_recurring_data()
	{
		if (!empty($this->input->post(null, true))) {


			$recrID = $this->czsecurity->xssCleanPostInput('editID');
			$merchantID = $this->merchantID;
			$table      = '';
			$rec_data   = $this->general_model->get_row_data('tbl_recurring_payment', array('recurrID' => $recrID, 'merchantID' => $merchantID));

			if (!empty($rec_data)) {

				$customerID  = $rec_data['customerID'];
				$res['rec_data'] = $rec_data;

				$res['card']     = $this->card_model->get_card_data($customerID, $merchantID);
				$res['pay_term'] = $this->general_model->get_table_data('tbl_payment_terms', array('merchantID' => $merchantID, 'enable' => '0'));

				echo json_encode($res);
				die;
			}
		}
		return false;
	}



	public function check_gateway_data()
	{
		$res = array();




		if (!empty($this->input->post(null, true))) {

			$num = 0;
			$gateID = $this->czsecurity->xssCleanPostInput('gatewayID');
			$app    = $this->czsecurity->xssCleanPostInput('pack');
			$mID = $this->merchantID;

			if ($app == 1)
				$num = 	$this->general_model->get_num_rows('tbl_subscriptions_qbo', array('paymentGateway' => $gateID, 'merchantDataID' => $mID, 'subscriptionStatus' => 1));
			if ($app == 2) {

				$num = 	$this->general_model->get_num_rows('tbl_subscriptions', array('paymentGateway' => $gateID, 'merchantDataID' => $mID, 'subscriptionStatus' => 1));
			}
			if ($app == 3)
				$num = 	$this->general_model->get_num_rows('tbl_subscriptions_fb', array('paymentGateway' => $gateID, 'merchantDataID' => $mID, 'subscriptionStatus' => 1));
			if ($app == 4)
				$num = 	$this->general_model->get_num_rows('tbl_subscriptions_xero', array('paymentGateway' => $gateID, 'merchantDataID' => $mID, 'subscriptionStatus' => 1));
			if ($app == 5)
				$num = 	$this->general_model->get_num_rows('tbl_chargezoom_subscriptions', array('paymentGateway' => $gateID, 'merchantDataID' => $mID, 'subscriptionStatus' => 1));


			if ($num == 0) {
				$res['status'] = 'success';
				$res['msg'] = 'Success';
			} else {
				$res['status'] = 'error';
				$res['msg'] = "Subscription Error: This gateway cannot be deleted. Please change Subscription gateway.";
			}
		} else {
			$res['status'] = 'error';
			$res['msg'] = 'Invalid Request';
		}

		echo json_encode($res);
		die;
	}



	public function check_url()
	{
		$res = array();


		if ($this->session->userdata('logged_in') != "") {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in') != "") {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}



		$portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');


		if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
			if (strpos($portal_url, ' ') > 0) {

				$res = array('portal_url' => 'Space is not allowed in url', 'status' => 'false');
				echo json_encode($res);
				die;
			}

			$res = array('portal_url' => 'Special character not allowed in url', 'status' => 'false');
			echo json_encode($res);
			die;
		} else {
			$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url));
			if ($num == 0) {
				$res = array('status' => 'success');
			} else if ($num == 1) {
				$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url, 'merchantID' => $merchantID));
				if ($num == 1)
					$res = array('status' => 'success');
				else
					$res = array('portal_url' => 'This is alreay used', 'status' => 'false');
			} else {
				$res = array('portal_url' => 'This is alreay used', 'status' => 'false');
			}

			echo json_encode($res);
			die;
		}


	}

	public function check_account_exists(){
		if ($this->session->userdata('logged_in') != "") {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		} else {
			echo '{"status" : "error"}';die;
		}

		$response = ['status' => 'error'];

		$args = $this->input->post(null, true);
		$args['merchantID'] = $merchantID;

		$card_data = $this->card_model->get_merchant_card_data($args);
		if(!$card_data) {
			$response = ['status' => 'success'];
		}

		echo json_encode($response);die;
	}

	public function view_invoice_transaction(){
		$res = array();

		
		$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

		$transactions  = $this->general_model->get_table_data('tbl_merchant_tansaction', array('merchant_invoiceID' => $invoiceID));

		$tr_data = '';
		$tr_head = '';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {

				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];

				$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs" >Transaction ID</th>
								<th class="text-right hidden-xs">Amount</th>
								<th class="text-right visible-lg">Date</th>
								<th class="text-right hidden-xs">Gateway Type</th>
								<th class="hidden-xs text-right">Status</th>
							</tr>
						</thead>';

				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3' && trim($transaction['transaction_user_status']) != '5') {
					$status = 'Success';
				} else if ($transaction['transaction_user_status'] == '3' || $transaction['transaction_user_status'] == '5') {
					
				} else {
					$status = 'Failed';
				}

				$tr_data .= '<tr class="paylist_data">' .

					'<td class="hidden-xs text-right">' . $transaction['transactionID'] . '</td>' .
					'<td class="hidden-xs text-right">$' . number_format($transaction['transactionAmount'], 2) . '</td>' .
					'<td class="hidden-xs text-right">' . date('M d, Y', strtotime($transaction['transactionDate'])) . '</td>
					<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $status . '</td>
						</tr>';
			}
		} else {
			$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs" >Transaction ID</th>
								<th class="text-right hidden-xs">Amount</th>
								<th class="text-right visible-lg">Date</th>
								<th class="text-right hidden-xs">Gateway Type</th>
								<th class="hidden-xs text-right">Status</th>
							</tr>
						</thead>';
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}

		$tr_data = $tr_head . $tr_data;


		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}

    /**
     * Check to ensure password is not the same as current password
     */
    public function chk_previous_password()
    {
        if (!empty($this->input->post())) {
			if ($this->session->userdata('logged_in')) {
				$newPassword        = $this->czsecurity->xssCleanPostInput('user_settings_password');
				$merchant           = $this->general_model->get_row_data(
					'tbl_merchant_data',
					array('merchID' => $this->merchantID)
				);
	
				if (!empty($merchant)) {
					if ($merchant['merchantPasswordNew'] !== null) {
						if (password_verify($newPassword, $merchant['merchantPasswordNew'])) {
							$data['status'] = 'error';
						} else {
							$data['status'] = 'success';
						}
					} else {
						if (md5($newPassword) === $merchant['merchantPassword']) {
							$data['status'] = 'error';
						} else {
							$data['status'] = 'success';
						}
					}
				} else {
					$data['status'] = 'success';
				}
			} else {
				$data['status'] = 'success';
			}

        } else {
			$data['status'] = 'error';
		}
		
		echo json_encode($data);
		die;
    }

	public function view_company_transaction_details()
	{
		$res = array();
		$type = $this->czsecurity->xssCleanPostInput('type');
		if($type == 'company'){
			$this->load->model('company/customer_model', 'customer_model');
		}else if($type == 'home'){
			$this->load->model('customer_model', 'customer_model');
		}else if($type == 'freshbooks'){
			$this->load->model('Freshbook_models/fb_customer_model', 'customer_model');
		}else if($type == 'comman'){
			$this->load->model('Common/data_model', 'customer_model');
		}else if($type == 'qbo'){
			$this->load->model('QBO_models/qbo_customer_model', 'customer_model');
		}
		$user_id 				= $this->merchantID;
		if ($this->czsecurity->xssCleanPostInput('action') == 'transaction') {
			$invoiceID = $this->czsecurity->xssCleanPostInput('trID');
			$action = 'transaction';
			$transactions           = $this->customer_model->get_transaction_data_by_id($invoiceID, $user_id, $action);
		} else {
			$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
			$action = 'invoice';
			$transactions           = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id, $action);
		}
		
		

		$tr_data = '';
		$tr_head = '';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
				$user_name = $transaction['FullName'];
				if($transaction['transaction_by_user_type'] == 1){
					$userData = $this->general_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $transaction['transaction_by_user_id']]);
					$user_name = $userData['firstName'].' '.$userData['lastName'];
				}elseif($transaction['transaction_by_user_type'] == 2){
					$userData = $this->general_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $transaction['transaction_by_user_id'], 'merchantID' => $transaction['merchantID']]);
					$user_name = $userData['userFname'].' '.$userData['userLname']; 
				}elseif($transaction['transaction_by_user_type'] == 4){
					// if type 4 then automatic payment
					$user_name = 'Automatic Payment'; 
				}
				
				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];

				$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs">Amount</th>
								<th class="text-right visible-lg">Date</th>
								<th class="hidden-xs text-right">User</th>
								<th class="text-right hidden-xs">Gateway</th>
								<th class="text-right hidden-xs" >Transaction ID</th>
							</tr>
						</thead>';

				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3' && trim($transaction['transaction_user_status']) != '5') {
					$status = 'Success';
				} else if ($transaction['transaction_user_status'] == '3' || $transaction['transaction_user_status'] == '5') {
					
					$status = ($transaction['transaction_user_status'] == 5) ? 'Authorize' : 'Void';

					

				} else {
					$status = 'Failed';
				}

				$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">$' . number_format($transaction['transactionAmount'], 2) . '</td>' .
					'<td class="hidden-xs text-right">' . date('M d, Y', strtotime($transaction['transactionDate'])) . '</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $transaction['transactionID'] . '</td>
						</tr>';
			}
		} else {
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}

		$tr_data = $tr_head . $tr_data;


		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}
	public function check_card_surcharge(){

		if ($this->session->userdata('logged_in') != "") {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in') != "") {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}
		
		$status = $surcharge = 0;
		$message = 'Invalid Request';
		$cardId = $this->czsecurity->xssCleanPostInput('cardId');
		$cardNumber = $this->czsecurity->xssCleanPostInput('cardNumber');

		if($cardId > 0){
			$message = 'Surcharge Value';
		} elseif($cardNumber > 0){
			$message = 'Surcharge Value';
			$surcharge = $this->card_model->chkCardSurcharge(['cardNumber' => $cardNumber]);
		}
		echo json_encode(array('success' => $status, 'message' => $message, 'data' => $surcharge)); die;
	}
	/*
	* Fetch and check custom surcharge apply or not
	* @return json
	*/
	public function customSurcharge(){

		if ($this->session->userdata('logged_in') != "") {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in') != "") {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}
		
		$status = $surcharge = 0;
		$message = 'Invalid Request';
		$cardId = $this->czsecurity->xssCleanPostInput('cardId');
		$cardNumber = $this->czsecurity->xssCleanPostInput('cardNumber');

		if($cardId > 0){
			$message = 'Surcharge Value';
			$card_data= $this->card_model->get_single_card_data($cardId);
			$cardNumber = $card_data['CardNo'];
			$surcharge = $this->card_model->chkCardSurcharge(['cardNumber' => $cardNumber]);
		} elseif($cardNumber > 0){
			$message = 'Surcharge Value';
			$surcharge = $this->card_model->chkCardSurcharge(['cardNumber' => $cardNumber]);
		}
		echo json_encode(array('success' => $status, 'message' => $message, 'data' => $surcharge)); die;
	}
	public function clearNotification(){

		
		$merchID = $this->czsecurity->xssCleanPostInput('merchID');
		
		if($merchID > 0){
			$con  = array('merchID' => $merchID);
			$input_data = array('notification_read' => 1 );
			$update = 	$this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);

			$con1  = array('receiver_id' => $merchID);
			$input_data1 = array('is_read' => 2 );
			$update1 = 	$this->general_model->update_row_data('tbl_merchant_notification', $con1, $input_data1);

			if($update1){
				echo json_encode(array('status' => 'success')); die;
			}
		}
		echo json_encode(array('status' => 'fail')); die;
	}

	public function readNotification(){
		
		$nfID = $this->czsecurity->xssCleanPostInput('nfID');
		
		if($nfID > 0){
			$con  = array('id' => $nfID);
			$input_data = array('is_read' => 2 );
			$update = 	$this->general_model->update_row_data('tbl_merchant_notification', $con, $input_data);
			if($update){
				echo json_encode(array('status' => 'success')); die;
			}
		}
		
		
		echo json_encode(array('status' => 'fail')); die;
	}
	
	public function loadNotification(){
		
		$limitStart = $this->czsecurity->xssCleanPostInput('limitStart');
		$notificationAllCount = $this->czsecurity->xssCleanPostInput('notificationAllCount');
		$merchID = $this->czsecurity->xssCleanPostInput('merchID');
		$base_path = $this->czsecurity->xssCleanPostInput('base_path');
		$unReadCount = $this->czsecurity->xssCleanPostInput('unReadCount');
		$condition  = array('receiver_id' => $merchID,'status' => 1);
		$orderBy = array('name' => 'created_at','type' => 'desc');
		$notification = $this->general_model->get_table_data('tbl_merchant_notification',$condition,$orderBy,5,$limitStart);

		// get merchant timezone
		$merchant_default_timezone = '';
		$merchant_data = $this->general_model->get_select_data('tbl_merchant_data', ['merchant_default_timezone'], ['merchID' => $merchID]);
		if(isset($merchant_data['merchant_default_timezone']) && !empty($merchant_data['merchant_default_timezone'])){
			$merchant_default_timezone = $merchant_data['merchant_default_timezone'];
		}
		$html = '';
		$heightClass = '';
        if($notificationAllCount > 5){
           $heightClass = 'loadHeight';
        }
		if(count($notification) > 0){
            foreach ($notification as $nf) {
                if($nf['type'] == 1){
                    $url = base_url(''.$base_path.'/Payments/payment_transaction');
                }else if($nf['type'] == 2){
                    if($nf['recieverType'] == 1){
                        $url = base_url(''.$base_path.'/home/invoice_details/'.$nf['typeID'].'');
                    }elseif($nf['recieverType'] == 2){
                        $url = base_url(''.$base_path.'/Create_invoice/invoice_details_page/'.$nf['typeID'].'');
                    }elseif($nf['recieverType'] == 3){
                        $url = base_url(''.$base_path.'/home/invoice_details/'.$nf['typeID'].'');
                    }elseif($nf['recieverType'] == 4){
                        $url = base_url(''.$base_path.'/Freshbooks_invoice/invoice_details_page/'.$nf['typeID'].'');
                    }elseif($nf['recieverType'] == 5){
                        $url = base_url(''.$base_path.'/Xero_invoice/invoice_details_page/'.$nf['typeID'].'');
                    }else{
                        $url = '';
                    }
                    
                }else if($nf['type'] == 3){

                    if($nf['recieverType'] == 1){
                        $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                    }elseif($nf['recieverType'] == 2){
                        $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                    }elseif($nf['recieverType'] == 3){
                        $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                    }elseif($nf['recieverType'] == 4){
                        $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                    }elseif($nf['recieverType'] == 5){
                        $url = base_url(''.$base_path.'/SettingSubscription/subscriptions');
                    }else{
                        $url = '';
                    }
                   
                }else{
                    $url = base_url(''.$base_path.'/home/index');
                }
                if($nf['is_read'] == 1){
                    $dis_color = 'black_show';
                }else{
                    $dis_color = 'grey_show';
                }
                $current_time = date('Y-m-d H:i:s');
                if($merchant_default_timezone){
                	$timezone = ['time' => $nf['created_at'], 'current_format' => date_default_timezone_get(), 'new_format' => $merchant_default_timezone];
                	$timezone1 = ['time' => $current_time, 'current_format' => date_default_timezone_get(), 'new_format' => $merchant_default_timezone];
                    $nf['created_at'] = getTimeBySelectedTimezone($timezone);
                    $current_time = getTimeBySelectedTimezone($timezone1);
                }

                $html .= '<li id="nf'.$nf['id'].'" class="dropdown-header-li ">
                    <div class="single-nf">
                        <div id="read_nf'.$nf['id'].'" data-count="'.$unReadCount.'" data-read="'.$nf['is_read'].'" data-nfID="'.$nf['id'].'" class="nf-sub readNotification">
                            <a href="'.$url.'">
                                <div class="nf-message '.$dis_color.' ">
                                    
                                    <div class="nf-message-desc">
                                        '.$nf['description'].'
                                    </div>
                                    <div class="nfCreatedTime">'.time_elapsed_string($nf['created_at'], false, $current_time).'</div>
                                </div>
                            </a>
                            
                        </div>
                    </div>
                </li>';
                
            }
            if($notificationAllCount > 5){
            }
        }
        if(!empty($html)){
        	echo json_encode(array('status' => 'success','data' => $html)); die;
        }
		
		
		echo json_encode(array('status' => 'fail')); die;
	}


	public function getPrintTransactionReceiptData()
	{
		$this->load->model('common_model');
		$transactionID = $this->czsecurity->xssCleanPostInput('id');
		$this->common_model->getPrintTransactionReceiptPDF($transactionID);
		echo json_encode(['success' => true, 'transactionID' => $transactionID]);
	}
	
	public function check_new_email()
	{
		$res = array();

		$merchantEmail  = $this->czsecurity->xssCleanPostInput('merchantEmail');
		$merchantID  = $this->czsecurity->xssCleanPostInput('merchantID');
		if($merchantID > 0){
			$get_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID ));
			$current_email = $get_data['merchantEmail'];
			if($current_email == $merchantEmail){
				$res = array('message' => 'Email Not Exitsts', 'status' => 'success');
				echo json_encode($res);
				die;
			}else{
				$app_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchantEmail' => $merchantEmail ));
			}
			
		}else{
			$app_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchantEmail' => $merchantEmail ));
		}
		if(count($app_data) > 0){
			$res = array('message' => 'Email Already Exitsts', 'status' => 'error');
			echo json_encode($res);
			die;
		}else{
			$res = array('message' => 'Email Not Exitsts', 'status' => 'success');
			echo json_encode($res);
			die;
		}
	}
	public function updateMerchant()
	{
		$res = array();

		$merchantEmail  = $this->czsecurity->xssCleanPostInput('merchantEmail');
		$firstName  = $this->czsecurity->xssCleanPostInput('firstName');
		$lastName  = $this->czsecurity->xssCleanPostInput('lastName');
		$merchantID  = $this->czsecurity->xssCleanPostInput('merchID');
		$controllerSet  = $this->czsecurity->xssCleanPostInput('controllerSet');

		if($merchantID > 0){
			$conditionMerch = array('merchID'=>$merchantID);
			$update_array =  array( 'firstName'  => $firstName, 'lastName' => $lastName, 'merchantEmail' => $merchantEmail);

        	$update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array); 

        	$this->session->set_flashdata('success', 'User Account Updated Successfully');
			
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
		}
		
		redirect($controllerSet.'home/my_account', 'refresh');
	}
	public function getRefundInvoice()
	{
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$transactionID =  $this->czsecurity->xssCleanPostInput('transactionID');
			$gatewaytype =  $this->czsecurity->xssCleanPostInput('gatewaytype');
			$integrationType =  $this->czsecurity->xssCleanPostInput('integrationType');

			
			$paydata = $this->general_model->getTransactionByIDOnlySuccess($transactionID,$integrationType,$user_id,1);
			$new_inv = '';
			$totalInvoiceAmount = 0.00;
			$invoiceIDs = [];
			$applySurcharge = false;
			$readonly = '';
			if(isset($paydata[0]['custom_data_fields']) && !empty($paydata[0]['custom_data_fields'])){
				$json_data = json_decode($paydata[0]['custom_data_fields'], 1);
                if(isset($json_data['invoice_surcharge']) && $json_data['invoice_surcharge']){
                    $applySurcharge = true;
                    $readonly = 'readonly="true"';
                }
			}

			if(count($paydata) > 0){
				/* With Invoice fetch html table*/
				$new_inv = '<div class="form-group alignTableInvoiceList" >
			      <div class="col-md-1 text-center"><b></b></div>
			        <div class="col-md-3 text-left"><b>Invoice</b></div>
			        <div class="col-md-3 text-right no-pad"><b>Date</b></div>
			        <div class="col-md-2 text-right"><b>Amount</b></div>
			        <div class="col-md-3 text-left"><b>Payment</b></div>
				   </div>';
				foreach ($paydata as $inv) {
					$new_inv .= '<div class="form-group alignTableInvoiceList" >
				       
				         <div class="col-md-1 text-center"><input  checked type="checkbox" class="chk_pay_refund check_'.$inv['RefNumber'].'" data-rowID="'. $inv['txnRowID'] .'" id="' . 'multiinv' . $inv['txnInvoiceID'] . '"  onclick="chk_refund_position_by_checkbox(this);" name="multi_inv[]" value="' . $inv['txnInvoiceID'] . '" /> </div>
				        <div class="col-md-3 text-left">' . $inv['RefNumber'] . '</div>
				        <div class="col-md-3 text-right  no-pad">' . date("M d, Y h:i A", strtotime($inv['transactionDate'])) . '</div>
				        <div class="col-md-2 text-right">' . '$' . number_format($inv['transactionAmount'], 2) . '</div>
					   <div class="col-md-3 text-left"><input '.$readonly.' type="text" name="pay_amount' . $inv['txnInvoiceID'] . '" onblur="chk_refund_position(this);"  class="form-control   multiinv' . $inv['txnInvoiceID'] . '  geter" data-id="multiinv' . $inv['txnInvoiceID'] . '" data-inv="' . $inv['txnInvoiceID'] . '" data-ref="' . $inv['RefNumber'] . '" data-value="' . $inv['transactionAmount'] . '"  value="' .number_format($inv['transactionAmount'], 2). '" /></div>
					   </div>';
						$inv_data[] = [
							'RefNumber' => $inv['RefNumber'],
							'txnInvoiceID' => $inv['txnInvoiceID'],
							'transactionAmount' => $inv['transactionAmount'],
						];
						$invoiceIDs[$inv['txnInvoiceID']] =  $inv['RefNumber'];
						$totalInvoiceAmount = $totalInvoiceAmount + $inv['transactionAmount'];
				}
			}else{
				/* Without Invoice fetch html table*/
				$paydata = $this->general_model->getTransactionByIDOnlySuccess($transactionID,$integrationType,$user_id,2);
				if(count($paydata) > 0){
					$new_inv = '<div class="form-group alignTableInvoiceList" >
				      <div class="col-md-1 text-center"><b></b></div>
				        <div class="col-md-3 text-left"><b>Invoice</b></div>
				        <div class="col-md-3 text-right no-pad"><b>Date</b></div>
				        <div class="col-md-2 text-right"><b>Amount</b></div>
				        <div class="col-md-3 text-left"><b>Payment</b></div>
					   </div>';
					foreach ($paydata as $inv) {
						$new_inv .= '<div class="form-group alignTableInvoiceList" >
					       
					         <div class="col-md-1 text-center"><input  checked type="checkbox" class="chk_pay_refund check_'.$inv['txnRowID'].'" data-rowID="'. $inv['txnRowID'] .'" id="' . 'multiinv' . $inv['txnRowID'] . '"  onclick="chk_refund_position_by_checkbox(this);" name="multi_inv[]" value="" /> </div>
					        <div class="col-md-3 text-left">--</div>
					        <div class="col-md-3 text-right  no-pad">' . date("M d, Y h:i A", strtotime($inv['transactionDate'])) . '</div>
					        <div class="col-md-2 text-right">' . '$' . number_format($inv['transactionAmount'], 2) . '</div>
						   <div class="col-md-3 text-left"><input type="text" '.$readonly.' name="pay_amount' . $inv['txnRowID'] . '" onblur="chk_refund_position(this);"  class="form-control   multiinv' . $inv['txnRowID'] . '  geter" data-id="multiinv' . $inv['txnRowID'] . '" data-inv="' . $inv['txnRowID'] . '" data-ref="' . $inv['transactionID'] . '" data-value="' . $inv['transactionAmount'] . '"  value="' .number_format($inv['transactionAmount'], 2). '" /></div>
						   </div>';
							$inv_data[] = [
								'RefNumber' => '--',
								'txnInvoiceID' => '',
								'transactionAmount' => $inv['transactionAmount'],
							];
							$invoiceIDs[$inv['txnRowID']] =  $inv['transactionID'];
							$totalInvoiceAmount = $totalInvoiceAmount + $inv['transactionAmount'];
					}
				}
				
			}
			$customerdata['invoices'] = $new_inv;
			$customerdata['invoiceIDs'] = $invoiceIDs;
			$customerdata['totalInvoiceAmount'] = $totalInvoiceAmount;
			$customerdata['integrationType'] = $integrationType;
			$customerdata['status']  = 'success';
			$customerdata['applySurcharge']  = $applySurcharge;
			echo json_encode($customerdata);
			die;
			

		}
	}
	

	public function check_merchant_user(){
		$userEmail  = $this->czsecurity->xssCleanPostInput('userEmail');
		$userID  = $this->czsecurity->xssCleanPostInput('userId');

		$is_exist_data = [
			'email' => $userEmail,
		];

		if(!empty($userID)){
			$is_exist_data['merchantUserID'] = $userID;
		}

		$isExistMessage = is_email_exists($is_exist_data);
		$isExistFlag = !empty($isExistMessage) ? false : true;

		echo json_encode(array('success' => $isExistFlag, 'message' => $isExistMessage)); die;
	}
	
	public function getTotalRefundOfInvoice()
	{
		if ($this->session->userdata('logged_in')) {
			$logged_in 				= $this->session->userdata('logged_in');
			$user_id 				= $logged_in['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$logged_in 				= $this->session->userdata('user_logged_in');
			$user_id 				= $logged_in['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$invoiceID =  $this->czsecurity->xssCleanPostInput('invoiceID');
			
			$integrationType =  $this->czsecurity->xssCleanPostInput('integrationType');

			
			$inv = $this->general_model->getTransactionByInvoiceID($invoiceID,$integrationType,$user_id,1);
			
			$applySurcharge = false;
			$readonly = '';
			if($logged_in['active_app'] == 1){
				$invoice_lineitem = $this->general_model->get_select_data('tbl_qbo_invoice_item', ['itemPrice'],array('invoiceID' => $invoiceID, 'merchantID' => $user_id, 'itemDescription' => 'Surcharge Fees'));
				if(isset($invoice_lineitem['itemPrice']) && $invoice_lineitem['itemPrice'] > 0){
	            	$applySurcharge = true;
	            	$readonly = 'readonly="true"';
	            }
			}elseif($logged_in['active_app'] == 2){

				$item_where = ['inv_itm_merchant_id' => $user_id, 'Descrip' => 'Surcharge Fees', 'TxnID' => $invoiceID];
	            $invoice_lineitem = $this->general_model->get_select_data('qb_test_invoice_lineitem', ['Rate'], $item_where);
	            if(isset($invoice_lineitem['Rate']) && $invoice_lineitem['Rate'] > 0){
	            	$applySurcharge = true;
	            	$readonly = 'readonly="true"';
	            }
			}elseif($logged_in['active_app'] == 5){
				$invoice_lineitem = $this->general_model->get_select_data('chargezoom_test_invoice_lineitem', ['Rate'], array('TxnID' => $invoiceID, 'Descrip' => 'Surcharge Fees'));
				if(isset($invoice_lineitem['Rate']) && $invoice_lineitem['Rate'] > 0){
	            	$applySurcharge = true;
	            	$readonly = 'readonly="true"';
	            }
			}
			$new_inv = '';
			$totalInvoiceAmount = 0.00;
			$invoiceIDs = [];

			if(isset($inv['id'])){
				/* With Invoice fetch html table*/
				$new_inv = '<div class="form-group alignTableInvoiceList" >
			      <div class="col-md-1 text-center"><b></b></div>
			        <div class="col-md-3 text-left"><b>Invoice</b></div>
			        <div class="col-md-3 text-right no-pad"><b>Date</b></div>
			        <div class="col-md-2 text-right"><b>Amount</b></div>
			        <div class="col-md-3 text-left"><b>Payment</b></div>
				   </div>';
				$new_inv .= '<div class="form-group alignTableInvoiceList" >
				       
			         <div class="col-md-1 text-center"><input  checked type="checkbox" class="chk_pay_invoice_refund check_'.$inv['RefNumber'].'" data-rowID="'. $inv['txnRowID'] .'" id="' . 'multiinv' . $inv['txnInvoiceID'] . '"  onclick="chk_invoice_refund_position_by_checkbox(this);" name="multi_inv[]" value="' . $inv['txnInvoiceID'] . '" /> </div>
			        <div class="col-md-3 text-left">' . $inv['RefNumber'] . '</div>
			        <div class="col-md-3 text-right  no-pad">' . date("M d, Y h:i A", strtotime($inv['transactionDate'])) . '</div>
			        <div class="col-md-2 text-right">' . '$' . number_format($inv['transactionAmount'], 2) . '</div>
				   <div class="col-md-3 text-left"><input type="text" '.$readonly.' name="pay_amount' . $inv['txnInvoiceID'] . '" onblur="chk_invoice_refund_position(this);"  class="form-control   multiinv' . $inv['txnInvoiceID'] . '  geter" data-id="multiinv' . $inv['txnInvoiceID'] . '" data-inv="' . $inv['txnInvoiceID'] . '" data-ref="' . $inv['RefNumber'] . '" data-value="' . $inv['transactionAmount'] . '" value="' .number_format($inv['transactionAmount'], 2). '"  /></div>
				   </div>';
					$inv_data[] = [
						'RefNumber' => $inv['RefNumber'],
						'txnInvoiceID' => $inv['txnInvoiceID'],
						'transactionAmount' => $inv['transactionAmount'],
					];
					$invoiceIDs[$inv['txnInvoiceID']] =  $inv['RefNumber'];
						$totalInvoiceAmount = $totalInvoiceAmount + $inv['transactionAmount'];
			}else{
				$inv = $this->general_model->getTransactionByInvoiceID($invoiceID,$integrationType,$user_id,2);
				
				if(isset($inv['id'])){
				
				}
			}
			$customerdata['invoices'] = $new_inv;
			$customerdata['invoiceIDs'] = $invoiceIDs;
			$customerdata['totalInvoiceAmount'] = $totalInvoiceAmount;
			$customerdata['applySurcharge']  = $applySurcharge;
			$customerdata['integrationType'] = $integrationType;
			$customerdata['status']  = 'success';
			echo json_encode($customerdata);
			die;
			

		}
	}
	public function getMerchantDefaultGatewaySupport(){
		
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$da 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}
		
		$condition = array('merchantID' => $user_id,'set_as_default' => 1 );
		$data = 	$this->general_model->get_row_data('tbl_merchant_gateway', $condition);

		$creditCardData = 	$this->general_model->get_table_data('tbl_merchant_gateway', array('merchantID' => $user_id,'creditCard' =>1));

		$eCheckData = 	$this->general_model->get_table_data('tbl_merchant_gateway', array('merchantID' => $user_id,'echeckStatus' =>1));

		if(isset($data['gatewayID'])){
			echo json_encode(array('status' => 'success', 'data' => $data,'creditCardData' => $creditCardData,'eCheckData' => $eCheckData)); die;
		}else{
			echo json_encode(array('status' => 'fail', 'data' => [],'creditCardData' => $creditCardData,'eCheckData' => $eCheckData)); die;
		}
		
	}
	public function printTransactionReceiptPDF()
	{
		$invoice_IDs = $this->session->userdata('invoice_IDs');
		$receipt_data = $this->session->userdata('receipt_data');

		foreach($receipt_data as $key => $rcData){
			$receipt_data[$key] = strip_tags($rcData);
		}
		
		$operator = '';
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
			$operator = $data['login_info']['firstName'].' '.$data['login_info']['lastName'];

		}

		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
			$operator = $data['login_info']['userFname'].' '.$data['login_info']['userLname'];
		}
		
		$app_data = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $user_id));

		$data['transactionAmount'] = '';
		$transactionCode = 0;
		$surCharge = 0;
		$isSurcharge = 0;
		$totalAmount = 0;
		$payAmountSet = 0;
		$transactionType = 0;
		$referenceMemo = '';
		$pdfFilePath = '';
		$transaction_id = '';
		$pdf_file_name = '';
		if(isset($receipt_data['transaction_id']) && !empty($receipt_data['transaction_id'])){

            $transaction_id = $receipt_data['transaction_id'];
            $con = array('transactionID' => $transaction_id);
            $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);
            
            $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount');
            $this->db->from('customer_transaction tr');
            $this->db->where($con);
            $this->db->group_by("tr.transactionID");
            $pay_amount = $this->db->get()->row_array();
			$customer_name = '';
			if ($app_data['appIntegration'] == '1') {
				$this->load->model('QBO_models/qbo_customer_model', 'customer_model');
				$customer_details = $this->customer_model->customer_by_id($pay_amount['customerListID'], $user_id);
                if($customer_details){
                    $customer_name = $customer_details->fullName;
                    if(!isset($receipt_data['billing_address1']) && $receipt_data['billing_address1'] == ''){
                    	$receipt_data['billing_name'] = '';
	                    $receipt_data['billing_address1'] = $customer_details->address1;
	                    $receipt_data['billing_address2'] = $customer_details->address2;
	                    $receipt_data['billing_city'] = $customer_details->City;
	                    $receipt_data['billing_zip'] = $customer_details->zipCode;
	                    $receipt_data['billing_state'] = $customer_details->State;
	                    $receipt_data['billing_country'] = $customer_details->Country;
	                    $receipt_data['shipping_name'] = '';
	                    $receipt_data['shipping_address1'] = $customer_details->ship_address1;
	                    $receipt_data['shipping_address2'] = $customer_details->ship_address2;
	                    $receipt_data['shipping_city'] = $customer_details->ship_city;
	                    $receipt_data['shipping_zip'] = $customer_details->ship_zipcode;
	                    $receipt_data['shipping_state'] = $customer_details->ship_state;
	                    $receipt_data['shiping_counry'] = $customer_details->ship_country;
	                    $receipt_data['Phone'] = $customer_details->phoneNumber;
	                    $receipt_data['Contact'] = $customer_details->userEmail;
                    }

                }
			}else if ($app_data['appIntegration'] == '2') {
				$this->load->model('customer_model', 'customer_model');
				$customer_details = $this->customer_model->customer_by_id($pay_amount['customerListID']);

                if($customer_details){
                    $customer_name = $customer_details->FullName;
                    if(!isset($receipt_data['billing_address1']) && $receipt_data['billing_address1'] == ''){
                    	$receipt_data['billing_name'] = '';
	                    $receipt_data['billing_address1'] = $customer_details->BillingAddress_Addr1;
	                    $receipt_data['billing_address2'] = $customer_details->BillingAddress_Addr2;
	                    $receipt_data['billing_city'] = $customer_details->BillingAddress_City;
	                    $receipt_data['billing_zip'] = $customer_details->BillingAddress_PostalCode;
	                    $receipt_data['billing_state'] = $customer_details->BillingAddress_State;
	                    $receipt_data['billing_country'] = $customer_details->BillingAddress_Country;
	                    $receipt_data['shipping_name'] = '';
	                    $receipt_data['shipping_address1'] = $customer_details->ShipAddress_Addr1;
	                    $receipt_data['shipping_address2'] = $customer_details->ShipAddress_Addr2;
	                    $receipt_data['shipping_city'] = $customer_details->ShipAddress_City;
	                    $receipt_data['shipping_zip'] = $customer_details->ShipAddress_PostalCode;
	                    $receipt_data['shipping_state'] = $customer_details->ShipAddress_State;
	                    $receipt_data['shiping_counry'] = $customer_details->ShipAddress_Country;
	                    $receipt_data['Phone'] = $customer_details->Phone;
	                    $receipt_data['Contact'] = $customer_details->Contact;
                    }
                    
                }
			}else if ($app_data['appIntegration'] == '3') {
				$this->load->model('Freshbook_models/fb_customer_model', 'customer_model');
				$customer_details = $this->customer_model->customer_by_id($pay_amount['customerListID'], $user_id);
                if($customer_details){
                    $customer_name = $customer_details->fullName;
                    if(!isset($receipt_data['billing_address1']) && $receipt_data['billing_address1'] == ''){
                    	$receipt_data['billing_name'] = '';
	                    $receipt_data['billing_address1'] = $customer_details->address1;
	                    $receipt_data['billing_address2'] = $customer_details->address2;
	                    $receipt_data['billing_city'] = $customer_details->City;
	                    $receipt_data['billing_zip'] = $customer_details->zipCode;
	                    $receipt_data['billing_state'] = $customer_details->State;
	                    $receipt_data['billing_country'] = $customer_details->Country;
	                    $receipt_data['shipping_name'] = '';
	                    $receipt_data['shipping_address1'] = $customer_details->ship_address1;
	                    $receipt_data['shipping_address2'] = $customer_details->ship_address2;
	                    $receipt_data['shipping_city'] = $customer_details->ship_city;
	                    $receipt_data['shipping_zip'] = $customer_details->ship_zipcode;
	                    $receipt_data['shipping_state'] = $customer_details->ship_state;
	                    $receipt_data['shiping_counry'] = $customer_details->ship_country;
	                    $receipt_data['Phone'] = $customer_details->phoneNumber;
	                    $receipt_data['Contact'] = $customer_details->userEmail;
                    }

                }
			}else if ($app_data['appIntegration'] == '4') {
				$this->load->model('Xero_models/xero_customer_model', 'customer_model');

				$customer_details = $this->customer_model->customer_by_id($pay_amount['customerListID'], $user_id);
                if($customer_details){
                    $customer_name = $customer_details->fullName;
                    if(!isset($receipt_data['billing_address1']) && $receipt_data['billing_address1'] == ''){
                    	$receipt_data['billing_name'] = '';
	                    $receipt_data['billing_address1'] = $customer_details->address1;
	                    $receipt_data['billing_address2'] = $customer_details->address2;
	                    $receipt_data['billing_city'] = $customer_details->City;
	                    $receipt_data['billing_zip'] = $customer_details->zipCode;
	                    $receipt_data['billing_state'] = $customer_details->State;
	                    $receipt_data['billing_country'] = $customer_details->Country;
	                    $receipt_data['shipping_name'] = '';
	                    $receipt_data['shipping_address1'] = $customer_details->ship_address1;
	                    $receipt_data['shipping_address2'] = $customer_details->ship_address2;
	                    $receipt_data['shipping_city'] = $customer_details->ship_city;
	                    $receipt_data['shipping_zip'] = $customer_details->ship_zipcode;
	                    $receipt_data['shipping_state'] = $customer_details->ship_state;
	                    $receipt_data['shiping_counry'] = $customer_details->ship_country;
	                    $receipt_data['Phone'] = $customer_details->phoneNumber;
	                    $receipt_data['Contact'] = $customer_details->userEmail;
                    }

                }
			}else if ($app_data['appIntegration'] == '5') {
				$this->load->model('company/customer_model', 'customer_model');
				$customer_details = $this->customer_model->customer_by_id($pay_amount['customerListID']);

                if($customer_details){
                    $customer_name = $customer_details->FullName;
                    if(!isset($receipt_data['billing_address1']) && $receipt_data['billing_address1'] == ''){
                    	$receipt_data['billing_name'] = '';
	                    $receipt_data['billing_address1'] = $customer_details->BillingAddress_Addr1;
	                    $receipt_data['billing_address2'] = $customer_details->BillingAddress_Addr2;
	                    $receipt_data['billing_city'] = $customer_details->BillingAddress_City;
	                    $receipt_data['billing_zip'] = $customer_details->BillingAddress_PostalCode;
	                    $receipt_data['billing_state'] = $customer_details->BillingAddress_State;
	                    $receipt_data['billing_country'] = $customer_details->BillingAddress_Country;
	                    $receipt_data['shipping_name'] = '';
	                    $receipt_data['shipping_address1'] = $customer_details->ShipAddress_Addr1;
	                    $receipt_data['shipping_address2'] = $customer_details->ShipAddress_Addr2;
	                    $receipt_data['shipping_city'] = $customer_details->ShipAddress_City;
	                    $receipt_data['shipping_zip'] = $customer_details->ShipAddress_PostalCode;
	                    $receipt_data['shipping_state'] = $customer_details->ShipAddress_State;
	                    $receipt_data['shiping_counry'] = $customer_details->ShipAddress_Country;
	                    $receipt_data['Phone'] = $customer_details->Phone;
	                    $receipt_data['Contact'] = $customer_details->Contact;
                    }

                }
			}
			
            if(isset($pay_amount['transactionAmount'])){
            	$payAmountSet = $pay_amount['transactionAmount'];
            	$transactionType = $pay_amount['transactionGateway'];

            	if($pay_amount['transactionGateway'] == 10){

            		$resultAmount = getiTransactTransactionDetails($user_id,$transaction_id);
            		$totalAmount = $resultAmount['totalAmount'];
            		$surCharge = $resultAmount['surCharge'];
            		$isSurcharge = $resultAmount['isSurcharge'];
            		if($resultAmount['payAmount'] != 0){
            			$payAmountSet = $resultAmount['payAmount'];
            		}   
            	}
            	$transactionCode = $pay_amount['transactionCode'];
            }
			$referenceMemo = $pay_amount['referenceMemo'];
            $data['transactionAmount'] = $payAmountSet;
            $data['transactionDetail'] = $pay_amount;
	        

	        $data['transactionCode'] = $transactionCode;
	        $data['isSurcharge'] = $isSurcharge;
	        $data['surchargeAmount'] = $surCharge;
	        $data['totalAmount'] = $totalAmount;
	        $data['transactionType'] = $transactionType;


			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
		
			$customer_data = $receipt_data;
			$invoice_data = [];
			if (!empty($invoice_IDs)) {
				foreach ($invoice_IDs as $inID) {
					if ($app_data['appIntegration'] == '1') {
						$condition2 = array('invoiceID' => $inID, 'merchantID' => $user_id);
						$invoice_data[]  = $this->general_model->get_row_data('QBO_test_invoice', $condition2);
					}else if ($app_data['appIntegration'] == '2') {
						$condition2 = array('TxnID' => $inID);
						$invoice_data[]  = $this->general_model->get_row_data('qb_test_invoice', $condition2);
					}else if ($app_data['appIntegration'] == '3') {
						$condition2 = array('invoiceID' => $inID, 'merchantID' => $user_id);
						$invoice_data[]  = $this->general_model->get_row_data('Freshbooks_test_invoice', $condition2);
					}else if ($app_data['appIntegration'] == '4') {
						$condition2 = array('invoiceID' => $inID, 'merchantID' => $user_id);
						$invoice_data[]  = $this->general_model->get_row_data('Xero_test_invoice', $condition2);
					}else{
						$condition2 = array('TxnID' => $inID);
						$invoice_data[]  = $this->general_model->get_row_data('chargezoom_test_invoice', $condition2);
					}
				}
			}
			if(isset($receipt_data['refundAmount']) && !empty($receipt_data['refundAmount'])){
				$data['transactionAmount'] = $receipt_data['refundAmount'];
			}

			$data['invoice_IDs'] = $invoice_IDs;
			$data['invoice_data'] = $invoice_data;

			ini_set('memory_limit','256M'); 
		  	$this->load->library("TPdf");
			
			$config_data = $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID'=> $user_id ));
			if(!empty($config_data['ProfileImage'])){
				$logo = LOGOURL1.$config_data['ProfileImage'];
			}else{
				$logo = CZLOGO;
			}

			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);   
			if(!defined('PDF_HEADER_TITLE')){
				define(PDF_HEADER_TITLE,'Invoice');
			} 

			if(!defined('PDF_HEADER_STRING')){
				define(PDF_HEADER_STRING,'');
			} 
		    $pdf->SetPrintHeader(False);
		    $pdf->SetCreator(PDF_CREATOR);
		    $pdf->SetAuthor('Chargezoom 1.0');
		    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		    $pdf->SetFont('dejavusans', '', 10, '', true);   
		    $pdf->setCellPaddings(1, 1, 1, 1);
			$pdf->setCellMargins(1, 1, 1, 1);
	    	$pdf->AddPage();
	  		
	  		$y = 20;
			$logo_div = '<div style="text-align:left; float:left ">
			<img src="'.$logo.'" border="0" height="50"/>
			</div>';
			
			$path = FCPATH.'uploads/transaction_pdf/';
			$pdf_file_name = 'Transaction-Receipt-'.$transaction_id.'.pdf';
	        $pdfFilePath = $path.$pdf_file_name;

	        if (!file_exists($path)) {
			    mkdir($path, 0777, true);
			}

			// set color for background
			$pdf->SetFillColor(255, 255, 255);
			// write the first column
			$pdf->writeHTMLCell(80, 50, 12, $y, $logo_div, 0, 0, 1, true, 'J', true);


			$y = 50;

			// set color for background
			$pdf->SetFillColor(255, 255, 255);

			// set color for text
			$pdf->SetTextColor(51, 51, 51);

			$invoice_id = '';
			$transactionDetail = $data['transactionDetail']; 
            $custom_data = (isset($transactionDetail) && isset($transactionDetail['custom_data_fields'])) ? $transactionDetail['custom_data_fields'] : false;
            $po_number = '';
			$payment_type = '';
            if($custom_data){
                $json_data = json_decode($custom_data, 1);
                if(isset($json_data['invoice_number'])){
                    $invoice_id = $json_data['invoice_number'];
                }
                if(isset($json_data['po_number'])){
                    $po_number = $json_data['po_number'];
                }
				if(isset($json_data['payment_type'])){
                    $payment_type = $json_data['payment_type'];
                }
            }
            $inv_ref = [];
            if(empty($invoice_id)){
            	if($invoice_data){
            		foreach ($invoice_data as $invoice) {
	            		if ($app_data['appIntegration'] == '1') {
	            			$inv_ref[] =  $invoice['refNumber'];
	            		}else{
							$inv_ref[] =  $invoice['RefNumber'];
	            		}
            		}
            	}
            }

            if($inv_ref){
            	$invoice_id = implode(',', $inv_ref);
            }
            $transactionDetail = $data['transactionDetail']; 
            $custom_data = isset($transactionDetail['custom_data_fields']) ? $transactionDetail['custom_data_fields'] : false;
            $date = (isset($transactionDetail['transactionDate']) && !empty($transactionDetail['transactionDate'])) ? $transactionDetail['transactionDate'] : date("Y-m-d H:i:s");
            if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
                $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
                $date = getTimeBySelectedTimezone($timezone);
            }

			$html = '';
			$html .= '<h2>Transaction Receipt</h2></br></br></br></br>';
			// write the first column
			$pdf->writeHTMLCell(80, 50, '', $y, $html, 0, 0, 0, true, 'J', true);

			$html = '';
			
			$returnMessageObj = messageConversionObj($transactionDetail,'');
			
			if(isset($returnMessageObj) && !empty($returnMessageObj)){
                if(isset($customer_data['checkPlan']) && !$customer_data['checkPlan']){
                        
                    $html .= '<span style="font-size: 12px;color:#cf4436;">Transaction Failed - Transaction limit has been reached please do upgrade your plan.</span><br/>';
                }else{
                    
                    $html .= '<span style="font-size: 12px;color: '.$returnMessageObj['returnColorCode'].';">'.$returnMessageObj['message'].'</span><br/>';
                }
            }else{
            	$html .= '<span style="font-size: 12px;color: #cf4436;;">Transaction Failed - Declined</span><br/>';
            }
			
			
			$pdf->writeHTMLCell(80, 50, '', 60, $html, 0, 0, 0, true, '', true);
			
			$html = '';
			if(isset($transactionDetail['transactionType']) && strpos(strtolower($transactionDetail['transactionType']), 'refund') !== false){
				$html .= '<b style="font-size: 14px;">Amount: </b><span style="font-size: 12px;">('.priceFormat($data['transactionAmount'], true).')</span><br/>';
			}else{
				$html .= '<b style="font-size: 14px;">Amount: </b><span style="font-size: 12px;">'.priceFormat($data['transactionAmount'], true).'</span><br/>';
			}
			
			$pdf->writeHTMLCell(80, 50, '', 70, $html, 0, 0, 0, true, 'J', true);
			$html = '';
			$html .= '<b>Date: </b><span>'.date("m/d/Y h:i A", strtotime($date)).'</span><br/><br/>';
			$html .= '<b>IP Address: </b><span>'.getClientIpAddr().'</span><br/><br/>';
			$html .= '<b>Invoice(s): </b><span>'.$invoice_id.'</span><br/><br/>';
			$html .= '<b>Payment Type: </b><span>'.$payment_type.'</span><br/><br/>';
			// write the first column
			$pdf->writeHTMLCell(80, 50, '', 80, $html, 0, 0, 0, true, 'J', true);

			$html = ' ';
			$html .= '<br/>';
			$html .= '<b>Operator: </b><span>'.$operator.'</span><br/><br/>';
			$html .= '<b>Transaction ID: </b><span>'.$transaction_id.'</span><br/><br/>';
			$html .= '<b>PO Number: </b><span>'.$po_number.'</span><br/><br/>';

			// write the first column
			$pdf->writeHTMLCell(80, 50, '', '', $html, 0, 1, 1, true, 'J', true);

			$billingAdd = 0;
            $shippingAdd = 0;

            $isAdd = 0;
            if($customer_data['billing_address1']  || $customer_data['billing_address2'] || $customer_data['billing_city'] || $customer_data['billing_state'] || $customer_data['billing_zip'] || $customer_data['billing_country']){
                $billingAdd = 1;
                $isAdd = 1;
            }
            if($customer_data['shipping_address1']  || $customer_data['shipping_address2'] || $customer_data['shipping_city'] || $customer_data['shipping_state'] || $customer_data['shipping_zip'] || $customer_data['shiping_counry'] ){
                $shippingAdd = 1;
                $isAdd = 1;
            }
            $phone_icon = '<img src="'.FCPATH.IMAGES.'phone_icon.png'.'" width="10" height="10"/>';
            $envelope_icon = '<img src="'.FCPATH.IMAGES.'envelope_icon.png'.'" width="10" height="10"/>';
            if($isAdd){
            	if($billingAdd){
            		$html = '<table style="border: 1px solid #e8e8e8;" cellpadding="4"  >
				    	<tr style="border: 1px solid #e8e8e8;background-color:#f9f9f9;">
				        	<th style="border: 1px solid #f9f9f9;color: #333333;" align="left"> Billing Address</th>
				    	</tr>';

				   	$address_html = '<br><br><span>  '.$customer_data['billing_name'].'</span><br/><span>  </span>';
				   	$address_html .= '<span>'.$customer_name.'</span><br/><br/><span>  </span>';
				   
				   	if ($customer_data['billing_address1'] != '') {
                    	$address_html .= $customer_data['billing_address1'].'<br><span>  </span>'; 
                    }

                    if ($customer_data['billing_address2'] != '') {
                    	$address_html .= $customer_data['billing_address2'].'<br><span>  </span>'; 
                    }

                    $address_html .= ''.($customer_data['billing_city']) ? $customer_data['billing_city'] . ',' : '';

                        $address_html .= ($customer_data['billing_state']) ? $customer_data['billing_state'] : '';
                        $address_html .= ($customer_data['billing_zip']) ? $customer_data['billing_zip'].'<br><span>  </span>' : ''; 
                        $address_html .= ($customer_data['billing_country']) ? $customer_data['billing_country'].'<br><span>  </span>' : ''; 
                    $address_html .= '<br><span>  </span>';  
                    $address_html .= '<span style=""> '.$phone_icon.'</span> '.$customer_data['Phone'].'<br><span>  </span>';
                    $address_html .= '<span style=""> '.$envelope_icon.'</span> <a href="javascript:void(0)"  style="text-decoration: none;">'.$customer_data['Contact'].'</a><br>';
					$html .=  '<tr style="border: 1px solid #e8e8e8;"><td  style="border: 1px solid #e8e8e8;" colspan="2">'.$address_html .'</td></tr>';

				   	$html .= '</table>';
            		
				   	$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 0, 1, true, 'J', true);
				}

				if($shippingAdd){
            		
				   	$html = '<table style="border: 1px solid #e8e8e8;" cellpadding="4"  >
				    	<tr style="border: 1px solid #e8e8e8;background-color:#f9f9f9;">
				        	<th style="border: 1px solid #f9f9f9;color: #333333;" align="left"> Shipping Address</th>
				    	</tr>';

				    $address_html = '<br><br><span>  '.$customer_data['shipping_name'].'</span><br/><span>  </span>';
				   	$address_html .= '<span>'.$customer_name.'</span><br/><br/><span>  </span>';

				   	if ($customer_data['shipping_address1'] != '') {
                    	$address_html .= $customer_data['shipping_address1'].'<br><span>  </span>'; 
                    }

                    if ($customer_data['shipping_address2'] != '') {
                    	$address_html .= $customer_data['shipping_address2'].'<br><span>  </span>';
                    }

                    $address_html .= ''.($customer_data['shipping_city']) ? $customer_data['shipping_city'] . ',' : '';

                        $address_html .= ($customer_data['shipping_state']) ? $customer_data['shipping_state'] : '';
                        $address_html .= ($customer_data['shipping_zip']) ? $customer_data['shipping_zip'].'<br><span>  </span>' : ''; 
                        $address_html .= ($customer_data['shiping_counry']) ? $customer_data['shiping_counry'].'<br><span>  </span>' : ''; 
                    $address_html .= '<br><span>  </span>';  
                    $address_html .= '<span style=""> '.$phone_icon.'</span> '.$customer_data['Phone'].'<br><span>  </span>';
                    $address_html .= '<span style=""> '.$envelope_icon.'</span> <a href="javascript:void(0)" style="text-decoration: none;">'.$customer_data['Contact'].'</a><br>';
					$html .=  '<tr style="border: 1px solid #e8e8e8;"><td  style="border: 1px solid #e8e8e8;" colspan="2">'.$address_html .'</td></tr>';

				   	$html .= '</table>';
				   	$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 1, 1, true, 'J', true);
				}
			}

			if(!empty($referenceMemo))
			{
				$html = '<b>Reference Memo: </b><span>'.$referenceMemo.'</span><br/><br/>';
				$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 0, 1, true, 'J', true);
			}

			$pdf->Output($pdfFilePath, 'F'); 
		}
		if($pdfFilePath){
			redirect('ajaxRequest/transactionReceipt/'.$transaction_id);
		}else{
			if ($app_data['appIntegration'] == '1') {
				redirect('QBO_controllers/home/index');
			}else if ($app_data['appIntegration'] == '2') {
				redirect('home/index');
			}else{
				redirect('company/home/index');
			}
		}
	}

	public function transactionReceipt($transaction_id)
	{
		$pdf_file_name = 'Transaction-Receipt-'.$transaction_id.'.pdf';
		$path = base_url('uploads/transaction_pdf/'.$pdf_file_name);
		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=".$pdf_file_name);
		@readfile($path);
	}
	
	public function check_session()
	{
		if ($this->session->userdata('logged_in') != "") {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}else if ($this->session->userdata('user_logged_in') != "") {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		} else {
			$merchantID = false;
		}
		
		echo json_encode(['status' => ($merchantID ? 'success' : 'failed')]);
		die;
	}
	/**
     * Description: Function used to update merchant surcharge status enable and disable
    */ 
	public function surchargeStatusUpdate()
	{
		$merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
		$appIntegration = $this->czsecurity->xssCleanPostInput('appIntegration');
		$surchargeStatus = $this->czsecurity->xssCleanPostInput('surchargeStatus');
		$merch_data = $this->general_model->get_row_data('tbl_merchant_surcharge', array('merchantID' => $merchantID));
		if(isset($merch_data['id'])){
			$conditionMerch = array('merchantID'=>$merchantID,'id' => $merch_data['id']);
  			$update_array =  array( 'isEnable'  => $surchargeStatus);

			$update = $this->general_model->update_row_data('tbl_merchant_surcharge',$conditionMerch, $update_array); 

		}else{
			$surChargeData = array(
				'isEnable' => $surchargeStatus,
				'surchargePercent' => '3.00',
				'merchantID' => $merchantID,
				'defaultItem' => 'Surcharge Fees',
				'defaultItemAccount' => 'Surcharge Income',
			);
			$insert = $this->general_model->insert_row('tbl_merchant_surcharge',$surChargeData);

		}
		/*SYNC QUICKBOOKS*/
		if($surchargeStatus == 1){
			/* QBO SYNC*/
			if($appIntegration == 1){
				require APPPATH .'libraries/qbo/QBO_data.php';
				$this->qboOBJ = new QBO_data($merchantID);
				$syncQBO = $this->qboOBJ->createDefaultSyncRecords($merchantID);
			}elseif($appIntegration == 2){
				/* QBD SYNC*/
				require APPPATH .'libraries/QBD_Sync.php';
				$this->qbdOBJ = new QBD_Sync($merchantID);
				$syncQBD = $this->qbdOBJ->createDefaultSyncRecords($merchantID);
			}	
		}
		if($surchargeStatus){
			$this->session->set_flashdata('success', 'Surcharge Enabled Successfully');
		}else{
			$this->session->set_flashdata('success', 'Surcharge Disabled Successfully');
		}
		if($appIntegration == 1){
	        $formURL = base_url('QBO_controllers/SettingConfig/surcharging');
	    }else if($appIntegration == 2){
	        $formURL = base_url('SettingConfig/surcharging');
	    }else if($appIntegration == 3){
	        $formURL = base_url('Freshbook_controllers/SettingConfig/surcharging');
	    }else if($appIntegration == 4){
	        $formURL = base_url('Xero_controllers/SettingConfig/surcharging');
	    }else if($appIntegration == 5){
	        $formURL = base_url('company/SettingConfig/surcharging');
	    }else{
	        $formURL = base_url('/');
	    }
		redirect($formURL, 'refresh');
	}

	/**
     * Function to return transaction list
     * 
     * @return json
     */
	public function view_integrations_transaction()
	{
		$res = array();

		$this->load->model('customer_model');

		if ($this->input->post('action') == 'transaction') {
			$invoiceID = $this->input->post('trID');
			$action = 'transaction';
		} else {
			$invoiceID = $this->input->post('invoiceID');
			$action = 'invoice';
		}
		$user_id 				= $this->merchantID;
		$transactions           = $this->data_model->get_invoice_transaction_data($invoiceID, $user_id, $action);
		$tr_data = '';
		$tr_head = '';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
				$user_name = $transaction['fullName'];
				// if type 1 then get merchant name
				if($transaction['transaction_by_user_type'] == 1){
					$userData = $this->customer_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $transaction['transaction_by_user_id']]);
					$user_name = $userData['firstName'].' '.$userData['lastName'];
				}elseif($transaction['transaction_by_user_type'] == 2){
					// if type 2 then get sub user name
					$userData = $this->customer_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $transaction['transaction_by_user_id'], 'merchantID' => $transaction['merchantID']]);
					$user_name = $userData['userFname'].' '.$userData['userLname']; 
				}elseif($transaction['transaction_by_user_type'] == 4){
					// if type 4 then automatic payment
					$user_name = 'Automatic Payment'; 
				}
				
				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];

				$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs">Amount</th>
								<th class="hidden-xs text-right">User</th>
								<th class="text-right hidden-xs">Gateway</th>
								<th class="text-right hidden-xs" >Transaction ID</th>
							</tr>
						</thead>';

				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3' && trim($transaction['transaction_user_status']) != '5') {
					$status = 'Success';
				} else if ($transaction['transaction_user_status'] == '3' || $transaction['transaction_user_status'] == '5') {
					$status = ($transaction['transaction_user_status'] == 5) ? 'Authorize' : 'Void';
				} else {
					$status = 'Failed';
				}

				
				$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">$' . number_format($transaction['transactionAmount'], 2) . '</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $transaction['transactionID'] . '</td>
						</tr>';

				$getTransactionRefundData = $this->customer_model->get_select_data('customer_transaction', ['*'], 'parent_id = "'.$transaction['id'].'" AND (transactionCode = "1" OR transactionCode = "100" OR transactionCode = "200" OR transactionCode = "300")');

				if($getTransactionRefundData){
					// if type 1 then get merchant name
					if($getTransactionRefundData['transaction_by_user_type'] == 1){
						$userDataRefund = $this->customer_model->get_select_data('tbl_merchant_data', ['firstName', 'lastName'], ['merchID' => $getTransactionRefundData['transaction_by_user_id']]);
						$refund_user_name = $userDataRefund['firstName'].' '.$userDataRefund['lastName'];
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 2){
						// if type 2 then get sub user name
						$userDataRefund = $this->customer_model->get_select_data('tbl_merchant_user', ['userFname', 'userLname'], ['merchantUserID' => $getTransactionRefundData['transaction_by_user_id'], 'merchantID' => $getTransactionRefundData['merchantID']]);
						$refund_user_name = $userDataRefund['userFname'].' '.$userDataRefund['userLname']; 
					}elseif($getTransactionRefundData['transaction_by_user_type'] == 4){
						// if type 4 then automatic payment
						$refund_user_name = 'Automatic Payment'; 
					}
					$tr_data .= '<tr class="paylist_data">' .
					'<td class="hidden-xs text-right">-$' . number_format($transaction['transactionAmount'], 2) . '</td>' .
					'<td class="text-right visible-lg"><span class="">' . ucfirst($refund_user_name) . '</span></td>' .
					'<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $getTransactionRefundData['transactionID'] . '</td>
						</tr>';
				}
			}
		} else {
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}

		$tr_data = $tr_head . $tr_data;

		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}
}
