<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Chargezoom\Core\Http\LoginRequest;
use \Chargezoom\Core\Authentication\Throttle;
use \Chargezoom\Core\Http\SessionRequest;
class Login extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('general_model');
		$this->load->model('login_model');

		define('INTERFACE_TYPE','Merchant');
	}

	public function index()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$port_url = current_url();

		$new_url   = explode('://', $port_url);


		if ($new_url[1] == 'payportal.net/' || $new_url[1] == 'www.payportal.net/') {

			redirect(base_url('login/main_page'));
		}

		$logo_data = explode('.', $new_url[1]);

		if(ENVIRONMENT == 'production' && count($logo_data) < 3){
			redirect("https://dashboard.".$new_url[1]);
		}



		$l_con    = array('portalprefix' => $logo_data[0]);
		$l_data = $this->login_model->get_domain_logo($l_con);


		if (!empty($l_data) && $l_data['type'] == 'Customer') {

			?> <script>
							window.location.href = '<?php echo base_url() . 'customer/'; ?>'
						</script>
			<?php
		}
		
		if (!empty($l_data)) {

			if ($this->session->userdata('logged_in')) {
				if ($this->session->userdata('logged_in')['active_app'] == '2')
					redirect('home', 'refresh');
				else  if ($this->session->userdata('logged_in')['active_app'] == '1')
					redirect('QBO_controllers/home', 'refresh');
				else  if ($this->session->userdata('logged_in')['active_app'] == '3')
					redirect('FreshBooks_controllers/home', 'refresh');
				else  if ($this->session->userdata('logged_in')['active_app'] == '4')
					redirect('Integration/home', 'refresh');
				else  if ($this->session->userdata('logged_in')['active_app'] == '5')
					redirect('company/home', 'refresh');
			}

			$data['logo']    = $l_data['resellerProfileURL'];
			$data['merchantHelpText'] = $l_data['merchantHelpText'];
		} else {

			redirect(base_url('not_found'));
		}

		$data['reseller_list'] = $this->general_model->get_table_data('tbl_reseller', array('isDelete' => '0'));
		$this->load->view('template/template_start', $data);
		$this->load->view('pages/login', $data);
		$this->load->view('template/template_scripts', $data);
		$this->load->view('template/template_end', $data);
	}


	public function main_page()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();


		$this->load->view('template/template_start', $data);
		$this->load->view('pages/page_comingsoon');
		$this->load->view('template/template_end', $data);
	}
	function user_login()
	{

		if(!$this->czsecurity->csrf_verify())
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: </strong>'.$this->czsecurity->errorMessages('token_expired').'</div>');
			redirect(base_url('/'));
		}

		$request 	= new LoginRequest($this->czsecurity->xssCleanPostInput('login-email'));
		$throttle 	= new Throttle($this->db, $request);
		$sessionRequest 	= new SessionRequest($this->db);
		try {

			$seconds = $throttle->checkLock();

			if ($seconds) {
				$this->session->set_flashdata(
					'error',
					'You have exceeded the maximum login attempts. You can retry again after ' . $seconds . ' seconds.'
				);

				redirect(base_url('/', 'refresh'));
			}
		} catch (\Exception $ex) {
			// For the moment... silence the exception
			// In the future - pass exception to Sentry
		}

		$conditionC  = array('merchantEmail' => $this->czsecurity->xssCleanPostInput('login-email'));
		$checkEmail     = $this->general_model->get_row_data('tbl_merchant_data', $conditionC);
		$suspendmessage = 'You have reached the maximum number of incorrect password limits. Your account has been suspended for 24 hours.';

		if(isset($checkEmail['merchID'])){
			
			/*Check account deactivate by failed attamp*/

			if($checkEmail['isLockedTemp'] == 1){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> '.$suspendmessage.'</div>');
				redirect(base_url('/'));
			}

			$user = $this->login_model->check_user_mecrchant($this->czsecurity->xssCleanPostInput('login-email'), $this->czsecurity->xssCleanPostInput('login-password'));


			if ($user && isset($user->merchantEmail)) {

				/*Update login attemps true*/
				if ($user->isSuspend == '0') {
					try {
						$throttle->addAttempt(true);
					} catch (\Exception $ex) {
						// For the moment... silence the exception
						// In the future - pass exception to Sentry
					}

					$sess_array = (array) $user;
					
					/* Update session record */
					$updateSession = $sessionRequest->updateSessionUser(SessionRequest::USER_TYPE_MERCHANT,$user->merchID,$this->session->userdata('session_id'));
					/*End session update*/

					if ($user->firstLogin == 0) {

						$condition  = array('merchantID' => $user->merchID);
						$config     = $this->general_model->get_row_data('tbl_config_setting', $condition);
						$appset     = $this->general_model->get_row_data('app_integration_setting', $condition);
						if (!empty($appset)) {
							$sess_array['active_app']	= $appset['appIntegration'];
							$sess_array['gatewayMode']	= $appset['transactionMode'];
						} else {
							$sess_array['active_app']	= '2';
							$sess_array['gatewayMode']	= 0;
						}
						$gatway_num =  $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);

						if ($gatway_num) {
							$sess_array['merchant_gateway']	= '1';
						} else {
							$sess_array['merchant_gateway']	= '0';
						}
						$filenum                   = $this->general_model->get_num_rows('tbl_company', $condition);
						$sess_array['fileID']      = $filenum;

						if (!empty($config)) {

							$sess_array['portalprefix']      = $config['portalprefix'];
						} else {
							$sess_array['portalprefix']      = '';
						}

						/* <---------------for Chat start-----------------> */
						$rid = $user->resellerID;
						$rs    = $this->general_model->get_row_data('tbl_reseller', array('resellerID' => $rid));
						if (!empty($rs)) {

						
							$sess_array['script']      = '';
							$sess_array['logo_img']  = '';
						} else {
							$sess_array['script']      = '';
							$sess_array['logo_img']    = '';
						}
						/* <------------End--------------> */



						if ($user->companyName != "" && $user->merchantAddress1 != "" && $user->merchantFullAddress != "" &&  $sess_array['merchant_gateway'] != "0" && $sess_array['fileID'] > 0) {

							$input_data   = array('firstLogin' => '1');
							$condition1   = array('merchID' => $user->merchID);
							$this->general_model->update_row_data('tbl_merchant_data', $condition1, $input_data);
						}


						$sess_array['active_app'] = '0';
						$this->session->set_userdata('logged_in', $sess_array);
						redirect(base_url('firstlogin/dashboard_first_login'));
					} else {

						$condition  = array('merchantID' => $user->merchID);
						$gatway_num =  $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);

						if ($gatway_num) {
							$sess_array['merchant_gateway']	= '1';
						} else {
							$sess_array['merchant_gateway']	= '0';
						}

						/* <---------------for Chat start-----------------> */
						$rid = $user->resellerID;
						$rs    = $this->general_model->get_row_data('tbl_reseller', array('resellerID' => $rid));


						if (!empty($rs)) {

							$sess_array['script']      = $rs['Chat'];
							$sess_array['logo_img']  = $rs['ProfileURL'];
						} else {
							$sess_array['script']      = '';
							$sess_array['logo_img']    = '';
						}
						/* <------------End--------------> */



						$this->general_model->track_user("Merchant", $sess_array['merchantEmail'], $sess_array['merchID']);


						$config     = $this->general_model->get_row_data('tbl_config_setting', $condition);
						if (!empty($config)) {

							$sess_array['portalprefix']      = $config['portalprefix'];
						} else {
							$sess_array['portalprefix']      = '';
						}
						$appset     = $this->general_model->get_row_data('app_integration_setting', $condition);
						if (!empty($appset)) {
							$sess_array['active_app']	= $appset['appIntegration'];
							$sess_array['gatewayMode']	= $appset['transactionMode'];
						} else {
							$sess_array['active_app']	= '2';
							$sess_array['gatewayMode']	= 0;
						}

						$this->session->set_userdata('logged_in', $sess_array);
						$merchantID       = $this->session->userdata('logged_in')['merchID'];
						if ($this->general_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

							$fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];

							$templatedatas =  $this->general_model->get_table_data('tbl_email_template_data', '');

							foreach ($templatedatas as $templatedata) {
								$insert_data = array(
									'templateName'  => $templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									'merchantID'      => $merchantID,
									'fromEmail'      => DEFAULT_FROM_EMAIL,
									'message'		  => $templatedata['message'],
									'emailSubject'   => $templatedata['emailSubject'],
									'createdAt'      => date('Y-m-d H:i:s')
								);
								$this->general_model->insert_row('tbl_email_template', $insert_data);
							}
						}


						if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

							$this->general_model->insert_inv_number($merchantID);
						}

						if ($this->general_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
							$this->general_model->insert_payterm($merchantID);
						}






						if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '1') {
							$furl  =	base_url('QBO_controllers/home/index');
							
						} else 
	                 if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '3') {
							$furl  =	base_url('FreshBooks_controllers/home/index');
							
						} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '4') {
							$furl =	base_url('Integration/home/index');
						} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '5') {
							$furl =	base_url('company/home/index');
						} else {
							$furl  =	base_url('home/index');
							
						}

						redirect($furl);
					}


					$this->session->set_userdata('logged_in', $sess_array);


					redirect(base_url('home/index'));
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"> Your account has been suspended.</div>');
					redirect(base_url('/'));
				}
			} else {
				$conditionC  = array('merchantEmail' => $this->czsecurity->xssCleanPostInput('login-email'));
				$checkEmail     = $this->general_model->get_row_data('tbl_merchant_data', $conditionC);

				if(isset($checkEmail['merchID'])){
					$login_failed_attemp = $checkEmail['login_failed_attemp'];

					if($login_failed_attemp == 10){
						$this->session->set_flashdata('message', '<div class="alert alert-danger"> Your account has been suspended for 24 hours.</div>');
						redirect(base_url('/'));
					}

					$login_failed_attemp_new = $login_failed_attemp + 1;

					if($login_failed_attemp_new >= 10){
						
						$input_data   = array('login_failed_attemp' => $login_failed_attemp_new,'isLockedTemp' => 1,'isSuspend' => 1,'accountLockedDate' => date('Y-m-d H:i:s'),'lockedMessage' => $suspendmessage);

						$condition1   = array('merchID' => $checkEmail['merchID']);
						
						$this->general_model->update_row_data('tbl_merchant_data',$condition1, $input_data);

						$this->session->set_flashdata('message', '<div class="alert alert-danger"> '.$suspendmessage.'</div>');
						redirect(base_url('/'));

					}else{
						$input_data   = array('login_failed_attemp' => $login_failed_attemp_new);
						$condition1   = array('merchID' => $checkEmail['merchID']);

						$this->general_model->update_row_data('tbl_merchant_data',$condition1, $input_data);
					}

					
				}

				try {
					$throttle->addAttempt(false);
				} catch (\Exception $ex) {
					// For the moment... silence the exception
					// In the future - throw exception to Sentry
				}

				$this->session->set_flashdata('user_login_data', $this->input->post(null, true) );
				
				redirect(base_url('/user/login/user_login_main'));
			}
		}else{

			try {

				$throttle->addAttempt(false);
			} catch (\Exception $ex) {
				// For the moment... silence the exception
				// In the future - throw exception to Sentry
			}

			$this->session->set_flashdata('user_login_data', $this->input->post(null, true) );
				redirect(base_url('/user/login/user_login_main'));
		}
	}




	function enable_merchant()
	{

		$code = $this->uri->segment('3');


		if ($code != "") {

			$res = $this->general_model->get_row_data('tbl_merchant_data', array('loginCode' => $code, 'isEnable' => '0'));
			if (!empty($res)) {

				$ins_data = array('loginCode' => '', 'isEnable' => '1');

				$this->general_model->update_row_data('tbl_merchant_data', array('loginCode' => $code, 'isEnable' => '0'), $ins_data);
				$this->session->set_flashdata('success', 'Thanks for verifying your email id. You may login now');
				redirect(base_url('login#login'), 'refresh');
			} else {
				$this->session->set_flashdata('success', 'Already enabled');
				redirect(base_url('login#login'), 'refresh');
			}
		}
	}


	function user_register()
	{
		return false;
		if(!$this->czsecurity->csrf_verify())
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: </strong>'.$this->czsecurity->errorMessages('token_expired').'</div>');
			redirect(base_url('/'));
		}
		$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
		$email   = $this->czsecurity->xssCleanPostInput('register-email');
		$FirstName = $this->czsecurity->xssCleanPostInput('register-firstname');
		$LastName = $this->czsecurity->xssCleanPostInput('register-lastname');

		$datas = array(
			'firstName' => $FirstName,
			'lastName' => $LastName,
			'merchantEmail' => $this->czsecurity->xssCleanPostInput('register-email'),
			'merchantPasswordNew' => password_hash($this->czsecurity->xssCleanPostInput('register-password'), PASSWORD_BCRYPT),
			'resellerID' => $this->czsecurity->xssCleanPostInput('reseller'),
			'plan_id' => $this->czsecurity->xssCleanPostInput('Plans'),
			'loginCode' => $code,
			'isEnable'  => '0',
			'date_added' => date('Y-m-d H:i:s')
		);

		$results = $this->general_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));
		if ($results) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  Email Already Exitsts.</div>');
		} else {

			$insert = $this->general_model->insert_row('tbl_merchant_data', $datas);
			$this->session->set_flashdata('success', 'Thanks, for registering with us. Please check your email and verify your account');
			$this->load->library('email');

			$subject = 'ChargeZoom, Verification mail';
			$this->email->from('support@chargezoom.com');
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->set_mailtype("html");

			
			$message = "<p>Dear $FirstName $LastName,<br><br>Thanks for registering with ChargeZoom. Please verify your email id by clicking following verification link.</p><br> <p><a href=" . base_url('login/enable_merchant/' . $code) . ">Click Here</a> </p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";
			$this->email->message($message);

			if ($this->email->send()) {
				$this->session->set_flashdata('success', 'Thanks, for registering with us. Please check your email and verify your account');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> Email was not sent, please contact your administrator.</div>' . $email);
			}
		}

		redirect(base_url('login#login'), 'refresh');
	}

	public function get_reseller_plan_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('reID');
		$plan1 = array();

		$data = $this->general_model->get_row_data('tbl_reseller', array('resellerID' => $id));

		$plans = explode(',', $data['plan_id']);
		foreach ($plans as $key => $plan) {
			$pdata = $this->general_model->get_row_data('plans', array('plan_id' => $plan));
			$plan1[$key]['plan_id']   = $pdata['plan_id'];
			$plan1[$key]['plan_name'] = $pdata['plan_name'];
		}

		echo json_encode($plan1);
	}





	public function recover_password()
	{
		if(!$this->czsecurity->csrf_verify())
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: </strong>'.$this->czsecurity->errorMessages('token_expired').'</div>');
			redirect('login#reminder', 'refresh');
		}

		$email  = $this->czsecurity->xssCleanPostInput('reminder-email');

		$result = $this->general_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));

		if ($result) {

			$request 	= new LoginRequest($email);
			$throttle 	= new Throttle($this->db, $request, 0, 3600);
			try {
				$attemptCount = $throttle->getForgetAttemptCount(INTERFACE_TYPE);
				if($attemptCount >= 3){
					$this->session->set_flashdata('error', 'Password Reset request limit reached. Request has been locked for an hour.');
					redirect('/', 'refresh');
				}
				
			} catch (\Exception $ex) {
				// For the moment... silence the exception
				// In the future - pass exception to Sentry
			}

			$this->load->helper('string');
			$code = random_string('alnum', 10);

			$update = $this->login_model->temp_reset_password($code, $email);
			if ($update) {
				try {
					$throttle->addForgetAttempt(INTERFACE_TYPE);
				} catch (\Exception $ex) {
					// For the moment... silence the exception
					// In the future - pass exception to Sentry
				}

				#Create Log on Password Update
				$log_session_data = [
					'session' => $this->session->userdata,
					'http_data' => $_SERVER
				];
				
				$logData = [
					'request_data' => json_encode($this->input->post(null, true), true),
					'session_data' => json_encode( $log_session_data, true),
					'executed_sql' => $this->db->last_query(),
					'log_date'	   => date('Y-m-d H:i:s'),
					'action_interface' => 'Merchant - Forgot Password',
					'header_data' => json_encode(apache_request_headers(), true),
				];
				$this->general_model->insert_row('merchant_password_log', $logData);

				#Log scripts end here

				$merchant_detail = $this->general_model->get_merchant_data(array('m.merchantEmail' => $email));
				$template_data = $this->general_model->get_row_data('tbl_eml_temp_reseller', ['resellerID' => $merchant_detail['resellerID'], 'templateType' => 2]);
				$from = 'donotreply@payportal.com';
				$fromName = '';
				$replyTo = '';
				if($template_data){
					$subject = stripslashes(str_replace('{{reseller_company}}', $merchant_detail['resellerCompanyName'], $template_data['emailSubject']));
					$message = $template_data['message'];
					if($template_data['fromEmail']){
						$from = $template_data['fromEmail'];
					}
					$fromName = $template_data['fromName'];
					$replyTo = $template_data['replyTo'];
					$merchant_name = ($merchant_detail['lastName']) ? $merchant_detail['firstName'] .' '.$merchant_detail['lastName'] : $merchant_detail['firstName'];
					$base_url = '<a clicktracking=off href="'.base_url().'">'.base_url().'</a>';
					$message = stripslashes(str_replace('{{merchant_name}}', $merchant_name, $message));
					$message = stripslashes(str_replace('{{login_url}}', $base_url, $message));
					$message = stripslashes(str_replace('{{merchant_email}}', $email, $message));				
					$message = stripslashes(str_replace('{{merchant_password}}', $code, $message));
					$message = stripslashes(str_replace('{{reseller_company}}', $merchant_detail['resellerCompanyName'], $message));

				}else{

					$subject = 'ChargeZoom - Reset Password';
					$message = "<p>Hello,<br><br>Your new password has been updated successfully. Please use the new password: " . $code . "</p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";
				}

				$request_data = [
					'to' => $email,
					'from' => $from,
					'fromname' => $fromName,
					'subject' => $subject,
					'replyto' => ($replyTo) ? $replyTo : $from,
					'html' => $message
				];
				$sendEmail = sendEmailUsingSendGrid($request_data);
				if ($sendEmail['success']) {
					$this->session->set_flashdata('message', '<div class="alert alert-success">A Password Reset request has been sent to your email on file.</div>');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  Email was not sent, please contact your administrator.</div>' . $code);
				}
			}

			redirect('/', 'refresh');
		} else {
			$this->session->set_flashdata('error', 'Error: Email not available.');
			
			redirect('/', 'refresh');
		}
	}

	public function changePass()
	{
		$session_data = $this->session->userdata('logged_in');
		$id = $session_data['merchID'];

		$password = $this->czsecurity->xssCleanPostInput('user-settings-password');

		$strength_check = $this->czsecurity->checkPasswordStrength($password);

		if($strength_check['status'] == 'OK') {
			$query = $this->login_model->savenewpass($id, $password);
			$message = 'Password Successfully Updated..!';

			$this->session->set_flashdata('success', $message);
			redirect('home/index', 'refresh');
		} else {
			$this->session->set_flashdata('error', $strength_check['message']);
			redirect('home/index', 'refresh');
		}
		
	}
}
