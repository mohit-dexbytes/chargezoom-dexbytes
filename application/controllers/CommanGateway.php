<?php 
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;
use GlobalPayments\Api\PaymentMethods\ECheck;

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

include_once APPPATH . 'libraries/integration/XeroSync.php';
include APPPATH . 'third_party/nmiDirectPost.class.php';
include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
include APPPATH . 'third_party/PayTraceAPINEW.php';
include APPPATH . 'third_party/PayPalAPINEW.php';
include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
require_once APPPATH."third_party/usaepay/usaepay.php";		
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
include APPPATH . 'third_party/Fluidpay.class.php';
include APPPATH . 'third_party/TSYS.class.php';
include APPPATH . 'third_party/EPX.class.php';

include APPPATH . 'third_party/Cardpointe.class.php';
use iTransact\iTransactSDK\iTTransaction;

include_once APPPATH . 'libraries/Freshbooks_data.php';
class CommanGateway extends CI_Controller 
{
	private $gatewayEnvironment;
	
	function __construct()
	{
		
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('card_model');
		if ($this->session->userdata('logged_in')) {
			$da['login_info']	= $this->session->userdata('logged_in');
			$this->merchantID			= $da['login_info']['merchID'];
            $this->resellerID        = $da['login_info']['resellerID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da['login_info']		= $this->session->userdata('user_logged_in');
			$this->merchantID      = $da['login_info']['merchantID'];
			$rs_Data                         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $this->merchantID));
            $this->resellerID                = $rs_Data['resellerID'];
		}
  		$this->load->config('auth_pay');
		$this->gatewayEnvironment = $this->config->item('environment');
	}
	
    public function getEmailTemplate(){

		
		$templateID = $this->czsecurity->xssCleanPostInput('templateID');
		
		if($templateID > 0){
			$con  = array('templateID' => $templateID);
			
			$getData = 	$this->general_model->get_row_data('tbl_email_template', $con);

			if($getData){
				echo json_encode(array('status' => 'success','data' => $getData)); die;
			}
		}
		echo json_encode(array('status' => 'fail','data' => [])); die;
	}
	public function send_mail(){
		$this->load->library('email');
		if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
		$merchant_data = $this->general_model->get_merchant_data(array('merchID' => $user_id));
        $logo_url      = '';
        $config_data   = $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage','customerPortalURL'), array('merchantID' => $user_id));
        
        if (!empty($config_data['ProfileImage'])) {
            $logo_url = base_url().LOGOURL.$config_data['ProfileImage'];
        }

        if($logo_url == ''){
        	$logo_url = CZLOGO;
        }
        $fromEmail = $this->czsecurity->xssCleanPostInput('fromEmail');
        if(empty($fromEmail)){
        	$fromEmail = $merchant_data['merchantEmail'];
        }

        $update_link         = $config_data['customerPortalURL'];

        $config_email  = $merchant_data['merchantEmail'];
        $mphone              = $merchant_data['merchantContact'];
        $cur_date            = date('Y-m-d');

        $addCC         = $this->czsecurity->xssCleanPostInput('ccEmail');
        $addBCC        = $this->czsecurity->xssCleanPostInput('bccEmail');
        $replyTo       = $this->czsecurity->xssCleanPostInput('replyEmail');
        
        $invoicetempID = $this->czsecurity->xssCleanPostInput('templateID');

        $fromEmail       = $this->czsecurity->xssCleanPostInput('fromEmail');
        $mailDisplayName = $this->czsecurity->xssCleanPostInput('mailDisplayName');
        $fromEmail       = ($fromEmail == '') ? 'donotreply@payportal.com' : $fromEmail;
        $mailDisplayName = ($mailDisplayName == '') ? $merchant_data['companyName'] : $mailDisplayName;
        $replyTo       = ($replyTo == '') ? $merchant_data['merchantEmail'] : $replyTo;
        $date       = date('Y-m-d H:i:s');
        $in_link = '';
        $currency      = "$";
        

        $merchant_name = $merchant_data['companyName'];

        $invoiceArr = $this->czsecurity->xssCleanPostInput('emailProcessInvoiceIDS');
        $toInvoice = (explode(",",$invoiceArr));
        $integrationType = $this->czsecurity->xssCleanPostInput('integrationType');
        if(count($toInvoice) > 0){
        	foreach ($toInvoice as $value) {
		        
        		$invoiceID = $invoicetempID = $value;
        		$emailData = [];
        		$subject = $this->czsecurity->xssCleanPostInput('emailSubject');
        		$message = $this->czsecurity->xssCleanPostInput('textarea-ckeditor',false);
    			
	            $link_data = $this->general_model->get_row_data('tbl_template_data', array('merchantID' => $user_id, 'invoiceID' => $invoiceID));
	            $ttt       = explode(PLINK, $update_link);

	            $purl = $ttt[0] . PLINK . '/customer/';
				$str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
				$shuffled = str_shuffle($str);
				$shuffled = substr($shuffled, 1, 12) . '=' . $user_id;
				$code     =    $this->safe_encode($shuffled);
				$invcode    =  $this->safe_encode($invoiceID);

				$in_link = '<a  target="_blank" href="' . $purl . 'update_payment/' . $code . '/' . $invcode . '" class="btn btn-primary">Click Here</a>';

	            if($integrationType == 5){
	            	$this->load->model('company/company_model','company_model');
	            	$emailCondition = [
		                'TxnID' => $invoicetempID
		            ];
		            $emailData = $this->general_model->get_row_data('chargezoom_test_invoice', $emailCondition);
		            $customerID = $emailData['Customer_ListID'];

		        	$customerData = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customerID));
		        	$toEmail       = $customerData['Contact'];

		        	$subject  = stripslashes(str_ireplace('{{invoice.refnumber}}',$emailData['RefNumber'], $subject));
		        	$message = stripslashes(str_ireplace('{{customer.company}}', $customerData['companyName'], $message));
		        	$message = stripslashes(str_ireplace('{{customer.name}}', $customerData['FullName'], $message));

		        	$message = stripslashes(str_ireplace('{{invoice.refnumber}}', $emailData['RefNumber'], $message));

		        }elseif($integrationType == 2){
		        	$this->load->model('company_model','company_model');

		        	$emailCondition = [
						'TxnID' => $invoicetempID,
						'qb_inv_merchantID' => $user_id
					];
		        	$emailData = $this->general_model->get_row_data('qb_test_invoice', $emailCondition);
		        	$customerID = $emailData['Customer_ListID'];

		        	$customerData = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID,'qbmerchantID'  => $user_id));
		        	$toEmail      = $customerData['Contact'];


		        	$subject = stripslashes(str_ireplace('{{invoice.refnumber}}',$emailData['RefNumber'], $subject));
		        	$message = stripslashes(str_ireplace('{{customer.company}}', $customerData['companyName'], $message));
		        	$message = stripslashes(str_ireplace('{{customer.name}}', $customerData['FullName'], $message));
		        	$message = stripslashes(str_ireplace('{{invoice.refnumber}}',$emailData['RefNumber'], $message));

		        }elseif($integrationType == 3){
		        	$this->load->model('Freshbook_models/fb_company_model', 'company_model');
		        	$emailCondition = [
						'invoiceID' => $invoicetempID,
						'merchantID' => $user_id,
					];
		        	$emailData = $this->general_model->get_row_data('Freshbooks_test_invoice', $emailCondition);
		        	$customerID = $emailData['CustomerListID'];

		        	$customerData = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $customerID,'merchantID'  => $user_id));
		        	$toEmail      = $customerData['userEmail'];

		        	$subject = stripslashes(str_ireplace('{{invoice.refnumber}}',$emailData['refNumber'], $subject));
		        	$message = stripslashes(str_ireplace('{{customer.company}}', $customerData['companyName'], $message));
		        	$message = stripslashes(str_ireplace('{{customer.name}}', $customerData['fullName'], $message));
		        	$message = stripslashes(str_ireplace('{{invoice.refnumber}}',$emailData['refNumber'], $message));

		        }elseif($integrationType == 1){
		        	$this->load->model('QBO_models/qbo_company_model', 'company_model');

		        	$emailCondition = [
						'invoiceID' => $invoicetempID,
						'merchantID' => $user_id
					];
		        	$emailData  = $this->general_model->get_row_data('QBO_test_invoice', $emailCondition);
		        	$customerID = $emailData['CustomerListID'];

		        	$customerData = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID,'merchantID'  => $user_id));
		        	$toEmail      = $customerData['userEmail'];

		        	$subject = stripslashes(str_ireplace('{{invoice.refnumber}}',$emailData['refNumber'], $subject));
		        	$message = stripslashes(str_ireplace('{{customer.company}}', $customerData['companyName'], $message));

		        	$message = stripslashes(str_ireplace('{{customer.name}}', $customerData['fullName'], $message));

		        	$message = stripslashes(str_ireplace('{{invoice.refnumber}}',$emailData['refNumber'], $message));
		        	
		        }elseif($integrationType == 4){
		        	$this->load->model('Xero_models/xero_company_model', 'company_model');
		        	$emailCondition = [
						'invoiceID' => $invoicetempID,
						'merchantID' => $user_id
					];
		        	$emailData = $this->general_model->get_row_data('Xero_test_invoice', $emailCondition);
		        	$customerID = $emailData['CustomerListID'];

		        	$customerData = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $customerID,'merchantID'  => $user_id));
		        	$toEmail       = $customerData['userEmail'];

		        	$subject  = stripslashes(str_replace('{{invoice.refnumber}}',$emailData['refNumber'], $subject));
		        	$message = stripslashes(str_replace('{{customer.company}}', $customerData['companyName'], $message));

		        	$message = stripslashes(str_replace('{{customer.name}}', $customerData['fullName'], $message));

		        	$message = stripslashes(str_replace('{{invoice.refnumber}}',$emailData['refNumber'], $message));
		        	
		        }
		        

		            
		            if(isset($emailData)){
		            	$duedate      = date('F d, Y', strtotime($emailData['DueDate']));
		            	$balance      = $emailData['BalanceRemaining'];
		            	$overday 	  = '';
		            	$tr_date      = date('F d, Y', strtotime($emailData['TimeCreated']));

		            	
		            	$subject = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $subject));

		            	$message = stripslashes(str_ireplace('{{merchant_name}}', $merchant_name, $message));

		            	$message = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $message));
				        $message = stripslashes(str_ireplace('{{invoice.balance}}', $balance, $message));
				        $message = stripslashes(str_ireplace('{{invoice.due_date|date("F j, Y")}}', $duedate, $message));
				        $message = stripslashes(str_ireplace('{{invoice.days_overdue}}', $overday, $message));
				        $message = stripslashes(str_ireplace('{{invoice.url_permalink}}', $in_link, $message));
				        $message = stripslashes(str_ireplace('{{invoice_payment_pagelink}}', $in_link, $message));

				        $message = stripslashes(str_ireplace('{{merchant_email}}', $config_email, $message));
				        $message = stripslashes(str_ireplace('{{merchant_phone}}', $mphone, $message));
				        $message = stripslashes(str_ireplace('{{current.date}}', $cur_date, $message));
				        $message = stripslashes(str_ireplace('{{logo}}', "<img width='150px' src='" . $logo_url . "'>", $message));

				        $message = stripslashes(str_ireplace('{{transaction.currency_symbol}}', $currency, $message));
				        $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}', $currency, $message));

				        $message = stripslashes(str_ireplace('{{transaction.amount}}', ($balance) ? ('$' . number_format($balance, 2)) : '0.00', $message));
				        $message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message));
				        $message = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $message));

		            	
		            	$email_data = array(
				            'customerID'   => $customerID,
				            'merchantID'   => $user_id,
				            'emailSubject' => $subject,
				            'emailfrom'    => $fromEmail,
				            'emailto'      => $toEmail,
				            'emailcc'      => $addCC,
				            'emailbcc'     => $addBCC,
				            'emailreplyto' => $replyTo,
				            'emailMessage' => $message,
				            'emailsendAt'  => $date,
				        );

				        if ($invoiceID != '') {
				            $email_data['invoiceID'] = $invoicetempID;
				        }
				        
				        $toArr = (explode(";",$toEmail));
				        $toArrEmail = [];

				        foreach ($toArr as $value) {
				            # code...
				            $toArrEmail[]['email'] = trim($value);
				        }

				        $toaddCCArrEmail = [];
				        if($addCC != null){
				            $toaddCCArr = (explode(";",$addCC));
				            
				            foreach ($toaddCCArr as $value) {
				                # code...
				                $toaddCCArrEmail[]['email'] = trim($value);
				            }
				        }
				        
				        $toaddBCCArrEmail = [];
				        if($addBCC != null){
				            $toaddBCCArr = (explode(";",$addBCC));
				            
				            foreach ($toaddBCCArr as $value) {
				                # code...
				                $toaddBCCArrEmail[]['email'] = trim($value);
				            }
				        }

				        $file_name = '';
				        if ($this->czsecurity->xssCleanPostInput('add_attachment') == 'on') {
				        	if ($invoicetempID != "") {
				        		$invoice = $this->company_model->get_invoice_details_data($invoicetempID);
				        		$inv_no = $invoice['invoice_data']['invoiceNumber'];
				                
				                if (!empty($invoice)) {
				                    $this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'F');
				                }

				            }

				            if ($inv_no != '') {
				                $file_name = $invoicetempID . '-' . "$inv_no.pdf";
				            } else {
				                $file_name = "$invoicetempID.pdf";
				            }
				        }

				        $request_array = [
				            "personalizations" => [
				                [
				                    "to" => $toArrEmail,
				                    "subject" => $subject,
				                    "cc"=> $toaddCCArrEmail,
				                    "bcc"=>$toaddBCCArrEmail

				                ],
				                
				            ],
				            "from" => [
				                "email" => $fromEmail,
				                "name" => $mailDisplayName
				            ],

				            "reply_to" => [
				                "email" => $replyTo
				            ],
				            "content" => [
				                [
				                "type" => "text/html",
				                "value" => $message
				                ]
				                ],
				            
				                    
				        ];
				        if(count($toaddCCArrEmail) == 0){
				            unset($request_array['personalizations'][0]['cc']);
				        }
				        if(count($toaddBCCArrEmail) == 0){
				            unset($request_array['personalizations'][0]['bcc']);
				        }

				        if($file_name!="")	
				        {
				        	$attachments = [];
				            $attachments[] = [
				                'filename' => $file_name,
				                'type'=> 'text/plain',
				                'content' => base64_encode(file_get_contents(FCPATH . '/uploads/invoice_pdf/' . $file_name))
				            ];
				            $request_array['attachments'] = $attachments;
				        }

				        // get api key for send grid
				        $api_key = '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';
				        $url = 'https://api.sendgrid.com/v3/mail/send';

				        // set authorization header
				        $headerArray = [
				            'Authorization: Bearer '.$api_key,
				            'Content-Type: application/json',
				            'Accept: application/json'
				        ];

				        $ch = curl_init($url);
				        curl_setopt ($ch, CURLOPT_POST, true);
				        curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				        curl_setopt($ch, CURLOPT_HEADER, true);

				        // add authorization header
				        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

				        $response = curl_exec($ch);
				        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
				        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				        curl_close($ch);

				        // parse header data from response
				        $header_text = substr($response, 0, $header_size);
				        // parse response body from curl response
				        $body = substr($response, $header_size);

				        $headers = createSendGridHeaders($header_text);
				        
				        
				        // if mail sent success
				        if($httpcode == '202' || $httpcode == '200'){
				           	$email_data['send_grid_email_id'] = isset($headers['X-Message-Id']) ? $headers['X-Message-Id'] : '';
							$email_data['send_grid_email_status'] = 'Sent';
				            $this->general_model->insert_row('tbl_template_data', $email_data);

				            
				        } else {
				        	$email_data['send_grid_email_status'] = 'Failed';
				            $this->general_model->insert_row('tbl_template_data', $email_data);
				            
				        }
				        $this->session->set_flashdata('success', 'All emails have been sent.');
		            }
        	}
        	if($integrationType == 1){
        		redirect('QBO_controllers/Create_invoice/Invoice_details/', 'refresh');
        	}else if($integrationType == 2){
        		redirect('home/invoices/', 'refresh');
        	}else if($integrationType == 3){
        		redirect('FreshBooks_controllers/Freshbooks_invoice/Invoice_details/', 'refresh');
        	}else if($integrationType == 4){
        		redirect('Integration/Invoices/invoice_list/', 'refresh');
        	}else if($integrationType == 5){
        		redirect('company/home/invoices/', 'refresh');
        	}
        }


	}

	/**
	 * processMultipleInvoices
	 *
	 * @return void
	 */
	public function processMultipleInvoices(){
		
		if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        $this->session->unset_userdata("batch_process_receipt_data");

        $receipt_data = [];

        $custom_data_fields = [];

		$merchant_data = $this->general_model->get_merchant_data(array('merchID' => $user_id));
     
        $integrationType = $this->czsecurity->xssCleanPostInput('integrationMerchantType');
 
        $date       = date('Y-m-d H:i:s');
     
        $invoiceArr = $this->czsecurity->xssCleanPostInput('processInvoiceIDS');
        $toInvoice = (explode(",",$invoiceArr));
        $totalProcessInvoice = count($toInvoice);
        $tRowID = 0;
       

		$credit_card_gateway = $this->czsecurity->xssCleanPostInput('credit_card_gateway');
		$electronic_check_gateway = $this->czsecurity->xssCleanPostInput('electronic_check_gateway');

		$gt_resultCC		= $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $credit_card_gateway));


		$gt_resultEC = [];

		// Check eCheck option
		if($electronic_check_gateway) 
		{
			$gt_resultEC		= $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $electronic_check_gateway));
		}
		


		$gatewayID = $gt_resultCC['gatewayID'];
		$gatewayType = $gt_resultCC['gatewayType'];
		$resellerID = $merchant_data['resellerID'];

		$gatewayMaster = $this->general_model->get_row_data('tbl_master_gateway', array('gateID' => $gatewayType));

		$gatewayName = $gatewayMaster['gatewayName'];

		if($gt_resultCC['gatewayType'] == 7 || (!empty($gt_resultEC) && $gt_resultEC['gatewayType']  == 7)){
			
			require_once FCPATH . '/vendor/autoload.php';
		}
		$transactionid = $qbListTxnID = '';
		$totalProcessdInvoice = 0;
		$responseCode = $accountCode = 0;

		if($integrationType == 3){
			$freshbookSync = new Freshbooks_data($this->merchantID, $this->resellerID);
		}
		$responsetext = 'Missing Billing Information';
		$response_code = 401;
		$transactionid = 'TXNFAILED'.time();

		if(isset($gt_resultCC) || isset($gt_resultEC)){

			if($totalProcessInvoice > 0){
	        	foreach ($toInvoice as $value) {


	        		$invoiceID = $invoicetempID = $value;
	        		// echo "invoiceID-$invoiceID";die;
		            
		            if($integrationType == 5){
		            	$emailCondition = [
			                'TxnID' => $invoicetempID
			            ];
			            $in_data = $this->general_model->get_row_data('chargezoom_test_invoice', $emailCondition);

			            $customerID = $in_data['Customer_ListID'];

			        	$comp_data = $this->general_model->get_row_data('chargezoom_test_customer', array('ListID' => $customerID));

			        	$invoiceTable = 'chargezoom_test_invoice';

			        	$companyName = $comp_data['companyName'];
			        	$first_name = $comp_data['FirstName'];
			        	$last_name = $comp_data['LastName'];
						$email =  $comp_data['Contact'];
						$name = $in_data['Customer_FullName'];
			        	$ispaid = 'true';

			        	$refnumber = $in_data['RefNumber'];

			        }elseif($integrationType == 2){
			        	$emailCondition = [
			                'TxnID' => $invoicetempID,
							'qb_inv_merchantID' => $user_id
			            ];
			        	$in_data = $this->general_model->get_row_data('qb_test_invoice', $emailCondition);
			        	$customerID = $in_data['Customer_ListID'];

			        	$comp_data = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID,'qbmerchantID'  => $user_id));

			        	$invoiceTable = 'qb_test_invoice';

			        	$companyName = $comp_data['companyName'];
			        	$first_name = $comp_data['FirstName'];
			        	$last_name = $comp_data['LastName'];
						$email =  $comp_data['Contact'];
						$name = $in_data['Customer_FullName'];
						$ispaid = 'true';
						$refnumber = $in_data['RefNumber'];
			        	
			        }elseif($integrationType == 3){
			        	$emailCondition = [
			                'invoiceID' => $invoicetempID,
							'merchantID' => $user_id,
			            ];
			        	$in_data = $this->general_model->get_row_data('Freshbooks_test_invoice', $emailCondition);
			        	$customerID = $in_data['CustomerListID'];

			        	$comp_data = $this->general_model->get_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $customerID,'merchantID'  => $user_id));

			        	$invoiceTable = 'Freshbooks_test_invoice';

			        	$companyName = $comp_data['companyName'];
			        	$first_name = $comp_data['firstName'];
			        	$last_name = $comp_data['lastName'];
						$email =  $comp_data['userEmail'];
						$name = $in_data['CustomerFullName'];
						$ispaid = 1;
						$refnumber = $in_data['refNumber'];
			        	
			        }elseif($integrationType == 1){
			        	$emailCondition = [
			                'invoiceID' => $invoicetempID,
							'merchantID' => $user_id
			            ];
			        	$in_data = $this->general_model->get_row_data('QBO_test_invoice', $emailCondition);
			        	$customerID = $in_data['CustomerListID'];

			        	$comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID,'merchantID'  => $user_id));

			        	$invoiceTable = 'QBO_test_invoice';

			        	$companyName = $comp_data['companyName'];
			        	$first_name = $comp_data['firstName'];
			        	$last_name = $comp_data['lastName'];
						$email =  $comp_data['userEmail'];
						$name = $in_data['CustomerFullName'];
						$ispaid = 1;
						$refnumber = $in_data['refNumber'];
			        } elseif($integrationType == 4){
			        	$emailCondition = [
			                'invoiceID' => $invoicetempID,
							'merchantID' => $user_id
			            ];
			        	$in_data = $this->general_model->get_row_data('Xero_test_invoice', $emailCondition);
			        	$customerID = $in_data['CustomerListID'];

			        	// $invoiceID = $invoicetempID = $in_data['invoiceID'];

			        	$comp_data = $this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $customerID,'merchantID'  => $user_id));

			        	$invoiceTable = 'Xero_test_invoice';

			        	$companyName = $comp_data['companyName'];
			        	$first_name = $comp_data['firstName'];
			        	$last_name = $comp_data['lastName'];
						$email =  $comp_data['userEmail'];
						$name = $in_data['CustomerFullName'];
						$ispaid = 'true';
						$refnumber = $in_data['refNumber'];
						$accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);
						if (empty($accountData)) {
							$errorMessage = "Please select a default payment account";
							$this->session->set_flashdata('message', "<div class='alert alert-danger'><strong>$errorMessage</strong>.</div>");
							redirect('Integration/Invoices/invoice_list', 'refresh');
						}
						$accountCode = $accountData['accountCode'];
			        } 
			        $amount    = $in_data['BalanceRemaining'];

			        $card_data_array     = $this->card_model->getRecentCustomerCardData($customerID,3);
			        if(!isset($card_data_array['CardID'])){
			        	
			        	$card_data_array     = $this->card_model->getRecentCustomerCardData($customerID,1);
						
			        }

			        $cardType = 'Unknown';
			        $accountNumber = '';
			        $card_no = '';
			        if(isset($card_data_array['CardNo'])){

            		
	            		$card_no = $card_data_array['CardNo'];
	            		$exyear    = $card_data_array['cardYear'];
	            		$expmonth = $card_data_array['cardMonth'];
	            		$cardType = $this->general_model->getType($card_no);
	            		$sch_method = 1;
	            		$cvv = $card_data_array['CardCVV'];
	            		if (strlen($expmonth) == 1) {
	                        $expmonth = '0' . $expmonth;
	                    }
            		}else{
            			$card_data_array = [];
	            		$sch_method = 0;
            		}

            		if($sch_method == 0){
            			$card_data_array     = $this->card_model->getRecentCustomerCardData($customerID,2);

            			if(isset($card_data_array['accountNumber'])){
            				
	            			$accountName = $card_data_array['accountName'];
		            		$accountNumber = $card_data_array['accountNumber'];
		            		$routeNumber = $card_data_array['routeNumber'];
		            		$accountHolderType = $card_data_array['accountHolderType'];
		            		$accountType = $card_data_array['accountType'];
		            		$sec_code = 'WEB';
		            		$sch_method = 2;
	            			
		            	}
            		}
            		
			        if(isset($card_data_array['cardMonth']) || isset($card_data_array['accountNumber'])){
			        	$address1 = $card_data_array['Billing_Addr1'];
						$address2 = $card_data_array['Billing_Addr2'];
						$city     = $card_data_array['Billing_City'];
						$zipcode  = $card_data_array['Billing_Zipcode'];
						$state    = $card_data_array['Billing_State'];
						$country  = $card_data_array['Billing_Country'];
						$phone 	  = $card_data_array['Billing_Contact'];
			        }
			        if($sch_method == 1){
			        	
                		$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                		$custom_data_fields['payment_type'] = $friendlyname;
			        }elseif($sch_method == 2){
                		$friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                		$custom_data_fields['payment_type'] = $friendlyname;
			        }else{

			        }
			        
			        if(count($card_data_array) > 0)
			        {
			            if(isset($in_data)){


			            	if($amount > 0 && $sch_method != 0)
			            	{


				            	if( ($gt_resultCC['gatewayType'] == 1 && $sch_method == 1) || ($gt_resultCC['gatewayType'] == 9 && $sch_method == 1)){
				            		/*NMI and Chargezoom Credit card*/
	            					$nmiuser  = $gt_resultCC['gatewayUsername'];
	            					$nmipass  = $gt_resultCC['gatewayPassword'];
	            					$gatewayID = $gt_resultCC['gatewayID'];
			            			$gatewayType = $gt_resultCC['gatewayType'];

	            					$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

	            					$transaction1 = new nmiDirectPost($nmi_data);

	            					$gatewayName = "NMI";
	            					$transaction1->setCcNumber($card_no);
	            					$exyear1 = substr($exyear, 2);
		                            if (strlen($expmonth) == 1) {
		                                $expmonth = '0' . $expmonth;
		                            }
		                            $expry = $expmonth . $exyear1;
		                            $transaction1->setCcExp($expry);
									$transaction1->setAmount($amount);

									$level_request_data = [
										'transaction' => $transaction1,
										'card_no' => $card_no,
										'merchID' => $user_id,
										'amount' => $amount,
										'invoice_id' => $invoiceID,
										'gateway' => 1
									];
									$transaction1 = addlevelThreeDataInTransaction($level_request_data);
		            				
		            				#Billing Details
									$transaction1->setCompany($companyName);
									$transaction1->setFirstName($first_name);
									$transaction1->setLastName($last_name);
									#Shipping Details
									$transaction1->setShippingCompany($companyName);
									$transaction1->setShippingFirstName($first_name);
									$transaction1->setShippingLastName($last_name);

									if($address1 != ''){
										$transaction1->setAddress1($address1);
										$transaction1->setShippingAddress1($address1);
									}
									if($address2 != ''){
										$transaction1->setAddress2($address2);
										$transaction1->setShippingAddress2($address2);
									}
									if($country != ''){
										$transaction1->setCountry($country);
										$transaction1->setShippingCountry($country);
									}
									if($city != ''){
										$transaction1->setCity($city);
										$transaction1->setShippingCity($city);
									}
									if($state != ''){
										$transaction1->setState($state);
										$transaction1->setShippingState($state);
									}
									if($zipcode != ''){
										$transaction1->setZip($zipcode);
										$transaction1->setShippingZip($zipcode);
									}
									if($phone != ''){
										$transaction1->setPhone($phone);
									}
									$transaction1->setAmount($amount);
									$transaction1->setTax('tax');
									$transaction1->sale();
									$result = $transaction1->execute();

									$transactionid = $result['transactionid'];

									if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}
									
				            		if ($result['response_code'] == '100') {
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;

										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;
										if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
		        					 	} else if($integrationType == 4){
										   	$tr_date           = date('Y-m-d H:i:s');
											$updateInvoiceData = [
												'invoiceID'       => $invoiceID,
												'accountCode'     => $accountCode,
												'trnasactionDate' => $tr_date,
												'amount'          => $amount,
											];
				   
										   	$xeroSync = new XeroSync($this->merchantID);
										   	$qbListTxnID  = $xeroSync->createPayment($updateInvoiceData);
										} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoicePayment['status'] == 'ok') {
												$qbListTxnID = $invoicePayment['payment_id'];
											}
										}
										$responsetext = 'SUCCESS';
				            		}else{
				            			$responsetext = $result['responsetext'];
				            			
				            		}
				            		
				            		$response_code = $result['response_code'];
				            		$type = $result['type'];
				            		$resellerID = $merchant_data['resellerID'];
				            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

				            	}else if( !empty($gt_resultEC) && (($gt_resultEC['gatewayType'] == 1 && $sch_method == 2) || ($gt_resultEC['gatewayType'] == 9 && $sch_method == 2)) ){
				            		/*NMI and Chargezoom eCheck Payment*/

	            					$nmiuser  = $gt_resultEC['gatewayUsername'];
	            					$nmipass  = $gt_resultEC['gatewayPassword'];
	            					$gatewayID = $gt_resultEC['gatewayID'];
			            			$gatewayType = $gt_resultEC['gatewayType'];

	            					$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

	            					$transaction1 = new nmiDirectPost($nmi_data);

	            					$gatewayName = "NMI ECheck";
	            					$transaction1->setAccountName($accountName);
			                        $transaction1->setAccount($accountNumber);
			                        $transaction1->setRouting($routeNumber);

			                        $transaction1->setAccountType($accountType);
									$transaction1->setAccountHolderType($accountHolderType);

									$transaction1->setSecCode($sec_code);
									$transaction1->setPayment('check');
		            				
		            				#Billing Details
									$transaction1->setCompany($companyName);
									$transaction1->setFirstName($first_name);
									$transaction1->setLastName($last_name);

									#Shipping Details
									$transaction1->setShippingCompany($companyName);
									$transaction1->setShippingFirstName($first_name);
									$transaction1->setShippingLastName($last_name);

									if($address1 != ''){
										$transaction1->setAddress1($address1);
										$transaction1->setShippingAddress1($address1);
									}
									if($address2 != ''){
										$transaction1->setAddress2($address2);
										$transaction1->setShippingAddress2($address2);
									}
									if($country != ''){
										$transaction1->setCountry($country);
										$transaction1->setShippingCountry($country);
									}
									if($city != ''){
										$transaction1->setCity($city);
										$transaction1->setShippingCity($city);
									}
									if($state != ''){
										$transaction1->setState($state);
										$transaction1->setShippingState($state);
									}
									if($zipcode != ''){
										$transaction1->setZip($zipcode);
										$transaction1->setShippingZip($zipcode);
									}
									if($phone != ''){
										$transaction1->setPhone($phone);
									}
									$transaction1->setAmount($amount);
									$transaction1->setTax('tax');
									$transaction1->sale();
									$result = $transaction1->execute();

									$transactionid = $result['transactionid'];

									if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}
									
				            		if ($result['response_code'] == '100') {
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;

										$this->general_model->update_row_data($invoiceTable, $condition, $data);


										$totalProcessdInvoice = $totalProcessdInvoice + 1;
										if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}
										$responsetext = 'SUCCESS';
				            		}else{
				            			$responsetext = $result['responsetext'];
				            			
				            		}
				            		
				            		$response_code = $result['response_code'];
				            		$type = $result['type'];
				            		
				            		
				            		$resellerID = $merchant_data['resellerID'];

				            		
				            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

				            	}
				            	else if($gt_resultCC['gatewayType'] == 2 && $sch_method == 1)
				            	{
				            		/*Authurize Credit Card*/	
				            		$apiloginID       = $gt_resultCC['gatewayUsername'];
	    							$transactionKey   = $gt_resultCC['gatewayPassword'];
	    							$gatewayID = $gt_resultCC['gatewayID'];
			            			$gatewayType = $gt_resultCC['gatewayType'];

				            		$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
						  			$transaction1->setSandbox($this->config->item('auth_test_mode'));

						  			$transaction1->__set('company',$companyName);
									$transaction1->__set('first_name',$first_name);
									$transaction1->__set('last_name', $last_name);

						  			if($address1 != ''){
										$transaction1->__set('address', $address1);
									}
									
									if($country != ''){
										$transaction1->__set('country',$country);
									}
									if($city != ''){
										$transaction1->__set('city',$city);
									}
									if($state != ''){
										$transaction1->__set('state',$state);
									}
									if($email != ''){
										$transaction1->__set('email',$email);
									}
									if($phone != ''){
										$transaction1->__set('phone',$phone);
									}
				            		$gatewayName = "Auth"; 
	            					$exyear1   = substr($exyear,2);
									if(strlen($expmonth)==1){
										$expmonth = '0'.$expmonth;
									}
						 	    	$expry    = $expmonth.$exyear1;  
									
						    		$result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);

		            				
		            				if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

		            				if($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != ''){
		            					$transactionid = $result->transaction_id;
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

										if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}
		        					 	$responsetext = 'SUCCESS';
		        					 	$response_code = $result->response_code;
		            				}else{
		            					$responsetext = $result->response_reason_text;
		            					$response_code = 400;
		            					$transactionid = 'TXNFAILED'.time();
		            				}
				            		
				            		
				            		$type = $result->transaction_type;
				            		$resellerID = $merchant_data['resellerID'];
				            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

				            	}
				            	else if( !empty($gt_resultEC) && ($gt_resultEC['gatewayType']  == 2 && $sch_method == 2))
				            	{
				            		/*Authurize echeck*/
				            		$apiloginID       = $gt_resultEC['gatewayUsername'];
	    							$transactionKey   = $gt_resultEC['gatewayPassword'];
	    							$gatewayID = $gt_resultEC['gatewayID'];
			            			$gatewayType = $gt_resultEC['gatewayType'];

				            		$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
						  			$transaction1->setSandbox($this->config->item('Sandbox'));

						  			$transaction1->__set('company',$companyName);
									$transaction1->__set('first_name',$first_name);
									$transaction1->__set('last_name', $last_name);

						  			if($address1 != ''){
										$transaction1->__set('address', $address1);
									}
									
									if($country != ''){
										$transaction1->__set('country',$country);
									}
									if($city != ''){
										$transaction1->__set('city',$city);
									}
									if($state != ''){
										$transaction1->__set('state',$state);
									}
									if($email != ''){
										$transaction1->__set('email',$email);
									}
									if($phone != ''){
										$transaction1->__set('phone',$phone);
									}
				            		$gatewayName = "Auth ECheck"; 
		            				$transaction1->setECheck($routeNumber, $accountNumber, $accountType, $bank_name='Wells Fargo Bank NA', $accountName, $sec_code);
		            				$result = $transaction1->authorizeAndCapture($amount);

		            				$transactionid = $result->transaction_id;
		            				if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

		            				if($result->response_code == '1'){
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

										if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}
		        					 	$responsetext = 'SUCCESS';
		            				}else{
		            					$responsetext = $result->response_reason_text;
		            				}

				            		
				            		$response_code = $result->response_code;
				            		$type = $result->transaction_type;

				            		$resellerID = $merchant_data['resellerID'];
				            		
				            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

				            	}
				            	else if($gt_resultCC['gatewayType'] == 3 && $sch_method == 1)
				            	{
				            		/*Paytrace Credit card */
				            		$this->load->config('paytrace');
				            		$payusername   = $gt_resultCC['gatewayUsername'];
									$paypassword   = $gt_resultCC['gatewayPassword'];
									$integratorId = $gt_resultCC['gatewaySignature'];
									$gatewayID = $gt_resultCC['gatewayID'];
				            		$gatewayType = $gt_resultCC['gatewayType'];
				            		
									$grant_type    = "password";
									
									$payAPI  = new PayTraceAPINEW();	
								
									$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
									$json1 	= $payAPI->jsonDecode($oauth_result['temp_json_response']);
									if(isset($json1['error'])){
										$transactionid = $trID  = 'TXN-Failed'.time();
		        					  	$responsetext  =  $json1['error_description'];
		        					 	$code = 401;
		        					 	$response_code =  401;
		        					 	$oauth_moveforward = 'Failed';
									}else{
										$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
									}

									
									if(!$oauth_moveforward){ 
										
										$json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
						
										//set Authentication value based on the successful oAuth response.
										//Add a space between 'Bearer' and access _token 
										$oauth_token = sprintf("Bearer %s",$json['access_token']);
										$gatewayName = 'Paytrace';
		            					if (strlen($expmonth) == 1) {
											$expmonth = '0' . $expmonth;
										}
					
										$request_data = array(
											"amount" => $amount,
											"credit_card"=> array (
												"number"=> $card_no,
												"expiration_month"=>$expmonth,
												"expiration_year"=>$exyear ),
											"invoice_id"=>$invoiceID,
											"billing_address"=> array(
												"name"=>$name,
												"street_address"=> $address1,
												"city"=> $city,
												"state"=> $state,
												"zip"=> $zipcode
											)
										); 

										$reqURL = URL_KEYED_SALE;

			            				$request_data = json_encode($request_data); 
										$result    =  $payAPI->processTransaction($oauth_token,$request_data, $reqURL );
										$response     = $payAPI->jsonDecode($result['temp_json_response']);	

										if(isset($response['transaction_id'])){
											$transactionid       = $response['transaction_id'];
										} else if(isset($response['check_transaction_id'])){
											$transactionid       = $response['check_transaction_id'];
										}else{
											$transactionid  = 'TXN-Failed'.time();
										}
										if ( (isset($response['http_status_code']) && $response['http_status_code'] =='200' ) || (isset($response['response_code']) && $response['response_code'] =='120') || (isset($response['response_code']) && $response['response_code'] =='101') )
										{
					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;
											if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
			        					 	$response_code = 200;
			        					 	$responsetext = 'SUCCESS';
										}else{
											
					            			$response_code = (isset($response['http_status_code'])?$response['http_status_code']: $response['response_code']);
					            			$responsetext = $response['status_message'];
										}
										
					            		
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		if($transactionid == ''){
					            			$transactionid = 'TXNFAILED'.time();
					            		}
					            		
									}
									$type = 'sale';
									$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
				            		
				            	}else if( !empty($gt_resultEC) && ($gt_resultEC['gatewayType']  == 3 && $sch_method == 2) )
				            	{
				            		/*Paytrace Echeck*/
				            		$this->load->config('paytrace');
				            		$oauth_moveforward = '';
				            		$payusername   = $gt_resultEC['gatewayUsername'];
									$paypassword   = $gt_resultEC['gatewayPassword'];
									$integratorId = $gt_resultEC['gatewaySignature'];
									$gatewayID = $gt_resultEC['gatewayID'];
				            		$gatewayType = $gt_resultEC['gatewayType'];
				            		
									$grant_type    = "password";
									
									$payAPI  = new PayTraceAPINEW();	
								
									$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

									$json1 	= $payAPI->jsonDecode($oauth_result['temp_json_response']);
									if(isset($json1['error'])){
										$transactionid = $trID  = 'TXN-Failed'.time();
		        					  	$responsetext  =  $json1['error_description'];
		        					 	$code = 401;
		        					 	$response_code =  401;
		        					 	$oauth_moveforward = 'Failed';
									}else{
										//call a function of Utilities.php to verify if there is any error with OAuth token. 
										$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
									}

									if(!$oauth_moveforward){ 
										$json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
						
										//set Authentication value based on the successful oAuth response.
										//Add a space between 'Bearer' and access _token 
										$oauth_token = sprintf("Bearer %s",$json['access_token']);
										$gatewayName = 'Paytrace ECheck';
		            					$request_data = array(
											"amount"          => $amount,
											"check"     => array(
												"account_number"=> $accountNumber,
												"routing_number"=> $routeNumber,
											),
											"integrator_id" => $integratorId,
											"billing_address" => array(
												"name" => $accountName,
												"street_address" => $address1. ', '.$address2,
												"city" => $city,
												"state" => $state,
												"zip" => $zipcode,
											),
										);

										$reqURL = URL_ACH_SALE;

			            				$request_data = json_encode($request_data); 
										$result    =  $payAPI->processTransaction($oauth_token,$request_data, $reqURL );
										$response     = $payAPI->jsonDecode($result['temp_json_response']);	

										if(isset($response['transaction_id'])){
											$transactionid       = $response['transaction_id'];
										} else if(isset($response['check_transaction_id'])){
											$transactionid       = $response['check_transaction_id'];
										}else{
											$transactionid  = 'TXN-Failed'.time();
										}
										
										
										if ( (isset($response['http_status_code']) && $response['http_status_code'] =='200' ) || (isset($response['response_code']) && $response['response_code'] =='120') || (isset($response['response_code']) && $response['response_code'] =='101') )
										{
					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;
											if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
			        					 	$response_code = 200;
			        					 	$responsetext = 'SUCCESS';
										}else{
											
					            			$response_code = (isset($response['http_status_code'])?$response['http_status_code']: $response['response_code']);
					            			$responsetext = $response['status_message'];
										}
										
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		if($transactionid == ''){
					            			$transactionid = time().'TXNFAILED';
					            		}
					            		
									}
									$type = 'sale';
									$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
				            		
				            	}else if($gt_resultCC['gatewayType'] == 4 && $sch_method == 1){
				            		$this->load->config('paypal');
									/*Paypal Credit card*/
									$username  = $gt_resultCC['gatewayUsername'];
									$password  = $gt_resultCC['gatewayPassword'];
									$signature = $gt_resultCC['gatewaySignature'];
									$gatewayID = $gt_resultCC['gatewayID'];
									$gatewayType = $gt_resultCC['gatewayType'];

									$config = array(
										'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
										'APIUsername' => $username, 	// PayPal API username of the API caller
										'APIPassword' => $password,	// PayPal API password of the API caller
										'APISignature' => $signature, 	// PayPal API signature of the API caller
										'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
										'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
									  );
						
									// Show Errors
									if($config['Sandbox'])
									{
										error_reporting(E_ALL);
										ini_set('display_errors', '1');
									}
									
									$this->load->library('paypal/Paypal_pro', $config);	
				            		if ($sch_method == 1){
		            					$DPFields = array(
											'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																					//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
											'ipaddress' => '', 							// Required.  IP address of the payer's browser.
											'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
										);
						                $expDateYear        =$exyear;
						                $padDateMonth 		= str_pad($expmonth, 2, '0', STR_PAD_LEFT);
										$currencyID    ="USD";	
										$CCDetails = array(
											'creditcardtype' => $cardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
											'acct' => $card_no, 								// Required.  Credit card number.  No spaces or punctuation.  
											'expdate' => $padDateMonth.$expDateYear,								// Requirements determined by your PayPal account settings.  Security digits for credit card.
											'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
											'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
										);

										$PayerInfo = array(
											'email' => $email, 								// Email address of payer.
											'payerid' => '', 							// Unique PayPal customer ID for payer.
											'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
											'business' => '' 							// Payer's business name.
										);  
										$lastName='';
										$PayerName = array(
											'salutation' => $companyName, 						// Payer's salutation.  20 char max.
											'firstname' => $name, 							// Payer's first name.  25 char max.
											'middlename' => '', 						// Payer's middle name.  25 char max.
											'lastname' => $last_name, 							// Payer's last name.  25 char max.
											'suffix' => ''								// Payer's suffix.  12 char max.
										);

										$BillingAddress = array(
											'street' => $address1, 						// Required.  First street address.
											'street2' => $address2, 						// Second street address.
											'city' => $city, 							// Required.  Name of City.
											'state' => $state, 							// Required. Name of State or Province.
											'countrycode' => $country, 					// Required.  Country code.
											'zip' => $zipcode, 							// Required.  Postal code of payer.
											'phonenum' => $phone 						// Phone Number of payer.  20 char max.
										);
		
								
					                    $PaymentDetails = array(
											'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
											'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
											'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
											'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
											'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
											'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
											'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
											'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
											'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
											'custom' => '', 						// Free-form field for your own use.  256 char max.
											'invnum' => '', 						// Your own invoice or tracking number
											'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
											'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
											'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
										);	

										$PayPalRequestData = array(
											'DPFields' => $DPFields, 
											'CCDetails' => $CCDetails, 
											'PayerInfo' => $PayerInfo, 
											'PayerName' => $PayerName, 
											'BillingAddress' => $BillingAddress, 
											'PaymentDetails' => $PaymentDetails, 
											
										);
							
					        			$PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
					        			if(isset($PayPalResult['TRANSACTIONID'])) {  
                     						$tranID = $PayPalResult['TRANSACTIONID'];
                     						$code='111';

                     					}else{ 
                     						$tranID = 'TXN-Failed'.time();
                     						$code='401';
                     					}
                     					$transactionid = $tranID;		

					        			if(!empty($PayPalResult['RAWRESPONSE']) &&   ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))) 
	                     				{
					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;

											if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
			        					 	$responsetext = 'SUCCESS';
	                     				}else{
											$responsetext = $PayPalResult["ACK"];
	                     				}

	                     				if(isset($PayPalResult)){
	                     					
						            		
						            		$response_code = $code;
						            		$type = 'Paypal_sale';
						            		
						            		
						            		$resellerID = $merchant_data['resellerID'];
						            		$gatewayName = 'Paypal';
						            		if($transactionid == ''){
						            			$transactionid = 'TXNFAILED'.time();
						            		}
						            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

	                     				}

		            				}
				            		
				            	}else if($gt_resultCC['gatewayType'] == 5 && $sch_method == 1){
				            		/*Stripe Credit card*/
									$password  = $gt_resultCC['gatewayPassword'];
									$gatewayID = $gt_resultCC['gatewayID'];
									$gatewayType = $gt_resultCC['gatewayType'];
									
				            		$processAmount =  (int)($amount*100);
					
									$plugin = new ChargezoomStripe();

					        		$plugin->setApiKey($password);
					        		
				            		if ($sch_method == 1){
				            			try{
				            				$res = \Stripe\Token::create([
	        									'card' => [
	        									'number' =>$card_no,
	        									'exp_month' => $expmonth,
	        									'exp_year' =>  $exyear
	        								   ]
			        						]);
											$tcharge= json_encode($res);  
				        					$rest = json_decode($tcharge);
				        					if($rest->id){
				        						$token = $rest->id; 
											    try{
													$charge =	\Stripe\Charge::create(array(
														"amount" => $processAmount,
														"currency" => "usd",
														"source" => $token, // obtained with Stripe.js
														"description" => "Charge Using Stripe Gateway",
													
													));	

													$charge= json_encode($charge);
	                      	
							   						$result = json_decode($charge);

							   						if(isset($result) && $result->paid=='1' && $result->failure_code=="")
					 								{
								            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
														$condition  = $emailCondition;
														$this->general_model->update_row_data($invoiceTable, $condition, $data);

														$totalProcessdInvoice = $totalProcessdInvoice + 1;
														$code		 =  '200';
														$trID 	 = $result->id;
														if($integrationType == 1){
						        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
						        					 	} elseif($integrationType == 3){
															$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
															if ($invoices['status'] == 'ok') {
																$qbListTxnID = $invoices['payment_id'];
															}
														}
						        					 	$responsetext = 'SUCCESS';
					 								}else{
					 									$code =  $result->failure_code;
					 									$responsetext = $result->status; 
					 									$trID 	 = 'TXN-Failed'.time();
					 								}
					 								$transactionid = $trID;
								            		
								            		$response_code = $code;
								            		
								            		
								            		
								            		$resellerID = $merchant_data['resellerID'];
								            		$gatewayName = 'Stripe';
								            		if($transactionid == ''){
								            			$transactionid = 'TXNFAILED'.time();
								            		}
								            		
											    }catch(\Stripe\Error\Card $e) {
												// Since it's a decline, \Stripe\Error\Card will be caught
													$body = $e->getJsonBody();
													$err  = $body['error'];
													$responsetext = $err;
													$response_code = 401;
											   }
			        						}
				            			}catch(\Stripe\Error\Card $e) {
										// Since it's a decline, \Stripe\Error\Card will be caught
											$body = $e->getJsonBody();
											$err  = $body['error'];
											$responsetext = $err;
											$response_code = 401;
									   }
									   $type = 'stripe_sale';
									   $tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
		            					
		            				}
				            	}
				            	else if($gt_resultCC['gatewayType'] == 6 && $sch_method == 1)
				            	{
				            		$this->load->config('usaePay');
				            		/*USAePay Credit card*/
									$username   = $gt_resultCC['gatewayUsername'];
									$password   = $gt_resultCC['gatewayPassword'];
									$gatewayID = $gt_resultCC['gatewayID'];
									$gatewayType = $gt_resultCC['gatewayType'];
									
				            		
				            		$transaction = new umTransaction;
									$transaction->key=$username;
									$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
									
									$transaction->pin=$password;
									$transaction->usesandbox=$this->config->item('Sandbox');
									$transaction->invoice=$invoiceID;   		// invoice number.  must be unique.
									$transaction->description="Invoice Payment";	// description of charge
									$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
									$transaction->command="sale";

				            		if ($sch_method == 1){
		            					$transaction->card = $card_no;
										$expyear1   = substr($exyear,2);
										if(strlen($expmonth)==1){
											$expmonth = '0'.$expmonth;
										}
										$expry    = $expmonth.$expyear1;  
										$transaction->exp = $expry;
			                           
			                        	if($address1 != ''){
											$transaction->billstreet = $address1;
											$transaction->shipstreet = $address1;
										}
										if($address2 != ''){
											$transaction->billstreet2 = $address2;
											$transaction->shipstreet2 = $address2;
										}
										
										if($country != ''){
											$transaction->billcountry = $country;
											$transaction->shipcountry = $country;
										}
										if($city != ''){
											$transaction->billcity = $city;
											$transaction->shipcity = $city;
										}
										if($state != ''){
											$transaction->billstate = $state;
											$transaction->shipstate = $state;
										}
										
										if($zipcode != ''){
											$transaction->shipzip = $zipcode;
										}
										if($phone != ''){
											$transaction->billphone = $phone;
											$transaction->shipphone = $phone;
										}

										$transaction->amount = $amount;
									    
										$result = $transaction->Process();

										if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
	                     				{
					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;
											$code		 =  '200';
									  		$trID 	 = $transaction->refnum;
									  		if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
			        					 	$responsetext = 'SUCCESS';
	                     				}else{
	                     					
									  		$code     =   '401'; 	
									   		$trID  = 'TXN-Failed'.time();	
									   		$responsetext = isset($transaction->result)?$transaction->result:'Transaction Failed';
	                     				}

	                     				$transactionid = $trID;
					            		$responsetext = $responsetext;
					            		$response_code = $code;
					            		$type = 'sale';
					            		
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		$gatewayName = 'USAePay';
					            		if($transactionid == ''){
					            			$transactionid = 'TXNFAILED'.time();
					            		}
					            		$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

		            				}
				            	}else if($gt_resultCC['gatewayType'] == 7 && $sch_method == 1){
				            		/*heartland Credit card*/
				            		$this->load->config('globalpayments');
	            					$payusername   = $gt_resultCC['gatewayUsername'];
									$secretApiKey   = $gt_resultCC['gatewayPassword'];

		                            $crtxnID = '';

		                            $config = new PorticoConfig();
		               
									$config->secretApiKey = $secretApiKey;
									if($this->config->item('Sandbox'))
					                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
					                else
					                  $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
									
									$config->developerId =  $this->config->item('DeveloperId');
                 					$config->versionNumber =  $this->config->item('VersionNumber');

									ServicesContainer::configureService($config);

									$card = new CreditCardData();
									$card->number = $card_no;
									$card->expMonth = $expmonth;
									$card->expYear = $exyear;
									
		                            $card->cardType = $cardType;

		                            $address                 = new Address();

		                            if($address1 != ''){
										$address->streetAddress1 = $address1;
									}
									
									if($country != ''){
										$address->country        = $country;
									}

									if($city != ''){
										$address->city           = $city;
									}

									if($state != ''){
										$address->state          = $state;
									}
									
									if($zipcode != ''){
										$address->postalCode     = $zipcode;
									}

		                            $invNo = $invoiceID;

		                            try
                            		{

                            			$response = $card->charge($amount)
		                                    ->withCurrency('USD')
		                                    ->withAddress($address)
		                                    ->withInvoiceNumber($invNo)
		                                    ->withAllowDuplicates(true)
		                                    ->execute();
		                                if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {

					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;

		                                	// add level three data
					                        $transaction = new Transaction();
					                        $transaction->transactionReference = new TransactionReference();
					                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
					                        $level_three_request = [
					                            'card_no' => $card_no,
					                            'amount' => $amount,
					                            'invoice_id' => $invNo,
					                            'merchID' => $user_id,
					                            'transaction_id' => $response->transactionId,
					                            'transaction' => $transaction,
					                            'levelCommercialData' => $levelCommercialData,
					                            'gateway' => 7
					                        ];
					                        addlevelThreeDataInTransaction($level_three_request);
					                        
		                                    $responsetext = 'SUCCESS';
		                                    $trID    = $response->transactionId;
		                                    $code		 =  '200';
								  			
		                                    if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
		                                } else {
		                                    
		                                    $responsetext     = $response->responseMessage;
		                                    $trID    = $response->transactionId;
		                                    $code		 =  $response->responseCode;
		                                }

		                                $transactionid = $trID;
					            		$responsetext = $responsetext;
					            		$response_code = $code;
					            		
					            		$gatewayID = $gt_resultCC['gatewayID'];
					            		$gatewayType = $gt_resultCC['gatewayType'];
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		$gatewayName = 'Heartland';
					            		if($transactionid == ''){
					            			$transactionid = 'TXNFAILED'.time();
					            		}
					            		


                            		}catch (BuilderException $e) {
		                                $error = 'Build Exception Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (ConfigurationException $e) {
		                                $error = 'ConfigurationException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (GatewayException $e) {
		                                $error = 'GatewayException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (UnsupportedTransactionException $e) {
		                                $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (ApiException $e) {
		                                $error = ' ApiException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;

		                            }
		                            $type = 'sale';
		                            $tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
		            				
				            	}else if( !empty($gt_resultEC) && ($gt_resultCC['gatewayType'] == 7 && $sch_method == 2) ){
				            		/*heartland Echeck*/
				            		$this->load->config('globalpayments');
	            					$payusername   = $gt_resultEC['gatewayUsername'];
									$secretApiKey   = $gt_resultEC['gatewayPassword'];

		                            $crtxnID = '';

		                            $config = new PorticoConfig();
		               
									$config->secretApiKey = $secretApiKey;
									if($this->config->item('Sandbox'))
					                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
					                else
					                  $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');

					              	$config->developerId =  $this->config->item('DeveloperId');
                 					$config->versionNumber =  $this->config->item('VersionNumber');
									
									ServicesContainer::configureService($config);

									$check = new ECheck();
			                        $check->accountNumber = $accountNumber;
			                        $check->routingNumber = $routeNumber;
			                        if(strtolower($accountType) == 'checking'){
			                            $check->accountType = 0;
			                        }else{
			                            $check->accountType = 1;
			                        }

			                        if(strtoupper($accountHolderType) == 'PERSONAL'){
			                            $check->checkType = 0;
			                        }else{
			                            $check->checkType = 1;
			                        }
			                        $check->checkHolderName = $accountName;
			                        $check->secCode = "WEB";

		                            $address                 = new Address();

		                            if($address1 != ''){
										$address->streetAddress1 = $address1;
									}
									
									if($country != ''){
										$address->country        = $country;
									}

									if($city != ''){
										$address->city           = $city;
									}

									if($state != ''){
										$address->state          = $state;
									}
									
									if($zipcode != ''){
										$address->postalCode     = $zipcode;
									}

		                            $invNo = $invoiceID;

		                            try
                            		{

                            			$response = $check->charge($amount)
					                        ->withCurrency(CURRENCY)
					                        ->withAddress($address)
					                        ->withInvoiceNumber($invNo)
					                        ->withAllowDuplicates(true)
					                        ->execute();
		                                if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {

					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;

		                                	// add level three data
					                        $transaction = new Transaction();
					                        $transaction->transactionReference = new TransactionReference();
					                       
		                                    $responsetext = 'SUCCESS';
		                                    $trID    = $response->transactionId;
		                                    $code		 =  '200';
								  			
		                                    if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
		                                } else {
		                                    
		                                    $responsetext     = $response->responseMessage;
		                                    $trID    = $response->transactionId;
		                                    $code		 =  $response->responseCode;
		                                }

		                                $transactionid = $trID;
					            		$responsetext = $responsetext;
					            		$response_code = $code;
					            		
					            		$gatewayID = $gt_resultEC['gatewayID'];
					            		$gatewayType = $gt_resultEC['gatewayType'];
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		$gatewayName = 'Heartland ECheck';
					            		if($transactionid == ''){
					            			$transactionid = 'TXNFAILED'.time();
					            		}
					            		


                            		}catch (BuilderException $e) {
		                                $error = 'Build Exception Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (ConfigurationException $e) {
		                                $error = 'ConfigurationException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (GatewayException $e) {
		                                $error = 'GatewayException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (UnsupportedTransactionException $e) {
		                                $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            } catch (ApiException $e) {
		                                $error = ' ApiException Failure: ' . $e->getMessage();
		                                $transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
		                                
		                            }
		                            $type = 'sale';
		                            $tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
		            				
				            	}
				            	else if($gt_resultCC['gatewayType'] == 8 && $sch_method == 1)
				            	{
				            		/*CyberSource Credit card*/
				            		$this->load->config('cyber_pay');
				            		$gmerchantID    = $gt_resultCC['gatewayUsername'];
		    			        	$secretApiKey   = $gt_resultCC['gatewayPassword'];
									$secretKey      = $gt_resultCC['gatewaySignature'];
									$gatewayID = $gt_resultCC['gatewayID'];
					            	$gatewayType = $gt_resultCC['gatewayType'];

									$option =array();
        				        	$option['merchantID']     = trim($gt_resultCC['gatewayUsername']);
            			        	$option['apiKey']         = trim($gt_resultCC['gatewayPassword']);
        							$option['secretKey']      = trim($gt_resultCC['gatewaySignature']);
				            		
	            					if($this->config->item('Sandbox'))
	        						$env   = $this->config->item('SandboxENV');
	        						else
	        						$env   = $this->config->item('ProductionENV');
	        						$option['runENV']      = $env;
	        			
	        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
	                    
			                        $config = $commonElement->ConnectionHost();
			                       
			                    	$merchantConfig = $commonElement->merchantConfigObject();
			                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
			                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
			                    	
			                        $cliRefInfoArr = [
			                    		"code" => "test_payment"
			                    	];
			                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
			                    	$flag = true;
			                        if ($flag == "true")
			                        {
			                            $processingInformationArr = [
			                    			"capture" => true, "commerceIndicator" => "internet"
			                    		];
			                        }
			                        else
			                        {
			                            $processingInformationArr = [
			                    			"commerceIndicator" => "internet"
			                    		];
			                        }
			                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
			                    
			                        $amountDetailsArr = [
			                    		"totalAmount" => $amount,
			                    		"currency" => CURRENCY
			                    	];
			                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

			                        $billtoArr = [
			                    		"firstName" => $first_name,
			                    		"lastName" => $last_name,
			                    		"address1" => $address1,
			                    		"postalCode" => $zipcode,
			                    		"locality" => $city,
			                    		"administrativeArea" => $state,
			                    		"country" => $country,
			                    		"phoneNumber" => $phone,
			                    		"company" => $companyName,
			                    		"email" => $email
			                    	];
			                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
			             	
			                        $orderInfoArr = [
			                    		"amountDetails" => $amountDetInfo, 
			                    		"billTo" => $billto
			                    	];
			                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
			                    	
			                        $paymentCardInfo = [
			                        		"expirationYear" => $exyear,
			                        		"number" => $card_no,
			                        		"expirationMonth" => $expmonth
			                        	];
			                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
			                    	
			                        $paymentInfoArr = [
			                    		"card" => $card
			                        ];
			                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
			                    
			                        $paymentRequestArr = [
			                    		"clientReferenceInformation" => $client_reference_information, 
			                    		"orderInformation" => $order_information, 
			                    		"paymentInformation" => $payment_information, 
			                    		"processingInformation" => $processingInformation
			                    	];
			                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
			                    	
			                        $api_response = list($response, $statusCode, $httpHeader) = null;

			                        try{
			                        	$api_response = $api_instance->createPayment($paymentRequest);
                            
			                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
			        					{
					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;

			        						$trID =   $api_response[0]['id'];
			        					  	$responsetext = 'SUCCESS';
			        					  
			        					 	$code =   '200';
			        					 	if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}

			        					}else{
			        						 $trID  =   $api_response[0]['id'];
			        					     $responsetext  =   $api_response[0]['status'];
			        					     $code =   $api_response[1];
			        					}

			        					$transactionid = $trID;
					            		$responsetext = $responsetext;
					            		$response_code = $code;
					            		
					            		
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		$gatewayName = 'Cybersource';
					            		if($transactionid == ''){
					            			$transactionid = 'TXNFAILED'.time();
					            		}
					            		
			                        }
			                        catch(Cybersource\ApiException $e)
			        				{
			        					
			        					$error = $e->getMessage();
			        					$transactionid = 'TXNFAILED'.time();
		                                $responsetext = $e->getMessage();
		                                $response_code = 401;
			        				}
			        				$type = 'sale';
			        				$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
		            				
				            	}
				            	else if($gt_resultCC['gatewayType'] == 10 && $sch_method == 1)
				            	{
				            		/*iTransact credit card */
				            		$apiUsername  = $gt_resultCC['gatewayUsername'];
            						$apiKey  = $gt_resultCC['gatewayPassword'];
            						$gatewayID = $gt_resultCC['gatewayID'];
			            			$gatewayType = $gt_resultCC['gatewayType'];
				            		
	            					$sdk = new iTTransaction();
				            		if (strlen($expmonth) > 1 && $expmonth <= 9) {
		                                $expmonth = substr($expmonth, 1);
		                            }

		                            $payload = array(
		                                "amount"          => ($amount * 100),
		                                "card"     => array(
		                                    "name" => $name,
		                                    "number" => $card_no,
		                                    "exp_month" => $expmonth,
		                                    "exp_year"  => $exyear,
		                                ),
		                                "billing_address" => array(
		                                    "line1"           => $address1,
		                                    "line2" => $address2,
		                                    "city"           => $city,
		                                    "state"          => $state,
		                                    "postal_code"            => $zipcode,
		                                ),
		                            );

		                            $gatewayName = iTransactGatewayName;

		            				$result = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);
		            				
		            				if ($result['status_code'] == '200' || $result['status_code'] == '201') {
		            				
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

		            					$trID =   $result['id'];
		        					  	
		        					  	$responsetext = 'SUCCESS';
		        					 	$code =   '200';

		        					 	if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}
		            				}else{
		            					
		            					$trID = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
		        					  	$responsetext  = $result['status'] = $result['error']['message'];
		        					 	$code =   '401';
		            				}
		            				$transactionid = $trID;
				            		$responsetext = $responsetext;
				            		$response_code = $code;
				            		$type = 'sale';
				            		
				            		
				            		$resellerID = $merchant_data['resellerID'];
				            		if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

				            		$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);


				            	}else if( !empty($gt_resultEC) && ($gt_resultEC['gatewayType']  == 10 && $sch_method == 2)){
				            		/*iTransact Echeck*/
				            		$apiUsername  = $gt_resultEC['gatewayUsername'];
            						$apiKey  = $gt_resultEC['gatewayPassword'];
            						$gatewayID = $gt_resultEC['gatewayID'];
			            			$gatewayType = $gt_resultEC['gatewayType'];
				            		
	            					$sdk = new iTTransaction();
				            		$gatewayName = iTransactGatewayName.' ECheck';
	            					$payload = [
		                                "amount" => ($amount * 100),
		                                "ach" => [
		                                    "name" => $accountName, 
		                                    "account_number" => $accountNumber, 
		                                    "routing_number" => $routeNumber, 
		                                    "phone_number" => $phone, 
		                                    "sec_code" => $sec_code, 
		                                    "savings_account" => ($accountType == 'savings') ? true : false, 
		                                   
		                                ],
		                                "address" => [
		                                    "line1" => $address1,
		                                    "line2" => $address2,
		                                    "city" => $city,
		                                    "state" => $state,
		                                    "postal_code" => $zipcode,
		                                    "country" => $country
		                                ],
		                               
		                            ];

		            				$result = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);
		            				if ($result['status_code'] == '200' || $result['status_code'] == '201') {
		            			
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

		            					$trID =   $result['id'];
		        					  
		        					  	$responsetext = 'SUCCESS';
		        					 	$code =   '200';

		        					 	if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}
		            				}else{
		            					$trID = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
		        					  	$responsetext  = $result['status'] = $result['error']['message'];
		        					 	$code =   '401';
		            				}
		            				$transactionid = $trID;
				            		$responsetext = $responsetext;
				            		$response_code = $code;
				            		$type = 'sale';
				            		
				            		
				            		$resellerID = $merchant_data['resellerID'];
				            		if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

				            		$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);


				            	}
				            	else if(  !empty($gt_resultEC) &&  ($gt_resultEC['gatewayType']  == 11 || $gt_resultEC['gatewayType']  == 13 ) && $sch_method == 2)
				            	{
				            		/*FluidPay eCheck*/
				            		$this->load->config('fluidpay');
				            		$gatewayUsername = $gt_resultEC['gatewayUsername'];
									$gatewayID = $gt_resultEC['gatewayID'];
			            			$gatewayType = $gt_resultEC['gatewayType'];
				            		$gatewayTransaction              = new Fluidpay();
						            $gatewayTransaction->environment = $this->gatewayEnvironment;
						            $gatewayTransaction->apiKey      = $gatewayUsername;
				            		
	            					$transaction = array(
		                                "type"                => "sale",
		                                "amount"              => $amount * 100,
		                                "tax_amount"          => 0,
		                                "shipping_amount"     => 0,
		                                "currency"            => "USD",
		                                "description"         => $refnumber . ' pay',
		                                "order_id"            => $refnumber,
		                                "po_number"           => null,
		                                "ip_address"          => getClientIpAddr(),
		                                "email_receipt"       => true,
		                                "email_address"       => $email,
		                                "create_vault_record" => true,
		                                "payment_method"      => array(
		                                    "ach" => array(
		                                        "routing_number" => $routeNumber,
		                                        "account_number" => $accountNumber,
		                                        "sec_code"       => "WEB",
		                                        "account_type"   => $accountType,
		                                    ),
		                                ),
		                                "billing_address"     => array(
		                                    "first_name"     => $first_name,
		                                    "last_name"      => $last_name,
		                                    "company"        => $companyName,
		                                    "address_line_1" => $address1,
		                                    "city"           => $city,
		                                    "state"          => $state,
		                                    "postal_code"    => $zipcode,
		                                    "phone"          => $phone,
		                                    "fax"            => $phone,
		                                    "email"          => $email,
		                                ),
		                                "shipping_address"    => array(
		                                    "first_name"     => $first_name,
		                                    "last_name"      => $last_name,
		                                    "company"        => $companyName,
		                                    "address_line_1" => $address1,
		                                    "city"           => $city,
		                                    "state"          => $state,
		                                    "postal_code"    => $zipcode,
		                                    "phone"          => $phone,
		                                    "fax"            => $phone,
		                                    "email"          => $email,
		                                ),
		                            );
		                            if($gt_resultEC['gatewayType']  == 11){
		                            	$gatewayName = FluidGatewayName.' ECheck';
		                            }else{
		                            	$gatewayName = BASYSGatewayName.' ECheck';
		                            }
		                            
		            				
		            				$result = $gatewayTransaction->processTransaction($transaction);
		            				
		            				if ($result['status'] == 'success') {

				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

		            					$trID  = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : '';
		        					  
		        					  	$responsetext = 'SUCCESS';
		        					 	$code = $result['data']['response_code'];
		        					 	if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}

		            				}else{
		            					$trID  = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : 'TXN-Failed'.time();
		            					
		        					  	$responsetext  =   $result['msg'];
		        					 	$code = 401;
		            				}

		            				$transactionid = $trID;
				            		$responsetext = $responsetext;
				            		$response_code = $code;
				            		$type = 'sale';
				            		
				            		
				            		$resellerID = $merchant_data['resellerID'];
				            		if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

				            		$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

				            	}else if( ($gt_resultCC['gatewayType'] == 11 || $gt_resultCC['gatewayType'] == 13) && $sch_method == 1)
				            	{
				            		$this->load->config('fluidpay');
				            		/*FluidPay and BasysIQpro Credit Card*/
				            		$gatewayUsername = $gt_resultCC['gatewayUsername'];
			            			$gatewayID = $gt_resultCC['gatewayID'];
			            			$gatewayType = $gt_resultCC['gatewayType'];
				            		$gatewayTransaction              = new Fluidpay();
						            $gatewayTransaction->environment = $this->gatewayEnvironment;
						            $gatewayTransaction->apiKey      = $gatewayUsername;
				            		
	            					$exyear1 = substr($exyear, 2);
		                            if (strlen($expmonth) == 1) {
		                                $expmonth = '0' . $expmonth;
		                            }
		                            $expry = $expmonth . '/' . $exyear1;

		                            $transaction = array(
		                                "type"                => "sale",
		                                "amount"              => $amount * 100,
		                                "tax_amount"          => 0,
		                                "shipping_amount"     => 0,
		                                "currency"            => "USD",
		                                "description"         => $refnumber . ' pay',
		                                "order_id"            => $refnumber,
		                                "po_number"           => null,
		                                "ip_address"          => getClientIpAddr(),
		                                "email_receipt"       => true,
		                                "email_address"       => $email,
		                                "create_vault_record" => true,
		                                "payment_method"      => array(
		                                    "card" => array(
		                                        "entry_type"      => "keyed",
		                                        "number"          => $card_no,
		                                        "expiration_date" => $expry,
		                                    ),
		                                ),
		                                "billing_address"     => array(
		                                    "first_name"     => $first_name,
		                                    "last_name"      => $last_name,
		                                    "company"        => $companyName,
		                                    "address_line_1" => $address1,
		                                    "city"           => $city,
		                                    "state"          => $state,
		                                    "postal_code"    => $zipcode,
		                                    "phone"          => $phone,
		                                    "fax"            => $phone,
		                                    "email"          => $email,
		                                ),
		                                "shipping_address"    => array(
		                                    "first_name"     => $first_name,
		                                    "last_name"      => $last_name,
		                                    "company"        => $companyName,
		                                    "address_line_1" => $address1,
		                                    "city"           => $city,
		                                    "state"          => $state,
		                                    "postal_code"    => $zipcode,
		                                    "phone"          => $phone,
		                                    "fax"            => $phone,
		                                    "email"          => $email,
		                                ),
		                            );
		                            if($gt_resultCC['gatewayType'] == 11){
		                            	$gatewayName = FluidGatewayName;
		                            }else{
		                            	$gatewayName = BASYSGatewayName;
		                            }
		                            

		            				$result = $gatewayTransaction->processTransaction($transaction);

		            				if ($result['status'] == 'success') {

				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

		            					$trID  = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : '';
	
		        					  	$responsetext = 'SUCCESS';
		        					 	$code = $result['data']['response_code'];
		        					 	if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}

		            				}else{
		            					$trID  = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : 'TXN-Failed'.time();
		        					  	$responsetext  =   $result['msg'];
		        					 	$code = 401;
		            				}

		            				$transactionid = $trID;
				            		$responsetext = $responsetext;
				            		$response_code = $code;
				            		$type = 'sale';
				            		
				            		
				            		$resellerID = $merchant_data['resellerID'];
				            		if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

				            		$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

				            	}else if($gt_resultCC['gatewayType'] == 12 && $sch_method == 1){

				            		/*TSYS Credit Card*/
				            		$this->load->config('TSYS');
				            		$type = 'sale';
				            		$deviceID = $gt_resultCC['gatewayMerchantID'].'01';
			            			$gatewayUsername = $gt_resultCC['gatewayUsername'];
			            			$gatewayPassword = $gt_resultCC['gatewayPassword'];
			            			$gatewayMerchantID = $gt_resultCC['gatewayMerchantID'];
			            			$gatewayID = $gt_resultCC['gatewayID'];
				            		$gatewayType = $gt_resultCC['gatewayType'];
				            		
						            $gatewayTransaction              = new TSYS();
						            $gatewayTransaction->environment = $this->gatewayEnvironment;
						            $gatewayTransaction->deviceID = $deviceID;
						            $result = $gatewayTransaction->generateToken($gatewayUsername,$gatewayPassword,$gatewayMerchantID);
						            $generateToken = '';
						            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){

						                $transactionid = $trID  = 'TXN-Failed'.time();

		        					  	$responsetext  =  $result['GenerateKeyResponse']['responseMessage'];
		        					 	$code = 401;
		        					 	$response_code =  401;
						            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){

						                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
						                
						            }
						            $gatewayTransaction->transactionKey = $generateToken;


				            		
			            			$responseType = 'SaleResponse';
	            					$exyear1 = substr($exyear, 2);
		                            if (strlen($expmonth) == 1) {
		                                $expmonth = '0' . $expmonth;
		                            }
		                            $expry = $expmonth . '/' . $exyear1;

		                            $transaction['Sale'] = array(
		                                "deviceID"                          => $deviceID,
		                                "transactionKey"                    => $generateToken,
		                                "cardDataSource"                    => "MANUAL",  
		                                "transactionAmount"                 => $amount * 100,
		                                "currencyCode"                      => "USD",
		                                "cardNumber"                        => $card_no,
		                                "expirationDate"                    => $expry,
		                                "addressLine1"                      => ($address1 != '')?$address1:'None',
		                                "zip"                               => ($zipcode != '')?$zipcode:'None',
		                                "orderNumber"                       => $refnumber,
		                                "notifyEmailID"                     => (($email != ''))?$email:'chargezoom@chargezoom.com',
		                                "firstName"                         => (($first_name != ''))?$first_name:'None',
		                                "lastName"                          => (isset($last_name))?$last_name:'None',
		                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
		                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
		                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
		                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
		                                "terminalOutputCapability"          => "DISPLAY_ONLY",
		                                "maxPinLength"                      => "UNKNOWN",
		                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
		                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
		                                "cardPresentDetail"                 => "CARD_PRESENT",
		                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
		                                "cardholderAuthenticationEntity"    => "OTHER",
		                                "cardDataOutputCapability"          => "NONE",

		                                "customerDetails"   => array( 
		                                            "contactDetails" => array(
		                                              
		                                                "addressLine1"=> ($address1 != '')?$address1:'None',
		                                                 "addressLine2"  => ($address2 != '')?$address2:'None',
		                                                "city"=>($city != '')?$city:'None',
		                                                "zip"=>($zipcode != '')?$zipcode:'None',
		                                            ),
		                                            "shippingDetails" => array( 
		                                                "firstName"=>(isset($first_name))?$first_name:'None',
		                                                "lastName"=>(isset($last_name))?$last_name:'None',
		                                                "addressLine1"=>($address1 != '')?$address1:'None',
		                                                 "addressLine2" => ($address2 != '')?$address2:'None',
		                                                "city"=>($city != '')?$city:'None',
		                                                "zip"=>($zipcode != '')?$zipcode:'None',
		                                                "emailID"=>(isset($email))?$email:'chargezoom@chargezoom.com'
		                                             )
		                                        )
		                            );
		                           
		                            $gatewayName = TSYSGatewayName;

		            				if($generateToken != ''){
	                                	$resultPay = $gatewayTransaction->processTransaction($transaction);
	                                	if (isset($resultPay[$responseType]['status']) && $resultPay[$responseType]['status'] == 'PASS') {
					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;

			            					$trID  = (isset($resultPay[$responseType]['transactionID'])) ? $resultPay[$responseType]['transactionID'] : '';

			        					  	$responsetext = 'SUCCESS';
			        					 	$code = 200;
			        					 	if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
			        					 	
	                                	}else{
	                                		$trID  = 'TXN-Failed'.time();;

			        					  	$responsetext  =  $resultPay[$responseType]['responseMessage'];
			        					 	$code = 401;
	                                	}
	                                	$transactionid = $trID;
					            		$responsetext = $responsetext;
					            		$response_code = $code;
					            		
					            		
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		
					            		if($transactionid == ''){
					            			$transactionid = 'TXNFAILED'.time();
					            		}
					            		
	                                }
	                                $tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);


				            	}else if( !empty($gt_resultEC) && $gt_resultEC['gatewayType']  == 12 && $sch_method == 2){
				            		/*TSYS Echeck*/
				            		$this->load->config('TSYS');
				            		$type = 'sale';
				            		$deviceID = $gt_resultEC['gatewayMerchantID'].'01';
									$gatewayUsername = $gt_resultEC['gatewayUsername'];
			            			$gatewayPassword = $gt_resultEC['gatewayPassword'];
			            			$gatewayMerchantID = $gt_resultEC['gatewayMerchantID'];
			            			$gatewayID = $gt_resultEC['gatewayID'];
				            		$gatewayType = $gt_resultEC['gatewayType'];
				            		
						            $gatewayTransaction              = new TSYS();
						            $gatewayTransaction->environment = $this->gatewayEnvironment;
						            $gatewayTransaction->deviceID = $deviceID;
						            $result = $gatewayTransaction->generateToken($gatewayUsername,$gatewayPassword,$gatewayMerchantID);
						            $generateToken = '';
						            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
						                $transactionid = $trID  = 'TXN-Failed'.time();

		        					  	$responsetext  =  $result['GenerateKeyResponse']['responseMessage'];
		        					 	$code = 401;
		        					 	$response_code =  401;
						            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){

						                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
						                
						            }
						            $gatewayTransaction->transactionKey = $generateToken;


				            		
	            					$gatewayName = TSYSGatewayName.' ECheck';
	            					$responseType = 'AchResponse';
	            					$transaction['Ach'] = array(
		                                "deviceID"              => $deviceID,
		                                "transactionKey"        => $generateToken,
		                                "transactionAmount"     => $amount * 100,
		                                "accountDetails"        => array(
		                                        "routingNumber" => $routeNumber,
		                                        "accountNumber" => $accountNumber,
		                                        "accountType"   => strtoupper($accountType),
		                                        "accountNotes"  => "count",
		                                        "addressLine1"  => ($address1 != '')?$address1:'None',
		                                        "addressLine2"  => ($address2 != '')?$address2:'None',
		                                        "zip"           => ($zipcode != '')?$zipcode:'None',
		                                        "city"          => ($city != '')?$city:'None',
		                                        "state"         => ($state != '')?$state:'AZ',
		                                        "country"       => ($country != '')?$country:'USA'
		                                ),
		                                "achSecCode"                => "WEB",
		                                "originateDate"             => date('Y-m-d'),
		                                "addenda"                   => "addenda",
		                                "firstName"                 => $first_name,
		                                "lastName"                  => $last_name,
		                                "customerPhone"             => ($phone != '')?$phone:'None',
		                                "addressLine1"              => ($address1 != '')?$address1:'None',
		                                "addressLine2"              => ($address2 != '')?$address2:'None',
		                                 "zip"                      => ($zipcode != '')?$zipcode:'None',
		                                "city"                      => ($city != '')?$city:'None',
		                                "state"                     => ($state != '')?$state:'AZ',
		                                "country"                   => ($country != '')?$country:'USA',
		                                "dob"                       => "1967-08-13",
		                                "ssn"                       => "1967-08-13",
		                                "driverLicenseNumber"       => "101",
		                                "driverLicenseIssuedState"  => ($state != '')?$state :'AZ',
		                                "developerID"               => "1234"    
		                            );
		            				
		            				if($generateToken != ''){
	                                	$resultPay = $gatewayTransaction->processTransaction($transaction);
	                                	if (isset($resultPay[$responseType]['status']) && $resultPay[$responseType]['status'] == 'PASS') {
					            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;

			            					$trID  = (isset($resultPay[$responseType]['transactionID'])) ? $resultPay[$responseType]['transactionID'] : '';

			        					  	$responsetext = 'SUCCESS';
			        					 	$code = 200;
			        					 	if($integrationType == 1){
			        					 		$qbListTxnID = $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
			        					 	} elseif($integrationType == 3){
												$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
												if ($invoices['status'] == 'ok') {
													$qbListTxnID = $invoices['payment_id'];
												}
											}
			        					 	
	                                	}else{
	                                		$trID  = 'TXN-Failed'.time();;

			        					  	$responsetext  =  $resultPay[$responseType]['responseMessage'];
			        					 	$code = 401;
	                                	}
	                                	$transactionid = $trID;
					            		$responsetext = $responsetext;
					            		$response_code = $code;
					            		
					            		
					            		
					            		$resellerID = $merchant_data['resellerID'];
					            		
					            		if($transactionid == ''){
					            			$transactionid = 'TXNFAILED'.time();
					            		}
					            		



	                                }
	                                $tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

				            	} else if($gt_resultCC['gatewayType'] == 14 && $sch_method == 1){
									/*CardPointe Credit card */
									$cardpointeuser   = $gt_resultCC['gatewayUsername'];
									$cardpointepass   = $gt_resultCC['gatewayPassword'];
									$cardpointeMerchID = $gt_resultCC['gatewayMerchantID'];
									$cardpointeSiteName = $gt_resultCC['gatewaySignature'];
									$gatewayID = $gt_resultCC['gatewayID'];
									$gatewayType = $gt_resultCC['gatewayType'];

	            					$client = new CardPointe();
				            		if (strlen($expmonth) > 1 && $expmonth <= 9) {
		                                $expmonth = substr($expmonth, 1);
		                            }

		                            
									if($expmonth < 10){
										$expmonth = 0 . $expmonth;
									} 

									$expry = $expmonth . '/' . substr($exyear, 2);
		                            $gatewayName = 'CardPointe';
		                            
									$result = $client->authorize_no($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $name, $address1, $city, $state, $zipcode);
		            				if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') {
		            					
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

		            					$trID =   $result['retref'];
		        					  	$responsetext = 'SUCCESS';
		        					 	$code =   '200';

		        					 	if($integrationType == 1){
		        					 		$this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}
		            				}else{
		            					
		            					$trID = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
		        					  	$responsetext  = $result['respstat'] = $result['resptext'];
		        					 	$code =   '401';
		            				}
		            				$transactionid = $trID;
				            		$responsetext = $responsetext;
				            		$response_code = $code;
				            		$type = 'sale';
				            		
				            		
				            		$resellerID = $merchant_data['resellerID'];
				            		if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

									$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);


				            	}else if( !empty($gt_resultEC) && $gt_resultEC['gatewayType']  == 14 && $sch_method == 2){
				            		/*CardPointe Echeck*/
									$cardpointeuser   = $gt_resultCC['gatewayUsername'];
									$cardpointepass   = $gt_resultCC['gatewayPassword'];
									$cardpointeMerchID = $gt_resultCC['gatewayMerchantID'];
									$cardpointeSiteName = $gt_resultCC['gatewaySignature'];
            						$gatewayID = $gt_resultEC['gatewayID'];
			            			$gatewayType = $gt_resultEC['gatewayType'];
				            		
	            					$client = new CardPointe();
				            		$gatewayName = 'CardPointe ECheck';

									$result = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountNumber, $routeNumber, $amount, $accountType, $name, $address1, $city, $state, $zipcode);

		            				if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') {
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;
										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;

		            					$trID =   $result['retref'];
		        					  	$responsetext = 'SUCCESS';
		        					 	$code =   '200';

		        					 	if($integrationType == 1){
		        					 		$this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
		        					 	} elseif($integrationType == 3){
											$invoicePayment    = $freshbookSync->create_invoice_payment($invoiceID, $amount);
											if ($invoices['status'] == 'ok') {
												$qbListTxnID = $invoices['payment_id'];
											}
										}
		            				}else{
		            					$trID = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
		        					  	$responsetext  = $result['respstat'] = $result['resptext'];
		        					 	$code =   '401';
		            				}
		            				$transactionid = $trID;
				            		$responsetext = $responsetext;
				            		$response_code = $code;
				            		$type = 'sale';
				            		
				            		
				            		$resellerID = $merchant_data['resellerID'];
				            		if($transactionid == ''){
				            			$transactionid = 'TXNFAILED'.time();
				            		}

				            		$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);


				            	}else if( $gt_resultCC['gatewayType']  == 15 && $sch_method == 1 )
				            	{
				            		$this->load->config('payarc');
				            		$this->load->library('PayarcGateway');
				            		// PayArc Payment Gateway, set enviornment and secret key
					                $this->payarcgateway->setApiMode($this->gatewayEnvironment);
					                $this->payarcgateway->setSecretKey($gt_resultCC['gatewayUsername']);

					                $address_info = ['address_line1' => $address1, 'address_line2' => $address2, 'state' => $state, 'country' => $country];


					                // Create Credit Card Token
                					$token_response = $this->payarcgateway->createCreditCardToken($card_no, $expmonth, $exyear, $cvv, $address_info);

               			 			$token_data = json_decode($token_response['response_body'], 1);
               			 			

               			 			if(isset($token_data['status']) && $token_data['status'] == 'error'){
               			 				$responsetext = $token_data['message'];
				            			$response_code = 401;
				            			$transactionid = 'TXNFAILED'.time();
               			 			} else if(isset($token_data['data']) && !empty($token_data['data']['id'])) 
               			 			{
               			 				// If token created
					                    $token_id = $token_data['data']['id'];

					                    $charge_payload = [];

					                    $charge_payload['token_id'] = $token_id;
					                    $charge_payload['amount'] = $amount * 100; // must be in cents and min amount is 50c USD

					                    $charge_payload['currency'] = 'usd'; 

					                    $charge_payload['capture'] = '1';

					                    $charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.

					                    if($zipcode) {
					                        $charge_payload['ship_to_zip'] = $zipcode; 
					                    };

					                    $charge_response = $this->payarcgateway->createCharge($charge_payload);

					                    $result = json_decode($charge_response['response_body'], 1);


					                    if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'submitted_for_settlement') 
					                    {
					                    	$transactionid = $responseId = $result['data']['id'];
					                    	$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;

											$this->general_model->update_row_data($invoiceTable, $condition, $data);

											$totalProcessdInvoice = $totalProcessdInvoice + 1;
											if($integrationType == 1){
			        					 		
			        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
			        					 	}
											$responsetext = 'SUCCESS';
											$response_code = 100;

					                    }else{
					                    	$responsetext = $result['message'];
				            				$response_code = 401;
				            				$transactionid = 'TXNFAILED'.time();
					                    }
					                    
               			 			}
               			 			$gatewayID = $gt_resultCC['gatewayID'];
				            		$gatewayType = $gt_resultCC['gatewayType'];
				            		$gatewayName = PayArcGatewayName;
				            		$type = 'sale';
				            		$resellerID = $merchant_data['resellerID'];
				            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType,$qbListTxnID,$custom_data_fields);


				            	}else if( $gt_resultCC['gatewayType']  == 16 && $sch_method == 1 )
				            	{
				            		/*EPX Credit card*/
				            		$this->load->config('EPX');
				            		$CUST_NBR = $gt_resultCC['gatewayUsername'];
               						$MERCH_NBR = $gt_resultCC['gatewayPassword'];
                					$DBA_NBR = $gt_resultCC['gatewaySignature'];
                					$TERMINAL_NBR = $gt_resultCC['extra_field_1'];
                					$orderId = time();

                					$amount = number_format($amount,2,'.','');
                					$exyear1 = substr($exyear, 2);
		                            if (strlen($expmonth) == 1) {
		                                $expmonth = '0' . $expmonth;
		                            }
		                            $expry = $exyear1.$expmonth;

					                $transaction = array(
					                    'CUST_NBR' => $CUST_NBR,
					                    'MERCH_NBR' => $MERCH_NBR,
					                    'DBA_NBR' => $DBA_NBR,
					                    'TERMINAL_NBR' => $TERMINAL_NBR,
					                    'TRAN_TYPE' => 'CCE1',
					                    'ACCOUNT_NBR' => $card_no,
					                    'EXP_DATE' => $expry,
					                    'CARD_ENT_METH' => 'E',
                    					'INDUSTRY_TYPE' => 'E',
					                    'AMOUNT' => $amount,
					                    'TRAN_NBR' => rand(1,10),
					                    'INVOICE_NBR' => $invoiceID,
					                    'ORDER_NBR' => $refnumber,
					                    'BATCH_ID' => time(),
					                    'VERBOSE_RESPONSE' => 'Y',


					                );
					                
					                if($cvv != ''){
					                    $transaction['CVV2'] = $cvv;
					                }
					                if($address1 != ''){
					                    $transaction['ADDRESS'] = $address1;
					                }
					                if($city != ''){
					                    $transaction['CITY'] = $city;
					                }
					                
					                if($zipcode != ''){
					                    $transaction['ZIP_CODE'] = $zipcode;
					                }
					                if($first_name != ''){
					                    $transaction['FIRST_NAME'] = $first_name;
					                }
					                if($last_name != ''){
					                    $transaction['LAST_NAME'] = $last_name;
					                }
					               
					                $gatewayTransaction              = new EPX();
					                $result = $gatewayTransaction->processTransaction($transaction);

	            					if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
	            					{	

	            						$transactionid = $transactionID = $result['AUTH_GUID'];
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;

										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;
										if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
		        					 	}
										$responsetext = 'SUCCESS';
										$response_code = 100;
				            		}else{
				            			$responsetext = $result['AUTH_RESP_TEXT'];
				            			$response_code = 401;
				            			$transactionid = 'TXNFAILED'.time();
				            			
				            		}
				            		$gatewayID = $gt_resultCC['gatewayID'];
				            		$gatewayType = $gt_resultCC['gatewayType'];
				            		$gatewayName = EPXGatewayName;
				            		$type = 'sale';
				            		$resellerID = $merchant_data['resellerID'];
				            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType,$qbListTxnID,$custom_data_fields);

				            	}else if(  !empty($gt_resultEC) && $gt_resultEC['gatewayType']  == 16 && $sch_method == 2 )
				            	{
				            		/*EPX ACH*/
				            		$this->load->config('EPX');
				            		$CUST_NBR = $gt_resultEC['gatewayUsername'];
               						$MERCH_NBR = $gt_resultEC['gatewayPassword'];
                					$DBA_NBR = $gt_resultEC['gatewaySignature'];
                					$TERMINAL_NBR = $gt_resultEC['extra_field_1'];
                					if($accountType == 'savings'){
					                    $txnType = 'CKS2'; 
					                }else{
					                    $txnType = 'CKC2';
					                }
                					$orderId = time();

                					$amount = number_format($amount,2,'.','');
                					
                					$transaction = array(
					                    'CUST_NBR' => $CUST_NBR,
					                    'MERCH_NBR' => $MERCH_NBR,
					                    'DBA_NBR' => $DBA_NBR,
					                    'TERMINAL_NBR' => $TERMINAL_NBR,
					                    'TRAN_TYPE' => $txnType,
					                    'ACCOUNT_NBR' => $accountNumber,
					                    'ROUTING_NBR' => $routeNumber,
					                    'RECV_NAME' => $accountName,
					                    'AMOUNT' => $amount,
					                    'TRAN_NBR' => rand(1,10),
					                    'INVOICE_NBR' => $invoiceID,
					                    'ORDER_NBR' => $orderId,
					                    'BATCH_ID' => time(),
					                    'VERBOSE_RESPONSE' => 'Y',


					                );
					                
					                if($address1 != ''){
					                    $transaction['ADDRESS'] = $address1;
					                }
					                if($city != ''){
					                    $transaction['CITY'] = $city;
					                }
					                
					                if($zipcode != ''){
					                    $transaction['ZIP_CODE'] = $zipcode;
					                }
					                if($first_name != ''){
					                    $transaction['FIRST_NAME'] = $first_name;
					                }
					                if($last_name != ''){
					                    $transaction['LAST_NAME'] = $last_name;
					                }
					               
					                $gatewayTransaction              = new EPX();
					                $result = $gatewayTransaction->processTransaction($transaction);

	            					if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
	            					{	

	            						$transactionid = $transactionID = $result['AUTH_GUID'];
				            			$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
										$condition  = $emailCondition;

										$this->general_model->update_row_data($invoiceTable, $condition, $data);

										$totalProcessdInvoice = $totalProcessdInvoice + 1;
										if($integrationType == 1){
		        					 		$qbListTxnID = $this->QBOSYNC($transactionid,$customerID,$amount,$user_id,$invoiceID);
		        					 	}
										$responsetext = 'SUCCESS';
										$response_code = 100;
				            		}else{
				            			$responsetext = $result['AUTH_RESP_TEXT'];
				            			$response_code = 401;
				            			$transactionid = 'TXNFAILED'.time();
				            			
				            		}
				            		$gatewayID = $gt_resultEC['gatewayID'];
				            		$gatewayType = $gt_resultEC['gatewayType'];
				            		$gatewayName = EPXGatewayName.' ECheck';
				            		$type = 'sale';
				            		$resellerID = $merchant_data['resellerID'];
				            		$tRowID = $this->transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType,$qbListTxnID,$custom_data_fields);

				            	}  else if($gt_resultCC['gatewayType'] == 17){
									// Maverick Gateway Begins
									
									$this->load->config('maverick');
									$this->load->library('MaverickGateway');

									// Maverick Payment
									$this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
									$this->maverickgateway->setAccessToken($gt_resultCC['gatewayUsername']);
									$this->maverickgateway->setTerminalId($gt_resultCC['gatewayPassword']);

									if($sch_method == 1) {
										
										$gatewayName = MaverickGatewayName;

										// Maverick Credit Card Begins
										if (strlen($expmonth) > 1 && $expmonth <= 9) {
											$expmonth = substr($expmonth, 1);
										}
										$expry = $expmonth . '/' . substr($exyear, 2);

										// Sale Payload
										$request_payload = [
											'level' => 1,
											'threeds' => [
												'id' => null,
											],
											'amount' => $amount,
											'card' => [
												'number' => $card_no,
												'cvv'    => '',
												'exp'    => $expry,
												'save'   => 'No',
												'address' => [
													'street' => $address1,
													'city' => $city,
													'state' => $state,
													'country' => $country,
													'zip' => $zipcode,
												]
											],
											'contact' => [
												'name'   => $name,
												'email'  => $email,
												'phone' => $phone,
											]
										];

										// Process Sale
										$r = $this->maverickgateway->processSale($request_payload);

										$result = [];

										$rbody = json_decode($r['response_body'], true);

										$result['response_code'] = $r['response_code'];

										$result['data'] = $rbody;

										if($r['response_code'] >= 200 && $r['response_code'] < 300) {
											if(($rbody['status']['status'] == 'Approved') || (isset($rbody['id']) && $rbody['id']))
											{
												$result['data'] = $rbody;
												$result['status'] = 'success';
												$result['msg'] = $result['message'] = 'Payment success.';
											} else {
												$result['status'] = 'failed';
												$result['msg'] = $result['message'] = 'Payment failed.';    
											}
										} else {
											$result['status'] = 'failed';
											$result['msg'] = $result['message'] = $rbody['message'];
										}

										if ($result['status'] == 'success') {
											$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);
	
											$totalProcessdInvoice = $totalProcessdInvoice + 1;
	
											$trID =   $result['data']['id'];
											$responsetext = 'SUCCESS';
											$code =   '200';
	
											if($integrationType == 1){
												 $this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
											}
										}else{
											$trID = 'TXNFAILED'.time();
											$responsetext  = $result['message'];
											$code =   $result['response_code'];
										}

										$transactionid = $trID;
										$responsetext = $responsetext;
										$response_code = $code;
										$type = 'sale';
										
										
										$resellerID = $merchant_data['resellerID'];
										if($transactionid == ''){
											$transactionid = 'TXNFAILED'.time();
										}
	
										$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
										// Maverick Credit Card Ends
									} else {
										$gatewayName = MaverickGatewayName.' ECheck';

										// Maverick eCheck Begins
										// ACH
										// get DBA id
										$dbaId = $this->maverickgateway->getDba(true);

										// electronic Sale
										$request_payload = [
												'amount'          => $amount,
												'routingNumber' => $c,
												'accountName'     => $accountName,
												'accountNumber'   => $accountNumber,
												'accountType'     => ucwords($accountType), // Checking or Savings
												'transactionType' => 'Debit', // Debit or Credit
												'customer' => [
													'email'     => $email,
													'firstName' => $name,
													'lastName'  => $last_name,
													"address1" => $address1,
													"address_2" => $address2,
													"city" => $city,
													"state" => $state,
													"zipCode" => $zipcode,
													"country" => $country
												],
												'dba' => [
													'id' => $dbaId,
												],
											];
										// Process ACH
										$r = $this->maverickgateway->processAch($request_payload);

										$result = [];

										$rbody = json_decode($r['response_body'], true);

										$result['response_code'] = $r['response_code'];

										$result['data'] = $rbody;

										if($r['response_code'] >= 200 && $r['response_code'] < 300) {
											if(isset($rbody['id']) && $rbody['id'])
											{
												$result['status'] = 'success';
												$result['msg'] = $result['message'] = 'Payment success.';
											} else {
												$result['status'] = 'failed';
												$result['msg'] = $result['message'] = 'Payment failed.';    
											}
										} else {
											$result['status'] = 'failed';
											$result['msg'] = $result['message'] = $rbody['message'];
										}
										
										if ($result['status'] == 'success') {
											$data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($amount), 'BalanceRemaining' => 0);
											$condition  = $emailCondition;
											$this->general_model->update_row_data($invoiceTable, $condition, $data);
	
											$totalProcessdInvoice = $totalProcessdInvoice + 1;
	
											$trID =   $result['data']['id'];											  $responsetext = 'SUCCESS';
											$code =   '200';
	
											if($integrationType == 1){
												$this->QBOSYNC($trID,$customerID,$amount,$user_id,$invoiceID);
											}
										}else{
											$trID = 'TXNFAILED'.time();
											$responsetext  = $result['message'];
											$code =   '401';
										}
										$transactionid = $trID;

										$responsetext = $responsetext;
										$response_code = $code;

										$type = 'sale';

										$resellerID = $merchant_data['resellerID'];
	
										$tRowID = $this->transactionSaved($transactionid,$responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
										// Maverick eCheck Ends
									}
									
									// Maverick Gateway Ends
								}else{

				            	}

				            	

				            }

			            }

			            if($transactionid == ''){
			            	$tRowID = $this->transactionSaved('','Missing Billing Information','400','sale',$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);
			            }

			        }else{

			        	$tRowID = $this->transactionSaved('','Missing Billing Information','400','sale',$gatewayID,$gatewayType,$customerID,$amount,$user_id,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields);

			        }

			        $receipt_data[] = array(
						'invoice_id'      => $invoiceID,
						'customerID' => $customerID,
						'transactionID' => $transactionid,
						'transactionRowID' => $tRowID
					);
					//print_r($receipt_data);die;
					
	        		
	        	}

	        	$remainingInvoice = $totalProcessInvoice - $totalProcessdInvoice;
	        	$receiptObj['data'] = $receipt_data;
	        	$receiptObj['remainingInvoice'] = $remainingInvoice;
	        	$receiptObj['integrationType'] = $integrationType;
	        	if($remainingInvoice > 0){
	        	}else{
	        	}
	        	
				$this->session->set_userdata("batch_process_receipt_data", $receiptObj);

	        	if($integrationType == 1){
	        		redirect('QBO_controllers/home/batchReciept/', 'refresh');
	        	}else if($integrationType == 2){
	        		redirect('home/batchReciept/', 'refresh');
	        	}else if($integrationType == 3){
	        		redirect('FreshBooks_controllers/home/batchReciept/', 'refresh');
	        	}else if($integrationType == 4){
	        		redirect('Integration/home/batchReciept/', 'refresh');
	        	}else if($integrationType == 5){
	        		redirect('company/home/batchReciept/', 'refresh');
	        	}
	        }
		}

	}
	
	public function safe_encode($string)
    {
        return strtr(base64_encode($string), '+/=', '-_-');
    }

    public function transactionSaved($transactionid, $responsetext,$response_code,$type,$gatewayID,$gatewayType,$customerID,$totalamount,$merchantID,$resellerID,$gatewayName,$invoiceID,$integrationType, $qbListTxnID,$custom_data_fields)
    {

    	$transactiondata                        = array();
		$transactiondata['transactionID']       = $transactionid;
		$transactiondata['transactionStatus']   = $responsetext;
		$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
		$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
		$transactiondata['transactionCode']     = $response_code;
		$transactiondata['qbListTxnID']       	= $qbListTxnID;
		$transactiondata['transactionType']    = $type;
		$transactiondata['gatewayID']          = $gatewayID;
		$transactiondata['transactionGateway'] = $gatewayType;
		$transactiondata['customerListID']     = $customerID;
		$transactiondata['transactionAmount']  = $totalamount;
		$transactiondata['merchantID']         = $merchantID;
		$transactiondata['resellerID']         = $resellerID;
		$transactiondata['gateway']            = $gatewayName;

		$res = $this->db->query('Select appIntegration from app_integration_setting where merchantID="'.$merchantID.'" ')->row_array();
		$service = $res['appIntegration'];
		  if($service==2 || $service==5)
			$transactiondata['invoiceTxnID']     = $invoiceID; 
		  else 	
			$transactiondata['invoiceID']        = $invoiceID; 

		$transactiondata = alterTransactionCode($transactiondata);

		$transactiondata['transaction_by_user_type'] = 1;
		$transactiondata['transaction_by_user_id'] = $merchantID;

		$callCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
		if($custom_data_fields){
            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
        }
		$trID = $this->general_model->insert_row('customer_transaction',$transactiondata);


		
		if($integrationType == 2 && !is_numeric($invoiceID)){
			$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
			$user      = $comp_data['qbwc_username'];
			$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $trID, '1', '', $user);
		}
		

        return $trID;
    }

    public function QBOSYNC($trID,$customerID,$totalamount,$merchantID,$invoiceID)
    {

    	$val = array(
			'merchantID' => $merchantID,
		);
		$data = $this->general_model->get_row_data('QBO_token',$val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
		$dataService = DataService::Configure(array(
    			 'auth_mode' => $this->config->item('AuthMode'),
				 'ClientID'  => $this->config->item('client_id'),
				 'ClientSecret' =>$this->config->item('client_secret'),
				 'accessTokenKey' =>  $accessToken,
				 'refreshTokenKey' => $refreshToken,
				 'QBORealmID' => $realmID,
				 'baseUrl' => $this->config->item('QBOURL'),
		));
		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

		$invse              = $invoiceID;
        $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $invse . "'");
    	            
    	$crtxnID = '';
    	$st     = '0';
    	$action = 'Pay Invoice';
    	$msg    = "Payment Success but not sysnc";

    	if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        {
			$theInvoice = current($targetInvoiceArray);
		

			$con = array('invoiceID' => $invse, 'merchantID' => $merchantID);
	        $res = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment'), $con);

	        $pay_amounts = $res['Total_payment'];
            $amount_data = $res['BalanceRemaining'];
            $refNum[]    = $res['refNumber'];

			$newPaymentObj = Payment::create([
			    "TotalAmt" => $totalamount,
			    "SyncToken" => 1, 
			    "CustomerRef" => $customerID,
			    "Line" => [
			     "LinkedTxn" =>[
			            "TxnId" => $invoiceID,
			            "TxnType" => "Invoice",
			        ],    
			       "Amount" => $totalamount
		        ]
			    ]);
			$savedPayment = $dataService->Add($newPaymentObj);
			$error = $dataService->getLastError();

			
		
			if ($error != null) {
			  	$st     = '0';
				$crtxnID = '';
				$msg    = "Payment Success but " . $error->getResponseBody();
			}
			else {
				$st     = '1';
			    $crtxnID =	$savedPayment->Id;
			    $msg    = "Payment Success";
			    
			}
		}
		$qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $trID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

        $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
        return $crtxnID;
    }
}