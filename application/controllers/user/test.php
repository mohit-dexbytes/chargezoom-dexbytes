<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use GlobalPayments\Api\Entities\Address;

require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use iTransact\iTransactSDK\iTTransaction;

class Test extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('general_model');

    }

    public function index()
    {

        $crl = curl_init();

        $headr   = array();
        $headr[] = 'Content-length: 0';
        $headr[] = 'Content-type: application/json';
        $headr[] = 'Authorization: Bearer QYABNV8EM4FXTFSG7EP9KAE28EKQVFD2';

        curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_URL, 'https://developers.tsys.com/sandbox/issuing/account/887000003195');
        $rest = curl_exec($crl);
        print_r($rest);
        curl_close($crl);

        print_r($rest);

        die;

        $soapURL = "http://stagegw.transnox.com:6012/servlets/TransNox_API_Server";

        try
        {

           
            $soapclient = new SoapClient('http://stagegw.transnox.com:6012/servlets/TransNox_API_Server/transportService.asmx?wsdl');
            
            $param = array('Sale' => array("deviceID" => "88700000319501",
                "transactionKey"                          => "QYABNV8EM4FXTFSG7EP9KAE28EKQVFD2",
                "cardDataSource"                          => "MANUAL",
                "transactionAmount"                       => "12",
                "cardNumber"                              => "4242424242424242",
                "expirationDate"                          => "1225",

                "cardHolderName"                          => "Jhon Dev"));

            $param1 = json_encode($param);

            $response = $soapclient->CreateTransaction($param1);
            echo "<pre>";
            print_r($response);
            echo '<br><br><br>';
            $array = json_decode(json_encode($response), true);
            echo "<pre>";
            print_r($array);
            echo '<br><br><br>';
            echo '<br><br><br>';
            foreach ($array as $item) {
                echo '<pre>';
                var_dump($item);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

      

    }

    public function saleNMI()
    {
        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/nmiCustomerVault.class.php';

        $nmi_data    = array('nmi_user' => 'demo', 'nmi_password' => 'password');
        $transaction = new nmiCustomerVault($nmi_data);
        $transaction->setCcNumber('4111111111111111');
        $expmonth = '12'; 
        $exyear  = '2021'; 
        $exyear1 = substr($exyear, 2);
        $expry   = $expmonth . $exyear1;
        $transaction->setCcExp($expry);
        $transaction->setCvv('123');
        $transaction->addAndCharge('100');
        $result = $transaction->execute();

        echo '<pre>';
        print_r($result);die;
    }

    public function salecustomerIdNMI()
    {
        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/nmiCustomerVault.class.php';

        $nmi_data = array('nmi_user' => 'demo', 'nmi_password' => 'password');

        $transaction = new nmiCustomerVault($nmi_data);
        $transaction->setCustomerVaultId('1084772025');
        $transaction->charge('100');
        $result = $transaction->execute();

        echo '<pre>';
        print_r($result);die;
    }

    public function authNMI()
    {
        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/nmiCustomerVault.class.php';

        $nmi_data    = array('nmi_user' => 'demo', 'nmi_password' => 'password');
        $transaction = new nmiCustomerVault($nmi_data);
        $transaction->setCcNumber('4111111111111111');
        $expmonth = '12';

        $exyear  = '2021';
        $exyear1 = substr($exyear, 2);
        $expry   = $expmonth . $exyear1;
        $transaction->setCcExp($expry);
        $transaction->setCvv('123');
        $transaction->addAndAuth('100');
        $result = $transaction->execute();

        echo '<pre>';
        print_r($result);die;
    }

    public function createAndSalePaytrace()
    {
        include APPPATH . 'third_party/PayTraceAPINEW.php';
        $this->load->config('paytrace');

        $payAPI            = new PayTraceAPINEW();
        $oauth_result      = $payAPI->oAuthTokenGenerator('password', 'mohit.dexbytes@gmail.com', 'Mkg@1234');
        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

        if (!$oauth_moveforward) {
            $json        = $payAPI->jsonDecode($oauth_result['temp_json_response']);
            $oauth_token = sprintf("Bearer %s", $json['access_token']);

            $time = 'm1_' . time();

            $request_data = array(
                "credit_card"     => array(
                    "number"           => '4012000098765439',
                    "expiration_month" => '01',
                    "expiration_year"  => '2021',
                ),
                "csc"             => '999',
                "customer_id"     => $time,
                'integrator_id'   => '916568GLn3E3',
                "billing_address" => array(
                    "name"           => 'Mohit',
                    "street_address" => '12th strt Sector 9',
                    "city"           => 'Los angelish',
                    "state"          => 'CA',
                    "zip"            => '74026',
                ),
            );
            $request_data = json_encode($request_data);
            $result2      = $payAPI->processTransaction($oauth_token, $request_data, URL_CREATE_CUSTOMER);

            $result3 = $payAPI->jsonDecode($result2['temp_json_response']);

            $request_data = array(
                "amount"          => '100',
                "customer_id"     => $time,
                'integrator_id'   => '916568GLn3E3',
                "billing_address" => array(
                    "name"           => 'Mohit',
                    "street_address" => '12th strt Sector 9',
                    "city"           => 'Los angelish',
                    "state"          => 'CA',
                    "zip"            => '74026',
                ),
            );
            $request_data = json_encode($request_data);
            $result1      = $payAPI->processTransaction($oauth_token, $request_data, URL_KEYED_SALE);

            $result = $payAPI->jsonDecode($result1['temp_json_response']);

            echo '<pre>';
            print_r([
                $result,
                $result1,
                $result2,
                $result3,
            ]);die;
        }

        echo '<pre>';
        print_r($result);die;
    }

    public function salecustomerIdPaytrace()
    {
        include APPPATH . 'third_party/PayTraceAPINEW.php';
        $this->load->config('paytrace');

        $payAPI            = new PayTraceAPINEW();
        $oauth_result      = $payAPI->oAuthTokenGenerator('password', 'mohit.dexbytes@gmail.com', 'Mkg@1234');
        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

        if (!$oauth_moveforward) {
            $json        = $payAPI->jsonDecode($oauth_result['temp_json_response']);
            $oauth_token = sprintf("Bearer %s", $json['access_token']);

            $request_data = array(
                "amount"          => '100',
                "customer_id"     => "1604304781",
                'integrator_id'   => '916568GLn3E3',
                "billing_address" => array(
                    "name"           => 'Mohit',
                    "street_address" => '12th strt Sector 9',
                    "city"           => 'Los angelish',
                    "state"          => 'CA',
                    "zip"            => '74026',
                ),
            );
            $request_data = json_encode($request_data);
            $result1      = $payAPI->processTransaction($oauth_token, $request_data, URL_KEYED_SALE);

            $result = $payAPI->jsonDecode($result1['temp_json_response']);

            echo '<pre>';
            print_r([
                $result,
                $result1,
            ]);die;
        }

        echo '<pre>';
        print_r($result);die;
    }

    public function createCustomerPaytrace()
    {
        include APPPATH . 'third_party/PayTraceAPINEW.php';
        $this->load->config('paytrace');

        $payAPI            = new PayTraceAPINEW();
        $oauth_result      = $payAPI->oAuthTokenGenerator('password', 'mohit.dexbytes@gmail.com', 'Mkg@1234');
        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

        if (!$oauth_moveforward) {
            $json        = $payAPI->jsonDecode($oauth_result['temp_json_response']);
            $oauth_token = sprintf("Bearer %s", $json['access_token']);

            $time = 'm1_' . time();

            $request_data = array(
                "credit_card"     => array(
                    "number"           => '4012000098765439',
                    "expiration_month" => '01',
                    "expiration_year"  => '2021',
                ),
                "csc"             => '999',
                "customer_id"     => $time,
                'integrator_id'   => '916568GLn3E3',
                "billing_address" => array(
                    "name"           => 'Mohit',
                    "street_address" => '12th strt Sector 9',
                    "city"           => 'Los angelish',
                    "state"          => 'CA',
                    "zip"            => '74026',
                ),
            );
            $request_data = json_encode($request_data);
            $result2      = $payAPI->processTransaction($oauth_token, $request_data, URL_CREATE_CUSTOMER);

            $result3 = $payAPI->jsonDecode($result2['temp_json_response']);

            echo '<pre>';
            print_r([
                $result2,
                $result3,
            ]);die;
        }

        echo '<pre>';
        print_r($result);die;
    }

    public function authcustomerIdPaytrace()
    {
        include APPPATH . 'third_party/PayTraceAPINEW.php';
        $this->load->config('paytrace');

        $payAPI            = new PayTraceAPINEW();
        $oauth_result      = $payAPI->oAuthTokenGenerator('password', 'mohit.dexbytes@gmail.com', 'Mkg@1234');
        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

        if (!$oauth_moveforward) {
            $json        = $payAPI->jsonDecode($oauth_result['temp_json_response']);
            $oauth_token = sprintf("Bearer %s", $json['access_token']);

            $request_data = array(
                "amount"          => '100',
                "customer_id"     => "1604304781",
                'integrator_id'   => '916568GLn3E3',
                "billing_address" => array(
                    "name"           => 'Mohit',
                    "street_address" => '12th strt Sector 9',
                    "city"           => 'Los angelish',
                    "state"          => 'CA',
                    "zip"            => '74026',
                ),
            );
            $request_data = json_encode($request_data);
            $result1      = $payAPI->processTransaction($oauth_token, $request_data, URL_KEYED_AUTHORIZATION);

            $result = $payAPI->jsonDecode($result1['temp_json_response']);

            echo '<pre>';
            print_r([
                $result,
                $result1,
            ]);die;
        }

        echo '<pre>';
        print_r($result);die;
    }

    public function createAndSaleiTransact()
    {

        $request_data = array(
            "card"    => array(
                "name"      => 'Mohit',
                "number"    => '4111111111111111',
                "exp_month" => '12',
                "exp_year"  => '2021',
                "cvv"       => '123',
            ),
            "address" => array(
                "line1"       => 'indore',
                "line2"       => 'indore',
                "city"        => 'indore',
                "state"       => 'MP',
                "postal_code" => '452001',
            ),
        );

        $sdk    = new iTTransaction();
        $result = $sdk->postCreateCustomer('chargezoom_sandbox_acct_falywa', 'laltyspiquazothy', $request_data);

        $request_data = array(
            "amount"            => (100 * 100),
            "customer_id"       => $result['id'],
            "payment_source_id" => $result['payment_sources'][0]['id'],
            "billing_address"   => array(
                "line1"       => 'indore',
                "line2"       => 'indore',
                "city"        => 'indore',
                "state"       => 'MP',
                "postal_code" => '452001',
            ),
            'capture'           => false,
        );

        $result1 = $sdk->postCardTransaction('chargezoom_sandbox_acct_falywa', 'laltyspiquazothy', $request_data);

        echo '<pre>';
        print_r([$result, $result1]);die;
    }

    public function authcustomerIdiTransact()
    {
        $request_data = array(
            "amount"            => (100 * 100),
            "customer_id"       => 'cus_cPUmpeccWkvi0FToGvYHfQ',
            "payment_source_id" => 'src_orPDU_6eWIB0gmLbYz6xnQ',
            "billing_address"   => array(
                "line1"       => 'indore',
                "line2"       => 'indore',
                "city"        => 'indore',
                "state"       => 'MP',
                "postal_code" => '452001',
            ),
            'capture'           => false,
        );

        $sdk     = new iTTransaction();
        $result1 = $sdk->postCardTransaction('chargezoom_sandbox_acct_falywa', 'laltyspiquazothy', $request_data);

        echo '<pre>';
        print_r($result1);die;
    }

    # Old Scripts
    
    
    #USAePay
    public function saleUSAePay()
    {
        require_once APPPATH . "third_party/usaepay/usaepay.php";
        $this->load->config('usaePay');

        $invNo                            = time();
        $transaction                      = new umTransaction;
        $transaction->ignoresslcerterrors = ($this->config->item('ignoresslcerterrors') !== null) ? $this->config->item('ignoresslcerterrors') : true;
        $transaction->key                 = '_fUz898UBxsm7SxwmAwOI7u861JHXAdm';
        $transaction->pin                 = '123456';
        $transaction->usesandbox          = $this->config->item('Sandbox');

        $transaction->invoice     = $invNo; // invoice number.  must be unique.
        $transaction->description = "Chargezoom Invoice Payment"; // description of charge
        $transaction->testmode    = $this->config->item('TESTMODE'); // Change this to 0 for the transaction to process
        $transaction->command     = "sale";

        $transaction->card = '4000100011112224';
        $transaction->exp  = '0921';

        $transaction->save_card   = true;
        $transaction->addcustomer = true;

        $transaction->amount = 54;
        $transaction->Process();

        echo '<pre>';
        print_r($transaction);die;
    }

    #Authorize.net
    public function saleAuthGateway()
    {
        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

        $transaction = new AuthorizeNetAIM('9mCupr2vP86', '6Nx749d7X8mHVbuT');
        $result      = $transaction->authorizeAndCapture('100', '4111111111111111', '0121');
        echo '<pre>';
        print_r($result);die;
    }

    public function salecustomerIdAuthGateway()
    {
        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

        $transaction = new AuthorizeNetAIM('9mCupr2vP86', '6Nx749d7X8mHVbuT');
        $transaction->__set('cust_id', 'aaaaaa');

        $result = $transaction->authorizeAndCapture(100);
        echo '<pre>';
        print_r($result);die;
    }

    #CyberSource
    public function saleCYBS()
    {
        $this->load->config('cyber_pay');

        $this->mName = 'Mohit';
        $amount      = 52;

        $option               = array();
        $option['merchantID'] = 'chargezoom'; 
        $option['apiKey']     = '36ff71a5f081e38053d92cf17cec372a994f9ec6'; 
        $option['secretKey']  = 'Qfq3nQFsPDruUY8GgQXppv9ax9Ctkbgz8CE6sS6SjxwZ2eWmTGRZ2/RrZ56LeChCoXZpB9y0VvcUz4V2IBafd+l5o/r1CarYFiId+y7hfi35xjMa+VTLMGQtghCfNWXynwgOgbbDwbJDosbI0Wy7vVJ0JxT/xQWnDVCt1sKHePShYpyaOQoFreJfGEl5BephYuPPlBfp+2dEHWBlcuyFVct65IjTHIV/ErVEhvnX5UpzNCaE0X+z26n4pyoqLHgTQ+rz8903RkF6COiq+jNJnBRoCSbqwD0PPkfxhanPdzdErFghDNs7IaXL39CjVG01z3LmDyRNeMt8TVwiwQb/+A=='; 

        if ($this->config->item('Sandbox')) {
            $env = $this->config->item('SandboxENV');
        } else {
            $env = $this->config->item('ProductionENV');
        }

        $option['runENV'] = $env;

        $commonElement  = new CyberSource\ExternalConfiguration($option);
        $config         = $commonElement->ConnectionHost();
        $merchantConfig = $commonElement->merchantConfigObject();
        $apiclient      = new CyberSource\ApiClient($config, $merchantConfig);
        $api_instance   = new CyberSource\Api\PaymentsApi($apiclient);

        $cliRefInfoArr = [
            "code" => $this->mName,
        ];
        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);

        $processingInformationArr = [
            "capture" => true, "commerceIndicator" => "internet",
        ];

        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);

        $amountDetailsArr = [
            "totalAmount" => $amount,
            "currency"    => CURRENCY,
        ];
        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

        $billtoArr = [
            "firstName"          => 'Indore',
            "lastName"           => 'Indore',
            "address1"           => 'Indore',
            "postalCode"         => '452001',
            "locality"           => 'Indore',
            "administrativeArea" => 'MP',
            "country"            => 'IN',
            "phoneNumber"        => '',
            "company"            => 'Indore',
            "email"              => 'mohit@gmail.com',
        ];
        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);

        $orderInfoArr = [
            "amountDetails" => $amountDetInfo,
            "billTo"        => $billto,
        ];
        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);

        $paymentCardInfo = [
            "expirationYear"  => '2021',
            "number"          => '5454545454545454',
            // "securityCode" => '',
            "expirationMonth" => '12',
        ];
        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);

        $paymentInfoArr = [
            "card" => $card,
        ];
        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);

        $paymentRequestArr = [
            "clientReferenceInformation" => $client_reference_information,
            "orderInformation"           => $order_information,
            "paymentInformation"         => $payment_information,
            "processingInformation"      => $processingInformation,
        ];
        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);

        $api_response = list($response, $statusCode, $httpHeader) = null;
        $type         = 'Sale';

        echo '<pre>';
        try
        {
            //Calling the Api
            $api_response = $api_instance->createPayment($paymentRequest);
            $trID         = $api_response[0]['id'];
            $msg          = $api_response[0]['status'];
            print_r([$trID, $msg]);
        } catch (Cybersource\ApiException $e) {
            print_r($e->getMessage());
        }
        die;
    }

    public function authCYBS()
    {
        $this->load->config('cyber_pay');

        $this->mName = 'Mohit';
        $amount      = 52;

        $option               = array();
        $option['merchantID'] = 'chargezoom'; 
        $option['apiKey']     = '36ff71a5f081e38053d92cf17cec372a994f9ec6'; 
        $option['secretKey']  = 'Qfq3nQFsPDruUY8GgQXppv9ax9Ctkbgz8CE6sS6SjxwZ2eWmTGRZ2/RrZ56LeChCoXZpB9y0VvcUz4V2IBafd+l5o/r1CarYFiId+y7hfi35xjMa+VTLMGQtghCfNWXynwgOgbbDwbJDosbI0Wy7vVJ0JxT/xQWnDVCt1sKHePShYpyaOQoFreJfGEl5BephYuPPlBfp+2dEHWBlcuyFVct65IjTHIV/ErVEhvnX5UpzNCaE0X+z26n4pyoqLHgTQ+rz8903RkF6COiq+jNJnBRoCSbqwD0PPkfxhanPdzdErFghDNs7IaXL39CjVG01z3LmDyRNeMt8TVwiwQb/+A=='; 

        if ($this->config->item('Sandbox')) {
            $env = $this->config->item('SandboxENV');
        } else {
            $env = $this->config->item('ProductionENV');
        }

        $option['runENV'] = $env;

        $commonElement  = new CyberSource\ExternalConfiguration($option);
        $config         = $commonElement->ConnectionHost();
        $merchantConfig = $commonElement->merchantConfigObject();
        $apiclient      = new CyberSource\ApiClient($config, $merchantConfig);
        $api_instance   = new CyberSource\Api\PaymentsApi($apiclient);

        $cliRefInfoArr = [
            "code" => $this->mName,
        ];
        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);

        $processingInformationArr = [
            "commerceIndicator" => "internet",
        ];

        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);

        $amountDetailsArr = [
            "totalAmount" => $amount,
            "currency"    => CURRENCY,
        ];
        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

        $billtoArr = [
            "firstName"          => 'Indore',
            "lastName"           => 'Indore',
            "address1"           => 'Indore',
            "postalCode"         => '452001',
            "locality"           => 'Indore',
            "administrativeArea" => 'MP',
            "country"            => 'IN',
            "phoneNumber"        => '',
            "company"            => 'Indore',
            "email"              => 'mohit@gmail.com',
        ];
        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);

        $orderInfoArr = [
            "amountDetails" => $amountDetInfo,
            "billTo"        => $billto,
        ];
        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);

        $paymentCardInfo = [
            "expirationYear"  => '2021',
            "number"          => '5454545454545454',
            // "securityCode" => '',
            "expirationMonth" => '12',
        ];
        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);

        $paymentInfoArr = [
            "card" => $card,
        ];
        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);

        $paymentRequestArr = [
            "clientReferenceInformation" => $client_reference_information,
            "orderInformation"           => $order_information,
            "paymentInformation"         => $payment_information,
            "processingInformation"      => $processingInformation,
        ];
        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);

        $api_response = list($response, $statusCode, $httpHeader) = null;
        $type         = 'Sale';

        echo '<pre>';
        try
        {
            //Calling the Api
            $api_response = $api_instance->createPayment($paymentRequest);
            $trID         = $api_response[0]['id'];
            $msg          = $api_response[0]['status'];
            print_r([$trID, $msg]);
        } catch (Cybersource\ApiException $e) {
            print_r($e->getMessage());
        }
        die;
    }

    #Heartland
    public function saleHeartland()
    {
        $this->load->config('globalpayments');

        $config = new PorticoConfig();

        $config->secretApiKey = 'skapi_cert_MdYrAgAeomEAM07AAe7vnrMnbePwCFgAcse3_zt85A';
        $config->serviceUrl   = $this->config->item('GLOBAL_URL');
        ServicesContainer::configureService($config);

        $card           = new CreditCardData();
        $card->number   = '5454545454545454';
        $card->expMonth = '01';
        $card->expYear  = '2021';

        $address                 = new Address();
        $address->streetAddress1 = 'Indore';
        $address->city           = 'Indore';
        $address->state          = 'MP';
        $address->postalCode     = '452001';
        $address->country        = 'IN';

        $invNo = time();

        try {

            $response = $card->charge(120)
                ->withCurrency('USD')
                ->withAddress($address)
                ->withInvoiceNumber($invNo)
                ->withAllowDuplicates(true)
                ->execute();

            echo '<pre>';
            print_r($response);die;
        } catch (BuilderException $e) {
            $error = 'Build Exception Failure: ' . $e->getMessage();
        } catch (ConfigurationException $e) {
            $error = 'ConfigurationException Failure: ' . $e->getMessage();
        } catch (GatewayException $e) {
            $error = 'GatewayException Failure: ' . $e->getMessage();
        } catch (UnsupportedTransactionException $e) {
            $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
        } catch (ApiException $e) {
            $error = ' ApiException Failure: ' . $e->getMessage();
        }

        echo '<pre>';
        print_r($error);die;
    }

    public function authHeartland()
    {
        $this->load->config('globalpayments');

        $config = new PorticoConfig();

        $config->secretApiKey = 'skapi_cert_MdYrAgAeomEAM07AAe7vnrMnbePwCFgAcse3_zt85A';
        $config->serviceUrl   = $this->config->item('GLOBAL_URL');
        ServicesContainer::configureService($config);

        $card           = new CreditCardData();
        $card->number   = '5454545454545454';
        $card->expMonth = '01';
        $card->expYear  = '2021';

        $address                 = new Address();
        $address->streetAddress1 = 'Indore';
        $address->city           = 'Indore';
        $address->state          = 'MP';
        $address->postalCode     = '452001';
        $address->country        = 'IN';

        $invNo = time();

        try {

            $response = $card->authorize(120)
                ->withCurrency('USD')
                ->withAddress($address)
                ->withInvoiceNumber($invNo)
                ->withAllowDuplicates(true)
                ->execute();

            echo '<pre>';
            print_r($response);die;
        } catch (BuilderException $e) {
            $error = 'Build Exception Failure: ' . $e->getMessage();
        } catch (ConfigurationException $e) {
            $error = 'ConfigurationException Failure: ' . $e->getMessage();
        } catch (GatewayException $e) {
            $error = 'GatewayException Failure: ' . $e->getMessage();
        } catch (UnsupportedTransactionException $e) {
            $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
        } catch (ApiException $e) {
            $error = ' ApiException Failure: ' . $e->getMessage();
        }

        echo '<pre>';
        print_r($error);die;
    }
}
