<?php
require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
class Sample extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    private $confiData;
    private $loginInfo;
    public function __construct()
    {
        parent::__construct();
        $this->load->config('xero');
        $this->load->model('general_model');
        $this->load->library('form_validation');

        $logged_in_data          = $this->session->userdata('logged_in');
        $this->resellerID        = $logged_in_data['resellerID'];
        $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        $merchID                 = $logged_in_data['merchID'];

        $this->loginInfo  = $logged_in_data;
        $this->merchantID = $merchID;
        $this->confiData  = [
            'urlAuthorize'            => $this->config->item('urlAuthorize'),
            'urlAccessToken'          => $this->config->item('urlAccessToken'),
            'urlResourceOwnerDetails' => $this->config->item('urlResourceOwnerDetails'),
            'clientId'                => $this->config->item('clientId'),
            'clientSecret'            => $this->config->item('urlResourceOwnerDetails'),
            'redirectUri'             => $this->config->item('redirectUri'),
        ];
    }

    public function index()
    {
        redirect('login', 'refresh');
    }

    public function syncContact()
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $if_modified_since = ''; // \DateTime | Only records created or modified since this timestamp will be returned
        $where             = ''; // string | Filter by an any element
        $order             = 'Name ASC'; // string | Order by an any element
        $i_ds              = ''; // string[] | Filter by a comma separated list of ContactIDs. Allows you to retrieve a specific set of contacts in a single call.
        $page              = 1; // int | e.g. page=1 - Up to 100 contacts will be returned in a single API call.
        $include_archived  = true; // bool | e.g. includeArchived=true - Contacts with a status of ARCHIVED will be included in the response
        $summary_only      = false; // bool | Use summaryOnly=true in GET Contacts endpoint to retrieve a smaller version of the response object. This returns only lightweight fields, excluding computation-heavy fields from the response, making the API calls quick and efficient.

        $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        $apiResponse = $accountingApi->getContacts($tok_data['tenant_id'], $if_modified_since, $where, $order, $i_ds, $page, $include_archived, $summary_only);

        echo '<pre>';
        print_r($apiResponse->getContacts());die;
    }

    public function getContact($contact_id)
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        try {
            $result = $apiInstance->getContact($tok_data['tenant_id'], $contact_id);
            echo '<pre>';
            print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling AccountingApi->getContact: ', $e->getMessage(), PHP_EOL;
        }
        die;
    }

    public function createContact()
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        $contacts = '{ "Id": "e997d6d7-6dad-4458-beb8-d9c1bf7f2edf", "Status": "OK", "ProviderName": "Xero API Partner", "DateTimeUTC": "/Date(1551399321121)/", "Contacts": [ { "ContactID": "3ff6d40c-af9a-40a3-89ce-3c1556a25591", "ContactStatus": "ACTIVE", "Name": "Foo9987", "EmailAddress": "sid32476@blah.com", "BankAccountDetails": "", "Addresses": [ { "AddressType": "STREET", "City": "", "Region": "", "PostalCode": "", "Country": "" }, { "AddressType": "POBOX", "City": "", "Region": "", "PostalCode": "", "Country": "" } ], "Phones": [ { "PhoneType": "DEFAULT", "PhoneNumber": "", "PhoneAreaCode": "", "PhoneCountryCode": "" }, { "PhoneType": "DDI", "PhoneNumber": "", "PhoneAreaCode": "", "PhoneCountryCode": "" }, { "PhoneType": "FAX", "PhoneNumber": "", "PhoneAreaCode": "", "PhoneCountryCode": "" }, { "PhoneType": "MOBILE", "PhoneNumber": "555-1212", "PhoneAreaCode": "415", "PhoneCountryCode": "" } ], "UpdatedDateUTC": "/Date(1551399321043+0000)/", "ContactGroups": [], "IsSupplier": false, "IsCustomer": false, "SalesTrackingCategories": [], "PurchasesTrackingCategories": [], "PaymentTerms": { "Bills": { "Day": 15, "Type": "OFCURRENTMONTH" }, "Sales": { "Day": 10, "Type": "DAYSAFTERBILLMONTH" } }, "ContactPersons": [], "HasValidationErrors": false } ] }'; 

        $summarize_errors = true;
        try {
            $result = $apiInstance->createContacts($tok_data['tenant_id'], ["Name"=> "Foo9987", "EmailAddress"=> "sid32476@blah.com", "BankAccountDetails"=> ""], $summarize_errors);
            echo '<pre>';
            print_r($result);die;
        } catch (Exception $e) {
            echo 'Exception when calling AccountingApi->createContacts: ', $e->getMessage(), PHP_EOL;
        }
        die;
    }

    public function getAccounts()
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $newDate = '2021-06-01T11:18:43.202-08:00';
        $if_modified_since = "$newDate"; // \DateTime | Only records created or modified since this timestamp will be returned
        $where = 'Status=="ACTIVE"'; // string | Filter by an any element
        $order = 'Name ASC'; // string | Order by an any element

        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        try {
            $result = $apiInstance->getAccounts($tok_data['tenant_id'], $if_modified_since, $where, $order);
            $entities_data = $result->getAccounts();

            $newMax = 0;
            if(!empty($entities_data)){
                foreach ($entities_data as $key => $accountData) {
                    $date = $accountData->getUpdatedDateUtc();
                    $date = explode('+', $date);
                    $newTime = substr($date[0], 6);

                    if($newMax < $newTime){
                        echo "new max: $newMax ----------";
                        $newMax = $newTime;
                        echo "new max: $newMax ---------- <br>";
                    }
                }
            }
            echo '<pre>';
            print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling AccountingApi->getAccounts: ', $e->getMessage(), PHP_EOL;
        }
        die;
    }

    public function getInvoices()
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $if_modified_since = ''; // \DateTime | Only records created or modified since this timestamp will be returned
        $where = ''; // string | Filter by an any element
        $order = 'InvoiceNumber ASC'; // string | Order by an any element
        $i_ds = ''; // string[] | Filter by a comma-separated list of InvoicesIDs.
        $invoice_numbers = ''; // string[] | Filter by a comma-separated list of InvoiceNumbers.
        $contact_i_ds = false; // string[] | Filter by a comma-separated list of ContactIDs.
        $statuses = ''; // string[] | Filter by a comma-separated list Statuses. For faster response times we recommend using these explicit parameters instead of passing OR conditions into the Where filter.
        $page = 1; // int | e.g. page=1 – Up to 100 invoices will be returned in a single API call with line items shown for each invoice
        $include_archived = True; // bool | e.g. includeArchived=true - Invoices with a status of ARCHIVED will be included in the response
        $created_by_my_app = false; // bool | When set to true you'll only retrieve Invoices created by your app
        $unitdp = 4; // int | e.g. unitdp=4 – (Unit Decimal Places) You can opt in to use four decimal places for unit amounts
        $summary_only = true; // bool | Use summaryOnly=true in GET Contacts and Invoices endpoint to retrieve a smaller version of the response object. This returns only lightweight fields, excluding computation-heavy fields from the response, making the API calls quick and efficient.

        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        try {
            $result = $apiInstance->getInvoices($tok_data['tenant_id'], $if_modified_since, $where, $order, $i_ds, $invoice_numbers, $contact_i_ds, $statuses, $page, $include_archived, $created_by_my_app, $unitdp, $summary_only);
            
            echo '<pre>';
            print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling AccountingApi->getAccounts: ', $e->getMessage(), PHP_EOL;
        }
        die;
    }

    public function deleteConnection($merchantID)
    {
        $chk_condition = array('merchantID' => $merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $apiInstance = new XeroAPI\XeroPHP\Api\IdentityApi(
            new GuzzleHttp\Client(),
            $config
        );
        try {
            $result = $apiInstance->deleteConnection('a29b5005-ca1f-4308-9d1c-8011512547eb');
            echo '<pre>';
            print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling AccountingApi->deleteConnection: ', $e->getMessage(), PHP_EOL;
        }
        die;
    }

    public function createPayment()
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        try {
            $payment = [
                "Invoice"=> [ 
                    "LineItems"=> [], 
                    "InvoiceID"=> "ddffea66-c19d-4504-bd92-3e9737d8867d" 
                ], 
                "Account"=> [ 
                    "Code"=> "2500" 
                ], 
                "Date"=> "2021-06-09", 
                "Amount"=> 1
            ];

            $result = $apiInstance->createPayment($tok_data['tenant_id'],$payment);
            echo '<pre>';
            print_r($result);
        } catch (Exception $e) {
            $erroMsg = $this->getError($e);
            echo 'Exception when calling AccountingApi->createPayment: ', $erroMsg;
        }
        die;
    }

    public function getPayment()
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        try {
            $payment = '62be8c20-4ca9-4337-b12c-9e8119f4e2e1';

            $result = $apiInstance->getPayment($tok_data['tenant_id'],$payment);
            echo '<pre>';
            print_r($result);
        } catch (Exception $e) {
            echo '<pre>';
            print_r($e->getCode());die;
            echo 'Exception when calling AccountingApi->getPayment: ', $e->getMessage(), PHP_EOL;
        }
        die;
    }

    public function updatePayment()
    {
        $chk_condition = array('merchantID' => $this->merchantID);
        $tok_data      = $this->general_model->get_row_data('tbl_xero_token', $chk_condition);
        $accessToken   = $tok_data['access_token'];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        try {
            $payment = '38133402-d23b-48a4-b68f-48d0a12a22f3';
            $data = [ 
                "Date"=> "2021-06-10", 
                // "Amount"=> 2 
                // "Status" => "DELETED" 
            ];
            $result = $apiInstance->deletePayment($tok_data['tenant_id'],$payment, $data);
            echo '<pre>';
            print_r($result);
        } catch (Exception $e) {
            $erroMsg = $this->getError($e);
            echo 'Exception when calling AccountingApi->deletePayment: ', $erroMsg;
        }
        die;
    }

    private function getError($error){
        $erroCode = $error->getCode();
        $errorBody = $error->getResponseBody();
        $errorBody = json_decode($errorBody, true);

        $finalError = "Token authentication error";
        if($erroCode == 401){
            return $finalError;
        }

        $all_error = [];
        foreach ($errorBody['Elements'] as $errorData) {
            if($errorData['HasValidationErrors']){
                $errorTypeKey = "ValidationErrors";
            } else {
                $all_error = [ "Something went wrong. Please check for more errors codes." ];
                continue; 
            }
            $erroList = $errorData[$errorTypeKey];
            foreach ($erroList as $erroMsg) {
                $all_error[] = $erroMsg['Message'];
            }
        }

        if(!empty($all_error)){
            $finalError = implode(" ", $all_error);
        }
        return $finalError;
    }
}
