<?php

/**
 * Creating Merchant Subscription Plan for customers
 
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
class Utils extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
        $this->load->library(['form_validation']);
		$this->load->model(['general_model']);
        
        if ($this->session->userdata('logged_in') != "") {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}

    }
    
    public function is_plan_exists() {
        $this->form_validation->set_rules('plan_url', 'Plan URL', 'required|xss_clean');			
          
        if ($this->form_validation->run() == true)
        {
            $input_data = $this->input->post(null, true);

            if ($this->session->userdata('logged_in')) {
                $data['login_info'] 	= $this->session->userdata('logged_in');
                $user_id 				= $data['login_info']['merchID'];
                $login_type = $this->session->userdata('logged_in')['active_app'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info'] 	= $this->session->userdata('user_logged_in');
    
                $user_id 				= $data['login_info']['merchantID'];
                $login_type = $this->session->userdata('user_logged_in')['active_app'];
            }

            

            $base_id = base64_encode($user_id);

            $input_data['user_id'] = $user_id;
            $input_data['base_id'] = $base_id;

            switch ($login_type) {
                case '1':
                    $isExist = $this->_is_qbo_plan_exists($input_data);
                    break;

                case '2':
                    $isExist = $this->_is_qb_plan_exists($input_data);
                    break;
                
                case '3':
                    $isExist = $this->_is_fb_plan_exists($input_data);
                    break;
                
                case '4':
                    $isExist = $this->_is_xero_plan_exists($input_data);
                    break;

                case '5':
                    $isExist = $this->_is_cz_plan_exists($input_data);
                    break;

                default:
                    $isExist = true;
                    break;
            }

            $message = ($isExist) ? 'Plan already exists. Please try another combination' : ''; 

            echo json_encode(array('success' => $isExist, 'message' => $message)); die;
        } else {
            echo json_encode(array('success' => true, 'message' => 'Invalid Request', 'data' => validation_errors())); die;
        }
    }

    private function _is_qb_plan_exists($args = []) {

        $coditionp = array('merchantID' => $args['user_id'], 'customerPortal' => '1');

        $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);     

		$ttt = explode(PLINK, $ur_data['customerPortalURL']);
        $purl = $ttt[0] . PLINK.'/customer/qbd_check_out/' . $args['base_id'] . '/';
        $merchantPlanURL = $purl . $args['plan_url'];

        $existCondition = [
            'merchantPlanURL' => $merchantPlanURL
        ];

        if(isset($args['planID']) && !empty($args['planID'])) {
            $existCondition['planID !='] = $args['planID'];
        }

        $ur_data = $this->general_model->get_select_data('tbl_subscriptions_plan_qb', array('merchantPlanURL'), $existCondition);

        return ($ur_data) ? true : false;
    }

    private function _is_cz_plan_exists($args = []) {
        $coditionp = array('merchantID' => $args['user_id'], 'customerPortal' => '1');

        $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);        
		$ttt = explode(PLINK, $ur_data['customerPortalURL']);
		$purl = $ttt[0] . PLINK.'/customer/company_check_out/' . $args['base_id'] . '/';

        $merchantPlanURL = $purl . $args['plan_url'];

        $existCondition = [
            'merchantPlanURL' => $merchantPlanURL
        ];

        if(isset($args['planID']) && !empty($args['planID'])) {
            $existCondition['planID !='] = $args['planID'];
        }

        $ur_data = $this->general_model->get_select_data('tbl_chargezoom_subscriptions_plan', array('merchantPlanURL'), $existCondition);
        return ($ur_data) ? true : false;
    }

    private function _is_fb_plan_exists($args = []) {
        $coditionp = array('merchantID' => $args['user_id'], 'customerPortal' => '1');

        $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);        
		$purl = $ur_data['customerPortalURL'].'fb_check_out/'.$args['base_id'].'/';

        $merchantPlanURL = $purl . $args['plan_url'];

        $existCondition = [
            'merchantPlanURL' => $merchantPlanURL
        ];

        if(isset($args['planID']) && !empty($args['planID'])) {
            $existCondition['planID !='] = $args['planID'];
        }

        $ur_data = $this->general_model->get_select_data('tbl_subscriptions_plan_fb', array('merchantPlanURL'), $existCondition);
        return ($ur_data) ? true : false;
    }

    private function _is_qbo_plan_exists($args = []) {

        $coditionp = array('merchantID' => $args['user_id'], 'customerPortal' => '1');

        $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);     

		$ttt = explode(PLINK, $ur_data['customerPortalURL']);
        $purl = $ttt[0] . PLINK.'/customer/qbo_check_out/' . $args['base_id'] . '/';
        $merchantPlanURL = $purl . $args['plan_url'];

        $existCondition = [
            'merchantPlanURL' => $merchantPlanURL
        ];

        if(isset($args['planID']) && !empty($args['planID'])) {
            $existCondition['planID !='] = $args['planID'];
        }

        $ur_data = $this->general_model->get_select_data('tbl_subscriptions_plan_qbo', array('merchantPlanURL'), $existCondition);

        return ($ur_data) ? true : false;
    }

    private function _is_xero_plan_exists($args = []) {

        $coditionp = array('merchantID' => $args['user_id'], 'customerPortal' => '1');

        $ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);     

		$ttt = explode(PLINK, $ur_data['customerPortalURL']);
        $purl = $ttt[0] . PLINK.'/customer/xero_check_out/' . $args['base_id'] . '/';
        $merchantPlanURL = $purl . $args['plan_url'];

        $existCondition = [
            'merchantPlanURL' => $merchantPlanURL
        ];

        if(isset($args['planID']) && !empty($args['planID'])) {
            $existCondition['planID !='] = $args['planID'];
        }

        $ur_data = $this->general_model->get_select_data('tbl_subscriptions_plan_xero', array('merchantPlanURL'), $existCondition);

        return ($ur_data) ? true : false;
    }
}