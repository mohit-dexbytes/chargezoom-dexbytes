<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use \Chargezoom\Core\Http\SessionRequest;
class Login extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
		$this->load->library('form_validation'); 
		$this->load->model('general_model');
		$this->load->model('user_login_model');
		$this->load->library(array('session', 'form_validation'));
		
   
	}
	
	public function index()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$this->load->view('template/template_start', $data);
		$this->load->view('pages/userlogin', $data);
		$this->load->view('template/template_scripts', $data);
		$this->load->view('template/template_end', $data);
	}
	
	
	public function user_login()
    {     
        $email = $this->czsecurity->xssCleanPostInput('userEmail'); 
        $password = $this->czsecurity->xssCleanPostInput('userPassword');
        
		
            $user = $this->user_login_model->get_user_login($email, $password);	
				
			if (!empty($user) ) {    
 		        $user1=  $this->general_model->get_merchant_user_data($user['merchantID'], $user['merchantUserID']);
				
				if(!empty($user1)){
					$auth = $this->general_model->get_auth_data($user1['authID']); 
					$user['authName'] = $auth; 
				}
				 
		    $this->session->set_userdata('user_logged_in',$user);
			redirect(base_url('home/index','refresh'));                
            }
            else
            {
                $this->session->set_flashdata('message','Login credentials does not match!');
                
           }
		  
            redirect(base_url('user/login#login'));
        
	}
	
	public function user_login_main()
    {     
		$data = $this->session->flashdata('user_login_data');
        
		if(!empty($data)){
			$email = $data['login-email']; 
			$password = $data['login-password'];
			
			$user = $this->user_login_model->get_user_login($email, $password);	
			
			if (!empty($user) ) {    
				$user1=  $this->general_model->get_merchant_user_data($user['merchantID'], $user['merchantUserID']);
				if(!empty($user1)){
					$auth = $this->general_model->get_auth_data($user1['authID']); 
					$user['authName'] = $auth; 
				}

				$merchantID = $user['merchantID'];

				$condition  = array('merchantID' => $user['merchantID']);
				$gatway_num =  $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);

				if ($gatway_num) {
					$user['merchant_gateway']	= '1';
				} else {
					$user['merchant_gateway']	= '0';
				}

				$merchant_data =  $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user['merchantID']));
				unset($merchant_data['merchantPassword']);
                unset($merchant_data['merchantPasswordNew']);
				$user['merchant_data']	= $merchant_data;

				
				$user['script']      = '';
				$user['logo_img']    = '';
				/* <------------End--------------> */

				$config     = $this->general_model->get_row_data('tbl_config_setting', $condition);
				if (!empty($config)) {

					$user['portalprefix']      = $config['portalprefix'];
				} else {
					$user['portalprefix']      = '';
				}

				$appset     = $this->general_model->get_row_data('app_integration_setting', $condition);
				if (!empty($appset)) {
					$user['active_app']	= $appset['appIntegration'];
					$user['gatewayMode']	= $appset['transactionMode'];
				} else {
					$user['active_app']	= '2';
					$user['gatewayMode']	= 0;
				}

				

				if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

					$this->general_model->insert_inv_number($merchantID);
				}

				if ($this->general_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
					$this->general_model->insert_payterm($merchantID);
				}

				
				/* Update session record */
				$sessionRequest 	= new SessionRequest($this->db);
				$updateSession = $sessionRequest->updateSessionUser(SessionRequest::USER_TYPE_SUBUSER,$user['merchantUserID'],$this->session->userdata('session_id'));
				/*End session update*/

				if ($user['active_app'] == '1') {
					$furl  =	'QBO_controllers/home/index'; // base_url('QBO_controllers/home/index');
					//$link['page_name'] ="index";
				} else if ($user['active_app'] == '3') {
					$furl  =	'FreshBooks_controllers/home/index'; //
				} else if ($user['active_app'] == '4') {
					$furl =	'Integration/home/index';
				} else if ($user['active_app'] == '5') {
					$furl =	'company/home/index';
				} else {
					$furl  =	'home/index';
				}
					
				$this->session->set_userdata('user_logged_in',$user);
				redirect(base_url($furl,'refresh'));                
			}
		}
		$strng1 = "Hmm. That didn't work.";
		$strng2 = "Let's get you back into your account.";
		$strng3 = '<a href="javascript:void(0)" id="link-reminder-login">Help me sign in </a>';
		$this->session->set_flashdata('message', '<div class="alert alert-danger"> <h4 class="error_step1"> '.$strng1.' </h4><h5 class="error_step2"> '.$strng2.' </h5><h5 class="error_step3"> '.$strng3.' </h5> </div>');
		redirect(base_url('/'));
    }
	
	public function user_logout()
	{
		 redirect(base_url('user/login/user_login'));
	}
	
	/**
	 * Function to change the password of merchant subuser
	 * 
	 * @return void
	 */
	public function change_password() : void
	{
		if ($this->session->userdata('user_logged_in')) {
			$userSession = $this->session->userdata('user_logged_in');
		} else {
			$this->session->set_flashdata('error', 'Invalid Request');
			redirect('/');
		}
		if (!empty($userSession)) {
			if ($userSession['active_app'] == 1)
			{
				$redirectURL = 'QBO_controllers/home/index';
			}
			elseif ($userSession['active_app'] == 2)
			{
				$redirectURL = 'home/index';
			}
			elseif ($userSession['active_app'] == 3)
			{
				$redirectURL = 'FreshBooks_controllers/home/index';
			}
			elseif ($userSession['active_app'] == 4)
			{
				$redirectURL = 'Integration/home/index';
			} elseif ($userSession['active_app'] == 5)
			{
				$redirectURL = 'company/home/index';
			}
			else
			{
				$redirectURL = '/';
			}
		} else {
			$redirectURL = 'firstlogin/dashboard_first_login';
		}
		if (!empty($this->input->post())) {

			$oldPassword = $this->czsecurity->xssCleanPostInput('user_settings_currentpassword');
			$newPassword = $this->czsecurity->xssCleanPostInput('user_settings_password');

			// Check Password Strength
			$strengthCheck = $this->czsecurity->checkPasswordStrength($newPassword);

			if($strengthCheck['status'] != 'OK'){
				$this->session->set_flashdata('error', $strengthCheck['message']);
				redirect($redirectURL);
			}

			$userData = $this->general_model->get_row_data('tbl_merchant_user', array('merchantUserID' => $userSession['merchantUserID']));
			if (!empty($userData) ) {
				$validPassword = true;
				if ($userData['userPasswordNew'] !== null) {
					if (!password_verify($oldPassword, $userData['userPasswordNew'])) {
						$validPassword = false;
					}
				} else if ($oldPassword != $userData['userPassword']) {
					$validPassword = false;
				}

				if($validPassword){
					$this->general_model->update_row_data('tbl_merchant_user', 
						array('merchantUserID' => $userSession['merchantUserID']),
						array(
							'userPassword' => null,
							'userPasswordNew' => password_hash(
								$newPassword,
								PASSWORD_BCRYPT
							),
						)
					);
					$this->session->set_flashdata('success', 'Password has been changed successfully');
				} else {
					$this->session->set_flashdata('error', 'Please enter correct current password');
				}
			} else {
				$this->session->set_flashdata('error', 'Please enter correct current password');
			}
		} else {
			$this->session->set_flashdata('error', 'Invalid Request');
		}
		redirect($redirectURL);
	}
		
		
}
 
 
 
 
 ?>