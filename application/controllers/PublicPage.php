<?php

class PublicPage extends CI_Controller
{
    	public function __construct()
	{
		parent::__construct();
		
		
	}
	
	
	public function termsAndCondition()
	{	 
	    $data['template'] 		= template_variable();
	    $this->load->view('template/template_start', $data);
	    $this->load->view('comman-pages/public_terms_and_condition');
	    $this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	public function privacyPolicy()
	{	 
	    $data['template'] 		= template_variable();
	    $this->load->view('template/template_start', $data);
	    $this->load->view('comman-pages/public_privacy_policy');
	    $this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
    
}

?>