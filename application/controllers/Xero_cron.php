<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xero_cron extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Xero.php';
		$this->load->model('general_model');
		$this->load->model('Xero_models/xero_subscription_model');
	}
	
	

	public function index(){
	    
	 
	   }

	
	public function create_invoice(){

		define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		 
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
		}
		define ( "OAUTH_CALLBACK", $url );

		$signatures = array (
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0' 
		);
 
		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );

        $subsdata  = $this->xero_subscription_model->get_subcription_data();
        
        if(!empty($subsdata)){
			foreach($subsdata as $subs){
			
				$item_val =  $this->xero_subscription_model->get_sub_invoice('tbl_subscription_invoice_item_xero',array("subscriptionID" => $subs['subscriptionID']));
				$duedate   = date("Y-m-d", strtotime($subs['nextGeneratingDate']));

				$randomNum =substr(str_shuffle("0123456789"), 0, 5);
			
				$in_num  =$subs['generatedInvoice']; 
				$paycycle = $subs['invoiceFrequency'];
				$date     = $subs['firstDate'];
					
				if($paycycle=='dly'){

					$in_num   = ($in_num)? $in_num+1:'1';
					$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day")); 
				
				} else if($paycycle=='1wk'){

					$in_num   = ($in_num)? $in_num+1:'1';
					$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
				
				} else if($paycycle=='2wk'){

					$in_num   = ($in_num)?$in_num +2 :'1' ;
					$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
				
				} else if($paycycle=='mon'){

					$in_num   = ($in_num)? $in_num+1:'1'; 
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				
				} else if($paycycle=='2mn'){

					$in_num   = ($in_num)?$in_num + 2*1:'1';
					$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
		
				} else if($paycycle=='qtr'){

					$in_num   = ($in_num)?$in_num + 3*1:'1';
					$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
				
				} else if($paycycle=='six'){

					$in_num   = ($in_num)?$in_num + 6*1:'1';
					$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				
				} else if($paycycle=='yrl'){

					$in_num   = ($in_num)?$in_num + 12*1:'0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				
				} else if($paycycle=='2yr'){

					$in_num   = ($in_num)?$in_num + 2*12:'1';
					$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				
				} else if($paycycle=='3yr'){

					$in_num   = ($in_num)?$in_num + 3*12:'1';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
		
				}	
				if($this->general_model->update_row_data('tbl_subscription_invoice_item_xero', array('subscriptionID'=>$subs['subscriptionID']),array('invoiceDataID'=>$randomNum)))
				{
					$gen_inv = 	$subs['generatedInvoice']+1;
					$this->general_model->update_row_data('tbl_subscriptions_xero', array('subscriptionID'=>$subs['subscriptionID']), array('nextGeneratingDate'=>$next_date,'generatedInvoice'=>$gen_inv ));
			
					if ($checkErrors > 0) {
						foreach ( $initialCheck as $check ) {
							echo 'Error: ' . $check . PHP_EOL;
						}
					} else {

						if($this->session->userdata('logged_in')){
							$user_id = $this->session->userdata('logged_in')['merchID'];
							$merchID = $user_id;
						}
					
						$val = array(
							'merchantID' => $merchID,
						);

						$xero_data = $this->general_model->get_row_data('tbl_xero_token',$val);
					
						$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
						$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
				
						if($subs['taxID'] == NULL){ 
							$lineArray = "";
							
							for($i=0; $i<count($item_val);$i++){
							
								$lineArray .= '<LineItem><Description>'.$item_val[$i]['itemDescription'].'</Description>';
								$lineArray .= '<UnitAmount>'.$item_val[$i]['itemRate'].'</UnitAmount>';
								$lineArray .= '<Quantity>'.$item_val[$i]['itemQuantity'].'</Quantity>';
								$lineArray .= '<AccountCode>400</AccountCode>';
								$lineArray .= '<ItemCode>'.$item_val[$i]['itemListID'].'</ItemCode></LineItem>';  
							}
							
							
							$invoice = '<Invoice>
									<Type>ACCREC</Type>
									<Contact>
									<ContactID>'.$subs['customerID'].'</ContactID>
									<Address>
										<AddressType>STREET</AddressType>
										<AddressLine1>'.$subs['address1'].'</AddressLine1>
										<AddressLine2>'.$subs['address2'].'</AddressLine2>
										<City>'.$subs['city'].'</City>
										<Region>'.$subs['state'].'</Region>
										<PostalCode>'.$subs['zipcode'].'</PostalCode>
										<Country>'.$subs['country'].'</Country>
									</Address>
									<Phones>
										<Phone>
										<PhoneType>MOBILE</PhoneType>
										<PhoneNumber>'.$subs['contactNumber'].'</PhoneNumber>
										</Phone>
									</Phones>
									</Contact>
									<DueDate>'.$duedate.'</DueDate>
									<LineItems>'.$lineArray.'</LineItems>
									<Status>AUTHORISED</Status>
									</Invoice>';
									
							$response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $invoice);
							if ($XeroOAuth->response['code'] == 200) {
								echo "Success";
							}
							else{
								echo "Failed to create invoice";
							}	 
						} else {
							$lineArray = "";
							
							for($i=0; $i<count($item_val);$i++){
							
								$lineArray .= '<LineItem><Description>'.$item_val[$i]['itemDescription'].'</Description>';
								$lineArray .= '<UnitAmount>'.$item_val[$i]['itemRate'].'</UnitAmount>';
								$lineArray .= '<Quantity>'.$item_val[$i]['itemQuantity'].'</Quantity>';
								$lineArray .= '<AccountCode>400</AccountCode>';
								$lineArray .= '<ItemCode>'.$item_val[$i]['itemListID'].'</ItemCode><TaxType>'.$subs['taxID'].'</TaxType></LineItem>';  
							}
							
							
							$invoice = '<Invoice>
									<Type>ACCREC</Type>
									<Contact>
									<ContactID>'.$subs['customerID'].'</ContactID>
									<Address>
										<AddressType>STREET</AddressType>
										<AddressLine1>'.$subs['address1'].'</AddressLine1>
										<AddressLine2>'.$subs['address2'].'</AddressLine2>
										<City>'.$subs['city'].'</City>
										<Region>'.$subs['state'].'</Region>
										<PostalCode>'.$subs['zipcode'].'</PostalCode>
										<Country>'.$subs['country'].'</Country>
									</Address>
									<Phones>
										<Phone>
										<PhoneType>MOBILE</PhoneType>
										<PhoneNumber>'.$subs['contactNumber'].'</PhoneNumber>
										</Phone>
									</Phones>
									</Contact>
									<DueDate>'.$duedate.'</DueDate>
									<LineItems>'.$lineArray.'</LineItems>
									<Status>AUTHORISED</Status>
									</Invoice>';
									
							$response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $invoice);
							if ($XeroOAuth->response['code'] == 200) {
							echo "Success";
							}
							else{
								echo "Failed to create invoice";
							}	 
						}
					}
				}	
			} 
	   	}
	}
}