<?php
/**
 * Surcharging
 * 
 * This file contains surcharge view, saved, update and sync data
 *
 * @author Chargezoom Developers <devteam@chargezoom.com>
 *
 */
class Surcharging extends CI_Controller
{
    private $merchantID;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('general_model');
        if ($this->session->userdata('logged_in')) {
            $da['login_info']   = $this->session->userdata('logged_in');
            $this->merchantID           = $da['login_info']['merchID'];
        
        }elseif ($this->session->userdata('user_logged_in')) {
            $da['login_info']       = $this->session->userdata('user_logged_in');
            $this->merchantID      = $da['login_info']['merchantID'];
        }else{
            redirect('login');
        }
    }

    /**
     * load view for surcharging 
     *
     *
     * @return view
     */
    public function index()
    {
        $data['primary_nav']     = primary_nav();
        $data['template']        = template_variable();
         
        if($this->session->userdata('logged_in')){
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }elseif($this->session->userdata('user_logged_in')){
            $data['login_info']     = $this->session->userdata('user_logged_in');
            $user_id      = $data['login_info']['merchantID'];
        }           
        $merchSurchargeData = $this->general_model->get_row_data('tbl_merchant_surcharge', array('merchantID' => $user_id));
        if(isset($merchSurchargeData['id'])){
            $data['isEnable'] = $merchSurchargeData['isEnable'];
        }else{
            $data['isEnable'] = 0;
        }
        $data['appIntegration'] = $data['login_info']['active_app'];

        if($data['appIntegration'] == 1){
            $this->load->model('QBO_models/qbo_customer_model');
            /*Data Sync from QBO*/
            $condition = array('merchantID' => $user_id, 'IsActive' => 'true');
            $data['items'] = $this->general_model->get_table_data('QBO_test_item', $condition);
            $account_list         = $this->general_model->get_table_data('QBO_accounts_list', array('merchantID' => $user_id));
            $data['account_lists'] = $account_list;
        }elseif($data['appIntegration'] == 2){
            $this->load->model('company_model');
            $data['items']   = $this->company_model->get_plan_data_invoice($user_id);
            $account = $this->company_model->get_product_account_data(array('merchantID' => $user_id));
            $data['account_lists'] = $account;
        }elseif($data['appIntegration'] == 5){
            $comp_data   = $this->general_model->get_row_data('tbl_company',array('merchantID' => $user_id));
            $items   = $this->general_model->get_table_data('chargezoom_test_item',array('companyListID' => $comp_data['id']));
            $data['items']   = $items;
            $account = ['Surcharge Income'];
            $data['account_lists'] = $account;
        }

        $data['surchargeData'] = $merchSurchargeData;
        $data['merchantID'] = $user_id;   
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/surcharging', $data);
        $this->load->view('template/page_footer',$data);
        $this->load->view('template/template_end', $data);
    }

    /**
     * Saved Surcharging Data 
     *
     *
     * @return 
     */
    public function surchargingSaved()
    {
        if($this->session->userdata('logged_in')){
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }elseif($this->session->userdata('user_logged_in')){
            $data['login_info']     = $this->session->userdata('user_logged_in');
            $user_id      = $data['login_info']['merchantID'];
        }   
        if($this->input->post(null, true))
        {
            $surchargePercent = $this->czsecurity->xssCleanPostInput('surcharge_percent');
 
            $surchargeRowID = $this->czsecurity->xssCleanPostInput('surchargeRowID');
          
            $defaultItemId = $this->czsecurity->xssCleanPostInput('default_item_id');
            $defaultAccountId = $this->czsecurity->xssCleanPostInput('default_account_id');
            $surChargeData = array(
                'surchargePercent' => $surchargePercent,
                'defaultItem' => $defaultItemId,
                'defaultItemAccount' => $defaultAccountId,
            );
            $conditionMerch = array('merchantID'=>$user_id,'id' => $surchargeRowID);
            $update = $this->general_model->update_row_data('tbl_merchant_surcharge',$conditionMerch, $surChargeData);
            if($update){
                $this->session->set_flashdata('success','Merchant Surcharge configuration saved');
            }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error: Merchant surcharge configuration save failed.</strong></div>');  
            }
          
        }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error: Merchant surcharge configuration save failed.</strong></div>');  
        }
        redirect('surcharging');
    }

    /** Update merchant surcharge status enable and disable
     * 
     *
     *
     * @return 
     */
    public function surchargeStatusUpdate()
    {
        $merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
        $appIntegration = $this->czsecurity->xssCleanPostInput('appIntegration');
        $surchargeStatus = $this->czsecurity->xssCleanPostInput('surchargeStatus');
        $merch_data = $this->general_model->get_row_data('tbl_merchant_surcharge', array('merchantID' => $merchantID));
        if(isset($merch_data['id'])){
            $conditionMerch = array('merchantID'=>$merchantID,'id' => $merch_data['id']);
            $update_array =  array( 'isEnable'  => $surchargeStatus);

            $update = $this->general_model->update_row_data('tbl_merchant_surcharge',$conditionMerch, $update_array); 

        }else{
            $surChargeData = array(
                'isEnable' => $surchargeStatus,
                'surchargePercent' => '3.00',
                'merchantID' => $merchantID,
                'defaultItem' => 'Surcharge Fees',
                'defaultItemAccount' => 'Surcharge Income',
            );
            $insert = $this->general_model->insert_row('tbl_merchant_surcharge',$surChargeData);

        }
        
        if($surchargeStatus == 1){
            /* QBO SYNC*/
            if($appIntegration == 1){
                require APPPATH .'libraries/qbo/QBO_data.php';
                $this->qboOBJ = new QBO_data($merchantID);
                $syncQBO = $this->qboOBJ->createDefaultSyncRecords($merchantID);
            }elseif($appIntegration == 2){
                /* QBD SYNC*/
                require APPPATH .'libraries/QBD_Sync.php';
                $this->qbdOBJ = new QBD_Sync($merchantID);
                $syncQBD = $this->qbdOBJ->createDefaultSyncRecords($merchantID);

            }elseif($appIntegration == 5){
                $comp_data   = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$merchantID));
                $itemExist   = $this->general_model->get_row_data('chargezoom_test_item',array('companyListID' => $comp_data['id'], 'FullName' => 'Surcharge Fees'));
                
                $product_data = array(
                    'IsActive' => 'true',
                    'TimeCreated' => date('Y-m-d H:i:s'),
                    'Name' => 'Surcharge Fees',
                    'FullName' => 'Surcharge Fees',
                    'Type' => 'Service',
                    'SalesPrice' => 0,
                    'SalesDesc' => 'Surcharge Fees',
                    'AccountRef' => '',
                    'Discount' => 0,
                    'Parent_ListID' => 0,
                    'Parent_FullName' => 0,
                    'companyListID' => $comp_data['id']
                );
                
                if($itemExist){
                    $list_id = $itemExist['ListID'];
                    $product_data['ListID'] = $list_id;
                    $this->general_model->update_row_data('chargezoom_test_item', ['ListID' => $itemExist['ListID'], 'companyListID' => $comp_data['id']], $product_data);
                }else{
                    $lsID = $this->general_model->get_random_string('PRO');
                    $product_data['ListID'] = $lsID;
                    $list_id = $lsID;

                    $this->general_model->insert_row('chargezoom_test_item', $product_data);
                }
                $conditionMerch = array('merchantID'=>$merchantID,'id' => $merch_data['id']);
                $update_array =  array( 'defaultItem'  => $list_id);

                $update = $this->general_model->update_row_data('tbl_merchant_surcharge',$conditionMerch, $update_array); 
            }   
        }
        if($surchargeStatus){
            $this->session->set_flashdata('success', 'Surcharge Enabled Successfully');
        }else{
            $this->session->set_flashdata('success', 'Surcharge Disabled Successfully');
        }
        redirect('surcharging');
    }
}