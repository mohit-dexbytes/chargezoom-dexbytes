<?php
/* This controller has following opration for send mail
 * getSendGridEmailStatusDetail
 * setSendGridEventWebhook
 * parseWebhookData
 * triggerSendgridWebhookData
 */
class SendGrid extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('general_model');
    }
    /*
    * Function fetch email status by sendgrid
    * @param: None
    * @return: Json data
    */
    public function getSendGridEmailStatusDetail()
    {
        if ($this->session->userdata('logged_in')) {
            $loginDetails   = $this->session->userdata('logged_in');
        }
        if ($this->session->userdata('user_logged_in')) {
            $loginDetails       = $this->session->userdata('user_logged_in');
        }

        $mailID = $this->input->post('mailID', true);
        $mailData = $this->general_model->get_row_data('tbl_template_data', ['mailID' => $mailID]);
        $msg_id = $mailData['send_grid_email_id'];
        $event_data = [];
        if($mailData['send_grid_email_id']){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.sendgrid.com/v3/messages/".$msg_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer ".getenv('SEND_GRID_API_KEY'),
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $email_details = json_decode($response, 1);
            
            if(isset($email_details['events'])){
                foreach ($email_details['events'] as $value) {
                    if(isset($loginDetails['merchant_default_timezone']) && !empty($loginDetails['merchant_default_timezone'])){
                        $timezone = ['time' => $value['processed'], 'current_format' => 'UTC', 'new_format' => $loginDetails['merchant_default_timezone']];
                        $value['processed'] = getTimeBySelectedTimezone($timezone);
                    }
                    $mx_server = isset($value['mx_server']) ? $value['mx_server'] : ''; 
                    $event_data[] = ['event_name' => $value['event_name'], 'mx_server' => $mx_server, 'date' => date('M d, Y h:i A', strtotime($value['processed']))];
                }
            }
        }

        echo json_encode(['data' => $event_data, 'mailData' => $mailData]);
        die;
    }
    /*
    * Function sendGrid webhook event request
    * @param: None
    * @return: curl data
    */
    public function setSendGridEventWebhook()
    {
        // request payload data
        $request_data = [
            'enabled' => true,
            'url' => base_url().'Common_controllers/sendGrid/triggerSendgridWebhookData',
            'group_resubscribe' => false,
            'delivered' => true,
            'group_unsubscribe' => false,
            'spam_report' => false,
            'bounce' => false,
            'deferred' => false,
            'unsubscribe' => false,
            'processed' => false,
            'open' => false,
            'click' => false,
            'dropped' => false
        ];

        // curl init
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/user/webhooks/event/settings",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PATCH",
          CURLOPT_POSTFIELDS => json_encode($request_data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".getenv('SEND_GRID_API_KEY'),
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        echo '</pre>';
        print_r($response);
    }

    /*
    * Function for parse sendgrid event for email open
    * @param: None
    * @return: None
    */
    public function parseWebhookData()
    {   
        $data = file_get_contents("php://input");
        // json decode of data
        $events = json_decode($data, true);
        if(!empty($events)){
            foreach ($events as $key => $event) {
                // check event for open email
                if($event['event'] == 'delivered'){
                    // get email for response data
                    $email = $event['email'];
                    // get message id for event data
                    $sg_message_id = current(explode('.', $event['sg_message_id']));
                    // check email exist or not
                    $checkEmail = $this->general_model->get_select_data('tbl_template_data', ['mailID'], ['send_grid_email_id' => $sg_message_id, 'emailto' => $email]);
                    if($checkEmail){
                        // update status of email to open
                        $this->general_model->update_row_data('tbl_template_data', ['mailID' => $checkEmail['mailID']], ['send_grid_email_status' => 'Delivered', 'send_grid_email_id' => $event['sg_message_id']]);
                    }
                }
            }
        }
    }
    /*
    * Function for trigger send grid webhook Callback URL 
    * @param: None
    * @return: None
    */
    public function triggerSendgridWebhookData()
    {
        ini_set('max_execution_time', 0);
        $data = file_get_contents("php://input");
        sleep(5);
        /* Start Fetch sendgrid status for CZ*/
        // json decode of data
        $events = json_decode($data, true);
        if(!empty($events)){
            foreach ($events as $key => $event) {
                // check event for open email
                if($event['event'] == 'delivered'){
                    // get email for response data
                    $email = $event['email'];
                    // get message id for event data
                    $sg_message_id = current(explode('.', $event['sg_message_id']));
                    // check email exist or not
                    $checkEmail = $this->general_model->get_select_data('tbl_template_data', ['mailID'], ['send_grid_email_id' => $sg_message_id, 'emailto' => $email]);
                    if($checkEmail){
                        // update status of email to open
                        $this->general_model->update_row_data('tbl_template_data', ['mailID' => $checkEmail['mailID']], ['send_grid_email_status' => 'Delivered', 'send_grid_email_id' => $event['sg_message_id']]);
                    }
                }
            }
        }
        /* End Fetch sendgrid status for CZ*/
        $urls = array(SEND_GRID_HPS_WEBHOOK_PARSE_URL);

        $multi_handle = curl_multi_init();
        $handles = array();
        // Loop through each URL and create a cURL Handle for the URL
        // The cURL handle will POST data that comes to this script to the url.
        foreach ($urls as $key => $url) {
            $handles[$key] = curl_init();
            curl_setopt($handles[$key], CURLOPT_URL, $url);
            curl_setopt($handles[$key], CURLOPT_POST, 1);
            curl_setopt($handles[$key], CURLOPT_POSTFIELDS, $data);

            curl_setopt($handles[$key], CURLOPT_RETURNTRANSFER, true);
            curl_multi_add_handle($multi_handle, $handles[$key]);
        }

        $running = null;
        // Run through each cURL handle and execute it.
        do {
            curl_multi_exec($multi_handle, $running);
        } while ($running);

        curl_multi_close($multi_handle);
    }
}