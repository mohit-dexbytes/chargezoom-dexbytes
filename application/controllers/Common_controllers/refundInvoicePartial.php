<?php
error_reporting(1);
/* This controller has following opration for gateways
 * NMI, Authorize.net, Paytrace, Paypal, Stripe Payment Gateway Operations

 * Refund create_customer_refund
 * Single Invoice Payment transaction refund
 * merchantID ans resellerID are Private Member
 */
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
include APPPATH . 'third_party/TSYS.class.php';
require APPPATH . 'libraries/integration/XeroSync.php';
include APPPATH . 'third_party/EPX.class.php';
use iTransact\iTransactSDK\iTTransaction;

use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

include_once APPPATH . 'libraries/Freshbooks_data.php';
class RefundInvoicePartial extends CI_Controller
{

    private $merchantID;
    private $resellerID;
    private $gatewayEnvironment;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();

        $this->load->config('quickbooks');
        
        $this->load->model('general_model');
        $this->load->model('card_model');

        $this->load->model('quickbooks');
        $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
        $this->load->model('customer_model', 'customer_model');

        if ($this->session->userdata('logged_in')) {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $merchID;
        } else {
            redirect('login', 'refresh');
        }
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    
    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

   
    public function commanRefundInvoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $user_id                = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id                = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $merchantID = $user_id;
        if (!empty($this->input->post(null, true))) {
            $transactionID =  $this->czsecurity->xssCleanPostInput('transactionID');
            $transactionGateway =  $this->czsecurity->xssCleanPostInput('gatewaytype');
            $integrationType =  $this->czsecurity->xssCleanPostInput('integrationType');
            $gatewayName =  $this->czsecurity->xssCleanPostInput('gatewayName');
            
            $refundTotalamountInput =  $this->czsecurity->xssCleanPostInput('refundTotalamountInput');

            $invoicePayAmounts = array();
            $txnRowIDArr = [];
            $invoiceIDs = [];
            $custom_data_fields = [];
            $invoiceRefundAmounts = [];
            $refundRemainingAmount = [];
            if(!empty($this->czsecurity->xssCleanPostInput('refundInvoiceIDs')))
            {
                $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('refundInvoiceIDs'));
               
            }
            if(!empty($this->czsecurity->xssCleanPostInput('refundInvoiceAmount')))
            {
                $invoiceRefundAmounts = explode(',', $this->czsecurity->xssCleanPostInput('refundInvoiceAmount'));
               
            }
            if(!empty($this->czsecurity->xssCleanPostInput('refundRemainingAmount')))
            {
                $refundRemainingAmount = explode(',', $this->czsecurity->xssCleanPostInput('refundRemainingAmount'));
               
            }
            
            if(!empty($this->czsecurity->xssCleanPostInput('txnRowIDs')))
            {

                $txnRowIDArr = explode(',', $this->czsecurity->xssCleanPostInput('txnRowIDs'));
               
            }

            $refundTXNID = $transactionID; 
            $paydata = $this->general_model->get_select_data('customer_transaction',
                    array('transactionID','transactionCard','transactionAmount','customerListID','merchantID','transactionGateway','gatewayID','invoiceTxnID','invoiceRefID','qbListTxnID','custom_data_fields'),array('transactionID'=>$transactionID));
            
            $gatlistval = $paydata['gatewayID'];
            $tID       = $paydata['transactionID']; 
            $customerID = $paydata['customerListID'];

            $custom_tr_data = (isset($paydata) && isset($paydata['custom_data_fields'])) ? $paydata['custom_data_fields'] : false;
                
            $payment_type = '';
            if($custom_tr_data){
                $json_data = json_decode($custom_tr_data, 1);
                if(isset($json_data['payment_type'])){
                    $payment_type = $json_data['payment_type'];
                    $custom_data_fields['payment_type'] = $payment_type;
                }
            }
            
            $result = [];
            $amount = $refundTotalamountInput;

            if ($amount == $paydata['transactionAmount']) {
                $restype = "Full";
            } else {
                $restype = "Partial";
            }
            $accountType = 'checking';
            if (strpos($gatewayName, 'ECheck') !== false) {
                $cardData = $this->card_model->getCardData($customerID,3);
                if(isset($cardData[0]['accountType']) && $cardData[0]['accountType'] != ''){
                    $accountType = $cardData[0]['accountType'];
                }
                
                $echeckType = true;
            }else{
                $echeckType = false;
            }
            $status = '';
            
            if($refundTotalamountInput > 0)
            {
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
                /* NMI and Chargezoom Gateway*/
                if($paydata['transactionGateway'] =='1' || $paydata['transactionGateway']=='9')
                {
                    include APPPATH . 'third_party/nmiDirectPost.class.php';
                    

                    $nmiuser  = $gt_result['gatewayUsername'];
                    $nmipass  =  $gt_result['gatewayPassword'];
                    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
                  
                    $transaction = new nmiDirectPost($nmi_data);
                    
                    $transaction->setTransactionId($tID); 
                    if($echeckType){
                        $transaction->setPayment('check');
                    }
                    $transaction->refund($tID,$amount);
                    $result     = $transaction->execute();

                    
                    if($result['response_code'] == '100')
                     {  
                        $refundTXNID = $result['transactionid'];
                        $status="success";
                        $message = "Transaction Refunded";
                        $this->session->set_flashdata('success',$message); 
                     }
                     else
                     {   
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -</strong> ' .$result['responsetext'] . '</div>'); 
                     }
                }
                elseif ($paydata['transactionGateway'] == '2')
                {
                    /*Authorize,net gateway refund*/
                    $this->load->config('auth_pay');
                    include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

                    $apiloginID     = $gt_result['gatewayUsername'];
                    $transactionKey = $gt_result['gatewayPassword'];
                    $transaction    = new AuthorizeNetAIM($apiloginID, $transactionKey);
                    $transaction->setSandbox($this->config->item('auth_test_mode'));
                    $merchantID = $paydata['merchantID'];

                    $card       = $paydata['transactionCard'];
                    $customerID = $paydata['customerListID'];
                    if($echeckType){
                        $transaction->__set('method','echeck');
                    }
                    $result = $transaction->credit($tID, $amount, $card);

                    $res     = $result;
                    $tr_type = 'refund';
                    if ($result->response_code == '1') {
                        $refundTXNID = ($result->transaction_id) ? $result->transaction_id : $transactionID;
                        $status="success";
                        $message = "Transaction Refunded";
                        $this->session->set_flashdata('success',$message); 
                    }else{
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $result->response_reason_text . '</div>');
                    }
                }
                elseif ($paydata['transactionGateway'] == '3') 
                {
                    /*PAytrace refund gateway*/
                    $this->load->config('paytrace');
                    include APPPATH . 'third_party/PayTraceAPINEW.php';
                    $payusername = $gt_result['gatewayUsername'];
                    $paypassword = $gt_result['gatewayPassword'];
                    $grant_type  = "password";
                    $payAPI      = new PayTraceAPINEW();
                    $merchantID  = $paydata['merchantID'];

                    $card = $paydata['transactionCard'];

                    $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                    //call a function of Utilities.php to verify if there is any error with OAuth token.
                    $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                    if (!$oauth_moveforward) {
                        $json = $payAPI->jsonDecode($oauth_result['temp_json_response']);
                        $oauth_token = sprintf("Bearer %s", $json['access_token']);
                        $total        = $amount;
                        $request_data = array("transaction_id" => $tID, 'amount' => $total);
                        $url = URL_TRID_REFUND;
                        
                        if($echeckType){
                            $request_data = array(
                                "check_transaction_id" => $tID,
                                "integrator_id" => $gt_result['gatewaySignature'],
                                'amount' => $total,
                            );
                            $url = URL_ACH_REFUND_TRANSACTION;
                        }
                        $request_data                 = json_encode($request_data);
                        $result                       = $payAPI->processTransaction($oauth_token, $request_data, $url);
                        $response                     = $payAPI->jsonDecode($result['temp_json_response']);
                        $response['http_status_code'] = $result['http_status_code'];
                        $tr_type                      = 'pay_refund';

                        if ($result['http_status_code'] == '200') {
                            $status="success";
                            $refundTXNID = (isset($response['transaction_id'])) ? $response['transaction_id'] : $transactionID;
                            $message = "Transaction Refunded";
                            $this->session->set_flashdata('success',$message); 
                        }else{
                            if (!empty($response['errors'])) {
                                $err_msg = $this->getError($response['errors']);
                            } else {
                                $err_msg = $approval_message;
                            }

                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -</strong> ' . $err_msg . '</div>');
                        }

                    }else{
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> Authentication failed.</div>');
                    }
                }
                elseif ($paydata['transactionGateway'] == '4') 
                {
                    /* Paypal payment refund */
                    $this->load->config('paypal');
                    include APPPATH . 'third_party/PayPalAPINEW.php';
                    $username  = $gt_result['gatewayUsername'];
                    $password  = $gt_result['gatewayPassword'];
                    $signature = $gt_result['gatewaySignature'];
                    $config = array(
                        'Sandbox'      => $this->config->item('Sandbox'), // Sandbox / testing mode option.
                        'APIUsername'  => $username, // PayPal API username of the API caller
                        'APIPassword'  => $password, // PayPal API password of the API caller
                        'APISignature' => $signature, // PayPal API signature of the API caller
                        'APISubject'   => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                        'APIVersion'   => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                    );

                    if ($config['Sandbox']) {
                        error_reporting(E_ALL);
                        ini_set('display_errors', '1');
                    }
                    $this->load->library('paypal/Paypal_pro', $config);

                    $RTFields = array(
                        'transactionid'       => $tID, 
                        'payerid'             => '', 
                        'invoiceid'           => '',
                        'refundtype'          => $restype, 
                        'amt'                 => $amount,
                        'currencycode'        => '', 
                        'note'                => '',
                        'retryuntil'          => '', 
                        'refundsource'        => '', 
                        'merchantstoredetail' => '', 
                        'refundadvice'        => '', 
                        'refunditemdetails'   => '', 
                        'msgsubid'            => '', 
                        'storeid'             => '',
                        'terminalid'          => '',
                    );

                    $PayPalRequestData = array('RTFields' => $RTFields);

                    $PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);

                    $result     = $PayPalResult;
                    $tr_type = 'Paypal_refund';
                    if ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
                        $status="success";
                        $refundTXNID = (isset($PayPalResult['REFUNDTRANSACTIONID'])) ? $PayPalResult['REFUNDTRANSACTIONID'] : $transactionID;
                        $message = "Transaction Refunded";
                        $this->session->set_flashdata('success',$message); 
                    } else {
                        $responsetext = $PayPalResult['L_LONGMESSAGE0'];
                        $code         = '401';
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $responsetext . '</div>');
                    }

                }
                elseif($paydata['transactionGateway'] == '5') {
                    /*Stripe refund transaction*/
                    include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
                    $nmiuser = $gt_result['gatewayUsername'];
                    $nmipass = $gt_result['gatewayPassword'];

                    $plugin = new ChargezoomStripe();
                    $plugin->setApiKey($nmipass);
                    $charge = \Stripe\Refund::create(array(
                        "charge" => $tID,
                        "amount"=> (int)($amount*100),
                    ));
                    $charge = json_encode($charge);
                    $result = json_decode($charge);

                    $res     = $result;
                    $tr_type = 'stripe_refund';

                    if (strtoupper($result->status) == strtoupper('succeeded')) {
                        $result->paid = 1;
                        $result->failure_code = '';
                        $refundTXNID = (isset($result->id)) ? $result->id : $transactionID;
                        $status="success";
                        $message = "Transaction Refunded";
                        $this->session->set_flashdata('success',$message); 
                    }else{
                        $responsetext = $result->status;
                        $result->paid = 0;
                        $result->failure_code = 400;

                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $responsetext . '</div>');
                    }
                }
                elseif ($paydata['transactionGateway'] == '6') {
                    /*USAePay Refund Transaction */
                    $this->load->config('usaePay');
                    require_once APPPATH . "third_party/usaepay/usaepay.php";
                    $payusername = $gt_result['gatewayUsername'];
                    $paypassword = $gt_result['gatewayPassword'];
                    $crtxnID = '';

                    $transaction                      = new umTransaction;
                    $transaction->ignoresslcerterrors = ($this->config->item('ignoresslcerterrors') !== null) ? $this->config->item('ignoresslcerterrors') : true;

                    $transaction->key        = $payusername; // Your Source Key
                    $transaction->pin        = $paypassword; // Source Key Pin
                    $transaction->usesandbox = $this->config->item('Sandbox'); // Sandbox true/false
                    $transaction->testmode   = $this->config->item('TESTMODE'); // Change this to 0 for the transaction to process
                    $transaction->command    = "refund"; // refund command to refund transaction.
                    $transaction->refnum     = $tID; // Specify refnum of the transaction that you would like to capture.
                    $transaction->amount     = $amount;

                    $merchantID = $this->merchantID;
                    $transaction->Process();
                    $trID1 = '';
                    if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') 
                    {
                        $status="success";
                        $message = "Transaction Refunded";

                        $msg        = $transaction->result;
                        $trID1      = $transaction->refnum;

                        $refundTXNID = (isset($transaction->transactionId)) ? $transaction->transactionId : $transactionID;

                        $result = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID1);

                        $this->session->set_flashdata('success',$message);

                    }else{
                        $responsetext = $transaction->result;
                        
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $responsetext . '</div>');
                    }
                }
                elseif ($paydata['transactionGateway'] == '7') {
                    /*Heartland Gateway Refund*/
                    $this->load->config('globalpayments');
                    require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
                    $secretApiKey = $gt_result['gatewayPassword'];
                    $crtxnID    = '';
                    $merchantID = $this->merchantID;
                    $config     = new PorticoConfig();

                    $config->secretApiKey = $secretApiKey;
                    $config->serviceUrl   = $this->config->item('GLOBAL_URL');

                    ServicesContainer::configureService($config);
                    try
                    {

                        $response = Transaction::fromId($tID)

                            ->refund($amount)
                            ->withCurrency("USD")
                            ->execute();

                        $tr1ID = '';
                        if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
                            $msg        = $response->responseMessage;
                            $tr1ID      = $response->transactionId;

                            $refundTXNID = (isset($response->transactionId)) ? $response->transactionId : $transactionID;
                            $status="success";
                            $message = "Transaction Refunded";
                            
                            $result        = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $tr1ID);
                            
                            $this->session->set_flashdata('success', $message);

                        } else {
                            $responsetext   = $response->responseMessage;
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $responsetext . '</div>');

                        }

                    } catch (BuilderException $e) {
                        $error = 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                    } catch (ConfigurationException $e) {
                        $error = 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                    } catch (GatewayException $e) {
                        $error = 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                    } catch (UnsupportedTransactionException $e) {
                        $error = 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                    } catch (ApiException $e) {
                        $error = 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                    }

                }
                elseif ($paydata['transactionGateway'] == '8') {
                    /*Cybersource gateway refund transaction*/
                    $this->load->config('cyber_pay');
                    $option               = array();
                    $option['merchantID'] = trim($gt_result['gatewayUsername']);
                    $option['apiKey']     = trim($gt_result['gatewayPassword']);
                    $option['secretKey']  = trim($gt_result['gatewaySignature']);

                    if ($this->config->item('Sandbox')) {
                        $env = $this->config->item('SandboxENV');
                    } else {
                        $env = $this->config->item('ProductionENV');
                    }

                    $option['runENV'] = $env;

                    $commonElement = new CyberSource\ExternalConfiguration($option);

                    $config = $commonElement->ConnectionHost();

                    $merchantConfig = $commonElement->merchantConfigObject();
                    $apiclient      = new CyberSource\ApiClient($config, $merchantConfig);
                    $api_instance   = new CyberSource\Api\RefundApi($apiclient);

                    $cliRefInfoArr = [
                        "code" => "Refund Payment",
                    ];
                    $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    $amountDetailsArr             = [
                        "totalAmount" => $amount,
                        "currency"    => CURRENCY,
                    ];
                    $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                    $orderInfoArry = [
                        "amountDetails" => $amountDetInfo,
                    ];

                    $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                    $paymentRequestArr = [
                        "clientReferenceInformation" => $client_reference_information,
                        "orderInformation"           => $order_information,
                    ];

                    $paymentRequest = new CyberSource\Model\RefundPaymentRequest($paymentRequestArr);
                    $merchantID     = $this->merchantID;
                    $inID           = '';
                    $crtxnID      = '';
                    $ins_id       = '';
                    $tr1ID        = '';
                    $tr_type      = 'refund';
                    $api_response = list($response, $statusCode, $httpHeader) = null;
                    try
                    {

                        $api_response = $api_instance->refundPayment($paymentRequest, $tID);
                        if ($api_response[0]['status'] != "DECLINED" && $api_response[1] == '201') {
                            $status="success";
                            $message = "Transaction Refunded";

                            $trID1      = $api_response[0]['id'];
                            $msg        = $api_response[0]['status'];

                            $code = '200';
                            $refundTXNID = (isset($api_response[0]['id'])) ? $api_response[0]['id'] : $transactionID;
                            $result  = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID1);
                            $this->session->set_flashdata('success', $message);
                            
                        }
                    } catch (Cybersource\ApiException $e) {

                        $responsetext = $e->getMessage();
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $responsetext . '</div>');
                    }

                }
                elseif ($paydata['transactionGateway'] == '10') {
                    /*iTransact gateway refund transaction*/
                    
                    $apiUsername  = $gt_result['gatewayUsername'];
                    $apiKey  = $gt_result['gatewayPassword'];
                    $tr_type     = 'refund';
                    
                    $payload = [
                        'amount' => ($amount * 100)
                    ];
                    $sdk = new iTTransaction();
                    
                    $result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, "$tID");


                    $res = $result;
                    if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                        $result['status_code'] = '200';
                        $status="success";
                        $refundTXNID = (isset($result['id'])) ? $result['id'] : $transactionID;
                        $message = "Transaction Refunded";
                        $this->session->set_flashdata('success', $message);
                    }else{
                        $err_msg = $result['status'] = $result['error']['message'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $err_msg . '</div>');
                    }

                }
                elseif ($paydata['transactionGateway'] == '11' || $paydata['transactionGateway'] == '13') {
                    /*Fluidpay and BasysIQPro gateway*/
                    include APPPATH . 'third_party/Fluidpay.class.php';
                    $this->load->config('fluidpay');
                    $tr_type     = 'refund';
                
                    $gatewayTransaction              = new Fluidpay();
                    $gatewayTransaction->environment = $this->gatewayEnvironment;
                    $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                    $refundAmount = $amount * 100;
                    $payload = [
                        'amount' => round($refundAmount,2)
                    ];

                    $result = $gatewayTransaction->refundTransaction($tID, $payload);
                    $res = $result;
                    if ($result['status'] == 'success') {
                        $status="success";
                        $message = "Transaction Refunded";
                        $refundTXNID = (isset($result['data']['id'])) ? $result['data']['id'] : $transactionID;
                        $this->session->set_flashdata('success', $message);

                    }else{
                        $err_msg = $result['msg'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $err_msg . '</div>');
                    }

                }
                elseif($paydata['transactionGateway'] == '12') {
                    /* TSYS gateway refund transaction*/
                    
                    $this->load->config('TSYS');
                    $tr_type     = 'refund';
                
                    $deviceID = $gt_result['gatewayMerchantID'].'01';    

                    $gatewayTransaction              = new TSYS();
                    $gatewayTransaction->environment = $this->gatewayEnvironment;
                    $gatewayTransaction->deviceID = $deviceID;
                    $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                    $generateToken = '';
                    $responseErrorMsg = '';
                    if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $result['GenerateKeyResponse']['responseMessage'] . '</div>');
                        $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                    }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                        $generateToken = $result['GenerateKeyResponse']['transactionKey'];

                        
                    }
                    
                    $gatewayTransaction->transactionKey = $generateToken;
                    $payload = [
                        'amount' => ($amount * 100)
                    ];
                    $responseType = 'ReturnResponse';
                    if($generateToken != ''){ 
                        $result = $gatewayTransaction->refundTransaction($tID, $payload);
                    }else{
                        $responseType = 'GenerateKeyResponse';
                    }
                    
                    $res = $result;
                    
                    $result['responseType'] = $responseType;
                    if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
                    {
                        $status="success";
                        $refundTXNID = (isset($result[$responseType]['transactionID'])) ? $result[$responseType]['transactionID'] : $transactionID;
                        $message = "Transaction Refunded";
                        $this->session->set_flashdata('success', $message);
                    }else{
                        $err_msg = $result[$responseType]['responseMessage'];
                        if($responseErrorMsg != ''){
                            $err_msg = $responseErrorMsg;
                        }
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $err_msg . '</div>');
                    }

                }
                elseif($paydata['transactionGateway'] =='14') {
                    include APPPATH . 'third_party/Cardpointe.class.php';
                    
					$cardpointuser  = $gt_result['gatewayUsername'];
					$cardpointepass   = $gt_result['gatewayPassword'];
					$cardpointeMerchID = $gt_result['gatewayMerchantID'];
                    $cardpointeSiteName  = $gt_result['gatewaySignature'];
					$client = new Cardpointe();
                    $tr_type     = 'refund';

                    $refundAmount = $amount;
                    

					$result = $client->refund($cardpointeSiteName, $cardpointeMerchID, $cardpointuser, $cardpointepass, $tID, $amount);

                    if ($result['resptext'] == 'Approved' || $result['resptext'] == 'Approval' || $result['resptext'] == "Success") {
                        $status="success";
                        $message = "Transaction Refunded";
                        $refundTXNID = (isset($result['retref'])) ? $result['retref'] : $transactionID;
                        $this->session->set_flashdata('success', $message);

                    }else{
                        $err_msg = $result['resptext'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $err_msg . '</div>');
                    }

                }
                elseif($paydata['transactionGateway'] == '15') {
                    
                    $this->load->config('payarc');
                    $this->load->library('PayarcGateway');

                    $this->gatewayEnvironment = $this->config->item('environment');

                    // PayArc Payment Gateway, set enviornment and secret key
                    $this->payarcgateway->setApiMode($this->gatewayEnvironment);
                    $this->payarcgateway->setSecretKey($gt_result['gatewayUsername']);
                    
                    $charge_response = $this->payarcgateway->refundCharge($tID, ($amount * 100));

                    $result = json_decode($charge_response['response_body'], 1);
                    
                    $res = $result;
                    if (isset($result['data']) && $result['data']['status'] == 'refunded') {
                        $status="success";
                        $message = "Transaction Refunded";
                        $refundTXNID = (isset($result['data']['id'])) ? $result['data']['id'] : $tID;
                        $this->session->set_flashdata('success', $message);

                    }else{
                        // Error while creating the credit card token
                        $this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['env' => $this->gatewayEnvironment, 'trxnID' => $tID, 'amount' => ($amount * 100)], $result);
                        $err_msg = $result['message'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $err_msg . '</div>');
                    }

                }elseif($paydata['transactionGateway'] == '16') {
                    
                    $this->load->config('EPX');
                    $CUST_NBR = $gt_result['gatewayUsername'];
                    $MERCH_NBR = $gt_result['gatewayPassword'];
                    $DBA_NBR = $gt_result['gatewaySignature'];
                    $TERMINAL_NBR = $gt_result['extra_field_1'];
                    $amount = number_format($amount,2,'.',''); 
                    $transaction = array(
                        'CUST_NBR' => $CUST_NBR,
                        'MERCH_NBR' => $MERCH_NBR,
                        'DBA_NBR' => $DBA_NBR,
                        'TERMINAL_NBR' => $TERMINAL_NBR,
                        'ORIG_AUTH_GUID' => $tID,
                        'CARD_ENT_METH' => 'Z',
                        'INDUSTRY_TYPE' => 'E',
                        'AMOUNT' => $amount,
                        'TRAN_NBR' => rand(1,10),
                        'BATCH_ID' => time(),
                        'VERBOSE_RESPONSE' => 'Y',
                    );
                    // transaction type based on payment type
                    if($echeckType == 1){
                        if($accountType == 'savings'){
                            $txnType = 'CKS3';
                        }else{
                            $txnType = 'CKC3';
                        }
                    }else{
                        $txnType = 'CCE9';
                        $transaction['CARD_ENT_METH'] = 'Z';
                        $transaction['INDUSTRY_TYPE'] = 'E';
                    }
                    $transaction['TRAN_TYPE'] = $txnType;
                    

                    $gatewayTransaction              = new EPX();
                    $result = $gatewayTransaction->processTransaction($transaction);
                    
                    $res = $result;
                    
                    if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                        $status="success";
                        $message = "Transaction Refunded";
                        $refundTXNID = (isset($result['AUTH_GUID'])) ? $result['AUTH_GUID'] : $tID;
                        $this->session->set_flashdata('success', $message);

                    }else{
                       
                        $err_msg = $result['AUTH_RESP_TEXT'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $err_msg . '</div>');
                    }

                }
                elseif($paydata['transactionGateway'] == '17') {

                    $this->load->config('maverick');
                    $this->load->library('MaverickGateway');

                    // Maverick Payment Gateway
                    $this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
                    $this->maverickgateway->setTerminalId($gt_result['gatewayPassword']);
                    $this->maverickgateway->setAccessToken($gt_result['gatewayUsername']);

                    $isEcheck =  false;
                    // eCheck Partial is false in default
                    if($isEcheck) {
                        $r = $this->maverickgateway->refundAchSale($tID, $amount);
                    } else {
                        $r = $this->maverickgateway->refundSale($tID, $amount);
                    }

                    $rbody = json_decode($r['response_body'], 1);
                
                    $result = [];
                    $result['data'] = $rbody;
                    
                    $result['response_code'] = $r['response_code'];

                    if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                        if($rbody['status']['status'] == 'Approved'){
                            $status="success";
                            $message = "Transaction Refunded";
                            $refundTXNID = (isset($result['data']['id'])) ? $result['data']['id'] : $tID;
                            $this->session->set_flashdata('success', $message);
                        } else {
                            $status = 'failed';
                            $message = 'Payment refund failed.';    

                            
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $message . '</div>');
                        }
                    } else {
                        $status = 'failed';
                        $message = $rbody['message'];

                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' . $message . '</div>');
                    }

                    $result['status'] = $status;
                    $result['message'] = $message;
                }


                /* After success*/
                if("SUCCESS" == strtoupper($status)){
                    $payIndex = 0;

                    if(!empty($invoiceIDs))
                    {
                        $ins_id   = '';
                        foreach($invoiceIDs as $inID)
                        {
                            $trID = $parent_id = $txnRowIDArr[$payIndex];
                            $invoiceActualRefundAmount = $invoiceRefundAmounts[$payIndex];

                            $refundRemainingAmountInvoice = $refundRemainingAmount[$payIndex];

                            if($invoiceActualRefundAmount > 0){
                                
                                $id = $this->general_model->insert_gateway_transaction_data($result,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$invoiceActualRefundAmount,$this->merchantID,$crtxnID='', $this->resellerID,$inID,$echeckType,$this->transactionByUser,$custom_data_fields,$parent_id);

                                
                                $this->invoiceUpdate($inID,$invoiceActualRefundAmount,$integrationType,$this->merchantID,$paydata['qbListTxnID'],$tID,$refundRemainingAmountInvoice);

                                if($integrationType == 2){
                                    $comp_data_user = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
                                    
                                    $refnd_trr = array('merchantID' => $merchantID, 'refundAmount' => $invoiceActualRefundAmount,
                                        'creditInvoiceID'               => $inID, 'creditTransactionID'       => $tID,
                                        'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
                                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);


                                    $user_id = $merchantID;
                                    $user    = $comp_data_user['qbwc_username'];
                                    $comp_id = $comp_data_user['id'];
                                    $ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                                    $refund  = $invoiceActualRefundAmount;
                                    $ins_data['customerID'] = $customerID;
                                    if (!empty($ittem)) {
                                        $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchantID));
                                        $new_inv_no = $inID;
                                        $inv_po = 'CZ000';
                                        if (!empty($in_data)) {
                                            $inv_pre    = $in_data['prefix'];
                                            $inv_po     = $in_data['postfix'] + 1;
                                            $new_inv_no = $inv_pre . $inv_po;
                                        }

                                        $ins_data['merchantDataID']    = $merchantID;
                                        $ins_data['creditDescription'] = "Credit as Refund";
                                        $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                                        $ins_data['creditDate']        = date('Y-m-d H:i:s');
                                        $ins_data['creditAmount']      = $invoiceActualRefundAmount;
                                        $ins_data['creditNumber']      = $new_inv_no;
                                        $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                                        $ins_data['Type']              = "Payment";
                                        $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                                        $item['itemListID']      = $ittem['ListID'];
                                        $item['itemDescription'] = $ittem['Name'];
                                        $item['itemPrice']       = $invoiceActualRefundAmount;
                                        $item['itemQuantity']    = 0;
                                        $item['crlineID']        = $ins_id;
                                        $acc_name                = $ittem['DepositToAccountName'];
                                        $acc_ID                  = $ittem['DepositToAccountRef'];
                                        $method_ID               = $ittem['PaymentMethodRef'];
                                        $method_name             = $ittem['PaymentMethodName'];
                                        $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                                        $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                                        
                                        if ($ins_id && $ins) {
                                            $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

                                            $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $ins_id, '1', '', $user);
                                        }

                                    }
                                }
                                
                                

                            }
                            $payIndex++; 
                        }
                        
                     
                    }else{
                        /*Without Invoice Refund*/
                        $trID = $parent_id = $txnRowIDArr[$payIndex];
                        $invoiceActualRefundAmount = $invoiceRefundAmounts[$payIndex];

                        if($invoiceActualRefundAmount > 0){
                            $id = $this->general_model->insert_gateway_transaction_data($result,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$invoiceActualRefundAmount,$this->merchantID,$crtxnID='', $this->resellerID,'',$echeckType,$this->transactionByUser,$custom_data_fields,$parent_id);
                        }
                    }

                    $this->sendRefundEmail($user_id, $paydata, $integrationType, $amount, $refundTXNID);
                }else{
                    $refundTXNID = 'TXNFAILED-'.time();
                }

                $customerData = $this->general_model->getCustomerDetails($customerID,$this->merchantID,$integrationType);
                $checkPlan = check_free_plan_transactions();

                $receipt_data = array(
                    'transaction_id' => $refundTXNID,
                    'IP_address' => getClientIpAddr(),
                    'billing_name' => $customerData['firstName'].' '.$customerData['lastName'],
                    'billing_address1' => $customerData['address1'],
                    'billing_address2' => $customerData['address2'],
                    'billing_city' => $customerData['City'],
                    'billing_zip' => $customerData['zipCode'],
                    'billing_state' => $customerData['State'],
                    'billing_country' => $customerData['Country'],
                    'shipping_name' => $customerData['firstName'].' '.$customerData['lastName'],
                    'shipping_address1' => $customerData['ship_address1'],
                    'shipping_address2' => $customerData['ship_address2'],
                    'shipping_city' => $customerData['ship_city'],
                    'shipping_zip' => $customerData['ship_zipcode'],
                    'shipping_state' => $customerData['ship_state'],
                    'shiping_counry' => $customerData['ship_country'],
                    'Phone' => $customerData['phoneNumber'],
                    'Contact' => $customerData['userEmail'],
                    'proccess_url' => '',
                    'proccess_btn_text' => 'Process New Sale',
                    'sub_header' => 'Sale',
                    'refundAmount' => $amount,
                    'checkPlan' => $checkPlan
                );
                
                
                $this->session->set_userdata("invoice_IDs", $invoiceIDs);
                
                if($integrationType == 1){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'QBO_controllers/Payments/create_customer_esale'; 
                    }else{
                        $receipt_data['proccess_url'] = 'QBO_controllers/Payments/create_customer_sale'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);

                    redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
                }elseif($integrationType == 2){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'Payments/create_customer_esale'; 
                    }else{
                        $receipt_data['proccess_url'] = 'Payments/create_customer_sale'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);

                    redirect('home/transation_sale_receipt',  'refresh');
                    
                }elseif($integrationType == 3){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'FreshBooks_controllers/Transactions/create_customer_esale'; 
                    }else{
                        $receipt_data['proccess_url'] = 'FreshBooks_controllers/Transactions/create_customer_sale'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);
                    
                    redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
                }elseif($integrationType == 4){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'Integration/Payments/create_customer_esale'; 
                    }else{
                        $receipt_data['proccess_url'] = 'Integration/Payments/create_customer_sale'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);
                    
                    redirect('Integration/Payments/transation_sale_receipt',  'refresh');
                }else{
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'company/Payments/create_customer_esale'; 
                    }else{
                        $receipt_data['proccess_url'] = 'company/Payments/create_customer_sale'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);
                    
                    redirect('company/home/transation_sale_receipt',  'refresh');
                }
            }
        }
    }
    
    public function invoiceUpdate($inID,$invoiceActualRefundAmount,$integrationType,$merchantID,$qbListTxnID='',$tID,$refundRemainingAmountInvoice)
    {
        $condition1 = array('merchantID'=> $merchantID, 'transactionID'=>$tID );
            
        $payData = $this->general_model->get_row_data('customer_transaction',$condition1);
        $custom_data = isset($payData['custom_data_fields']) ? $payData['custom_data_fields'] : [];
        
        $surchargePer = 0;
        $surchargeVal = 0;
        if($custom_data){
            $json_data = json_decode($custom_data, 1);
            if(isset($json_data['invoice_surcharge'])){
                $surchargePer = $json_data['invoice_surcharge'];
            }
            if(isset($json_data['surcharge_amount_value'])){
                $surchargeVal = $json_data['surcharge_amount_value'];
            }
        }
        
        if($integrationType == 1){
            
            $condition  = array('invoiceID'=> $inID,'merchantID'=> $merchantID );  
            $invoiceData = $this->general_model->get_row_data('QBO_test_invoice',$condition);
            $customerID = $invoiceData['CustomerListID'];
            $dataUpdate = [];
            
            $Total_payment = $invoiceData['Total_payment'];
            $amountPaid = $Total_payment - $invoiceData['BalanceRemaining'];
            $AppliedAmount = $amountPaid - $invoiceActualRefundAmount;

            $amountRemaining = $invoiceData['BalanceRemaining'] + $invoiceActualRefundAmount - $surchargeVal;

            $dataUpdate['AppliedAmount'] = round($AppliedAmount,2);
            $dataUpdate['BalanceRemaining'] = round($amountRemaining,2);
            
            
            $dataUpdate['IsPaid'] = 0;

            $this->general_model->update_row_data('QBO_test_invoice',$condition, $dataUpdate);
             
            $val = array('merchantID' => $merchantID);
            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];

            $dataService = DataService::Configure(array(
                'auth_mode' => $this->config->item('AuthMode'),
                'ClientID'  => $this->config->item('client_id'),
                'ClientSecret' => $this->config->item('client_secret'),
                'accessTokenKey' =>  $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID' => $realmID,
                'baseUrl' => $this->config->item('QBOURL'),
            ));
            
            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");
           
            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {   
                $this->load->model('QBO_models/qbo_invoices_model');
                $invoice_items = $this->qbo_invoices_model->get_item_data_new(array('invoiceID' => $inID, 'merchantID' => $merchantID, 'releamID' => $realmID));
                $lineArray = [];
                $total_invoice_amount = 0;
                foreach ($invoice_items as $item) {
                    $amount = $item['itemPrice'] * $item['itemQty'];
                    $total_invoice_amount += $amount;

                    if($item['itemDescription'] == "Surcharge Fees"){

                        if($dataUpdate['AppliedAmount'] > 0){
                            $new_item_amount = $item['itemPrice'] - $surchargeVal;
                            $total_invoice_amount = $total_invoice_amount - $surchargeVal;
                            $amount = $item['itemPrice'] = $new_item_amount;

                        }else{
                            $total_invoice_amount = $total_invoice_amount - $item['itemPrice'];
                            $amount = $item['itemPrice'] = 0;
                        }
                    }

                    $LineObj = Line::create([
                        "Amount" => $amount,
                        "DetailType" => "SalesItemLineDetail",
                        "Description" => $item['itemDescription'],
                        "SalesItemLineDetail" => [
                            "ItemRef" => $item['itemRefID'],
                            "Qty" => $item['itemQty'],
                            "UnitPrice" => $item['itemPrice'],
                            "TaxCodeRef" => [
                                "value" => (isset($item['itemTax']) && $item['itemTax']) ? "TAX" : "NON",
                            ]

                        ],
                    ]);
                    $lineArray[] = $LineObj;
                }
                $theInvoice = current($targetInvoiceArray);
                $theResourceObj = Invoice::update($theInvoice, [
                    "Balance" => $total_invoice_amount,
                    "TotalAmt" => $total_invoice_amount,
                    "Line" => $lineArray,
                    "sparse" => true,
                    "MetaData" => [
                        "LastUpdatedTime" => date('Y-m-d') . 'T' . date('H:i:s'),
                    ]
                ]);
                $resultingObj = $dataService->Update($theResourceObj);
                $error = $dataService->getLastError();
                if(!empty($payData['qbListTxnID'])){
                    $newPaymentObj = Payment::create([
                        "Id" => $payData['qbListTxnID'],
                        "SyncToken" => "0"
                    ]);
                    $deletePayment = $dataService->Delete($newPaymentObj);
                }
                
                $transactionAmount = $refundRemainingAmountInvoice - $invoiceActualRefundAmount;
                if($transactionAmount > 0){

                    $newPaymentObj = Payment::create([
                        "TotalAmt" => $transactionAmount,
                        "SyncToken" => 1, 
                        "CustomerRef" => $customerID,
                        "Line" => [
                            "LinkedTxn" => [
                                "TxnId" => $inID,
                                "TxnType" => "Invoice",
                            ],
                            "Amount" => $transactionAmount
                        ]
                    ]);
                    
                    $savePayment = $dataService->Add($newPaymentObj);
                   
                    $crtxnID =  $savePayment->Id;
                   
                    if($crtxnID != ''){
                        $condition = array('id' => $payData['id']);
                        $update_data =   array('qbListTxnID' => $crtxnID);

                        $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                    }
                }
                
            }

        }elseif($integrationType == 2){
            $invoice_lineitem_amount = 0;
            $condition  = array('TxnID'=>$inID,'qb_inv_merchantID'=> $merchantID );  
            $invoiceData = $this->general_model->get_row_data('qb_test_invoice',$condition);
            $dataUpdate = [];
            $dataUpdate['AppliedAmount'] = abs($invoiceData['AppliedAmount']) - $invoiceActualRefundAmount;
            $dataUpdate['BalanceRemaining'] = $invoiceData['BalanceRemaining'] + $invoiceActualRefundAmount;
            $dataUpdate['IsPaid'] = 'false';
           
            $item_where = ['inv_itm_merchant_id' => $merchantID, 'Descrip' => 'Surcharge Fees', 'TxnID' => $inID];
            $invoice_lineitem = $this->general_model->get_select_data('qb_test_invoice_lineitem', ['Rate'], $item_where);
            if($invoice_lineitem){
                if($invoice_lineitem['Rate'] > 0){
                    
                    if($dataUpdate['AppliedAmount'] > 0){

                        $new_item_amount = $invoice_lineitem['Rate'] - $surchargeVal;
                        $this->general_model->update_row_data('qb_test_invoice_lineitem', $item_where, ['Rate' => $new_item_amount]);
                        $dataUpdate['BalanceRemaining'] = $dataUpdate['BalanceRemaining'] - $surchargeVal;
                    }else{
                        $this->general_model->update_row_data('qb_test_invoice_lineitem', $item_where, ['Rate' => 0]);
                        $dataUpdate['BalanceRemaining'] = $dataUpdate['BalanceRemaining'] - $invoice_lineitem['Rate'];
                    }
                }
            }
            $this->general_model->update_row_data('qb_test_invoice',$condition, $dataUpdate);

        }elseif($integrationType == 3){
            $updatedTXNAmount = $refundRemainingAmountInvoice - $invoiceActualRefundAmount;
            $freshbookSync = new Freshbooks_data($this->merchantID, $this->resellerID);
            $invoicePayment    = $freshbookSync->update_invoice_payment($inID, $invoiceActualRefundAmount, $qbListTxnID, $updatedTXNAmount);
        }elseif($integrationType == 4){
            $condition  = array('invoiceID'=>$inID,'merchantID'=> $merchantID );  
            $invoiceData = $this->general_model->get_row_data('Xero_test_invoice',$condition);
            $dataUpdate = [];
            
            $Total_payment = $invoiceData['Total_payment'];
            $amountPaid = $Total_payment - $invoiceData['BalanceRemaining'];
            $AppliedAmount = $amountPaid - $invoiceActualRefundAmount;

            $dataUpdate['AppliedAmount'] = $amountPaid;
            $dataUpdate['BalanceRemaining'] = $invoiceData['BalanceRemaining'] + $invoiceActualRefundAmount - $surchargeVal;

            $data['IsPaid'] = 0;
            
            $this->general_model->update_row_data('Xero_test_invoice',$condition, $data);
            
            $xeroSync = new XeroSync($this->merchantID);

            $deletTransaction = [
                'paymentId' => $qbListTxnID,
                'storeTXN'  => false,
            ];
            $deletePayment = $xeroSync->deletePayment($deletTransaction);
            
            $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $this->merchantID, 'isPaymentAccepted' => 1]);
            $transactionAmount = $refundRemainingAmountInvoice - $invoiceActualRefundAmount;
            if($transactionAmount > 0){

                $tr_date           = date('Y-m-d H:i:s');
                $updateInvoiceData = [
                    'invoiceID'       => $inID,
                    'accountCode'     => $accountData['accountCode'],
                    'trnasactionDate' => $tr_date,
                    'amount'          => $transactionAmount,
                ];
                
                $crtxnID  = $xeroSync->createPayment($updateInvoiceData);
                if($crtxnID != ''){
                    $condition = array('id' => $payData['id']);
                    $update_data =   array('qbListTxnID' => $crtxnID);

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                }
            }
            
        }elseif($integrationType == 5){
            $condition  = array('TxnID'=>$inID );  
            $invoiceData = $this->general_model->get_row_data('chargezoom_test_invoice',$condition);
            $dataUpdate = [];
            $dataUpdate['AppliedAmount'] = abs($invoiceData['AppliedAmount']) - $invoiceActualRefundAmount;
            $dataUpdate['BalanceRemaining'] = $invoiceData['BalanceRemaining'] + $invoiceActualRefundAmount;
            $dataUpdate['IsPaid'] = 'false';
            $invoice_lineitem = $this->general_model->get_select_data('chargezoom_test_invoice_lineitem', ['Rate'], array('TxnID' => $inID, 'Descrip' => 'Surcharge Fees'));
            if(isset($invoice_lineitem['Rate']) && $invoice_lineitem['Rate'] > 0){

                
                if($dataUpdate['AppliedAmount'] > 0){

                    $new_item_amount = $invoice_lineitem['Rate'] - $surchargeVal;
                    $this->general_model->update_row_data('chargezoom_test_invoice_lineitem', array('TxnID' => $inID, 'Descrip' => 'Surcharge Fees'), ['Rate' => $new_item_amount]);
                    $dataUpdate['BalanceRemaining'] = $dataUpdate['BalanceRemaining'] - $surchargeVal;
                }else{
                    $this->general_model->update_row_data('chargezoom_test_invoice_lineitem', array('TxnID' => $inID, 'Descrip' => 'Surcharge Fees'), ['Rate' => 0]);
                    $dataUpdate['BalanceRemaining'] = $dataUpdate['BalanceRemaining'] - $invoice_lineitem['Rate'];
                }
            }
            $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $dataUpdate);
        }else{
             
        }

        return true;
    }
    public function totalToPartialRefundInvoice()
    {
        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        include APPPATH . 'third_party/PayTraceAPINEW.php';
        include APPPATH . 'third_party/PayPalAPINEW.php';
        include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
        require_once APPPATH . "third_party/usaepay/usaepay.php";
        require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
        include APPPATH . 'third_party/Fluidpay.class.php';

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $user_id                = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id                = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $merchantID = $user_id;
        $refundTXNID = 'TXNFAILED'.time();
        if (!empty($this->input->post(null, true))) {

            $refundIntegrationType =  $this->czsecurity->xssCleanPostInput('refundIntegrationType',true);

            $refundTotalamountInput =  $this->czsecurity->xssCleanPostInput('refundTotalInvoiceAmountInput',true);

            $invoiceID = $this->czsecurity->xssCleanPostInput('refundInvoiceID',true);

            $refundRemainingAmount = $this->czsecurity->xssCleanPostInput('refundInvoiceRemainingAmount',true);

            $transactionData = $this->general_model->getInvoiceSuccessTransaction($invoiceID,$merchantID,$refundIntegrationType);
            $status = '';
            $remainingPay = $refundTotalamountInput;

            $refundedAmount = 0;
            if(count($transactionData) > 0){
                $remainingPay = $refundTotalamountInput;
                foreach ($transactionData as $txn) {
                    $txnAmount = $txn['transactionAmount'];
                    if($txnAmount > 0){
                        $gatewayName =  $txn['gateway'];
                        $transactionGateway =  $txn['transactionGateway'];
                        $gatlistval = $txn['gatewayID'];
                        $tID       = $txn['transactionID']; 
                        $customerID = $txn['customerListID'];
                        $amount = 0;
                        
                        if (strpos($gatewayName, 'ECheck') !== false) {
                            $echeckType = true;
                        }else{
                            $echeckType = false;
                        }
                        

                        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));

                        if( ($remainingPay <= $txnAmount) && ($remainingPay > 0) ){
                            $remaingTxnAmount = $txnAmount - $remainingPay;

                            $restype = "Partial";
                            $amount = $remainingPay;
                            
                            $remainingPay = 0;

                        }else if($remainingPay > $txnAmount){
                            $restype = "Full";
                            $remainingPay = $remainingPay - $txnAmount;
                            $amount = $txnAmount;
                            $remaingTxnAmount = 0;
                        }else{
                            $restype = "Full";
                            $remainingPay = 0;
                            $amount = 0;
                            $remaingTxnAmount = 0;
                        }
                    }else{
                       $amount = 0; 
                    }
                    
                    
                    if($amount > 0){
                        /* NMI and Chargezoom Gateway*/
                        if($transactionGateway =='1' || $transactionGateway =='9')
                        {
                            
                            
                            $nmiuser  = $gt_result['gatewayUsername'];
                            $nmipass  =  $gt_result['gatewayPassword'];
                            $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
                          
                            $transaction = new nmiDirectPost($nmi_data);
                            
                            $transaction->setTransactionId($tID); 
                            if($echeckType){
                                $transaction->setPayment('check');
                            }
                            $transaction->refund($tID,$amount);
                            $result     = $transaction->execute();

                            
                            if($result['response_code'] == '100')
                            {  
                                $refundTXNID = $result['transactionid'];
                                $status="success";
                                $message = "Transaction Refunded";
                                
                            }
                            else
                            {   
                                $status="error";
                                $message = $result['responsetext'];
                            }
                        }
                        elseif ($transactionGateway == '2')
                        {
                            /*Authorize,net gateway refund*/
                            $this->load->config('auth_pay');
                            

                            $apiloginID     = $gt_result['gatewayUsername'];
                            $transactionKey = $gt_result['gatewayPassword'];
                            $transaction    = new AuthorizeNetAIM($apiloginID, $transactionKey);
                            $transaction->setSandbox($this->config->item('auth_test_mode'));
                            $merchantID = $txn['merchantID'];

                            $card       = $txn['transactionCard'];
                            
                            if($echeckType){
                                $transaction->__set('method','echeck');
                            }
                            $result = $transaction->credit($tID, $amount, $card);

                            $res     = $result;
                            $tr_type = 'refund';
                            if ($result->response_code == '1') {
                                $refundTXNID = ($result->transaction_id) ? $result->transaction_id : $transactionID;
                                $status="success";
                                $message = "Transaction Refunded";
                                
                            }else{
                                $status="error";
                                $message = $result->response_reason_text;
                                
                            }
                        }
                        elseif ($transactionGateway == '3') 
                        {
                            /*PAytrace refund gateway*/
                            $this->load->config('paytrace');
                           
                            $payusername = $gt_result['gatewayUsername'];
                            $paypassword = $gt_result['gatewayPassword'];
                            $grant_type  = "password";
                            $payAPI      = new PayTraceAPINEW();
                            $merchantID  = $txn['merchantID'];

                            $card = $txn['transactionCard'];

                            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                            //call a function of Utilities.php to verify if there is any error with OAuth token.
                            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                            if (!$oauth_moveforward) {
                                $json = $payAPI->jsonDecode($oauth_result['temp_json_response']);
                                $oauth_token = sprintf("Bearer %s", $json['access_token']);
                                $total        = $amount;
                                $request_data = array("transaction_id" => $tID, 'amount' => $total);
                                $url = URL_TRID_REFUND;
                                
                                if($echeckType){
                                    $request_data = array(
                                        "check_transaction_id" => $tID,
                                        "integrator_id" => $gt_result['gatewaySignature'],
                                        'amount' => $total,
                                    );
                                    $url = URL_ACH_REFUND_TRANSACTION;
                                }
                                $request_data                 = json_encode($request_data);
                                $result                       = $payAPI->processTransaction($oauth_token, $request_data, $url);
                                $response                     = $payAPI->jsonDecode($result['temp_json_response']);
                                $response['http_status_code'] = $result['http_status_code'];
                                $tr_type                      = 'pay_refund';

                                if ($result['http_status_code'] == '200') {
                                    $status="success";
                                    $refundTXNID = (isset($response['transaction_id'])) ? $response['transaction_id'] : $transactionID;
                                    $message = "Transaction Refunded";
                                    
                                }else{
                                    if (!empty($response['errors'])) {
                                        $err_msg = $this->getError($response['errors']);
                                    } else {
                                        $err_msg = $approval_message;
                                    }
                                    $status="error";
                                    $message = $err_msg;

                                }

                            }else{
                                $status="error";
                                $message = 'Gateway Authentication failed';
                            }
                        }
                        elseif ($transactionGateway == '4') 
                        {
                            /* Paypal payment refund */
                            $this->load->config('paypal');
                            
                            $username  = $gt_result['gatewayUsername'];
                            $password  = $gt_result['gatewayPassword'];
                            $signature = $gt_result['gatewaySignature'];
                            $config = array(
                                'Sandbox'      => $this->config->item('Sandbox'), // Sandbox / testing mode option.
                                'APIUsername'  => $username, // PayPal API username of the API caller
                                'APIPassword'  => $password, // PayPal API password of the API caller
                                'APISignature' => $signature, // PayPal API signature of the API caller
                                'APISubject'   => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                                'APIVersion'   => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                            );

                            if ($config['Sandbox']) {
                                error_reporting(E_ALL);
                                ini_set('display_errors', '1');
                            }
                            $this->load->library('paypal/Paypal_pro', $config);

                            $RTFields = array(
                                'transactionid'       => $tID, 
                                'payerid'             => '', 
                                'invoiceid'           => '',
                                'refundtype'          => $restype, 
                                'amt'                 => $amount,
                                'currencycode'        => '', 
                                'note'                => '',
                                'retryuntil'          => '', 
                                'refundsource'        => '', 
                                'merchantstoredetail' => '', 
                                'refundadvice'        => '', 
                                'refunditemdetails'   => '', 
                                'msgsubid'            => '', 
                                'storeid'             => '',
                                'terminalid'          => '',
                            );

                            $PayPalRequestData = array('RTFields' => $RTFields);

                            $PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);

                            $result     = $PayPalResult;
                            $tr_type = 'Paypal_refund';
                            if ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
                                $status="success";
                                $refundTXNID = (isset($PayPalResult['REFUNDTRANSACTIONID'])) ? $PayPalResult['REFUNDTRANSACTIONID'] : $transactionID;
                                $message = "Transaction Refunded";
                                 
                            } else {
                                $responsetext = $PayPalResult['L_LONGMESSAGE0'];
                                $code         = '401';
                                $status="error";
                                $message = $responsetext;
                               
                            }

                        }
                        elseif($transactionGateway == '5') 
                        {
                            /*Stripe refund transaction*/
                            
                            $nmiuser = $gt_result['gatewayUsername'];
                            $nmipass = $gt_result['gatewayPassword'];

                            $plugin = new ChargezoomStripe();
                            $plugin->setApiKey($nmipass);
                            $charge = \Stripe\Refund::create(array(
                                "charge" => $tID,
                                "amount"=> (int)($amount*100),
                            ));
                            $charge = json_encode($charge);
                            $result = json_decode($charge);

                            $res     = $result;
                            $tr_type = 'stripe_refund';

                            if (strtoupper($result->status) == strtoupper('succeeded')) {
                                $result->paid = 1;
                                $result->failure_code = '';
                                $refundTXNID = (isset($result->id)) ? $result->id : $transactionID;
                                $status="success";
                                $message = "Transaction Refunded";
                                
                            }else{
                                $responsetext = $result->status;
                                $result->paid = 0;
                                $result->failure_code = 400;

                                $status="error";
                                $message = $responsetext;
                            }
                        }
                        elseif ($transactionGateway == '6') 
                        {
                            /*USAePay Refund Transaction */
                            $this->load->config('usaePay');
                            
                            $payusername = $gt_result['gatewayUsername'];
                            $paypassword = $gt_result['gatewayPassword'];
                            $crtxnID = '';

                            $transaction                      = new umTransaction;
                            $transaction->ignoresslcerterrors = ($this->config->item('ignoresslcerterrors') !== null) ? $this->config->item('ignoresslcerterrors') : true;

                            $transaction->key        = $payusername; // Your Source Key
                            $transaction->pin        = $paypassword; // Source Key Pin
                            $transaction->usesandbox = $this->config->item('Sandbox'); // Sandbox true/false
                            $transaction->testmode   = $this->config->item('TESTMODE'); // Change this to 0 for the transaction to process
                            $transaction->command    = "refund"; // refund command to refund transaction.
                            $transaction->refnum     = $tID; // Specify refnum of the transaction that you would like to capture.
                            $transaction->amount     = $amount;
                            
                            $merchantID  = $txn['merchantID'];
                            $transaction->Process();
                            $trID1 = '';
                            if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') 
                            {
                                $status="success";
                                $message = "Transaction Refunded";

                                $refundTXNID = (isset($transaction->transactionId)) ? $transaction->transactionId : $transactionID;

                            }else{
                                $responsetext = $transaction->result;
                                $status="error";
                                $message = $responsetext;
                                
                            }
                        }
                        elseif ($transactionGateway == '7') 
                        {
                            /*Heartland Gateway Refund*/
                            $this->load->config('globalpayments');
                            
                            $secretApiKey = $gt_result['gatewayPassword'];
                            $crtxnID    = '';
                            $merchantID  = $txn['merchantID'];
                            $config     = new PorticoConfig();

                            $config->secretApiKey = $secretApiKey;
                            $config->serviceUrl   = $this->config->item('GLOBAL_URL');

                            ServicesContainer::configureService($config);
                            try
                            {

                                $response = Transaction::fromId($tID)

                                    ->refund($amount)
                                    ->withCurrency("USD")
                                    ->execute();

                                $tr1ID = '';
                                if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
                                    
                                    $refundTXNID = (isset($response->transactionId)) ? $response->transactionId : $transactionID;
                                    $authCode = $response->authorizationCode;
                                    $status="success";
                                    $message = "Transaction Refunded";
                                    $result = array('transactionCode' => '200', 'status' => $response->responseMessage, 'transactionId' => $refundTXNID, 'authCode' => $authCode);

                                } else {
                                    $responsetext   = $response->responseMessage;
                                    $status="error";
                                    $message = $responsetext;

                                }

                            } catch (BuilderException $e) {
                                
                                $status="error";
                                $message = $e->getMessage();
                            } catch (ConfigurationException $e) {
                                
                                $status="error";
                                $message = $e->getMessage();
                            } catch (GatewayException $e) {
                                $status="error";
                                $message = $e->getMessage();
                            } catch (UnsupportedTransactionException $e) {
                                $status="error";
                                $message = $e->getMessage();
                            } catch (ApiException $e) {
                                $status="error";
                                $message = $e->getMessage();
                            }

                        }
                        elseif ($transactionGateway == '8') 
                        {
                            /*Cybersource gateway refund transaction*/
                            $this->load->config('cyber_pay');
                            $option               = array();
                            $option['merchantID'] = trim($gt_result['gatewayUsername']);
                            $option['apiKey']     = trim($gt_result['gatewayPassword']);
                            $option['secretKey']  = trim($gt_result['gatewaySignature']);

                            if ($this->config->item('Sandbox')) {
                                $env = $this->config->item('SandboxENV');
                            } else {
                                $env = $this->config->item('ProductionENV');
                            }

                            $option['runENV'] = $env;

                            $commonElement = new CyberSource\ExternalConfiguration($option);

                            $config = $commonElement->ConnectionHost();

                            $merchantConfig = $commonElement->merchantConfigObject();
                            $apiclient      = new CyberSource\ApiClient($config, $merchantConfig);
                            $api_instance   = new CyberSource\Api\RefundApi($apiclient);

                            $cliRefInfoArr = [
                                "code" => "Refund Payment",
                            ];
                            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                            $amountDetailsArr             = [
                                "totalAmount" => $amount,
                                "currency"    => CURRENCY,
                            ];
                            $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                            $orderInfoArry = [
                                "amountDetails" => $amountDetInfo,
                            ];

                            $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                            $paymentRequestArr = [
                                "clientReferenceInformation" => $client_reference_information,
                                "orderInformation"           => $order_information,
                            ];

                            $paymentRequest = new CyberSource\Model\RefundPaymentRequest($paymentRequestArr);
                            $merchantID  = $txn['merchantID'];
                            $inID           = '';
                            
                            $crtxnID      = '';
                            $ins_id       = '';
                            $tr1ID        = '';
                            $tr_type      = 'refund';
                            $api_response = list($response, $statusCode, $httpHeader) = null;
                            try
                            {

                                $api_response = $api_instance->refundPayment($paymentRequest, $tID);
                                if ($api_response[0]['status'] != "DECLINED" && $api_response[1] == '201') {
                                    $status="success";
                                    $message = "Transaction Refunded";
                                    $refundTXNID = (isset($api_response[0]['id'])) ? $api_response[0]['id'] : $transactionID;
                                }else{
                                    $status="error";
                                    $message = $api_response[0]['status'];
                                }
                            } catch (Cybersource\ApiException $e) {

                                $responsetext = $e->getMessage();
                                $status="error";
                                $message = $responsetext;
                                
                            }

                        }
                        elseif ($transactionGateway == '10') 
                        {
                            /*iTransact gateway refund transaction*/
                            
                            $apiUsername  = $gt_result['gatewayUsername'];
                            $apiKey  = $gt_result['gatewayPassword'];
                            $tr_type     = 'refund';
                            
                            $payload = [
                                'amount' => ($amount * 100)
                            ];
                            $sdk = new iTTransaction();
                            
                            $result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, "$tID");


                            $res = $result;
                            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                                
                                $status="success";
                                $refundTXNID = (isset($result['id'])) ? $result['id'] : $transactionID;
                                $message = "Transaction Refunded";
                                
                            }else{
                                $err_msg = $result['status'] = $result['error']['message'];
                                $status="error";
                                $message = $err_msg;
                                
                            }

                        }
                        elseif ($transactionGateway == '11' || $transactionGateway == '13') {
                            /*Fluidpay and BasysIQPro gateway*/
                            
                            $this->load->config('fluidpay');
                            $tr_type     = 'refund';
                        
                            $gatewayTransaction              = new Fluidpay();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                            $refundAmount = $amount * 100;
                            $payload = [
                                'amount' => round($refundAmount,2)
                            ];

                            $result = $gatewayTransaction->refundTransaction($tID, $payload);
                            $res = $result;
                            if ($result['status'] == 'success') {
                                $status="success";
                                $message = "Transaction Refunded";
                                $refundTXNID = (isset($result['data']['id'])) ? $result['data']['id'] : $transactionID;
                                

                            }else{
                                $err_msg = $result['msg'];
                                $status="error";
                                $message = $err_msg;
                            }

                        }
                        elseif($transactionGateway == '12') 
                        {
                            /* TSYS gateway refund transaction*/

                            $this->load->config('TSYS');
                            $tr_type     = 'refund';
                        
                            $deviceID = $gt_result['gatewayMerchantID'].'01';    

                            $gatewayTransaction              = new TSYS();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->deviceID = $deviceID;
                            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                            $generateToken = '';
                            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                                $status="error";
                                $message = $result['GenerateKeyResponse']['responseMessage'];
                                
                            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){

                                $generateToken = $result['GenerateKeyResponse']['transactionKey'];

                                
                            }
                            
                            $gatewayTransaction->transactionKey = $generateToken;
                            $payload = [
                                'amount' => ($amount * 100)
                            ];
                            $responseType = 'ReturnResponse';
                            if($generateToken != ''){ 
                                $result = $gatewayTransaction->refundTransaction($tID, $payload);
                            }else{
                                $responseType = 'GenerateKeyResponse';
                            }
                            
                            $res = $result;
                            
                            $result['responseType'] = $responseType;
                            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
                            {
                                $status="success";
                                $refundTXNID = (isset($result[$responseType]['transactionID'])) ? $result[$responseType]['transactionID'] : $transactionID;
                                $message = "Transaction Refunded";
                                
                            }else{
                                $err_msg = $result[$responseType]['responseMessage'];
                                if($responseErrorMsg != ''){
                                    $err_msg = $responseErrorMsg;
                                }
                                $status="error";
                                $message = $err_msg;
                            }

                        }else{
                            if($txn['transactionType'] == 'Offline Payment'){
                                $status="success";
                                $refundTXNID = 'Offline'.time();
                                $result  = array('transactionID' => $refundTXNID
                                 );
                                $gt_result['gatewayType'] = 0;
                                $message = "Transaction Refunded";
                                
                            }else{
                                $status="error";
                                $message = 'Gateway Not Available';
                            }
                            
                        }
                        if("SUCCESS" == strtoupper($status))
                        {
                            if($refundIntegrationType == 5){
                                $inID = $txn['invoiceTxnID'];
                            }else if($refundIntegrationType == 2){
                                $inID = $txn['invoiceTxnID'];
                            }else if($refundIntegrationType == 1 || $refundIntegrationType == 3 || $refundIntegrationType == 4 ){
                                $inID = $txn['invoiceID'];
                            }
                            $parent_id = $txn['txnRowID'];
                            $id = $this->general_model->insert_gateway_transaction_data($result,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$amount,$this->merchantID,$crtxnID='', $this->resellerID,$inID,$echeckType,$this->transactionByUser,$custom_data_fields,$parent_id);

                            $refundedAmount += $amount;

                            $this->invoiceUpdate($inID,$amount,$refundIntegrationType,$this->merchantID,$txn['qbListTxnID'],$tID,$txn['transactionAmount']);

                            if($refundIntegrationType == 2)
                            {
                                $comp_data_user = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
                                
                                $refnd_trr = array('merchantID' => $merchantID, 'refundAmount' => $amount,
                                    'creditInvoiceID'               => $inID, 'creditTransactionID'       => $tID,
                                    'creditTxnID'                   => '', 'refundCustomerID'       => $customerID,
                                    'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
                                $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);


                                $user_id = $merchantID;
                                $user    = $comp_data_user['qbwc_username'];
                                $comp_id = $comp_data_user['id'];
                                $ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                                $refund  = $invoiceActualRefundAmount;
                                $ins_data['customerID'] = $customerID;
                                if (!empty($ittem)) {
                                    $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchantID));
                                    $new_inv_no = $inID;
                                    $inv_po = 'CZ000';
                                    if (!empty($in_data)) {
                                        $inv_pre    = $in_data['prefix'];
                                        $inv_po     = $in_data['postfix'] + 1;
                                        $new_inv_no = $inv_pre . $inv_po;
                                    }

                                    $ins_data['merchantDataID']    = $merchantID;
                                    $ins_data['creditDescription'] = "Credit as Refund";
                                    $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                                    $ins_data['creditDate']        = date('Y-m-d H:i:s');
                                    $ins_data['creditAmount']      = $amount;
                                    $ins_data['creditNumber']      = $new_inv_no;
                                    $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                                    $ins_data['Type']              = "Payment";
                                    $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                                    $item['itemListID']      = $ittem['ListID'];
                                    $item['itemDescription'] = $ittem['Name'];
                                    $item['itemPrice']       = $amount;
                                    $item['itemQuantity']    = 0;
                                    $item['crlineID']        = $ins_id;
                                    $acc_name                = $ittem['DepositToAccountName'];
                                    $acc_ID                  = $ittem['DepositToAccountRef'];
                                    $method_ID               = $ittem['PaymentMethodRef'];
                                    $method_name             = $ittem['PaymentMethodName'];
                                    $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                                    $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                                    
                                    if ($ins_id && $ins) {
                                        $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

                                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $ins_id, '1', '', $user);
                                    }

                                }
                            }
                        }

                    }
                    
                }

                if(strtolower($status) == 'success'){
                    $paydata = [
                        'customerListID' => $customerID,
                        'transactionID' => $refundTXNID,
                    ];

                    $refundedAmount = round($refundedAmount, 2);
                    $this->sendRefundEmail($user_id, $paydata, $refundIntegrationType, $refundedAmount, $refundTXNID);
                }
            }else{

            }
            $customerData = $this->general_model->getCustomerDetails($customerID,$this->merchantID,$refundIntegrationType);
            $checkPlan = check_free_plan_transactions();

            $receipt_data = array(
                'transaction_id' => $refundTXNID,
                'IP_address' => getClientIpAddr(),
                'billing_name' => $customerData['firstName'].' '.$customerData['lastName'],
                'billing_address1' => $customerData['address1'],
                'billing_address2' => $customerData['address2'],
                'billing_city' => $customerData['City'],
                'billing_zip' => $customerData['zipCode'],
                'billing_state' => $customerData['State'],
                'billing_country' => $customerData['Country'],
                'shipping_name' => $customerData['firstName'].' '.$customerData['lastName'],
                'shipping_address1' => $customerData['ship_address1'],
                'shipping_address2' => $customerData['ship_address2'],
                'shipping_city' => $customerData['ship_city'],
                'shipping_zip' => $customerData['ship_zipcode'],
                'shipping_state' => $customerData['ship_state'],
                'shiping_counry' => $customerData['ship_country'],
                'Phone' => $customerData['phoneNumber'],
                'Contact' => $customerData['userEmail'],
                'proccess_url' => '',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header' => 'Sale',
                'refundAmount' => $refundTotalamountInput,
                'checkPlan' => $checkPlan
            );
            
            $invoiceIDs[] = $invoiceID;

            $this->session->set_userdata("invoice_IDs", $invoiceIDs);

            if($status == 'error'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -</strong> ' .$message. '</div>');
            }else{
                $this->session->set_flashdata('success', $message);
            }
            
            if($refundIntegrationType == 1){
                if($echeckType){
                    $receipt_data['proccess_url'] = 'QBO_controllers/Payments/create_customer_esale'; 
                }else{
                    $receipt_data['proccess_url'] = 'QBO_controllers/Payments/create_customer_sale'; 
                }
                
                $this->session->set_userdata("receipt_data",$receipt_data);

                redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
            }elseif($refundIntegrationType == 2){
                if($echeckType){
                    $receipt_data['proccess_url'] = 'Payments/create_customer_esale'; 
                }else{
                    $receipt_data['proccess_url'] = 'Payments/create_customer_sale'; 
                }
                
                $this->session->set_userdata("receipt_data",$receipt_data);

                redirect('home/transation_sale_receipt',  'refresh');
                
            }elseif($refundIntegrationType == 3){
                if($echeckType){
                    $receipt_data['proccess_url'] = 'FreshBooks_controllers/Transactions/create_customer_esale'; 
                }else{
                    $receipt_data['proccess_url'] = 'FreshBooks_controllers/Transactions/create_customer_sale'; 
                }
                
                $this->session->set_userdata("receipt_data",$receipt_data);
                
                redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
            }elseif($refundIntegrationType == 4){
                if($echeckType){
                    $receipt_data['proccess_url'] = 'Integration/Payments/create_customer_esale'; 
                }else{
                    $receipt_data['proccess_url'] = 'Integration/Payments/create_customer_sale'; 
                }
                
                $this->session->set_userdata("receipt_data",$receipt_data);
                
                redirect('Integration/Payments/transation_sale_receipt',  'refresh');
            }else{
                if($echeckType){
                    $receipt_data['proccess_url'] = 'company/Payments/create_customer_esale'; 
                }else{
                    $receipt_data['proccess_url'] = 'company/Payments/create_customer_sale'; 
                }
                
                $this->session->set_userdata("receipt_data",$receipt_data);
                
                redirect('company/home/transation_sale_receipt',  'refresh');
            }
            
        }
            
    }
        
    /**
     * Function to send email for successful refund
     * 
     * @param int   $merchantID
     * @param array $paydata
     * @param int   $integrationType
     * @param float $amount
     * 
     * @return void
     */
    private function sendRefundEmail(int $merchantID, array $paydata, int $integrationType, float $amount, string $refundTXNID) : void
    {
        
        $condition_mail = array('templateType' => '7', 'merchantID' => $merchantID);
        $refNumber     = $toEmail = $company = $customer = '';
        $trDate        = date('Y-m-d H:i:s');

        if($integrationType == 1){
            $customerData = $this->general_model->get_row_data('QBO_custom_customer', [ 'merchantID' => $merchantID, 'Customer_ListID' => $paydata['customerListID']]);
            if($customerData){
                $toEmail        = $customerData['userEmail'];
                $company        = $customerData['companyName'];
                $customer       = $customerData['fullName'];
            }
        } elseif($integrationType == 2) {
            $customerData = $this->general_model->get_row_data('qb_test_customer', [ 'qbmerchantID' => $merchantID, 'ListID' => $paydata['customerListID']]);
            if($customerData){
                $toEmail        = $customerData['Contact'];
                $company        = $customerData['companyName'];
                $customer       = $customerData['FullName'];
            }
        } elseif($integrationType == 3) {
            $customerData = $this->general_model->get_row_data('Freshbooks_custom_customer', [ 'merchantID' => $merchantID, 'Customer_ListID' => $paydata['customerListID']]);
            if($customerData){
                $toEmail        = $customerData['userEmail'];
                $company        = $customerData['companyName'];
                $customer       = $customerData['fullName'];
            }
        } elseif($integrationType == 4) {
            $customerData = $this->general_model->get_row_data('Xero_custom_customer', [ 'merchantID' => $merchantID, 'Customer_ListID' => $paydata['customerListID']]);
            if($customerData){
                $toEmail        = $customerData['userEmail'];
                $company        = $customerData['companyName'];
                $customer       = $customerData['fullName'];
            }
        } elseif($integrationType == 5) {
            $customerData = $this->general_model->get_row_data('chargezoom_test_customer', [ 'qbmerchantID' => $merchantID, 'ListID' => $paydata['customerListID']]);
            if($customerData){
                $toEmail        = $customerData['Contact'];
                $company        = $customerData['companyName'];
                $customer       = $customerData['FullName'];
            }
        }

        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $paydata['customerListID'], $refNumber, $amount, $trDate, $refundTXNID);
    }
}
