<?php
error_reporting(1);
/* This controller has following opration for gateways
 * NMI, Authorize.net, Paytrace, Paypal, Stripe Payment Gateway Operations

 * Refund create_customer_refund
 * Single Invoice Payment transaction refund
 * merchantID ans resellerID are Private Member
 */
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
include APPPATH . 'third_party/TSYS.class.php';
include APPPATH . 'third_party/EPX.class.php';
require APPPATH .'libraries/Manage_payments.php';
use iTransact\iTransactSDK\iTTransaction;

use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\ServicesConfig;
use GlobalPayments\Api\ServicesContainer;
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

class CapturePayment extends CI_Controller
{

    private $merchantID;
    private $resellerID;
    private $gatewayEnvironment;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();

        $this->load->config('quickbooks');
        
        $this->load->model('general_model');
        $this->load->model('card_model');

        $this->load->model('quickbooks');
        $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
        $this->load->model('customer_model', 'customer_model');

        if ($this->session->userdata('logged_in')) {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $merchID;
        } else {
            redirect('login', 'refresh');
        }
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    
    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

   
    public function captureAmount()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $user_id                = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id                = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $merchID = $merchantID = $user_id;
        if(!empty($this->input->post(null, true)))
        {

            $custom_data_fields = [];
            $tID     = $this->czsecurity->xssCleanPostInput('captureTxnID');
            $integrationType     = $this->czsecurity->xssCleanPostInput('captureIntegrationType');

            $con     = array('transactionID'=>$tID);
            $paydata = $this->general_model->get_row_data('customer_transaction',$con);

            $customerID = $paydata['customerListID'];

            $gateway = $paydata['gatewayID'];

                 
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
            $echeckType = (strrpos($paydata['gateway'], 'ECheck'))? false : true;

            if($this->czsecurity->xssCleanPostInput('setMail')){
                $chh_mail =1;
            }else{
                $chh_mail =0;
            }
            if($tID!='' && !empty($gt_result))
            {

                // get custom field data
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) 
                {
                    
                    $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                }
                $amount =  $this->czsecurity->xssCleanPostInput('amount');
                
                $params = array(
                    'transactionId' => $tID,
                    'payAmount' => $amount,
                    'gatewayUsername' => $gt_result['gatewayUsername'],
                    'gatewayPassword' => $gt_result['gatewayPassword'],
                    'gatewaySignature' => $gt_result['gatewaySignature'],
                    'extra_field_1' => $gt_result['extra_field_1'],
                    'gatewayType' => $gt_result['gatewayType'],
                );

                $captureData = [
                    'transactionID'     => $tID,
                    'amount'            => $amount,
                    'gatewayID'         => $paydata['gatewayID'],
                    'customerListID'    => $customerID,
                    'storeResult'       => false,
                    'returnResult'      => true,
                    'transactionByUser' => $this->transactionByUser,
                ];
                $paymentObj = new Manage_payments($this->merchantID);
                $paidResult = $paymentObj->_captureTransaction($captureData);
                
                $custom_tr_data = (isset($paydata) && isset($paydata['custom_data_fields'])) ? $paydata['custom_data_fields'] : false;
                
                $payment_type = '';
                if($custom_tr_data){
                    $json_data = json_decode($custom_tr_data, 1);
                    if(isset($json_data['payment_type'])){
                        $payment_type = $json_data['payment_type'];
                        $custom_data_fields['payment_type'] = $payment_type;
                    }
                }
                $invoiceIDs=array();  
                if (isset($paidResult['response']) && $paidResult['response']) 
                {
                        
                    if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
                    {
                        $invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = [$amount];
                    }
                    $refnum=array();
                    if(!empty($invoiceIDs))
                    {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {

                            
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];

                            $savedID = $this->general_model->insert_gateway_transaction_data($paidResult['result'],'capture',$gateway,$gt_result['gatewayType'],$customerID,$actualInvoicePayAmount,$merchantID, '', $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);

                            $this->invoiceUpdate($inID,$actualInvoicePayAmount,$integrationType,$this->merchantID,$paydata,$tID,$chh_mail,$savedID);

                            $payIndex++;
                            
                        }
                    }else{
                        $transactionRowID = $savedID = $this->general_model->insert_gateway_transaction_data($paidResult['result'],'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID, '', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
                    }
                    $this->session->set_flashdata('success', 'Capture Successful');
                    

                }else{
                    
                    $msg = $paidResult['errorMsg'];
                    $trID = 'TXNFAILED-'.time();
                    $captureTXNID = $trID;
                    
                    $res =array('FAILEDTXNID' => $trID,'FAILEDTXNCODE'=>300, 'FAILEDTXNSTATUS'=>$msg, 'transactionId'=> $trID );

                    $transactionRowID = $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$paydata['customerListID'],$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);

                    $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                }

                if (isset($savedID) && $savedID) {
                    $condition2         = array('id' => $savedID);
                    $storedTransactData = $this->general_model->get_row_data('customer_transaction', $condition2);
                    $transactionRowID = $storedTransactData['id']; 
                    if ($storedTransactData) {
                        $captureTXNID = $storedTransactData['transactionID'];
                    }
                }
                

                $customerData = $this->general_model->getCustomerDetails($customerID,$this->merchantID,$integrationType);

                $checkPlan = check_free_plan_transactions();

                $receipt_data = array(
                    'transaction_id' => $captureTXNID,
                    'transactionRowID' => $transactionRowID,
                    'IP_address' => getClientIpAddr(),
                    'billing_name' => $customerData['firstName'].' '.$customerData['lastName'],
                    'billing_address1' => $customerData['address1'],
                    'billing_address2' => $customerData['address2'],
                    'billing_city' => $customerData['City'],
                    'billing_zip' => $customerData['zipCode'],
                    'billing_state' => $customerData['State'],
                    'billing_country' => $customerData['Country'],
                    'shipping_name' => $customerData['firstName'].' '.$customerData['lastName'],
                    'shipping_address1' => $customerData['ship_address1'],
                    'shipping_address2' => $customerData['ship_address2'],
                    'shipping_city' => $customerData['ship_city'],
                    'shipping_zip' => $customerData['ship_zipcode'],
                    'shipping_state' => $customerData['ship_state'],
                    'shiping_counry' => $customerData['ship_country'],
                    'Phone' => $customerData['phoneNumber'],
                    'Contact' => $customerData['userEmail'],
                    'proccess_url' => '',
                    'proccess_btn_text' => 'Process New Auth',
                    'sub_header' => 'Sale',
                    'refundAmount' => $amount,
                    'checkPlan' => $checkPlan
                );
                
                
                $this->session->set_userdata("invoice_IDs", $invoiceIDs);
                
                if($integrationType == 1){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'QBO_controllers/Payments/create_customer_auth'; 
                    }else{
                        $receipt_data['proccess_url'] = 'QBO_controllers/Payments/create_customer_auth'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);

                    redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
                }elseif($integrationType == 2){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'Payments/create_customer_auth'; 
                    }else{
                        $receipt_data['proccess_url'] = 'Payments/create_customer_auth'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);

                    redirect('home/transation_sale_receipt',  'refresh');
                    
                }elseif($integrationType == 3){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'FreshBooks_controllers/Transactions/create_customer_auth'; 
                    }else{
                        $receipt_data['proccess_url'] = 'FreshBooks_controllers/Transactions/create_customer_auth'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);
                    
                    redirect('FreshBooks_controllers/home/transation_sale_receipt',  'refresh');
                }elseif($integrationType == 3){
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'Xero_controllers/Payments/create_customer_auth'; 
                    }else{
                        $receipt_data['proccess_url'] = 'Xero_controllers/Payments/create_customer_auth'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);
                    
                    redirect('Xero_controllers/home/transation_sale_receipt',  'refresh');
                }else{
                    if($echeckType){
                        $receipt_data['proccess_url'] = 'company/Payments/create_customer_auth'; 
                    }else{
                        $receipt_data['proccess_url'] = 'company/Payments/create_customer_auth'; 
                    }
                    
                    $this->session->set_userdata("receipt_data",$receipt_data);
                    
                    redirect('company/home/transation_sale_receipt',  'refresh');
                }


            }
        }
        

        
    }
    
    public function invoiceUpdate($inID,$actualInvoicePayAmount,$integrationType,$merchantID,$paydata,$tID,$chh_mail,$txnRowID='')
    {
        
        if($integrationType == 1){
            $transaction_custom_data_fields = (isset($paydata['custom_data_fields']) && !empty($paydata['custom_data_fields'])) ? json_decode($paydata['custom_data_fields'], 1) : [];
            $cardType = isset($transaction_custom_data_fields['card_type']) ? $transaction_custom_data_fields['card_type'] : '';

            $condition1 = array('invoiceID'=> $inID,'merchantID'=> $merchantID, 'transactionID'=>$tID );
            
            $payData = $this->general_model->get_row_data('customer_transaction',$condition1);

            $condition  = array('invoiceID'=> $inID,'merchantID'=> $merchantID );  
            $invoiceData = $this->general_model->get_row_data('QBO_test_invoice',$condition);
            $customerID = $invoiceData['CustomerListID'];
            $dataUpdate = [];


            $amount_data = $invoiceData['BalanceRemaining'];
            $actualInvoicePayAmount = $actualInvoicePayAmount;
            $isPaid      = 0;
            $BalanceRemaining = 0.00;
            
            if($amount_data == $actualInvoicePayAmount){
                $actualInvoicePayAmount = $amount_data;
                $isPaid      = 1;

            }else{

                $actualInvoicePayAmount = $actualInvoicePayAmount;
                $isPaid      = 0;
                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                
            }
            $AppliedAmount = $invoiceData['AppliedAmount'] + $actualInvoicePayAmount;

            $dataUpdate['AppliedAmount'] = round($AppliedAmount,2);
            $dataUpdate['BalanceRemaining'] = round($BalanceRemaining,2);
            
            
            $dataUpdate['IsPaid'] = $isPaid;

            $this->general_model->update_row_data('QBO_test_invoice',$condition, $dataUpdate);
             
            $val = array('merchantID' => $merchantID);
            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];

            $dataService = DataService::Configure(array(
                'auth_mode' => $this->config->item('AuthMode'),
                'ClientID'  => $this->config->item('client_id'),
                'ClientSecret' => $this->config->item('client_secret'),
                'accessTokenKey' =>  $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID' => $realmID,
                'baseUrl' => $this->config->item('QBOURL'),
            ));
            
            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");
           
            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
               
                $theInvoice = current($targetInvoiceArray);

                $createPaymentObject = [
                    "TotalAmt" => $actualInvoicePayAmount,
                    "SyncToken" => 1, 
                    "CustomerRef" => $customerID,
                    "Line" => [
                        "LinkedTxn" => [
                            "TxnId" => $inID,
                            "TxnType" => "Invoice",
                        ],
                        "Amount" => $actualInvoicePayAmount
                    ]
                ];

                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $merchantID, false);
                if($paymentMethod){
                    $createPaymentObject['PaymentMethodRef'] = [
                        'value' => $paymentMethod['payment_id']
                    ];
                }
                if(isset($tID) && $tID != ''){
                    $createPaymentObject['PaymentRefNum'] = $tID;
                }
                $newPaymentObj = Payment::create($createPaymentObject);
             
                $savedPayment = $dataService->Add($newPaymentObj);

                $error = $dataService->getLastError();
                if ($error != null) {
                    $err = '';
                    $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>' .   $err . '</div>');

                    $st = '0';
                    $action = 'Pay Invoice';
                    $msg = "Payment Success but " . $error->getResponseBody();
                    $qbID = $tID;
                    $pinv_id = '';
                } else {
                    $pinv_id = '';
                    $crtxnID = $pinv_id = $savedPayment->Id;
                    $st = '1';
                    $action = 'Pay Invoice';
                    $msg = "Payment Success";
                    $qbID = $tID;
                }

                $qbID = $inID;
                $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $tID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                if($syncid){
                    $qbSyncID = 'CQ-'.$syncid;

                    $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                }
                
                $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$actualInvoicePayAmount,$user_id,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);
                
            }
            $comp_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
            $tr_date   =date('Y-m-d H:i:s');
            $ref_number =  $tID;
            $toEmail = $comp_data['userEmail'];
            $company=$comp_data['companyName']; 
            $customer = $comp_data['fullName'];

        }elseif($integrationType == 2){

            $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID'=>$inID,'qb_inv_merchantID'=> $merchantID));
            $customerID = $theInvoice['Customer_ListID'];
            if (!empty($theInvoice))
            {
                $amount_data = $theInvoice['BalanceRemaining'];
                $actualInvoicePayAmount = $actualInvoicePayAmount;
                $isPaid      = 'false';
                $BalanceRemaining = 0.00;
                
                $refnum[] = $theInvoice['RefNumber'];
                if($amount_data == $actualInvoicePayAmount){
                    $actualInvoicePayAmount = $amount_data;
                    $isPaid      = 'true';

                }else{

                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                    $isPaid      = 'false';
                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                    
                    
                }
                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                $tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));


                $comp_data_user = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
                $user      = $comp_data_user['qbwc_username'];

                if(!is_numeric($inID)){
                    
                    $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,$txnRowID, '1', '', $user);
                }

                
                        
            }
            $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
            $tr_date   =date('Y-m-d H:i:s');
            $ref_number =  $tID;
            $toEmail = $comp_data['Contact'];
            $company=$comp_data['companyName']; 
            $customer = $comp_data['FullName'];  
            

        }elseif($integrationType == 3){
            
            
        }elseif($integrationType == 4){
            
            
        }elseif($integrationType == 5){
            $condition  = array('TxnID'=>$inID );  
            $invoiceData = $this->general_model->get_row_data('chargezoom_test_invoice',$condition);
            $customerID = $invoiceData['Customer_ListID'];
            $dataUpdate = [];

            $amount_data = $invoiceData['BalanceRemaining'];
            $actualInvoicePayAmount = $actualInvoicePayAmount;
            $isPaid      = 'false';
            $BalanceRemaining = 0.00;
            
                
            if($amount_data == $actualInvoicePayAmount){
                $actualInvoicePayAmount = $amount_data;
                $isPaid      = 'true';

            }else{

                $actualInvoicePayAmount = $actualInvoicePayAmount;
                $isPaid      = 'false';
                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                
                
            }
            $dataUpdate['AppliedAmount'] = abs($invoiceData['AppliedAmount']) + $actualInvoicePayAmount;
            $dataUpdate['BalanceRemaining'] = $BalanceRemaining;
            $dataUpdate['IsPaid'] = $isPaid;

            $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $dataUpdate);

            $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName'), array('qbmerchantID'=>$merchantID));
            $tr_date   =date('Y-m-d H:i:s');
            $ref_number =  $tID;
            $toEmail = $comp_data['Contact'];
            $company=$comp_data['companyName']; 
            $customer = $comp_data['FullName'];

        }else{
             
        }

        if($chh_mail =='1')
        {
            $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
        }
        return true;
    }
    
}
