<?php
/**
 * Class CommonAjax
 *
 * Common Ajax class.
 *
 * @author Mohit Gupta <mohit.gupta@chargezoom.com>
 */

class CommonAjax extends CI_Controller
{
    private $merchantID;
    private $resellerID;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model');
        if ($this->session->userdata('logged_in')) {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $merchID;
        } else {
            redirect('login', 'refresh');
        }
    }

    /**
	 * City State Country list ajax
     * 
     * @param null
	 * @return JSON
	 */
    public function city_ajax() : JSON
    {
        $city = $this->general_model->get_table_select_data('city', ['city_name'], []);
        if(!empty($city)){
            $city = array_map(function($x){ return $x['city_name']; }, $city);
        }

        $state = $this->general_model->get_table_select_data('state', ['state_name'], []);
        if(!empty($state)){
            $state = array_map(function($x){ return $x['state_name']; }, $state);
        }

        $country = $this->general_model->get_table_select_data('country', ['country_name'], []);
        if(!empty($country)){
            $country = array_map(function($x){ return $x['country_name']; }, $country);
        }

        echo json_encode(array(
            'success' => true,
            'data' => [
                'city' => $city,
                'state' => $state,
                'country' => $country,
            ]
        )); die;
    }
}
?>
