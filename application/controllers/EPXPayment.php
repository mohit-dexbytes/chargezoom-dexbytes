<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */

include APPPATH . 'third_party/EPX.class.php';

class EPXPayment extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    private $gatewayEnvironment;
    public function __construct()
    {
        parent::__construct();
        $this->load->config('quickbooks');
        $this->load->model('quickbooks');
        $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
        $this->load->model('general_model');
        $this->load->model('company_model');
        $this->db1 = $this->load->database('otherdb', true);
        $this->load->model('customer_model');
        $this->load->model('card_model');
        $this->load->library('form_validation');
        $this->load->config('EPX');
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID = $merchID;
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index(){
        redirect('home/', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $da      = $this->session->userdata('logged_in');
            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da      = $this->session->userdata('user_logged_in');
            $user_id = $da['merchantID'];
        }

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
       
        $cusproID = '';
        $error    = '';
        $cusproID = $this->input->post('customerProcessID');
		$checkPlan = check_free_plan_transactions();

        if ($this->form_validation->run() == true) {
            $custom_data_fields = [];
            $cusproID  = '';
            $error     = '';
            $cusproID  = $this->input->post('customerProcessID');
            $invoiceID = $this->input->post('invoiceProcessID');
            $cardID    = $this->input->post('CardID');
            if (!$cardID || empty($cardID)) {
                $cardID = $this->input->post('schCardID');
            }

            $gatlistval = $this->input->post('sch_gateway');
            if (!$gatlistval || empty($gatlistval)) {
                $gatlistval = $this->input->post('gateway');
            }

            $gateway = $gatlistval;
            $responseId = $transactionID = 'TXNFAILED'.time();

            $amount     = $this->input->post('inv_amount');
            $in_data    = $this->quickbooks->get_invoice_data_pay($invoiceID, $user_id);
            $sch_method = $this->input->post('sch_method');

            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }


            if ($checkPlan && !empty($in_data)) {

                $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $customerID = $in_data['Customer_ListID'];
                $c_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName','FirstName', 'LastName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                $companyID  = $c_data['companyID'];

                $Customer_ListID = $in_data['Customer_ListID'];

                $cardID_upd = '';

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $orderId = time();
                $amount    = $this->input->post('inv_amount');
                $amount = number_format($amount,2,'.','');
                $transaction = array(
                        'CUST_NBR' => $CUST_NBR,
                        'MERCH_NBR' => $MERCH_NBR,
                        'DBA_NBR' => $DBA_NBR,
                        'TERMINAL_NBR' => $TERMINAL_NBR,
                        'AMOUNT' => $amount,
                        'TRAN_NBR' => rand(1,10),
                        'BATCH_ID' => time(),
                        'VERBOSE_RESPONSE' => 'Y',
                );
                if($c_data['FirstName'] != ''){
                    $transaction['FIRST_NAME'] = $c_data['FirstName'];
                }
                if($c_data['LastName'] != ''){
                    $transaction['LAST_NAME'] = $c_data['LastName'];
                }
                if (!empty($cardID)) {

                    $cr_amount = 0;
                    $name = $in_data['Customer_FullName'];

                    if ($sch_method == "1") {
                        $getewayName = 'EPX';
                        $echeckType = false;

                        if ($cardID != "new1") {

                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
                            $cvv       = $card_data['CardCVV'];
                            $cardType  = $card_data['CardType'];
                            $address1  = $card_data['Billing_Addr1'];
                            $address2  = $card_data['Billing_Addr2'];
                            $city      = $card_data['Billing_City'];
                            $zipcode   = $card_data['Billing_Zipcode'];
                            $state     = $card_data['Billing_State'];
                            $country   = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        } else {

                            $card_no  = $this->input->post('card_number');
                            $expmonth = $this->input->post('expiry');
                            $exyear   = $this->input->post('expiry_year');
                            $cardType = $this->general_model->getType($card_no);
                            $cvv      = '';
                            if ($this->input->post('cvv') != "") {
                                $cvv = $this->input->post('cvv');
                            }

                            $address1 = $this->input->post('address1');
                            $address2 = $this->input->post('address2');
                            $city     = $this->input->post('city');
                            $country  = $this->input->post('country');
                            $phone    = $this->input->post('contact');
                            $state    = $this->input->post('state');
                            $zipcode  = $this->input->post('zipcode');
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;
                        if (strlen($expmonth) == 1) {
                            $expmonth = '0' . $expmonth;
                        }
                        $exyear1  = substr($exyear, 2);
                        $transaction['EXP_DATE'] = $exyear1.$expmonth;
                        $transaction['ACCOUNT_NBR'] = $card_no;
                        $transaction['TRAN_TYPE'] = 'CCE1';
                        $transaction['CARD_ENT_METH'] = 'E';
                        $transaction['INDUSTRY_TYPE'] = 'E';

                        if($address1 != ''){
                            $transaction['ADDRESS'] = $address1;
                        }
                        if($city != ''){
                            $transaction['CITY'] = $city;
                        }
                        if($zipcode != ''){
                            $transaction['ZIP_CODE'] = $zipcode;
                        }
                        
                        
                    } else if ($sch_method == "2") {
                        $echeckType = true;
                        $getewayName = 'EPX ECheck';

                        if ($cardID == 'new1') {
                            $accountDetails = [
                                'accountName'        => $this->input->post('acc_name'),
                                'accountNumber'      => $this->input->post('acc_number'),
                                'routeNumber'        => $this->input->post('route_number'),
                                'accountType'        => $this->input->post('acct_type'),
                                'accountHolderType'  => $this->input->post('acct_holder_type'),
                                'Billing_Addr1'      => $this->input->post('address1'),
                                'Billing_Addr2'      => $this->input->post('address2'),
                                'Billing_City'       => $this->input->post('city'),
                                'Billing_Country'    => $this->input->post('country'),
                                'Billing_Contact'    => $this->input->post('contact'),
                                'Billing_State'      => $this->input->post('state'),
                                'Billing_Zipcode'    => $this->input->post('zipcode'),
                                'customerListID'     => $customerID,
                                'companyID'          => $companyID,
                                'merchantID'         => $user_id,
                                'createdAt'          => date("Y-m-d H:i:s"),
                                'secCodeEntryMethod' => $this->input->post('secCode'),
                            ];
                        } else {
                            $accountDetails = $this->card_model->get_single_card_data($cardID);
                        }
                        $accountNumber = $accountDetails['accountNumber'];
                        $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        $transaction['RECV_NAME'] = $accountDetails['accountName'];
                        $transaction['ACCOUNT_NBR'] = $accountDetails['accountNumber'];
                        $transaction['ROUTING_NBR'] = $accountDetails['routeNumber'];

                        if($accountDetails['accountType'] == 'savings'){
                            $transaction['TRAN_TYPE'] = 'CKS2';
                        }else{
                            $transaction['TRAN_TYPE'] = 'CKC2';
                        }
                        if($accountDetails['Billing_Addr1'] != ''){
                            $transaction['ADDRESS'] = $accountDetails['Billing_Addr1'];
                        }
                        if($accountDetails['Billing_City'] != ''){
                            $transaction['CITY'] = $accountDetails['Billing_City'];
                        }
                        if( $accountDetails['Billing_Zipcode'] != ''){
                            $transaction['ZIP_CODE'] = $accountDetails['Billing_Zipcode'];
                        }
                        
                    }
                    
                    $gatewayTransaction              = new EPX();
                    $result = $gatewayTransaction->processTransaction($transaction);

                    if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                        $responseId = $transactionID = $result['AUTH_GUID'];
                        $txnID                      = $in_data['TxnID'];
                        $ispaid                     = 'true';

                        $bamount = $in_data['BalanceRemaining'] - $amount;
                        if ($bamount > 0) {
                            $ispaid = 'false';
                        }

                        $app_amount = $in_data['AppliedAmount'] + (-$amount);
                        $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);

                        $condition = array('TxnID' => $in_data['TxnID']);
                        $this->general_model->update_row_data('qb_test_invoice', $condition, $data);

                        $user = $in_data['qbwc_username'];
                       
                        if ($cardID == "new1") {
                            if ($sch_method == "1") {

                                $card_no  = $this->input->post('card_number');
                                $cardType = $this->general_model->getType($card_no);

                                $expmonth = $this->input->post('expiry');
                                $exyear   = $this->input->post('expiry_year');
                                $cvv      = $this->input->post('cvv');

                                $card_data = array(
                                    'cardMonth'       => $expmonth,
                                    'cardYear'        => $exyear,
                                    'CardType'        => $cardType,
                                    'CustomerCard'    => $card_no,
                                    'CardCVV'         => $cvv,
                                    'Billing_Addr1'   => $this->input->post('address1'),
                                    'Billing_Addr2'   => $this->input->post('address2'),
                                    'Billing_City'    => $this->input->post('city'),
                                    'Billing_Country' => $this->input->post('country'),
                                    'Billing_Contact' => $this->input->post('phone'),
                                    'Billing_State'   => $this->input->post('state'),
                                    'Billing_Zipcode' => $this->input->post('zipcode'),
                                    'customerListID'  => $customerID,

                                    'companyID'       => $companyID,
                                    'merchantID'      => $this->merchantID,
                                    'createdAt'       => date("Y-m-d H:i:s"),
                                );

                                $id1 = $this->card_model->process_card($card_data);
                            } else if ($sch_method == "2") {
                                $id1 = $this->card_model->process_ack_account($accountDetails);
                            }
                        }

                        $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                        $ref_number     = $in_data['RefNumber'];
                        $tr_date        = date('Y-m-d H:i:s');
                        $toEmail        = $c_data['Contact'];
                        $company        = $c_data['companyName'];
                        $customer       = $c_data['FullName'];
                        if ($chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                        }
                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                    } else {
                        $transactionID = 'TXNFAILED'.time();
                        $message = $result['AUTH_RESP_TEXT'];
                        $code = 300;
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['AUTH_RESP_TEXT'] . '</div>');
                    }
                    $result['transactionid'] = $responseId;
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID = '', $this->resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                    if ( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != ''  && !is_numeric($invoiceID)) {
                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
            }

        } else {
            $error = 'Validation Error. Please fill the requred fields';
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
        }

        if ($cusproID == "2") {
            redirect('home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3" && $in_data['TxnID'] != '') {
            redirect('home/invoice_details/' . $in_data['TxnID'], 'refresh');
        }

        $invoice_IDs = array();

        $receipt_data = array(
            'proccess_url'      => 'home/invoices',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }
        redirect('home/transation_receipt/' . $in_data['TxnID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $custom_data_fields = [];
            $applySurcharge = false;
            // get custom field data
            if (!empty($this->input->post('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->input->post('invoice_id');
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->input->post('po_number');
            }

            $inputData = $this->input->post();

            $checkPlan = check_free_plan_transactions();

            $responseId = $transactionID = 'TXNFAILED'.time();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                if ($this->session->userdata('logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }

                $customerID = $this->input->post('customerID');
                if (!$customerID || empty($customerID)) {
                    $customerID = create_card_customer($this->input->post(), '1');
                }

                $comp_data = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $qbd_comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));
                $user      = $qbd_comp_data['qbwc_username'];

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $orderId = time();

                $user_id     = $merchantID;
                $cardID      = $this->input->post('card_list');
                $contact     = $this->input->post('phone');
                $cvv         = '';
                if ($this->input->post('card_number') != "" && $cardID == 'new1') {
                    $card_no  = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');

                    $exyear = $this->input->post('expiry_year');
                    if ($this->input->post('cvv') != "") {
                        $cvv = $this->input->post('cvv');
                    }

                    $address1 = $this->input->post('baddress1');
                    $address2 = $this->input->post('baddress2');
                    $city     = $this->input->post('bcity');
                    $country  = $this->input->post('bcountry');
                    $phone    = $this->input->post('bphone');
                    $state    = $this->input->post('bstate');
                    $zipcode  = $this->input->post('bzipcode');

                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    if ($card_data['CardCVV']) {
                        $cvv = $card_data['CardCVV'];
                    }

                    $cardType = $card_data['CardType'];
                    $address1 = $card_data['Billing_Addr1'];
                    $address2 = $card_data['Billing_Addr2'];
                    $city     = $card_data['Billing_City'];
                    $zipcode  = $card_data['Billing_Zipcode'];
                    $state    = $card_data['Billing_State'];
                    $country  = $card_data['Billing_Country'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $exyear1.$expmonth;

                $name    = $this->input->post('firstName') . ' ' . $this->input->post('lastName');
                $address = $this->input->post('address');
                $country = $this->input->post('country');
                $city    = $this->input->post('city');
                $state   = $this->input->post('state');
                $amount  = $this->input->post('totalamount');
                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;
                $zipcode = ($this->input->post('zipcode')) ? $this->input->post('zipcode') : '74035';
                $calamount = $amount * 100;

                $amount = number_format($amount,2,'.','');

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE1',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',


                );
                
                if($cvv && !empty($cvv)){
                    $transaction['CVV2'] = $cvv;
                }
                if($inputData['baddress1'] != ''){
                    $transaction['ADDRESS'] = $inputData['baddress1'];
                }
                if($inputData['bcity'] != ''){
                    $transaction['CITY'] = $inputData['bcity'];
                }
                
                if($inputData['bzipcode'] != ''){
                    $transaction['ZIP_CODE'] = $inputData['bzipcode'];
                }
                if($inputData['firstName'] != ''){
                    $transaction['FIRST_NAME'] = $inputData['firstName'];
                }
                if($inputData['lastName'] != ''){
                    $transaction['LAST_NAME'] = $inputData['lastName'];
                }
                if(!empty($this->input->post('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->input->post('invoice_id'), $merchantID, 2);
                    $transaction['ORDER_NBR'] = $new_invoice_number;
                }
                $crtxnID = '';
                $invID   = '';
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
               
            
				
				if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    
                    $message = $result['AUTH_RESP_TEXT'];
                    $responseId = $transactionID = $result['AUTH_GUID'];

                    $invoiceIDs                 = array();
					$invoicePayAmounts = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
						$invoicePayAmounts = explode(',', $this->input->post('invoice_pay_amount'));
                    }
                    $refnum = array();
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

                            if (!empty($theInvoice))
                            {
                                
                                $amount_data = $theInvoice['BalanceRemaining'];
                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                    $actualInvoicePayAmount += $surchargeAmount;
                                    $updatedInvoiceData = [
                                        'inID' => $inID,
                                        'merchantID' => $user_id,
                                        'amount' => $surchargeAmount,
                                    ];
                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,2);
                                    $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
                                    $amount_data = $theInvoice['BalanceRemaining'];

                                }
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                if($saleAmountRemaining > 0){
                                    $refnum[] = $theInvoice['RefNumber'];
                                    if($amount_data == $actualInvoicePayAmount){
                                        $actualInvoicePayAmount = $amount_data;
                                        $isPaid      = 'true';

                                    }else{

                                        $actualInvoicePayAmount = $actualInvoicePayAmount;
                                        $isPaid      = 'false';
                                        $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                        
                                        
                                    }
                                    $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                    $tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));
                                    $result['transactionid'] = $responseId;
                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                    $comp_data1 = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$user_id));
                                    $user      = $comp_data1['qbwc_username'] ;
                                    if(!is_numeric($inID))
                                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
                                }
                                
                            }
                            $payIndex++;
                            
                        }
                    } else {


                        $transactiondata = array();
                        $inID            = '';
                        $result['transactionid'] = $responseId;
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($cardID == "new1" && !($this->input->post('tc'))) {
                        $card_no   = $this->input->post('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->input->post('expiry');

                        $exyear = $this->input->post('expiry_year');

                        $cvv       = $this->input->post('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $contact,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                    $ref_number     = implode(',', $refnum);
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['Contact'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['FullName'];
                    if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $message = $result['AUTH_RESP_TEXT'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $message . '</div>');
                    $result['transactionid'] = $responseId;
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>');
            }

        }
        $invoice_IDs = array();
        if (!empty($this->input->post('invoice_id'))) {
            $invoice_IDs = explode(',', $this->input->post('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => $_SERVER['REMOTE_ADDR'],
            'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'billing_address1'  => $this->input->post('baddress1'),
            'billing_address2'  => $this->input->post('baddress2'),
            'billing_city'      => $this->input->post('bcity'),
            'billing_zip'       => $this->input->post('bzipcode'),
            'billing_state'     => $this->input->post('bstate'),
            'billing_country'   => $this->input->post('bcountry'),
            'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'shipping_address1' => $this->input->post('address1'),
            'shipping_address2' => $this->input->post('address2'),
            'shipping_city'     => $this->input->post('city'),
            'shipping_zip'      => $this->input->post('zipcode'),
            'shipping_state'    => $this->input->post('state'),
            'shiping_counry'    => $this->input->post('country'),
            'Phone'             => $this->input->post('phone'),
            'Contact'           => $this->input->post('email'),
            'proccess_url'      => 'Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        
        if (!empty($this->input->post())) {

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->input->post('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->input->post('invoice_id');
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->input->post('po_number');
            }

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $responseId = 'TXNFAILED-'.time();
            $checkPlan = check_free_plan_transactions();

            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $this->input->post('totalamount');
            $name = $this->input->post('firstName'). " " . $this->input->post('lastName');
            $firstName = ($this->input->post('firstName') != '')?$this->input->post('firstName'):'None';
            $lastName = ($this->input->post('lastName') != '')?$this->input->post('lastName'):'None';
            $address1 = ($this->input->post('baddress1') != '')?$this->input->post('baddress1'):'';
            $zipcode = ($this->input->post('bzipcode') != '')?$this->input->post('bzipcode'):'';
            $city = ($this->input->post('bcity') != '')?$this->input->post('bcity'):'';

            $customerID   = $this->input->post('customerID');

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $comp_data = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->input->post('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->input->post('account_name'),
                        'accountNumber'      => $this->input->post('account_number'),
                        'routeNumber'        => $this->input->post('route_number'),
                        'accountType'        => $this->input->post('acct_type'),
                        'accountHolderType'  => $this->input->post('acct_holder_type'),
                        'Billing_Addr1'      => $this->input->post('baddress1'),
                        'Billing_Addr2'      => $this->input->post('baddress2'),
                        'Billing_City'       => $this->input->post('bcity'),
                        'Billing_Country'    => $this->input->post('bcountry'),
                        'Billing_Contact'    => $this->input->post('phone'),
                        'Billing_State'      => $this->input->post('bstate'),
                        'Billing_Zipcode'    => $this->input->post('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $amount = number_format($amount,2,'.','');
                if($accountDetails['accountType'] == 'savings'){
                    $txnType = 'CKS2'; 
                }else{
                    $txnType = 'CKC2';
                }
                $orderId = time();

                

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => $txnType,
                    'ACCOUNT_NBR' => $accountDetails['accountNumber'],
                    'ROUTING_NBR' => $accountDetails['routeNumber'],
                    'RECV_NAME' => $accountDetails['accountName'],
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',


                );
                if(!empty($this->input->post('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->input->post('invoice_id'), $merchantID, 2);
                    $transaction['ORDER_NBR'] = $new_invoice_number;
                }
                if($address1 != ''){
                    $transaction['ADDRESS'] = $address1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
                if($firstName != ''){
                    $transaction['FIRST_NAME'] = $firstName;
                }
                if($lastName != ''){
                    $transaction['LAST_NAME'] = $lastName;
                }
               
                $gatewayTransaction = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);

                $invoiceIDs = [];

                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    $responseId = $result['AUTH_GUID'];

					$invoiceIDs = [];
                    $invoicePayAmounts = [];
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->input->post('invoice_pay_amount'));
                    }
					$refnum = array();
					$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' => $merchantID));

					if (!empty($invoiceIDs)) {
                        $payIndex = 0;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();

							$theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

							if (!empty($theInvoice)) {
                                $amount_data = $theInvoice['BalanceRemaining'];

                                $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                $isPaid      = 'false';
                                $BalanceRemaining = 0.00;
                                $refnum[] = $theInvoice['RefNumber'];

                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 'true';

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 'false';
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $txnAmount = $actualInvoicePayAmount;

                                $AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
                                
                                $tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));
                                $result['transactionid'] = $responseId;
                                $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $txnAmount, $merchantID, $crtxnID = '', $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);

								$user      = $comp_data['qbwc_username'];
                                if(!is_numeric($inID))
								    $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
							}
                            $payIndex++;
						}
					} else {
                        $result['transactionid'] = $responseId;
                        $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
					}

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number     = '';
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['companyEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['firstName'] . ' ' . $comp_data['lastName'];

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $result['transactionid'] = $responseId;
                    $id              = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['AUTH_RESP_TEXT'] . '</div>');
                }
            
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            
            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->input->post('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'billing_address1'  => $this->input->post('baddress1'),
                'billing_address2'  => $this->input->post('baddress2'),
                'billing_city'      => $this->input->post('bcity'),
                'billing_zip'       => $this->input->post('bzipcode'),
                'billing_state'     => $this->input->post('bstate'),
                'billing_country'   => $this->input->post('bcountry'),
                'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'shipping_address1' => $this->input->post('address1'),
                'shipping_address2' => $this->input->post('address2'),
                'shipping_city'     => $this->input->post('city'),
                'shipping_zip'      => $this->input->post('zipcode'),
                'shipping_state'    => $this->input->post('state'),
                'shiping_counry'    => $this->input->post('country'),
                'Phone'             => $this->input->post('phone'),
                'Contact'           => $this->input->post('email'),
                'proccess_url'      => 'Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('Payments/create_customer_esale', 'refresh');
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $inputData = $this->input->post();
            $responseId = 'TXNFAILED-'.time();

            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {

                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];

                $customerID = $this->input->post('customerID');
                if (!$customerID || empty($customerID)) {
                    $customerID = create_card_customer($this->input->post(), '1');
                }

                $comp_data = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
                $companyID = $comp_data['companyID'];

                if ($this->input->post('card_number') != "") {
                    $card_no  = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');

                    $exyear = $this->input->post('expiry_year');
                    $cvv     = $this->input->post('cvv');
                } else {

                    $cardID = $this->input->post('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];

                    $exyear = $card_data['cardYear'];
                    $cvv = $card_data['CardCVV'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $exyear1.$expmonth;

                $name    = $this->input->post('firstName') . ' ' . $this->input->post('lastName');
                $baddress1 = $this->input->post('baddress1');
                $baddress2 = $this->input->post('baddress2');
                $country = $this->input->post('bcountry');
                $city    = $this->input->post('bcity');
                $state   = $this->input->post('bstate');
                $amount  = $this->input->post('totalamount');
                $zipcode = ($this->input->post('bzipcode')) ? $this->input->post('bzipcode') : '74035';
                $calamount = $amount * 100;

                $orderId = time();
                $amount = number_format($amount,2,'.','');
                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE2',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',
                );
                if (!empty($po_number)) {
                    $transaction['ORDER_NBR'] = $po_number;
                }
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $transaction['INVOICE_NBR'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }
                if($cvv != ''){
                    $transaction['CVV2'] = $cvv;
                }
                if($baddress1 != ''){
                    $transaction['ADDRESS'] = $baddress1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);

                $crtxnID = '';
                $invID = $inID = '';
                
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    $responseId = $result['AUTH_GUID'];
                    /* This block is created for saving Card info in encrypted form  */

                    if ($this->input->post('card_number') != "") {

                        $card_no  = $this->input->post('card_number');
                        $cvv      = $this->input->post('cvv');
                        $cardType = $this->general_model->getType($card_no);

                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $cardType,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'Billing_Addr1'   => $this->input->post('baddress1'),
                            'Billing_Addr2'   => $this->input->post('baddress2'),
                            'Billing_City'    => $this->input->post('bcity'),
                            'Billing_Country' => $this->input->post('bcountry'),
                            'Billing_Contact' => $this->input->post('phone'),
                            'Billing_State'   => $this->input->post('bstate'),
                            'Billing_Zipcode' => $this->input->post('bzipcode'),
                            'customerListID'  => $this->input->post('customerID'),
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                        );

                        $id1 = $this->card_model->process_card($card_data);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg = $result['AUTH_RESP_TEXT'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  '. $err_msg .'</strong></div>');
                }
                $result['transactionid'] = $responseId;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>');
            }

           

            $invoice_IDs = array();

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'billing_address1'  => $this->input->post('baddress1'),
                'billing_address2'  => $this->input->post('baddress2'),
                'billing_city'      => $this->input->post('bcity'),
                'billing_zip'       => $this->input->post('bzipcode'),
                'billing_state'     => $this->input->post('bstate'),
                'billing_country'   => $this->input->post('bcountry'),
                'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'shipping_address1' => $this->input->post('address1'),
                'shipping_address2' => $this->input->post('address2'),
                'shipping_city'     => $this->input->post('city'),
                'shipping_zip'      => $this->input->post('zipcode'),
                'shipping_state'    => $this->input->post('state'),
                'shiping_counry'    => $this->input->post('country'),
                'Phone'             => $this->input->post('phone'),
                'Contact'           => $this->input->post('email'),
                'proccess_url'      => 'Payments/create_customer_auth',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Authorize',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            redirect('home/transation_sale_receipt', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $compdata          = $this->customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/payment_auth', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID     = $this->input->post('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }
            $transactionid = $transactionID = 'TXNFAILED'.time();

            if ($tID != '' && !empty($gt_result)) {
                
                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);

                $customerID = $paydata['customerListID'];
                $amount     = $paydata['transactionAmount'];
                $customerID = $paydata['customerListID'];
                $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $orderId = time();
                $amount = number_format($amount,2,'.','');
                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'ORIG_AUTH_GUID' => $tID,
                    'TRAN_TYPE' => 'CCE4',
                    'CARD_ENT_METH' => 'Z',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',
                );
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    $transactionid = $transactionID = $result['AUTH_GUID'];
                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "4");

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];

                    $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['Contact'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['FullName'];
                    if ($chh_mail == '1') {

                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                    }
                    $this->session->set_flashdata('success', ' Successfully Captured Authorization');
                } else {
                    
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $result['AUTH_RESP_TEXT'] . '</div>');
                }
                $result['transactionid'] = $transactionid;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');

            }
            $invoice_IDs = array();
           

            $receipt_data = array(
                'proccess_url'      => 'Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            

            redirect('home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');
            
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $user_id = $da['merchantID'];
        }

        if ($this->input->post('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }
        $custom_data_fields = [];

        $invoices   = $this->input->post('multi_inv');
        $cardID     = $this->input->post('CardID1');
        $gateway    = $this->input->post('gateway1');
        $customerID = $this->input->post('customermultiProcessID');
        $comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID, 'qbmerchantID' => $user_id));
        $companyID  = $comp_data['companyID'];
        $transactionid = 'TXNFAILED-'.time();
        $cusproID = '';
        $error    = '';
        $cusproID = $this->input->post('customermultiProcessID');

        $resellerID = $this->resellerID;
        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && !empty($invoices)) {
            foreach ($invoices as $invoiceID) {
                $pay_amounts = $this->input->post('pay_amount' . $invoiceID);
                $in_data     = $this->quickbooks->get_invoice_data_pay($invoiceID);

                $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                $comp_data   = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'FirstName', 'LastName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $user_id));
                
                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];

                if ($cardID != "" || $gateway != "") {

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['Customer_ListID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->input->post('card_number');
                            $expmonth   = $this->input->post('expiry');
                            $exyear     = $this->input->post('expiry_year');
                            $cvv        = $this->input->post('cvv');
    
                            $address1 = $this->input->post('address1');
                            $address2 = $this->input->post('address2');
                            $city     = $this->input->post('city');
                            $country  = $this->input->post('country');
                            $phone    = $this->input->post('contact');
                            $state    = $this->input->post('state');
                            $zipcode  = $this->input->post('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        if (!empty($cardID)) {

                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];

                            $amount = $pay_amounts;
                            $amount = $amount - $cr_amount;

                            $name    = $in_data['Customer_FullName'];
                            $address = $in_data['ShipAddress_Addr1'];
                           
                            $exyear1 = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $exyear1.$expmonth;
                            $amount = number_format($amount,2,'.','');
                            $CUST_NBR = $gt_result['gatewayUsername'];
                            $MERCH_NBR = $gt_result['gatewayPassword'];
                            $DBA_NBR = $gt_result['gatewaySignature'];
                            $TERMINAL_NBR = $gt_result['extra_field_1'];
                            $orderId = time();
                           
                            $transaction = array(
                                'CUST_NBR' => $CUST_NBR,
                                'MERCH_NBR' => $MERCH_NBR,
                                'DBA_NBR' => $DBA_NBR,
                                'TERMINAL_NBR' => $TERMINAL_NBR,
                                'TRAN_TYPE' => 'CCE1',
                                'ACCOUNT_NBR' => $card_no,
                                'EXP_DATE' => $expry,
                                'CARD_ENT_METH' => 'E',
                                'INDUSTRY_TYPE' => 'E',
                                'AMOUNT' => $amount,
                                'TRAN_NBR' => rand(1,10),
                                'INVOICE_NBR' => $invoiceID,
                                'ORDER_NBR' => $orderId,
                                'BATCH_ID' => time(),
                                'VERBOSE_RESPONSE' => 'Y',
                            );
                            if($comp_data['FirstName'] != ''){
                                $transaction['FIRST_NAME'] = $comp_data['FirstName'];
                            }
                            if($comp_data['LastName'] != ''){
                                $transaction['LAST_NAME'] = $comp_data['LastName'];
                            }
                            if($cvv != ''){
                                $transaction['CVV2'] = $cvv;
                            }
                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            
                            if($zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }

                           
                            $gatewayTransaction              = new EPX();
                            $result = $gatewayTransaction->processTransaction($transaction);
                            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                            {
                                $responseId = $transactionid = $result['AUTH_GUID'];
                                $txnID   = $in_data['TxnID'];
                                $ispaid  = 'true';
                                $bamount = $in_data['BalanceRemaining'] - $amount;
                                if ($bamount > 0) {
                                    $ispaid = 'false';
                                }

                                $app_amount = $in_data['AppliedAmount'] + (-$amount);
                                $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => $app_amount, 'BalanceRemaining' => $bamount);
                                $condition  = array('TxnID' => $in_data['TxnID']);
                                $this->general_model->update_row_data('qb_test_invoice', $condition, $data);

                                $user = $in_data['qbwc_username'];

                                if ($this->input->post('card_number') != "" && $cardID == "new1" && !($this->input->post('tc'))) {

                                    $card_no  = $this->input->post('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->input->post('expiry');
                                    $exyear   = $this->input->post('expiry_year');
                                    $cvv      = $this->input->post('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->input->post('address1'),
                                        'Billing_Addr2'   => $this->input->post('address2'),
                                        'Billing_City'    => $this->input->post('city'),
                                        'Billing_Country' => $this->input->post('country'),
                                        'Billing_Contact' => $this->input->post('phone'),
                                        'Billing_State'   => $this->input->post('state'),
                                        'Billing_Zipcode' => $this->input->post('zipcode'),
                                        'customerListID'  => $in_data['Customer_ListID'],

                                        'companyID'       => $comp_data['companyID'],
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                }

                                if ($chh_mail == '1') {
                                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                                    $ref_number     = $in_data['RefNumber'];
                                    $tr_date        = date('Y-m-d H:i:s');
                                    $toEmail        = $comp_data['Contact'];
                                    $company        = $comp_data['companyName'];
                                    $customer       = $comp_data['FullName'];

                                    $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                                }
                                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                            } else {
        
                                $err_msg = $result['AUTH_RESP_TEXT'];
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }
                            $result['transactionid'] = $transactionid;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $user_id, $crtxnID='', $resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                            if ( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' && !is_numeric($invoiceID)) {
                                $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                            }

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>');
                }

            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - Gateway and Card are required</strong>.</div>');
        }

        if(!$checkPlan){
            
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
        
        if ($cusproID != "") {
            redirect('home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('home/invoices', 'refresh');
        }

    }
    public function create_customer_void()
    {
        if (!empty($this->input->post())) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->input->post('txnvoidID');
            $con     = array(
                'transactionID' => $tID,
            );
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($tID != '' && !empty($gt_result)) {
                $con     = array('transactionID' => $tID);
                $paydata = $this->general_model->get_row_data('customer_transaction', $con);

                $customerID = $paydata['customerListID'];
                $gatewayName = $paydata['gateway'];
                $accountType = 'checking';
                
                $amount     = $paydata['transactionAmount'];
                $customerID = $paydata['customerListID'];
                $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $amount = number_format($amount,2,'.','');
                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'ORIG_AUTH_GUID' => $tID,
                    'TRAN_NBR' => rand(1,10),
                    'VERBOSE_RESPONSE' => 'Y',
                );
                if (strpos($gatewayName, 'ECheck') !== false) {
                    $cardData = $this->card_model->getCardData($customerID,3);
                    if(isset($cardData[0]['accountType']) && $cardData[0]['accountType'] != ''){
                        $accountType = $cardData[0]['accountType'];
                    }
                    if($accountType == 'savings'){
                        $txnType = 'CKSX';
                    }else{
                        $txnType = 'CKCX';
                    }
                    
                    $echeckType = true;
                }else{
                    $echeckType = false;
                    $txnType = 'CCEX';
                    $transaction['CARD_ENT_METH'] = 'Z';
                    $transaction['INDUSTRY_TYPE'] = 'E';
                }
                $transaction['TRAN_TYPE'] = $txnType;

                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                    
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "3");

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                    if ($chh_mail == '1') {
                        $condition  = array('transactionID' => $tID);
                        $customerID = $paydata['customerListID'];

                        $comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchantID));
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['Contact'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['FullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                    }

                    $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                } else {
                    $err_msg = $result['AUTH_RESP_TEXT'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');
            }
        }
        redirect('Payments/payment_capture', 'refresh');
    }

}
