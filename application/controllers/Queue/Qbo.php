<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
require APPPATH .'libraries/qbo/QBO_data.php';

class Qbo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('QBO_models/qbo_company_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);
    }

    /**
     * Run QBO Sync Function
     *
     * @return void
     */
    public function run_sync($start_status=null){
        
        $this->load->model('Queue_Model', 'queue_model');
        
        // Pick either Pending Queue or Paused Queue
        $status = ($start_status == 'pause') ? Queue_Model :: QUEUE_PAUSED : Queue_Model :: QUEUE_PENDING;
       
        $queue = $this->queue_model->getNextQueue($status);

        // Queue will break after this limit and resume in next run
        $queueMaxLimit = 600;

        $queueID = '';

        if($queue) {

            $queueID = $queue->id;

            // Update Queue Status from Pending to Inprogress
            $this->queue_model->updateQueueStatus($queue->id, Queue_Model :: QUEUE_INPROGRESS);
            
            try{

                $queueNextStatus = Queue_Model :: QUEUE_COMPLETED;
                $queueNextOffset = $queue->queue_offset + $queueMaxLimit;
            
                $objQboData = new QBO_data($queue->merchantID); 

                // Pass the Queue Offset and Max Limit
                $args = ['offset' => $queue->queue_offset, 'max_limit' => $queueMaxLimit, 'forcePull' => true];

                switch($queue->queue_operation){
                    case 'QBO/sync_customers':
                        $objQboData->get_customer_data($args, $queueNextStatus);
                    break;
                    case 'QBO/sync_invoices':
                        $objQboData->get_invoice_data($args, $queueNextStatus);
                    break;
                    case 'QBO/sync_products':
                        $objQboData->get_items_data();
                    break;
                    case 'QBO/sync_payment_methods':
                        $objQboData->get_payment_method();
                    break;
                    case 'QBO/sync_all':
                        $objQboData->syncAll();
                    break;
                }

                // Update Queue Status to Failed
                $this->queue_model->updateQueueStatus($queue->id, $queueNextStatus, $queueNextOffset);

            } catch (Exception $e) {
                // Update Queue Status to Failed
                $this->queue_model->updateQueueStatus($queue->id, Queue_Model :: QUEUE_FAILED, $queueNextOffset, ['message' => $e->getMessage()]);
            }
        }
        exit('Queue #'.$queueID.' Ended');
    }

}