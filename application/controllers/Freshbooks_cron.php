<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


ob_start();
require_once dirname(__FILE__) . '/../../vendor/autoload.php';

use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;

use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;

include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

include APPPATH . 'third_party/Fluidpay.class.php';
include APPPATH . 'third_party/nmiDirectPost.class.php';
include APPPATH . 'third_party/nmiCustomerVault.class.php';
include APPPATH . 'third_party/PayTraceAPINEW.php';
include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

class Freshbooks_cron extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        include_once APPPATH . 'libraries/Freshbooks_data.php';

        $this->load->model('general_model');
        $this->load->model('Freshbook_models/fb_subscription_model');
        $this->load->config('fluidpay');

        $this->load->config('payarc');
        $this->load->library('PayarcGateway');

        $this->load->config('maverick');
        $this->load->library('MaverickGateway');
    }

    public function index()
    {

    }

    public function refresh_fbs_token()
    {

        $Freshbooks_datas = $this->general_model->get_table_data('tbl_freshbooks', '');

        $fb_data  = $this->general_model->get_row_data('tbl_freshbooks_config', array('id' => 1));
        $clientID = $fb_data['ClientID'];
        $key      = $fb_data['ClientSecret'];
        $refreshURL =  $fb_data['refreshURL'];
        if (!empty($Freshbooks_datas)) {
            foreach ($Freshbooks_datas as $Freshbooks_data) {

                if ($Freshbooks_data['accountType'] == 1) {

                    $refreshtoken = $Freshbooks_data['refreshToken'];
                    $curl         = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL            => "https://api.freshbooks.com/auth/oauth/token",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING       => "",
                        CURLOPT_MAXREDIRS      => 10,
                        CURLOPT_TIMEOUT        => 30,
                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST  => "POST",
                        CURLOPT_POSTFIELDS     => "{\n    \"grant_type\": \"refresh_token\",\n    \"client_secret\": \"$key\",\n    \"refresh_token\": \"$refreshtoken\",\n    \"client_id\": \"$clientID\",\n    \"redirect_uri\": \"$refreshURL/\"\n}",
                        CURLOPT_HTTPHEADER     => array(
                            "api-version: alpha",
                            "cache-control: no-cache",
                            "content-type: application/json",
                        ),
                    ));

                    $response = curl_exec($curl);

                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        echo "cURL Error #:" . $err;
                    }
                    $response = json_decode($response);

                    if (isset($response->access_token) && !empty($response->access_token) && isset($response->refresh_token) && !empty($response->refresh_token)) {
                        $in_data['accessToken']  = $response->access_token;
                        $in_data['refreshToken'] = $response->refresh_token;
                        $in_data['merchantID']   = $Freshbooks_data['merchantID'];

                        $chk_condition = array('merchantID' => $Freshbooks_data['merchantID']);
                        $num           = $this->general_model->get_num_rows('tbl_freshbooks', $chk_condition);
                        if ($num > 0) {
                            $in_data['updatedAt'] = date('Y-m-d H:i:s');
                            $this->general_model->update_row_data('tbl_freshbooks', $chk_condition, $in_data);
                        } else {
                            $in_data['createdAt'] = date('Y-m-d H:i:s');
                            $this->general_model->insert_row('tbl_freshbooks', $in_data);
                        }
                    }
                }

            }
        }
    }

    public function create_invoice()
    {

        $subsdata = $this->fb_subscription_model->get_subcription_data();

        if (!empty($subsdata)) {

            foreach ($subsdata as $subs) {
                if ($subs['generatedInvoice'] < $subs['totalInvoice'] || $subs['totalInvoice'] == 0) {
                    $item_val = $this->fb_subscription_model->get_sub_invoice('tbl_subscription_invoice_item_fb', array("subscriptionID" => $subs['subscriptionID']));

                    $duedate   = date("Y-m-d", strtotime($subs['nextGeneratingDate']));
                    $randomNum = substr(str_shuffle("0123456789"), 0, 5);

                    $in_num   = $subs['generatedInvoice'];
                    $paycycle = $subs['invoiceFrequency'];
                    $date     = $subs['firstDate'];
                    $merchID = $subs['merchantDataID'];

                    if ($paycycle == 'dly') {
                        $in_num    = ($in_num) ? $in_num + 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
                    }
                    if ($paycycle == '1wk') {
                        $in_num    = ($in_num) ? $in_num + 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                    }

                    if ($paycycle == '2wk') {
                        $in_num    = ($in_num) ? $in_num + 2 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                    }
                    if ($paycycle == 'mon') {
                        $in_num    = ($in_num) ? $in_num + 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
                    if ($paycycle == '2mn') {
                        $in_num    = ($in_num) ? $in_num + 2 * 1 : '1';
                        $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
                    }
                    if ($paycycle == 'qtr') {
                        $in_num    = ($in_num) ? $in_num + 3 * 1 : '1';
                        $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
                    }
                    if ($paycycle == 'six') {
                        $in_num    = ($in_num) ? $in_num + 6 * 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
                    if ($paycycle == 'yrl') {
                        $in_num    = ($in_num) ? $in_num + 12 * 1 : '0';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }

                    if ($paycycle == '2yr') {
                        $in_num    = ($in_num) ? $in_num + 2 * 12 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
                    if ($paycycle == '3yr') {
                        $in_num    = ($in_num) ? $in_num + 3 * 12 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
                    if ($this->general_model->update_row_data('tbl_subscription_invoice_item_fb',
                        array('subscriptionID' => $subs['subscriptionID']), array('invoiceDataID' => $randomNum))) {

                        $val = array(
                            'merchantID' => $subs['merchantDataID'],
                        );
                        $Freshbooks_data = $this->general_model->get_row_data('tbl_freshbooks', $val);
                        $resellerID      = $subs['resellerID'];
                        $redID           = $resellerID;
                        $condition1      = array('resellerID' => $resellerID);
                        
                        $sub1    = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                        $domain1 = $sub1['merchantPortalURL'];
                        $URL1    = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth'; 
                        
                        $subdomain           = $Freshbooks_data['sub_domain'];
                        $key                 = $Freshbooks_data['secretKey'];
                        $accessToken         = $Freshbooks_data['accessToken'];
                        $access_token_secret = $Freshbooks_data['oauth_token_secret'];
                        
                        $nexnum = 1000;
                        $inv_po = 'FB-1000';
                        $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
                        if (!empty($in_data)) {
                            $inv_pre   =   $in_data['prefix'];
                            $nexnum    =   $in_data['postfix'] + 1;
                            $inv_po    = $inv_pre . $nexnum;
                        }
                        
                        $tname = '';
                        $taxval = $trate = $taxID = 0;
                        
                        $totalAmount = $i = 0;
                        foreach ($item_val as $listItems) {
                            if($subs['generatedInvoice'] > 1 && $listItems['oneTimeCharge']){
								continue;
							}
                            $totalAmountItem = $taxval = 0;
                            $totalAmountItem = ($listItems['itemQuantity'] * $listItems['itemRate']);
                            $taxval = ($totalAmountItem * $trate)/100;
                            
                            $item_val[$i]['itemTotal'] = $totalAmountItem + $taxval;
                            $item_val[$i]['itemTax'] = $taxID;
                            $item_val[$i]['taxName']       = $tname;
                            $item_val[$i]['taxPercent']    = $trate;
                            $item_val[$i]['taxValue']      = $taxval;
                            
                            ++$i;
                        }
                        

                        if (isset($accessToken) && isset($access_token_secret)) {

                            if($Freshbooks_data['accountType'] == 1){

                                $fb_items = [];
                                foreach ($item_val as $single_item) {
                                    $item = [];

                                    $itemTaxData = $this->general_model->get_table_data('tbl_freshbook_subscription_item_tax', [ 'itemID' => $single_item['itemID'], 'merchantID' => $merchID ]);
                                    if(!empty($itemTaxData)){
                                        $taxIndex = 1;
                                        foreach($itemTaxData as $taxItem){
                                            $item["taxName$taxIndex"] = $taxItem['friendlyName'];
                                            $item["taxNumber$taxIndex"] = $taxItem['number'];
                                            $item["taxAmount$taxIndex"] =  $taxItem['taxRate'];
                                            ++$taxIndex;
                                        }
                                    }

                                    $item['qty'] = $single_item['itemQuantity'];
                                    $item['name'] = $single_item['itemFullName'];
                                    $item['description'] = $single_item['itemDescription'];
                                    $item['type'] = 0;
                                    $item['unit_cost'] = array('amount' => $single_item['itemRate'], 'code' => 'USD');
                                    $fb_items[] = $item;
                                }

                                $c = new FreshbooksNew($subdomain, $key, $URL1, $subdomain, $accessToken, $access_token_secret, $merchID);
                                $invoice = array(
                                    'customerid' => $subs['customerID'], 'create_date' => date('Y-m-d'), 'due_offset_days' => '', 'invoice_number' => $inv_po, 'country' => $subs['country'], 'province' => $subs['state'], 'street' => $subs['address2'], 'address' => $subs['address1'], 'city' => $subs['city'],
                                    'status' => 1, "lines" => $fb_items
                                );

                                $invoices = $c->add_invoices($invoice);

                                if ($invoices) {
                                    foreach ($item_val as $itm) {
                                        $ins['itemID']      = $itm['itemID'];
                                        $ins['invoiceID']   = $invoices;
                                        $ins['merchantID']  = $merchID;
                                        $ins['totalAmount']      = $itm['itemTotal'];
                                        $ins['itemQty']          = $itm['itemQuantity'];
                                        $ins['itemDescription']  = $itm['itemDescription'];
                                        $ins['itemPrice'] = $itm['itemRate'];
                                        $ins['taxName']     = $itm['taxName'];
                                        $ins['taxValue']    = $itm['taxValue'];
                                        $ins['taxPercent']  = $itm['taxPercent'];
                                        $ins['createdAt']   = date('Y-m-d H:i:s');

                                        $this->general_model->insert_row('tbl_freshbook_invoice_item', $ins);
                                    }
                                }

                            } else {

                                $c = new Freshbooks($subdomain, $key, null, $subdomain, $accessToken, $access_token_secret);
        
                                if ($subs['taxID'] == '' && $subs['taxPer'] == 0) {
                                    $lineArray = "";
        
                                    for ($i = 0; $i < count($item_val); $i++) {
        
                                        $lineArray .= '<line><name>' . $item_val[$i]['itemListID'] . '</name>';
                                        $lineArray .= '<description>' . $item_val[$i]['itemDescription'] . '</description>';
                                        $lineArray .= '<unit_cost>' . $item_val[$i]['itemRate'] . '</unit_cost>';
                                        $lineArray .= '<quantity>' . $item_val[$i]['itemQuantity'] . '</quantity>';
                                        $lineArray .= '<type>Item</type></line>';
        
                                    }
        
                                    $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                    <request method="invoice.create">
                                    <invoice>
                                    <client_id>' . $subs['customerID'] . '</client_id>
                                    <contacts>
                                        <contact>
                                            <contact_id>0</contact_id>
                                        </contact>
                                        </contacts>
                                        <number>' . $inv_po . '</number>
                                        <status>draft</status>
                                        <p_street1>' . $subs['address1'] . '</p_street1>
                                        <p_street2>' . $subs['address2'] . '</p_street2>
                                        <p_city>' . $subs['city'] . '</p_city>
                                        <p_state>' . $subs['state'] . '</p_state>
                                        <p_country>' . $subs['country'] . '</p_country>
                                        <p_code>' . $subs['zipcode'] . '</p_code>
                                        <lines>' .
                                                $lineArray . '
                                        </lines>
                                        </invoice>
                                        </request> ';
        
                                    $invoices = $c->add_invoices($invoice);
        
                                } else {
        
                                    $lineArray = "";
        
                                    for ($i = 0; $i < count($item_val); $i++) {
        
                                        $lineArray .= '<line><name>' . $item_val[$i]['itemListID'] . '</name>';
                                        $lineArray .= '<description>' . $item_val[$i]['itemDescription'] . '</description>';
                                        $lineArray .= '<unit_cost>' . $item_val[$i]['itemRate'] . '</unit_cost>';
                                        $lineArray .= '<quantity>' . $item_val[$i]['itemQuantity'] . '</quantity> <tax1_name>' . $subs['taxID'] . '</tax1_name><tax1_percent>' . $subs['taxPer'] . '</tax1_percent>';
                                        $lineArray .= '<type>Item</type></line>';
        
                                    }
        
                                    $invoice = '<?xml version="1.0" encoding="utf-8"?>
                                    <request method="invoice.create">
                                    <invoice>
                                    <client_id>' . $subs['customerID'] . '</client_id>
                                    <contacts>
                                        <contact>
                                            <contact_id>0</contact_id>
                                        </contact>
                                        </contacts>
                                        <status>draft</status>
                                        <p_street1>' . $subs['address1'] . '</p_street1>
                                        <p_street2>' . $subs['address2'] . '</p_street2>
                                        <p_city>' . $subs['city'] . '</p_city>
                                        <p_state>' . $subs['state'] . '</p_state>
                                        <p_country>' . $subs['country'] . '</p_country>
                                        <p_code>' . $subs['zipcode'] . '</p_code>
                                        <lines>' .
                                                $lineArray . '
                                        </lines>
                                        </invoice>
                                        </request> ';
        
                                    $invoices = $c->add_invoices($invoice);
        
                                }
                            }


                            if (is_numeric($invoices)) {
                                $schedule['invoiceID']  = $invoices;

                                $subscription_auto_invoices_data = [
                                    'subscriptionID' => $subs['subscriptionID'],
                                    'invoiceID' => $schedule['invoiceID'],
                                    'app_type' => 3 //FB
                                ];
            
                                $this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);

                                $schedule['gatewayID']  = $subs['paymentGateway'];
                                $schedule['cardID']     = $subs['cardID'];
                                $schedule['customerID'] = $subs['customerID'];

                                if ($subs['automaticPayment']) {
                                    $schedule['autoPay']       = 1;
                                    $schedule['paymentMethod'] = 1;
                                } else {
                                    $schedule['autoPay']       = 1;
                                    $schedule['paymentMethod'] = 2;
                                }

                                
                                $gen_inv = $subs['generatedInvoice'] + 1;
                                $this->general_model->update_row_data('tbl_subscriptions_fb',
                                    array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));

                                $schedule['merchantID'] = $subs['merchantDataID'];
                                $schedule['createdAt']  = date('Y-m-d H:i:s');
                                $schedule['updatedAt']  = date('Y-m-d H:i:s');
                                $this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);

                                $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $nexnum));

                            }

                        }

                    }
                }
            }
        }
    }

    public function pay_invoice()
    {

        $invoice_data = $this->fb_subscription_model->get_invoice_data_fresh_auto_pay();
        if (!empty($invoice_data)) {
            foreach ($invoice_data as $in_data) {
                if ($in_data['BalanceRemaining'] != '0.00') {

                    if ($in_data['DueDate'] != '') {

                        $card_data  = $this->card_model->get_single_card_data($cardID);
                        $card_no    = $card_data['CardNo'];
                        $cvv        = $card_data['CardCVV'];
                        $expmonth   = $card_data['cardMonth'];
                        $exyear     = $card_data['cardYear'];
                        $cardType   = $card_data['CardType'];
                        $address1   = $card_data['Billing_Addr1'];
                        $city       = $card_data['Billing_City'];
                        $zipcode    = $card_data['Billing_Zipcode'];
                        $state      = $card_data['Billing_State'];
                        $country    = $card_data['Billing_Country'];
                        $customerID = $in_data['Customer_ListID'];
                        $user_id    = $in_data['merchantID'];
                        $c_data     = $this->general_model->get_select_data('Freshbooks_custom_customer', array('companyID', 'firstName', 'fullName', 'lastName', 'companyName'), array('Customer_ListID' => $customerID, 'merchantID' => $user_id));

                        if ($in_data['schedule'] != "") {
                            $amount = $in_data['scheduleAmount'];
                        } else {
                            $amount = $in_data['BalanceRemaining'];
                        }

                        $pay_sts = "";

                        if ($in_data['gatewayType'] == '1' || $in_data['gatewayType'] == '9') {
                            include APPPATH . 'third_party/nmiDirectPost.class.php';
                            $nmiuser         = $in_data['gatewayUsername'];
                            $nmipass         = $in_data['gatewayPassword'];
                            $nmi_data        = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                            $Customer_ListID = $in_data['Customer_ListID'];

                            $transaction1 = new nmiDirectPost($nmi_data);
                            $transaction1->setCcNumber($card_data['CardNo']);
                            $expmonth = $card_data['cardMonth'];
                            $exyear   = $card_data['cardYear'];
                            $exyear   = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;
                            $transaction1->setCcExp($expry);
                            $transaction1->setCvv($card_data['CardCVV']);

                            $transaction1->setAmount($amount);

                            // add level III data
                            $level_request_data = [
                                'transaction' => $transaction1,
                                'card_no' => $card_data['CardNo'],
                                'merchID' => $user_id,
                                'amount' => $amount,
                                'invoice_id' => $in_data['invoiceID'],
                                'gateway' => 1
                            ];
                            $transaction1 = addlevelThreeDataInTransaction($level_request_data);

                            $transaction1->sale();
                            $result = $transaction1->execute();
                            $type   = 'sale';
                            if ($result['response_code'] == "100") {
                                $pay_sts = "SUCCESS";
                                $res     = $result;
                            } else {

                            }


                        }

                        if ($in_data['gatewayType'] == '2') {

                            $apiloginID     = $in_data['gatewayUsername'];
                            $transactionKey = $in_data['gatewayPassword'];

                            $transaction1 = new AuthorizeNetAIM($apiloginID, $transactionKey);

                            $exyear = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;

                            $transaction1->setSandbox($this->config->item('Sandbox'));
                            $transaction1->__set('company', $c_data['companyName']);
                            $transaction1->__set('first_name', $c_data['fullName']);
                            $transaction1->__set('address', $address1);
                            $transaction1->__set('country', $country);
                            $transaction1->__set('city', $city);
                            $transaction1->__set('state', $state);
                            $transaction1->__set('zip_code', $zipcode);

                            $exyear = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;

                            $crtxnID = '';
                            $type    = 'auth_capture';
                            $result  = $transaction1->authorizeAndCapture($amount, $card_no, $expry);

                            if ($result->response_code == "1") {
                              
                                $pay_sts = "SUCCESS";
                            } else {

                                $this->send_mail_data($in_data, '4');
                            }
                            
                            $res = $result;
                        }

                        if ($in_data['gatewayType'] == '3') {

                            $payusername = $in_data['gatewayUsername'];
                            $paypassword = $in_data['gatewayPassword'];
                            $integratorId = $in_data['gatewaySignature'];
                            $grant_type  = "password";

                            $payAPI = new PayTraceAPINEW();

                            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

                            //call a function of Utilities.php to verify if there is any error with OAuth token.
                            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                            if (!$oauth_moveforward) {
                                $json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

                                //set Authentication value based on the successful oAuth response.
                                //Add a space between 'Bearer' and access _token
                                $oauth_token = sprintf("Bearer %s", $json['access_token']);

                                $name     = $in_data['Customer_FullName'];
                                $address  = $in_data['ShipAddress_Addr1'];
                                $city     = $in_data['ShipAddress_City'];
                                $state    = $in_data['ShipAddress_State'];
                                $zipcode  = $in_data['ShipAddress_PostalCode'];
                                $card_no  = $card_data['CardNo'];
                                $expmonth = $card_data['cardMonth'];

                                $exyear = $card_data['cardYear'];
                                $cvv = $card_data['CardCVV'];
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }

                                $invoice_number = rand('500000', '200000');
                                $request_data = array(
                                    "amount"          => $amount,
                                    "credit_card"     => array(
                                        "number"           => $card_no,
                                        "expiration_month" => $expmonth,
                                        "expiration_year"  => $exyear),
                                    "csc"             => $cvv,
                                    "invoice_id"      => $invoice_number,
                                    "billing_address" => array(
                                        "name"           => $name,
                                        "street_address" => $address,
                                        "city"           => $city,
                                        "state"          => $state,
                                        "zip"            => $zipcode,
                                    ),
                                );
                                $type         = 'auth_capture';
                                $request_data = json_encode($request_data);
                                $result       = $payAPI->processTransaction($oauth_token, $request_data, URL_KEYED_SALE);
                                $response     = $payAPI->jsonDecode($result['temp_json_response']);

                                if ($result['http_status_code'] == '200') {
                                    $pay_sts = "SUCCESS";
                                    $txnID   = $in_data['TxnID'];

                                    // add level three data in transaction
                                    if($response['success']){
                                        $level_three_data = [
                                            'card_no' => $card_no,
                                            'merchID' => $in_data['merchantID'],
                                            'amount' => $amount,
                                            'token' => $oauth_token,
                                            'integrator_id' => $integratorId,
                                            'transaction_id' => $response['transaction_id'],
                                            'invoice_id' => $invoice_number,
                                            'gateway' => 3,
                                        ];
                                        addlevelThreeDataInTransaction($level_three_data);
                                    }
                                    $ispaid  = 'true';

                                    $bamount = $in_data['BalanceRemaining'] - $amount;
                                    if ($bamount > 0) {
                                        $ispaid = 'false';
                                    }

                                    $app_amount = $in_data['AppliedAmount'] - $amount;
                                    $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
                                    $condition  = array('TxnID' => $in_data['TxnID']);

                                    $this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $data);

                                   
                                } else {

                                    $this->send_mail_data($in_data, '4');
                                }
                                $id = $this->general_model->insert_gateway_transaction_data($result, 'pay_sale', $in_data['gatewayID'], $in_data['gatewayType'], $in_data['Customer_ListID'], $amount, $in_data['merchantID'], $crtxnID, $in_data['resellerID'], $in_data['TxnID']);

                            }

                        }

                        if ($in_data['gatewayType'] == '4') {

                            $username  = $in_data['gatewayUsername'];
                            $password  = $in_data['gatewayPassword'];
                            $signature = $in_data['gatewaySignature'];

                            $config = array(
                                'Sandbox'      => $this->config->item('Sandbox'), // Sandbox / testing mode option.
                                'APIUsername'  => $username, // PayPal API username of the API caller
                                'APIPassword'  => $password, // PayPal API password of the API caller
                                'APISignature' => $signature, // PayPal API signature of the API caller
                                'APISubject'   => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                                'APIVersion'   => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                            );

                            // Show Errors
                            if ($config['Sandbox']) {
                                error_reporting(E_ALL);
                                ini_set('display_errors', '1');
                            }

                            $this->load->library('paypal/Paypal_pro', $config);

                            $creditCardType = 'Visa';
                           
                            $creditCardNumber = $card_data['CardNo'];
                            $expDateMonth     = $card_data['cardMonth'];
                            $expDateYear      = $card_data['cardYear'];
                            $creditCardNumber = $card_no;

                            // Month must be padded with leading zero
                            $padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                            $cvv2Number   = $card_data['CardCVV'];
                            $currencyID   = "USD";
                            $cust_data    = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $in_data['Customer_ListID']));

                            $firstName   = $cust_data['firstName'];
                            $lastName    = $cust_data['lastName'];
                            $address1    = $in_data['ShipAddress_Addr1'];
                            $address2    = $in_data['ShipAddress_Addr12'];
                            $country     = $in_data['ShipAddress_Country'];
                            $city        = $in_data['ShipAddress_City'];
                            $state       = $in_data['ShipAddress_State'];
                            $zip         = $in_data['ShipAddress_PostalCode'];
                            $phone       = $cust_data['Phone'];
                            $email       = $cust_data['Contact'];
                            $companyName = $cust_data['companyName'];

                            $DPFields = array(
                                'paymentaction'    => 'Sale', // How you want to obtain payment.
                                //Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                                'ipaddress'        => '', // Required.  IP address of the payer's browser.
                                'returnfmfdetails' => '0', // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                            );

                            $CCDetails = array(
                                'creditcardtype' => $creditCardType, // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                                'acct'           => $creditCardNumber, // Required.  Credit card number.  No spaces or punctuation.
                                'expdate'        => $padDateMonth . $expDateYear, // Required.  Credit card expiration date.  Format is MMYYYY
                                'cvv2'           => $cvv2Number, // Requirements determined by your PayPal account settings.  Security digits for credit card.
                                'startdate'      => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
                                'issuenumber'    => '', // Issue number of Maestro or Solo card.  Two numeric digits max.
                            );

                            $PayerInfo = array(
                                'email'       => $email, // Email address of payer.
                                'payerid'     => '', // Unique PayPal customer ID for payer.
                                'payerstatus' => 'verified', // Status of payer.  Values are verified or unverified
                                'business'    => '', // Payer's business name.
                            );

                            $PayerName = array(
                                'salutation' => $companyName, // Payer's salutation.  20 char max.
                                'firstname'  => $firstName, // Payer's first name.  25 char max.
                                'middlename' => '', // Payer's middle name.  25 char max.
                                'lastname'   => $lastName, // Payer's last name.  25 char max.
                                'suffix'     => '', // Payer's suffix.  12 char max.
                            );

                            $BillingAddress = array(
                                'street'      => $address1, // Required.  First street address.
                                'street2'     => $address2, // Second street address.
                                'city'        => $city, // Required.  Name of City.
                                'state'       => $state, // Required. Name of State or Province.
                                'countrycode' => $country, // Required.  Country code.
                                'zip'         => $zip, // Required.  Postal code of payer.
                                'phonenum'    => $phone, // Phone Number of payer.  20 char max.
                            );

                            $PaymentDetails = array(
                                'amt'          => $amount, // Required.  Total amount of order, including shipping, handling, and tax.
                                'currencycode' => $currencyID, // Required.  Three-letter currency code.  Default is USD.
                                'itemamt'      => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                                'shippingamt'  => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                                'insuranceamt' => '', // Total shipping insurance costs for this order.
                                'shipdiscamt'  => '', // Shipping discount for the order, specified as a negative number.
                                'handlingamt'  => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                                'taxamt'       => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                                'desc'         => '', // Description of the order the customer is purchasing.  127 char max.
                                'custom'       => '', // Free-form field for your own use.  256 char max.
                                'invnum'       => '', // Your own invoice or tracking number
                                'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
                                'notifyurl'    => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                                'recurring'    => '', // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                            );

                            $PayPalRequestData = array(
                                'DPFields'       => $DPFields,
                                'CCDetails'      => $CCDetails,
                                'PayerInfo'      => $PayerInfo,
                                'PayerName'      => $PayerName,
                                'BillingAddress' => $BillingAddress,

                                'PaymentDetails' => $PaymentDetails,

                            );
                            $type         = 'auth_capture';
                            $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);

                            if (!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))) {
                                $pay_sts = "SUCCESS";
                                $txnID   = $in_data['TxnID'];
                                $ispaid  = 'true';

                                $bamount = $in_data['BalanceRemaining'] - $amount;
                                if ($bamount > 0) {
                                    $ispaid = 'false';
                                }

                                $app_amount = $in_data['AppliedAmount'] - $amount;
                                $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
                                $condition  = array('TxnID' => $in_data['TxnID']);

                                $this->general_model->update_row_data('Freshbooks_test_invoice', $condition, $data);
                                $this->send_mail_data($in_data, '5');

                            } else {

                                $this->send_mail_data($in_data, '4');
                            }
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'Paypal_sale', $in_data['gatewayID'], $in_data['gatewayType'], $in_data['Customer_ListID'], $amount, $in_data['merchantID'], $crtxnID, $in_data['resellerID'], $in_data['TxnID']);

                        }

                        if ($in_data['gatewayType'] == '5') {

                            include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';

                            $stripeKey = $in_data['gatewayUsername'];
                            $strPass   = $in_data['gatewayPassword'];
                            $amount    = $in_data['BalanceRemaining'];

                            $real_amt = (int) ($amount * 100);

                            $plugin = new ChargezoomStripe();
                            $plugin->setApiKey($stripeKey);

                            $res = \Stripe\Token::create([
                                'card' => [
                                    'number'    => $card_no,
                                    'exp_month' => $expmonth,
                                    'exp_year'  => $exyear,
                                    'cvc'       => $cvv,
                                    'name'      => $c_data['fullName'],
                                ],
                            ]);

                            $tcharge = json_encode($res);

                            $rest = json_decode($tcharge);
                            if ($rest->id) {
                                $plugin = new ChargezoomStripe();
                                $plugin->setApiKey($strPass);

                                $charge = \Stripe\Charge::create(array(
                                    "amount"      => $real_amt,
                                    "currency"    => "usd",
                                    "source"      => $rest->id,
                                    "description" => "Charge Using Stripe Gateway",

                                ));

                                $charge = json_encode($charge);

                                $result  = json_decode($charge);
                                $type    = 'stripe_sale';
                                $crtxnID = '';
                                if ($result->paid == '1' && $result->failure_code == "") {
                                    $pay_sts = "SUCCESS";
                                } else {
                                    $this->send_mail_data($in_data, '4');
                                }

                                $res = $result;
                            }

                        }
                        if ($in_data['gatewayType'] == '6') {

                            require_once APPPATH . "third_party/usaepay/usaepay.php";
                            $this->load->config('usaePay');

                            $crtxnID          = '';
                            $invNo            = mt_rand(1000000, 2000000);
                            $transaction      = new umTransaction;
                            $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
                            $transaction->key = $username;

                            $transaction->pin         = $password;
                            $transaction->usesandbox  = $this->config->item('Sandbox');
                            $transaction->invoice     = $invNo; // invoice number.  must be unique.
                            $transaction->description = "Chargezoom Invoice Payment"; // description of charge

                            $transaction->testmode = $this->config->item('TESTMODE'); // Change this to 0 for the transaction to process
                            $transaction->command  = "sale";

                            $transaction->card = $card_no;
                            $expyear           = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry            = $expmonth . $expyear;
                            $transaction->exp = $expry;
                            if ($cvv != "") {
                                $transaction->cvv2 = $cvv;
                            }

                            $transaction->billstreet  = $address1;
                            $transaction->billstreet2 = $address2;
                            $transaction->billcountry = $country;
                            $transaction->billcity    = $city;
                            $transaction->billstate   = $state;
                            $transaction->billzip     = $zipcode;

                            $transaction->shipstreet  = $address1;
                            $transaction->shipstreet2 = $address2;
                            $transaction->shipcountry = $country;
                            $transaction->shipcity    = $city;
                            $transaction->shipstate   = $state;
                            $transaction->shipzip     = $zipcode;
                            $transaction->amount      = $amount;

                            $transaction->Process();
                            $type = 'sale';
                            if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') {

                                $msg  = $transaction->result;
                                $trID = $transaction->refnum;

                                $res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
                                $pay_sts = "SUCCESS";

                            } else {
                                $msg  = $transaction->result;
                                $trID = $transaction->refnum;
                                $res  = array('transactionCode' => 300, 'status' => $msg, 'transactionId' => $trID);

                            }

                        }
                        if ($in_data['gatewayType'] == '7') {
                            $this->load->config('globalpayments');

                            $crtxnID = '';

                            $config = new PorticoConfig();

                            $config->secretApiKey = $secretApiKey;
                            $config->serviceUrl   = $this->config->item('GLOBAL_URL');

                            ServicesContainer::configureService($config);
                            $card           = new CreditCardData();
                            $card->number   = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear  = $exyear;
                            if ($cvv != "") {
                                $card->cvn = $cvv;
                            }

                            $card->cardType = $cardType;

                            $address                 = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city           = $city;
                            $address->state          = $state;
                            $address->postalCode     = $zipcode;
                            $address->country        = $country;

                            $invNo = mt_rand(1000000, 2000000);
                            try
                            {
                                $response = $card->charge($amount)
                                    ->withCurrency('USD')
                                    ->withAddress($address)
                                    ->withInvoiceNumber($invNo)
                                    ->withAllowDuplicates(true)
                                    ->execute();

                                if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {

                                    // add level three data
                                    $transaction = new Transaction();
                                    $transaction->transactionReference = new TransactionReference();
                                    $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                                    $level_three_request = [
                                        'card_no' => $card_no,
                                        'amount' => $amount,
                                        'invoice_id' => $invNo,
                                        'merchID' => $user_id,
                                        'transaction_id' => $response->transactionId,
                                        'transaction' => $transaction,
                                        'levelCommercialData' => $levelCommercialData,
                                        'gateway' => 7
                                    ];
                                    addlevelThreeDataInTransaction($level_three_request);

                                    $msg     = $response->responseMessage;
                                    $trID    = $response->transactionId;
                                    $st      = '0';
                                    $action  = 'Pay Invoice';
                                    $msg     = "Payment Success ";
                                    $res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
                                    $pay_sts = "SUCCESS";

                                } else {
                                    $res = array('transactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $trID);

                                    $this->send_mail_data($in_data, '4');
                                }

                            } catch (BuilderException $e) {
                                $error = 'Build Exception Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (ConfigurationException $e) {
                                $error = 'ConfigurationException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (GatewayException $e) {
                                $error = 'GatewayException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (UnsupportedTransactionException $e) {
                                $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (ApiException $e) {
                                $error = ' ApiException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            }

                        }

                        if ($in_data['gatewayType'] == '8') {
                            $this->load->config('cyber_pay');

                            $option =array();
                            $option['merchantID']     = trim($in_data['gatewayUsername']);
                            $option['apiKey']         = trim($in_data['gatewayPassword']);
                            $option['secretKey']      = trim($in_data['gatewaySignature']);
                            
                            if($this->config->item('Sandbox')){
                                $env   = $this->config->item('SandboxENV');
                            } else{
                                $env   = $this->config->item('ProductionENV');
                            }
                            $option['runENV']      = $env;
                            $Customer_ListID = $in_data['Customer_ListID'];

                            $commonElement = new CyberSource\ExternalConfiguration($option);
                            $config = $commonElement->ConnectionHost();
                        
                            $merchantConfig = $commonElement->merchantConfigObject();
                            $apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                            $api_instance = new CyberSource\Api\PaymentsApi($apiclient);

                            $cliRefInfoArr = [
                                "code" => $merchantCompnay
                            ];
                            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);

                            if($flag == "true") {
                                $processingInformationArr = [
                                    "capture" => true, "commerceIndicator" => "internet"
                                ];
                            } else {
                                $processingInformationArr = [
                                    "commerceIndicator" => "internet"
                                ];
                            }
                            $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);

                            $amountDetailsArr = [
                                "totalAmount" => $amount,
                                "currency" => CURRENCY
                            ];
                            $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                            $billtoArr = [
                                "firstName" => $firstName,
                                "lastName" => $lastName,
                                "address1" => $address1,
                                "postalCode" => $zipcode,
                                "locality" => $city,
                                "administrativeArea" => $state,
                                "country" => $country,
                                "phoneNumber" => $phone,
                                "company" => $companyName,
                                "email" => $email
                            ];
                            $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);

                            $orderInfoArr = [
                                "amountDetails" => $amountDetInfo, 
                                "billTo" => $billto
                            ];
                            $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);

                            $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];
                            $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                            
                            $paymentInfoArr = [
                                "card" => $card
                            ];
                            $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                        
                            $paymentRequestArr = [
                                "clientReferenceInformation" => $client_reference_information, 
                                "orderInformation" => $order_information, 
                                "paymentInformation" => $payment_information, 
                                "processingInformation" => $processingInformation
                            ];
                            $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                            
                            $api_response = list($response, $statusCode, $httpHeader) = null;

                            try
                            {
                                //Calling the Api
                                $api_response = $api_instance->createPayment($paymentRequest);
                                
                                if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201'){
                                    $trID =   $api_response[0]['id'];
                                    $msg  =   $api_response[0]['status'];
                                   
                                    $pay_sts = "SUCCESS";
                                    
                                    $code =   '200';
                                    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                } else {
                                    $trID  =   $api_response[0]['id'];
                                    $msg  =   $api_response[0]['status'];
                                    $code =   $api_response[1];
                                    $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
                                    
                                    $error = $api_response[0]['status'];
                                }
                            }
                            catch(Cybersource\ApiException $e)
                            {
                                $error = $e->getMessage();
                                $res =array('transactionCode'=>500, 'status'=>$error, 'transactionId'=> '' ); 
                            }

                        } 
                        
                        if ($in_data['gatewayType'] == '10') {

                            $apiUsername     = $in_data['gatewayUsername'];
                            $apiKey = $in_data['gatewayPassword'];

                            $payload = array(
                                "amount"          => ($amount * 100),
                                "card"     => array(
                                    "name" => $name,
                                    "number" => $card_no,
                                    "exp_month" => $expmonth,
                                    "exp_year"  => $exyear,
                                    "cvv"             => $cvv,
                                ),
                                "billing_address" => array(
                                    "line1"        => $address1,
                                    "line2" => $address2,
                                    "city"         => $city,
                                    "state"        => $state,
                                    "postal_code"  => $zip,
                                ),
                            );

                            $sdk = new iTTransaction();
                            $res = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);
                            if ($res['status_code'] == "200" || $res['status_code'] == "201") {
                                $res['status_code'] = "200";
                                $pay_sts = "SUCCESS";
                                $this->send_mail_data($in_data, '5');
                            } else {
                                $this->send_mail_data($in_data, '4');
                            }
                        } 
                        
                        if ($in_data['gatewayType'] == '11' || $in_data['gatewayType'] == '13') {

                            $apiUsername     = $in_data['gatewayUsername'];
							
							$payload = [
								"amount" => ($amount * 100),
								
								"address" => [
									"line1" => $card_data['Billing_Addr1'],
									"line2" => $card_data['Billing_Addr2'],
									"city" => $card_data['Billing_City'],
									"state" => $card_data['Billing_State'],
									"postal_code" => $card_data['Billing_Zipcode'],
									"country" => $card_data['Billing_Country']
								],
								"name" => $name, 
							];
							if($payOption == 2){
								$payload["payment_method"] = [
									'ach' => [
										"routing_number" => $card_data['routeNumber'],
                                        "account_number" => $card_data['accountNumber'],
                                        "sec_code"       => $card_data['secCodeEntryMethod'],
                                        "account_type"   => $card_data['accountType'], 
									]
								];
							} else {
								$exyear1  = substr($exyear, 2);
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
								}
								$expry = $expmonth . $exyear1;

								$payload["payment_method"] = [
									"card" => array(
                                        "entry_type"      => "keyed",
                                        "number"          => $card_no,
                                        "expiration_date" => $expry,
                                        "cvc"             => $cvv,
                                    ),
								];
							}
							
                            $gatewayTransaction              = new Fluidpay();
							$gatewayTransaction->environment = $this->gatewayEnvironment;
							$gatewayTransaction->apiKey      = $apiUsername;
							$result = $gatewayTransaction->processTransaction($payload);
                            $res = $result;
							if ($result['status'] == 'success') {
								$responseId = $result['data']['id'];
                                $pay_sts = "SUCCESS";
                            } else {
                                $this->send_mail_data($in_data, '4');
                            }
						}

                        if ($in_data['gatewayType'] == '15') {
                            
                            // Payarc

                            $apiUsername     = $in_data['gatewayUsername'];
                            
                            $exyear  = $exyear;
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }

                            $this->payarcgateway->setApiMode($this->gatewayEnvironment);
                            $this->payarcgateway->setSecretKey($apiUsername);

                            // Create Credit Card Token
                            $address_info = [
                                                'address_line1' => $card_data['Billing_Addr1'],
                                                'address_line2' => $card_data['Billing_Addr2'],
                                                'state' =>  $card_data['Billing_State'],
                                                'country' => '',
                                            ];
                            $zipcode = $card_data['Billing_Country'];

                            $token_response = $this->payarcgateway->createCreditCardToken($card_no, $expmonth, $exyear, $cvv, $address_info);

                            $token_data = json_decode($token_response['response_body'], 1);

                            if(isset($token_data['status']) && $token_data['status'] == 'error'){
                                $this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['card_no' => $card_no, 'exp_month' => $expmonth, 'exp_year' => $exyear, 'cvv' => $cvv, 'address' => $address_info], $token_data);
                                // Error while creating the credit card token
                                $pay_sts = "ERROR";
                                $err_msg      = $token_data['message'];

                            } else if(isset($token_data['data']) && !empty($token_data['data']['id'])) {

                                // If token created
                                $token_id = $token_data['data']['id'];
                        
                                $charge_payload = [];
                        
                                $charge_payload['token_id'] = $token_id;
                                
                                if(isset($email) && $email){
                                    $charge_payload['email'] = $email; // Customer's email address.
                                }
                                if(isset($phone) && $phone){
                                    $charge_payload['phone_number'] = $phone; // Customer's contact phone number..
                                }
                        
                                $charge_payload['amount'] = $amount * 100; // must be in cents and min amount is 50c USD
                        
                                $charge_payload['currency'] = 'usd'; 
                        
                                $charge_payload['capture'] = '1';
                        
                                $charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.
                        
                                if(isset($zipcode) && $zipcode) {
                                    $charge_payload['ship_to_zip'] = $zipcode; 
                                };

                                $charge_payload['statement_description'] = '';
                        
                                $charge_response = $this->payarcgateway->createCharge($charge_payload);
                        
                                $result = json_decode($charge_response['response_body'], 1);
                                
                                // Handle Card Decline Error
                                if (isset($result['data']) && $result['data']['object']== 'Charge' && !empty($result['data']['failure_message']))
                                {
                                    $result['message'] = $result['data']['failure_message'];
                                }

                                $res = $result;
                                
                                if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'submitted_for_settlement') {
                                    $responseId = $result['data']['id'];
                                    $pay_sts = "SUCCESS";
                                } else {
                                    $this->send_mail_data($in_data, '4');
                                }
                            }
                        }

                        // Maverick Payment
                        if ($in_data['gatewayType'] == '17') {

                            // Maverick Payment Gateway
                            $this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
                            $this->maverickgateway->setTerminalId($in_data['gatewayPassword']);
                            $this->maverickgateway->setAccessToken($in_data['gatewayUsername']);

							if($payOption == 2){
							    // ACH
                                // get DBA id
                                $dbaId = $this->maverickgateway->getDba(true);

                                // electronic Sale
                                $request_payload = [
                                        'amount'          => $amount,
                                        'routingNumber' => $card_data['routeNumber'],
                                        'accountName'     => $card_data['accountName'],
                                        'accountNumber'   => $card_data['accountNumber'],
                                        'accountType'     => ucwords($card_data['accountType']), // Checking or Savings
                                        'transactionType' => 'Debit', // Debit or Credit
                                        'customer' => [
                                            'email'     => $email,
                                            'firstName' => $firstName,
                                            'lastName'  => $lastName,
                                            "address1"  => $card_data['Billing_Addr1'],
                                            "address_2" => $card_data['Billing_Addr2'],
                                            "city"      => $card_data['Billing_City'],
                                            "state"     => $card_data['Billing_State'],
                                            "zipCode"   => $card_data['Billing_Zipcode'],
                                            "country"   => $card_data['Billing_Country'],
                                            "phone"     => $phone,
                                        ],
                                        'dba' => [
                                            'id' => $dbaId,
                                        ],
                                    ];
                                // Process ACH
                                $r = $this->maverickgateway->processAch($request_payload);
							} else {
                                
                                $exyear1  = substr($exyear, 2);
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
								}
                                $expry = $expmonth .'/'. $exyear1;
                                
                                // Sale Payload
                                $request_payload = [
                                    'level' => 1,
                                    'threeds' => [
                                        'id' => null,
                                    ],
                                    'amount' => $amount,
                                    'card' => [
                                        'number' => $card_no,
                                        'cvv'    => $cvv,
                                        'exp'    => $expry,
                                        'save'   => 'No',
                                        'address' => [
                                            'street' => $card_data['Billing_Addr1'],
                                            'city' => $card_data['Billing_City'],
                                            'state' => $card_data['Billing_State'],
                                            'country' => $card_data['Billing_Country'],
                                            'zip' => $card_data['Billing_Zipcode'],
                                        ]
                                    ],
                                    'contact' => [
                                        'name'   => $firstName.' '.$lastName,
                                        'email'  => $email,
                                        'phone' => $phone,
                                    ]
                                ];
            
                                // Process Sale
                                $r = $this->maverickgateway->processSale($request_payload);
							}
                            
                            $result = [];
                            
                            $rbody = json_decode($r['response_body'], 1);
                            
                            $result['data'] = $rbody;
                            
                            $result['response_code'] = $r['response_code'];
            
                            if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                                if($rbody['status']['status'] == 'Approved'){
                                    $result['status'] = 'success';
                                    $result['msg'] = $result['message'] = 'Payment success.';
                                } else {
                                    $result['status'] = 'failed';
                                    $result['msg'] = $result['message'] = 'Payment failed.';    
                                }
                            } else {
                                $result['status'] = 'failed';
                                $result['msg'] = $result['message'] = $rbody['message'];
                            }
                            
                            $res = $result;
							if ($result['status'] == 'success') {
								$responseId = $result['data']['id'];
                                $pay_sts = "SUCCESS";
                            } else {
                            }
						}
                        
                        $crtxnID = '';
                        if ($pay_sts == "SUCCESS") {
                            $txnID  = $in_data['invoiceID'];

                            $freshbookSync  = new Freshbooks_data($in_data['merchantID'], $in_data['resellerID']);
                            $invoicePayment = $freshbookSync->create_invoice_payment($in_data['invoiceID'], $amount);
                            if ($invoicePayment['status'] != 'ok') {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
                            } else {
                                $crtxnID = $invoicePayment['payment_id'];
                            }

                        }

                        $transactionByUser = ['id' => null, 'type' => 4];
                        $id = $this->general_model->insert_gateway_transaction_data($res, $type, $in_data['gatewayID'], $in_data['gatewayType'], $customerID, $amount, $in_data['merchantID'], $crtxnID, $in_data['resellerID'], $in_data['invoiceID'], false, $transactionByUser);
                    }
                }
            }
        }

    }

}
