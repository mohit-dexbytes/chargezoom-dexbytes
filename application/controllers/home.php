<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';

class Home extends CI_Controller
{
    protected $loginDetails;
	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		
		$this->load->model('general_model');
		$this->load->model('customer_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->db1->query("SET SESSION sql_mode = ''");
        $this->db->query("SET SESSION sql_mode = ''");
		if (($this->session->userdata('logged_in')['firstLogin'] == 0 &&  $this->session->userdata('logged_in')['firstLogin'] != NULL) || ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2')) {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
		$this->loginDetails = get_names();
	}



	public function index()
	{

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		/* New updated dashboard date 28-10-2020 */
		$month = date("M-Y");
		$data['recent_pay']		 = $this->company_model->get_recent_volume_dashboard($user_id,$month);
		$data['card_payment'] = $this->company_model->get_creditcard_payment($user_id,$month);

		$data['eCheck_payment'] = $this->company_model->get_echeck_payment($user_id,$month);

	
		$data['outstanding_total'] = $this->company_model->get_outstanding_payment($user_id);
		$condition 			 	= array("comp.merchantID" => $user_id);

		$data['recent_paid']    = $this->customer_model->get_recent_paid_data($user_id);
	
		$data['oldest_invs']    = $this->company_model->get_oldest_due($user_id);

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
		$data['plantype_vt'] = $this->general_model->chk_merch_plantype_vt($user_id);

		$data['plantype_vs'] = 0;

		
		$merchData = $this->general_model->get_select_data('tbl_merchant_data',array('rhgraphOption','rhgraphFromDate','rhgraphToDate'), array('merchID' => $user_id));
		
		$data['rhgraphOption'] = isset($merchData['rhgraphOption'])?$merchData['rhgraphOption']:0;
		$data['rhgraphFromDate'] = isset($merchData['rhgraphFromDate'])?$merchData['rhgraphFromDate']:'';
		$data['rhgraphToDate'] = isset($merchData['rhgraphToDate'])?$merchData['rhgraphToDate']:'';
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/index', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function pie_value()
	{
		$data = array();

		if ($this->session->userdata('logged_in')) {
			$da 	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}
		$condition 			 	= array("comp.merchantID" => $user_id);
		$data['company_due']    = $this->company_model->get_invoice_due_by_company($condition, '1');
		$data['customer_due']   = $this->company_model->get_invoice_due_by_company($condition, '0');

		echo json_encode($data);
		die;

		return false;
	}
	public function company()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];
		$condition  = array('merchantID' => $user_id);
		$data['companies'] = $this->general_model->get_table_data('tbl_company', $condition);
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_company', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function get_company_details()
	{

		$CompanyID      =  $this->czsecurity->xssCleanPostInput('id');
		$condition 		= array('id' => $CompanyID);
		$companydata	= $this->general_model->get_row_data('tbl_company', $condition);

		if (!empty($companydata)) {

?>

			<table class="table table-bordered table-striped table-vcenter">
				<thead>
					<tr>
						<th class="text-left">Attribute</th>
						<th class="visible-lg text-left">Details</th>
					</tr>
				</thead>
				<tbody>


					<tr>
						<th class="text-left"><strong>App Name</strong></th>
						<td class="text-left visible-lg"><?php echo $companydata['companyName']; ?></td>
					</tr>
					<tr>
						<th class="text-left"><strong>QBWC Username</strong></th>
						<td class="text-left visible-lg"><?php echo $companydata['qbwc_username']; ?></td>
					</tr>
					<tr>
						<th class="text-left"><strong>QBWC Password</strong></th>
						<td class="text-left visible-lg"> <?php echo $companydata['qbwc_password']; ?> </td>

					</tr>
					<tr>
						<th class="text-left"><strong>App Tag Line</strong></th>
						<td class="text-left visible-lg"><?php echo ($companydata['companyTagline']) ? $companydata['companyTagline'] : '----'; ?></td>
					</tr>
					<tr>
						<th class="text-left"><strong>Email</strong></th>
						<td class="text-left visible-lg"><?php echo $companydata['companyEmail']; ?></td>
					</tr>
					<tr>
						<th class="text-left"><strong>Contact</strong></th>
						<td class="text-left visible-lg"><?php echo $companydata['companyContact']; ?></td>
					</tr>
					<tr>
						<th class="text-left"><strong>Address</strong></th>
						<td class="text-left visible-lg"><?php echo $companydata['companyAddress1']; ?></td>
					</tr>



				</tbody>
			</table>

		<?php     }

		die;
	}


	public function serch_customer()
	{
		if (!empty($this->czsecurity->xssCleanPostInput('search_data'))) {
			$data['primary_nav'] 	= primary_nav();
			$data['template'] 		= template_variable();
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}


			$key = $this->czsecurity->xssCleanPostInput('search_data');


			$data['customers'] = $this->customer_model->get_serch_customer($key, $user_id);
			$data['activeval'] = "InActive";


			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('pages/page_customers', $data);
			$this->load->view('template/page_footer', $data);
			$this->load->view('template/template_end', $data);
		}
	}




	public function qbd_log()
	{



		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['alls'] = $this->customer_model->get_all_qbd_records($user_id);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_log', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function customer()
	{



		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
			$merchantEmailID = $this->session->userdata('logged_in')['merchantEmail'];

		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
			$merchantEmailID = $data['login_info']['merchant_data']['merchantEmail'];
		}
		$data['page_num']      = 'customer_qbd';
		if ($this->uri->segment('3') == "InActive") {
		
			$data['type'] = '0';
			$data['activeval'] = "Active";
		} else {
			
			$data['type'] = '1';
			$data['activeval'] = "InActive";
		}

		$condition1               = array('merchantID' => $user_id, 'systemMail' => 0);

		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];
		$data['merchantEmailID'] = $merchantEmailID;

		$data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_customers', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
		$this->load->view('pages/page_qbd_model', $data);
	}
	public function customer_ajax_list()
	{
		$showEmail = false;
		if($this->session->userdata('logged_in')){
		   $user_id = $this->session->userdata('logged_in')['merchID'];
		   $merchantEmailID = $this->session->userdata('logged_in')['merchantEmail'];

		   $merchID = $user_id;
		   $showEmail = true;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in')['merchantID'];
			$merchID = $user_id;
			$merchantEmailID = $this->session->userdata('user_logged_in')['merchant_data']['merchantEmail'];

			if(in_array('Send Email',$this->session->userdata('user_logged_in')['authName'])){
				$showEmail = true;
			}
		}
	   $type = $this->czsecurity->xssCleanPostInput('type');
	   $postdata = $this->input->post(null, true);
	   if($type == 0){
			$customer_data = $this->customer_model->get_all_active_customers($user_id,$postdata,0);
			$count = $this->customer_model->get_all_customer_count($user_id, 0);
	   }else{
			$customer_data = $this->customer_model->get_all_active_customers($user_id,$postdata,1);
			$count = $this->customer_model->get_all_customer_count($user_id, 1);
	   }
	   
	
	   $data = array();
	   $no = $_POST['start'];
	   
		$customers = $customer_data;
		if(isset($customers) && $customers)
		{
			foreach($customers as $customer)
			{

				// Calculate customer payment
				$customerID = $customer['ListID'];

				// Old Balance
				$balance = $customer['Payment'];
				// New Balance
				$balance = $this->customer_model->getCustomerBalancePayment($customerID, $user_id); 

				$balance = number_format((float)$balance, 2,'.','');

			
				$customerEmailId = $customer['ListID'];
				$customerEmailName = $customer['customerFullName'];
				$customerEmail = $customer['customerEmail'];
				$customerEmailCompanyID = $customer['companyID'];
				
				$emailAnchor = "<div class='text-left'>$customerEmail";
				if($showEmail){
					$emailAnchor = "<div class='text-left cust_view'><a href='#set_tempemail' onclick=\"set_template_data_qbd('$customerEmailId','$customerEmailName', '$customerEmailCompanyID','$customerEmail','$merchantEmailID')\" title='' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-original-title=''>$customerEmail</a>";
				}
				$no++;
				$row = array();
				$url = base_url('home/view_customer/'.$customer['ListID']);
				if($customer['IsActive']=='false' ) { 
						$name = $customer['customerFullName']; 
				} else {   
					if($customer['IsActive']=='true' && $customer['ListID']!=''  )
					{ 	  
						$name = '<a href='.$url.'>'. $customer['customerFullName'] .'</a>'; 
					} else { 
						$name = $customer['customerFullName']; 
					} 
				}
				$row[] = "<div class='text-left cust_view'>$name";
				$row[] = "<div class='text-left'>". $customer['customerFirstName'].' '.$customer['customerLastName']."";
				$row[] = $emailAnchor;
				$row[] = "<div class='text-right'>$".$balance."</div>";
				$data[] = $row;
			}
		}
			  
		   
	   $output = array(
		   "draw" => $_POST['draw'],
		   "recordsTotal" =>  $count,
		   "recordsFiltered" => $count,
		   "data" => $data,
	   );
	   //output to json format
	   echo json_encode($output);
	   die;
   }
	public function view_customer($cusID = '')
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$data['merchantEmailID'] = $this->session->userdata('logged_in')['merchantEmail'];
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$merchantEmailID = $this->session->userdata('user_logged_in')['merchant_data']['merchantEmail'];
			$data['merchantEmailID'] = $merchantEmailID;
			$user_id 				= $data['login_info']['merchantID'];
		}

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
			$data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}

		$data['merchID']      = $user_id;
		$data['customer'] 		  = $this->customer_model->customer_by_id($cusID);
		if ($data['customer'] != '') {

			// Convert added date in timezone 
	        if(isset($data['customer']->TimeCreated) && isset($data['login_info']['merchant_default_timezone']) && !empty($data['login_info']['merchant_default_timezone']) ){
	          $timezone = ['time' => $data['customer']->TimeCreated, 'current_format' => 'UTC', 'new_format' => $data['login_info']['merchant_default_timezone']];
	          $data['customer']->TimeCreated = getTimeBySelectedTimezone($timezone);
	        }
	        
			$data['page_num']      = 'customer_qbd';
			$data['types']  		  =	 $this->general_model->get_table_data('tbl_teplate_type', '');
			$condition				  = array('merchantID' => $user_id);
			
			$data['invoices'] 		  = $this->customer_model->get_invoice_upcomming_data($cusID, $user_id);

			
			$data['notes']   		  = $this->customer_model->get_customer_note_data($cusID, $user_id);
			
			$condition1               = array('merchantID' => $user_id, 'systemMail' => 0);

			$data['from_mail'] = DEFAULT_FROM_EMAIL;
			$data['mailDisplayName'] = $this->loginDetails['companyName'];

			$data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);

		
			$paydata					  =	$this->customer_model->get_customer_invoice_data_payment($cusID);
			$data['pay_invoice']       = ($paydata->applied_amount) ?: '0.00';
			$data['pay_upcoming']      = ($paydata->upcoming_balance) ? $paydata->upcoming_balance : '0.00';
			$data['pay_remaining']     = ($paydata->remaining_amount) ? $paydata->remaining_amount : '0.00';
			$data['pay_due_amount']  = ($paydata->applied_due) ? $paydata->applied_due : '0.00';

			$data['card_data_array']     = $this->card_model->get_customer_card_data($cusID);

			
			$mail_con  = array('merchantID' => $user_id, 'customerID' => $cusID);

			$data['editdatas']	= $this->general_model->get_email_history($mail_con);
			
			//--------------- To get  Subsriptions -----------------//
			$sub  = array('customerID' => $cusID);
			$data['getsubscriptions']   = $this->general_model->get_subscriptions_data($sub);
			$plantype = $this->general_model->chk_merch_plantype_status($user_id);
			$data['plantype'] = $plantype;
			$data['transaction_history_data']   = $this->customer_model->get_transaction_history_data($user_id, $cusID);
			
			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);

			$this->load->view('pages/page_customer_details', $data);
			$this->load->view('pages/page_qbd_model', $data);
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		} else {
			redirect('home/index', 'refresh');
		}
	}




	public function invoices2()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		if(!empty($this->input->post(null, true))) {
			$search = $this->input->post(null, true);
			$data['serachString'] = $search['search_data'];
		}

		$data['page_num']      = 'customer_qbd';
		$condition				  = array('merchantID' => $user_id);
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		
		$today 				    = date('Y-m-d');

		$condition1 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "IsPaid" => "true",  "cmp.merchantID" => $user_id);
		$condition2 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "IsPaid" => "false", "inv.userStatus !=" => 'cancel',  "cmp.merchantID" => $user_id);
		$condition3 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "IsPaid" => "false",  "inv.userStatus !=" => 'cancel', "cmp.merchantID" => $user_id);
		$condition4 			 = array("IsPaid" => "false", 'userStatus' => 'cancel', "companyID" => $user_id);

		$data['success_invoice'] = $this->company_model->get_invoice_data_count($condition1, 1);
		$data['upcomming_inv']   = $this->customer_model->get_invoice_schedule_data_count($user_id);
		$data['failed_inv']      = $this->company_model->get_invoice_data_count_failed($user_id);
		$data['cancel_inv']      = $this->customer_model->get_invoice_past_data_count($user_id);
		$invoices    			 = $this->company_model->get_invoice_data($user_id);
		$condition  = array('comp.merchantID' => $user_id);

		$data['credits'] = $this->general_model->get_credit_user_data($condition);

		$data['invoices']    = $invoices;

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_invoices', $data);
		$this->load->view('pages/page_qbd_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function invoices()
	{
		$filter = $this->czsecurity->xssCleanPostInput('status_filter');
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['filter'] = $filter;
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		if(!empty($this->input->post(null, true))) {
			$search = $this->input->post(null, true);
			if(isset($search['search_data'])){
				$data['serachString'] = $search['search_data'];
			}
		}

		
		$data['page_num']      = 'customer_qbd';
		$condition				  = array('merchantID' => $user_id);
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);

		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];

		$merchant_condition = [
			'merchID' => $user_id,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
			$data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}

		
		$today 				    = date('Y-m-d');

		$condition1 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "IsPaid" => "true",  "cmp.merchantID" => $user_id);
		$condition2 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "IsPaid" => "false", "inv.userStatus !=" => 'cancel',  "cmp.merchantID" => $user_id);
		$condition3 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < " => $today, "IsPaid" => "false",  "inv.userStatus !=" => 'cancel', "cmp.merchantID" => $user_id);
		$condition4 			 = array("IsPaid" => "false", 'userStatus' => 'cancel', "companyID" => $user_id);

		$data['success_invoice'] = $this->company_model->get_invoice_paid_data_count($user_id);
		$data['upcomming_inv']   = $this->customer_model->get_invoice_schedule_data_count($user_id);
		$data['failed_inv']      = $this->company_model->get_invoice_data_count_failed($user_id);
		$data['cancel_inv']      = $this->company_model->get_invoice_past_data_count($user_id);
		
		$condition  = array('comp.merchantID' => $user_id);

		$data['credits'] = $this->general_model->get_credit_user_data($condition);

		$data['invoices']    = array();

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
		$data['plantype_vt'] = $this->general_model->chk_merch_plantype_vt($user_id);
		$data['plantype'] = $plantype;

		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];
		$data['merchantEmail'] = $this->session->userdata('logged_in')['merchantEmail'];
		
        
		$templateList = $this->general_model->getTemplateForInvoice($user_id);


		$data['templateList'] = $templateList;
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_invoices', $data);
		$this->load->view('pages/page_qbd_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	public function invoice_ajax(){
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$filter = $this->czsecurity->xssCleanPostInput('status_filter');
		$customerID = $this->czsecurity->xssCleanPostInput('customerID');
		
		if($filter != ''){
			if($filter == 'All'){
				$invoices    			 = $this->company_model->get_invoice_data($user_id, $customerID);
				$count = $this->company_model->get_invoice_list_count($user_id, $customerID);
			}else{
				if($filter == 'Current'){
					$invoices    			 = $this->company_model->get_open_invoice_data($user_id, $customerID);
					$count = $this->company_model->get_open_invoice_data_count($user_id, $customerID);
				}
				else{
					$invoices    			 = $this->company_model->get_invoice_data_by_status($user_id, $filter, $customerID);
					$count = $this->company_model->get_invoice_data_by_status_count($user_id,$filter, $customerID);
				}
			}
		}
		else{
			$invoices = $this->company_model->get_open_invoice_data($user_id, $customerID);
			$count = $this->company_model->get_open_invoice_data_count($user_id, $customerID);
		}
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		
		$plantype_as = $this->general_model->chk_merch_plantype_as($user_id);
		$plantype_vt = $this->general_model->chk_merch_plantype_vt($user_id);
	
		$data = array();
		$no = $_POST['start'];
		foreach ($invoices as $invoice) {
			$no++;
			$row = array();
			$base_url1 = base_url('home/view_customer/' . $invoice['ListID']);
			$base_url2 = base_url('home/invoice_details/'. $invoice['TxnID']);
			if(empty($customerID)){

				$row[] = "<div class='text-center visible-lg'><input class='singleCheck' type='checkbox' name='selectRow[]'' value='" . $invoice['TxnID'] . "' ></div>";

				if ($plantype) {
					$row[] = "<div class='text-left visible-lg'>".$invoice['FullName']."";
				} else {
					$row[] = "<div class='text-left visible-lg cust_view'><a href='" . $base_url1 . "' >".$invoice['FullName']." </a>";
				}

				if ($plantype_vt) {
					$row[] = "<div class='text-right cust_view'>".$invoice['RefNumber']."";
				}else{
					$row[] = "<div class='text-right cust_view'><a href='" . $base_url2 . "' >".$invoice['RefNumber']." </a>";
				}
			}else{
				if ($plantype_vt) {
					$row[] = "<div class='text-left cust_view'>".$invoice['RefNumber']."";
				}else{
					$row[] = "<div class='text-left cust_view'><a href='" . $base_url2 . "' >".$invoice['RefNumber']." </a>";
				}
			}
			$row[] = "<div class='hidden-xs text-right'> " . date('M d, Y', strtotime($invoice['DueDate'])) . " </div>";
			
			
			$row[] = '<div class="hidden-xs text-right cust_view"> <a href="#pay_data_process" onclick="set_payment_data(\'' . $invoice['TxnID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">' . '$' . number_format(($invoice['BalanceRemaining']), 2) . ' </a> </div>';

			
			$row[] =	"<div class='text-right'>".ucfirst($invoice['status'])."</div>";
			$link = '';
			$disabled_class = '';
			if(strtolower($filter) == 'paid'){
				$transaction_history = $this->general_model->get_select_data('customer_transaction',array('gateway'), array('customerListID' => $invoice['ListID'], 'invoiceTxnID' => $invoice['TxnID'], 'merchantID' => $user_id, 'transactionType' => 'sale', 'transactionCode' => 200));

				if($transaction_history && strtolower($transaction_history['gateway']) == 'heartland echeck'){
					$disabled_class = "disabled";
				}
			}
			if ($invoice['status'] == "Paid") { 
				$link .= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li><a href="javascript:void(0);" id="txnRefund'. $invoice['TxnID'].'"  invoice-id="'. $invoice['TxnID'].'" integration-type="2" data-url="'.base_url().'ajaxRequest/getTotalRefundOfInvoice" class="refunTotalInvoiceAmountCustom">Refund</a></li>
                               
						          </ul>
						          </div> </div>';
			}
			else  if (($invoice['status'] = 'Scheduled' or  $invoice['status'] == 'Past Due')  && strtoupper($invoice['userStatus']) != strtoupper('Cancel')) { 
				$link .= '<div class="text-center">
             	            	
					<div class="btn-group dropbtn">
					<a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
					<ul class="dropdown-menu text-left">
					<li> <a href="#invoice_process" class=""  data-backdrop="static" data-keyboard="false"
				data-toggle="modal" onclick="set_invoice_process_id(\'' . $invoice['TxnID']. '\',\'' . $invoice['Customer_ListID'] . '\',\'' . $invoice['BalanceRemaining'] . '\',1);">Process</a></li>
			  <li><a href="#invoice_schedule" onclick="set_invoice_schedule_id(\'' . $invoice['TxnID'] . '\',\'' . $invoice['Customer_ListID'] . '\',\'' . $invoice['BalanceRemaining'] . '\',\'' . date('M d Y', strtotime($invoice['DueDate'])) . '\');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
				   
			   <li> <a href="#set_subs" class=""  onclick=
		   "set_sub_status_id(\'' . $invoice['TxnID']. '\',\'' . $invoice['Customer_ListID'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>';
		   if ($invoice['AppliedAmount'] == 0 || $invoice['AppliedAmount'] == '0.00') {
			$link .= '<li> <a href="#invoice_cancel" onclick="set_invoice_id(\'' . $invoice['TxnID'] . '\');" data-keyboard="false" data-toggle="modal" class="">Void</a></li>';
		   }
		   $link .= '</ul>
                                     </div> </div>  ';
			}
		 else { 
			 $link .= '<div class="btn-group dropbtn">
			 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
			 <ul class="dropdown-menu text-left">

				 <li> <a href="javascript:void(0);" disabled class="">Voided</a></li></ul>
				 </div>';
		 }
		 $row[] = $link;

			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $count,
			"recordsFiltered" => $count,
			"data" => $data,
			
		);
		//output to json format
		echo json_encode($output);
		die;	

	}
	public function invoice_details()
	{
		$invoiceID             =  $this->uri->segment(3);
		$condition1 			= array('item_ListID' => $invoiceID);
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];
		$merchant_condition = [
			'merchID' => $user_id,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
			$data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}
		$data['page_num']      = 'invoice_details';
		$conditiong				  = array('merchantID' => $user_id);

		$condition2 			= array('TxnID' => $invoiceID, 'qb_inv_merchantID' => $user_id);
		$invoice_data           = $this->general_model->get_qbd_invoice_details('qb_test_invoice', $invoiceID, $user_id);
		$data['plans']          = $this->company_model->get_plan_data_invoice($user_id);
		$code = '';
		$link_data = $this->general_model->get_row_data('tbl_template_data', array('merchantID' => $user_id, 'invoiceID' => $invoiceID));

		$data['from_mail'] = DEFAULT_FROM_EMAIL;
        $data['mailDisplayName'] = $this->loginDetails['companyName'];


		$coditionp = array('merchantID' => $user_id, 'customerPortal' => '1');

		$ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);
		
		$data['ur_data'] = $ur_data;
		$position1 = $position = 0;
		$purl = '';
		if(isset($ur_data['customerPortalURL']) && !empty($ur_data['customerPortalURL'])){
			$ttt = explode(PLINK, $ur_data['customerPortalURL']);
			$purl = $ttt[0] . PLINK . '/customer/';
		}
		$invoiceID = $invoice_data['TxnID'];
		$str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
		$shuffled = str_shuffle($str);
		$shuffled = substr($shuffled, 1, 12) . '=' . $user_id;
		$code     =    $this->safe_encode($shuffled);
		$invcode    =  $this->safe_encode($invoiceID);
		$data['paylink']  = $purl . 'update_payment/' . $code . '/' . $invcode;


		$condition3 			= array('ListID' => $invoice_data['Customer_ListID'], 'qbmerchantID' => $user_id);
		$customer_data			= $this->general_model->get_row_data('qb_test_customer', $condition3);
		
		$data['notes']   		= $this->customer_model->get_customer_note_data($invoice_data['Customer_ListID'], $user_id);
		$data['customer_data']  = $customer_data;
		$data['invoice_data']   = $invoice_data;

		$taxname = $this->company_model->get_tax_data($user_id);
		$data['taxes'] = $taxname;
		$company_ID = $this->db->select('id')->from('tbl_company')->where('merchantID', $user_id)->get();
		$cID = '';
		if($company_ID->num_rows()>0 ) {
			$company_IDs = $company_ID->row_array();
			$cID = $company_IDs['id'];
		}
		$data['invoice_items']   = $this->company_model->get_invoice_details_item_data($invoiceID,$cID);

		$con = array('invoiceTxnID' => $invoiceID);
		$transactionData = $this->general_model->get_row_data('customer_transaction', $con);
		if(!empty($transactionData)){
			if (strpos($transactionData['gateway'], 'ECheck') !== false) {
	            $transactionData['paymentType'] = 2;
	        }else if (strpos($transactionData['gateway'], 'Offline') !== false) {
	            $transactionData['paymentType'] = 'Offline Payment';
	        }else{
	            $transactionData['paymentType'] = 1;
	        }
		}
		
		
		$data['transaction'] = $transactionData;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);

		$this->load->view('pages/page_invoice_details', $data);
		$this->load->view('pages/page_qbd_model', $data);
		$this->load->view('pages/page_popup_modals', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function invoice_details_print()
	{
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		
		$invoiceID             =  $this->uri->segment(3);

		$invoice  = $this->company_model->get_invoice_details_data($invoiceID);
		
		$this->general_model->generate_invoice_pdf_qbd($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'D');
	}




	public function update_customer_invoice_cancel()
	{


		$today 				    = date('Y-m-d');
		$txnID           =  $this->czsecurity->xssCleanPostInput('invoiceID');
		$condition		 = array("DATE_FORMAT(DueDate,'%Y-%m-%d') > " => $today, "IsPaid" => "false", 'TxnID' => $txnID);
		$invData         = $this->general_model->get_row_data('qb_test_invoice', $condition);

		if (!empty($invData)) {
			$updateData  = array();
			$updateData['userStatus'] = 'cancel';

			if ($this->general_model->update_row_data('qb_test_invoice', $condition, $updateData)) {

				$this->session->set_flashdata('success', 'Successfully Updated');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Error in process.</div>');
			}
		}

		redirect('home/invoices', 'refresh');
	}





	public function update_invoice_cancel()
	{


		$today 				    = date('Y-m-d');

		$txnID           =  $this->uri->segment(3);
		$condition		 = array("DATE_FORMAT(DueDate,'%Y-%m-%d') > " => $today, "IsPaid" => "false", 'TxnID' => $txnID);
		$invData         = $this->general_model->get_row_data('qb_test_invoice', $condition);

		if (!empty($invData)) {
			$updateData  = array();
			$updateData['userStatus'] = 'cancel';

			if ($this->general_model->update_row_data('qb_test_invoice', $condition, $updateData)) {

				$this->session->set_flashdata('success', 'Successfully Updated');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>Error in process.</div>');
			}
		}

		redirect('home/invoices', 'refresh');
	}



	/**************End********************/


	public function delete_company()
	{

		$companyID = $this->czsecurity->xssCleanPostInput('companyID');
		$condition =  array('id' => $companyID);
		$row_data = $this->general_model->get_row_data('tbl_company', $condition);
		$del      = $this->general_model->delete_row_data('tbl_company', $condition);
		if ($del) {

			$username =  $row_data['qbwc_username'];
			$file = "my-quickbooks-wc-file$username.qwc";
			unlink(FCPATH . "uploads/$file");
		}

		redirect(base_url('home/company'));
	}


	/*********************Check Existing Email***************************/
	public function check_exist_email()
	{


		$companyEmail  = $this->czsecurity->xssCleanPostInput('companyEmail');
		$condition     = array('companyEmail' => $companyEmail);


		if ($this->general_model->get_num_rows('tbl_company', $condition) == 0) {

			$res = array('status' => 'true');
			echo json_encode($res);
		} else {

			$error = array('companyEmail' => 'Email already exist', 'status' => 'false');
			echo json_encode($error);
		}
		die;
	}
	
	public function plan_product(){
		
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}

		if($this->session->userdata('user_logged_in')){
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}
				
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/plan_product', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}		
	public function plan_product_ajax(){
        if($this->session->userdata('logged_in')){
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in')['merchantID'];
			$merchID = $user_id;
		}
        $cond                  = array("merchantID" => $merchID);
        $plans =  $this->company_model->get_product_list($user_id);
       $count =  $this->company_model->get_product_list_count($user_id);
        $data = array();
		$no = $_POST['start'];
        if(isset($plans) && $plans)
        {
            foreach($plans as $plan)
            {
                $no++;
                $row = array();
                $url = base_url('MerchantUser/create_product/'.$plan['ListID']);
                $row[] = "<div class='text-left cust_view'><a href='" . $url . "' data-toggle='tooltip' title='Edit' >".$plan['Name']."</a>";
                $row[] = "<div class='text-left hidden-xs'>".$plan['Pro_desc']."";
                $row[] = "<div class='text-left'>".($plan['QuantityOnHand'])?$plan['QuantityOnHand']:'0'."";
                $row[] = "<div class='hidden-xs text-left'>".$plan['Type']."";
                $row[] = "<div class='hidden-xs text-right'>$".number_format($plan['SalesPrice'],2)."";
                $data[] = $row;
            }
        }
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" =>  $count,
			"recordsFiltered" => $count,
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
		die;
    }

	public function plan_product_details()
	{


		$planID             =  $this->czsecurity->xssCleanPostInput('planID');
		$condition 		 = array('ListID' => $planID);
		$plandata		     = $this->general_model->get_row_data('qb_test_item', $condition);

		if (!empty($plandata)) {
		?>
			<table class="table table-bordered table-striped table-vcenter">
				<thead>
					<tr>
						<th class="text-left">Attribute</th>
						<th class="visible-lg text-left">Details</th>
					</tr>
				</thead>
				<tbody>

					<tr>
						<th class="text-left"><strong>Item ID</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['ListID']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong>Name</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['Name']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong>Description</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['FullName']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong>Type</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['Type']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Parent ListID</strong></th>
						<td class="text-left visible-lg"><?php echo ($plandata['Parent_ListID']) ? $plandata['Parent_ListID'] : '--'; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Subitem Of</strong></th>
						<td class="text-left visible-lg"><?php echo ($plandata['Parent_FullName']) ? $plandata['Parent_FullName'] : '--'; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Sales Price</strong></th>
						<td class="text-left visible-lg">$<?php echo number_format($plandata['SalesPrice'], 2); ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Sales Desc</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['SalesDesc']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Cost</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['PurchaseCost']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> PurchaseDesc</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['PurchaseDesc']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> PrefVendor ListID</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['PrefVendor_FullName']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> PrefVendor</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['PrefVendor_FullName']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Created Time</strong></th>
						<td class="text-left visible-lg"><?php if (!empty($plandata['TimeCreated'])) echo date('M d, Y', strtotime($plandata['TimeCreated']));
															else echo ''; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong>Modified Time</strong></th>
						<td class="text-left visible-lg"><?php if (!empty($plandata['TimeModified'])) echo date('M d, Y', strtotime($plandata['TimeModified']));
															else echo ''; ?></a></td>
					</tr>



					<tr>
						<th class="text-left"><strong> Manufacturer PartNumber</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['ManufacturerPartNumber']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Sales TaxCode ListID</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['SalesTaxCode_ListID']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Sales TaxCode FullName</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['SalesTaxCode_FullName']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Build Point</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['BuildPoint']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Reorder Point</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['ReorderPoint']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Quantity On Hand</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['QuantityOnHand']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Cost</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['AverageCost']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Quantity On Order</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['QuantityOnOrder']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Quantity On Sales Order</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['QuantityOnSalesOrder']; ?></a></td>
					</tr>
					<tr>
						<th class="text-left"><strong> Tax Rate</strong></th>
						<td class="text-left visible-lg"><?php echo $plandata['TaxRate']; ?></a></td>
					</tr>


				</tbody>
			</table>

<?php     }

		die;
	}

	/*********************Check Existing Email***************************/

	/*********************Check Existing Email***************************/
	public function get_gateway_data()
	{


		$gatewayID  = $this->czsecurity->xssCleanPostInput('gatewayID');
		$condition     = array('gatewayID' => $gatewayID);

		$res  = $this->general_model->get_row_data('tbl_merchant_gateway', $condition);

		if (!empty($res)) {

			$res['status'] = 'true';
			echo json_encode($res);
		}

		die;
	}




	//------------- Merchant gateway START ------------//

	public function gateway()
	{


		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}


		$condition	= array('merchantID' => $user_id);

		$data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway', '');
		$data['gateways'] = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_merchant', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	

	public function create_gateway()
	{
		$this->load->library('Gateway');
		$cr_status = 1;
		$ach_status = 0;
		$extra1 = '';
		$signature = '';

		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {

				$user_id 				=  $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}

			$condition				= array('merchantID' => $user_id);

			$gt_status = $isSurcharge = $surchargePercentage = 0;
			$gt_obj    = new Gateway();
			$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
			$gmID            = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
			if ($gatetype == '1') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
				$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
				$gt_status = $gt_obj->chk_nmi_gateway_auth($nmi_data);
				if ($this->czsecurity->xssCleanPostInput('nmi_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '2') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');
				$auth_data = array('user' => $nmiuser, 'password' => $nmipassword);
				$gt_status = $gt_obj->chk_auth_gateway_auth($auth_data);
				if ($this->czsecurity->xssCleanPostInput('auth_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('auth_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '3') {
				$this->load->config('paytrace');
				
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
				$auth_data = array('user' => $nmiuser, 'password' => $nmipassword);
				$signature  = PAYTRACE_INTEGRATOR_ID; 
				$gt_status = $gt_obj->chk_paytrace_gateway_auth($auth_data);
				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '4') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
				$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
				$auth_data		 = array('user' => $nmiuser, 'password' => $nmipassword, 'signature' => $signature);


				$gt_status = $gt_obj->chk_paypal_gateway_auth($auth_data);
			} else if ($gatetype == '5') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
				$auth_data		 = array('user' => $nmiuser, 'password' => $nmipassword);

				$gt_status 		 = $gt_obj->chk_stripe_gateway_auth($auth_data);
			} else if ($gatetype == '6') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
				$auth_data		 = array('user' => $nmiuser, 'password' => $nmipassword);

			

			} else if ($gatetype == '7') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
				if ($this->czsecurity->xssCleanPostInput('heart_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
				$auth_data		 = array('user' => $nmiuser, 'password' => $nmipassword);
				
			} else if ($gatetype == '8') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
				$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
			} else if ($gatetype == '9') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');
				$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
				$gt_status = $gt_obj->chk_nmi_gateway_auth($nmi_data);
				if ($this->czsecurity->xssCleanPostInput('cz_cr_status')){
					$cr_status       =  1;
				}
				else{
					$cr_status       =  0;
				}
				if ($this->czsecurity->xssCleanPostInput('cz_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '10') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
					$isSurcharge = 1;
				}
				else{
					$isSurcharge = 0;
				}
				$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
			} else if ($gatetype  == '11') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == '13') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == '12') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword');
				$gmID     = $this->czsecurity->xssCleanPostInput('tsysMerchID');

				if ($this->czsecurity->xssCleanPostInput('tsys_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('tsys_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '14') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
                $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
			} else if ($gatetype  == '15') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser');
				$nmipassword     = '';
				$cr_status       =  1;
				$ach_status       =  0;
			} else if ($gatetype  == '17') {

				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId');

				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
			} else if ($gatetype == '16') {
                $nmiuser         = $this->input->post('EPXCustNBR');
                $nmipassword     = $this->input->post('EPXMerchNBR');
                $signature     = $this->input->post('EPXDBANBR');
                $extra1   = $this->input->post('EPXterminal');
                if ($this->input->post('EPX_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
            }

			

			$gatedata        =  $this->czsecurity->xssCleanPostInput('g_list');
			$frname          = $this->czsecurity->xssCleanPostInput('frname');
			

			$insert_data    = array(
				'gatewayUsername' => $nmiuser,
				'gatewayPassword' => $nmipassword,
				'gatewayMerchantID' => $gmID,
				'gatewaySignature' => $signature,
				'extra_field_1'   => $extra1,
				'gatewayType' => $gatetype,
				'merchantID' => $user_id,
				'gatewayFriendlyName' => $frname,
				'creditCard'       => $cr_status,
				'echeckStatus'      => $ach_status,
				'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
			);

			if ($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {
				
				$val1 = array(
                    'merchantID' => $user_id,
                );
                
                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
                
                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
				$this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);
				
				$this->session->set_flashdata('success', 'Successfully Inserted');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}


			redirect(base_url('home/gateway'));
		}
	}





	//----------- TO update the gateway  --------------//

	public function update_gateway()
	{
		if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {
			$ach_status       =  0;
			$signature = '';
			$extra1 = '';
			$cr_status = 1;
			$isSurcharge = $surchargePercentage = 0;
			
			$id = $this->czsecurity->xssCleanPostInput('gatewayEditID');
			$chk_condition = array('gatewayID' => $id);
			$gatetype         = $this->czsecurity->xssCleanPostInput('gateway');
			$gmID                  = $this->czsecurity->xssCleanPostInput('mid');
			if ($gatetype == 'NMI') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword1');
				if ($this->czsecurity->xssCleanPostInput('nmi_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if (strtolower($gatetype) == 'authorize.net') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey1');

				if ($this->czsecurity->xssCleanPostInput('auth_cr_status1'))
					$cr_status       = 1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('auth_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == 'Paytrace') {
		
				$this->load->config('paytrace');

				$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword1');
				$signature  = PAYTRACE_INTEGRATOR_ID; 

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
					$cr_status       =  1; 
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == 'Paypal') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword1');
				$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature1');
			} else if ($gatetype == 'Stripe') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword1');
			} else if ($gatetype == 'USAePay') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin1');
			} else if ($gatetype == 'Heartland') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey1');
				if ($this->czsecurity->xssCleanPostInput('heart_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
			} else if ($gatetype == 'Cybersource') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
				$signature       = $this->czsecurity->xssCleanPostInput('secretKey1');
			} else if ($gatetype == 'Chargezoom') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword1');
				if ($this->czsecurity->xssCleanPostInput('cz_cr_status1')){
					$cr_status       =  1;
				}else{
					$cr_status       =  0;
				}

				if ($this->czsecurity->xssCleanPostInput('cz_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == iTransactGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY1');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('add_surcharge_box1')){
					$isSurcharge = 1;
				}
				else{
					$isSurcharge = 0;
				}
				$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage1');
			} else if ($gatetype  == FluidGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == TSYSGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword1');
				$gmID                  = $this->czsecurity->xssCleanPostInput('tsysMerchID1');
				if ($this->czsecurity->xssCleanPostInput('tsys_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('tsys_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if($gatetype == 'CardPointe') {
				$nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword1');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID1');
                $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName1');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status1', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status1', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
			}  else if ($gatetype  == BASYSGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if($gatetype == PayArcGatewayName){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser1');
                $nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
			} else if ($gatetype == EPXGatewayName) {
                $nmiuser         = $this->input->post('EPXCustNBR1');
                $nmipassword     = $this->input->post('EPXMerchNBR1');
                $signature     = $this->input->post('EPXDBANBR1');
                $extra1   = $this->input->post('EPXterminal1');
                if ($this->input->post('EPX_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
             
            }else if ($gatetype  == MaverickGatewayName) {

				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId1');

				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}

			$frname           = $this->czsecurity->xssCleanPostInput('fname');
			

			$insert_data    = array(
				'gatewayUsername' => $nmiuser,
				'gatewayPassword' => $nmipassword,
				'gatewayMerchantID' => $gmID,
				'gatewayFriendlyName' => $frname,
				'gatewaySignature' => $signature,
				'extra_field_1' => $extra1,
				'creditCard'       => $cr_status,
				'echeckStatus'      => $ach_status,
				'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
			);


			if ($this->general_model->update_row_data('tbl_merchant_gateway', $chk_condition, $insert_data)) {
				$this->session->set_flashdata('success', 'Successfully Updated');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}

			redirect(base_url('home/gateway'));
		}
	}



	public function get_gatewayedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gatewayid');
		$val = array(
			'gatewayID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
		$data['gateway'] = getGatewayNames($data['gatewayType']);
		
		echo json_encode($data);
	}



	public function set_gateway_default()
	{


		if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {

			if ($this->session->userdata('logged_in')) {
				$da['login_info'] 	= $this->session->userdata('logged_in');

				$merchID 				= $da['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$da['login_info'] 	= $this->session->userdata('user_logged_in');

				$merchID 				= $da['login_info']['merchantID'];
			}

			$id = $this->czsecurity->xssCleanPostInput('gatewayid');
			$val = array(
				'gatewayID' => $id,
			);
			$val1 = array(
				'merchantID' => $merchID,
			);
			$update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
			$this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
			$update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

			$this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
		}
		redirect(base_url('home/gateway'));
	}


	/**************Delete credit********************/

	public function delete_gateway()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				=  $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
		$condition =  array('gatewayID' => $gatewayID);
		$num = 	$this->general_model->get_num_rows('tbl_subscriptions', array('paymentGateway' => $gatewayID, 'merchantDataID' => $user_id, 'subscriptionStatus' => 1));
		if ($num == 0) {
			$del      = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition);
			if ($del) {
				$this->session->set_flashdata('success', 'Successfully Deleted');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Error: Gateway can not be deleted, Please change subscription gateway</strong></div>');
		}
		redirect(base_url('home/gateway'));
	}







	public function accounting_software()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['id'];

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_accouting _software', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function  delele_note()
	{

		if ($this->czsecurity->xssCleanPostInput('noteID') != "") {

			$noteID = $this->czsecurity->xssCleanPostInput('noteID');

			if ($this->db->query("Delete from tbl_private_note where noteID =  '" . $noteID . "' ")) {

				array('status' => "success");
				echo json_encode(array('status' => "success"));
				die;
			}
			return false;
		}
	}


	public function add_note()
	{
		if ($this->session->userdata('logged_in')) {
			$da['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $da['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$da['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['login_info']['merchantID'];
		}
		if (!empty($this->czsecurity->xssCleanPostInput('customerID'))) {
			$cusID = $this->czsecurity->xssCleanPostInput('customerID');
			if ($this->czsecurity->xssCleanPostInput('private_note') != "") {

				$private_note  = $this->czsecurity->xssCleanPostInput('private_note');
				$data_ar = array('privateNote' => $private_note, 'privateNoteDate' => date('Y-m-d H:i:s'), 'customerID' => $cusID, 'merchantID' => $user_id);
				$id   = $this->general_model->insert_row('tbl_private_note', $data_ar);
				if ($id > 0) {
					array('status' => "success");
					echo json_encode(array('status' => "success"));
					die;
				} else {
					array('status' => "error");
					echo json_encode(array('status' => "success"));
					die;
				}
			}
		}
	}

	public function general_volume()
	{
		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['merchID'];

		$get_result = $this->company_model->chart_all_volume($user_id);
		if ($get_result['data']) {
			$result_set = array();
			$result_value1 = array();
			$result_value2 = array();
			
			$result_online_value = array();
			$result_online_month = array();
			$result_online_value = array();
			$result_online_month = array();

			$result_eCheck_value = array();
			$result_eCheck_month = array();
			foreach ($get_result['data'] as $count_merch) {

				array_push($result_value1, $count_merch['revenu_Month']);
				array_push($result_value2, (float) $count_merch['revenu_volume']);
				array_push($result_online_month, $count_merch['revenu_Month']);
				array_push($result_online_value, (float) $count_merch['online_volume']);
				array_push($result_eCheck_month, $count_merch['revenu_Month']);
				array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
			}
			$opt_array['revenu_month'] =   $result_value1;
			$opt_array['revenu_volume'] =   $result_value2;
			
			$opt_array['online_month'] =   $result_online_month;
			$opt_array['online_volume'] =   $result_online_value;

			$opt_array['eCheck_month'] =   $result_eCheck_month;
			$opt_array['eCheck_volume'] =   $result_eCheck_value;

			$opt_array['totalRevenue'] =   $get_result['totalRevenue'];
			echo json_encode($opt_array);
		}
	}
	public function get_qbd_sale_invoices()
	{
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$invoices 		  = $this->customer_model->get_invoice_upcomming_data($customerID, $merchantID);
			$new_inv = '<div class="form-group alignTableInvoiceList" >
		      <div class="col-md-2 text-center"><b></b></div>
		        <div class="col-md-2 text-left"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2 text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
			   </div>';
			$inv_data = [];
			foreach ($invoices as $inv) {
				if (strtoupper($inv['status']) != 'CANCEL') {
					$new_inv .= '<div class="form-group alignTableInvoiceList" >
		       
		         <div class="col-md-2 text-center"><input type="checkbox" class="chk_pay check_'.$inv['RefNumber'].'" id="' . 'multiinv' . $inv['TxnID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['TxnID'] . '" /> </div>
		        <div class="col-md-2 text-left">' . $inv['RefNumber'] . '</div>
		        <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
		        <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
			   <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['TxnID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['TxnID'] . '  geter" data-id="multiinv' . $inv['TxnID'] . '" data-inv="' . $inv['TxnID'] . '" data-ref="' . $inv['RefNumber'] . '" data-value="' . $inv['BalanceRemaining'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
			   </div>';
					$inv_data[] = [
						'RefNumber' => $inv['RefNumber'],
						'TxnID' => $inv['TxnID'],
						'BalanceRemaining' => $inv['BalanceRemaining'],
					];
				}
			}


			$card = '';
			$card_name = '';
			$customerdata = array();






			$condition     =  array('ListID' => $customerID, 'qbmerchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('qb_test_customer', $condition);
			if (!empty($customerdata)) {
 
				$customerdata['status'] =  'success';
				$customerdata['invoices']   = $new_inv;
				$customerdata['inv_data']   = $inv_data;
				$invoice_count = count($invoices); 
				$customerdata['invoice_records']   = ($invoice_count > 0) ? $invoice_count : 0;

				echo json_encode($customerdata);
				die;
			}
		}
	}

	public function get_qbd_customer_invoices()
	{
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$invoices 		  = $this->customer_model->get_invoice_upcomming_data($customerID, $merchantID);
			$new_inv = '<div class="form-group" >
		      <div class="col-md-2 text-center"><b>Select</b></div>
		        <div class="col-md-2 text-left"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2 text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
		       </div>';
			foreach ($invoices as $inv) {
				if (strtoupper($inv['status']) != 'CANCEL') {
					$new_inv .= '<div class="form-group" >
		       
		         <div class="col-md-2 text-center"><input type="checkbox" class="chk_pay"  id="' . 'multiinv' . $inv['TxnID'] . '"  onclick="chk_inv_position(this);" name="multi_inv[]" value="' . $inv['TxnID'] . '" /> </div>
		        <div class="col-md-2 text-left">' . $inv['RefNumber'] . '</div>
		        <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
		        <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
		       <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['TxnID'] . '"    onblur="chk_pay_position(this);"  class="form-control   geter multiinv' . $inv['TxnID'] . '" data-id="multiinv' . $inv['TxnID'] . '"  class="form-control" value="' . $inv['BalanceRemaining'] . '" /></div>
		       </div>';
				}
			}


			$card = '';
			$card_name = '';
			$customerdata = array();



			$condition     =  array('ListID' => $customerID, 'qbmerchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('qb_test_customer', $condition);
			if (!empty($customerdata)) {

			
				$customerdata['status'] =  'success';

				$card_data =   $this->card_model->getCustomerCardDataByID($customerID);
				$customerdata['card']  = $card_data;
				$customerdata['invoices']   = $new_inv;

				echo json_encode($customerdata);
				die;
			}
		}
	}




	public function get_product_data()
	{
		if ($this->session->userdata('logged_in')) {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$data = $this->company_model->get_plan_data_invoice($merchantID);

		if (!empty($data)) {
			foreach ($data as $key => $item) {
				if (empty($item['FullName'])) {
					$item['FullName'] = $item['Name'];
					$data[$key] = $item;
				}
			}
		}
		$data = json_encode($data);
		echo   $data = str_replace("'", "", $data);

		die;
	}
	public function get_subs_item_count_data()
	{
		$sbID   = $this->czsecurity->xssCleanPostInput('subID');
		$data1['items']      = $this->general_model->get_table_data('tbl_subscription_invoice_item', array('subscriptionID' => $sbID, 'invoiceDataID' => 0));
		$data1['rows']      = $this->general_model->get_num_rows('tbl_subscription_invoice_item', array('subscriptionID' => $sbID, 'invoiceDataID' => 0));
		echo json_encode($data1);
		die;
	}
	public function get_invoice_item_count_data()
	{

		if ($this->session->userdata('logged_in')) {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$sbID   = $this->czsecurity->xssCleanPostInput('invID');
		$con = array('TxnID' => $sbID);
		$data1['items']      = $this->general_model->get_table_data('qb_test_invoice_lineitem', $con);
		$data1['rows']      = $this->general_model->get_num_rows('qb_test_invoice_lineitem', $con);
		echo json_encode($data1);
		die;
	}

	function safe_encode($string)
	{
		return strtr(base64_encode($string), '+/=', '-_-');
	}
	public function transation_receipt($txt_id = '', $invoiceId = '', $trans_id = '')
	{

		$page_data = $this->session->userdata('receipt_data');
		$page_data['transaction_id'] = $trans_id;
		$this->session->set_userdata("receipt_data", $page_data);
		foreach($page_data as $key => $rcData){
			$page_data[$key] = strip_tags($rcData);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_data'] = $page_data;
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
			$data['transaction_id'] = $trans_id;
			$transaction_id = $trans_id;
			$con = array('transactionID' => $transaction_id);
            $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);
            $data['transactionDetail'] = $pay_amount;

            $data['transactionAmount'] = ($pay_amount) ? $pay_amount['transactionAmount'] : '0.00';

            $surCharge = 0;
            $totalAmount = 0;
            $isSurcharge = 0;
           
            $transactionType = $pay_amount['transactionGateway'];
            if(isset($pay_amount['transactionGateway']) && $pay_amount['transactionID'] != '' ){
                if($pay_amount['transactionGateway'] == 10){

                    $resultAmount = getiTransactTransactionDetails($user_id,$trans_id);
                    $totalAmount = $resultAmount['totalAmount'];
                    $surCharge = $resultAmount['surCharge'];
                    $isSurcharge = $resultAmount['isSurcharge'];
                    if($resultAmount['payAmount'] != 0){
                        $data['transactionAmount'] = $resultAmount['payAmount'];
                    }   
                }
            }
            $data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;
            $data['transactionCode'] = ($pay_amount) ? $pay_amount['transactionCode'] : '0';
			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
			$in_data  =  $this->session->userdata('in_data');
			$data['invoice'] = $invoiceId;
			$data['invoice_number'] = $in_data['RefNumber'];
			$condition2 			= array('TxnID' => $invoiceId);
			$invoice_data = [];
			$invoice_data[]           = $this->general_model->get_row_data('qb_test_invoice', $condition2);
			$condition3 			= array('ListID' => $in_data['Customer_ListID']);
			$customer_data			= $this->general_model->get_row_data('qb_test_customer', $condition3);
			$customer_data = convertCustomerDataFieldName($customer_data,2);
			$data['customer_data'] = $customer_data;
			$data['invoice_data'] = $invoice_data;
		


			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);

			$this->load->view('comman-pages/transaction_proccess_receipt', $data);
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}
	public function transation_credit_receipt($invoiceId = "null", $customer_id = "null", $trans_id = "null")
	{

		$page_data = $this->session->userdata('receipt_data'); 
		$page_data['transaction_id'] = $trans_id;
        $this->session->set_userdata("receipt_data", $page_data);
		foreach($page_data as $key => $rcData){
			$page_data[$key] = strip_tags($rcData);
		}
		
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_data'] = $page_data;
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
			$data['transaction_id'] = $trans_id;
			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
			$data['invoice'] = '';
			$data['invoice_number'] = '';
			
			$condition3 			= array('ListID' => $customer_id);
			$customer_data			= $this->general_model->get_row_data('qb_test_customer', $condition3);
			$customer_data = convertCustomerDataFieldName($customer_data,2);
			$data['customer_data'] = $customer_data;
			
			
			$surCharge = 0;
			$isSurcharge = 0;
			$totalAmount = 0;
			$transactionType = 0;

			if(isset($invoiceId) && !empty($invoiceId) && $invoiceId != 'transaction'){
                $in_data   =    $this->company_model->get_invoice_data_pay($invoiceId);
                $data['invoice'] = $invoiceId;
                $data['invoice_number'] = $in_data['RefNumber'];
            }
            $invoice_IDs = [];
            $invoice_data = [];
			if($trans_id != null){
                $condition4             = array('transactionID' => $trans_id, 'merchantID' => $user_id,'customerListID' => $customer_id);
                $transactionData          = $this->general_model->get_row_data('customer_transaction', $condition4);
                $data['transactionAmount'] = $transactionData['transactionAmount'];
                $data['transactionCode'] = $transactionData['transactionCode'];
                $transactionType = $transactionData['transactionGateway'];
                $data['transactionDetail'] = $transactionData;
                /*Invoice Set*/
                $invoiceArray = json_decode($transactionData['custom_data_fields']);
                $invoiceStr = '';
               	
                if(!empty($invoiceArray) && isset($invoiceArray->invoice_number)){

                	$invoiceStr = $invoiceArray->invoice_number;
                	$invoice_IDs = explode(',', $invoiceStr);
                }
                
                
				if (!empty($invoice_IDs)) {

					foreach ($invoice_IDs as $inID) {
						$condition2 = array('TxnID' => $inID);
						$invoice_data[]  = $this->general_model->get_row_data('qb_test_invoice', $condition2);
					}
				}
				$data['invoice_IDs'] = $invoice_IDs;
				
			 	$data['invoice_data'] = $invoice_data;

            }else{
                $data['transactionAmount'] = 0;
                $data['transactionCode'] = 0;
                $data['invoice_IDs'] = $invoice_IDs;
			 	$data['invoice_data'] = $invoice_data;
                
            }

            if(isset($transactionType)){
				if($transactionType == 10){

	        		$resultAmount = getiTransactTransactionDetails($user_id,$trans_id);
	        		$totalAmount = $resultAmount['totalAmount'];
	        		$surCharge = $resultAmount['surCharge'];
	        		$isSurcharge = $resultAmount['isSurcharge'];
	        		if($resultAmount['payAmount'] != 0){
	        			$data['transactionAmount'] = $resultAmount['payAmount'];
	        		}   
	        	}
			}
        	$data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;

			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('comman-pages/transaction_proccess_receipt', $data);
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}
	
	public function transation_sale_receipt()
	{

		$invoice_IDs = $this->session->userdata('invoice_IDs');
		$receipt_data = $this->session->userdata('receipt_data'); 

		if(!empty($receipt_data)){
            foreach($receipt_data as $key => $rcData){
                $receipt_data[$key] = strip_tags($rcData);
            }
        }

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$transactionCode = 0;
		$surCharge = 0;
		$isSurcharge = 0;
		$totalAmount = 0;
		$transactionType = 0;
		$referenceMemo = '';
		$pay_amount = [];
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
		
			$data['transactionAmount'] = '';
			$data['transactionCode'] = 0;
			if(isset($receipt_data['transaction_id']) && !empty($receipt_data['transaction_id'])){
                $transaction_id = $receipt_data['transaction_id'];
                $transactionRowID = isset($receipt_data['transactionRowID'])?$receipt_data['transactionRowID']:'';
                $con = array('transactionID' => $transaction_id);
                $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount');
                $this->db->from('customer_transaction tr');
                $this->db->where($con);
                if(isset($transactionRowID) && !empty($transactionRowID)){
                	$this->db->where("tr.id",$transactionRowID);
                }
                $this->db->group_by("tr.transactionID");
                $pay_amount = $this->db->get()->row_array();
                
                if(isset($data['customer_details']) && $data['customer_details']){
                	$data['customer_details'] = $this->customer_model->customer_by_id($pay_amount['customerListID']);
                	$receipt_data['FullName'] = $data['customer_details']->FullName;
                }
                if(isset($pay_amount) && !empty($pay_amount)){
                	$data['transactionAmount'] = $pay_amount['transactionAmount'];
                
	                $payAmountSet = isset($pay_amount['transactionAmount'])?$pay_amount['transactionAmount']:0;

	                $transactionType = $pay_amount['transactionGateway'];
	                
	                if($pay_amount['transactionGateway'] == 10){

	                    $resultAmount = getiTransactTransactionDetails($user_id,$transaction_id);
	                    $totalAmount = $resultAmount['totalAmount'];
	                    $surCharge = $resultAmount['surCharge'];
	                    $isSurcharge = $resultAmount['isSurcharge'];
	                    if($resultAmount['payAmount'] != 0){
	                        $payAmountSet = $resultAmount['payAmount'];
	                    }   
	                }
					$referenceMemo = $pay_amount['referenceMemo'];
	                $data['transactionCode'] = $pay_amount['transactionCode'];
	                $data['transactionAmount'] = $payAmountSet;
	                $data['transactionDetail'] = $pay_amount;
                }
                
            }

			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
		
		
			$data['customer_data'] = $receipt_data;
			$invoice_data = [];
			if (!empty($invoice_IDs)) {

				foreach ($invoice_IDs as $inID) {
					$condition2 = array('TxnID' => $inID);
					$invoiceData  = $this->general_model->get_row_data('qb_test_invoice', $condition2);
					if(!empty($invoiceData)){
                        $invoice_data[]  = $invoiceData;
                    }
				}
			}

			$data['referenceMemo'] = $referenceMemo;
			$data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;
			$data['invoice_IDs'] = $invoice_IDs;
			$data['invoice_data'] = $invoice_data;
			$data['transactionDetail'] = $pay_amount;
			if(isset($receipt_data['refundAmount']) && !empty($receipt_data['refundAmount'])){
				$data['transactionAmount'] = $receipt_data['refundAmount'];
			}

			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('comman-pages/transaction_receipt', $data);
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}

    public function my_account()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');

            $user_id                = $data['login_info']['merchID'];
            $data['loginType'] = 1;
        }else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];

            $data['loginType'] = 2;
        }   

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();
        
        $condition  = array('merchantID' => $user_id);
        $data['invoices'] = $this->general_model->get_table_data('tbl_merchant_billing_invoice', $condition);

        $plandata = $this->general_model->chk_merch_plantype_data($user_id);
        $resellerID = $data['login_info']['resellerID'];
        $planID = $plandata->plan_id;
        $planname = $this->general_model->chk_merch_planFriendlyName($resellerID,$planID);

		if(isset($planname) && !empty($planname)){
			$data['planname'] = $planname;
		}else{
			$data['planname'] = $plandata->plan_name;
		}


        if($plandata->cardID > 0 && $plandata->payOption > 0){
            $carddata = $this->card_model->get_merch_card_data($plandata->cardID);
        }else{
            $carddata = [];
        }
        
        $data['plan'] = $plandata;

        $data['carddata'] = $carddata;

        $conditionMerch = array('merchID'=>$user_id);
		$data['merchantData'] = $this->general_model->get_row_data('tbl_merchant_data', $conditionMerch);

		$data['interface'] = 2;
		$data['merchantID'] = $user_id;
		
        
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_my_account', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function update_card_data()
	{
	    
		$success_msg = null;
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}	
		  	
	        
			$resellerID  = $this->czsecurity->xssCleanPostInput('resellerID');
			    
        	$merchantcardID = $this->czsecurity->xssCleanPostInput('cardID');	

        	$merchantID = $user_id;

        	$condition = array('merchantListID'=>$merchantID);

        	$conditionMerch = array('merchID'=>$merchantID);
        	
        	$billing_first_name = ($this->czsecurity->xssCleanPostInput('billing_first_name') != null)?$this->czsecurity->xssCleanPostInput('billing_first_name'):null;

        	$billing_last_name = ($this->czsecurity->xssCleanPostInput('billing_last_name')!= null)?$this->czsecurity->xssCleanPostInput('billing_last_name'):null;

        	$billing_phone_number = ($this->czsecurity->xssCleanPostInput('billing_phone_number')!= null)?$this->czsecurity->xssCleanPostInput('billing_phone_number'):null;

        	$billing_email = ($this->czsecurity->xssCleanPostInput('billing_email')!= null)?$this->czsecurity->xssCleanPostInput('billing_email'):null;

        	$billing_address = ($this->czsecurity->xssCleanPostInput('billing_address')!= null)?$this->czsecurity->xssCleanPostInput('billing_address'):null;

        	$billing_state = ($this->czsecurity->xssCleanPostInput('billing_state')!= null)?$this->czsecurity->xssCleanPostInput('billing_state'):null;

        	$billing_city = ($this->czsecurity->xssCleanPostInput('billing_city')!= null)?$this->czsecurity->xssCleanPostInput('billing_city'):null;

        	$billing_zipcode = ($this->czsecurity->xssCleanPostInput('billing_zipcode')!= null)?$this->czsecurity->xssCleanPostInput('billing_zipcode'):null;
        	$statusInsert = 0;
        	/* check is_address_update condition 1 than only address update and 2 for all */
        	if($this->czsecurity->xssCleanPostInput('is_address_update') == 1){
        		$insert_array =  array( 
        							"billing_first_name" => $billing_first_name,
								  	"billing_last_name" => $billing_last_name,
								  	"billing_phone_number" => $billing_phone_number,
								  	"billing_email" => $billing_email,
								   	"Billing_Addr1" => $billing_address,
								    "Billing_Country" =>null,
								    "Billing_State" => $billing_state,
								    "Billing_City" =>$billing_city,
								    "Billing_Zipcode" => $billing_zipcode
											 );
				if($merchantcardID!="")
			    {
			    	
					$id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
					
					
					$success_msg = 'Address Updated Successfully';  

			    }
        	}else{
        		/* Save credit card data */
	        	if($this->czsecurity->xssCleanPostInput('payOption') == 1){

	        		$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
	        		$card_type = $this->general_model->getcardType($card_no);
					$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$type = 'Credit';
					$friendlyname = $card_type . ' - ' . substr($card_no, -4);
					$insert_array =  array( 'CardMonth'  =>$expmonth,
								   	'CardYear'	 =>$exyear, 
								   	'resellerID'  =>$resellerID,
								   	'merchantListID'=>$merchantID,
								   	'accountNumber'   => null,
									'routeNumber'     => null,
									'accountName'   => null,
									'accountType'   => null,
									'accountHolderType'   => null,
									'secCodeEntryMethod'   => null,
								   	'merchantFriendlyName' => $friendlyname,
								  	"billing_first_name" => $billing_first_name,
								  	"billing_last_name" => $billing_last_name,
								  	"billing_phone_number" => $billing_phone_number,
								  	"billing_email" => $billing_email,
								   	"Billing_Addr1" => $billing_address,
								    "Billing_Country" =>null,
								    "Billing_State" => $billing_state,
								    "Billing_City" =>$billing_city,
								    "Billing_Zipcode" => $billing_zipcode
											 );
					if($merchantcardID!="")
				    {
				    	
						   
						$insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
						$insert_array['CardCVV']        = ''; 
						$insert_array['CardType']        = $card_type;
						$insert_array['createdAt']    = date('Y-m-d H:i:s');
						$id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
						
						
						$success_msg = 'Credit Card Updated Successfully';  

				    }else{
				        $insert_array['CardType']    = $card_type;
						$insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
						$insert_array['CardCVV']        = '';
						$insert_array['createdAt']    = date('Y-m-d H:i:s');
						      
							
						$id = $this->card_model->insert_merchant_card_data($insert_array);
						$statusInsert = 1;
						$merchantcardID = $id;
						$success_msg = 'Credit Card Inserted Successfully';  
						
				    }

	        	}else if($this->czsecurity->xssCleanPostInput('payOption') == 2){
	        		/* Save checking card data */
	        		$acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
					$route_number = $this->czsecurity->xssCleanPostInput('route_number');
					$acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
					$secCode      = $this->czsecurity->xssCleanPostInput('secCode');
					$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
					$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
					$card_type = 'Checking';
					$type = 'Checking';
					$friendlyname = $type . ' - ' . substr($acc_number, -4);
					$card_data = array(
						'CardType'     => $card_type,
						'CardMonth'  => null,
						'CardYear'	 => null, 
						'MerchantCard' => null, 
						'CardCVV' => null,
						'accountNumber'   => $acc_number,
						'routeNumber'     => $route_number,
						'accountName'   => $acc_name,
						'accountType'   => $acct_type,
						'accountHolderType'   => $acct_holder_type,
						'secCodeEntryMethod'   => $secCode,
						'merchantListID'=>$merchantID,
						'resellerID'  =>$resellerID,
						'merchantFriendlyName' => $friendlyname,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						"billing_first_name" => $billing_first_name,
					  	"billing_last_name" => $billing_last_name,
					  	"billing_phone_number" => $billing_phone_number,
					  	"billing_email" => $billing_email,
					   	"Billing_Addr1" => $billing_address,
					    "Billing_Country" =>null,
					    "Billing_State" => $billing_state,
					    "Billing_City" =>$billing_city,
					    "Billing_Zipcode" => $billing_zipcode
								 
					);
					if($merchantcardID!="")
				    {
						
						$id = $this->card_model->update_merchant_card_data($condition, $card_data);  
						
						

						$success_msg = 'Checking Card Updated Successfully';  
				    }else{
				    	
						$id = $this->card_model->insert_merchant_card_data($card_data);
						$merchantcardID = $id;
						$statusInsert = 1;
						$success_msg = 'Checking Card Inserted Successfully';  
				    }


	        	}else{
	        		/* do nothing*/
	        		$id = false;
	        	}
        	}
			if($statusInsert == 1){
				$merchant_condition = ['merchID' => $merchantID];
	            $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$merchant_condition);
				if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                {
					/* Start campaign in hatchbuck CRM*/  
	                $this->load->library('hatchBuckAPI');
	                
	                $merchantData['merchant_type'] = 'Quickbox Desktop (QBD)';        
	                
	                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
	                if($status['statusCode'] == 400){
	                    $resource = $this->hatchbuckapi->createContact($merchantData);
	                    if($resource['contactID'] != '0'){
	                        $contact_id = $resource['contactID'];
	                        $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
	                    }
	                }
	                /* End campaign in hatchbuck CRM*/ 
	            }
    		   
			}	

        	if( $id ){
        		/* Update Pay option type in merchant table */
        		$update_array =  array( 'payOption'  => $this->czsecurity->xssCleanPostInput('payOption'), 'cardID' => $merchantcardID );

        		$update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array); 

	  	 		$this->session->set_flashdata('success', $success_msg);
			}else{
			 
			   	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
			}

	    	redirect('home/my_account/');
	}
    public function marchant_invoice()
    {


        if ($this->uri->segment(4) != "") {
            $data['primary_nav']    = primary_nav();
            $data['template']       = template_variable();
            if ($this->session->userdata('logged_in')) {
                $data['login_info']     = $this->session->userdata('logged_in');

                $user_id                = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info']     = $this->session->userdata('user_logged_in');

                $user_id                = $data['login_info']['merchantID'];
            }
            $invoiceID             =  $this->uri->segment(4);
            
            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);

            $this->load->view('pages/marchant_invoice', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);
        } else {
            redirect(base_url('home/invoices'));
        }
    }

    public function level_three()
	{
		$data['login_info'] = $this->session->userdata('logged_in');
		if ($data['login_info']) {
			$user_id = $data['login_info']['merchID'];
		}else{
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['level_three_master_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master']);
		$data['level_three_visa_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa']);
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/level_three', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	// add or update visa details
    public function level_three_visa()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];	
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');
            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['customer_tax_id'] = $this->czsecurity->xssCleanPostInput('customer_tax_id');
            $insert_data['commodity_code'] = $this->czsecurity->xssCleanPostInput('commodity_code');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['addtnl_tax_freight'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_freight');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['line_item_commodity_code'] = $this->czsecurity->xssCleanPostInput('line_item_commodity_code');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['card_type'] = 'visa';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'visa']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa'], $insert_data);
                
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('home/level_three');
    }

	// add or update master details
    public function level_three_master_card()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');

            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['addtnl_tax_indicator'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_indicator');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['debit_credit_indicator'] = $this->czsecurity->xssCleanPostInput('debit_credit_indicator');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['addtnl_tax_type'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_type');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['net_gross_indicator'] = $this->czsecurity->xssCleanPostInput('net_gross_indicator');
            
            $insert_data['card_type'] = 'master';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'master']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master'], $insert_data);
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('home/level_three');
    }
    public function dashboardReport()
	{
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$filterType  = $this->czsecurity->xssCleanPostInput('revenue_filter',true);
		/*Update filter*/

		$endDate  = $startDate = date('Y-m-d');
		if($this->czsecurity->xssCleanPostInput('endDate',true) != null){
			$endDate  = $this->czsecurity->xssCleanPostInput('endDate',true);
		}
		if($this->czsecurity->xssCleanPostInput('startDate',true) != null){
			$startDate  = $this->czsecurity->xssCleanPostInput('startDate',true);
		}
		$update_array =  array( 'rhgraphOption'  => $filterType, 'rhgraphToDate' => $endDate, 'rhgraphFromDate' => $startDate );
		$conditionMerch = array('merchID'=>$user_id);

        $update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array);
		if($filterType == 0){
			$opt_array = $this->getAnnualRevenue($user_id);
			echo json_encode($opt_array);
		}else if($filterType == 1){
			$startDate = date('Y-m-d', strtotime('today - 30 days'));
			$endDate = date('Y-m-d');
			$opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,1);
			echo json_encode($opt_array);
		}else if($filterType == 2){
            $startDate = date('Y-m-01');
            $endDate = date('Y-m-d');
            $opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,2);
            echo json_encode($opt_array);
        }else if($filterType == 3){
			$opt_array = $this->general_model->getHourlyRevenue($user_id);
			echo json_encode($opt_array);
		}else if($filterType == 4){
			$opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,2);
			echo json_encode($opt_array);
		}
		
	}
	public function updateMerchant()
	{
		$res = array();

		$merchantEmail  = $this->czsecurity->xssCleanPostInput('merchantEmail');
		$firstName  = $this->czsecurity->xssCleanPostInput('firstName');
		$lastName  = $this->czsecurity->xssCleanPostInput('lastName');
		$merchantID  = $this->czsecurity->xssCleanPostInput('merchantID');
		$controllerSet  = $this->czsecurity->xssCleanPostInput('controllerSet');

		if($merchantID > 0){
			$conditionMerch = array('merchID'=>$merchantID);
			$update_array =  array( 'firstName'  => $firstName, 'lastName' => $lastName, 'merchantEmail' => $merchantEmail);

        	$update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array); 

        	$this->session->set_flashdata('success', 'User Account Updated Successfully');
			
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
		}
		
		redirect($controllerSet.'/home/my_account', 'refresh');
	}
	public function getAnnualRevenue($user_id){
		$get_result = $this->company_model->chart_all_volume($user_id);
		
		if ($get_result['data']) {
			
			$result_set = array();
			$result_value1 = array();
			$result_value2 = array();
			$result_online_value = array();
			$result_online_month = array();
			$result_eCheck_value = array();
			$result_eCheck_month = array();
			foreach ($get_result['data'] as $count_merch) {

				array_push($result_value1, $count_merch['revenu_Month']);
				array_push($result_value2, (float) $count_merch['revenu_volume']);
				array_push($result_online_month, $count_merch['revenu_Month']);
				array_push($result_online_value, (float) $count_merch['online_volume']);
				array_push($result_eCheck_month, $count_merch['revenu_Month']);
				array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
			}
			$ob[] = [];
			$obRevenu = [];
			$obOnline = [];
			$obeCheck = [];
			$in = 0;

			foreach ($result_value1 as $value) {
				$custmonths = date("M", strtotime($value));
				$ob1 = [];
				$obR = [];
				$obON = [];
				$obEC = [];
				$inc = strtotime($value);
				$ob1[0] = $inc;
				$obR[] = $inc;
				$obR[] = $custmonths;
				$ob1[1] = $result_value2[$in];

				$ob[] = $ob1;

				$obON[0] = $inc;
				$obON[1] = $result_online_value[$in];
				$obOnline[] = $obON;


				$obEC[0] = $inc;
				$obEC[1] = $result_eCheck_value[$in];
				$obeCheck[] = $obEC;

				$obRevenu[] = $obR; 
				$in++;
			}



			$opt_array['revenu_month'] =   $obRevenu;
			$opt_array['revenu_volume'] =   $ob;
			
			$opt_array['online_month'] =   $result_online_month;
			$opt_array['online_volume'] =   $obOnline;
			
			$opt_array['eCheck_month'] =   $result_eCheck_month;
			$opt_array['eCheck_volume'] =   $obeCheck;

			$opt_array['totalRevenue'] =   $get_result['totalRevenue'];
			$opt_array['totalCCA'] =   $get_result['totalCCA'];
			$opt_array['totalECLA'] =   $get_result['totalECLA'];
			return ($opt_array);
			
		}
		return [];
	}
	public function batchReciept()
    {
        

        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');
            $user_id                = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];
        }

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();

        $page_data = $this->session->userdata('batch_process_receipt_data'); 
        
        $invoices = $page_data['data'];
        $invoiceObj = [];
        if(count($invoices) > 0){
            foreach ($invoices as $value) {
                
                $invoiceObj[] = $this->general_model->getInvoiceBatchTransactionList($value['invoice_id'],$value['customerID'],$value['transactionRowID'],2);

                # code...
            }
        }
        
        $data['allTransaction'] = $invoiceObj;
        $data['statusCode'] = array('200','100','111','1','102','120');
        $data['integrationType'] = 2;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/batch_invoice_reciept', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }
}
