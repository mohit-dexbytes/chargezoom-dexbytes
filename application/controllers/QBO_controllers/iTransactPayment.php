<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\RefundReceipt;

include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

class iTransactPayment extends CI_Controller
{
    private $merchantID;
    private $resellerID;
    private $transactionByUser;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('quickbooks');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID = $merchID;
    }

    public function index()
    {
        redirect('QBO_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        $user_id = $this->merchantID;
        $custom_data_fields = [];
        $cardID = $this->czsecurity->xssCleanPostInput('CardID');
        if (!$cardID || empty($cardID)) {
            $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
        }

        $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
        }

        $gateway = $gatlistval;

        $cusproID   = array();
        $error      = array();
        $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

        $resellerID = $this->resellerID;
        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

		$checkPlan = check_free_plan_transactions();

        if ($checkPlan && $cardID != "" && $gateway != "") {
            $in_data = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
            $merchantID = $merchID;

            $apiUsername  = $gt_result['gatewayUsername'];
            $apiKey  = $gt_result['gatewayPassword'];
            $isSurcharge = $gt_result['isSurcharge'];
            $val = array(
                'merchantID' => $merchID,
            );
            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken  = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];
            $dataService  = DataService::Configure(array(
                'auth_mode'       => $this->config->item('AuthMode'),
                'ClientID'        => $this->config->item('client_id'),
                'ClientSecret'    => $this->config->item('client_secret'),
                'accessTokenKey'  => $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID'      => $realmID,
                'baseUrl'         => $this->config->item('QBOURL'),
            ));

            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

            if (!empty($in_data)) {
                $customerID = $in_data['CustomerListID'];

                $c_data    = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchID));
                $companyID = $c_data['companyID'];

                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                    if (!empty($cardID)) {

                        $cr_amount = 0;
                        $amount    = $in_data['BalanceRemaining'];

                        $amount         = $this->czsecurity->xssCleanPostInput('inv_amount');
                        $theInvoice     = current($targetInvoiceArray);
                       
                        $error = $dataService->getLastError();
                        if ($error != null) {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error in invoice payment process</strong></div>');
                            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
                        }

                        $amount = $amount - $cr_amount;

                        $crtxnID = '';
                        $inID    = array();

                        $name    = $in_data['fullName'];
                        $address = $in_data['ShipAddress_Addr1'];
                        $city    = $in_data['ShipAddress_City'];
                        $state   = $in_data['ShipAddress_State'];
                        $zipcode = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                        $achValue = false;
                        if ($sch_method == "1") {

                            if ($cardID != "new1") {
    
                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];
    
                            } else {
    
                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                    $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                                }
    
                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('contact');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            }
                            $cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                            if (strlen($expmonth) > 1 && $expmonth <= 9) {
                                $expmonth = substr($expmonth, 1);
                            }
    
                            $payload = array(
                                "amount"          => ($amount * 100),
                                "card"     => array(
                                    "name" => $name,
                                    "number" => $card_no,
                                    "exp_month" => $expmonth,
                                    "exp_year"  => $exyear,
                                ),
                                "address" => array(
                                    "line1"           => $address1,
                                    "line2" => $address2,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"            => $zipcode,
                                ),
                            );

                            $cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$payload['card']['cvv'] = $cvv;
							}
    
                        } else if ($sch_method == "2") {
                            $achValue = true;
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                    'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                    'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                    'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                    'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                    'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                    'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'merchantID'         => $user_id,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }
                            $accountNumber = $accountDetails['accountNumber'];
                            $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;

                            $cardType = 'Check';
    
                            $payload = [
                                "amount" => ($amount * 100),
                                "ach" => [
                                    "name" => $name,
                                    "account_number" => $accountDetails['accountNumber'], 
                                    "routing_number" => $accountDetails['routeNumber'], 
                                    "phone_number" => $accountDetails['Billing_Contact'], 
                                    "sec_code" => $accountDetails['secCodeEntryMethod'], 
                                    "savings_account" => ($accountDetails['accountType'] == 'savings') ? true : false, 
                                    
                                   
                                ],
                                "address" => [
                                    "line1" => $accountDetails['Billing_Addr1'],
                                    "line2" => $accountDetails['Billing_Addr2'],
                                    "city" => $accountDetails['Billing_City'],
                                    "state" => $accountDetails['Billing_State'],
                                    "postal_code" => $accountDetails['Billing_Zipcode'],
                                    "country" => $accountDetails['Billing_Country']
                                ],
                               
                            ];
                        }
                        
                        $sdk = new iTTransaction();
                        $result = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);
    
                        if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                            $result['status_code'] = 200;

                            $createPaymentObject = [
                                "TotalAmt"    => $amount,
                                "SyncToken"   => 1, 
                                "CustomerRef" => $customerID,
                                "Line"        => [
                                    "LinkedTxn" => [
                                        "TxnId"   => $invoiceID,
                                        "TxnType" => "Invoice",
                                    ],
                                    "Amount"    => $amount,
                                ],
                            ];

                            $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, $achValue);
                            if($paymentMethod){
                                $createPaymentObject['PaymentMethodRef'] = [
                                    'value' => $paymentMethod['payment_id']
                                ];
                            }
                            if(isset($result['id']) && $result['id'] != '')
                            {
                                $createPaymentObject['PaymentRefNum'] = substr($result['id'], 0, 21);
                            }

                            $newPaymentObj = Payment::create($createPaymentObject);
                            $savedPayment = $dataService->Add($newPaymentObj);

                            $error = $dataService->getLastError();
                            if ($error != null) {
                                $er = '';
                                $er .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                $er .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                $er .= "The Response message is: " . $error->getResponseBody() . "\n";
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error' . $er . ' </strong>.</div>');

                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success but " . $error->getResponseBody();
                                $qbID   = $result['id'];
                            } else {
                                $crtxnID        = $savedPayment->Id;
                                
                                $ref_number     = $in_data['refNumber'];
                                $tr_date        = date('Y-m-d H:i:s');
                                $toEmail        = $c_data['userEmail'];
                                $company        = $c_data['companyName'];
                                $customer       = $c_data['fullName'];

                                $condition_mail = array('templateType' => '15', 'merchantID' => $merchID);
                                
                                $st     = '1';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success";
                                $qbID   = $result['id'];

                              
                                $this->session->set_flashdata('success', 'Success </strong></div>');
                              
                            }

                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }
                        } else {
                            $err_msg = $result['status'] = $result['error']['message'];
                            $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
                            
                            $st     = '0';
                            $action = 'Pay Invoice';
                            $msg    = "Payment Failed";
                            $qbID   = $invoiceID;
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                        }
                        $qbID   = $invoiceID;
                        $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result['id'],'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                        if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;

                            $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                            
                            
                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }
                        $transactiondata = array();
                        if (isset($result['id'])) {
                            $transactiondata['transactionID'] = $result['id'];
                        } else {
                            $transactiondata['transactionID'] = '';
                        }

                        $transactiondata['transactionStatus']   = $result['status'];
                        $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                        $transactiondata['transactionmodified'] = date('Y-m-d H:i:s');
                        $transactiondata['transactionCode']     = $result['status_code'];
                       

                        if ($error == null) {
                            $transactiondata['qbListTxnID'] = $crtxnID;
                        }

                        $transactiondata['transactionType']    = 'sale';
                        $transactiondata['gatewayID']          = $gateway;
                        $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                        $transactiondata['customerListID']     = $in_data['CustomerListID'];
                        $transactiondata['transactionAmount']  = $amount;
                        $transactiondata['merchantID']         = $merchantID;
                        $transactiondata['resellerID'] = $resellerID;
                        $transactiondata['gateway']    = iTransactGatewayName;
                        $transactiondata['invoiceID']  = $invoiceID;
                        $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        }
                        if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }

                        $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

                        if ($st == 1 && $chh_mail == '1') {
                            if($isSurcharge){
                                $condition_mail = array('templateType' => '16', 'merchantID' => $merchID);
                               
                                $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date,$result['id']);
                            }else{
                                $condition_mail = array('templateType' => '5', 'merchantID' => $merchID);
                                $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['id']);
                            }
                            
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if ($cusproID == "2") {
            redirect('QBO_controllers/home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3") {
            redirect('QBO_controllers/Create_invoice/invoice_details_page/' . $in_data['invoiceID'], 'refresh');
        }

        $trans_id     = $result['id'];
        $invoice_IDs  = array();
        $receipt_data = array(
            'proccess_url'      => 'QBO_controllers/Create_invoice/Invoice_details',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
        }

        redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
    }

    public function create_customer_sale()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        
        $checkPlan = check_free_plan_transactions();
        if ($checkPlan && !empty($this->input->post(null, true))) {
            $custom_data_fields = [];
            $applySurcharge = false;
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $inv_array   = array();
            $inv_invoice = array();
            $gatlistval  = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($gatlistval != "" && !empty($gt_result)) {
                if ($this->session->userdata('logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }
                $resellerID  = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchantID))['resellerID'];
                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey      = $gt_result['gatewayPassword'];
                $isSurcharge = $gt_result['isSurcharge'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID  = $comp_data['companyID'];

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $expry  = $expmonth . $exyear;
                    $cvv    = $this->czsecurity->xssCleanPostInput('cvv');

                } else {

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    $cvv       = $card_data['CardCVV'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                if (strlen($expmonth) > 1 && $expmonth <= 9) {
                    $expmonth = substr($expmonth, 1);
                }

                /*Added card type in transaction table*/
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                $phone = $this->czsecurity->xssCleanPostInput('phone');

                $name     = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country  = $this->czsecurity->xssCleanPostInput('bcountry');
                $city     = $this->czsecurity->xssCleanPostInput('bcity');
                $state    = $this->czsecurity->xssCleanPostInput('bstate');
                $amount   = $this->czsecurity->xssCleanPostInput('totalamount');
                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;
                $zipcode  = ($this->czsecurity->xssCleanPostInput('bzipcode')) ? $this->czsecurity->xssCleanPostInput('bzipcode') : '74035';

                $request_data = array(
                    "amount"          => ($amount * 100),
                    "card"            => array(
                        "name"      => $name,
                        "number"    => $card_no,
                        "exp_month" => $expmonth,
                        "exp_year"  => $exyear,
                    ),
                    "address" => array(
                        "line1"       => $address1,
                        "line2"       => $address2,
                        "city"        => $city,
                        "state"       => $state,
                        "postal_code" => $zipcode,
                    ),
                );

                $cvv = trim($cvv);
                if($cvv && !empty($cvv)){
                    $request_data['card']['cvv'] = $cvv;
                }

                $meta_data = [];
                if($this->czsecurity->xssCleanPostInput('invoice_id')){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                    $meta_data['invoice_number'] = $new_invoice_number;
                }

                if($this->czsecurity->xssCleanPostInput('po_number')){
                    $meta_data['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
                if($meta_data){
                    $request_data['metadata'] = $meta_data;
                }

                $sdk       = new iTTransaction();
                $result    = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);
                $inv_array = '';

                if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                    $result['status_code'] = 200;

                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    $val  = array('merchantID' => $merchantID);
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));
                    $refNum = array();
                    $qblist = array();
                    $trID = $result['id'];

                    if (!empty($invoiceIDs)) {
                        $saleAmountRemaining = $amount;
                        $payIndex = 0;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();
                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                            $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            
                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;


                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                $actualInvoicePayAmount += $surchargeAmount;
                                $amount_data += $surchargeAmount;
                                $updatedInvoiceData = [
                                    'inID' => $inID,
                                    'targetInvoiceArray' => $targetInvoiceArray,
                                    'merchantID' => $user_id,
                                    'dataService' => $dataService,
                                    'realmID' => $realmID,
                                    'accessToken' => $accessToken,
                                    'refreshToken' => $refreshToken,
                                    'amount' => $surchargeAmount,
                                ];
                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                            }
                            $ispaid      = 0;
                            $isRun = 0;
                            if($saleAmountRemaining > 0){
                                $BalanceRemaining = 0.00;
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 1;

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 0;
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

                                
                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);

                                    $createPaymentObject = [
                                        "TotalAmt" => $actualInvoicePayAmount,
                                        "SyncToken" => 1, 
                                        "CustomerRef" => $customerID,
                                        "Line" => [
                                            "LinkedTxn" => [
                                                "TxnId" => $inID,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount" => $actualInvoicePayAmount
                                        ]
                                    ];
        
                                    $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                    if($paymentMethod){
                                        $createPaymentObject['PaymentMethodRef'] = [
                                            'value' => $paymentMethod['payment_id']
                                        ];
                                    }
                                    if(isset($trID) && $trID != '')
                                    {
                                        $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                    }
        
                                    $newPaymentObj = Payment::create($createPaymentObject);

                                    $savedPayment = $dataService->Add($newPaymentObj);

                                    $error = $dataService->getLastError();
                                    if ($error != null) {
                                        $err = '';
                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                        $st = '0';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success but " . $error->getResponseBody();
                                        $qbID = $trID;
                                        $pinv_id = '';
                                    } else {
                                        $pinv_id = '';
                                        $crtxnID = $pinv_id = $savedPayment->Id;
                                        $st = '1';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success";
                                        $qbID = $trID;
                                    }
                                    $qbID = $inID;
                                    $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                    $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                    if($syncid){
                                        $qbSyncID = 'CQ-'.$syncid;

                                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                    }
                                    $transactiondata = array();
                                    if (isset($result['id'])) {
                                        $transactiondata['transactionID'] = $trID;
                                    } else {
                                        $transactiondata['transactionID'] = 'TXNFAILED'.time();
                                    }
                                    $transactiondata['transactionStatus']   = $result['status'];
                                    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                                    $transactiondata['transactionmodified'] = date('Y-m-d H:i:s');
                                    $transactiondata['transactionCode']     = $result['status_code'];

                                    $transactiondata['transactionType']    = 'sale';
                                    $transactiondata['gatewayID']          = $gatlistval;
                                    $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                                    $transactiondata['customerListID']     = $customerID;
                                    $transactiondata['transactionAmount']  = $actualInvoicePayAmount;
                                    $transactiondata['merchantID']         = $merchantID;
                                    $transactiondata['resellerID']         = $resellerID;
                                    $transactiondata['gateway']            = iTransactGatewayName;
                                    $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                                    $transactiondata['invoiceID'] = $inID;
                                    if($custom_data_fields){
                                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                                    }

                                    $transactiondata['qbListTxnID'] = $pinv_id;
                                    $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                                    if(!empty($this->transactionByUser)){
                                        $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                        $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                    }
                                    if($custom_data_fields){
                                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
                                    $id                             = $this->general_model->insert_row('customer_transaction', $transactiondata);
                                }
                            }
                            $payIndex++;

                        }
                        
                    } else {

                        $transactiondata = array();
                        if (isset($result['id'])) {
                            $transactiondata['transactionID'] = $result['id'];
                        } else {
                            $transactiondata['transactionID'] = '';
                        }
                        $transactiondata['transactionStatus']   = $result['status'];
                        $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                        $transactiondata['transactionmodified'] = date('Y-m-d H:i:s');
                        $transactiondata['transactionCode']     = $result['status_code'];
                        $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                        $transactiondata['transactionType']    = 'sale';
                        $transactiondata['gatewayID']          = $gatlistval;
                        $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                        $transactiondata['customerListID']     = $this->czsecurity->xssCleanPostInput('customerID');
                        if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
                        $transactiondata['transactionAmount']  = $amount;
                        $transactiondata['merchantID']         = $merchantID;
                        $transactiondata['resellerID']         = $resellerID;
                        $transactiondata['gateway']            = iTransactGatewayName;
                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        }
                        if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
                        $id                                    = $this->general_model->insert_row('customer_transaction', $transactiondata);

                    }

                    /* This block is created for saving Card info in encrypted form  */

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_type      = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                        $friendlyname   = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
                        $card_condition = array(
                            'customerListID'           => $customerID,
                            'customerCardfriendlyName' => $friendlyname,
                        );

                        $crdata = $this->card_model->chk_card_firendly_name($customerID, $friendlyname);

                        if ($crdata > 0) {

                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'CardType'                     => $card_type,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '', 
                                'updatedAt'                    => date("Y-m-d H:i:s"),
                                'Billing_Addr1'                => $address1,
                                'Billing_Addr2'                => $address2,
                                'Billing_City'                 => $city,
                                'Billing_State'                => $state,
                                'Billing_Country'              => $country,
                                'Billing_Contact'              => $phone,
                                'Billing_Zipcode'              => $zipcode,
                            );

                            $this->card_model->update_card_data($card_condition, $card_data);
                        } else {
                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'CardType'                     => $card_type,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '',
                                'customerListID'               => $customerID,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'customerCardfriendlyName'     => $friendlyname,
                                'createdAt'                    => date("Y-m-d H:i:s"),
                                'Billing_Addr1'                => $address1,
                                'Billing_Addr2'                => $address2,
                                'Billing_City'                 => $city,
                                'Billing_State'                => $state,
                                'Billing_Country'              => $country,
                                'Billing_Contact'              => $phone,
                                'Billing_Zipcode'              => $zipcode,
                            );

                            $id1 = $this->card_model->insert_card_data($card_data);

                        }

                    }

                    if (!empty($refNum)) {
                        $ref_number = implode(',', $refNum);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['userEmail'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['fullName'];
                    if ($chh_mail == '1') {
                        /* Send mail to isSurcharge activate*/
                        if($isSurcharge){
                            $condition_mail = array('templateType' => '16', 'merchantID' => $merchantID);
                           
                            
                            $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date,$result['id']);
                        }else{
                            $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date,$result['id']);
                        }
                        
                    }
                    $condition_mail = array('templateType' => '15', 'merchantID' => $merchantID);
                    
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {

                    $err_msg      = $result['status']      = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                    $transactiondata = array();
                    if (isset($result['id'])) {
                        $transactiondata['transactionID'] = $result['id'];
                    } else {
                        $transactiondata['transactionID'] = '';
                    }
                    $transactiondata['transactionStatus']   = $result['status'];
                    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                    $transactiondata['transactionmodified'] = date('Y-m-d H:i:s');
                    $transactiondata['transactionCode']     = $result['status_code'];
                    $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                    $transactiondata['transactionType']    = 'sale';
                    $transactiondata['gatewayID']          = $gatlistval;
                    $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                    $transactiondata['customerListID']     = $customerID;
                    $transactiondata['transactionAmount']  = $amount;
                    $transactiondata['merchantID']         = $merchantID;
                    $transactiondata['resellerID']         = $resellerID;
                    $transactiondata['gateway']            = iTransactGatewayName;
                    if($custom_data_fields){
                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                    }
                    $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                    if(!empty($this->transactionByUser)){
                        $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                        $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                    }
                    if($custom_data_fields){
                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                    }
                    $id                                    = $this->general_model->insert_row('customer_transaction', $transactiondata);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $result['id'],
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => '',
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $custom_data_fields = [];

            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

			$user_id = $merchantID;

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
            $name = $this->czsecurity->xssCleanPostInput('firstName'). " ". $this->czsecurity->xssCleanPostInput('lastName');

            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey = $gt_result['gatewayPassword'];

                $customerID   = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $payload = [
                    "amount" => ($amount * 100),
                    "ach" => [
                        "name" => $name,
                        "account_number" => $accountDetails['accountNumber'],
                        "routing_number" => $accountDetails['routeNumber'], 
                        "phone_number" => $accountDetails['Billing_Contact'], 
                        "sec_code" => $accountDetails['secCodeEntryMethod'],
                        "savings_account" => (strtolower($accountDetails['accountType']) == 'savings') ? true : false, 
                    ],
                    "address" => [
                        "line1" => $accountDetails['Billing_Addr1'],
                        "line2" => $accountDetails['Billing_Addr2'],
                        "city" => $accountDetails['Billing_City'],
                        "state" => $accountDetails['Billing_State'],
                        "postal_code" => $accountDetails['Billing_Zipcode'],
                        "country" => $accountDetails['Billing_Country']
                    ],
                ];
                // $newPayload = json_decode($payload);
                $meta_data = [];
                if($this->czsecurity->xssCleanPostInput('invoice_id')){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
                    $meta_data['invoice_number'] = $new_invoice_number;
                }
                
                if($this->czsecurity->xssCleanPostInput('po_number')){
                    $meta_data['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
                if($meta_data){
                    $payload['metadata'] = $meta_data;
                }

                $sdk = new iTTransaction();
                $result = $sdk->postACHTransaction($apiUsername, $apiKey, $payload);
                if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                    $result['status_code'] = 200;

                    $trID = $result['id'];

                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    $val = array('merchantID' => $user_id);
                    $tokenData = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken = $tokenData['accessToken'];
                    $refreshToken = $tokenData['refreshToken'];
                    $realmID      = $tokenData['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode' => $this->config->item('AuthMode'),
                        'ClientID'  => $this->config->item('client_id'),
                        'ClientSecret' => $this->config->item('client_secret'),
                        'accessTokenKey' =>  $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID' => $realmID,
                        'baseUrl' => $this->config->item('QBOURL'),
                    ));

                    $refNumber = array();
                    $merchantID = $user_id;
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();


                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");


                            $con = array('invoiceID' => $inID, 'merchantID' => $user_id);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;
                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            $ispaid      = 0;
                            $isRun = 0;
                            $BalanceRemaining = 0.00;
                            if($amount_data == $actualInvoicePayAmount){
                                $actualInvoicePayAmount = $amount_data;
                                $isPaid      = 1;

                            }else{

                                $actualInvoicePayAmount = $actualInvoicePayAmount;
                                $isPaid      = 0;
                                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                
                            }
                            $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;
                            $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
                            $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                $theInvoice = current($targetInvoiceArray);

                                
                                $createPaymentObject = [
                                    "TotalAmt" => $actualInvoicePayAmount,
                                    "SyncToken" => 1,
                                    "CustomerRef" => $customerID,
                                    "Line" => [
                                        "LinkedTxn" => [
                                            "TxnId" => $inID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount" => $actualInvoicePayAmount
                                    ]
                                ];
    
                                $paymentMethod = $this->general_model->qbo_payment_method('Check', $this->merchantID, true);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($trID) && $trID != '')
                                {
                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                }
    
                                $newPaymentObj = Payment::create($createPaymentObject);

                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = '';
                                    $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

                                    $st = '0';
                                    $action = 'Pay Invoice';
                                    $msg = "Payment Success but " . $error->getResponseBody();

                                    $qbID = $trID;
                                    $pinv_id = '';
                                } else {
                                    $pinv_id = '';
                                    $pinv_id        =  $savedPayment->Id;
                                    $st = '1';
                                    $action = 'Pay Invoice';
                                    $msg = "Payment Success";
                                    $qbID = $trID;
                                }
                                $qbID = $inID;
                                $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                if($syncid){
                                    $qbSyncID = 'CQ-'.$syncid;

                                    
                                    
                                    $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                }
                                $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                            }
                            $payIndex++;
                        }
                    } else {

                        $crtxnID = '';
                        $inID = '';
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number     = '';
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['userEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['fullName'];

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg = $result['status'] = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();

                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                    
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                }

               
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }
            
            $receipt_data = array(
                'transaction_id'    => $result['id'],
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'QBO_controllers/Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('QBO_controllers/Payments/create_customer_esale', 'refresh');
    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnID');

            $con     = array(
                'transactionCode' => 200,
                'transactionID' => $tID
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey = $gt_result['gatewayPassword'];
            $customerID = $paydata['customerListID'];
            $amount = $paydata['transactionAmount'];

            $payload = [
                'amount' => ($paydata['transactionAmount'] * 100)
            ];
            $sdk = new iTTransaction();
            
            $result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, "$tID");

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                $result['status_code'] = '200';
                $this->customer_model->update_refund_payment($tID, 'iTransact');

                $c_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

                if (!empty($paydata['invoiceID'])) {

                    $val = array(
                        'merchantID' => $paydata['merchantID'],
                    );
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];
                    $dataService  = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));

                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                    $refund = $amount;

                    $item_data = $this->qbo_customer_model->get_merc_invoice_item_data($paydata['invoiceID'],$paydata['merchantID']);

                    if (!empty($item_data)) {
                        $lineArray = array();
                            
                        $i = 0;
                        for ($i = 0; $i < count($item_data); $i++) {
                            $LineObj = Line::create([
                                "Amount"              => $item_data[$i]['itemQty']*$item_data[$i]['itemPrice'],
                                "DetailType"          => "SalesItemLineDetail",
                                "Description" => $item_data[$i]['itemDescription'],
                                "SalesItemLineDetail" => [
                                    "ItemRef" => $item_data[$i]['itemID'],
                                    "Qty" => $item_data[$i]['itemQty'],
                                    "UnitPrice" => $item_data[$i]['itemPrice'],
                                    "TaxCodeRef" => [
                                        "value" => (isset($item_data[$i]['itemTax']) && $item_data[$i]['itemTax']) ? "TAX" : "NON",
                                    ]
                                ],
        
                            ]);
                            $lineArray[] = $LineObj;
                        }

                      
                        $acc_data = $this->general_model->get_select_data('QBO_accounts_list', array('accountID'), array('accountName' => 'Undeposited Funds', 'merchantID' => $this->merchantID));
                        if (!empty($acc_data)) {
                            $ac_value = $acc_data['accountID'];
                        } else {
                            $ac_value = '4';
                        }
                        $invoice_tax = $this->qbo_customer_model->get_merc_invoice_tax_data($paydata['invoiceID'],$paydata['merchantID']);
                        $theResourceObj = RefundReceipt::create([
                            "CustomerRef"         => $paydata['customerListID'],
                            "Line"                => $lineArray,
                            "DepositToAccountRef" => [
                                "value" => $ac_value,
                                "name"  => "Undeposited Funds",
                            ],
                            "TxnTaxDetail" => [
                                "TxnTaxCodeRef" => [
                                    "value" => (isset($invoice_tax['taxID']) && $invoice_tax['taxID']) ? $invoice_tax['taxID'] : "",
                                ],
                            ],
                        ]);
                        $resultingObj = $dataService->Add($theResourceObj);
                        $error        = $dataService->getLastError();

                        $ins_id = '';

                        if ($error != null) {

                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund ' . $error->getResponseBody() . '</strong></div>');
                            
                        } else {
                            $ins_id = $resultingObj->Id;
                        }
                        $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $total,
                            'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),

                        );
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                    }
                } else {
                    $ins_id    = '';
                    $refnd_trr = array(
                        'merchantID'      => $paydata['merchantID'], 'refundAmount' => $total,
                        'creditInvoiceID' => '', 'creditTransactionID'              => $tID,
                        'creditTxnID'     => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                        'createdAt'       => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                    );
                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                }

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {
                $err_msg = $result['status'] = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] :'TXNFAILED'.time();

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['status'] . '</strong>.</div>');
            }

            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result['id'];
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = 'refund';
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName." ECheck";
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');
        }
    }

    public function payment_evoid()
    {
        $custom_data_fields = [];
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con     = array(
                'transactionCode' => 200,
                'transactionID' => $tID
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey = $gt_result['gatewayPassword'];
            
            $amount            = $paydata['transactionAmount'];
            $customerID = $paydata['customerListID'];

            $payload = [];
            $sdk = new iTTransaction();
            $result = $sdk->voidCardTransaction($apiUsername, $apiKey, $payload, $tID);

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                $result['status_code'] = '200';

                $condition   = array('transactionID' => $tID);
                $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
            } else {
                $err_msg = $result['status'] = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
                
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['status'] . '</strong>.</div>');
            }

            $transactiondata                        = array();
            $transactiondata['transactionID']       = $tID;
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = 'void'; 
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName." ECheck";
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('QBO_controllers/Payments/evoid_transaction', 'refresh');
        }
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        
        $checkPlan = check_free_plan_transactions();
        $custom_data_fields = [];
        if ($checkPlan && !empty($this->input->post(null, true))) {
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($gatlistval != "" && !empty($gt_result)) {

                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey      = $gt_result['gatewayPassword'];

                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $expry  = $expmonth . $exyear;
                } else {

                    $cardID    = $this->czsecurity->xssCleanPostInput('card_list');
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $cvv       = $card_data['CardCVV'];

                    $exyear = $card_data['cardYear'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                if (strlen($expmonth) > 1 && $expmonth <= 9) {
                    $expmonth = substr($expmonth, 1);
                }

                $name    = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address = $this->czsecurity->xssCleanPostInput('address');
                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country = $this->czsecurity->xssCleanPostInput('bcountry');
                $city    = $this->czsecurity->xssCleanPostInput('bcity');
                $state   = $this->czsecurity->xssCleanPostInput('bstate');
                $amount  = $this->czsecurity->xssCleanPostInput('totalamount');
                $phone  = $this->czsecurity->xssCleanPostInput('phone');
                $zipcode = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '74035';

                $request_data = array(
                    "amount"  => ($amount * 100),
                    "card"    => array(
                        "name"      => $name,
                        "number"    => $card_no,
                        "exp_month" => $expmonth,
                        "exp_year"  => $exyear,
                    ),
                    "address" => array(
                        "line1"       => $baddress1,
                        "line2"       => $baddress2,
                        "city"        => $city,
                        "state"       => $state,
                        "postal_code" => $zipcode,
                    ),
                    'capture' => false,
                );

                $cvv = trim($cvv);
                if($cvv && !empty($cvv)){
                    $request_data['card']['cvv'] = $cvv;
                }
                $meta_data = [];
                if(!empty($this->czsecurity->xssCleanPostInput('invoice_number')) ){
                    $meta_data['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                if (!empty($po_number)) {
                    $meta_data['po_number'] = $po_number;
                }
                if($meta_data){
                    $request_data['metadata'] = $meta_data;
                }

                $sdk    = new iTTransaction();
                $result = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);

                if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                    $result['status_code'] = '200';

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_type      = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                        $friendlyname   = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
                        $card_condition = array(
                            'customerListID'           => $customerID,
                            'customerCardfriendlyName' => $friendlyname,
                        );

                        $crdata = $this->card_model->chk_card_firendly_name($customerID, $friendlyname);

                        if ($crdata > 0) {

                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'CardType'                     => $card_type,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '',
                                'updatedAt'                    => date("Y-m-d H:i:s"),
                                'Billing_Addr1'                => $address1,
                                'Billing_Addr2'                => $address2,
                                'Billing_City'                 => $city,
                                'Billing_State'                => $state,
                                'Billing_Country'              => $country,
                                'Billing_Contact'              => $phone,
                                'Billing_Zipcode'              => $zipcode,
                            );

                            $this->card_model->update_card_data($card_condition, $card_data);
                        } else {
                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'CardType'                     => $card_type,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '',
                                'customerListID'               => $customerID,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'customerCardfriendlyName'     => $friendlyname,
                                'createdAt'                    => date("Y-m-d H:i:s"),
                                'Billing_Addr1'                => $address1,
                                'Billing_Addr2'                => $address2,
                                'Billing_City'                 => $city,
                                'Billing_State'                => $state,
                                'Billing_Country'              => $country,
                                'Billing_Contact'              => $phone,
                                'Billing_Zipcode'              => $zipcode,
                            );

                            $id1 = $this->card_model->insert_card_data($card_data);

                        }
                        $custom_data_fields['card_type'] = $cardType;

                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg      = $result['status']      = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] :'TXNFAILED'.time();
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $transactiondata = array();
                if (isset($result['id'])) {
                    $transactiondata['transactionID'] = $result['id'];
                } else {
                    $transactiondata['transactionID'] = '';
                }
                $transactiondata['transactionStatus']   = $result['status'];
                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                $transactiondata['transactionmodified'] = date('Y-m-d H:i:s');
                $transactiondata['transactionCode']     = $result['status_code'];
                $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                $transactiondata['transaction_user_status'] = '5';
                $transactiondata['transactionType']         = 'auth';
                $transactiondata['gatewayID']               = $gatlistval;
                $transactiondata['transactionGateway']      = $gt_result['gatewayType'];
                $transactiondata['customerListID']          = $customerID;
                $transactiondata['transactionAmount']       = $amount;
                $transactiondata['merchantID']              = $merchantID;
                $transactiondata['resellerID']              = $this->resellerID;
                $transactiondata['gateway']                 = iTransactGatewayName;
                $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                if($custom_data_fields){
                    $transactiondata['custom_data_fields'] = json_encode($custom_data_fields);
                }
                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        $invoice_IDs = array();

        $receipt_data = array(
            'transaction_id'    => $result['id'],
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => '', 
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_auth',
            'proccess_btn_text' => 'Process New Transaction',
            'sub_header'        => 'Authorize',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey      = $gt_result['gatewayPassword'];
            $isSurcharge = $gt_result['isSurcharge'];

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $payload = [
                'amount' => ($paydata['transactionAmount'] * 100),
            ];
            $sdk = new iTTransaction();

            $result = $sdk->captureAuthTransaction($apiUsername, $apiKey, $payload, $tID);

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                /* This block is created for saving Card info in encrypted form  */
                $result['status_code'] = '200';
                /* This block is created for saving Card info in encrypted form  */

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {
                    if($isSurcharge){
                        $condition_mail = array('templateType' => '16', 'merchantID' => $merchantID);
                       
                        $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date,$result['id']);
                    }else{
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
                    }
                    

                }
                $condition_mail = array('templateType' => '15', 'merchantID' => $merchantID);
               
                $this->session->set_flashdata('success', 'Successfully Captured Authorization');

            } else {
                $err_msg      = $result['status']      = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $transactiondata = array();
            if (isset($result['id'])) {
                $transactiondata['transactionID'] = $result['id'];
            } else {
                $transactiondata['transactionID'] = '';
            }
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionmodified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionCard']     = $paydata['transactionCard'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['transactionType']     = 'capture';
            $transactiondata['customerListID']      = $paydata['customerListID'];
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName;
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }
            $id                                     = $this->general_model->insert_row('customer_transaction', $transactiondata);

            $invoice_IDs = array();

            $receipt_data = array(
                'proccess_url'      => 'QBO_controllers/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if ($result['id'] == '') {
                $result['id'] = 'null';
            }
            redirect('QBO_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result['id'], 'refresh');

        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_void()
    {
        //Show a form here which collects someone's name and e-mail address
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey      = $gt_result['gatewayPassword'];

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $payload = [];
            $sdk     = new iTTransaction();

            $result = $sdk->voidCardTransaction($apiUsername, $apiKey, $payload, $tID);

            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                $result['status_code'] = '200';
                $condition             = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "3");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
                }
                $this->session->set_flashdata('success', 'Success ');
            } else {
                $err_msg      = $result['status']      = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();

                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $transactiondata = array();
            if (isset($result['id'])) {
                $transactiondata['transactionID'] = $result['id'];
            } else {
                $transactiondata['transactionID'] = '';
            }
            $transactiondata['transactionStatus']   = $result['status'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result['status_code'];
            $transactiondata['transactionCard']     = $paydata['transactionCard'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['transactionType']     = 'pay_void';
            $transactiondata['customerListID']      = $paydata['customerListID'];
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = iTransactGatewayName;
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
        }
        redirect('QBO_controllers/Payments/payment_capture', 'refresh');
    }

    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID, 'transactionCode' => '200');
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);
            if (!empty($paydata)) {
                $gatlistval = $paydata['gatewayID'];

                $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
                $apiUsername = $gt_result['gatewayUsername'];
                $apiKey      = $gt_result['gatewayPassword'];

                $customerID = $paydata['customerListID'];
                $merchantID = $paydata['merchantID'];
                $total      = $this->czsecurity->xssCleanPostInput('ref_amount');
                $amount     = $total;

                $payload = [
                    'amount' => ($paydata['transactionAmount'] * 100)
                ];
                $sdk = new iTTransaction();
                
                $result = $sdk->refundTransaction($apiUsername, $apiKey, $payload, "$tID");

                if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                    $result['status_code'] = '200';

                    $c_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

                    if (!empty($paydata['invoiceID'])) {

                        $val = array(
                            'merchantID' => $paydata['merchantID'],
                        );
                        $data = $this->general_model->get_row_data('QBO_token', $val);

                        $accessToken  = $data['accessToken'];
                        $refreshToken = $data['refreshToken'];
                        $realmID      = $data['realmID'];
                        $dataService  = DataService::Configure(array(
                            'auth_mode'       => $this->config->item('AuthMode'),
                            'ClientID'        => $this->config->item('client_id'),
                            'ClientSecret'    => $this->config->item('client_secret'),
                            'accessTokenKey'  => $accessToken,
                            'refreshTokenKey' => $refreshToken,
                            'QBORealmID'      => $realmID,
                            'baseUrl'         => $this->config->item('QBOURL'),
                        ));

                        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                        $refund = $amount;

                        $item_data = $this->qbo_customer_model->get_merc_invoice_item_data($paydata['invoiceID'],$paydata['merchantID']);
                            

                    if (!empty($item_data)) {
                        $lineArray = array();
                               
                        $i = 0;
                        for ($i = 0; $i < count($item_data); $i++) {
                            $LineObj = Line::create([
                                "Amount"              => $item_data[$i]['itemQty']*$item_data[$i]['itemPrice'],
                                "DetailType"          => "SalesItemLineDetail",
                                "Description" => $item_data[$i]['itemDescription'],
                                "SalesItemLineDetail" => [
                                    "ItemRef" => $item_data[$i]['itemID'],
                                    "Qty" => $item_data[$i]['itemQty'],
                                    "UnitPrice" => $item_data[$i]['itemPrice'],
                                    "TaxCodeRef" => [
                                        "value" => (isset($item_data[$i]['itemTax']) && $item_data[$i]['itemTax']) ? "TAX" : "NON",
                                    ]
                                ],
        
                            ]);
                            $lineArray[] = $LineObj;
                        }

                           
                            $acc_data = $this->general_model->get_select_data('QBO_accounts_list', array('accountID'), array('accountName' => 'Undeposited Funds', 'merchantID' => $this->merchantID));
                            if (!empty($acc_data)) {
                                $ac_value = $acc_data['accountID'];
                            } else {
                                $ac_value = '4';
                            }
                            $invoice_tax = $this->qbo_customer_model->get_merc_invoice_tax_data($paydata['invoiceID'],$paydata['merchantID']);
                            $theResourceObj = RefundReceipt::create([
                                "CustomerRef"         => $paydata['customerListID'],
                                "Line"                => $lineArray,
                                "DepositToAccountRef" => [
                                    "value" => $ac_value,
                                    "name"  => "Undeposited Funds",
                                ],
                                "TxnTaxDetail" => [
                                    "TxnTaxCodeRef" => [
                                        "value" => (isset($invoice_tax['taxID']) && $invoice_tax['taxID']) ? $invoice_tax['taxID'] : "",
                                    ],
                                ],
                            ]);
                            $resultingObj = $dataService->Add($theResourceObj);
                            $error        = $dataService->getLastError();

                            $ins_id = '';

                            if ($error != null) {

                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund ' . $error->getResponseBody() . '</strong></div>');
                            } else {
                                $ins_id = $resultingObj->Id;
                            }
                            $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $total,
                                'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                                'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),

                            );
                            $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                        }
                    } else {
                        $ins_id    = '';
                        $refnd_trr = array(
                            'merchantID'      => $paydata['merchantID'], 'refundAmount' => $total,
                            'creditInvoiceID' => '', 'creditTransactionID'              => $tID,
                            'creditTxnID'     => $ins_id, 'refundCustomerID'            => $paydata['customerListID'],
                            'createdAt'       => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                        );
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                    }
                    $this->customer_model->update_refund_payment($tID, iTransactGatewayName);
                    $this->session->set_flashdata('success', 'Success ');
                } else {
                    $err_msg      = $result['status']      = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                }

                $transactiondata = array();
                if (isset($result['id'])) {
                    $transactiondata['transactionID'] = $result['id'];
                } else {
                    $transactiondata['transactionID'] = '';
                }
                $transactiondata['transactionStatus'] = $result['status'];
                $transactiondata['transactionDate']   = date('Y-m-d H:i:s');
                $transactiondata['transactionCode']   = $result['status_code'];
                $transactiondata['gatewayID']          = $gatlistval;
                $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                $transactiondata['transactionType']    = 'refund';
                $transactiondata['customerListID']     = $paydata['customerListID'];
                $transactiondata['transactionAmount']  = $amount;
                $transactiondata['merchantID']         = $merchantID;
                if (!empty($paydata['invoiceID'])) {
                    $transactiondata['invoiceID'] = $paydata['invoiceID'];
                }
                $transactiondata['resellerID'] = $this->resellerID;
                $transactiondata['gateway']    = iTransactGatewayName;
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                if($custom_data_fields){
                    $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                }
                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Invalid Transaction</div>');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('payrefund'))) {
                $invoice_IDs = array();

                $receipt_data = array(
                    'proccess_url'      => 'QBO_controllers/Payments/payment_refund',
                    'proccess_btn_text' => 'Process New Refund',
                    'sub_header'        => 'Refund',
                );

                $this->session->set_userdata("receipt_data", $receipt_data);
                $this->session->set_userdata("invoice_IDs", $invoice_IDs);

                if ($paydata['invoiceTxnID'] == '') {
                    $paydata['invoiceTxnID'] = 'null';
                }
                if ($paydata['customerListID'] == '') {
                    $paydata['customerListID'] = 'null';
                }
                if ($result['id'] == '') {
                    $result['id'] = 'null';
                }
                redirect('QBO_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result['id'], 'refresh');

            } else {
                redirect('QBO_controllers/Payments/payment_transaction', 'refresh');
            }
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');
        $merchantID = $merchID;

        $cusproID   = '';
        $custom_data_fields = [];
        $cusproID   = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $error      = '';
        $resellerID = $this->resellerID;

		$checkPlan = check_free_plan_transactions();
        if ($checkPlan && $cardID != "" && $gateway != "") {
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey = $gt_result['gatewayPassword'];

            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
            if (!empty($invoices)) {
                foreach ($invoices as $key => $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];
                        $c_data          = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
                        $companyID       = $c_data['companyID'];

                        if ($cardID == 'new1') {
                            $cardID_upd   = $cardID;
                            $card_no      = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth     = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear       = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv          = $this->czsecurity->xssCleanPostInput('cvv');
                            $cardType     = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
                            $cardType  = $card_data['CardType'];
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        if (!empty($cardID)) {

                            $cr_amount = 0;
                            $amount = $pay_amounts;
                            $amount = $amount - $cr_amount;

                            if (strlen($expmonth) > 1 && $expmonth <= 9) {
                                $expmonth = substr($expmonth, 1);
                            }

                            $name    = $in_data['fullName'];
                            $address = $in_data['ShipAddress_Addr1'];
                            $address2 = $in_data['ShipAddress_Addr2'];
                            $city    = $in_data['ShipAddress_City'];
                            $state   = $in_data['ShipAddress_State'];
                            $zipcode = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                            $payload = array(
                                "amount"          => ($amount * 100),
                                "card"     => array(
                                    "name" => $name,
                                    "number" => $card_no,
                                    "exp_month" => $expmonth,
                                    "exp_year"  => $exyear,
                                ),
                                "address" => array(
                                    "line1"           => $address,
                                    "line2" => $address2,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"            => $zipcode,
                                ),
                            );

                            $cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$payload['card']['cvv'] = $cvv;
							}

                            $sdk = new iTTransaction();
                            $result = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);

                            if ($result['status_code'] == '200' || $result['status_code'] == '201') {
                                
                                $result['status_code'] = '200';
                                $val = array(
                                    'merchantID' => $merchID,
                                );
                                $data = $this->general_model->get_row_data('QBO_token', $val);

                                $accessToken  = $data['accessToken'];
                                $refreshToken = $data['refreshToken'];
                                $realmID      = $data['realmID'];
                                $dataService  = DataService::Configure(array(
                                    'auth_mode'       => $this->config->item('AuthMode'),
                                    'ClientID'        => $this->config->item('client_id'),
                                    'ClientSecret'    => $this->config->item('client_secret'),
                                    'accessTokenKey'  => $accessToken,
                                    'refreshTokenKey' => $refreshToken,
                                    'QBORealmID'      => $realmID,
                                    'baseUrl'         => $this->config->item('QBOURL'),
                                ));

                                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                                $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);
                                }
                               
                                $createPaymentObject = [
                                    "TotalAmt"    => $amount,
                                    "SyncToken"   => 1,
                                    "CustomerRef" => $Customer_ListID,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $invoiceID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $amount,
                                    ],
                                ];
    
                                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($result['id']) && $result['id'] != '')
                                {
                                    $createPaymentObject['PaymentRefNum'] = substr($result['id'], 0, 21);
                                }
    
                                $newPaymentObj = Payment::create($createPaymentObject);
                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();
                                    $qbID   = $result['id'];

                                } else {

                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                    $st     = '1';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success";
                                    $qbID   = $result['id'];
                                }

                                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                                    $this->load->library('encrypt');
                                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');

                                    $cardType     = $this->general_model->getType($card_no);
                                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                                   
                                    $cardType       = $this->general_model->getType($card_no);
                                    $card_condition = array(
                                        'customerListID'           => $this->czsecurity->xssCleanPostInput('customerID'),
                                        'customerCardfriendlyName' => $friendlyname,
                                    );
                                    $cid      = $this->czsecurity->xssCleanPostInput('customerID');
                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $query = $this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

                                    $crdata = $query->row_array()['numrow'];

                                    if ($crdata > 0) {

                                        $card_data = array('cardMonth' => $expmonth,
                                            'cardYear'                     => $exyear,
                                            'CardType'                     => $cardType,
                                            'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                            'CardCVV'                      => '', 
                                            'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                            'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                            'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                            'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                            'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('contact'),
                                            'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                            'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                            'customerListID'               => $in_data['Customer_ListID'],
                                            'customerCardfriendlyName'     => $friendlyname,
                                            'companyID'                    => $companyID,
                                            'merchantID'                   => $merchID,
                                            'updatedAt'                    => date("Y-m-d H:i:s"));

                                        $this->db1->update('customer_card_data', $card_condition, $card_data);
                                    } else {

                                        $is_default = 0;
                                        $checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
                                        if($checkCustomerCard == 0){
                                            $is_default = 1;
                                        }
                                        
                                        $card_data = array('cardMonth' => $expmonth,
                                            'cardYear'                     => $exyear,
                                            'CardType'                     => $cardType,
                                            'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                            'CardCVV'                      => '', 
                                            'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                            'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                            'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                            'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                            'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('contact'),
                                            'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                            'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                            'customerListID'               => $in_data['Customer_ListID'],
                                            'customerCardfriendlyName'     => $friendlyname,
                                            'companyID'                    => $companyID,
                                            'merchantID'                   => $merchID,
                                            'is_default'                   => $is_default,
                                            'createdAt'                    => date("Y-m-d H:i:s"));


                                        $id1 = $this->db1->insert('customer_card_data', $card_data);

                                    }

                                }

                            } else {
                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Failed";
                                $qbID   = $invoiceID;
                                
                                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] :'TXNFAILED'.time();
                                $err_msg = $result['status'] = $result['error']['message'];

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }
                            $transactiondata = array();
                            if (isset($result['id'])) {
                                $transactiondata['transactionID'] = $result['id'];
                            } else {
                                $transactiondata['transactionID'] = '';
                            }
                            $transactiondata['transactionStatus']   = $result['status'];
                            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                            $transactiondata['transactionCode']     = $result['status_code'];
                           
                            if ($error == null) {
                                $transactiondata['qbListTxnID'] = $savedPayment->Id;
                            }
                            $transactiondata['transactionType']    = 'sale';
                            $transactiondata['gatewayID']          = $gateway;
                            $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                            $transactiondata['customerListID']     = $in_data['CustomerListID'];
                            $transactiondata['transactionAmount']  = $amount;
                            $transactiondata['merchantID']         = $merchantID;
                            $transactiondata['resellerID'] = $resellerID;
                            $transactiondata['gateway']    = iTransactGatewayName;
                            $transactiondata['invoiceID']  = $invoiceID;
                            if(!empty($this->transactionByUser)){
                                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                            }
                            if($custom_data_fields){
                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                            }
                            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if(!$checkPlan){
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

}
