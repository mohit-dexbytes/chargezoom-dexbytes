<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\CreditMemo;

class Credit extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_customer_model');
	  if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index()
	{
	    	
		 if($this->session->userdata('logged_in')){
			$data['login_info'] 	= $this->session->userdata('logged_in');
			
			$user_id 				= $data['login_info']['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			
			$user_id 				= $data['login_info']['merchantID'];
			}	


       
		$merchID = $user_id;
	
		$condition =  array('merchantID'=>$merchID);
     	 

	     $val = array(
		'merchantID' => $merchID,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
                         'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		 
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			$qbo_data = $this->general_model->get_select_data('tbl_qbo_config',array('lastUpdated'),array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CreditMemoQuery'));
        
  			  if(!empty($qbo_data))
   			 {
    
    
       		 $entities = $dataService->Query("SELECT * FROM CreditMemo where Metadata.LastUpdatedTime > '".$qbo_data['lastUpdated']."' ");
    
   			 }else{
       			  $entities = $dataService->Query("SELECT * FROM CreditMemo ");
             
   			 }
    
   
            $eror='';
			$error = $dataService->getLastError();
	if ($error != null) 
	{
			$eror.="The Status code is: " . $error->getHttpStatusCode() . "\n";
			$eror.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$eror.="The Response message is: " . $error->getResponseBody() . "\n";
		 $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong> '.$eror.'</div>'); 
		   redirect(base_url('QBO_controllers/home/index'));
		}
    
        if(!empty($qbo_data))
           {  $updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('00:00:00');
              $updatedata['updatedAt'] =date('Y-m-d H:i:s');
             if(!empty($entities))
            $this->general_model->update_row_data('tbl_qbo_config',array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CreditMemoQuery'),$updatedata);
           }
        else{
           $updateda= date('Y-m-d') . 'T' . date('00:00:00');
           $upteda = array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CreditMemoQuery','lastUpdated'=>$updateda, 'createdAt'=>date('Y-m-d H:i:s'), 'updatedAt'=>date('Y-m-d H:i:s'));
             if(!empty($entities))
           $this->general_model->insert_row('tbl_qbo_config', $upteda);
           } 
    
        
    	if(!empty($entities))
        {
        
    		foreach ($entities as $oneCredit) 
            {
    			
            
            
               $qb_credit['TxnID']         =  $oneCredit->Id;
            $qb_credit['CustomerListID']   = $oneCredit->CustomerRef;
            $qb_credit['RefNumber']        = ($oneCredit->AutoDocNumber)?$oneCredit->AutoDocNumber:$oneCredit->DocNumber;
            $qb_credit['CreditRemaining']  = $oneCredit->RemainingCredit;
            $qb_credit['AccountListID']    = $oneCredit->ARAccountRef;
            $qb_credit['TxnDate']          = $oneCredit->TxnDate;
             $qb_credit['DueDate']         = $oneCredit->DueDate;
            $qb_credit['TxnNumber']        = $oneCredit->DocNumber;
            $qb_credit['customerMemoNote'] = $oneCredit->CustomerMemo;
             $qb_credit['creditMemoNote']  = $oneCredit->PrivateNote;
             $qb_credit['TimeCreated']       = date('Y-m-d H:i:s',strtotime($oneCredit->MetaData->CreateTime));
             $qb_credit['TimeModified']      =  date('Y-m-d H:i:s',strtotime($oneCredit->MetaData->LastUpdatedTime));
             $qb_credit['SubTotal']          = $oneCredit->TxnDate;
             $qb_credit['TotalAmount']       = $oneCredit->TotalAmt;
             $qb_credit['merchantID']        = $merchID;
             $qb_credit['companyID']         = $realmID; 
           
    			 if($this->general_model->get_num_rows('qbo_customer_credit',array('companyID'=>$realmID,'TxnID'=>$oneCredit->Id , 'merchantID'=>$merchID)) > 0){  
               
                
                  
                 $this->general_model->update_row_data('qbo_customer_credit',array('companyID'=>$realmID,'TxnID'=>$oneCredit->Id , 'merchantID'=>$merchID), $qb_credit);
               
               }else{
           
                $this->general_model->insert_row('qbo_customer_credit', $qb_credit);
             
               }
            
           
    		 
    		}
      
        
       
        
        }
    		

     $data['primary_nav']  = primary_nav();
     $data['template']     = template_variable();
     $condition            = array('cr.merchantID'=>$user_id); 
     $data['credits']      = $this->qbo_customer_model->get_credit_user_data($user_id);
     $plantype = $this->general_model->chk_merch_plantype_status($user_id);
     $data['plantype'] = $plantype;
     
     
     $this->load->view('template/template_start', $data);
     $this->load->view('template/page_head', $data);
     $this->load->view('QBO_views/page_credit', $data);
     $this->load->view('template/page_footer',$data);
     $this->load->view('template/template_end', $data);

	}
	
	
	 public function credit()
	{
	    
	
 		
		 if($this->session->userdata('logged_in')){
			$data['login_info'] 	= $this->session->userdata('logged_in');
			
			$user_id 				= $data['login_info']['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			
			$user_id 				= $data['login_info']['merchantID'];
			}	


       
		$merchID = $user_id;
	
		$condition =  array('merchantID'=>$merchID);
     	 

	     $val = array(
		'merchantID' => $merchID,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
        'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		 
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			$qbo_data = $this->general_model->get_select_data('tbl_qbo_config',array('lastUpdated'),array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CreditMemoQuery'));
        
  			  if(!empty($qbo_data))
   			 {
    
    
       		 $entities = $dataService->Query("SELECT * FROM CreditMemo where Metadata.LastUpdatedTime > '".$qbo_data['lastUpdated']."' ");
    
   			 }else{
       			  $entities = $dataService->Query("SELECT * FROM CreditMemo ");
             
   			 }
    
   
			//Add a new Invoice
$eror='';
			$error = $dataService->getLastError();
			if ($error != null) {
			$eror.="The Status code is: " . $error->getHttpStatusCode() . "\n";
			$eror.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$eror.="The Response message is: " . $error->getResponseBody() . "\n";
		 $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong> '.$eror.'</div>'); 
		   redirect(base_url('QBO_controllers/home/index'));
		}
    
        if(!empty($qbo_data))
           {  $updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('00:00:00');
              $updatedata['updatedAt'] =date('Y-m-d H:i:s');
           
            $this->general_model->update_row_data('tbl_qbo_config',array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CreditMemoQuery'),$updatedata);
           }
        else{
           $updateda= date('Y-m-d') . 'T' . date('00:00:00');
           $upteda = array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CreditMemoQuery','lastUpdated'=>$updateda, 'createdAt'=>date('Y-m-d H:i:s'), 'updatedAt'=>date('Y-m-d H:i:s'));
           $this->general_model->insert_row('tbl_qbo_config', $upteda);
           } 
    

    
	if(!empty($entities))
    {
    
		foreach ($entities as $oneCredit) 
        {
			
                
        
           $qb_credit['TxnID'] =  $oneCredit->Id;
        $qb_credit['CustomerListID']   = $oneCredit->CustomerRef;
        $qb_credit['RefNumber']        = ($oneCredit->AutoDocNumber)?$oneCredit->AutoDocNumber:$oneCredit->DocNumber;
        $qb_credit['CreditRemaining']  = $oneCredit->RemainingCredit;
        $qb_credit['AccountListID']    = $oneCredit->ARAccountRef;
        $qb_credit['TxnDate']          = $oneCredit->TxnDate;
         $qb_credit['DueDate']          = $oneCredit->DueDate;
        $qb_credit['TxnNumber']        = $oneCredit->DocNumber;
        $qb_credit['customerMemoNote'] = $oneCredit->CustomerMemo;
         $qb_credit['creditMemoNote'] = $oneCredit->PrivateNote;
         $qb_credit['TimeCreated']       = date('Y-m-d H:i:s',strtotime($oneCredit->MetaData->CreateTime));
         $qb_credit['TimeModified']      =  date('Y-m-d H:i:s',strtotime($oneCredit->MetaData->LastUpdatedTime));
         $qb_credit['SubTotal']          = $oneCredit->TxnDate;
         $qb_credit['TotalAmount']       = $oneCredit->TotalAmt;
         $qb_credit['merchantID']        = $merchID;
         $qb_credit['companyID']          = $realmID; 
       
			 if($this->general_model->get_num_rows('qbo_customer_credit',array('companyID'=>$realmID,'TxnID'=>$oneCredit->Id , 'merchantID'=>$merchID)) > 0){  
           
            
              
             $this->general_model->update_row_data('qbo_customer_credit',array('companyID'=>$realmID,'TxnID'=>$oneCredit->Id , 'merchantID'=>$merchID), $qb_credit);
           
           }else{
       
            $this->general_model->insert_row('qbo_customer_credit', $qb_credit);
         
           }
        
       
		}
  
    
   
    
    }
		





      $data['primary_nav']  = primary_nav();
     $data['template']   = template_variable();
	 
	 $condition  = array('cr.merchantID'=>$user_id); 
	 
 $data['credits'] = $this->qbo_customer_model->get_credit_user_data($user_id);
	
	
	 	
     $this->load->view('template/template_start', $data);
     $this->load->view('template/page_head', $data);
     $this->load->view('QBO_views/page_credit', $data);
     $this->load->view('template/page_footer',$data);
     $this->load->view('template/template_end', $data);
	
	}
	
	

	   
	
  	   public function create_credit()
	{
		 if($this->session->userdata('logged_in'))
        {
        
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
        
		$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		$val = array('merchantID'=>$user_id);
		
		if(!empty($this->input->post(null, true)))
		{
			 
				$total=0;	
			    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod)
			    {
 
                       $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
					   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
					   $insert_row['itemPrice']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					 
					   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					  $insert_row['updatedAt'] = date('Y-m-d H:i:s');
					   
					   
					   $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];
					    $insert_row['itemTotal'] =$total;
					   $item_val[$key] =$insert_row;
                }	
               
        $cr_note =$this->czsecurity->xssCleanPostInput('cr_note') ;
        $cr_des =$this->czsecurity->xssCleanPostInput('cr_description'); 
        $data = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
       'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		 
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            $lineArray = array();
      
             $i = 0;
             for($i=0; $i<count($item_val);$i++){
               $LineObj = Line::create([
                   "Amount" => $item_val[$i]['itemTotal'],
                    "DetailType" => "SalesItemLineDetail",
    		        "SalesItemLineDetail" => [
    			  "ItemRef" => $item_val[$i]['itemListID'],
    			 
    			  ]
    			  ]);
               $lineArray[] = $LineObj;
            }
			//Add a new Credit
				$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
			$theResourceObj = CreditMemo::create([
			"CustomerRef" => $customer_ref, 
			'PrivateNote'=>$cr_des,
		   'CustomerMemo'=>$cr_note,
			"Line" => $lineArray,
	     	]);
	    	 $resultingObj = $dataService->Add($theResourceObj);
	    	 
			$error = $dataService->getLastError();
               
				 if($error!=null){
				 
		
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
			       
				 }else{
				  				
				 $this->session->set_flashdata('success', 'Successfully Credit');
 
					
				 }
				
					redirect('QBO_controllers/credit','refresh');
				
        }
              
		
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				  if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				$condition				= array('merchantID'=>$user_id );
			 
				$data['plans']          = $this->general_model->get_table_data('QBO_test_item',array('merchantID'=>$user_id,'IsActive'=>'true'));
				$compdata				= $this->general_model->get_table_data('QBO_custom_customer',array('merchantID'=>$user_id ,'customerStatus'=>'true'));
				$data['customers']		= $compdata	;
				
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/create_credit', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
  


	
	
        }
	
	
	
}