<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 *
 * This is a tiny pretend application which throws something into the queue so
 * that the Web Connector can process it.
 *
 * @author Keith Palmer <keith@consolibyte.com>
 *
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;

include_once APPPATH .'libraries/Manage_payments.php';
require APPPATH .'libraries/qbo/QBO_data.php';

class SettingSubscription extends CI_Controller
{

    private $merchantID;
    private $resellerID;
    private $transactionByUser;

    public function __construct()
    {
        parent::__construct();

        $this->load->config('quickbooks');
        $this->load->model('quickbooks');
        $this->load->model('card_model');
        $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
        $this->load->model('customer_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('QBO_models/qbo_company_model');
        $this->load->model('QBO_models/qbo_subscription_model');
        $this->load->model('general_model');
        $this->load->model('company_model');

        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $merchID = $logged_in_data['merchID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID = $merchID;
    }

    public function index()
    {

      
        redirect('QBO_controllers/home', 'refresh');
    }

    public function status()
    {
        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $invoiceID = $this->czsecurity->xssCleanPostInput('subscID');
        $in_data   = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);

        if (!empty($in_data)) {

            $amount          = $in_data['BalanceRemaining'];
            $Customer_ListID = $in_data['CustomerListID'];
            $amount          = $this->czsecurity->xssCleanPostInput('inv_amount');
            $merchID         = $user_id;

            $val = array(
                'merchantID' => $merchID,
            );

            $transaction                        = array();
            $check                              = $this->czsecurity->xssCleanPostInput('check_number');
            $transaction['transactionID']       = $check;
            $transaction['transactionStatus']   = 'Offline Payment';
            $transaction['transactionDate']     = date('Y-m-d H:i:s');
            $transaction['transactionModified'] = date('Y-m-d H:i:s');
            $transaction['transactionCode']     = '100';
            $transaction['invoiceID']           = $invoiceID;
            $transaction['transactionType']     = "Offline Payment";
            $transaction['customerListID']      = $in_data['CustomerListID'];
            $transaction['transactionAmount']   = $amount;
            $transaction['merchantID']          = $merchID;
            $transaction['resellerID']          = $this->resellerID;


            $ispaid = 'true';
            $status = $this->czsecurity->xssCleanPostInput('status');

            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken  = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];
            $dataService = DataService::Configure(array(
                'auth_mode' => $this->config->item('AuthMode'),
                'ClientID'  => $this->config->item('client_id'),
                'ClientSecret' => $this->config->item('client_secret'),
                'accessTokenKey' =>  $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID' => $realmID,
                'baseUrl' => $this->config->item('QBOURL'),
            ));

            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                $theInvoice = current($targetInvoiceArray);
            }
            $updatedInvoice = Invoice::update($theInvoice, [
                "sparse " => 'true',
            ]);

            $updatedResult = $dataService->Update($updatedInvoice);

            $createPaymentObject = [
                "TotalAmt"    => $amount,
                "SyncToken"   => $updatedResult->SyncToken,
                "CustomerRef" => $updatedResult->CustomerRef,
                "Line"        => [
                    "LinkedTxn" => [
                        "TxnId"   => $updatedResult->Id,
                        "TxnType" => "Invoice",
                    ],
                    "Amount"    => $amount,
                ],
            ];

            $paymentMethod = $this->general_model->qbo_payment_method('Cash', $this->merchantID, true);
            if($paymentMethod){
                $createPaymentObject['PaymentMethodRef'] = [
                    'value' => $paymentMethod['payment_id']
                ];
            }
            if(isset($check) && $check != '')
            {
                $createPaymentObject['PaymentRefNum'] = substr($check, 0, 21);
            }
            $newPaymentObj = Payment::create($createPaymentObject);
            $savedPayment = $dataService->Add($newPaymentObj);


            $error = $dataService->getLastError();
            if ($error != null) {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong></div>');
            } else {
                $crtxnID                    = $savedPayment->Id;
                $transaction['qbListTxnID'] = $crtxnID;
                $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
                
                if(!empty($this->transactionByUser)){
                    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                $id                         = $this->general_model->insert_row('customer_transaction', $transaction);
                $this->session->set_flashdata('success', 'Success');
            }
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid invoice</strong></div>');
        }

        redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
    }

    public function subscriptions_old()
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['merchantID'] = $user_id;
        $condition          = array('merchantID' => $user_id);

        $data['gateways']      = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['subscriptions'] = $this->qbo_customer_model->get_subscriptions_plan_data($user_id);
        $data['subs_data'] = $this->qbo_customer_model->get_total_subscription($user_id);


        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/page_subscriptions', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function subscriptions()
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

       

        $data['merchantID']    = $user_id;
        $condition             = array('merchantID' => $user_id);
        $data['gateways']      = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['subscriptions'] = $this->qbo_customer_model->get_subscriptions_plan_data($user_id);
        $data['subs_data'] = $this->qbo_customer_model->get_total_subscription($user_id);
        $this->load->view('template/template_start', $data);
     
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/page_subscriptions', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_subscription()
    {
        
        //Show a form here which collects someone's name and e-mail address
        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {
                $da     = $this->session->userdata('logged_in');

                $user_id                = $da['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da     = $this->session->userdata('user_logged_in');

                $user_id                = $da['merchantID'];
            }

            $qboOBJ = new QBO_data($user_id); 
            $qboOBJ->get_items_data(); 
            $qboOBJ->get_customer_data();
            $qboOBJ->get_tax_data();
            
            $subscriptionID = false;

			$in_prefix = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
			if (empty($in_prefix)) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

				redirect('company/SettingSubscription/create_subscription', 'refresh');
			}

            $total = 0;
			$current_date = date('Y-m-d');
            
            $taxList = $this->czsecurity->xssCleanPostInput('is_tax_check');
            foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
                $insert_row['itemListID']   = $this->czsecurity->xssCleanPostInput('productID')[$key];
                $insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
                $insert_row['itemRate']     = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                if(isset($taxList[$key])){
                    $insert_row['itemTax'] = $taxList[$key];
                }else{
                    $insert_row['itemTax'] = '';
                }
                
                if(empty($insert_row['itemTax'])){
					$insert_row['itemTax'] = 0;
				} else {
					$insert_row['itemTax'] = 1;
				}

                $insert_row['itemFullName']    = $this->czsecurity->xssCleanPostInput('description')[$key];
                $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
                if (($this->czsecurity->xssCleanPostInput('onetime_charge')[$key]) && $this->czsecurity->xssCleanPostInput('onetime_charge')[$key] != 'Recurring') {
                    $insert_row['oneTimeCharge'] = '1';
                } else {
                    $insert_row['oneTimeCharge'] = '0';
                }

                $total          = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
                $item_val[$key] = $insert_row;
            }

            $sname      = $this->czsecurity->xssCleanPostInput('sub_name');
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $plan       = $this->czsecurity->xssCleanPostInput('duration_list');
            if ($plan > 0) {
                $subsamount = $total / $plan;
            } else {
                $subsamount = $total;
            }

            $st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
            $invoice_date = $first_date = $st_date;
            
            $freetrial = $this->czsecurity->xssCleanPostInput('freetrial');

            $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
            $address1 = $this->czsecurity->xssCleanPostInput('address1');
            $address2 = $this->czsecurity->xssCleanPostInput('address2');
            $country  = $this->czsecurity->xssCleanPostInput('country');
            $state    = $this->czsecurity->xssCleanPostInput('state');
            $city     = $this->czsecurity->xssCleanPostInput('city');
            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
            $phone    = $this->czsecurity->xssCleanPostInput('phone');
            $paycycle = $this->czsecurity->xssCleanPostInput('paycycle');
            $planid   = $this->czsecurity->xssCleanPostInput('sub_plan');

            $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
            $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
            $bcity     = $this->czsecurity->xssCleanPostInput('bcity');
            $bstate    = $this->czsecurity->xssCleanPostInput('bstate');
            $bcountry  = $this->czsecurity->xssCleanPostInput('bcountry');
            $bphone    = $this->czsecurity->xssCleanPostInput('bphone');
            $bzipcode  = $this->czsecurity->xssCleanPostInput('bzipcode');

            if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
                $paygateway = $this->czsecurity->xssCleanPostInput('gateway_list');
            } else {
                $paygateway = 0;
            }

            if ($this->czsecurity->xssCleanPostInput('taxes') != '') {
                $taxval = $this->czsecurity->xssCleanPostInput('taxes');
            } else {
                $taxval = 0;
            }
            
            $end_date = date('Y-m-d', strtotime($st_date . "+$plan months"));

            $totalInv = ($plan > 0) ? $plan + $freetrial - 1 : 1;
            $nextGeneratingDate = $st_date;
            $fromdateCus =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($st_date)) . " -1 day"));
            $conditionGt                = array('planID' => $planid);

            $subplansData       = $this->general_model->get_row_data('tbl_subscriptions_plan_qbo', $conditionGt);
            if($subplansData['proRate']){
                $proRate = $subplansData['proRate']; 
            }else{
                $proRate = 0;
            }
            if($subplansData['proRateBillingDay']){
                $proRateday = $subplansData['proRateBillingDay'];  
            }
            else{
                $proRateday = 1;             
            }
            
			if ($paycycle == 'dly') {
                $totalInv   = ($totalInv) ? $totalInv : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv day"));

                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 day"));

            } else if ($paycycle == '1wk') {
                $totalInv   = ($totalInv) ? $totalInv : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 week"));
            } else if ($paycycle == '2wk') {
                $totalInv   = ($totalInv) ? $totalInv + 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 week"));
            } else if ($paycycle == 'mon') {
                
                $totalInv   = ($totalInv) ? $totalInv + 1 : '1';
                if($proRate==1)
                {
                    $end_date =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));  

                    $nextGeneratingDate =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
                } else
                {

                    $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));  
                    $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
                }


            } else if ($paycycle == '2mn') {
                $totalInv   = ($totalInv) ? $totalInv + 2 * 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 month"));


            } else if ($paycycle == 'qtr') {
                $totalInv   = ($totalInv) ? $totalInv + 3 * 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +3 month"));

            } else if ($paycycle == 'six') {
                $totalInv   = ($totalInv) ? $totalInv + 6 * 1 : '1';
                $end_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +6 month"));
            } else if ($paycycle == 'yrl') {
                $totalInv   = ($totalInv) ? $totalInv + 12 * 1 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +12 month"));
            } else if ($paycycle == '2yr') {
                $totalInv   = ($totalInv) ? $totalInv + 2 * 12 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +24 month"));
            } else if ($paycycle == '3yr') {
                $totalInv   = ($totalInv) ? $totalInv + 3 * 12 : '1';
                $end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
                $nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +36 month"));
            }
            
            if ($this->czsecurity->xssCleanPostInput('autopay')) {

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card         = array();
                    $card_type    = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                    $friendlyName = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);

                    $card['card_number']     = $this->czsecurity->xssCleanPostInput('card_number');
                    $card['expiry']          = $this->czsecurity->xssCleanPostInput('expiry');
                    $card['expiry_year']     = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $card['cvv']             = '';
                    $card['friendlyname']    = $friendlyName;
                    $card['customerID']      = $customerID;
                    $card['Billing_Addr1']   = $this->czsecurity->xssCleanPostInput('baddress1');
                    $card['Billing_Addr2']   = $this->czsecurity->xssCleanPostInput('baddress2');
                    $card['Billing_City']    = $this->czsecurity->xssCleanPostInput('bcity');
                    $card['Billing_State']   = $this->czsecurity->xssCleanPostInput('bstate');
                    $card['Billing_Contact'] = $this->czsecurity->xssCleanPostInput('bphone');
                    $card['Billing_Country'] = $this->czsecurity->xssCleanPostInput('bcountry');
                    $card['Billing_Zipcode'] = $this->czsecurity->xssCleanPostInput('bzipcode');
                    
                    $card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
                    if (!empty($card_data)) {
                        $this->card_model->update_card($card, $customerID, $friendlyName);
                        $cardID = $card_data['CardID'];
                    } else {

                        $cardID = $this->card_model->insert_new_card($card);
                    }
                }else if ($this->czsecurity->xssCleanPostInput('acc_number') != "") {
                    $card = array();
                    $acc_number         = $this->czsecurity->xssCleanPostInput('acc_number');
                    $route_number       = $this->czsecurity->xssCleanPostInput('route_number');
                    $acc_name           = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCode            = 'WEB';
                    $acct_type          = $this->czsecurity->xssCleanPostInput('acct_type');
                    $acct_holder_type   = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                    
                    $friendlyName = 'Checking - ' . substr($acc_number, -4);

                    $insert_array =  array(
                        'accountNumber'  => $acc_number,
                        'routeNumber'    => $route_number,
                        'accountName' => $acc_name,
                        'secCodeEntryMethod'      =>$secCode,
                        'accountType'      => $acct_type,
                        'accountHolderType'      => $acct_holder_type,
                        'CardType' => 'Echeck',
                        'Billing_Addr1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'   => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_State'  => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('bphone'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID' => $customerID,
                        'merchantID'     => $user_id,
                        'customerCardfriendlyName' => $friendlyName,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        
                    );
                    $con  = array(
                        'customerListID' => $customerID,
                        'merchantID'     => $user_id,
                        'customerCardfriendlyName' => $friendlyName
                    );

                    $card_data = $this->card_model->chk_card_firendly_name($customerID, $friendlyName);


                    if ($card_data > 0) {
                        $cardID = $this->card_model->update_card_data($con, $insert_array);
                        $cardID  = $card_data['CardID'];
                    } else {

                        $cardID = $this->card_model->insert_card_data($insert_array);
                    }
                }
            }
            $subdata = array(

                'baddress1'          => $this->czsecurity->xssCleanPostInput('baddress1'),
                'baddress2'          => $this->czsecurity->xssCleanPostInput('baddress2'),
                'bcity'              => $this->czsecurity->xssCleanPostInput('bcity'),
                'bstate'             => $this->czsecurity->xssCleanPostInput('bstate'),
                'bcountry'           => $this->czsecurity->xssCleanPostInput('bcountry'),
                'bphone'             => $this->czsecurity->xssCleanPostInput('bphone'),
                'bzipcode'           => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'subscriptionName'   => $sname,
                'customerID'         => $customerID,
                'subscriptionPlan'   => $plan,
                'subscriptionAmount' => $total,
                'nextGeneratingDate'    =>$nextGeneratingDate,
                'firstDate'          => $first_date,
                'startDate'          => $st_date,
                'endDate'            => $end_date,
                'paymentGateway'     => $paygateway,
                'address1'           => $address1,
                'address2'           => $address2,
                'country'            => $country,
                'state'              => $state,
                'city'               => $city,
                'zipcode'            => $zipcode,
                'contactNumber'      => $phone,
                'totalInvoice'       => $plan,
                'invoicefrequency'   => $paycycle,
                'freeTrial'          => $freetrial,
                'taxID'              => $taxval,
                'planID'             => $planid,

            );

            if ($this->czsecurity->xssCleanPostInput('autopay')) {
                $subdata['cardID']           = $cardID;
                $subdata['automaticPayment'] = '1';
            } else {
                $subdata['cardID']           = 0;
                $subdata['automaticPayment'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('email_recurring')) {

                $subdata['emailRecurring'] = '1';
            } else {
                $subdata['emailRecurring'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('billinfo')) {

                $subdata['usingExistingAddress'] = '1';
            } else {
                $subdata['usingExistingAddress'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('subID') != "") {
                $rowID = $subscriptionID = $this->czsecurity->xssCleanPostInput('subID');
                $subdata['updatedAt'] = date('Y-m-d H:i:s');

                $subs = $this->general_model->get_row_data('tbl_subscriptions_qbo', array('subscriptionID' => $rowID));
                $date = $first_date;

                $in_num = $subs['generatedInvoice'];
                if(strtotime($current_date) <= strtotime($st_date) && $subs['generatedInvoice'] == 0){
					$subdata['nextGeneratingDate'] = $st_date;
				}

               
                $subdata['generatedInvoice'] = $in_num;
                $subdata['merchantDataID'] = $user_id;

                $ins_data = $this->general_model->update_row_data('tbl_subscriptions_qbo', array('subscriptionID' => $rowID), $subdata);

                $this->general_model->delete_row_data('tbl_subscription_invoice_item_qbo', array('subscriptionID' => $rowID));

                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $rowID;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $item);
                }
            } else {
                $subdata['merchantDataID']     = $user_id;
                $subdata['createdAt']          = date('Y-m-d H:i:s');
                $subdata['generatedInvoice'] = 0;
                if(strtotime($current_date) <= strtotime($st_date)){
					$subdata['nextGeneratingDate'] = $st_date;
				}
                
                $ins_data  = $subscriptionID = $this->general_model->insert_row('tbl_subscriptions_qbo', $subdata);

                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $ins_data;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $item);
                }
            }
            if ($ins_data && $ins) {
				if($subdata['generatedInvoice'] == 0){
					$subdata['subscriptionID'] = $subscriptionID;
					$subdata['subplansData'] = $subplansData;
					$subdata['subscriptionItems'] = $item_val;
					$this->_createSubscriptionFirstInvoice($subdata);
				}
                $this->session->set_flashdata('success', 'Successfully Created Subscription');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
            }

            redirect('QBO_controllers/SettingSubscription/subscriptions', 'refresh');
        }

        if ($this->uri->segment('4') != "") {
            $sbID         = $this->uri->segment('4');
            $data['subs'] = $this->general_model->get_row_data('tbl_subscriptions_qbo', array('subscriptionID' => $sbID));
            
            $data['c_cards'] = $this->card_model->get_card_expiry_data($data['subs']['customerID']);
            $data['items']   = $this->general_model->get_table_data('tbl_subscription_invoice_item_qbo', array('subscriptionID' => $sbID));
        }
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $condition     = array('merchantID' => $user_id);
        $conditionPlan = array('merchantID' => $user_id, 'IsActive' => 'true');
        $condition11   = array('merchantDataID' => $user_id);

        $merchant_condition = [
			'merchID' => $user_id,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

        $data['subplans']    = $this->general_model->get_table_data('tbl_subscriptions_plan_qbo', $condition11);
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['frequencies'] = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
        $data['durations']   = $this->general_model->get_table_data('tbl_subscription_plan', '');
        $data['plans']       = $this->general_model->get_table_data('QBO_test_item', $conditionPlan);
        $compdata          = $this->qbo_customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;

        $conditiontax = array('tax.merchantID' => $user_id);

        $taxes         = $this->qbo_company_model->get_qbo_tax_data($user_id);
        $data['taxes'] = $taxes;

        $data['selectedTaxRate'] = 0;
        if(isset($data['subs']) && isset($data['subs']['taxID']) && !empty($data['subs']['taxID'])){
            $subtaxID = $data['subs']['taxID'];
            $keyT = array_search($subtaxID, array_column($taxes, 'Id'));
            $data['selectedTaxRate'] = $taxes[$keyT]['total_tax_rate'];
        }
     
        
        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/create_subscription', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_tax_id()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id = $data['login_info']['merchantID'];
        }

        $id  = $this->czsecurity->xssCleanPostInput('tax_id');
        $val = array(
            'taxID' => $id,
            'merchantId' => $user_id,
        );

        $data = $this->general_model->get_row_data('tbl_taxe_code_qbo', $val);
        if($data){
            $data['taxRate'] = $data['total_tax_rate'];
            $data['friendlyName'] = $data['Name'];
        }
        echo json_encode($data);
    }

    public function create_invoice()
    {

        if (!empty($this->input->post(null, true))) {

            $total = 0;
            foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
                $insert_row['itemListID']      = $this->czsecurity->xssCleanPostInput('productID')[$key];
                $insert_row['itemQuantity']    = $this->czsecurity->xssCleanPostInput('quantity')[$key];
                $insert_row['itemRate']        = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                $insert_row['itemFullName']    = $this->czsecurity->xssCleanPostInput('description')[$key];
                $insert_row['itemTax']         = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
                
                $itemDes = $this->czsecurity->xssCleanPostInput('description')[$key];

                $itemDes = str_replace('<','< ',$itemDes);
                $itemDes = str_replace('>','> ',$itemDes);
                $insert_row['itemDescription'] = $itemDes;

                $total                         = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
                $item_val[$key]                = $insert_row;
            }

            $sname      = $this->czsecurity->xssCleanPostInput('sub_name');
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $plan       = $this->czsecurity->xssCleanPostInput('duration_list');
            if ($plan > 0) {
                $subsamount = $total / $plan;
            } else {
                $subsamount = $total;
            }
            $first_date   = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
            $invoice_date = date('d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
            $st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
            $freetrial = $this->czsecurity->xssCleanPostInput('freetrial');
            if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
                $paygateway = $this->czsecurity->xssCleanPostInput('gateway_list');
                $cardID     = $this->czsecurity->xssCleanPostInput('card_list');
            } else {
                $paygateway = 0;
                $cardID     = 0;
            }

            $address1 = $this->czsecurity->xssCleanPostInput('address1');
            $address2 = $this->czsecurity->xssCleanPostInput('address2');
            $country  = $this->czsecurity->xssCleanPostInput('country');
            $state    = $this->czsecurity->xssCleanPostInput('state');
            $city     = $this->czsecurity->xssCleanPostInput('city');
            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
            $paycycle = $this->czsecurity->xssCleanPostInput('paycycle');
            $tax      = $this->czsecurity->xssCleanPostInput('texes');
            $end_date = date('Y-m-d', strtotime($st_date . "+$plan months"));
            if ($this->czsecurity->xssCleanPostInput('autopay')) {

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card                 = array();
                    $friendlyName         = $this->czsecurity->xssCleanPostInput('friendlyname');
                    $card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number');
                    $card['expiry']       = $this->czsecurity->xssCleanPostInput('expiry');
                    $card['expiry_year']  = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $card['cvv']          = $this->czsecurity->xssCleanPostInput('cvv');
                    $card['friendlyname'] = $friendlyName;
                    $card['customerID']   = $customerID;

                    $card_data = $this->card->model->check_friendly_name($customerID, $friendlyName);
                    if (!empty($card_data)) {
                        $this->card_model->update_card($card, $customerID, $friendlyName);
                        $cardID = $card_data['CardID'];
                    } else {

                        $cardID = $this->card_model->insert_new_card($card);
                    }
                }
            }

            $subdata = array(
                'subscriptionName'   => $sname,
                'customerID'         => $customerID,
                'subscriptionPlan'   => $plan,
                'subscriptionAmount' => $total,
                'generatingDate'     => $invoice_date,
                'firstDate'          => $first_date,
                'startDate'          => $st_date,
                'endDate'            => $end_date,
                'paymentGateway'     => $paygateway,
                'address1'           => $address1,
                'address2'           => $address2,
                'country'            => $country,
                'state'              => $state,
                'city'               => $city,
                'zipcode'            => $zipcode,
                'totalInvoice'       => $plan,
                'invoicefrequency'   => $paycycle,
                'freeTrial'          => $freetrial,
                'taxID'              => $tax,

            );

            if ($this->czsecurity->xssCleanPostInput('autopay')) {
                $subdata['cardID']           = $cardID;
                $subdata['automaticPayment'] = '1';
            } else {
                $subdata['automaticPayment'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('billinfo')) {

                $subdata['usingExistingAddress'] = '1';
            } else {
                $subdata['usingExistingAddress'] = '0';
            }

            if ($this->czsecurity->xssCleanPostInput('subID') != "") {
                $rowID                = $this->czsecurity->xssCleanPostInput('subID');
                $subdata['updatedAt'] = date('Y-m-d H:i:s');


                $subs = $this->general_model->get_row_data('tbl_subscriptions', array('subscriptionID' => $rowID));
                $date = $first_date;

                $in_num = $subs['generatedInvoice'];


                if ($paycycle == 'dly') {
                    $in_num    = ($in_num) ? $in_num : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
                }
                if ($paycycle == '1wk') {
                    $in_num    = ($in_num) ? $in_num : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                }

                if ($paycycle == '2wk') {
                    $in_num    = ($in_num) ? $in_num + 1 : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                }
                if ($paycycle == 'mon') {
                    $in_num    = ($in_num) ? $in_num : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                }
                if ($paycycle == '2mn') {
                    $in_num    = ($in_num) ? $in_num + 2 * 1 : '0';
                    $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
                }
                if ($paycycle == 'qtr') {
                    $in_num    = ($in_num) ? $in_num + 3 * 1 : '0';
                    $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
                }
                if ($paycycle == 'six') {
                    $in_num    = ($in_num) ? $in_num + 6 * 1 : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                }
                if ($paycycle == 'yrl') {
                    $in_num    = ($in_num) ? $in_num + 12 * 1 : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                }

                if ($paycycle == '2yr') {
                    $in_num    = ($in_num) ? $in_num + 2 * 12 : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                }
                if ($paycycle == '3yr') {
                    $in_num    = ($in_num) ? $in_num + 3 * 12 : '0';
                    $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                }

                $subdata['nextGeneratingDate'] = $next_date;

                $ins_data = $this->general_model->update_row_data('tbl_subscriptions', array('subscriptionID' => $rowID), $subdata);

                $this->general_model->delete_row_data('tbl_subscription_invoice_item_qbo', array('subscriptionID' => $rowID));

                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $rowID;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $item);
                }
            } else {
                $subdata['merchantDataID']     = $this->session->userdata('logged_in')['merchID'];
                $subdata['createdAt']          = date('Y-m-d H:i:s');
                $subdata['nextGeneratingDate'] = $first_date;
                $ins_data                      = $this->general_model->insert_row('tbl_subscriptions', $subdata);

                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $ins_data;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item_qbo', $item);
                }
            }

            if ($ins_data && $ins) {

                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
            }

            redirect('home/invoices', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $condition           = array('merchantID' => $user_id);
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
        $data['frequencies'] = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
        $data['durations']   = $this->general_model->get_table_data('tbl_subscription_plan', '');
        $data['plans']       = $this->company_model->get_plan_data($user_id);
        $compdata            = $this->customer_model->get_customers_data($user_id);
        $data['customers']   = $compdata;

        $taxes         = $this->general_model->get_table_data('tbl_taxes', '');
        $data['taxes'] = $taxes;
        $data['selectedID'] = $this->general_model->getTermsSelectedID($user_id);
        
        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('pages/create_new_invoice', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function edit_invoice()
    {

        if (!empty($this->input->post(null, true))) {

            $total = 0;
            foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {

                $insert_row['itemListID']      = $this->czsecurity->xssCleanPostInput('productID')[$key];
                $insert_row['itemQuantity']    = $this->czsecurity->xssCleanPostInput('quantity')[$key];
                $insert_row['itemRate']        = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                $insert_row['itemFullName']    = $this->czsecurity->xssCleanPostInput('description')[$key];
                $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
                $total                         = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
                $item_val[$key]                = $insert_row;
            }

            $editSeq    = $this->czsecurity->xssCleanPostInput('edit_sequence');
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            $customer  = $this->customer_model->customer_by_id($customerID);
            $txnID     = $this->czsecurity->xssCleanPostInput('txnID');
            $refNumber = $this->czsecurity->xssCleanPostInput('refNumber');
            $fullname  = $customer->FullName;
            $user      = $customer->qbwc_username;
            $duedate   = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('duedate')));

            $address1 = $this->czsecurity->xssCleanPostInput('address1');
            $address2 = $this->czsecurity->xssCleanPostInput('address2');
            $country  = $this->czsecurity->xssCleanPostInput('country');
            $state    = $this->czsecurity->xssCleanPostInput('state');
            $city     = $this->czsecurity->xssCleanPostInput('city');
            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
            if($total > 0){
                $isPaid = 'false';
            }else{
                $isPaid = 'true';
            }
            $inv_data = array(
                'Customer_FullName'      => $fullname,
                'Customer_ListID'        => $customerID,
                'TxnID'                  => $txnID,
                'RefNumber'              => $refNumber,
                'TimeCreated'            => date('Y-m-d H:i:S'),
                'TimeModified'           => date('Y-m-d H:i:S'),
                'DueDate'                => $duedate,
                'ShipAddress_Addr1'      => $address1,
                'ShipAddress_Addr2'      => $address2,
                'ShipAddress_City'       => $city,
                'ShipAddress_Country'    => $country,
                'ShipAddress_State'      => $state,
                'ShipAddress_PostalCode' => $zipcode,
                'IsPaid'                 => $IsPaid,
                'invoiceRefNumber'       => $refNumber,
                'EditSequence'           => $editSeq,
            );

            if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                $card                 = array();
                $friendlyName         = $this->czsecurity->xssCleanPostInput('friendlyname');
                $card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number');
                $card['expiry']       = $this->czsecurity->xssCleanPostInput('expiry');
                $card['expiry_year']  = $this->czsecurity->xssCleanPostInput('expiry_year');
                $card['cvv']          = $this->czsecurity->xssCleanPostInput('cvv');
                $card['friendlyname'] = $friendlyName;
                $card['customerID']   = $customerID;

                $card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
                if (!empty($card_data)) {
                    $this->card_model->update_card($card, $customerID, $friendlyName);
                    $cardID = $card_data['CardID'];
                } else {

                    $cardID = $this->card_model->insert_new_card($card);
                }
            }

            $invoice_data = $this->general_model->get_row_data('tbl_custom_invoice', array('Customer_ListID' => $customerID, 'invoiceRefNumber' => $refNumber));

            if (!empty($invoice_data)) {
              
                $rowID    = $invoice_data['insertInvID'];
                $ins_data = $this->general_model->update_row_data('tbl_custom_invoice', array('Customer_ListID' => $customerID, 'invoiceRefNumber' => $refNumber), $inv_data);

                $this->general_model->delete_row_data('tbl_subscription_invoice_item', array('invoiceDataID' => $rowID));
                $randomNum = $rowID;
                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $rowID;
                    $item['invoiceDataID']  = $rowID;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
                }

            } else {
                $randomNum               = substr(str_shuffle("0123456789"), 0, 5);
                $inv_data['insertInvID'] = $randomNum;
                $ins_data                = $this->general_model->insert_row('tbl_custom_invoice', $inv_data);

                foreach ($item_val as $k => $item) {
                    $item['subscriptionID'] = $randomNum;
                    $item['invoiceDataID']  = $randomNum;
                    $ins                    = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
                }
            }

            if ($ins_data && $ins) {
                $this->quickbooks->enqueue(QUICKBOOKS_MOD_INVOICE, $randomNum, '', '', $user);
                $this->session->set_flashdata('success', 'Successfully Processed Invoice');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong></div>');
            }

            redirect('home/invoices', 'refresh');
        }

        if ($this->uri->segment('3') != "") {
            $invoiceID = $this->uri->segment('3');
            $invoice   = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $invoiceID));
            $products  = $this->general_model->get_table_data('qb_test_invoice_lineitem', array('TxnID' => $invoiceID));
            $custom    = $this->general_model->get_row_data('tbl_custom_invoice', array('RefNumber' => $invoice['RefNumber']));
            if (!empty($custom)) {
                $data['custom'] = $custom;
            }
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $condition        = array('merchantID' => $user_id);
        $data['gateways'] = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
       
        $data['plans']     = $this->company_model->get_plan_data($user_id);
        $compdata          = $this->customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;
        $data['invoice']   = $invoice;
        $data['items']     = $products;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('pages/edit_invoice', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function update_gateway()
    {

        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {
                $data['login_info'] = $this->session->userdata('logged_in');

                $user_id = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info'] = $this->session->userdata('user_logged_in');

                $user_id = $data['login_info']['merchantID'];
            }


            if (!empty($this->czsecurity->xssCleanPostInput('gate')) && !empty($this->czsecurity->xssCleanPostInput('gateway_old')) && !empty($this->czsecurity->xssCleanPostInput('gateway_new'))) {
                $sub_ID = implode(',', $this->czsecurity->xssCleanPostInput('gate'));

                $old_gateway = $this->czsecurity->xssCleanPostInput('gateway_old');
                $new_gateway = $this->czsecurity->xssCleanPostInput('gateway_new');

                $condition = array('merchantDataID' => $user_id, 'paymentGateway' => $old_gateway);
                $num_rows  = $this->general_model->get_num_rows('tbl_subscriptions_qbo', $condition);

                if ($num_rows > 0) {
                    $update_data = array('paymentGateway' => $new_gateway);

                    $sss = $this->db->query("update tbl_subscriptions_qbo set paymentGateway='" . $new_gateway . "' where subscriptionID IN ($sub_ID) ");

                    if ($sss) {

                        $this->session->set_flashdata('success', 'Successfully Updated');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In update process. </div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> No data selected. </div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Validation Error. </div>');
            }
        }
        redirect('QBO_controllers/SettingSubscription/subscriptions', 'refresh');
    }

    //------------- To edit gateway ------------------//

    public function get_gateway_id()
    {

        $id         = $this->czsecurity->xssCleanPostInput('gateway_id');
        $merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
        $val        = array(
            'paymentGateway' => $id,
            'merchantDataID' => $merchantID,
            'merchantID'     => $merchantID,
        );

        $datas = $this->qbo_subscription_model->get_subscriptions_gatway_data($val);

        ?>


		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-left col-md-6"> <strong> Customer Name</strong></th>
					
					<th class="text-center col-md-6"><strong>Select List </strong></th>

				</tr>
				<?php
if (!empty($datas)) {
            foreach ($datas as $c_data) {
                ?>

						<tr>
							<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['fullName']; ?> </td>
							
							<td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['subscriptionID']; ?>" /> </td>


						</tr>


					<?php }
        } else {?>

					<tr>
						<td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Record Found"; ?> </td>

						<td class="text-right visible-lg col-md-6 control-label"> </td>

					<?php }?>

			</tbody>
		</table>


<?php die;
    }

    public function delete_subscription()
    {
        if ($this->czsecurity->xssCleanPostInput('qbosubscID') != "") {
            $subscID   = $this->czsecurity->xssCleanPostInput('qbosubscID');
            $condition = array('subscriptionID' => $subscID);

            $this->general_model->delete_row_data('tbl_subscription_invoice_item_qbo', $condition);

            $del = $this->general_model->delete_row_data('tbl_subscriptions_qbo', $condition);
            if ($del) {

                $this->session->set_flashdata('success', 'Successfully Deleted ');
                $res = array('status' => "success");
                echo json_encode($res);
                die;
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error </strong></div>');
            }
        }
       
        return false;
    }

    public function get_item_data()
    {

        if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

            $itemID                 = $this->czsecurity->xssCleanPostInput('itemID');
            $itemdata               = $this->general_model->get_row_data('QBO_test_item', array('ListID' => $itemID));
            $itemdata['SalesPrice'] = number_format($itemdata['SalesPrice'], 2, '.', '');
            echo json_encode($itemdata);
            die;
        }
        return false;
    }

    public function get_subplan()
    {

        $planid = $this->czsecurity->xssCleanPostInput('planID');

        $val = $this->qbo_company_model->get_subplan_data($planid);

        echo json_encode($val);
    }

    public function subscription_invoices()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$sbID = $this->uri->segment(4);
        $data['subs'] = $this->general_model->get_row_data('tbl_subscriptions_qbo', array('subscriptionID' => $sbID));

        $conditionGt                = array('planID' => $data['subs']['planID']);
        $conditionCust              = array('Customer_ListID' => $data['subs']['customerID'],'merchantID' =>$user_id);

        $subplansData       = $this->general_model->get_row_data('tbl_subscriptions_plan_qbo', $conditionGt);
        $subplansCustomer       = $this->general_model->get_row_data('QBO_custom_customer', $conditionCust);

		$data['page_num']      = 'customer_qbo';
		$condition				  = array('merchantID' => $user_id);
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		$invoices    			 = $this->qbo_subscription_model->get_subscription_invoice_data($user_id, $sbID);
		$data['totalInvoice'] = count($invoices);

        $data['revenue']             = $this->qbo_subscription_model->get_subscription_revenue($user_id, $sbID);

        $data['customerName'] = $subplansCustomer['fullName'];
        $data['planName'] = $subplansData['planName'];

		$condition  = array('comp.merchantID' => $user_id);
        
		$data['credits'] = $this->general_model->get_credit_user_data($condition);
        
		$data['invoices']    = $invoices;
        
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;
        
		$this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/page_subscription_details', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
	}

    protected function _createSubscriptionFirstInvoice($subs){
        $merchantID = $merchID = $subs['merchantDataID'];
        $customerID = $subs['customerID'];

        $autopay    = 0;
        $cardID     = $subs['cardID'];
        $paygateway = $subs['paymentGateway'];
        $autopay    = $subs['automaticPayment'];

        if ($subs['generatedInvoice'] < $subs['freeTrial']) {
            $free_trial = '1';
        } else {
            $free_trial = '0';
        }

        $total = $recurring = $onetime = 0;

        $tax = $taxval = $subs['taxID'];
        if ($tax) {
            $tax_data = $this->general_model->get_row_data('tbl_taxe_code_qbo', array('taxID' => $tax, 'merchantID' => $merchantID));
            $taxRate  = $tax_data['total_tax_rate'];
            $taxlsID  = $tax_data['taxID'];
        } else {
            $taxRate = 0;
            $taxlsID = 0;
        }

        $cs_data   = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));

        $isInvoiceExist = $this->general_model->get_row_data('tbl_subscription_auto_invoices', array('app_type' => 1, 'subscriptionID' => $subs['subscriptionID']));

        $item_val = [];
        $products = $subs['subscriptionItems'];
        foreach ($products as $key => $prod) {
            if($isInvoiceExist && $prod['oneTimeCharge']){
                continue;
            }
            
            $insert_row['itemListID']      = $prod['itemListID'];
            $insert_row['itemQuantity']    = $prod['itemQuantity'];
            $insert_row['itemRate']        = $prod['itemRate'];
            $insert_row['itemFullName']    = $prod['itemFullName'];
            $insert_row['itemTax']         = $prod['itemTax'];
            $insert_row['itemDescription'] = $prod['itemDescription'];
            $insert_row['itemTotal'] = $itemTotal = $prod['itemQuantity'] * $prod['itemRate'];
            /* Fetch product type*/
            $itemFetchdata  = $this->general_model->get_select_data('QBO_test_item', array('Type','ListID'), array('productID' => $prod['itemListID'], 'merchantID' => $merchantID));
            $insert_row['itemListType'] = $itemFetchdata['Type'];

            if($insert_row['itemTax'] == 1) {
                $itemTotal += ($insert_row['itemTotal'] * $taxRate / 100);
            }

            if($prod['oneTimeCharge']){
                $onetime += $itemTotal;
            } else {
                $recurring += $itemTotal;
            }

            $total = $total + $itemTotal;

            $item_val[] = $insert_row;
        }

        $val = array(
            'merchantID' => $subs['merchantDataID'],
        );

        $chh_mail = $subs['emailRecurring'];

        $inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
        if (!empty($inv_data)) {
            $inv_pre    = $inv_data['prefix'];
            $inv_po     = $inv_data['postfix'] + 1;
            $new_inv_no = $inv_pre . $inv_po;
            $Number     = $new_inv_no;
        }

        $customer_ref = $subs['customerID'];
        $address1     = $subs['address1'];
        $address2     = $subs['address2'];
        $country      = $subs['country'];
        $state        = $subs['state'];
        $city         = $subs['city'];
        $zipcode      = $subs['zipcode'];
        $phone        = $subs['bphone'];
        $baddress1    = $subs['baddress1'];
        $baddress2    = $subs['baddress2'];
        $bcountry     = $subs['bcountry'];
        $bstate       = $subs['bstate'];
        $bcity        = $subs['bcity'];
        $bzipcode     = $subs['bzipcode'];
        $bphone       = $subs['bphone'];

        $inv_date = $duedate = date($subs['nextGeneratingDate']);

        $fullname  = $cs_data['fullName'];
        $companyID = $cs_data['companyID'];

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $date      = $subs['firstDate'];

        if ($subs['subplansData']['proRate']) {
            $proRate = $subs['subplansData']['proRate'];
        } else {
            $proRate = 0;
        }

        if ($subs['subplansData']['proRateBillingDay']) {
            $proRateday = $subs['subplansData']['proRateBillingDay'];
            $nextInvoiceDate = $subs['subplansData']['nextMonthInvoiceDate'];
        } else {
            $proRateday = 1;
			$nextInvoiceDate = date('d', strtotime($date));
        }

        $in_num    = $subs['generatedInvoice'];
        $paycycle  = $subs['invoicefrequency'];

        $prorataCalculation = prorataCalculation([
            'paycycle' => $subs['invoicefrequency'],
            'proRateday' => $proRateday,
            'nextInvoiceDate' => $nextInvoiceDate,
            'total_amount' => $total,
            'onetime' => $onetime,
            'recurring' => $recurring,
            'date' => $date,
            'in_num' => $in_num,
            'proRate' => $proRate,
        ]);

        $next_date = $prorataCalculation['next_date'];
        $total = $prorataCalculation['total_amount'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),
        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $lineArray = array();
        
        if (!$tax) {
            $i = 0;
            for ($i = 0; $i < count($item_val); $i++) {
                $prorataCalculatedRate = 1;
                if($products[$i]['oneTimeCharge'] == 0){
                    $prorataCalculatedRate = $prorataCalculation['prorataCalculatedRate'];
                }
                $itemTotalAmount = $item_val[$i]['itemTotal'] * $prorataCalculatedRate;
                /* Check itemType is group */
                if($item_val[$i]['itemListType'] == 'Group'){
                    $LineObj = Line::create([
                        "Amount" => $itemTotalAmount,
                        "DetailType" => "GroupLineDetail",
                        "Description" => $item_val[$i]['itemDescription'],
                        "GroupLineDetail" => [
                            "GroupItemRef" => $item_val[$i]['itemListID'],
                            "Quantity" => $item_val[$i]['itemQuantity'],
                            
                        ],
                    ]);
                }else{
                    $LineObj = Line::create([
                        "Amount"              => $itemTotalAmount,
                        "DetailType"          => "SalesItemLineDetail",
                        "Description"         => $item_val[$i]['itemDescription'],
                        "SalesItemLineDetail" => [
                            "ItemRef"   => $item_val[$i]['itemListID'],
                            "Qty"       => $item_val[$i]['itemQuantity'],
                            "UnitPrice" => $item_val[$i]['itemRate'] * $prorataCalculatedRate,
                        ],

                    ]);
                }

                
                $lineArray[] = $LineObj;
            }

            //Add a new Invoice
            $theResourceObj = Invoice::create([
                "CustomerRef" => $customer_ref,
                "DocNumber"   => $Number,
                "BillAddr"    => [
                    "City"                   => $bcity,
                    "Line1"                  => $baddress1,
                    "Line2"                  => $baddress2,
                    "CountrySubDivisionCode" => $bstate,
                    "Country"                => $bcountry,
                    "PostalCode"             => $bzipcode,
                ],
                "ShipAddr"    => [
                    "Line1"      => $address1,
                    "Line2"      => $address2,
                    "City"       => $city,
                    "Country"    => $country,
                    "PostalCode" => $zipcode,
                ],

                "TotalAmt"    => $total,
                "Line"        => $lineArray,
                "DueDate"     => $duedate,
            ]);
        } else {
            $inv_amnt = 0;
            for ($i = 0; $i < count($item_val); $i++) {
                $prorataCalculatedRate = 1;
                if($products[$i]['oneTimeCharge'] == 0){
                    $prorataCalculatedRate = $prorataCalculation['prorataCalculatedRate'];
                }
                $amount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'];
                $inv_amnt += $amount;

                $itemTotalAmount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'] * $prorataCalculatedRate;

                /* Check itemType is group */
                if($item_val[$i]['itemListType'] == 'Group'){
                    $LineObj = Line::create([
                        "Amount" => $itemTotalAmount,
                        "DetailType" => "GroupLineDetail",
                        "Description" => $item_val[$i]['itemDescription'],
                        "GroupLineDetail" => [
                            "GroupItemRef" => $item_val[$i]['itemListID'],
                            "Quantity" => $item_val[$i]['itemQuantity'],
                            
                        ],
                    ]);
                }else{
                    $LineObj = Line::create([
                        "Amount"              => $itemTotalAmount,
                        "DetailType"          => "SalesItemLineDetail",
                        "Description"         => $item_val[$i]['itemDescription'],
                        "SalesItemLineDetail" => [
                            "ItemRef"    => $item_val[$i]['itemListID'],
                            "Qty"        => $item_val[$i]['itemQuantity'],
                            "UnitPrice"  => $item_val[$i]['itemRate'] * $prorataCalculatedRate,
                            "TaxCodeRef" => [
                                "value" => (isset($item_val[$i]['itemTax']) && $item_val[$i]['itemTax']) ? "TAX" : "NON",
                            ],
                        ],
                    ]);
                }
                
                $lineArray[] = $LineObj;
            }

            $theResourceObj = Invoice::create([
                "CustomerRef"  => $customer_ref,
                "DocNumber"    => $Number,
                "BillAddr"     => [
                    "City"                   => $bcity,
                    "Line1"                  => $baddress1,
                    "Line2"                  => $baddress2,
                    "CountrySubDivisionCode" => $bstate,
                    "Country"                => $bcountry,
                    "PostalCode"             => $bzipcode,
                ],
                "ShipAddr"     => [
                    "Line1"      => $address1,
                    "Line2"      => $address2,
                    "City"       => $city,
                    "Country"    => $country,
                    "PostalCode" => $zipcode,
                ],

                "TxnTaxDetail" => [
                    "TxnTaxCodeRef" => [
                        "value" => $taxval,
                    ],
                ],

                "TotalAmt"     => $total,
                "Line"         => $lineArray,
                "DueDate"      => $duedate,
            ]);
        }

        $resultingObj = $dataService->Add($theResourceObj);

        $error = $dataService->getLastError();

        $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $inv_po));

        $QBO_invoice_details['refNumber']              = $Number;
        $QBO_invoice_details['ShipAddress_Addr1']      = $address1;
        $QBO_invoice_details['ShipAddress_Addr2']      = $address2;
        $QBO_invoice_details['ShipAddress_City']       = $city;
        $QBO_invoice_details['ShipAddress_Country']    = $country;
        $QBO_invoice_details['ShipAddress_PostalCode'] = $zipcode;
        $QBO_invoice_details['ShipAddress_State']      = $state;

        $QBO_invoice_details['BillAddress_Addr1']      = $baddress1;
        $QBO_invoice_details['BillAddress_Addr2']      = $baddress2;
        $QBO_invoice_details['BillAddress_City']       = $bcity;
        $QBO_invoice_details['BillAddress_Country']    = $bcountry;
        $QBO_invoice_details['BillAddress_PostalCode'] = $bzipcode;
        $QBO_invoice_details['BillAddress_State']      = $bstate;

        $QBO_invoice_details['DueDate']          = $duedate;
        $QBO_invoice_details['BalanceRemaining'] = $total;
        $QBO_invoice_details['Total_payment']    = $total;
        $QBO_invoice_details['CustomerListID']   = $customer_ref;

        $QBO_invoice_details['IsPaid']       = 0;
        $QBO_invoice_details['merchantID']   = $merchID;
        $QBO_invoice_details['companyID']    = $realmID;
        $QBO_invoice_details['TimeCreated']  = date('Y-m-d H:i:s');
        $QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s');

        $error1 = '';
        if ($error != null) {
            $error1 .= $error->getResponseBody() . "\n";

            $st                                = '0';
            $error1                            = 'Error: Can not connect to QuickBooks';
            $action                            = 'Add Invoice';
            $QBO_invoice_details['UserStatus'] = 1;
            $invID                             = mt_rand(6000000, 9000000);
            $QBO_invoice_details['invoiceID']  = $invID;
        } else {
            $invID                            = $resultingObj->Id;
            $st                               = '1';
            $QBO_invoice_details['invoiceID'] = $invID;
            $error1                           = 'success';
            $action                           = 'Add Invoice';
        }

        $this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);

        if ($invID) {

            $schedule['invoiceID']  = $invID;
            $schedule['gatewayID']  = $paygateway;
            $schedule['cardID']     = $cardID;
            $schedule['customerID'] = $customerID;

            if ($autopay) {
                $schedule['autoInvoicePay']       = 1;
                $schedule['paymentMethod'] = 1;
            } else {
                $schedule['autoInvoicePay']       = 0;
                $schedule['paymentMethod'] = 0;
            }
            $schedule['merchantID'] = $merchID;
            $schedule['createdAt']  = date('Y-m-d H:i:s');
            $schedule['updatedAt']  = date('Y-m-d H:i:s');
            $this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);

            $subscription_auto_invoices_data = [
                'subscriptionID' => $subs['subscriptionID'],
                'invoiceID'      => $invID,
                'app_type'       => 1, //QBO
            ];

            $this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);

            if ($chh_mail == '1') {
                $inv_no    = $QBO_invoice_details['refNumber'];
                $invoiceID = $QBO_invoice_details['invoiceID'];
                $invoice   = $this->qbo_company_model->get_invoice_details_data($invoiceID);
                $item_arr  = [];
                foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
                    $insert_row['itemListID']   = $this->czsecurity->xssCleanPostInput('productID')[$key];
                    $insert_row['itemName']     = $this->czsecurity->xssCleanPostInput('plan_name')[$key];
                    $insert_row['Quantity']     = $this->czsecurity->xssCleanPostInput('quantity')[$key];
                    $insert_row['itemPrice']    = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
                    $insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
                    $insert_row['itemTax']      = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
                    $insert_row['Description']  = $this->czsecurity->xssCleanPostInput('description')[$key];
                    $insert_row['itemTotal']    = $this->czsecurity->xssCleanPostInput('total')[$key];
                    $item_arr[$key]             = $insert_row;
                }
                $this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $item_arr, 'F');
                $condition_mail = array('templateType' => '3', 'merchantID' => $merchID);

                $toEmail          = $cs_data['userEmail'];
                $company          = $cs_data['companyName'];
                $customer         = $cs_data['fullName'];
                $invoice_due_date = $duedate;
                $invoicebalance   = $QBO_invoice_details['BalanceRemaining'];

                $tr_date = date('Y-m-d H:i:s');
                $this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $customerID, $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $inv_no);
            }

            $first_date = strtotime($date);
            $current_time = time();
            if($subs['automaticPayment'] == 1 && $first_date <= $current_time){
                $paymentObj = new Manage_payments($subs['merchantDataID']);
                $paymentDetails =   $this->card_model->get_single_card_data($subs['cardID']);
                $saleData = [
                    'paymentDetails' => $paymentDetails,
                    'transactionByUser' => $this->transactionByUser,
                    'ach_type' => (!empty($paymentDetails['CardNo'])) ? 1 : 2,
                    'invoiceID'	=> $invID,
                    'crtxnID'	=> '',
                    'companyName'	=> $cs_data['companyName'],
                    'fullName'	=> $cs_data['fullName'],
                    'firstName'	=> $cs_data['firstName'],
                    'lastName'	=> $cs_data['lastName'],
                    'contact'	=> $cs_data['phoneNumber'],
                    'email'	=> $cs_data['userEmail'],
                    'amount'	=> $total,
                    'gatewayID' => $subs['paymentGateway'],
                    'storeResult' => true,
                    'customerListID' => $cs_data['Customer_ListID'],
                ];
    
                $saleData = array_merge($saleData, $subs);
                $paidResult = $paymentObj->_processSaleTransaction($saleData);
                if(isset($paidResult['response']) && $paidResult['response']){

                    $createPaymentObject = [
                        "TotalAmt"    => $total,
                        "SyncToken"   => 1,
                        "CustomerRef" => $customerID,
                        "Line"        => [
                            "LinkedTxn" => [
                                "TxnId"   => $invID,
                                "TxnType" => "Invoice",
                            ],
                            "Amount"    => $total,
                        ],
                    ];

                    $achValue = (!empty($paymentDetails['CardNo'])) ? false : true;
                    $paymentMethod = $this->general_model->qbo_payment_method($paymentDetails['CardType'], $merchID, $achValue);
                    if($paymentMethod){
                        $createPaymentObject['PaymentMethodRef'] = [
                            'value' => $paymentMethod['payment_id']
                        ];
                    }

                    $paidTransaction = $this->general_model->get_row_data('customer_transaction', ['id' => $paidResult['id']]);
                    if($paidTransaction && $paidTransaction['transactionID'] != ''){
                        $createPaymentObject['PaymentRefNum'] = substr($paidTransaction['transactionID'], 0, 21);
                    }

                    $newPaymentObj = Payment::create($createPaymentObject);
                    $savedPayment = $dataService->Add($newPaymentObj);
                    $crtxnID        = $savedPayment->Id;
                    $this->general_model->update_row_data('customer_transaction', ['id' => $paidResult['id']], ['qbListTxnID' => $crtxnID]);
                    
                    $invUpdate       = array('IsPaid' => '1', 'AppliedAmount' => $QBO_invoice_details['BalanceRemaining'], 'BalanceRemaining' => 0);
                    $conditionInv = array('invoiceID' => $invID, 'merchantID' => $merchID);
                    $this->general_model->update_row_data('QBO_test_invoice', $conditionInv, $invUpdate);
                }
            }
        }

        $qblog_data = array(
            'merchantID' => $merchID, 'qbAction'          => $action, 'qbStatus'   => $st,
            'updatedAt'  => date('Y-m-d H:i:s'),
            'createdAt'  => date('Y-m-d H:i:s'), 'qbText' => $error1, 'qbActionID' => $invID,
        );
        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qblog_data);
        if($syncid){
            $qbSyncID = 'CQ-'.$syncid;
            
            $qbSyncStatus = $action.' #'.$invID.' '.$error1;
            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
        }

        $gen_inv = $subs['generatedInvoice'] + 1;
        $this->general_model->update_row_data('tbl_subscriptions_qbo',
            array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));

        $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']), array('postfix' => $inv_po));
    }
}
