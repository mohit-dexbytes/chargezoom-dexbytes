<?php
/**
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\RefundReceipt;

require APPPATH .'libraries/Manage_payments.php';
class AuthPayment extends CI_Controller
{
    private $merchantID;
    private $resellerID;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();
        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        
        $this->load->config('auth_pay');
        $this->load->model('quickbooks');
        $this->load->model('card_model');

        $this->load->model('general_model');
        $this->load->model('QBO_models/qbo_company_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->merchantID = $logged_in_data['merchID'];
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $logged_in_data['merchantID'];
        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {

        redirect('QBO_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $user_id   = $merchID;
        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		$checkPlan = check_free_plan_transactions();
        $custom_data_fields = [];
        $cardID = $this->czsecurity->xssCleanPostInput('CardID');
        if (!$cardID || empty($cardID)) {
        $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
        }

        $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
        $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
        }
        $gateway = $gatlistval;
        $cusproID  = '';
        $cusproID  = $this->czsecurity->xssCleanPostInput('customerProcessID');
        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        $ref_number = array();
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

        if ($checkPlan && $cardID != "" && $gateway != "") {

            $in_data   = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
            $m_data    = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));

            $resellerID = $this->resellerID;

            $apiloginID     = $gt_result['gatewayUsername'];
            $transactionKey = $gt_result['gatewayPassword'];
            if (!empty($in_data)) {

                $customerID = $in_data['CustomerListID'];
                $c_data     = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $user_id));
               
                $companyID = $c_data['companyID'];

                if ($cardID == 'new1') {
                    $cardID_upd = $cardID;
                    $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv        = $this->czsecurity->xssCleanPostInput('cvv');

                    $address1 = $this->czsecurity->xssCleanPostInput('address1');
                    $address2 = $this->czsecurity->xssCleanPostInput('address2');
                    $city     = $this->czsecurity->xssCleanPostInput('city');
                    $country  = $this->czsecurity->xssCleanPostInput('country');
                    $phone    = $this->czsecurity->xssCleanPostInput('contact');
                    $state    = $this->czsecurity->xssCleanPostInput('state');
                    $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');

                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $user_id,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                    ];

                } else {
                    $card_data = $accountDetails = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $cvv       = $card_data['CardCVV'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];

                    $address1 = $card_data['Billing_Addr1'];
                    $address2 = $card_data['Billing_Addr2'];
                    $city     = $card_data['Billing_City'];
                    $zipcode  = $card_data['Billing_Zipcode'];
                    $state    = $card_data['Billing_State'];
                    $country  = $card_data['Billing_Country'];
                    $phone    = $card_data['Billing_Contact'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $error = '';

                if (!empty($cardID)) {
                    $val = array(
                        'merchantID' => $merchID,
                    );
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];
                    $dataService  = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));

                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                    $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

                    if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {

                        if ($in_data['BalanceRemaining'] > 0) {
                            $cr_amount = 0;
                            $amount    = $in_data['BalanceRemaining'];
                            $amount    = $this->czsecurity->xssCleanPostInput('inv_amount');

                            $amount       = $amount - $cr_amount;
                            $transaction1 = new AuthorizeNetAIM($apiloginID, $transactionKey);
                            $transaction1->setSandbox($this->config->item('auth_test_mode'));

                            if($sch_method == 1) {
                                if (strlen($exyear) > 2) {
                                    $minLength = 4 - strlen($exyear);
                                    $exyear    = substr($exyear, $minLength);
                                }
    
                                $cardType =  $this->general_model->getType($card_no);
                                $isACH = false;

                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }
                                $expry = $expmonth . $exyear;
    
                                $result = $transaction1->authorizeAndCapture($amount, $card_no, $expry);

                                $echeckString = "";
                            } else {
                                $transaction1->setECheck($accountDetails['routeNumber'], $accountDetails['accountNumber'], $accountDetails['accountType'], $bank_name='Wells Fargo Bank NA', $accountDetails['accountName'], $accountDetails['secCodeEntryMethod']);
                                $result = $transaction1->authorizeAndCapture($amount);
                                $echeckString = " ECheck";

                                $cardType =  'Check';
                                $isACH = true;
                            }

                            $crtxnID = '';
                            $inID    = '';

                            if ($result->response_code == "1"  && $result->transaction_id != 0 && $result->transaction_id != '') {

                                $val = array(
                                    'merchantID' => $merchID,
                                );
                                $data = $this->general_model->get_row_data('QBO_token', $val);

                                $accessToken  = $data['accessToken'];
                                $refreshToken = $data['refreshToken'];
                                $realmID      = $data['realmID'];
                                $dataService  = DataService::Configure(array(
                                    'auth_mode'       => $this->config->item('AuthMode'),
                                    'ClientID'        => $this->config->item('client_id'),
                                    'ClientSecret'    => $this->config->item('client_secret'),
                                    'accessTokenKey'  => $accessToken,
                                    'refreshTokenKey' => $refreshToken,
                                    'QBORealmID'      => $realmID,
                                    'baseUrl'         => $this->config->item('QBOURL'),
                                ));

                                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                                $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");


                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);
                                }
                                

                                $createPaymentObject = [
                                    "TotalAmt"    => $amount,
                                    "SyncToken"   => 1,
                                    "CustomerRef" => $customerID,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $invoiceID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $amount,
                                    ],
                                ];
                                
                                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, $isACH);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($result->transaction_id) && $result->transaction_id != ''){
                                    $createPaymentObject['PaymentRefNum'] = substr($result->transaction_id, 0, 21);
                                }

                                $newPaymentObj = Payment::create($createPaymentObject);
                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                   
                                    
                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();
                                    $qbID   = $result->transaction_id;
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> '.$msg .'</div>');
                                   

                                } else {
                                    $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                                    $ref_number     = $in_data['refNumber'];
                                    $tr_date        = date('Y-m-d H:i:s');
                                    $toEmail        = $c_data['userEmail'];
                                    $company        = $c_data['companyName'];
                                    $customer       = $c_data['fullName'];
                                    
                                    $inID    = $invoiceID;
                                    $crtxnID = $savedPayment->Id;

                                    $st     = '1';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success";
                                    $qbID   = $result->transaction_id;
                                    $this->session->set_flashdata('success', 'Success');

                                    
                                }

                                if ($sch_method == 1 && $this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                                    $cardDetails = [
                                        'card_no'    => $card_no,
                                        'cvv'        => $cvv,
                                        'customerID' => $customerID,
                                        'expmonth'   => $expmonth,
                                        'exyear'     => $exyear,
                                        'companyID'  => $c_data['companyID'],
                                        'user_id'    => $user_id,
                                        'address1'   => $address1,
                                        'city'       => $city,
                                        'state'      => $state,
                                        'country'    => $country,
                                        'phone'      => $phone,
                                        'zipcode'    => $zipcode,
                                        'country'    => $country,
                                    ];

                                    save_qbo_card($cardDetails, $user_id);
                                } else if($sch_method == 2 && !($this->czsecurity->xssCleanPostInput('tc')) && $cardID == "new1" ){
						            $id1 = $this->card_model->process_ack_account($accountDetails);
                                }

                            } else {
                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Failed";
                                $qbID   = $invoiceID;
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
                            }
                            $qbID   = $invoiceID;
                            $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result->transaction_id,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                            $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                            if($syncid){
                                $qbSyncID = 'CQ-'.$syncid;
                                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                            }
                            
                            
                            $transactiondata                        = array();
                            if ($result->transaction_id != 0 && $result->transaction_id != '') 
                            {
                                $transactiondata['transactionID']       = $result->transaction_id;
                                $transactiondata['transactionCode']     = $result->response_code;
                            }else{
                                $transactiondata['transactionID']       = 'TXNFAILED'.time();
                                $transactiondata['transactionCode']     = 400;
                            }
                            
                            $transactiondata['transactionStatus']   = $result->response_reason_text;
                            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                            $transactiondata['transactionCard']     = substr($result->account_number, 4);
                            $transaction['qbListTxnID'] = $crtxnID;
                            $transactiondata['transactionType']    = $result->transaction_type;
                            $transactiondata['gatewayID']          = $gateway;
                            $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                            $transactiondata['customerListID']     = $in_data['CustomerListID'];
                            $transactiondata['transactionAmount']  = $result->amount;
                            $transactiondata['merchantID']         = $merchID;
                            $transactiondata['invoiceID']          = $inID;
                            $transactiondata['resellerID']         = $resellerID;
                            $transactiondata['gateway']            = "Auth$echeckString";
                            $CallCampaign = $this->general_model->triggerCampaign($merchID,$transactiondata['transactionCode']);
                            if(!empty($this->transactionByUser)){
                                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                            }
                            if($custom_data_fields){
                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                            }

                            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

                            if ($st == 1 && $chh_mail == '1') {
                                $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result->transaction_id);
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');
                    }

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        $cusproID = 1;
        if ($cusproID != "") {
            $trans_id     = ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time();
            $invoice_IDs  = array();
            $receipt_data = array(
                'proccess_url'      => 'QBO_controllers/Create_invoice/Invoice_details',
                'proccess_btn_text' => 'Process New Invoice',
                'sub_header'        => 'Sale',
                'checkPlan'         =>  $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            $this->session->set_userdata("in_data", $in_data);
            
            redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $trans_id, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

    public function create_customer_sale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if ($this->session->userdata('logged_in')) {
            $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

		$checkPlan = check_free_plan_transactions();

        if ($checkPlan && !empty($this->input->post(null, true))) {
            $inv_array   = array();
            $inv_invoice = array();

            $custom_data_fields = [];
            $applySurcharge = false;
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($gatlistval != "" && !empty($gt_result)) {
                $apiloginID     = $gt_result['gatewayUsername'];
                $transactionKey = $gt_result['gatewayPassword'];
                $resellerID = $this->resellerID;
                $custID     = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $custID, 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
                $transaction->setSandbox($this->config->item('auth_test_mode'));

                $cvv = '';

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear = substr($exyear, 2);
                    $expry  = $expmonth . $exyear;
                    $cvv    = $this->czsecurity->xssCleanPostInput('cvv');

                } else {

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];

                    $exyear = $card_data['cardYear'];
                    if (strlen($exyear) > 2) {
                        $minLength = 4 - strlen($exyear);
                        $exyear    = substr($exyear, $minLength);
                    }
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth . $exyear;
                    $cvv = $card_data['CardCVV'];
                }
                /*Added card type in transaction table*/
                $cardType =  $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $address1 = $this->czsecurity->xssCleanPostInput('address');
                $address2 = '';
                $city     = $this->czsecurity->xssCleanPostInput('city');
                $country  = $this->czsecurity->xssCleanPostInput('country');
                $phone    = $this->czsecurity->xssCleanPostInput('phone');
                $state    = $this->czsecurity->xssCleanPostInput('state');
                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');

                $transaction->__set('company', $this->czsecurity->xssCleanPostInput('companyName'));
                $transaction->__set('first_name', $this->czsecurity->xssCleanPostInput('fistName'));
                $transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
                $transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
                $transaction->__set('country', $this->czsecurity->xssCleanPostInput('country'));
                $transaction->__set('city', $this->czsecurity->xssCleanPostInput('city'));
                $transaction->__set('state', $this->czsecurity->xssCleanPostInput('state'));
                $transaction->__set('phone', $this->czsecurity->xssCleanPostInput('phone'));

                $transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                    $transaction->__set('invoice_num', $new_invoice_number);
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $transaction->__set('po_num', $this->czsecurity->xssCleanPostInput('po_number'));
                }

                $result = $transaction->authorizeAndCapture($amount, $card_no, $expry);
                if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '') {

                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    $val  = array('merchantID' => $merchantID);
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));
                    $refNumber = array();
                    $qblist    = array();
                    if (!empty($invoiceIDs)) {
                        
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");
                            
                            $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            
                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;


                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                $actualInvoicePayAmount += $surchargeAmount;
                                $amount_data += $surchargeAmount;
                                $updatedInvoiceData = [
                                    'inID' => $inID,
                                    'targetInvoiceArray' => $targetInvoiceArray,
                                    'merchantID' => $user_id,
                                    'dataService' => $dataService,
                                    'realmID' => $realmID,
                                    'accessToken' => $accessToken,
                                    'refreshToken' => $refreshToken,
                                    'amount' => $surchargeAmount,
                                ];
                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                            }
                            $ispaid      = 0;
                            $isRun = 0;
                            
                            if($saleAmountRemaining > 0){
                                $BalanceRemaining = 0.00;
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 1;

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 0;
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

                                
                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);
                                
                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);
                                     
                                    $createPaymentObject = [
                                        "TotalAmt" => $actualInvoicePayAmount,
                                        "SyncToken" => 1,
                                        "CustomerRef" => $custID,
                                        "Line" => [
                                            "LinkedTxn" => [
                                                "TxnId" => $inID,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount" => $actualInvoicePayAmount
                                        ]
                                    ];

                                    $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                    if($paymentMethod){
                                        $createPaymentObject['PaymentMethodRef'] = [
                                            'value' => $paymentMethod['payment_id']
                                        ];
                                    }
                                    
                                    if(isset($result->transaction_id) && $result->transaction_id != ''){
                                        $createPaymentObject['PaymentRefNum'] = substr($result->transaction_id, 0, 21);
                                    }
                                    
                                    $newPaymentObj = Payment::create($createPaymentObject);
                                    
                                    $savedPayment = $dataService->Add($newPaymentObj);

                                    $error = $dataService->getLastError();
                                   
                                    if ($error != null) {

                                        
                                        $err = '';
                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                        $st = '0';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success but " . $error->getResponseBody();

                                        $qbID = $trID;
                                        $pinv_id = '';
                                    } else {
                                        
                                        $pinv_id = '';
                                        $pinv_id        =  $savedPayment->Id;
                                        $st = '1';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success";
                                        $qbID = $trID;
                                        $customerID     = $custID;
                                        $ref_number     = implode(',', $refNumber);
                                        
                                    
                                    }
                                    $qbID = $inID;
                                    $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                    $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                    if($syncid){
                                        $qbSyncID = 'CQ-'.$syncid;

                                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                    }

                                    $transactiondata                        = array();
                                    if ($result->transaction_id != 0 && $result->transaction_id != '') 
                                    {
                                        $transactiondata['transactionID']       = $result->transaction_id;
                                        $transactiondata['transactionCode']     = $result->response_code;
                                    }else{
                                        $transactiondata['transactionID']       = 'TXNFAILED'.time();
                                        $transactiondata['transactionCode']     = 400;
                                    }
                                    $transactiondata['transactionStatus']   = $result->response_reason_text;
                                    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                                    $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                                    
                                    $transactiondata['transactionCard']     = substr($result->account_number, 4);
                                    $transactiondata['gatewayID']           = $gatlistval;
                                    $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
                                    $transactiondata['transactionType']     = $result->transaction_type;
                                    $transactiondata['customerListID']      = $custID;
                                    $transactiondata['transactionAmount']   = $actualInvoicePayAmount;
                                    $transactiondata['merchantID']          = $merchantID;
                                    $transactiondata['resellerID']          = $this->resellerID;
                                    $transactiondata['invoiceID']           = $inID;
                                    $transactiondata['qbListTxnID']         = $pinv_id;
                                    $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                                    if($custom_data_fields){
                                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
                                    $transactiondata['gateway'] = "Auth";
                                    $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                                    if(!empty($this->transactionByUser)){
                                        $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                        $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                    }
                                    if($custom_data_fields){
                                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                                    }

                                    $id                         = $this->general_model->insert_row('customer_transaction', $transactiondata);
                                }
                            }
                            $payIndex++;
                        }
                        
                    } else {
                        $transactiondata                        = array();
                        if ($result->transaction_id != 0 && $result->transaction_id != '') 
                        {
                            $transactiondata['transactionID']       = $result->transaction_id;
                            $transactiondata['transactionCode']     = $result->response_code;
                        }else{
                            $transactiondata['transactionID']       = 'TXNFAILED'.time();
                            $transactiondata['transactionCode']     = 400;
                        }
                        $transactiondata['transactionStatus']   = $result->response_reason_text;
                        $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                        $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                        $transactiondata['transactionCard']     = substr($result->account_number, 4);
                        $transactiondata['gatewayID']           = $gatlistval;
                        $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
                        $transactiondata['transactionType']     = $result->transaction_type;
                        $transactiondata['customerListID']      = $custID;
                        $transactiondata['transactionAmount']   = $result->amount;
                        $transactiondata['merchantID']          = $merchantID;
                        $transactiondata['resellerID']          = $this->resellerID;
                        $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                        $transactiondata['gateway'] = "Auth";
                        if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
                        $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        }
                        if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }

                        $id                         = $this->general_model->insert_row('customer_transaction', $transactiondata);
                        $ref_number = '';
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                                        
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['userEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['fullName'];
                    if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $custID, $ref_number, $amount, $tr_date, $result->transaction_id);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $transactiondata                        = array();
                    if ($result->transaction_id != 0 && $result->transaction_id != '') 
                    {
                        $transactiondata['transactionID']       = $result->transaction_id;
                        $transactiondata['transactionCode']     = $result->response_code;
                    }else{
                        $transactiondata['transactionID']       = 'TXNFAILED'.time();
                        $transactiondata['transactionCode']     = 400;
                    }
                    
                    $transactiondata['transactionStatus']   = $result->response_reason_text;
                    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                    $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                    
                    $transactiondata['transactionCard']     = substr($result->account_number, 4);
                    $transactiondata['gatewayID']           = $gatlistval;
                    $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
                    $transactiondata['transactionType']     = $result->transaction_type;
                    $transactiondata['customerListID']      = $custID;
                    $transactiondata['transactionAmount']   = $result->amount;
                    $transactiondata['merchantID']          = $merchantID;
                    $transactiondata['resellerID']          = $this->resellerID;
                    $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                    $transactiondata['gateway'] = "Auth";
                    if($custom_data_fields){
                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                    }
                    $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                    if(!empty($this->transactionByUser)){
                        $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                        $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                    }
                    if($custom_data_fields){
                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                    }

                    $id                         = $this->general_model->insert_row('customer_transaction', $transactiondata);
                    
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
                }

                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                    $card_type      = $this->general_model->getType($card_no);
                    $friendlyname   = $card_type . ' - ' . substr($card_no, -4);
                    $card_condition = array(
                        'customerListID'           => $custID,
                        'customerCardfriendlyName' => $friendlyname,
                    );

                    $crdata = $this->card_model->chk_card_firendly_name($custID, $friendlyname);

                    if ($crdata > 0) {

                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $merchantID,
                            'CardType'                     => $card_type,
                            'CustomerCard'                 => $this->card_model->encrypt($card_no),
                            'CardCVV'                      => '', 
                            'updatedAt'                    => date("Y-m-d H:i:s"),
                            'Billing_Addr1'                => $address1,
                            'Billing_Addr2'                => $address2,
                            'Billing_City'                 => $city,
                            'Billing_State'                => $state,
                            'Billing_Country'              => $country,
                            'Billing_Contact'              => $phone,
                            'Billing_Zipcode'              => $zipcode,
                        );

                        $this->card_model->update_card_data($card_condition, $card_data);
                    } else {
                        $card_data = array('cardMonth' => $expmonth,
                            'cardYear'                     => $exyear,
                            'CardType'                     => $card_type,
                            'CustomerCard'                 => $this->card_model->encrypt($card_no),
                            'CardCVV'                      => '',
                            'customerListID'               => $custID,
                            'companyID'                    => $companyID,
                            'merchantID'                   => $merchantID,
                            'customerCardfriendlyName'     => $friendlyname,
                            'createdAt'                    => date("Y-m-d H:i:s"),
                            'Billing_Addr1'                => $address1,
                            'Billing_Addr2'                => $address2,
                            'Billing_City'                 => $city,
                            'Billing_State'                => $state,
                            'Billing_Country'              => $country,
                            'Billing_Contact'              => $phone,
                            'Billing_Zipcode'              => $zipcode,
                        );
                        

                        $id1 = $this->card_model->insert_card_data($card_data);

                    }

                }

               
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');


    }

    public function create_customer_auth()
    {
        
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");

        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        
		$checkPlan = check_free_plan_transactions();
        $custom_data_fields = [];
        if ($checkPlan && !empty($this->input->post(null, true))) {

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($gatlistval != "" && !empty($gt_result)) {
                $apiloginID     = $gt_result['gatewayUsername'];
                $transactionKey = $gt_result['gatewayPassword'];

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID  = $comp_data['companyID'];

                $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
                $transaction->setSandbox($this->config->item('auth_test_mode'));

                $cvv = '';

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear = substr($exyear, 2);
                    $expry  = $expmonth . $exyear;
                    $cvv    = $this->czsecurity->xssCleanPostInput('cvv');

                } else {

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];

                    $exyear = $card_data['cardYear'];
                    if (strlen($exyear) > 2) {
                        $minLength = 4 - strlen($exyear);
                        $exyear    = substr($exyear, $minLength);
                    }

                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth . $exyear;
                    $cvv = $card_data['CardCVV'];
                    
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $po_number = $this->czsecurity->xssCleanPostInput('po_number');
                if (!empty($po_number)) {
                    $custom_data_fields['po_number'] = $po_number;
                }

                $address1 = $this->czsecurity->xssCleanPostInput('address');
                $address2 = '';
                $city     = $this->czsecurity->xssCleanPostInput('city');
                $country  = $this->czsecurity->xssCleanPostInput('country');
                $phone    = $this->czsecurity->xssCleanPostInput('phone');
                $state    = $this->czsecurity->xssCleanPostInput('state');
                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');

                $transaction->__set('company', $this->czsecurity->xssCleanPostInput('companyName'));
                $transaction->__set('first_name', $this->czsecurity->xssCleanPostInput('fistName'));
                $transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
                $transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
                $transaction->__set('country', $this->czsecurity->xssCleanPostInput('country'));
                $transaction->__set('city', $this->czsecurity->xssCleanPostInput('city'));
                $transaction->__set('state', $this->czsecurity->xssCleanPostInput('state'));
                $transaction->__set('phone', $this->czsecurity->xssCleanPostInput('phone'));

                $transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                if (!empty($po_number)) {
                    $transaction->__set('po_num', $po_number);
                }
                $result = $transaction->authorizeOnly($amount, $card_no, $expry);

                if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '') 
                {

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_type      = $this->general_model->getType($card_no);

                        $friendlyname   = $card_type . ' - ' . substr($card_no, -4);
                        $card_condition = array(
                            'customerListID'           => $customerID,
                            'customerCardfriendlyName' => $friendlyname,
                        );

                        $crdata = $this->card_model->chk_card_firendly_name($customerID, $friendlyname);

                        if ($crdata > 0) {

                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'CardType'                     => $card_type,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '',
                                'updatedAt'                    => date("Y-m-d H:i:s"),
                                'Billing_Addr1'                => $address1,
                                'Billing_Addr2'                => $address2,
                                'Billing_City'                 => $city,
                                'Billing_State'                => $state,
                                'Billing_Country'              => $country,
                                'Billing_Contact'              => $phone,
                                'Billing_Zipcode'              => $zipcode,
                            );

                            $this->card_model->update_card_data($card_condition, $card_data);
                        } else {
                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'CardType'                     => $card_type,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '',
                                'customerListID'               => $customerID,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'customerCardfriendlyName'     => $friendlyname,
                                'createdAt'                    => date("Y-m-d H:i:s"),
                                'Billing_Addr1'                => $address1,
                                'Billing_Addr2'                => $address2,
                                'Billing_City'                 => $city,
                                'Billing_State'                => $state,
                                'Billing_Country'              => $country,
                                'Billing_Contact'              => $phone,
                                'Billing_Zipcode'              => $zipcode,
                            );

                            $id1 = $this->card_model->insert_card_data($card_data);

                        }
                        $custom_data_fields['card_type'] = $cardType;

                    }

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
                }

                $transactiondata                            = array();
                if ($result->transaction_id != 0 && $result->transaction_id != '') 
                {
                    $transactiondata['transactionID']           = $result->transaction_id;
                    $transactiondata['transactionCode']         = $result->response_code;
                }else{
                    $transactiondata['transactionID']       = 'TXNFAILED'.time();
                    $transactiondata['transactionCode']         = 400;
                }
                
                $transactiondata['transactionStatus']       = $result->response_reason_text;
                $transactiondata['transactionDate']         = date('Y-m-d H:i:s');
                $transactiondata['transactionModified']     = date('Y-m-d H:i:s');
                
                $transactiondata['transactionCard']         = substr($result->account_number, 4);
                $transactiondata['gatewayID']               = $gatlistval;
                $transactiondata['transactionGateway']      = $gt_result['gatewayType'];
                $transactiondata['transactionType']         = $result->transaction_type;
                $transactiondata['transaction_user_status'] = '5';
                $transactiondata['customerListID']          = $this->czsecurity->xssCleanPostInput('customerID');
                $transactiondata['transactionAmount']       = $result->amount;
                $transactiondata['merchantID']              = $merchantID;
                $transactiondata['resellerID']              = $this->resellerID;
                $transactiondata['gateway']                 = "Auth";
                $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                if($custom_data_fields){
                    $transactiondata['custom_data_fields'] = json_encode($custom_data_fields);
                }

                $id                                         = $this->general_model->insert_row('customer_transaction', $transactiondata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_auth',
            'proccess_btn_text' => 'Process New Transaction',
            'sub_header'        => 'Authorize',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
        
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID = $this->czsecurity->xssCleanPostInput('txnID1');

            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $merchantID = $this->session->userdata('logged_in')['merchID'];
            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $apiloginID     = $gt_result['gatewayUsername'];
            $transactionKey = $gt_result['gatewayPassword'];

            $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
            $transaction->setSandbox($this->config->item('auth_test_mode'));

            $customerID = $paydata['customerListID'];
            $amount = $paydata['transactionAmount'];
           
            $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
            $result    = $transaction->priorAuthCapture($tID, $amount);

            if ($result->response_code == '1') {

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {

                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                }
               
                $this->session->set_flashdata('success', 'Successfully Captured Authorization');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');

            }

            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result->transaction_id;
            $transactiondata['transactionStatus']   = $result->response_reason_text;
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result->response_code;
            $transactiondata['transactionCard']     = $result->account_number;
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];

            $transactiondata['transactionType']   = $result->transaction_type;
            $transactiondata['customerListID']    = $customerID;
            $transactiondata['transactionAmount'] = $result->amount;
            $transactiondata['merchantID']        = $merchantID;
            $transactiondata['resellerID']        = $this->resellerID;
            $transactiondata['gateway']           = "Auth";
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }

            $id          = $this->general_model->insert_row('customer_transaction', $transactiondata);
            $invoice_IDs = array();
           
            $receipt_data = array(
                'proccess_url'      => 'QBO_controllers/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if ($result->transaction_id == '') {
                $result->transaction_id = 'null';
            }
            redirect('QBO_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result->transaction_id, 'refresh');
            
        }

        return false;

    }

    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiloginID     = $gt_result['gatewayUsername'];
            $transactionKey = $gt_result['gatewayPassword'];

            $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
            $transaction->setSandbox($this->config->item('auth_test_mode'));

            $card = '';

            $card       = $paydata['transactionCard'];
            $customerID = $paydata['customerListID'];

            $amount = $paydata['transactionAmount'];
            $total  = $this->czsecurity->xssCleanPostInput('ref_amount');
            $amount = $total;
           
            $merchantID = $paydata['merchantID'];
            if (empty($card)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Regiter your card to refund</strong></div>');
                

            }

            $result = $transaction->credit($tID, $amount, $card);

            if ($result->response_code == '1') {

                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );
                $data = $this->general_model->get_row_data('QBO_token', $val);

                $accessToken  = $data['accessToken'];
                $refreshToken = $data['refreshToken'];
                $realmID      = $data['realmID'];
                $dataService  = DataService::Configure(array(
                    'auth_mode'       => $this->config->item('AuthMode'),
                    'ClientID'        => $this->config->item('client_id'),
                    'ClientSecret'    => $this->config->item('client_secret'),
                    'accessTokenKey'  => $accessToken,
                    'refreshTokenKey' => $refreshToken,
                    'QBORealmID'      => $realmID,
                    'baseUrl'         => $this->config->item('QBOURL'),
                ));

                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                $refund  = $amount;
                $ref_inv = explode(',', $paydata['invoiceID']);
                if (!empty($paydata['invoiceID'])) {

                   
                    $item_data = $this->qbo_customer_model->get_merc_invoice_item_data($paydata['invoiceID'],$paydata['merchantID']);
                            

                    if (!empty($item_data)) {
                        $lineArray = array();
                               
                            $i = 0;
                            for ($i = 0; $i < count($item_data); $i++) {
                                $LineObj = Line::create([
                                    "Amount"              => $item_data[$i]['itemQty']*$item_data[$i]['itemPrice'],
                                    "DetailType"          => "SalesItemLineDetail",
                                    "Description" => $item_data[$i]['itemDescription'],
                                    "SalesItemLineDetail" => [
                                        "ItemRef" => $item_data[$i]['itemID'],
                                        "Qty" => $item_data[$i]['itemQty'],
                                        "UnitPrice" => $item_data[$i]['itemPrice'],
                                        "TaxCodeRef" => [
                                            "value" => (isset($item_data[$i]['itemTax']) && $item_data[$i]['itemTax']) ? "TAX" : "NON",
                                        ]
                                    ],
            
                                ]);
                                $lineArray[] = $LineObj;
                            }
                       
                        $acc_data = $this->general_model->get_select_data('QBO_accounts_list', array('accountID'), array('accountName' => 'Undeposited Funds', 'merchantID' => $paydata['merchantID']));
                        if (!empty($acc_data)) {
                            $ac_value = $acc_data['accountID'];
                        } else {
                            $ac_value = '4';
                        }
                        $invoice_tax = $this->qbo_customer_model->get_merc_invoice_tax_data($paydata['invoiceID'],$paydata['merchantID']);
                        $theResourceObj = RefundReceipt::create([
                            "CustomerRef"         => $paydata['customerListID'],
                            "Line"                => $lineArray,
                            "DepositToAccountRef" => [
                                "value" => $ac_value,
                                "name"  => "Undeposited Funds",
                            ],
                            "TxnTaxDetail" => [
                                "TxnTaxCodeRef" => [
                                    "value" => (isset($invoice_tax['taxID']) && $invoice_tax['taxID']) ? $invoice_tax['taxID'] : "",
                                ],
                            ],
                        ]);
                        $resultingObj = $dataService->Add($theResourceObj);
                        $error        = $dataService->getLastError();

                        if ($error != null) {

                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund ' . $error . '</strong></div>');
                            redirect('QBO_controllers/Payments/payment_transaction', 'refresh');
                        }
                        $ins_id    = $resultingObj->Id;
                        $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $total,
                            'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),

                        );
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                    }
                } else {
                    $ins_id    = '';
                    $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $total,
                        'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                        'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),

                    );
                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                }

                $this->qbo_customer_model->update_refund_payment($tID, 'AUTH');
                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
            }
            $transactiondata                        = array();
            if ($result->transaction_id != 0 && $result->transaction_id != '') 
            {
                $transactiondata['transactionID']       = $result->transaction_id;
                $transactiondata['transactionCode']     = $result->response_code;
            }else{
                $transactiondata['transactionID']       = 'TXNFAILED'.time();
                $transactiondata['transactionCode']     = 400;
            }
            $transactiondata['transactionStatus']   = $result->response_reason_text;
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];

            $transactiondata['transactionType']   = $result->transaction_type;
            $transactiondata['customerListID']    = $customerID;
            $transactiondata['transactionAmount'] = $result->amount;
            $transactiondata['merchantID']        = $merchantID;
            $transactiondata['resellerID']        = $this->resellerID;
            $transactiondata['gateway']           = "Auth";
            if (!empty($paydata['invoiceID'])) {
                $transactiondata['invoiceID'] = $paydata['invoiceID'];
            }
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }

            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            if (!empty($this->czsecurity->xssCleanPostInput('payrefund'))) {
                $invoice_IDs = array();
              
                $receipt_data = array(
                    'proccess_url'      => 'QBO_controllers/Payments/payment_refund',
                    'proccess_btn_text' => 'Process New Refund',
                    'sub_header'        => 'Refund',
                );

                $this->session->set_userdata("receipt_data", $receipt_data);
                $this->session->set_userdata("invoice_IDs", $invoice_IDs);

                if ($paydata['invoiceTxnID'] == '') {
                    $paydata['invoiceTxnID'] = 'null';
                }
                if ($paydata['customerListID'] == '') {
                    $paydata['customerListID'] = 'null';
                }
                if ($result->transaction_id == '') {
                    $result->transaction_id = 'null';
                }
                redirect('QBO_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result->transaction_id, 'refresh');
            } else {

                redirect('QBO_controllers/Payments/payment_transaction', 'refresh');

            }

        }

    }

   
    public function create_customer_void()
    {
        //Show a form here which collects someone's name and e-mail address
        $result = array();
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID1');

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($tID != '' && !empty($gt_result)) {
                $apiloginID     = $gt_result['gatewayUsername'];
                $transactionKey = $gt_result['gatewayPassword'];

                $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
                $transaction->setSandbox($this->config->item('auth_test_mode'));

                $customerID = $paydata['customerListID'];
                $amount    = $paydata['transactionAmount'];
                $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
                $comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
                $result    = $transaction->void($tID);
                if ($result->response_code == '1') {

                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                    if ($chh_mail == '1') {
                        $condition  = array('transactionID' => $tID);
                        $customerID = $paydata['customerListID'];
                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['userEmail'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['fullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                    }

                    $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                   
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  "' . $result->response_reason_text . '"</strong></div>');

                }
                $transactiondata                        = array();
                if ($result->transaction_id != 0 && $result->transaction_id != '') 
                {
                    $transactiondata['transactionID']       = $result->transaction_id;
                    $transactiondata['transactionCode']     = $result->response_code;
                }else{
                    $transactiondata['transactionID']       = 'TXNFAILED'.time();
                    $transactiondata['transactionCode']     = 400;
                }
                $transactiondata['transactionStatus']   = $result->response_reason_text;
                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                $transactiondata['gatewayID']           = $gatlistval;
                $transactiondata['transactionGateway']  = $gt_result['gatewayType'];

                $transactiondata['transactionType']   = $result->transaction_type;
                $transactiondata['customerListID']    = $this->czsecurity->xssCleanPostInput('customerID');
                $transactiondata['transactionAmount'] = $result->amount;
                $transactiondata['merchantID']        = $this->session->userdata('logged_in')['merchID'];
                $transactiondata['gateway']           = "Auth";
                $transactiondata['resellerID']        = $this->resellerID;
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                if($custom_data_fields){
                    $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                }

                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');

            }
            redirect('QBO_controllers/Payments/payment_capture', 'refresh');
        }

    }

    public function Auth_Process()
    {

        $tran = new AuthorizeNetAIM('6L7z3mEakZjV', '367sbrCsKp878N4j');
        $data = $tran->authorizeOnly('20', '4111111111111111', '1217');

        echo "<pre>";
        print_r($data);
        die;

    }

    public function Void_Process()
    {

        $tran = new AuthorizeNetAIM('6L7z3mEakZjV', '367sbrCsKp878N4j');
        $data = $tran->void($tran_id);
        echo "<pre>";
        print_r($data);
        die;

    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");

        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

		$checkPlan = check_free_plan_transactions();
        $user_id = $merchantID;
        if (!empty($this->input->post(null, true))) {

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $apiloginID     = $gt_result['gatewayUsername'];
                $transactionKey = $gt_result['gatewayPassword'];

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
                $transaction->setSandbox($this->config->item('auth_test_mode'));

                $customerID	= $this->czsecurity->xssCleanPostInput('customerID');
				$payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code =     'WEB';
                
                if($payableAccount == '' || $payableAccount == 'new1') {
					$accountDetails = [
						'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
						'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
						'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'companyID'     => $companyID,
						'merchantID'   => $merchantID,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $sec_code
					];
				} else {
					$accountDetails = $this->card_model->get_single_card_data($payableAccount);
                    
				}
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$transaction->setECheck($accountDetails['routeNumber'], $accountDetails['accountNumber'], $accountDetails['accountType'], $bank_name='Wells Fargo Bank NA', $accountDetails['accountName'], $accountDetails['secCodeEntryMethod']);

                $transaction->__set('company', $this->czsecurity->xssCleanPostInput('companyName'));
                $transaction->__set('first_name', $this->czsecurity->xssCleanPostInput('fistName'));
                $transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
                $transaction->__set('address', $this->czsecurity->xssCleanPostInput('baddress'));
                $transaction->__set('country', $this->czsecurity->xssCleanPostInput('bcountry'));
                $transaction->__set('city', $this->czsecurity->xssCleanPostInput('bcity'));
                $transaction->__set('state', $this->czsecurity->xssCleanPostInput('bstate'));
                $transaction->__set('zip', $this->czsecurity->xssCleanPostInput('bzipcode'));

                $transaction->__set('ship_to_address', $this->czsecurity->xssCleanPostInput('address'));
                $transaction->__set('ship_to_country', $this->czsecurity->xssCleanPostInput('country'));
                $transaction->__set('ship_to_city', $this->czsecurity->xssCleanPostInput('city'));
                $transaction->__set('ship_to_state', $this->czsecurity->xssCleanPostInput('state'));
                $transaction->__set('ship_to_zip', $this->czsecurity->xssCleanPostInput('zipcode'));

                $transaction->__set('phone', $this->czsecurity->xssCleanPostInput('phone'));

                $transaction->__set('email', $this->czsecurity->xssCleanPostInput('email'));
                $amount = $this->czsecurity->xssCleanPostInput('totalamount');

                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                    $transaction->__set('invoice_num', $new_invoice_number);
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $transaction->__set('po_num', $this->czsecurity->xssCleanPostInput('po_number'));
                }
                
                $result = $transaction->authorizeAndCapture($amount);

                if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '') {

                    $trID = $result->transaction_id;

					$invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

					$val = array('merchantID' => $user_id);
					$tokenData = $this->general_model->get_row_data('QBO_token', $val);

					$accessToken = $tokenData['accessToken'];
					$refreshToken = $tokenData['refreshToken'];
					$realmID      = $tokenData['realmID'];

					$dataService = DataService::Configure(array(
						'auth_mode' => $this->config->item('AuthMode'),
						'ClientID'  => $this->config->item('client_id'),
						'ClientSecret' => $this->config->item('client_secret'),
						'accessTokenKey' =>  $accessToken,
						'refreshTokenKey' => $refreshToken,
						'QBORealmID' => $realmID,
						'baseUrl' => $this->config->item('QBOURL'),
					));

					$refNumber = array();
                    $merchantID = $user_id;
					if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();


							$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
							// $invse = $inID;
							$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");


							$con = array('invoiceID' => $inID, 'merchantID' => $user_id);

							$res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;
                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            $ispaid      = 0;
                            $isRun = 0;
                            $BalanceRemaining = 0.00;
                            if($amount_data == $actualInvoicePayAmount){
                                $actualInvoicePayAmount = $amount_data;
                                $isPaid      = 1;

                            }else{

                                $actualInvoicePayAmount = $actualInvoicePayAmount;
                                $isPaid      = 0;
                                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                
                            }
                            $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;
                            $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
                            $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

							if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
								$theInvoice = current($targetInvoiceArray);

                                $createPaymentObject = [
									"TotalAmt" => $actualInvoicePayAmount,
									"SyncToken" => 1,
									"CustomerRef" => $customerID,
									"Line" => [
										"LinkedTxn" => [
											"TxnId" => $inID,
											"TxnType" => "Invoice",
										],
										"Amount" => $actualInvoicePayAmount
									]
                                ];

                                $paymentMethod = $this->general_model->qbo_payment_method('Check', $this->merchantID, true);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($trID) && $trID != ''){
                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                }

								$newPaymentObj = Payment::create($createPaymentObject);

								$savedPayment = $dataService->Add($newPaymentObj);

								$error = $dataService->getLastError();
								if ($error != null) {
									$err = '';
									$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
									$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
									$err .= "The Response message is: " . $error->getResponseBody() . "\n";
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Success but " . $error->getResponseBody();

									$qbID = $trID;
									$pinv_id = '';
								} else {
									$pinv_id = '';
									$pinv_id        =  $savedPayment->Id;
									$st = '1';
									$action = 'Pay Invoice';
									$msg = "Payment Success";
									$qbID = $trID;
								}

                                $qbID = $inID;
                                $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result->transaction_id,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                

                                $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                if($syncid){
                                    $qbSyncID = 'CQ-'.$syncid;
                                    $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                                    
                                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                }
								$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
							}
                            $payIndex++;
						}
					} else {

						$crtxnID = '';
						$inID = '';
						$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
					}

                    if($payableAccount == '' || $payableAccount == 'new1') {
						$id1 = $this->card_model->process_ack_account($accountDetails);
                    }
                    if ($this->czsecurity->xssCleanPostInput('tr_checked'))
                        $chh_mail = 1;
                    else
                        $chh_mail = 0;
                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number     = '';
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['userEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['fullName'];
                    if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result->transaction_id);
					}
                   
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
                }

               
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }
            
            $receipt_data = array(
                'transaction_id' => ($result->transaction_id != 0 && $result->transaction_id != '')?$result->transaction_id:'TXNFAILED'.time(),
                'IP_address' => getClientIpAddr(),
                'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
                'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact' => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url' => 'QBO_controllers/Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header' => 'Sale',
                'checkPlan' => $checkPlan
            );
            
            $this->session->set_userdata("receipt_data",$receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('QBO_controllers/home/transation_sale_receipt',  'refresh'); 
        } else {
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please fill all details.</div>'); 		
		}
        redirect('QBO_controllers/Payments/create_customer_esale', 'refresh');

    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address

        if (!empty($this->input->post(null, true))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiloginID     = $gt_result['gatewayUsername'];
            $transactionKey = $gt_result['gatewayPassword'];

            $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
            $transaction->setSandbox($this->config->item('auth_test_mode'));

            $transaction->__set('method', 'echeck');
            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];

            $result = $transaction->credit($tID, $amount);

            if ($result->response_code == '1') {

                $c_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
              
                $merchantID    = $this->session->userdata('logged_in')['merchID'];
                $condition     = array('templateType' => '7');
                $view_data     = $this->company_model->template_data($condition);
                $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID));
                $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchantID));
                $currency      = "$";
                $subject       = $view_data['emailSubject'];
                $config_email  = $merchant_data['merchantEmail'];
                $merchant_name = $merchant_data['companyName'];
                $logo_url      = $merchant_data['merchantProfileURL'];
                $mphone        = $merchant_data['merchantContact'];
                $cur_date      = date('Y-m-d');
                $currency      = "$";
                $customer      = $c_data['fullName'];
                $cardno        = '';
                $cardno        = $paydata['transactionCard'];
                $message       = $view_data['message'];
                $message       = stripslashes(str_replace('{{ merchant_name }}', $merchant_name, $message));
                $message       = stripslashes(str_replace('{{ logo }}', "<img src='$logo_url'>", $message));
                $message       = stripslashes(str_replace('{{ transaction.amount }}', ($amount) ? ($amount) : '0.00', $message));
                $message       = stripslashes(str_replace('{{ transaction.currency_symbol }}', $currency, $message));
                $message       = stripslashes(str_replace('{{ customer.company }}', $customer, $message));
                $message       = stripslashes(str_replace('{{ merchant_email }}', $config_email, $message));
                $message       = stripslashes(str_replace('{{ transaction.transaction_date}}', $cur_date, $message));
                $message       = stripslashes(str_replace('{{ creditcard.mask_number }}', $cardno, $message));
                $message       = stripslashes(str_replace('{{invoice.currency_symbol}}', $currency, $message));

                $fromEmail = $merchant_data['merchantEmail'];
                $toEmail   = $c_data['userEmail'];
                $addCC     = $view_data['addCC'];
                $addBCC    = $view_data['addBCC'];
                $replyTo   = $view_data['replyTo'];
                $this->load->library('email');
                $email_data = array('customerID' => $customerID,
                    'merchantID'                     => $merchant_data['merchID'],
                    'emailSubject'                   => $subject,
                    'emailfrom'                      => $fromEmail,
                    'emailto'                        => $toEmail,
                    'emailcc'                        => $addCC,
                    'emailbcc'                       => $addBCC,
                    'emailreplyto'                   => $replyTo,
                    'emailMessage'                   => $message,
                    'emailsendAt'                    => date("Y-m-d H:i:s"),

                );

                $this->email->clear();
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from($fromEmail, $merchant_name);
                $this->email->to($toEmail);
                $this->email->subject($subject);
                $this->email->message($message);
                if ($this->email->send()) {
                    $this->general_model->insert_row('tbl_template_data', $email_data);
                }

               
                $this->qbo_customer_model->update_refund_payment($tID, 'AUTH');
                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result->transaction_id;
            $transactiondata['transactionStatus']   = $result->response_reason_text;
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionCode']     = $result->response_code;
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];

            $transactiondata['transactionType']   = $result->transaction_type;
            $transactiondata['customerListID']    = $customerID;
            $transactiondata['transactionAmount'] = $result->amount;
            $transactiondata['merchantID']        = $merchantID;
            $transactiondata['resellerID']        = $this->resellerID;
            $transactiondata['gateway']           = "AUTH Echeck";

            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }

            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');

        }

    }

    public function payment_evoid()
    {
        //Show a form here which collects someone's name and e-mail address
        $result = array();
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID1');

            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($tID != '' && !empty($gt_result)) {
                $apiloginID     = $gt_result['gatewayUsername'];
                $transactionKey = $gt_result['gatewayPassword'];

                $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);
                $transaction->setSandbox($this->config->item('auth_test_mode'));

                $transaction->__set('method', 'echeck');
                $customerID = $paydata['customerListID'];
                $amount = $paydata['transactionAmount'];

                $result = $transaction->void($tID);
                if ($result->response_code == '1') {

                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                    $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  "' . $result->response_reason_text . '"</strong></div>');

                }
                $transactiondata                        = array();
                $transactiondata['transactionID']       = $result->transaction_id;
                $transactiondata['transactionStatus']   = $result->response_reason_text;
                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                $transactiondata['transactionCode']     = $result->response_code;
                $transactiondata['gatewayID']           = $gatlistval;
                $transactiondata['transactionGateway']  = $gt_result['gatewayType'];

                $transactiondata['transactionType']   = $result->transaction_type;
                $transactiondata['customerListID']    = $this->czsecurity->xssCleanPostInput('customerID');
                $transactiondata['transactionAmount'] = $result->amount;
                $transactiondata['merchantID']        = $paydata['merchantID'];
                $transactiondata['gateway']           = "AUTH Echeck";
                $transactiondata['resellerID']        = $this->resellerID;
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                if($custom_data_fields){
                    $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                }

                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>');

            }

            redirect('QBO_controllers/Payments/evoid_transaction', 'refresh');
        }

    }

    /*****************Delete Payment Transaction****************/

    public function delete_qbo_transaction()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        if (!empty($this->czsecurity->xssCleanPostInput('paytxnID'))) {
            $ptxnID  = $this->czsecurity->xssCleanPostInput('paytxnID');
            if(isset($_POST['txnvoidID'])){
                $paydata = $this->general_model->get_row_data('customer_transaction', array('transactionID' => $_POST['txnvoidID'], 'merchantID' => $merchID));
            }else{
                $paydata = $this->general_model->get_row_data('customer_transaction', array('id' => $ptxnID, 'merchantID' => $merchID));
            } 
            $ptxnID = $paydata['id'];
            $transaction_id = $paydata['transactionID'];
            if (!empty($paydata)) {

                $transactionGateway = $paydata['transactionGateway'];
                $gatlistval = $paydata['gatewayID'];
                $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
    		    
                $payment_capture_page     =  ($this->czsecurity->xssCleanPostInput('payment_capture_page')) ? $this->czsecurity->xssCleanPostInput('payment_capture_page') : 0; 
                $setMailVoid     =  ($this->czsecurity->xssCleanPostInput('setMailVoid')) ? 1 : 0; 
			    $paymentType = (strrpos($paydata['gateway'], 'ECheck'))? 2 : 1;
                
                 $voidObj = new Manage_payments($merchID);
                 $voidedTransaction = $voidObj->voidTransaction([
                     'trID' => $paydata['transactionID'],
                     'gatewayID' => $gatlistval,
				     'paymentType' => $paymentType,
                 ]);
				
                
                if($voidedTransaction){
                    if ((!empty($paydata['qbListTxnID']))) {
                        $qb_txn_id = $paydata['qbListTxnID'];
    
                        $val = array(
                            'merchantID' => $merchID,
                        );
                        $data = $this->general_model->get_row_data('QBO_token', $val);
                        if (!empty($data)) {
                            $accessToken  = $data['accessToken'];
                            $refreshToken = $data['refreshToken'];
                            $realmID      = $data['realmID'];
                            $dataService  = DataService::Configure(array(
                                'auth_mode'       => $this->config->item('AuthMode'),
                                'ClientID'        => $this->config->item('client_id'),
                                'ClientSecret'    => $this->config->item('client_secret'),
                                'accessTokenKey'  => $accessToken,
                                'refreshTokenKey' => $refreshToken,
                                'QBORealmID'      => $realmID,
                                'baseUrl'         => $this->config->item('QBOURL'),
                            ));
                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
    
                            $newPaymentObj = Payment::create([
                                "Id"        => $qb_txn_id,
                                "SyncToken" => '2',
                            ]);
                            $savedPayment = $dataService->delete($newPaymentObj);
                            if ($savedPayment) {
                                $st = $this->general_model->update_row_data('customer_transaction',
                                    array('transactionID' => $paydata['transactionID'], 'merchantID' => $merchID), array('transaction_user_status' => '3', 'transactionModified' => date('Y-m-d H:i:s')));
                                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>');
                        }
                    } else {
    
                        if ($paydata['invoiceID'] == '' || $paydata['invoiceID'] == null) {
    
                            $st = $this->general_model->update_row_data('customer_transaction',
                                array('transactionID' => $paydata['transactionID'], 'merchantID' => $merchID), array('transaction_user_status' => '3', 'transactionModified' => date('Y-m-d H:i:s')));
                            if (!$st) {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed</strong></div>');
                            }
                        }
                    }
                    $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
                }

                if ($setMailVoid == '1') {
                    $customerID = $paydata['customerListID'];
                    $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchID);
                    $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
                }
                $receipt_data = array(
                    'proccess_url' => 'QBO_controllers/Payments/payment_capture',
                    'proccess_btn_text' => 'Process New Transaction',
                    'sub_header' => 'Void',
                );
                
                $this->session->set_userdata("receipt_data",$receipt_data);
                
                
                redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$transaction_id,  'refresh');


                if($payment_capture_page)
                    redirect('QBO_controllers/Payments/payment_capture','refresh');
                else
                    redirect('QBO_controllers/Payments/payment_transaction','refresh');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>');
                redirect('QBO_controllers/Payments/payment_transaction', 'refresh');
            }

        }
        redirect('QBO_controllers/Payments/payment_transaction', 'refresh');
    }

    public function pay_multi_invoice()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

		$checkPlan = check_free_plan_transactions();
        $user_id    = $merchID;
        $resellerID = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID))['resellerID'];
        $cardID  = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway = $this->czsecurity->xssCleanPostInput('gateway1');

        $cusproID = '';
        $custom_data_fields = [];
        $cusproID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

        if ($checkPlan && $cardID != "" && $gateway != "") {

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $apiloginID     = $gt_result['gatewayUsername'];
            $transactionKey = $gt_result['gatewayPassword'];

            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');

            if (!empty($invoices)) {
                foreach ($invoices as $key => $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];
                        $c_data          = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
                        $companyID       = $c_data['companyID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
                            $cardType     = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
                            
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        $error = '';

                        if (!empty($cardID)) {

                            if ($in_data['BalanceRemaining'] > 0) {
                                $cr_amount = 0;
                                $amount    = $in_data['BalanceRemaining'];
                                $amount    = $pay_amounts;
                                $cardType =  $this->general_model->getType($card_no);

                                $amount = $pay_amounts;
                                $transaction1 = new AuthorizeNetAIM($apiloginID, $transactionKey);
                                $transaction1->setSandbox($this->config->item('auth_test_mode'));

                                if (strlen($exyear) > 2) {
                                    $minLength = 4 - strlen($exyear);
                                    $exyear    = substr($exyear, $minLength);
                                }
                                
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }
                                $expry = $expmonth . $exyear;

                                $result = $transaction1->authorizeAndCapture($amount, $card_no, $expry);

                                if ($result->response_code == "1" && $result->transaction_id != 0 && $result->transaction_id != '') {

                                    $val = array(
                                        'merchantID' => $merchID,
                                    );
                                    $data = $this->general_model->get_row_data('QBO_token', $val);

                                    $accessToken  = $data['accessToken'];
                                    $refreshToken = $data['refreshToken'];
                                    $realmID      = $data['realmID'];
                                    $dataService  = DataService::Configure(array(
                                        'auth_mode'       => $this->config->item('AuthMode'),
                                        'ClientID'        => $this->config->item('client_id'),
                                        'ClientSecret'    => $this->config->item('client_secret'),
                                        'accessTokenKey'  => $accessToken,
                                        'refreshTokenKey' => $refreshToken,
                                        'QBORealmID'      => $realmID,
                                        'baseUrl'         => $this->config->item('QBOURL'),
                                    ));

                                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                                    $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

                                    if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                        $theInvoice = current($targetInvoiceArray);
                                    }
                                    
                                    $createPaymentObject = [
                                        "TotalAmt"    => $amount,
                                        "SyncToken"   => 1,
                                        "CustomerRef" => $Customer_ListID,
                                        "Line"        => [
                                            "LinkedTxn" => [
                                                "TxnId"   => $invoiceID,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount"    => $amount,
                                        ],
                                    ];

                                    $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                    if($paymentMethod){
                                        $createPaymentObject['PaymentMethodRef'] = [
                                            'value' => $paymentMethod['payment_id']
                                        ];
                                    }
                                    if(isset($result->transaction_id) && $result->transaction_id != ''){
                                        $createPaymentObject['PaymentRefNum'] = substr($result->transaction_id, 0, 21);
                                    }

                                    $newPaymentObj = Payment::create($createPaymentObject);
                                    $savedPayment = $dataService->Add($newPaymentObj);

                                    $error = $dataService->getLastError();
                                    if ($error != null) {
                                        $err = '';
                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error : </strong>' . $err . '</div>');

                                        $st     = '0';
                                        $action = 'Pay Invoice';
                                        $msg    = "Payment Success but " . $error->getResponseBody();
                                        $qbID   = $result->transaction_id;

                                    } else {
                                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');
                                        $st     = '1';
                                        $action = 'Pay Invoice';
                                        $msg    = "Payment Success";
                                        $qbID   = $result->transaction_id;
                                    }

                                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                                        $cardDetails = [
                                            'card_no'    => $card_no,
                                            'cvv'        => $cvv,
                                            'customerID' => $Customer_ListID,
                                            'expmonth'   => $expmonth,
                                            'exyear'     => $exyear,
                                            'companyID'  => $c_data['companyID'],
                                            'user_id'    => $user_id,
                                            'address1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                            'city'       => $this->czsecurity->xssCleanPostInput('city'),
                                            'state'      => $this->czsecurity->xssCleanPostInput('state'),
                                            'country'    => $this->czsecurity->xssCleanPostInput('country'),
                                            'phone'      => $this->czsecurity->xssCleanPostInput('phone'),
                                            'zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        ];

                                        save_qbo_card($cardDetails, $user_id);
                                    }

                                } else { $st = '0';
                                    $action                         = 'Pay Invoice';
                                    $msg                            = "Payment Failed";
                                    
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result->response_reason_text . '</div>');
                                }
                                $qbID                           = $invoiceID;
                                
                                $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result->transaction_id,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                if($syncid){
                                    $qbSyncID = 'CQ-'.$syncid;

                                    $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                }
                                $transactiondata                        = array();
                                if ($result->transaction_id != 0 && $result->transaction_id != '') 
                                {
                                    $transactiondata['transactionID']       = $result->transaction_id;
                                    $transactiondata['transactionCode']     = $result->response_code;
                                }else{
                                    $transactiondata['transactionID']       = 'TXNFAILED'.time();
                                    $transactiondata['transactionCode']     = 400;
                                }
                                $transactiondata['transactionStatus']   = $result->response_reason_text;
                                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                                $transactiondata['transactionCard']     = substr($result->account_number, 4);
                                if ($error == null) {
                                    $transaction['qbListTxnID'] = $savedPayment->Id;
                                }
                                $transactiondata['transactionType']    = $result->transaction_type;
                                $transactiondata['gatewayID']          = $gateway;
                                $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                                $transactiondata['customerListID']     = $in_data['CustomerListID'];
                                $transactiondata['transactionAmount']  = $result->amount;
                                $transactiondata['merchantID']         = $merchID;
                                $transactiondata['invoiceID']          = $invoiceID;
                                $transactiondata['resellerID']         = $this->resellerID;
                                $transactiondata['gateway']            = "Auth";
                                $CallCampaign = $this->general_model->triggerCampaign($merchID,$transactiondata['transactionCode']);
                                if(!empty($this->transactionByUser)){
                                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                }
                                if($custom_data_fields){
                                    $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                                }

                                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

                            } else {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>');
                            }

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if(!$checkPlan){
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

}
