<?php

/**
 *create_customer_sale
 * Pay_invoice
 * Create_customer_refund
 * create_customer_capture
 * multiple invoice payment 
 * 

 */

 use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

class PaypalPayment extends CI_Controller
{
    
    private  $resellerID;
	private $transactionByUser;
    
	public function __construct()
	{
		parent::__construct();
	   
	    include APPPATH . 'third_party/PayPalAPINEW.php';
			$this->load->config('paypal');
		
	    $this->load->model('quickbooks');
		$this->load->model('card_model');
	   
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_customer_model');
		$this->load->model('company_model');
     $this->db1 = $this->load->database('otherdb', TRUE);
	  if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
			$logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $logged_in_data['merchantID'];
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	    
	
	   	redirect('QBO_controllers/home','refresh'); 
	}
	
	
	  
	  
	   public function create_customer_sale()
	   {
		   
			$this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");
			$checkPlan = check_free_plan_transactions();
				
		   if($checkPlan && !empty($this->input->post(null, true)))
		   {	
		   			$custom_data_fields = [];
		   			$applySurcharge = false;
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$applySurcharge = true;
						$custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
					}

					if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
						$custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
					}

					$inv_array=array();
				 if($this->session->userdata('logged_in')){
					$user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
				if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 	
				
				$country  = "US";
				
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  	$resellerID = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=> $merchantID))['resellerID'];
			   if($gatlistval !="" && !empty($gt_result) )
			   {
					
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);			
					
					
					$customerID =  $this->czsecurity->xssCleanPostInput('customerID');
      
					$comp_data  = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
				    $companyID  = $comp_data['companyID'];
                 
                   	  if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
					    $card_no 	= $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth  	= $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv        = $this->czsecurity->xssCleanPostInput('cvv');
				  }else {
					  
						    $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
					    	$expmonth =  $card_data['cardMonth']; 
							$exyear   = $card_data['cardYear'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$cvv     = $card_data['CardCVV'];
					}
					/*Added card type in transaction table*/
	                $cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;				
							 
					$paymentType 		=	'Sale';
					$companyName        = $this->czsecurity->xssCleanPostInput('companyName');
					$firstName 			= $this->czsecurity->xssCleanPostInput('fistName');
					$lastName 			= $this->czsecurity->xssCleanPostInput('lastName');
					$creditCardType 	= 'Visa';
					$creditCardNumber 	= $card_no;
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                    
					$expDateYear=  	$exyear;
					$cvv2Number =   $cvv;
					$address1	=   $this->czsecurity->xssCleanPostInput('address');
					$address2 	= '';
					$city 		=   $this->czsecurity->xssCleanPostInput('city');
					$state 		= $this->czsecurity->xssCleanPostInput('state');
				$zipcode=	$zip 		= $this->czsecurity->xssCleanPostInput('zipcode');
					$email      = $this->czsecurity->xssCleanPostInput('email');
					$phone      = $this->czsecurity->xssCleanPostInput('phone');
					 $currencyID = 'USD';
					if($this->czsecurity->xssCleanPostInput('country')=="United States" ||$this->czsecurity->xssCleanPostInput('country')=="United State"){
					  $country 	= 'US';	// US or other valid country code
					  $currencyID = 'USD';
					}
					
					if($this->czsecurity->xssCleanPostInput('country')=="Canada" ||$this->czsecurity->xssCleanPostInput('country')=="canada"){
					  $country 	  = 'CAD';	// US or other valid country code
					  $currencyID = 'CAD';
					}
					
					$amount 	= $this->czsecurity->xssCleanPostInput('totalamount');	//actual amount should be substituted here
					// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
					// update amount with surcharge 
	                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
	                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
	                    $amount += round($surchargeAmount, 2);
	                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
	                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
	                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
	                    
	                }
	                $totalamount  = $amount;  
                    
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							// 'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);

						$cvv2Number = trim($cvv2Number);
						if($cvv2Number && !empty($cvv2Number)){
							$CCDetails['cvv2'] = $cvv2Number;
						}
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);

		                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
		                	$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                    		
		                	$PaymentDetails['invnum'] = $new_invoice_number;
		                }

		                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {

		                	$PaymentDetails['custom'] = 'PO Number: '.$this->czsecurity->xssCleanPostInput('po_number');
		                }

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
									
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				
					
					if(!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))) 
					{
								
					    	     $tranID ='' ;$amt='0.00';
        					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt =$PayPalResult["AMT"];  }
					    
					        	$code = '111'; 
					        	$tranID = $trID = $PayPalResult['TRANSACTIONID']; 
								$invoiceIDs=array();
								$invoicePayAmounts = array();
            				   if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
            				   {
            				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
									$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
								}
				   
				             	$val = array('merchantID' => $merchantID);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			          
				           if(!empty($invoiceIDs))
				           {
				           		$payIndex = 0;
				           		$saleAmountRemaining = $amount;
		                        foreach ($invoiceIDs as $inID) {
		                            $theInvoice = array();
		                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
		                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

		                            $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
		                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

		                            
		                            $refNumber[]   =  $res1['refNumber'];
		                            $txnID      = $inID;
		                            $pay_amounts = 0;


		                            $amount_data = $res1['BalanceRemaining'];
		                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
		                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

		                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
		                                $actualInvoicePayAmount += $surchargeAmount;
		                                $amount_data += $surchargeAmount;
		                                $updatedInvoiceData = [
		                                    'inID' => $inID,
		                                    'targetInvoiceArray' => $targetInvoiceArray,
		                                    'merchantID' => $user_id,
		                                    'dataService' => $dataService,
		                                    'realmID' => $realmID,
		                                    'accessToken' => $accessToken,
		                                    'refreshToken' => $refreshToken,
		                                    'amount' => $surchargeAmount,
		                                ];
		                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
		                            }

		                            $ispaid      = 0;
		                            $isRun = 0;
		                            if($saleAmountRemaining > 0){
		                                $BalanceRemaining = 0.00;
		                                if($amount_data == $actualInvoicePayAmount){
		                                    $actualInvoicePayAmount = $amount_data;
		                                    $isPaid      = 1;

		                                }else{

		                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
		                                    $isPaid      = 0;
		                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
		                                    
		                                }
		                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

		                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

		                                
		                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

		                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
		                                    $theInvoice = current($targetInvoiceArray);

											$createPaymentObject = [
		                                        "TotalAmt" => $actualInvoicePayAmount,
		                                        "SyncToken" => 1,
		                                        "CustomerRef" => $customerID,
		                                        "Line" => [
		                                            "LinkedTxn" => [
		                                                "TxnId" => $inID,
		                                                "TxnType" => "Invoice",
		                                            ],
		                                            "Amount" => $actualInvoicePayAmount
		                                        ]
		                                    ];
					
											$paymentMethod = $this->general_model->qbo_payment_method($creditCardType, $this->merchantID, false);
											if($paymentMethod){
												$createPaymentObject['PaymentMethodRef'] = [
													'value' => $paymentMethod['payment_id']
												];
											}
											if(isset($trID) && $trID != '')
					                        {
					                            $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
					                        }
											$newPaymentObj = Payment::create($createPaymentObject);

		                                    $savedPayment = $dataService->Add($newPaymentObj);

		                                    $error = $dataService->getLastError();
		                                    if ($error != null) {
		                                        $err = '';
		                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
		                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
		                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
		                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

		                                        $st = '0';
		                                        $action = 'Pay Invoice';
		                                        $msg = "Payment Success but " . $error->getResponseBody();
		                                        $qbID = $trID;
		                                        $pinv_id = '';
		                                    } else {
		                                        $pinv_id = '';
		                                        $crtxnID = $pinv_id = $savedPayment->Id;
		                                        $st = '1';
		                                        $action = 'Pay Invoice';
		                                        $msg = "Payment Success";
		                                        $qbID = $trID;
		                                        
		                                    }
		                                    $qbID = $inID;
		                                    $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

		                                    $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
		                                    if($syncid){
		                                        $qbSyncID = 'CQ-'.$syncid;

		                                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

		                                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
		                                    }
		                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
		                                    $transactiondata= array();
                    				       	$transactiondata['transactionID']       = $tranID;
                    					   	$transactiondata['transactionStatus']   = $PayPalResult["ACK"];
                    					   	$transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
                    					    $transactiondata['transactionModified']= date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));
                    					   	$transactiondata['transactionCode']     = $code; 
                    						$transactiondata['transactionType']    = "Paypal_sale";	
                    						$transactiondata['gatewayID']          = $gatlistval;
                                           	$transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
											$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                    					   	$transactiondata['customerListID']      = $customerID;
                    					   	$transactiondata['transactionAmount']   = $actualInvoicePayAmount;
                    					   	$transactiondata['merchantID']        = $merchantID;
                    					   	$transactiondata['resellerID']        = $this->resellerID;
                    					   	$transactiondata['gateway']           = "Paypal";
                    					    $transactiondata['invoiceID']      = $inID;
                    					     
                    					     $transactiondata['qbListTxnID']  = $p_id;
                    				        if($custom_data_fields){
				                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
				                            }
                    					   	$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

											if(!empty($this->transactionByUser)){
											    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
											    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
											}
											if($custom_data_fields){
					                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
					                        }
                    				       	$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		                                }
		                            }
		                            $payIndex++;

		                        }

				          }
    				          else
    						  {
						      $transactiondata= array();
						      	 $tranID ='' ;$amt='0.00';
        					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt =$PayPalResult["AMT"];  }
        					   
        				       $transactiondata['transactionID']       = $tranID;
        					   $transactiondata['transactionStatus']    = $PayPalResult["ACK"];
        					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
        					    $transactiondata['transactionModified']= date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));
        					   $transactiondata['transactionCode']     = $code;  
							   $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
        						$transactiondata['transactionType']    = "Paypal_sale";	
        						$transactiondata['gatewayID']          = $gatlistval;
                               $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;					
        					   $transactiondata['customerListID']      = $customerID;
        					   $transactiondata['transactionAmount']   = $amt;
        					   $transactiondata['merchantID']   = $merchantID;
        					   $transactiondata['resellerID']   = $this->resellerID;
        					   $transactiondata['gateway']   = "Paypal";
        					   if($custom_data_fields){
	                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
	                            }
        					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

								if(!empty($this->transactionByUser)){
								    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
								    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
								}
								if($custom_data_fields){
		                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
		                        }
        				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
						      
						  } 
						
						
						
						  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    	{
                    				 		$card_type      =$this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                    				        $friendlyname   =  $card_type.' - '.substr($this->czsecurity->xssCleanPostInput('card_number'),-4);
                    						$card_condition = array(
                    										 'customerListID' => $customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    					
                    						$crdata =	$this->card_model->chk_card_firendly_name( $customerID,$friendlyname)	;			
                    					     
                    					   
                    						if($crdata >0)
                    						{
                    							
                    						   $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	     =>$exyear,
                    										  'companyID'    =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										  'updatedAt'    => date("Y-m-d H:i:s"),
                    										   'Billing_Addr1'	 =>$address1,
                        										  'Billing_Addr2'	 =>$address2,
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										  );
                    										  
                    							     		  
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					     	$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										 'customerListID' => $customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    					
                    								
                    				            $id1 =    $this->card_model->insert_card_data($card_data);	
                    				            
                    				           
                    				            
                    						
                    						}
                    				
                    				 }
					if($chh_mail =='1')
					{
					   
					  	$condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID);
					  	if (!empty($refNumber)) {
                            $ref_number = implode(',', $refNumber);
                        } else {
                            $ref_number = '';
                        } 
					  	
					  	$tr_date   =date('Y-m-d H:i:s');
					  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
					 
					   	$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
					 }  
					$this->session->set_flashdata('success', 'Transaction Successful');
 
				} 
				else
				{
					$code='401';
					
						if(!empty( $PayPalResult["RAWRESPONSE"]))
					{
				
					 $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
				       $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt =$PayPalResult["AMT"];  }
					   $transactiondata= array();
				       $transactiondata['transactionID']       = $tranID;
					   $transactiondata['transactionStatus']    = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));
					   $transactiondata['transactionCode']     = $code;  
					    $transactiondata['gateway']   = "Paypal";
						$transactiondata['transactionType']    = "Paypal_sale";	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $amt;
					   $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID;
					 	if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);	
					   $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					} 
				}	
					    
					
			   }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 
				}		
		 
              
		   }
		   $invoice_IDs = array();
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				}
			
				$receipt_data = array(
					'transaction_id' => isset($PayPalResult['TRANSACTIONID'])?$PayPalResult['TRANSACTIONID']:'TXNFAILED'.time(),
					'IP_address' => getClientIpAddr(),
					'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
					'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
					'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
					'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
					'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
					'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
					'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
					'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
					'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
					'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
					'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
					'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
					'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
					'Contact' => $this->czsecurity->xssCleanPostInput('email'),
					'proccess_url' => 'QBO_controllers/Payments/create_customer_sale',
					'proccess_btn_text' => 'Process New Sale',
					'sub_header' => 'Sale',
					'checkPlan'	=> $checkPlan
				);
				
				$this->session->set_userdata("receipt_data",$receipt_data);
				$this->session->set_userdata("invoice_IDs",$invoice_IDs);
				
				
				redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
		   
		   
	   }
	
	
	
	  
	   public function create_customer_auth()
	   {
			$this->session->unset_userdata("receipt_data");
			$this->session->unset_userdata("invoice_IDs");
		   	 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					
				$checkPlan = check_free_plan_transactions();
		   	
		   	   if($checkPlan && !empty($this->input->post(null, true))){
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			    $custom_data_fields = [];
			    $po_number = $this->czsecurity->xssCleanPostInput('po_number');
	            if (!empty($po_number)) {
	                $custom_data_fields['po_number'] = $po_number;
	            }
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
				}

				if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
					$custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
				}
			   if($gatlistval !="" && !empty($gt_result) )
			   {
					$username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					$option = array('API_UserName' => $username,
						'API_Password'       => $password,
						'API_Signature'      => $signature,
						'API_Endpoint'       => "https://api-3t.paypal.com/nvp",
						'envoironment'       => 'sandbox',
						'version'            => '123');
					
				
			
				$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID'=>$merchantID));
				$companyID  = $comp_data['companyID'];	

                   	  if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
					    $card_no 	= $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth  	= $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv        = $this->czsecurity->xssCleanPostInput('cvv');
				  }else {
					  
						    $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
					    	$expmonth =  $card_data['cardMonth']; 
							$exyear   = $card_data['cardYear'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$cvv     = $card_data['CardCVV'];
					}
					$cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;				
							 
					$paymentType 		=	'Authorization';
					$companyName        = urlencode($this->czsecurity->xssCleanPostInput('companyName'));
					$firstName 			= urlencode($this->czsecurity->xssCleanPostInput('firstName'));
					$lastName 			= urlencode($this->czsecurity->xssCleanPostInput('lastName'));
					$creditCardType 	= urlencode('Visa');
					$creditCardNumber 	= urlencode($card_no);
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));

					$expDateYear= urlencode($exyear);
					$cvv2Number = urlencode($cvv);
					$address1	= urlencode($this->czsecurity->xssCleanPostInput('address'));
					$address2 	= '';
					$city 		= urlencode($this->czsecurity->xssCleanPostInput('city'));
					$state 		= urlencode($this->czsecurity->xssCleanPostInput('state'));
					$zipcode=	$zip 		= $this->czsecurity->xssCleanPostInput('zipcode');
			     	 $country 	= 'US';	
					if(strtoupper($this->czsecurity->xssCleanPostInput('country'))=="UNITED STATES" || strtoupper($this->czsecurity->xssCleanPostInput('country'))=="UNITED STATE"){
					  $country 	= 'US';	// US or other valid country code
					}
					
					if($this->czsecurity->xssCleanPostInput('country')=="Canada" ||$this->czsecurity->xssCleanPostInput('country')=="canada"){
					  $country 	= 'CAD';	// US or other valid country code
					}
					
					$amount 	= $this->czsecurity->xssCleanPostInput('totalamount');	//actual amount should be substituted here
					$currencyID = 'USD';// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
					$invnum = '';
                    $custom = '';
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                        $new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_number');
                        
                        $invnum = $new_invoice_number;
                    }

                    if (!empty($po_number)) {
                        $custom = 'PO Number: '.$po_number;
                    }  
								   
					// Add request-specific fields to the request string.
					$nvpStr =	"&PAYMENTACTION=$paymentType&AMT=$amount&invnum=$invnum&custom=$custom&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
								"&EXPDATE=$padDateMonth$expDateYear&FIRSTNAME=$firstName&LASTNAME=$lastName&COMPANYNAME=$companyName".
								"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";

					$cvv2Number = trim($cvv2Number);
					if($cvv2Number && !empty($cvv2Number)){
						$nvpStr .= "&CVV2=$cvv2Number";
					}

					// Execute the API operation; see the PPHttpPost function above.
						// Set up your API credentials, PayPal end point, and API version.
						
					$paypal = new PayPalAPINEW($option);
					
					
					$httpParsedResponseAr = $paypal ->PPHttpPost('DoDirectPayment', $nvpStr, $option);
				
				  
					if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
						
							$code = '111'; 
					  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    				     {
                    				 		$card_type      =$this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                    				        $friendlyname   =  $card_type.' - '.substr($this->czsecurity->xssCleanPostInput('card_number'),-4);
                    						$card_condition = array(
                    										 'customerListID' => $customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    					
                    						$crdata =	$this->card_model->chk_card_firendly_name( $customerID,$friendlyname)	;			
                    					     
                    					   
                    						if($crdata >0)
                    						{
                    							
                    						   $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	     =>$exyear,
                    										  'companyID'    =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'',
                    										  'updatedAt'    => date("Y-m-d H:i:s"),
                    										   'Billing_Addr1'	 =>$address1,
                        										  'Billing_Addr2'	 =>$address2,
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										  );
                    										  
                    							     		  
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					     	$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										 'customerListID' => $customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    					
                    								
                    				            $id1 =    $this->card_model->insert_card_data($card_data);	
                    				            
                    				           
                    				            
                    						
                    						}
                    						$custom_data_fields['card_type'] = $cardType;
                    				
                    				 }

				 $this->session->set_flashdata('success', 'Transaction Successful');
 
				}
				else{
					 $code='401';
					 $responsetext= urldecode($httpParsedResponseAr['L_LONGMESSAGE0']);
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
					
					}
					  $transactiondata= array();
					  	 $tranID ='' ;$amt='0.00';
					     if(isset($httpParsedResponseAr['TRANSACTIONID'])) { $tranID = $httpParsedResponseAr['TRANSACTIONID'];   $amt = urldecode($httpParsedResponseAr["AMT"]);  }
				       $transactiondata['transactionID']       =  $tranID;
					   $transactiondata['transactionStatus']    = $httpParsedResponseAr["ACK"];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime(urldecode($httpParsedResponseAr["TIMESTAMP"])));  
					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s',strtotime($httpParsedResponseAr["TIMESTAMP"]));
					   $transactiondata['transactionCode']     = $code; 
					    $transactiondata['transaction_user_status']     = '5' ;
						$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
						$transactiondata['transactionType']    = "Paypal_auth";	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   =  $amt;
					   $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paypal";
					    if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				       if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
		                    $transactiondata['custom_data_fields'] = json_encode($custom_data_fields);
		                }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		   }else{
	     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Authentication failed.</div>');
	     	
        
		 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 
			
        
				
		}			   
			  
			  $invoice_IDs = array();
			
		  
			  $receipt_data = array(
				  'transaction_id' => $httpParsedResponseAr['TRANSACTIONID'],
				  'IP_address' => getClientIpAddr(),
				  'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				  'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				  'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				  'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				  'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				  'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				  'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				  'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				  'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				  'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				  'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				  'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				  'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				  'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				  'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				  'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				  'proccess_url' => 'QBO_controllers/Payments/create_customer_auth',
				  'proccess_btn_text' => 'Process New Transaction',
				  'sub_header' => 'Authorize',
				  'checkPlan'	=> $checkPlan
			  );
			  
			  $this->session->set_userdata("receipt_data",$receipt_data);
			  $this->session->set_userdata("invoice_IDs",$invoice_IDs);
			  
			  
			  redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');   

	}		 

	public function create_customer_refund()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		//Show a form here which collects someone's name and e-mail address
		 	 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
	
		if(!empty($this->input->post(null, true))){
			     
					 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					   
				 
			     $tID     = $this->czsecurity->xssCleanPostInput('txnpaypalID');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			    
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
		
			       
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);	
						
    		
				 $customerID = $paydata['customerListID'];
				
				    $amount     =  $paydata['transactionAmount']; 
				
				 	 $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
				 	
				 	 if($amount==$total)
				 	 {
				 	    $restype="Full" ;
				 	 }else{
				 	    $restype="Partial";  
				 	 }
                 $amount     = $total ;
				 
			 
                   	$RTFields = array(
					'transactionid' => $tID, 							// Required.  PayPal transaction ID for the order you're refunding.
					'payerid' => '', 								// Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
					'invoiceid' => '', 								// Your own invoice tracking number.
					'refundtype' => $restype, 							// Required.  Type of refund.  Must be Full, Partial, or Other.
					'amt' =>  $amount, 									// Refund Amt.  Required if refund type is Partial.  
					'currencycode' => '', 							// Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
					'note' => '',  									// Custom memo about the refund.  255 char max.
					'retryuntil' => '', 							// Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
					'refundsource' => '', 							// Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
					'merchantstoredetail' => '', 					// Information about the merchant store.
					'refundadvice' => '', 							// Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
					'refunditemdetails' => '', 						// Details about the individual items to be returned.
					'msgsubid' => '', 								// A message ID used for idempotence to uniquely identify a message.
					'storeid' => '', 								// ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
					'terminalid' => ''								// ID of the terminal.  50 char max.
				);	
				
		$PayPalRequestData = array('RTFields' => $RTFields);
		
		$PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);
				
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
				{  
				     
				$code = '111';
			  
			    $c_data               =  $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));

			 
			   
			   
				 if(!empty($paydata['invoiceID']))
				{
			 
				$val = array(
					'merchantID' => $paydata['merchantID'],
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
            			'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			   $item_data = $this->qbo_customer_model->get_merc_invoice_item_data($paydata['invoiceID'],$paydata['merchantID']);
                            

                    if (!empty($item_data)) {
                        $lineArray = array();
                               
							$i = 0;
							for ($i = 0; $i < count($item_data); $i++) {
								$LineObj = Line::create([
									"Amount"              => $item_data[$i]['itemQty']*$item_data[$i]['itemPrice'],
									"DetailType"          => "SalesItemLineDetail",
									"Description" => $item_data[$i]['itemDescription'],
									"SalesItemLineDetail" => [
										"ItemRef" => $item_data[$i]['itemID'],
										"Qty" => $item_data[$i]['itemQty'],
										"UnitPrice" => $item_data[$i]['itemPrice'],
										"TaxCodeRef" => [
											"value" => (isset($item_data[$i]['itemTax']) && $item_data[$i]['itemTax']) ? "TAX" : "NON",
										]
									],
			
								]);
								$lineArray[] = $LineObj;
							}
							$acc_id = '';
							$acc_name = '';
    			
					  $invoice_tax = $this->qbo_customer_model->get_merc_invoice_tax_data($paydata['invoiceID'],$paydata['merchantID']);
					  $acc_data = $this->general_model->get_select_data('QBO_accounts_list', array('accountID'), array('accountName' => 'Undeposited Funds', 'merchantID' => $paydata['merchantID']));
						if (!empty($acc_data)) {
							$ac_value = $acc_data['accountID'];
						} else {
							$ac_value = '4';
						}
    		   	$theResourceObj = RefundReceipt::create([
        		    "CustomerRef" => $paydata['customerListID'], 
		            "Line" => $lineArray,
		          	"DepositToAccountRef" =>  [
    			        "value" => $ac_value,
    			        "name"=> "Undeposited Funds",
					  ],
					  "TxnTaxDetail" => [
						"TxnTaxCodeRef" => [
							"value" => (isset($invoice_tax['taxID']) && $invoice_tax['taxID']) ? $invoice_tax['taxID'] : "",
						],
					],
		      ]);	  
    		$resultingObj = $dataService->Add($theResourceObj);
			$error = $dataService->getLastError();	  
			 
			 
					
					if ($error != null)
				 {
					 
				
                      	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund '.$error.'</strong></div>'); 
                      		redirect('QBO_controllers/Payments/payment_transaction','refresh');
                  }
                   $ins_id = $resultingObj->Id;
                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
				  	          
				  	           );	
				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
			   }	
             }
             else{
                         
                         $ins_id = '';
                            $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
        				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
        				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
        				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
        				  	          
        				  	           );	
               
        				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                     }
				     

              
					
				     
				  $this->qbo_customer_model->update_refund_payment($tID, 'PAYPAL');    
					 
		
			      
			        	 $this->session->set_flashdata('success', 'Successfully Refunded Payment');
 
 
				 }else{
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					  $code = '401';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
				 }
				       $transactiondata= array();
				       	 $tranID ='' ;$amt='0.00';
					      if(isset($PayPalResult['REFUNDTRANSACTIONID'])) { $tranID = $PayPalResult['REFUNDTRANSACTIONID'];   $amt =$PayPalResult['GROSSREFUNDAMT'];  }
				       $transactiondata['transactionID']      = $tranID;
					   $transactiondata['transactionStatus']  = $PayPalResult['ACK'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));
					   $transactiondata['transactionType']    = 'Paypal_refund' ;
					    $transactiondata['transactionCode']   = $code;
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amt ;
					   $transactiondata['merchantID']  = $merchantID ;
					   $transactiondata['resellerID']   = $this->resellerID;
					   
					    if(!empty($paydata['invoiceID']))
        			     	{
        				      $transactiondata['invoiceID']  = $paydata['invoiceID'];
        			     	}
					   $transactiondata['gateway']   = "Paypal";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					   if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				
			
				
    
                       
      }
          else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Authentication failed.</div>'); 
	    		redirect('QBO_controllers/Payments/payment_transaction','refresh');
		}	    
				if(!empty($this->czsecurity->xssCleanPostInput('payrefund')))
		        {
					$invoice_IDs = array();
					
				
					$receipt_data = array(
						'proccess_url' => 'QBO_controllers/Payments/payment_refund',
						'proccess_btn_text' => 'Process New Refund',
						'sub_header' => 'Refund',
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					if($paydata['invoiceTxnID'] == ''){
						$paydata['invoiceTxnID'] ='null';
						}
						if($paydata['customerListID'] == ''){
							$paydata['customerListID'] ='null';
						}
						if($PayPalResult['REFUNDTRANSACTIONID']== ''){
							$PayPalResult['REFUNDTRANSACTIONID'] ='null';
						}
					
					redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$PayPalResult['REFUNDTRANSACTIONID'],  'refresh');	

	        	} else {		
				 
				redirect('QBO_controllers/Payments/payment_transaction','refresh');
				
	        	} 
			
			
				
	}
	
	
	/*****************Capture Transaction***************/
	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
		    
		    
		          	 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					
		       
				 $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnID');
    			 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			   if( $paydata['gatewayID'] > 0){ 
			      
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			        if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
			  
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	

			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      
				$DCFields = array(
						'authorizationid' => $tID, 				// Required. The authorization identification number of the payment you want to capture. This is the transaction ID returned from DoExpressCheckoutPayment or DoDirectPayment.
						'amt' =>  $amount , 							// Required. Must have two decimal places.  Decimal separator must be a period (.) and optional thousands separator must be a comma (,)
						'completetype' => 'Complete', 					// Required.  The value Complete indiciates that this is the last capture you intend to make.  The value NotComplete indicates that you intend to make additional captures.
						'currencycode' => '', 					// Three-character currency code
						'invnum' => '', 						// Your invoice number
						'note' => '', 							// Informational note about this setlement that is displayed to the buyer in an email and in his transaction history.  255 character max.
						'softdescriptor' => '', 				// Per transaction description of the payment that is passed to the customer's credit card statement.
						'storeid' => '', 						// ID of the merchant store.  This field is required for point-of-sale transactions.  Max: 50 char
						'terminalid' => ''						// ID of the terminal.  50 char max.  
					);
					
		$PayPalRequestData = array('DCFields' => $DCFields);
			
		$PayPalResult = $this->paypal_pro->DoCapture($PayPalRequestData);
		
			
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
					$customerID = $paydata['customerListID'];
					$tr_date   =date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
					if($chh_mail =='1')
                            {
                               
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
							
			        
			        	 $this->session->set_flashdata('success', 'Successfully Captured Authorization');
 
			        
				 }else{
					 $code = '401';
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
				     
					 }
					 
					   $transactiondata= array();
					    $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt = $PayPalResult['AMT'];  }
					   
				       $transactiondata['transactionID']      =$tranID ;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult["TIMESTAMP"]));  
					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));
					   $transactiondata['transactionType']    = 'Paypal_capture';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  =$amt;
					    $transactiondata['merchantID']        =  $merchantID;
					    $transactiondata['resellerID']        = $this->resellerID;
					   $transactiondata['gateway']            = "Paypal";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']); 
					   if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				     
					 }	
					 $invoice_IDs = array();
					
					 $receipt_data = array(
						 'proccess_url' => 'QBO_controllers/Payments/payment_capture',
						 'proccess_btn_text' => 'Process New Transaction',
						 'sub_header' => 'Capture',
					 );
					 
					 $this->session->set_userdata("receipt_data",$receipt_data);
					 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					 if($paydata['invoiceTxnID'] == ''){
						 $paydata['invoiceTxnID'] ='null';
						 }
						 if($paydata['customerListID'] == ''){
							 $paydata['customerListID'] ='null';
						 }
						 if($PayPalResult['TRANSACTIONID']== ''){
							 $PayPalResult['TRANSACTIONID'] ='null';
						 }
					 
					 redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$PayPalResult['TRANSACTIONID'],  'refresh');
        }
              
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	
	
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if(!empty($this->input->post(null, true))){
		    
		        
		        	 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					
			
    			  $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnvoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				  $gatlistval = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
			   
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
					
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      
				$DVFields = array(
						'authorizationid' => $tID, 				// Required.  The value of the original authorization ID returned by PayPal.  NOTE:  If voiding a transaction that has been reauthorized, use the ID from the original authorization, not the reauth.
						'note' => 'This test void',  							// An information note about this void that is displayed to the payer in an email and in his transaction history.  255 char max.
						'msgsubid' => ''						// A message ID used for idempotence to uniquely identify a message.
					);	
								
					$PayPalRequestData = array('DVFields' => $DVFields);
					$PayPalResult = $this->paypal_pro->DoVoid($PayPalRequestData);
		
		
			 
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
					
			        
			         $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
 
 
				 }else{
					  $code = '401';
				   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
				
				 }
				       $transactiondata= array();
				         $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['AUTHORIZATIONID'])) { $tranID = $PayPalResult['AUTHORIZATIONID'];    }
				       
				       $transactiondata['transactionID']      = $tranID;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					   $transactiondata['transactionModified']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					   $transactiondata['transactionType']    = 'Paypal_void';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $paydata['transactionAmount'];
					   $transactiondata['merchantID']  = $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paypal";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			
		
				
					redirect('QBO_controllers/Payments/payment_capture','refresh');
		}     
			
	}
	
	
	
	
	
	
	 	 
public function pay_invoice()
{
	$this->session->unset_userdata("receipt_data");
	$this->session->unset_userdata("invoice_IDs");
	$this->session->unset_userdata("in_data");
       if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
			        	$resellerID = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=> $merchID))['resellerID'];
	     
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		$checkPlan = check_free_plan_transactions();
		 
		  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
		  if (!$cardID || empty($cardID)) {
		  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
		  }
		  $custom_data_fields = [];
  
		  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
		  if (!$gatlistval || empty($gatlistval)) {
		  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
		  }
		  $gateway = $gatlistval;		
		   $cusproID='';
       $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
        if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
         $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
	   
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
         
				   if($checkPlan && $cardID!="" || $gateway!=""){  
					 
				   
					if(!empty($in_data))
					{ 
					
						$customerID = $in_data['CustomerListID'];
                        $c_data   = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), array('Customer_ListID'=>$customerID,'merchantID'=>$merchID));
			            $companyID = $c_data['companyID'];		 
    		   
           if($cardID=='new1')
           {
				$cardID_upd  =$cardID;
				$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
				$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
				$cardType       = $this->general_model->getType($card_no);
				$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				
           	}
	        else{
	            $card_data    =   $this->card_model->get_single_card_data($cardID); 
	            $card_no  = $card_data['CardNo'];
	           	$cvv      =  $card_data['CardCVV'];
				$expmonth =  $card_data['cardMonth'];
				$exyear   = $card_data['cardYear'];
	            
	        }
	        $cardType = $this->general_model->getType($card_no);
            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
            $custom_data_fields['payment_type'] = $friendlyname;
        
        
		     
			 
	
		 if(!empty($cardID))
		 {
			       
			        	$val = array(
				      	'merchantID' => $merchID,
			      	   );
    				$data = $this->general_model->get_row_data('QBO_token',$val);
    				
    				
    
    				$accessToken = $data['accessToken'];
    				$refreshToken = $data['refreshToken'];
    				 $realmID      = $data['realmID']; 
    				$dataService = DataService::Configure(array(
                		'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
    				));
    		
    			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
    	            
    	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
					if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
                	{
			       
			       
			       
			       
			       
							
			        	if( $in_data['BalanceRemaining'] > 0)
			        	{
					        $amount 	    =	 $in_data['BalanceRemaining'];
						
							$amount           = $this->czsecurity->xssCleanPostInput('inv_amount');
							
						$theInvoice = current($targetInvoiceArray);	   
					
				
					    	$error = $dataService->getLastError();
			              if ($error != null) 
			              {	
						   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error in invoice payment process</strong></div>');
                           redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
						   
			              }
						   
							
							
								$expDateMonth 		= $expmonth;
								$creditCardNumber 	= $card_no;
									$expDateYear=  	$exyear;
									$cvv2Number  = $cvv;
						   $creditCardType    = 'Visa';
								// Month must be padded with leading zero
						$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
							$currencyID     = "USD";
						  
                           
                            $firstName = $in_data['firstName'];
                            $lastName =  $in_data['lastName']; 
                            $companyName =  $in_data['companyName']; 
							$address1 = $in_data['ShipAddress_Addr1']; 
                            $address2 = $in_data['ShipAddress_Addr2']; 
							$country  = $in_data['ShipAddress_Country']; 
							$city     = $in_data['ShipAddress_City'];
							$state    = $in_data['ShipAddress_State'];		
							$zipcode=	$zip  = $in_data['ShipAddress_PostalCode']; 
								$phone = $in_data['phoneNumber']; 
								$email = $in_data['userEmail']; 
										
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							// 'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);

						$cvv2Number = trim($cvv2Number);
						if($cvv2Number && !empty($cvv2Number)){
							$CCDetails['cvv2'] = $cvv2Number;
						}
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
									
									
									
						
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				        
			$inID ='';
			    $crtxnID = '';	
			    
		 $error = '';    
			   
					 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
					 {  
					
					           $code = '111';
						 $txnID      = $in_data['invoiceID'];  
						 $ispaid 	 = 'true';
						
						 	$tranID = $PayPalResult['TRANSACTIONID'];  
						
					
				$val = array(
					'merchantID' => $merchID,
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
            				'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
	            
	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
				
				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
					$theInvoice = current($targetInvoiceArray);
				}
				
				$createPaymentObject = [
				    "TotalAmt" => $amount,
				    "SyncToken" => 1,
				    "CustomerRef" => $customerID,
				    "Line" => [
				     "LinkedTxn" =>[
				            "TxnId" => $invoiceID,
				            "TxnType" => "Invoice",
				        ],    
				       "Amount" => $amount
			        ]
				];

				$paymentMethod = $this->general_model->qbo_payment_method($creditCardType, $this->merchantID, false);
				if($paymentMethod){
					$createPaymentObject['PaymentMethodRef'] = [
						'value' => $paymentMethod['payment_id']
					];
				}
				if(isset($tranID) && $tranID != '')
                {
                    $createPaymentObject['PaymentRefNum'] = substr($tranID, 0, 21);
                }
				$newPaymentObj = Payment::create($createPaymentObject);
				$savedPayment = $dataService->Add($newPaymentObj);
				
				
				
				$error = $dataService->getLastError();
			if ($error != null) {
			echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
				
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                $st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); $qbID= $tranID;
			
				
			}
			else {
			    $inID = $updatedResult->Id;
				$crtxnID = $savedPayment->Id;
				$condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchID); 
        							  $ref_number =  $in_data['refNumber']; 
        							  $tr_date   =date('Y-m-d H:i:s');
        							  	$toEmail = $c_data['userEmail']; $company=$c_data['companyName']; $customer = $c_data['fullName'];
			    if($chh_mail =='1')
        							 {
        							   
        							    
        							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
									 } 
									
			    $st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID= $tranID;
			    
				
				
				 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
				
				
			}

                  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    				     {
                    				 		$card_type      =$this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                    				        $friendlyname   =  $card_type.' - '.substr($this->czsecurity->xssCleanPostInput('card_number'),-4);
                    						$card_condition = array(
                    										 'customerListID' => $customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    					
                    						$crdata =	$this->card_model->chk_card_firendly_name( $customerID,$friendlyname)	;			
                    					     
                    					   
                    						if($crdata >0)
                    						{
                    							
                    						   $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	     =>$exyear,
                    										  'companyID'    =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'',
                    										  'updatedAt'    => date("Y-m-d H:i:s"),
                    										   'Billing_Addr1'	 =>$address1,
                        										  'Billing_Addr2'	 =>$address2,
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										  );
                    										  
                    							     		  
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					     	$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'',
                    										 'customerListID' => $customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    					
                    								
                    				            $id1 =    $this->card_model->insert_card_data($card_data);	
                    				            
                    				           
                    				            
                    						
                    						}
                    				
                    				 }
    			
    			
					   } else{
					    $code = '401';
    				     	$st='0'; $action='Pay Invoice'; $msg ="Payment Failed"; $qbID=$inID;
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> </div>'); 
					   }  
					   $qbID = $invoiceID;
					    $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $tranID,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$merchID,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				        $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
						if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;
                            

							$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }
						
                       $transaction= array();
                         $tranID ='' ;$amt='0.00';
					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                       
				       $transaction['transactionID']       = $tranID;
					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
					   $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					    $transaction['transactionModified']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					   $transaction['transactionCode']     = $code;  
					  
						$transaction['transactionType']    = "Paypal_sale";	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transaction['customerListID']      = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   =$amt ;
					   $transaction['merchantID']   = $merchID;
					   $transaction['invoiceID']   = $inID;
					   $transaction['resellerID']   = $this->resellerID;
					   $transaction['gateway']   = "Paypal";
					     if ($error == null) {
					    $transaction['qbListTxnID']   = $crtxnID;
					    }
					   $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
					   if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 
						if($custom_data_fields){
                            $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);   
				      
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>'); 
				}
          
                	}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');  
				}
          
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>'); 
		  }
			
			 if($cusproID=="2"){
			 	 redirect('QBO_controllers/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" ){
			
			 	redirect('QBO_controllers/Create_invoice/invoice_details_page/'.$in_data['invoiceID'],'refresh');
			 }
			 $trans_id = isset($PayPalResult['transactionid']) ? $PayPalResult['transactionid'] : '';
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'QBO_controllers/Create_invoice/Invoice_details',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
					 'checkPlan'	=> $checkPlan
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		  

    }     

  public function get_card_expiry_data($customerID)
  {  
  
                       $card = array();
               		   $this->load->library('encrypt');
				
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $customer_card['CardNo']  = substr($this->card_model->decrypt($card_data['CustomerCard']),12) ;
						 $customer_card['CardID'] = $card_data['CardID'] ;
						 $customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						 $card[$key] = $customer_card;
					   }
				}		
					
                return  $card;

     }
 
  public function get_card_edit_data()
  {
	  
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->card_model->get_single_card_data($cardID);
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
  }
  
  
  
		

	  
	 public function view_transaction(){

				
				$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
				
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
		        $transactions           = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id);
				
				if(!empty($transactions) )
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-center"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					<td class="hidden-xs text-right"><?php echo number_format($transaction['transactionAmount'],2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
<td class="text-right visible-lg"><?php if($transaction['transactionCode']=='300'){ ?> <span class="btn btn-alt1 btn-danger">Failed</span> <?php }else if($transaction['transactionCode']=='100'){ ?> <span class="btn btn-alt1 btn-success">Success</span><?php } ?></td>
					
				</tr>
				
		<?php     }

				}else{
					echo '<tr><td colspan="5" class="text-center">No record available</td></tr>';
				}
              die;				

      }	

	   
	  
	 	 
public function pay_multi_invoice()
{
       if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
	    $resellerID = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=> $merchID))['resellerID']; 
	 	
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
		  $cusproID='';
		  $custom_data_fields = [];
       $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

       
		$checkPlan = check_free_plan_transactions();
	   
	   if($checkPlan && $cardID!="" && $gateway!="")
	   {  
	       
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
		if($config['Sandbox'])
		{
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}
		
		$this->load->library('paypal/Paypal_pro', $config);	

	       
					 
	       $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
         if(!empty($invoices)) 
         { 
            foreach($invoices as $key=> $invoiceID)
            {
                $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                  $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
		if(!empty($in_data))
		{ 
		  
			$Customer_ListID = $in_data['CustomerListID'];
             	$c_data   = $this->general_model->get_select_data('QBO_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID, 'merchantID'=>$merchID));
			$companyID = $c_data['companyID'];		 
    		   
           if($cardID=='new1')
           {
				$cardID_upd  =$cardID;
				$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
				$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
				$cardType       = $this->general_model->getType($card_no);
				$friendlyname   =  $cardType.' - '.substr($card_no,-4);
           }
        else{
            $card_data    =   $this->card_model->get_single_card_data($cardID); 
            $card_no  = $card_data['CardNo'];
           	$cvv      =  $card_data['CardCVV'];
			$expmonth =  $card_data['cardMonth'];
			$exyear   = $card_data['cardYear'];
        }
        $cardType = $this->general_model->getType($card_no);
        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
        $custom_data_fields['payment_type'] = $friendlyname;
        
        
		     
			 
		 if(!empty($cardID))
		 {
							
				if( $in_data['BalanceRemaining'] > 0){
				    $cr_amount = 0;
					        $amount 	    =	 $in_data['BalanceRemaining'];
							$amount           =$pay_amounts;	
							$amount           = $amount-$cr_amount;
							$expDateMonth 		= $expmonth;
								$creditCardNumber 	= $card_no;
									$expDateYear=  	$exyear;
									$cvv2Number  = $cvv;
						 	$creditCardType 	= 'Visa';
					  		
								
								// Month must be padded with leading zero
						$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
								
							$currencyID     = "USD";
						  
                           
                           $firstName = $in_data['firstName'];
                            $lastName =  $in_data['lastName']; 
                            $companyName =  $in_data['companyName']; 
							$address1 = $in_data['ShipAddress_Addr1']; 
                            $address2 = $in_data['ShipAddress_Addr2']; 
							$country  = $in_data['ShipAddress_Country']; 
							$city     = $in_data['ShipAddress_City'];
							$state    = $in_data['ShipAddress_State'];		
								$zip  = $in_data['ShipAddress_PostalCode']; 
								$phone = $in_data['phoneNumber']; 
								$email = $in_data['userEmail']; 
										
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							// 'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);

						$cvv2Number = trim($cvv2Number);
						if($cvv2Number && !empty($cvv2Number)){
							$CCDetails['cvv2'] = $cvv2Number;
						}
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
							
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
					 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
					 {  
					
					           $code = '111';
					           $tranID = $PayPalResult['TRANSACTIONID']; 
					
				$val = array(
					'merchantID' => $merchID,
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
        				'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
	            
	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
				
				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
					$theInvoice = current($targetInvoiceArray);
				}
				
				
				$createPaymentObject = [
				    "TotalAmt" => $amount,
				    "SyncToken" => 1, 
				    "CustomerRef" => $Customer_ListID,
				    "Line" => [
				     "LinkedTxn" =>[
				            "TxnId" => $invoiceID,
				            "TxnType" => "Invoice",
				        ],    
				       "Amount" => $amount
			        ]
				];

				$paymentMethod = $this->general_model->qbo_payment_method($creditCardType, $this->merchantID, false);
				if($paymentMethod){
					$createPaymentObject['PaymentMethodRef'] = [
						'value' => $paymentMethod['payment_id']
					];
				}
				if(isset($tranID) && $tranID != '')
                {
                    $createPaymentObject['PaymentRefNum'] = substr($tranID, 0, 21);
                }
				$newPaymentObj = Payment::create($createPaymentObject);
				$savedPayment = $dataService->Add($newPaymentObj);
				
				
				
				$error = $dataService->getLastError();
			if ($error != null) {
		$err="The Status code is: " . $error->getHttpStatusCode() . "\n";
				$err.=$error->getOAuthHelperError() . "\n";
					$err.="The Response message is: " . $error->getResponseBody() . "\n";
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error '.$err.'</strong></div>');
                $st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); $qbID=$tranID;
			
				
			}
			else {
				
			
				
					 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
				
					$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$tranID;
				
			}
                	 		         if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    				     {
                    				 		$card_type      =$this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
                    				        $friendlyname   =  $card_type.' - '.substr($this->czsecurity->xssCleanPostInput('card_number'),-4);
                    						$card_condition = array(
                    										 'customerListID' => $customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    					
                    						$crdata =	$this->card_model->chk_card_firendly_name( $customerID,$friendlyname)	;			
                    					     
                    					   
                    						if($crdata >0)
                    						{
                    							
                    						   $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	     =>$exyear,
                    										  'companyID'    =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										  'updatedAt'    => date("Y-m-d H:i:s"),
                    										   'Billing_Addr1'	 =>$address1,
                        										  'Billing_Addr2'	 =>$address2,
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										  );
                    										  
                    							     		  
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					     	$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'',
                    										 'customerListID' => $customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    					
                    								
                    				            $id1 =    $this->card_model->insert_card_data($card_data);	
                    				            
                    				           
                    				            
                    						
                    						}
                    				
                    				 }
    			

					   } else{
					   $code = '401';
				     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				    	$st='0'; $action='Pay Invoice'; $msg ="Payment Failed"; $qbID=$invoiceID;
					   
					   }  
					   $qbID=$invoiceID;
				  
				       $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $tranID,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$merchID,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				        $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
						if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;
                            
							$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }
                       $transaction= array();
                        $tranID ='' ;$amt='0.00';
					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                       
				       $transaction['transactionID']       = $tranID;
					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
					   $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					    $transactiondata['transactionModified']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					   $transaction['transactionCode']     = $code;  
					  
						$transaction['transactionType']    = "Paypal_sale";	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transaction['customerListID']      = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   =$amt ;
					   $transaction['merchantID']   = $merchID;
					   $transaction['invoiceID']   = $updatedResult->Id;
					   $transaction['resellerID']   = $resellerID;
					   $transaction['gateway']   = "Paypal";
					     if ($error == null) {
					    $transaction['qbListTxnID']   = $savedPayment->Id;
					    }
					            
					   $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']); 
					    if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 
						if($custom_data_fields){
                            $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}
	    	else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>'); 
			 }
			 
	   }
         }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');    
         }    	 
			 
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>'); 
		  }

		  	if(!$checkPlan){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
						
		    	if($cusproID!=""){
			 	 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
		   	 }

    }      
	   
	 
}

