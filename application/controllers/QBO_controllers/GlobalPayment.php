<?php
/**
 * This Controller has Authorize.net Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 
 */
 
require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\PaymentMethods\ECheck;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

class GlobalPayment extends CI_Controller
{
    
     private $merchantID;
     private $resellerID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
		
		
        $this->load->config('globalpayments');
		$this->load->model('quickbooks');
        $this->load->model('QBO_models/qbo_customer_model','qbo_customer_model');
		$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_company_model','qbo_company_model');
		$this->load->model('card_model');
		$this->load->library('form_validation');
		 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='1' )
		  {
		  	$logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $this->merchantID = $logged_in_data['merchID'];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $logged_in_data['merchantID'];
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index()
	{
	    
	    redirect('QBO_controllers/Payments/payment_transaction','refresh');
	}
	    
	

	
	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');

			if($this->czsecurity->xssCleanPostInput('sch_method') != 2){
			     $this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');
                $this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
            }
    
    		if($this->czsecurity->xssCleanPostInput('cardID')=="new1")
            { 
            
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
			$checkPlan = check_free_plan_transactions();
            $pay_option = $this->czsecurity->xssCleanPostInput('sch_method');
            $echeck_payment = false;
            $custom_data_fields = [];
            if($pay_option == 2){
                $echeck_payment = true;
            }   
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		       
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	 $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			      $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);
			    if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
			    
			   $ref_number= array();
			    if(!empty( $in_data)) 
			    {
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				   $customerID = $in_data['CustomerListID'];
					    $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
					     $c_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail', 'firstName', 'lastName'), $con_cust);
    			     $secretApiKey   = $gt_result['gatewayPassword'];
    			     
    			     if($cardID!="new1")
    			     {
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
                        $phone   =  $card_data['Billing_Contact'];
                        $accountNumber  = $card_data['accountNumber'];
                        $routeNumber  = $card_data['routeNumber'];
                        $accountName  = $card_data['accountName'];
                        $secCodeEntryMethod  = $card_data['secCodeEntryMethod'];
                        $accountType  = $card_data['accountType'];
                        $accountHolderType  = $card_data['accountHolderType'];
    			     }
    			     else
    			     {
                        $accountNumber  = $this->czsecurity->xssCleanPostInput('acc_number');
                        $routeNumber  = $this->czsecurity->xssCleanPostInput('route_number');
                        $accountName  = $this->czsecurity->xssCleanPostInput('acc_name');
                        $secCodeEntryMethod  = $this->czsecurity->xssCleanPostInput('secCode');
                        $accountType  = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                        $accountHolderType  = $this->czsecurity->xssCleanPostInput('acct_type');

    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }
                    if($pay_option == 2){
                        
                        $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;
                    }else{
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);

                        $custom_data_fields['payment_type'] = $friendlyname;
                    }                               
        		$crtxnID='';	     
        	
                 $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
   
   
            
                ServicesContainer::configureService($config);
                $card = new CreditCardData();
                $card->number = $card_no;
                $card->expMonth = $expmonth;
                $card->expYear = $exyear;
				// add customer name
                $firstName = isset($c_data['firstName']) ? $c_data['firstName'] : '';
                $lastName = isset($c_data['lastName']) ? $c_data['lastName'] : '';
                $cardHolderName = ($lastName) ? $firstName.' '.$lastName : $firstName;
                $card->cardHolderName = $cardHolderName;

				$cvv = trim($cvv);
				if($cvv && !empty($cvv)){
					$card->cvn = $cvv;
				}

                 $card->cardType=$cardType;
           
                $address = new Address();
                $address->streetAddress1 = $address1;
                $address->city = $city;
                $address->state = $state;
                $address->postalCode = $zipcode;
                $address->country = $country;
                
               $refNum=array();$ref_numbeer='';
                $invNo  =mt_rand(1000000,2000000);
             	try
                {
                    if($pay_option == 2){
                        $check = new ECheck();
                        $check->accountNumber = $accountNumber;
                        $check->routingNumber = $routeNumber;
                        if(strtolower($accountType) == 'checking'){
                            $check->accountType = 0;
                        }else{
                            $check->accountType = 1;
                        }

                        if(strtoupper($accountHolderType) == 'PERSONAL'){
                            $check->checkType = 0;
                        }else{
                            $check->checkType = 1;
                        }
                        $check->checkHolderName = $accountName;
                        $check->secCode = "WEB";

                        $response = $check->charge($amount)
                        ->withCurrency(CURRENCY)
                        ->withAddress($address)
                        ->withInvoiceNumber($invNo)
                        ->withAllowDuplicates(true)
                        ->execute();
                    }else{

                        $response = $card->charge($amount)
                        ->withCurrency('USD')
                        ->withAddress($address)
                        ->withInvoiceNumber($invNo)
                        ->withAllowDuplicates(true)
                        ->execute();
                    }
                    
                    if($response->responseCode != 0 && $response->responseCode != '00')
                    {
                        $error='Gateway Error. Invalid Account Details';
                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
                        if($cusproID=="2"){
                            redirect('QBO_controllers/home/view_customer/'.$customerID);
                        }else if($cusproID=="3" ){
                            redirect('QBO_controllers/Create_invoice/invoice_details_page/'.$in_data['invoiceID']);
                        }else{
                            redirect('QBO_controllers/Create_invoice/Invoice_details');
                        }
                    }

                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                        if($pay_option != 2){
                         	// add level three data
                            $transaction = new Transaction();
                            $transaction->transactionReference = new TransactionReference();
                            $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                            $level_three_request = [
                                'card_no' => $card_no,
                                'amount' => $amount,
                                'invoice_id' => $invNo,
                                'merchID' => $user_id,
                                'transaction_id' => $response->transactionId,
                                'transaction' => $transaction,
                                'levelCommercialData' => $levelCommercialData,
                                'gateway' => 7
                            ];
                            addlevelThreeDataInTransaction($level_three_request);
                        } else {
                            $cardType = 'Check';
                        }

                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         	$st='0'; $action='Pay Invoice'; $msg ="Payment Success "; 
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                                  
                     	 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
                         
                             $txnID      = $in_data['invoiceID'];  
							 $ispaid 	 = 1;
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 0;
							 $app_amount = $in_data['Total_payment']+$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('invoiceID'=>$in_data['invoiceID'],'merchantID'=>$user_id  );
						
							 $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
                          
                       $val = array(
							'merchantID' => $user_id,
						);
						$data = $this->general_model->get_row_data('QBO_token',$val);

						$accessToken  = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];
						$dataService  = DataService::Configure(array(
						 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
						));
		
        			    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        	            
        	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
        				        				
        				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        				{
        					$theInvoice = current($targetInvoiceArray);
        			
                				
                				
                                $createPaymentObject = [
                				    "TotalAmt" => $amount,
                				    "SyncToken" => 1,
                				    "CustomerRef" => $customerID,
                				    "Line" => [
                                        "LinkedTxn" =>[
                                                "TxnId" => $invoiceID,
                                                "TxnType" => "Invoice",
                                            ],    
                                        "Amount" => $amount
                			        ]
                                ];
    
                                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, $echeck_payment);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($trID) && $trID != ''){
                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                }
    
                                $newPaymentObj = Payment::create($createPaymentObject);
                             
                				$savedPayment = $dataService->Add($newPaymentObj);
                				
                		    	$crtxnID=	$savedPayment->Id;
                				
                				$error = $dataService->getLastError();
                              
                			    if ($error != null) 
                			    {
                	
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                				$st='';
                
                				$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); 
                				
                				$qbID=$trID;
                				
                			   }
                    		   else 
                    		   {  $refNum[] =  $in_data['refNumber']; 
                    		       
                    		       	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
                    		           
                    		        	 $this->session->set_flashdata('success', 'Successfully Processed Invoice ');
 
                    			}
                                      
                          
							}
							$ref_number=implode(',',$refNum); 
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								
								$tr_date   =date('Y-m-d H:i:s');
								$toEmail = $c_data['userEmail']; $company=$c_data['companyName']; $customer = $c_data['fullName'];   
                          	
							
							if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    		{
								$card_type      =$this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
								$friendlyname   =  $card_type.' - '.substr($this->czsecurity->xssCleanPostInput('card_number'),-4);
								$card_condition = array(
													'customerListID' => $customerID, 
													'customerCardfriendlyName'=>$friendlyname,
												);
							
								$crdata =	$this->card_model->chk_card_firendly_name( $customerID,$friendlyname)	;			
									
								
								if($crdata >0)
								{
									
									$card_data = array('cardMonth'   =>$expmonth,
													'cardYear'	     =>$exyear,
													'companyID'    =>$c_data['companyID'],
													'merchantID'   => $user_id,
													'CardType'     =>$card_type,
													'CustomerCard' =>$this->card_model->encrypt($card_no),
													'CardCVV'      =>'', 
													'updatedAt'    => date("Y-m-d H:i:s"),
													'Billing_Addr1'	 =>$address1,
														'Billing_Addr2'	 =>$address2,
														'Billing_City'	 =>$city,
														'Billing_State'	 =>$state,
														'Billing_Country'	 =>$country,
														'Billing_Contact'	 =>$phone,
														'Billing_Zipcode'	 =>$zipcode,
													);
													
													
							
									$this->card_model->update_card_data($card_condition, $card_data);				 
								}
								else
								{
									$card_data = array('cardMonth'   =>$expmonth,
													'cardYear'	 =>$exyear, 
													'CardType'     =>$card_type,
													'CustomerCard' =>$this->card_model->encrypt($card_no),
													'CardCVV'      =>'',  
													'customerListID' => $customerID, 
													'companyID'    =>$c_data['companyID'],
													'merchantID'   => $user_id,
													'customerCardfriendlyName'=>$friendlyname,
													'createdAt' 	=> date("Y-m-d H:i:s"),
													'Billing_Addr1'	 =>$address1,
													'Billing_Addr2'	 =>$address2,	 
														'Billing_City'	 =>$city,
														'Billing_State'	 =>$state,
														'Billing_Country'	 =>$country,
														'Billing_Contact'	 =>$phone,
														'Billing_Zipcode'	 =>$zipcode,
													);
							
										
									$id1 =    $this->card_model->insert_card_data($card_data);	
									
									
									
								
								}
						
							}
							 
							 $qbID=$invoiceID; 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           
                         $st='0'; $action='Pay Invoice'; $qbID=$invoiceID;
					   
					       
				
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['CustomerListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['invoiceID'], $echeck_payment, $this->transactionByUser, $custom_data_fields); 
                     
                    if($res['transactionCode'] == 200 && $chh_mail =='1')
                    {
                        $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $trID, $res['transactionId']);
                    }
                 } 
                catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    
                    
                    if($error!='')
                    {
                           $st='0'; $action='Pay Invoice'; $msg ="Payment Failed"; $qbID=$invoiceID;
                    }
                    $qbID=$invoiceID;
                    $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
                    $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
				    if($syncid){
                        $qbSyncID = 'CQ-'.$syncid;

                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                        
                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                    }         
		    }
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			 if($cusproID=="2"){
			 	 redirect('QBO_controllers/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" ){
			 	redirect('QBO_controllers/Create_invoice/invoice_details_page/'.$in_data['invoiceID'],'refresh');
			 }
			 $trans_id = $response->transactionId;
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'QBO_controllers/Create_invoice/Invoice_details',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
					 'checkPlan'  => $checkPlan
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		 
	}
	
	
	
	public function create_customer_sale()
	{ 
	      
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('cardID')=="new1")
            { 
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           
			$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
	            $custom_data_fields = [];
	            $applySurcharge = false;
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                	$applySurcharge = true;
                    $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
        			     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        		if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
        			  
        			if(!empty($gt_result) )
        			{
        			     $apiloginID      = $gt_result['gatewayUsername'];
        				 $secretApiKey   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        			
					    $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
					     $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail','firstName','lastName'), $con_cust);
        				$companyID  = $comp_data['companyID'];
        			 
        		          
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                                $cardType = $this->general_model->getType($card_no);
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						    $phone    =  $card_data['Billing_Contact'];
        					
        					}
        					/*Added card type in transaction table*/
			                $cardType = $this->general_model->getType($card_no);
			                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
			                $custom_data_fields['payment_type'] = $friendlyname;
	        					   
                             $config = new PorticoConfig();
                           
                              $config->secretApiKey = $secretApiKey;
                              $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                            ServicesContainer::configureService($config);
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;

                            // add customer name
                            $firstName = isset($comp_data['firstName']) ? $comp_data['firstName'] : '';
                            $lastName = isset($comp_data['lastName']) ? $comp_data['lastName'] : '';
                            $cardHolderName = ($lastName) ? $firstName.' '.$lastName : $firstName;
                            $card->cardHolderName = $cardHolderName;
							
							$cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$card->cvn = $cvv;
							}

                             $card->cardType=$cardType;
                       
                            $address = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city = $city;
                            $address->state = $state;
                            $address->postalCode = $zipcode;
                            $address->country = $country;
                            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            // update amount with surcharge 
                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                                $amount += round($surchargeAmount, 2);
                                
                                $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                                $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                                $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');

                            }
                            $totalamount  = $amount;
                            $invNo  =mt_rand(1000000,2000000);
                            if($this->czsecurity->xssCleanPostInput('invoice_id')){
                                $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
                                $invNo = $new_invoice_number;
                            }
                         	try
                            {
                             $response = $card->charge($amount)
                            ->withCurrency('USD')
                            ->withAddress($address)
                            ->withInvoiceNumber($invNo)
                            ->withAllowDuplicates(true)
                            ->execute();
        				  
            	          	$crtxnID='';
            				
            			  	 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                             {

								// add level three data
		                        $transaction = new Transaction();
		                        $transaction->transactionReference = new TransactionReference();
		                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
		                        $level_three_request = [
		                            'card_no' => $card_no,
		                            'amount' => $amount,
		                            'invoice_id' => $invNo,
		                            'merchID' => $user_id,
		                            'transaction_id' => $response->transactionId,
		                            'transaction' => $transaction,
		                            'levelCommercialData' => $levelCommercialData,
		                            'gateway' => 7
		                        ];
		                        addlevelThreeDataInTransaction($level_three_request);

                                 $msg = $response->responseMessage;
                                 $trID = $response->transactionId;
                                 
                                 $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				   
            				 $invoiceIDs=array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                                $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
        				     }
				     
				         
				        
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			            $refNumber =array();
                 
				           if(!empty($invoiceIDs))
				           {
                                $payIndex = 0;
                                $saleAmountRemaining = $amount;
                                foreach ($invoiceIDs as $inID) {
                                    $theInvoice = array();
                                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                                    $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                                    $con = array('invoiceID' => $inID, 'merchantID' => $user_id);
                                    $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                                    
                                    $refNumber[]   =  $res1['refNumber'];
                                    $txnID      = $inID;
                                    $pay_amounts = 0;


                                    $amount_data = $res1['BalanceRemaining'];
                                    $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                    if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                        $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                        $actualInvoicePayAmount += $surchargeAmount;
                                        $amount_data += $surchargeAmount;
                                        $updatedInvoiceData = [
                                            'inID' => $inID,
                                            'targetInvoiceArray' => $targetInvoiceArray,
                                            'merchantID' => $user_id,
                                            'dataService' => $dataService,
                                            'realmID' => $realmID,
                                            'accessToken' => $accessToken,
                                            'refreshToken' => $refreshToken,
                                            'amount' => $surchargeAmount,
                                        ];
                                        $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                                    }
                                    $ispaid      = 0;
                                    $isRun = 0;
                                    if($saleAmountRemaining > 0){
                                        $BalanceRemaining = 0.00;
                                        if($amount_data == $actualInvoicePayAmount){
                                            $actualInvoicePayAmount = $amount_data;
                                            $isPaid      = 1;

                                        }else{

                                            $actualInvoicePayAmount = $actualInvoicePayAmount;
                                            $isPaid      = 0;
                                            $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                            
                                        }
                                        $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

                                        $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

                                        
                                        $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                        if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                            $theInvoice = current($targetInvoiceArray);

                                            $createPaymentObject = [
                                                "TotalAmt" => $actualInvoicePayAmount,
                                                "SyncToken" => 1,
                                                "CustomerRef" => $customerID,
                                                "Line" => [
                                                    "LinkedTxn" => [
                                                        "TxnId" => $inID,
                                                        "TxnType" => "Invoice",
                                                    ],
                                                    "Amount" => $actualInvoicePayAmount
                                                ]
                                            ];
                
                                            $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                            if($paymentMethod){
                                                $createPaymentObject['PaymentMethodRef'] = [
                                                    'value' => $paymentMethod['payment_id']
                                                ];
                                            }
                                            if(isset($trID) && $trID != ''){
			                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
			                                }
                
                                            $newPaymentObj = Payment::create($createPaymentObject);
                                         
                                            $savedPayment = $dataService->Add($newPaymentObj);

                                            $error = $dataService->getLastError();
                                            if ($error != null) {
                                                $err = '';
                                                $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                                $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                                $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                                $st = '0';
                                                $action = 'Pay Invoice';
                                                $msg = "Payment Success but " . $error->getResponseBody();
                                                $qbID = $trID;
                                                $pinv_id = '';
                                            } else {
                                                $pinv_id = '';
                                                $crtxnID = $pinv_id = $savedPayment->Id;
                                                $st = '1';
                                                $action = 'Pay Invoice';
                                                $msg = "Payment Success";
                                                $qbID = $trID;
                                            }
                                            $qbID = $inID;
                                            $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                            $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                            if($syncid){
                                                $qbSyncID = 'CQ-'.$syncid;

                                                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                            }
                                            
                                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$actualInvoicePayAmount,$user_id,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);
                                            
                                        }
                                    }
                                    $payIndex++;

                                }
				            }
				          else
				          {
				              
				                    $crtxnID='';
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
				          }
				          
				          
				          $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number = implode(',',$refNumber); 
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];
				          	if($chh_mail =='1')
							 {
							   
							    
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $res['transactionId']);
							 } 
            				       
            			         if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc')) )
            				     {
            				 		
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            				    
            				     	 $this->session->set_flashdata('success', 'Transaction Successful');
 
 	
            				 } 
            				 else
            				 {
            				          $crtxnID ='';
                                      $msg = $response->responseMessage;
                                      $trID = $response->transactionId;
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                                     
                              }
                         
                         
                         
                             }
                            catch (BuilderException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ConfigurationException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (GatewayException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (UnsupportedTransactionException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ApiException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => $response->transactionId,
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'	 => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
	    
	    
    	}
	
	
	public function create_customer_auth()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	    
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('cardID')=="new1")
            { 
        
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           	$custom_data_fields = [];
			$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    { 
                
                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
	   
        			     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			
                			  
        			if(!empty($gt_result) )
        			{
        			     $apiloginID      = $gt_result['gatewayUsername'];
        				 $secretApiKey   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        		
        			    $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
					     $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail', 'firstName', 'lastName'), $con_cust);
        			
        				$companyID  = $comp_data['companyID'];
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						$cardType = $this->general_model->getType($card_no);
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						    $phone    =  $card_data['Billing_Contact'];
        					        
        					}
                            $cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
        					$custom_data_fields['card_type'] = $cardType; 
                             $config = new PorticoConfig();
                           
                              $config->secretApiKey = $secretApiKey;
                              $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                            ServicesContainer::configureService($config);
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;

							// add customer name
                            $firstName = isset($comp_data['firstName']) ? $comp_data['firstName'] : '';
                            $lastName = isset($comp_data['lastName']) ? $comp_data['lastName'] : '';
                            $cardHolderName = ($lastName) ? $firstName.' '.$lastName : $firstName;
                            $card->cardHolderName = $cardHolderName;

							$cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$card->cvn = $cvv;
							}

                             $card->cardType=$cardType;
                       
                            $address = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city = $city;
                            $address->state = $state;
                            $address->postalCode = $zipcode;
                            $address->country = $country;
                              $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $invNo  =mt_rand(1000000,2000000);
                         	try
                            {
                            	$level2CommercialData = new CommercialData(TaxType::NOT_USED, 'Level_II');
	                            $level2CommercialData->taxType = 'SALESTAX';
	                            $level2CommercialData->taxAmount = '0.00';
	                            $level2CommercialData->poNumber = $this->input->post('po_number', true);
	                            $response = $card->authorize($amount)
	                            ->withCurrency('USD')
	                            ->withAddress($address)
	                            ->withCommercialRequest(true)
	                    		->withCommercialData($level2CommercialData)
	                            ->withInvoiceNumber('TBD')
	                            ->withAllowDuplicates(true)
	                            ->execute();
        				  
        						
            		
            	          	$crtxnID='';
            				
            			  	 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                             {

								// add level three data
		                        $transaction = new Transaction();
		                        $transaction->transactionReference = new TransactionReference();
		                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
		                        $level_three_request = [
		                            'card_no' => $card_no,
		                            'amount' => $amount,
		                            'invoice_id' => $invNo,
		                            'merchID' => $user_id,
		                            'transaction_id' => $response->transactionId,
		                            'transaction' => $transaction,
		                            'levelCommercialData' => $levelCommercialData,
		                            'gateway' => 7,
                                    'po_number' => $this->input->post('po_number', true)
		                        ];
		                        addlevelThreeDataInTransaction($level_three_request);

                                 $msg = $response->responseMessage;
                                 $trID = $response->transactionId;
                                 
                                 $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
                				 /* This block is created for saving Card info in encrypted form  */
            				 
            			        	if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1" &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				     {
            				 		
            				     
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            				
            			
            			
            			        $this->session->set_flashdata('success', 'Transaction Successful');
 
 	
            				 } 
            				 else
            				 {
                                      $msg = $response->responseMessage;
                                      $trID = $response->transactionId;
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                     
                              }
                         
                         
                         
                            $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
          
                             }
                            catch (BuilderException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ConfigurationException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (GatewayException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (UnsupportedTransactionException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ApiException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					
				
					$receipt_data = array(
						'transaction_id' => $response->transactionId,
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_auth',
						'proccess_btn_text' => 'Process New Transaction',
						'sub_header' => 'Authorize',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
                 
	    
	    
    	}
    	
    	
		
	public function create_customer_void()
	{
	
		$custom_data_fields = [];
		if(!empty($this->input->post(null, true)))
		{
		    
		      	 if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
				
			     $tID     = $this->czsecurity->xssCleanPostInput('strtxnvoidID');
				 
				 $con     = array('transactionID'=>$tID,'transactionType'=>'auth');
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			  
    			 $gateway = $paydata['gatewayID'];
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
				   if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
        		if($tID!='' && !empty($gt_result))
        		{
        			  
        		    $secretApiKey  =  $gt_result['gatewayPassword'];
        		
    		      $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
   
   
                $amount =  $paydata['transactionAmount'];
                ServicesContainer::configureService($config);
                    $customerID = $paydata['customerListID'];
                      $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
             	try
                {
                 $response = Transaction::fromId($tID)
                     ->void()
                   ->execute();
                    
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                         
                         	$condition = array('transactionID'=>$tID);
					
				         	$update_data =   array('transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s'));
					
				        	$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
				        	
				        	
				        	if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
                        
                         
						 $this->session->set_flashdata('success', 'Successfully Captured Authorize Transaction');
 	 
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     
                     
                     $id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$paydata['customerListID'],$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                 } 
                    catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
			   
				       
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }			 
                    redirect('QBO_controllers/Payments/payment_capture','refresh');
			
        }
              
				
		 redirect('QBO_controllers/Payments/payment_transaction','refresh');

	}
	
	
		/*****************Capture Transaction***************/
	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true)))
		{
		    
		      	 if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
				
			     $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			  
    			 $gateway = $paydata['gatewayID'];
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
				   if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
        		if($tID!='' && !empty($gt_result))
        		{
        			  
        		    $secretApiKey  =  $gt_result['gatewayPassword'];
        		
    		      $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
   
   
                $amount =  $paydata['transactionAmount'];
                ServicesContainer::configureService($config);
                $customerID = $paydata['customerListID'];
                                $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
                
             	try
                {
                    $response= Transaction::fromId($tID)
                  ->capture($amount)
                   ->execute();
                    
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                         
                         	$condition = array('transactionID'=>$tID);
					
				         	$update_data =   array('transaction_user_status'=>"4");
					
							$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
							$condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];
				        	if($chh_mail =='1')
                            {
                                  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
							}
							
                         
							 $this->session->set_flashdata('success', 'Successfully Captured Authorize Transaction');
 	 	 
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     
                     
                     $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$paydata['customerListID'],$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                 } 
                    catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
			   
				       
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }	
				 $invoice_IDs = array();
				
			 
				 $receipt_data = array(
					 'proccess_url' => 'QBO_controllers/Payments/payment_capture',
					 'proccess_btn_text' => 'Process New Transaction',
					 'sub_header' => 'Capture',
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 if($paydata['invoiceTxnID'] == ''){
					 $paydata['invoiceTxnID'] ='null';
					 }
					 if($paydata['customerListID'] == ''){
						 $paydata['customerListID'] ='null';
					 }
					 if($response->transactionId == ''){
						 $response->transactionId ='null';
					 }
				 
				 redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$response->transactionId,  'refresh');		 		 
				
        }
              
				
		redirect('QBO_controllers/Payments/payment_transaction','refresh');

	}
	
	
	
	
		
	public function pay_multi_invoice()
	{
	  
	      
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
		
			$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
        
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                            
             $cusproID=''; $error='';
             $custom_data_fields = [];
		     $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			$checkPlan = check_free_plan_transactions();
               
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		        
		    	 $cardID_upd ='';
			     
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
			     $amount               = $this->czsecurity->xssCleanPostInput('totalPay');
			     $customerID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			     $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
			       $invoiceIDs =implode(',', $invoices);
            
			   $inv_data   =    $this->qbo_customer_model->get_due_invoice_data($invoiceIDs,$user_id);
			   
			    if(!empty( $invoices) && !empty($inv_data)) 
			    {
                   $cusproID=  $customerID  = $inv_data['CustomerListID']; 
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  
    			     $secretApiKey   = $gt_result['gatewayPassword'];
    			     
    			     if($cardID!="new1")
    			     {
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['cardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					$phone    =  $card_data['Billing_Contact'];
    			     }
    			     else
    			     {
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			    }
                    $cardType = $this->general_model->getType($card_no);
                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                    $custom_data_fields['payment_type'] = $friendlyname;                              
        		$crtxnID='';	     
        	
                 $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
   
   
            
                ServicesContainer::configureService($config);
                $card = new CreditCardData();
                $card->number = $card_no;
                $card->expMonth = $expmonth;
                $card->expYear = $exyear;
				
				$cvv = trim($cvv);
				if($cvv && !empty($cvv)){
					$card->cvn = $cvv;
				}

                 $card->cardType=$cardType;
           
                $address = new Address();
                $address->streetAddress1 = $address1;
                $address->city = $city;
                $address->state = $state;
                $address->postalCode = $zipcode;
                $address->country = $country;
                
                
                $invNo  =mt_rand(1000000,2000000);
             	try
                {
                     $response = $card->charge($amount)
                    ->withCurrency('USD')
                    ->withAddress($address)
                    ->withInvoiceNumber($invNo)
                    ->withAllowDuplicates(true)
                    ->execute();
                    
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {

						// add level three data
                        $transaction = new Transaction();
                        $transaction->transactionReference = new TransactionReference();
                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                        $level_three_request = [
                            'card_no' => $card_no,
                            'amount' => $amount,
                            'invoice_id' => $invNo,
                            'merchID' => $user_id,
                            'transaction_id' => $response->transactionId,
                            'transaction' => $transaction,
                            'levelCommercialData' => $levelCommercialData,
                            'gateway' => 7
                        ];
                        addlevelThreeDataInTransaction($level_three_request);

                        $msg = $response->responseMessage;
                        $trID = $response->transactionId;
                        
                        $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                    
                    
                    	 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 	 
                    
            			$c_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
            			$companyID = $c_data['companyID'];
            			
            			
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			            
                 
				           if(!empty($invoices))
				           {
				               
				              foreach($invoices as $invoiceID)
				              {
        				                $theInvoice = array();
        							  
        						       
        							   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '".$invoiceID."'");
        							    $condition  = array('invoiceID'=>$invoiceID,'merchantID'=>$user_id );
        						            $in_data =$this->general_model->get_select_data('QBO_test_invoice',array('BalanceRemaining','Total_payment'),$condition);
        								     $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                             $txnID       = $invoiceID;  
            						            $ispaid   = 1;
            							
            						    	$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 0;
            							 $app_amount = $in_data['Total_payment']+$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $to_amount = $in_data['BalanceRemaining']+$in_data['Total_payment'];
            							 
        							    $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
        							 		
        								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        								{
        									$theInvoice = current($targetInvoiceArray);
        								
            								
            								$createPaymentObject = [
            									"TotalAmt" => $pay_amounts,
            									"SyncToken" => 1,
            									"CustomerRef" => $customerID,
            									"Line" => [
            									 "LinkedTxn" =>[
            											"TxnId" => $invoiceID,
            											"TxnType" => "Invoice",
            										],    
            									   "Amount" => $pay_amounts
            									]
                                            ];
                
                                            $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                            if($paymentMethod){
                                                $createPaymentObject['PaymentMethodRef'] = [
                                                    'value' => $paymentMethod['payment_id']
                                                ];
                                            }
                                            if(isset($trID) && $trID != ''){
			                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
			                                }
                
                                            $newPaymentObj = Payment::create($createPaymentObject);
            							 
            								$savedPayment = $dataService->Add($newPaymentObj);
        								
        								
        								
            								$error = $dataService->getLastError();
            								if ($error != null)
            						   	{
            							    $err='';
            					        	$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
            									$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
            								$err.="The Response message is: " . $error->getResponseBody() . "\n";
            								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>'.	$err.'</div>');
            
            							$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody();
            							
            							$qbID=$trID;	
            								$pinv_id='';
            							}
            							else 
            							{ 
            							      $pinv_id='';
            							       $pinv_id        =  $savedPayment->Id;
            							    	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
            							    	
            						
            							
            							 $this->session->set_flashdata('success', 'Transaction Successful');
 
            							
            							}
                                          $qbID = $invoiceID;  
            					          $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				                            $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
				                            if($syncid){
						                        $qbSyncID = 'CQ-'.$syncid;

                                                

                                                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

						                       
						                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
						                    }
            							    $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$pay_amounts,$user_id,$pinv_id, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);     		
            							
        						}
				              }
						 
				            }
                           
                            
                             if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				 {
            				 		
								$cardType       = $this->general_model->getType($card_no);
								$friendlyname   =  $cardType.' - '.substr($card_no,-4);
            				       
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
							 
							 
                     }
                     else
                     {
                         $crtxnID='';
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                         
                     }
                     
                     
                     
                   
                 } 
                catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    
		    }
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			if(!$checkPlan){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
		    	 if($cusproID!="")
				 {
					 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
				 }
	      
	}
	
	
	
		  
	  
	 	public function test_index()
	   {
         $config = new PorticoConfig();
       
         $config->secretApiKey = 'skapi_cert_MYl2AQAowiQAbLp5JesGKh7QFkcizOP2jcX9BrEMqQ';
          $config->serviceUrl = 'https://cert.api2.heartlandportico.com';
   
   
        
        ServicesContainer::configureService($config);
        
         
        $card = new CreditCardData();
        $card->number = "4111111111111111";
        $card->expMonth = "12";
        $card->expYear = "2022";
        $card->cvn = "123";
         $card->cardType='VISA';
       
        $address = new Address();
        $address->streetAddress1 = "1231 E Dyer Rd Ste 290";
        $address->city = "Santa Ana";
        $address->state = "CA";
        $address->postalCode = "92705";
        $address->country = "United States";
        $invNo  =mt_rand(1000000,2000000);
     try
        {
            
       
          $response= Transaction::fromId('1176936353')
             ->capture(4)
             ->execute();
            
            
            $response = $card->authorize(15)
            ->withCurrency('USD')
            ->withAddress($address)
           ->execute();
    
           
          
             $response = $card->charge(15)
            ->withCurrency('USD')
            ->withAddress($address)
            ->withInvoiceNumber($invNo)
            ->withAllowDuplicates(true)
            ->execute();  
            echo "<pre>";
              print_r($response);
            $body = '<h1>Success!</h1>';
            $body .= '<p>Thank you, Test User for your order of $15.00</p>';
            echo "APPROVAL". $response->responseMessage;
            echo "Transaction Id: " . $response->transactionId;
            echo "<br />Invoice Number:".$invNo ;
        
            // i'm running windows, so i had to update this:
            
            } 
            catch (BuilderException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (ConfigurationException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (GatewayException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (UnsupportedTransactionException $e)
            {
               echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (ApiException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
    
	    
	}
	
    public function create_customer_esale()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            
            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
            $checkPlan = check_free_plan_transactions();

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }


            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $secretApiKey  = $gt_result['gatewayPassword'];
                
                $comp_data     =$this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName', 'phoneNumber','userEmail', 'FullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
                $companyID  = $comp_data['companyID'];

                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code =     'WEB';

                if($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID' => $customerID,
                        'companyID'     => $companyID,
                        'merchantID'   => $merchantID,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                try
                {
                    $config = new PorticoConfig();
               
                    $config->secretApiKey = $secretApiKey;
                    if($this->config->item('Sandbox')){
                        $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                    }
                    else{
                        $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
                        $config->developerId =  $this->config->item('DeveloperId');
                        $config->versionNumber =  $this->config->item('VersionNumber');
                    }
       
                    ServicesContainer::configureService($config);
                    $check = new ECheck();
                    $check->accountNumber = $accountDetails['accountNumber'];
                    $check->routingNumber = $accountDetails['routeNumber'];
                    if(strtolower($accountDetails['accountType']) == 'checking'){
                        $check->accountType = 0;
                    }else{
                        $check->accountType = 1;
                    }

                    if(strtoupper($accountDetails['accountHolderType']) == 'PERSONAL'){
                        $check->checkType = 0;
                    }else{
                        $check->checkType = 1;
                    }
                    $check->checkHolderName = $accountDetails['accountName'];
                    $check->secCode = "WEB";
               
                    $address = new Address();
                    $address->streetAddress1 = $accountDetails['Billing_Addr1'];
                    $address->city = $accountDetails['Billing_City'];
                    $address->state = $accountDetails['Billing_State'];
                    $address->postalCode = $accountDetails['Billing_Zipcode'];
                    $address->country = $accountDetails['Billing_Country'];
                    
                    $invNo = '';
                    if($this->czsecurity->xssCleanPostInput('invoice_id')){
                        $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                        $invNo = $new_invoice_number;
                    }

                    $response = $check->charge($amount)
                                    ->withCurrency(CURRENCY)
                                    ->withAddress($address)
                                    ->withInvoiceNumber($invNo)
                                    ->withAllowDuplicates(true)
                                    ->execute();
                    $msg = $response->responseMessage;
                    $trID = $response->transactionId;
                    $res =array('transactionCode' => 200, 'status'=>$msg, 'transactionId'=> $trID );
                    if($response->responseCode == '00' ||  $response->responseCode == 0){
                        if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                        {
                            if ($this->czsecurity->xssCleanPostInput('tr_checked')){
                                $chh_mail = 1;
                            }else{
                                $chh_mail = 0;
                            }
                            $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                            
                            $ref_number = '';
                            $tr_date   = date('Y-m-d H:i:s');
                            $toEmail = $comp_data['userEmail'];
                            $company = $comp_data['companyName'];
                            $customer = $comp_data['FullName'];

                            if($payableAccount == '' || $payableAccount == 'new1') {
                                $id1 = $this->card_model->process_ack_account($accountDetails);
                            }
                            
                            $invoiceIDs = array();
                            $invoicePayAmounts = array();
                            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                                $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                                $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                            }

                            $val = array('merchantID' => $merchantID);
                            $tokenData = $this->general_model->get_row_data('QBO_token', $val);

                            $accessToken = $tokenData['accessToken'];
                            $refreshToken = $tokenData['refreshToken'];
                            $realmID      = $tokenData['realmID'];

                            $dataService = DataService::Configure(array(
                                'auth_mode' => $this->config->item('AuthMode'),
                                'ClientID'  => $this->config->item('client_id'),
                                'ClientSecret' => $this->config->item('client_secret'),
                                'accessTokenKey' =>  $accessToken,
                                'refreshTokenKey' => $refreshToken,
                                'QBORealmID' => $realmID,
                                'baseUrl' => $this->config->item('QBOURL'),
                            ));
                            $refNumber = array();
                            $merchantID = $user_id;
                            if (!empty($invoiceIDs)) {
                                $this->session->set_userdata("invoice_IDs",$invoiceIDs);
                                $payIndex = 0;
                                $saleAmountRemaining = $amount;
                                foreach ($invoiceIDs as $inID) {
                                    $theInvoice = array();
                                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                                    $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");


                                    $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
                                    $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                                    $refNumber[]   =  $res1['refNumber'];
                                    $txnID      = $inID;
                                    $pay_amounts = 0;
                                    $amount_data = $res1['BalanceRemaining'];
                                    $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                    $ispaid      = 0;
                                    $isRun = 0;
                                    $BalanceRemaining = 0.00;

                                    if($amount_data == $actualInvoicePayAmount){
                                        $actualInvoicePayAmount = $amount_data;
                                        $isPaid      = 1;

                                    }else{

                                        $actualInvoicePayAmount = $actualInvoicePayAmount;
                                        $isPaid      = 0;
                                        $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                        
                                    }
                                    $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;
                                    $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
                                    $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                    if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                        $theInvoice = current($targetInvoiceArray);

                                       
                                        $createPaymentObject = [
                                            "TotalAmt" => $actualInvoicePayAmount,
                                            "SyncToken" => 1, 
                                            "CustomerRef" => $customerID,
                                            "Line" => [
                                                "LinkedTxn" => [
                                                    "TxnId" => $inID,
                                                    "TxnType" => "Invoice",
                                                ],
                                                "Amount" => $actualInvoicePayAmount
                                            ]
                                        ];
            
                                        $paymentMethod = $this->general_model->qbo_payment_method('Check', $this->merchantID, true);
                                        if($paymentMethod){
                                            $createPaymentObject['PaymentMethodRef'] = [
                                                'value' => $paymentMethod['payment_id']
                                            ];
                                        }
	                                    if(isset($trID) && $trID != '')
	                                    {
			                                $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
			                            }
            
                                        $newPaymentObj = Payment::create($createPaymentObject);

                                        $savedPayment = $dataService->Add($newPaymentObj);
                                        $this->session->set_flashdata('success', ' Transaction Successful');

                                        $error = $dataService->getLastError();
                                        if ($error != null) {
                                            $err = '';
                                            $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                            $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                            $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                            $st = '0';
                                            $action = 'Pay Invoice';
                                            $msg = "Payment Success but " . $error->getResponseBody();

                                            $qbID = $trID;
                                            $pinv_id = '';
                                        } else {
                                            $pinv_id = '';
                                            $pinv_id        =  $savedPayment->Id;
                                            $st = '1';
                                            $action = 'Pay Invoice';
                                            $msg = "Payment Success";
                                            $qbID = $trID;
                                        }
                                        $qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                        $id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $merchantID, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                                    }
                                    $payIndex++;
                                }
                            }else{

                                $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gatlistval, $gt_result['gatewayType'],$customerID,$amount,$merchantID, '', $this->resellerID,$inID='', true, $this->transactionByUser, $custom_data_fields);
                                $this->session->set_flashdata('success', ' Transaction Successful');
                            }

                            if($chh_mail =='1')
                            {
                                $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $res['transactionId']);
                            }
                        }else{
                            $res['transactionCode'] = $response->responseCode;
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gatlistval, $gt_result['gatewayType'],$customerID,$amount,$merchantID,$trID, $this->resellerID,$inID='', true, $this->transactionByUser, $custom_data_fields);
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $msg . '</div>');
                        }
                    }else{
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Invalid Request</strong>.</div>');
                        redirect('QBO_controllers/Payments/create_customer_esale');

                    }
                } catch (Exception $e)
                {
                    $error='Transaction Failed - ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    redirect('QBO_controllers/Payments/create_customer_esale');

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
                redirect('QBO_controllers/Payments/create_customer_esale');

            }

            $receipt_data = array(
                'transaction_id' => $response->transactionId,
                'IP_address' => getClientIpAddr(),
                'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
                'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact' => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url' => 'QBO_controllers/Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header' => 'Sale',
                'checkPlan' => $checkPlan
            );
            $this->session->set_userdata("receipt_data",$receipt_data);
            redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
        }
        redirect('QBO_controllers/Payments/create_customer_esale');
    }

    /*****************Refund Transaction***************/
    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }elseif ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $ref_inv  = '';
        if (!empty($this->input->post(null, true))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnstrID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiKey  =  $gt_result['gatewayPassword'];

            $gatewayName = getGatewayName($gt_result['gatewayType']);
            $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "Heartland";

            $customerID = $paydata['customerListID'];

            $amount = $total = $paydata['transactionAmount'];
            if(isset($_POST['ref_amount'])){
                $total  = $this->czsecurity->xssCleanPostInput('ref_amount');
                $amount = $total;
            }

            $user_id = $merchantID;
            $config     = new PorticoConfig();

            $config->secretApiKey = $apiKey;
            $config->serviceUrl   = $this->config->item('GLOBAL_URL');

            ServicesContainer::configureService($config);
            $error = '';
            try{

                $response = Transaction::fromId($tID)
                    ->refund($amount)
                    ->withCurrency("USD")
                    ->execute();

                if($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
                    $msg        = $response->responseMessage;
                    $tr1ID      = $response->transactionId;
                    $pay_status = 'SUCCESS';
                    
                    $res        = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $tr1ID);
                    
                   $val = array(
                        'merchantID' => $paydata['merchantID'],
                    );
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];
                    $dataService = DataService::Configure(array(
                        'auth_mode' => $this->config->item('AuthMode'),
                        'ClientID'  => $this->config->item('client_id'),
                        'ClientSecret' => $this->config->item('client_secret'),
                        'accessTokenKey' =>  $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID' => $realmID,
                        'baseUrl' => $this->config->item('QBOURL'),
                    ));

                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");


                    $refund = $amount;
                    $ref_inv = explode(',', $paydata['invoiceID']);
                    if (!empty($paydata['invoiceID'])) {

                        $item_data = $this->qbo_customer_model->get_merc_invoice_item_data($paydata['invoiceID'],$paydata['merchantID']);
                                

                        if (!empty($item_data)) {
                            $lineArray = array();
                                   
                                    $i = 0;
                                    for ($i = 0; $i < count($item_data); $i++) {
                                        $LineObj = Line::create([
                                            "Amount"              => $item_data[$i]['itemQty']*$item_data[$i]['itemPrice'],
                                            "DetailType"          => "SalesItemLineDetail",
                                            "Description" => $item_data[$i]['itemDescription'],
                                            "SalesItemLineDetail" => [
                                                "ItemRef" => $item_data[$i]['itemID'],
                                                "Qty" => $item_data[$i]['itemQty'],
                                                "UnitPrice" => $item_data[$i]['itemPrice'],
                                                "TaxCodeRef" => [
                                                    "value" => (isset($item_data[$i]['itemTax']) && $item_data[$i]['itemTax']) ? "TAX" : "NON",
                                                ]
                                            ],
                    
                                        ]);
                                        $lineArray[] = $LineObj;
                                    }
                                    $acc_id = '';
                                    $acc_name = '';
                            $acc_data = $this->general_model->get_select_data('QBO_accounts_list', array('accountID'), array('accountName' => 'Undeposited Funds', 'merchantID' => $paydata['merchantID']));
                            if (!empty($acc_data)) {
                                $ac_value = $acc_data['accountID'];
                            } else {
                                $ac_value = '4';
                            }
                            $invoice_tax = $this->qbo_customer_model->get_merc_invoice_tax_data($paydata['invoiceID'],$paydata['merchantID']);
                            $theResourceObj = RefundReceipt::create([
                                "CustomerRef" => $paydata['customerListID'],
                                "Line" => $lineArray,
                                "DepositToAccountRef" =>  [
                                    "value" => $ac_value,
                                    "name" => "Undeposited Funds",
                                ],
                                "TxnTaxDetail" => [
                                    "TxnTaxCodeRef" => [
                                        "value" => (isset($invoice_tax['taxID']) && $invoice_tax['taxID']) ? $invoice_tax['taxID'] : "",
                                    ],
                                ],
                            ]);
                            $resultingObj = $dataService->Add($theResourceObj);
                            $error = $dataService->getLastError();

                            $ins_id = '';
                            if ($error != null) {


                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund ' . $error->getResponseBody() . '</strong></div>');
                            } else {
                                $ins_id = $resultingObj->Id;
                            }
                            $refnd_trr = array(
                                'merchantID' => $paydata['merchantID'], 'refundAmount' => $total,
                                'creditInvoiceID' => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                                'creditTxnID' => $ins_id, 'refundCustomerID' => $paydata['customerListID'],
                                'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'),

                            );

                            $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                        }
                    } else {

                        $ins_id = '';
                        $refnd_trr = array(
                            'merchantID' => $paydata['merchantID'], 'refundAmount' => $total,
                            'creditInvoiceID' => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                            'creditTxnID' => $ins_id, 'refundCustomerID' => $paydata['customerListID'],
                            'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'),

                        );

                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                    }

                    $this->qbo_customer_model->update_refund_payment($tID, 'NMI');
                    $this->session->set_flashdata('success', 'Successfully Refunded Payment');
                } else {
                    $msg   = $response->responseMessage;
                    $tr1ID = $response->transactionId;
                    $res   = array('trnsactionCode' => $response->responseCode, 'status' => $msg, 'transactionId' => $tr1ID);
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($res,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$paydata['transactionAmount'],$user_id,$crtxnID='', $this->resellerID,$paydata['invoiceTxnID']);
            } catch (BuilderException $e) {
                $error = 'Transaction Failed - ' . $e->getMessage();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
            } catch (ConfigurationException $e) {
                $error = 'Transaction Failed - ' . $e->getMessage();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
            } catch (GatewayException $e) {
                $error = 'Transaction Failed - ' . $e->getMessage();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
            } catch (UnsupportedTransactionException $e) {
                $error = 'Transaction Failed - ' . $e->getMessage();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
            } catch (ApiException $e) {
                $error = 'Transaction Failed - ' . $e->getMessage();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
            }

            if($error){
                redirect('QBO_controllers/Payments/payment_refund');
            }

            $receipt_data = array(
                'proccess_url' => 'QBO_controllers/Payments/payment_refund',
                'proccess_btn_text' => 'Process New Refund',
                'sub_header' => 'Refund',
            );
            
            $this->session->set_userdata("receipt_data",$receipt_data);
            $this->session->set_userdata("invoice_IDs",$ref_inv);
            
            if($paydata['invoiceTxnID'] == ''){
                $paydata['invoiceTxnID'] ='null';
            }
            if($paydata['customerListID'] == ''){
                $paydata['customerListID'] ='null';
            }
            if($res['transactionId'] == ''){
                $res['transactionId'] ='null';
            }
            redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$res['transactionId'],  'refresh');

        }
        redirect('QBO_controllers/Payments/payment_refund');
    }
}