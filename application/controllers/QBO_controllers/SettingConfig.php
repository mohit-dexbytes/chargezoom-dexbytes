<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class SettingConfig extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('general');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {

        } else if ($this->session->userdata('user_logged_in') != "") {

        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {

        redirect('QBO_controllers/home', 'refresh');
    }

    public function setting_customer_portal()
    {
        if($this->session->userdata('logged_in')){
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id = $data['login_info']['merchID'];
        
        }	
        else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id = $data['login_info']['merchantID'];
        }
        $codition           = array('merchantID' => $user_id);
        $rootDomain         = "https://demo.payportal.com/";
        if ($this->input->post(null, true)) {
            $enable      = $this->czsecurity->xssCleanPostInput('enable');
            $portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');
            $service_url = $this->czsecurity->xssCleanPostInput('service_url');
            $hepltext = $this->czsecurity->xssCleanPostInput('customer_help_text');
            $myInfo   = $this->czsecurity->xssCleanPostInput('myInfo');
            $myPass   = $this->czsecurity->xssCleanPostInput('myPass');
            $mySubsc  = $this->czsecurity->xssCleanPostInput('mySubscriptions');
            $image    = '';

            if (!empty($_FILES['picture']['name'])) {
                $config['image_library']  = 'uploads';
                $config['upload_path']    = 'uploads/merchant_logo/';
                $config['allowed_types']  = 'jpg|jpeg|png|gif';
                $config['file_name']      = time() . $_FILES['picture']['name'];
                $config['create_thumb']   = false;
                $config['maintain_ratio'] = false;
                $config['quality']        = '60%';
                $config['widht']          = LOGOWIDTH;
                $config['height']         = LOGOHEIGHT;
                $config['new_image']      = 'uploads/merchant_logo/';
                //Load upload library and initialize configuration
                $this->load->library('upload', $config);


                $this->upload->initialize($config);

                if ($this->upload->do_upload('picture')) {

                    $uploadData = $this->upload->data();
                    $picture    = $picture    = $uploadData['file_name'];
                } else {
                    $picture = '';
                }
                $image = $picture;
            }
            $url = "https://" . $portal_url . CUS_PORTAL . "/customer/";

            if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
                if (strpos($portal_url, ' ') > 0) {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error: Invalid portal url space not allowed.</strong></div>');
                    redirect('home/index', 'refresh');
                }

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error: Invalid portal url special character not allowed.</strong></div>');
                redirect('home/index', 'refresh');
            }

            $insert_data = array('customerPortal' => $enable, 'customerPortalURL' => $url, 'customerHelpText' => $hepltext, 'serviceurl' => $service_url, 'showInfoTab' => $myInfo, 'showPassword' => $myPass, 'showSubscription' => $mySubsc, 'merchantID' => $user_id, 'portalprefix' => $portal_url);

            $update_data = array('customerPortal' => $enable, 'customerPortalURL' => $url, 'customerHelpText' => $hepltext, 'serviceurl' => $service_url, 'showInfoTab' => $myInfo, 'showPassword' => $myPass, 'showSubscription' => $mySubsc, 'portalprefix' => $portal_url);
            if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $user_id)) > 0) {

                $Edit = $this->general_model->check_existing_edit_portalprefix('tbl_config_setting', $user_id, $portal_url);

                if ($Edit) {
                    $this->session->set_flashdata('message', 'Error: Customer Portal URL already exitsts. Please try again.');

                } else {

                    if ($image != "") {
                        $update_data['ProfileImage'] = $image;
                    }

                    $this->general_model->update_row_data('tbl_config_setting', $codition, $update_data);

                    $this->session->set_flashdata('success', 'Successfully Updated');
                }

                redirect(base_url('QBO_controllers/SettingConfig/setting_customer_portal'), 'refresh');

            } else {
                $check = $this->general_model->check_existing_portalprefix($portal_url);

                if ($check) {
                    $this->session->set_flashdata('message', 'Error: Customer Portal URL already exitsts. Please try again.');

                } else {
                    if ($image != "") {
                        $insert_data['ProfileImage'] = $image;
                    }

                    $this->general_model->insert_row('tbl_config_setting', $insert_data);

                    $this->session->set_flashdata('success', 'successfully Inserted'
                    );
                }

                redirect(base_url('QBO_controllers/SettingConfig/setting_customer_portal'), 'refresh');

            }
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['setting'] = $this->general_model->get_row_data('tbl_config_setting', $codition);
        if ($data['setting']['serviceurl'] == '') {
            $merchant                      = $this->general_model->get_select_data('tbl_merchant_data', array('weburl'), array('merchID' => $user_id));
            $data['setting']['serviceurl'] = $merchant['weburl'];
        }
        /* Check merchant plan type is VT */
        $planData = $this->general_model->chk_merch_plantype_data($user_id);
        $isPlanVT = 0;
		if(!empty($planData)){
			if($planData->merchant_plan_type == 'VT'){
				$isPlanVT = 1;
			}
		}
        $data['isPlanVT'] = $isPlanVT;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/page_customer_portal', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function check_url()
    {
        $res = array();

        if ($this->session->userdata('logged_in') != "") {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }else if ($this->session->userdata('user_logged_in') != "") {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $portal_url = $this->czsecurity->xssCleanPostInput('portal_url');

        if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
            if (strpos($portal_url, ' ') > 0) {

                $res = array('portal_url' => 'Space is not allowed in url', 'status' => 'false');
                echo json_encode($res);
                die;
            }

            $res = array('portal_url' => 'Special character not allowed in url', 'status' => 'false');
            echo json_encode($res);
            die;
        } else {
            $num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url));
            if ($num == 0) {
                $res = array('status' => 'success');
            } else if ($num == 1) {
                $num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url, 'merchantID' => $merchantID));
                if ($num == 1) {
                    $res = array('status' => 'success');
                } else {
                    $res = array('portal_url' => 'This is alreay used', 'status' => 'false');
                }

            } else {
                $res = array('portal_url' => 'This is alreay used', 'status' => 'false');
            }

            echo json_encode($res);
            die;

        }

       
    }

    public function create_template()
    {

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['id'];

        if ($this->czsecurity->xssCleanPostInput('templateName') != "") {

            $templateName = $this->czsecurity->xssCleanPostInput('templateName');
            $type         = $this->czsecurity->xssCleanPostInput('type');
            $fromEmail    = $this->czsecurity->xssCleanPostInput('fromEmail');
            $toEmail      = $this->czsecurity->xssCleanPostInput('toEmail');
            $addCC        = $this->czsecurity->xssCleanPostInput('ccEmail');
            $addBCC       = $this->czsecurity->xssCleanPostInput('bccEmail');
            $replyTo      = $this->czsecurity->xssCleanPostInput('replyEmail');
            $message      = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
            $subject      = $this->czsecurity->xssCleanPostInput('emailSubject');
            $createdAt    = date('Y-m-d H:i:s');

            if ($this->czsecurity->xssCleanPostInput('add_attachment')) {
                $add_attachment = '1';
            } else {
                $add_attachment = '0';
            }

            $insert_data = array('templateName' => $templateName,
                'templateType'                      => $type,
                'companyID'                         => $user_id,
                'fromEmail'                         => $fromEmail,
                'toEmail'                           => $toEmail,
                'addCC'                             => $addCC,
                'addBCC'                            => $addBCC,
                'replyTo'                           => $replyTo,
                'message'                           => $message,
                'emailSubject'                      => $subject,
                'attachedTo'                        => $add_attachment,

            );
            if ($this->czsecurity->xssCleanPostInput('tempID') != "") {
                $insert_data['updatedAt'] = date('Y-m-d H:i:s');
                $condition                = array('templateID' => $this->czsecurity->xssCleanPostInput('tempID'));
                $data['templatedata']     = $this->general_model->update_row_data('tbl_email_template', $condition, $insert_data);
            } else {
                $insert_data['createdAt'] = date('Y-m-d H:i:s');
                $id                       = $this->general_model->insert_row('tbl_email_template', $insert_data);
            }
            redirect('Settingmail/email_temlate', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->uri->segment('3')) {
            $temID                = $this->uri->segment('3');
            $condition            = array('templateID' => $temID);
            $data['templatedata'] = $this->general_model->get_row_data('tbl_email_template', $condition);

        }
        $data['types'] = $this->general_model->get_table_data('tbl_teplate_type', '');

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('pages/page_email_template', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function populate_state()
    {
        $this->load->model('general_model');
        $id   = $this->czsecurity->xssCleanPostInput('id');
        $data = $this->general_model->get_table_data('state', array('country_id' => $id));
        echo json_encode($data);
    }

    public function populate_city()
    {
        $this->load->model('general_model');
        $id   = $this->czsecurity->xssCleanPostInput('id');
        $data = $this->general_model->get_table_data('city', array('state_id' => $id));
        echo json_encode($data);
    }

    public function profile_setting()
    {
        if ($this->session->userdata('logged_in') != "") {
			$user_id = $this->session->userdata('logged_in')['merchID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$user_id = $this->session->userdata('user_logged_in')['merchantID'];
		}

        if (!empty($this->input->post(null, true))) {

            if (!empty($_FILES['picture']['name'])) {
                $config['upload_path']   = 'uploads/merchant_logo/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name']     = time() . $_FILES['picture']['name'];

                //Load upload library and initialize configuration
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('picture')) {
                    $uploadData = $this->upload->data();
                    $picture    = $picture    = base_url() . 'uploads/merchant_logo/' . $uploadData['file_name'];
                } else {
                    $picture = '';
                }
                $input_data['merchantProfileURL'] = $picture;
            }

            $input_data['merchantFullAddress'] = $this->czsecurity->xssCleanPostInput('merchantFullAddress');
            $input_data['merchantTagline']     = $this->czsecurity->xssCleanPostInput('tagline');
            $helptext                          = $this->czsecurity->xssCleanPostInput('tagline');
            $input_data['firstName']           = $this->czsecurity->xssCleanPostInput('firstName');
            $input_data['lastName']            = $this->czsecurity->xssCleanPostInput('lastName');
            $input_data['merchantEmail']       = $this->czsecurity->xssCleanPostInput('merchantEmail');

            $input_data['merchantAddress1']         = $this->czsecurity->xssCleanPostInput('merchantAddress1');
            $input_data['companyName']              = $this->czsecurity->xssCleanPostInput('companyName');
            $input_data['merchantContact']          = $this->czsecurity->xssCleanPostInput('merchantContact');
            $input_data['merchantAddress2']         = $this->czsecurity->xssCleanPostInput('merchantAddress2');
            $input_data['merchantCountry']          = $this->czsecurity->xssCleanPostInput('country');
            $input_data['merchantState']            = $this->czsecurity->xssCleanPostInput('state');
            $input_data['merchantCity']             = $this->czsecurity->xssCleanPostInput('city');
            $input_data['merchantZipCode']          = $this->czsecurity->xssCleanPostInput('merchantZipCode');
            $input_data['merchantAlternateContact'] = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
            $input_data['updatedAt']                = date('Y-m-d H:i:s');
            $input_data['weburl']                   = $this->czsecurity->xssCleanPostInput('weburl');
            $input_data['merchant_default_timezone'] = $this->czsecurity->xssCleanPostInput('merchant_default_timezone');

            if ($input_data['merchant_default_timezone']) {
                $session_data = $this->session->userdata('logged_in');
                if($session_data){
                    $session_data['merchant_default_timezone'] = $input_data['merchant_default_timezone'];
                    $this->session->set_userdata('logged_in', $session_data);  
                }else{
                    $session_data = $this->session->userdata('user_logged_in');
                    if($session_data){
                        $session_data['merchant_default_timezone'] = $input_data['merchant_default_timezone'];
                        $this->session->set_userdata('user_logged_in', $session_data);  
                    }
                }
            }

            $pre   = $this->czsecurity->xssCleanPostInput('prefix');
            $post  = $this->czsecurity->xssCleanPostInput('postfix');
            $array = array($pre, $post);
            $qq    = implode('-', $array);
            $data  = array(

                'prefix'     => $pre,
                'postfix'    => $post,
                'invoiceNo'  => $qq,
                'merchantID' => $user_id,

            );

            if (!empty($this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id)))) {

                $condition = array('merchantID' => $user_id);

              
                $this->general_model->update_row_data('tbl_merchant_invoices', $condition, $data);

            } else {

                $status_ins = $this->general_model->insert_row('tbl_merchant_invoices', $data);
                if ($status_ins) {

                    $this->session->set_flashdata('success', ' Successfully Inserted');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

                }
            }

            $id        = $this->czsecurity->xssCleanPostInput('merchID');
            $condition = array('merchID' => $user_id);
            
            $status    = $this->general_model->update_row_data('tbl_merchant_data', $condition, $input_data);
            if ($status) {
                $input_data['merchID'] = $user_id;
                
                $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$condition);
                if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                {
                    /* Start campaign in hatchbuck CRM*/  
                    $this->load->library('hatchBuckAPI');
                    $input_data['merchant_type'] = 'Merchant Update';        
                    
                    $resource = $this->hatchbuckapi->createContact($input_data);
                    if($status['statusCode'] == 400){
                        
                        $resource = $this->hatchbuckapi->updateContact($input_data);
                    }else{
                        $resource = $this->hatchbuckapi->updateContact($input_data);
                    }
                    /* End campaign in hatchbuck CRM*/
                }
                $this->session->set_flashdata('success', 'Successfully Updated');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

            }
            redirect('QBO_controllers/SettingConfig/profile_setting', 'refresh');

        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');

        $con                = array('merchID' => $user_id);
        $fix                = $this->general_model->get_row_data('tbl_merchant_data', $con);
        $invoice['invoice'] = $fix;
        $invoice['prefix']  = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));

        $country               = $this->general_model->get_table_data('country', '');
        $data['country_datas'] = $country;

        $state               = $this->general_model->get_table_data('state', '');
        $data['state_datas'] = $state;

        $city               = $this->general_model->get_table_data('city', '');
        $data['city_datas'] = $city;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/page_createprefix', $invoice);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    //-------------- To load the coming soon page ---------------//
    public function comingsoon()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('pages/page_comingsoon', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    //-------------- To load the API Key page ---------------//
    public function apikey()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in') != "") {
			$data['login_info'] = $this->session->userdata('logged_in');
			$user_id = $data['login_info']['merchID'];
		}else if ($this->session->userdata('user_logged_in') != "") {
			$data['login_info'] = $this->session->userdata('user_logged_in');
			$user_id = $data['login_info']['merchantID'];
		}

        $data['api_data'] = $this->general_model->get_table_data('tbl_merchant_api', array('MerchantID' => $user_id));
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/APIKey', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_api_key()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('api_name')) && !empty($this->czsecurity->xssCleanPostInput('domain_name'))) {
            if ($this->session->userdata('logged_in') != "") {
				$user_id = $this->session->userdata('logged_in')['merchID'];
			}else if ($this->session->userdata('user_logged_in') != "") {
				$user_id = $this->session->userdata('user_logged_in')['merchantID'];
			}

            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchID,companyName, merchantEmail'), array('merchID' => $user_id));

            $name   = $this->czsecurity->xssCleanPostInput('api_name');
            $domain = $this->czsecurity->xssCleanPostInput('domain_name');

            $dev_api_key = $this->general_model->get_random_number($m_data, 'DEV');
            $pro_api_key = $this->general_model->get_random_number($m_data, 'PRO');

            $api_data['MerchantID'] = $user_id;
            $api_data['APIKeyDev']  = $dev_api_key;
            $api_data['APIKeyPro']  = $pro_api_key;
            $api_data['AppName']    = $name;
            $api_data['Domain']     = $domain;
            $api_data['Status']     = 1;
            $api_data['APIPackage'] = 1;
            $indid                  = $this->general_model->insert_row('tbl_merchant_api', $api_data);
            if ($indid) {
                $this->session->set_flashdata('success', 'Successfully Created');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');
        }
        redirect('QBO_controllers/SettingConfig/apikey', 'refresh');
    }

    public function delete_api_key()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('apiID'))) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
            $apiID   = $this->czsecurity->xssCleanPostInput('apiID');
            $m_data  = $this->general_model->get_row_data('tbl_merchant_api', array('MerchantID' => $user_id, 'apiID' => $apiID));

            if (!empty($m_data)) {

                if ($this->general_model->delete_row_data('tbl_merchant_api', array('MerchantID' => $user_id, 'apiID' => $apiID))) {
                    $indid = $this->card_model->delete_dev_api_key(array('MerchantID' => $user_id, 'APIKeyDev' => $m_data['APIKeyDev']));
                    $this->session->set_flashdata('success', 'Success');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error</strong></div>');
                }

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Error, Invalid API Key</strong></div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');
        }

        redirect('QBO_controllers/SettingConfig/apikey', 'refresh');

    }
}
