<?php

ob_start();
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

class Update_payment extends CI_Controller
{
    private $resellerID;
    
	public function __construct()
	{
		parent::__construct();
			$this->load->config('globalpayments');
			
		$this->load->config('usaePay');
		$this->load->model('quickbooks');
		$this->load->model('general_model');
		$this->load->model('card_model');
		
	}
	

	
	public function index(){
	    
	   
	   
	 if(!empty($this->czsecurity->xssCleanPostInput('mid'))) 
	 {
	   
	    $marchant_id = $this->czsecurity->xssCleanPostInput('mid');
        $invoice_no = $this->czsecurity->xssCleanPostInput('invid');
        $token = $this->czsecurity->xssCleanPostInput('token');
        
        $val = array(
			'merchantID' => $marchant_id,
		);
		$data = $this->general_model->get_row_data('QBO_token',$val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		 $realmID      = $data['realmID'];
		$dataService = DataService::Configure(array(
    		 'auth_mode' => $this->config->item('AuthMode'),
			 'ClientID'  => $this->config->item('client_id'),
			 'ClientSecret' =>$this->config->item('client_secret'),
    		'accessTokenKey' =>  $accessToken,
    		'refreshTokenKey' => $refreshToken,
    		'QBORealmID' =>  $realmID,
    		 'baseUrl' => $this->config->item('QBOURL'),
		));
        $con = array('emailCode'=>$token);
        $checkCode = $this->general_model->get_num_rows('tbl_template_data', $con);
                
	    if($checkCode > 0)
	    {
	        $con = array('invoiceID'=>$invoice_no,'merchantID'=>$marchant_id);
    	    $result = $this->general_model->get_row_data('QBO_test_invoice', $con);
    	    
    	    $data['get_invoice'] = $result;
    	    $payamount = $result['BalanceRemaining'];
    	    $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoice_no, $marchant_id);
    	    $transactionByUser = ['id' => $in_data['Customer_ListID'], 'type' => 3];
    	    $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID'=>$marchant_id));
    		$resellerID = $rs_Data['resellerID'];
    	    //Invoice detail
    	  
    	      $con = array('merchantID'=>$marchant_id,'set_as_default'=>1);
    	      $get_gateway = $this->general_model->get_row_data('tbl_merchant_gateway', $con);
    	      $gateway_id = $get_gateway['gatewayID'];
    	      if($get_gateway['gatewayType']==5 )
    	      $data['userName']   =$get_gateway['gatewayUsername'];
    	      else
    	        $data['userName']   ='';
    	      
    	    $bill_email='';  
    	    
    	    if($this->czsecurity->xssCleanPostInput('pay'))
    	    {
             
				 $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
				 $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoice_no'");
				 $qblistID = '';
		
		
		        //Billing Data
		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
		        $cardtype = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
		        $address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
		        $zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
		        $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
		        $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
            		        
		        $savepaymentinfo = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('savepaymentinfo'));
    		    $sendrecipt = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('sendrecipt'));
				$customerID = $in_data['CustomerListID'];
				$companyID = $in_data['companyID'];
				$phone = $in_data['Phone'];
				//card data array
				//save card process
				 $card_data = array(
					'cardMonth'    =>$expmonth,
					'cardYear'	   =>$expyear, 
					'CardType'     =>$cardtype,
					'CustomerCard' =>$cnumber,
					'CardCVV'      =>$cvv, 
					'customerListID' =>$customerID, 
					'companyID'      =>$companyID,
					'merchantID'     => $marchant_id,
					'createdAt' 	 => date("Y-m-d H:i:s"),
					'Billing_Addr1'	 =>$address,
					'Billing_Addr2'	 =>$address2,	 
					'Billing_City'	 =>$city,
					'Billing_State'	 =>$state,
					'Billing_Country'	 =>$country,
					'Billing_Contact'	 =>$phone,
					'Billing_Zipcode'	 =>$zip,
				 );
		
					$condition_mail         = array('templateType'=>'5', 'merchantID'=>$marchant_id); 
					$ref_number =  $result['refNumber']; 
					$tr_date   =date('Y-m-d H:i:s');
					$toEmail = $in_data['userEmail']; 
					$company=$in_data['companyName']; 
					$customer = $in_data['fullName'];
		
		
				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
				{
					$theInvoice = current($targetInvoiceArray);	  
					
					$gateway = $get_gateway['gatewayType']; 
					$gateway = ($gateway == 9) ? '1' : $gateway;
					
					switch ($gateway) {
						case "1":
						    include APPPATH . 'third_party/nmiDirectPost.class.php';
    	                    include APPPATH . 'third_party/nmiCustomerVault.class.php';
                            //start NMI
                            $nmiuser   = $get_gateway['gatewayUsername'];
            		        $nmipass   = $get_gateway['gatewayPassword'];
            		        $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
            		        if($payamount > 0){
                            	$transaction1 = new nmiDirectPost($nmi_data); 
        						$transaction1->setCcNumber($cnumber);
        					    $expmonth =  $expmonth;
        						$exyear   = $expyear;
        						$exyear   = substr($exyear,2);
        						if(strlen($expmonth)==1){
        							$expmonth = '0'.$expmonth;
        						}
        					    $expry    = $expmonth.$exyear;  
        						$transaction1->setCcExp($expry);
        						$transaction1->setCvv($cvv);
        						$transaction1->setAmount($payamount);
        			            $transaction1->sale();
        			            // add level III data
								$level_request_data = [
									'transaction' => $transaction1,
									'card_no' => $cnumber,
									'merchID' => $marchant_id,
									'amount' => $payamount,
									'invoice_id' => $result['TxnID'],
									'gateway' => 1
								];
								$transaction1 = addlevelThreeDataInTransaction($level_request_data);
        					    $getwayResponse = $transaction1->execute(); 
        					    
        					    if( $getwayResponse['response_code']=="100")
        					    {
        					         $txnID      = $result['TxnID'];  
            						 $ispaid 	 = '1';
            						 $pay      = ($payamount);
        					      	 $remainbal  = $result['BalanceRemaining']- $payamount;
            						 $data   	 = array('IsPaid'=>$ispaid,'BalanceRemaining'=>$remainbal );
            						 $condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
        						     $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
									 
									if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
									{
										$theInvoice = current($targetInvoiceArray);
								
										$updatedInvoice = Invoice::update($theInvoice, [
											
											 "sparse " => 'true'
										]);
										$updatedResult = $dataService->Update($updatedInvoice);
									
										$newPaymentObj = Payment::create([
											"TotalAmt" => $amount,
											"SyncToken" => $updatedResult->SyncToken,
											"CustomerRef" => $updatedResult->CustomerRef,
											"Line" => [
											 "LinkedTxn" =>[
													"TxnId" => $updatedResult->Id,
													"TxnType" => "Invoice",
												],    
											   "Amount" => $amount
											]
										 ]);
								 
										$savedPayment = $dataService->Add($newPaymentObj);
										$qblistID=	$savedPayment->Id;
										$invoiceIDs=	$updatedResult->Id;
										$error = $dataService->getLastError();
										if($error){
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
										}
									} 
								}
								else{
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$getwayResponse['responsetext'].'</div>');  
									redirect($_SERVER['HTTP_REFERER']);
								}
									
									
								$transaction['transactionID']      = $getwayResponse['transactionid'];
								$transaction['transactionStatus']  = $getwayResponse['responsetext'];
								$transaction['transactionCode']    = $getwayResponse['response_code'];
								$transaction['transactionType']    = ($getwayResponse['type'])?$getwayResponse['type']:'auto-nmi';
								$transaction['transactionDate']    = date('Y-m-d H:i:s');  
								$transaction['transactionModified']    = date('Y-m-d H:i:s');  
								$transaction['invoiceID']          = $invoiceIDs;  
								$transaction['gatewayID']          = $gateway_id;
								$transaction['transactionGateway'] = $get_gateway['gatewayType'];
								$transaction['customerListID']     = $in_data['CustomerListID'];
								$transaction['qbListTxnID']        = $qblistID;
								$transaction['transactionAmount']  = $payamount;
								$transaction['merchantID']         = $marchant_id;
								$transaction['gateway']            = "NMI";
								$transaction['resellerID']         = $resellerID;
							  	$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction['transactionCode']);

								if(!empty($transactionByUser)){
								    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
								    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transaction);
            				    if(!empty($id)){
            				        if(!empty($savepaymentinfo)){
    	                                $this->card_model->process_card($card_data);
                			         }
                					//save card process end
                					if(!empty($sendrecipt)){
                					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
                					}
                					
								 $this->session->set_flashdata('success','Successfully Paid');  
								   redirect('update_payment/thankyou/');
								}
								else{
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Payment record not inserted</div>');  
								} 
        				    }
            	       
            	        break;
					case "2":
						
						include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
						$this->load->config('auth_pay');
						//Login in Auth
						$apiloginID       = $get_gateway['gatewayUsername'];
						$transactionKey   = $get_gateway['gatewayPassword'];
						if($payamount > 0){
							$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
							$transaction1->setSandbox($this->config->item('Sandbox'));
							$card_no  = $cnumber;
							$expmonth = $expmonth;
							$exyear   = $expyear;
							$exyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$amount =$payamount;
								$expry = $expmonth.$exyear;  
							$getwayResponse = $transaction1->authorizeAndCapture($payamount,$card_no,$expry);
						   if( $getwayResponse->response_code=="1")
						   {
							   if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
								{
									$theInvoice = current($targetInvoiceArray);
							
									$updatedInvoice = Invoice::update($theInvoice, [
										 "sparse " => 'true'
									]);
									$updatedResult = $dataService->Update($updatedInvoice);
								
									$newPaymentObj = Payment::create([
										"TotalAmt" => $amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
										 "LinkedTxn" =>[
												"TxnId" => $updatedResult->Id,
												"TxnType" => "Invoice",
											],    
										   "Amount" => $amount
										]
									 ]);
							 
									$savedPayment = $dataService->Add($newPaymentObj);
									$qblistID=	$savedPayment->Id;
									$invoiceIDs=	$updatedResult->Id;
									$error = $dataService->getLastError();
									if($error){
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
									}
								}
								else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');  
								}
								
								$txnID      = $result['invoiceID'];  
								$ispaid 	 = '1';
								$pay        = $payamount;
								$remainbal  = $result['BalanceRemaining'] - $pay;
								$data   	 = array('IsPaid'=>$ispaid, 'BalanceRemaining'=>$remainbal );
								$condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
								$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
						   }
						   
						   else{
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$getwayResponse->response_reason_text .'</div>');  
									redirect($_SERVER['HTTP_REFERER']);
						   }
						   
							$transactiondata= array();
							$transactiondata['transactionID']       = $getwayResponse->transaction_id;
							$transactiondata['transactionStatus']   = $getwayResponse->response_reason_text;
							$transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
							$transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
							$transactiondata['transactionCode']     = $getwayResponse->response_code;  
							$transactiondata['transactionCard']     = substr($getwayResponse->account_number,4);  
							$transactiondata['transactionType'] = ($getwayResponse['type'])?$getwayResponse['type']:'Auth';	   
							$transactiondata['gatewayID']           = $gateway_id;
							$transactiondata['transactionGateway']  = $get_gateway['gatewayType'];
							$transactiondata['customerListID']      = $in_data['CustomerListID'];
							$transactiondata['transactionAmount']   = $payamount;
							$transactiondata['invoiceID']           = $invoiceIDs;  
							$transactiondata['qbListTxnID']         = $qblistID;
							$transactiondata['merchantID']          = $marchant_id;
							$transactiondata['gateway']             = "Auth";
							$transactiondata['resellerID']          = $resellerID;
					  
						  	$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transactiondata['transactionCode']);
						  	if(!empty($transactionByUser)){
							    $transactiondata['transaction_by_user_type'] = $transactionByUser['type'];
							    $transactiondata['transaction_by_user_id'] = $transactionByUser['id'];
							}
							$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
							if(!empty($id)){
							    if(!empty($savepaymentinfo)){
	                                $this->card_model->process_card($card_data);
            			         }
            					//save card process end
            					if(!empty($sendrecipt)){
            					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
            					}
								 $this->session->set_flashdata('success','Successfully Paid');  
								   redirect('update_payment/thankyou/');
							}
							else{
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Payment record not inserted</div>');  
							}
						   
						}
						
						break;
					case "3":
						
						include APPPATH . 'third_party/PayTraceAPINEW.php';
						$this->load->config('paytrace');
						$payusername   = $get_gateway['gatewayUsername'];
						$paypassword   = $get_gateway['gatewayPassword'];
						$integratorId  = $get_gateway['gatewaySignature'];

						$grant_type    = "password";
						
						$name = $fname." ".$lname;
						if($payamount > 0){
							
							$expmonth = $expmonth;
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						  $amount = $payamount;
							$payAPI  = new PayTraceAPINEW();	
							$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
							//call a function of Utilities.php to verify if there is any error with OAuth token. 
							$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
							
							if(!$oauth_moveforward){ 
								$json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
								//set Authentication value based on the successful oAuth response.
								//Add a space between 'Bearer' and access _token 
								$oauth_token = sprintf("Bearer %s",$json['access_token']);
								$request_data = array(
									"amount"            => $payamount,
									"credit_card"       => array (
										"number"            => $cnumber,
										"expiration_month"  =>$expmonth,
										"expiration_year"   =>$expyear
									),
									
									"csc"               => $cvv,
									"invoice_id"        =>$invoice_no,
									
									"billing_address"=> array(
										"name"          =>$name,
										"street_address"=> $address,
										"city"          => $city,
										"state"         => $state,
										"zip"           => $zip
									)
								);  
						 
							   $request_data = json_encode($request_data); 
							   $gatewayres    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
							   $response  = $payAPI->jsonDecode($gatewayres['temp_json_response']); 
							   if ( $gatewayres['http_status_code']=='200' )
							   {
							   		// add level three data in transaction
				                    if($response['success']){
				                        $level_three_data = [
				                            'card_no' => $cnumber,
				                            'merchID' => $marchant_id,
				                            'amount' => $payamount,
				                            'token' => $oauth_token,
				                            'integrator_id' => $integratorId,
				                            'transaction_id' => $response['transaction_id'],
				                            'invoice_id' => $invoice_no,
				                            'gateway' => 3,
				                        ];
				                        addlevelThreeDataInTransaction($level_three_data);
				                    }

								   if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
									{
										$theInvoice = current($targetInvoiceArray);
								
										$updatedInvoice = Invoice::update($theInvoice, [
											 "sparse " => 'true'
										]);
										$updatedResult = $dataService->Update($updatedInvoice);
									
										$newPaymentObj = Payment::create([
											"TotalAmt" => $amount,
											"SyncToken" => $updatedResult->SyncToken,
											"CustomerRef" => $updatedResult->CustomerRef,
											"Line" => [
											 "LinkedTxn" =>[
													"TxnId" => $updatedResult->Id,
													"TxnType" => "Invoice",
												],    
											   "Amount" => $amount
											]
										 ]);
								 
										$savedPayment = $dataService->Add($newPaymentObj);
										$qblistID=	$savedPayment->Id;
										$invoiceIDs=	$updatedResult->Id;
										$error = $dataService->getLastError();
										if($error){
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
										}
									}
									else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');  
									}
								   
									$txnID      = $result['invoiceID'];  
									$ispaid 	 = '1';
									$pay        = $payamount;
									$remainbal  = $result['BalanceRemaining']-$pay;
									$data   	 = array('IsPaid'=>$ispaid, 'BalanceRemaining'=>$remainbal );
									$condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
									$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
							   }
							   else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$response['status_message'].'</div>'); 
										redirect($_SERVER['HTTP_REFERER']);
								}
									
								$transactiondata= array();
								if(isset($response['transaction_id'])){
									$transactiondata['transactionID']   = $response['transaction_id'];
								}
								else{
									$transactiondata['transactionID']   = '';
								}
								$transactiondata['transactionStatus']   = $response['status_message'];
								$transactiondata['transactionDate']     = date('Y-m-d H:i:s'); 
									$transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
								$transactiondata['transactionCode']     = $gatewayres['http_status_code'];
								$transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
								$transactiondata['transactionType']     = ($response['type'])?$response['type']:'Pay-sale';
								$transactiondata['gatewayID']           = $gateway_id;
								$transactiondata['transactionGateway']  = $get_gateway['gatewayType'];							
								$transactiondata['customerListID']      = $in_data['CustomerListID'];
								$transactiondata['invoiceID']        	= $invoiceIDs;  
								$transactiondata['qbListTxnID']        	= $qblistID;
								$transactiondata['transactionAmount']   = $payamount;
								$transactiondata['merchantID']          = $marchant_id;
								$transactiondata['gateway']             = "Paytrace";
								$transactiondata['resellerID']          = $resellerID;
							   	$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transactiondata['transactionCode']);
							   	if(!empty($transactionByUser)){
								    $transactiondata['transaction_by_user_type'] = $transactionByUser['type'];
								    $transactiondata['transaction_by_user_id'] = $transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
								if(!empty($id)){
								    if(!empty($savepaymentinfo)){
    	                                $this->card_model->process_card($card_data);
                			         }
                					//save card process end
                					if(!empty($sendrecipt)){
                					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
                					}
                					
									 $this->session->set_flashdata('success','Successfully Paid');  
									   redirect('update_payment/thankyou/');
								}
								else{
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Payment record not inserted</div>');  
								}
							   
							}
			
						}
						
						break;
					case "4":
						include APPPATH . 'third_party/PayPalAPINEW.php';
						$this->load->config('paypal');
					  
						$config = array(
							'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
							'APIUsername' => $get_gateway['gatewayUsername'], 	// PayPal API username of the API caller
							'APIPassword' => $get_gateway['gatewayPassword'],	// PayPal API password of the API caller
							'APISignature' => $get_gateway['gatewaySignature'], 	// PayPal API signature of the API caller
							'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
							'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
						  );
						  $this->load->library('paypal/Paypal_pro', $config);	  
						  if($config['Sandbox'])
							{
								error_reporting(E_ALL);
								ini_set('display_errors', '1');
							}
						
						//Billing Data
						$fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
						$lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
						$cardtype = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
						$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
						$address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
						$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
						$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
						$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
						$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
						$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
						$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
						$expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
						$cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
						$name = $fname." ".$lname;
						if($payamount > 0)
						{
							$amount       = $payamount;
							$creditCardType   = 'Visa';
							$creditCardNumber = $cnumber;
							$expDateMonth     = $expmonth;
							$expDateYear      = $expyear;
							$creditCardType   = ($cardtype)?$cardtype:$creditCardType;
							$padDateMonth 	  = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
							$cvv2Number       =   $cvv;
							$currencyID       = "USD";
							
							$firstName = $fname;
							$lastName =  $lname; 
							$address1 = $address; 
							$address2 = $address2; 
							$country  = $country; 
							$city     = $city;
							$state    = $state;		
							$zip  = $zip;  
							$email = $bill_email; 
											
							$DPFields = array(
								'paymentaction' => 'Sale', 	// How you want to obtain payment.  
								'ipaddress' => '', 							// Required.  IP address of the payer's browser.
								'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
							);
											
							$CCDetails = array(
								'creditcardtype' => $cardtype, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
								'acct'           => $cnumber, 								// Required.  Credit card number.  No spaces or punctuation.  
								'expdate'        => $expmonth.$expyear, 							// Required.  Credit card expiration date.  Format is MMYYYY
								'cvv2'           => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
								'startdate'      => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
								'issuenumber'    => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
							);
											
							$PayerInfo = array(
								'email'          => $bill_email, 								// Email address of payer.
								'payerid'        => '', 							// Unique PayPal customer ID for payer.
								'payerstatus'    => 'verified', 						// Status of payer.  Values are verified or unverified
								'business'       => '' 							// Payer's business name.
							);  
											
							$PayerName = array(
								'salutation'     => '', 						// Payer's salutation.  20 char max.
								'firstname'      => $fname, 							// Payer's first name.  25 char max.
								'middlename'     => '', 						// Payer's middle name.  25 char max.
								'lastname'       => $lname, 							// Payer's last name.  25 char max.
								'suffix'         => ''								// Payer's suffix.  12 char max.
							);
										
							$BillingAddress = array(
								'street'         => $address1, 						// Required.  First street address.
								'street2'        => $address2, 						// Second street address.
								'city'           => $city, 							// Required.  Name of City.
								'state'          => $state, 							// Required. Name of State or Province.
								'countrycode'    => $country, 					// Required.  Country code.
								'zip'            => $zip 						// Phone Number of payer.  20 char max.
							);
						
												
						   $PaymentDetails = array(
								'amt'            => $payamount,					// Required.  Three-letter currency code.  Default is USD.
								'itemamt'        => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt'    => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt'   => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt'    => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt'    => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt'         => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc'           => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom'         => '', 						// Free-form field for your own use.  256 char max.
								'invnum'         => '', 						// Your own invoice or tracking number
								'buttonsource'   => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl'      => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring'      => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

							$PayPalRequestData = array(
								'DPFields'       => $DPFields, 
								'CCDetails'      => $CCDetails, 
								'PayerInfo'      => $PayerInfo, 
								'PayerName'      => $PayerName, 
								'BillingAddress' => $BillingAddress, 
								'PaymentDetails' => $PaymentDetails, 
								
							);
								
							$PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
							if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
							{
							  
								$code ='200';
								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
								{
									$theInvoice = current($targetInvoiceArray);
							
									$updatedInvoice = Invoice::update($theInvoice, [
										 "sparse " => 'true'
									]);
									$updatedResult = $dataService->Update($updatedInvoice);
									$newPaymentObj = Payment::create([
										"TotalAmt" => $amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
											 "LinkedTxn" =>[
													"TxnId" => $updatedResult->Id,
													"TxnType" => "Invoice"
											  ],    
										   "Amount" => $amount
										]
									 ]);
							 
									$savedPayment = $dataService->Add($newPaymentObj);
									$qblistID=	$savedPayment->Id;
									$invoiceIDs=	$updatedResult->Id;
									$error = $dataService->getLastError();
									if($error){
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
									}
								}
								else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');  
								}
								$txnID      = $result['invoiceID'];  
								$ispaid 	 = '1';
								$pay        = $payamount;
								$remainbal  = $result['BalanceRemaining']- $pay;
								$data   	 = array('IsPaid'=>$ispaid, 'BalanceRemaining'=>$remainbal );
								$condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
								$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
							 }
							 else{
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $PayPalResult["ACK"].'</div>'); 
									redirect($_SERVER['HTTP_REFERER']);
								}
								$transaction= array();
								$tranID ='' ;
								$amt='0.00';
								if(isset($PayPalResult['TRANSACTIONID'])) { 
									$tranID = $PayPalResult['TRANSACTIONID'];   
									$amt=$PayPalResult["AMT"];  
								}
								
								$transaction['transactionID']       = $tranID;
								$transaction['transactionStatus']   = $PayPalResult["ACK"];
								$transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
									$transactiondata['transactionModified']    = date('Y-m-d H:i:s');  
								$transaction['transactionCode']     = $code;  
								$transaction['transactionType']     = ($PayPalResult['type'])?$PayPalResult['type']:'Paypal-sale';	
								$transaction['gatewayID']           = $gateway_id;
								$transaction['transactionGateway']  = $get_gateway['gatewayType'];					
								$transaction['customerListID']      = $in_data['CustomerListID'];
								$transaction['transactionAmount']   = $payamount;
								$transaction['invoiceID']           = $invoiceIDs;  
								$transaction['qbListTxnID']         = $qblistID;
								$transaction['merchantID']          = $marchant_id;
								$transaction['gateway']             = "Paypal";
								$transaction['resellerID']          = $resellerID;
								$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction['transactionCode']);
								if(!empty($transactionByUser)){
								    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
								    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transaction);
								if(!empty($id)){
								    if(!empty($savepaymentinfo)){
    	                                $this->card_model->process_card($card_data);
                			         }
                					//save card process end
                					if(!empty($sendrecipt)){
                					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
                					}
									$this->session->set_flashdata('success','Successfully Paid');  
									redirect('update_payment/thankyou/');
								}
								else{
								   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Payment record not inserted</div>');  
								}
								
							
						}else{
							$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> No due payment</div>'); 
							 redirect($_SERVER['HTTP_REFERER']);
						}
						
						break;
					case "5":
						include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
			
						$token = $this->czsecurity->xssCleanPostInput('stripeToken'); 
					 
						if($payamount > 0 && $token !='')
						{
							
							$paidamount =  (int)($payamount*100);
							$amount     = $payamount;
														
							$plugin = new ChargezoomStripe();
							$plugin->setApiKey($gt_result['gatewayPassword']);
							$charge =	\Stripe\Charge::create(array(
								  "amount" => $paidamount,
								  "currency" => "usd",
								  "source" => $token, // obtained with Stripe.js
								  "description" => "Charge Using Stripe Gateway",
								 
								));	
							$charge= json_encode($charge);
							
							$resultstripe = json_decode($charge);
							
						   
							$trID='';
							if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
							 {
								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
								{
									$theInvoice = current($targetInvoiceArray);
							
									$updatedInvoice = Invoice::update($theInvoice, [
										 "sparse " => 'true'
									]);
									$updatedResult = $dataService->Update($updatedInvoice);
								
									$newPaymentObj = Payment::create([
										"TotalAmt" => $amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
										 "LinkedTxn" =>[
												"TxnId" => $updatedResult->Id,
												"TxnType" => "Invoice",
											],    
										   "Amount" => $amount
										]
									 ]);
							 
									$savedPayment = $dataService->Add($newPaymentObj);
									$qblistID=	$savedPayment->Id;
									$invoiceIDs=	$updatedResult->Id;
									$error = $dataService->getLastError();
									if($error){
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
									}
								}
								else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');  
								}
								$trID       = $resultstripe->id;
								$code		=  '200';
								$txnID      = $result['invoiceID'];  
								$ispaid 	= '1';
								$pay        = $amount;
								$remainbal  = $result['BalanceRemaining']-$pay;
								$data   	= array('IsPaid'=>$ispaid,  'BalanceRemaining'=>$remainbal );
								$condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
								$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
							}
							 else{
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $resultstripe->status .'</div>'); 
									redirect($_SERVER['HTTP_REFERER']);
								}
								
							$transaction['transactionID']       = $trID;
							$transaction['transactionStatus']   = $resultstripe->status;
							$transaction['transactionDate']     = date('Y-m-d H:i:s');  
							$transaction['transactionModified'] = date('Y-m-d H:i:s');  
							$transaction['transactionCode']     = $code;  
							$transaction['invoiceID']        	= $invoiceIDs;  
							$transaction['qbListTxnID']        	= $qblistID;
							$transaction['transactionType']     = ($resultstripe->type)?$resultstripe->type:'Stripe';		
							$transaction['gatewayID']           = $gateway_id;
							$transaction['transactionGateway']  = $get_gateway['gatewayType'];					
							$transaction['customerListID']      = $in_data['CustomerListID'];
							$transaction['transactionAmount']   = $payamount;
							$transaction['merchantID']          = $marchant_id;
							$transaction['gateway']             = "Stripe";
							$transaction['resellerID']          = $resellerID;
							$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction['transactionCode']);
							if(!empty($transactionByUser)){
							    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
							    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
							}
							$id = $this->general_model->insert_row('customer_transaction',   $transaction); 
							
							if(!empty($id)){
							    if(!empty($savepaymentinfo)){
	                                $this->card_model->process_card($card_data);
            			         }
            					//save card process end
            					if(!empty($sendrecipt)){
            					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
            					}
                					
								$this->session->set_flashdata('success','Successfully Paid');  
								   redirect('update_payment/thankyou/');
							}
							else{
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Payment record not inserted</div>');  
									redirect($_SERVER['HTTP_REFERER']);
							}
							 
						}
						
						break;
						
						case "6":
					    require_once APPPATH."third_party/usaepay/usaepay.php";	
    		           
    		            
    		            //Billing Data
        		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
        		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
        		      
        		        
        		        $address1 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
        		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
        		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
        		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
        		        $zipcode = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
        		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
        		        $cardType = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
        		        $card_no = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
        		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
        		        $exyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
        		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
        		        
        		        
        		        
                        $payusername   = $get_gateway['gatewayUsername'];
	                    $password      = $get_gateway['gatewayPassword'];
                        $customerID    =  $in_data['CustomerListID'];
									
						$cvv='';	
        		       
        					 
						$invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						$transaction->key=$payusername;
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Public Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
                        $transaction->card = $card_no;
						$expyear   = substr($exyear,2);
						if(strlen($expmonth)==1){
							$expmonth = '0'.$expmonth;
						}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
                         
							$transaction->billfname = $fname;
							$transaction->billlname = $lname;
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity    = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zipcode;
							
							
							$transaction->shipfname = $fname;
							$transaction->shiplname = $lname;
							$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity    = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zipcode;
							
							$amount =$payamount;
                            
                         
							$transaction->amount = $amount;
							$transaction->Process();
                            
                            			        $error=''; 	
                            			        
                            			        
                             if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                             {			        
                                		
                                                     $msg = $transaction->result;
                                                      $trID = $transaction->refnum;
                                				      $tr_type  = 'sale';
                                				       $result1 =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
							     
							     
							     
							     
								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
								{
									$theInvoice = current($targetInvoiceArray);
							
									$updatedInvoice = Invoice::update($theInvoice, [
										 "sparse " => 'true'
									]);
									$updatedResult = $dataService->Update($updatedInvoice);
								
									$newPaymentObj = Payment::create([
										"TotalAmt" => $amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
										 "LinkedTxn" =>[
												"TxnId" => $updatedResult->Id,
												"TxnType" => "Invoice",
											],    
										   "Amount" => $amount
										]
									 ]);
							 
									$savedPayment = $dataService->Add($newPaymentObj);
									$qblistID=	$savedPayment->Id;
									$invoiceIDs=	$updatedResult->Id;
									$error = $dataService->getLastError();
									if($error){
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
									}
								}
								else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');  
								}
							
							
								
								$code		=  '200';
								$txnID      = $result['invoiceID'];  
								$ispaid 	= '1';
								$pay        = $amount;
								$remainbal  = $result['BalanceRemaining']-$pay;
								$data   	= array('IsPaid'=>$ispaid,  'BalanceRemaining'=>$remainbal );
								$condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
								$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
							    $trid = $this->general_model->insert_gateway_transaction_data($result1, 'sale',  $get_gateway['gatewayID'], $get_gateway['gatewayType'],$in_data['CustomerListID'],$amount,$marchant_id,$qblistID,$resellerID, $txnID, false, $transactionByUser);												 
                        		
                        		if(!empty($savepaymentinfo)){
	                                $this->card_model->process_card($card_data);
            			         }
            					//save card process end
            					if(!empty($sendrecipt)){
            					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
            					}
                        		
                        		
                        		$this->session->set_flashdata('success','Successfully Paid ');  
								redirect('update_payment/thankyou/');					
								
							}
							 else
							 {
							        
                                                     $msg = $transaction->result;
                                                      $trID = $transaction->refnum;
                                				      $tr_type  = 'sale';
                                				       $result1 =array('transactionCode'=>'300', 'status'=>$msg, 'transactionId'=> $trID );
                                	  $trid = $this->general_model->insert_gateway_transaction_data($result1, 'sale',  $get_gateway['gatewayID'], $get_gateway['gatewayType'],$in_data['CustomerListID'],$amount,$marchant_id,$qblistID,$resellerID, $result['invoiceID'], false, $transactionByUser);												 
                        				       
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $transaction->error .'</div>'); 
									redirect($_SERVER['HTTP_REFERER']);
								}
							
					
						break;
						   
                      case "7":
                        
    		           require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
    		           
    		            //Billing Data
        		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
        		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
        		        $cardType = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
        		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
        		        $address1 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
        		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
        		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
        		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
        		        $zipcode = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
        		        $card_no = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
        		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
        		        $exyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
        		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
        		        
        		                    $amount =  $payamount;
        		                     $payusername   = $get_gateway['gatewayUsername'];
									$secretApiKey   =  $get_gateway['gatewayPassword'];
									
								    $config = new PorticoConfig();
               
                                    $config->secretApiKey = $secretApiKey;
                                    $config->serviceUrl =  $this->config->item('GLOBAL_URL');
        		                    $customerID = $in_data['CustomerListID'] ;
        		                        ServicesContainer::configureService($config);
                                        $card = new CreditCardData();
                                        $card->number = $card_no;
                                        $card->expMonth = $expmonth;
                                        $card->expYear = $exyear;
                                        if($cvv!="")
                                        $card->cvn = $cvv;
                                       $card->cardType=$cardType;
                                   
                                        $address = new Address();
                                        $address->streetAddress1 = $address1;
                                        $address->city = $city;
                                        $address->state = $state;
                                        $address->postalCode = $zipcode;
                                        $address->country = $country;
                                        
        		         
                                        $invNo  =mt_rand(5000000,20000000);
                                     	try
                                        {
                                                 $response = $card->charge($amount)
                                                ->withCurrency("USD")
                                                ->withAddress($address)
                                                ->withInvoiceNumber($invNo)
                                                ->withAllowDuplicates(true)
                                                ->execute();
                            
                            			        $error=''; 	  
                                			   if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                                              {

													// add level three data
							                        $transaction = new Transaction();
							                        $transaction->transactionReference = new TransactionReference();
							                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
							                        $level_three_request = [
							                            'card_no' => $card_no,
							                            'amount' => $amount,
							                            'invoice_id' => $invNo,
							                            'merchID' => $marchant_id,
							                            'transaction_id' => $response->transactionId,
							                            'transaction' => $transaction,
							                            'levelCommercialData' => $levelCommercialData,
							                            'gateway' => 7
							                        ];
							                        addlevelThreeDataInTransaction($level_three_request);

                                                     $msg = $response->responseMessage;
                                                     $trID = $response->transactionId;
                                				     $code_data ="SUCCESS";
                                				      $tr_type  = 'sale';
                                				       $result1 =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                				 	$this->session->set_flashdata('success','Payment Successfully Updated');  
            										  if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
                        								{
                        									$theInvoice = current($targetInvoiceArray);
                        							
                        									$updatedInvoice = Invoice::update($theInvoice, [
                        										 "sparse " => 'true'
                        									]);
                        									$updatedResult = $dataService->Update($updatedInvoice);
                        								
                        									$newPaymentObj = Payment::create([
                        										"TotalAmt" => $amount,
                        										"SyncToken" => $updatedResult->SyncToken,
                        										"CustomerRef" => $updatedResult->CustomerRef,
                        										"Line" => [
                        										 "LinkedTxn" =>[
                        												"TxnId" => $updatedResult->Id,
                        												"TxnType" => "Invoice",
                        											],    
                        										   "Amount" => $amount
                        										]
                        									 ]);
                        							 
                        									$savedPayment = $dataService->Add($newPaymentObj);
                        									$qblistID=	$savedPayment->Id;
                        									$invoiceIDs=	$updatedResult->Id;
                        									$error = $dataService->getLastError();
                        									if($error){
                        										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
                        									}
                        								}
                        								else{
                        										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');  
                        								}
                        							 $trid = $this->general_model->insert_gateway_transaction_data($result1, 'sale',  $get_gateway['gatewayID'], $get_gateway['gatewayType'],$in_data['CustomerListID'],$amount,$marchant_id,$qblistID,$resellerID, $result['invoiceID'], false, $transactionByUser);												 
                        								$code		=  '200';
                        								$txnID      = $result['invoiceID'];  
                        								$ispaid 	= '1';
                        								$pay        = $amount;
                        								$remainbal  = $result['BalanceRemaining']-$pay;
                        								$applbal  = $result['Total_payment']+$pay;
                        								$data   	= array('IsPaid'=>$ispaid,  'BalanceRemaining'=>$remainbal,'Total_payment'=>$applbal );
                        								$condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
                        								$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
                        								
                        								
                        								if(!empty($savepaymentinfo)){
                        	                                $this->card_model->process_card($card_data);
                                    			         }
                                    					//save card process end
                                    					if(!empty($sendrecipt)){
                                    					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
                                    					}	 
                        									 
                        									 
                        								 $this->session->set_flashdata('success','Successfully Paid');  
								                         redirect('update_payment/thankyou/');
            									}
            									else
            									{
            										   
            										   $msg = $response->responseMessage;
                                                        $trID = $response->transactionId;
                                                         $result1 =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
            											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
            								     	    $trid = $this->general_model->insert_gateway_transaction_data($result1, 'sale',   $get_gateway['gatewayID'], $get_gateway['gatewayType'],$in_data['CustomerListID'],$payamount,$marchant_id,$crtxnID='',$resellerID, $result['invoiceID'], false, $transactionByUser);												 
                        							     redirect($_SERVER['HTTP_REFERER']);
            										}
            									
                                           }
                                           
                                           
                                             catch (BuilderException $e)
                                            {
                                                $error= 'Build Exception Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (ConfigurationException $e)
                                            {
                                                $error='ConfigurationException Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (GatewayException $e)
                                            {
                                                $error= 'GatewayException Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (UnsupportedTransactionException $e)
                                            {
                                                $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (ApiException $e)
                                            {
                                                $error=' ApiException Failure: ' . $e->getMessage();
                                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            
                                            
                                            if($error!="")
                                             redirect($_SERVER['HTTP_REFERER']);
        		        break;  
        		        
        		        
        		        case "8":
					   $this->load->config('cyber_pay');
        		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
        		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
        		        $flag = 'true';
        		        
        		        $address1 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
        		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
        		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
        		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
        		        $zipcode = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
        		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
        		        $cardType = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardType'));
        		        $card_no = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
        		        $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
        		        $exyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
        		        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
        		        
        		        $phone="4158880000"; $email="test@gmail.com"; $companyName='Dummy Company';
        		        
                          $option =array();
        				        $option['merchantID']     = $get_gateway['gatewayUsername'];
            			        $option['apiKey']         = $get_gateway['gatewayPassword'];
        						$option['secretKey']      = $get_gateway['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" => "test_payment"
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $fname,
        					"lastName"  =>$lname,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$tr_type  = 'sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        				
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					    $trID =   $api_response[0]['id'];
        					    $msg  =   $api_response[0]['status'];
        					  
        					    $code =   '200';
        					    
                                $result1 =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                            
							     
							     
								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
								{
									$theInvoice = current($targetInvoiceArray);
							
									$updatedInvoice = Invoice::update($theInvoice, [
										 "sparse " => 'true'
									]);
									$updatedResult = $dataService->Update($updatedInvoice);
								
									$newPaymentObj = Payment::create([
										"TotalAmt" => $amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
										 "LinkedTxn" =>[
												"TxnId" => $updatedResult->Id,
												"TxnType" => "Invoice",
											],    
										   "Amount" => $amount
										]
									 ]);
							 
									$savedPayment = $dataService->Add($newPaymentObj);
									$qblistID=	$savedPayment->Id;
									$invoiceIDs=	$updatedResult->Id;
									$error = $dataService->getLastError();
									if($error){
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');    
									}
								}
								else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in QBO</div>');  
								}
							
							
								
								$code		=  '200';
								$txnID      = $result['invoiceID'];  
								$ispaid 	= '1';
								$pay        = $amount;
								$remainbal  = $result['BalanceRemaining']-$pay;
								$data   	= array('IsPaid'=>$ispaid,  'BalanceRemaining'=>$remainbal );
								$condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
								$this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
							    $trid = $this->general_model->insert_gateway_transaction_data($result1, 'sale',  $get_gateway['gatewayID'], $get_gateway['gatewayType'],$in_data['CustomerListID'],$amount,$marchant_id,$qblistID,$resellerID, $txnID, false, $transactionByUser);												 
                        		
                        		if(!empty($savepaymentinfo)){
	                                $this->card_model->process_card($card_data);
            			         }
            					//save card process end
            					if(!empty($sendrecipt)){
            					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
            					}
                        		
                        		$this->session->set_flashdata('success','Successfully Paid');  
								redirect('update_payment/thankyou/');					
								
							}
							 else
							 {
							         $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                    $tr_type  = 'sale';
                                	$result1 =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Payment Failed '.$msg.'</strong></div>'); 
                                                  
                                	  $trid = $this->general_model->insert_gateway_transaction_data($result1, 'sale',  $get_gateway['gatewayID'], $get_gateway['gatewayType'],$in_data['CustomerListID'],$amount,$marchant_id,$qblistID,$resellerID, $result['invoiceID'], false, $transactionByUser);												 
                        				       
									$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $transaction->error .'</div>'); 
									redirect($_SERVER['HTTP_REFERER']);
								}
        				}  
        				catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$error.' </strong></div>');
        				}
					
						break;
						
						
					default:
					   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Link is nor valid</div>'); 
						redirect($_SERVER['HTTP_REFERER']);
				}
				
				}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invalid invoice</div>'); 
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
         	else
         	{
	         	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>This Link is not valid.</div>'); 
				redirect($_SERVER['HTTP_REFERER']);
	        }
		}
		else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>This Link is not valid.</div>'); 
	    	redirect($_SERVER['HTTP_REFERER']);
	    }
	    
	 }
	 else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invalid Request</div>'); 
	    	redirect($_SERVER['HTTP_REFERER']);
	    }   
	}
	  

	
}