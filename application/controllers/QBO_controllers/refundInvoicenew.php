<?php 
/* This controller has following opration for gateways
 * NMI, Authorize.net, Paytrace, Paypal, Stripe Payment Gateway Operations
 
 * Refund create_customer_refund
 * Single Invoice Payment transaction refund
 * merchantID ans resellerID are Private Member
 */
 


use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException; 
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Services\ReportingService;
 
 
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;
class RefundInvoice extends CI_Controller
{
    
     private $merchantID;
     private $resellerID;
    public function __construct()
	{
		parent::__construct();
		 $this->load->config('globalpayments');
		$this->load->config('auth_pay');
     	$this->load->config('paytrace');
     	$this->load->config('paypal');  
		$this->load->model('general_model');
		$this->load->model('card_model');
		 $this->load->model('QBO_models/qbo_customer_model');
	
		 if($this->session->userdata('logged_in')){
		$da['login_info']	= $this->session->userdata('logged_in');
		$this->merchantID	= $da['login_info']['merchID'];
	    $this->resellerID   = $this->session->userdata('logged_in')['resellerID'];
		}
		else if($this->session->userdata('user_logged_in')){
	
	   $this->merchantID = $this->session->userdata('user_logged_in')['merchantID'];
	   $rs_Data          = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID'=>$this->merchantID));
	   $this->resellerID = $rs_Data['resellerID'];
		}else{
			redirect('login','refresh');
		  }	

	}
	
	function index()
	{
	     redirect('OBO_controllers/Payments/payment_transaction','refresh');
	}

	public function create_customer_refund()
	{
	    
	    
	   
	  
	     if(!empty($this->czsecurity->xssCleanPostInput('pay_amount')))
	     {
	     $index   = $this->czsecurity->xssCleanPostInput('chekRef')-1;
	      $trID   = $this->czsecurity->xssCleanPostInput('trID');
	      $pay    = $this->czsecurity->xssCleanPostInput('pay_amount');
	       $refAmt = $pay[$index]; 
	
	      $invdata = $this->czsecurity->xssCleanPostInput('multi_inv');
	      $trID  = $invdata[$index];
	  
	     }
	       if(!empty($this->czsecurity->xssCleanPostInput('ref_amount')))
	       {
	           $refAmt = $this->czsecurity->xssCleanPostInput('ref_amount');
	             $trID   = $this->czsecurity->xssCleanPostInput('trID');
	       }
	       
	      
	          if(!empty($this->czsecurity->xssCleanPostInput('ref_invID')))
	       {
	             $invoiceID=  $this->czsecurity->xssCleanPostInput('ref_invID');
	             $index   = $this->czsecurity->xssCleanPostInput('chekRef')-1;
        	      $trID   = $this->czsecurity->xssCleanPostInput('trID');
        	      $pay    = $this->czsecurity->xssCleanPostInput('pay_amount');
        	       $refAmt = $pay[$index]; 
        	
        	      $invdata = $this->czsecurity->xssCleanPostInput('multi_inv');
        	      $trID  = $invdata[$index];
	       }
	       
	      

	      $paydata = $this->general_model->get_select_data('customer_transaction',
	      array('transactionID','transactionCard','transactionAmount','customerListID','merchantID','transactionGateway','gatewayID','invoiceTxnID','invoiceRefID','qbListTxnID'),array('id'=>$trID));
	      $con=array('id'=>$trID);
		 $gatlistval = $paydata['gatewayID'];
		  $tID       = $paydata['transactionID']; 
		 
		  
		 
	      if($paydata['transactionGateway']=='1' || $paydata['transactionGateway']=='9')
	      {
	         include APPPATH . 'third_party/nmiDirectPost.class.php';
	         
	          
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword'];
    		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
			  
			
				$transaction = new nmiDirectPost($nmi_data);
				 $customerID = $paydata['customerListID'];
				 
				
				 $amount     =  $paydata['transactionAmount']; 
				 $amount     = $refAmt;
				 
			    $transaction->setTransactionId($tID);  
			  
				$transaction->refund($tID,$amount);
				
				$result     = $transaction->execute();
				
				   
				 if($result['response_code'] == '100')
				 {  
		            $status="success";
                    $this->qbo_customer_model->update_refund($trID, 'NMI');	
					
			        $this->session->set_flashdata('success',' Success'); 
				 }
				 else
				 {	 
					$this->session->set_flashdata('success',$result['responsetext']); 
				 }
				      
	         $id = $this->general_model->insert_gateway_transaction_data($result,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$amount,$this->merchantID,$crtxnID='', $this->resellerID,$in_data['TxnID']);  
	          
	      }
	       if($paydata['transactionGateway']=='2')
	      {
	           include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
	           
	             
    			 $gatlistval = $paydata['gatewayID'];
				  $tID       = $paydata['transactionID']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
			  
			
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
                $transaction->setSandbox($this->config->item('Sandbox'));

				
				$merchantID = $paydata['merchantID'];
				
				 $card    = $paydata['transactionCard'];
				 $customerID = $paydata['customerListID'];
				 $amount     =  $paydata['transactionAmount']; 
			     $amount     =  $refAmt;
			  
				$result     = $transaction->credit($tID, $amount, $card);
					
				if($result->response_code == '1')
				{  
			        $status ='success';
				    $this->qbo_customer_model->update_refund_payment($tID, 'AUTH');	
			        $this->session->set_flashdata('success','Success'); 
				}
				else
				{
					 
					 
					
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->response_reason_text.'</div>'); 
				}
				   
	          
	      }
	       if($paydata['transactionGateway']=='3')
	      {
	          
	           include APPPATH . 'third_party/PayTraceAPINEW.php';
	           
	            
				
			   	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
			    $payAPI = new PayTraceAPINEW();
				$merchantID = $paydata['merchantID'];
				
				 $card    = $paydata['transactionCard'];
				 $customerID = $paydata['customerListID'];
				 $amount     =  $paydata['transactionAmount']; 
			     $amount     =  $refAmt;
			   $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
				 $amount1  =  $paydata['transactionAmount']; 
				  $total   = $refAmt;
				  $amount  =  $total;
				if($paydata['transactionCode']=='200'){
					 $request_data = array( "transaction_id" => $tID,'amount'=>$total );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_TRID_REFUND);
				     $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    		
				}
				
			   
				   if ( $result['http_status_code']=='200' )
				 {
			
				  $response['http_status_code'] =200;
				  $status = 'success';
				  $this->qbo_customer_model->update_refund_payment($tID, 'PAYTRACE');
				    $this->session->set_flashdata('success',$response['status_message']); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - </strong> '.$err_msg.'</div>'); 
                }
				 
				      
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed.</strong></div>'); 
		}		
				
			     
			     
	          
	          
	      } 
	      
	      if($paydata['transactionGateway']=='4')
	      {
	         include APPPATH . 'third_party/PayPalAPINEW.php';
	             	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
					  
					 if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);	
						
    		
				 $customerID = $paydata['customerListID'];
				 
				 
				 $amount     =  $paydata['transactionAmount']; 
				 	$merchantID =$paydata['merchantID']	;  
				 
				 	 $total   = $refAmt;
				 	 
				 	 if($amount==$total)
				 	 {
				 	    $restype="Full" ;
				 	 }else{
				 	    $restype="Partial" ;  
				 	 }
                    $amount     = $total ; 
					  
                   	$RTFields = array(
					'transactionid' => $tID, 							// Required.  PayPal transaction ID for the order you're refunding.
					'payerid' => '', 								// Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
					'invoiceid' => '', 								// Your own invoice tracking number.
					'refundtype' => $restype, 							// Required.  Type of refund.  Must be Full, Partial, or Other.
					'amt' =>  $amount, 									// Refund Amt.  Required if refund type is Partial.  
					'currencycode' => '', 							// Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
					'note' => '',  									// Custom memo about the refund.  255 char max.
					'retryuntil' => '', 							// Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
					'refundsource' => '', 							// Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
					'merchantstoredetail' => '', 					// Information about the merchant store.
					'refundadvice' => '', 							// Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
					'refunditemdetails' => '', 						// Details about the individual items to be returned.
					'msgsubid' => '', 								// A message ID used for idempotence to uniquely identify a message.
					'storeid' => '', 								// ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
					'terminalid' => ''								// ID of the terminal.  50 char max.
				);	
					
            		$PayPalRequestData = array('RTFields' => $RTFields);
            		
            		$PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);	  
            			
				
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
				{  
				     
				$code = '111';
			    $status ='success';
				     
				  $this->qbo_customer_model->update_refund_payment($tID, 'PAYPAL');    
		
					
			        $this->session->set_flashdata('success','Success'); 
				 }else{
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					  $code = '401';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$responsetext.'</div>'); 
				 }
				       $transactiondata= array();
				       	 $tranID ='' ;$amt='0.00';
					     
				      
	          
	      }
	       if($paydata['transactionGateway']=='5')
	      {
	         include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
 
	    	
	    	 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			   $amount     = $refAmt;
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 
    		    $plugin = new ChargezoomStripe();
            	$plugin->setApiKey($nmipass);
				$charge = \Stripe\Refund::create(array(
				  "charge" => $tID,
				  "amount"=>($amount*100)
				));
			  
				 
				 $customerID = $paydata['customerListID'];
	
				
			      $charge= json_encode($charge);
				  
				   $result = json_decode($charge);
			
				  
					  
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded'))
				 {  
				 $status = 'success';    
				$amount = ($result->amount/100) ;
				 $code ='200';
			    $trID = $result->id;
			  	
				  		     
				    $this->qbo_customer_model->update_refund_payment($tID, 'STRIPE');    
					 
		
					
			        $this->session->set_flashdata('success','Successfully Refunded'); 
				 }
				 else
				 {
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }      
				    
				 
	    	
	    	
	          
	      }
	      
	      
	       if($paydata['transactionGateway']=='6')
	      {
	        
	         require_once APPPATH."third_party/usaepay/usaepay.php";	
	    	
	    	 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			 
			       $amount        = $refAmt;
    			$payusername      = $gt_result['gatewayUsername'];
    		    $paypassword      =  $gt_result['gatewayPassword']; 
    		    $inID ='';
				 $customerID = $paydata['customerListID'];
				 $inID       = $paydata['invoiceTxnID'];
	            	$crtxnID='';	     
        	
                     $transaction=new umTransaction;
					 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
	 
					 $transaction->key=$payusername; 		// Your Source Key
					 $transaction->pin= $paypassword;		// Source Key Pin
					 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
					 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
					 $transaction->command="refund";    // refund command to refund transaction.
					 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
					 $transaction->amount = $amount ;
					 $customerID = $paydata['customerListID'];
					 
				 
					$transaction->Process();
					     
					    $trID1 ='';
					 if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                        
                         $msg = $transaction->result;
                         $trID1 = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID1 );
                          $this->customer_model->update_refund($trID, 'USAEPAY');    
					 
		                  $ins_id = '';
                          $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	          
            				  	           );	
            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                         $this->session->set_flashdata('success','Transaction Successfully Refunded'); 
                         
							 
							 
                     }
                     else
                     {
                       $trID1='';
                         $msg = $transaction->result;
                         $trID1 = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'300', 'status'=>$msg, 'transactionId'=> $trID1 );
                      
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>'); 
                         
                     }
                     
                     
                 
             
                       $id = $this->general_model->insert_gateway_transaction_data($res,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$refAmt,$this->merchantID,$crtxnID='', $this->resellerID,$inID);  
                 }
	             
	            
	       if($paydata['transactionGateway']=='7')
	      {
	        
	          require_once dirname(__FILE__) . '/../../../vendor/autoload.php'; 
	    	
	    	 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			 
			       $amount     = $refAmt;
    		    $secretApiKey  =  $gt_result['gatewayPassword']; 
    		   $inID ='';
				 $customerID = $paydata['customerListID'];
				 $inID       = $paydata['invoiceTxnID'];
	            	$crtxnID='';	     
        	
                 $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
              	
                  ServicesContainer::configureService($config);
               
			   try
			   {
			   
            
			       
			       
			     $response= Transaction::fromId($tID)
			    
                 ->refund($amount)
                 ->withCurrency("USD")
                 ->execute();
	            $tr1ID='';   $error='';
                 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                         $msg = $response->responseMessage;
                         $tr1ID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $tr1ID );
                          $this->customer_model->update_refund($trID, 'HEARTLAND');    
					 
		                  $ins_id = '';
                          $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$refAmt,
            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	          
            				  	           );	
            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                         $this->session->set_flashdata('success','Transaction Successfully Refunded'); 
                         
							 
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $tr1ID = $response->transactionId;
                           $res =array('trnsactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $tr1ID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Refund Process Failed</strong></div>'); 
                         
                     }
                     
                     
             
                       $id = $this->general_model->insert_gateway_transaction_data($res,'refund',$gatlistval,$gt_result['gatewayType'],$customerID,$refAmt,$this->merchantID,$crtxnID='', $this->resellerID,$inID);  
                  
			   }
			    catch (BuilderException $e)
                    {
                        $error= 'Build Exception Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='ConfigurationException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'GatewayException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error=' ApiException Failure: ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    
                    
	             } else if ($paydata['transactionGateway'] == '14') {
					include APPPATH . 'third_party/Cardpointe.class.php';

					$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

					$cardpointuser  = $gt_result['gatewayUsername'];
					$cardpointepass   = $gt_result['gatewayPassword'];
					$cardpointeMerchID = $gt_result['gatewayMerchantID'];
					$cardpointeSiteName  = $gt_result['gatewaySignature'];
					$client = new Cardpointe();
					$customerID  = $paydata['customerListID'];
					$amount = $paydata['transactionAmount'];
					$amount = $refAmt;

					$res = $client->refund($cardpointeSiteName, $cardpointeuser, $cardpointuser, $cardpointepass, $tID, $amount);

					if ($result['response_code'] == '100') {

						$refundStatus = true;

						$this->qbo_customer_model->update_refund($trID, 'Chargezoom');

						$this->session->set_flashdata('success', 'Successfully Updated');
					} else {

						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $result['responsetext'] . '</strong>.</div>');
					}
					$transactiondata                        = array();
					$transactiondata['transactionID']       = $result['transactionid'];
					$transactiondata['transactionStatus']   = $result['responsetext'];
					$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					$transactiondata['transactionType']     = $result['type'];
					$transactiondata['transactionCode']     = $result['response_code'];
					$transactiondata['transactionGateway']  = $paydata['transactionGateway'];
					$transactiondata['gatewayID']           = $gatlistval;
					$transactiondata['customerListID']      = $customerID;
					$transactiondata['transactionAmount']   = $amount;
					$transactiondata['merchantID']          = $this->merchantID;

					$transactiondata['invoiceID'] = $paydata['invoiceID'];

					$transactiondata['resellerID'] = $this->resellerID;
					$transactiondata['gateway']    = "Chargezoom";

					if(!empty($this->transactionByUser)){
						$transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						$transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
					}

					$id = $this->general_model->insert_row('customer_transaction', $transactiondata);

				}
	           
			 
	            $val = array(
    					'merchantID' => $paydata['merchantID'],
    				);
				
    				$merchID =$paydata['merchantID'];
        		  
			   
				 if(strtoupper($status)=="SUCCESS")
				{
		
			
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken   = $data['accessToken'];
				$refreshToken  = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
				'auth_mode' => 'oauth2',
				'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
				'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
				'accessTokenKey' =>  $accessToken,
				'refreshTokenKey' => $refreshToken,
				'QBORealmID' =>  $realmID,
				'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			   
			   
			         $refund =$amount;
				  $paydata['invoiceID'];
				    if(!empty($paydata['invoiceID']) )
				    {
				         
				     
			   
			   
            			   
            			   $item_data = $this->qbo_customer_model->get_invoice_item_data($paydata['invoiceID']);
            			   if(!empty($item_data))
            			   {
            			   $LineObj = Line::create([
                               "Amount" => $total,
                                "DetailType" => "SalesItemLineDetail",
                		            "SalesItemLineDetail" => [
                			        "ItemRef" =>$item_data['itemID'] ,
                			       ]
                			      
                			  ]);
                			  
                			  if(!empty($item_data['AssetAccountRef']))
                			  {
                			     $acc_id = $item_data['AssetAccountRef'];
                			     $acc_name = $item_data['AssetAccountName'];
                			   }
                			   if(!empty($item_data['IncomeAccountRef']))
                			   {
                			        $acc_id = $item_data['IncomeAccountRef'];
                			     $acc_name = $item_data['IncomeAccountName'];
                			   }
                			      if(!empty($item_data['ExpenseAccountRef']))
                			      {
                			         $acc_id = $item_data['ExpenseAccountRef'];
                			     $acc_name = $item_data['ExpenseAccountName'];  
                			      }
                		     $acc_data = $this->general_model->get_select_data('QBO_accounts_list',array('accountID'),array('accountName'=>'Undeposited Funds'));
                		     if(!empty($acc_data))
                		     {
                		          $ac_value =$acc_data['accountID'];
                		     }else{
                		        $ac_value ='4'; 
                		     }
                		   	$theResourceObj = RefundReceipt::create([
                    		    "CustomerRef" => $paydata['customerListID'], 
            		            "Line" => $LineObj,
            		          	"DepositToAccountRef" =>  [
                			        "value" =>$ac_value ,
                			        "name"=> "Undeposited Funds",
                			       ]
            		      ]);	  
                		$resultingObj = $dataService->Add($theResourceObj);
            			$error = $dataService->getLastError();	  
            			 
            			 
            				$err='';	
            					if ($error != null)
            				 {
            					 $err.='The Status code is: ' . $error->getHttpStatusCode() . "\n";
	                            $err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                $err.=$error->getResponseBody() . "\n";
            				
                                  	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund '.$err.'</strong></div>'); 
                              }
                               $ins_id = $resultingObj->Id;
                                $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
            				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	          
            				  	           );	
            				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
            			   }	
                      }
				
				}	
			   
	  
	      
	       if(!empty($this->czsecurity->xssCleanPostInput('ref_invID')))
	       {
	         	redirect('QBO_controllers/Create_invoice/Invoice_details');  
	       }
	    	redirect('QBO_controllers/Payments/payment_transaction');
	    
	    
	    
	}
	
	
	
	 public function getError($eee){ 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }
	 
	 
	
}	