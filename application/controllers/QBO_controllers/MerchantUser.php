<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
class MerchantUser extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
	

		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_customer_model');
		$this->load->model('QBO_models/qbo_company_model');
		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '1') {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
	}


	public function index()
	{


		redirect('QBO_controllers/home', 'refresh');
	}


	/********** Get Admin Details ********/

	public function admin_role()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];
		$conditin    = array('merchantID' => $user_id);

		$roles        = $this->general_model->get_table_data('tbl_role', $conditin);
		$rolenew = array();
		$authnew = array();

		foreach ($roles as $key => $role) {

			$auth = $this->general_model->get_auth_data($role['authID']);

			$role['authName'] = $auth;
			$authnew[$key] = $role;
		}

		$data['roles_data']      = $authnew;
		$rolename = $this->general_model->get_table_data('tbl_auth', '');
		$data['auths'] = $rolename;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_userrole', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/********** Add, Update and Edit Admin Roles ********/

	public function create_role()
	{

		if (!empty($this->input->post(null, true))) {


			$qq = implode(',', $this->czsecurity->xssCleanPostInput('role'));
			$input_data['roleName']  = $this->czsecurity->xssCleanPostInput('roleName');
			$input_data['authID'] = $qq;
			$merchantID  = $this->session->userdata('logged_in')['merchID'];
			$input_data['merchantID'] = $merchantID;

			if ($this->czsecurity->xssCleanPostInput('roleID') != "") {

				$id = $this->czsecurity->xssCleanPostInput('roleID');

				$condition = array('roleID' => $id);
				$update = $this->general_model->update_row_data('tbl_role', $condition, $input_data);
				if ($update) {


					$this->session->set_flashdata('success', 'Successfully Updated');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong...</div>');
				}
			} else {
				$insert = $this->general_model->insert_row('tbl_role', $input_data);
				if ($insert) {


					$this->session->set_flashdata('success', 'Successfully Inserted');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong...</div>');
				}
			}



			redirect(base_url('QBO_controllers/MerchantUser/admin_role'));
		}

		if ($this->uri->segment('3') != "") {

			$roleId = $this->uri->segment('3');
			$con = array('roleID' => $roleId);
			$data['role'] = $this->general_model->get_row_data('tbl_role', $con);
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id = $data['login_info']['merchID'];

		$rolename = $this->general_model->get_table_data('tbl_auth', '');

		$data['auths'] = $rolename;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_userrole', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/********** Get Role ID ********/

	public function get_role_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('role_id');
		$val = array(
			'roleID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_role', $val);
		echo json_encode($data);
	}


	/********** Delete Admin Role ********/



	public function delete_role()
	{

		$roleID = $this->czsecurity->xssCleanPostInput('roleID1');
		$condition =  array('roleID' => $roleID);

		$del      = $this->general_model->delete_row_data('tbl_role', $condition);
		if ($del) {


			$this->session->set_flashdata('success', 'Successfully Deleted');
		} else {


			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong...</div>');
		}

		redirect(base_url('QBO_controllers/MerchantUser/admin_role'));
	}






	/********** Get Admin Users Records ********/




	public function admin_user()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];

		$users = $this->general_model->get_merchant_user_data($user_id);
		$usernew = array();
		foreach ($users as $key => $user) {
			$auth = $this->general_model->get_auth_data($user['authID']);
			$user['authName'] = $auth;
			$usernew[$key] = $user;
		}

		$data['user_data']  = $usernew;

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/admin_user', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/********** Add, Update and Edit Admin Users ********/

	public function create_user()
	{
		$this->load->model('api_model');
		if ($this->session->userdata('logged_in')) {
            $data['login_info']      = $this->session->userdata('logged_in');
            $merchantID = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']      = $this->session->userdata('user_logged_in');
            $merchantID = $data['login_info']['merchantID'];
        }

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('userFname', 'userFname', 'required');
			$this->form_validation->set_rules('userLname', 'userLname', 'required');


			if ($this->form_validation->run() == true) {

				$input_data['userFname']	   = $this->czsecurity->xssCleanPostInput('userFname');
				$input_data['userLname']	   = $this->czsecurity->xssCleanPostInput('userLname');
				$input_data['userEmail']	   = $this->czsecurity->xssCleanPostInput('userEmail');

				$is_exist_data = [
					'email' => $input_data['userEmail']
				];

				if ($this->czsecurity->xssCleanPostInput('userID') == "") {
					$input_data['userPasswordNew']	= password_hash(
					    $this->czsecurity->xssCleanPostInput('userPassword'),
                        PASSWORD_BCRYPT
                    );
				} else {
					$is_exist_data['merchantUserID'] = $this->czsecurity->xssCleanPostInput('userID');
				}

				$input_data['userAddress']	   = $this->czsecurity->xssCleanPostInput('userAddress');
				$input_data['roleId']          = $this->czsecurity->xssCleanPostInput('roleID');
				$input_data['merchantID']	   = $merchantID;

				$is_exist = is_email_exists($is_exist_data);
				if ($is_exist) {
					$this->session->set_flashdata('message', '<strong>Error:</strong> ' . $is_exist);
				} else {
					if ($this->czsecurity->xssCleanPostInput('userID') != "") {

						$id = $this->czsecurity->xssCleanPostInput('userID');
						$chk_condition = array('merchantUserID' => $id, 'merchantID' => $merchantID);
						$updt = $this->general_model->update_row_data('tbl_merchant_user', $chk_condition, $input_data);
						if ($updt) {
							$this->session->set_flashdata('success', 'Successfully Updated');
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
						}
					} else {
						$insert = $this->general_model->insert_row('tbl_merchant_user', $input_data);
						if ($insert) {
							$this->session->set_flashdata('success', 'Successfully Created New User');
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
						}
					}
				}

				redirect(base_url('QBO_controllers/MerchantUser/admin_user'));
			}
		}

		if ($this->uri->segment('4')) {
			$userID  			  = $this->uri->segment('4');
			$con                = array('merchantUserID' => $userID, 'merchantID' => $merchantID);
			$data['user'] 	  = $this->general_model->get_row_data('tbl_merchant_user', $con);
			if(!$data['user'] || empty($data['user'])){
				$this->session->set_flashdata('error', 'Invalid Request');
				redirect(base_url('QBO_controllers/MerchantUser/admin_user'));
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info']     = $this->session->userdata('logged_in');
		$user_id = $data['login_info']['merchID'];

		$username = $this->general_model->get_table_data('tbl_auth', '');
		$data['auths'] = $username;

		$con1                = array('merchantID' => $user_id);
		$role = $this->general_model->get_table_data('tbl_role', $con1);
		$data['role_name'] = $role;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/adduser', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/**************Delete Admin Users********************/

	public function delete_user()
	{

		$userID = $this->czsecurity->xssCleanPostInput('merchantID1');
		$condition =  array('merchantUserID' => $userID);
		$del      = $this->general_model->delete_row_data('tbl_merchant_user', $condition);
		if ($del) {

			$this->session->set_flashdata('success', 'Successfully Deleted');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> MerchantID not found.</div>');
		}

		redirect(base_url('QBO_controllers/MerchantUser/admin_user'));
	}



	public function create_credit()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {

			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemPrice']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];

				$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['updatedAt'] = date('Y-m-d H:i:s');


				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
			if (!empty($in_data)) {
				$inv_pre   = $in_data['prefix'];
				$inv_po    = $in_data['postfix'] + 1;
				$new_inv_no = $inv_pre . $inv_po;
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

				redirect('QBO_controllers/home/invoices', 'refresh');
			}

			$ins_data['customerID']     = $this->czsecurity->xssCleanPostInput('customerID');
			$ins_data['creditDescription']     = $this->czsecurity->xssCleanPostInput('cr_description');
			$ins_data['creditMemo']    = $this->czsecurity->xssCleanPostInput('cr_note');
			$ins_data['creditDate'] = date('Y-m-d H:i:s');
			$ins_data['creditAmount'] = $total;
			$ins_data['creditNumber'] = $new_inv_no;
			$ins_data['updatedAt'] = date('Y-m-d H:i:s');
			$exist_row =   $this->general_model->get_select_data('qb_test_customer', array('companyID'), array('ListID' => $ins_data['customerID']));

			$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $exist_row['companyID']));
			$user    =  $cusdata['qbwc_username'];
			
			$ins_data['merchantDataID'] = $user_id;


			$ins_id = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

			foreach ($item_val as $k => $item) {
				$item['crlineID'] = $ins_id;
				$ins = $this->general_model->insert_row('tbl_credit_item', $item);
			}

			if ($ins_id && $ins) {
				$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));
				$this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1', '', $user);

				

				$this->session->set_flashdata('success', 'Successfully Created Credit');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}

			redirect('QBO_controllers/MerchantUser/create_credit', 'refresh');
		}

		
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);

		$data['plans']          = $this->general_model->get_table_data('QBO_test_item', array('merchantID' => $user_id));
		$compdata				= $this->general_model->get_table_data('QBO_custom_customer', array('merchantID' => $user_id));
		$data['customers']		= $compdata;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/create_credit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function get_plan_data_item()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		$plan =    $this->general_model->get_table_data('QBO_test_item', array('merchantID' => $user_id));
		echo json_encode($plan);
		die;
	}




	public function recover_pwd()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$id = $this->uri->segment('4');
		$results = $this->general_model->get_select_data('tbl_merchant_user', array('userFname', 'userLname', 'userEmail', 'merchantID'), array('merchantUserID' => $id));
		$res = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID', 'firstName', 'lastName','merchantEmail','merchantContact'), array('merchID' => $results['merchantID']));
		$res_data = $this->general_model->get_select_data('tbl_reseller', array('resellerfirstName', 'lastName', 'resellerCompanyName', 'resellerEmail'), array('resellerID' => $res['resellerID']));
		$Merchant_url = $this->general_model->get_select_data('Config_merchant_portal', array('merchantPortalURL'), array('resellerID' => $res['resellerID']));

		if (!empty($results)) {
			$name = $results['userFname'];
			$login_url = $Merchant_url['merchantPortalURL'];
			$toEmail = $results['userEmail'];
			$this->load->helper('string');
			$password = random_string('alnum', 16);
			$marchant_name = $res['firstName'] . ' ' . $res['lastName'];
			$condition = array('merchantUserID' => $id);
			$data = array(
                'userPasswordNew' => password_hash($password, PASSWORD_BCRYPT)
			);
			$update = $this->general_model->update_row_data('tbl_merchant_user', $condition, $data);
			if ($update) {
				$temp_data = $this->general_model->get_row_data('tbl_email_template', array('merchantID' => $results['merchantID'], 'templateType' => '14'));
				$message = $temp_data['message'];
				$subject = $temp_data['emailSubject'];
				if ($temp_data['fromEmail'] != '') {
					$fromEmail = $temp_data['fromEmail'];
				} else {

					$fromEmail = $res_data['resellerEmail'];
				}
				
				$mailDisplayName = $marchant_name;
				if ($temp_data['mailDisplayName'] != '') {
					$mailDisplayName = $temp_data['mailDisplayName'];
				}
				$logo_url ='';
				$config_data = $this->general_model->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
				if(!empty($config_data)){
			   		$logo_url  = $config_data['ProfileImage']; 
				}
				if( !empty( $config_data['ProfileImage'])){
				   	$logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  
				}else{
				  	$logo_url = CZLOGO; 
				}
    
    			$logo_url=	"<img src='".$logo_url."' />";
				$login_url = '<a href=' . $login_url . '>' . $login_url . '<a/>';
				$message = stripslashes(str_replace('{{userFname}}', $name, $message));
				$message = stripslashes(str_replace('{{login_url}}', $login_url, $message));
				$message = stripslashes(str_replace('{{userEmail}}', $toEmail, $message));
				$message = stripslashes(str_replace('{{userPassword}}', $password, $message));
				$message = stripslashes(str_replace('{{merchant_name}}', $marchant_name, $message));
				$message = stripslashes(str_replace('{{merchant_email}}', $res['merchantEmail'], $message));
				$message = stripslashes(str_replace('{{merchant_phone}}', $res['merchantContact'], $message));
				$message = stripslashes(str_replace('{{logo}}',$logo_url ,$message ));



				if($temp_data['replyTo'] != ''){
					$replyTo = $temp_data['replyTo'];
				}else{
					$replyTo = $results['userEmail'];
				}
				$addCC      = $temp_data['addCC'];
				$addBCC		= $temp_data['addBCC'];
				$email_data          = array(
					'customerID'=>'',
					'merchantID'=>$user_id, 
					'emailSubject'=>$subject,
					'emailfrom'=>$fromEmail,
					'emailto'=>$toEmail,
					'emailcc'=>$addCC,
					'emailbcc'=>$addBCC,
					'emailreplyto'=>$replyTo,
					'emailMessage'=>$message,
					'emailsendAt'=> date('Y-m-d H:i:s'),
					
				);

				$mail_sent = $this->general_model->sendMailBySendgrid($toEmail,$subject,$fromEmail,$mailDisplayName,$message,$replyTo, $addCC, $addBCC);

				if($mail_sent){
					$email_data['send_grid_email_id'] = $mail_sent;
					$email_data['send_grid_email_status'] = 'Sent';
					$this->general_model->insert_row('tbl_template_data', $email_data);
					$this->session->set_flashdata('message', '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Password Reset request has been sent. If your email is listed with an account, an email will be sent to you.</div>');
				} else {
					$email_data['send_grid_email_status'] = 'Failed';
					$email_data['mailStatus']=0;
					$this->general_model->insert_row('tbl_template_data', $email_data);
					$this->session->set_flashdata('message', '<div class="alert alert-danger">Email was not sent, please contact your administrator.</div>');
				}
			}
			redirect(base_url() . 'QBO_controllers/MerchantUser/admin_user');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Email was not sent, please contact your administrator.</div>');
			redirect(base_url() . 'QBO_controllers/MerchantUser/admin_user');
		}
	}
}
