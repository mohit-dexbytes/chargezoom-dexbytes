<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\RefundReceipt;

include APPPATH . 'third_party/Fluidpay.class.php';

class FluidpayPayment extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    public function __construct()
    {
        parent::__construct();

        $this->load->model('quickbooks');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('customer_model');
        $this->load->config('fluidpay');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID          = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID         = $merchID;
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        redirect('QBO_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        $user_id   = $this->merchantID;
		$checkPlan = check_free_plan_transactions();
        $custom_data_fields = [];
        $cardID = $this->czsecurity->xssCleanPostInput('CardID');
        if (!$cardID || empty($cardID)) {
            $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
        }

        $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
        }

        $gateway = $gatlistval;
        $custom_data_fields = [];
        $cusproID   = array();
        $error      = array();
        $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

        $resellerID = $this->resellerID;
        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        if ($checkPlan && $cardID != "" && $gateway != "") {
            $in_data = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
            $merchantID = $merchID;

            $val = array(
                'merchantID' => $merchID,
            );
            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken  = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];
            $dataService  = DataService::Configure(array(
                'auth_mode'       => $this->config->item('AuthMode'),
                'ClientID'        => $this->config->item('client_id'),
                'ClientSecret'    => $this->config->item('client_secret'),
                'accessTokenKey'  => $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID'      => $realmID,
                'baseUrl'         => $this->config->item('QBOURL'),
            ));

            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

            if (!empty($in_data)) {
                $customerID = $in_data['CustomerListID'];

                $comp_data       = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchID));

                $c_data    = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchID));
                $companyID = $c_data['companyID'];

                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                    if (!empty($cardID)) {

                        $cr_amount = 0;
                        $amount    = $in_data['BalanceRemaining'];

                        $amount     = $this->czsecurity->xssCleanPostInput('inv_amount');
                        $theInvoice = current($targetInvoiceArray);
                       
                        $error = $dataService->getLastError();
                        if ($error != null) {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error in invoice payment process</strong></div>');
                            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
                        }

                        $amount = $amount - $cr_amount;

                        $crtxnID = '';
                        $inID    = array();

                        $name    = $in_data['fullName'];
                        $address = $in_data['ShipAddress_Addr1'];
                        $city    = $in_data['ShipAddress_City'];
                        $state   = $in_data['ShipAddress_State'];
                        $zipcode = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                        $achValue = false;
                        if ($sch_method == "1") {

                            if ($cardID != "new1") {

                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];
                                $phone    = $card_data['Billing_Contact'];
                            } else {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                    $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                                }

                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('contact');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            }
                            $cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;

                            $exyear1  = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear1;
                            $calamount = $amount * 100;
                            $transaction = array(
                                "type"                => "sale",
                                "amount"              => round($calamount,2),
                                "tax_amount"          => 0,
                                "shipping_amount"     => 0,
                                "currency"            => "USD",
                                "description"         => $in_data['refNumber'] . ' pay',
                                "po_number"           => null,
                                "ip_address"          => getClientIpAddr(),
                                "email_receipt"       => false,
                                "email_address"       => $comp_data['userEmail'],
                                "create_vault_record" => true,
                                "payment_method"      => array(
                                    "card" => array(
                                        "entry_type"      => "keyed",
                                        "number"          => $card_no,
                                        "expiration_date" => $expry,
                                    ),
                                ),
                                "billing_address"     => array(
                                    "first_name"     => $comp_data['firstName'],
                                    "last_name"      => $comp_data['lastName'],
                                    "company"        => $comp_data['companyName'],
                                    "address_line_1" => $address1,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                    "email"          => $comp_data['userEmail'],
                                ),
                                "shipping_address"    => array(
                                    "first_name"     => $comp_data['firstName'],
                                    "last_name"      => $comp_data['lastName'],
                                    "company"        => $comp_data['companyName'],
                                    "address_line_1" => $address1,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                    "email"          => $comp_data['userEmail'],
                                ),
                            );

                            if($cvv && !empty($cvv)){
                                $transaction['payment_method']['card']['cvc'] = $cvv;
                            }

                        } else if ($sch_method == "2") {
                            $achValue = true;
                            $cardType = 'Check';
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                    'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                    'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                    'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                    'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                    'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                    'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'merchantID'         => $user_id,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }
                            $accountNumber = $accountDetails['accountNumber'];
                            $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                            $calamount = $amount * 100;
                            $transaction = array(
                                "type"                => "sale",
                                "amount"              => round($calamount,2),
                                "tax_amount"          => 0,
                                "shipping_amount"     => 0,
                                "currency"            => "USD",
                                "description"         => $in_data['refNumber'] . ' pay',
                                "po_number"           => null,
                                "ip_address"          => getClientIpAddr(),
                                "email_receipt"       => false,
                                "email_address"       => $comp_data['userEmail'],
                                "create_vault_record" => true,
                                "payment_method"      => array(
                                    "ach" => array(
                                        "routing_number" => $accountDetails['routeNumber'],
                                        "account_number" => $accountDetails['accountNumber'],
                                        "sec_code"       => $accountDetails['secCodeEntryMethod'],
                                        "account_type"   => $accountDetails['accountType'],
                                    ),
                                ),
                                "billing_address"     => array(
                                    "first_name"     => $comp_data['firstName'],
                                    "last_name"      => $comp_data['lastName'],
                                    "company"        => $comp_data['companyName'],
                                    "address_line_1" => $accountDetails['Billing_Addr1'],
                                    "address_line_2" => $accountDetails['Billing_Addr2'],
                                    "city"           => $accountDetails['Billing_City'],
                                    "state"          => $accountDetails['Billing_State'],
                                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                                    "phone"          => $accountDetails['Billing_Contact'],
                                    "fax"            => $accountDetails['Billing_Contact'],
                                    "email"          => $comp_data['userEmail'],
                                ),
                                "shipping_address"    => array(
                                    "first_name"     => $comp_data['firstName'],
                                    "last_name"      => $comp_data['lastName'],
                                    "company"        => $comp_data['companyName'],
                                    "address_line_1" => $accountDetails['Billing_Addr1'],
                                    "address_line_2" => $accountDetails['Billing_Addr2'],
                                    "city"           => $accountDetails['Billing_City'],
                                    "state"          => $accountDetails['Billing_State'],
                                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                                    "phone"          => $accountDetails['Billing_Contact'],
                                    "fax"            => $accountDetails['Billing_Contact'],
                                    "email"          => $comp_data['userEmail'],
                                ),
                            );
                        }

                        $responseId = '';

                        $gatewayTransaction              = new Fluidpay();
                        $gatewayTransaction->environment = $this->gatewayEnvironment;
                        $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];
                        $result = $gatewayTransaction->processTransaction($transaction);

                        if ($result['status'] == 'success') {
                            $responseId = $result['data']['id'];

                            $createPaymentObject = [
                                "TotalAmt"    => $amount,
                                "SyncToken"   => 1,
                                "CustomerRef" => $customerID,
                                "Line"        => [
                                    "LinkedTxn" => [
                                        "TxnId"   => $invoiceID,
                                        "TxnType" => "Invoice",
                                    ],
                                    "Amount"    => $amount,
                                ],
                            ];

                            $paymentMethod = $this->general_model->qbo_payment_method($cardType, $user_id, $achValue);
                            if($paymentMethod){
                                $createPaymentObject['PaymentMethodRef'] = [
                                    'value' => $paymentMethod['payment_id']
                                ];
                            }
                            if(isset($responseId) && $responseId != ''){
                                $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                            }

                            $newPaymentObj = Payment::create($createPaymentObject);
                            $savedPayment = $dataService->Add($newPaymentObj);

                            $error = $dataService->getLastError();
                            if ($error != null) {
                                $er = '';
                                $er .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                $er .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                $er .= "The Response message is: " . $error->getResponseBody() . "\n";
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error' . $er . ' </strong>.</div>');

                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success but " . $error->getResponseBody();
                                $qbID   = $responseId;
                            } else {
                                $crtxnID        = $savedPayment->Id;
                                $condition_mail = array('templateType' => '5', 'merchantID' => $merchID);
                                $ref_number     = $in_data['refNumber'];
                                $tr_date        = date('Y-m-d H:i:s');
                                $toEmail        = $c_data['userEmail'];
                                $company        = $c_data['companyName'];
                                $customer       = $c_data['fullName'];

                               
                                $st     = '1';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success";
                                $qbID   = $responseId;

                               
                                $this->session->set_flashdata('success', 'Success </strong></div>');
                              
                            }

                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }
                        } else {
                            $err_msg      = $result['msg'];

                            $st     = '0';
                            $action = 'Pay Invoice';
                            $msg    = "Payment Failed";
                            $qbID   = $invoiceID;
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                        }
                        $qbID   = $invoiceID;
                        
                        $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' =>$responseId,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                        if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;

                            $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                            
                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }


                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $invoiceID, $achValue, $this->transactionByUser, $custom_data_fields);

                        if(isset($st) && $st == 2 && $chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if ($cusproID == "2") {
            redirect('QBO_controllers/home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3") {
         
            redirect('QBO_controllers/Create_invoice/invoice_details_page/' . $in_data['invoiceID'], 'refresh');
        }

        $invoice_IDs  = array();
        $receipt_data = array(
            'proccess_url'      => 'QBO_controllers/Create_invoice/Invoice_details',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }

        redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
    }

    public function create_customer_sale()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $inputData = $this->input->post(null, true);
            $inv_array   = array();
            $inv_invoice = array();
            $gatlistval  = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();

            $custom_data_fields = [];
            $applySurcharge = false;
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                if ($this->session->userdata('logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }
                $resellerID = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchantID))['resellerID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID  = $comp_data['companyID'];

                $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $expry  = $expmonth . $exyear;
                    $cvv    = $this->czsecurity->xssCleanPostInput('cvv');

                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    $cvv       = $card_data['CardCVV'];
                    $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                }
                /*Added card type in transaction table*/
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . $exyear1;

                $phone = $this->czsecurity->xssCleanPostInput('phone');

                $name     = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country  = $this->czsecurity->xssCleanPostInput('bcountry');
                $city     = $this->czsecurity->xssCleanPostInput('bcity');
                $state    = $this->czsecurity->xssCleanPostInput('bstate');
                $amount   = $this->czsecurity->xssCleanPostInput('totalamount');
                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;
                $zipcode  = ($this->czsecurity->xssCleanPostInput('bzipcode')) ? $this->czsecurity->xssCleanPostInput('bzipcode') : '74035';
                $calamount = $amount * 100;

                $transaction = array(
                    "type"                => "sale",
                    "amount"              => round($calamount,2),
                    "tax_amount"          => 0,
                    "shipping_amount"     => 0,
                    "currency"            => "USD",
                    "description"         => 'Chargezoom CC sale',
                    "po_number"           => null,
                    "ip_address"          => getClientIpAddr(),
                    "email_receipt"       => true,
                    "email_address"       => $inputData['email'],
                    "create_vault_record" => true,
                    "payment_method"      => array(
                        "card" => array(
                            "entry_type"      => "keyed",
                            "number"          => $card_no,
                            "expiration_date" => $expry,
                        ),
                    ),
                    "billing_address"     => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['baddress1'],
                        "address_line_2" => $inputData['baddress2'],
                        "city"           => $inputData['bcity'],
                        "state"          => $inputData['bstate'],
                        "postal_code"    => $inputData['bzipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                    "shipping_address"    => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['address1'],
                        "address_line_2" => $inputData['address2'],
                        "city"           => $inputData['city'],
                        "state"          => $inputData['state'],
                        "postal_code"    => $inputData['zipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                );

                if($cvv && !empty($cvv)){
                    $transaction['payment_method']['card']['cvc'] = $cvv;
                }

                if(!empty($this->czsecurity->xssCleanPostInput('po_number'))){
                    $transaction['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }

                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                    $transaction['order_id'] = $new_invoice_number;
                }

                $crtxnID = '';
                $invID   = '';

                $gatewayTransaction              = new Fluidpay();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];
				$result = $gatewayTransaction->processTransaction($transaction);
                
				if ($result['status'] == 'success') {

                    
                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    $val  = array('merchantID' => $merchantID);
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));
                    $refNum = array();
                    $qblist = array();
                    $trID = $result['data']['id'];
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();
                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                            $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            
                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;


                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                $actualInvoicePayAmount += $surchargeAmount;
                                $amount_data += $surchargeAmount;
                                $updatedInvoiceData = [
                                    'inID' => $inID,
                                    'targetInvoiceArray' => $targetInvoiceArray,
                                    'merchantID' => $user_id,
                                    'dataService' => $dataService,
                                    'realmID' => $realmID,
                                    'accessToken' => $accessToken,
                                    'refreshToken' => $refreshToken,
                                    'amount' => $surchargeAmount,
                                ];
                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                            }
                            $ispaid      = 0;
                            $isRun = 0;
                            if($saleAmountRemaining > 0){
                                $BalanceRemaining = 0.00;
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 1;

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 0;
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

                                
                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);

                                    $createPaymentObject = [
                                        "TotalAmt" => $actualInvoicePayAmount,
                                        "SyncToken" => 1, 
                                        "CustomerRef" => $customerID,
                                        "Line" => [
                                            "LinkedTxn" => [
                                                "TxnId" => $inID,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount" => $actualInvoicePayAmount
                                        ]
                                    ];

                                    $paymentMethod = $this->general_model->qbo_payment_method($cardType, $merchantID, false);
                                    if($paymentMethod){
                                        $createPaymentObject['PaymentMethodRef'] = [
                                            'value' => $paymentMethod['payment_id']
                                        ];
                                    }
                                    if(isset($trID) && $trID != ''){
                                        $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                    }
                                    $newPaymentObj = Payment::create($createPaymentObject);

                                    $savedPayment = $dataService->Add($newPaymentObj);

                                    $error = $dataService->getLastError();
                                    if ($error != null) {
                                        $err = '';
                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                        $st = '0';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success but " . $error->getResponseBody();
                                        $qbID = $trID;
                                        $pinv_id = '';
                                    } else {
                                        $pinv_id = '';
                                        $crtxnID = $pinv_id = $savedPayment->Id;
                                        $st = '1';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success";
                                        $qbID = $trID;
                                    }
                                    $qbID = $inID;
                                    $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                    $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                    if($syncid){
                                        $qbSyncID = 'CQ-'.$syncid;

                                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                    }
                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                }
                            }
                            $payIndex++;

                        }
                        
                       
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }
                    
                    /* This block is created for saving Card info in encrypted form  */

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no = $this->czsecurity->xssCleanPostInput('card_number');

                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address1,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $phone,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    if (!empty($refNum)) {
                        $ref_number = implode(',', $refNum);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['userEmail'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['fullName'];
                    if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $trID);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg      = $result['msg'];
                    if(!isset($result['data'])){
						$result['data'] = [
							'id' => '',
							'response_code' => 400
						]; 
					}
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');

                    $crtxnID = '';
                    $inID    = '';
                    $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $result['data']['id'],
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => '',
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

            $user_id = $merchantID;

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $this->czsecurity->xssCleanPostInput('totalamount');
            $name   = $this->czsecurity->xssCleanPostInput('firstName') . " " . $this->czsecurity->xssCleanPostInput('lastName');
            $responseId = '';
            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $customerID   = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->czsecurity->xssCleanPostInput('account_name'),
                        'accountNumber'      => $this->czsecurity->xssCleanPostInput('account_number'),
                        'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                        'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                        'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                        'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('baddress1'),
                        'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('baddress2'),
                        'Billing_City'       => $this->czsecurity->xssCleanPostInput('bcity'),
                        'Billing_Country'    => $this->czsecurity->xssCleanPostInput('bcountry'),
                        'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('phone'),
                        'Billing_State'      => $this->czsecurity->xssCleanPostInput('bstate'),
                        'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $calamount = $amount * 100;
                $transaction = array(
                    "type"                => "sale",
                    "amount"              => round($calamount,2),
                    "tax_amount"          => 0,
                    "shipping_amount"     => 0,
                    "currency"            => "USD",
                    "description"         => 'Payportal Esale',
                    "po_number"           => null,
                    "ip_address"          => getClientIpAddr(),
                    "email_receipt"       => false,
                    "email_address"       => $comp_data['userEmail'],
                    "create_vault_record" => true,
                    "payment_method"      => array(
                        "ach" => array(
                            "routing_number" => $accountDetails['routeNumber'],
                            "account_number" => $accountDetails['accountNumber'],
                            "sec_code"       => $accountDetails['secCodeEntryMethod'],
                            "account_type"   => $accountDetails['accountType'],
                        ),
                    ),
                    "billing_address"     => array(
                        "first_name"     => $this->czsecurity->xssCleanPostInput('firstName'),
                        "last_name"      => $this->czsecurity->xssCleanPostInput('lastName'),
                        "company"        => $this->czsecurity->xssCleanPostInput('companyName'),
                        "address_line_1" => $this->czsecurity->xssCleanPostInput('baddress1'),
                        "address_line_2" => $this->czsecurity->xssCleanPostInput('baddress2'),
                        "city"           => $this->czsecurity->xssCleanPostInput('bcity'),
                        "state"          => $this->czsecurity->xssCleanPostInput('bstate'),
                        "postal_code"    => $this->czsecurity->xssCleanPostInput('bzipcode'),
                       
                        "phone"          => $this->czsecurity->xssCleanPostInput('phone'),
                        "fax"            => $this->czsecurity->xssCleanPostInput('phone'),
                        "email"          => $this->czsecurity->xssCleanPostInput('email'),
                    ),
                    "shipping_address"    => array(
                        "first_name"     => $this->czsecurity->xssCleanPostInput('firstName'),
                        "last_name"      => $this->czsecurity->xssCleanPostInput('lastName'),
                        "company"        => $this->czsecurity->xssCleanPostInput('companyName'),
                        "address_line_1" => $this->czsecurity->xssCleanPostInput('address1'),
                        "address_line_2" => $this->czsecurity->xssCleanPostInput('address2'),
                        "city"           => $this->czsecurity->xssCleanPostInput('city'),
                        "state"          => $this->czsecurity->xssCleanPostInput('state'),
                        "postal_code"    => $this->czsecurity->xssCleanPostInput('zipcode'),
                        
                        "phone"          => $this->czsecurity->xssCleanPostInput('phone'),
                        "fax"            => $this->czsecurity->xssCleanPostInput('phone'),
                        "email"          => $this->czsecurity->xssCleanPostInput('email'),
                    ),
                );

                if(!empty($this->czsecurity->xssCleanPostInput('po_number'))){
                    $transaction['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }

                if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                    $transaction['order_id'] = $new_invoice_number;
                }
                
                $gatewayTransaction              = new Fluidpay();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                $result = $gatewayTransaction->processTransaction($transaction);

                if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];

                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    $val       = array('merchantID' => $user_id);
                    $tokenData = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $tokenData['accessToken'];
                    $refreshToken = $tokenData['refreshToken'];
                    $realmID      = $tokenData['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));

                    $refNumber = array();
                    $merchantID = $user_id;
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                            $con         = array('invoiceID' => $inID, 'merchantID' => $user_id);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;
                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            $ispaid      = 0;
                            $isRun = 0;
                            $BalanceRemaining = 0.00;
                            if($amount_data == $actualInvoicePayAmount){
                                $actualInvoicePayAmount = $amount_data;
                                $isPaid      = 1;

                            }else{

                                $actualInvoicePayAmount = $actualInvoicePayAmount;
                                $isPaid      = 0;
                                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                
                            }
                            $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;
                            $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
                            $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                $theInvoice = current($targetInvoiceArray);

                              

                                $createPaymentObject = [
                                    "TotalAmt"    => $actualInvoicePayAmount,
                                    "SyncToken"   => 1,
                                    "CustomerRef" => $customerID,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $inID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $actualInvoicePayAmount,
                                    ],
                                ];

                                $paymentMethod = $this->general_model->qbo_payment_method('Check', $user_id, true);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($responseId) && $responseId != ''){
                                    $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                                }

                                $newPaymentObj = Payment::create($createPaymentObject);

                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = '';
                                    $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' . $err . '</div>');

                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();

                                    $qbID    = $responseId;
                                    $pinv_id = '';
                                } else {
                                    $pinv_id = '';
                                    $pinv_id = $savedPayment->Id;
                                    $st      = '1';
                                    $action  = 'Pay Invoice';
                                    $msg     = "Payment Success";
                                    $qbID    = $responseId;
                                }
                                $qbID = $inID;


                                $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $responseId,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                if($syncid){
                                    $qbSyncID = 'CQ-'.$syncid;

                                    

                                    $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                                    
                                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                }
                                $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                            }
                            $payIndex++;
                        }
                    } else {

                        $crtxnID = '';
                        $inID    = '';
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number     = '';
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['userEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['fullName'];

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg      = $result['msg'];
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
            
            $invoice_IDs = array();
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => getClientIpAddr(),
                'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
                'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
                'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
                'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
                'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
                'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
                'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
                'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
                'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
                'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
                'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
                'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
                'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
                'Phone'             => $this->czsecurity->xssCleanPostInput('phone'),
                'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
                'proccess_url'      => 'QBO_controllers/Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('QBO_controllers/Payments/create_customer_esale', 'refresh');
    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnID');

            $con = array(
                'transactionID'   => $tID,
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $apiUsername = $gt_result['gatewayUsername'];
            $apiKey      = $gt_result['gatewayPassword'];
            $customerID  = $paydata['customerListID'];
            $amount      = $paydata['transactionAmount'];

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $payload = [
                'amount' => ($amount * 100 )
            ];
            $result = $gatewayTransaction->refundTransaction($tID, $payload);

            if ($result['status'] == 'success') {
                $this->customer_model->update_refund_payment($tID, '');

                if (!empty($paydata['invoice_id'])) {
                    $paymts   = explode(',', $paydata['tr_amount']);
                    $invoices = explode(',', $paydata['invoice_id']);
                    $ins_id   = '';
                    foreach ($invoices as $k1 => $inv) {

                        $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paymts[$k1],
                            'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                    }

                    $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $this->merchantID));
                    $user_id = $this->merchantID;
                    $user    = $cusdata['qbwc_username'];
                    $comp_id = $cusdata['id'];
                    $ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                    $refund  = $amount;

                    if (empty($ittem)) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
                       
                        exit;
                    }
                    $ins_data['customerID'] = $customerID;

                    foreach ($invoices as $k => $inv) {
                        $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                        if (!empty($in_data)) {
                            $inv_pre    = $in_data['prefix'];
                            $inv_po     = $in_data['postfix'] + 1;
                            $new_inv_no = $inv_pre . $inv_po;
                        }
                        $ins_data['merchantDataID']    = $this->merchantID;
                        $ins_data['creditDescription'] = "Credit as Refund";
                        $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                        $ins_data['creditDate']        = date('Y-m-d H:i:s');
                        $ins_data['creditAmount']      = $paymts[$k];
                        $ins_data['creditNumber']      = $new_inv_no;
                        $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                        $ins_data['Type']              = "Payment";
                        $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                        $item['itemListID']      = $ittem['ListID'];
                        $item['itemDescription'] = $ittem['Name'];
                        $item['itemPrice']       = $paymts[$k];
                        $item['itemQuantity']    = 0;
                        $item['crlineID']        = $ins_id;
                        $acc_name                = $ittem['DepositToAccountName'];
                        $acc_ID                  = $ittem['DepositToAccountRef'];
                        $method_ID               = $ittem['PaymentMethodRef'];
                        $method_name             = $ittem['PaymentMethodName'];
                        $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                        $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                        $refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $paymts[$k],
                            'creditInvoiceID'                             => $invID, 'creditTransactionID'          => $tID,
                            'creditTxnID'                                 => $ins_id, 'refundCustomerID'            => $customerID,
                            'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                            'paymentMethod'                               => $method_ID, 'paymentMethodName'        => $method_name,
                            'AccountRef'                                  => $acc_ID, 'AccountName'                 => $acc_name,
                        );

                        

                    }

                } else {
                    $inv       = '';
                    $ins_id    = '';
                    $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
                        'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                        'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
                    );
                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                }

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {
                $err_msg      = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['status'] . '</strong>.</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], true, $this->transactionByUser, $custom_data_fields);

            redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');
        }
    }

    public function payment_evoid()
    {
        $custom_data_fields = [];
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];

        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con = array(
                'transactionID'   => $tID,
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $result = $gatewayTransaction->voidTransaction($tID);
                
            if ($result['status'] == 'success') {

                $condition   = array('transactionID' => $tID);
                $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
            } else {
                $err_msg      = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['status'] . '</strong>.</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
        }
        redirect('QBO_controllers/Payments/evoid_transaction', 'refresh');
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {
            
            $inputData = $this->input->post(null, true);
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $po_number = $this->czsecurity->xssCleanPostInput('po_number');
                if (!empty($po_number)) {
                    $custom_data_fields['po_number'] = $po_number;
                }
                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];
                $cardID    = $this->czsecurity->xssCleanPostInput('card_list');

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $expry  = $expmonth . $exyear;
                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $cvv       = $card_data['CardCVV'];

                    $exyear = $card_data['cardYear'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth . $exyear1;

                $name      = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address   = $this->czsecurity->xssCleanPostInput('address');
                $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country   = $this->czsecurity->xssCleanPostInput('bcountry');
                $city      = $this->czsecurity->xssCleanPostInput('bcity');
                $state     = $this->czsecurity->xssCleanPostInput('bstate');
                $amount    = $this->czsecurity->xssCleanPostInput('totalamount');
                $zipcode   = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '74035';
                $calamount = $amount * 100;

                $transaction = array(
                    "type"                => "authorize",
                    "amount"              => round($calamount,2),
                    "tax_amount"          => 0,
                    "shipping_amount"     => 0,
                    "currency"            => "USD",
                    "description"         => 'Credit Card authorize',
                    "po_number"           => null,
                    "ip_address"          => getClientIpAddr(),
                    "email_receipt"       => true,
                    "email_address"       => $inputData['email'],
                    "create_vault_record" => true,
                    "payment_method"      => array(
                        "card" => array(
                            "entry_type"      => "keyed",
                            "number"          => $card_no,
                            "expiration_date" => $expry,
                        ),
                    ),
                    "billing_address"     => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['baddress1'],
                        "address_line_2" => $inputData['baddress2'],
                        "city"           => $inputData['bcity'],
                        "state"          => $inputData['bstate'],
                        "postal_code"    => $inputData['bzipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                    "shipping_address"    => array(
                        "first_name"     => $inputData['firstName'],
                        "last_name"      => $inputData['lastName'],
                        "company"        => $inputData['companyName'],
                        "address_line_1" => $inputData['address1'],
                        "address_line_2" => $inputData['address2'],
                        "city"           => $inputData['city'],
                        "state"          => $inputData['state'],
                        "postal_code"    => $inputData['zipcode'],
                        "phone"          => $inputData['phone'],
                        "fax"            => $inputData['phone'],
                        "email"          => $inputData['email'],
                    ),
                );

                if($cvv && !empty($cvv)){
                    $transaction['payment_method']['card']['cvc'] = $cvv;
                }
                if(!empty($po_number)){
                    $transaction['po_number'] = $po_number;
                }
                if(!empty($this->czsecurity->xssCleanPostInput('invoice_number'))){
                    $transaction['order_id'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                $crtxnID = '';
                $invID = $inID = $responseId  = '';

                $gatewayTransaction              = new Fluidpay();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

                $result = $gatewayTransaction->processTransaction($transaction);
                
                if ($result['status'] == 'success') {
                    $responseId = $result['data']['id'];

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $inputData['baddress1'],
                            'Billing_Addr2'   => $inputData['baddress2'],
                            'Billing_City'    => $inputData['bcity'],
                            'Billing_State'   => $inputData['bstate'],
                            'Billing_Country' => $inputData['bcountry'],
                            'Billing_Contact' => $inputData['phone'],
                            'Billing_Zipcode' => $inputData['zipcode'],
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $custom_data_fields['card_type'] = $cardType;
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg      = $result['msg'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }

                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        $invoice_IDs = array();

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => '',
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_auth',
            'proccess_btn_text' => 'Process New Transaction',
            'sub_header'        => 'Authorize',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $transactionData = [
                "amount" => ( (float) $amount) * 100,
            ];

            $result = $gatewayTransaction->captureTransaction($tID, $transactionData);
            
            if ($result['status'] == 'success') {
                /* This block is created for saving Card info in encrypted form  */

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {

                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                }
               

                $this->session->set_flashdata('success', 'Successfully Captured Authorization');

            } else {
                $err_msg      = $result['msg'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : '';
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            $invoice_IDs = array();

            $receipt_data = array(
                'proccess_url'      => 'QBO_controllers/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if (!isset($result['data']) || $result['data']['id'] == '') {
                $result['transactionid'] = 'null';
            } else {
                $result['transactionid'] = $result['data']['id'];
            }
            redirect('QBO_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $result['transactionid'], 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_void()
    {
        //Show a form here which collects someone's name and e-mail address
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
            
            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $result = $gatewayTransaction->voidTransaction($tID);
                
            if ($result['status'] == 'success') {
                $condition             = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "3");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
                }
                $this->session->set_flashdata('success', 'Success ');
            } else {
                $err_msg      = $result['status']      = $result['error']['message'];
                $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : '';
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
        }
        redirect('QBO_controllers/Payments/payment_capture', 'refresh');
    }

    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnID');

            $con = array(
                'transactionID'   => $tID,
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $customerID  = $paydata['customerListID'];
            $amount      = $paydata['transactionAmount'];

            $gatewayTransaction              = new Fluidpay();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];

            $payload = [
                'amount' => ($amount * 100)
            ];
            $result = $gatewayTransaction->refundTransaction($tID, $payload);

            if ($result['status'] == 'success') {
                $this->customer_model->update_refund_payment($tID, '');

                if (!empty($paydata['invoice_id'])) {
                    $paymts   = explode(',', $paydata['tr_amount']);
                    $invoices = explode(',', $paydata['invoice_id']);
                    $ins_id   = '';
                    foreach ($invoices as $k1 => $inv) {

                        $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paymts[$k1],
                            'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                    }

                    $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $this->merchantID));
                    $user_id = $this->merchantID;
                    $user    = $cusdata['qbwc_username'];
                    $comp_id = $cusdata['id'];
                    $ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                    $refund  = $amount;

                    if (empty($ittem)) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
                        exit;
                    }
                    $ins_data['customerID'] = $customerID;

                    foreach ($invoices as $k => $inv) {
                        $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                        if (!empty($in_data)) {
                            $inv_pre    = $in_data['prefix'];
                            $inv_po     = $in_data['postfix'] + 1;
                            $new_inv_no = $inv_pre . $inv_po;
                        }
                        $ins_data['merchantDataID']    = $this->merchantID;
                        $ins_data['creditDescription'] = "Credit as Refund";
                        $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                        $ins_data['creditDate']        = date('Y-m-d H:i:s');
                        $ins_data['creditAmount']      = $paymts[$k];
                        $ins_data['creditNumber']      = $new_inv_no;
                        $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                        $ins_data['Type']              = "Payment";
                        $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                        $item['itemListID']      = $ittem['ListID'];
                        $item['itemDescription'] = $ittem['Name'];
                        $item['itemPrice']       = $paymts[$k];
                        $item['itemQuantity']    = 0;
                        $item['crlineID']        = $ins_id;
                        $acc_name                = $ittem['DepositToAccountName'];
                        $acc_ID                  = $ittem['DepositToAccountRef'];
                        $method_ID               = $ittem['PaymentMethodRef'];
                        $method_name             = $ittem['PaymentMethodName'];
                        $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                        $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                        $refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $paymts[$k],
                            'creditInvoiceID'                             => $invID, 'creditTransactionID'          => $tID,
                            'creditTxnID'                                 => $ins_id, 'refundCustomerID'            => $customerID,
                            'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                            'paymentMethod'                               => $method_ID, 'paymentMethodName'        => $method_name,
                            'AccountRef'                                  => $acc_ID, 'AccountName'                 => $acc_name,
                        );


                    }

                } else {
                    $inv       = '';
                    $ins_id    = '';
                    $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
                        'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                        'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
                    );
                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                }

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {
                $err_msg      = $result['msg'];
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['status'] . '</strong>.</div>');
            }

            $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, $custom_data_fields);

            redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');
        }
    }

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');
        $merchantID = $merchID;

		$checkPlan = check_free_plan_transactions();
        $cusproID   = '';
        $cusproID   = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $error      = '';
        $custom_data_fields = [];
        $resellerID = $this->resellerID;

        if ($cardID != "" && $gateway != "") {
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
            if (!empty($invoices)) {
                foreach ($invoices as $key => $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];
                        $c_data          = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'firstName', 'lastName', 'companyName'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
                        $companyID       = $c_data['companyID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
    
                            $address1 = $this->czsecurity->xssCleanPostInput('address1');
                            $address2 = $this->czsecurity->xssCleanPostInput('address2');
                            $city     = $this->czsecurity->xssCleanPostInput('city');
                            $country  = $this->czsecurity->xssCleanPostInput('country');
                            $phone    = $this->czsecurity->xssCleanPostInput('contact');
                            $state    = $this->czsecurity->xssCleanPostInput('state');
                            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }

                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;
                        if (!empty($cardID)) {

                            $cr_amount = 0;
                            $amount    = $pay_amounts;
                            $amount    = $amount - $cr_amount;

                            $name     = $in_data['fullName'];
                            $address  = $in_data['ShipAddress_Addr1'];
                            $address2 = $in_data['ShipAddress_Addr2'];
                            $city     = $in_data['ShipAddress_City'];
                            $state    = $in_data['ShipAddress_State'];
                            $zipcode  = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;
                           
                            
                            $gatewayTransaction              = new Fluidpay();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->apiKey      = $gt_result['gatewayUsername'];
                            $calamount = $amount * 100;
                            $transaction = array(
                                "type"                => "sale",
                                "amount"              => round($calamount,2),
                                "tax_amount"          => 0,
                                "shipping_amount"     => 0,
                                "currency"            => "USD",
                                "description"         => 'CC sale',
                                "po_number"           => null,
                                "ip_address"          => getClientIpAddr(),
                                "email_receipt"       => false,
                                "create_vault_record" => true,
                                "payment_method"      => array(
                                    "card" => array(
                                        "entry_type"      => "keyed",
                                        "number"          => $card_no,
                                        "expiration_date" => $expry,
                                    ),
                                ),
                                "billing_address"     => array(
                                    "first_name"     => $c_data['firstName'],
                                    "last_name"      => $c_data['lastName'],
                                    "company"        => $c_data['companyName'],
                                    "address_line_1" => $address,
                                    "address_line_2" => $address2,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                ),
                                "shipping_address"    => array(
                                    "first_name"     => $c_data['firstName'],
                                    "last_name"      => $c_data['lastName'],
                                    "company"        => $c_data['companyName'],
                                    "address_line_1" => $address,
                                    "address_line_2" => $address2,
                                    "city"           => $city,
                                    "state"          => $state,
                                    "postal_code"    => $zipcode,
                                    "phone"          => $phone,
                                    "fax"            => $phone,
                                ),
                            );

                            if($cvv && !empty($cvv)){
                                $transaction['payment_method']['card']['cvc'] = $cvv;
                            }

                            $responseId = $crtxnID = '';
                            
            				$result = $gatewayTransaction->processTransaction($transaction);
                            if ($result['status'] == 'success') {
                                $responseId = $result['data']['id'];

                                $val                   = array(
                                    'merchantID' => $merchID,
                                );
                                $data = $this->general_model->get_row_data('QBO_token', $val);

                                $accessToken  = $data['accessToken'];
                                $refreshToken = $data['refreshToken'];
                                $realmID      = $data['realmID'];
                                $dataService  = DataService::Configure(array(
                                    'auth_mode'       => $this->config->item('AuthMode'),
                                    'ClientID'        => $this->config->item('client_id'),
                                    'ClientSecret'    => $this->config->item('client_secret'),
                                    'accessTokenKey'  => $accessToken,
                                    'refreshTokenKey' => $refreshToken,
                                    'QBORealmID'      => $realmID,
                                    'baseUrl'         => $this->config->item('QBOURL'),
                                ));

                                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                                $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");


                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);
                                }
                               

                                $createPaymentObject = [
                                    "TotalAmt"    => $amount,
                                    "SyncToken"   => 1,
                                    "CustomerRef" => $Customer_ListID,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $invoiceID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $amount,
                                    ],
                                ];

                                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $merchID, false);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($responseId) && $responseId != ''){
                                    $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                                }

                                $newPaymentObj = Payment::create($createPaymentObject);
                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();
                                    $qbID   = $responseId;
                                    $crtxnID        = $savedPayment->Id;

                                } else {

                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                    $st     = '1';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success";
                                    $qbID   = $responseId;
                                }

                                if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                                    $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                                    $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear    = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                                    $card_type = $this->general_model->getType($card_no);

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $card_type,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'customerListID'  => $Customer_ListID,
                                        'companyID'       => $companyID,
                                        'merchantID'      => $merchID,

                                        'createdAt'       => date("Y-m-d H:i:s"),
                                        'Billing_Addr1'   => $address1,
                                        'Billing_Addr2'   => $address2,
                                        'Billing_City'    => $city,
                                        'Billing_State'   => $state,
                                        'Billing_Country' => $country,
                                        'Billing_Contact' => $phone,
                                        'Billing_Zipcode' => $zipcode,
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                }

                            } else {
                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Failed";
                                $qbID   = $invoiceID;

                                $err_msg      = $result['msg'];

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }

                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $merchID, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if(!$checkPlan){
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

}
