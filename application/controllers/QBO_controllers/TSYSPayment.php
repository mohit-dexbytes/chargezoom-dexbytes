<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\RefundReceipt;

include APPPATH . 'third_party/TSYS.class.php';
class TSYSPayment extends CI_Controller
{
    private $resellerID;
    private $gatewayEnvironment;
    private $merchantID;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();

        $this->load->model('quickbooks');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('customer_model');
        $this->load->config('TSYS');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID          = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID         = $merchID;
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        redirect('QBO_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        $user_id   = $this->merchantID;
        $custom_data_fields = [];
        $cardID = $this->czsecurity->xssCleanPostInput('CardID');
        if (!$cardID || empty($cardID)) {
            $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
        }

        $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
        }

        $gateway = $gatlistval;

        $cusproID   = array();
        $error      = array();
        $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

        $resellerID = $this->resellerID;
        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        $checkPlan = check_free_plan_transactions();

        if ($checkPlan && $cardID != "" && $gateway != "") {
            $in_data = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
            $merchantID = $merchID;

            $val = array(
                'merchantID' => $merchID,
            );
            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken  = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];
            $dataService  = DataService::Configure(array(
                'auth_mode'       => $this->config->item('AuthMode'),
                'ClientID'        => $this->config->item('client_id'),
                'ClientSecret'    => $this->config->item('client_secret'),
                'accessTokenKey'  => $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID'      => $realmID,
                'baseUrl'         => $this->config->item('QBOURL'),
            ));

            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            $responseType = 'SaleResponse';
            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

            if (!empty($in_data)) {
                $customerID = $in_data['CustomerListID'];

                $comp_data       = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchID));

                $c_data    = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchID));
                $companyID = $c_data['companyID'];

                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                $responseErrorMsg = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;

                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                    if (!empty($cardID)) {

                        $cr_amount = 0;
                        $amount    = $in_data['BalanceRemaining'];

                        $amount     = $this->czsecurity->xssCleanPostInput('inv_amount');
                        $theInvoice = current($targetInvoiceArray);
                       
                        $error = $dataService->getLastError();
                        if ($error != null) {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error in invoice payment process</strong></div>');
                            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
                        }

                        $amount = $amount - $cr_amount;

                        $crtxnID = '';
                        $inID    = array();

                        $name    = $in_data['fullName'];
                        $address = $in_data['ShipAddress_Addr1'];
                        $city    = $in_data['ShipAddress_City'];
                        $state   = $in_data['ShipAddress_State'];
                        $zipcode = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';
                        
                        $achValue = false;
                        if ($sch_method == "1") {
                            $responseType = 'SaleResponse';
                            if ($cardID != "new1") {

                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];
                                $phone    = $card_data['Billing_Contact'];
                            } else {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                    $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                                }

                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('contact');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            }
                            $cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;


                            $exyear1  = substr($exyear, 2);
                            if(empty($exyear1)){
                                $exyear1  = $exyear;
                            }
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth .'/'. $exyear1;

                            $amount = round($amount,2);

                            $transaction['Sale'] = array(
                                "deviceID"                          => $deviceID,
                                "transactionKey"                    => $generateToken,
                                "cardDataSource"                    => "MANUAL",  
                                "transactionAmount"                 => (int)($amount * 100),
                                "currencyCode"                      => "USD",
                                "cardNumber"                        => $card_no,
                                "expirationDate"                    => $expry,
                                "cvv2"                              => $cvv,
                                "addressLine1"                      => ($address1 != '')?$address1:'None',
                                "zip"                               => ($zipcode != '')?$zipcode:'None',
                                "orderNumber"                       => $in_data['refNumber'],
                                "notifyEmailID"                     => (($comp_data['userEmail'] != ''))?$comp_data['userEmail']:'chargezoom@chargezoom.com',
                                "firstName"                         => (($comp_data['firstName'] != ''))?$comp_data['firstName']:'None',
                                "lastName"                          => (($comp_data['lastName'] != ''))?$comp_data['lastName']:'None',
                                
                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                "terminalOutputCapability"          => "DISPLAY_ONLY",
                                "maxPinLength"                      => "UNKNOWN",
                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                "cardPresentDetail"                 => "CARD_PRESENT",
                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                "cardholderAuthenticationEntity"    => "OTHER",
                                "cardDataOutputCapability"          => "NONE",

                                "customerDetails"   => array( 
                                    "contactDetails" => array(
                                        
                                        "addressLine1"=> ($address1 != '')?$address1:'None',
                                         "addressLine2"  => ($address2 != '')?$address2:'None',
                                        "city"=>($city != '')?$city:'None',
                                        "zip"=>($zipcode != '')?$zipcode:'None',
                                       
                                    ),
                                    "shippingDetails" => array( 
                                        "firstName"=>(($comp_data['firstName'] != ''))?$comp_data['firstName']:'None',
                                        "lastName"=>(($comp_data['lastName'] != ''))?$comp_data['lastName']:'None',
                                        "addressLine1"=>($address1 != '')?$address1:'None',
                                         "addressLine2" => ($address2 != '')?$address2:'None',
                                        "city"=>($city != '')?$city:'None',
                                        "zip"=>($zipcode != '')?$zipcode:'None',
                                       
                                        "emailID"=>(($comp_data['userEmail'] != ''))?$comp_data['userEmail']:'chargezoom@chargezoom.com'
                                     )
                                )
                            );
                            if($cvv == ''){
                                unset($transaction['Sale']['cvv2']);
                            }
                           
                        } else if ($sch_method == "2") {
                            $achValue = true;
                            $cardType = 'Check';

                            $responseType = 'AchResponse';
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                    'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                    'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                    'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                    'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                    'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                    'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'merchantID'         => $user_id,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),

                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }
                            $accountNumber = $accountDetails['accountNumber'];
                            $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                            $amount = round($amount,2);

                            $transaction['Ach'] = array(
                                "deviceID"              => $deviceID,
                                "transactionKey"        => $generateToken,
                                "transactionAmount"     => (int)($amount * 100),
                                "accountDetails"        => array(
                                        "routingNumber" => $route_number,
                                        "accountNumber" => $account_number,
                                        "accountType"   => strtoupper($account_holder_type),
                                        "accountNotes"  => "count",
                                        "addressLine1"  => ($address1 != '')?$address1:'None',
                                        "addressLine2"  => ($address2 != '')?$address2:'None',
                                        "zip"           => ($zipcode != '')?$zipcode:'None',
                                        "city"          => ($city != '')?$city:'None',
                                        "state"         => ($state != '')?$state:'AZ',
                                        "country"       => ($country != '')?$country:'USA'
                                ),
                                "achSecCode"                => "WEB",
                                "originateDate"             => date('Y-m-d'),
                                "addenda"                   => "addenda",
                                "firstName"                 => $comp_data['FirstName'],
                                "lastName"                  => $comp_data['LastName'],
                                "customerPhone"             => ($phone != '')?$phone:'None',
                                "addressLine1"              => ($address1 != '')?$address1:'None',
                                "addressLine2"              => ($address2 != '')?$address2:'None',
                                 "zip"                      => ($zipcode != '')?$zipcode:'None',
                                "city"                      => ($city != '')?$city:'None',
                                "state"                     => ($state != '')?$state:'AZ',
                                "country"                   => ($country != '')?$country:'USA',
                                "dob"                       => "1967-08-13",
                                "ssn"                       => "1967-08-13",
                                "driverLicenseNumber"       => "101",
                                "driverLicenseIssuedState"  => ($state != '')?$state :'AZ',
                                "developerID"               => "1234"    
                            );
                        }

                        $responseId = '';

                        
                        if($generateToken != ''){
                            $result = $gatewayTransaction->processTransaction($transaction);
                        }else{
                            $responseType = 'GenerateKeyResponse';
                        }

                        if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                            $responseId = $result[$responseType]['transactionID'];

                            $createPaymentObject = [
                                "TotalAmt"    => $amount,
                                "SyncToken"   => 1, 
                                "CustomerRef" => $customerID,
                                "Line"        => [
                                    "LinkedTxn" => [
                                        "TxnId"   => $invoiceID,
                                        "TxnType" => "Invoice",
                                    ],
                                    "Amount"    => $amount,
                                ],
                            ];
            
                            $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, $achValue);
                            if($paymentMethod){
                                $createPaymentObject['PaymentMethodRef'] = [
                                    'value' => $paymentMethod['payment_id']
                                ];
                            }
                            if(isset($responseId) && $responseId != '')
                            {
                                $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                            }
                            $newPaymentObj = Payment::create($createPaymentObject);
                            $savedPayment = $dataService->Add($newPaymentObj);

                            $error = $dataService->getLastError();
                            if ($error != null) {
                                $er = '';
                                $er .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                $er .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                $er .= "The Response message is: " . $error->getResponseBody() . "\n";
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error' . $er . ' </strong>.</div>');

                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success but " . $error->getResponseBody();
                                $qbID   = $responseId;
                            } else {
                                $crtxnID        = $savedPayment->Id;
                                $condition_mail = array('templateType' => '5', 'merchantID' => $merchID);
                                $ref_number     = $in_data['refNumber'];
                                $tr_date        = date('Y-m-d H:i:s');
                                $toEmail        = $c_data['userEmail'];
                                $company        = $c_data['companyName'];
                                $customer       = $c_data['fullName'];

                                

                                $st     = '1';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success";
                                $qbID   = $responseId;

                                $this->session->set_flashdata('success', 'Success </strong></div>');
                            }

                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }
                        } else {
                            $err_msg      = $result[$responseType]['responseMessage'];
                            if($responseErrorMsg != ''){
                                $err_msg = $responseErrorMsg;
                            }
                            $st     = '0';
                            $action = 'Pay Invoice';
                            $msg    = "Payment Failed";
                            $qbID   = $invoiceID;
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                        }
                        $qbID   = $invoiceID;
                        $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $responseId,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                        if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;

                            $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }

                        $result['responseType'] = $responseType;
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $invoiceID, $achValue, $this->transactionByUser, $custom_data_fields);

                        if ($st == 1 && $chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if ($cusproID == "2") {
            redirect('QBO_controllers/home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3") {
            redirect('QBO_controllers/Create_invoice/invoice_details_page/' . $in_data['invoiceID'], 'refresh');
        }

        $invoice_IDs  = array();
        $receipt_data = array(
            'proccess_url'      => 'QBO_controllers/Create_invoice/Invoice_details',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }

        redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
    }

    public function create_customer_sale()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        
		$checkPlan = check_free_plan_transactions();
        $responseType = 'SaleResponse';
        if ($checkPlan && !empty($this->input->post(null, true))) {

            $custom_data_fields = [];
            $applySurcharge = false;
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }


            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $inputData = $this->input->post(null, true);
            $inv_array   = array();
            $inv_invoice = array();
            $gatlistval  = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($gatlistval != "" && !empty($gt_result)) {
                if ($this->session->userdata('logged_in')) {
                   $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }
                $resellerID = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchantID))['resellerID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');
                $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID  = $comp_data['companyID'];

                /*Generate tsys token*/
                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                $responseErrorMsg = '';

                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }

                $gatewayTransaction->transactionKey = $generateToken;


                $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $expry  = $expmonth . $exyear;
                    $cvv    = $this->czsecurity->xssCleanPostInput('cvv');
                    $cardType = $this->general_model->getType($card_no);
                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    $cvv       = $card_data['CardCVV'];
                    $cardType = $card_data['CardType'];
                }
                /*Added card type in transaction table*/
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;
                
                $exyear1  = substr($exyear, 2);
                if(empty($exyear1)){
                    $exyear1  = $exyear;
                }
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth .'/'. $exyear1;

                
                $name     = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                
                $amount   = $this->czsecurity->xssCleanPostInput('totalamount');
                

                $orderId = time();
                if($this->czsecurity->xssCleanPostInput('invoice_id')){
                    $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
                    $orderId = $new_invoice_number;
                }
                
                $address1 = ($this->czsecurity->xssCleanPostInput('baddress1') != '')?$this->czsecurity->xssCleanPostInput('baddress1'):'None';
                $address2 = ($this->czsecurity->xssCleanPostInput('baddress1') != '')?$this->czsecurity->xssCleanPostInput('baddress1'):'None';
                $zipcode = ($this->czsecurity->xssCleanPostInput('bzipcode') != '')?$this->czsecurity->xssCleanPostInput('bzipcode'):'74035';
                $city = ($this->czsecurity->xssCleanPostInput('bcity') != '')?$this->czsecurity->xssCleanPostInput('bcity'):'None';
                $state = ($this->czsecurity->xssCleanPostInput('bstate') != '')?$this->czsecurity->xssCleanPostInput('bstate'):'AZ';
                $country = ($this->czsecurity->xssCleanPostInput('bcountry') != '')?$this->czsecurity->xssCleanPostInput('bcountry'):'USA';
                $phone = ($this->czsecurity->xssCleanPostInput('phone') != '')?$this->czsecurity->xssCleanPostInput('phone'):'None';
                $firstName = ($this->czsecurity->xssCleanPostInput('firstName') != '')?$this->czsecurity->xssCleanPostInput('firstName'):'None';
                $lastName = ($this->czsecurity->xssCleanPostInput('lastName') != '')?$this->czsecurity->xssCleanPostInput('lastName'):'None';
                $companyName = ($this->czsecurity->xssCleanPostInput('companyName') != '')?$this->czsecurity->xssCleanPostInput('companyName'):'None';

                $email = ($this->czsecurity->xssCleanPostInput('email') != '')?$this->czsecurity->xssCleanPostInput('email'):'chargezoom@chargezoom.com';

                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;

                $amount = round($amount,2);

                $transaction['Sale'] = array(
                    "deviceID"                          => $deviceID,
                    "transactionKey"                    => $generateToken,
                    "cardDataSource"                    => "MANUAL",  
                    "transactionAmount"                 => (int)($amount * 100),
                    "currencyCode"                      => "USD",
                    "cardNumber"                        => $card_no,
                    "expirationDate"                    => $expry,
                    "cvv2"                              => $cvv,
                    "addressLine1"                      => $address1,
                    "zip"                               => $zipcode,
                    "orderNumber"                       => "$orderId",
                    "notifyEmailID"                     => $email ,
                    "firstName"                         => $firstName,
                    "lastName"                          => $lastName,
                    "purchaseOrder" => $this->czsecurity->xssCleanPostInput('po_number'),
                    "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                    "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                    "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                    "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                    "terminalOutputCapability"          => "DISPLAY_ONLY",
                    "maxPinLength"                      => "UNKNOWN",
                    "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                    "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                    "cardPresentDetail"                 => "CARD_PRESENT",
                    "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                    "cardholderAuthenticationEntity"    => "OTHER",
                    "cardDataOutputCapability"          => "NONE",

                    "customerDetails"   => array( 
                            "contactDetails" => array(
                                "addressLine1"=> $address1,
                                "addressLine2" => $address2,
                                "city"=>$city,
                                "zip"=>$zipcode,
                            ),
                            "shippingDetails" => array( 
                                "firstName"=> $firstName,
                                "lastName"=> $lastName,
                                "addressLine1"=> $address1,
                                "addressLine2"                      => $address2,
                                "city"=>$city,
                                "zip"=>$zipcode,
                                "emailID"=> $email
                             )
                        )
                );
                if($cvv == ''){
                    unset($transaction['Sale']['cvv2']);
                }
                
                $crtxnID = '';
                $invID   = '';
                $responseType = 'SaleResponse';
                if($generateToken != ''){
                    $result = $gatewayTransaction->processTransaction($transaction);
                }else{
                    $responseType = 'GenerateKeyResponse';
                }
                
				$result['responseType'] = $responseType;
                if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                    $responseId = $result[$responseType]['transactionID'];
                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

                    $val  = array('merchantID' => $merchantID);
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));
                    $refNum = array();
                    $qblist = array();
                    $trID   =  $result[$responseType]['transactionID'];

                    if(!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();
                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                            $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            
                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;


                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                $actualInvoicePayAmount += $surchargeAmount;
                                $amount_data += $surchargeAmount;
                                $updatedInvoiceData = [
                                    'inID' => $inID,
                                    'targetInvoiceArray' => $targetInvoiceArray,
                                    'merchantID' => $user_id,
                                    'dataService' => $dataService,
                                    'realmID' => $realmID,
                                    'accessToken' => $accessToken,
                                    'refreshToken' => $refreshToken,
                                    'amount' => $surchargeAmount,
                                ];
                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                            }

                            $ispaid      = 0;
                            $isRun = 0;
                            if($saleAmountRemaining > 0){
                                $BalanceRemaining = 0.00;
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 1;

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 0;
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

                                
                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);

                                    $createPaymentObject = [
                                        "TotalAmt" => $actualInvoicePayAmount,
                                        "SyncToken" => 1, 
                                        "CustomerRef" => $customerID,
                                        "Line" => [
                                            "LinkedTxn" => [
                                                "TxnId" => $inID,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount" => $actualInvoicePayAmount
                                        ]
                                    ];
                    
                                    $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                    if($paymentMethod){
                                        $createPaymentObject['PaymentMethodRef'] = [
                                            'value' => $paymentMethod['payment_id']
                                        ];
                                    }
                                    if(isset($trID) && $trID != '')
                                    {
                                        $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                    }
                                    $newPaymentObj = Payment::create($createPaymentObject);

                                    $savedPayment = $dataService->Add($newPaymentObj);

                                    $error = $dataService->getLastError();
                                    if ($error != null) {
                                        $err = '';
                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                        $st = '0';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success but " . $error->getResponseBody();
                                        $qbID = $trID;
                                        $pinv_id = '';
                                    } else {
                                        $pinv_id = '';
                                        $crtxnID = $pinv_id = $savedPayment->Id;
                                        $st = '1';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success";
                                        $qbID = $trID;
                                    }
                                    $qbID = $inID;
                                    $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                    $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                    if($syncid){
                                        $qbSyncID = 'CQ-'.$syncid;

                                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                    }
                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                }
                            }
                            $payIndex++;

                        }
                        
                       
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }

                    /* This block is created for saving Card info in encrypted form  */

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no = $this->czsecurity->xssCleanPostInput('card_number');

                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address1,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $phone,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }
                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    if (!empty($refNum)) {
                        $ref_number = implode(',', $refNum);
                    } else {
                        $ref_number = '';
                    }

                    $tr_date  = date('Y-m-d H:i:s');
                    $toEmail  = $comp_data['userEmail'];
                    $company  = $comp_data['companyName'];
                    $customer = $comp_data['fullName'];
                    if ($chh_mail == '1') {

                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg =  $result[$responseType]['responseMessage'];
                    if($responseErrorMsg == ''){
                        $responseErrorMsg = $err_msg;
                    }
                    
                     
                    
                    $crtxnID = '';
                    $inID    = '';
                    $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        if($responseErrorMsg != ''){
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $responseErrorMsg . '</div>');
        }
        $invoice_IDs = array();
        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => (isset($result[$responseType]['transactionID'])) ? $result[$responseType]['transactionID'] : 'TXNFail-'.time(),
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => '', 
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_esale()
    {
        redirect('QBO_controllers/Payments/create_customer_esale', 'refresh');
    }

    public function payment_erefund()
    {
       
       redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');
        
    }

    public function payment_evoid()
    {
       
        redirect('QBO_controllers/Payments/evoid_transaction', 'refresh');
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        
		$checkPlan = check_free_plan_transactions();
        if ($checkPlan && !empty($this->input->post(null, true))) {
            $custom_data_fields = [];

            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            }

            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }

            $inputData = $this->input->post(null, true);
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($gatlistval != "" && !empty($gt_result)) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];
                $cardID    = $this->czsecurity->xssCleanPostInput('card_list');

                $deviceID = $gt_result['gatewayMerchantID'].'01';
                $gatewayTransaction              = new TSYS();
                $gatewayTransaction->environment = $this->gatewayEnvironment;
                $gatewayTransaction->deviceID = $deviceID;
                $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
                $generateToken = '';
                $responseErrorMsg = '';
                if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                    $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                    $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                    
                }
                $gatewayTransaction->transactionKey = $generateToken;

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $expry  = $expmonth . $exyear;
                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $cvv       = $card_data['CardCVV'];

                    $exyear = $card_data['cardYear'];
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if(empty($exyear1)){
                    $exyear1  = $exyear;
                }
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $expmonth .'/'. $exyear1;
                $orderId = time();

                $name      = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
                $address   = $this->czsecurity->xssCleanPostInput('address');
                $baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
                $baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
                $country   = $this->czsecurity->xssCleanPostInput('bcountry');
                $city      = $this->czsecurity->xssCleanPostInput('bcity');
                $state     = $this->czsecurity->xssCleanPostInput('bstate');
                $amount    = $this->czsecurity->xssCleanPostInput('totalamount');
                $zipcode   = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '74035';


                $transaction['Auth'] = array(
                    "deviceID"                          => $deviceID,
                    "transactionKey"                    => $generateToken,
                    "cardDataSource"                    => "MANUAL",  
                    "transactionAmount"                 => $amount * 100,
                    "currencyCode"                      => "USD",
                    "cardNumber"                        => $card_no,
                    "expirationDate"                    => $expry,
                    "cvv2"                              => $cvv,
                    "addressLine1"                      => ($inputData['baddress1'] != '')?$inputData['baddress1']:'None',
                    "zip"                               => ($inputData['bzipcode'] != '')?$inputData['bzipcode']:'None',
                    "orderNumber"                       => "$orderId",
                    "notifyEmailID"                     => (($inputData['email'] != ''))?$inputData['email']:'chargezoom@chargezoom.com',
                    "firstName"                         => (($inputData['firstName'] != ''))?$inputData['firstName']:'None',
                    "lastName"                          => (($inputData['lastName'] != ''))?$inputData['lastName']:'None',
                    "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                    "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                    "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                    "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                    "terminalOutputCapability"          => "DISPLAY_ONLY",
                    "maxPinLength"                      => "UNKNOWN",
                    "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                    "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                    "cardPresentDetail"                 => "CARD_PRESENT",
                    "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                    "cardholderAuthenticationEntity"    => "OTHER",
                    "cardDataOutputCapability"          => "NONE",

                    "customerDetails"   => array( 
                            "contactDetails" => array(
                               
                                "addressLine1"=> ($inputData['baddress1'] != '')?$inputData['baddress1']:'None',
                                 "addressLine2"                      => ($inputData['baddress2'] != '')?$inputData['baddress2']:'None',
                                "city"=>(($inputData['bcity'] != ''))?$inputData['bcity']:'None',
                               
                                "zip"=>($inputData['bzipcode'] != '')?$inputData['bzipcode']:'None',
                               
                            ),
                            "shippingDetails" => array( 
                                "firstName"=> (($inputData['firstName'] != ''))?$inputData['firstName']:'None',
                                "lastName"=> (($inputData['lastName'] != ''))?$inputData['lastName']:'None',
                                "addressLine1"=>($inputData['address1'] != '')?$inputData['address1']:'None',
                                "addressLine2"                      => ($inputData['address2'] != '')?$inputData['address2']:'None',
                                "city"=>(($inputData['city'] != ''))?$inputData['city']:'None',
                               
                                "zip"=>($inputData['zipcode'] != '')?$inputData['zipcode']:'None',
                               
                                "emailID"=>(($inputData['email'] != ''))?$inputData['email']:'chargezoom@chargezoom.com'
                             )
                        )
                );
                $responseType = 'AuthResponse';
                if($cvv == ''){
                    unset($transaction['Auth']['cvv2']);
                }
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $transaction['Auth']['orderNumber'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }

                if (!empty($po_number)) {
                    $transaction['Auth']['purchaseOrder'] = $po_number;
                }
                $crtxnID = '';
                $invID =$inID = $responseId  = 'TXNFail-'.time();
                if($generateToken != ''){
                    $result = $gatewayTransaction->processTransaction($transaction);
                }else{
                    $responseType = 'GenerateKeyResponse';
                }
                if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                    $responseId = $result[$responseType]['transactionID'];

                    if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                        $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

                        $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $inputData['baddress1'],
                            'Billing_Addr2'   => $inputData['baddress2'],
                            'Billing_City'    => $inputData['bcity'],
                            'Billing_State'   => $inputData['bstate'],
                            'Billing_Country' => $inputData['bcountry'],
                            'Billing_Contact' => $inputData['phone'],
                            'Billing_Zipcode' => $inputData['zipcode'],
                        );

                        $this->card_model->process_card($card_data);
                        $custom_data_fields['card_type'] = $cardType;
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg      = $result[$responseType]['responseMessage'];
                    if($responseErrorMsg != ''){
                        $err_msg = $responseErrorMsg;
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }
                $result['responseType'] = $responseType;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        $invoice_IDs = array();

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => getClientIpAddr(),
            'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
            'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
            'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
            'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
            'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
            'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
            'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
            'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
            'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
            'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
            'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
            'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
            'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
            'Phone'             => '', 
            'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_auth',
            'proccess_btn_text' => 'Process New Transaction',
            'sub_header'        => 'Authorize',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

           $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;
            $responseType = 'CaptureResponse';
            if($generateToken != ''){
                $result = $gatewayTransaction->captureTransaction($tID);
            }else{
                $responseType = 'GenerateKeyResponse';
            }
            
            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                /* This block is created for saving Card info in encrypted form  */

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {

                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                }
                

                $this->session->set_flashdata('success', 'Successfully Captured Authorization');

            } else {
                $err_msg      = $result[$responseType]['responseMessage'];
                if($responseErrorMsg != ''){
                    $err_msg = $responseErrorMsg;
                }
               
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }
            $result['responseType'] = $responseType;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            $invoice_IDs = array();

            $receipt_data = array(
                'proccess_url'      => 'QBO_controllers/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            if (!isset($result[$responseType]['transactionID']) || $result[$responseType]['transactionID'] == '') {
                $transactionid = 'null';
            } else {
                $transactionid = $result[$responseType]['transactionID'];
            }
            redirect('QBO_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');

        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_void()
    {
        //Show a form here which collects someone's name and e-mail address
        $custom_data_fields = [];
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID        = $this->czsecurity->xssCleanPostInput('txnvoidID');
            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
            
            $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;
            $responseType = 'VoidResponse';
            if($generateToken != ''){
                $result = $gatewayTransaction->voidTransaction($tID);
            }else{
                $responseType = 'GenerateKeyResponse';
            }
           
            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
            {
                $condition             = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "3");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];
                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
                }
                $this->session->set_flashdata('success', 'Success ');
            } else {
                $err_msg      =  $result[$responseType]['responseMessage'];
                /**/
                if($responseErrorMsg != ''){
                    $err_msg = $responseErrorMsg;
                }
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }
            $result['responseType'] = $responseType;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'void', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
        }
        redirect('QBO_controllers/Payments/payment_capture', 'refresh');
    }

    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnID');

            $con = array(
                'transactionID'   => $tID,
            );
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $customerID  = $paydata['customerListID'];
            $amount      = $paydata['transactionAmount'];

            $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;

            $payload = [
                'amount' => ($amount * 100)
            ];
            $responseType = 'ReturnResponse';
            if($generateToken != ''){
                $result = $gatewayTransaction->refundTransaction($tID, $payload);
            }else{
                $responseType = 'GenerateKeyResponse';
            }
            
            
             if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                $this->customer_model->update_refund_payment($tID, '');

                if (!empty($paydata['invoice_id'])) {
                    $paymts   = explode(',', $paydata['tr_amount']);
                    $invoices = explode(',', $paydata['invoice_id']);
                    $ins_id   = '';
                    foreach ($invoices as $k1 => $inv) {

                        $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paymts[$k1],
                            'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'));
                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);

                    }

                    $cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username', 'id'), array('merchantID' => $this->merchantID));
                    $user_id = $this->merchantID;
                    $user    = $cusdata['qbwc_username'];
                    $comp_id = $cusdata['id'];
                    $ittem   = $this->general_model->get_row_data('qb_test_item', array('companyListID' => $comp_id, 'Type' => 'Payment'));
                    $refund  = $amount;

                    if (empty($ittem)) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>');
                        exit;
                    }
                    $ins_data['customerID'] = $customerID;

                    foreach ($invoices as $k => $inv) {
                        $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
                        if (!empty($in_data)) {
                            $inv_pre    = $in_data['prefix'];
                            $inv_po     = $in_data['postfix'] + 1;
                            $new_inv_no = $inv_pre . $inv_po;
                        }
                        $ins_data['merchantDataID']    = $this->merchantID;
                        $ins_data['creditDescription'] = "Credit as Refund";
                        $ins_data['creditMemo']        = "This credit is given to refund for a invoice ";
                        $ins_data['creditDate']        = date('Y-m-d H:i:s');
                        $ins_data['creditAmount']      = $paymts[$k];
                        $ins_data['creditNumber']      = $new_inv_no;
                        $ins_data['updatedAt']         = date('Y-m-d H:i:s');
                        $ins_data['Type']              = "Payment";
                        $ins_id                        = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

                        $item['itemListID']      = $ittem['ListID'];
                        $item['itemDescription'] = $ittem['Name'];
                        $item['itemPrice']       = $paymts[$k];
                        $item['itemQuantity']    = 0;
                        $item['crlineID']        = $ins_id;
                        $acc_name                = $ittem['DepositToAccountName'];
                        $acc_ID                  = $ittem['DepositToAccountRef'];
                        $method_ID               = $ittem['PaymentMethodRef'];
                        $method_name             = $ittem['PaymentMethodName'];
                        $ins_data['updatedAt']   = date('Y-m-d H:i:s');
                        $ins                     = $this->general_model->insert_row('tbl_credit_item', $item);
                        $refnd_trr               = array('merchantID' => $paydata['merchantID'], 'refundAmount' => $paymts[$k],
                            'creditInvoiceID'                             => $invID, 'creditTransactionID'          => $tID,
                            'creditTxnID'                                 => $ins_id, 'refundCustomerID'            => $customerID,
                            'createdAt'                                   => date('Y-m-d H:i:s'), 'updatedAt'       => date('Y-m-d H:i:s'),
                            'paymentMethod'                               => $method_ID, 'paymentMethodName'        => $method_name,
                            'AccountRef'                                  => $acc_ID, 'AccountName'                 => $acc_name,
                        );

                       
                    }

                } else {
                    $inv       = '';
                    $ins_id    = '';
                    $refnd_trr = array('merchantID' => $this->merchantID, 'refundAmount' => $paydata['transactionAmount'],
                        'creditInvoiceID'               => $inv, 'creditTransactionID'       => $tID,
                        'creditTxnID'                   => $ins_id, 'refundCustomerID'       => $customerID,
                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'  => date('Y-m-d H:i:s'),
                    );
                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                }

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {
                $err_msg      = $result[$responseType]['responseMessage'];
                if($responseErrorMsg != ''){
                    $err_msg = $responseErrorMsg;
                }
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $err_msg . '</strong>.</div>');
            }
            $result['responseType'] = $responseType;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'refund', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $this->merchantID, $crtxnID = '', $this->resellerID, $paydata['invoiceTxnID'], false, $this->transactionByUser, $custom_data_fields);

            redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');
        }
    }

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

		$checkPlan = check_free_plan_transactions();
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');
        $merchantID = $merchID;

        $cusproID   = '';
        $custom_data_fields = [];
        $cusproID   = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
        $error      = '';
        $resellerID = $this->resellerID;

        if ($checkPlan && $cardID != "" && $gateway != "") {
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $deviceID = $gt_result['gatewayMerchantID'].'01';
            $gatewayTransaction              = new TSYS();
            $gatewayTransaction->environment = $this->gatewayEnvironment;
            $gatewayTransaction->deviceID = $deviceID;
            $result = $gatewayTransaction->generateToken($gt_result['gatewayUsername'],$gt_result['gatewayPassword'],$gt_result['gatewayMerchantID']);
            $generateToken = '';
            $responseErrorMsg = '';
            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                
            }
            $gatewayTransaction->transactionKey = $generateToken;


            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
            if (!empty($invoices)) {
                foreach ($invoices as $key => $invoiceID) {
                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];
                        $c_data          = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'firstName', 'lastName', 'companyName','userEmail'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
                        $companyID       = $c_data['companyID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv        = $this->czsecurity->xssCleanPostInput('cvv');
                            $cardType = $this->general_model->getType($card_no);
    
                            $address1 = $this->czsecurity->xssCleanPostInput('address1');
                            $address2 = $this->czsecurity->xssCleanPostInput('address2');
                            $city     = $this->czsecurity->xssCleanPostInput('city');
                            $country  = $this->czsecurity->xssCleanPostInput('country');
                            $phone    = $this->czsecurity->xssCleanPostInput('contact');
                            $state    = $this->czsecurity->xssCleanPostInput('state');
                            $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
                            $cardType = $card_data['CardType'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        if (!empty($cardID)) {

                            $cr_amount = 0;
                            $amount    = $pay_amounts;
                            $amount    = $amount - $cr_amount;

                            $name     = $in_data['fullName'];
                            $address  = $in_data['ShipAddress_Addr1'];
                            $address2 = $in_data['ShipAddress_Addr2'];
                            $city     = $in_data['ShipAddress_City'];
                            $state    = $in_data['ShipAddress_State'];
                            $zipcode  = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth .'/'. $exyear;
                           
                            $amount = round($amount,2);
                            $responseType = 'SaleResponse';
                            $transaction['Sale'] = array(
                                "deviceID"                          => $deviceID,
                                "transactionKey"                    => $generateToken,
                                "cardDataSource"                    => "MANUAL",  
                                "transactionAmount"                 => (int)($amount * 100),
                                "currencyCode"                      => "USD",
                                "cardNumber"                        => $card_no,
                                "expirationDate"                    => $expry,
                                "cvv2"                              => $cvv,
                                "addressLine1"                      => ($address1 != '')?$address1:'None',
                                "zip"                               => ($zipcode != '')?$zipcode:'None',
                                "orderNumber"                       => $in_data['refNumber'],
                                "notifyEmailID"                     => (($c_data['userEmail'] != ''))?$c_data['userEmail']:'chargezoom@chargezoom.com',
                                "firstName"                         => (($c_data['firstName'] != ''))?$c_data['firstName']:'None',
                                "lastName"                          => (($c_data['lastName'] != ''))?$c_data['lastName']:'None',
                               
                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                "terminalOutputCapability"          => "DISPLAY_ONLY",
                                "maxPinLength"                      => "UNKNOWN",
                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                "cardPresentDetail"                 => "CARD_PRESENT",
                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                "cardholderAuthenticationEntity"    => "OTHER",
                                "cardDataOutputCapability"          => "NONE",

                                "customerDetails"   => array( 
                                        "contactDetails" => array(
                                           
                                            "addressLine1"=> ($address1 != '')?$address1:'None',
                                             "addressLine2"                      => ($address2 != '')?$address2:'None',
                                            "city"=>($city != '')?$city:'None',
                                            "zip"=>($zipcode != '')?$zipcode:'None',
                                         
                                        ),
                                        "shippingDetails" => array( 
                                            "firstName"=> (($c_data['firstName'] != ''))?$c_data['firstName']:'None',
                                            "lastName"=> (($c_data['lastName'] != ''))?$c_data['lastName']:'None',
                                            "addressLine1"=>($address1 != '')?$address1:'None',
                                            "addressLine2"                      => ($address2 != '')?$address2:'None',
                                            "city"=>($city != '')?$city:'None',
                                            "zip"=>($zipcode != '')?$zipcode:'None',
                                          
                                            "emailID"=>(($c_data['userEmail'] != ''))?$c_data['userEmail']:'chargezoom@chargezoom.com'
                                         )
                                    )
                            );

                            if($cvv == ''){
                                unset($transaction['Sale']['cvv2']);
                            }

                            $responseId = $crtxnID = '';
                            if($generateToken != ''){
                                $result = $gatewayTransaction->processTransaction($transaction);
                            }else{
                                $responseType = 'GenerateKeyResponse';
                            }
            				
                            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
                            {
                                $responseId = $result[$responseType]['transactionID'];

                                $val                   = array(
                                    'merchantID' => $merchID,
                                );
                                $data = $this->general_model->get_row_data('QBO_token', $val);

                                $accessToken  = $data['accessToken'];
                                $refreshToken = $data['refreshToken'];
                                $realmID      = $data['realmID'];
                                $dataService  = DataService::Configure(array(
                                    'auth_mode'       => $this->config->item('AuthMode'),
                                    'ClientID'        => $this->config->item('client_id'),
                                    'ClientSecret'    => $this->config->item('client_secret'),
                                    'accessTokenKey'  => $accessToken,
                                    'refreshTokenKey' => $refreshToken,
                                    'QBORealmID'      => $realmID,
                                    'baseUrl'         => $this->config->item('QBOURL'),
                                ));

                                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                                $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");


                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);
                                }
                                

                                $createPaymentObject = [
                                    "TotalAmt"    => $amount,
                                    "SyncToken"   => 1, 
                                    "CustomerRef" => $Customer_ListID,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $invoiceID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $amount,
                                    ],
                                ];
                
                                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($responseId) && $responseId != '')
                                {
                                    $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                                }
                                $newPaymentObj = Payment::create($createPaymentObject);
                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();
                                    $qbID   = $responseId;
                                    $crtxnID        = $savedPayment->Id;

                                } else {

                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                    $st     = '1';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success";
                                    $qbID   = $responseId;
                                }

                                if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
                                    $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                                    $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear    = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                                    $card_type = $this->general_model->getType($card_no);

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $card_type,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'customerListID'  => $customerID,
                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,

                                        'createdAt'       => date("Y-m-d H:i:s"),
                                        'Billing_Addr1'   => $address1,
                                        'Billing_Addr2'   => $address2,
                                        'Billing_City'    => $city,
                                        'Billing_State'   => $state,
                                        'Billing_Country' => $country,
                                        'Billing_Contact' => $phone,
                                        'Billing_Zipcode' => $zipcode,
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                }

                            } else {
                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Failed";
                                $qbID   = $invoiceID;

                                $err_msg      = $result[$responseType]['responseMessage'];
                                if($responseErrorMsg != ''){
                                    $err_msg = $responseErrorMsg;
                                }
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }
                            $result['responseType'] = $responseType;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $merchID, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if(!$checkPlan){
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }
        
        if ($cusproID != "") {
            redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

}
