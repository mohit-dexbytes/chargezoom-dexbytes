<?php

/**
 * This Controller has NMI Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 * Perform Customer Card oprtations using this controller
 * Create, Delete, Modify 
 */

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

require APPPATH .'libraries/qbo/QBO_data.php';

class Payments extends CI_Controller
{
	private $merchantID;
	private $resellerID;
	private $transactionByUser;

	public function __construct()
	{
		parent::__construct();


		include APPPATH . 'third_party/nmiDirectPost.class.php';

		include APPPATH . 'third_party/nmiCustomerVault.class.php';

		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->load->model('customer_model');
		$this->load->model('QBO_models/qbo_customer_model');
		$this->load->model('general_model');

		$this->load->model('card_model');
		$this->load->library('form_validation');
		$this->load->model('QBO_models/qbo_invoices_model');


		$this->db1 = $this->load->database('otherdb', TRUE);
		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '1') {
			
			$logged_in_data = $this->session->userdata('logged_in');
            $this->merchantID = $logged_in_data['merchID'];
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $logged_in_data['merchantID'];
		} else {
			redirect('login', 'refresh');
		}
	}



	public function index()
	{
		redirect('QBO_controllers/Payments/payment_transaction', 'refresh');
	}






	public function pay_invoice()
	{

		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
		if ($this->session->userdata('logged_in')) {


			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		


	
	        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');
			$cusproID = '';
			$error = '';
			$cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
			$cardID_upd = '';

			$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');

			$custom_data_fields = [];
			$cardID = $this->czsecurity->xssCleanPostInput('CardID');
			if (!$cardID || empty($cardID)) {
			$cardID = $this->czsecurity->xssCleanPostInput('schCardID');
			}
	
			$gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
			if (!$gatlistval || empty($gatlistval)) {
			$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
			}
			$gateway = $gatlistval;

			$amount               = $this->czsecurity->xssCleanPostInput('inv_amount');

			$in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);
			$customerID = $in_data['CustomerListID'];

			$checkPlan = check_free_plan_transactions();

			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
				$merchant_condition = [
					'merchID' => $user_id,
				];
	
				
					$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			
	
	
			if ($checkPlan && $gatlistval != "" && !empty($gt_result) && $sch_method == 2) {
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  = $gt_result['gatewayPassword'];
				$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);



				$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $in_data['CustomerListID'], 'merchantID' => $user_id));
				$companyID  = $comp_data['companyID'];

		
				$transaction = new nmiDirectPost($nmi_data);

				if ($cardID != "new1") {
					$accountDetails =   $this->card_model->get_single_card_data($cardID);
					$transaction->setAccountName($in_data['fullName']);
					$transaction->setAccount($accountDetails['accountNumber']);
					$transaction->setRouting($accountDetails['routeNumber']);
					$transaction->setAccountType($accountDetails['accountType']);
					$transaction->setAccountHolderType($accountDetails['accountHolderType']);
					$transaction->setSecCode($accountDetails['secCodeEntryMethod']);

					$transaction->setPayment('check');
					$custom_data_fields['payment_type'] = $accountDetails['customerCardfriendlyName'];
				} else {

					$transaction->setAccountName($this->czsecurity->xssCleanPostInput('acc_name'));
					$transaction->setAccount($this->czsecurity->xssCleanPostInput('acc_number'));
					$transaction->setRouting($this->czsecurity->xssCleanPostInput('route_number'));

					$sec_code = "WEB";

					$transaction->setAccountType($this->czsecurity->xssCleanPostInput('acct_type'));
					$transaction->setAccountHolderType($this->czsecurity->xssCleanPostInput('acct_holder_type'));
					$transaction->setSecCode($sec_code);

					$transaction->setPayment('check');

					$accountDetails = [
						'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
						'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
						'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
						'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
						'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
						'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
						'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
						'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
						'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
						'customerListID'     => $customerID,
						'companyID'          => $companyID,
						'merchantID'         => $user_id,
						'createdAt'          => date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
					];
					$accountNumber = $accountDetails['accountNumber'];
                	$friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;
				}
				
				#Billing Details
				$transaction->setCompany($comp_data['companyName']);
				$transaction->setFirstName($comp_data['firstName']);
				$transaction->setLastName($comp_data['lastName']);
				$transaction->setAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction->setAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction->setPhone($this->czsecurity->xssCleanPostInput('contact'));

				#Shipping Details
				$transaction->setShippingCompany($comp_data['companyName']);
				$transaction->setShippingFirstName($comp_data['firstName']);
				$transaction->setShippingLastName($comp_data['lastName']);
				$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));

			
				$amount = $this->czsecurity->xssCleanPostInput('inv_amount');
				$transaction->setAmount($amount);
				$transaction->setTax('tax');
				$transaction->sale();
				$result = $transaction->execute();

				if ($result['response_code'] == '100') {

					$trID = $result['transactionid'];
					$st = '0';
					$action = 'Pay Invoice';
					$msg = "Payment Success ";


					$this->session->set_flashdata('success', 'Successfully Processed Invoice');
					$customerID = $in_data['CustomerListID'];

					$txnID      = $in_data['invoiceID'];
					$ispaid 	 = 1;
					$bamount    = $in_data['BalanceRemaining'] - $amount;
					if ($bamount > 0){
						$ispaid 	 = 0;
					}
						
					$app_amount = $in_data['Total_payment'] + $amount;
					$data   	 = array('IsPaid' => $ispaid, 'BalanceRemaining' => $bamount);
					$condition  = array('invoiceID' => $in_data['invoiceID'], 'merchantID' => $user_id);

					$this->general_model->update_row_data('QBO_test_invoice', $condition, $data);

					$val = array(
						'merchantID' => $user_id,
					);
					$data = $this->general_model->get_row_data('QBO_token', $val);

					$accessToken  = $data['accessToken'];
					$refreshToken = $data['refreshToken'];
					$realmID      = $data['realmID'];
					$dataService  = DataService::Configure(array(
						'auth_mode' => $this->config->item('AuthMode'),
						'ClientID'  => $this->config->item('client_id'),
						'ClientSecret' => $this->config->item('client_secret'),
						'accessTokenKey' =>  $accessToken,
						'refreshTokenKey' => $refreshToken,
						'QBORealmID' => $realmID,
						'baseUrl' => $this->config->item('QBOURL'),
					));

					$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

					$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='" . $invoiceID . "'");


					if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
						$theInvoice = current($targetInvoiceArray);

						
						$createPaymentObject = [
							"TotalAmt" => $amount,
							"SyncToken" => 1, 
							"CustomerRef" => $customerID,
							"Line" => [
								"LinkedTxn" => [
									"TxnId" => $invoiceID,
									"TxnType" => "Invoice",
								],
								"Amount" => $amount
							]
						];

						$paymentMethod = $this->general_model->qbo_payment_method('Check', $this->merchantID, true);
						if($paymentMethod){
							$createPaymentObject['PaymentMethodRef'] = [
								'value' => $paymentMethod['payment_id']
							];
						}
						if(isset($trID) && $trID != '')
                        {
                            $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                        }

						$newPaymentObj = Payment::create($createPaymentObject);

						$savedPayment = $dataService->Add($newPaymentObj);

						$crtxnID =	$savedPayment->Id;

						$error = $dataService->getLastError();

						if ($error != null) {

							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
							$st = '';

							$st = '0';
							$action = 'Pay Invoice';
							$msg = "Payment Success but " . $error->getResponseBody();

							$qbID = $trID;
						} else {

							$st = '1';
							$action = 'Pay Invoice';
							$msg = "Payment Success";
							$qbID = $trID;


							$this->session->set_flashdata('success', 'Successfully Processed Invoice');
						}
					}
					
					//send mail on esale
					$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
					$customerID = ''; 
					$ref_number = '';
					$tr_date   = date('Y-m-d H:i:s');
					$toEmail = $comp_data['userEmail'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['fullName'];

					if ($cardID == "new1") {
						$this->card_model->process_ack_account($accountDetails);
					}

					$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $in_data['CustomerListID'], $amount, $user_id, $crtxnID, $this->resellerID, $in_data['invoiceID'], $echeckType = true, $this->transactionByUser, $custom_data_fields);
					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
				}

				$transactiondata = array();
				$transactiondata['transactionID']       = $result['transactionid'];
				$transactiondata['transactionStatus']    = $result['responsetext'];
				$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
				$transactiondata['transactionCode']     = $result['response_code'];
				$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
				$transactiondata['transactionType']    = $result['type'];
				$transactiondata['gatewayID']          = $gatlistval;
				$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
				$transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
				$transactiondata['transactionAmount']   = $this->czsecurity->xssCleanPostInput('totalamount');
				$transactiondata['merchantID']   = $user_id;
				$transactiondata['resellerID']   = $this->resellerID;
				$transactiondata['gateway']   = "NMI ECheck";

				$transactiondata = alterTransactionCode($transactiondata);
				$CallCampaign = $this->general_model->triggerCampaign($user_id,$transactiondata['transactionCode']);
				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				if($custom_data_fields){
                    $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                }
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			}
			$ref_number = array();
		if ($checkPlan && !empty($in_data) && $sch_method == 1) {
			if (!empty($cardID) && !empty($gateway)) {
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
				$customerID = $in_data['CustomerListID'];
				$con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
				$c_data   = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
				$resellerID = $this->resellerID;
				$nmiuser   = $gt_result['gatewayUsername'];
				$nmipass   = $gt_result['gatewayPassword'];
				$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
				$error = '';

				$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $user_id));
				$companyID  = $comp_data['companyID'];

				if ($cardID != "new1") {
					$card_data =   $this->card_model->get_single_card_data($cardID);
					$card_no  = $card_data['CardNo'];
					$expmonth =  $card_data['cardMonth'];
					$exyear   = $card_data['cardYear'];
					$cvv      = $card_data['CardCVV'];
					$cardType = $card_data['CardType'];
					$address1 = $card_data['Billing_Addr1'];
					$city     =  $card_data['Billing_City'];
					$zipcode  = $card_data['Billing_Zipcode'];
					$state    = $card_data['Billing_State'];
					$country  = $card_data['Billing_Country'];
				} else {
					$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					
					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('cvv') != "")
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
				}
				$cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$crtxnID = '';

				$transaction1 = new nmiDirectPost($nmi_data);
				$transaction1->setCcNumber($card_no);

				if (strlen($exyear) > 2) {
					$minLength = 4 - strlen($exyear);
					$exyear    = substr($exyear, $minLength);
				}
				
				if (strlen($expmonth) == 1) {
					$expmonth = '0' . $expmonth;
				}
				$expry    = $expmonth . $exyear;
				$transaction1->setCcExp($expry);

				$cvv = trim($cvv);
				if($cvv && !empty($cvv)){
					$transaction1->setCvv($cvv);
				}
				
				#Billing Details
				$transaction1->setCompany($comp_data['companyName']);
				$transaction1->setFirstName($comp_data['fullName']);
				$transaction1->setAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction1->setAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction1->setCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction1->setCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction1->setState($this->czsecurity->xssCleanPostInput('state'));
				$transaction1->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction1->setPhone($this->czsecurity->xssCleanPostInput('contact'));
				$transaction1->setEmail($c_data['userEmail']);

				#Shipping Details
				$transaction1->setShippingCompany($comp_data['companyName']);
				$transaction1->setShippingFirstName($comp_data['fullName']);
				$transaction1->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction1->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction1->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction1->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction1->setShippingState($this->czsecurity->xssCleanPostInput('state'));
				$transaction1->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction1->setEmail($c_data['userEmail']);

				$transaction1->setAmount($amount);

				//add level III data
				$level_request_data = [
					'transaction' => $transaction1,
					'card_no' => $card_no,
					'merchID' => $user_id,
					'amount' => $amount,
					'invoice_id' => $invoiceID,
					'gateway' => 1
				];
				$transaction1 = addlevelThreeDataInTransaction($level_request_data);
				
				$transaction1->sale();
				$result = $transaction1->execute();
				$inID = '';
				$crtxnID = '';

				if ($result['response_code'] == "100") {


					$trID = $result['transactionid'];
					$st = '0';
					$action = 'Pay Invoice';
					$msg = "Payment Success ";


					$this->session->set_flashdata('success', 'Successfully Processed Invoice');


					$txnID      = $in_data['invoiceID'];
					$ispaid 	 = 1;
					$bamount    = $in_data['BalanceRemaining'] - $amount;
					if ($bamount > 0)
						$ispaid 	 = 0;
					$data   	 = array('IsPaid' => $ispaid, 'BalanceRemaining' => $bamount);
					$condition  = array('invoiceID' => $in_data['invoiceID'], 'merchantID' => $user_id);

					$this->general_model->update_row_data('QBO_test_invoice', $condition, $data);

					$val = array(
						'merchantID' => $user_id,
					);
					$data = $this->general_model->get_row_data('QBO_token', $val);

					$accessToken  = $data['accessToken'];
					$refreshToken = $data['refreshToken'];
					$realmID      = $data['realmID'];
					$dataService  = DataService::Configure(array(
						'auth_mode' => $this->config->item('AuthMode'),
						'ClientID'  => $this->config->item('client_id'),
						'ClientSecret' => $this->config->item('client_secret'),
						'accessTokenKey' =>  $accessToken,
						'refreshTokenKey' => $refreshToken,
						'QBORealmID' => $realmID,
						'baseUrl' => $this->config->item('QBOURL'),
					));

					$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

					$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='" . $invoiceID . "'");


					if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
						$theInvoice = current($targetInvoiceArray);

						
						$createPaymentObject = [
							"TotalAmt" => $amount,
							"SyncToken" => 1, 
							"CustomerRef" => $customerID,
							"Line" => [
								"LinkedTxn" => [
									"TxnId" => $invoiceID,
									"TxnType" => "Invoice",
								],
								"Amount" => $amount
							]
						];

						$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
						if($paymentMethod){
							$createPaymentObject['PaymentMethodRef'] = [
								'value' => $paymentMethod['payment_id']
							];
						}
						if(isset($trID) && $trID != '')
                        {
                            $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                        }

						$newPaymentObj = Payment::create($createPaymentObject);

						$savedPayment = $dataService->Add($newPaymentObj);

						$crtxnID =	$savedPayment->Id;

						$error = $dataService->getLastError();

						if ($error != null) {

							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
							$st = '';

							$st = '0';
							$action = 'Pay Invoice';
							$msg = "Payment Success but " . $error->getResponseBody();

							$qbID = $trID;
						} else {

							$st = '1';
							$action = 'Pay Invoice';
							$msg = "Payment Success";
							$qbID = $trID;


							$this->session->set_flashdata('success', 'Successfully Processed Invoice');
						}
					}


					$qbID = $invoiceID;
					$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
					$ref_number =  $in_data['refNumber'];
					$tr_date   = date('Y-m-d H:i:s');
					$toEmail = $c_data['userEmail'];
					$company = $c_data['companyName'];
					$customer = $c_data['fullName'];
					
					
					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

						$cardDetails = [
							'card_no' => $card_no,
							'cvv' => $cvv,
							'customerID' => $customerID,
							'expmonth' => $expmonth,
							'exyear' => $exyear,
							'companyID' => $c_data['companyID'],
							'user_id' => $user_id,
							'address1' => $address1,
							'city' => $city,
							'state' => $state,
							'country' => $country,
							'phone' => $phone,
							'zipcode' => $zipcode,
							'country' => $country,
						];
						
						save_qbo_card($cardDetails, $user_id);
					}
				} else {
					$error =  $msg = $result['responsetext'];

					$st = '0';
					$action = 'Pay Invoice';
					$msg = "Payment Failed";
					$qbID = $invoiceID;


					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' . $error . '</strong></div>');
				}

				$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $in_data['CustomerListID'], $amount, $user_id, $crtxnID, $this->resellerID, $in_data['invoiceID'], false, $this->transactionByUser, $custom_data_fields);

				if ($st == 1 && $chh_mail == '1') {
					$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $trID);
				}

				if ($error != '') {
					$st = '0';
					$action = 'Pay Invoice';
					$msg = "Payment Failed";
					$qbID = $invoiceID;
				}
				$qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
			}
		} 
		
		if(empty($in_data)){
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - This is not valid invoice! </strong>.</div>');
		}
		
		if ($cusproID == "2") {
			redirect('QBO_controllers/home/view_customer/' . $customerID, 'refresh');
		}
		if ($cusproID == "3") {
			redirect('QBO_controllers/Create_invoice/invoice_details_page/' . $in_data['invoiceID'], 'refresh');
		}
		$trans_id = $result['transactionid'];
		$invoice_IDs = array();
		$receipt_data = array(
			'proccess_url' => 'QBO_controllers/Create_invoice/Invoice_details',
			'proccess_btn_text' => 'Process New Invoice',
			'sub_header' => 'Sale',
			'checkPlan'  => $checkPlan
		);
		
		$this->session->set_userdata("receipt_data",$receipt_data);
		$this->session->set_userdata("invoice_IDs",$invoice_IDs);
		$this->session->set_userdata("in_data",$in_data);
		if ($cusproID == "1") {
			redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		}
		redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		
	}





	public function pay_old_invoice()
	{
		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;

		$invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		$cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		$gateway			   = $this->czsecurity->xssCleanPostInput('gateway');
		$cusproID = '';
		$cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		$m_data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
		$resellerID = 	$m_data['resellerID'];

		$in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
		$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));


		$nmiuser   = $gt_result['gatewayUsername'];
		$nmipass   = $gt_result['gatewayPassword'];
		$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
		$gatewayName = getGatewayName($gt_result['gatewayType']);
		$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

		$error = '';
		if ($cardID != "" || $gateway != "") {


			if (!empty($in_data)) {

				$customerID = $in_data['CustomerListID'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchID);
				$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
				$companyID  = $comp_data['companyID'];
				if ($cardID == 'new1') {
					$cardID_upd  = $cardID;
					$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
					$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
					$cardType       = $this->general_model->getType($card_no);
					$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
				} else {
					$card_data    =   $this->card_model->get_single_card_data($cardID);
					$card_no  = $card_data['CardNo'];
					$cvv      =  $card_data['CardCVV'];
					$expmonth =  $card_data['cardMonth'];
					$exyear   = $card_data['cardYear'];
				}
				$cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$ref_number = array();
				if (!empty($cardID)) {

					if ($in_data['BalanceRemaining'] > 0) {
						$cr_amount = 0;
						$amount    =	 $in_data['BalanceRemaining'];
						$amount    = $this->czsecurity->xssCleanPostInput('inv_amount');
						$amount    = $amount - $cr_amount;



						$transaction1 = new nmiDirectPost($nmi_data);
						$transaction1->setCcNumber($card_no);

						$exyear   = substr($exyear, 2);
						if (strlen($expmonth) == 1) {
							$expmonth = '0' . $expmonth;
						}
						$expry    = $expmonth . $exyear;
						$transaction1->setCcExp($expry);
						$transaction1->setCvv($cvv);
						$transaction1->setAmount($amount);

						$transaction1->sale();
						//add level III data
						$level_request_data = [
							'transaction' => $transaction1,
							'card_no' => $card_no,
							'merchID' => $merchID,
							'amount' => $amount,
							'invoice_id' => $invoiceID,
							'gateway' => 1
						];
						$transaction1 = addlevelThreeDataInTransaction($level_request_data);
						
						$result = $transaction1->execute();
						$inID = '';
						$crtxnID = '';

						if ($result['response_code'] == "100") {


							$val = array(
								'merchantID' => $merchID,
							);
							$data = $this->general_model->get_row_data('QBO_token', $val);

							$accessToken  = $data['accessToken'];
							$refreshToken = $data['refreshToken'];
							$realmID      = $data['realmID'];
							$dataService  = DataService::Configure(array(
								'auth_mode' => $this->config->item('AuthMode'),
								'ClientID'  => $this->config->item('client_id'),
								'ClientSecret' => $this->config->item('client_secret'),
								'accessTokenKey' =>  $accessToken,
								'refreshTokenKey' => $refreshToken,
								'QBORealmID' => $realmID,
								'baseUrl' => $this->config->item('QBOURL'),
							));

							$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

							$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");


							if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
								$theInvoice = current($targetInvoiceArray);

								$updatedInvoice = Invoice::update($theInvoice, [
									"sparse" => 'true'
								]);

								$updatedResult = $dataService->Update($updatedInvoice);

								$newPaymentObj = Payment::create([
									"TotalAmt" => $amount,
									"SyncToken" => $updatedResult->SyncToken,
									"CustomerRef" => $updatedResult->CustomerRef,
									"Line" => [
										"LinkedTxn" => [
											"TxnId" => $updatedResult->Id,
											"TxnType" => "Invoice",
										],
										"Amount" => $amount
									]
								]);

								$savedPayment = $dataService->Add($newPaymentObj);

								$crtxnID =	$savedPayment->Id;

								$error = $dataService->getLastError();

								if ($error != null) {

									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Success but " . $error->getResponseBody();
									$qbID = $result['transactionid'];
								} else {
									$st = '1';
									$action = 'Pay Invoice';
									$msg = "Payment Success";
									$qbID = $result['transactionid'];


									$this->session->set_flashdata('success', 'Successfully Processed Invoice');
								}

								if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

									$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');

									$cardType       = $this->general_model->getType($card_no);
									$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
									$card_condition = array(
										'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
										'customerCardfriendlyName' => $friendlyname,
									);
									$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
									$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
									$exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
									$cvv      =    $this->czsecurity->xssCleanPostInput('cvv');

									$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

									$crdata =   $query->row_array()['numrow'];

									if ($crdata > 0) {

										$card_data = array(
											'cardMonth'   => $expmonth,
											'cardYear'	 => $exyear,
											'CardType'    => $cardType,
											'CustomerCard' => $this->card_model->encrypt($card_no),
											'CardCVV'      => '', 
											'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
											'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
											'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
											'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
											'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
											'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
											'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
											'customerListID' => $in_data['Customer_ListID'],
											'customerCardfriendlyName' => $friendlyname,
											'companyID'     => $companyID,
											'merchantID'   => $merchID,
											'updatedAt' 	=> date("Y-m-d H:i:s")
										);


										$this->db1->update('customer_card_data', $card_condition, $card_data);
									} else {
										$is_default = 0;
                                        $checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
                                        if($checkCustomerCard == 0){
                                            $is_default = 1;
                                        }
										$card_data = array(
											'cardMonth'   => $expmonth,
											'cardYear'	 => $exyear,
											'CardType'    => $cardType,
											'CustomerCard' => $this->card_model->encrypt($card_no),
											'CardCVV'      => '', 
											'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
											'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
											'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
											'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
											'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
											'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
											'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
											'customerListID' => $in_data['Customer_ListID'],
											'customerCardfriendlyName' => $friendlyname,
											'companyID'     => $companyID,
											'merchantID'   => $merchID,
											'is_default'			   => $is_default,
											'createdAt' 	=> date("Y-m-d H:i:s")
										);


										$id1 = $this->db1->insert('customer_card_data', $card_data);
									}
								}




								$invoiceID =	$updatedResult->Id;
							} else {
								$st = '0';
								$action = 'Pay Invoice';
								$msg = "Payment Success but " . $error->getResponseBody();
								$qbID = $result['transactionid'];
								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice.</div>');
							}
						} else {
							$st = '0';
							$action = 'Pay Invoice';
							$msg = "Payment Failed";
							$qbID = $invoiceID;

							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '.</div>');
						}
						$qbID = $invoiceID;
						$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' =>$result['transactionid'],'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

						$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
						if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;

							$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                             
                            
                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }
						$transaction['transactionID']      = $result['transactionid'];
						$transaction['transactionStatus']  = $result['responsetext'];
						$transaction['transactionCode']    =  $result['response_code'];
						$transaction['transactionType']   =  ($result['type']) ? $result['type'] : 'auto-nmi';
						$transaction['transactionDate']    = date('Y-m-d H:i:s');
						$transaction['transactionModified']    = date('Y-m-d H:i:s');
						$transaction['gatewayID']          = $gateway;
						$transaction['transactionGateway']  = $gt_result['gatewayType'];
						$transaction['customerListID']     = $in_data['CustomerListID'];
						$transaction['transactionAmount']  = $amount;
						$transaction['merchantID']   = $merchID;

						$transaction['qbListTxnID']   = $crtxnID;

						$transaction['resellerID']   = $this->resellerID;
						$transaction['gateway']   = $gatewayName;
						$transaction['invoiceID']   = $invoiceID;

						$transaction = alterTransactionCode($transaction);
						$CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);

						if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
                            $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                        }
						$id = $this->general_model->insert_row('customer_transaction',   $transaction);

						if ($chh_mail == '1' && $st == 1) {
							$condition_mail         = array('templateType' => '5', 'merchantID' => $merchID);
							$ref_number =  $in_data['refNumber'];
							$tr_date   = date('Y-m-d H:i:s');
							$toEmail = $comp_data['userEmail'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['fullName'];

							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
						}
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card!</strong>.</div>');
		}


		if ($cusproID != "") {
			redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
		} else {
			redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
		}
	}




	public function get_vault_data($customerID)
	{
		$card = array();

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}


		$sql = "SELECT * from customer_card_data  where  customerListID='$customerID'  and  merchantID ='$merchID' order by CardID desc limit 1  ";
		$query1 = $this->db1->query($sql);
		$card_data = $query1->row_array();
		if (!empty($card_data)) {

			$card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']);
			$card['cardMonth']  = $card_data['cardMonth'];
			$card['cardYear']  = $card_data['cardYear'];
			$card['CardID']    = $card_data['CardID'];
			$card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
			$card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
		}

		return  $card;
	}



	public function delete_card_data()
	{
		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}
		if (!empty($this->czsecurity->xssCleanPostInput('delCardID'))) {


			$cardID = $this->czsecurity->xssCleanPostInput('delCardID');
			$customer =  $this->czsecurity->xssCleanPostInput('delCustodID');


			$num  = $this->general_model->get_num_rows('tbl_subscriptions_qbo', array('CardID' => $cardID, 'merchantDataID' => $merchID));
			if ($num == 0) {
				$sts =  $this->card_model->delete_card_data(array('CardID' => $cardID));
				if ($sts) {
					$this->session->set_flashdata('success', 'Successfully Deleted');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Error: This card cannot be deleted. Please change Customer card on Subscription</strong></div>');
			}
			redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
		}
	}



	public function update_card_data()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');

		$card_data = $this->card_model->get_single_card_data($cardID);
		$con_cust = array('Customer_ListID' => $card_data['customerListID'], 'merchantID' => $merchID);
		$c_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
		$companyID  = $c_data['companyID'];
		$customer = $card_data['customerListID'];

		if ($card_data['CardType'] != 'Echeck') {

			$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
			$decryptedcvv  = $this->czsecurity->xssCleanPostInput('edit_cvv');
			$cvv      = ''; 
			$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
		}

		if ($this->czsecurity->xssCleanPostInput('edit_acc_number') !== '') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('edit_acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('edit_route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('edit_acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('edit_secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('edit_acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
			$expmonth = $exyear   = 	$cvv = 0;
			$card_no  = '';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
		}

		$cardID = $this->czsecurity->xsscleanpostinput('edit_cardID');

		
		$b_addr1 = $this->czsecurity->xsscleanpostinput('baddress1');
		$b_addr2 = $this->czsecurity->xsscleanpostinput('baddress2');
		$b_city = $this->czsecurity->xsscleanpostinput('bcity');
		$b_state = $this->czsecurity->xsscleanpostinput('bstate');
		$b_country = $this->czsecurity->xsscleanpostinput('bcountry');
		$b_contact = $this->czsecurity->xsscleanpostinput('bcontact');
		$b_zip = $this->czsecurity->xsscleanpostinput('bzipcode');
		$is_default = ($this->czsecurity->xsscleanpostinput('defaultMethod') != null )?$this->czsecurity->xsscleanpostinput('defaultMethod'):0;
		$merchantID = $merchID;

		$condition = array('CardID' => $cardID);
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CardCVV'      => $cvv,			
			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zip,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'is_default'		 => $is_default,
			'updatedAt' 	=> date("Y-m-d H:i:s")
		);


		$decryptedCard = '';
		if($friendlyname != ''){
        	$insert_array['customerCardfriendlyName'] = $friendlyname;
        }

		if ($this->czsecurity->xsscleanpostinput('edit_card_number') != '') {
			$card_no  = $this->czsecurity->xsscleanpostinput('edit_card_number');
			$decryptedCard = $card_no;
			$insert_array['CustomerCard'] = $this->card_model->encrypt($card_no);
			$card_type = $this->general_model->getType($card_no);

			$friendlyname = $card_type . ' - ' . substr($card_no, -4);
			$insert_array['customerCardfriendlyName'] = $friendlyname;
			$insert_array['CardType'] = $card_type;
		} else {
			$decryptedCard     = $card_data['CardNo'];
		}

		$isAuthorised = true;

		if ($decryptedCard != '') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['fullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}
		
		if($isAuthorised){
			$insert_array['CardCVV'] = '';

			if($is_default == 1){
				$conditionDefaultSet = array('customerListID' => $customer, 'merchantID' => $merchID);
				$updateData = $this->card_model->update_customer_card_data($conditionDefaultSet,['is_default' => 0]);
			}
			$id = $this->card_model->update_card_data($condition,  $insert_array);
			if ($id) {
				$this->session->set_flashdata('success', 'Successfully Updated Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
		}

		redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
	}






	public function insert_new_data()
	{


		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$customer = $this->czsecurity->xssCleanPostInput('customerID');
		$con_cust = array('Customer_ListID' => $customer, 'merchantID' => $merchID);
		$c_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
		$companyID  = $c_data['companyID'];

		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
			$decryptedCard = $card_no;
			$card_type = $this->general_model->getType($card_no);
			$card_no  = $this->card_model->encrypt($card_no);
			$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
			$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
			$decryptedcvv = $cvv;
			$cvv      = ''; 
			$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
		}
		if ($this->czsecurity->xssCleanPostInput('formselector') == '2') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
			$expmonth = $exyear   = 	$cvv = 0;
			$card_no  = '';
			$card_type = 'Echeck';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
		}

		$b_addr1 = $this->czsecurity->xsscleanpostinput('address1');
		$b_addr2 = $this->czsecurity->xsscleanpostinput('address2');
		$b_city = $this->czsecurity->xsscleanpostinput('city');
		$b_state = $this->czsecurity->xsscleanpostinput('state');
		$b_zipcode = $this->czsecurity->xsscleanpostinput('zipcode');
		$b_country = $this->czsecurity->xsscleanpostinput('country');
		$b_contact = $this->czsecurity->xsscleanpostinput('contact');

		$is_default = ($this->czsecurity->xsscleanpostinput('defaultMethod') != null )?$this->czsecurity->xsscleanpostinput('defaultMethod'):0;
		
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $card_no,
			'CardCVV'      => $cvv,
			'CardType'      => $card_type,
			'customerListID' => $customer,
			'merchantID'     => $merchID,
			'companyID'      => $companyID,
			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zipcode,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'customerCardfriendlyName' => $friendlyname,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'createdAt' 	=> date("Y-m-d H:i:s")
		);

		$isAuthorised = true;
		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['fullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}

		if($isAuthorised){
			$insert_array['CardCVV'] = '';
			if($is_default == 1){
				$condition = array('customerListID' => $customer, 'merchantID' => $merchID);
				
				$updateData = $this->card_model->update_customer_card_data($condition,['is_default' => 0]);
				
			}else{
				$checkCustomerCard = $this->card_model->get_customer_card_data($customer);
				if(count($checkCustomerCard) == 0){
					$is_default == 1;
				}
	        	
			}
			$insert_array['is_default'] = $is_default;
			$id = $this->card_model->insertBillingdata($insert_array);
			if ($id) {
				$this->session->set_flashdata('success', 'Successfully Inserted Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}
		}

		redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
	}



	public function create_customer_old_sale()
	{

		if (!empty($this->input->post(null, true))) {
			$inv_array = array();
			$inv_invoice = array();
			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$resellerID = $this->resellerID;
			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;


			if ($gatlistval != "" && !empty($gt_result)) {
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  = $gt_result['gatewayPassword'];
				$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

				$invoiceIDs = array();
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				}

				$customerID = $this->czsecurity->xssCleanPostInput('customerID');

				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

				$companyID  = $comp_data['companyID'];

				$transaction = new nmiDirectPost($nmi_data);

				if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == 'new1') {
					$card_no = $this->czsecurity->xssCleanPostInput('card_number');

					$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$exyear   = substr($exyear, 2);
					$expry    = $expmonth . $exyear;
					$transaction->setCcExp($expry);
					$transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));
				} else {

					$cardID = $this->czsecurity->xssCleanPostInput('card_list');

					$card_data = $this->card_model->get_single_card_data($cardID);
					$card_no = $card_data['CardNo'];
					$transaction->setCcNumber($card_data['CardNo']);
					$expmonth =  $card_data['cardMonth'];

					$exyear   = $card_data['cardYear'];
					$exyear   = substr($exyear, 2);
					if (strlen($expmonth) == 1) {
						$expmonth = '0' . $expmonth;
					}
					$expry    = $expmonth . $exyear;
					$transaction->setCcExp($expry);
					$transaction->setCvv($card_data['CardCVV']);
				}
				$cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
				$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
				$transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));

				$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
				$amount = $this->czsecurity->xssCleanPostInput('totalamount');

				//add level III data
				$level_request_data = [
					'transaction' => $transaction,
					'card_no' => $card_no,
					'merchID' => $merchantID,
					'amount' => $amount,
					'invoice_id' => '',
					'gateway' => 1
				];
				$transaction = addlevelThreeDataInTransaction($level_request_data);
				$transaction->setAmount($amount);
				$transaction->setTax('tax');
				$transaction->sale();
				$result = $transaction->execute();

				if ($result['response_code'] == '100') {

					$val = array('merchantID' => $merchantID);
					$data = $this->general_model->get_row_data('QBO_token', $val);

					$accessToken = $data['accessToken'];
					$refreshToken = $data['refreshToken'];
					$realmID      = $data['realmID'];

					$dataService = DataService::Configure(array(
						'auth_mode' => $this->config->item('AuthMode'),
						'ClientID'  => $this->config->item('client_id'),
						'ClientSecret' => $this->config->item('client_secret'),
						'accessTokenKey' =>  $accessToken,
						'refreshTokenKey' => $refreshToken,
						'QBORealmID' => $realmID,
						'baseUrl' => $this->config->item('QBOURL'),
					));



					if (!empty($invoiceIDs)) {

						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();


							$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
							$invse = $inID;
							$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $invse . "'");


							if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
								$theInvoice = current($targetInvoiceArray);

								$updatedInvoice = Invoice::update($theInvoice, [
									"sparse" => 'true'
								]);
								$con = array('invoiceID' => $invse, 'merchantID' => $merchantID);
								$res = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment'), $con);
								$amount_data = $res['BalanceRemaining'];
								$refNum[] = $res['refNumber'];
								$amount1      =  $res['BalanceRemaining'] + $res['Total_payment'];
								$updatedResult = $dataService->Update($updatedInvoice);
								$newPaymentObj = Payment::create([
									"TotalAmt" => $amount1,
									"SyncToken" => $updatedResult->SyncToken,
									"CustomerRef" => $updatedResult->CustomerRef,
									"Line" => [
										"LinkedTxn" => [
											"TxnId" => $updatedResult->Id,
											"TxnType" => "Invoice",
										],
										"Amount" => $amount_data
									]
								]);

								$savedPayment = $dataService->Add($newPaymentObj);



								$error = $dataService->getLastError();
								if ($error != null) {
									$err = '';
									$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
									$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
									$err .= "The Response message is: " . $error->getResponseBody() . "\n";
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Success but " . $error->getResponseBody();
									$qbID = $result['transactionid'];
								} else {
									$pinv_id = '';
									$pinv_id        =  $savedPayment->Id;
									$st = '1';
									$action = 'Pay Invoice';
									$msg = "Payment Success";
									$qbID = $result['transactionid'];
									
									

									$this->session->set_flashdata('success', 'Transaction Successful');
								}
								$qbID = $updatedResult->Id;
								$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result['transactionid'],'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

								$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
								if($syncid){
		                            $qbSyncID = 'CQ-'.$syncid;
		                            

									$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

		                            
		                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
		                        }
								$transactiondata = array();
								$transactiondata['transactionID']       = $result['transactionid'];
								$transactiondata['transactionStatus']   = $result['responsetext'];
								$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
								$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
								$transactiondata['transactionCode']     = $result['response_code'];

								$transactiondata['transactionType']    = $result['type'];
								$transactiondata['gatewayID']          = $gatlistval;
								$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
								$transactiondata['customerListID']      = $customerID;
								$transactiondata['transactionAmount']   = $amount_data;
								$transactiondata['merchantID']          = $merchantID;
								$transactiondata['invoiceID']           = $invse;
								if (!empty($pinv_id))
									$transactiondata['qbListTxnID']      = $pinv_id;
								$transactiondata['resellerID']   = $this->resellerID;
								$transactiondata['gateway']   = $gatewayName;

								$transactiondata = alterTransactionCode($transactiondata);
								$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
								if(!empty($this->transactionByUser)){
								    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
								    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

								if($st == 1 && $chh_mail == '1') {
									$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
									$ref_number = implode(',', $refNum);
									$tr_date   = date('Y-m-d H:i:s');
									$toEmail = $comp_data['userEmail'];
									$company = $comp_data['companyName'];
									$customer = $comp_data['fullName'];

									$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
								}
							}
						}
					} else {
						$amount_data = $amount;
						$transactiondata = array();
						$transactiondata['transactionID']       = $result['transactionid'];
						$transactiondata['transactionStatus']   = $result['responsetext'];
						$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
						$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
						$transactiondata['transactionCode']     = $result['response_code'];

						$transactiondata['transactionType']    = $result['type'];
						$transactiondata['gatewayID']          = $gatlistval;
						$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
						$transactiondata['customerListID']      = $customerID;
						$transactiondata['transactionAmount']   = $amount_data;
						$transactiondata['merchantID']          = $merchantID;
						$transactiondata['resellerID']        = $this->resellerID;
						$transactiondata['gateway']          = $gatewayName;


						$transactiondata = alterTransactionCode($transactiondata);
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					}



					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == "new1"  &&  $tc == 0) {
						$this->load->library('encrypt');
						$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
						$card_condition = array(
							'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
							'customerCardfriendlyName' => $friendlyname,
						);
						$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
						$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      =    $this->czsecurity->xssCleanPostInput('cvv');

						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and merchantID='" . $merchantID . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

						$crdata =   $query->row_array()['numrow'];

						if ($crdata > 0) {
							$card_type = $this->general_model->getType($card_no);
							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	     => $exyear,
								'CardType' => $card_type,
								'companyID'    => $companyID,
								'merchantID'   => $merchantID,
								'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
								'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
								'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
								'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
								'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
								'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
								'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
								'CustomerCard' => $this->card_model->encrypt($card_no),
								'CardCVV'      => '',
								'updatedAt'    => date("Y-m-d H:i:s")
							);

							$this->db1->update('customer_card_data', $card_condition, $card_data);
						} else {
							$card_type = $this->general_model->getType($card_no);
							$customerListID = $this->czsecurity->xsscleanpostinput('customerID');
							$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	     => $exyear,
								'CardType' => $card_type,
								'CustomerCard' => $this->card_model->encrypt($card_no),
								'CardCVV'      => '', 
								'customerListID' => $customerListID,
								'companyID'     => $companyID,
								'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
								'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
								'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
								'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
								'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
								'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
								'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
								'merchantID'   => $merchantID,
								'is_default'			   => $is_default,
								'customerCardfriendlyName' => $friendlyname,
								'createdAt' 	=> date("Y-m-d H:i:s")
							);

							$id1 = $this->db1->insert('customer_card_data', $card_data);
						}
					}

					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
					$transactiondata = array();
					$transactiondata['transactionID']       = $result['transactionid'];
					$transactiondata['transactionStatus']    = $result['responsetext'];
					$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
					$transactiondata['transactionCode']     = $result['response_code'];

					$transactiondata['transactionType']    = $result['type'];
					$transactiondata['gatewayID']          = $gatlistval;
					$transactiondata['transactionGateway']    = $gt_result['gatewayType'];
					$transactiondata['customerListID']      = $customerID;
					$transactiondata['transactionAmount']   = $amount;
					$transactiondata['merchantID']          = $merchantID;
					if (!empty($invoiceIDs)) {
						$transactiondata['invoiceID']            = implode(',', $invoiceIDs);
					}
					$transactiondata['resellerID']   = $this->resellerID;
					$transactiondata['gateway']   = $gatewayName;

					$transactiondata = alterTransactionCode($transactiondata);
					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					if(!empty($this->transactionByUser)){
					    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
					$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
			}

			redirect('QBO_controllers/Payments/create_customer_sale', 'refresh');
		}


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];

		$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_sale', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function create_customer_sale()
	{

		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {

				$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
			}
			$amount = $this->czsecurity->xssCleanPostInput('totalamount');
			$invoiceIDs = array();
			$invoicePayAmounts = array();
			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
				$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
			}
			
			$payIndex = 0;
			
			$checkPlan = check_free_plan_transactions();

			if ($checkPlan && $this->form_validation->run() == true) {

				
				$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				if (!empty($gt_result)) {
					$nmiuser   = $gt_result['gatewayUsername'];
					$nmipass   = $gt_result['gatewayPassword'];
					$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
					$invoiceIDs = array();
					$custom_data_fields = [];
					$applySurcharge = false;
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$applySurcharge = true;
						$custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}

					if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
						$custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
					}

					$customerID = $this->czsecurity->xssCleanPostInput('customerID');

					$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
					$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
					$companyID  = $comp_data['companyID'];


					$cardID = $this->czsecurity->xssCleanPostInput('card_list');

					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						if ($this->czsecurity->xssCleanPostInput('cvv') != "")
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv');
					} else {
						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no  = $card_data['CardNo'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						if ($card_data['CardCVV'])
							$cvv      = $card_data['CardCVV'];
						$cardType = $card_data['CardType'];
						$address1 = $card_data['Billing_Addr1'];
						$city     =  $card_data['Billing_City'];
						$zipcode  = $card_data['Billing_Zipcode'];
						$state    = $card_data['Billing_State'];
						$country  = $card_data['Billing_Country'];
					}
					$cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');


					$transaction = new nmiDirectPost($nmi_data);

					if ($this->czsecurity->xsscleanpostinput('card_number') != "" && $this->czsecurity->xsscleanpostinput('card_list') == 'new1') {
						$card_no = $this->czsecurity->xsscleanpostinput('card_number');
						$transaction->setCcNumber($this->czsecurity->xsscleanpostinput('card_number'));
						$expmonth =  $this->czsecurity->xsscleanpostinput('expiry');
						$cardType = $this->general_model->getType($card_no);
						$exyear   = $this->czsecurity->xsscleanpostinput('expiry_year');
						$exyear   = substr($exyear, 2);
						$expry    = $expmonth . $exyear;
						$transaction->setCcExp($expry);
						$cvv = $this->czsecurity->xssCleanPostInput('cvv');
					} else {

						$cardID = $this->czsecurity->xssCleanPostInput('card_list');

						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no = $card_data['CardNo'];
						$transaction->setCcNumber($card_data['CardNo']);
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						if (strlen($exyear) > 2) {
							$minLength = 4 - strlen($exyear);
							$exyear   = substr($exyear, $minLength);
						}
						$cardType = $card_data['CardType'];

						if (strlen($expmonth) == 1) {
							$expmonth = '0' . $expmonth;
						}
						$expry    = $expmonth . $exyear;
						$transaction->setCcExp($expry);
						$cvv = $card_data['CardCVV'];
					}
					/*Added card type in transaction table*/
	                $cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;

					$cvv = trim($cvv);
					if($cvv && !empty($cvv)){
						$transaction->setCvv($cvv);
					}
					
					#Billing Details
					$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->setAddress1($this->czsecurity->xssCleanPostInput('baddress1'));
					$transaction->setAddress2($this->czsecurity->xssCleanPostInput('baddress2'));
					$transaction->setCountry($this->czsecurity->xssCleanPostInput('bcountry'));
					$transaction->setCity($this->czsecurity->xssCleanPostInput('bcity'));
					$transaction->setState($this->czsecurity->xssCleanPostInput('bstate'));
					$transaction->setZip($this->czsecurity->xssCleanPostInput('bzipcode'));
					$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));
					$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));

					#Shipping Details
					$transaction->setShippingCompany($this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->setShippingFirstName($this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->setShippingLastName($this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
					$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
					$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
					$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
					$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
					$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
					$transaction->setShippingEmail($this->czsecurity->xssCleanPostInput('email'));

					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
						$transaction->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $new_invoice_number);
					}

					if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
						$transaction->setPoNumber($this->czsecurity->xssCleanPostInput('po_number'));
					}

					$amount = $this->czsecurity->xssCleanPostInput('totalamount');
					// update amount with surcharge 
	                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
	                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
	                    $amount += round($surchargeAmount, 2);
	                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
	                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
	                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
	                    
	                }
	                $totalamount  = $amount;

					//add level III data
					$level_request_data = [
						'transaction' => $transaction,
						'card_no' => $card_no,
						'merchID' => $user_id,
						'amount' => $amount,
						'invoice_id' => '',
						'gateway' => 1
					];
					$transaction = addlevelThreeDataInTransaction($level_request_data);

					$transaction->setAmount($amount);
					$transaction->sale();
					$result = $transaction->execute();

					if ($result['response_code'] == '100') {

							

						$trID = $result['transactionid'];
						$invoiceIDs = array();
						$invoicePayAmounts = array();
						if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
							$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
							$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
						}



						$val = array('merchantID' => $user_id);
						$data = $this->general_model->get_row_data('QBO_token', $val);

						$accessToken = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];

						$dataService = DataService::Configure(array(
							'auth_mode' => $this->config->item('AuthMode'),
							'ClientID'  => $this->config->item('client_id'),
							'ClientSecret' => $this->config->item('client_secret'),
							'accessTokenKey' =>  $accessToken,
							'refreshTokenKey' => $refreshToken,
							'QBORealmID' => $realmID,
							'baseUrl' => $this->config->item('QBOURL'),
						));

						$refNumber = array();

						if (!empty($invoiceIDs)) {
							$payIndex = 0;
							$saleAmountRemaining = $amount;
							foreach ($invoiceIDs as $inID) {
								$theInvoice = array();


								$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

								$con = array('invoiceID' => $inID, 'merchantID' => $user_id);
								$res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

								
								$refNumber[]   =  $res1['refNumber'];
								$txnID      = $inID;
								$pay_amounts = 0;


								$amount_data = $res1['BalanceRemaining'];
								$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
								if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

	                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
	                                $actualInvoicePayAmount += $surchargeAmount;
	                                $amount_data += $surchargeAmount;
	                                $updatedInvoiceData = [
	                                    'inID' => $inID,
	                                    'targetInvoiceArray' => $targetInvoiceArray,
	                                    'merchantID' => $user_id,
	                                    'dataService' => $dataService,
	                                    'realmID' => $realmID,
	                                    'accessToken' => $accessToken,
	                                    'refreshToken' => $refreshToken,
	                                    'amount' => $surchargeAmount,
	                                ];
	                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
	                            }
								$ispaid 	 = 0;
								$isRun = 0;
								if($saleAmountRemaining > 0){
									$BalanceRemaining = 0.00;
	                                if($amount_data == $actualInvoicePayAmount){
										$actualInvoicePayAmount = $amount_data;
										$isPaid 	 = 1;

									}else{

										$actualInvoicePayAmount = $actualInvoicePayAmount;
										$isPaid 	 = 0;
										$BalanceRemaining = $amount_data - $actualInvoicePayAmount;
										
									}
	                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

	                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
									
									$this->general_model->update_row_data('QBO_test_invoice', $con, $data);

									if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
										$theInvoice = current($targetInvoiceArray);

										$createPaymentObject = [
											"TotalAmt" => $actualInvoicePayAmount,
											"SyncToken" => 1,
											"CustomerRef" => $customerID,
											"Line" => [
												"LinkedTxn" => [
													"TxnId" => $inID,
													"TxnType" => "Invoice",
												],
												"Amount" => $actualInvoicePayAmount
											]
										];
				
										$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
										if($paymentMethod){
											$createPaymentObject['PaymentMethodRef'] = [
												'value' => $paymentMethod['payment_id']
											];
										}
										if(isset($trID) && $trID != '')
				                        {
				                            $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
				                        }
				
										$newPaymentObj = Payment::create($createPaymentObject);

										$savedPayment = $dataService->Add($newPaymentObj);



										$error = $dataService->getLastError();
										if ($error != null) {
											$err = '';
											$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
											$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
											$err .= "The Response message is: " . $error->getResponseBody() . "\n";
											$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

											$st = '0';
											$action = 'Pay Invoice';
											$msg = "Payment Success but " . $error->getResponseBody();

											$qbID = $trID;
											$pinv_id = '';
										} else {
											$pinv_id = '';
											$pinv_id        =  $savedPayment->Id;
											$st = '1';
											$action = 'Pay Invoice';
											$msg = "Payment Success";
											$qbID = $trID;

										
										}
										$qbID = $inID;
										$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

										$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
										if($syncid){
				                            $qbSyncID = 'CQ-'.$syncid;

											$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

				                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
				                        }
										$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
									}
								}

								$payIndex++;	

							}

							
						} else {

							$crtxnID = '';
							$inID = '';
							$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
						}
						$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
						if (!empty($refNumber))
							$ref_number = implode(',', $refNumber);
						else
							$ref_number = '';
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						if ($chh_mail == '1') {
							


							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
						}

						if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

							$card_type  = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
							$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
							$card_condition = array(
								'customerListID' => $customerID,
								'customerCardfriendlyName' => $friendlyname,
							);

							$crdata =	$this->card_model->check_friendly_name($customerID, $friendlyname);


							if (!empty($crdata)) {

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	     => $exyear,
									'companyID'    => $companyID,
									'merchantID'   => $user_id,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '', 
									'updatedAt'    => date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,

									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $phone,
									'Billing_Zipcode'	 => $zipcode,
								);



								$this->card_model->update_card_data($card_condition, $card_data);
							} else {
								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '', 
									'customerListID' => $customerID,
									'companyID'     => $companyID,
									'merchantID'   => $user_id,
									'customerCardfriendlyName' => $friendlyname,
									'createdAt' 	=> date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,

									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $phone,
									'Billing_Zipcode'	 => $zipcode,
								);

								$id1 =    $this->card_model->insert_card_data($card_data);
							}
						}
						$this->session->set_flashdata('success', 'Transaction Successful');
					} else {
						$crtxnID = '';

						$msg = $result['responsetext'];
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
						$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
				}
			} else {


				$error = 'Validation Error. Please fill the requred fields';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
			}
			$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => (isset($result) && isset($result['transactionid'])) ? $result['transactionid'] : '',
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['merchID'] 	= $user_id;

		$this->customerSync($user_id);

		$qboOBJ = new QBO_data($user_id); 
        $qboOBJ->get_payment_method();
		
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if (!merchant_gateway_allowed($merchant_condition)) {
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];
		$merchant_surcharge = $this->general_model->get_row_data('tbl_merchant_surcharge', array('merchantID' => $user_id));
		$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$data['plantype'] = $plantype;
		$data['merchant_surcharge'] = $merchant_surcharge;
		$planData = $this->general_model->chk_merch_plantype_data($user_id);
		$is_es_plan = false;
		if(!empty($planData)){
			if($planData->merchant_plan_type == 'SS' || $planData->merchant_plan_type == 'ES'){
				$is_es_plan = true;
			}
		}
		$data['is_es_plan'] = $is_es_plan;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_sale', $data);
		$this->load->view('QBO_views/page_popup_modals', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}





	/*****************Authorize Transaction***************/



	public function create_customer_auth()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}

			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			$checkPlan = check_free_plan_transactions();
			$custom_data_fields = [];
			$po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
			if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  = $gt_result['gatewayPassword'];
				$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";


				$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
				$companyID  = $comp_data['companyID'];
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');

				$transaction = new nmiDirectPost($nmi_data);

				if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
					$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
					$card_no = $this->czsecurity->xssCleanPostInput('card_number');

					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

					$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					$exyear   = substr($exyear, 2);
					$expry    = $expmonth . $exyear;
					$transaction->setCcExp($expry);
					$cvv = $this->czsecurity->xssCleanPostInput('cvv');
				} else {

					$cardID = $this->czsecurity->xssCleanPostInput('card_list');

					$card_data = $this->card_model->get_single_card_data($cardID);
					$card_no = $card_data['CardNo'];
					$transaction->setCcNumber($card_data['CardNo']);
					$expmonth =  $card_data['cardMonth'];

					$exyear   = $card_data['cardYear'];
					if (strlen($exyear) > 2) {
						$minLength = 4 - strlen($exyear);
						$exyear   = substr($exyear, $minLength);
					}
					
					if (strlen($expmonth) == 1) {
						$expmonth = '0' . $expmonth;
					}
					$expry    = $expmonth . $exyear;
					$transaction->setCcExp($expry);
					$cvv = $card_data['CardCVV'];
				}
				$cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$cvv = trim($cvv);
				if($cvv && !empty($cvv)){
					$transaction->setCvv($cvv);
				}

				#Billing Details
				$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
				$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->setAddress1($this->czsecurity->xssCleanPostInput('baddress1'));
				$transaction->setAddress2($this->czsecurity->xssCleanPostInput('baddress2'));
				$transaction->setCountry($this->czsecurity->xssCleanPostInput('bcountry'));
				$transaction->setCity($this->czsecurity->xssCleanPostInput('bcity'));
				$transaction->setState($this->czsecurity->xssCleanPostInput('bstate'));
				$transaction->setZip($this->czsecurity->xssCleanPostInput('bzipcode'));
				$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));
				$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));

				#Shipping Details
				$transaction->setShippingCompany($this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->setShippingFirstName($this->czsecurity->xssCleanPostInput('firstName'));
				$transaction->setShippingLastName($this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction->setShippingEmail($this->czsecurity->xssCleanPostInput('email'));

				$amount = $this->czsecurity->xssCleanPostInput('totalamount');
				$new_invoice_number = '';
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_number');
                    $transaction->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $new_invoice_number);
                }

				//add level III data
				$level_request_data = [
					'transaction' => $transaction,
					'card_no' => $card_no,
					'merchID' => $merchantID,
					'amount' => $amount,
					'invoice_id' => $new_invoice_number,
					'gateway' => 1
				];
				if (!empty($po_number)) {
                    $transaction->setPoNumber($po_number);
                    $level_request_data['ponumber'] = $po_number;
                }
				$transaction = addlevelThreeDataInTransaction($level_request_data);

				$transaction->setAmount($amount);
				$transaction->setTax('tax');
				$transaction->auth();
				$result = $transaction->execute();

				if ($result['response_code'] == '100') {

					/* This block is created for saving Card info in encrypted form  */

					if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

						$this->load->library('encrypt');
						$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
						$card_type  = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
						$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);

						$card_condition = array(
							'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
							'customerCardfriendlyName' => $friendlyname,
						);
						$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
						$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      =    $this->czsecurity->xssCleanPostInput('cvv');

						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and merchantID='" . $merchantID . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

						$crdata =   $query->row_array()['numrow'];

						if ($crdata > 0) {

							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	     => $exyear,
								'companyID'    => $companyID,
								'merchantID'   => $merchantID,
								'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
								'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
								'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
								'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
								'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
								'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
								'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
								'CustomerCard' => $this->card_model->encrypt($card_no),
								'CardCVV'      => '', 
								'updatedAt'    => date("Y-m-d H:i:s")
							);

							$this->db1->update('customer_card_data', $card_condition, $card_data);
						} else {
							$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($customerID,$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	 => $exyear,
								'CustomerCard' => $this->card_model->encrypt($card_no),
								'CardCVV'      => '', 
								'customerListID' => $customerID,
								'companyID'     => $companyID,
								'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
								'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
								'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
								'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
								'Billing_Contact' => $this->czsecurity->xssCleanPostInput('bphone'),
								'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
								'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
								'merchantID'   => $merchantID,
								'is_default'			   => $is_default,
								'customerCardfriendlyName' => $friendlyname,
								'createdAt' 	=> date("Y-m-d H:i:s")
							);

							$id1 = $this->db1->insert('customer_card_data', $card_data);
						}
						$custom_data_fields['card_type'] = $cardType;
					}


					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
				}

				$transactiondata = array();
				$transactiondata['transactionID']       = $result['transactionid'];
				$transactiondata['transactionStatus']   = $result['responsetext'];
				$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
				$transactiondata['transactionCode']     = $result['response_code'];
				$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
				$transactiondata['transactionType']    = $result['type'];
				$transactiondata['gatewayID']          = $gatlistval;
				$transactiondata['transaction_user_status'] = '5';
				$transactiondata['transactionGateway']  = $gt_result['gatewayType'];
				$transactiondata['customerListID']      = $customerID;
				$transactiondata['transactionAmount']   = $amount;
				$transactiondata['merchantID']          = $merchantID;
				$transactiondata['resellerID']        = $this->resellerID;
				$transactiondata['gateway']   = $gatewayName;
				$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
				$transactiondata = alterTransactionCode($transactiondata);
				$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				if($custom_data_fields){
                    $transactiondata['custom_data_fields'] = json_encode($custom_data_fields);
                }
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
			}
			$invoice_IDs = array();
		
			$receipt_data = array(
				'transaction_id' => $result['transactionid'],
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'QBO_controllers/Payments/create_customer_auth',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Authorize',
				'checkPlan'	=> $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			
			redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['merchID'] 	= $user_id;

		$this->customerSync($user_id);


		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if (!merchant_gateway_allowed($merchant_condition)) {
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);

		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']      = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];
		$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_auth', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/*****************Refund Transaction***************/
	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if ($this->session->userdata('logged_in')) {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}elseif ($this->session->userdata('user_logged_in')) {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}
		if (!empty($this->input->post(null, true))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnID');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);

			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

			$nmiuser  = $gt_result['gatewayUsername'];
			$nmipass  =  $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
			$gatewayName = getGatewayName($gt_result['gatewayType']);
			$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

			$transaction = new nmiDirectPost($nmi_data);
			$customerID = $paydata['customerListID'];



			$total   = $this->czsecurity->xssCleanPostInput('ref_amount');
			$amount     = $total;



			$transaction->setTransactionId($tID);
			
			$transaction->refund($tID, $amount);

			$result     = $transaction->execute();


			if ($result['response_code'] == '100') {


				$val = array(
					'merchantID' => $paydata['merchantID'],
				);
				$data = $this->general_model->get_row_data('QBO_token', $val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				$realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
					'auth_mode' => $this->config->item('AuthMode'),
					'ClientID'  => $this->config->item('client_id'),
					'ClientSecret' => $this->config->item('client_secret'),
					'accessTokenKey' =>  $accessToken,
					'refreshTokenKey' => $refreshToken,
					'QBORealmID' => $realmID,
					'baseUrl' => $this->config->item('QBOURL'),
				));

				$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");


				$refund = $amount;
				$ref_inv = explode(',', $paydata['invoiceID']);
				if (!empty($paydata['invoiceID'])) {






					$item_data = $this->qbo_customer_model->get_merc_invoice_item_data($paydata['invoiceID'],$paydata['merchantID']);
                            

                    if (!empty($item_data)) {
                        $lineArray = array();
                               
                                $i = 0;
                                for ($i = 0; $i < count($item_data); $i++) {
                                    $LineObj = Line::create([
                                        "Amount"              => $item_data[$i]['itemQty']*$item_data[$i]['itemPrice'],
                                        "DetailType"          => "SalesItemLineDetail",
                                        "Description" => $item_data[$i]['itemDescription'],
                                        "SalesItemLineDetail" => [
                                            "ItemRef" => $item_data[$i]['itemID'],
                                            "Qty" => $item_data[$i]['itemQty'],
											"UnitPrice" => $item_data[$i]['itemPrice'],
											"TaxCodeRef" => [
												"value" => (isset($item_data[$i]['itemTax']) && $item_data[$i]['itemTax']) ? "TAX" : "NON",
											]
                                        ],
                
                                    ]);
                                    $lineArray[] = $LineObj;
                                }
								$acc_id = '';
								$acc_name = '';
						
						$acc_data = $this->general_model->get_select_data('QBO_accounts_list', array('accountID'), array('accountName' => 'Undeposited Funds', 'merchantID' => $paydata['merchantID']));
						if (!empty($acc_data)) {
							$ac_value = $acc_data['accountID'];
						} else {
							$ac_value = '4';
						}
						$invoice_tax = $this->qbo_customer_model->get_merc_invoice_tax_data($paydata['invoiceID'],$paydata['merchantID']);
						$theResourceObj = RefundReceipt::create([
							"CustomerRef" => $paydata['customerListID'],
							"Line" => $lineArray,
							"DepositToAccountRef" =>  [
								"value" => $ac_value,
								"name" => "Undeposited Funds",
							],
							"TxnTaxDetail" => [
								"TxnTaxCodeRef" => [
									"value" => (isset($invoice_tax['taxID']) && $invoice_tax['taxID']) ? $invoice_tax['taxID'] : "",
								],
							],
						]);
						$resultingObj = $dataService->Add($theResourceObj);
						$error = $dataService->getLastError();

						$ins_id = '';
						if ($error != null) {


							$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund ' . $error->getResponseBody() . '</strong></div>');
							
						} else {
							$ins_id = $resultingObj->Id;
						}
						$refnd_trr = array(
							'merchantID' => $paydata['merchantID'], 'refundAmount' => $total,
							'creditInvoiceID' => $paydata['invoiceID'], 'creditTransactionID' => $tID,
							'creditTxnID' => $ins_id, 'refundCustomerID' => $paydata['customerListID'],
							'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'),

						);

						$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
					}
				} else {

					$ins_id = '';
					$refnd_trr = array(
						'merchantID' => $paydata['merchantID'], 'refundAmount' => $total,
						'creditInvoiceID' => $paydata['invoiceID'], 'creditTransactionID' => $tID,
						'creditTxnID' => $ins_id, 'refundCustomerID' => $paydata['customerListID'],
						'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'),

					);

					$this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
				}


				$this->qbo_customer_model->update_refund_payment($tID, 'NMI');


				$this->session->set_flashdata('success', 'Successfully Refunded Payment');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong>.</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $result['transactionid'];
			$transactiondata['transactionStatus']  = $result['responsetext'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result['type'];
			$transactiondata['transactionCode']   = $result['response_code'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']         = $gatlistval;
			$transactiondata['customerListID']     = $customerID;
			if (!empty($paydata['invoiceID'])) {
				$transactiondata['invoiceID']  = $paydata['invoiceID'];
			}
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']  = $merchantID;
			$transactiondata['resellerID']   = $this->resellerID;
			$transactiondata['gateway']   = $gatewayName;
			
			$transactiondata = alterTransactionCode($transactiondata);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			$invoice_IDs = array();
			$receipt_data = array(
				'proccess_url' => 'QBO_controllers/Payments/payment_refund',
				'proccess_btn_text' => 'Process New Refund',
				'sub_header' => 'Refund',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result['transactionid'] == ''){
				$result['transactionid'] ='null';
			}
			redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result['transactionid'],  'refresh');

			
		}


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['merchID'];

		$compdata				= $this->customer_model->get_customers($user_id);

		$data['customers']		= $compdata;


		$this->load->view('template/template_start', $data);



		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_refund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/*****************Capture Transaction***************/

	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
			$tID     = $this->czsecurity->xssCleanPostInput('txnID');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			if ($paydata['gatewayID'] > 0) {

				$gatlistval = $paydata['gatewayID'];

				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  =  $gt_result['gatewayPassword'];
				$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

				$transaction = new nmiDirectPost($nmi_data);



				$customerID = $paydata['customerListID'];
				$amount  =  $paydata['transactionAmount'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

				$transaction->setTransactionId($tID);
				$transaction->setAmount($amount);
				if ($paydata['transactionType'] == 'auth') {

					$transaction->capture($tID, $amount);
				}
				$result     = $transaction->execute();

				if ($result['response_code'] == '100') {


					$condition = array('transactionID' => $tID);

					$update_data =   array('transaction_user_status' => "4");

					$this->general_model->update_row_data('customer_transaction', $condition, $update_data);
					$condition = array('transactionID' => $tID);
					$customerID = $paydata['customerListID'];


					$tr_date   = date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['userEmail'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['fullName'];
					if ($chh_mail == '1') {
						
						$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
					}
					
					$this->session->set_flashdata('success', 'Successfully Captured Authorization');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
				}

				$transactiondata = array();
				$transactiondata['transactionID']      = $result['transactionid'];
				$transactiondata['transactionStatus']  = $result['responsetext'];
				$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
				$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
				$transactiondata['transactionType']    = $result['type'];
				$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
				$transactiondata['gatewayID']        = $gatlistval;
				$transactiondata['transactionCode']   = $result['response_code'];
				$transactiondata['customerListID']     = $customerID;
				$transactiondata['transactionAmount']  = $amount;
				$transactiondata['merchantID']         =  $merchantID;
				$transactiondata['resellerID']        = $this->resellerID;
				$transactiondata['gateway']   = $gatewayName;

				$transactiondata = alterTransactionCode($transactiondata);
				$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
			$invoice_IDs = array();
			
		
			$receipt_data = array(
				'proccess_url' => 'QBO_controllers/Payments/payment_capture',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Capture',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='null';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result['transactionid'] == ''){
				$result['transactionid'] ='null';
			}
			redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result['transactionid'],  'refresh'); 
		}


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['id'];

		$compdata				= $this->customer_model->get_customers($user_id);

		$data['customers']		= $compdata;


		$this->load->view('template/template_start', $data);



		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_transaction', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/*****************Void Transaction***************/


	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {


				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {

				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}


			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$gatlistval = $paydata['gatewayID'];
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;

			$nmiuser  = $gt_result['gatewayUsername'];
			$nmipass  =  $gt_result['gatewayPassword'];

			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
			$gatewayName = getGatewayName($gt_result['gatewayType']);
			$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

			$transaction = new nmiDirectPost($nmi_data);



			$customerID = $paydata['customerListID'];
			$amount  =  $paydata['transactionAmount'];


			$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
			$transaction->setTransactionId($tID);

			$transaction->void($tID);

			$result     = $transaction->execute();

			if ($result['response_code'] == '100') {

				
				$condition = array('transactionID' => $tID);

				$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

				$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

				if ($chh_mail == '1') {
					$condition = array('transactionID' => $tID);
					$customerID = $paydata['customerListID'];

					$tr_date   = date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['userEmail'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['fullName'];
					$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
				}

				
				$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $result['transactionid'];
			$transactiondata['transactionStatus']  = $result['responsetext'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result['type'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']        = $gatlistval;
			$transactiondata['transactionCode']   = $result['response_code'];
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']         = $merchantID;
			$transactiondata['resellerID']         = $this->resellerID;
			$transactiondata['gateway']   = $gatewayName;

			$transactiondata = alterTransactionCode($transactiondata);
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);


			redirect('QBO_controllers/Payments/payment_capture', 'refresh');
		}
	}



	public function payment_capture()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']   = $this->qbo_customer_model->get_transaction_data_captue($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_capture', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function refund_transaction()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['transactions']   = $this->qbo_customer_model->get_refund_transaction_data($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_refund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}




	public function payment_refund()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']   = $this->qbo_customer_model->get_transaction_datarefund($user_id);

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_refund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function payment_transaction_old()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$this->qboOBJ = new QBO_data($user_id); 
		$this->qboOBJ->get_invoice_data();

		$data['transactions']   = $this->qbo_customer_model->get_transaction_history_data($user_id);

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_transaction', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/* ---------------------- Ajax Payment Transaction Start --------------------- */
	public function ajax_payment_list()
	{
		error_reporting(0);
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$list  = $this->qbo_invoices_model->get_datatables_transaction($user_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();

			$res_inv = '';
			if (!empty($person->invoiceID) && preg_match('/^[0-9,]+$/', $person->invoiceID)) {
				$inv = $person->invoiceID;

				$invList = explode(',', $inv);

				foreach($invList as $key1 => $value) {
					$value = trim($value);
					if(empty($value) || $value == ''){
						unset($invList[$key1]);	
					}
				}

				$inv = implode(',', $invList); 

				$qq = $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  QBO_test_invoice where invoiceID IN($inv) and  merchantID=$user_id  GROUP BY merchantID ");

				if ($qq->num_rows > 0) {
					$res_inv = $qq->row_array()['invoce'];
				}
			}

			$invoice = ($res_inv) ? $res_inv : '--';
			$balance = ($person->transactionAmount) ? number_format($person->transactionAmount, 2) : '0.00';
			$transactionID = ($person->transactionID) ? $person->transactionID : '---';

			$amount = ($person->transactionAmount) ? $person->transactionAmount : '0.00';


			$row[] = $person->fullName;
			
			$row[] = '<div class="hidden-xs text-right"> ' . $invoice . ' </div>';
			$row[] = '<div class="hidden-xs text-right"> $' . $balance . ' </div>';
			$row[] = "<div class='hidden-xs text-right'> " . date('M d, Y', strtotime($person->transactionDate)) . " </div>";
			$type = '';
			if ($person->partial != '0') {

				$type .= "<label class='label label-warning'>Partially Refunded :" . number_format($person->partial, 2) . " </label><br/>";
			} else if ($person->partial == $person->transactionAmount) {
				$type .= "<label class='label label-success'>Fully Refunded : " . number_format($person->partial, 2) . "</label><br/>";
			} else {

				$type .= "";
			}

			
			$row[] = "<div class='hidden-xs text-right'>" . $type . strtoupper($person->transactionType);

			$row[] = '<div class="hidden-xs text-right"> ' . $transactionID . ' </div>';

			$link = '';


			$link .= '<div class="text-center"> <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">';

			if (
				in_array($person->transactionCode, array('100', '200', '111', '1')) &&  in_array(strtoupper($person->transactionType), array('SALE', 'AUTH_CAPTURE', 'Offline Payment', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
			) {

				$link .= '<li> <a href="#payment_refunds" class="" onclick="set_refund_pay(' . $person->id . ', ' . $person->transactionID . ', ' . $person->transactionGateway . ', ' . $amount . ' );" 
        					 data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>';
			}

			$link .= '<li><a href="#payment_delete" class=""  onclick="set_transaction_pay(' . $person->id . ');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a></li>
					        </ul>
					   </div> </div>';



			$row[] = $link;

			$data[] = $row;
		}



		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->qbo_invoices_model->count_all_transaction($user_id),
			"recordsFiltered" => $this->qbo_invoices_model->count_filtered_transaction($user_id),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
		die;
	}



	/* ---------------------- Ajax Payment Transaction End --------------------- */




	/*********ECheck Transactions**********/

	public function evoid_transaction()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']    = $this->qbo_customer_model->get_transaction_data_erefund($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_ecapture', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function echeck_transaction()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']    = $this->qbo_customer_model->get_transaction_data_erefund($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_erefund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function payment_erefund()
	{
		//Show a form here which collects someone's name and e-mail address
		$merchantID = $this->session->userdata('logged_in')['merchID'];
		if (!empty($this->input->post(null, true))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnID');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);



			$gatlistval = $paydata['gatewayID'];

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			$nmiuser  = $gt_result['gatewayUsername'];
			$nmipass  =  $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);


			$transaction = new nmiDirectPost($nmi_data);
			$customerID = $paydata['customerListID'];

			
			$amount     =  $paydata['transactionAmount'];

			$transaction->setPayment('check');
			$transaction->setTransactionId($tID);
			
			$transaction->refund($tID, $amount);

			$result     = $transaction->execute();



			if ($result['response_code'] == '100') {

				$this->qbo_customer_model->update_refund_payment($tID, 'NMI');

				$this->session->set_flashdata('success', 'Successfully Refunded Payment');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong>.</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $result['transactionid'];
			$transactiondata['transactionStatus']  = $result['responsetext'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']       = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result['type'];
			$transactiondata['transactionCode']   = $result['response_code'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']         = $gatlistval;
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']        = $merchantID;
			$transactiondata['resellerID']        = $this->resellerID;
			$transactiondata['gateway']   = "NMI ECheck";

			$transactiondata = alterTransactionCode($transactiondata);
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);

			redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');
		}
	}
	public function payment_evoid()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if (!empty($this->input->post(null, true))) {

			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$gatlistval = $paydata['gatewayID'];
			$merchantID = $this->session->userdata('logged_in')['merchID'];
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));


			$nmiuser  = $gt_result['gatewayUsername'];
			$nmipass  =  $gt_result['gatewayPassword'];

			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);


			$transaction = new nmiDirectPost($nmi_data);



			$customerID = $paydata['customerListID'];
			$amount  =  $paydata['transactionAmount'];
			$transaction->setPayment('check');
			$transaction->setTransactionId($tID);

			$transaction->void($tID);

			$result     = $transaction->execute();

			if ($result['response_code'] == '100') {

				
				$condition = array('transactionID' => $tID);

				$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

				$this->general_model->update_row_data('customer_transaction', $condition, $update_data);


				$this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $result['transactionid'];
			$transactiondata['transactionStatus']  = $result['responsetext'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result['type'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']        = $gatlistval;
			$transactiondata['transactionCode']   = $result['response_code'];
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']         = $merchantID;
			$transactiondata['resellerID']        = $this->resellerID;
			$transactiondata['gateway']   = "NMI ECheck";
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
			$transactiondata = alterTransactionCode($transactiondata);
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);


			redirect('QBO_controllers/Payments/evoid_transaction', 'refresh');
		}
	}




	public function create_customer_esale()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$merchant_condition = [
				'merchID' => $merchantID,
			];

			$user_id = $merchantID;

			$custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

			if (!merchant_gateway_allowed($merchant_condition)) {
				$gt_result = $this->general_model->merchant_default_gateway($merchant_condition);
				$gt_result = $gt_result[0];

				$gatlistval = $gt_result['gatewayID'];
			} else {
				$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			}

			$checkPlan = check_free_plan_transactions();

			if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
				$nmiuser  = $gt_result['gatewayUsername'];
				$nmipass  = $gt_result['gatewayPassword'];
				$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

				$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
				$companyID  = $comp_data['companyID'];
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');

				$payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
				$sec_code =     'WEB';

				if($payableAccount == '' || $payableAccount == 'new1') {
					$accountDetails = [
						'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
						'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
						'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'companyID'     => $companyID,
						'merchantID'   => $merchantID,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $sec_code
					];
				} else {
					$accountDetails = $this->card_model->get_single_card_data($payableAccount);
				}
				$accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$transaction = new nmiDirectPost($nmi_data);
				
				$transaction->setAccountName($accountDetails['accountName']);
				$transaction->setAccount($accountDetails['accountNumber']);
				$transaction->setRouting($accountDetails['routeNumber']);
				
				$transaction->setAccountType($accountDetails['accountType']);
				$transaction->setAccountHolderType($accountDetails['accountHolderType']);
				$transaction->setSecCode($sec_code);
				$transaction->setPayment('check');
				
				#Billing Details
				$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
				$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->setAddress1($this->czsecurity->xssCleanPostInput('baddress1'));
				$transaction->setAddress2($this->czsecurity->xssCleanPostInput('baddress2'));
				$transaction->setCountry($this->czsecurity->xssCleanPostInput('bcountry'));
				$transaction->setCity($this->czsecurity->xssCleanPostInput('bcity'));
				$transaction->setState($this->czsecurity->xssCleanPostInput('bstate'));
				$transaction->setZip($this->czsecurity->xssCleanPostInput('bzipcode'));
				$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));

				#Shipping Details
				$transaction->setShippingCompany($this->czsecurity->xssCleanPostInput('companyName'));
				$transaction->setShippingFirstName($this->czsecurity->xssCleanPostInput('firstName'));
				$transaction->setShippingLastName($this->czsecurity->xssCleanPostInput('lastName'));
				$transaction->setShippingAddress1($this->czsecurity->xssCleanPostInput('address1'));
				$transaction->setShippingAddress2($this->czsecurity->xssCleanPostInput('address2'));
				$transaction->setShippingCountry($this->czsecurity->xssCleanPostInput('country'));
				$transaction->setShippingCity($this->czsecurity->xssCleanPostInput('city'));
				$transaction->setShippingState($this->czsecurity->xssCleanPostInput('state'));
				$transaction->setShippingZip($this->czsecurity->xssCleanPostInput('zipcode'));
				$transaction->setShippingEmail($this->czsecurity->xssCleanPostInput('email'));
				
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
					$transaction->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '.$new_invoice_number);
				}

				if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
					$transaction->setPoNumber($this->czsecurity->xssCleanPostInput('po_number'));
				}

				$amount = $this->czsecurity->xssCleanPostInput('totalamount');
				$transaction->setAmount($amount);
				$transaction->setTax('tax');
				$transaction->sale();
				
				$result = $transaction->execute();

				if ($result['response_code'] == '100') {

					$trID = $result['transactionid'];

					$invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xsscleanpostinput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xsscleanpostinput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xsscleanpostinput('invoice_pay_amount'));
                    }

					$val = array('merchantID' => $user_id);
					$tokenData = $this->general_model->get_row_data('QBO_token', $val);

					$accessToken = $tokenData['accessToken'];
					$refreshToken = $tokenData['refreshToken'];
					$realmID      = $tokenData['realmID'];

					$dataService = DataService::Configure(array(
						'auth_mode' => $this->config->item('AuthMode'),
						'ClientID'  => $this->config->item('client_id'),
						'ClientSecret' => $this->config->item('client_secret'),
						'accessTokenKey' =>  $accessToken,
						'refreshTokenKey' => $refreshToken,
						'QBORealmID' => $realmID,
						'baseUrl' => $this->config->item('QBOURL'),
					));

					$refNumber = array();
                    $merchantID = $user_id;
					if (!empty($invoiceIDs)) {
						$payIndex = 0;
                        $saleAmountRemaining = $amount;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();


							$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
							$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");


							$con = array('invoiceID' => $inID, 'merchantID' => $user_id);
							$res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;
                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            $ispaid      = 0;
                            $isRun = 0;
                            $BalanceRemaining = 0.00;
                            if($amount_data == $actualInvoicePayAmount){
                                $actualInvoicePayAmount = $amount_data;
                                $isPaid      = 1;

                            }else{

                                $actualInvoicePayAmount = $actualInvoicePayAmount;
                                $isPaid      = 0;
                                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                
                            }
                            $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;
                            $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
                            $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

							if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
								$theInvoice = current($targetInvoiceArray);

								$createPaymentObject = [
									"TotalAmt" => $actualInvoicePayAmount,
									"SyncToken" => 1,
									"CustomerRef" => $customerID,
									"Line" => [
										"LinkedTxn" => [
											"TxnId" => $inID,
											"TxnType" => "Invoice",
										],
										"Amount" => $actualInvoicePayAmount
									]
								];
		
								$paymentMethod = $this->general_model->qbo_payment_method('Check', $this->merchantID, true);
								if($paymentMethod){
									$createPaymentObject['PaymentMethodRef'] = [
										'value' => $paymentMethod['payment_id']
									];
								}
								if(isset($trID) && $trID != '')
		                        {
		                            $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
		                        }
								$newPaymentObj = Payment::create($createPaymentObject);

								$savedPayment = $dataService->Add($newPaymentObj);

								$error = $dataService->getLastError();
								if ($error != null) {
									$err = '';
									$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
									$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
									$err .= "The Response message is: " . $error->getResponseBody() . "\n";
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Success but " . $error->getResponseBody();

									$qbID = $trID;
									$pinv_id = '';
								} else {
									$pinv_id = '';
									$pinv_id        =  $savedPayment->Id;
									$st = '1';
									$action = 'Pay Invoice';
									$msg = "Payment Success";
									$qbID = $trID;
								}
								$qbID = $inID;
								$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

								$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
								if($syncid){
		                            $qbSyncID = 'CQ-'.$syncid;

		                            

									$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

		                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
		                        }
								$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
							}
							$payIndex++;
						}
					} else {

						$crtxnID = '';
						$inID = '';
						$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
					}
					
					if ($this->czsecurity->xssCleanPostInput('tr_checked'))
                        $chh_mail = 1;
                    else
						$chh_mail = 0;
						
					//send mail on esale
					$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
					$ref_number = '';
					$tr_date   = date('Y-m-d H:i:s');
					$toEmail = $comp_data['userEmail'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['fullName'];
					
					if($payableAccount == '' || $payableAccount == 'new1') {
						$id1 = $this->card_model->process_ack_account($accountDetails);
					}
					if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
					}
				
					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
				}

			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
			}

			$invoice_IDs = array();
			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
				$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
			}

			$receipt_data = array(
				'transaction_id' => $result['transactionid'],
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'QBO_controllers/Payments/create_customer_esale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
				'checkPlan'	=> $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_name'] 	= "NMI ESale";


		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['merchID'] 	= $user_id;
		$this->customerSync($user_id);

		$qboOBJ = new QBO_data($user_id); 
        $qboOBJ->get_payment_method();

		$condition				= array('merchantID' => $user_id);

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}
		
		$gateway		= $this->general_model->get_gateway_data($user_id, 'echeck');
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		
		$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_esale', $data);
		$this->load->view('QBO_views/page_popup_modals', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/**********END****************/

	public function chk_friendly_name()
	{
		$res = array();
		$frname  = $this->czsecurity->xssCleanPostInput('frname');
		$condition = array('gatewayFriendlyName' => $frname);
		$num   = $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
		if ($num) {
			$res = array('gfrname' => 'Friendly name already exist', 'status' => 'false');
		} else {
			$res = array('status' => 'true');
		}
		echo json_encode($res);
		die;
	}


	/*****************Test NMI Validity***************/

	public function testNMI()
	{


		$nmiuser  = $this->czsecurity->xssCleanPostInput('nmiuser');
		$nmipass  = $this->czsecurity->xssCleanPostInput('nmipassword');



		$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

		$transaction = new nmiDirectPost($nmi_data);

		$transaction->setOrderDescription('Some Item');
		$transaction->setAmount('1.00');
		$transaction->setTax('1.00');
		$transaction->setShipping('1.00');

		$transaction->setCcNumber('4111111111111111');
		$transaction->setCcExp('1113');
		$transaction->setCvv('999');

		$transaction->setCompany('Some company');
		$transaction->setFirstName('John');
		$transaction->setLastName('Smith');
		$transaction->setAddress1('888');
		$transaction->setCity('Dallas');
		$transaction->setState('TX');
		$transaction->setZip('77777');
		$transaction->setPhone('5555555555');
		$transaction->setEmail('test@domain.com');

		$transaction->auth();

		$result = $transaction->execute();

		if ($result['responsetext'] == "SUCCESS") {

			$res = array('status' => 'true');
			echo json_encode($res);
		} else {

			$error = array('nmiPassword' => 'User name or password not matched', 'status' => 'false');
			echo json_encode($error);
		}
	}




	//-------------------------  START -----------------------------//

	//------------------ To view the credit -----------------------//	 

	public function credit()
	{



		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}



		$merchID = $user_id;

		$condition =  array('merchantID' => $merchID);


		$val = array(
			'merchantID' => $merchID,
		);

		$data = $this->general_model->get_row_data('QBO_token', $val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];

		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' => $this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),

		));

		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
		$qbo_data = $this->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery'));

		if (!empty($qbo_data)) {


			$entities = $dataService->Query("SELECT * FROM CreditMemo where Metadata.LastUpdatedTime > '" . $qbo_data['lastUpdated'] . "' ");
		} else {
			$entities = $dataService->Query("SELECT * FROM CreditMemo ");
		}


		//Add a new Invoice
		$eror = '';
		$error = $dataService->getLastError();
		if ($error != null) {
			$eror .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
			$eror .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$eror .= "The Response message is: " . $error->getResponseBody() . "\n";
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> ' . $eror . '</div>');
			redirect(base_url('QBO_controllers/home/index'));
		}

		if (!empty($qbo_data)) {
			$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('00:00:00');
			$updatedata['updatedAt'] = date('Y-m-d H:i:s');

			$this->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery'), $updatedata);
		} else {
			$updateda = date('Y-m-d') . 'T' . date('00:00:00');
			$upteda = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
			$this->general_model->insert_row('tbl_qbo_config', $upteda);
		}



		if (!empty($entities)) {

			foreach ($entities as $oneCredit) {

				$qb_credit['TxnID'] =  $oneCredit->Id;
				$qb_credit['CustomerListID']   = $oneCredit->CustomerRef;
				$qb_credit['RefNumber']        = ($oneCredit->AutoDocNumber) ? $oneCredit->AutoDocNumber : $oneCredit->DocNumber;
				$qb_credit['CreditRemaining']  = $oneCredit->RemainingCredit;
				$qb_credit['AccountListID']    = $oneCredit->ARAccountRef;
				$qb_credit['TxnDate']          = $oneCredit->TxnDate;
				$qb_credit['DueDate']          = $oneCredit->DueDate;
				$qb_credit['TxnNumber']        = $oneCredit->DocNumber;
				$qb_credit['customerMemoNote'] = $oneCredit->CustomerMemo;
				$qb_credit['creditMemoNote'] = $oneCredit->PrivateNote;
				$qb_credit['TimeCreated']       = date('Y-m-d H:i:s', strtotime($oneCredit->MetaData->CreateTime));
				$qb_credit['TimeModified']      =  date('Y-m-d H:i:s', strtotime($oneCredit->MetaData->LastUpdatedTime));
				$qb_credit['SubTotal']          = $oneCredit->TxnDate;
				$qb_credit['TotalAmount']       = $oneCredit->TotalAmt;
				$qb_credit['merchantID']        = $merchID;
				$qb_credit['companyID']          = $realmID;

				if ($this->general_model->get_num_rows('qbo_customer_credit', array('companyID' => $realmID, 'TxnID' => $oneCredit->Id, 'merchantID' => $merchID)) > 0) {



					$this->general_model->update_row_data('qbo_customer_credit', array('companyID' => $realmID, 'TxnID' => $oneCredit->Id, 'merchantID' => $merchID), $qb_credit);
				} else {

					$this->general_model->insert_row('qbo_customer_credit', $qb_credit);

				}


			}
		}






		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		
		$condition  = array('cr.merchantID' => $user_id);

		$data['credits'] = $this->qbo_customer_model->get_credit_user_data($user_id);



		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_credit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	//----------- TO update the credit  --------------//

	public function update_credit()
	{
		if ($this->czsecurity->xssCleanPostInput('creditEditID') != "") {

			$id = $this->czsecurity->xssCleanPostInput('creditEditID');
			$chk_condition = array('creditID' => $id);

			$input_data['creditDate']  = date("Y-m-d");
			$input_data['creditAmount']  = $this->czsecurity->xssCleanPostInput('amount');
			$input_data['creditDescription']  = $this->czsecurity->xssCleanPostInput('description');

			if ($this->general_model->update_row_data('tbl_credits', $chk_condition, $input_data)) {

				$this->session->set_flashdata('success', 'Successfully Updated');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
		}
		redirect(base_url('QBO_controllers/Payments/credit'));
	}





	public function get_creditedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('credit_id');
		$val = array(
			'creditID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_credits', $val);
		echo json_encode($data);
	}



	//-------------for view credit  data--------------//


	public function get_credit_id()

	{

		$creditID          =  $this->czsecurity->xssCleanPostInput('customerID');
		$condition 		= array('creditName' => $creditID);
		$creditdatas		= $this->general_model->get_table_data('tbl_credits', $condition);

?>

		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-right"> <strong> Credit Date</strong></th>

					<th class="text-right"><strong>Amount ($)</strong></th>
					<th class="text-right"><strong> Processed On</strong></th>
					<th class="text-left"><strong> Edit/Delete</strong></th>
				</tr>
				<?php
				if (!empty($creditdatas)) {
					foreach ($creditdatas as $creditdata) {
				?>

						<tr>
							<td class="text-right visible-lg"><?php echo date('F d, Y', strtotime($creditdata['creditDate'])); ?> </a> </td>
							<td class="text-right visible-lg"> <?php echo number_format($creditdata['creditAmount'], 2); ?> </a> </td>
							<td class="text-right visible-lg"> <?php if ($creditdata['creditStatus'] == "0") {
																	echo "Yet to Processed";
																} else {
																	echo $creditdata['creditDate'];
																} ?> </a>
							</td>

							<td class="text-left visible-lg"> <a href="javascript:void(0);" class="btn btn-default" onclick="set_edit_credit('<?php echo $creditdata['creditID'];  ?>');" title="Edit"> <i class="fa fa-edit"> </i> </a>

								<a href="#qbo_del_credit" onclick="qbo_del_credit_id('<?php echo $creditdata['creditID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Delete Credit" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>



							</td>


						</tr>


				<?php     }
				}  ?>

			</tbody>
		</table>


		<?php die;
	}



	public function get_card_data()
	{
		$customerdata = array();
		if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
			$crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_single_card_data($crID);
			if (!empty($card_data)) {
				$customerdata['status'] =  'success';
				$customerdata['card']     = $card_data;
				echo json_encode($customerdata);
				die;
			}
		}
	}



	/**************Delete credit********************/

	public function delete_credit()
	{

		$creditID = $this->czsecurity->xssCleanPostInput('qbo_invoicecreditid');
		$condition =  array('creditID' => $creditID);
		$del      = $this->general_model->delete_row_data('tbl_credits', $condition);

		if ($del) {

			$this->session->set_flashdata('success', 'Successfully Deleted');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Somthing Is Wrong...</div>');
		}

		redirect(base_url('QBO_controllers/Payments/credit'));
	}





	public function get_card_expiry_data($customerID)
	{

		$card = array();
		$this->load->library('encrypt');



		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'    ";
		$query1 = $this->db1->query($sql);
		$card_datas =   $query1->result_array();
		if (!empty($card_datas)) {
			foreach ($card_datas as $key => $card_data) {

				$customer_card['CardNo']  = substr($this->card_model->decrypt($card_data['CustomerCard']), 12);
				$customer_card['CardID'] = $card_data['CardID'];
				$customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
				$card[$key] = $customer_card;
			}
		}

		return  $card;
	}

	public function get_card_edit_data()
	{

		if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
			$cardID = $this->czsecurity->xssCleanPostInput('cardID');
			$data   = $this->card_model->get_single_mask_card_data($cardID);
			echo json_encode(array('status' => 'success', 'card' => $data));
			die;
		}
		echo json_encode(array('status' => 'success'));
		die;
	}





	public function check_qbo_vault()
	{


		$card = '';
		$card_name = '';
		$customerdata = array();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] = $this->session->userdata('logged_in');

			$merchantID = $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');

			$merchantID = $data['login_info']['merchantID'];
		}
		$gatewayID 		= $this->czsecurity->xssCleanPostInput('gatewayID');
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');

			$condition     =  array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('QBO_custom_customer', $condition);
			if (!empty($customerdata)) {

				$customerdata['status'] =  'success';
				$conditionGW = array('gatewayID' => $gatewayID);
				$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
				$cardTypeOption = 2;
				if(isset($gateway['gatewayID'])){
					if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 1;
					}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 2;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 3;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 4;
					}else{
						$cardTypeOption = 2;
					}
					
				}
				$customerdata['cardTypeOption']  = $cardTypeOption;
				$card_data =   $this->card_model->getCardData($customerID,$cardTypeOption);
				$customerdata['card']  = $card_data;

				echo json_encode($customerdata);
				die;
			}
		}else{
			$customerdata = [];
			$conditionGW = array('gatewayID' => $gatewayID);
			$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
			$cardTypeOption = 2;
			if(isset($gateway['gatewayID'])){
				if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 1;
				}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 2;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 3;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 4;
				}else{
					$cardTypeOption = 2;
				}
				
			}
			
			$customerdata['status']  = 'success';
			$customerdata['cardTypeOption']  = $cardTypeOption;
			echo json_encode($customerdata);
				die;
		}
	}


	public function view_transaction()
	{


		$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['merchID'];
		$transactions           = $this->qbo_customer_model->get_invoice_transaction_data($invoiceID, $user_id);

		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
		?>
				<tr>
					<td class="text-left"><?php echo ($transaction['transactionID']) ? $transaction['transactionID'] : '----'; ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($transaction['transactionAmount'], 2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo ucfirst($transaction['transactionType']); ?></td>
					<td class="text-right visible-lg"><?php if ($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') { ?> <span class="">Success</span><?php } else { ?> <span class="">Failed</span> <?php } ?></td>

				</tr>

<?php     }
		} else {
			echo '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}
		die;
	}




	public function pay_multi_invoiceOLD()
	{
		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$resellerID = $this->resellerID;
		$cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		$gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');

		$cusproID = '';
		$cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

		$checkPlan = check_free_plan_transactions();

		if ($checkPlan && $cardID != "" || $gateway != "") {

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));


			$nmiuser   = $gt_result['gatewayUsername'];
			$nmipass   = $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
			$gatewayName = getGatewayName($gt_result['gatewayType']);
			$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

			$invoices = $this->czsecurity->xssCleanPostInput('multi_inv');


			if (!empty($invoices)) {
				foreach ($invoices as $key => $invoiceID) {

					$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
					$in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

					if (!empty($in_data)) {

						$Customer_ListID = $in_data['CustomerListID'];
						$c_data   = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
						$companyID = $c_data['companyID'];
						if ($cardID == 'new1') {
							$cardID_upd  = $cardID;
							$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
							$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
							$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
							$cardType       = $this->general_model->getType($card_no);
							$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
						} else {
							$card_data    =   $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
							$cvv      =  $card_data['CardCVV'];
							$expmonth =  $card_data['cardMonth'];
							$exyear   = $card_data['cardYear'];
						}
						$cardType = $this->general_model->getType($card_no);
                		$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                		$custom_data_fields['payment_type'] = $friendlyname;

						$error = '';


						if (!empty($cardID)) {

							if ($in_data['BalanceRemaining'] > 0) {
								$cr_amount = 0;
								$amount1    = $pay_amounts;

								$amount    = $pay_amounts;
								$amount    = $amount - $cr_amount;

								if ($amount > $in_data['BalanceRemaining']) {
									$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error Payment Aomont has exceeded to actual amount</strong></div>');

									redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
								}

								$transaction1 = new nmiDirectPost($nmi_data);
								$transaction1->setCcNumber($card_no);

								$exyear   = substr($exyear, 2);
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
								}
								$expry    = $expmonth . $exyear;
								$transaction1->setCcExp($expry);
								$transaction1->setCvv($cvv);
								$transaction1->setAmount($amount);
								// add level III data
								$level_request_data = [
									'transaction' => $transaction1,
									'card_no' => $card_no,
									'merchID' => $merchID,
									'amount' => $amount,
									'invoice_id' => $invoiceID,
									'gateway' => 1
								];
								$transaction1 = addlevelThreeDataInTransaction($level_request_data);
								
								$transaction1->sale();
								$result = $transaction1->execute();



								if ($result['response_code'] == "100") {




									$val = array(
										'merchantID' => $merchID,
									);
									$data = $this->general_model->get_row_data('QBO_token', $val);

									$accessToken = $data['accessToken'];
									$refreshToken = $data['refreshToken'];
									$realmID      = $data['realmID'];
									$dataService = DataService::Configure(array(
										'auth_mode' => $this->config->item('AuthMode'),
										'ClientID'  => $this->config->item('client_id'),
										'ClientSecret' => $this->config->item('client_secret'),
										'accessTokenKey' =>  $accessToken,
										'refreshTokenKey' => $refreshToken,
										'QBORealmID' => $realmID,
										'baseUrl' => $this->config->item('QBOURL'),
									));

									$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

									$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");



									if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
										$theInvoice = current($targetInvoiceArray);
									}
									$updatedInvoice = Invoice::update($theInvoice, [
										"sparse " => 'true'
									]);

									$updatedResult = $dataService->Update($updatedInvoice);

									$error = $dataService->getLastError();

									$newPaymentObj = Payment::create([
										"TotalAmt" => $amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
											"LinkedTxn" => [
												"TxnId" => $updatedResult->Id,
												"TxnType" => "Invoice",
											],
											"Amount" => $amount
										]
									]);

									$savedPayment = $dataService->Add($newPaymentObj);
									$error = $dataService->getLastError();


									if ($error != null) {

										
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
										$st = '0';
										$action = 'Pay Invoice';
										$msg = "Payment Success but " . $error->getResponseBody();
										$qbID = $result['transactionid'];
									} else {

										
										$this->session->set_flashdata('success', 'Successfully Processed Invoice');


										$st = '1';
										$action = 'Pay Invoice';
										$msg = "Payment Success";
										$qbID = $result['transactionid'];
									}
									$invoiceID =	$updatedResult->Id;

									if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

										$this->load->library('encrypt');
										$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');

										
										$cardType       = $this->general_model->getType($card_no);
										$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
										$card_condition = array(
											'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
											'customerCardfriendlyName' => $friendlyname,
										);
										$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
										$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
										$exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
										$cvv      =    $this->czsecurity->xssCleanPostInput('cvv');

										$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

										$crdata =   $query->row_array()['numrow'];

										if ($crdata > 0) {

											$card_data = array(
												'cardMonth'   => $expmonth,
												'cardYear'	 => $exyear,
												'CardType'    => $cardType,
												'CustomerCard' => $this->card_model->encrypt($card_no),
												'CardCVV'      => '', 
												'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
												'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
												'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
												'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
												'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
												'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
												'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
												'customerListID' => $in_data['Customer_ListID'],
												'customerCardfriendlyName' => $friendlyname,
												'companyID'     => $companyID,
												'merchantID'   => $merchID,
												'updatedAt' 	=> date("Y-m-d H:i:s")
											);


											$this->db1->update('customer_card_data', $card_condition, $card_data);
										} else {
											$is_default = 0;
									     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
								        	if($checkCustomerCard == 0){
								        		$is_default = 1;
								        	}
											$card_data = array(
												'cardMonth'   => $expmonth,
												'cardYear'	 => $exyear,
												'CardType'    => $cardType,
												'CustomerCard' => $this->card_model->encrypt($card_no),
												'CardCVV'      => '',
												'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
												'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
												'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
												'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
												'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
												'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
												'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
												'customerListID' => $in_data['Customer_ListID'],
												'customerCardfriendlyName' => $friendlyname,
												'companyID'     => $companyID,
												'merchantID'   => $merchID,
												'is_default'			   => $is_default,
												'createdAt' 	=> date("Y-m-d H:i:s")
											);

											$id1 = $this->db1->insert('customer_card_data', $card_data);
										}
									}
								} else {
									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Failed";
									$qbID = $invoiceID;
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '.</div>');
								}
								$qbID = $invoiceID;
								$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result['transactionid'],'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

								$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
								if($syncid){
		                            $qbSyncID = 'CQ-'.$syncid;

									$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

		                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
		                        }
								$transaction['transactionID']      = $result['transactionid'];
								$transaction['transactionStatus']  = $result['responsetext'];
								$transaction['transactionCode']    =  $result['response_code'];
								$transaction['transactionType']   =  ($result['type']) ? $result['type'] : 'auto-nmi';
								$transaction['transactionDate']    = date('Y-m-d H:i:s');
								$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
								$transaction['gatewayID']          = $gateway;
								$transaction['transactionGateway']  = $gt_result['gatewayType'];
								$transaction['customerListID']     = $in_data['CustomerListID'];
								$transaction['transactionAmount']  = $amount;
								$transaction['merchantID']   = $merchID;
								if ($error == null) {
									$transaction['qbListTxnID']   = $savedPayment->Id;
								}
								$transaction['resellerID']   = $resellerID;
								$transaction['gateway']   = $gatewayName;
								$transaction['invoiceID']   = $invoiceID;

								$transaction = alterTransactionCode($transaction);
								$CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
								if(!empty($this->transactionByUser)){
								    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
								    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transaction);
							} else {
								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>');
							}
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
						}
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
					}
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card!</strong>.</div>');
		}

		if(!$checkPlan){
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

		if ($cusproID != "") {
			redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
		} else {
			redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
		}
	}




	public function pay_multi_invoice()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		$custom_data_fields = [];

		$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
		$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
		if ($this->czsecurity->xssCleanPostInput('CardID1') == "new1") {


			$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
			$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
			$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
		}

		$cusproID = '';
		$cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

		if ($this->form_validation->run() == true) {

			$cardID_upd = '';
			$invoices           = $this->czsecurity->xssCleanPostInput('multi_inv');
			$cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			$gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');

			$customerID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

			$txnID = '';
			$error = '';

			$invoiceIDs = implode(',', $invoices);

			$inv_data   =    $this->qbo_customer_model->get_due_invoice_data($invoiceIDs, $user_id);

			if (!empty($invoices) && !empty($inv_data)) {
				$cusproID      =   $customerID  = $inv_data['CustomerListID'];
				$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $user_id));
				$companyID  = $comp_data['companyID'];

				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

				if (!empty($gt_result)) {

					$Customer_ListID = $customerID;
					$amount  = $this->czsecurity->xssCleanPostInput('totalPay');
					$nmiuser   = $gt_result['gatewayUsername'];
					$nmipass   = $gt_result['gatewayPassword'];
					$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
					if ($cardID != "new1") {

						$card_data =   $this->card_model->get_single_card_data($cardID);
						$card_no  = $card_data['CardNo'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						$cvv      = $card_data['CardCVV'];
						$cardType = $card_data['CardType'];
						$address1 = $card_data['Billing_Addr1'];
						$address2 = $card_data['Billing_Addr2'];
						$city     =  $card_data['Billing_City'];
						$zipcode  = $card_data['Billing_Zipcode'];
						$state    = $card_data['Billing_State'];
						$country  = $card_data['Billing_Country'];
						$phone = $card_data['Billing_Contact'];
					} else {

						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cardType = $this->general_model->getType($card_no);
						$cvv = '';
						if ($this->czsecurity->xssCleanPostInput('cvv') != "")
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$address1 =  $this->czsecurity->xssCleanPostInput('address1');
						$address2 = $this->czsecurity->xssCleanPostInput('address2');
						$city    = $this->czsecurity->xssCleanPostInput('city');
						$country = $this->czsecurity->xssCleanPostInput('country');
						$phone   =  $this->czsecurity->xssCleanPostInput('phone');
						$state   =  $this->czsecurity->xssCleanPostInput('state');
						$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
					}
					$cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;


					$transaction1 = new nmiDirectPost($nmi_data);
					$transaction1->setCcNumber($card_no);

					if (strlen($exyear) > 2) {
						$minLength = 4 - strlen($exyear);
						$exyear    = substr($exyear, $minLength);
					}
					
					if (strlen($expmonth) == 1) {
						$expmonth = '0' . $expmonth;
					}
					$expry    = $expmonth . $exyear;
					$transaction1->setCcExp($expry);

					$cvv = trim($cvv);
					if($cvv && !empty($cvv)){
						$transaction1->setCvv($cvv);
					}

					$transaction1->setAmount($amount);

					// add level III data
					$level_request_data = [
						'transaction' => $transaction1,
						'card_no' => $card_no,
						'merchID' => $user_id,
						'amount' => $amount,
						'invoice_id' => '',
						'gateway' => 1
					];
					$transaction1 = addlevelThreeDataInTransaction($level_request_data);
					
					#Billing Details
					$transaction1->setCompany($comp_data['companyName']);
					$transaction1->setFirstName($comp_data['firstName']);
					$transaction1->setLastName($comp_data['lastName']);
					$transaction1->setAddress1($address1);
					$transaction1->setAddress2($address2);
					$transaction1->setCountry($country);
					$transaction1->setCity($city);
					$transaction1->setState($state);
					$transaction1->setZip($zipcode);
					$transaction1->setPhone($phone);

					#Shipping Details
					$transaction1->setShippingCompany($comp_data['companyName']);
					$transaction1->setShippingFirstName($comp_data['firstName']);
					$transaction1->setShippingLastName($comp_data['lastName']);
					$transaction1->setShippingAddress1($address1);
					$transaction1->setShippingAddress2($address2);
					$transaction1->setShippingCountry($country);
					$transaction1->setShippingCity($city);
					$transaction1->setShippingState($state);
					$transaction1->setShippingZip($zipcode);

					$transaction1->sale();
					$result = $transaction1->execute();
					$crtxnID = '';
					$res = $result;
					if ($result['response_code'] == "100") {
						$trID  = $result['transactionid'];
						$c_data     = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('Customer_ListID' => $customerID, 'merchantID' => $user_id));
						$companyID = $c_data['companyID'];


						$val = array('merchantID' => $user_id);
						$data = $this->general_model->get_row_data('QBO_token', $val);

						$accessToken = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];

						$dataService = DataService::Configure(array(
							'auth_mode' => $this->config->item('AuthMode'),
							'ClientID'  => $this->config->item('client_id'),
							'ClientSecret' => $this->config->item('client_secret'),
							'accessTokenKey' =>  $accessToken,
							'refreshTokenKey' => $refreshToken,
							'QBORealmID' => $realmID,
							'baseUrl' => $this->config->item('QBOURL'),
						));



						if (!empty($invoices)) {

							foreach ($invoices as $invoiceID) {
								$theInvoice = array();


								$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $invoiceID . "'");
								$condition  = array('invoiceID' => $invoiceID, 'merchantID' => $user_id);
								$in_data = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'Total_payment'), $condition);
								$pay_amounts = 0;
								$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
								$txnID       = $invoiceID;
								$ispaid   = 1;

								$bamount    = $in_data['BalanceRemaining'] - $pay_amounts;
								if ($bamount > 0)
									$ispaid 	 = 0;
								$app_amount = $in_data['Total_payment'] + $pay_amounts;
								$data   	 = array('IsPaid' => $ispaid, 'BalanceRemaining' => $bamount);
								$to_amount = $in_data['BalanceRemaining'] + $in_data['Total_payment'];

								$this->general_model->update_row_data('QBO_test_invoice', $condition, $data);

								if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
									$theInvoice = current($targetInvoiceArray);

									

									$createPaymentObject = [
										"TotalAmt" => $pay_amounts,
										"SyncToken" => 1,
										"CustomerRef" => $Customer_ListID,
										"Line" => [
											"LinkedTxn" => [
												"TxnId" => $invoiceID,
												"TxnType" => "Invoice",
											],
											"Amount" => $pay_amounts
										]
									];
			
									$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
									if($paymentMethod){
										$createPaymentObject['PaymentMethodRef'] = [
											'value' => $paymentMethod['payment_id']
										];
									}
									if(isset($trID) && $trID != '')
			                        {
			                            $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
			                        }
									$newPaymentObj = Payment::create($createPaymentObject);

									$savedPayment = $dataService->Add($newPaymentObj);



									$error = $dataService->getLastError();
									if ($error != null) {
										$err = '';
										$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
										$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
										$err .= "The Response message is: " . $error->getResponseBody() . "\n";
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

										$st = '0';
										$action = 'Pay Invoice';
										$msg = "Payment Success but " . $error->getResponseBody();

										$qbID = $trID;
										$pinv_id = '';
									} else {
										$pinv_id = '';
										$pinv_id        =  $savedPayment->Id;
										$st = '1';
										$action = 'Pay Invoice';
										$msg = "Payment Success";
										$qbID = $trID;

										$this->session->set_flashdata('success', 'Transaction Successful');
									}
									$qbID = $invoiceID;
									$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

									$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
									if($syncid){
			                            $qbSyncID = 'CQ-'.$syncid;

			                           

										$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

			                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
			                        }
									$id = $this->general_model->insert_gateway_transaction_data($res, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $pay_amounts, $user_id, $pinv_id, $this->resellerID, $txnID, false, $this->transactionByUser, $custom_data_fields);
								}
							}
						}

						if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

							$cardDetails = [
								'card_no' => $card_no,
								'cvv' => $cvv,
								'customerID' => $customerID,
								'expmonth' => $expmonth,
								'exyear' => $exyear,
								'companyID' => $c_data['companyID'],
								'user_id' => $user_id,
								'address1' => $address1,
								'city' => $city,
								'state' => $state,
								'country' => $country,
								'phone' => $phone,
								'zipcode' => $zipcode,
								'country' => $country,
							];
							
							save_qbo_card($cardDetails, $user_id);
						}


						$this->session->set_flashdata('success', 'Successfully Processed Invoice');
					} else {


						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '.</div>');
					}




				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Payment Authentication Failed! </strong>.</div>');
				}
			} else {
				$error = 'Invalid Invoice';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
			}
		} else {


			$error = 'Validation Error. Please fill the requred fields';
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
		}

		if ($cusproID != "") {
			redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
		} else {
			redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
		}
	}



	public function pay_multi_invoice11()
	{
		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$resellerID = $this->resellerID;
		$cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		$gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');

		$cusproID = '';
		$cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');


		if ($cardID != "" || $gateway != "") {

			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));


			$nmiuser   = $gt_result['gatewayUsername'];
			$nmipass   = $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
			$gatewayName = getGatewayName($gt_result['gatewayType']);
			$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

			$invoices = $this->czsecurity->xssCleanPostInput('multi_inv');


			if (!empty($invoices)) {
				foreach ($invoices as $key => $invoiceID) {

					$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
					$in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

					if (!empty($in_data)) {

						$Customer_ListID = $in_data['CustomerListID'];
						$c_data   = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
						$companyID = $c_data['companyID'];
						if ($cardID == 'new1') {
							$cardID_upd  = $cardID;
							$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
							$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
							$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
							$cardType       = $this->general_model->getType($card_no);
							$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
						} else {
							$card_data    =   $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
							$cvv      =  $card_data['CardCVV'];
							$expmonth =  $card_data['cardMonth'];
							$exyear   = $card_data['cardYear'];
						}
						$cardType = $this->general_model->getType($card_no);
                		$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                		$custom_data_fields['payment_type'] = $friendlyname;

						$error = '';


						if (!empty($cardID)) {

							if ($in_data['BalanceRemaining'] > 0) {
								$cr_amount = 0;
								$amount1    = $pay_amounts;

								$amount    = $pay_amounts;
								$amount     = $amount - $cr_amount;

								if ($amount > $in_data['BalanceRemaining']) {
									$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error Payment Aomont has exceeded to actual amount</strong></div>');

									redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
								}

								$transaction1 = new nmiDirectPost($nmi_data);
								$transaction1->setCcNumber($card_no);

								$exyear   = substr($exyear, 2);
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
								}
								$expry    = $expmonth . $exyear;
								$transaction1->setCcExp($expry);
								$transaction1->setCvv($cvv);
								$transaction1->setAmount($amount);
								// add level III data
								$level_request_data = [
									'transaction' => $transaction1,
									'card_no' => $card_no,
									'merchID' => $merchID,
									'amount' => $amount,
									'invoice_id' => $invoiceID,
									'gateway' => 1
								];
								$transaction1 = addlevelThreeDataInTransaction($level_request_data);
								$transaction1->sale();
								$result = $transaction1->execute();



								if ($result['response_code'] == "100") {




									$val = array(
										'merchantID' => $merchID,
									);
									$data = $this->general_model->get_row_data('QBO_token', $val);

									$accessToken = $data['accessToken'];
									$refreshToken = $data['refreshToken'];
									$realmID      = $data['realmID'];
									$dataService = DataService::Configure(array(
										'auth_mode' => $this->config->item('AuthMode'),
										'ClientID'  => $this->config->item('client_id'),
										'ClientSecret' => $this->config->item('client_secret'),
										'accessTokenKey' =>  $accessToken,
										'refreshTokenKey' => $refreshToken,
										'QBORealmID' => $realmID,
										'baseUrl' => $this->config->item('QBOURL'),
									));

									$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

									$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");



									if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
										$theInvoice = current($targetInvoiceArray);
									}
									$updatedInvoice = Invoice::update($theInvoice, [
										"sparse " => 'true'
									]);

									$updatedResult = $dataService->Update($updatedInvoice);

									$error = $dataService->getLastError();

									$newPaymentObj = Payment::create([
										"TotalAmt" => $amount,
										"SyncToken" => $updatedResult->SyncToken,
										"CustomerRef" => $updatedResult->CustomerRef,
										"Line" => [
											"LinkedTxn" => [
												"TxnId" => $updatedResult->Id,
												"TxnType" => "Invoice",
											],
											"Amount" => $amount
										]
									]);

									$savedPayment = $dataService->Add($newPaymentObj);
									$error = $dataService->getLastError();


									if ($error != null) {

									
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
										$st = '0';
										$action = 'Pay Invoice';
										$msg = "Payment Success but " . $error->getResponseBody();
										$qbID = $result['transactionid'];
									} else {
										

										$this->session->set_flashdata('success', 'Successfully Processed Invoice');

										$st = '1';
										$action = 'Pay Invoice';
										$msg = "Payment Success";
										$qbID = $result['transactionid'];
									}
									$invoiceID =	$updatedResult->Id;

									if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

										$this->load->library('encrypt');
										$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');

										
										$cardType       = $this->general_model->getType($card_no);
										$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
										$card_condition = array(
											'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
											'customerCardfriendlyName' => $friendlyname,
										);
										$cid      =    $this->czsecurity->xssCleanPostInput('customerID');
										$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
										$exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
										$cvv      =    $this->czsecurity->xssCleanPostInput('cvv');

										$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

										$crdata =   $query->row_array()['numrow'];

										if ($crdata > 0) {

											$card_data = array(
												'cardMonth'   => $expmonth,
												'cardYear'	 => $exyear,
												'CardType'    => $cardType,
												'CustomerCard' => $this->card_model->encrypt($card_no),
												'CardCVV'      => '',
												'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
												'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
												'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
												'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
												'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
												'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
												'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
												'customerListID' => $in_data['Customer_ListID'],
												'customerCardfriendlyName' => $friendlyname,
												'companyID'     => $companyID,
												'merchantID'   => $merchID,
												'updatedAt' 	=> date("Y-m-d H:i:s")
											);


											$this->db1->update('customer_card_data', $card_condition, $card_data);
										} else {
											$is_default = 0;
									     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
								        	if($checkCustomerCard == 0){
								        		$is_default = 1;
								        	}
											$card_data = array(
												'cardMonth'   => $expmonth,
												'cardYear'	 => $exyear,
												'CardType'    => $cardType,
												'CustomerCard' => $this->card_model->encrypt($card_no),
												'CardCVV'      => '',
												'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
												'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
												'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
												'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
												'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
												'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
												'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
												'customerListID' => $in_data['Customer_ListID'],
												'customerCardfriendlyName' => $friendlyname,
												'companyID'     => $companyID,
												'merchantID'   => $merchID,
												'is_default'	=> $is_default,
												'createdAt' 	=> date("Y-m-d H:i:s")
											);
											$id1 = $this->db1->insert('customer_card_data', $card_data);
										}
									}
								} else {
									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Failed";
									$qbID = $invoiceID;
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '.</div>');
								}
								$qbID = $invoiceID;
								$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result['transactionid'],'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

								$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
								if($syncid){
		                            $qbSyncID = 'CQ-'.$syncid;

									$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
		                           
		                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
		                        }
								$transaction['transactionID']      = $result['transactionid'];
								$transaction['transactionStatus']  = $result['responsetext'];
								$transaction['transactionCode']    =  $result['response_code'];
								$transaction['transactionType']   =  ($result['type']) ? $result['type'] : 'auto-nmi';
								$transaction['transactionDate']    = date('Y-m-d H:i:s');
								$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
								$transaction['gatewayID']          = $gateway;
								$transaction['transactionGateway']  = $gt_result['gatewayType'];
								$transaction['customerListID']     = $in_data['CustomerListID'];
								$transaction['transactionAmount']  = $amount;
								$transaction['merchantID']   = $merchID;
								if ($error == null) {
									$transaction['qbListTxnID']   = $savedPayment->Id;
								}
								$transaction['resellerID']   = $resellerID;
								$transaction['gateway']   = $gatewayName;
								$transaction['invoiceID']   = $invoiceID;
								
								$transaction = alterTransactionCode($transaction);
								$CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
								if(!empty($this->transactionByUser)){
								    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
								    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
								}
								$id = $this->general_model->insert_row('customer_transaction',   $transaction);
							} else {
								$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>');
							}
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
						}
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
					}
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card!</strong>.</div>');
		}


		if ($cusproID != "") {
			redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
		} else {
			redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
		}
	}







	public function check_vault()
	{


		$card = '';
		$card_name = '';
		$customerdata = array();
		$invoiceIDs = [];
		$gatewayID 		= $this->czsecurity->xssCleanPostInput('gatewayID');
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			if ($this->session->userdata('logged_in')) {
				$data['login_info'] = $this->session->userdata('logged_in');
	
				$merchantID = $data['login_info']['merchID'];
			} else if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] = $this->session->userdata('user_logged_in');
	
				$merchantID = $data['login_info']['merchantID'];
			}

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');
			$condition     =  array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('QBO_custom_customer', $condition);

			if (!empty($customerdata)) {
				$this->qboOBJ = new QBO_data($merchantID); 

				$this->qboOBJ->get_invoice_data(['customerId' => $customerID]);

				$invoices = $this->qbo_customer_model->get_invoice_upcomming_data($customerID, $merchantID);

				$table = '';
				$new_inv = '';
				$totalInvoiceAmount = 0.00;
				if (!empty($invoices)) {

					#Invoice existence check at QBO
					$invoices = $this->qboOBJ->check_qbo_invoice_mul($invoices);

					if(!empty($invoices)) {
						

						$new_inv = '<div class="form-group alignTableInvoiceList" >
				      	<div class="col-md-1 text-center"><b></b></div>
				        	<div class="col-md-3 text-left"><b>Number</b></div>
				        	<div class="col-md-3 text-right"><b>Due Date</b></div>
				        	<div class="col-md-2 text-right"><b>Amount</b></div>
				        	<div class="col-md-3 text-left"><b>Payment</b></div>
					    </div>';

						foreach ($invoices as $inv) {
							if ($inv['status'] != 'Cancel') {

								$new_inv .= '<div class="form-group alignTableInvoiceList" >
				       
					         	<div class="col-md-1 text-center"><input checked type="checkbox" class="chk_pay check_'.$inv['refNumber'].'" id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
					        	<div class="col-md-3 text-left">' . $inv['refNumber'] . '</div>
					        	<div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
					        	<div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
						   		<div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['invoiceID'] . '  geter" data-id="multiinv' . $inv['invoiceID'] . '" data-inv="' . $inv['invoiceID'] . '" data-ref="' . $inv['refNumber'] . '" data-value="' . $inv['BalanceRemaining'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
						   		</div>';
								$inv_data[] = [
									'RefNumber' => $inv['refNumber'],
									'TxnID' => $inv['invoiceID'],
									'BalanceRemaining' => $inv['BalanceRemaining'],
								];
								$invoiceIDs[$inv['invoiceID']] =  $inv['refNumber'];
								$totalInvoiceAmount = $totalInvoiceAmount + $inv['BalanceRemaining'];
								
							}
						}
					}
				}

				if(empty($customerdata['companyName'])){
					$customerdata['companyName'] = $customerdata['fullName'];
				}
				
				$customerdata['invoices'] = $new_inv;
				$customerdata['invoiceIDs'] = $invoiceIDs;
				$customerdata['totalInvoiceAmount'] = $totalInvoiceAmount;
				$customerdata['status'] =  'success';

				$ach_data =   $this->card_model->get_ach_info_data($customerID);
				$ACH = [];
				if(!empty($ach_data)){
					foreach($ach_data as $card){
						$ACH[] = $card['CardID'];
					}
				}
				$recentACH = end($ACH);
				$customerdata['ach_data']  = $ach_data;
				$customerdata['recent_ach_account']  = $recentACH;

				$conditionGW = array('gatewayID' => $gatewayID);
				$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
				$cardTypeOption = 2;
				if(isset($gateway['gatewayID'])){
					if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 1;
					}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 2;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 3;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 4;
					}else{
						$cardTypeOption = 2;
					}
					
				}
				$customerdata['cardTypeOption']  = $cardTypeOption;
				$card_data =   $this->card_model->getCardData($customerID,$cardTypeOption);
				$customerdata['card']  = $card_data;
				$recentCard = end($card_data);
				$customerdata['recent_card']  = ($recentCard && isset($recentCard['CardID'])) ? $recentCard['CardID'] : '';
				$customerdata['invoiceIDs'] = $invoiceIDs;
				echo json_encode($customerdata);
				die;
			}
		}else{
			$customerdata = [];
			$conditionGW = array('gatewayID' => $gatewayID);
			$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
			$cardTypeOption = 2;
			if(isset($gateway['gatewayID'])){
				if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 1;
				}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 2;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 3;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 4;
				}else{
					$cardTypeOption = 2;
				}
				
			}
			
			$customerdata['status']  = 'success';
			$customerdata['cardTypeOption']  = $cardTypeOption;
			echo json_encode($customerdata);
				die;
		}
	}



	//pawan code here

	public function get_invoice()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$id = '';
		if (!empty($this->czsecurity->xssCleanPostInput('id'))) {
			$id = $this->czsecurity->xssCleanPostInput('id');
		}
		$invoices = $this->qbo_customer_model->get_invoice_upcomming_data($id, $merchID);
		if ($invoices) {
			echo '<table class="mytable" width="100%">';
			echo "<tr><th>Select</th><th>Ref Number</th><th>Total Amount</th></tr>";

			foreach ($invoices as $inv) {
				echo "<tr><td><input type='checkbox' name='inv' value='" . $inv['BalanceRemaining'] . "' class='test' rel='" . $inv['id'] . "'/></td><td>" . $inv['refNumber'] . "</td><td>" . $inv['BalanceRemaining'] . "</td>";
			}

			echo '</table>';
		} else {
			echo 'No pending Invoices Found';
		}
	}





	public function check_transaction_payment()
	{
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$da 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}

		if (!empty($this->czsecurity->xssCleanPostInput('trID'))) {


			$trID = $this->czsecurity->xssCleanPostInput('trID');
			$av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');

			$tr_data = $this->general_model->get_select_data('customer_transaction', array('transactionID'), array('id' => $trID));
			$tID     = $tr_data['transactionID'];
			$p_data = $this->qbo_customer_model->get_transaction_details_data(array('tr.transactionID' => $tID, 'cust.merchantID' => $user_id, 'tr.merchantID' => $user_id));
			if (!empty($p_data)) {
				if ($p_data['transactionAmount'] >= $av_amount) {
					$resdata['status']  = 'success';
				} else {
					$resdata['status']  = 'error';
					$resdata['message']  = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
				}
			} else {
				$resdata['status']  = 'error';
				$resdata['message']  = 'Invalid transactions ';
			}
		} else {
			$resdata['status']  = 'error';
			$resdata['message']  = 'Invalid request';
		}
		echo json_encode($resdata);
		die;
	}



	public function create_customer_sale_new()
	{


		if ($this->session->userdata('logged_in')) {


			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		if (!empty($this->input->post(null, true))) {
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {

				$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
			}



			if ($this->form_validation->run() == true) {


				$gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				if (!empty($gt_result)) {
					$nmiuser   = $gt_result['gatewayUsername'];
					$nmipass   = $gt_result['gatewayPassword'];
					$nmi_data  = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
					$invoiceIDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}

					$customerID = $this->czsecurity->xssCleanPostInput('customerID');

					$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
					$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
					$companyID  = $comp_data['companyID'];


					$cardID = $this->czsecurity->xssCleanPostInput('card_list');

					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						if ($this->czsecurity->xssCleanPostInput('cvv') != "")
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv');
					} else {
						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no  = $card_data['CardNo'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						if ($card_data['CardCVV'])
							$cvv      = $card_data['CardCVV'];
						$cardType = $card_data['CardType'];
						$address1 = $card_data['Billing_Addr1'];
						$city     =  $card_data['Billing_City'];
						$zipcode  = $card_data['Billing_Zipcode'];
						$state    = $card_data['Billing_State'];
						$country  = $card_data['Billing_Country'];
					}
					$cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');


					$transaction = new nmiDirectPost($nmi_data);

					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == 'new1') {
						$card_no = $this->czsecurity->xssCleanPostInput('card_number');
						$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');

						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear, 2);
						$expry    = $expmonth . $exyear;
						$transaction->setCcExp($expry);
						$transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));
					} else {

						$cardID = $this->czsecurity->xssCleanPostInput('card_list');

						$card_data = $this->card_model->get_single_card_data($cardID);
						$card_no = $card_data['CardNo'];
						$transaction->setCcNumber($card_data['CardNo']);
						$expmonth =  $card_data['cardMonth'];

						$exyear   = $card_data['cardYear'];
						$exyear   = substr($exyear, 2);
						if (strlen($expmonth) == 1) {
							$expmonth = '0' . $expmonth;
						}
						$expry    = $expmonth . $exyear;
						$transaction->setCcExp($expry);
						$transaction->setCvv($card_data['CardCVV']);
					}
					$cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;
	                
					$transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
					$transaction->setFirstName($this->czsecurity->xssCleanPostInput('firstName'));
					$transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
					$transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
					$transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
					$transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
					$transaction->setState($this->czsecurity->xssCleanPostInput('state'));
					$transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
					$transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));

					$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
					$amount = $this->czsecurity->xssCleanPostInput('totalamount');
					$transaction->setAmount($amount);

					// add level III data
					$level_request_data = [
						'transaction' => $transaction,
						'card_no' => $card_no,
						'merchID' => $user_id,
						'amount' => $amount,
						'invoice_id' => '',
						'gateway' => 1
					];
					$transaction = addlevelThreeDataInTransaction($level_request_data);
					$transaction->sale();
					$result = $transaction->execute();

					if ($result['response_code'] == '100') {



						$trID = $result['transactionid'];
						$invoiceIDs = array();
						if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
							$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						}



						$val = array('merchantID' => $user_id);
						$data = $this->general_model->get_row_data('QBO_token', $val);

						$accessToken = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];

						$dataService = DataService::Configure(array(
							'auth_mode' => $this->config->item('AuthMode'),
							'ClientID'  => $this->config->item('client_id'),
							'ClientSecret' => $this->config->item('client_secret'),
							'accessTokenKey' =>  $accessToken,
							'refreshTokenKey' => $refreshToken,
							'QBORealmID' => $realmID,
							'baseUrl' => $this->config->item('QBOURL'),
						));

						$refNumber = array();

						if (!empty($invoiceIDs)) {

							foreach ($invoiceIDs as $inID) {
								$theInvoice = array();


								$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");


								$con = array('invoiceID' => $inID, 'merchantID' => $user_id);
								$res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment'), $con);
								$amount_data = $res1['BalanceRemaining'];
								$refNumber[]   =  $res1['refNumber'];
								$txnID      = $inID;
								$ispaid 	 = 1;

								$app_amount = $res1['Total_payment'] + $amount;
								$data   	 = array('IsPaid' => $ispaid, 'BalanceRemaining' => '0.00');

								$this->general_model->update_row_data('QBO_test_invoice', $con, $data);

								if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
									$theInvoice = current($targetInvoiceArray);

									

									$newPaymentObj = Payment::create([
										"TotalAmt" => $app_amount,
										"SyncToken" => 1,
										"CustomerRef" => $customerID,
										"Line" => [
											"LinkedTxn" => [
												"TxnId" => $inID,
												"TxnType" => "Invoice",
											],
											"Amount" => $amount_data
										]
									]);

									$savedPayment = $dataService->Add($newPaymentObj);



									$error = $dataService->getLastError();
									if ($error != null) {
										$err = '';
										$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
										$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
										$err .= "The Response message is: " . $error->getResponseBody() . "\n";
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

										$st = '0';
										$action = 'Pay Invoice';
										$msg = "Payment Success but " . $error->getResponseBody();

										$qbID = $trID;
										$pinv_id = '';
									} else {
										$pinv_id = '';
										$pinv_id        =  $savedPayment->Id;
										$st = '1';
										$action = 'Pay Invoice';
										$msg = "Payment Success";
										$qbID = $trID;
										

										$this->session->set_flashdata('success', 'Transaction Successful');
									}
									$qbID = $inID;
									$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

									$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
									if($syncid){
			                            $qbSyncID = 'CQ-'.$syncid;
			                            

										$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
										
			                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
			                        }
									$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $user_id, $pinv_id, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
								}
							}
						} else {

						
							$crtxnID = '';
							$inID = '';
							$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
						}

						if ($chh_mail == '1') {

							$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
							if (!empty($refNumber))
								$ref_number = implode(',', $refNumber);
							else
								$ref_number = '';
							$tr_date   = date('Y-m-d H:i:s');
							$toEmail = $comp_data['userEmail'];
							$company = $comp_data['companyName'];
							$customer = $comp_data['fullName'];

							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $result['transactionid']);
						}


						if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

							$card_type  = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
							$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
							$card_condition = array(
								'customerListID' => $customerID,
								'customerCardfriendlyName' => $friendlyname,
							);

							$crdata =	$this->card_model->check_friendly_name($customerID, $friendlyname);


							if (!empty($crdata)) {

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	     => $exyear,
									'companyID'    => $companyID,
									'merchantID'   => $user_id,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '',
									'updatedAt'    => date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,

									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $phone,
									'Billing_Zipcode'	 => $zipcode,
								);



								$this->card_model->update_card_data($card_condition, $card_data);
							} else {
								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '',
									'customerListID' => $customerID,
									'companyID'     => $companyID,
									'merchantID'   => $user_id,
									'customerCardfriendlyName' => $friendlyname,
									'createdAt' 	=> date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,

									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $phone,
									'Billing_Zipcode'	 => $zipcode,
								);

								$id1 =    $this->card_model->insert_card_data($card_data);
							}
						}


						$this->session->set_flashdata('success', 'Transaction Successful');
					} else {
						$crtxnID = '';
						$msg = $result['responsetext'];

						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
						$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
				}
			} else {


				$error = 'Validation Error. Please fill the requred fields';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
			}
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];

		$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_sale_new', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	private function customerSync($merchID){
		$val = array(
			'merchantID' => $merchID,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' =>$this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),
		));
		
		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

		$qbo_data = $this->general_model->get_select_data('tbl_qbo_config',array('lastUpdated'),array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery'));
 
		if(!empty($qbo_data))
		{
			$last_date   = $qbo_data['lastUpdated'];
            $timezone1 = ['time' => $last_date, 'current_format' => date_default_timezone_get(), 'new_format' => DEFAULT_TIMEZONE];
            $nft = explode(' ', getTimeBySelectedTimezone($timezone1));
            $latedata = $nft[0]."T".$nft[1]."-07:00";
			  
			$sqlQuery = "SELECT * FROM Customer where   Metadata.LastUpdatedTime > '$latedata'";
		}else{
			$sqlQuery = "SELECT * FROM Customer";
		}

		$keepRunning = true;
		$entities = [];
		$st    = 0;
		$limit = 0;
		$total = 0;
		while($keepRunning === true){
            $mres   = MAXRESULT;
			$st_pos = MAXRESULT * $st + 1;
			$s_data = $dataService->Query("$sqlQuery  STARTPOSITION $st_pos MAXRESULTS $mres ");
			if ($s_data && !empty($s_data)) {
				$entities = array_merge($entities, $s_data);
				$total += count($s_data);
				$st = $st + 1;

				$limit = ($st) * MAXRESULT;
			} else {
				$keepRunning = false;
				break;
			}
		}
		
		$error = $dataService->getLastError();
     	$err='';
		if ($error != null) {
			$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
			$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err.="The Response message is: " . $error->getResponseBody() . "\n";

			$this->session->set_flashdata('message', '<div class="alert alert-danger">' . QBO_DISCONNECT_MSG . '</div>');
        	redirect(base_url('QBO_controllers/home/index'));
		}
        
		if(!empty($qbo_data))
		{ 
			$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
			$updatedata['updatedAt'] =date('Y-m-d H:i:s');
			if(!empty($entities))
				$this->general_model->update_row_data('tbl_qbo_config',array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery'),$updatedata);
		}
        else{
			$updateda= date('Y-m-d') . 'T' . date('H:i:s');
			$upteda = array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery','lastUpdated'=>$updateda, 'createdAt'=>date('Y-m-d H:i:s'), 'updatedAt'=>date('Y-m-d H:i:s'));
			if(!empty($entities))
				$this->general_model->insert_row('tbl_qbo_config', $upteda);
		} 
        
   
		if(!empty($entities))
		{
			foreach ($entities as $oneCustomer) {
				
				$QBO_customer_details = [];
				
				$QBO_customer_details['Customer_ListID'] = $oneCustomer->Id;
				$QBO_customer_details['firstname'] = $oneCustomer->GivenName;
				$QBO_customer_details['lastname'] = $oneCustomer->FamilyName;
				$QBO_customer_details['fullname'] = $oneCustomer->DisplayName;
				$QBO_customer_details['userEmail'] = ($oneCustomer->PrimaryEmailAddr)?$oneCustomer->PrimaryEmailAddr->Address:'';
				$QBO_customer_details['phoneNumber'] = ($oneCustomer->PrimaryPhone)?$oneCustomer->PrimaryPhone->FreeFormNumber:'';
				
				$QBO_customer_details['address1'] = (isset($oneCustomer->BillAddr->Line1) && $oneCustomer->BillAddr->Line1)?$oneCustomer->BillAddr->Line1:'';
				$QBO_customer_details['address2'] = (isset($oneCustomer->BillAddr->Line2) && $oneCustomer->BillAddr->Line2)?$oneCustomer->BillAddr->Line2:'';
				$QBO_customer_details['zipCode'] = (isset($oneCustomer->BillAddr->PostalCode) && $oneCustomer->BillAddr->PostalCode)?$oneCustomer->BillAddr->PostalCode:'';
				$QBO_customer_details['Country'] = (isset($oneCustomer->BillAddr->Country) && $oneCustomer->BillAddr->Country)?$oneCustomer->BillAddr->Country:'';
				$QBO_customer_details['State'] = (isset($oneCustomer->BillAddr->CountrySubDivisionCode) && $oneCustomer->BillAddr->CountrySubDivisionCode)?$oneCustomer->BillAddr->CountrySubDivisionCode:'';
				$QBO_customer_details['City'] = (isset($oneCustomer->BillAddr->City) && $oneCustomer->BillAddr->City)?$oneCustomer->BillAddr->City:'';
				
				$QBO_customer_details['ship_address1'] = (isset($oneCustomer->ShipAddr->Line1) && $oneCustomer->ShipAddr->Line1)?$oneCustomer->ShipAddr->Line1:'';
				$QBO_customer_details['ship_address2'] = (isset($oneCustomer->ShipAddr->Line2) && $oneCustomer->ShipAddr->Line2)?$oneCustomer->ShipAddr->Line2:'';
				$QBO_customer_details['ship_zipcode'] = (isset($oneCustomer->ShipAddr->PostalCode) && $oneCustomer->ShipAddr->PostalCode)?$oneCustomer->ShipAddr->PostalCode:'';
				$QBO_customer_details['ship_country'] = (isset($oneCustomer->ShipAddr->Country) && $oneCustomer->ShipAddr->Country)?$oneCustomer->ShipAddr->Country:'';
				$QBO_customer_details['ship_state'] = (isset($oneCustomer->ShipAddr->CountrySubDivisionCode) && $oneCustomer->ShipAddr->CountrySubDivisionCode)?$oneCustomer->ShipAddr->CountrySubDivisionCode:'';
				$QBO_customer_details['ship_city'] = (isset($oneCustomer->ShipAddr->City) && $oneCustomer->ShipAddr->City)?$oneCustomer->ShipAddr->City:'';
				
				
				$QBO_customer_details['companyName'] = str_replace("'", "\'",$oneCustomer->CompanyName);
				$QBO_customer_details['companyID'] = $realmID;
				$QBO_customer_details['createdAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->CreateTime));
				$QBO_customer_details['updatedAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->LastUpdatedTime));
				$QBO_customer_details['listID'] = '1';
				$QBO_customer_details['customerStatus'] = $oneCustomer->Active;;
				$QBO_customer_details['merchantID'] = $merchID;
				if($this->general_model->get_num_rows('QBO_custom_customer',array('companyID'=>$realmID,'Customer_ListID'=>$oneCustomer->Id , 'merchantID'=>$merchID)) > 0){  
					$this->general_model->update_row_data('QBO_custom_customer',array('companyID'=>$realmID, 'Customer_ListID'=>$oneCustomer->Id, 'merchantID'=>$merchID), $QBO_customer_details);
			
				}else{
					$this->general_model->insert_row('QBO_custom_customer', $QBO_customer_details);
				}
			
			}
		}
		return true;
	}

	public function payment_transaction()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$this->qboOBJ = new QBO_data($user_id); 
		$this->qboOBJ->get_invoice_data();
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_transaction_new', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function transaction_ajax(){
		if ($this->session->userdata('logged_in')) {
			$login_info 	= $this->session->userdata('logged_in');
			$user_id 				= $login_info['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$login_info 	= $this->session->userdata('user_logged_in');
			$user_id 				= $login_info['merchantID'];
		}

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data = array();
		$no = $_POST['start'];

		$transactions   = $this->qbo_customer_model->get_transaction_history_ajax_data($user_id);
		$count = $this->qbo_customer_model->get_transaction_history_ajax_total_data($user_id);

		if(!empty($transactions)){
			foreach($transactions as $transaction){
				$row = [];
				$inv_url1 = '';
				if (!empty($transaction['invoice_id']) && !empty($transaction['invoice_no'])) {
					$invs = explode(',', $transaction['invoice_id']);
					$invoice_no = explode(',', $transaction['invoice_no']);
					foreach ($invs as $k => $inv) {
						$inv_url = base_url() . 'QBO_controllers/Create_invoice/invoice_details_page/' . trim($inv);
						if (isset($invoice_no[$k]))
							if ($plantype) {
								$inv_url1 .= $invoice_no[$k].',';
							} else {
								$inv_url1 .= ' <a href="' . $inv_url . '">' . $invoice_no[$k] . '</a>,';
							}
					}

					$inv_url1 = substr($inv_url1, 0, -1);
				} else {
					$inv_url1 .= '<a href="javascript:void(0);">---</a> ';
				}

				$gateway = ($transaction['gateway']) ? $transaction['gateway'] : $transaction['transactionType'];
				
				if ($plantype) {
					$row[] = '<div class="text-left visible-lg">'.$transaction['fullName'].'</div>';
				} else {
					$custURL = base_url('QBO_controllers/home/view_customer/' . $transaction['customerListID']);
					$row[] = '<div class="text-left visible-lg cust_view"><a href="'.$custURL.'">'.$transaction['fullName'].'</a></div>';
				}

				$row[] = '<div class="text-right cust_view">'.$inv_url1.'</div>';
				if(isset($transaction['transactionType']) && strpos(strtolower($transaction['transactionType']), 'refund') !== false){
					$row[] = '<div class="hidden-xs text-right cust_view"><a href="#pay_data_process" onclick="set_qbo_payment_transaction_data(\''.$transaction['transactionID'].'\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">($'. number_format($transaction['transactionAmount'], 2).')</a></div>';
				}else{
					$row[] = '<div class="hidden-xs text-right cust_view"><a href="#pay_data_process" onclick="set_qbo_payment_transaction_data(\''.$transaction['transactionID'].'\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$'. number_format($transaction['transactionAmount'], 2).'</a></div>';
				}

				$transactionDate = $transaction['transactionDate'];
				if(isset($login_info['merchant_default_timezone']) && !empty($login_info['merchant_default_timezone'])){
					$timezone = ['time' => $transactionDate, 'current_format' => 'UTC', 'new_format' => $login_info['merchant_default_timezone']];
					$transactionDate = getTimeBySelectedTimezone($timezone);
				} else {
					$timezone = ['time' => $transactionDate, 'current_format' => 'UTC', 'new_format' => DEFAULT_TIMEZONE];
					$transactionDate = getTimeBySelectedTimezone($timezone);
				}
				$row[] = '<div class=" text-right">'.date('M d, Y h:i A', strtotime($transactionDate)).'</div>';

				$showRefund = 0;
				if ($transaction['partial'] == $transaction['transactionAmount']) {
					$showTypeCustom = "<label class='label label-success'>Fully Refunded: " . '$' . number_format($transaction['partial'], 2) . "</label><br/>";
				} else if ($transaction['partial'] != '0') {
					$showRefund = 1;
					$showTypeCustom = "<label class='label label-warning'>Partially Refunded: " . '$' . number_format($transaction['partial'], 2) . " </label><br/>";
				} else {
					$showRefund = 1;
					$showTypeCustom = "";
				}

				
				if(strpos(strtolower($transaction['transactionType']), 'refund') !== false){
					$showRefund=0;
					$showType = "Refund";
				}else if (strpos($transaction['transactionType'], 'sale') !== false || strtoupper($transaction['transactionType']) == 'AUTH_CAPTURE') {
					$showType = "Sale";
				} else if (strpos($transaction['transactionType'], 'Offline Payment') !== false) {
					$showType = "Offline Payment";
				} else if (strpos($transaction['transactionType'], 'capture') !== false || strtoupper($transaction['transactionType']) != 'AUTH_CAPTURE') {
					$showType = "Capture";
				}
				$row[] = '<div class="hidden-xs text-right">'.$showTypeCustom.$showType.'</div>';
				$row[] = '<div class="text-right hidden-xs hidden-sm">'.$transaction['transactionID'].'</div>';

				if($showRefund == 0) { 
					$disabled_select = 'disabled'; 
				}else{
					$disabled_select = '';
				} 

				$selectOption = '<a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm '.$disabled_select.' dropdown-toggle">Select <span class="caret"></span></a>
				<ul class="dropdown-menu text-left">';

				if ($transaction['transaction_user_status'] == '1' || $transaction['transaction_user_status'] == '2') {
					if (
						in_array($transaction['transactionCode'], array('100', '200', '111', '1')) &&
						in_array(strtoupper($transaction['transactionType']), array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'OFFLINE PAYMENT', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
					) {
						if (($transaction['tr_Day'] == 0 &&  ($transaction['transactionGateway'] == '2' || $transaction['transactionGateway'] == '3')) ||  strtoupper($transaction['transactionType']) == 'OFFLINE PAYMENT') {
							$selectOption .= '<li> <a href="javascript:void(0);" class="" data-title="Pending Transaction" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>';
						} else {
							if ($transaction['partial'] == $transaction['transactionAmount']) {
								$selectOption .= '<li> 
									<a href="javascript:void(0)" id="txnRefund'.$transaction['transactionID'].'" transaction-id="'.$transaction['transactionID'].'" transaction-gatewayType="'.$transaction['transactionGateway'].' transaction-gatewayName="'. $transaction['gateway'].'" integration-type="1" class="refunAmountCustom" data-url="'. base_url("ajaxRequest/getRefundInvoice").'"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
								</li>';
							} else {
								if(strtolower($gateway) != 'heartland echeck'){
									$selectOption .= '<li> 
										<a href="javascript:void(0)" id="txnRefund'. $transaction['transactionID'] .'" transaction-id="'.$transaction['transactionID'] .'" transaction-gatewayType="'. $transaction['transactionGateway'] .'" transaction-gatewayName="'. $transaction['gateway'].'" integration-type="1" class="refunAmountCustom" data-url="'.base_url("ajaxRequest/getRefundInvoice").'"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a>
									</li>';
								}
							}
						}
					}
					if($transaction['transactionGateway'] != 5 && $transaction['transaction_user_status'] != '2'){
						$selectOption .= '<li><a href="#payment_delete" class="" onclick="set_transaction_pay('.$transaction['id'].');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Void</a></li>';
					}
				}  else {
					$selectOption .= '<li><a href="javascript:void(0);" data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>';
				} 
				$transaction_auto_id = $transaction['transactionID'];
				$selectOption .= '<li><a href="javascript:void(0);" onclick="getPrintTransactionReceiptData(\''. $transaction_auto_id.'\', 1)">Print</a></li>';
				$selectOption .= '</ul>';
				$row[] = '<div class="text-center hidden-xs"><div class="btn-group dropbtn">'.$selectOption.'</div></div>';

				$data[] = $row;
			}
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" =>  $count,
			"recordsFiltered" => $count,
			"data" => $data,
		);
		echo json_encode($output);
		die;	
	}
}
