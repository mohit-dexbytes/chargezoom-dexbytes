<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;

class Create_invoice extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('QBO_models/qbo_company_model');
        $this->load->model('QBO_models/qbo_customer_model');
        
          $this->load->model('QBO_models/qbo_invoices_model');
        
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', TRUE);
        
        if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
    	}
    	
    		public function index(){
		    
		        redirect(base_url('QBO_controllers/home'));
		        
		     }
		    
		    
		    
   	public function ajax_invoices_list()
	{
		    
    		 error_reporting(0);
    		
    		 if($this->session->userdata('logged_in')){
        		$user_id = $this->session->userdata('logged_in')['merchID'];
        		$merchID = $user_id;
	    	}
             $condition =  array('merchantID'=>$merchID);
     
    	     $val = array(
    		'merchantID' => $merchID,
    		);
    		
    		$data = $this->general_model->get_row_data('QBO_token',$val);
    		
    		$accessToken = $data['accessToken'];
    		$refreshToken = $data['refreshToken'];
        	$realmID      = $data['realmID'];
    		$condition1 =  array('qb.merchantID'=>$merchID, 'cs.merchantID'=>$merchID,'qb.companyID'=>$realmID);
    		
    		$list = $this->qbo_invoices_model->get_datatables_invoices($condition1);
    		 $plantype = $this->general_model->chk_merch_plantype_status($user_id);
            $data['plantype'] = $plantype;
    		 $data = array();
             $no = $_POST['start'];
             foreach ($list as $person) {
                $no++;
                $row = array();
                 
             //  if($person->approve_by_admin=='1'){
             
                $balance= ($person->BalanceRemaining)?number_format($person->BalanceRemaining, 2):"0.00";
                $base_url1 = base_url()."QBO_controllers/home/view_customer/".$person->CustomerListID;
                $base_url2 = base_url()."QBO_controllers/Create_invoice/invoice_details_page/".$person->invoiceID;
             //   $row[] = $person->custname;
                if($plantype){
                $row[] = "<div class='hidden-xs text-left'>$person->custname";
                } else {
                    $row[] = "<div class='hidden-xs text-left cust_view'><a href='".$base_url1."' >$person->custname </a>";
                }
    			$row[] = "<div class='hidden-xs text-right cust_view'><a href='".$base_url2."' >$person->refNumber </a>";
    			$row[] = "<div class='hidden-xs text-right'> ".date('M d, Y', strtotime($person->DueDate))." </div>";
    			
    			  if($person->Total_payment!="0.00"){
    			      
    			       $row[] ='<div class="hidden-xs text-right cust_view"> <a href="#pay_data_process" onclick="set_qbo_payment_data(\''.$person->invoiceID.'\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">'. '$'.number_format(($person->Total_payment),2).' </a> </div>' ;
    			            
				 } else{ 
						 
						$row[] ='<div class="hidden-xs text-right cust_view"> <a href="#"> '.  '$'.number_format(($person->Total_payment),2).' </a> </div>';
				}
    			$row[] ='<div class="hidden-xs text-right"> '.'$'.number_format(($person->BalanceRemaining),2).'</div>';
    		 //  $row[] =	'<div class="hidden-xs text-right" >$'.BalanceRemaining."</div>";
    		   $row[] =	"<div class='hidden-xs text-right'>$person->status</div>";
    		   
    		   	$link ='';	
            
                  if(($person->BalanceRemaining == 0 ||  $person->IsPaid=='1') && $person->UserStatus=='0' ){
             	      
						          
						          	$link.= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li><a href="#payment_refunds" onclick="set_refund_invoices(\''.$person->invoiceID.'\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>
                               
						          </ul>
						          </div> </div>';
             	        
             	      
             	        }else  if($person->UserStatus=='1' ){ 
             	                	$link.= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li><a href="javascript:void(0);"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Cancelled</a></li>
                               
						          </ul>
						          </div> </div>';
             	        }
             		   else {
                //  set_talent_contact(\''.$person->stage_name.'\',\''.$person->email.'\' );
             	            	$link.='<div class="text-center">
             	            	
                     	            	<div class="btn-group dropbtn">
                                         <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                         <ul class="dropdown-menu text-left">
                                            <li> <a href="#qbo_invoice_process" class=""  data-backdrop="static" data-keyboard="false"
                                     data-toggle="modal" onclick="set_qbo_invoice_process_id(\''.$person->invoiceID.'\',\''.$person->CustomerListID.'\',\''.$person->BalanceRemaining.'\',1);">Process</a></li>
                                       <li><a href="#qbo_invoice_schedule" onclick="set_invoice_schedule_date_id(\''.$person->invoiceID.'\',\''.$person->CustomerListID.'\',\''.$person->BalanceRemaining.'\',\''.date('M d Y',strtotime($person->DueDate)).'\');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                                            
                                        <li> <a href="#set_subs" class=""  onclick=
        							"set_sub_status_id(\''.$person->invoiceID.'\',\''.$person->BalanceRemaining.'\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>';
        							if($person->BalanceRemaining==$person->Total_payment)
        							{
        							
        							 $link.='<li> <a href="#qbo_invoice_delete" onclick="get_invoice_id(\''.$person->invoiceID.'\');" data-keyboard="false" data-toggle="modal" class="">Void</a></li>';
        							}
        							
        						  $link.='</ul>
                                     </div> </div>  ';
             	        }
             	
             	   $row[] =$link;
    
                	$data[] = $row;
              //   }
            }
    
    			$output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->qbo_invoices_model->count_all_invoices($condition1),
                            "recordsFiltered" => $this->qbo_invoices_model->count_filtered_invoices($condition1),
                            "data" => $data,
                    );
    			//output to json format
    			echo json_encode($output); die;
    		      
		      
		      
		  }
		    
		    
		
      
		    
		
	public function Invoice_details()
	{

	    if($this->session->userdata('logged_in')){
		$user_id = $this->session->userdata('logged_in')['merchID'];
		$merchID = $user_id;
		}
        $condition =  array('merchantID'=>$merchID);
     
	     $val = array(
		'merchantID' => $merchID,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
	
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
    	$realmID      = $data['realmID'];
		$condition1 =  array('qb.merchantID'=>$merchID, 'cs.merchantID'=>$merchID,'qb.companyID'=>$realmID);
		$dataService = DataService::Configure(array(
                        'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			$qbo_data = $this->general_model->get_select_data('tbl_qbo_config',array('lastUpdated'),array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'InvoiceQuery'));
            
  			  if(!empty($qbo_data))
   			 {
                $last_date   = $qbo_data['lastUpdated'];
                 //  2015-07-23T10:58:12-07:00
                   // $current_date =date('Y-m-d H:i:s');
             $my_timezone = date_default_timezone_get();
                  $from = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
                  $to   = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
                $last_date =  $this->general_model->datetimeconvqbo($last_date, $from, $to);
                $last_date1= date('Y-m-d H:i',strtotime('-9 hour',strtotime($last_date)));
              
                $latedata= date('Y-m-d',strtotime($last_date1)).'T'.date('H:i:s',strtotime($last_date1));  
            
    	 $entities = $dataService->Query("SELECT * FROM Invoice where Metadata.LastUpdatedTime > '".$latedata."' ");
    
   			 }else{
       			  $entities = $dataService->Query("SELECT * FROM Invoice ");
   			 }

	       $err='';
			$error = $dataService->getLastError();
			if ($error != null) {
			$err.=$error->getHttpStatusCode() . "\n";
			$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err.="The Response message is: " . $error->getResponseBody() . "\n";
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> '.$err.'</div>');
        	redirect('QBO_controllers/home/index','refresh');
		}
    
        if(!empty($qbo_data))  {  $updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
              $updatedata['updatedAt'] =date('Y-m-d H:i:s');
           
            $this->general_model->update_row_data('tbl_qbo_config',array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'InvoiceQuery'),$updatedata);
           }
        else{
           $updateda= date('Y-m-d') . 'T' . date('H:i:s');
           $upteda = array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'InvoiceQuery','lastUpdated'=>$updateda, 'createdAt'=>date('Y-m-d H:i:s'), 'updatedAt'=>date('Y-m-d H:i:s'));
           $this->general_model->insert_row('tbl_qbo_config', $upteda);
           } 
    
 //$entities = $dataService->Query("SELECT * FROM Invoice ");
    
	if(!empty($entities))
    {
      
      
       // print_r($entities); die;
  
		foreach ($entities as $oneInvoice) {
		        
		     $QBO_invoice_details['invoiceID'] = $oneInvoice->Id;
			 $QBO_invoice_details['refNumber'] = $oneInvoice->DocNumber;
			 
		  if($oneInvoice->ShipAddr != ""){
			 $QBO_invoice_details['ShipAddress_Addr1'] = ($oneInvoice->ShipAddr->Line1)?$oneInvoice->ShipAddr->Line1:'NULL';
			 $QBO_invoice_details['ShipAddress_Addr2'] = ($oneInvoice->ShipAddr->Line2)?$oneInvoice->ShipAddr->Line2:'NULL';
			 $QBO_invoice_details['ShipAddress_City'] = ($oneInvoice->ShipAddr->City)?$oneInvoice->ShipAddr->City:'NULL';
			  $QBO_invoice_details['ShipAddress_State'] = ($oneInvoice->ShipAddr->CountrySubDivisionCode)?$oneInvoice->ShipAddr->CountrySubDivisionCode:'NULL';
			 $QBO_invoice_details['ShipAddress_Country'] = ($oneInvoice->ShipAddr->Country)?$oneInvoice->ShipAddr->Country:'NULL';
			 $QBO_invoice_details['ShipAddress_PostalCode'] = ($oneInvoice->ShipAddr->PostalCode)?$oneInvoice->ShipAddr->PostalCode:'NULL';
			 }
		  if($oneInvoice->BillAddr != ""){
		      $QBO_invoice_details['BillAddress_Addr1'] = ($oneInvoice->BillAddr->Line1)?$oneInvoice->BillAddr->Line1:'NULL';
			 $QBO_invoice_details['BillAddress_Addr2'] = ($oneInvoice->BillAddr->Line2)?$oneInvoice->BillAddr->Line2:'NULL';
			 $QBO_invoice_details['BillAddress_City'] = ($oneInvoice->BillAddr->City)?$oneInvoice->BillAddr->City:'NULL';
			 	 $QBO_invoice_details['BillAddress_State'] = ($oneInvoice->BillAddr->CountrySubDivisionCode)?$oneInvoice->BillAddr->CountrySubDivisionCode:'NULL';
			 $QBO_invoice_details['BillAddress_Country'] = ($oneInvoice->BillAddr->Country)?$oneInvoice->BillAddr->Country:'NULL';
			 
			 $QBO_invoice_details['BillAddress_PostalCode'] = ($oneInvoice->BillAddr->PostalCode)?$oneInvoice->BillAddr->PostalCode:'NULL';
			 }
			
			 $QBO_invoice_details['DueDate'] = $oneInvoice->DueDate;
			 $QBO_invoice_details['BalanceRemaining'] = $oneInvoice->Balance;
			 $QBO_invoice_details['Total_payment'] = $oneInvoice->TotalAmt;
			 $QBO_invoice_details['CustomerListID'] = $oneInvoice->CustomerRef;
			 
			 if($oneInvoice->Balance==0)
			 $QBO_invoice_details['IsPaid'] = 1;
			 else
			  $QBO_invoice_details['IsPaid'] = 0;
          
			 $QBO_invoice_details['merchantID'] = $merchID;
             $QBO_invoice_details['companyID'] = $realmID;
             $QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s',strtotime($oneInvoice->MetaData->CreateTime));
             $QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s',strtotime($oneInvoice->MetaData->LastUpdatedTime));
             
        
             
        
			 if($this->general_model->get_num_rows('QBO_test_invoice',array('companyID'=>$realmID,'invoiceID'=>$oneInvoice->Id , 'merchantID'=>$merchID)) > 0)
             {  
           
             if($oneInvoice->TxnTaxDetail)
              {
                  
                 $taxref = $oneInvoice->TxnTaxDetail->TxnTaxCodeRef;
                  
                 if($taxref)
                 {
                     
                 $tax_data =    $this->general_model->get_row_data('tbl_taxes_qbo',array('taxID'=>$taxref, 'merchantID'=>$merchID));
                  $QBO_invoice_details['taxRate']=$tax_data['taxRate'];
                 }else{
                    
                   //  print_r($oneInvoice->TxnTaxDetail->TaxLine);
                $QBO_invoice_details['taxRate']=0;  
            }
            
              
               
               $QBO_invoice_details['totalTax'] = $oneInvoice->TxnTaxDetail->TotalTax;
               }
               else{
                     
               $QBO_invoice_details['totalTax'] = 0.00;
               }
              
             $this->general_model->update_row_data('QBO_test_invoice',array('companyID'=>$realmID, 'invoiceID'=>$oneInvoice->Id, 'merchantID'=>$merchID), $QBO_invoice_details);
             
          $line_items = $oneInvoice->Line;
               
         //    echo   count($line_items); 
             // print_r($oneInvoice->TxnTaxDetail);
             //print_r($oneInvoice->Line); 
             
            $l_data=array();
             $k=0;
             
             $l_data=array();
           if(!empty($line_items)  )
             {
              
          
               $this->general_model->delete_row_data('tbl_qbo_invoice_item',array('invoiceID'=>$oneInvoice->Id,'merchantID'=>$merchID,'releamID'=>$realmID));
               foreach($line_items as $line)
                 {
                 
                 
                 if(isset($line->Id))
                 {
               
                  
                  $l_data['itemID'] =$line->LineNum ;
                  
                  if($line->Description)
                  $l_data['itemDescription'] =$line->Description;
                  $l_data['totalAmount'] =$line->Amount ;
                  
                   $l_data['itemRefID'] =$line->SalesItemLineDetail->ItemRef;
                  if($line->SalesItemLineDetail->UnitPrice)
                  $l_data['itemPrice'] =$line->SalesItemLineDetail->UnitPrice;
                  else
                   $l_data['itemPrice'] =0.00;
                   if($line->SalesItemLineDetail->Qty)
                  $l_data['itemQty'] =$line->SalesItemLineDetail->Qty;
                  else
                   $l_data['itemQty'] =1;
               //   $l_data['itemQty'] =$line->SalesItemLineDetail->Qty;
                    $l_data['invoiceID'] =$oneInvoice->Id;
                  $l_data['merchantID'] =$merchID;
                  $l_data['releamID'] =$realmID ;
                   $l_data['createdAt'] =date('Y-m-d H:i:s') ;
                  
                
                 $this->general_model->insert_row('tbl_qbo_invoice_item',$l_data);
                 }
                 
              } 
              
            
             }
          
           }
          else
          {
               
               
               
               $line_items = $oneInvoice->Line;
               
        
             $l_data=array();
             $k=0;
             
             $l_data=array();
             //	print_r($line_items);die; 
           if(!empty($line_items)  )
             {
                 
               $this->general_model->delete_row_data('tbl_qbo_invoice_item',array('invoiceID'=>$oneInvoice->Id,'merchantID'=>$merchID,'releamID'=>$realmID));
               foreach($line_items as $line)
                 {
                      
                 
                 if(isset($line->Id))
                 {
               
                  
                  $l_data['itemID'] =$line->LineNum ;
                  if($line->Description)
                  $l_data['itemDescription'] =$line->Description;
                  $l_data['totalAmount'] =$line->Amount ;
                  
                   $l_data['itemRefID'] =$line->SalesItemLineDetail->ItemRef;
                  if($line->SalesItemLineDetail->UnitPrice)
                  $l_data['itemPrice'] =$line->SalesItemLineDetail->UnitPrice;
                  else
                   $l_data['itemPrice'] =0.00;
                   if($line->SalesItemLineDetail->Qty)
                  $l_data['itemQty'] =$line->SalesItemLineDetail->Qty;
                  else
                   $l_data['itemQty'] =1;
               //   $l_data['itemQty'] =$line->SalesItemLineDetail->Qty;
                    $l_data['invoiceID'] =$oneInvoice->Id;
                  $l_data['merchantID'] =$merchID;
                  $l_data['releamID'] =$realmID ;
                   $l_data['createdAt'] =date('Y-m-d H:i:s') ;
                  
                
                 $this->general_model->insert_row('tbl_qbo_invoice_item',$l_data);
                 }
                 
             /*    else if($line->DetailType=='SubTotalLineDetail')
                 {
               
                  
                  $l_data['itemID'] =$line->LineNum ;
                  if($line->Description)
                  $l_data['itemDescription'] =$line->Description;
                  $l_data['totalAmount'] =$line->Amount ;
                  
                   $l_data['itemRefID'] =$line->SalesItemLineDetail->ItemRef;
                  if($line->SalesItemLineDetail->UnitPrice)
                  $l_data['itemPrice'] =$line->SalesItemLineDetail->UnitPrice;
                  else
                   $l_data['itemPrice'] =0.00;
                   if($line->SalesItemLineDetail->Qty)
                  $l_data['itemQty'] =$line->SalesItemLineDetail->Qty;
                  else
                   $l_data['itemQty'] =1;
               //   $l_data['itemQty'] =$line->SalesItemLineDetail->Qty;
                    $l_data['invoiceID'] =$oneInvoice->Id;
                  $l_data['merchantID'] =$merchID;
                  $l_data['releamID'] =$realmID ;
                   $l_data['createdAt'] =date('Y-m-d H:i:s') ;
                  
                
                 $this->general_model->insert_row('tbl_qbo_invoice_item',$l_data);
                 }*/
                 
              } 
             }
            
              if($oneInvoice->TxnTaxDetail)
              {
                 $taxref = $oneInvoice->TxnTaxDetail->TxnTaxCodeRef;
                  
                 if($taxref)
                 {
                 $tax_data =    $this->general_model->get_row_data('tbl_taxes_qbo',array('taxID'=>$taxref, 'merchantID'=>$merchID));
                  $QBO_invoice_details['taxRate']=$tax_data['taxRate'];
                 }else{
                    
                   //  print_r($oneInvoice->TxnTaxDetail->TaxLine);
                $QBO_invoice_details['taxRate']=0;  
            }
            
              
               
               $QBO_invoice_details['totalTax'] = $oneInvoice->TxnTaxDetail->TotalTax;
               }
               else{
                     
               $QBO_invoice_details['totalTax'] = 0.00;
               }
              
               
               
       
            $this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
         
           }
        
      
			// $this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
		}
    }
	  

		
		 $data['primary_nav']  = primary_nav();
		 $data['template']   = template_variable();
			  
 	       $merchID  = $this->session->userdata('logged_in')['merchID'];
       
           $data['page_num']      = 'customer_qbo';    
		//	$data['invoices']             = $this->qbo_customer_model->get_invoice_list_data($condition1);
			
		//	print_r(	$data['invoices'] ); die;
			$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		//	print_r($data['gateway_datas']);die;
            $data['in_details']           = $this->qbo_customer_model->get_invoice_details($merchID);  
            
            $data['page'] = 'page_invoices';
                
             $plantype = $this->general_model->chk_merch_plantype_status($user_id);
            $data['plantype'] = $plantype;    
                
			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('QBO_views/page_invoices',$data);
		  //  $this->load->view('QBO_views/page_invoices_ajax', $data);
		
		
			$this->load->view('QBO_views/page_qbo_model', $data);
			$this->load->view('template/page_footer',$data);
			$this->load->view('template/template_end', $data);
		
		
	}

			
    
			
	public function invoice_details_page()
	{

     $oneInvoice=array();
     if($this->session->userdata('logged_in')){
		$data['login_info']	= $this->session->userdata('logged_in');
		//print_r($data['login_info']); die;
		$user_id 				= $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$data['login_info']		= $this->session->userdata('user_logged_in');
		$user_id 				= $data['login_info']['merchantID'];
		}	

		$merchID = $user_id;

	       	$condition1 =  array('qb.merchantID'=>$merchID, 'cs.merchantID'=>$merchID);
		
			$condition =  array('merchantID'=>$merchID);
     	 
        	 $inv_id =$this->uri->segment('4');
	     $val = array(
		'merchantID' => $merchID,
		);
		
		$data1 = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data1['accessToken'];
		$refreshToken = $data1['refreshToken'];
		$realmID      = $data1['realmID'];
	
		
	//	$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $oneInvoice = $this->general_model->get_row_data('QBO_test_invoice',array('invoiceID'=>$inv_id, 'merchantID'=>$merchID,'companyID'=>$realmID) );
 
   
	if(!empty($oneInvoice))
    {
      $oneInvoice['l_items'] = $this->qbo_invoices_model->get_item_data(array('invoiceID'=>$inv_id, 'merchantID'=>$merchID,'releamID'=>$realmID));
        $oneInvoice['cust_data']=$this->general_model->get_row_data('QBO_custom_customer',array('merchantID'=>$merchID,'Customer_ListID'=>$oneInvoice['CustomerListID']));
    
    //print_r($oneInvoice);die;
  
  
    }else{
            	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error Invalid Invoice </strong></div>');
        	redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
    }

		 $data['primary_nav']  = primary_nav();
		 $data['template']   = template_variable();
		 $data['page_num']      = 'invoice_details';    
         $invoiceID=$inv_id;
         $data['invoice_data']     = $oneInvoice;
		 $con = array('invoiceID'=>$oneInvoice['invoiceID'],'merchantID'=>$merchID);
		 $data['notes']   		  = $this->qbo_customer_model->get_customer_note_data($oneInvoice['cust_data']['Customer_ListID'], $merchID); 
		 $data['gateway_datas']	  = $this->general_model->get_gateway_data($merchID);
		 $data['transaction']    =  $this->general_model->get_row_data('customer_transaction',$con);
		 $conditionPlan 			= array('merchantID'=>$merchID,'IsActive'=>'true' );
		 $data['plans'] = $this->general_model->get_table_data('QBO_test_item',$conditionPlan);
         $code ='';
		 $link_data = $this->general_model->get_row_data('tbl_template_data', array('merchantID'=>$merchID,'invoiceID'=>$invoiceID));
		 $user_id =$merchID ;
		 $coditionp=array('merchantID'=>$user_id,'customerPortal'=>'1'); 
         $ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 
          $data['ur_data'] = $ur_data;
          $position1=$position=0;    $purl='';
          $ttt = explode(PLINK,$ur_data['customerPortalURL']);
             $purl = $ttt[0].PLINK.'/customer/';
        		  if(!empty($link_data))
        		 {
        		       if($link_data['emailCode']!="" )
        		        $code =$link_data['emailCode'];
        		        else
        		        $code =  $this->safe_encode($user_id);
        		        $invcode    =  $this->safe_encode($invoiceID);
        			    // $in_link  = '<a  target="_blank" href="'.base_url().'update_payment/'.$code.'/'.$invcode.'" class="btn btn-primary">Click Here</a>';
        		    	$data['paylink']  = $purl.'update_payment/'.$code.'/'.$invcode;
        		 }
       	    		else
        	    	{
        		    	
        				 $str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
                         $shuffled = str_shuffle($str);
                         $shuffled = substr($shuffled,1,12).'='.$user_id;
                         $code     =    $this->safe_encode($shuffled);
                          $invcode    =  $this->safe_encode($invoiceID);
                         	$data['paylink']  = $purl.'update_payment/'.$code.'/'.$invcode;
                         
        		}
        		
        		
        	$taxes = $this->qbo_company_model->get_qbo_tax_data($user_id);
			$data['taxes'] = $taxes;	
			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('QBO_views/page_invoice_details',$data);
			$this->load->view('QBO_views/page_qbo_model', $data);
			$this->load->view('template/page_footer',$data);
			$this->load->view('template/template_end', $data);
		
		
	}
	
	
	
/*	public function invoice_details_print()
	{
	    
 // print_r($this->session->userdata('logged_in'));
		
		if($this->session->userdata('logged_in')){
		$data['login_info'] 	= $this->session->userdata('logged_in');
		//print_r($data['login_info']); die;
		$user_id 				= $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$data['login_info'] 	= $this->session->userdata('user_logged_in');
		
		$user_id 				= $data['login_info']['merchantID'];
		}	
		
		 $invoiceID             =  $this->uri->segment(4);  
	   

		$merchID = $user_id;

	       	$condition1 =  array('qb.merchantID'=>$merchID, 'cs.merchantID'=>$merchID);
		
			$condition =  array('merchantID'=>$merchID);
     	 
    	 $inv_id =$this->uri->segment('4');
	     $val = array(
		'merchantID' => $merchID,
		);
		
		$data1 = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data1['accessToken'];
		$refreshToken = $data1['refreshToken'];
		$realmID      = $data1['realmID'];
		$data['template'] 		= template_variable();
			
		
		$oneInvoice = $this->general_model->get_row_data('QBO_test_invoice',array('invoiceID'=>$inv_id, 'merchantID'=>$merchID,'companyID'=>$realmID) );
 		$conf= $this->general_model->get_select_data('tbl_config_setting',array('ProfileImage'),array('merchantID'=>$merchID) );
 		if(!empty($conf)) $logo=base_url().LOGOURL.$conf['ProfileImage']; else $logo=CZLOGO;
        	$data['m_logo']  = $logo;
	if(!empty($oneInvoice))
    {
       $oneInvoice['invoice_items'] = $this->general_model->get_table_data('tbl_qbo_invoice_item',array('invoiceID'=>$inv_id, 'merchantID'=>$merchID,'releamID'=>$realmID));
        $oneInvoice['customer_data']=$this->general_model->get_row_data('QBO_custom_customer',array('merchantID'=>$merchID,'Customer_ListID'=>$oneInvoice['CustomerListID']));
    
    
  
  
    }else{
            	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error Invalid Invoice </strong></div>');
        	redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
    }
		
	
	$data['invoice_data']   = $oneInvoice ;
	// print_r($this->company_model->get_invoice_item_data( $invoiceID)); die;
	
	
	 //$html='';
	 //   $data['invoice_items']   = $this->company_model->get_invoice_item_data( $invoiceID);
	
	
		
	     	$no = $data['invoice_data']['refNumber'];
			$pdfFilePath = "$no.pdf"; 
			// if (!file_exists($pdfFilePath))
			 //{
		
			 ini_set('memory_limit','320M'); 
			
		 $html = $this->load->view('QBO_views/page_invoice_details_print', $data, true); 
		
			 $this->load->library('pdf');
			 $pdf = $this->pdf->load();
			// $html='<img src="<img src="https://testreseller.payportal.com/uploads/merchant_logo/1565943622chargezoom.png"/>';
		     $pdf->WriteHTML($html); // write the HTML into the PDF
	     	// $pdf->showImageErrors = true;
			  //  $pdf->WriteHTML('<img src="https://testreseller.payportal.com/uploads/merchant_logo/1565943622chargezoom.png"  >');
			// $pdf->debug = true;
			$pdf->Output($pdfFilePath, 'D'); // save to file because we can
	//	} 
		

	}*/
	
	
	
	
	
	
		public function invoice_details_print()
	{
	    
	    
	   if($this->session->userdata('logged_in')){
		$data['login_info'] 	= $this->session->userdata('logged_in');
		//print_r($data['login_info']); die;
		$user_id 				= $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$data['login_info'] 	= $this->session->userdata('user_logged_in');
		
		$user_id 				= $data['login_info']['merchantID'];
		}	
		
		  $invoiceID             =  $this->uri->segment(4);
		 
	   	 $data['template'] 		= template_variable();
		$merchID = $user_id;

		
		$condition2 			= array('invoiceID'=>$invoiceID);
		$invoice_data           = $this->general_model->get_row_data('QBO_test_invoice',$condition2);
		$condition3 			= array('Customer_ListID'=>$invoice_data['CustomerListID'] );
		$customer_data			= $this->general_model->get_row_data('QBO_custom_customer',$condition3);
	
		
		$condition4 			= array('merchID'=>$user_id );
		
		$company_data			= $this->general_model->get_select_data('tbl_merchant_data',array('firstName','lastName','companyName','merchantAddress1','merchantEmail','merchantContact','merchantAddress2','merchantCity','merchantState','merchantCountry','merchantZipCode'),$condition4);
		$config_data			= $this->general_model->get_select_data('tbl_config_setting',array('ProfileImage'),array('merchantID'=>$merchID ));
			if($config_data['ProfileImage']!="")
			$logo = base_url().LOGOURL.$config_data['ProfileImage'];
			else
				$logo = CZLOGO;
		    $data['customer_data']  = $customer_data ;
    	    $data['company_data']    = $company_data;
		
	         $invoice_items  = $this->qbo_company_model->get_invoice_item_data($invoiceID);
	   
	     	$no = $invoiceID;
			$pdfFilePath = "$no.pdf"; 
			
	 ini_set('memory_limit','320M'); 
	  $this->load->library("TPdf");
			// echo PDF_HEADER_LOGO; die; 
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
	define(PDF_HEADER_TITLE,'Invoice');
	define(PDF_HEADER_STRING,'');
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Chargezoom 1.0');
    $pdf->SetTitle($data['template']['title']);
     $pdf->SetPrintHeader(False);
  //  $pdf->SetSubject('TCPDF Tutorial');
   // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');   
  
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  
  
    $pdf->SetFont('dejavusans', '', 10, '', true);   
  // set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);
    // Add a page
  
  $pdf->AddPage();
  
   
		$y = 20;
		$logo_div='<div style="text-align:left; float:left ">
		<img src="'.$logo.'"  border="0" />
		</div>';


		// set color for background
		$pdf->SetFillColor(255, 255, 255);
		// write the first column
		$pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);
		//$pdf->SetTextColor(51, 51, 51);

		//$pdf->writeHTMLCell(80, 50, '', '', '<div style="text-align:right">Invoice</div>',0, 1, 1, true, 'J', true);
  
   
	 //$tt = "\n".$company_data['firstName']." ".$company_data['lastName']."<br/>".$company_data['companyName']."<br/>".($company_data['merchantAddress1'])."<br/> ".($company_data['merchantAddress2'])."<br/>".($company_data['merchantCity']).", ".($company_data['merchantState'])." ".($company_data['merchantZipCode']).'<br/>'.($company_data['merchantCountry']); 
	
      $tt = "\n".$company_data['firstName']." ".$company_data['lastName']."<br/>";
      
         if(!empty($company_data['companyName']))
        $tt .= $company_data['companyName']."<br/>";
       
       if(!empty($company_data['merchantAddress1']) )
      $tt .= ($company_data['merchantAddress1'])."<br/>";
      
        if(!empty($company_data['merchantAddress2']))
      $tt .= ($company_data['merchantAddress2'])."<br/>";
      
       if(!empty($company_data['merchantCity'] || $company_data['merchantState'] || $company_data['merchantZipCode'] )) 
      $tt .= ($company_data['merchantCity']).", ".($company_data['merchantState'])." ".($company_data['merchantZipCode']).'<br/>';
      
      if(!empty($company_data['merchantCountry']) )
      $tt .= ($company_data['merchantCountry']); 
	
	    $y = 50;
		// set color for background
		$pdf->SetFillColor(255, 255, 255);

		// set color for text
		$pdf->SetTextColor(51, 51, 51);

		// write the first column
		$pdf->writeHTMLCell(80, 50, '', $y, "<b>From:</b><br/>".$tt, 0, 0, 1, true, 'J', true);

		// set color for background
	//	$pdf->SetFillColor(215, 235, 255);

		// set color for text
				$pdf->SetTextColor(51, 51, 51);

		$pdf->writeHTMLCell(80, 50, '', '', '<b>Invoice: </b>'.$invoice_data['refNumber']."<br/><br/>".'<b>Due Date: </b>'.date("m/d/Y",strtotime($invoice_data['DueDate'])),0, 1, 1, true, 'J', true);
		
		$lll ='';
	
		if($customer_data['firstName']!=''){ $lll.= $customer_data['firstName'].' '.$customer_data['lastName']."<br>"; }else{
                $lll.= $invoice_data['CustomerFullName']."<br>";
                }

						$lll.=$customer_data['companyName'].'<br/>';
						
					        if($invoice_data['BillAddress_Addr1']!=''){
                              $lll.=$invoice_data['BillAddress_Addr1'].'<br>'. $invoice_data['BillAddress_Addr2']; 
							  
							   //$lll.='<br>';
					        }
					         
					           $lll.=($invoice_data['BillAddress_City'])?$invoice_data['BillAddress_City'].',':'--';
					          
					           
                               $lll.=($invoice_data['BillAddress_State'])?$invoice_data['BillAddress_State '].' ':'--'; 
                               
                               
                                  $lll.=($invoice_data['BillAddress_PostalCode'])?$invoice_data['BillAddress_PostalCode']:'--';
                               
							    $lll.='<br>';
                              
							    
							    
                                  $lll.=($customer_data['phoneNumber'])?$customer_data['phoneNumber']:'--';
							      $lll.='<br>';
							     
                              $email = ($customer_data['userEmail'])?$customer_data['userEmail']:'#';
                               
                                $lll.='<a href="'.$email.'">'.$email.'</a>';
							  
				$lll_ship='';				  
			 if($customer_data['firstName']!=''){ $lll_ship.=$customer_data['firstName'].' '.$customer_data['lastName']."<br>"; }else{
                $lll_ship.=$invoice_data['CustomerFullName']."<br>"; 
                }
                $lll_ship.=$customer_data['companyName'].'<br>';
							if($invoice_data['ShipAddress_Addr1']!=''){
                                $lll_ship.=$invoice_data['ShipAddress_Addr1'].'<br>'. $invoice_data['ShipAddress_Addr2']; 
							//	 $lll_ship.='<br>';
							}
                               $lll_ship.=($invoice_data['ShipAddress_City'])?$invoice_data['ShipAddress_City'].',':'--'.','; 
                                $lll_ship.=($invoice_data['ShipAddress_State'])?$invoice_data['ShipAddress_State'].' ':'--';
                                $lll_ship.=($invoice_data['ShipAddress_PostalCode'])?$invoice_data['ShipAddress_PostalCode']:'--';
								 $lll_ship.='<br>';
                               $lll_ship.=($customer_data['phoneNumber'])?$customer_data['phoneNumber']:'--';
							    $lll_ship.='<br>';
                           
                                $email = ($customer_data['userEmail'])?$customer_data['userEmail']:'#';
                               
                                $lll_ship.='<a href="'.$email.'">'.$email.'</a>';


$y = $pdf->getY();
//$y = 20;
		// write the first column
		$pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>".$lll, 0, 0, 1, true, 'J', true);
		$pdf->SetTextColor(51, 51, 51);
		$pdf->writeHTMLCell(80, 0, '', '', '<b>Shipping Address</b><br/>'.$lll_ship."<br/>",0, 1, 1, true, 'J', true);
$pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
		$pdf->setCellMargins(0, 1, 0, 0);
  // $pdf->writeHTML($tt1, true, false, true, false, ''); 
$html= "\n\n\n".'<h3></h3><table style="border: 2px solid black;"   cellpadding="4" >
    <tr style="border: 1px solid black;background-color:#d3d3d3;color:#FFFFFF;">
        
        <th style="border: 2px solid black;" colspan="2" align="left"><b>Product/Services</b></th>
        <th style="border: 2px solid black;" align="center"><b>Qty</b></th>
		<th style="border: 2px solid black;" align="right"><b>Unit Rate</b></th>
		<th style="border: 2px solid black;" align="right"><b>Amount</b></th>
        
    </tr>';
     $html1='';$total_val =0;
						$totaltax =0 ; $total=0; $tax=0; 
						foreach($invoice_items as $key=>$item)
						{
							
						$total+=  $item['itemQty']*$item['itemPrice']; 
				   
                    $html.='<tr style="border: 1px solid black;">
                     
						<td style="border: 2px solid black;" colspan="2" >'.$item['Name'].'</td>
                        <td style="border: 2px solid black;" align="center" >'.$item['itemQty '].'</td>
                        <td style="border: 2px solid black;" align="right" >'.number_format($item['itemPrice'],2).'</td>
						<td style="border: 2px solid black;" align="right">'.number_format($item['itemQty']*$item['itemPrice'] , 2).'</td>
                    </tr>';
						} 
						$total_val =$total+$invoice_data['totalTax'];
						$total_tax = ($invoice_data['totalTax'])?$invoice_data['totalTax']:0;
						$trate = ($invoice_data['taxRate'])?$invoice_data['taxRate']:'0.00';
			$html.= '<tr><td style="border: 2px solid black;" colspan="3">Tax</td><td style="border: 2px solid black;" align="right">Tax Rate '.$trate.'%</td><td style="border: 2px solid black;" align="right">'.number_format($total_tax,2).'</td></tr>';
			$html.= '<tr><td style="border: 1px solid black;" colspan="4">Subtotal</td><td style="border: 1px solid black;" align="right">'.number_format($total,2).'</td></tr>';	
$html.= '<tr><td style="border: 1px solid black;" colspan="4">Total</td><td style="border: 1px solid black;" align="right">'.number_format($total_val,2).'</td></tr>';	
$html.= '<tr><td style="border: 1px solid black;" colspan="4">Payment</td><td style="border: 1px solid black;" align="right">'.number_format((-$invoice_data['AppliedAmount']),2).'</td></tr>';	
$html.= '<tr><td  colspan="4">Balance</td><td  align="right">'.number_format(($invoice_data['BalanceRemaining']),2).'</td></tr>';				
		
		 $email1 = ($company_data['merchantEmail'])?$company_data['merchantEmail']:'#';
	
	
	$html.='</table>';
	$html.='</table>';


$pdf->writeHTML($html, true, false, true, false, '');
	
 
   
    // ---------------------------------------------------------    
  
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output($pdfFilePath, 'D');  
			
		
	}
	
	

	public function add_invoice()
	{


		$data['login_info'] 	= $this->session->userdata('logged_in');	
		$user_id 				= $data['login_info']['merchID'];
	    $data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
			  
		$condition1 = array('merchantID' => $user_id ); 
		$condition2 = array('merchantID' => $user_id,'IsActive'=>'true' ); 
		//$data['invoices'] = $this->general_model->get_table_data('QBO_test_invoice','');
		$data['customers'] = $this->qbo_customer_model->get_customers_data($user_id);
		$data['plans'] = $this->general_model->get_table_data('QBO_test_item',$condition2);
		
	    $conditiontax = array('tax.merchantID' => $user_id,'IsActive'=>'true');   
			
		$taxes = $this->qbo_company_model->get_qbo_tax_data($user_id);
				$data['taxes'] = $taxes;
				
				 if($this->uri->segment(4)!="")
		       {
		        $customerID = $this->uri->segment(4);
		        $data['Invcustomer']  = $this->general_model->get_row_data('QBO_custom_customer',array('Customer_ListID'=>$customerID));
		        $data['cards']        = $this->card_model->get_card_data($customerID);
		       }
		$condition				= array('merchantID'=>$user_id );		
	    $data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);			
	    
	    $country       =   $this->general_model->get_table_data('country', '');
	    $data['country_datas'] = $country;
	    
		//$data['user_id'] = $user_id;
        $data['netterms']  = $this->qbo_company_model->get_terms_data($user_id);
        
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/create_new_invoice_online',$data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);

	}
	
/*	public function invoice_create()
	{

		if(!empty($this->input->post(null, true)))
		{
          if($this->session->userdata('logged_in')!="" )
		  {
		      	$user_id = $this->session->userdata('logged_in')['merchID'];
		        $merchID = $user_id;
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		     	$user_id = $this->session->userdata('logged_in')['merchantID'];
		        $merchID = $user_id;
		  }
		$total=0;	
	
	    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod)
	    {
           // print_r($this->input->post(null, true));die;
               $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
			   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
			   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
			   $insert_row['itemFullName'] =$this->czsecurity->xssCleanPostInput('description')[$key];
			   $insert_row['itemTax'] =$this->czsecurity->xssCleanPostInput('tax_check')[$key];
			   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
			   $insert_row['itemTotal'] =$this->czsecurity->xssCleanPostInput('total')[$key];
			   $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];	
			   $item_val[$key] =$insert_row;
        }

        
       // print_r($item_val);die;
	

		$val = array(
			'merchantID' => $merchID,
		);
		
		$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
		$address1 = $this->czsecurity->xssCleanPostInput('address1');
		$address2 = $this->czsecurity->xssCleanPostInput('address2');
		$country = $this->czsecurity->xssCleanPostInput('country');
		$state = $this->czsecurity->xssCleanPostInput('state');
		$city = $this->czsecurity->xssCleanPostInput('city');
		$zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
		$phone = $this->czsecurity->xssCleanPostInput('phone');
		
	$inv_date  = date($this->czsecurity->xssCleanPostInput('invdate'));
		$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');
		if($pterm != "" && $pterm != 0){
		   $duedate =  date("Y-m-d",  strtotime($inv_date. "+ $pterm day"));
	//	 $duedate = date("Y-m-d", strtotime("$inv_date +$pterm day" ));
		}else{
		  $duedate    = date('Y-m-d', strtotime($inv_date));
		}

	//	$duedate    = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('duedate')));
        
        //card-details
		$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
		$cardID       = $this->czsecurity->xssCleanPostInput('card_list');	
		$customerID = $this->czsecurity->xssCleanPostInput('customerID');
		if($this->czsecurity->xssCleanPostInput('autopay'))
		{
				
				if($this->czsecurity->xssCleanPostInput('card_number')!="")
				{
                	 	$card =array();
						$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
				        $card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number'); 
					    $card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
						$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
						$card['cvv']         = $this->czsecurity->xssCleanPostInput('cvv');
						$card['friendlyname']= $friendlyName;
						$card['customerID']  = $customerID; 
						$card['merchantID']  = $merchID; 
						$card['Billing_Addr1']     = $this->czsecurity->xssCleanPostInput('baddress1');
						$card['Billing_Addr2']     = $this->czsecurity->xssCleanPostInput('baddress2');
					    $card['Billing_City']     = $this->czsecurity->xssCleanPostInput('bcity');
					    $card['Billing_State']     = $this->czsecurity->xssCleanPostInput('bstate');
					    $card['Billing_Contact']     = $this->czsecurity->xssCleanPostInput('bphone');
					    $card['Billing_Country']     = $this->czsecurity->xssCleanPostInput('bcountry');
					     $card['Billing_Zipcode']     = $this->czsecurity->xssCleanPostInput('bzipcode');
						 
						$card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
				 if(!empty($card_data)){
				 	  $this->card_model->update_card($card, $customerID,$friendlyName );
					  $cardID  = $card_data['CardID'];
				 }else{			     
					  $cardID = $this->card_model->insert_new_card($card);
				 }
				}	
		}
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
                        'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		 
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            $lineArray = array();
        if($this->czsecurity->xssCleanPostInput('taxes') == "")
        {
             $i = 0;
             for($i=0; $i<count($item_val);$i++){
               $LineObj = Line::create([
                   "Amount" => $item_val[$i]['itemTotal'],
                    "DetailType" => "SalesItemLineDetail",
    		        "SalesItemLineDetail" => [
    			  "ItemRef" => $item_val[$i]['itemListID'],
    			  "Qty" => $item_val[$i]['itemQuantity'],
    			  "UnitPrice" => $item_val[$i]['itemRate'],
    			  ]
    			  ]);
               $lineArray[] = $LineObj;
            }
			//Add a new Invoice
			$theResourceObj = Invoice::create([
			"CustomerRef" => $customer_ref, 
			 "ShipAddr" => [
			 "Line1"=>  $address1,
			 "City"=>  $city,
			 "Country"=>  $country,
			 "PostalCode"=>  $zipcode
		 ],
		 
		  "Line" => $lineArray,
       
		 "DueDate" =>  $duedate,
	
		 
		
		]);
		
        }
        else
        {

				//     $taxval = explode(",",$this->czsecurity->xssCleanPostInput('taxes'));
				// 	$taxref = $taxval[0];
				// 	$taxamt1 = $taxval[1];
				
			 		$taxval = $this->czsecurity->xssCleanPostInput('taxes');
			 		
			 		 $inv_amnt = '';
			         for($i=0; $i<count($item_val);$i++)
			         {
			         	$amount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'];
			         	$inv_amnt += $amount;
			         	
			         	
			           $LineObj = Line::create([
			               "Amount" => $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'],
			                "DetailType" => "SalesItemLineDetail",
					        "SalesItemLineDetail" => [
						  "ItemRef" => $item_val[$i]['itemListID'],
						  "Qty" => $item_val[$i]['itemQuantity'],
						  "UnitPrice" => $item_val[$i]['itemRate'],
						   "TaxCodeRef" => [
                                  "value" => "TAX"
                                ]
                                
						  ],
						  
						  ]);
			           $lineArray[] = $LineObj;
			        }
			     
					$theResourceObj = Invoice::create([
					"CustomerRef" => $customer_ref, 
					 "ShipAddr" => [
					 "Line1"=>  $address1,
					 "City"=>  $city,
					 "Country"=>  $country,
					 "PostalCode"=>  $zipcode
				 ],
				 
				 "TxnTaxDetail" => [
                        "TxnTaxCodeRef" => [
                            "value" => $taxval
                        ],
                    ],
					  
				 "TotalAmt" => $total,
				 
				  "Line" => $lineArray,
			        
				 "DueDate" =>  $duedate,	 
				
				]);

		}   
	
		
           $resultingObj = $dataService->Add($theResourceObj);
			$error = $dataService->getLastError();
			
			
			//print_r($error); die;
			
			if ($error != null)
			{
			      $st ='0';
    			 $error1='success'; $action = 'Add Invoice';  
			    $QBO_invoice_details['invoiceID'] = $invID;
			  $QBO_invoice_details['refNumber'] = $Number;
			    

				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>');

				redirect(base_url('QBO_controllers/Create_invoice/add_invoice'));
				
			}
			else
			{
			     $invID =  $resultingObj->Id;  
			     $st ='1';
    			 $error1='success'; $action = 'Add Invoice';  
			}
			if($invID)
			{ 
			   $schedule['invoiceID'] =  $resultingObj->Id;
			    $schedule['gatewayID'] =  $paygateway;
			     $schedule['cardID'] =  $cardID;
			      $schedule['customerID'] =  $customerID;
			      
			       if($this->czsecurity->xssCleanPostInput('autopay'))
			       {
			        $schedule['autoPay'] =  1;
			         $schedule['paymentMethod'] =   1;
			       }
			       else
			       {
			        $schedule['autoPay'] =  1; 
			         $schedule['paymentMethod']  =   2;
			       }
			         $schedule['merchantID']     =  $merchID;
			           $schedule['createdAt']    =  date('Y-m-d H:i:s');
			             $schedule['updatedAt']  =  date('Y-m-d H:i:s');
			     $this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);
		
			//	$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
						}
				$qblog_data = array('merchantID'=>$merchID,'qbAction'=>$action,'qbStatus'=>$st, 
                			         'updatedAt'=>date('Y-m-d H:i:s'), 
                			         'createdAt'=>date('Y-m-d H:i:s'), 'qbText'=>$error1, 'qbActionID'=>$invID, 
                			        );
                			        $id = $this->general_model->insert_row('tbl_qbo_log', $qblog_data); 					   
					redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
		}
	}
*/

   public function edit_custom_invoice()
   {
       	if(!empty($this->input->post(null, true)))
		{
          if($this->session->userdata('logged_in')!="" )
		  {
		      	$user_id = $this->session->userdata('logged_in')['merchID'];
		        $merchID = $user_id;
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		     	$user_id = $this->session->userdata('logged_in')['merchantID'];
		        $merchID = $user_id;
		  }
	    	$total=0;	
	
	        $invoiceID = $this->czsecurity->xssCleanPostInput('invNo');
    	    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod)
    	    {
          // print_r($this->input->post(null, true));die;
               $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
			   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
			   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
			   $insert_row['itemFullName'] =$this->czsecurity->xssCleanPostInput('description')[$key];
			 //  $insert_row['itemTax'] =$this->czsecurity->xssCleanPostInput('tax_check')[$key];
			   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
			  // $insert_row['itemTotal'] =$this->czsecurity->xssCleanPostInput('total')[$key];
			 //  $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];
			 
			 $total=$total+  ($insert_row['itemQuantity']* $insert_row['itemRate']);
			   $item_val[$key] =$insert_row;
        }

       //print_r($item_val);

		$val = array(
			'merchantID' => $merchID,
		);
	    $taxID = $this->czsecurity->xssCleanPostInput('taxes');	
	   $tx_val = 	$this->general_model->get_select_data('tbl_taxes_qbo',array('taxRate'),array(	'merchantID' => $merchID,'taxID'=>$taxID));
	   //print_r($tx_val);
	  $tx_rate =  $tx_val['taxRate'] ; 
	
	//	$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
	
		
	    $inv_date  = date('Y-m-d',strtotime($this->czsecurity->xssCleanPostInput('invDate')));


    	$duedate    = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('dueDate')));
        
      
	//	$customerID = $this->czsecurity->xssCleanPostInput('customerID');
	
		$data = $this->general_model->get_row_data('QBO_token',$val);
	
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
         'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            $lineArray = array();
                $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
               // echo "<pre>";
               //  print_r($targetInvoiceArray); echo sizeof($targetInvoiceArray) ;
                     
      
        if($this->czsecurity->xssCleanPostInput('taxes') == "")
        {
             $i = 0;
             for($i=0; $i<count($item_val);$i++){
               $LineObj = Line::create([
                   "Amount" => $item_val[$i]['itemTotal'],
                    "DetailType" => "SalesItemLineDetail",
    		        "SalesItemLineDetail" => [
    			  "ItemRef" => $item_val[$i]['itemListID'],
    			  "Qty" => $item_val[$i]['itemQuantity'],
    			  "UnitPrice" => $item_val[$i]['itemRate'],
    			  ],
    			  
    			  ]);
               $lineArray[] = $LineObj;
            }
           
                     
                        if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        				{
        					$theInvoice = current($targetInvoiceArray);
        		   
                				$updatedInvoice = Invoice::update($theInvoice, [
                				  "TotalAmt" => $total,
				 
                        	         "Line" => $lineArray,
                                
                        	         "DueDate" =>  $duedate,
                        	         
                				    "sparse" => 'true'
                				]);
        				}		
                
			//Add a new Invoice
		/*	$theResourceObj = Invoice::create([
			"CustomerRef" => $customer_ref, 
		
	         "TotalAmt" => $total,
				 
	         "Line" => $lineArray,
        
	         "DueDate" =>  $duedate,
	         
	         ]); */
		
        }
        else
        {

	
			$theInvoice = current($targetInvoiceArray);
			 		$taxval = $this->czsecurity->xssCleanPostInput('taxes');
			 	
			         for($i=0; $i<count($item_val);$i++)
			         {
			         	$amount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'];
			         
			         	$inv_amnt += $amount;
			         	
			         	
			           $LineObj = Line::create([
			               "Amount" => $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'],
			                "DetailType" => "SalesItemLineDetail",
					        "SalesItemLineDetail" => [
						  "ItemRef" => $item_val[$i]['itemListID'],
						  "Qty" => $item_val[$i]['itemQuantity'],
						  "UnitPrice" => $item_val[$i]['itemRate'],
						   "TaxCodeRef" => [
                                  "value" => "TAX"
                                ]
                                
						  ],
						  
						  ]);
			           $lineArray[] = $LineObj;
			        }
			        
			      
			 		 $inv_amnt = 0;
					$theResourceObj = Invoice::update($theInvoice,[
				
				   "TxnTaxDetail" => [
                        "TxnTaxCodeRef" => [
                            "value" => $taxID
                        ],
                    ],
					  
				 "TotalAmt" => $total,
				 
				  "Line" => $lineArray,
			        
				 "DueDate" =>  $duedate,	 
				  "sparse" => 'true',
				]);
        	 $error = $dataService->getLastError();
		    // print_r($error); die;
		}   

	
           $resultingObj = $dataService->Update($theResourceObj);
         
		
	
		
	        $error1 ='';
			if ($error != null)
			{
			    
	          
			      $error1.=  $error->getResponseBody() . "\n";
    							   
			     $st ='0';
    			 $error1= 'success'; $action = 'Add Invoice';  
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error'.$error1.'  </strong></div>');

			//	redirect(base_url('QBO_controllers/Create_invoice/add_invoice'));
				
			}
			else
			{
			   
			     $st = '1';
    			 $error1= 'success'; $action = 'Update Invoice';  
			}
		
			 $QBO_invoice_details['DueDate'] = $duedate;
			 $QBO_invoice_details['BalanceRemaining'] = $total;
			 $QBO_invoice_details['Total_payment']  = $total;
			 $QBO_invoice_details['IsPaid']         = 0;
		 
             $QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s',$this->czsecurity->xssCleanPostInput('invDate'));
             $QBO_invoice_details['TimeModified']= date('Y-m-d H:i:s');
			 $this->general_model->update_row_data('QBO_test_invoice',array('invoiceID'=>$invoiceID,'merchantID'=>$merchID), $QBO_invoice_details);
			 
			 
			 
			 
			  $this->general_model->delete_row_data('tbl_qbo_invoice_item',array('invoiceID'=>$invoiceID,'merchantID'=>$merchID,'releamID'=>$realmID));
             if(!empty($item_val))
             {
                 foreach($item_val as $key=> $line)
                 {
                 
                 
                  
                  
                  $l_data['itemID'] =$key+1 ;
               
                  $l_data['itemDescription'] =$line['itemDescription'];
                  $l_data['totalAmount'] =$total ;
                  
                   $l_data['itemRefID'] =$line['itemListID'];
                
                  $l_data['itemPrice'] =$line['itemRate'];
              
                  $l_data['itemQty'] =$line['itemQuantity'];
                 
                    $l_data['invoiceID'] =$invoiceID;
                  $l_data['merchantID'] =$merchID;
                  $l_data['releamID'] =$realmID ;
                   $l_data['createdAt'] =date('Y-m-d H:i:s') ;
                  
                
                 $this->general_model->insert_row('tbl_qbo_invoice_item',$l_data);
                 }
                 
              } 
			 
			 
          
			 
		
			$qblog_data = array('merchantID'=>$merchID,'qbAction'=>$action,'qbStatus'=>$st, 
                			         'updatedAt'=>date('Y-m-d H:i:s'), 
                			         'createdAt'=>date('Y-m-d H:i:s'), 'qbText'=>$error1, 'qbActionID'=>$invoiceID, 
                			        );
                			        $id = $this->general_model->insert_row('tbl_qbo_log', $qblog_data); 
                			        
                	if($this->czsecurity->xssCleanPostInput('index')=='self')		        
					redirect(base_url('QBO_controllers/Create_invoice/invoice_details_page/'.$invoiceID));
					else
					redirect(base_url('QBO_controllers/Create_invoice/invoice_details'));
		}
		else
		{
		     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Invalid Request</div>'); 
			
		    redirect(base_url('QBO_controllers/Create_invoice/invoice_details'));
		}
       
   }

	public function invoice_create()
	{

		if(!empty($this->input->post(null, true)))
		{
          if($this->session->userdata('logged_in')!="" )
		  {
		      	$user_id = $this->session->userdata('logged_in')['merchID'];
		        $merchID = $user_id;
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		     	$user_id = $this->session->userdata('logged_in')['merchantID'];
		        $merchID = $user_id;
		  }
		$total=0;	
	
	    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod)
	    {
          // print_r($this->input->post(null, true));die;
               $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
			   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
			   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
			   $insert_row['itemFullName'] =$this->czsecurity->xssCleanPostInput('description')[$key];
			   $insert_row['itemTax'] =$this->czsecurity->xssCleanPostInput('tax_check')[$key];
			   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
			   $insert_row['itemTotal'] =$this->czsecurity->xssCleanPostInput('total')[$key];
			   $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];	
			   $item_val[$key] =$insert_row;
        }

        
         $inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$merchID));
	    if(!empty($inv_data))
		{
			$inv_pre   = $inv_data['prefix'];
			$inv_po    = $inv_data['postfix']+1;
			$new_inv_no= $inv_pre.$inv_po;
			$Number=$new_inv_no ;
		}
		else
		{
			  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>'); 
				
			  redirect('/QBO_controllers/Create_invoice/add_invoice','refresh'); 
		}	

	

		$val = array(
			'merchantID' => $merchID,
		);
		
		$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
		$address1 = $this->czsecurity->xssCleanPostInput('address1');
		$address2 = $this->czsecurity->xssCleanPostInput('address2');
		$country = $this->czsecurity->xssCleanPostInput('country');
		$state = $this->czsecurity->xssCleanPostInput('state');
		$city = $this->czsecurity->xssCleanPostInput('city');
		$zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
		$phone = $this->czsecurity->xssCleanPostInput('phone');
		$baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
		$baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
		$bcountry = $this->czsecurity->xssCleanPostInput('bcountry');
		$bstate = $this->czsecurity->xssCleanPostInput('bstate');
		$bcity = $this->czsecurity->xssCleanPostInput('bcity');
		$bzipcode = $this->czsecurity->xssCleanPostInput('bzipcode');
		$bphone = $this->czsecurity->xssCleanPostInput('bphone');
		
	    $inv_date  = date($this->czsecurity->xssCleanPostInput('invdate'));
		$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');
		if($pterm != "" && $pterm != 0){
		   $duedate =  date("Y-m-d",  strtotime($inv_date. "+ $pterm day"));
	//	 $duedate = date("Y-m-d", strtotime("$inv_date +$pterm day" ));
		}else{
		  $duedate    = date('Y-m-d', strtotime($inv_date));
		}

	//	$duedate    = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('duedate')));
        
        //card-details
		$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
		$cardID       = $this->czsecurity->xssCleanPostInput('card_list');	
		$customerID = $this->czsecurity->xssCleanPostInput('customerID');
		if($this->czsecurity->xssCleanPostInput('autopay'))
		{
				
				if($this->czsecurity->xssCleanPostInput('card_number')!="")
				{
                	 	$card =array();
						$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
				        $card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number'); 
					    $card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
						$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
						$card['cvv']         = $this->czsecurity->xssCleanPostInput('cvv');
						$card['friendlyname']= $friendlyName;
						$card['customerID']  = $customerID; 
						$card['merchantID']  = $merchID; 
						$card['Billing_Addr1']     = $this->czsecurity->xssCleanPostInput('baddress1');
						$card['Billing_Addr2']     = $this->czsecurity->xssCleanPostInput('baddress2');
					    $card['Billing_City']     = $this->czsecurity->xssCleanPostInput('bcity');
					    $card['Billing_State']     = $this->czsecurity->xssCleanPostInput('bstate');
					    $card['Billing_Contact']     = $this->czsecurity->xssCleanPostInput('bphone');
					    $card['Billing_Country']     = $this->czsecurity->xssCleanPostInput('bcountry');
					     $card['Billing_Zipcode']     = $this->czsecurity->xssCleanPostInput('bzipcode');
						 
						$card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
				 if(!empty($card_data)){
				 	  $this->card_model->update_card($card, $customerID,$friendlyName );
					  $cardID  = $card_data['CardID'];
				 }else{			     
					  $cardID = $this->card_model->insert_new_card($card);
				 }
				}	
		}
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
         'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            $lineArray = array();
      
        if($this->czsecurity->xssCleanPostInput('taxes') == "")
        {
             $i = 0;
             for($i=0; $i<count($item_val);$i++){
               $LineObj = Line::create([
                   "Amount" => $item_val[$i]['itemTotal'],
                    "DetailType" => "SalesItemLineDetail",
    		        "SalesItemLineDetail" => [
    			  "ItemRef" => $item_val[$i]['itemListID'],
    			  "Qty" => $item_val[$i]['itemQuantity'],
    			  "UnitPrice" => $item_val[$i]['itemRate'],
    			  ],
    			  
    			  ]);
               $lineArray[] = $LineObj;
            }
            
                
			//Add a new Invoice
			$theResourceObj = Invoice::create([
			"CustomerRef" => $customer_ref, 
			"DocNumber" =>$Number,
			  "BillAddr" => [
        			 "City"=>  $bcity,
        			  "Line1"=>  $baddress1,
        			 "CountrySubDivisionCode"=>  $bstate,
        			 "Country"=>  $bcountry,
        			 "PostalCode"=>  $bzipcode
		            ],
					 "ShipAddr" => [
					 "Line1"=>  $address1,
					 "City"=>  $city,
					 "Country"=>  $country,
					 "PostalCode"=>  $zipcode
				 ],
		 
       
	 "TotalAmt" => $total,
				 
	  "Line" => $lineArray,
        
	 "DueDate" =>  $duedate,	
	
		 
		
		]);
		
        }
        else
        {

	
			
			 		$taxval = $this->czsecurity->xssCleanPostInput('taxes');
			 	
			 		 $inv_amnt = 0;
			         for($i=0; $i<count($item_val);$i++)
			         {
			         	$amount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'];
			         
			         	$inv_amnt += $amount;
			         	
			         	
			           $LineObj = Line::create([
			               "Amount" => $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'],
			                "DetailType" => "SalesItemLineDetail",
					        "SalesItemLineDetail" => [
						  "ItemRef" => $item_val[$i]['itemListID'],
						  "Qty" => $item_val[$i]['itemQuantity'],
						  "UnitPrice" => $item_val[$i]['itemRate'],
						   "TaxCodeRef" => [
                                  "value" => "TAX"
                                ]
                                
						  ],
						  
						  ]);
			           $lineArray[] = $LineObj;
			        }
			        
			        
			       
			     
					$theResourceObj = Invoice::create([
					"CustomerRef" => $customer_ref, 
				    "DocNumber"=>$Number,
				    "BillAddr" => [
        			 "City"=>  $bcity,
        			  "Line1"=>  $baddress1,
        			 "CountrySubDivisionCode"=>  $bstate,
        			 "Country"=>  $bcountry,
        			 "PostalCode"=>  $bzipcode
		            ],
					 "ShipAddr" => [
					 "Line1"=>  $address1,
					 "City"=>  $city,
					 "Country"=>  $country,
					 "PostalCode"=>  $zipcode
				 ],
				 
		
				 
				 
				 "TxnTaxDetail" => [
                        "TxnTaxCodeRef" => [
                            "value" => $taxval
                        ],
                    ],
					  
				 "TotalAmt" => $total,
				 
				  "Line" => $lineArray,
			        
				 "DueDate" =>  $duedate,	 
				
				]);

		}   

	
           $resultingObj = $dataService->Add($theResourceObj);
        
			$error = $dataService->getLastError();
	 
			$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$merchID), array('postfix'=>$inv_po) );
			
	        $error1 ='';
			if ($error != null)
			{
			    
			//  print_r($error); die;
			      $error1.=  $error->getResponseBody() . "\n";
    							   
			     $st ='0';
    			 $error1= 'success'; $action = 'Add Invoice';  
	
			 $invID = mt_rand(6000000,9000000);	  
			 $QBO_invoice_details['invoiceID'] = $invID;
			 $QBO_invoice_details['refNumber'] = $Number;
			 $QBO_invoice_details['ShipAddress_Addr1'] = $address1;
			 $QBO_invoice_details['ShipAddress_Addr2'] = $address2;
			 $QBO_invoice_details['ShipAddress_City']  = $city;
			 $QBO_invoice_details['ShipAddress_Country']    = $country;
			 $QBO_invoice_details['ShipAddress_PostalCode'] = $zipcode;
			 $QBO_invoice_details['ShipAddress_State']      = $state;
			 
			 $QBO_invoice_details['BillAddress_Addr1'] = $baddress1;
			 $QBO_invoice_details['BillAddress_Addr2'] = $baddress2;
			 $QBO_invoice_details['BillAddress_City']  = $bcity;
			 $QBO_invoice_details['BillAddress_Country']    = $bcountry;
			 $QBO_invoice_details['BillAddress_PostalCode'] = $bzipcode;
			 $QBO_invoice_details['BillAddress_State']      = $bstate;
			
			
			 $QBO_invoice_details['DueDate'] = $duedate;
			 $QBO_invoice_details['BalanceRemaining'] = $total;
			 $QBO_invoice_details['Total_payment']  = $total;
			 $QBO_invoice_details['CustomerListID'] = $customer_ref;
		
			 $QBO_invoice_details['IsPaid']         = 0;
		     // $QBO_invoice_details['UserStatus'] = 0;
          
			 $QBO_invoice_details['merchantID']  = $merchID;
             $QBO_invoice_details['companyID']   = $realmID;
             $QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s');
             $QBO_invoice_details['TimeModified']= date('Y-m-d H:i:s');
			 $this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
			 //$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>');

				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error'.$error1.'  </strong></div>');

			//	redirect(base_url('QBO_controllers/Create_invoice/add_invoice'));
				
			}
			else
			{
			    $invID =  $resultingObj->Id;  
			     $st = '1';
    			 $error1= 'success'; $action = 'Add Invoice';  
			}
			if($invID)
			{
			    
			   $schedule['invoiceID'] =$invID;  
			    $schedule['gatewayID'] =  $paygateway;
			     $schedule['cardID'] =  $cardID;
			      $schedule['customerID'] =  $customerID;
			      
			       if($this->czsecurity->xssCleanPostInput('autopay'))
			       {
			        $schedule['autoPay'] =  1;
			         $schedule['paymentMethod'] =   1;
			       }
			       else
			       {
			         $schedule['autoPay'] =  1; 
			         $schedule['paymentMethod']  =   2;
			       }
			         $schedule['merchantID']     =  $merchID;
			         $schedule['createdAt']    =  date('Y-m-d H:i:s');
			         $schedule['updatedAt']  =  date('Y-m-d H:i:s');
			     $this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);
		
			$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>'); 
				
			
			}
			
			$qblog_data = array('merchantID'=>$merchID,'qbAction'=>$action,'qbStatus'=>$st, 
                			         'updatedAt'=>date('Y-m-d H:i:s'), 
                			         'createdAt'=>date('Y-m-d H:i:s'), 'qbText'=>$error1, 'qbActionID'=>$invID, 
                			        );
                			        $id = $this->general_model->insert_row('tbl_qbo_log', $qblog_data); 					   
					redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
		}
	}


	public function delete_invoice()
	{
		if($this->session->userdata('logged_in')){
		$user_id = $this->session->userdata('logged_in')['merchID'];
		$merchID = $user_id;
		}

		$val = array(
			'merchantID' => $merchID,
		);
		
		$inv_id = $this->czsecurity->xssCleanPostInput('invID');
	
		$condition = array("invoiceID"=> $inv_id);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

		$dataService = DataService::Configure(array(
                         'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		));

		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        
		$getInvoices = $dataService->Query("SELECT * FROM Invoice WHERE id = '$inv_id'");
        //print_r($getInvoices);die;
		//$currentResultObj = $dataService->Delete($getInvoices);

		$error = $dataService->getLastError();
		if ($error != null) {
				// 		echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
				// echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
				// echo "The Response message is: " . $error->getResponseBody() . "\n";

			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>.</div>');

			redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
		}else{
	//$delete_invoice = $this->general_model->delete_row_data('QBO_test_invoice', $condition);

			$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Deleted Invoice</strong></div>'); 
				
				redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));

		}
	}

	public function get_item_data()
	{
		
		 if($this->session->userdata('logged_in')!="" )
		  {
		      	$user_id = $this->session->userdata('logged_in')['merchID'];
		      
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		     	$user_id = $this->session->userdata('logged_in')['merchantID'];
		       
		  }

		
		if($this->czsecurity->xssCleanPostInput('itemID')!=""){
			
		$itemID = $this->czsecurity->xssCleanPostInput('itemID');
		$itemdata = $this->general_model->get_row_data('QBO_test_item', array('productID'=>$itemID,'merchantID'=>$user_id));
		echo json_encode($itemdata);
		die;
	}
      return false;
	}
	



     
      function safe_encode($string) {
      return strtr(base64_encode($string), '+/=', '-_-');
   }	
	

}