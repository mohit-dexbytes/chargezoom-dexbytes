<?php

/**
 * This Controller has NMI Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 * Perform Customer Card oprtations using this controller
 * Create, Delete, Modify 
 */

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

require APPPATH .'libraries/qbo/QBO_data.php';

class CardPointePayment extends CI_Controller
{
	private $resellerID;
	private $transactionByUser;

	public function __construct()
	{
		parent::__construct();
		
		include APPPATH . 'third_party/Cardpointe.class.php';

		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->load->model('customer_model');
		$this->load->model('QBO_models/qbo_customer_model');
		$this->load->model('general_model');

		$this->load->model('card_model');
		$this->load->library('form_validation');
		$this->load->model('QBO_models/qbo_invoices_model');


		$this->db1 = $this->load->database('otherdb', TRUE);
		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '1') {
			
			$logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
		} else {
			redirect('login', 'refresh');
		}
		
	}

    /**
     * Redirect to transaction page 
    */

	public function index()
	{
		redirect('QBO_controllers/CardPointePayment/payment_transaction', 'refresh');
	}
    /**
     * pay invoice by invoice listing page
     * @return Receipt Page
    */
	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		$checkPlan = check_free_plan_transactions();
        $custom_data_fields = [];
        $cardID = $this->czsecurity->xssCleanPostInput('CardID');
        if (!$cardID || empty($cardID)) {
            $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
        }

        $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
        }

        $gateway = $gatlistval;

        $cusproID   = array();
        $error      = array();
        $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

        $resellerID = $this->resellerID;
        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        if ($checkPlan && $cardID != "" && $gateway != "") {
            $in_data = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
			$cardpointeuser   = $gt_result['gatewayUsername'];
        	$cardpointepass   = $gt_result['gatewayPassword'];
        	$cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $merchantID = $merchID;
            $cardpointeSiteName   = $gt_result['gatewaySignature'];
            $val = array(
                'merchantID' => $merchID,
            );
            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken  = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];
            $dataService  = DataService::Configure(array(
                'auth_mode'       => $this->config->item('AuthMode'),
                'ClientID'        => $this->config->item('client_id'),
                'ClientSecret'    => $this->config->item('client_secret'),
                'accessTokenKey'  => $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID'      => $realmID,
                'baseUrl'         => $this->config->item('QBOURL'),
            ));

            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

            if (!empty($in_data)) {
                $customerID = $in_data['CustomerListID'];

                $comp_data       = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchID));

                $c_data    = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchID));
                $companyID = $c_data['companyID'];

                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                    if (!empty($cardID)) {

                        $cr_amount = 0;
                        $amount    = $in_data['BalanceRemaining'];

                        $amount     = $this->czsecurity->xssCleanPostInput('inv_amount');
                        $theInvoice = current($targetInvoiceArray);
                        
                        $error = $dataService->getLastError();
                        if ($error != null) {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error in invoice payment process</strong></div>');
                            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
                        }

                        $amount = $amount - $cr_amount;

                        $crtxnID = '';
                        $inID    = array();

                        $name    = $in_data['fullName'];
                        $address = $in_data['ShipAddress_Addr1'];
                        $city    = $in_data['ShipAddress_City'];
                        $state   = $in_data['ShipAddress_State'];
                        $zipcode = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                        $achValue = false;
						$client = new CardPointe();
                        if ($sch_method == "1") {

                            if ($cardID != "new1") {

                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];
                                $phone    = $card_data['Billing_Contact'];
                                
                            } else {

                                $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                                    $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                                }

                                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                                $city     = $this->czsecurity->xssCleanPostInput('city');
                                $country  = $this->czsecurity->xssCleanPostInput('country');
                                $phone    = $this->czsecurity->xssCleanPostInput('contact');
                                $state    = $this->czsecurity->xssCleanPostInput('state');
                                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                            }
                            $cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;

                            $exyear1  = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear1;

							$result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $address1, $city, $state, $zipcode);
                           
                        } else if ($sch_method == "2") {
                            $achValue = true;
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->czsecurity->xssCleanPostInput('acc_name'),
                                    'accountNumber'      => $this->czsecurity->xssCleanPostInput('acc_number'),
                                    'routeNumber'        => $this->czsecurity->xssCleanPostInput('route_number'),
                                    'accountType'        => $this->czsecurity->xssCleanPostInput('acct_type'),
                                    'accountHolderType'  => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
                                    'Billing_Addr1'      => $this->czsecurity->xssCleanPostInput('address1'),
                                    'Billing_Addr2'      => $this->czsecurity->xssCleanPostInput('address2'),
                                    'Billing_City'       => $this->czsecurity->xssCleanPostInput('city'),
                                    'Billing_Country'    => $this->czsecurity->xssCleanPostInput('country'),
                                    'Billing_Contact'    => $this->czsecurity->xssCleanPostInput('contact'),
                                    'Billing_State'      => $this->czsecurity->xssCleanPostInput('state'),
                                    'Billing_Zipcode'    => $this->czsecurity->xssCleanPostInput('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->czsecurity->xssCleanPostInput('secCode'),
                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }
                            $accountNumber = $accountDetails['accountNumber'];
                            $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;

                            $cardType = 'Check';

							$result = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountDetails['accountNumber'], $accountDetails['routeNumber'], $amount, $accountDetails['accountType'], $name, $accountDetails['Billing_Addr1'], $accountDetails['Billing_City'],$accountDetails['Billing_State'],$accountDetails['Billing_Zipcode']);
                        }
						$responseId = isset($result['retref'])?$result['retref']:'TXNFailed'.time();
                        if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved'){
                            $responseId = $result['retref'];

							$createPaymentObject = [
                                "TotalAmt"    => $amount,
                                "SyncToken"   => 1, 
                                "CustomerRef" => $customerID,
                                "Line"        => [
                                    "LinkedTxn" => [
                                        "TxnId"   => $invoiceID,
                                        "TxnType" => "Invoice",
                                    ],
                                    "Amount"    => $amount,
                                ],
                            ];

							$paymentMethod = $this->general_model->qbo_payment_method($cardType, $merchID, $achValue);
                            if($paymentMethod){
                                $createPaymentObject['PaymentMethodRef'] = [
                                    'value' => $paymentMethod['payment_id']
                                ];
                            }
                            if(isset($responseId) && $responseId != ''){
                                $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                            }

                            $newPaymentObj = Payment::create($createPaymentObject);
                            $savedPayment = $dataService->Add($newPaymentObj);

                            $error = $dataService->getLastError();
                            if ($error != null) {
                                $er = '';
                                $er .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                $er .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                $er .= "The Response message is: " . $error->getResponseBody() . "\n";
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error' . $er . ' </strong>.</div>');

                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success but " . $error->getResponseBody();
                                $qbID   = $responseId;
                            } else {
                                $crtxnID        = $savedPayment->Id;
                                $condition_mail = array('templateType' => '5', 'merchantID' => $merchID);
                                $ref_number     = $in_data['refNumber'];
                                $tr_date        = date('Y-m-d H:i:s');
                                $toEmail        = $c_data['userEmail'];
                                $company        = $c_data['companyName'];
                                $customer       = $c_data['fullName'];

                               
                                $st     = '1';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success";
                                $qbID   = $responseId;

                                $this->session->set_flashdata('success', 'Success </strong></div>');
                                
                            }

                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->czsecurity->xssCleanPostInput('address1'),
                                        'Billing_Addr2'   => $this->czsecurity->xssCleanPostInput('address2'),
                                        'Billing_City'    => $this->czsecurity->xssCleanPostInput('city'),
                                        'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
                                        'Billing_Contact' => $this->czsecurity->xssCleanPostInput('contact'),
                                        'Billing_State'   => $this->czsecurity->xssCleanPostInput('state'),
                                        'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }
                        } else {
                            $err_msg      = $result['resptext'];

                            $st     = '0';
                            $action = 'Pay Invoice';
                            $msg    = "Payment Failed";
                            $qbID   = $invoiceID;
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                        }
                        $qbID   = $invoiceID;
                        
                        $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' =>$responseId,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                        if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;

                            $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                            
                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }


                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $invoiceID, $achValue, $this->transactionByUser, $custom_data_fields);
						
						if ($chh_mail == '1' && ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved')) {
							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
						}
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if ($cusproID == "2") {
            redirect('QBO_controllers/home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3") {
          
            redirect('QBO_controllers/Create_invoice/invoice_details_page/' . $in_data['invoiceID'], 'refresh');
        }

        $invoice_IDs  = array();
        $receipt_data = array(
            'proccess_url'      => 'QBO_controllers/Create_invoice/Invoice_details',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }

        redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
	}

    /**
     * Fetch customer card data by customer ID
     * @return array
    */
	public function get_vault_data($customerID): array
	{
		$card = array();

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}


		$sql = "SELECT * from customer_card_data  where  customerListID='$customerID'  and  merchantID ='$merchID' order by CardID desc limit 1  ";
		$query1 = $this->db1->query($sql);
		$card_data = $query1->row_array();
		if (!empty($card_data)) {

			$card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']);
			$card['cardMonth']  = $card_data['cardMonth'];
			$card['cardYear']  = $card_data['cardYear'];
			$card['CardID']    = $card_data['CardID'];
			$card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
			$card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
		}

		return  $card;
	}

    /**
     * Delete customer card data
     * @return Redirect to customer details page
    */

	public function delete_card_data()
	{
		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}
		if (!empty($this->czsecurity->xssCleanPostInput('delCardID'))) {


			$cardID = $this->czsecurity->xssCleanPostInput('delCardID');
			$customer =  $this->czsecurity->xssCleanPostInput('delCustodID');


			$num  = $this->general_model->get_num_rows('tbl_subscriptions_qbo', array('CardID' => $cardID, 'merchantDataID' => $merchID));
			if ($num == 0) {
				$sts =  $this->card_model->delete_card_data(array('CardID' => $cardID));
				if ($sts) {
					
					$this->session->set_flashdata('success', 'Successfully Deleted');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Transaction Failed -  This card cannot be deleted. Please change Customer card on Subscription</strong></div>');
			}
			redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
		}
	}

    /**
     * Update card data and redirect 
     */
	public function update_card_data()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');

		
		$card_data = $this->card_model->get_single_card_data($cardID);
		$con_cust = array('Customer_ListID' => $card_data['customerListID'], 'merchantID' => $merchID);
		$c_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
		$companyID  = $c_data['companyID'];
		$customer = $card_data['customerListID'];

		if ($this->czsecurity->xssCleanPostInput('edit_expiry') != '') {

			$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
			$decryptedcvv  = $this->czsecurity->xssCleanPostInput('edit_cvv');
			$cvv      = '';
			$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
		}

		if ($this->czsecurity->xssCleanPostInput('edit_acc_number') !== '') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('edit_acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('edit_route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('edit_acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('edit_secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('edit_acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
			$expmonth = $exyear   = 	$cvv = 0;
			$card_no  = '';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
		}

		$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');

		$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
		$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
		
		$b_addr1 = $this->czsecurity->xssCleanPostInput('baddress1');
		$b_addr2 = $this->czsecurity->xssCleanPostInput('baddress2');
		$b_city = $this->czsecurity->xssCleanPostInput('bcity');
		$b_state = $this->czsecurity->xssCleanPostInput('bstate');
		$b_country = $this->czsecurity->xssCleanPostInput('bcountry');
		$b_contact = $this->czsecurity->xssCleanPostInput('bcontact');
		$b_zip = $this->czsecurity->xssCleanPostInput('bzipcode');
		$merchantID = $merchID;

		$condition = array('CardID' => $cardID);
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,

			'CardCVV'      => $cvv,

            'customerCardfriendlyName' => $friendlyname,
			
			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zip,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'updatedAt' 	=> date("Y-m-d H:i:s")
		);



		if ($this->czsecurity->xssCleanPostInput('edit_card_number') != '') {
			$card_no  = $this->czsecurity->xssCleanPostInput('edit_card_number');
			$decryptedCard = $card_no;
			$insert_array['CustomerCard'] = $this->card_model->encrypt($card_no);
			$card_type = $this->general_model->getType($card_no);

			$friendlyname = $card_type . ' - ' . substr($card_no, -4);
			$insert_array['customerCardfriendlyName'] = $friendlyname;
			$insert_array['CardType'] = $card_type;
		} else {
			$decryptedCard     = $card_data['CardNo'];
		}

		$isAuthorised = true;
		if ($this->czsecurity->xssCleanPostInput('edit_expiry') != '') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['fullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}
		
		if($isAuthorised){
			$insert_array['CardCVV'] = '';
			$id = $this->card_model->update_card_data($condition,  $insert_array);
			if ($id) {
				$this->session->set_flashdata('success', 'Successfully Updated Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
		}

		redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
	}

    /**
     * Insert card data and redirect 
     */
	public function insert_new_data()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$customer = $this->czsecurity->xssCleanPostInput('customerID');
		$con_cust = array('Customer_ListID' => $customer, 'merchantID' => $merchID);
		$c_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
		$companyID  = $c_data['companyID'];

		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
			$decryptedCard = $card_no;
			$card_type = $this->general_model->getType($card_no);
			$card_no  = $this->card_model->encrypt($card_no);
			$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
			$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
			$decryptedcvv = $cvv;
			$cvv      = '';
			$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
			$acc_number   = $route_number = $acc_name = $secCode = $acct_type = $acct_holder_type = '';
		}
		if ($this->czsecurity->xssCleanPostInput('formselector') == '2') {
			$acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
			$route_number = $this->czsecurity->xssCleanPostInput('route_number');
			$acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
			$secCode      = $this->czsecurity->xssCleanPostInput('secCode');
			$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
			$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
			$expmonth = $exyear   = 	$cvv = 0;
			$card_no  = '';
			$card_type = 'Echeck';
            $friendlyname     = 'Checking - ' . substr($acc_number, -4);
		}

		$b_addr1 = $this->czsecurity->xssCleanPostInput('address1');
		$b_addr2 = $this->czsecurity->xssCleanPostInput('address2');
		$b_city = $this->czsecurity->xssCleanPostInput('city');
		$b_state = $this->czsecurity->xssCleanPostInput('state');
		$b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
		$b_country = $this->czsecurity->xssCleanPostInput('country');
		$b_contact = $this->czsecurity->xssCleanPostInput('contact');
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $card_no,
			'CardCVV'      => $cvv,
			'CardType'      => $card_type,
			'customerListID' => $customer,
			'merchantID'     => $merchID,
			'companyID'      => $companyID,
			'Billing_Addr1'     => $b_addr1,
			'Billing_Addr2'     => $b_addr2,
			'Billing_City'      => $b_city,
			'Billing_State'     => $b_state,
			'Billing_Zipcode'   => $b_zipcode,
			'Billing_Country'   => $b_country,
			'Billing_Contact'   => $b_contact,
			'customerCardfriendlyName' => $friendlyname,
			'accountNumber'   => $acc_number,
			'routeNumber'     => $route_number,
			'accountName'   => $acc_name,
			'accountType'   => $acct_type,
			'accountHolderType'   => $acct_holder_type,
			'secCodeEntryMethod'   => $secCode,
			'createdAt' 	=> date("Y-m-d H:i:s")
		);

		$isAuthorised = true;
		if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
			require APPPATH .'libraries/Manage_payments.php';
			$params = $insert_array;
			$params['CustomerCard'] = $decryptedCard;
			$params['CardCVV'] = $decryptedcvv;
			$params['name'] = $c_data['fullName'];
			$params['amount'] = DEFAULT_AUTH_AMOUNT;
			$params['authType'] = 1;/* 1 for used auto authrize amount 2 for given manually amount */
			$paymentObject = new Manage_payments($merchID);
			$isAuthorised = $paymentObject->authoriseTransaction($params);
		}

		if($isAuthorised){
			$insert_array['CardCVV'] = '';
			$id = $this->card_model->insert_card_data($insert_array);
			if ($id) {
				
				$this->session->set_flashdata('success', 'Successfully Inserted Card');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}
		}

		redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
	}

    /**
     * Customer sale page transaction and return to reciept page
     * @return Receipt page
    */
	public function create_customer_sale()
	{

		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post())) {
		
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
			$this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {

				$this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
				$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
			}

			$checkPlan = check_free_plan_transactions();

			if ($checkPlan && $this->form_validation->run() == true) {


				$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				if (!empty($gt_result)) {
					$cardpointeuser   = $gt_result['gatewayUsername'];
					$cardpointepass   = $gt_result['gatewayPassword'];
					$cardpointeMerchID = $gt_result['gatewayMerchantID'];
                    $cardpointeSiteName  = $gt_result['gatewaySignature'];
					$invoiceIDs = array();
					$custom_data_fields = [];
                    $applySurcharge = false;
                    $achValue = false;
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $applySurcharge = true;
						$custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
						$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}

					if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
						$custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
					}

					$customerID = $this->czsecurity->xssCleanPostInput('customerID');

					$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
					$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
					$companyID  = $comp_data['companyID'];


					$cardID = $this->czsecurity->xssCleanPostInput('card_list');

					$cvv = '';
					if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear  = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $exyear1 = substr($exyear, 2);
                        $expry   = $expmonth . $exyear1;
                        $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;
                    } else {
                        $card_data = $this->card_model->get_single_card_data($cardID);

                        $card_no  = $card_data['CardNo'];
                        $expmonth = $card_data['cardMonth'];
                        $exyear   = $card_data['cardYear'];
                        $exyear1  = substr($exyear, 2);
                        if (strlen($expmonth) == 1) {
                            $expmonth = '0' . $expmonth;
                        }
                        $expry = $expmonth . $exyear1;
                        $cvv   = $card_data['CardCVV'];
                    }
                   

					$name     = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 = $this->czsecurity->xssCleanPostInput('address2');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$phone   =  $this->czsecurity->xssCleanPostInput('phone');
					$state   =  $this->czsecurity->xssCleanPostInput('state');
					$zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
				
					$client = new CardPointe(); 
					
					/*Added card type in transaction table*/
                    $cardType = $this->general_model->getType($card_no);
                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                    $custom_data_fields['payment_type'] = $friendlyname;

					$cvv = trim($cvv);
					
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
					}

					$amount = $this->czsecurity->xssCleanPostInput('totalamount');
                    // update amount with surcharge 
                    if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                        $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                        $amount += round($surchargeAmount, 2);
                        $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                        $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                        $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                        
                    }
                    $totalamount  = $amount;
                   
					$result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $address1, $city, $state, $zipcode);
					
					if ($result['respcode'] == '00') {

						$trID = $result['retref'];
						$invoiceIDs = array();
						$invoicePayAmounts = array();
						if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
							$invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
							$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
						}
						$val = array('merchantID' => $user_id);
						$data = $this->general_model->get_row_data('QBO_token', $val);

						$accessToken = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];

						$dataService = DataService::Configure(array(
							'auth_mode' => $this->config->item('AuthMode'),
							'ClientID'  => $this->config->item('client_id'),
							'ClientSecret' => $this->config->item('client_secret'),
							'accessTokenKey' =>  $accessToken,
							'refreshTokenKey' => $refreshToken,
							'QBORealmID' => $realmID,
							'baseUrl' => $this->config->item('QBOURL'),
						));

						$refNumber = array();

						if (!empty($invoiceIDs)) {
							$payIndex = 0;
							$saleAmountRemaining = $amount;
							foreach ($invoiceIDs as $inID) {
								$theInvoice = array();
								$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

								$con = array('invoiceID' => $inID, 'merchantID' => $user_id);
								$res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);
								
								$refNumber[]   =  $res1['refNumber'];
								$txnID      = $inID;
								$pay_amounts = 0;
								$amount_data = $res1['BalanceRemaining'];
								$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                    $actualInvoicePayAmount += $surchargeAmount;
                                    $amount_data += $surchargeAmount;
                                    $updatedInvoiceData = [
                                        'inID' => $inID,
                                        'targetInvoiceArray' => $targetInvoiceArray,
                                        'merchantID' => $user_id,
                                        'dataService' => $dataService,
                                        'realmID' => $realmID,
                                        'accessToken' => $accessToken,
                                        'refreshToken' => $refreshToken,
                                        'amount' => $surchargeAmount,
                                    ];
                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                                }
								$ispaid 	 = 0;
								$isRun = 0;
								if($saleAmountRemaining > 0){
									$BalanceRemaining = 0.00;
	                                if($amount_data == $actualInvoicePayAmount){
										$actualInvoicePayAmount = $amount_data;
										$isPaid 	 = 1;

									}else{

										$actualInvoicePayAmount = $actualInvoicePayAmount;
										$isPaid 	 = 0;
										$BalanceRemaining = $amount_data - $actualInvoicePayAmount;
										
									}
	                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

	                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
									
									$this->general_model->update_row_data('QBO_test_invoice', $con, $data);

									if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
										$theInvoice = current($targetInvoiceArray);

										$createPaymentObject = [
											"TotalAmt" => $actualInvoicePayAmount,
											"SyncToken" => 1,
											"CustomerRef" => $customerID,
											"Line" => [
												"LinkedTxn" => [
													"TxnId" => $inID,
													"TxnType" => "Invoice",
												],
												"Amount" => $actualInvoicePayAmount
											]
										];
			
										$paymentMethod = $this->general_model->qbo_payment_method($cardType, $user_id, $achValue);
										if($paymentMethod){
											$createPaymentObject['PaymentMethodRef'] = [
												'value' => $paymentMethod['payment_id']
											];
										}
										if(isset($responseId) && $responseId != ''){
											$createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
										}

										$newPaymentObj = Payment::create($createPaymentObject);

										$savedPayment = $dataService->Add($newPaymentObj);

										$error = $dataService->getLastError();
										if ($error != null) {
											$err = '';
											$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
											$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
											$err .= "The Response message is: " . $error->getResponseBody() . "\n";
											$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

											$st = '0';
											$action = 'Pay Invoice';
											$msg = "Payment Success but " . $error->getResponseBody();

											$qbID = $trID;
											$pinv_id = '';
										} else {
											$pinv_id = '';
											$pinv_id        =  $savedPayment->Id;
											$st = '1';
											$action = 'Pay Invoice';
											$msg = "Payment Success";
											$qbID = $trID;

										
										}
										$qbID = $inID;
										$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

										$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
										if($syncid){
				                            $qbSyncID = 'CQ-'.$syncid;

											$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

				                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
				                        }
										$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
									}
								}

								$payIndex++;	

							}

							
						} else {

							$crtxnID = '';
							$inID = '';
							$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
						}
						$condition_mail         = array('templateType' => '5', 'merchantID' => $user_id);
						if (!empty($refNumber))
							$ref_number = implode(',', $refNumber);
						else
							$ref_number = '';
						$tr_date   = date('Y-m-d H:i:s');
						$toEmail = $comp_data['userEmail'];
						$company = $comp_data['companyName'];
						$customer = $comp_data['fullName'];
						if ($chh_mail == '1') {
							$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $trID);
						}

						
						if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

							
							$card_type  = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
							$friendlyname = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);
							$card_condition = array(
								'customerListID' => $customerID,
								'customerCardfriendlyName' => $friendlyname,
							);

							$crdata =	$this->card_model->check_friendly_name($customerID, $friendlyname);


							if (!empty($crdata)) {

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	     => $exyear,
									'companyID'    => $companyID,
									'merchantID'   => $user_id,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '', 
									'updatedAt'    => date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,

									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $phone,
									'Billing_Zipcode'	 => $zipcode,
								);



								$this->card_model->update_card_data($card_condition, $card_data);
							} else {
								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'     => $this->general_model->getType($card_no),
									'CustomerCard' => $this->card_model->encrypt($card_no),
									'CardCVV'      => '',
									'customerListID' => $customerID,
									'companyID'     => $companyID,
									'merchantID'   => $user_id,
									'customerCardfriendlyName' => $friendlyname,
									'createdAt' 	=> date("Y-m-d H:i:s"),
									'Billing_Addr1'	 => $address1,

									'Billing_City'	 => $city,
									'Billing_State'	 => $state,
									'Billing_Country'	 => $country,
									'Billing_Contact'	 => $phone,
									'Billing_Zipcode'	 => $zipcode,
								);

								$id1 =    $this->card_model->insert_card_data($card_data);
							}
						}
						
						$this->session->set_flashdata('success', 'Transaction Successful');
					} else {
						$crtxnID = '';

						$msg = $result['resptext'];
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
						$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
				}
			} else {


				$error = 'Validation Error. Please fill the requred fields';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
			}
			$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => (isset($result) && isset($result['retref'])) ? $result['retref'] : '',
						'IP_address' => $_SERVER['REMOTE_ADDR'],
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		
		}

		$data['merchID'] 	= $user_id;

		$this->customerSync($user_id);

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if (!merchant_gateway_allowed($merchant_condition)) {
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$condition				= array('merchantID' => $user_id);
		$gateway        		= $this->general_model->get_gateway_data($user_id);
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		$data['stp_user']      = $gateway['stripeUser'];

		$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$data['plantype'] = $plantype;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_sale', $data);
		$this->load->view('QBO_views/page_popup_modals', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

    /**
     * Authorize Transaction and return to reciept page
     * @return Receipt page
    */
    public function create_customer_auth()
    {
    	$this->session->unset_userdata("receipt_data");
    	$this->session->unset_userdata("invoice_IDs");
    	if (!empty($this->input->post())) {
		
    		$inputData = $this->input->post();
    		$gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
    		$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
    		$checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
    		if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
    			$merchantID = $this->session->userdata('logged_in')['merchID'];
    			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
    			$cardpointeuser   = $gt_result['gatewayUsername'];
    			$cardpointepass   = $gt_result['gatewayPassword'];
    			$cardpointeMerchID = $gt_result['gatewayMerchantID'];
                $cardpointeSiteName = $gt_result['gatewaySignature'];

    			$comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
    			$companyID = $comp_data['companyID'];
    			$cardID    = $this->czsecurity->xssCleanPostInput('card_list');

    			if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {

                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear  = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear1 = substr($exyear, 2);
                    $expry   = $expmonth . $exyear1;
                    $cvv     = $this->czsecurity->xssCleanPostInput('cvv');
                    $cardType = $this->general_model->getType($card_no);
                    $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                    $custom_data_fields['payment_type'] = $friendlyname;
                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $expmonth = $card_data['cardMonth'];
                    $card_no  = $card_data['CardNo'];

                    $exyear  = $card_data['cardYear'];
                    $exyear1 = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth . $exyear1;
                    $cvv   = $card_data['CardCVV'];
                    $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                }
                

    			$name      = $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName');
    			$address   = $this->czsecurity->xssCleanPostInput('address');
    			$baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
    			$baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
    			$country   = $this->czsecurity->xssCleanPostInput('bcountry');
    			$city      = $this->czsecurity->xssCleanPostInput('bcity');
    			$state     = $this->czsecurity->xssCleanPostInput('bstate');
    			$amount    = $this->czsecurity->xssCleanPostInput('totalamount');
    			$zipcode   = ($this->czsecurity->xssCleanPostInput('zipcode')) ? $this->czsecurity->xssCleanPostInput('zipcode') : '55555';


    			$client = new CardPointe();

    			$result = $client->authorize($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $baddress1, $city, $state, $zipcode);

    			$responseType = 'AuthResponse';
    			$res = $result;
    			

    			$crtxnID = '';
    			$invID = $inID = $responseId  = '';


    			if ($result['respcode'] == '00') {

    				if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
    					$card_no   = $this->czsecurity->xssCleanPostInput('card_number');
    					$card_type = $this->general_model->getType($card_no);
    					$expmonth  = $this->czsecurity->xssCleanPostInput('expiry');

    					$exyear = $this->czsecurity->xssCleanPostInput('expiry_year');

    					$cvv       = $this->czsecurity->xssCleanPostInput('cvv');
    					$card_data = array(
    						'cardMonth'       => $expmonth,
    						'cardYear'        => $exyear,
    						'CardType'        => $card_type,
    						'CustomerCard'    => $card_no,
    						'CardCVV'         => $cvv,
    						'customerListID'  => $customerID,
    						'companyID'       => $companyID,
    						'merchantID'      => $merchantID,

    						'createdAt'       => date("Y-m-d H:i:s"),
    						'Billing_Addr1'   => $inputData['baddress1'],
    						'Billing_Addr2'   => $inputData['baddress2'],
    						'Billing_City'    => $inputData['bcity'],
    						'Billing_State'   => $inputData['bstate'],
    						'Billing_Country' => $inputData['bcountry'],
    						'Billing_Contact' => $inputData['phone'],
    						'Billing_Zipcode' => $inputData['zipcode'],
    					);

    					$this->card_model->process_card($card_data);
                        $custom_data_fields['card_type'] = $cardType;
    				}
    				$this->session->set_flashdata('success', 'Successfully Authorized Credit Card Payment');

    			} else {
    				$err_msg      = $result['resptext'];
    				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed:</strong>'. ' ' . $err_msg . '</div>');
    			}

    			$id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
    		} else {
    			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
    		}
	   }
    	$invoice_IDs = array();

    	$receipt_data = array(
    		'transaction_id'    => $result['retref'],
    		'IP_address'        => $_SERVER['REMOTE_ADDR'],
    		'billing_name'      => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
    		'billing_address1'  => $this->czsecurity->xssCleanPostInput('baddress1'),
    		'billing_address2'  => $this->czsecurity->xssCleanPostInput('baddress2'),
    		'billing_city'      => $this->czsecurity->xssCleanPostInput('bcity'),
    		'billing_zip'       => $this->czsecurity->xssCleanPostInput('bzipcode'),
    		'billing_state'     => $this->czsecurity->xssCleanPostInput('bstate'),
    		'billing_country'   => $this->czsecurity->xssCleanPostInput('bcountry'),
    		'shipping_name'     => $this->czsecurity->xssCleanPostInput('firstName') . ' ' . $this->czsecurity->xssCleanPostInput('lastName'),
    		'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
    		'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
    		'shipping_city'     => $this->czsecurity->xssCleanPostInput('city'),
    		'shipping_zip'      => $this->czsecurity->xssCleanPostInput('zipcode'),
    		'shipping_state'    => $this->czsecurity->xssCleanPostInput('state'),
    		'shiping_counry'    => $this->czsecurity->xssCleanPostInput('country'),
    		'Phone'             => '', 
    		'Contact'           => $this->czsecurity->xssCleanPostInput('email'),
    		'proccess_url'      => 'QBO_controllers/Payments/create_customer_auth',
    		'proccess_btn_text' => 'Process New Transaction',
    		'sub_header'        => 'Authorize',
    		'checkPlan'         => $checkPlan
    	);

    	$this->session->set_userdata("receipt_data", $receipt_data);
    	$this->session->set_userdata("invoice_IDs", $invoice_IDs);

    	redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

	/**
     * Capture Transaction Amount and return to transaction page
     * @return Transaction page
    */
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if (!empty($this->input->post())) {
			if ($this->session->userdata('logged_in')) {
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
			$tID     = $this->czsecurity->xssCleanPostInput('txnID');
			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			if ($paydata['gatewayID'] > 0) {

				$gatlistval = $paydata['gatewayID'];

				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;
				$cardpointeuser  = $gt_result['gatewayUsername'];
				$cardpointepass  =  $gt_result['gatewayPassword'];
				$cardpointeMerchID = $gt_result['gatewayMerchantID'];
                $cardpointeSiteName  = $gt_result['gatewaySignature'];
				$gatewayName = getGatewayName($gt_result['gatewayType']);
				$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "CardPointe";

				$client = new CardPointe();

				$customerID = $paydata['customerListID'];
				$amount  =  $paydata['transactionAmount'];
				$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
				$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

				if ($paydata['transactionType'] == 'auth') {

					$result = $client->capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $tID, $amount);
                    if ($result['respcode'] == '00') {
                        $res_code = 100;
                        $condition = array('transactionID' => $tID);
                        $update_data =   array('transaction_user_status' => "4");

                        $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                        $condition = array('transactionID' => $tID);
                        $customerID = $paydata['customerListID'];


                        $tr_date   = date('Y-m-d H:i:s');
                        $ref_number =  $tID;
                        $toEmail = $comp_data['userEmail'];
                        $company = $comp_data['companyName'];
                        $customer = $comp_data['fullName'];
                        if ($chh_mail == '1') {
                            
                            $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');
                        }
                     
                        $this->session->set_flashdata('success', 'Successfully Captured Authorization');
                    } else {
                        $res_code = 300;
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['resptext'] . '</div>');
                    }
				} else {
                    $res_code = 300;
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['resptext'] . '</div>');
                }
				
				$transactiondata = array();
				$transactiondata['transactionID']      = $result['retref'];
				$transactiondata['transactionStatus']  = $result['resptext'];
				$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
				$transactiondata['transactionModified'] = date('Y-m-d H:i:s');
				$transactiondata['transactionType']    = 'capture';
				$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
				$transactiondata['gatewayID']        = $gatlistval;
				$transactiondata['transactionCode']   = $res_code;
				$transactiondata['customerListID']     = $customerID;
				$transactiondata['transactionAmount']  = $amount;
				$transactiondata['merchantID']         =  $merchantID;
				$transactiondata['resellerID']        = $this->resellerID;
				$transactiondata['gateway']   = $gatewayName;

				$transactiondata = alterTransactionCode($transactiondata);
				$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
				if(!empty($this->transactionByUser)){
				    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
				    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
				}
                if($custom_data_fields){
                    $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                }
				$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed:</strong>'. ' ' . $result['resptext'] .'</div>');
			}
			$invoice_IDs = array();
		
			$receipt_data = array(
				'proccess_url' => 'QBO_controllers/CardPointePayment/payment_capture',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Capture',
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			if($paydata['invoiceTxnID'] == ''){
				$paydata['invoiceTxnID'] ='transaction';
			}
			if($paydata['customerListID'] == ''){
				$paydata['customerListID'] ='null';
			}
			if($result['retref'] == ''){
				$result['retref'] ='null';
			}
			redirect('QBO_controllers/home/transation_credit_receipt/'.$paydata['invoiceTxnID'].'/'.$paydata['customerListID'].'/'.$result['retref'],  'refresh'); 
		}


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['id'];

		$compdata				= $this->customer_model->get_customers($user_id);

		$data['customers']		= $compdata;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_transaction', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/**
     * Void Transaction Amount and return to reciept page
     * @return Receipt page
    */
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
        $custom_data_fields = [];
		if (!empty($this->input->post())) {

			if ($this->session->userdata('logged_in')) {
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}


			$tID     = $this->czsecurity->xssCleanPostInput('txnvoidID');

			$con     = array('transactionID' => $tID);
			$paydata = $this->general_model->get_row_data('customer_transaction', $con);
			$gatlistval = $paydata['gatewayID'];
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;

			$cardpointeuser  = $gt_result['gatewayUsername'];
			$cardpointepass  =  $gt_result['gatewayPassword'];
			$cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $cardpointeSiteName  = $gt_result['gatewaySignature'];
			$gatewayName = getGatewayName($gt_result['gatewayType']);
			$gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "CardPointe";

			$client = new CardPointe();
			$customerID = $paydata['customerListID'];
			$amount  =  $paydata['transactionAmount'];

			$con_cust = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
			$result = $client->void($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $amount, $tID);
			if ($result['response_code'] == '100') {

				$condition = array('transactionID' => $tID);

				$update_data =   array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

				$this->general_model->update_row_data('customer_transaction', $condition, $update_data);

				if ($chh_mail == '1') {
					$condition = array('transactionID' => $tID);
					$customerID = $paydata['customerListID'];

					$tr_date   = date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['userEmail'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['fullName'];
					$this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');
				}

				$this->session->set_flashdata('success', 'The transaction has been voided.');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
			}
			$transactiondata = array();
			$transactiondata['transactionID']      = $result['retref'];
			$transactiondata['transactionStatus']  = $result['res'];
			$transactiondata['transactionDate']    = date('Y-m-d H:i:s');
			$transactiondata['transactionModified']     = date('Y-m-d H:i:s');
			$transactiondata['transactionType']    = $result['type'];
			$transactiondata['transactionGateway'] = $gt_result['gatewayType'];
			$transactiondata['gatewayID']        = $gatlistval;
			$transactiondata['transactionCode']   = $result['response_code'];
			$transactiondata['customerListID']     = $customerID;
			$transactiondata['transactionAmount']  = $amount;
			$transactiondata['merchantID']         = $merchantID;
			$transactiondata['resellerID']         = $this->resellerID;
			$transactiondata['gateway']   = $gatewayName;

			$transactiondata = alterTransactionCode($transactiondata);
			$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
			if(!empty($this->transactionByUser)){
			    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
			    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
			}
            if($custom_data_fields){
                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
            }
			$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);


			redirect('QBO_controllers/CardPointePayment/payment_capture', 'refresh');
		}
	}

    /**
     * Payment capture page data function
     * @return Capture Page View
    */
	public function payment_capture()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']   = $this->qbo_customer_model->get_transaction_data_captue($user_id);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_capture', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
    /**
     * Payment Refund Transaction page 
     * @return view
    */
	public function payment_refund()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['transactions']    = $this->qbo_customer_model->get_transaction_data_refund($user_id);

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_refund', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
    /**
     * Transaction page 
     * @return view
    */
	public function payment_transaction()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$this->qboOBJ = new QBO_data($user_id); 
		$this->qboOBJ->get_invoice_data();

		$data['transactions']   = $this->qbo_customer_model->get_transaction_history_data($user_id);

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_transaction', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	 /**
     * Fetch Transaction Data 
     * @return json
    */
	public function ajax_payment_list()
	{
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$list  = $this->qbo_invoices_model->get_datatables_transaction($user_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();

			$res_inv = '';
			if (!empty($person->invoiceID) && preg_match('/^[0-9,]+$/', $person->invoiceID)) {
				$inv = $person->invoiceID;

				$invList = explode(',', $inv);

				foreach($invList as $key1 => $value) {
					$value = trim($value);
					if(empty($value) || $value == ''){
						unset($invList[$key1]);	
					}
				}

				$inv = implode(',', $invList); 

				$qq = $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  QBO_test_invoice where invoiceID IN($inv) and  merchantID=$user_id  GROUP BY merchantID ");

				if ($qq->num_rows > 0) {
					$res_inv = $qq->row_array()['invoce'];
				}
			}

			$invoice = ($res_inv) ? $res_inv : '--';
			$balance = ($person->transactionAmount) ? number_format($person->transactionAmount, 2) : '0.00';
			$transactionID = ($person->transactionID) ? $person->transactionID : '---';

			$amount = ($person->transactionAmount) ? $person->transactionAmount : '0.00';


			$row[] = $person->fullName;
			
			$row[] = '<div class="hidden-xs text-right"> ' . $invoice . ' </div>';
			$row[] = '<div class="hidden-xs text-right"> $' . $balance . ' </div>';
			$row[] = "<div class='hidden-xs text-right'> " . date('M d, Y', strtotime($person->transactionDate)) . " </div>";
			$type = '';
			if ($person->partial != '0') {

				$type .= "<label class='label label-warning'>Partially Refunded :" . number_format($person->partial, 2) . " </label><br/>";
			} else if ($person->partial == $person->transactionAmount) {
				$type .= "<label class='label label-success'>Fully Refunded : " . number_format($person->partial, 2) . "</label><br/>";
			} else {

				$type .= "";
			}

			$row[] = "<div class='hidden-xs text-right'>" . $type . strtoupper($person->transactionType);

			$row[] = '<div class="hidden-xs text-right"> ' . $transactionID . ' </div>';

			$link = '';


			$link .= '<div class="text-center"> <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">';

			if (
				in_array($person->transactionCode, array('100', '200', '111', '1')) &&  in_array(strtoupper($person->transactionType), array('SALE', 'AUTH_CAPTURE', 'Offline Payment', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
			) {

				$link .= '<li> <a href="#payment_refunds" class="" onclick="set_refund_pay(' . $person->id . ', ' . $person->transactionID . ', ' . $person->transactionGateway . ', ' . $amount . ' );" 
        					 data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>';
			}

			$link .= '<li><a href="#payment_delete" class=""  onclick="set_transaction_pay(' . $person->id . ');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a></li>
					        </ul>
					   </div> </div>';



			$row[] = $link;

			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->qbo_invoices_model->count_all_transaction($user_id),
			"recordsFiltered" => $this->qbo_invoices_model->count_filtered_transaction($user_id),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
		die;
	}
	/*------------------- Ajax Payment Transaction End -------------- */


	/**
     * eCheck Sale A Transaction 
     * @return Receipt Page
    */
	public function create_customer_esale()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		
		if (!empty($this->input->post())) {

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$merchant_condition = [
				'merchID' => $merchantID,
			];

			$user_id = $merchantID;

			$custom_data_fields = [];
            // get custom field data
            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
            }

            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
            }

			if (!merchant_gateway_allowed($merchant_condition)) {
				$gt_result = $this->general_model->merchant_default_gateway($merchant_condition);
				$gt_result = $gt_result[0];

				$gatlistval = $gt_result['gatewayID'];
			} else {
				$gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
			}

			$checkPlan = check_free_plan_transactions();

			if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
				$cardpointeuser   = $gt_result['gatewayUsername'];
				$cardpointepass   = $gt_result['gatewayPassword'];
				$cardpointeMerchID = $gt_result['gatewayMerchantID'];
                $cardpointeSiteName   = $gt_result['gatewaySignature'];
				$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
				$companyID  = $comp_data['companyID'];
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');

				$payableAccount = $this->czsecurity->xssCleanPostInput('payable_ach_account');
				$sec_code =     'WEB';

				if($payableAccount == '' || $payableAccount == 'new1') {
					$accountDetails = [
						'accountName' => $this->czsecurity->xssCleanPostInput('account_name'),
						'accountNumber' => $this->czsecurity->xssCleanPostInput('account_number'),
						'routeNumber' => $this->czsecurity->xssCleanPostInput('route_number'),
						'accountType' => $this->czsecurity->xssCleanPostInput('acct_type'),
						'accountHolderType' => $this->czsecurity->xssCleanPostInput('acct_holder_type'),
						'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
						'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'companyID'     => $companyID,
						'merchantID'   => $merchantID,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						'secCodeEntryMethod' => $sec_code
					];
				} else {
					$accountDetails = $this->card_model->get_single_card_data($payableAccount);
				}
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

				$client = new CardPointe();
				
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
				}

				$amount = $this->czsecurity->xssCleanPostInput('totalamount');
				
				$result = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountDetails['accountNumber'], $accountDetails['routeNumber'], $amount, $accountDetails['accountType'], $accountDetails['accountName'], $accountDetails['Billing_Addr1'], $accountDetails['Billing_City'], $accountDetails['Billing_State'], $accountDetails['Billing_Zipcode']);

				if ($result['resptext'] == 'Approved' || $result['resptext'] == 'Approval') {

					$trID = $result['retref'];

					$invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
                    }

					$val = array('merchantID' => $user_id);
					$tokenData = $this->general_model->get_row_data('QBO_token', $val);

					$accessToken = $tokenData['accessToken'];
					$refreshToken = $tokenData['refreshToken'];
					$realmID      = $tokenData['realmID'];

					$dataService = DataService::Configure(array(
						'auth_mode' => $this->config->item('AuthMode'),
						'ClientID'  => $this->config->item('client_id'),
						'ClientSecret' => $this->config->item('client_secret'),
						'accessTokenKey' =>  $accessToken,
						'refreshTokenKey' => $refreshToken,
						'QBORealmID' => $realmID,
						'baseUrl' => $this->config->item('QBOURL'),
					));

					$refNumber = array();
                    $merchantID = $user_id;
					if (!empty($invoiceIDs)) {
						$payIndex = 0;
                        $saleAmountRemaining = $amount;
						foreach ($invoiceIDs as $inID) {
							$theInvoice = array();


							$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

							$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

							$con = array('invoiceID' => $inID, 'merchantID' => $user_id);
							$res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;
                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            $ispaid      = 0;
                            $isRun = 0;
                            $BalanceRemaining = 0.00;
                            if($amount_data == $actualInvoicePayAmount){
                                $actualInvoicePayAmount = $amount_data;
                                $isPaid      = 1;

                            }else{

                                $actualInvoicePayAmount = $actualInvoicePayAmount;
                                $isPaid      = 0;
                                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                
                            }
                            $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;
                            $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
                            $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

							if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
								$theInvoice = current($targetInvoiceArray);

								$createPaymentObject = [
									"TotalAmt" => $actualInvoicePayAmount,
									"SyncToken" => 1, 
									"CustomerRef" => $customerID,
									"Line" => [
										"LinkedTxn" => [
											"TxnId" => $inID,
											"TxnType" => "Invoice",
										],
										"Amount" => $actualInvoicePayAmount
									]
								];

                                $paymentMethod = $this->general_model->qbo_payment_method('Check', $user_id, true);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($responseId) && $responseId != ''){
                                    $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                                }

                                $newPaymentObj = Payment::create($createPaymentObject);

								$savedPayment = $dataService->Add($newPaymentObj);

								$error = $dataService->getLastError();
								if ($error != null) {
									$err = '';
									$err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
									$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
									$err .= "The Response message is: " . $error->getResponseBody() . "\n";
									$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .	$err . '</div>');

									$st = '0';
									$action = 'Pay Invoice';
									$msg = "Payment Success but " . $error->getResponseBody();

									$qbID = $trID;
									$pinv_id = '';
								} else {
									$pinv_id = '';
									$pinv_id        =  $savedPayment->Id;
									$st = '1';
									$action = 'Pay Invoice';
									$msg = "Payment Success";
									$qbID = $trID;
								}
								$qbID = $inID;
								$qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

								$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
								if($syncid){
		                            $qbSyncID = 'CQ-'.$syncid;
									$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

		                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
		                        }
								$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
							}
							$payIndex++;
						}
					} else {

						$crtxnID = '';
						$inID = '';
						$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
					}
					
					if ($this->czsecurity->xssCleanPostInput('tr_checked'))
                        $chh_mail = 1;
                    else
						$chh_mail = 0;
						
					//send mail on esale
					$condition_mail         = array('templateType' => '5', 'merchantID' => $merchantID);
					$ref_number = '';
					$tr_date   = date('Y-m-d H:i:s');
					$toEmail = $comp_data['userEmail'];
					$company = $comp_data['companyName'];
					$customer = $comp_data['fullName'];
					
					if($payableAccount == '' || $payableAccount == 'new1') {
						$id1 = $this->card_model->process_ack_account($accountDetails);
					}
					if ($chh_mail == '1') {
                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $trID);
					}
				
					$this->session->set_flashdata('success', 'Transaction Successful');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['resptext'] . '</div>');
				}

			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
			}

			$invoice_IDs = array();
			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
				$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
			}

			$receipt_data = array(
				'transaction_id' => $result['retref'],
				'IP_address' => $_SERVER['REMOTE_ADDR'],
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'QBO_controllers/Payments/create_customer_esale',
				'proccess_btn_text' => 'Process New Sale',
				'sub_header' => 'Sale',
				'checkPlan'	=> $checkPlan
			);
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_name'] 	= "NMI ESale";


		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['merchID'] 	= $user_id;
		$this->customerSync($user_id);

		$condition				= array('merchantID' => $user_id);

		$merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}
		
		$gateway		= $this->general_model->get_gateway_data($user_id, 'echeck');
		$data['gateways']      = $gateway['gateway'];
		$data['gateway_url']   = $gateway['url'];
		
		$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/payment_esale', $data);
		$this->load->view('QBO_views/page_popup_modals', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/**********END****************/

    /**
     * Fetch merchant gateway friendly name 
     * @return Json
    */
	public function chk_friendly_name()
	{
		$res = array();
		$frname  = $this->czsecurity->xssCleanPostInput('frname');
		$condition = array('gatewayFriendlyName' => $frname);
		$num   = $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
		if ($num) {
			$res = array('gfrname' => 'Friendly name already exist', 'status' => 'false');
		} else {
			$res = array('status' => 'true');
		}
		echo json_encode($res);
		die;
	}

    /**
     * Sync credit
     * @return Credit Page View
    */
	public function credit()
	{

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$merchID = $user_id;
		$condition =  array('merchantID' => $merchID);
		$val = array(
			'merchantID' => $merchID,
		);
		$data = $this->general_model->get_row_data('QBO_token', $val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];

		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' => $this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),

		));

		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
		$qbo_data = $this->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery'));

		if (!empty($qbo_data)) {


			$entities = $dataService->Query("SELECT * FROM CreditMemo where Metadata.LastUpdatedTime > '" . $qbo_data['lastUpdated'] . "' ");
		} else {
			$entities = $dataService->Query("SELECT * FROM CreditMemo ");
		}


		//Add a new Invoice
		$eror = '';
		$error = $dataService->getLastError();
		if ($error != null) {
			$eror .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
			$eror .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$eror .= "The Response message is: " . $error->getResponseBody() . "\n";
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> ' . $eror . '</div>');
			redirect(base_url('QBO_controllers/home/index'));
		}

		if (!empty($qbo_data)) {
			$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('00:00:00');
			$updatedata['updatedAt'] = date('Y-m-d H:i:s');

			$this->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery'), $updatedata);
		} else {
			$updateda = date('Y-m-d') . 'T' . date('00:00:00');
			$upteda = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'CreditMemoQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
			$this->general_model->insert_row('tbl_qbo_config', $upteda);
		}
		if (!empty($entities)) {

			foreach ($entities as $oneCredit) {

				$qb_credit['TxnID'] =  $oneCredit->Id;
				$qb_credit['CustomerListID']   = $oneCredit->CustomerRef;
				$qb_credit['RefNumber']        = ($oneCredit->AutoDocNumber) ? $oneCredit->AutoDocNumber : $oneCredit->DocNumber;
				$qb_credit['CreditRemaining']  = $oneCredit->RemainingCredit;
				$qb_credit['AccountListID']    = $oneCredit->ARAccountRef;
				$qb_credit['TxnDate']          = $oneCredit->TxnDate;
				$qb_credit['DueDate']          = $oneCredit->DueDate;
				$qb_credit['TxnNumber']        = $oneCredit->DocNumber;
				$qb_credit['customerMemoNote'] = $oneCredit->CustomerMemo;
				$qb_credit['creditMemoNote'] = $oneCredit->PrivateNote;
				$qb_credit['TimeCreated']       = date('Y-m-d H:i:s', strtotime($oneCredit->MetaData->CreateTime));
				$qb_credit['TimeModified']      =  date('Y-m-d H:i:s', strtotime($oneCredit->MetaData->LastUpdatedTime));
				$qb_credit['SubTotal']          = $oneCredit->TxnDate;
				$qb_credit['TotalAmount']       = $oneCredit->TotalAmt;
				$qb_credit['merchantID']        = $merchID;
				$qb_credit['companyID']          = $realmID;

				if ($this->general_model->get_num_rows('qbo_customer_credit', array('companyID' => $realmID, 'TxnID' => $oneCredit->Id, 'merchantID' => $merchID)) > 0) {



					$this->general_model->update_row_data('qbo_customer_credit', array('companyID' => $realmID, 'TxnID' => $oneCredit->Id, 'merchantID' => $merchID), $qb_credit);
				} else {

					$this->general_model->insert_row('qbo_customer_credit', $qb_credit);
				}

			}
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$condition  = array('cr.merchantID' => $user_id);

		$data['credits'] = $this->qbo_customer_model->get_credit_user_data($user_id);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_credit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

    /**
     * Update credit data
     * @return Credit Page View
    */
	public function update_credit()
	{
		if ($this->czsecurity->xssCleanPostInput('creditEditID') != "") {

			$id = $this->czsecurity->xssCleanPostInput('creditEditID');
			$chk_condition = array('creditID' => $id);

			$input_data['creditDate']  = date("Y-m-d");
			$input_data['creditAmount']  = $this->czsecurity->xssCleanPostInput('amount');
			$input_data['creditDescription']  = $this->czsecurity->xssCleanPostInput('description');

			if ($this->general_model->update_row_data('tbl_credits', $chk_condition, $input_data)) {
				$this->session->set_flashdata('success', 'Successfully Updated');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
		}
		redirect(base_url('QBO_controllers/CardPointePayment/credit'));
	}

    /**
     * get credit data by ID
     * @return Json
    */
	public function get_creditedit_id()
	{
		$id = $this->czsecurity->xssCleanPostInput('credit_id');
		$val = array(
			'creditID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_credits', $val);
		echo json_encode($data);
	}

    /**
     * get card data
     * @return Json
    */
	public function get_card_data()
	{
		$customerdata = array();
		if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
			$crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_single_card_data($crID);
			if (!empty($card_data)) {
				$customerdata['status'] =  'success';
				$customerdata['card']     = $card_data;
				echo json_encode($customerdata);
				die;
			}
		}
	}

	/**
     * delete credit data by ID
     * @return Credit List page
    */
	public function delete_credit()
	{

		$creditID = $this->czsecurity->xssCleanPostInput('qbo_invoicecreditid');
		$condition =  array('creditID' => $creditID);
		$del      = $this->general_model->delete_row_data('tbl_credits', $condition);

		if ($del) {
			
			$this->session->set_flashdata('success', 'Successfully Deleted');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Somthing Is Wrong...</div>');
		}
		redirect(base_url('QBO_controllers/CardPointePayment/credit'));
	}

    /**
     * Fetch Card Expiry Data
     * @param $customerID
     * @return array
    */
	public function get_card_expiry_data($customerID):array
	{

		$card = array();
		$this->load->library('encrypt');

		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'    ";
		$query1 = $this->db1->query($sql);
		$card_datas =   $query1->result_array();
		if (!empty($card_datas)) {
			foreach ($card_datas as $key => $card_data) {

				$customer_card['CardNo']  = substr($this->card_model->decrypt($card_data['CustomerCard']), 12);
				$customer_card['CardID'] = $card_data['CardID'];
				$customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
				$card[$key] = $customer_card;
			}
		}

		return  $card;
	}

    /**
     * Fetch Card Data
     * @return json
    */
	public function get_card_edit_data()
	{

		if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
			$cardID = $this->czsecurity->xssCleanPostInput('cardID');
			$data   = $this->card_model->get_single_mask_card_data($cardID);
			echo json_encode(array('status' => 'success', 'card' => $data));
			die;
		}
		echo json_encode(array('status' => 'success'));
		die;
	}

    /**
     * Fetch QBO Customer Card Data
     * @return json
    */
	public function check_qbo_vault()
	{


		$card = '';
		$card_name = '';
		$customerdata = array();

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] = $this->session->userdata('logged_in');

			$merchantID = $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] = $this->session->userdata('user_logged_in');

			$merchantID = $data['login_info']['merchantID'];
		}
		$gatewayID 		= $this->czsecurity->xssCleanPostInput('gatewayID');
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');

			$condition     =  array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('QBO_custom_customer', $condition);
			if (!empty($customerdata)) {

				$customerdata['status'] =  'success';
				$conditionGW = array('gatewayID' => $gatewayID);
				$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
				$cardTypeOption = 2;
				if(isset($gateway['gatewayID'])){
					if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 1;
					}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 2;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 3;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 4;
					}else{
						$cardTypeOption = 2;
					}
					
				}
				$customerdata['cardTypeOption']  = $cardTypeOption;
				$card_data =   $this->card_model->getCardData($customerID,$cardTypeOption);
				
				$customerdata['card']  = $card_data;

				echo json_encode($customerdata);
				die;
			}
		}else{
			$customerdata = [];
			$conditionGW = array('gatewayID' => $gatewayID);
			$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
			$cardTypeOption = 2;
			if(isset($gateway['gatewayID'])){
				if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 1;
				}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 2;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 3;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 4;
				}else{
					$cardTypeOption = 2;
				}
				
			}
			
			$customerdata['status']  = 'success';
			$customerdata['cardTypeOption']  = $cardTypeOption;
			echo json_encode($customerdata);
				die;
		}
	}

    /**
     * Fetch Transaction
     * @return HTML
    */
	public function view_transaction()
	{
		$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['merchID'];
		$transactions           = $this->qbo_customer_model->get_invoice_transaction_data($invoiceID, $user_id);

		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {
		?>
				<tr>
					<td class="text-left"><?php echo ($transaction['transactionID']) ? $transaction['transactionID'] : '----'; ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($transaction['transactionAmount'], 2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo ucfirst($transaction['transactionType']); ?></td>
					<td class="text-right visible-lg"><?php if ($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') { ?> <span class="">Success</span><?php } else { ?> <span class="">Failed</span> <?php } ?></td>

				</tr>

<?php     }
		} else {
			echo '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}
		die;
	}

    /**
     * Pay multiple invoice 
     * @return Redirect to page with message
    */
	public function pay_multi_invoice()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
		$gateway    = $this->czsecurity->xssCleanPostInput('gateway1');
		$merchantID = $merchID;
        $custom_data_fields = [];

		$checkPlan = check_free_plan_transactions();
		$cusproID   = '';
		$cusproID   = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		$error      = '';
		$resellerID = $this->resellerID;

		if ($cardID != "" && $gateway != "") {
			$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
			$cardpointeuser   = $gt_result['gatewayUsername'];
			$cardpointepass   = $gt_result['gatewayPassword'];
			$cardpointeMerchID = $gt_result['gatewayMerchantID'];
            $cardpointeSiteName  = $gt_result['gatewaySignature'];
			$invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
			if (!empty($invoices)) {
				foreach ($invoices as $key => $invoiceID) {
					$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
					$in_data     = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
					if (!empty($in_data)) {

						$Customer_ListID = $in_data['CustomerListID'];
						$c_data          = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'firstName', 'lastName', 'companyName'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
						$companyID       = $c_data['companyID'];

						if ($cardID == 'new1') {
							$cardID_upd = $cardID;
							$card_no    = $this->czsecurity->xssCleanPostInput('card_number');
							$expmonth   = $this->czsecurity->xssCleanPostInput('expiry');
							$exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv        = $this->czsecurity->xssCleanPostInput('cvv');
	
							$address1 = $this->czsecurity->xssCleanPostInput('address1');
							$address2 = $this->czsecurity->xssCleanPostInput('address2');
							$city     = $this->czsecurity->xssCleanPostInput('city');
							$country  = $this->czsecurity->xssCleanPostInput('country');
							$phone    = $this->czsecurity->xssCleanPostInput('contact');
							$state    = $this->czsecurity->xssCleanPostInput('state');
							$zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
						} else {
							$card_data = $this->card_model->get_single_card_data($cardID);
							$card_no   = $card_data['CardNo'];
							$cvv       = $card_data['CardCVV'];
							$expmonth  = $card_data['cardMonth'];
							$exyear    = $card_data['cardYear'];
	
							$address1 = $card_data['Billing_Addr1'];
							$address2 = $card_data['Billing_Addr2'];
							$city     = $card_data['Billing_City'];
							$zipcode  = $card_data['Billing_Zipcode'];
							$state    = $card_data['Billing_State'];
							$country  = $card_data['Billing_Country'];
							$phone    = $card_data['Billing_Contact'];
						}
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;


						if (!empty($cardID)) {

							$cr_amount = 0;
							$amount    = $pay_amounts;
							$amount    = $amount - $cr_amount;

							$name     = $in_data['fullName'];
							$address  = $in_data['ShipAddress_Addr1'];
							$address2 = $in_data['ShipAddress_Addr2'];
							$city     = $in_data['ShipAddress_City'];
							$state    = $in_data['ShipAddress_State'];
							$zipcode  = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

							if (strlen($expmonth) == 1) {
								$expmonth = '0' . $expmonth;
							}
							$expry = $expmonth . $exyear;
						   
							$client = new CardPointe();	

							$responseId = $crtxnID = '';
							$result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $address1, $city, $state, $zipcode);

							if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') {
								$responseId = $result['retref'];

								$val                   = array(
									'merchantID' => $merchID,
								);
								$data = $this->general_model->get_row_data('QBO_token', $val);

								$accessToken  = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								$realmID      = $data['realmID'];
								$dataService  = DataService::Configure(array(
									'auth_mode'       => $this->config->item('AuthMode'),
									'ClientID'        => $this->config->item('client_id'),
									'ClientSecret'    => $this->config->item('client_secret'),
									'accessTokenKey'  => $accessToken,
									'refreshTokenKey' => $refreshToken,
									'QBORealmID'      => $realmID,
									'baseUrl'         => $this->config->item('QBOURL'),
								));

								$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

								$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

								if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
									$theInvoice = current($targetInvoiceArray);
								}
								

								$createPaymentObject = [
									"TotalAmt"    => $amount,
									"SyncToken"   => 1,
									"CustomerRef" => $Customer_ListID,
									"Line"        => [
										"LinkedTxn" => [
											"TxnId"   => $invoiceID,
											"TxnType" => "Invoice",
										],
										"Amount"    => $amount,
									],
								];

								$paymentMethod = $this->general_model->qbo_payment_method($cardType, $merchID, false);
								if($paymentMethod){
									$createPaymentObject['PaymentMethodRef'] = [
										'value' => $paymentMethod['payment_id']
									];
								}
								if(isset($responseId) && $responseId != ''){
									$createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
								}

								$newPaymentObj = Payment::create($createPaymentObject);

								$savedPayment = $dataService->Add($newPaymentObj);

								$error = $dataService->getLastError();
								if ($error != null) {
									$err = "The Status code is: " . $error->getHttpStatusCode() . "\n";
									$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
									$err .= "The Response message is: " . $error->getResponseBody() . "\n";
									$st     = '0';
									$action = 'Pay Invoice';
									$msg    = "Payment Success but " . $error->getResponseBody();
									$qbID   = $responseId;
									$crtxnID        = $savedPayment->Id;

								} else {

									$this->session->set_flashdata('success', 'Successfully Processed Invoice');

									$st     = '1';
									$action = 'Pay Invoice';
									$msg    = "Payment Success";
									$qbID   = $responseId;
								}

								if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
									$card_no   = $this->czsecurity->xssCleanPostInput('card_number');
									$expmonth  = $this->czsecurity->xssCleanPostInput('expiry');
									$exyear    = $this->czsecurity->xssCleanPostInput('expiry_year');
									$cvv       = $this->czsecurity->xssCleanPostInput('cvv');
									$card_type = $this->general_model->getType($card_no);

									$card_data = array(
										'cardMonth'       => $expmonth,
										'cardYear'        => $exyear,
										'CardType'        => $card_type,
										'CustomerCard'    => $card_no,
										'CardCVV'         => $cvv,
										'customerListID'  => $Customer_ListID,
										'companyID'       => $companyID,
										'merchantID'      => $merchID,

										'createdAt'       => date("Y-m-d H:i:s"),
										'Billing_Addr1'   => $address1,
										'Billing_Addr2'   => $address2,
										'Billing_City'    => $city,
										'Billing_State'   => $state,
										'Billing_Country' => $country,
										'Billing_Contact' => $phone,
										'Billing_Zipcode' => $zipcode,
									);

									$id1 = $this->card_model->process_card($card_data);
								}

							} else {
								$st     = '0';
								$action = 'Pay Invoice';
								$msg    = "Payment Failed";
								$qbID   = $invoiceID;

								$err_msg      = $result['resptext'];

								$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
							}

							$id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $merchID, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
						}

					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
					}

				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
			}

		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
		}

		if(!$checkPlan){
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
		}

		if ($cusproID != "") {
			redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
		} else {
			redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
		}

	}

    /**
     * Fetch Customer Card Data
     * @return json
    */
	public function check_vault()
	{


		$card = '';
		$card_name = '';
		$customerdata = array();
		$invoiceIDs = [];
		$gatewayID 		= $this->czsecurity->xssCleanPostInput('gatewayID');
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			if ($this->session->userdata('logged_in')) {
				$data['login_info'] = $this->session->userdata('logged_in');
	
				$merchantID = $data['login_info']['merchID'];
			} else if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] = $this->session->userdata('user_logged_in');
	
				$merchantID = $data['login_info']['merchantID'];
			}

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');
			$condition     =  array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('QBO_custom_customer', $condition);

			if (!empty($customerdata)) {
				$this->qboOBJ = new QBO_data($merchantID); 

				$this->qboOBJ->get_invoice_data(['customerId' => $customerID]);

				$invoices = $this->qbo_customer_model->get_invoice_upcomming_data($customerID, $merchantID);

				$table = '';
				if (!empty($invoices)) {

					#Invoice existence check at QBO
					$invoices = $this->qboOBJ->check_qbo_invoice_mul($invoices);

					if(!empty($invoices)) {
						$table .= '<table class="col-md-offset-3 mytable" width="50%">';
						$table .= "<tr>
							<th class='text-left'>Invoice</th>
							<th class='text-right'>Amount</th>
						</tr>";

						foreach ($invoices as $inv) {
							if ($inv['status'] != 'Cancel') {

								$invoiceIDs[$inv['invoiceID']] =  $inv['refNumber'];
								$table .= "<tr>
								<td class='text-left'><input type='checkbox' id='test-inv-checkbox-".$inv['invoiceID']."'' name='inv' value='" . $inv['BalanceRemaining'] . "' class='test' data-checkclass='" . $inv['refNumber'] . "' rel='" . $inv['invoiceID'] . "'/> " . $inv['refNumber'] . "</td>
								<td class='text-right'>" .  '$' .  number_format((float) $inv['BalanceRemaining'], 2, '.', ',') . "</td>
								</tr>";
							}
						}
						$table .= "</table>";
					}
				}

				if(empty($customerdata['companyName'])){
					$customerdata['companyName'] = $customerdata['fullName'];
				}
				
				$customerdata['invoices'] = $table;
				$customerdata['status'] =  'success';

				$ach_data =   $this->card_model->get_ach_info_data($customerID);
				$ACH = [];
				if(!empty($ach_data)){
					foreach($ach_data as $card){
						$ACH[] = $card['CardID'];
					}
				}
				$recentACH = end($ACH);
				$customerdata['ach_data']  = $ach_data;
				$customerdata['recent_ach_account']  = $recentACH;

				$conditionGW = array('gatewayID' => $gatewayID);
				$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
				$cardTypeOption = 2;
				if(isset($gateway['gatewayID'])){
					if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 1;
					}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 2;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
						$cardTypeOption = 3;
					}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
						$cardTypeOption = 4;
					}else{
						$cardTypeOption = 2;
					}
					
				}
				$customerdata['cardTypeOption']  = $cardTypeOption;
				$card_data =   $this->card_model->getCardData($customerID,$cardTypeOption);
				
				$customerdata['card']  = $card_data;
				$recentCard = end($card_data);
				$customerdata['recent_card']  = ($recentCard && isset($recentCard['CardID'])) ? $recentCard['CardID'] : '';
				$customerdata['invoiceIDs'] = $invoiceIDs;
				echo json_encode($customerdata);
				die;
			}
		}else{
			$customerdata = [];
			$conditionGW = array('gatewayID' => $gatewayID);
			$gateway	= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
			$cardTypeOption = 2;
			if(isset($gateway['gatewayID'])){
				if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 1;
				}else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 2;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
					$cardTypeOption = 3;
				}else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
					$cardTypeOption = 4;
				}else{
					$cardTypeOption = 2;
				}
				
			}
			
			$customerdata['status']  = 'success';
			$customerdata['cardTypeOption']  = $cardTypeOption;
			echo json_encode($customerdata);
				die;
		}
	}
    /**
     * Get Invoce Details
     * @return HTML
    */
	public function get_invoice()
	{

		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$id = '';
		if (!empty($this->czsecurity->xssCleanPostInput('id'))) {
			$id = $this->czsecurity->xssCleanPostInput('id');
		}
		$invoices = $this->qbo_customer_model->get_invoice_upcomming_data($id, $merchID);
		if ($invoices) {
			echo '<table class="mytable" width="100%">';
			echo "<tr><th>Select</th><th>Ref Number</th><th>Total Amount</th></tr>";

			foreach ($invoices as $inv) {
				echo "<tr><td><input type='checkbox' name='inv' value='" . $inv['BalanceRemaining'] . "' class='test' rel='" . $inv['id'] . "'/></td><td>" . $inv['refNumber'] . "</td><td>" . $inv['BalanceRemaining'] . "</td>";
			}

			echo '</table>';
		} else {
			echo 'No pending Invoices Found';
		}
	}

    /**
     * Check transaction amount
     * @return Json
    */
	public function check_transaction_payment()
	{
		if ($this->session->userdata('logged_in')) {
			$da	= $this->session->userdata('logged_in');

			$user_id 				= $da['merchID'];
		} else if ($this->session->userdata('user_logged_in')) {
			$da 	= $this->session->userdata('user_logged_in');

			$user_id 				= $da['merchantID'];
		}

		if (!empty($this->czsecurity->xssCleanPostInput('trID'))) {
			$trID = $this->czsecurity->xssCleanPostInput('trID');
			$av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');

			$tr_data = $this->general_model->get_select_data('customer_transaction', array('transactionID'), array('id' => $trID));
			$tID     = $tr_data['transactionID'];
			$p_data = $this->qbo_customer_model->get_transaction_details_data(array('tr.transactionID' => $tID, 'cust.merchantID' => $user_id, 'tr.merchantID' => $user_id));
			if (!empty($p_data)) {
				if ($p_data['transactionAmount'] >= $av_amount) {
					$resdata['status']  = 'success';
				} else {
					$resdata['status']  = 'error';
					$resdata['message']  = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
				}
			} else {
				$resdata['status']  = 'error';
				$resdata['message']  = 'Invalid transactions ';
			}
		} else {
			$resdata['status']  = 'error';
			$resdata['message']  = 'Invalid request';
		}
		echo json_encode($resdata);
		die;
	}

	/**
     * Customer Sync By QBO
     * @return Json
    */
	private function customerSync($merchID){
		$val = array(
			'merchantID' => $merchID,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' =>$this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),
		));
		
		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

		$qbo_data = $this->general_model->get_select_data('tbl_qbo_config',array('lastUpdated'),array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery'));
 
		if(!empty($qbo_data))
		{
			$last_date   = $qbo_data['lastUpdated'];
			
			$my_timezone = date_default_timezone_get();
			$from = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
			$to   = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
			$last_date =  $this->general_model->datetimeconvqbo($last_date, $from, $to);

			$last_date1= strtotime('-8 hours',strtotime($last_date));
		
			$latedata= date_format(date_timestamp_set(new DateTime(), $last_date1)->setTimezone(new DateTimeZone('America/Los_Angeles')), 'c');
			  
			$sqlQuery = "SELECT * FROM Customer where   Metadata.LastUpdatedTime > '$latedata'";
		}else{
			$sqlQuery = "SELECT * FROM Customer";
		}

		$keepRunning = true;
		$entities = [];
		$st    = 0;
		$limit = 0;
		$total = 0;
		while($keepRunning === true){
            $mres   = MAXRESULT;
			$st_pos = MAXRESULT * $st + 1;
			$s_data = $dataService->Query("$sqlQuery  STARTPOSITION $st_pos MAXRESULTS $mres ");
			if ($s_data && !empty($s_data)) {
				$entities = array_merge($entities, $s_data);
				$total += count($s_data);
				$st = $st + 1;

				$limit = ($st) * MAXRESULT;
			} else {
				$keepRunning = false;
				break;
			}
		}
		
		$error = $dataService->getLastError();
     	$err='';
		if ($error != null) {
			$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
			$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err.="The Response message is: " . $error->getResponseBody() . "\n";

			$this->session->set_flashdata('message', '<div class="alert alert-danger">' . QBO_DISCONNECT_MSG . '</div>');
        	redirect(base_url('QBO_controllers/home/index'));
		}
        
		if(!empty($qbo_data))
		{ 
			$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
			$updatedata['updatedAt'] =date('Y-m-d H:i:s');
			if(!empty($entities))
				$this->general_model->update_row_data('tbl_qbo_config',array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery'),$updatedata);
		}
        else{
			$updateda= date('Y-m-d') . 'T' . date('H:i:s');
			$upteda = array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery','lastUpdated'=>$updateda, 'createdAt'=>date('Y-m-d H:i:s'), 'updatedAt'=>date('Y-m-d H:i:s'));
			if(!empty($entities))
				$this->general_model->insert_row('tbl_qbo_config', $upteda);
		} 
        
   
		if(!empty($entities))
		{
			foreach ($entities as $oneCustomer) {
				
				$QBO_customer_details = [];
				
				$QBO_customer_details['Customer_ListID'] = $oneCustomer->Id;
				$QBO_customer_details['firstname'] = $oneCustomer->GivenName;
				$QBO_customer_details['lastname'] = $oneCustomer->FamilyName;
				$QBO_customer_details['fullname'] = $oneCustomer->FullyQualifiedName;
				$QBO_customer_details['userEmail'] = ($oneCustomer->PrimaryEmailAddr)?$oneCustomer->PrimaryEmailAddr->Address:'';
				$QBO_customer_details['phoneNumber'] = ($oneCustomer->PrimaryPhone)?$oneCustomer->PrimaryPhone->FreeFormNumber:'';
				
				$QBO_customer_details['address1'] = (isset($oneCustomer->BillAddr->Line1) && $oneCustomer->BillAddr->Line1)?$oneCustomer->BillAddr->Line1:'';
				$QBO_customer_details['address2'] = (isset($oneCustomer->BillAddr->Line2) && $oneCustomer->BillAddr->Line2)?$oneCustomer->BillAddr->Line2:'';
				$QBO_customer_details['zipCode'] = (isset($oneCustomer->BillAddr->PostalCode) && $oneCustomer->BillAddr->PostalCode)?$oneCustomer->BillAddr->PostalCode:'';
				$QBO_customer_details['Country'] = (isset($oneCustomer->BillAddr->Country) && $oneCustomer->BillAddr->Country)?$oneCustomer->BillAddr->Country:'';
				$QBO_customer_details['State'] = (isset($oneCustomer->BillAddr->CountrySubDivisionCode) && $oneCustomer->BillAddr->CountrySubDivisionCode)?$oneCustomer->BillAddr->CountrySubDivisionCode:'';
				$QBO_customer_details['City'] = (isset($oneCustomer->BillAddr->City) && $oneCustomer->BillAddr->City)?$oneCustomer->BillAddr->City:'';
				
				$QBO_customer_details['ship_address1'] = (isset($oneCustomer->ShipAddr->Line1) && $oneCustomer->ShipAddr->Line1)?$oneCustomer->ShipAddr->Line1:'';
				$QBO_customer_details['ship_address2'] = (isset($oneCustomer->ShipAddr->Line2) && $oneCustomer->ShipAddr->Line2)?$oneCustomer->ShipAddr->Line2:'';
				$QBO_customer_details['ship_zipcode'] = (isset($oneCustomer->ShipAddr->PostalCode) && $oneCustomer->ShipAddr->PostalCode)?$oneCustomer->ShipAddr->PostalCode:'';
				$QBO_customer_details['ship_country'] = (isset($oneCustomer->ShipAddr->Country) && $oneCustomer->ShipAddr->Country)?$oneCustomer->ShipAddr->Country:'';
				$QBO_customer_details['ship_state'] = (isset($oneCustomer->ShipAddr->CountrySubDivisionCode) && $oneCustomer->ShipAddr->CountrySubDivisionCode)?$oneCustomer->ShipAddr->CountrySubDivisionCode:'';
				$QBO_customer_details['ship_city'] = (isset($oneCustomer->ShipAddr->City) && $oneCustomer->ShipAddr->City)?$oneCustomer->ShipAddr->City:'';
				
				
				$QBO_customer_details['companyName'] = str_replace("'", "\'",$oneCustomer->CompanyName);
				$QBO_customer_details['companyID'] = $realmID;
				$QBO_customer_details['createdAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->CreateTime));
				$QBO_customer_details['updatedAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->LastUpdatedTime));
				$QBO_customer_details['listID'] = '1';
				$QBO_customer_details['customerStatus'] = $oneCustomer->Active;;
				$QBO_customer_details['merchantID'] = $merchID;
				if($this->general_model->get_num_rows('QBO_custom_customer',array('companyID'=>$realmID,'Customer_ListID'=>$oneCustomer->Id , 'merchantID'=>$merchID)) > 0){  
					$this->general_model->update_row_data('QBO_custom_customer',array('companyID'=>$realmID, 'Customer_ListID'=>$oneCustomer->Id, 'merchantID'=>$merchID), $QBO_customer_details);
			
				}else{
					$this->general_model->insert_row('QBO_custom_customer', $QBO_customer_details);
				}
			
			}
		}
		return true;
	}
}
