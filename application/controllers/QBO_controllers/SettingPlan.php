<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 * 
 * This is a tiny pretend application which throws something into the queue so 
 * that the Web Connector can process it. 
 * 
 * @author Keith Palmer <keith@consolibyte.com>
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
require APPPATH .'libraries/qbo/QBO_data.php';

class SettingPlan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		
    	include APPPATH . 'third_party/nmiDirectPost.class.php';
	   
	    include APPPATH . 'third_party/nmiCustomerVault.class.php';
	
	    $this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
	    	$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_customer_model');
		$this->load->model('QBO_models/qbo_company_model');
		$this->load->model('company_model');

		
		        if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	
	   	redirect('QBO_controllers/SettingPlan/plans','refresh'); 
	}
	
	public function plans()
		{
		

		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				$data['merchantID']     =  $user_id ;
					
				
			
			 
		    	 $condition1				= array('merchantDataID'=>$user_id );
		        $data['plandata']   = $this->general_model->get_table_data('tbl_subscriptions_plan_qbo', $condition1);
		      
				$this->load->view('template/template_start', $data);
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/page_plans', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
		
	}	

	 public function create_plan()
	{
		
			  if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}

				$qboOBJ = new QBO_data($user_id); 
				$qboOBJ->get_items_data();
				
				$base_id = base64_encode($user_id);
	       		$coditionp=array('merchantID'=>$user_id,'customerPortal'=>'1');
		
				$urp_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortal'),$coditionp); 	
			       
			    if(empty($urp_data))
				{
					$this->session->set_flashdata('error','<strong>Errr: Please enable your customer portal</strong>'); 
					redirect('QBO_controllers/SettingConfig/setting_customer_portal','refresh');   
				} 
			    
		
     
     
     	      	$ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 
     	      
     	     
        		$data['ur_data'] = $ur_data;
				$position1=$position=0;    $purl='';
     
				$ttt = explode(PLINK,$ur_data['customerPortalURL']);
				
				$purl = $ttt[0].PLINK.'/customer/';
			    	
                $purl = $purl.'qbo_check_out/'.$base_id.'/';
              
				$data['dpurl']=$purl;
	
	    
	    	     $inv_pre_data =$this->general_model->get_row_data('tbl_merchant_invoices',array('merchantID'=>$user_id));
        		 if(empty($inv_pre_data))
        		 {
        		     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Errr: Please set your invoice prefix to create invoice</strong></div>'); 
        		    	redirect('QBO_controllers/SettingConfig/profile_setting','refresh');   
        		 }
	    
		if(!empty($this->input->post(null, true)))
		{
			 $this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');						
			$this->form_validation->set_rules('plan_name', 'Plan Name', 'required');
			if($this->czsecurity->xssCleanPostInput('autopay')){
				$this->form_validation->set_rules('gateway_list', 'Payment Gateway', 'required');
			}
        	  $rowID='';
   				 if($this->czsecurity->xssCleanPostInput('planID') !="") 
				 $rowID = $this->czsecurity->xssCleanPostInput('planID');
     	if ($this->form_validation->run() == true)
		{
        
				$total=0;	
			    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod){
                       $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
					   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
					   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					   $insert_row['itemTax'] =$this->czsecurity->xssCleanPostInput('tax_check')[$key];
					   $insert_row['itemFullName'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					   if ($this->czsecurity->xssCleanPostInput('onetime_charge')[$key] == 'One Time' || $this->czsecurity->xssCleanPostInput('onetime_charge')[$key] == '1') {
						   
						  $insert_row['oneTimeCharge'] ='1';  
					   }else{
						 $insert_row['oneTimeCharge'] ='0';  	
                         $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];	
					   }
					   
					   	if(empty($insert_row['itemTax'])){
							$insert_row['itemTax'] = 0;
					   	} else {
							$insert_row['itemTax'] = 1;
						}
					   
					   	$itemdata = $this->general_model->get_row_data('QBO_test_item', array('ListID' => $insert_row['itemListID'], 'merchantID' => $user_id));
						if($itemdata)
							$insert_row['itemFullName'] = $itemdata['FullName'];
					 
					   $item_val[$key] =$insert_row;
                }				
				
			    $pname      = $this->czsecurity->xssCleanPostInput('plan_name');
    				
				$plan        = $this->czsecurity->xssCleanPostInput('duration_list');		
				if($plan > 0){
		        	$subsamount  = $total/$plan ;	
				}else{ 
				   	$subsamount  = $total ;
				}    
				$first_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));		
				$invoice_date = date('d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));		
				$st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));			
				$freetrial    = $this->czsecurity->xssCleanPostInput('freetrial');
				
				$paygateway    = $this->czsecurity->xssCleanPostInput('gateway_list');
				$address1     = $this->czsecurity->xssCleanPostInput('address1');
				$address2     = $this->czsecurity->xssCleanPostInput('address2');				
				$country      = $this->czsecurity->xssCleanPostInput('country');
				$state        = $this->czsecurity->xssCleanPostInput('state');
				$city	      = $this->czsecurity->xssCleanPostInput('city');
				$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');
				$phone        = $this->czsecurity->xssCleanPostInput('phone');
				$paycycle     = $this->czsecurity->xssCleanPostInput('paycycle');
				$shoturl     = $this->czsecurity->xssCleanPostInput('short_url');
			 	$confirm_page_url = $this->czsecurity->xssCleanPostInput('confirm_page_url');
                $subdata =array(
				  'planName'    => $pname,
				   'subscriptionPlan'   =>$plan,
				   'subscriptionAmount' =>$total,
					'totalInvoice'      => $plan,
					'invoicefrequency'  => $paycycle,
					'freeTrial'			=> $freetrial,
				    'postPlanURL' => $shoturl,
					  'confirm_page_url' => $confirm_page_url, 
					'merchantPlanURL' => $purl
				
				);  
				
				if($this->czsecurity->xssCleanPostInput('plan_url')!='')
				{
			    	$plan_url = $this->czsecurity->xssCleanPostInput('plan_url');
			    	
			    	
			    	
			    	$ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 	
			       
			    	if(!empty($ur_data))
			    	{
					    $subdata['merchantPlanURL'] =$purl.$plan_url;
						$subdata['postPlanURL'] = $plan_url;
			    	}else{
			    	   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Please create merchant portak URL</div>');  
			    	   		redirect('QBO_controllers/SettingPlan/plans','refresh');
			    	}		
				}else{
				    
				    
					$subdata['postPlanURL'] = $shoturl;
					$subdata['merchantPlanURL'] = $purl.$shoturl;
					
				}
        
        			if($paycycle=='mon')
				{
				     if($this->czsecurity->xssCleanPostInput('pro_rate'))
				     {
				         	$subdata['proRate'] ='1';   
				         	
				         	$subdata['proRateBillingDay']    =$this->czsecurity->xssCleanPostInput('pro_billing_date');   
				         	$subdata['nextMonthInvoiceDate'] =$this->czsecurity->xssCleanPostInput('pro_next_billing_date');   
				         
				     }else{
				         
				            $subdata['proRate'] ='0';   
				         	
				         	$subdata['proRateBillingDay']    =($this->czsecurity->xssCleanPostInput('pro_billing_date'))?$this->czsecurity->xssCleanPostInput('pro_billing_date'):0;   
				         	$subdata['nextMonthInvoiceDate'] =($this->czsecurity->xssCleanPostInput('pro_next_billing_date'))?$this->czsecurity->xssCleanPostInput('pro_next_billing_date'):0; 
				     }
				    
				}
				
				if($this->czsecurity->xssCleanPostInput('autopay')){
				
					$subdata['automaticPayment'] ='1';
					$subdata['paymentGateway'] = (empty($paygateway)) ? 0 : $paygateway;
				}else{
					$subdata['automaticPayment'] ='0';	
					$subdata['paymentGateway'] ='0';	
				}
				
				if($this->czsecurity->xssCleanPostInput('email_recurring')){
					
					$subdata['emailRecurring'] ='1';
				}else{
					$subdata['emailRecurring'] ='0';	
				}
				
				if($this->czsecurity->xssCleanPostInput('require_shipping')){
				    $subdata['isShipping'] ='1';
				}
				else
				{
				    $subdata['isShipping'] ='0';
				}

				if($this->czsecurity->xssCleanPostInput('require_billing')){
				    $subdata['isBilling'] ='1';
				}
				else
				{
				    $subdata['isBilling'] ='0';
				}

        		if($this->czsecurity->xssCleanPostInput('require_service')){
				    $subdata['isService'] ='1';
				}
				else
				{
				    $subdata['isService'] ='0';
				}     
        
                
				if($this->czsecurity->xssCleanPostInput('billinfo')){
					
					$subdata['usingExistingAddress'] ='1';
				}else{
					$subdata['usingExistingAddress'] ='0';	
				}


				$isCC = ($this->czsecurity->xssCleanPostInput('cc') != null)?$this->czsecurity->xssCleanPostInput('cc'):0;
				$isEC = ($this->czsecurity->xssCleanPostInput('ec')  != null)?$this->czsecurity->xssCleanPostInput('ec'):0;
				$subdata['creditCard']  = $isCC;
				$subdata['eCheck']  = $isEC;

				if($this->czsecurity->xssCleanPostInput('planID') !="") {
				 $rowID = $this->czsecurity->xssCleanPostInput('planID');
				 $subdata['updatedAt']  = date('Y-m-d H:i:s');
				 
				 
				     $subs =  $this->general_model->get_row_data('tbl_subscriptions_plan_qbo',array('planID'=>$rowID));
				     	 
				 $ins_data = $this->general_model->update_row_data('tbl_subscriptions_plan_qbo',array('planID'=>$rowID) ,$subdata);	
				
   				$this->general_model->delete_row_data('tbl_subscription_plan_item_qbo', array('planID'=>$rowID));
				   
				    foreach($item_val as $k=>$item){
						$item['planID'] = $rowID;
						$ins = $this->general_model->insert_row('tbl_subscription_plan_item_qbo',$item);	
				    } 	
					
				}else{
				    $subdata['merchantDataID'] = $user_id;
                    $subdata['createdAt']  = date('Y-m-d H:i:s');
                    
					$rowID = $ins_data = $this->general_model->insert_row('tbl_subscriptions_plan_qbo',$subdata);	
					
					foreach($item_val as $k=>$item){
						$item['planID'] = $ins_data;
						$ins = $this->general_model->insert_row('tbl_subscription_plan_item_qbo',$item);	
				    } 		
					
				}
					
				 if($ins_data && $ins){
					
			        $this->session->set_flashdata('success','Success'); 
				 }else{
					 
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }
				
					redirect('QBO_controllers/SettingPlan/create_plan/'.$rowID,'refresh');
				
        }
       else{
		    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Validation Error</strong></div>'); 
		    		redirect('QBO_controllers/SettingPlan/create_plan/'.$rowID,'refresh');
		}			
		     
        }
			  
			  if($this->uri->segment('4')!="")
			  {
				$sbID = $this->uri->segment('4');
				$data['subs']		= $this->general_model->get_row_data('tbl_subscriptions_plan_qbo',array('planID'=>$sbID));
				
				$data['items']      = $this->general_model->get_table_data('tbl_subscription_plan_item_qbo',array('planID'=>$sbID)); 

			  }
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				
				$data['merchID'] 		= $user_id;
				$merchant_condition = [
					'merchID' => $user_id,
				];
		
				$data['isCreditCard'] = 0;
				$data['isEcheck'] = 0;
				$data['defaultGateway'] = false;
				if(!merchant_gateway_allowed($merchant_condition)){
					$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
					$data['defaultGateway'] = $defaultGateway[0];
				} elseif(isset($data['subs']['paymentGateway'])){
					$conditionGW				= array('gatewayID' => $data['subs']['paymentGateway']);
					$gatewayGetByID		= $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
					if($gatewayGetByID){
						$data['isCreditCard'] = $gatewayGetByID['creditCard'];
						$data['isEcheck'] = $gatewayGetByID['echeckStatus'];
					}
				}				
				
				$condition				= array('merchantID'=>$user_id );
  			    $conditionPlan 			= array('merchantID'=>$user_id,'IsActive'=>'true' );
					$data['base_id'] = base64_encode($user_id);
			    $data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
				$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy','' );
				$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan','' );
		    	
				$itemPlans = $this->general_model->get_table_data('QBO_test_item',$conditionPlan);
				$jsPlans = json_encode($itemPlans);
				$jsPlans = str_replace("'", "", $jsPlans);
				$jsPlans = str_replace( '\r\n', " ", $jsPlans);
				$data['plans'] = $itemPlans;
				$data['jsPlans'] = $jsPlans;

				$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
			
				
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/create_plan', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


}   
	
 
	
     public function delete_plan(){
	
	 $plancID = $this->czsecurity->xssCleanPostInput('plancID');
	 $condition =  array('planID'=>$plancID); 
	 $this->general_model->delete_row_data('tbl_subscription_plan_item_qbo', $condition);
	
	 $del  = $this->general_model->delete_row_data('tbl_subscriptions_plan_qbo', $condition);
            if($del){
					
			        $this->session->set_flashdata('success','Successfully Deleted'); 
				 }else{
					 
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>'); 
				 }
	   
						redirect('QBO_controllers/SettingPlan/plans','refresh');
	}
 
	
	 
	 
	 
	 public function checkout(){
	     
	     $data['template'] 		= template_variable();
	     $this->load->view('template/template_start', $data);
		 
	     $this->load->view('QBO_views/test');
	     $this->load->view('template/page_footer',$data);
		 $this->load->view('template/template_end', $data);
	     
	 }
	 
      public function recurring_payment()
     {
         
         	if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}
         
         
         if(!empty($this->input->post(null, true)))
         {
         if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
              $editrecID = ''; $recpayTerm='';
             $editrecID  = $this->czsecurity->xssCleanPostInput('editrecID');
             
             $customerID = $this->czsecurity->xssCleanPostInput('recCustomer');
              $cardID  = $this->czsecurity->xssCleanPostInput('rec_cardID');
              $payterm ='';$gateway='0';
              	$gt_data = $this->general_model->get_select_data('tbl_merchant_gateway',array('gatewayID'),array('merchantID'=>$user_id,'set_as_Default'=>1)); 	
         if(!empty($gt_data))
             $gateway    = $gt_data['gatewayID'];
             $recurAuto   = $this->czsecurity->xssCleanPostInput('recurAuto');
             if(!empty($this->czsecurity->xssCleanPostInput('rec_payTem')))
              $payterm = implode(',',$this->czsecurity->xssCleanPostInput('rec_payTem'));
               $recurAuto = $this->czsecurity->xssCleanPostInput('recurAuto');
               
               $amount =  sprintf('%0.2f', $this->czsecurity->xssCleanPostInput('recAmount'));
               $card = array('cardID'=>$cardID, 'amount'=>$amount, 'gateway'=>$gateway, 'customerID'=>$customerID,'merchantID'=>$user_id, 'optionData'=>$recurAuto, 'paymentTerm'=>$payterm );
				$card['recurring_send_mail'] = $chh_mail;
				if($recurAuto == 4){
					$card['month_day'] = $this->czsecurity->xssCleanPostInput('calendar_date_day');
				}
         
               if($editrecID!="")
               {
                  $card['updatedAt']  = date('Y-m-d H:i:s');
                  $con =array('recurrID'=>$editrecID);
                  $this->general_model->update_row_data('tbl_recurring_payment',$con, $card);
               }
               else
               {
                   $card['createdAt']  = date('Y-m-d H:i:s');
                   $card['updatedAt']  = date('Y-m-d H:i:s');
                   $this->general_model->insert_row('tbl_recurring_payment', $card);
                   
               }
               
               
               redirect('QBO_controllers/home/view_customer/'.$customerID, 'refresh');
             
         }
         
     }
    
    
     public function delete_recurring()
    {
		                if($this->session->userdata('logged_in') )
				    	{
			               $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') )
			        	{
			               $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
		     if($this->uri->segment('4')!="")
             {
		
		
		
			              $recurringID =  $this->uri->segment('4'); 
			              
			            
			        	$qq= $this->general_model->get_select_data('tbl_recurring_payment', array('customerID'), array('recurrID'=>$recurringID,'merchantID'=>$merchID));
					 	if(!empty($qq))
					 	{
					 	    $customer=$qq['customerID'];
					 	}
					
		       $sts =  $this->general_model->delete_row_data('tbl_recurring_payment',array('recurrID'=>$recurringID,'merchantID'=>$merchID));
				 if($sts){
		    $this->session->set_flashdata('success','Successfully Deleted'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
		 }
		 
	    	redirect('QBO_controllers/home/view_customer/'.$customer,'refresh');
        }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
		 }      
       	redirect('QBO_controllers/home/index','refresh');      
             
	}
	
		 public function create_new_plan()
	{
		
			  if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}
				
				$base_id = base64_encode($user_id);
	       	$coditionp=array('merchantID'=>$user_id,'customerPortal'=>'1');
		
		$urp_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortal'),$coditionp); 	
			       
			    if(empty($urp_data))
        		 {
        		     	$this->session->set_flashdata('error','<strong>Errr: Please enable your customer portal</strong>'); 
        		    	redirect('QBO_controllers/SettingConfig/setting_customer_portal','refresh');   
        		 } 
			    
		
     
     
     	      $ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 
     	      
     	     
        		$data['ur_data'] = $ur_data;
                  $position1=$position=0;    $purl='';
     
                    $ttt = explode(PLINK,$ur_data['customerPortalURL']);
                 
                    $purl = $ttt[0].PLINK.'/customer/';
			    	
                $purl = $purl.'qbo_check_out/'.$base_id.'/';
              
  			       $data['dpurl']=$purl;
	
	    
	    	     $inv_pre_data =$this->general_model->get_row_data('tbl_merchant_invoices',array('merchantID'=>$user_id));
        		 if(empty($inv_pre_data))
        		 {
        		     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Errr: Please set your invoice prefix to create invoice</strong></div>'); 
        		    	redirect('QBO_controllers/SettingConfig/profile_setting','refresh');   
        		 }
	    
		if(!empty($this->input->post(null, true)))
		{
			 $this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');						
			$this->form_validation->set_rules('plan_name', 'Plan Name', 'required');
			$this->form_validation->set_rules('gateway_list', 'Payment Gateway', 'required');
        	  $rowID='';
   				 if($this->czsecurity->xssCleanPostInput('planID') !="") 
				 $rowID = $this->czsecurity->xssCleanPostInput('planID');
     	if ($this->form_validation->run() == true)
		{
        
				$total=0;	
			    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod){
                       $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
					   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
					   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					   $insert_row['itemTax'] =$this->czsecurity->xssCleanPostInput('tax_check')[$key];
					   $insert_row['itemFullName'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					   if($this->czsecurity->xssCleanPostInput('onetime_charge')[$key]){
						   
						  $insert_row['oneTimeCharge'] ='1';  
					   }else{
						 $insert_row['oneTimeCharge'] ='0';  	
                         $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];	
					   } 
					   
					   
					 
					   $item_val[$key] =$insert_row;
                }				
				
			    $pname      = $this->czsecurity->xssCleanPostInput('plan_name');
    				
				$plan        = $this->czsecurity->xssCleanPostInput('duration_list');		
				if($plan > 0){
		        	$subsamount  = $total/$plan ;	
				}else{ 
				   	$subsamount  = $total ;
				}    
				$first_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));		
				$invoice_date = date('d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));		
				$st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));			
				$freetrial    = $this->czsecurity->xssCleanPostInput('freetrial');
				$paygateway    = $this->czsecurity->xssCleanPostInput('gateway_list');
				$address1     = $this->czsecurity->xssCleanPostInput('address1');
				$address2     = $this->czsecurity->xssCleanPostInput('address2');				
				$country      = $this->czsecurity->xssCleanPostInput('country');
				$state        = $this->czsecurity->xssCleanPostInput('state');
				$city	      = $this->czsecurity->xssCleanPostInput('city');
				$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');
				$phone        = $this->czsecurity->xssCleanPostInput('phone');
				$paycycle     = $this->czsecurity->xssCleanPostInput('paycycle');
				$shoturl     = $this->czsecurity->xssCleanPostInput('short_url');
			 $confirm_page_url = $this->czsecurity->xssCleanPostInput('confirm_page_url');
                $subdata =array(
				  'planName'    => $pname,
				   'subscriptionPlan'   =>$plan,
				   'subscriptionAmount' =>$total,
					 'paymentGateway'    => $paygateway,
					'totalInvoice'      => $plan,
					'invoicefrequency'  => $paycycle,
					'freeTrial'			=> $freetrial,
				    'postPlanURL' => $shoturl,
					  'confirm_page_url' => $confirm_page_url, 
					'merchantPlanURL' => $purl
				
				);  
				
				if($this->czsecurity->xssCleanPostInput('plan_url')!='')
				{
			    	$plan_url = $this->czsecurity->xssCleanPostInput('plan_url');
			    	
			    	
			    	
			    	$ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 	
			       
			    	if(!empty($ur_data))
			    	{
					    $subdata['merchantPlanURL'] =$purl.$plan_url;
						$subdata['postPlanURL'] = $plan_url;
			    	}else{
			    	   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Please create merchant portak URL</div>');  
			    	   		redirect('QBO_controllers/SettingPlan/plans','refresh');
			    	}		
				}else{
				    
				    
					$subdata['postPlanURL'] = $shoturl;
					$subdata['merchantPlanURL'] = $purl.$shoturl;
					
				}
        
        			if($paycycle=='mon')
				{
				     if($this->czsecurity->xssCleanPostInput('pro_rate'))
				     {
				         	$subdata['proRate'] ='1';   
				         	
				         	$subdata['proRateBillingDay']    =$this->czsecurity->xssCleanPostInput('pro_billing_date');   
				         	$subdata['nextMonthInvoiceDate'] =$this->czsecurity->xssCleanPostInput('pro_next_billing_date');   
				         
				     }
				    
				}
				
				if($this->czsecurity->xssCleanPostInput('autopay')){
				
					$subdata['automaticPayment'] ='1';
				}else{
					$subdata['automaticPayment'] ='0';	
				}
				
				if($this->czsecurity->xssCleanPostInput('email_recurring')){
					
					$subdata['emailRecurring'] ='1';
				}else{
					$subdata['emailRecurring'] ='0';	
				}
				
				if($this->czsecurity->xssCleanPostInput('require_shipping')){
				    $subdata['isShipping'] ='1';
				}
				else
				{
				    $subdata['isShipping'] ='0';
				}

				if($this->czsecurity->xssCleanPostInput('require_billing')){
				    $subdata['isBilling'] ='1';
				}
				else
				{
				    $subdata['isBilling'] ='0';
				}
				
        		if($this->czsecurity->xssCleanPostInput('require_service')){
				    $subdata['isService'] ='1';
				}
				else
				{
				    $subdata['isService'] ='0';
				}     
        
                
				if($this->czsecurity->xssCleanPostInput('billinfo')){
					
					$subdata['usingExistingAddress'] ='1';
				}else{
					$subdata['usingExistingAddress'] ='0';	
				}
			
				if($this->czsecurity->xssCleanPostInput('planID') !="") {
				 $rowID = $this->czsecurity->xssCleanPostInput('planID');
				 $subdata['updatedAt']  = date('Y-m-d H:i:s');
				 				 
				     $subs =  $this->general_model->get_row_data('tbl_subscriptions_plan_qbo',array('planID'=>$rowID));
				     	 
				 $ins_data = $this->general_model->update_row_data('tbl_subscriptions_plan_qbo',array('planID'=>$rowID) ,$subdata);	
				
   				$this->general_model->delete_row_data('tbl_subscription_plan_item_qbo', array('planID'=>$rowID));
				   
				    foreach($item_val as $k=>$item){
						$item['planID'] = $rowID;
						$ins = $this->general_model->insert_row('tbl_subscription_plan_item_qbo',$item);	
				    } 	
					
				}else{
				    $subdata['merchantDataID'] = $user_id;
                    $subdata['createdAt']  = date('Y-m-d H:i:s');
                    
					$rowID = $ins_data = $this->general_model->insert_row('tbl_subscriptions_plan_qbo',$subdata);	
					
					foreach($item_val as $k=>$item){
						$item['planID'] = $ins_data;
						$ins = $this->general_model->insert_row('tbl_subscription_plan_item_qbo',$item);	
				    } 		
					
				}
					
				 if($ins_data && $ins){
					
			        $this->session->set_flashdata('success','Success'); 
				 }else{
					 
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }
				
					redirect('QBO_controllers/SettingPlan/create_new_plan/'.$rowID,'refresh');
				
        }
       else{
		    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Validation Error</strong></div>'); 
		    		redirect('QBO_controllers/SettingPlan/create_new_plan/'.$rowID,'refresh');
		}			
		     
        }
			  
			  if($this->uri->segment('4')!="")
			  {
				$sbID = $this->uri->segment('4');
				$data['subs']		= $this->general_model->get_row_data('tbl_subscriptions_plan_qbo',array('planID'=>$sbID));
			
				$data['items']      = $this->general_model->get_table_data('tbl_subscription_plan_item_qbo',array('planID'=>$sbID)); 

			  }
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				
				$condition				= array('merchantID'=>$user_id );
  			    $conditionPlan 			= array('merchantID'=>$user_id,'IsActive'=>'true' );
					$data['base_id'] = base64_encode($user_id);
			    $data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
				$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy','' );
				$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan','' );
		    	$data['plans'] = $this->general_model->get_table_data('QBO_test_item',$conditionPlan);
				$compdata				= $this->qbo_customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
			
				
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/create_new_plan', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


}   
	
 
	
}

