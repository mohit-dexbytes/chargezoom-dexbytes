<?php

/**
 * This Controller has Stripe Payment Gateway Process
 * 
 * Create_customer_sale for Sale process : here we use the payment token
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */
error_reporting(0);
error_reporting(E_ERROR | E_PARSE);

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;
class CyberSource extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
   	public function __construct()
	{
		parent::__construct();
		
		
        $this->load->config('cyber_pay');
    	$this->load->model('quickbooks');
        $this->load->model('QBO_models/qbo_customer_model','qbo_customer_model');
		$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_company_model','qbo_company_model');
		$this->load->model('card_model');
		$this->load->library('form_validation');
		 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='1' )
		  {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	
	public function index()
	{
	    
    	redirect('QBO_controllers/home/index','refresh');
	    
	}
	
	
   	public function pay_invoice()
	{
		 $this->session->unset_userdata("receipt_data");
		 $this->session->unset_userdata("invoice_IDs");
		 $this->session->unset_userdata("in_data");
	      if($this->session->userdata('logged_in'))
	      {
			    $user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
		
			    $user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
               
                $this->form_validation->set_rules('contact', 'Phone Number', 'trim|required|xss-clean');
            }
            
                 $cusproID=''; $error='';
		    	 $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    $custom_data_fields = [];	
			$checkPlan = check_free_plan_transactions();
              
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		       
		          $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;	
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			     $in_data              =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			    if(!empty( $in_data)) 
			    {
			          
    			        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			        $customerID =  $in_data['Customer_ListID'];
    				  	$c_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID', 'firstName', 'lastName',  'companyName','userEmail', 'phoneNumber','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
    			     	$companyID = $c_data['companyID'];
    			        $gmerchantID    = $gt_result['gatewayUsername'];
    			        $secretApiKey   = $gt_result['gatewayPassword'];
						$secretKey      = $gt_result['gatewaySignature'];
    			     
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			       
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					$phone    = ($card_data['Billing_Contact'])?$card_data['Billing_Contact']:$c_data['phoneNumber'];
    					
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }                              
         
             		$cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;

    				if( $in_data['BalanceRemaining'] > 0)
    				{
        				    
                    	$crtxnID='';  
                        $flag="true";
                        $error=$user='';
                        $user = $in_data['qbwc_username'];
        				$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "test_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $c_data['firstName'],
                    		"lastName" => $c_data['lastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => $phone,
                    		"company" => $c_data['companyName'],
                    		"email" => $c_data['userEmail']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"expirationMonth" => $expmonth
							];
							
						$cvv = trim($cvv);
						if($cvv && !empty($cvv)){
							$paymentCardInfo['securityCode'] = $cvv;
						}
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                    	
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    	
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                    	
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  
        					  
        					  	$st='0'; $action='Pay Invoice'; $msg ="Payment Success "; 
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                                   
                          	 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
                         
                             $txnID      = $in_data['invoiceID'];  
							 $ispaid 	 = 1;
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 0;
							 $app_amount = $in_data['Total_payment']+$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('invoiceID'=>$in_data['invoiceID'],'merchantID'=>$user_id  );
						
							 $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
                          
                       $val = array(
							'merchantID' => $user_id,
						);
						$data = $this->general_model->get_row_data('QBO_token',$val);

						$accessToken  = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];
						$dataService  = DataService::Configure(array(
						 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
						));
		
        			    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        	            
        	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
        				$refNum=array();
        				
        				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        				{
        					$theInvoice = current($targetInvoiceArray);
        			
                				
                				$newPaymentObj = Payment::create([
                				    "TotalAmt" => $amount,
                				    "SyncToken" => 1,
                				    "CustomerRef" => $customerID,
                				    "Line" => [
                				     "LinkedTxn" =>[
                				            "TxnId" => $invoiceID,
                				            "TxnType" => "Invoice",
                				        ],    
                				       "Amount" => $amount
                			        ]
                				    ]);
                             
                				$savedPayment = $dataService->Add($newPaymentObj);
                				
                		    	$crtxnID=	$savedPayment->Id;
                				
                				$error = $dataService->getLastError();
                              
                			    if ($error != null) 
                			    {
                	
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                				$st='';
                
                				$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); 
                				
                				$qbID=$trID;
                				
                			   }
                    		   else 
                    		   {  $refNum[] =  $in_data['refNumber']; 
                    		       
                    		       	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
                    		           
                    		        $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
                    		
                    			}
                                      
                          
							}    
								$ref_number=implode(',',$refNum); 
								$condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								
								$tr_date   =date('Y-m-d H:i:s');
								$toEmail = $c_data['userEmail']; $company=$c_data['companyName']; $customer = $c_data['fullName'];  
        							  	
                             	
									 
							       $qbID=$invoiceID; 
        						
                    			         if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                    				 	$card_type      =$this->general_model->getType($card_no);
                    				        $friendlyname   =  $card_type.' - '.substr($card_no,-4);
                    						$card_condition = array(
                    										 'customerListID' =>$customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    					
                    						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
                    					     
                    					   
                    						if(!empty($crdata))
                    						{
                    							
                    						   $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	     =>$exyear,
                    										  'companyID'    =>$companyID,
                    										  'merchantID'   => $user_id,
                    										  'CardType'     =>$this->general_model->getType($card_no),
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'',
                    										  'updatedAt'    => date("Y-m-d H:i:s"),
                    										   'Billing_Addr1'	 =>$address1,
                        										 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										  );
                    										  
                    							     		  
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					     	$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'     =>$this->general_model->getType($card_no),
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        										 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->insert_card_data($card_data);	
                    				            
                    						
                    						}
                    				
                    				 }
        					    
        					 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
                    		
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					       
        					      $error = $api_response[0]['status'];
        					        $st='0'; $action='Pay Invoice'; $msg ="Payment Failed".$error; $qbID=$invoiceID;
        					        
        					      $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
        					          	 
 
                    		
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$invoiceID, false, $this->transactionByUser, $custom_data_fields);
								
								if($res['transactionCode'] == 200 && $chh_mail =='1')
								{
									$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $res['transactionId']);
								} 

                                if($error!='')
                                {
                                       $st='0'; $action='Pay Invoice'; $msg ="Payment Failed"; $qbID=$invoiceID;
                                }
                                $qbID=$invoiceID;
                                $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
                                
                  
                                $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
                            
                                if($syncid){
                                    $qbSyncID = 'CQ-'.$syncid;


                                    $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                                    
                                   
                                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                }       
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    	    
        			}  
        			
                      
        				   
        				           
        		    }
        		    else        
                    {
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                        
                    }
                 
        		   
		    } 
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			   if($cusproID=="2"){
			 	 redirect('QBO_controllers/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" ){
			 
			 	redirect('QBO_controllers/Create_invoice/invoice_details_page/'.$in_data['invoiceID'],'refresh');
			 }
			 $trans_id = $api_response[0]['id'];
			 $invoice_IDs = array();
			 $receipt_data = array(
				 'proccess_url' => 'QBO_controllers/Create_invoice/Invoice_details',
				 'proccess_btn_text' => 'Process New Invoice',
				 'sub_header' => 'Sale',
				 'checkPlan'  => $checkPlan
			 );
			 
			 $this->session->set_userdata("receipt_data",$receipt_data);
			 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
			 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
	      
	}
	
	
	 
   
	public function create_customer_sale()
	{ 
	      
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	        if($this->session->userdata('logged_in'))
		    {
				$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
			
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
          
            }
            
           
			$checkPlan = check_free_plan_transactions();
	    	if ($this->form_validation->run() == true)
		    {
	            $custom_data_fields = [];
	            $applySurcharge = false;
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                	$applySurcharge = true;
                    $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
                }

                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                    $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
                }
                       if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			
        			if(!empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				
                        $comp_data     =$this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                           
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        		           
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						 
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        					   
        				}
        				else 
        				{     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
        							
        				}
        				/*Added card type in transaction table*/
		                $cardType = $this->general_model->getType($card_no);
		                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
		                $custom_data_fields['payment_type'] = $friendlyname;

							error_reporting(1);
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								// update amount with surcharge 
				                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
				                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
				                    $amount += round($surchargeAmount, 2);
				                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
				                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
				                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
				                    
				                }
				                $totalamount  = $amount;
								$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                      $contact=  $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');
                                 
						
                            $crtxnID='';  
                            $flag="true";
                            $error=$user='';
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        						
        						
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        			
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" =>  $comp_data['companyName']
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}

        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        			
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];

                        $invoiceDetail = [];

                        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id')) || !empty($this->czsecurity->xssCleanPostInput('po_number'))) {
                            $new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
                            $invoiceDetailArr = [
                                'invoiceNumber' => $new_invoice_number,
                                'purchaseOrderNumber' => $this->czsecurity->xssCleanPostInput('po_number')
                            ];
                            $invoiceDetail = new CyberSource\Model\Ptsv2paymentsOrderInformationInvoiceDetails($invoiceDetailArr);
                        }

                        if($invoiceDetail){
                            $orderInfoArr['invoiceDetails'] = $invoiceDetail; 
                        }
                        
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"expirationMonth" => $expmonth
						];
						
						$cvv = trim($cvv);
						if($cvv && !empty($cvv)){
							$paymentCardInfo['securityCode'] = $cvv;
						}
						
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					

        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{


        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 /* This block is created for saving Card info in encrypted form  */
								 $this->session->set_flashdata('success', 'Transaction Successful');
        				         
							$invoiceIDs=array();      
							$invoicePayAmounts = array();
							if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
							{
								$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
								$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
							}
				     
				         
				        
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			            $refNumber =array();
                 
				           if(!empty($invoiceIDs))
				           {
                                $payIndex = 0;
                                $saleAmountRemaining = $amount;
                                foreach ($invoiceIDs as $inID) {
                                    $theInvoice = array();
                                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                                    $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                                    $con = array('invoiceID' => $inID, 'merchantID' => $user_id);
                                    $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                                    
                                    $refNumber[]   =  $res1['refNumber'];
                                    $txnID      = $inID;
                                    $pay_amounts = 0;


                                    $amount_data = $res1['BalanceRemaining'];
                                    $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                    if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                        $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                        $actualInvoicePayAmount += $surchargeAmount;
                                        $amount_data += $surchargeAmount;
                                        $updatedInvoiceData = [
                                            'inID' => $inID,
                                            'targetInvoiceArray' => $targetInvoiceArray,
                                            'merchantID' => $user_id,
                                            'dataService' => $dataService,
                                            'realmID' => $realmID,
                                            'accessToken' => $accessToken,
                                            'refreshToken' => $refreshToken,
                                            'amount' => $surchargeAmount,
                                        ];
                                        $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                                    }

                                    $ispaid      = 0;
                                    $isRun = 0;
                                    if($saleAmountRemaining > 0){
                                        $BalanceRemaining = 0.00;
                                        if($amount_data == $actualInvoicePayAmount){
                                            $actualInvoicePayAmount = $amount_data;
                                            $isPaid      = 1;

                                        }else{

                                            $actualInvoicePayAmount = $actualInvoicePayAmount;
                                            $isPaid      = 0;
                                            $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                            
                                        }
                                        $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

                                        $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

                                        
                                        $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                        if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                            $theInvoice = current($targetInvoiceArray);

                                            $newPaymentObj = Payment::create([
                                                "TotalAmt" => $actualInvoicePayAmount,
                                                "SyncToken" => 1,
                                                "CustomerRef" => $customerID,
                                                "Line" => [
                                                    "LinkedTxn" => [
                                                        "TxnId" => $inID,
                                                        "TxnType" => "Invoice",
                                                    ],
                                                    "Amount" => $actualInvoicePayAmount
                                                ]
                                            ]);

                                            $savedPayment = $dataService->Add($newPaymentObj);

                                            $error = $dataService->getLastError();
                                            if ($error != null) {
                                                $err = '';
                                                $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                                $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                                $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                                $st = '0';
                                                $action = 'Pay Invoice';
                                                $msg = "Payment Success but " . $error->getResponseBody();
                                                $qbID = $trID;
                                                $pinv_id = '';
                                            } else {
                                                $pinv_id = '';
                                                $crtxnID = $pinv_id = $savedPayment->Id;
                                                $st = '1';
                                                $action = 'Pay Invoice';
                                                $msg = "Payment Success";
                                                $qbID = $trID;
                                            }
                                            $qbID = $inID;
                                            $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                            $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                            if($syncid){
                                                $qbSyncID = 'CQ-'.$syncid;

                                                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                            }
                                            
                                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$actualInvoicePayAmount,$user_id,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);
                                        }
                                    }
                                    $payIndex++;

                                }
						 
				            }
				          else
				          {
				              
				                    $crtxnID='';
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
				          }
				          
				          
				          $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number = implode(',',$refNumber); 
							  $tr_date   =date('Y-m-d H:i:s');
							  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
							 
				          	if($chh_mail =='1')
							 {
							   
							  
							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $res['transactionId']);
							 } 
            				       
				     
            				       
            			         if( $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
            				 		
            				       $card_type      =$this->general_model->getType($card_no);
                    			   $friendlyname   =  $card_type.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            							
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            				               			
            					 $this->session->set_flashdata('success', 'Transaction Successful');
 
 
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                                     
                              }
                         
                         
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
					}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => $api_response[0]['id'],
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'  =>  $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh'); 
	    
	    
    	}
	
	public function create_customer_auth()
	{ 
	   
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	    
	        if($this->session->userdata('logged_in'))
		    {
				$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	    			
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
		
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
          
            }
            
           
			$checkPlan = check_free_plan_transactions();
			$custom_data_fields = [];
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		    	$po_number = $this->czsecurity->xssCleanPostInput('po_number');
	            if (!empty($po_number)) {
	                $custom_data_fields['po_number'] = $po_number;
	            }
	   
                       if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			  
        			if(!empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
                        $comp_data     =$this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('ListID'=>$customerID, 'merchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        		           $cardID = $this->czsecurity->xssCleanPostInput('card_list');
        				   $cvv='';	
						   if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
						   {	
									$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
									$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
									
									$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
									if($this->czsecurity->xssCleanPostInput('cvv')!="")
									$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
							}
							else 
							{     
										$card_data= $this->card_model->get_single_card_data($cardID);
										$card_no  = $card_data['CardNo'];
										$expmonth =  $card_data['cardMonth'];
										$exyear   = $card_data['cardYear'];
										if($card_data['CardCVV'])
										$cvv      = $card_data['CardCVV'];
										$cardType = $card_data['CardType'];
										
							}
							$cardType = $this->general_model->getType($card_no);
			                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
			                $custom_data_fields['payment_type'] = $friendlyname;
	                
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								$address1 =  $this->czsecurity->xssCleanPostInput('address');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                        $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');

						
                        $crtxnID='';  
                        $flag='false';
                        $error=$user='';
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" => "test_payment"
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$invoiceDetail = [];

                        if (!empty($this->czsecurity->xssCleanPostInput('invoice_number')) || !empty($po_number)) {
                            $new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_number');
                            $invoiceDetailArr = [
                                'invoiceNumber' => $new_invoice_number,
                                'purchaseOrderNumber' => $po_number
                            ];
                            $invoiceDetail = new CyberSource\Model\Ptsv2paymentsOrderInformationInvoiceDetails($invoiceDetailArr);
                        }

                        if($invoiceDetail){
                            $orderInfoArr['invoiceDetails'] = $invoiceDetail; 
                        }
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"expirationMonth" => $expmonth
						];
						
						$cvv = trim($cvv);
						if($cvv && !empty($cvv)){
							$paymentCardInfo['securityCode'] = $cvv;
						}

        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Auth'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        				
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					    $trID =   $api_response[0]['id'];
        					    $msg  =   $api_response[0]['status'];
        					  
        					    $code =   '200';
        					    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
								$transactiondata= array();
								$inID='';
							 
							 
            				       
            			           if( $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				       {
            				 		
									$card_type      =$this->general_model->getType($card_no);
                    				$friendlyname   =  $card_type.' - '.substr($card_no,-4);
										$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						    $crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
										if(!empty($crdata))
										{
            							
            						        $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
										   $this->card_model->update_card_data($card_condition, $card_data);				 
										}
										else
										{
											$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
											$id1 =    $this->card_model->insert_card_data($card_data);	
											
										
										}
            							$custom_data_fields['card_type'] = $cardType;
									}
            				   
            			
            				 $this->session->set_flashdata('success', 'Transaction Successful');
 
 
 
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                       
                              }
                        
                             $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields); 
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
					}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
					$invoice_IDs = array();
					
					$receipt_data = array(
						'transaction_id' => $api_response[0]['id'],
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_auth',
						'proccess_btn_text' => 'Process New Transaction',
						'sub_header' => 'Authorize',
						'checkPlan'  =>  $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
	       
	    
    	}
	
	
		
    		
	public function create_customer_capture()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('strtxnID', 'Transaction ID', 'required|xss_clean');
			
             
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
				
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
                	$comp_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName', 'userEmail','fullName'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
             	
				 
    			            	$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                            $config    = $commonElement->ConnectionHost();
                        	$merchantConfig = $commonElement->merchantConfigObject();
                        	$apiclient    = new CyberSource\ApiClient($config, $merchantConfig);
                        	$api_instance = new CyberSource\Api\CaptureApi($apiclient);
                                
                                 $cliRefInfoArr = [
                    	    	"code" => $this->mName
                    	        ];
				 
                    		   $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                  $amountDetailsArr = [
                                      "totalAmount" => $amount,
                                      "currency" => CURRENCY
                                  ];
                                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                                
                                  $orderInfoArry = [
                                    "amountDetails" => $amountDetInfo
                                  ];
                                
                                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                                  $requestArr = [
                                    "clientReferenceInformation" => $client_reference_information,
                                    "orderInformation" => $order_information
                                  ];
                                  //Creating model
                                  $request = new CyberSource\Model\CapturePaymentRequest($requestArr);
                                  $api_response = list($response,$statusCode,$httpHeader)=null;
                 
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->capturePayment($request, $tID);  
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  	$condition = array('transactionID'=>$tID);
            					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
								$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
								$condition = array('transactionID'=>$tID);
                                 
								
								  $tr_date   =date('Y-m-d H:i:s');
								  $ref_number =  $tID;
								  $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];
        					  	if($chh_mail =='1')
                                {
                                     
                                    $this->general_model->send_mail_voidcapture_data($user_id,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                                
								}
								
        					    
        					    	 $this->session->set_flashdata('success', 'Successfully Cpatured Transaction');
 
 
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
        					      
        				         $this->session->set_flashdata('success', 'Payment '.$error);
 
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                         
                         
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
                  
				  
				  $invoice_IDs = array();
				  
			  
				  $receipt_data = array(
					  'proccess_url' => 'QBO_controllers/Payments/payment_capture',
					  'proccess_btn_text' => 'Process New Transaction',
					  'sub_header' => 'Capture',
				  );
				  
				  $this->session->set_userdata("receipt_data",$receipt_data);
				  $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				  
				  if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($api_response[0]['id'] == ''){
						$api_response[0]['id'] ='null';
					}
				  redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$api_response[0]['id'],  'refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('QBO_controllers/Payments/payment_capture','refresh');
			
	}
	
	
	
	
		
	public function pay_multi_invoice()
	{
	
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
		
			$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
        
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                            
			$checkPlan = check_free_plan_transactions();
             $cusproID=''; $error='';
             $custom_data_fields = [];
		     $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		  
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		        
		    	 $cardID_upd ='';
			     
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
			     $amount               = $this->czsecurity->xssCleanPostInput('totalPay');
			     $customerID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
			     $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
			       $invoiceIDs =implode(',', $invoices);
            
			   $inv_data   =    $this->qbo_customer_model->get_due_invoice_data($invoices,$user_id);
		  
			    if(!empty( $invoices) && !empty($inv_data)) 
			    {
                   $cusproID=  $customerID  = $inv_data['CustomerListID']; 
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  	$comp_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','firstName', 'companyName','lastName','userEmail','phoneNumber'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
            			$companyID = $comp_data['companyID'];
    			     $secretApiKey   = $gt_result['gatewayPassword'];
    			     //	  
    			     if($cardID!="new1")
    			     {
    			         
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['cardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$phone    = $card_data['Billing_Contact'];
    					$country  = $card_data['Billing_Country'];
    					 
    			     }
    			     else
    			     { 
    			         
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }  
    			     $cardType = $this->general_model->getType($card_no);
	                 $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                 $custom_data_fields['payment_type'] = $friendlyname;
	                 
    			     
    			      
        		$crtxnID=''; $flag='true';	     
        	
                  $option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "test_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $comp_data['firstName'],
                    		"lastName" => $comp_data['lastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => ($phone)?$phone:$comp_data['phoneNumber'],
                    		"company" => $comp_data['companyName'],
                    		"email" => $comp_data['userEmail']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"expirationMonth" => $expmonth
							];
							
						$cvv = trim($cvv);
						if($cvv && !empty($cvv)){
							$paymentCardInfo['securityCode'] = $cvv;
						}

                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                    	
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                    	
                    
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    	
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                             
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
                          $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					   
                        $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                        
                           $this->session->set_flashdata('success', 'Successfully Processed Invoice');
            		
            			
            			
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			            
                 
				           if(!empty($invoices))
				           {
				               
				              foreach($invoices as $invoiceID)
				              {
        				                $theInvoice = array();
        							  
        						       
        							   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '".$invoiceID."'");
        							    $condition  = array('invoiceID'=>$invoiceID,'merchantID'=>$user_id );
        						            $in_data =$this->general_model->get_select_data('QBO_test_invoice',array('BalanceRemaining','Total_payment'),$condition);
        								     $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                             $txnID       = $invoiceID;  
            						            $ispaid   = 1;
            							
            						    	$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 0;
            							 $app_amount = $in_data['Total_payment']+$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $to_amount = $in_data['BalanceRemaining']+$in_data['Total_payment'];
            							 
        							    $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
        							 		
        								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        								{
        									$theInvoice = current($targetInvoiceArray);
        								
            								
            								
            								$newPaymentObj = Payment::create([
            									"TotalAmt" => $pay_amounts,
            									"SyncToken" => 1,
            									"CustomerRef" => $cusproID,
            									"Line" => [
            									 "LinkedTxn" =>[
            											"TxnId" => $invoiceID,
            											"TxnType" => "Invoice",
            										],    
            									   "Amount" => $pay_amounts
            									]
            									]);
            							 
            								$savedPayment = $dataService->Add($newPaymentObj);
        								
        								
        								
            								$error = $dataService->getLastError();
            								if ($error != null)
            						   	{
            							    $err='';
            					        	$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
            									$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
            								$err.="The Response message is: " . $error->getResponseBody() . "\n";
            								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>'.	$err.'</div>');
            
            							$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody();
            							
            							$qbID=$trID;	
            								$pinv_id='';
            							}
            							else 
            							{ 
            							      $pinv_id='';
            							       $pinv_id        =  $savedPayment->Id;
            							    	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
            							    	
            							    
            							     $this->session->set_flashdata('success', 'Transaction Successful');
                          
            							}
                                            $qbID = $invoiceID;
            					            $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				                            $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
                                            if($syncid){
                                                $qbSyncID = 'CQ-'.$syncid;

                                                

                                                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                                
                                                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                            }   
            							 $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$pay_amounts,$user_id,$pinv_id, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);     		
            							
        						}
				              }
						 
				            }
                           
                            
                             if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				 {
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
							 
							 
                     }
                     else
                     {
                         $crtxnID='';
                         
                               $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                         
                     }
                     
                     
                     
                   
                 } 
                	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
		    }
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			if(!$checkPlan){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
		
		    	 if($cusproID!="")
				 {
					 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
				 } 
	      
	}
	
	
	
	
	}  