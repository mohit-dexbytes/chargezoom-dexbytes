<?php
require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
class Webhook extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
	
		include APPPATH . 'third_party/nmiDirectPost.class.php';
	    include APPPATH . 'third_party/nmiCustomerVault.class.php';
	    include APPPATH . 'third_party/PayTraceAPINEW.php';
	    include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        
		$this->load->model('quickbooks');
		$this->load->model('card_model');
	 	$this->load->model('general_model');
	    $this->load->model('QBO_models/Qbo_company_model','qbo_company_model');
        $this->db1 = $this->load->database('otherdb', TRUE);
	}
	
	
	public function index(){
		$entityBody = file_get_contents('php://input');

		#Create Log
		$this->general_model->insert_row('qbo_webhook_log', [ 'request_data' => $entityBody ]);
		
		$entityData = json_decode($entityBody, true);
	    if(!empty($entityData)){
			$entityList = $entityData['eventNotifications'];

			foreach ($entityList as $key => $companyRequest) {
				$companyId = $companyRequest['realmId'];
				$dataChangeEvent = $companyRequest['dataChangeEvent']['entities'];

				$chk_condition = array('realmID' => $companyId);
				$tokenData = $this->general_model->get_row_data('QBO_token', $chk_condition);
				if(!empty($tokenData)){

					$accessToken = $tokenData['accessToken'];
					$refreshToken = $tokenData['refreshToken'];
					$realmID      = $tokenData['realmID'];
					$merchantID      = $tokenData['merchantID'];

					$dataService = DataService::Configure(array(
						'auth_mode' => $this->config->item('AuthMode'),
						'ClientID'  => $this->config->item('client_id'),
						'ClientSecret' => $this->config->item('client_secret'),
						'accessTokenKey' =>  $accessToken,
						'refreshTokenKey' => $refreshToken,
						'QBORealmID' => $realmID,
						'baseUrl' => $this->config->item('QBOURL'),
					));

					$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

					foreach ($dataChangeEvent as $request) {
						$event = $request['name'];
						$eventId = $request['id'];

						switch ($event) {
							case 'Item':
								$this->_syncItem($dataService, $tokenData, $eventId);
								break;

							case 'Customer':
								$this->_syncCustomer($dataService, $tokenData, $eventId);
								break;
							
							case 'Invoice':
								$this->_syncInvoice($dataService, $tokenData, $eventId);
								break;

							case 'Tax':
								# code...
								break;
							
							default:
								return false;
								break;
						}
					}
				}
			}
		}
	}

	private function _syncInvoice($dataService, $tokenData, $invoiceId){
		$queryData = $dataService->Query("SELECT * FROM Invoice where id = '$invoiceId'");
		$merchantID = $tokenData['merchantID'];
		$realmID = $companyId = $tokenData['realmID'];

		if(!empty($queryData)) {
			$oneInvoice = $queryData[0];

			$QBO_invoice_details['invoiceID'] = $oneInvoice->Id;
			$QBO_invoice_details['refNumber'] = $oneInvoice->DocNumber;

			if ($oneInvoice->ShipAddr != "") {
				$QBO_invoice_details['ShipAddress_Addr1'] = ($oneInvoice->ShipAddr->Line1) ? $oneInvoice->ShipAddr->Line1 : '';
				$QBO_invoice_details['ShipAddress_Addr2'] = ($oneInvoice->ShipAddr->Line2) ? $oneInvoice->ShipAddr->Line2 : '';
				$QBO_invoice_details['ShipAddress_City'] = ($oneInvoice->ShipAddr->City) ? $oneInvoice->ShipAddr->City : '';
				$QBO_invoice_details['ShipAddress_State'] = ($oneInvoice->ShipAddr->CountrySubDivisionCode) ? $oneInvoice->ShipAddr->CountrySubDivisionCode : '';
				$QBO_invoice_details['ShipAddress_Country'] = ($oneInvoice->ShipAddr->Country) ? $oneInvoice->ShipAddr->Country : '';
				$QBO_invoice_details['ShipAddress_PostalCode'] = ($oneInvoice->ShipAddr->PostalCode) ? $oneInvoice->ShipAddr->PostalCode : '';
			}
			if ($oneInvoice->BillAddr != "") {
				$QBO_invoice_details['BillAddress_Addr1'] = ($oneInvoice->BillAddr->Line1) ? $oneInvoice->BillAddr->Line1 : '';
				$QBO_invoice_details['BillAddress_Addr2'] = ($oneInvoice->BillAddr->Line2) ? $oneInvoice->BillAddr->Line2 : '';
				$QBO_invoice_details['BillAddress_City'] = ($oneInvoice->BillAddr->City) ? $oneInvoice->BillAddr->City : '';
				$QBO_invoice_details['BillAddress_State'] = ($oneInvoice->BillAddr->CountrySubDivisionCode) ? $oneInvoice->BillAddr->CountrySubDivisionCode : '';
				$QBO_invoice_details['BillAddress_Country'] = ($oneInvoice->BillAddr->Country) ? $oneInvoice->BillAddr->Country : '';

				$QBO_invoice_details['BillAddress_PostalCode'] = ($oneInvoice->BillAddr->PostalCode) ? $oneInvoice->BillAddr->PostalCode : '';
			}

			$QBO_invoice_details['DueDate'] = $oneInvoice->DueDate;
			$QBO_invoice_details['BalanceRemaining'] = $oneInvoice->Balance;
			$QBO_invoice_details['Total_payment'] = $oneInvoice->TotalAmt;
			$QBO_invoice_details['CustomerListID'] = $oneInvoice->CustomerRef;

			if ($oneInvoice->PrivateNote != 'Voided'){
				$QBO_invoice_details['userStatus'] = 0;
				if ($oneInvoice->Balance == 0)
					$QBO_invoice_details['IsPaid'] = 1;
				else
					$QBO_invoice_details['IsPaid'] = 0;
			} else {
				$QBO_invoice_details['userStatus'] = 1;
			}

			$QBO_invoice_details['merchantID'] = $merchantID;
			$QBO_invoice_details['companyID'] = $realmID;
			$QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->CreateTime));
			$QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->LastUpdatedTime));

			if ($oneInvoice->TxnTaxDetail) {
				$taxref = $oneInvoice->TxnTaxDetail->TxnTaxCodeRef;
				if ($taxref) {
					$tax_data =    $this->general_model->get_row_data('tbl_taxe_code_qbo', array('taxID' => $taxref, 'merchantID' => $merchID));
					$QBO_invoice_details['taxRate'] = $tax_data['total_tax_rate'];
					$QBO_invoice_details['taxID'] = $taxref;
				} else {
					$QBO_invoice_details['taxRate'] = 0;
				}

				$QBO_invoice_details['totalTax'] = $oneInvoice->TxnTaxDetail->TotalTax;
			} else {
				$QBO_invoice_details['totalTax'] = 0.00;
			}

			if(!isset($QBO_invoice_details['taxRate']) || $QBO_invoice_details['taxRate'] === NULL) {
				$QBO_invoice_details['taxRate'] = 0;
			}

			if ($this->general_model->get_num_rows('QBO_test_invoice', array('companyID' => $realmID, 'invoiceID' => $oneInvoice->Id, 'merchantID' => $merchantID)) > 0) {

				$this->general_model->update_row_data('QBO_test_invoice', array('companyID' => $realmID, 'invoiceID' => $oneInvoice->Id, 'merchantID' => $merchantID), $QBO_invoice_details);

				$line_items = $oneInvoice->Line;

				$l_data = array();
				$k = 0;

				if (!empty($line_items)) {
					
					$this->general_model->delete_row_data('tbl_qbo_invoice_item', array('invoiceID' => $oneInvoice->Id, 'merchantID' => $merchantID, 'releamID' => $realmID));
					foreach ($line_items as $line) {
						$l_data = array();

						if (isset($line->Id)) {


							$l_data['itemID'] = $line->LineNum;

							if ($line->Description)
								$l_data['itemDescription'] = $line->Description;
							$l_data['totalAmount'] = ($line->Amount) ? $line->Amount : '0';

							$l_data['itemRefID'] = $line->SalesItemLineDetail->ItemRef;
							if ($line->SalesItemLineDetail->UnitPrice)
								$l_data['itemPrice'] = $line->SalesItemLineDetail->UnitPrice;
							else
								$l_data['itemPrice'] = 0.00;
							if ($line->SalesItemLineDetail->Qty)
								$l_data['itemQty'] = $line->SalesItemLineDetail->Qty;
							else
								$l_data['itemQty'] = 1;

							$l_data['invoiceID'] = $oneInvoice->Id;
							$l_data['merchantID'] = $merchantID;
							$l_data['releamID'] = $realmID;
							$l_data['createdAt'] = date('Y-m-d H:i:s');

							if ($line->SalesItemLineDetail->TaxCodeRef)
								$l_data['itemTax'] = ($line->SalesItemLineDetail->TaxCodeRef == "TAX") ? 1 : 0;
							else
								$l_data['itemTax'] = 0;

							$this->general_model->insert_row('tbl_qbo_invoice_item', $l_data);
						}
					}
				}
			} else {
				$line_items = $oneInvoice->Line;

				$l_data = array();
				$k = 0;

				if (!empty($line_items)) {
					
					$this->general_model->delete_row_data('tbl_qbo_invoice_item', array('invoiceID' => $oneInvoice->Id, 'merchantID' => $merchantID, 'releamID' => $realmID));
					foreach ($line_items as $line) {
						$l_data = array();
						if (isset($line->Id)) {

							$l_data['itemID'] = $line->LineNum;
							if ($line->Description)
								$l_data['itemDescription'] = $line->Description;
							$l_data['totalAmount'] = ($line->Amount) ? $line->Amount : '0';

							$l_data['itemRefID'] = $line->SalesItemLineDetail->ItemRef;
							if ($line->SalesItemLineDetail->UnitPrice)
								$l_data['itemPrice'] = $line->SalesItemLineDetail->UnitPrice;
							else
								$l_data['itemPrice'] = 0.00;
							if ($line->SalesItemLineDetail->Qty)
								$l_data['itemQty'] = $line->SalesItemLineDetail->Qty;
							else
								$l_data['itemQty'] = 1;

							$l_data['invoiceID'] = $oneInvoice->Id;
							$l_data['merchantID'] = $merchantID;
							$l_data['releamID'] = $realmID;
							$l_data['createdAt'] = date('Y-m-d H:i:s');

							if ($line->SalesItemLineDetail->TaxCodeRef)
								$l_data['itemTax'] = ($line->SalesItemLineDetail->TaxCodeRef == "TAX") ? 1 : 0;
							else
								$l_data['itemTax'] = 0;

							$this->general_model->insert_row('tbl_qbo_invoice_item', $l_data);
						}
					}
				}

				if ($oneInvoice->TxnTaxDetail) {
					$taxref = $oneInvoice->TxnTaxDetail->TxnTaxCodeRef;

					if ($taxref) {
						$tax_data =    $this->general_model->get_row_data('tbl_taxes_qbo', array('taxID' => $taxref, 'merchantID' => $merchantID));
						$QBO_invoice_details['taxRate'] = $tax_data['taxRate'];
					} else {
						$QBO_invoice_details['taxRate'] = 0;
					}

					$QBO_invoice_details['totalTax'] = $oneInvoice->TxnTaxDetail->TotalTax;
				} else {
					$QBO_invoice_details['totalTax'] = 0.00;
				}

				if($QBO_invoice_details['taxRate'] === NULL) {
					$QBO_invoice_details['taxRate'] = 0;
				}
				$this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
			}
		}
		return true;
	}

	private function _syncCustomer($dataService, $tokenData, $customerId){
		$queryData = $dataService->Query("SELECT * FROM Customer where   Id ='$customerId' ");
		$merchantID = $tokenData['merchantID'];
		$realmID = $companyId = $tokenData['realmID'];

		if(!empty($queryData)) {
			$oneCustomer = $queryData[0];
			$QBO_customer_details = [];
				
			$QBO_customer_details['Customer_ListID'] = $oneCustomer->Id;
			$QBO_customer_details['firstname'] = $oneCustomer->GivenName;
			$QBO_customer_details['lastname'] = $oneCustomer->FamilyName;
			$QBO_customer_details['fullname'] = $oneCustomer->DisplayName;
			$QBO_customer_details['userEmail'] = ($oneCustomer->PrimaryEmailAddr)?$oneCustomer->PrimaryEmailAddr->Address:'';
			$QBO_customer_details['phoneNumber'] = ($oneCustomer->PrimaryPhone)?$oneCustomer->PrimaryPhone->FreeFormNumber:'';
			
			$QBO_customer_details['address1'] = (isset($oneCustomer->BillAddr->Line1) && $oneCustomer->BillAddr->Line1)?$oneCustomer->BillAddr->Line1:'';
			$QBO_customer_details['address2'] = (isset($oneCustomer->BillAddr->Line2) && $oneCustomer->BillAddr->Line2)?$oneCustomer->BillAddr->Line2:'';
			$QBO_customer_details['zipCode'] = (isset($oneCustomer->BillAddr->PostalCode) && $oneCustomer->BillAddr->PostalCode)?$oneCustomer->BillAddr->PostalCode:'';
			$QBO_customer_details['Country'] = (isset($oneCustomer->BillAddr->Country) && $oneCustomer->BillAddr->Country)?$oneCustomer->BillAddr->Country:'';
			$QBO_customer_details['State'] = (isset($oneCustomer->BillAddr->CountrySubDivisionCode) && $oneCustomer->BillAddr->CountrySubDivisionCode)?$oneCustomer->BillAddr->CountrySubDivisionCode:'';
			$QBO_customer_details['City'] = (isset($oneCustomer->BillAddr->City) && $oneCustomer->BillAddr->City)?$oneCustomer->BillAddr->City:'';
			
			$QBO_customer_details['ship_address1'] = (isset($oneCustomer->ShipAddr->Line1) && $oneCustomer->ShipAddr->Line1)?$oneCustomer->ShipAddr->Line1:'';
			$QBO_customer_details['ship_address2'] = (isset($oneCustomer->ShipAddr->Line2) && $oneCustomer->ShipAddr->Line2)?$oneCustomer->ShipAddr->Line2:'';
			$QBO_customer_details['ship_zipcode'] = (isset($oneCustomer->ShipAddr->PostalCode) && $oneCustomer->ShipAddr->PostalCode)?$oneCustomer->ShipAddr->PostalCode:'';
			$QBO_customer_details['ship_country'] = (isset($oneCustomer->ShipAddr->Country) && $oneCustomer->ShipAddr->Country)?$oneCustomer->ShipAddr->Country:'';
			$QBO_customer_details['ship_state'] = (isset($oneCustomer->ShipAddr->CountrySubDivisionCode) && $oneCustomer->ShipAddr->CountrySubDivisionCode)?$oneCustomer->ShipAddr->CountrySubDivisionCode:'';
			$QBO_customer_details['ship_city'] = (isset($oneCustomer->ShipAddr->City) && $oneCustomer->ShipAddr->City)?$oneCustomer->ShipAddr->City:'';
			
			
			$QBO_customer_details['companyName'] = $oneCustomer->CompanyName;
			$QBO_customer_details['companyID'] = $realmID;
			$QBO_customer_details['createdAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->CreateTime));
			$QBO_customer_details['updatedAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->LastUpdatedTime));
			$QBO_customer_details['listID'] = '1';
			$QBO_customer_details['customerStatus'] = $oneCustomer->Active;;
			$QBO_customer_details['merchantID'] = $merchantID;
			
			if($this->general_model->get_num_rows('QBO_custom_customer',array('companyID'=>$realmID,'Customer_ListID'=>$oneCustomer->Id , 'merchantID'=>$merchantID)) > 0){  
				$this->general_model->update_row_data('QBO_custom_customer',array('companyID'=>$realmID, 'Customer_ListID'=>$oneCustomer->Id, 'merchantID'=>$merchantID), $QBO_customer_details);
			}else{
				$this->general_model->insert_row('QBO_custom_customer', $QBO_customer_details);
			}
		}
		return true;
	}

	private function _syncItem($dataService, $tokenData, $itemId){
		$queryData = $dataService->Query("SELECT * FROM Item where Id='$itemId'");
		$merchantID = $tokenData['merchantID'];
		$realmID = $companyId = $tokenData['realmID'];

		if(!empty($queryData)) {
			$oneItem = $queryData[0];

			$QBO_item_details['productID']         = $oneItem->Id;
			$QBO_item_details['IsActive']          = $oneItem->Active;
			$QBO_item_details['TimeCreated']       = date('Y-m-d H:i:s', strtotime($oneItem->MetaData->CreateTime));
			$QBO_item_details['TimeModified']      = date('Y-m-d H:i:s', strtotime($oneItem->MetaData->LastUpdatedTime));
			$QBO_item_details['Name']              = $oneItem->Name;
			$QBO_item_details['SKU']               = $oneItem->Sku;
			$QBO_item_details['SalesDescription']  = $oneItem->Description;
			$QBO_item_details['saleCost']          = $oneItem->UnitPrice;
			$QBO_item_details['Type']              = $oneItem->Type;
			$QBO_item_details['parent_ListID']     = $oneItem->ParentRef;
			$QBO_item_details['QuantityOnHand']    = $oneItem->QtyOnHand;
			$QBO_item_details['InvStartDate']      = $oneItem->InvStartDate;
			$QBO_item_details['AssetAccountRef']   = $oneItem->AssetAccountRef;
			$QBO_item_details['purchaseCost']      = $oneItem->PurchaseCost;
			$QBO_item_details['purchaseDesc']      = $oneItem->PurchaseDesc;
			$QBO_item_details['IncomeAccountRef']  = $oneItem->IncomeAccountRef;
			$QBO_item_details['ExpenseAccountRef'] = $oneItem->ExpenseAccountRef;
			$QBO_item_details['CompanyID']         = $realmID;
			$QBO_item_details['merchantID']        = $merchantID;

			if ($this->general_model->get_num_rows('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneItem->Id, 'merchantID' => $merchantID)) > 0) {
				$this->general_model->update_row_data('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneItem->Id, 'merchantID' => $merchantID), $QBO_item_details);
			} else {
				$this->general_model->insert_row('QBO_test_item', $QBO_item_details);
			}
		}
		return true;
	}
}