<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;

class QBO_cron extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->config('quickbooks_online');
        $this->load->model('general_model');
    }

    public function index()
    {
        $QBO_Token_data = $this->general_model->get_table_data('QBO_token', '');

        if (!empty($QBO_Token_data)) {
            foreach ($QBO_Token_data as $QBO_Token) {

                $ID = $QBO_Token['ID'];

                $refresh_token = $QBO_Token['refreshToken'];

                $access_token = $QBO_Token['accessToken'];
                $realmID      = $QBO_Token['realmID'];

                if($QBO_Token['adminID'] == 1){
                    $config_data  = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 1]);
                }else{
                    $config_data  = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 0]);
                }
                
                $grant_type = 'refresh_token';

                if (!empty($config_data)) {
                    foreach ($config_data as $configs) {

                        $authorizationRequestUrl = $configs['authorizationRequestUrl'];
                        $tokenEndPointUrl        = $configs['tokenEndPointUrl'];
                        $client_id               = $configs['client_id'];

                        $client_secret = $configs['client_secret'];

                    }

                }

                $client = new Client($client_id, $client_secret);

                $result = $client->refreshAccessToken($tokenEndPointUrl, $grant_type, $refresh_token);

                if (!empty($result)) {
                    if ($result['access_token'] != "") {
                        $QBO_Cron['accessToken']  = $result['access_token'];
                        $QBO_Cron['refreshToken'] = $result['refresh_token'];
                        $QBO_Cron['Validity']     = $result['expires_in'];
                        $QBO_Cron['updatedAT']    = date('Y-m-d H:i:s');
                        $condition                = array('ID' => $ID);
                        $this->general_model->update_row_data('QBO_token', $condition, $QBO_Cron);
                    }

                }

            }
        }
	}
	
	public function sync_deleted_invoices(){
		ini_set('max_execution_time', '3000');
		$merchantData = $this->general_model->get_table_data('QBO_token',[],['name' => 'merchantID', 'type' => 'asc']);

		if(!empty($merchantData)) {
			foreach ($merchantData as $merchant) {
				$dataService = DataService::Configure(array(
					'auth_mode' => $this->config->item('AuthMode'),
					'ClientID'  => $this->config->item('client_id'),
					'ClientSecret' => $this->config->item('client_secret'),
					'accessTokenKey' =>  $merchant['accessToken'],
					'refreshTokenKey' => $merchant['refreshToken'],
					'QBORealmID' => $merchant['realmID'],
					'baseUrl' => $this->config->item('QBOURL'),
				));

				$invoiceList = $this->general_model->get_table_select_data('QBO_test_invoice',['invoiceID'],['merchantID' => $merchant['merchantID'], 'isDeleted' => 0]);
				if(!empty($invoiceList)){
					$ids = '';
					$inv = array_column($invoiceList,'invoiceID');

					foreach($inv as $kk => $id) {
						$ids .= "'$id',";
					}

					$ids = substr($ids, 0, -1);

					$sql = "SELECT Id FROM Invoice WHERE Id IN ($ids)";
					$deletedEntities = $dataService->Query($sql);

					$newId = array_column($deletedEntities,'Id');
					
					$deleteId = [];
					foreach($inv as $kk => $id) {
						if(!in_array($id, $newId)){
							$deleteId[] = $id;
						}
					}

					if(!empty($deleteId)){
						$this->general_model->update_mul_row_data('QBO_test_invoice', array('invoiceID' => $deleteId), ['isDeleted' => 1]);
					}
				}
				
			}
		}
	}
}

class Client
{
    private $client_id;
    private $client_secret;

    /**
     * HTTP Methods
     */
    const HTTP_METHOD_GET    = 'GET';
    const HTTP_METHOD_POST   = 'POST';
    const HTTP_METHOD_PUT    = 'PUT';
    const HTTP_METHOD_DELETE = 'DELETE';
    const HTTP_METHOD_HEAD   = 'HEAD';
    const HTTP_METHOD_PATCH  = 'PATCH';

    public function __construct($client_id, $client_secret)
    {
        if (!extension_loaded('curl')) {
            throw new Exception('The PHP exention curl must be installed to use this library.', Exception::CURL_NOT_FOUND);
        }
        if (!isset($client_id) || !isset($client_secret)) {
            throw new Exception('The App key must be set.', Exception::InvalidArgumentException);
        }

        $this->client_id     = $client_id;
        $this->client_secret = $client_secret;

    }

    private function generateAccessTokenHeader($access_token)
    {
        $authorizationheader = 'Bearer ' . $access_token;
        return $authorizationheader;
    }

    public function getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state)
    {
        $parameters = array(
            'client_id'     => $this->client_id,
            'scope'         => $scope,
            'redirect_uri'  => $redirect_uri,
            'response_type' => $response_type,
            'state'         => $state,
            //The include_granted_scope is always set to false. No need to pass.
        );
        $authorizationRequestUrl .= '?' . http_build_query($parameters, null, '&', PHP_QUERY_RFC1738);
        return $authorizationRequestUrl;
    }

    public function getAccessToken($tokenEndPointUrl, $code, $redirectUrl, $grant_type)
    {
        if (!isset($grant_type)) {
            throw new InvalidArgumentException('The grant_type is mandatory.', InvalidArgumentException::INVALID_GRANT_TYPE);
        }

        $parameters = array(
            'grant_type'   => $grant_type,
            'code'         => $code,
            'redirect_uri' => $redirectUrl,
        );
        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );

        //Try catch???
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }

    public function refreshAccessToken($tokenEndPointUrl, $grant_type, $refresh_token)
    {
        $parameters = array(
            'grant_type'    => $grant_type,
            'refresh_token' => $refresh_token,
        );

        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }

    private function generateAuthorizationHeader()
    {
        $encodedClientIDClientSecrets = base64_encode($this->client_id . ':' . $this->client_secret);
        $authorizationheader          = 'Basic ' . $encodedClientIDClientSecrets;
        return $authorizationheader;
    }

    private function executeRequest($url, $parameters = array(), $http_header, $http_method)
    {

        $curl_options = array();

        switch ($http_method) {
            case self::HTTP_METHOD_GET:
                $curl_options[CURLOPT_HTTPGET] = 'true';
                if (is_array($parameters) && count($parameters) > 0) {
                    $url .= '?' . http_build_query($parameters);
                } elseif ($parameters) {
                    $url .= '?' . $parameters;
                }
                break;
            case self::HTTP_METHOD_POST:
                $curl_options[CURLOPT_POST] = '1';
                if (is_array($parameters) && count($parameters) > 0) {
                    $body                             = http_build_query($parameters);
                    $curl_options[CURLOPT_POSTFIELDS] = $body;
                }
                break;
            default:
                break;
        }

        if (is_array($http_header)) {
            $header = array();
            foreach ($http_header as $key => $value) {
                $header[] = "$key: $value";
            }
            $curl_options[CURLOPT_HTTPHEADER] = $header;
        }
        $curl_options[CURLOPT_SSL_VERIFYPEER] = 0;
        $curl_options[CURLOPT_URL] = $url;
        $ch                        = curl_init();

        curl_setopt_array($ch, $curl_options);

        //Don't display, save it on result
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Execute the Curl Request
        $result = curl_exec($ch);

        $headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        if ($curl_error = curl_error($ch)) {
            throw new Exception($curl_error);
        } else {
            $json_decode = json_decode($result, true);
        }
        curl_close($ch);

        return $json_decode;
    }
}
