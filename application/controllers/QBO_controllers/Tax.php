<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\TaxService;
use QuickBooksOnline\API\Facades\TaxRate;

class Tax extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_company_model');
		
	  if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	    
	
	   	redirect('QBO_controllers/home','refresh'); 
	}
	
	
	public function tax_list()
	{   
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if($this->session->userdata('logged_in') ){
			$data['login_info']  =$this->session->userdata('logged_in');
			$user_id = $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in') ){
			$data['login_info']  =$this->session->userdata('user_logged_in');
			$user_id = $this->session->userdata('user_logged_in')['merchantID'];
		}

		$merchID = $user_id;	 
		
		$val = array(
			'merchantID' => $merchID,
		);

		$condition =  array('merchantID'=>$merchID);
     	 
	    
		$data1 = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data1['accessToken'];
		$refreshToken = $data1['refreshToken'];
		$realmID      = $data1['realmID']; 
	
		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' =>$this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),
		));
		

		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			
		/***********************Gettting Agency Data*****************/

			
		$tagency = $dataService->Query("SELECT * FROM TaxAgency  ");  
		
		$err_msg='';
		$error = $dataService->getLastError();
		if ($error != null) 
		{
			$err_msg.="The Status code is: " . $error->getHttpStatusCode() . "\n";
			$err_msg.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err_msg.="The Response message is: " . $error->getResponseBody() . "\n";
			
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong>'.$err_msg.'</div>');   
			
		}
		else
		{
			if(!empty($tagency))
			{
				foreach ($tagency as $oneagen) 
				{
					$agcy_num = 	$this->general_model->get_num_rows('tbl_taxagency_qbo',array('agencyID'=>$oneagen->Id,'merchantID'=>$merchID,'releamID'=>$realmID));
					
					$QBO_tax_agency['agencyID'] = $oneagen->Id;
					$QBO_tax_agency['agencyName'] = $oneagen->DisplayName;
					$QBO_tax_agency['agencyDomain'] = $oneagen->domain;
					$QBO_tax_agency['createdAt'] = date('Y-m-d H:i:s',strtotime($oneagen->MetaData->CreateTime));
					$QBO_tax_agency['updatedAt'] = date('Y-m-d H:i:s',strtotime($oneagen->MetaData->LastUpdatedTime));
					$QBO_tax_agency['merchantID'] = $merchID;
						$QBO_tax_agency['releamID'] = $realmID;
						if($oneagen->TaxTrackedOnSales)
						{
						$QBO_tax_agency['trackOnSales'] = 1;
						}else{
							$QBO_tax_agency['trackOnSales'] = 0;  
						}
					if($agcy_num==0)
					$this->general_model->insert_row('tbl_taxagency_qbo', $QBO_tax_agency);
					else
						$this->general_model->update_row_data('tbl_taxagency_qbo',array('agencyID'=>$oneagen->Id,'merchantID'=>$merchID,'releamID'=>$realmID), $QBO_tax_agency);
					
				}
				
				#Create Logs for SYNC New Entry
				$qbo_log = array('syncType' => 'CQ','type' => 'tax','qbStatus' => 1, 'qbAction' => 'TaxDataImport', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
				$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
				if($syncid){
                    $qbSyncID = 'CQ-'.$syncid;
                    $qbSyncStatus = 'Tax Data Import Successfully.';
                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                }
			}		     
		}
			
			
		/************************End Tax Agency****************/	

		//Add a new Invoice
		$taxs = $dataService->Query("SELECT * FROM TaxRate where RateValue !='' ");
		$taxCode = $dataService->Query("SELECT * FROM TaxCode where Name !='NON'");
	
		$err_msg='';
		$error = $dataService->getLastError();
		if ($error != null) {
			$err_msg.="The Status code is: " . $error->getHttpStatusCode() . "\n";
			$err_msg.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err_msg.="The Response message is: " . $error->getResponseBody() . "\n";
		
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong>'.$err_msg.'</div>');   
		}

		if(isset($taxs) && !empty($taxs)) {
			foreach ($taxs as $onetax) 
			{
				$QBO_tax_details['taxID'] = $onetax->Id;
				$QBO_tax_details['friendlyName'] = $onetax->Name;
				$QBO_tax_details['taxRate'] = $onetax->RateValue;
				$QBO_tax_details['date'] = date('Y-m-d H:i:s',strtotime($onetax->MetaData->CreateTime));
				$QBO_tax_details['merchantID'] = $merchID;
				$QBO_tax_details['agencyID'] =  $onetax->AgencyRef;
			
				
				$ch_num = $this->general_model->get_num_rows('tbl_taxes_qbo',array('taxID'=>$onetax->Id, 'merchantID'=>$merchID));
				if($ch_num==0)
				$this->general_model->insert_row('tbl_taxes_qbo', $QBO_tax_details);
				else
				$this->general_model->update_row_data('tbl_taxes_qbo',array('taxID'=>$onetax->Id, 'merchantID'=>$merchID), $QBO_tax_details);
			}
		}

		if(isset($taxCode) && !empty($taxCode)) {
			$del1 = $this->general_model->delete_row_data('tbl_taxe_code_qbo', $condition);

			foreach ($taxCode as $taxref) {
				$totalTaxRate = 0;
				$taxIds= '';
				
				if($taxref->SalesTaxRateList == ''){
					$tax_num = 0;
				} else if(array_key_exists("TaxRateRef",$taxref->SalesTaxRateList->TaxRateDetail)){
					$taxIds = $tax_num = $taxref->SalesTaxRateList->TaxRateDetail->TaxRateRef;
					$keyT = array_search($tax_num, array_column($taxs, 'Id'));
					$taxData = $taxs[$keyT];
					$totalTaxRate = $taxData->RateValue;
				} else if(array_key_exists("TaxRateRef",$taxref->SalesTaxRateList->TaxRateDetail[0])){
					$tax_num = $taxref->SalesTaxRateList->TaxRateDetail[0]->TaxRateRef;

					$taxIds = [];
					$taxRateDetail = $taxref->SalesTaxRateList->TaxRateDetail;
					foreach ($taxRateDetail as $trData) {
						$keyT = array_search($trData->TaxRateRef, array_column($taxs, 'Id'));
						$taxData = $taxs[$keyT];
						$totalTaxRate += $taxData->RateValue;
						$taxRateRef = $trData->TaxRateRef;
						$taxIds[] = $taxRateRef;
					}
					$taxIds = implode(',',$taxIds);
				}
				
				$QBO_tax_ref['taxRef'] = $tax_num;
				$QBO_tax_ref['Name'] = $taxref->Name;
				$QBO_tax_ref['Description'] = $taxref->Description;
				$QBO_tax_ref['taxID'] = $taxref->Id;
				$QBO_tax_ref['merchantID'] = $merchID;
				
				$QBO_tax_ref['total_tax_rate'] = $totalTaxRate;
				$QBO_tax_ref['tax_ids'] = $taxIds;
				$QBO_tax_ref['is_active'] = (strtolower($taxref->Active) == 'true') ? 1 : 0;

				$ch_num1 = $this->general_model->get_num_rows('tbl_taxe_code_qbo',array('taxID'=>$taxref->Id, 'merchantID'=>$merchID));
				if($ch_num1==0)
				$this->general_model->insert_row('tbl_taxe_code_qbo', $QBO_tax_ref);
				else
					$this->general_model->update_row_data('tbl_taxe_code_qbo',array('taxID'=>$onetax->Id, 'merchantID'=>$merchID), $QBO_tax_ref);
				
				
			}
		}
		    
		$taxname = $this->qbo_company_model->get_qbo_tax_data($merchID);
		$data['taxes'] = $taxname;
		$taxagency= $this->general_model->get_table_data('tbl_taxagency_qbo',$condition);
		$data['agency'] = $taxagency;
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/taxes', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	

	 public function create_taxes()
 
	{
	  if(!empty($this->input->post(null, true))){
	 
	   $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('friendlyName', 'Friendly Name', 'required|xss_clean');
		$this->form_validation->set_rules('taxRate', 'Tax Rate', 'required|xss_clean');
		
		if ($this->form_validation->run() == true)
		{
		                if($this->session->userdata('logged_in') ){
	                    
			           $user_id = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			        	
			           $user_id = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
		  
			$merchID = $user_id;
			$val = array(
				'merchantID' => $merchID,
			);
			$condition =  array('merchantID'=>$merchID);

			

			$input_data['friendlyName']	   = $this->czsecurity->xssCleanPostInput('friendlyName');
			$input_data['taxRate']	   = $this->czsecurity->xssCleanPostInput('taxRate');
			$input_data['date'] = date('Y-m-d');
				$input_data['agency'] = $this->czsecurity->xssCleanPostInput('taxAgency');

					if($this->czsecurity->xssCleanPostInput('taxID')!="" )
					{
					    
				      
			        $id = $this->czsecurity->xssCleanPostInput('taxID');

					$data1 = $this->general_model->get_row_data('QBO_token',$val);

					$accessToken = $data1['accessToken'];
					$refreshToken = $data1['refreshToken'];
				
					 $realmID      = $data1['realmID']; 

					$dataService = DataService::Configure(array(
					'auth_mode' => 'oauth2',
					'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
					'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
					'accessTokenKey' =>  $accessToken,
					'refreshTokenKey' => $refreshToken,
					'QBORealmID' => $realmID,
					'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"

					));

					$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

					$entities = $dataService->Query("SELECT * FROM TaxRate where Id='".$id."'");
					
				    
					if(empty($entities)) exit();//No Record for the Customer with Id = 48

						//Get the first element
					$theTaxes = reset($entities);
						 $TaxRateDetails = array();
                        $rnd = rand();
                                              
                        
                        $TaxService = TaxService::update($theTaxes, [
                          "TaxRateDetails" => [
                          	 "TaxRateName" =>  $input_data['friendlyName'],
                             "RateValue" =>  $input_data['taxRate']
                          ]
                        ]);

					$resultingCustomerUpdatedObj = $dataService->Update($TaxService);

					
					$error = $dataService->getLastError();
					if ($error != null) {
						echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
						echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
						echo "The Response message is: " . $error->getResponseBody() . "\n";
						exit();
					}
					
						$this->session->set_flashdata('success','Successfully Updated');
			    	
					}
					else
					{
						$data1 = $this->general_model->get_row_data('QBO_token',$val);

					$accessToken = $data1['accessToken'];
					$refreshToken = $data1['refreshToken'];
                   $realmID      = $data1['realmID']; 
					$dataService = DataService::Configure(array(
					'auth_mode' => 'oauth2',
					'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
					'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
					'accessTokenKey' =>  $accessToken,
					'refreshTokenKey' => $refreshToken,
					'QBORealmID' =>  $realmID ,
					'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"

					));

					$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                        $TaxRateDetails = array();
                        $rnd = rand();
                        
                           $currentTaxServiceDetail = TaxRate::create([
                             "TaxRateName" =>  $input_data['friendlyName'],
                             "RateValue" =>  $input_data['taxRate'],
                             "TaxAgencyId" => $input_data['agency'],
                             "TaxApplicableOn" => "Sales"
                           ]);
                           $TaxRateDetails[] = $currentTaxServiceDetail;
                      
                        
                        $TaxService = TaxService::create([
                          "TaxCode" => $input_data['friendlyName'],
                          "TaxRateDetails" => $TaxRateDetails
                        ]);

						$result = $dataService->Add($TaxService);
						
						$error = $dataService->getLastError();
						if ($error != null) {
						     $this->session->set_flashdata('message', '<div class="alert alert-danger"><Strong>Error:</strong>  Your Friendly Name Already Exists</div>');	
						    redirect(base_url('QBO_controllers/Tax/tax_list'));
						}

                         $this->session->set_flashdata('success', 'Your Information Stored successfully');						 
						 
					}
					   redirect(base_url('QBO_controllers/Tax/tax_list'));
		
		}
		
	}
			
 }
 
 
	public function get_tax_id()
    {
		  if($this->session->userdata('logged_in') ){
	                    
			           $user_id = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			        	
			           $user_id = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
        $id = $this->czsecurity->xssCleanPostInput('taxID');
		$val = array(
		'taxID' => $id,'merchantID'=>$user_id
		);
		
		$data = $this->general_model->get_row_data('tbl_taxes_qbo',$val);
        echo json_encode($data);
    }
	
	
	
public function delete_tax(){
	
		         if($this->session->userdata('logged_in') ){
	                    
			           $user_id = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			        	
			           $user_id = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
		 	$id = $this->czsecurity->xssCleanPostInput('tax_id');
	
	if(!empty($id))
	{
			$condition =  array('taxID'=>$id); 
		
			$merchID = $user_id;
			$val = array(
				'merchantID' => $merchID,
			);

			$data1 = $this->general_model->get_row_data('QBO_token',$val);

			$accessToken = $data1['accessToken'];
			$refreshToken = $data1['refreshToken'];
		    $realmID      = $data1['realmID']; 

			$dataService = DataService::Configure(array(
			'auth_mode' => 'oauth2',
			'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
			'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' =>  $realmID,
			'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"

			));

			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

			$targetTaxArray = $dataService->Query("SELECT * FROM TaxRate where Id='".$id."'");
        
            $theTax = reset($targetTaxArray);
            $updatetax = TaxRate::update($theTax, [
                'Active' => 'false',
               
            ]);
          
        $currentResultObj = $dataService->Update($updatetax);	
	
		$error = $dataService->getLastError();
		
	
	   $err='';
		if ($error != null) {
		    $err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
 			$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
 			$err.="The Response message is: " . $error->getResponseBody() . "\n";
		 $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> '.$err.'</div>');  
		}
		else{
           $this->session->set_flashdata('success', 'Successfully Deleted');             
	  	}
	  	
	}else{
	    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Validation Error:</strong> Tax Reference is not available </div>');   
	} 	
	  
	  	redirect(base_url('QBO_controllers/Tax/tax_list'));
    	 

	}
	
	
	
}