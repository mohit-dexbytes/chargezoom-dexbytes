<?php
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;


class UsaePay extends CI_Controller
{
	private $merchantID;
    private  $resellerID;
	private $transactionByUser;

	public function __construct()
	{
		parent::__construct();
	    
		
	    require_once APPPATH."third_party/usaepay/usaepay.php";	
		$this->load->config('usaePay');
		$this->load->model('quickbooks');
	  
		$this->load->model('general_model');
	     $this->load->model('card_model');
		$this->load->model('QBO_models/qbo_customer_model');
		$this->load->library('form_validation');
        $this->db1 = $this->load->database('otherdb', TRUE);
	     	if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
			   	$logged_in_data = $this->session->userdata('logged_in');
	            $this->resellerID = $logged_in_data['resellerID'];
				$merchID = $logged_in_data['merchID'];
	            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
			  	$logged_in_data = $this->session->userdata('user_logged_in');
	            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
	            $merchID = $logged_in_data['merchantID'];
	            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
	            $this->resellerID = $rs_Data['resellerID'];
		  }else{
			redirect('login','refresh');
		  }
		$this->merchantID = $merchID;
	}
	
	
	public function index(){
	    
	
	   	redirect('QBO_controllers/home','refresh'); 
	}
	
	
	
	
	public function pay_invoice()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           
			$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
		       
		       
		     	 $custom_data_fields = [];
		         $cusproID=''; $error='';
		    	 $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	 $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     
				  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
				  if (!$cardID || empty($cardID)) {
				  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
				  }
		  
				  
				  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
				  if (!$gatlistval || empty($gatlistval)) {
				  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
				  }
				  $gateway = $gatlistval;
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			     $in_data =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $user_id);
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
			   
			      $ref_number= array();
			    if(!empty( $in_data)) 
			    {
			          
    			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    				  $username   = $gt_result['gatewayUsername'];
    			      $password   = $gt_result['gatewayPassword'];
    			      $customerID =  $in_data['Customer_ListID'];
    			     
    			     $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
        			 $c_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
        		   	 $companyID  = $c_data['companyID']; 
    			   
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$address2 = $card_data['Billing_Addr2'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			    }      
    			    $cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;                        
         
                		if( $in_data['BalanceRemaining'] > 0)
        				{
				    
                    	$crtxnID='';  
                        $invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						$transaction->key=$username;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
						
						
					
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
							
							$cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$transaction->cvv2 = $cvv;
							}
                           
						
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zipcode;
						
							
					     	$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zipcode;
						
							$transaction->amount = $amount;
						 
							$transaction->Process();
					
                     if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                            $txnID      = $in_data['invoiceID'];  
							 $ispaid 	 = 1;
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 0;
							 $app_amount = $in_data['Total_payment']+$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('invoiceID'=>$in_data['invoiceID'],'merchantID'=>$user_id  );
							 
							 $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
                          
                       $val = array(
							'merchantID' => $user_id,
						);
						$data = $this->general_model->get_row_data('QBO_token',$val);

						$accessToken  = $data['accessToken'];
						$refreshToken = $data['refreshToken'];
						$realmID      = $data['realmID'];
						$dataService  = DataService::Configure(array(
						 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
						));
		
        			    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        	            
        	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
        				
        				
        				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        				{
        					$theInvoice = current($targetInvoiceArray);
        			
                				
                				
								$createPaymentObject = [
                				    "TotalAmt" => $amount,
                				    "SyncToken" => 1, 
                				    "CustomerRef" => $customerID,
                				    "Line" => [
                				     "LinkedTxn" =>[
                				            "TxnId" => $invoiceID,
                				            "TxnType" => "Invoice",
                				        ],    
                				       "Amount" => $amount
                			        ]
								];
                
                                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if(isset($trID) && $trID != '')
                                {
                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                }
                                $newPaymentObj = Payment::create($createPaymentObject);
                             
                				$savedPayment = $dataService->Add($newPaymentObj);
                				
                		    	$crtxnID=	$savedPayment->Id;
                				
                				$error = $dataService->getLastError();
                              
                			    if ($error != null) 
                			    {
                	
                				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                
                				$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); 
                				
                				$qbID=$trID;
                				
                			   }
                    		   else 
                    		   {
									   $st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
									   $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
									   $ref_number =  $in_data['refNumber'];
									  $tr_date   =date('Y-m-d H:i:s');
										  $toEmail = $c_data['userEmail']; $company=$c_data['companyName']; $customer = $c_data['fullName'];  
                    		        	
										
                    				$this->session->set_flashdata('success','Successfully Processed Invoice'); 
                    			}
                                      
                          
        			    	}    
                          
							 
							 $qbID=$invoiceID; 
                     }
                     else
                     {
                           $msg = $transaction->result;
                           $trID = $transaction->refnum;
                           $res =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                           
                         
                     }		
						
                       $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$invoiceID, false, $this->transactionByUser, $custom_data_fields);
					   
					   	if($res['transactionCode'] == 200 && $chh_mail =='1')
						{
							$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $res['transactionId']);
						}
                  
		             }
            		    else        
                        {
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                            
                        }
			    }
			    else
			    {
			       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');  
			        
			    }
		   
		   }
		   else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			
			 if($cusproID=="2"){
			 	 redirect('QBO_controllers/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" ){
			 	redirect('QBO_controllers/Create_invoice/invoice_details_page/'.$in_data['invoiceID'],'refresh');
			 }
			 $trans_id = $transaction->refnum;
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'QBO_controllers/Create_invoice/Invoice_details',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
					 'checkPlan'  => $checkPlan
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		  
	      
	}
	
	
	
	
	
	

	public function create_customer_sale()
	{ 
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	  
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
			$checkPlan = check_free_plan_transactions();
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
	   			$custom_data_fields = [];
	   			$applySurcharge = false;
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$applySurcharge = true;
					$custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
				}

				if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
					$custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
				}
        			   if($this->czsecurity->xssCleanPostInput('setMail'))
			             $chh_mail =1;
			             else
			             $chh_mail =0;     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   if($this->czsecurity->xssCleanPostInput('tc')) 
            			 $tc = 1;
            			 else
            			 $tc = 0; 
        			
        			if(!empty($gt_result) )
        			{
        			     $user      = $gt_result['gatewayUsername'];
        				 $password   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        			
        			     $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$user_id) ;
        			 $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
        		   	$companyID  = $comp_data['companyID'];
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        				 
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('baddress1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('bcity');
    	                        $country =$this->czsecurity->xssCleanPostInput('bcountry');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('bphone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('bstate');
								$zipcode =  $this->czsecurity->xssCleanPostInput('bzipcode');
								$cardType = $this->general_model->getType($card_no);
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$address2 = $card_data['Billing_Addr2'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        					}
        				/*Added card type in transaction table*/
		                $cardType = $this->general_model->getType($card_no);
		                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
		                $custom_data_fields['payment_type'] = $friendlyname;
                	 
						$invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						$transaction->key=$user;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
							
							$cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$transaction->cvv2 = $cvv;
							}

							$transaction->billcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->billfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->billlname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->billstreet = $this->czsecurity->xssCleanPostInput('baddress1');
							$transaction->billstreet2 = $this->czsecurity->xssCleanPostInput('baddress2');
							$transaction->billcountry = $this->czsecurity->xssCleanPostInput('bcountry');
							$transaction->billcity = $this->czsecurity->xssCleanPostInput('bcity');
							$transaction->billstate = $this->czsecurity->xssCleanPostInput('bstate');
							$transaction->billzip = $this->czsecurity->xssCleanPostInput('bzipcode');
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->shipcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->shipfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->shiplname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->shipstreet = $this->czsecurity->xssCleanPostInput('address1');
							$transaction->shipstreet2 = $this->czsecurity->xssCleanPostInput('address2');
							$transaction->shipcountry = $this->czsecurity->xssCleanPostInput('country');
							$transaction->shipcity = $this->czsecurity->xssCleanPostInput('city');
							$transaction->shipstate = $this->czsecurity->xssCleanPostInput('state');
							$transaction->shipzip = $this->czsecurity->xssCleanPostInput('zipcode');
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('phone');
							$amount = $this->czsecurity->xssCleanPostInput('totalamount');
							// update amount with surcharge 
			                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
			                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
			                    $amount += round($surchargeAmount, 2);
			                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
			                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
			                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
			                    
			                }
			                $totalamount  = $amount;
                            
                            $transaction->email = $this->czsecurity->xssCleanPostInput('email');
							
							$transaction->amount = $amount;

		            		if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
		            			$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $user_id, 1);
								$transaction->orderid = $new_invoice_number;
							}

							if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
								$transaction->ponum = $this->czsecurity->xssCleanPostInput('po_number');
							}

							$transaction->Process();
        				  
            	          	$crtxnID='';
            		if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                       
                       	
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                       
                				 /* This block is created for saving Card info in encrypted form  */
            				   
                                    
							 $invoiceIDs=array();
							 $invoicePayAmounts = array();
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
								$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
								$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount')); 
        				     }
						 
				          
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                               
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			          
                 
				           if(!empty($invoiceIDs))
				            {
				            	$saleAmountRemaining = $amount;
				            	$payIndex = 0;
                                foreach ($invoiceIDs as $inID) {
                                    $theInvoice = array();
                                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                                    $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                                    $con = array('invoiceID' => $inID, 'merchantID' => $user_id);
                                    $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount' ), $con);

                                    
                                    $refNumber[]   =  $res1['refNumber'];
                                    $txnID      = $inID;
                                    $pay_amounts = 0;


                                    $amount_data = $res1['BalanceRemaining'];
                                    $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                                    if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

		                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
		                                $actualInvoicePayAmount += $surchargeAmount;
		                                $amount_data += $surchargeAmount;
		                                $updatedInvoiceData = [
		                                    'inID' => $inID,
		                                    'targetInvoiceArray' => $targetInvoiceArray,
		                                    'merchantID' => $user_id,
		                                    'dataService' => $dataService,
		                                    'realmID' => $realmID,
		                                    'accessToken' => $accessToken,
		                                    'refreshToken' => $refreshToken,
		                                    'amount' => $surchargeAmount,
		                                ];
		                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
		                            }
                                    $ispaid      = 0;
                                    $isRun = 0;
                                    if($saleAmountRemaining > 0){
                                        $BalanceRemaining = 0.00;
		                                if($amount_data == $actualInvoicePayAmount){
		                                    $actualInvoicePayAmount = $amount_data;
		                                    $isPaid      = 1;

		                                }else{

		                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
		                                    $isPaid      = 0;
		                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
		                                    
		                                }
		                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

		                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

		                                
		                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                        if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                            $theInvoice = current($targetInvoiceArray);

											$createPaymentObject = [
                                                "TotalAmt" => $actualInvoicePayAmount,
                                                "SyncToken" => 1, 
                                                "CustomerRef" => $customerID,
                                                "Line" => [
                                                    "LinkedTxn" => [
                                                        "TxnId" => $inID,
                                                        "TxnType" => "Invoice",
                                                    ],
                                                    "Amount" => $actualInvoicePayAmount
                                                ]
                                            ];
							
											$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
											if($paymentMethod){
												$createPaymentObject['PaymentMethodRef'] = [
													'value' => $paymentMethod['payment_id']
												];
											}
											if(isset($trID) && $trID != '')
			                                {
			                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
			                                }
											$newPaymentObj = Payment::create($createPaymentObject);

                                            $savedPayment = $dataService->Add($newPaymentObj);

                                            $error = $dataService->getLastError();
                                            if ($error != null) {
                                                $err = '';
                                                $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                                $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                                $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                                $st = '0';
                                                $action = 'Pay Invoice';
                                                $msg = "Payment Success but " . $error->getResponseBody();
                                                $qbID = $trID;
                                                $pinv_id = '';
                                            } else {
                                                $pinv_id = '';
                                                $crtxnID = $pinv_id = $savedPayment->Id;
                                                $st = '1';
                                                $action = 'Pay Invoice';
                                                $msg = "Payment Success";
                                                $qbID = $trID;
                                            }
                                            $qbID = $inID;
                                            $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                            $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                            if($syncid){
                                                $qbSyncID = 'CQ-'.$syncid;

                                                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                            }
                                            
                                            $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$actualInvoicePayAmount,$user_id,$pinv_id, $this->resellerID,$inID, false, $this->transactionByUser, $custom_data_fields);
                                            
                                        }
                                    }
                                    $payIndex++;

                                }
				            }
				          else
				          {
				              
				                    $crtxnID='';
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
				          }
				            
            				      
						 
						 
            				       
            			         if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
            				 		
            				        $friendlyname   =  $this->czsecurity->xssCleanPostInput('friendlyname');
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										 
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            			 	if($chh_mail =='1')
            						 {
            						   
            						  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
            						  $ref_number = implode(',',$refNumber); 
            						  $tr_date   =date('Y-m-d H:i:s');
            						  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
            						   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
            						 } 
									 $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
									 $ref_number = ''; 
									 $tr_date   =date('Y-m-d H:i:s');
										 $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
									 
            				    $this->session->set_flashdata('success','Transaction Successful'); 
            			
            				 } 
            				 else
                            {
                               $msg = $transaction->result;
                               $trID = $transaction->refnum;
                               $result =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
                               $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                $id = $this->general_model->insert_gateway_transaction_data($result,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                  
                         
                          }	
                         
                         
                         
                           
                           
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			
					$invoice_IDs = array();
					if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
						$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
					}
				
					$receipt_data = array(
						'transaction_id' => $transaction->refnum,
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_sale',
						'proccess_btn_text' => 'Process New Sale',
						'sub_header' => 'Sale',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
	    
    	}
	
	 
	
	/******************* usaepay refund function start ********************/
	
   public function create_customer_refund()
   {
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
		        if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
				$tID       = $this->czsecurity->xssCleanPostInput('usaepayID');
				$con      = array('transactionID'=>$tID);
				$paydata   = $this->general_model->get_row_data('customer_transaction',$con);
				$gatlistval  = $paydata['gatewayID'];
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
				$paypassword  =  $gt_result['gatewayPassword'];
				$grant_type    = "password";
		
		    	 $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$payusername; 		// Your Source Key
                 $transaction->pin= $paypassword;		// Source Key Pin
                 $transaction->usesandbox=true;		// Sandbox true/false
                 $transaction->testmode=0;    // Change this to 0 for the transaction to process
                 
                 $transaction->command="refund";    // refund command to refund transaction.
                  
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.

			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
			 
				 if($transaction->Process()){
				    $transactionCode ='200';
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"2",'transactionModified'=>date('Y-m-d H:i:s') );
					
					  $this->qbo_customer_model->update_refund($trID, 'USAEPAY');	
					$this->session->set_flashdata('success','Success'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				      $transactiondata= array();
				      $transactiondata['transactionID']       = $transaction->refnum;
				       $transactiondata['transactionStatus']    = $transaction->authcode;
					  $transactiondata['transactionDate']    = date('Y-m-d h:i:s');  
					  $transactiondata['transactionType']    = 'usaepay_refund';
					  $transactiondata['transactionGateway']= $gt_result['gatewayType'];
					  $transactiondata['gatewayID']        = $gatlistval;
					  $transactiondata['transactionCode']   = $transactionCode;
					  $transactiondata['customerListID']     = $customerID;
					  $transactiondata['transactionAmount']  = $amount;
					  $transactiondata['merchantID']  = $merchantID;
					  $transactiondata['resellerID']   = $this->session->userdata('logged_in')['resellerID'];
					  $transactiondata['gateway']   = "USAePay";  
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}     
						if($custom_data_fields){
			                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			             }  
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			
					   $invoice_IDs = array();
					 
				   
					   $receipt_data = array(
						   'proccess_url' => 'QBO_controllers/Payments/payment_refund',
						   'proccess_btn_text' => 'Process New Transaction',
						   'sub_header' => 'Capture',
					   );
					   
					   $this->session->set_userdata("receipt_data",$receipt_data);
					   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					   
					   if($paydata['invoiceTxnID'] == ''){
						$paydata['invoiceTxnID'] ='null';
						}
						if($paydata['customerListID'] == ''){
							$paydata['customerListID'] ='null';
						}
						if($transaction->refnum == ''){
							$transaction->refnum ='null';
						}
					   redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$transaction->refnum,  'refresh');
					
		}
	}
	
	
	/********************** end refunc function *******************/
	
	
	
	
	
	
/************************* auth function ***********************/	
	
	
	
	public function create_customer_auth()
	{ 
	      
	    $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
				
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
            
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
             
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           
			$checkPlan = check_free_plan_transactions();
			$custom_data_fields = [];
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {

				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					
				}

				$po_number = $this->czsecurity->xssCleanPostInput('po_number');
	            if (!empty($po_number)) {
	                $custom_data_fields['po_number'] = $po_number;
	            }
        			     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			  
        			if(!empty($gt_result) )
        			{
        			     $user      = $gt_result['gatewayUsername'];
        				 $password   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				$comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('merchantID'=>$user_id,'Customer_ListID' => $customerID));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$address2 = $card_data['Billing_Addr2'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        					}
        					$cardType = $this->general_model->getType($card_no);
			                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
			                $custom_data_fields['payment_type'] = $friendlyname;
        					 
						$invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$user;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="authonly";	
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
							
							$cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$transaction->cvv2 = $cvv;
							}

							$transaction->billcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->billfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->billlname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->billstreet = $this->czsecurity->xssCleanPostInput('baddress1');
							$transaction->billstreet2 = $this->czsecurity->xssCleanPostInput('baddress2');
							$transaction->billcountry = $this->czsecurity->xssCleanPostInput('bcountry');
							$transaction->billcity = $this->czsecurity->xssCleanPostInput('bcity');
							$transaction->billstate = $this->czsecurity->xssCleanPostInput('bstate');
							$transaction->billzip = $this->czsecurity->xssCleanPostInput('bzipcode');
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->billphone = $this->czsecurity->xssCleanPostInput('bphone');
							
							$transaction->shipcompany = $this->czsecurity->xssCleanPostInput('companyName');
							$transaction->shipfname = $this->czsecurity->xssCleanPostInput('firstName');
							$transaction->shiplname = $this->czsecurity->xssCleanPostInput('lastName');
							$transaction->shipstreet = $this->czsecurity->xssCleanPostInput('address1');
							$transaction->shipstreet2 = $this->czsecurity->xssCleanPostInput('address2');
							$transaction->shipcountry = $this->czsecurity->xssCleanPostInput('country');
							$transaction->shipcity = $this->czsecurity->xssCleanPostInput('city');
							$transaction->shipstate = $this->czsecurity->xssCleanPostInput('state');
							$transaction->shipzip = $this->czsecurity->xssCleanPostInput('zipcode');
							$transaction->shipphone = $this->czsecurity->xssCleanPostInput('phone');
							$amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $transaction->email = $this->czsecurity->xssCleanPostInput('email');
							
							$transaction->amount = $amount;
							if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
								$transaction->orderid = $this->czsecurity->xssCleanPostInput('invoice_number');
							}

							if (!empty($po_number)) {
								$transaction->ponum = $po_number;
							}
							$transaction->Process();
        				   
            	          	$crtxnID='';
            				
            			  	 	     
					    if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                        {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				  
                				 /* This block is created for saving Card info in encrypted form  */
            				    
            				       
            			         if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
            				 		
            				        $friendlyname   =  $this->czsecurity->xssCleanPostInput('friendlyname');
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            						$custom_data_fields['card_type'] = $cardType;
            				
            				 }
            				    $this->session->set_flashdata('success','Transaction Successful'); 
            			
            				 } 
            				 else
            				 {
                                    	     
				
                        
                                     $msg = $transaction->result;
                                     $trID = $transaction->refnum;
                                     
                                     $res =array('transactionCode'=>'300', 'status'=>$msg, 'transactionId'=> $trID );
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                      
                                     
                              }
                         
                         
                           $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);  
                           
                           
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
					$invoice_IDs = array();
					
					$receipt_data = array(
						'transaction_id' => $transaction->refnum,
						'IP_address' => getClientIpAddr(),
						'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
						'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
						'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
						'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
						'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
						'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
						'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
						'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
						'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
						'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
						'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
						'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
						'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
						'Contact' => $this->czsecurity->xssCleanPostInput('email'),
						'proccess_url' => 'QBO_controllers/Payments/create_customer_auth',
						'proccess_btn_text' => 'Process New Transaction',
						'sub_header' => 'Authorize',
						'checkPlan'  => $checkPlan
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					
					redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');

	    
	    
    	}
	
	 
	
    
    /****************  usaepay auth end ******************/
	
	/******************** usaepay customer capture function start***************************/
	
	
	
	public function create_customer_capture()
	{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('usatxnID', 'Transaction ID', 'required|xss_clean');
			
               
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('usatxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                 
                 $transaction->command="capture";   // void command cancels a pending transaction.
                 //$transaction->command="void:release";    // void:release speeds up the process of releasing customer's funds.   
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
					$customerID = $paydata['customerListID'];
					$tr_date   =date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
						if($chh_mail =='1')
                            {
                              
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
							}
					$this->session->set_flashdata('success','Successfully Captured'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				 
				   $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
				 
				   $invoice_IDs = array();
				  
			   
				   $receipt_data = array(
					   'proccess_url' => 'QBO_controllers/Payments/payment_capture',
					   'proccess_btn_text' => 'Process New Transaction',
					   'sub_header' => 'Capture',
				   );
				   
				   $this->session->set_userdata("receipt_data",$receipt_data);
				   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				   
				   if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($transaction->refnum == ''){
						$transaction->refnum ='null';
					}
				   redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$transaction->refnum,  'refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('QBO_controllers/Payments/payment_capture','refresh');
			
	}
	
	
	
	/***************************** usaepay customer capture end***********************************/
	
	
	
	/**************************** uasepay  void Payment **************************/
	public function create_customer_void()
	{
		$custom_data_fields = [];
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('usavoidID', 'Transaction ID', 'required|xss_clean');
			
               
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('usavoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			
			   
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
    			 $nmiuser  = $gt_result['gatewayUsername'];
    		     $nmipass  =  $gt_result['gatewayPassword'];
				 
    		     $transaction=new umTransaction;
				 $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
 
                 $transaction->key=$nmiuser; 		// Your Source Key
                 $transaction->pin= $nmipass;		// Source Key Pin
                 $transaction->usesandbox=$this->config->item('Sandbox');		// Sandbox true/false
                 $transaction->testmode=$this->config->item('TESTMODE');    // Change this to 0 for the transaction to process
                 
                 $transaction->command="void";    // void command cancels a pending transaction.
                 
                 $transaction->refnum=$tID;		// Specify refnum of the transaction that you would like to capture.
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			 
		     	  $transaction->Process();
				  if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                  {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
					$condition = array('transactionID'=>$tID);
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
						if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
                            
					$this->session->set_flashdata('success','Successfully Voided'); 
				 }else{
				    $transactionCode ='000';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$transaction->error.'</div>'); 
				 }
				 
				   $id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser, $custom_data_fields);
				 
			
				
					redirect('QBO_controllers/Payments/payment_capture','refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Voided";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('QBO_controllers/Payments/payment_capture','refresh');
			
	}
	
	
	/**************************** usaepay void payment function end*********************/
	
	
     
	public function pay_multi_invoice()
	{
	 if($this->session->userdata('logged_in')){
					
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
    
       	$this->form_validation->set_rules('totalPay', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway1', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('CardID1', 'Card ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('CardID1')=="new1")
            { 
        
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
                            
             $cusproID=''; 
             $custom_data_fields = [];
             $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID'); 
			$checkPlan = check_free_plan_transactions();
               
	    	if ($checkPlan && $this->form_validation->run() == true)
		    {
    
            $cardID_upd ='';
            $invoices           = $this->czsecurity->xssCleanPostInput('multi_inv');
            $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
            $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
 
            $customerID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
          

            $error='';
$txnID='';
               $invoiceIDs =implode(',', $invoices);
            
			   $inv_data   =    $this->qbo_customer_model->get_due_invoice_data($invoiceIDs,$user_id);
			   
			    if(!empty( $invoices) && !empty($inv_data)) 
			    {
                    $cusproID      =   $customerID  = $inv_data['CustomerListID']; 
            $comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' =>$customerID , 'merchantID'=>$user_id));
            $companyID  = $comp_data['companyID'];
     
             $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   
			if(!empty($gt_result))
        	{
        	                 $username   = $gt_result['gatewayUsername'];
                            $password   = $gt_result['gatewayPassword'];
                            $Customer_ListID = $customerID;
                          $amount  = $this->czsecurity->xssCleanPostInput('totalPay');
                          
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    						$address2 = $card_data['Billing_Addr2'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			    }          
    			    $cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;                    
         
                	
				    
                    	$crtxnID='';  
                        $invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$username;
						$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
						
						$transaction->pin=$password;
						$transaction->usesandbox=$this->config->item('Sandbox');
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Multi Invoice Payment";	// description of charge
					
						$transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
						$transaction->command="sale";	
						
						
					
                            $transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
							
							$cvv = trim($cvv);
							if($cvv && !empty($cvv)){
								$transaction->cvv2 = $cvv;
							}

						
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zipcode;
							
							
					     	$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zipcode;
						
							
							$transaction->amount = $amount;
						 
							$result = $transaction->Process();
					
                     if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                     {
                        
                         $msg = $transaction->result;
                         $trID = $transaction->refnum;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                       
                                     
                        	$c_data     = $this->general_model->get_select_data('QBO_custom_customer',array('companyID'), array('Customer_ListID'=>$customerID, 'merchantID'=>$user_id));
            		    	$companyID = $c_data['companyID'];
            			
            			
				             	$val = array('merchantID' => $user_id);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
            							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						
			            
                 
				           if(!empty($invoices))
				           {
				               
				              foreach($invoices as $invoiceID)
				              {
        				                $theInvoice = array();
        							  
        						       
        							   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '".$invoiceID."'");
        							    $condition  = array('invoiceID'=>$invoiceID,'merchantID'=>$user_id );
        						            $in_data =$this->general_model->get_select_data('QBO_test_invoice',array('BalanceRemaining','Total_payment'),$condition);
        								     $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                             $txnID       = $invoiceID;  
            						            $ispaid   = 1;
            							
            						    	$bamount    = $in_data['BalanceRemaining']-$pay_amounts;
            							 if($bamount > 0)
            							  $ispaid 	 = 0;
            							 $app_amount = $in_data['Total_payment']+$pay_amounts;
            							 $data   	 = array('IsPaid'=>$ispaid, 'Total_payment'=>($app_amount) , 'BalanceRemaining'=>$bamount );
            							 $to_amount = $in_data['BalanceRemaining']+$in_data['Total_payment'];
            							 
        							    $this->general_model->update_row_data('QBO_test_invoice',$condition, $data);
        							 		
        								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        								{
        									$theInvoice = current($targetInvoiceArray);
        								
            								
											$createPaymentObject = [
            									"TotalAmt" => $to_amount,
            									"SyncToken" => 1,
            									"CustomerRef" => $customerID,
            									"Line" => [
            									 "LinkedTxn" =>[
            											"TxnId" => $invoiceID,
            											"TxnType" => "Invoice",
            										],    
            									   "Amount" => $pay_amounts
            									]
											];
							
											$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
											if($paymentMethod){
												$createPaymentObject['PaymentMethodRef'] = [
													'value' => $paymentMethod['payment_id']
												];
											}
											if(isset($trID) && $trID != '')
			                                {
			                                    $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
			                                }
											$newPaymentObj = Payment::create($createPaymentObject);
            							 
            								$savedPayment = $dataService->Add($newPaymentObj);
        								
        								
        								
            								$error = $dataService->getLastError();
            								if ($error != null)
            						   	{
            							    $err='';
            					        	$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
            									$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
            								$err.="The Response message is: " . $error->getResponseBody() . "\n";
            								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>'.	$err.'</div>');
            
            							$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody();
            							
            							$qbID=$trID;	
            								$pinv_id='';
            							}
            							else 
            							{ 
            							      $pinv_id='';
            							       $pinv_id        =  $savedPayment->Id;
            							    	$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$trID;
            								$this->session->set_flashdata('success','Transaction Successful'); 
            							
            							}
            							  $qbID = $invoiceID;
            					          $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$user_id,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				                            $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
				                            if($syncid){
						                        $qbSyncID = 'CQ-'.$syncid;
						                        
						                        

                                				$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

						                       
						                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
						                    }
            							   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$pay_amounts,$user_id,$pinv_id, $this->resellerID,$txnID, false, $this->transactionByUser, $custom_data_fields);     		
            							
        						}
				              }
						 
				            }
                              
                                 	 if( $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				        {
            				 
                    				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
                    				 		
                    				        $friendlyname   =  $this->czsecurity->xssCleanPostInput('friendlyname');
                    				        $cardType       = $this->general_model->getType($card_no);
                    						$card_condition = array(
                    										 'customerListID' => $customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    										
                    							$cid      =    $customerID;			
                    							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
                    						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
                    							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
                    					
                    						$crdata =	$this->card_model->chk_card_firendly_name($cid,$friendlyname);			
                					   
                					
                    					 
                    						if($crdata > 0)
                    						{
                    							
                    						  $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'    =>$cardType,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'',
                    										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                    	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                    	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                    	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                    	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                    	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                    	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                    										 'customerListID' =>$customerID, 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                    									
                    					
                    						   $this->card_model->update_card_data($card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    					      $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'    =>$cardType,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 
                    										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                    	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                    	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                    	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                    	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                    	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                    	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                    										 'customerListID' =>$customerID,
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										 'createdAt' 	=> date("Y-m-d H:i:s") );
                    										 
                    							 		 
                    								
                    				            $id1 = $this->card_model->insert_card_data($card_data);	
                    						
                    						}
            				
            				          }
            				    
            				    	  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
            				 	
            						
                                 }
                                 else
                                 {
                                     
                                     $msg = $transaction->result;
                                     $trID = $transaction->refnum;
                                     
                                     $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.' </strong></div>'); 
                                      }		
            						
                              $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$txnID, false, $this->transactionByUser, $custom_data_fields);      
                               
            			
            				           
            		    }
                		    else        
                            {
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Payment Authentication Failed! </strong>.</div>');
                                
                            }
                     
             }
             else
             {
                $error='Invalid Invoice';
                  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
              }
            }
    		else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			   
			if(!$checkPlan){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
			
		    	if($cusproID!="")
				 {
					 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 	redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
				 }
	      
	}
	
	
	
}

